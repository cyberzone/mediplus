﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
namespace Mediplus.WebReports
{
    public partial class ScandCardPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ViewState["InsCardPath"] = (string) System.Configuration.ConfigurationSettings.AppSettings["InsCardPath"] ;

                string PT_ID = Convert.ToString(Request.QueryString["PT_ID"]);

                if (GlobalValues.HospitalName == "SMCH")
                {
                    imgFront.Height = 380;
                    imgFront.Width = 560;

                    imgBack.Height = 380;
                    imgBack.Width = 560;
                }

                //imgFront.ImageUrl = @Convert.ToString(ViewState["InsCardPath"])  + Convert.ToString(Session["InsCardFrontPath"]);

                //imgBack.ImageUrl =@Convert.ToString(ViewState["InsCardPath"])+ Convert.ToString(Session["InsCardBackPath"]);

                imgFront.ImageUrl = "../HMS/DisplayOtherCardImage.aspx?PT_ID=" + PT_ID + "&FileName=" + Convert.ToString(Session["InsCardFrontPath"]);
                imgBack.ImageUrl = "../HMS/DisplayOtherCardImage1.aspx?PT_ID=" + PT_ID + "&FileName=" + Convert.ToString(Session["InsCardBackPath"]);
            }

        }
    }
}