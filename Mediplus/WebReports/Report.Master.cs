﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus.WebReports
{
    public partial class Report : System.Web.UI.MasterPage
    {
        public string strTitle = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

            strTitle  = strName;

        }
    }
}