﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.WebReports
{
    public partial class eAuthApprovalOP : System.Web.UI.Page
    {
        #region Methods

        void BindPTDetails()
        {
            string Criteria = " 1=1 ";// AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + hidPT_ID.Value + "' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                //if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                //    lblNationality.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                //lblProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //lblSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                //lblMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                //lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                lblDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                // lblPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                lblPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);


            }

        }

        void BindeAuthorization()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HAR_ID  ='" + hideAuthHARID.Value + "'";



            DataSet DS = new DataSet();
            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationRequestGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                hidPT_ID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HAR_PT_ID"]);
                lblPTNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_PT_ID"]);
                lbleAuthStDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_START_DATEDesc"]);
                lbleAuthEndDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_END_DATEDesc"]);
                lblAuthorizationNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_ID_PAYER"]);
                lblRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_FILE_ID"]);
                lblInsName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_INS_NAME"]);
               // lblAppStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["ApprovalStatus"]);
                lblAppStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_APPROVAL_STATUS"]);

                lblComment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_FILE_COMMENTS"]);
                lblTreatmentType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_ENCOUNTER_TYPEDesc"]);
                lblPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_MEMBER_ID"]);

                //objeAuth.HAR_PT_ID = Convert.ToString(DS.Tables[0].Rows[0]["HAR_PT_ID"]);
                //objeAuth.HAR_START_DATE = Convert.ToString(DS.Tables[0].Rows[0]["HAR_START_DATEDesc"]);
                //objeAuth.HAR_END_DATE = Convert.ToString(DS.Tables[0].Rows[0]["HAR_END_DATEDesc"]);
                //objeAuth.HAR_ID_PAYER = Convert.ToString(DS.Tables[0].Rows[0]["HAR_ID_PAYER"]);
                //objeAuth.HAR_FILE_ID = Convert.ToString(DS.Tables[0].Rows[0]["HAR_ID_PAYER"]);
                //objeAuth.HAR_INS_NAME = Convert.ToString(DS.Tables[0].Rows[0]["HAR_INS_NAME"]);
                //objeAuth.ApprovalStatus = Convert.ToString(DS.Tables[0].Rows[0]["ApprovalStatus"]);
                //objeAuth.HAR_FILE_COMMENTS = Convert.ToString(DS.Tables[0].Rows[0]["HAR_FILE_COMMENTS"]);


            }
            else
            {

            }


        }

        void BindeAuthDiagnosis()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HAD_HAR_ID='" + hideAuthHARID.Value + "'";

            DataSet DS = new DataSet();
            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gveAuthDiagnosis.DataSource = DS;
                gveAuthDiagnosis.DataBind();

            }
            else
            {
                gveAuthDiagnosis.DataBind();
            }


        }

        void BindeAuthActivities()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HAA_HAR_ID='" + hideAuthHARID.Value + "'";
            DataSet DS = new DataSet();

            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationActivitiesGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                //string strObsValue = "", strObsCC = "", strObsMangPlan = "";
                //strObsValue = Convert.ToString(DS.Tables[0].Rows[0]["HAA_OBS_VALUE"]);

                //string[] arrObsValue;
                //arrObsValue = strObsValue.Split(':');

                //if (arrObsValue.Length > 0)
                //{
                //    strObsCC = arrObsValue[1];
                //    strObsCC = strObsCC.Replace(", Management Plans", "");
                //    strObsMangPlan = arrObsValue[2];


                //}

                //txteAuthCC.Text = strObsCC;
                //txteAuthManagementPlan.Text = strObsMangPlan;

                if (DS.Tables[0].Rows.Count > 0)
                {
                    gveAuthActivity.DataSource = DS;
                    gveAuthActivity.DataBind();
                }
                else
                {
                    gveAuthActivity.DataBind();
                }


            }

        }



        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                hideAuthHARID.Value = Convert.ToString(Request.QueryString["HAR_ID"]);

                lblHospitalName.Text = Convert.ToString(Session["Branch_Name"]).Trim();
                lblLicenseNo.Text = Convert.ToString(Session["Branch_ProviderID"]).Trim();

                BindeAuthorization();
                BindeAuthDiagnosis();
                BindeAuthActivities();

                BindPTDetails();
            }
        }
    }
}