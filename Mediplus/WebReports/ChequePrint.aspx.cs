﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.WebReports
{
    public partial class ChequePrint : System.Web.UI.Page
    {
        public string TRANS_NO { set; get; }


        PaymentEntryBAL objPay = new PaymentEntryBAL();

        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindPaymentEntry()
        {
            objPay = new PaymentEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND  APE_TRANS_NO='" + TRANS_NO + "' AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objPay.PaymentEntryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["BANK_NAME"] = Convert.ToString(DR["APE_BANK_NAME"]);
                    lblPayAgainst.Text = Convert.ToString(DR["APE_ACC_NAME"]);
                    lblAmount.Text = Convert.ToString(DR["APE_AMOUNT"]);

                    

                    if (lblAmount.Text.Trim() != "")
                    {
                        Int32 intAmount = (int)Math.Round( Convert.ToDecimal(lblAmount.Text), 0);
                        lblAmountInwords.Text = NumberToWords(intAmount);
                    }


                    //  txtChequeNo.Text = Convert.ToString(DR["APE_CHEQUE_NO"]);
                    lblChequeDate.Text = Convert.ToString(DR["APE_CHEQUE_DATEDesc"]);
                    //  drpBank.SelectedValue = Convert.ToString(DR["APE_BANK_NAME"]);


                    //strCategory = drpCategory.SelectedValue;

                    //if (drpCategory.SelectedValue.ToUpper() == "PDC PAYABLE".ToUpper() || drpCategory.SelectedValue.ToUpper() == "PDC RECEIVABLE".ToUpper())
                    //{
                    //    divCheque.Visible = true;
                    //}


                }

            }


        }

        void BindAccountChequeSetting()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  ACS_ACCOUNT_CODE='" + Convert.ToString(ViewState["BANK_NAME"]) + "' AND ACS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DS = objCom.fnGetFieldValue("*", "AC_ACCOUNT_CHEQUE_SETUP", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                string strTop = "50px", strLeft = "100px";
                string strFontFamily = "Arial", strFontSize = "9";
                bool bolFontBold = false;

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strTop = Convert.ToString(DR["ACS_TOP"]) +"px";
                    strLeft = Convert.ToString(DR["ACS_LEFT"])+"px";

                    strFontFamily = Convert.ToString(DR["ACS_FONT"]);

                    strFontSize = Convert.ToString(DR["ACS_FONT_SIZE"]);
                    bolFontBold = Convert.ToBoolean(DR["ACS_FONT_BOLD"]);



                    if (Convert.ToString(DR["ACS_FIELD"]) == "CHQ_DATE")
                    {
                         

                       divChequeDate.Style.Add("Top", strTop);
                       divChequeDate.Style.Add("Left", strLeft);


                        lblChequeDate.Style.Add("font-family", strFontFamily);
                        lblChequeDate.Style.Add("font-size", strFontSize);

                        if (bolFontBold == true)
                        {
                            lblChequeDate.Style.Add("font-weight", "Bold");
                        }

                        //lblChequeDate.Font.Name = strFontFamily;
                        //lblChequeDate.Font.Size = Convert.ToInt32(strFontSize);
                        //lblChequeDate.Font.Bold = bolFontBold;

                    }

                    if (Convert.ToString(DR["ACS_FIELD"]) == "PAY_AGAINST")
                    {
                        //divPayAgainst.Style.Add("Top", Convert.ToString(DR["ACS_TOP"]));
                        //divPayAgainst.Style.Add("Left", Convert.ToString(DR["ACS_LEFT"]));

                       divPayAgainst.Style.Add("Top", strTop);
                       divPayAgainst.Style.Add("Left", strLeft);

                        lblPayAgainst.Style.Add("font-family", strFontFamily);
                        lblPayAgainst.Style.Add("font-size", strFontSize);

                        if (bolFontBold == true)
                        {
                            lblPayAgainst.Style.Add("font-weight", "Bold");
                        }


                        //lblPayAgainst.Font.Name = strFontFamily;
                        //lblPayAgainst.Font.Size = Convert.ToInt32(strFontSize);
                        //lblPayAgainst.Font.Bold = bolFontBold;

                    }

                    if (Convert.ToString(DR["ACS_FIELD"]) == "AMOUNT_INWORDS")
                    {
                        //divAmountInwords.Style.Add("Top", Convert.ToString(DR["ACS_TOP"]));
                        //divAmountInwords.Style.Add("Left", Convert.ToString(DR["ACS_LEFT"]));

                         divAmountInwords.Style.Add("Top", strTop);
                         divAmountInwords.Style.Add("Left", strLeft);


                        lblAmountInwords.Style.Add("font-family", strFontFamily);
                        lblAmountInwords.Style.Add("font-size", strFontSize);

                        if (bolFontBold == true)
                        {
                            lblAmountInwords.Style.Add("font-weight", "Bold");
                        }


                        //lblAmountInwords.Font.Name = strFontFamily;
                        //lblAmountInwords.Font.Size = Convert.ToInt32(strFontSize);
                        //lblAmountInwords.Font.Bold = bolFontBold;

                    }

                    if (Convert.ToString(DR["ACS_FIELD"]) == "AMOUNT")
                    {
                        //divAmount.Style.Add("Top", Convert.ToString(DR["ACS_TOP"]));
                        //divAmount.Style.Add("Left", Convert.ToString(DR["ACS_LEFT"]));

                        divAmount.Style.Add("Top", strTop);
                        divAmount.Style.Add("Left", strLeft);


                        lblAmount.Style.Add("font-family", strFontFamily);
                        lblAmount.Style.Add("font-size", strFontSize);

                        if (bolFontBold == true)
                        {
                            lblAmount.Style.Add("font-weight", "Bold");
                        }



                        //lblAmount.Font.Name = strFontFamily;
                        //lblAmount.Font.Size = Convert.ToInt32(strFontSize);
                        //lblAmount.Font.Bold = bolFontBold;

                    }

                }
            }


        }

        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                TRANS_NO = Convert.ToString(Request.QueryString["TRANS_NO"]);
                BindPaymentEntry();
                BindAccountChequeSetting();
            }
        }
    }
}