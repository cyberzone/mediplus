﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Mediplus_BAL;


namespace Mediplus.WebReport
{
    public partial class Radiologies : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
         

        void BindRadiologyRequest()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID in (" + EMR_ID + ")";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.RadiologyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadRequest.DataSource = DS;
                gvRadRequest.DataBind();
            }
            else
            {
                gvRadRequest.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRadiologyRequest();
            }
        }
    }
}