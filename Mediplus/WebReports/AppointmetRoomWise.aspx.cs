﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.WebReports
{
    public partial class AppointmetRoomWise : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();



        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }


            }


        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS' ";



            DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.ScreenCustomizationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_BOOKED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Booked"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_CANCELLED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Cancelled"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_CONFIRMED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Confirmed"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_FILE_DRAWN")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["FileDrawn"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_MEETING")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Meeting"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_NOT_ARRIVED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NotArrived"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_NOTE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Note"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OTHERS")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Others"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OUT_OFFICE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OutOfOffice"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_PT_ARRIVED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PTArrived"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_PT_BILLED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PTBilled"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_SEEN_BY_DR")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SeenByDr"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_SEEN_BY_NURSE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SeenByNurse"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_WAITING")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Waiting"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_FONT_SIZE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["FontSize"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_COMPLETED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Completed"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OT_CASES")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OTCases"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                }
            }

        }

        void BindAppointment()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);




            ////string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' and HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            ////    Criteria += " AND HSFM_DEPT_ID='" +  Convert.ToString(ViewState["DEPT_ID"]) + "'";




            ////dboperations dbo = new dboperations();

            ////DataSet DSDoctors = new DataSet();
            ////DSDoctors = dbo.retun_doctor_detailss(Criteria);


            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='APP_ROOM'";

            string strDisplayRoomCodes = "";
            string[] arrDisplayRoomCodes = hidDisplayRoomCode.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrDisplayRoomCodes.Length; intSegType++)
            {
                if (strDisplayRoomCodes != "")
                {
                    strDisplayRoomCodes += ",'" + arrDisplayRoomCodes[intSegType] + "'";
                }
                else
                {
                    strDisplayRoomCodes = "'" + arrDisplayRoomCodes[intSegType] + "'";
                }
            }

            Criteria += " AND HCM_CODE IN  (" + strDisplayRoomCodes + ")";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DSRoom = new DataSet();

            DSRoom = objCommBal.CommonMastersGet(Criteria);



            string strSelectedDate = Convert.ToString(ViewState["APP_DATE"]);  //System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
            string strday = strTSelDate.DayOfWeek.ToString();

            strData.Append("<Table border='1' style='border-color:black;width:100%' ><tr>");
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
            {

                strData.Append("<td width='250px'  class='Header' >TIME</td>");

                strData.Append("<td width='250px' style='display:none;'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

            }
            else
            {

                strData.Append("<td width='250px'  style='display:none;'    class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px' class='Header' >TIME</td>");
            }

            if (DSRoom.Tables[0].Rows.Count > 0)
            {

                for (Int32 i = 0; i <= DSRoom.Tables[0].Rows.Count - 1; i++)
                {
                    strData.Append("<td   class='Header' >");
                    strData.Append(Convert.ToString(DSRoom.Tables[0].Rows[i]["HCM_DESC"]));
                    strData.Append("</td>");
                }
            }
            strData.Append("</tr>");


            strData.Append("<tr>");
            strData.Append("<td     width='250px' ></td>");
            for (Int32 i = 0; i <= DSRoom.Tables[0].Rows.Count - 1; i++)
            {
                strData.Append("<td >");
                strData.Append("<table style='width:100%'><tr><td BORDER='1' class='SubHeader' width='70px;'> FILE NO. </td><td class='SubHeader' width='100px;'>  PT. NAME  </td><td class='SubHeader' width='100px;'>PRO.</td></tr></table>");

                strData.Append("</td>");
            }

            strData.Append("</tr>");


            Int32 intday = strTSelDate.Day;


            String strlTime;
            String strNewTime1 = "";



            strlTime = Convert.ToString(AppointmentStart);
            strNewTime1 = "00";

            AppointmentStart = AppointmentStart * 100;
            AppointmentEnd = AppointmentEnd * 100;

            int[] arrCount = new int[DSRoom.Tables[0].Rows.Count];

            for (Int32 i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                if (strNewTime1 == "")
                {
                    strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                }

                // i = i + AppointmentInterval;
                string[] arrlTime = strlTime.Split(':');
                if (arrlTime.Length > 1)
                {
                    strlTime = arrlTime[0];
                }

                if (Convert.ToInt32(strNewTime1) >= 60)
                {
                    strlTime = Convert.ToString(Convert.ToInt32(Convert.ToInt32(strlTime) + 1).ToString("D2")) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                }
                else
                {
                    strlTime = Convert.ToString(Convert.ToInt32(strlTime).ToString("D2") + ":" + strNewTime1);
                }

                i = Convert.ToInt32(strlTime.Replace(":", ""));

                Boolean IsBlocked = false;

                if (GlobalValues.FileDescription == "ALTAIF")
                {
                    if (strlTime == "12:30" || strlTime == "13:00" || strlTime == "13:30" || strlTime == "14:00" || strlTime == "14:30" || strlTime == "15:00" || strlTime == "15:30" || strlTime == "16:00" || strlTime == "16:30" || strlTime == "21:00")
                    {
                        IsBlocked = true;
                    }
                }

                if (Convert.ToInt32(strlTime.Replace(":", "")) <= AppointmentEnd && strlTime != "00:00")
                {
                    DateTime dtstlTime;
                    dtstlTime = Convert.ToDateTime(strlTime);

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
                    {
                        if (IsBlocked == false)
                        {
                            strData.Append("<tr>");
                            strData.Append("<td   width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(strlTime);
                            strData.Append("</td>");


                            strData.Append("<td  style='display:none'  width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(dtstlTime.ToString(@"hh\:mm tt"));
                            strData.Append("</td>");

                        }
                    }
                    else
                    {
                        if (IsBlocked == false)
                        {

                            strData.Append("<tr>");
                            strData.Append("<td style='display:none'   width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(strlTime);
                            strData.Append("</td>");


                            strData.Append("<td   width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(dtstlTime.ToString(@"hh\:mm tt"));
                            strData.Append("</td>"); ;
                        }

                    }



                    for (int j = 0; j <= DSRoom.Tables[0].Rows.Count - 1; j++)
                    {
                        if (arrCount[j] > 0)
                        {
                            arrCount[j] = arrCount[j] - 1;
                            goto ForEnd;
                        }

                        Boolean isBolcked = false;


                        //   BindStaffData(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]));
                        isBolcked = BindCheckBlockedAppointment(i);

                        DataSet DSAppt = new DataSet();
                        DSAppt = GetAppointment(Convert.ToString(DSRoom.Tables[0].Rows[j]["HCM_CODE"]), strlTime);
                        if (DSAppt.Tables[0].Rows.Count > 0)
                        {
                            String strlTime2 = "";
                            Int32 AppointmentStart2 = 0, AppointmentEnd2 = 0;

                            string strStartTime = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]);
                            //string[] arrStartTime = strStartTime.Split(':');
                            //strStartTime = arrStartTime[0] +""+ arrStartTime[1];
                            strStartTime = strStartTime.Replace(":", "");


                            string strStartFnish = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]);
                            //string[] arrStartFnish =strStartFnish.Split(':');
                            //strStartFnish = arrStartFnish[0] + "" + arrStartFnish[1];
                            strStartFnish = strStartFnish.Replace(":", "");

                            AppointmentStart2 = Convert.ToInt32(strStartTime);
                            AppointmentEnd2 = Convert.ToInt32(strStartFnish);

                            strlTime2 = strStartTime.Substring(0, 2);
                            Int32 intRowCount = 0;
                            String strNewTime2 = Convert.ToString(AppointmentStart2);
                            strNewTime2 = strNewTime2.Substring(strNewTime2.Length - 2, 2);

                            for (Int32 k = AppointmentStart2; k <= AppointmentEnd2; k++)
                            {
                                if (strNewTime2 == "")
                                {
                                    strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
                                }

                                string[] arrlTime2 = strlTime2.Split(':');
                                if (arrlTime2.Length > 1)
                                {
                                    strlTime2 = arrlTime2[0];
                                }

                                if (Convert.ToInt32(strNewTime2) >= 60)
                                {
                                    strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                                }
                                else
                                {
                                    strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
                                }

                                k = Convert.ToInt32(strlTime2.Replace(":", ""));
                                strNewTime2 = "";

                                intRowCount = intRowCount + 1;

                            }

                            arrCount[j] = intRowCount - 2;

                           
                            if (IsBlocked == false)
                            {
                                string strFormatSTTime = "";
                                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                                {
                                    DateTime dtstlTime1;
                                    dtstlTime1 = Convert.ToDateTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]));
                                    dtstlTime1.ToString(@"hh\:mm tt");


                                    string[] arrSTTime = Convert.ToString(dtstlTime1).Split(' ');
                                    string strSTTime = "";
                                    if (arrSTTime.Length > 0)
                                    {
                                        strSTTime = arrSTTime[1];
                                        strSTTime = strSTTime.Substring(0, strSTTime.LastIndexOf(':'));
                                    }

                                    string[] arrSTTime1 = Convert.ToString(dtstlTime1).Split(' ');


                                    if (arrSTTime.Length > 1)
                                    {
                                        strFormatSTTime = strSTTime + "  " + arrSTTime1[2];

                                    }
                                }
                                else
                                {
                                    strFormatSTTime = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]);
                                }



                                string strFormatFITime = "";
                                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                                {
                                    DateTime dtstlTime1;
                                    dtstlTime1 = Convert.ToDateTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));
                                    dtstlTime1.ToString(@"hh\:mm tt");


                                    string[] arrFITime = Convert.ToString(dtstlTime1).Split(' ');
                                    string strFITime = "";
                                    if (arrFITime.Length > 0)
                                    {
                                        strFITime = arrFITime[1];
                                        strFITime = strFITime.Substring(0, strFITime.LastIndexOf(':'));
                                    }

                                    string[] arrFITime1 = Convert.ToString(dtstlTime1).Split(' ');


                                    if (arrFITime.Length > 1)
                                    {
                                        strFormatFITime = strFITime + "  " + arrFITime1[2];

                                    }
                                }
                                else
                                {
                                    strFormatFITime = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]);
                                }


                                string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
                                string strStatus = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAS_STATUS"]);


                                string strStatusColor = GetAppointmentStatus(Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]));

                                if (strStatusColor != null && strStatusColor != "")
                                {
                                    strColor = strStatusColor;
                                }

                                strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  rowspan=" + Convert.ToString(intRowCount - 1) + "    align='center' class='TextBoxStyle'   rowspan=" + Convert.ToString(intRowCount - 1) + " style='vertical-align:top;border-color:black;  background-color: #a6a6a6;'>");
                              //  strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  align='center' class='TextBoxStyle'  style='vertical-align:top;border-color:black;  background-color: #a6a6a6;background-image:url(" + ResolveUrl("~/images/Slash.png") + "'>");
                                strData.Append("<table cellpadding='0' cellspacing='0' style='width:100%'><tr><td class='Content'  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   width='70px'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + "</td><td class='Content' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  width='100px'>" + "  " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]) + "</td><td  class='Content' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  width='100px'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]) + "</td></tr></table>");
                                strData.Append("</td>");
                            }
                        }
                        else if (isBolcked == true)
                        {
                            // strData.Append("<td ALIGN='CENTER'  height='50px'   class='AppBox' style='border-color:black;background-color: white;'>");
                            strData.Append("<td  ALIGN='CENTER' width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "    class='TextBoxStyle' style='border-color:black;background-color: white;  cursor:pointer;'>");

                            strData.Append("<span class=lblCaption1>BLOCKED</span>");

                            strData.Append("</td>");
                        }
                        else
                        {
                            if (IsBlocked == false)
                            {

                                strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='TextBoxStyle' style='border-color:black;background-color: white;'>");
                                strData.Append("<span class=lblCaption1>NO SHEDULE</span>");
                                strData.Append("</td>");
                            }

                        }
                    ForEnd: ;
                    }


                    if (IsBlocked == false)
                    {

                        strData.Append("</tr>");
                    }
                }

                strNewTime1 = "";

            }


          //String strrtime2;
        //String strlTime2;
        //String strNewTime2;


          //strrtime2 = strFrom2.Substring(strFrom2.Length - 2, 2);
        //strlTime2 = strFrom2.Substring(0, 2);
        //strNewTime2 = strFrom2.Substring(strFrom2.Length - 2, 2);


          //for (Int32 i = intStart2; i <= intEnd2; i++)
        //{
        //    if (strNewTime2 == "")
        //    {
        //        strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
        //    }

          //    // i = i + AppointmentInterval;
        //    string[] arrlTime = strlTime2.Split(':');
        //    if (arrlTime.Length > 1)
        //    {
        //        strlTime2 = arrlTime[0];
        //    }

          //    if (Convert.ToInt32(strNewTime2) >= 60)
        //    {
        //        strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime2) - 60);
        //    }
        //    else
        //    {
        //        strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
        //    }

          //    i = Convert.ToInt32(strlTime2.Replace(":", ""));


          //    if (Convert.ToInt32(strlTime2.Replace(":", "")) <= Convert.ToInt32(strTo2.Replace(":", "")) && strlTime2 != "00:00")
        //    {
        //        strData.Append("<tr>");
        //        strData.Append("<td   class='RowHeader'>");
        //        strData.Append(strlTime2);
        //        strData.Append("</td>");

          //        for (int j = 0; j <= DSDoctors.Tables[0].Rows.Count - 1; j++)
        //        {

          //            DataSet DSAppt = new DataSet();
        //            DSAppt = GetAppointment(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]), strlTime2);
        //            if (DSAppt.Tables[0].Rows.Count > 0)
        //            {
        //                string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
        //                strData.Append("<td   style='vertical-align:top;background-color: " + strColor + ";'>");
        //                strData.Append("<a class='Content'   href='javascript:PatientPopup1(" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APPOINTMENTID"]) + ",0,0,0)'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"] + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"])) + "</a>");
        //                strData.Append("</td>");
        //            }
        //            else
        //            {
        //                strData.Append("<td    style='background-color: white;'>");
        //                strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime2 + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
        //                strData.Append("</td>");

          //            }

          //        }


          //        strData.Append("</tr>");
        //    }

          //    strNewTime2 = "";

          //}





      FunEnd: ;



        }

        DataSet GetAppointment(string RoomCode, string FromTime)
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            string strlTime;
            strlTime = FromTime;

            string strNewTime1 = "";
            if (strNewTime1 == "")
            {
                strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
            }

            // i = i + AppointmentInterval;


            string[] arrlTime = strlTime.Split(':');
            if (arrlTime.Length > 1)
            {
                strlTime = arrlTime[0];
            }

            if (Convert.ToInt32(strNewTime1) >= 60)
            {
                strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
            }
            else
            {
                strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
            }

            string Criteria = " 1=1 ";


            Criteria += "  AND HAM_ROOM ='" + RoomCode + "'";

            if (Convert.ToString(ViewState["DR_ID"]) != "" && Convert.ToString(ViewState["DR_ID"]) != null)
            {
                Criteria += " AND HAM_DR_CODE IN ('" + Convert.ToString(ViewState["DR_ID"]) + "')";
            }


            if(  Convert.ToString(ViewState["DEPT_ID"]) !=""     && Convert.ToString(ViewState["DEPT_ID"]) != null )
            {
            Criteria += " AND HAM_DR_CODE IN ( SELECT  HSFM_STAFF_ID FROM HMS_STAFF_MASTER WHERE HSFM_DEPT_ID='" + Convert.ToString(ViewState["DEPT_ID"]) + "' )";
            }

            string strSelectedDate = Convert.ToString(ViewState["APP_DATE"]); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

          //  Criteria += " AND HAM_DR_CODE='" + DRCode + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            Criteria += "   AND   HAM_STARTTIME = '" + strForStartDate + " " + FromTime + ":00'";


            //Criteria += "  AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00' AND   HAM_STARTTIME <= '" + strForStartDate + " " + FromTime + ":00'";
            //Criteria += "  AND HAM_FINISHTIME >= '" + strForStartDate + " " + strlTime + ":00' AND   HAM_FINISHTIME <= '" + strForStartDate + " " + strlTime + ":00'";


            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                //    arrAppDtls[0] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_APPOINTMENTID"]);
                //    arrAppDtls[1] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_FILENUMBER"]);
                //    arrAppDtls[2] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_PT_NAME"]);


            }


            return DSAppt;
        }

        void GetAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1  ";

            //if (drpAppStatus.SelectedIndex != 0)
            //{
            //    Criteria += " AND HAS_STATUS='" + drpAppStatus.SelectedValue + "'";
            //}
            //else
            //{
            //  Criteria += " AND  HAS_STATUS IN('" + drpAppStatus.Items[1].Value + "','" + drpAppStatus.Items[2].Value + "','" + drpAppStatus.Items[3].Value + "' )";
            //  }
            DS = dbo.AppointmentStatusGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }

        string GetAppointmentStatus(string strStatusID)
        {
            string strColor = "";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string Criteria = " 1=1    AND HAS_ID=" + strStatusID;

            DS = dbo.AppointmentStatusGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                strColor = Convert.ToString(DS.Tables[0].Rows[0]["HAS_COLOR"]);

            }
            return strColor;

        }

        void BindStaffData(string DR_ID)
        {
            ViewState["HSFM_NO_APNT_DATEFROM1"] = "";
            ViewState["HSFM_NO_APNT_FROM1TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO1"] = "";
            ViewState["HSFM_NO_APNT_TO1TIME"] = 0;


            ViewState["HSFM_NO_APNT_DATEFROM2"] = "";
            ViewState["HSFM_NO_APNT_FROM2TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO2"] = "";
            ViewState["HSFM_NO_APNT_TO2TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATEFROM3"] = "";
            ViewState["HSFM_NO_APNT_FROM3TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO3"] = "";
            ViewState["HSFM_NO_APNT_TO3TIME"] = 0;

            ViewState["HSFM_APNT_TIME_INT"] = 30;

            DataSet DS = new DataSet();

            string Criteria = "  HSFM_STAFF_ID='" + DR_ID + "'";

            DS = (DataSet)ViewState["DSAllStaff"];

            DataRow[] result = DS.Tables[0].Select(Criteria);
            foreach (DataRow DR in result)
            {
                ViewState["RosterID"] = Convert.ToString(DR["HSFM_ROSTER_ID"]);

                if (DR.IsNull("HSFM_NO_APNT_DATEFROM1Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATEFROM1Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM1"] = Convert.ToString(DR["HSFM_NO_APNT_DATEFROM1Desc"]);
                }

                if (DR.IsNull("HSFM_NO_APNT_FROM1TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_FROM1TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_FROM1TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM1TIME"] = Convert.ToString(DR["HSFM_NO_APNT_FROM1TIME"]).Replace(":", "");

                }
                if (DR.IsNull("HSFM_NO_APNT_DATETO1Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATETO1Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO1"] = Convert.ToString(DR["HSFM_NO_APNT_DATETO1Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_TO1TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_TO1TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_TO1TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO1TIME"] = Convert.ToString(DR["HSFM_NO_APNT_TO1TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATEFROM2Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATEFROM2Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM2"] = Convert.ToString(DR["HSFM_NO_APNT_DATEFROM2Desc"]);
                }

                if (DR.IsNull("HSFM_NO_APNT_FROM2TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_FROM2TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_FROM2TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM2TIME"] = Convert.ToString(DR["HSFM_NO_APNT_FROM2TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATETO2Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATETO2Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO2"] = Convert.ToString(DR["HSFM_NO_APNT_DATETO2Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_TO2TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_TO2TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_TO2TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO2TIME"] = Convert.ToString(DR["HSFM_NO_APNT_TO2TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATEFROM3Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATEFROM3Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM3"] = Convert.ToString(DR["HSFM_NO_APNT_DATEFROM3Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_FROM3TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_FROM3TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_FROM3TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM3TIME"] = Convert.ToString(DR["HSFM_NO_APNT_FROM3TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATETO3Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATETO3Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO3"] = Convert.ToString(DR["HSFM_NO_APNT_DATETO3Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_TO3TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_TO3TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_TO3TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO3TIME"] = Convert.ToString(DR["HSFM_NO_APNT_TO3TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_APNT_TIME_INT") == false && Convert.ToString(DR["HSFM_APNT_TIME_INT"]) != "")
                {
                    ViewState["HSFM_APNT_TIME_INT"] = Convert.ToString(DR["HSFM_APNT_TIME_INT"]).Replace(":", "");
                }


            }
        }

        Boolean BindCheckBlockedAppointment(Int32 intTime)
        {


            string strStartDate = Convert.ToString(ViewState["APP_DATE"]);
            string[] arrDate = strStartDate.Split('/');
            string strDay = "";

            if (arrDate.Length > 1)
            {
                strDay = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
            }
            //DateTime dtAppointmentDate = Convert.ToDateTime(strDay);

            DateTime dtAppointmentDate = DateTime.ParseExact(strStartDate, "dd/MM/yyyy", null);



            DateTime dtHSFM_NO_APNT_DATEFROM1;
            DateTime dtHSFM_NO_APNT_DATETO1;

            DateTime dtHSFM_NO_APNT_DATEFROM2;
            DateTime dtHSFM_NO_APNT_DATETO2;


            DateTime dtHSFM_NO_APNT_DATEFROM3;
            DateTime dtHSFM_NO_APNT_DATETO3;


            int intHSFM_NO_APNT_FROM1TIME = 0;
            int intHSFM_NO_APNT_TO1TIME = 0;

            int intHSFM_NO_APNT_FROM2TIME = 0;
            int intHSFM_NO_APNT_TO2TIME = 0;

            int intHSFM_NO_APNT_FROM3TIME = 0;
            int intHSFM_NO_APNT_TO3TIME = 0;






            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM1TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM1TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM1TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO1TIME"]) != "")
            {
                intHSFM_NO_APNT_TO1TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO1TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM1"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO1"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM1 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM1"]);
                dtHSFM_NO_APNT_DATETO1 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO1"]);

                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM1 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO1)
                {
                    if (intHSFM_NO_APNT_FROM1TIME <= intTime && intHSFM_NO_APNT_TO1TIME >= intTime)
                    {
                        return true;
                    }
                }
            }



            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM2TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM2TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM2TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO2TIME"]) != "")
            {
                intHSFM_NO_APNT_TO2TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO2TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM2"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO2"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM2 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM2"]);
                dtHSFM_NO_APNT_DATETO2 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO2"]);
                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM2 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO2)
                {
                    if (intHSFM_NO_APNT_FROM2TIME <= intTime && intHSFM_NO_APNT_TO2TIME >= intTime)
                    {
                        return true;
                    }
                }
            }


            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM3TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM3TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM3TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO3TIME"]) != "")
            {
                intHSFM_NO_APNT_TO3TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO3TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM3"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO3"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM3 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM3"]);
                dtHSFM_NO_APNT_DATETO3 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO3"]);
                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM3 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO3)
                {
                    if (intHSFM_NO_APNT_FROM3TIME <= intTime && intHSFM_NO_APNT_TO3TIME >= intTime)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        void BindAllStaffData()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' and HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);

            ViewState["DSAllStaff"] = DS;

        }

        string GetDeptName(string DeptID)
        {
            string DeptName = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            string Criteria = " HDM_DEP_ID='" + DeptID + "'";
            DS = objCom.DepMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptName = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_NAME"]);
            }

            return DeptName;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ViewState["APP_DATE"] = Convert.ToString(Request.QueryString["APP_DATE"]);
                ViewState["DR_ID"] = Convert.ToString(Request.QueryString["DR_ID"]);
                ViewState["DEPT_ID"] = Convert.ToString(Request.QueryString["DEPT_ID"]);

                BindSystemOption();
                BindScreenCustomization();

                BindAllStaffData();
                GetAppointmentStatus();

                BindAppointment();
            }

        }
    }
}