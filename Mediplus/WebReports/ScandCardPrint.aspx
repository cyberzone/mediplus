﻿<%@ Page Title="" Language="C#"   AutoEventWireup="true" CodeBehind="ScandCardPrint.aspx.cs" Inherits="Mediplus.WebReports.ScandCardPrint" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
          
    </script>
    <style media="print" type="text/css">
        #btnPrint {
            display:none;
        }

    </style>
</head>


<body onload="javascript:window.print();">
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <table style="width: 100%;">
                <tr>
                    <td style="height:50px;">

                        <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="javascript:window.print();"  />

                    </td>
                </tr>
            </table>
           
            <table  style="width:100%;"  >
                <tr>
                    <td style="width:40%"></td>
                    <td style="vertical-align:central;" >

                        <asp:Image ID="imgFront" runat="server" Height="200px" Width="320px"   />


                    </td>
                     <td style="width:40%"></td>
                </tr>
                <tr>
                    <td style="height:10px;"></td>
                </tr>
                <tr>
                     <td style="width:40%"></td>
                    <td style="vertical-align:central;" >

                        <asp:Image ID="imgBack" runat="server" Height="200px" Width="320px"   />

                    </td>
                     <td style="width:40%"></td>

                </tr>

            </table>
              
        </div>
    </form>
</body>
</html>
