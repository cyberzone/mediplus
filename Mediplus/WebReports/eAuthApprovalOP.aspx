﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="eAuthApprovalOP.aspx.cs" Inherits="Mediplus.WebReports.eAuthApprovalOP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .Header {
            font: bold 18px/100% Arial;
            background-color: #fff;
            color: black;
            text-align: Center;
            height: 30px;
            width: 50px;
            border-color: black;
        }
         .SubHeader {
           font: bold 15px/100% Arial;
           background-color: #fff;
           vertical-align:middle;
           text-align:center;
           border: 1px; 
           border-color:black;
           border: thin; border-color: #CCCCCC; border-style: groove;;
           height: 30px;
        }

        .RowHeader {
            font: bold 11px/100% Arial;
            text-align: center;
            border-color: black;
        }

        .Content {
            font: 12px/100% Arial;
            background-color: #fff;
            vertical-align:middle;
            text-align:center;
            border: 1px; 
            border-color:black;border: thin; border-color: #CCCCCC; border-style: groove;
           
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
     <div style="margin: 0px auto; width: 800px">
            <input type="hidden" id="hideAuthHARID" runat="server" />
            <input type="hidden" id="hidPT_ID" runat="server" />

      

        <br />
        <span class="PageHeader" style="font-size: 18px;">Authorization </span>
        <br />
        <br />
           <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Authorization Effective Date:</td>
                <td >
                    <asp:label id="lbleAuthStDate" runat="server" cssclass="lblCaption1" width="150px" ></asp:label>
                </td>
             <td class="lblCaption1">Authorization Expiry Date:</td>
                <td >
                    <asp:label id="lbleAuthEndDate" runat="server" cssclass="lblCaption1" width="150px"></asp:label>
                </td>
               
            </tr>
         
           <tr>
                <td class="lblCaption1">Authorization No:</td>
                <td>
                    <asp:label id="lblAuthorizationNo" runat="server" cssclass="lblCaption1"></asp:label><br />
                    
                </td>
              <td class="lblCaption1">Authorization Type:</td>
                <td  class="lblCaption1">
                   INITIAL PROSPECTIVE
                    
                </td>
               
            </tr>
              <tr>
                <td class="lblCaption1">Treatment Type:</td>
                <td>
                    <asp:label id="lblTreatmentType" runat="server" cssclass="lblCaption1"></asp:label><br />
                </td>
              <td class="lblCaption1">
                 
                    Approval Status:
              </td>
                <td  class="lblCaption1">
                   
                      <asp:label id="lblAppStatus" runat="server" cssclass="lblCaption1"></asp:label><br />
                </td>
               
            </tr>

             <tr>
                <td class="lblCaption1">Company Name:</td>
                <td>
                    <asp:label id="lblInsName" runat="server" cssclass="lblCaption1"></asp:label><br />
                </td>
              <td class="lblCaption1">
                

              </td>
                <td  class="lblCaption1">
                 
                    
                </td>
               
            </tr>

               <tr>
                     <td class="lblCaption1">Comment:</td>
                <td  colspan="3">
                    <asp:label id="lblComment" runat="server" cssclass="lblCaption1"></asp:label><br />
                </td>

               </tr>

                <tr>
                <td class="lblCaption1">Hospital Name:</td>
                <td>
                    <asp:label id="lblHospitalName" runat="server" cssclass="lblCaption1"></asp:label><br />
                </td>
              <td class="lblCaption1">
                  License No :

              </td>
                <td  class="lblCaption1">
                   <asp:label id="lblLicenseNo" runat="server" cssclass="lblCaption1"></asp:label><br />
                    
                </td>
               
            </tr>

          </table>
          <table style="width: 100%;">
              <tr>
                  <td  class="lblCaption1" style="font-weight:bold;">
                      Patient Details 
                  </td>
                  <td  class="lblCaption1"  style="font-weight:bold;">
                      Service Details 
                  </td>
              </tr>
              <tr>
                  <td class="lblCaption1" style="vertical-align:top;">
                      Patient No : <asp:label id="lblPTNo" runat="server" cssclass="lblCaption1"></asp:label><br />
                      Full Name : <asp:label id="lblPTName" runat="server" cssclass="lblCaption1"></asp:label><br />
                      Date Of Birth : <asp:label id="lblDOB" runat="server" cssclass="lblCaption1"></asp:label><br />
                      Product Name : <asp:label id="lblPolicyType" runat="server" cssclass="lblCaption1"></asp:label><br />
                      Daman Card No : <asp:label id="lblPolicyNo" runat="server" cssclass="lblCaption1"></asp:label><br />
                       
                  </td>
                  <td  class="lblCaption1" style="vertical-align:top;">
                      Referene Number : <asp:label id="lblRefNo" runat="server" cssclass="lblCaption1"></asp:label><br />
                  </td>
              </tr>
          </table>


            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1" >
                        The below mentioned diagnosis is the interpretation of the information sent on the Authorization Request Form.
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">
                <asp:GridView ID="gveAuthDiagnosis" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>

                                    <asp:TemplateField HeaderText="Code">
                                        <ItemTemplate>

                                            <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("HAD_DIAG_CODE") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>

                                            <asp:Label ID="Label3" CssClass="GridRow" runat="server" Text='<%# Bind("HAD_DIAG_NAME") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                                         
                                </Columns>
                                 
                            </asp:GridView>

                        </td>
               </tr>
                <tr>
                    <td style="width:20px;">

                    </td>
                </tr>
                <tr>
                      <td class="lblCaption1  BoldStyle">
                        
                            <asp:GridView ID="gveAuthActivity" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblServCode" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_SERV_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblServName" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_SERV_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                
                                     <asp:TemplateField HeaderText="Requested Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("QTY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Approved Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_APPROVED_QTY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Net Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_PAYMENT_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Patient Share">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_PATIENT_SHARE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Denial Code">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_DENIAL_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                

                            </asp:GridView>
                    </td>
                </tr>
               


            </table>
          </div>
</asp:Content>
