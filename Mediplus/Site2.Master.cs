﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using EMR_BAL;

namespace Mediplus
{
    public partial class Site2 : System.Web.UI.MasterPage
    { 

        
     

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

       
       

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {

                Response.Redirect("Default.aspx?NoSession=1");
            }

            if (!IsPostBack)
            {
                string strMenuName = Convert.ToString(Request.QueryString["MenuName"]);
                ucLeftSubMenu.Menu_Header = strMenuName;

            
                if (strMenuName == "OP_EMR")
                {
                    myTopnav.Style.Add("height", "0px");
                  //  pnlLeftMenuAccordion.Visible = false;
                    divHeaderLine.Visible = false;
                    myTopnavEMR.Style.Add("display", "block");
                   EMRTopMenu.Visible = true;
                   EMRTopMenu.LoadEMRData = true;
                  

                }

            }
        }

     

        //void papulateMenuTree()
        //{

        //    DataSet DS = new DataSet();
        //    CommonBAL objCom = new CommonBAL();
        //    string Criteria1 = " 1=1 AND  HM_MENU_STATUS=1 AND HM_MENU_TYPE='MENU' AND HM_MENU_FOR ='HMS' AND HM_MENU_HEADER='Billing' ";
        //    //Criteria1 += " AND (HM_DEPT_ID  LIKE'%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR HM_DEPT_ID='' OR HM_DEPT_ID IS NULL)";
        //    //Criteria1 += " AND (HM_DEPT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR HM_DEPT_ID_HIDE='' OR HM_DEPT_ID_HIDE IS NULL )";


        //    Int32 i;


        //    DS = objCom.fnGetFieldValue(" * ", "HMS_MENUS", Criteria1, "HM_MENU_ORDER");


        //    if (DS.Tables[0].Rows.Count > 0)
        //    {

        //        foreach (DataRow dbRow in DS.Tables[0].Rows)
        //        {

        //            strData.Append("<a  href=#  onclick=openInNewTab('" + Convert.ToString(dbRow["HM_MENU_URL"]) + "')>" + Convert.ToString(dbRow["HM_MENU_NAME"]) + "</a>");



        //            //MenuItem L1 = new MenuItem(Convert.ToString(dbRow["HM_MENU_NAME"]), Convert.ToString(dbRow["HM_MENU_ID"]), "", Convert.ToString(dbRow["HM_MENU_URL"]), "_self");
        //            //Menu1.Items.Add(L1);
        //            ///}
        //            ///


        //        }


        //    }
        //}
        
    }
}