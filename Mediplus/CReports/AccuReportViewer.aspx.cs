﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using Mediplus_BAL;
using System.IO;

namespace Mediplus.CReports
{
    public partial class AccuReportViewer : System.Web.UI.Page
    {
        ReportDocument CR = new ReportDocument();

        public static string DataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        public static string DBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        public static string DBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        public static string DBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void AccuReportLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../AccuReportLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void ExportReport()
        {

            try
            {


                CR = new ReportDocument();
                string ReportName = (String)Request.Params["ReportName"] == null ? "" : Request.Params["ReportName"];
                CR.Load(GlobalValues.ReportPath + ReportName);

                //  CR.SetDatabaseLogon(DBUserId, DBUserPWD, DataSource, DBName);//"sa", "@rajkumar", "SQL2005DB\\SQLSERVER2008", "InternalProject");
                CR.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, @GlobalValues.DB_SERVER1, "ACCUMED");

                AccuReportLog("DataBase Credential done ");

                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();
                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();

                crParameterDiscreteValue.Value = "1=1";
                crParameterValues.Add(crParameterDiscreteValue);
                String SelectionFormula = (String)Request.Params["SelectionFormula"] == null ? "" : Request.Params["SelectionFormula"];
                CR.RecordSelectionFormula = SelectionFormula;


                AccuReportLog(" Formula Assigned ");
                //crParameterFieldDefinitions = CR.DataDefinition.ParameterFields;
                //crParameterFieldDefinition = crParameterFieldDefinitions["@Criteria"];
                //crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);
                CrystalReportViewer1.RefreshReport();

                CrystalReportViewer1.ReportSource = CR;
                CrystalReportViewer1.DataBind();
                CrystalReportViewer1.HasCrystalLogo = true;
                // CrystalReportViewer1.Zoom(82);
                CrystalReportViewer1.HasToggleGroupTreeButton = false;
                CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
                // dvReport.Visible = true;

                //this.CrystalReportViewer1.Dispose();
                //this.CrystalReportViewer1 = null;
                //CR.Close();
                //CR.Dispose();
                //GC.Collect();

                AccuReportLog("Report Completed ");
            }
            catch (Exception ex)
            {


                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReportViewer.ExportReport");
                TextFileWriting(ex.Message.ToString());
            }
            finally
            {
                CR.Close();
                CR.Dispose();
            }

        }

        void PDFReport()
        {


            ReportDocument crystalReport = new ReportDocument();

            try
            {

                string ReportName = (String)Request.Params["ReportName"] == null ? "" : Request.Params["ReportName"];
                crystalReport.Load(@GlobalValues.REPORT_PATH + ReportName);
                crystalReport.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, @GlobalValues.DB_SERVER1, "ACCUMED");
                String SelectionFormula = (String)Request.Params["SelectionFormula"] == null ? "" : Request.Params["SelectionFormula"];
                crystalReport.RecordSelectionFormula = SelectionFormula;
                //ReportViewer.ReportSource = crystalReport;

                // convert rpt to pdf binary content.
                byte[] pdfContent = null;
                using (MemoryStream ms = (MemoryStream)crystalReport.ExportToStream(ExportFormatType.PortableDocFormat))
                {
                    pdfContent = ms.ToArray();
                    ms.Close();

                }
                //  crystalReport.Close();
                //  crystalReport.Dispose();


                Response.ClearHeaders();
                Response.ClearContent();
                Response.AppendHeader("Content-Type", "application/pdf");
                Response.BinaryWrite(pdfContent);
            }
            catch (Exception ex)
            {


                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReportViewer.PDFReport");
                TextFileWriting(ex.Message.ToString());
            }
            finally
            {
                crystalReport.Close();
                crystalReport.Dispose();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Request.Params["Type"]) != "pdf")
            {
                ExportReport();
            }
            else
            {
                PDFReport();
            }

            //  ExportReport();


        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (CR != null)
            {
                CR.Close();
                CR.Dispose();
            }
        }
    }
}