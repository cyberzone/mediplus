﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Mediplus_BAL;
using System.IO;


namespace Mediplus.CReports
{
    public partial class ChartViewer : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument crystalReport = new ReportDocument();

            try
            {
              
                string ReportName = (String)Request.Params["ReportName"] == null ? "" : Request.Params["ReportName"];
                crystalReport.Load(@GlobalValues.REPORT_PATH + ReportName);

                crystalReport.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, @GlobalValues.DB_SERVER1, GlobalValues.DB_DATABASE);

                String SelectionFormula = (String)Request.Params["SelectionFormula"] == null ? "" : Request.Params["SelectionFormula"];
                crystalReport.RecordSelectionFormula = SelectionFormula;
                //ReportViewer.ReportSource = crystalReport;

                // CrystalReportViewer1.RefreshReport();
                // CrystalReportViewer1.ReportSource = crystalReport;
                // CrystalReportViewer1.DataBind();
                // CrystalReportViewer1.HasCrystalLogo = true;
                // CrystalReportViewer1.Zoom(82);
                // CrystalReportViewer1.HasToggleGroupTreeButton = false;
                //CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;



                // convert rpt to pdf binary content.
                byte[] pdfContent = null;
                using (MemoryStream ms = (MemoryStream)crystalReport.ExportToStream(ExportFormatType.PortableDocFormat))
                {
                    pdfContent = ms.ToArray();
                    ms.Close();

                }
              //  crystalReport.Close();
              //  crystalReport.Dispose();
                Response.AppendHeader("Content-Type", "application/pdf");
                Response.BinaryWrite(pdfContent);
            }
             catch (Exception ex)
            {
               

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ChartViewer.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
            finally
            {
                crystalReport.Close();
                crystalReport.Dispose();
            }
        

        /*

            string tempDir =  @"C:\inetpub\wwwroot\Mediplus\Mediplus\HMS\Uploads\Temp\"; // System.Configuration.ConfigurationSettings.AppSettings["TempDir"];
            string tempFileName = Session.SessionID.ToString() + ".";

            tempFileName += "htm";
           string    contentType = "text/html";
            CrystalDecisions.Shared.HTMLFormatOptions hop = new CrystalDecisions.Shared.HTMLFormatOptions();
            hop.HTMLBaseFolderName = tempDir;
            hop.HTMLFileName = tempFileName;
            crystalReport.ExportOptions.FormatOptions = hop;

             string tempFileNameUsed;

               string[] fp = crystalReport.FilePath.Split("\\".ToCharArray());
            string leafDir = fp[fp.Length-1];
            // strip .rpt extension
            leafDir = leafDir.Substring(0,leafDir.Length-4);
            tempFileNameUsed = string.Format("{0}{1}\\{2}", tempDir, leafDir, tempFileName);

            crystalReport.Close();
            crystalReport.Dispose();


            Response.ClearHeaders();
            Response.ClearContent();
         
            Response.AppendHeader("Content-Type", "text/html");

            Response.WriteFile(tempFileNameUsed);

            */

         
        }



  protected void exportReport(CrystalDecisions.CrystalReports.Engine.ReportClass selectedReport, CrystalDecisions.Shared.ExportFormatType eft)
{
    selectedReport.ExportOptions.ExportFormatType = eft;

    string contentType ="";
    // Make sure asp.net has create and delete permissions in the directory
    string tempDir = System.Configuration.ConfigurationSettings.AppSettings["TempDir"];
    string tempFileName = Session.SessionID.ToString() + ".";
    switch (eft)
    {
    case CrystalDecisions.Shared.ExportFormatType.PortableDocFormat : 
        tempFileName += "pdf";
        contentType = "application/pdf";
        break;
    case CrystalDecisions.Shared.ExportFormatType.WordForWindows : 
        tempFileName+= "doc";
        contentType = "application/msword";
        break;
    case CrystalDecisions.Shared.ExportFormatType.Excel : 
        tempFileName+= "xls";
        contentType = "application/vnd.ms-excel";
        break;
    case CrystalDecisions.Shared.ExportFormatType.HTML32 : 
    case CrystalDecisions.Shared.ExportFormatType.HTML40 : 
        tempFileName+= "htm";
        contentType = "text/html";
        CrystalDecisions.Shared.HTMLFormatOptions hop = new CrystalDecisions.Shared.HTMLFormatOptions();
        hop.HTMLBaseFolderName = tempDir;
        hop.HTMLFileName = tempFileName;
        selectedReport.ExportOptions.FormatOptions = hop;
        break;
    }

    CrystalDecisions.Shared.DiskFileDestinationOptions dfo = new CrystalDecisions.Shared.DiskFileDestinationOptions();
    dfo.DiskFileName = tempDir + tempFileName;
    selectedReport.ExportOptions.DestinationOptions = dfo;
    selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;

    selectedReport.Export();
    selectedReport.Close();

    string tempFileNameUsed;
    if (eft == CrystalDecisions.Shared.ExportFormatType.HTML32 || eft == CrystalDecisions.Shared.ExportFormatType.HTML40)
    {
        string[] fp = selectedReport.FilePath.Split("\\".ToCharArray());
        string leafDir = fp[fp.Length-1];
        // strip .rpt extension
        leafDir = leafDir.Substring(0, leafDir.Length);
        tempFileNameUsed = string.Format("{0}{1}\\{2}", tempDir, leafDir, tempFileName);
    }
    else
        tempFileNameUsed = tempDir + tempFileName;

    Response.ClearContent();
    Response.ClearHeaders();
    Response.ContentType = contentType;

    Response.WriteFile(tempFileNameUsed);
    Response.Flush();
    Response.Close();

    System.IO.File.Delete(tempFileNameUsed);
}
    }
}