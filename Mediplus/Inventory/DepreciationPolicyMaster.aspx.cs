﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;


namespace Mediplus.Inventory
{
    public partial class DepreciationPolicyMaster : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindBranch()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.BranchMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpBranch.DataSource = ds;
                drpBranch.DataValueField = "HBM_ID";
                drpBranch.DataTextField = "HBM_ID";
                drpBranch.DataBind();

            }

        }


        void BindDepreciation()
        {


            DataSet DS = new DataSet();


            string Criteria = " 1=1  AND  ADP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";


            MastersBAL objProd = new MastersBAL();
            DS = objProd.DepreciationPolicyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDepPolicy.DataSource = DS;
                gvDepPolicy.DataBind();
            }
            else
            {
                gvDepPolicy.DataBind();
            }
        }

        void BindDepMethord()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_TYPE ='DEPRECIATION_METHODS' ";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDepMethod.DataSource = DS;
                drpDepMethod.DataTextField = "AM_NAME";
                drpDepMethod.DataValueField = "AM_CODE";
                drpDepMethod.DataBind();

                drpDepMethod.Items.Insert(0, "--- Select ---");
                drpDepMethod.Items[0].Value = "";
            }
            else
            {
                drpDepMethod.DataBind();
            }


        }

        void Clear()
        {
            drpBranch.SelectedIndex = 0;
            txtCode.Text = "";
            txtName.Text = "";
            drpDepMethod.SelectedIndex = 0;
            txtValue.Text = "";
            chkStatus.Checked = true;
        }



        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            try
            {

                if (!IsPostBack)
                {

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    // txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindBranch();
                    BindDepMethord();
                    BindDepreciation();


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtCode.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter  Code','Please Enter  Code')", true);
                    goto FunEnd;
                }

                if (txtName.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Name','Please Enter Name')", true);
                    goto FunEnd;
                }
                if (drpDepMethod.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please select the Method','Please select the Method')", true);
                    goto FunEnd;
                }

                if (txtValue.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Value','Please Enter Value')", true);
                    goto FunEnd;
                }


                MastersBAL objMas = new MastersBAL();

                objMas.BranchID = Convert.ToString(Session["Branch_ID"]);
                objMas.ADP_CODE = txtCode.Text.Trim();
                objMas.ADP_NAME = txtName.Text;

                objMas.ADP_DEPRECIATION_METHOD = drpDepMethod.SelectedValue;
                objMas.ADP_VALUE = txtValue.Text.Trim();
                objMas.ADP_ACTIVE = Convert.ToString(chkStatus.Checked);
                objMas.DepreciationPolicyAdd();

                Clear();
                BindDepreciation();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('   Saved Successfully','Saved Successfully')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvDepPolicy.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvDepPolicy.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                Label lblgvBranchID = (Label)gvScanCard.Cells[0].FindControl("lblgvBranchID");
                Label lblgvCode = (Label)gvScanCard.Cells[0].FindControl("lblgvCode");
                Label lblgvName = (Label)gvScanCard.Cells[0].FindControl("lblgvName");
                Label lblgvMethod = (Label)gvScanCard.Cells[0].FindControl("lblgvMethod");
                Label lblgvValue = (Label)gvScanCard.Cells[0].FindControl("lblgvValue");
                Label lblgvActivce = (Label)gvScanCard.Cells[0].FindControl("lblgvActivce");


                drpBranch.SelectedValue = lblgvBranchID.Text;
                txtCode.Text = lblgvCode.Text.Trim();
                txtName.Text = lblgvName.Text.Trim();
                drpDepMethod.SelectedValue = lblgvMethod.Text.Trim();
                txtValue.Text = lblgvValue.Text.Trim();
                chkStatus.Checked = Convert.ToBoolean(lblgvActivce.Text.Trim());

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnJournalRefresh_Click(object sender, ImageClickEventArgs e)
        {
            BindDepreciation();
        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}