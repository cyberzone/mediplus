﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="DepreciationPolicyMaster.aspx.cs" Inherits="Mediplus.Inventory.DepreciationPolicyMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script language="javascript" type="text/javascript">

         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }

         function ShowTypeDive() {

             alter('ter');
         }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

     <table style="width: 75%">
            <tr>
                <td align="left">
                     <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Depreciation Policy"></asp:Label>
                    
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                             <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

         <div style="padding-top: 0px; width: 75%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;"> 
               <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>

                <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                     <td class="lblCaption1" style="width: 100px;">Branch
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                    <asp:DropDownList ID="drpBranch" runat="server" CssClass="TextBoxStyle" Width="90.5%" Height="25px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Code 
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCode" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Name
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                         
                </tr>
                <tr>

                    <td class="lblCaption1" style="width: 100px;">Method
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>
                                     <asp:DropDownList ID="drpDepMethod" runat="server" CssClass="TextBoxStyle" Width="90.5%" Height="25px">
                                        

                                  </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
               
                     <td class="lblCaption1" style="width: 100px;"> Value
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                       <asp:TextBox ID="txtValue" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                     <td class="lblCaption1" style="width: 100px;"> 
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="chkStatus" runat="server"  Text="Active" CssClass="TextBoxStyle" Checked="true" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                  
                    </tr>

                </table>

            <br />


              <div style="padding-top: 0px; width: 100%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                               <div style="width: 100%; text-align: right; float: right;">
                                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                    <ContentTemplate>

                                        <asp:ImageButton ID="btnJournalRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnJournalRefresh_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>


              <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvDepPolicy" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
                                                EnableModelValidation="True" Width="99%">
                                                <HeaderStyle CssClass="GridHeader_Gray" />
                                                <RowStyle CssClass="GridRow" />
                                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvTransNo" CssClass="lblCaption1" runat="server" OnClick="Select_Click">
                                                                <asp:Label ID="lblgvBranchID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ADP_BRANCH_ID") %>' Visible="false"></asp:Label>
                                                            
                                                                <asp:Label ID="lblgvCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ADP_CODE") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="  Name" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvName" CssClass="lblCaption1" runat="server" OnClick="Select_Click">
                                                                <asp:Label ID="lblgvName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ADP_NAME") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Method" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvMethod" CssClass="lblCaption1" runat="server" OnClick="Select_Click">
                                                                <asp:Label ID="lblgvMethod" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ADP_DEPRECIATION_METHOD") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Value" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvValue" CssClass="lblCaption1" runat="server" OnClick="Select_Click">
                                                                <asp:Label  ID="lblgvValue" CssClass="label" Font-Size="11px" Width="100%" runat="server" Text='<%# Bind("ADP_VALUE") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="Activce" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvActivce" CssClass="lblCaption1" runat="server" OnClick="Select_Click">
                                                                <asp:Label  ID="lblgvActivce" CssClass="label" Font-Size="11px" Width="100%" runat="server" Text='<%# Bind("ADP_ACTIVE") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
          </div>

             </div>
</asp:Content>
