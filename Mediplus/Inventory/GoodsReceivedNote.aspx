﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="GoodsReceivedNote.aspx.cs" Inherits="Mediplus.Inventory.GoodsReceivedNote" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

        
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }

    </script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

         function ValSave() {

             if (document.getElementById('<%=txtTransNo.ClientID%>').value == "") {
                 ShowErrorMessage('Trans. No', 'Please Enter Trans. No')
                     return false;
                 }


                 if (document.getElementById('<%=txtTransDate.ClientID%>').value == "") {

                     ShowErrorMessage('Trans. Date', 'Please Enter Trans. Date')
                     return false;
                 }

                 if (document.getElementById('<%=txtSupplier.ClientID%>').value == "") {

                     ShowErrorMessage('Supplier', 'Please Select Supplier')
                     return false;
                 }

         }

         function ShowErrorMessage(vMessage1, vMessage2) {
             document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
            document.getElementById("divMessage").style.display = 'block';

        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
           document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
       }




    function CalculateAmount() {

        var vQty = document.getElementById("<%=txtQty.ClientID%>").value;
        var vUnitPrice = document.getElementById("<%=txtUnitPrice.ClientID%>").value;
        vAmount = vQty * vUnitPrice;

        document.getElementById("<%=txtAmount.ClientID%>").value = vAmount;
    }

    function CalculateNetAmt() {

        var decTotalAmount = 0, decDiscount = 0, decDiscountAmount = 0, decNetAmount = 0;
        var vDiscType;


        decTotalAmount = document.getElementById("<%=txtTotalAmount.ClientID%>").value;
        vDiscType = document.getElementById("<%=drpDiscountType.ClientID%>").value;
        decDiscount = document.getElementById("<%=txtTotalDisc.ClientID%>").value;


        if (decTotalAmount != "" && decTotalAmount != 0 && decDiscount != "" && decDiscount != 0) {
            if (vDiscType == '$') {
                decDiscountAmount = decDiscount;
            }
            else {
                decDiscountAmount = (decTotalAmount * decDiscount) / 100;
            }

            decNetAmount = decTotalAmount - decDiscountAmount;

            document.getElementById("<%=txtNetAmount.ClientID%>").value = decNetAmount;
        }
        else {

            decNetAmount = decTotalAmount;
            document.getElementById("<%=txtNetAmount.ClientID%>").value = decNetAmount;
        }

    }
    function ShowMasterPopup() {

        document.getElementById("divGRNPopup").style.display = 'block';


    }

    function HideMasterPopup() {
        document.getElementById("<%=divGRNPopup.ClientID%>").style.display = 'none';


    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <input type="hidden" id="hidPaymentNo" runat="server" />
        <div>
            <div id="divMessageClose" style="display: none; position: absolute; top: 278px; left: 712px;">
                <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" />
            </div>
            <div style="padding-left: 60%; width: 100%;">
                 <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
            </div>
            <table style="width: 90%">
                <tr>
                    <td align="left">
                       
                    </td>
                </tr>
                
            </table>
            <div style="padding-top: 0px; width: 80%; height: 700px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">


                <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" GRN Entry " Width="100%">
                    <ContentTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td align="left">
                                   <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="GRN"></asp:Label>
                                              </td>
                                 <td align="right">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                           <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete"   OnClientClick="return window.confirm('Do you want to Delete this data?')" />

                                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save"  OnClientClick="return ValSave();" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                        </table>
                        <div style="padding-top: 0px; width: 100%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">

                             <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>

                            <div style="width: 100%; text-align: right; float: right;">
                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <ContentTemplate>

                                        <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <table cellpadding="5" cellspacing="5" style="width: 100%">
                                <tr>
                                    <td class="lblCaption1" style="width: 100px;">Trans. No.
                               <asp:ImageButton ID="imgGRNZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display GRN Entry" OnClick="imgGRNZoom_Click" />

                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTransNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" AutoPostBack="true" OnTextChanged="txtTransNo_TextChanged"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Date
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTransDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                                    Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1" style="width: 100px;">Ref. No.
 
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtReferanceNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Payment
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpPaymentType" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>

                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>

                                    <td class="lblCaption1" style="width: 100px;">Supplier
                                    </td>
                                    <td colspan="3">
                                        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtSupplier" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                                <div id="divwidth" style="visibility: hidden;"></div>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtSupplier" MinimumPrefixLength="1" ServiceMethod="GetSupplierList"
                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                                </asp:AutoCompleteExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Store
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpStore" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>

                                    <td class="lblCaption1">Status
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpStatus" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                                    <asp:ListItem Value="A">Active</asp:ListItem>
                                                    <asp:ListItem Value="I">InActive</asp:ListItem>

                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>

                                </tr>
                                    <td class="lblCaption1" style="width: 100px;">PO. No.
 
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtPONo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"  ></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    </tr>
                            </table>
                            <div style="padding: 10px;">

                                <table cellpadding="3" cellspacing="3" style="width: 100%">
                                    <tr>
                                        <td class="lblCaption1" style="width:40%;">Item Dtls
                                        </td>
                                         <td class="lblCaption1" style="width:10%;">Expiry Date
                                        </td>
                                        <td class="lblCaption1" style="width:10%;">U/M
                                        </td>
                                        <td class="lblCaption1" style="width:5%;">QTY
                                        </td>
                                        <td class="lblCaption1" style="width:10%;">Unit Price
                                        </td>

                                        <td class="lblCaption1" style="width:10%;">Amount
                                        </td>
                                        <td class="lblCaption1"  style="width:10%;">Tax Amount
                                        </td>
                                         <td class="lblCaption1" style="width:5%;">Tax %
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtProduct" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" AutoPostBack="true" OnTextChanged="txtProduct_TextChanged"></asp:TextBox>
                                                    <div id="div1" style="visibility: hidden;"></div>
                                                    <asp:AutoCompleteExtender ID="aceProduct" runat="Server" TargetControlID="txtProduct" MinimumPrefixLength="1" ServiceMethod="GetProductList"
                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                                                    </asp:AutoCompleteExtender>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                             <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                                    Enabled="True" TargetControlID="txtExpiryDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="drpUnitType" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>

                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtQty" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" Text="1" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmount()" AutoPostBack="true"  OnTextChanged="txtQty_TextChanged"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                <ContentTemplate>
                                                   
                                                    <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmount()" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged" ></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                                <ContentTemplate>
                                                     <input type="hidden" id="hidTaxType" runat="server" />
                                                    <asp:TextBox ID="txtTaxAmount" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtTaxPres" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);" Enabled="false" ></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>

                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                <ContentTemplate>
                                                    <asp:ImageButton ID="btnAddItem" runat="server" ImageUrl="~/Images/New.png" Style="height: 25px; width: 25px;" ToolTip="Create New Entry" OnClick="btnAddItem_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>
                                <div style="padding-top: 0px; width: 100%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvGRNTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                EnableModelValidation="True" Width="100%">
                                                <HeaderStyle CssClass="GridHeader_Gray" />
                                                <RowStyle CssClass="GridRow" />
                                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvItemCode" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_TRANS_NO") %>' Visible="false"></asp:Label>

                                                                <asp:Label ID="lblgvItemCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_ITEM_CODE") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvItemName" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvItemName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_ITEM_NAME") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Expiry Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvExpiryDate" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvExpiryDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_EXPIRY_DATEDesc") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="U/M" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvUnitType" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvUnitType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_UNIT_TYPE") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Qty" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvQty" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvQty" CssClass="label" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_QTY") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvUnitPrice" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvUnitPrice" CssClass="label" Width="70px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_UNIT_PRICE") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvAmount" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvAmount" CssClass="label" Width="70px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("AGRT_AMOUNT") %>'></asp:Label>

                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="15px" Width="15px"
                                                                OnClick="Delete_Click" />&nbsp;&nbsp;
                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <table cellpadding="5" cellspacing="5" style="width: 100%">
                                <tr>
                                    <td class="lblCaption1">Total Amount
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);" ReadOnly="true"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Discount Type
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpDiscountType" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="drpDiscountType_SelectedIndexChanged">
                                                    <asp:ListItem Value="$">Absolute</asp:ListItem>
                                                    <asp:ListItem Value="%">Percentage</asp:ListItem>

                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Discount
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTotalDisc" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtTotalDisc_TextChanged"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Net Amount
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtNetAmount" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);" ReadOnly="true"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <div id="divGRNPopup" runat="server" visible="false" style="overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 150px;">
                            <div style="width: 100%; text-align: right; float: right; position: absolute; top: 0px; right: 0px;">

                                <img src="../Images/Close.png" style="height: 25px; width: 25px;" onclick="HideMasterPopup()" />

                            </div>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="gvGRNMaster" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="GridHeader_Gray" />
                                            <RowStyle CssClass="GridRow" />
                                            <AlternatingRowStyle CssClass="GridAlterRow" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Trans. No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton11" CssClass="lblCaption1" runat="server" OnClick="GRNEdit_Click">
                                                            <asp:Label ID="lblgvTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRM_TRANS_NO") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkgvProdName" CssClass="lblCaption1" runat="server" OnClick="GRNEdit_Click">
                                                            <asp:Label ID="lblgvDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRM_TRANS_DATEDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment Type" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkgvType" CssClass="lblCaption1" runat="server" OnClick="GRNEdit_Click">
                                                            <asp:Label ID="lblgvPaymentType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRM_PAYMENT_TYPE") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Supplier" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkgvCategory" CssClass="lblCaption1" runat="server" OnClick="GRNEdit_Click">
                                                            <asp:Label ID="lblgvSupplier" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRM_SUPPLIER_NAME") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Amt" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkgvTotalAmt" CssClass="lblCaption1" runat="server" OnClick="GRNEdit_Click">
                                                            <asp:Label ID="lblgvTotalAmt" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRM_TOTAL_AMOUNT") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Net Amt" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkgvNetlAmt" CssClass="lblCaption1" runat="server" OnClick="GRNEdit_Click">
                                                            <asp:Label ID="lblgvNetAmt" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGRM_NET_AMOUNT") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </div>

                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Journal " Width="100%">
                    <ContentTemplate>
                         <div style="padding-top: 0px; width: 100%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                               <div style="width: 100%; text-align: right; float: right;">
                                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                    <ContentTemplate>

                                        <asp:ImageButton ID="btnJournalRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnJournalRefresh_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvJournal" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowDataBound="gvJournal_RowDataBound"
                                                EnableModelValidation="True" Width="99%">
                                                <HeaderStyle CssClass="GridHeader_Gray" />
                                                <RowStyle CssClass="GridRow" />
                                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvTransNo" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("TransNo") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblgvTransType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("TransType") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblgvTransDateDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("TransDateDesc") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Account Name" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvName" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvTransDesc" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:Label ID="lblgvTransDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("TransDesc") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Dr" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvAmount" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:TextBox  ID="lblgvAmountDr" CssClass="label" Font-Size="11px" Width="100%" runat="server" style="text-align:right;padding-right:5px;border:none;" Text='<%# Bind("Amount") %>'></asp:TextBox>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cr" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvAmountCr" CssClass="lblCaption1" runat="server" OnClick="GRNTransEdit_Click">
                                                                <asp:TextBox ID="lblgvAmountCr" CssClass="label" Font-Size="11px" Width="100%" runat="server" style="text-align:right;padding-right:5px;border:none;"  Text='<%# Bind("Amount") %>'></asp:TextBox>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>


                </div>
        </div>
  

   </asp:Content>
