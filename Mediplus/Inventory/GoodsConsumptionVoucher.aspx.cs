﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;
namespace Mediplus.Inventory
{
    public partial class GoodsConsumptionVoucher : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();
        CommonBAL objCom = new CommonBAL();
        ProductMasterBAL objProd = new ProductMasterBAL();
        GoodsConsumptionVoucherBAL objGCV = new GoodsConsumptionVoucherBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUnitType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='UNITS_MEASUREMENT'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpUnitType.DataSource = DS;
                drpUnitType.DataTextField = "AM_NAME";
                drpUnitType.DataValueField = "AM_CODE";
                drpUnitType.DataBind();
            }
            //drpUnitType.Items.Insert(0, "Select");
            //drpUnitType.Items[0].Value = "";


        }

        void BindDepartment()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HDM_STATUS='A'  AND HDM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            CommonBAL objMast = new CommonBAL();
            DS = objMast.DepMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = DS;
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
            }
            drpDepartment.Items.Insert(0, "--- Select ---");
            drpDepartment.Items[0].Value = "";


        }

      
        void BindGCVMaster()
        {
            objGCV = new GoodsConsumptionVoucherBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND AGCM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            DS = objGCV.GoodsConsumptionVoucherMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["SaveMode"] = "M";

                    txtTransDate.Text = Convert.ToString(DR["AGCM_TRANS_DATEDesc"]);
                    txtReferanceNo.Text = Convert.ToString(DR["AGCM_REF_NO"]);
                    drpDepartment.SelectedValue = Convert.ToString(DR["AGCM_DEPARTMENT"]);
                 
                    drpStatus.SelectedValue = Convert.ToString(DR["AGCM_STATUS"]);

                    txtRemarks.Text = Convert.ToString(DR["AGCM_REMARKS"]);

                }
            }

        }

        void BindGCVMasterGrid()
        {
            objGCV = new GoodsConsumptionVoucherBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";


            DS = objGCV.GoodsConsumptionVoucherMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGCVMaster.DataSource = DS;
                gvGCVMaster.DataBind();
            }
            else
            {
                gvGCVMaster.DataBind();
            }
        }

        void BuildGCVTransDtls()
        {
            string Criteria = "1=2";
            objGCV = new GoodsConsumptionVoucherBAL();
            DataSet DS = new DataSet();

            DS = objGCV.GoodsConsumptionVoucherTransGet(Criteria);

            ViewState["GCVTrans"] = DS.Tables[0];
        }

        void BindGCVTransDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND AGCT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND AGCT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            objGCV = new GoodsConsumptionVoucherBAL();
            DataSet DS = new DataSet();

            DS = objGCV.GoodsConsumptionVoucherTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["GCVTrans"] = DS.Tables[0];

            }
        }

        void BindTempGCVTransDtls()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["GCVTrans"];
            if (DT.Rows.Count > 0)
            {
                gvGCVTrans.DataSource = DT;
                gvGCVTrans.DataBind();

            }
            else
            {
                gvGCVTrans.DataBind();
            }

        }

        void Clear()
        {
            // objGCV.AGCT_REF_NO = "";
            //objGCV.AGCT_PAYMENT_TYPE = drpPaymentType.SelectedValue;

            txtReferanceNo.Text = "";
            //drpPaymentType.SelectedIndex = 0;
            //txtSupplier.Text = "";
            //drpStore.SelectedIndex = 0;

            drpDepartment.SelectedIndex = 0;
            drpStatus.SelectedIndex = 0;
            //objGCV.AGCT_TOTAL_AMOUNT = txtTotalAmount.Text = "";

            //drpDiscountType.SelectedIndex = 0;
            //txtTotalDisc.Text = "";
            //txtNetAmount.Text = "";

            txtRemarks.Text = "";

            BuildGCVTransDtls();
            BindTempGCVTransDtls();

            ViewState["SaveMode"] = "A";

            divGCVPopup.Visible = false;
        }

        void ClearTrans()
        {
            txtProduct.Text = "";

            if (drpUnitType.Items.Count > 0)
                drpUnitType.SelectedIndex = 0;
            txtQty.Text = "";
            txtDesc.Text = "";

            ViewState["gvServSelectIndex"] = "";
        }

        
        #endregion

        #region AutoExt
      
        [System.Web.Services.WebMethod]
        public static string[] GetProductList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 AND APM_ACTIVE = 'A'   ";
            Criteria += " AND (APM_CODE like '%" + prefixText + "%' OR  APM_NAME like '%" + prefixText + "%' )";

            ProductMasterBAL objProd = new ProductMasterBAL();

            string[] Data;

            ds = objProd.ProductMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["APM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["APM_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            try
            {

                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");



                    //objCom = new CommonBAL();
                    txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GCV");
                    BuildGCVTransDtls();
                    BindDepartment();
                    BindUnitType();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtTransNo.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. No.','Red')", true);
                    goto FunEnd;
                }

                if (txtTransDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. Date','Red')", true);
                    goto FunEnd;
                }


                objGCV = new GoodsConsumptionVoucherBAL(); ;
                objGCV.BranchID = Convert.ToString(Session["Branch_ID"]);
                objGCV.AGCM_TRANS_NO = txtTransNo.Text.Trim();
                objGCV.AGCM_TRANS_DATE = txtTransDate.Text.Trim();
                objGCV.AGCM_REF_NO = txtReferanceNo.Text;
                objGCV.AGCM_DEPARTMENT = drpDepartment.SelectedValue;
                objGCV.AGCM_REMARKS = txtRemarks.Text;
             
               
                objGCV.AGCM_STATUS = drpStatus.SelectedValue;
                

                objGCV.MODE = Convert.ToString(ViewState["SaveMode"]);
                objGCV.UserID = Convert.ToString(Session["User_ID"]);

                string strTransNo = "";
                strTransNo = objGCV.GoodsConsumptionVoucherMasterAdd();

                txtTransNo.Text = strTransNo;


                objCom = new CommonBAL();
                string Criteria = " AGCT_TRANS_NO = '" + txtTransNo.Text.Trim() + "'";

                objCom.fnDeleteTableData("AC_GOODS_CONSUMPTION_VOUCHER_TRANS", Criteria);

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["GCVTrans"];

                foreach (DataRow DR in DT.Rows)
                {
                    objGCV.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objGCV.AGCT_TRANS_NO = txtTransNo.Text.Trim();
                    objGCV.AGCT_SL_NO = Convert.ToString(DR["AGCT_SL_NO"]);
                    objGCV.AGCT_ITEM_CODE = Convert.ToString(DR["AGCT_ITEM_CODE"]);
                    objGCV.AGCT_ITEM_NAME = Convert.ToString(DR["AGCT_ITEM_NAME"]);
                    objGCV.AGCT_UNIT_TYPE = Convert.ToString(DR["AGCT_UNIT_TYPE"]);
                    objGCV.AGCT_QTY = Convert.ToString(DR["AGCT_QTY"]);
                    objGCV.AGCT_DESC = Convert.ToString(DR["AGCT_DESC"]);
                    objGCV.GoodsConsumptionVoucherTransAdd();
                }





                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GCV");
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('   Saved Successfully','Green')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnCtlrRefresh_Click(object sender, EventArgs e)
        {
            txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GCV");
            Clear();
        }

        protected void imgGCVZoom_Click(object sender, ImageClickEventArgs e)
        {
              BindGCVMasterGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
              divGCVPopup.Visible = true;
        }

        protected void btnAddItem_Click(object sender, ImageClickEventArgs e)
        {
            try
            {


                string strTestCodeDesc = "", ItemCode = "", ItemDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["GCVTrans"];

                strTestCodeDesc = txtProduct.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Producte','Red')", true);

                    goto FunEnd;
                }
                ItemCode = arrTestCodeDesc[0];
                ItemDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["AGCT_ITEM_CODE"] = ItemCode;
                    DT.Rows[R]["AGCT_ITEM_NAME"] = ItemDesc;

                    DT.Rows[R]["AGCT_UNIT_TYPE"] = drpUnitType.SelectedValue;
                    DT.Rows[R]["AGCT_QTY"] = txtQty.Text.Trim();
                    DT.Rows[R]["AGCT_DESC"] = txtDesc.Text.Trim();
                    DT.AcceptChanges();
                }
                else
                {
                    objrow["AGCT_ITEM_CODE"] = ItemCode;
                    objrow["AGCT_ITEM_NAME"] = ItemDesc;

                    objrow["AGCT_UNIT_TYPE"] = drpUnitType.SelectedValue;
                    objrow["AGCT_QTY"] = txtQty.Text.Trim();
                    objrow["AGCT_DESC"] = txtDesc.Text.Trim();


                    DT.Rows.Add(objrow);
                }


                ViewState["GCVTrans"] = DT;


                BindTempGCVTransDtls();
                ClearTrans();

 

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddItem_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void GCVTransEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvItemCode = (Label)gvScanCard.Cells[0].FindControl("lblgvItemCode");
            Label lblgvItemName = (Label)gvScanCard.Cells[0].FindControl("lblgvItemName");
            Label lblgvUnitType = (Label)gvScanCard.Cells[0].FindControl("lblgvUnitType");

            Label lblgvQty = (Label)gvScanCard.Cells[0].FindControl("lblgvQty");
            Label lblgvUnitPrice = (Label)gvScanCard.Cells[0].FindControl("lblgvUnitPrice");
            Label lblgvAmount = (Label)gvScanCard.Cells[0].FindControl("lblgvAmount");
            Label lblgvDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvDesc");

            txtProduct.Text = lblgvItemCode.Text + "~" + lblgvItemName.Text;

            drpUnitType.SelectedValue = lblgvUnitType.Text;
            txtQty.Text = lblgvQty.Text;
            txtDesc.Text = lblgvDesc.Text;
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["GCVTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["GCVTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["GCVTrans"] = DT;

                }


                BindTempGCVTransDtls();
               

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void txtTransNo_TextChanged(object sender, EventArgs e)
        {
            Clear();
            BindGCVMaster();
            BindGCVTransDtls();
            BindTempGCVTransDtls();

        }

        protected void GCVEdit_Click(object sender, EventArgs e)
        {
            Clear();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvTransNo = (Label)gvScanCard.Cells[0].FindControl("lblgvTransNo");

            txtTransNo.Text = lblgvTransNo.Text;


            divGCVPopup.Visible = false;
            BindGCVMaster();
            BindGCVTransDtls();
            BindTempGCVTransDtls();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideTestMasterPopup()", true);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string Criteria = " AGCT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_GOODS_CONSUMPTION_VOUCHER_TRANS", Criteria);

                string Criteria1 = " AGCM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_GOODS_CONSUMPTION_VOUCHER_MASTER", Criteria1);


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' Data Deleted ','Data Deleted ')", true);
                Clear();
                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GCV");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }
    }
}