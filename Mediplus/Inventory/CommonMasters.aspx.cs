﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;


namespace Mediplus.Inventory
{
    public partial class CommonMasters : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCommonMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";

            if (drpType.SelectedIndex != 0)
            {
                Criteria += " AND AM_TYPE ='" + drpType.SelectedValue + "'";
            }

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvCommonMaster.DataSource = DS;
                gvCommonMaster.DataBind();
            }
            else
            {
                gvCommonMaster.DataBind();
            }


        }

 

        void Clear()
        {
            txtCode.Text = "";
            txtName.Text = "";
            chkStatus.Checked = true;
            txtCode.Enabled = true;
            drpType.SelectedIndex = 0;
            txtOrder.Text = "";
        }

        #endregion
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            lblStatus.Text = "";
            if (!IsPostBack)
            {
                string strType = Convert.ToString(Request.QueryString["Type"]);

                if (strType != "")
                {

                    for (int intCount = 0; intCount < drpType.Items.Count; intCount++)
                    {
                        if (drpType.Items[intCount].Value == strType)
                        {
                            drpType.SelectedValue = strType;
                            goto ForTitle;
                        }

                    }
                ForTitle: ;
                }

                BindCommonMaster();
            }
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvCommonMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvCommonMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");
                Label lblType = (Label)gvScanCard.Cells[0].FindControl("lblType");
                Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");
                Label lblOrder = (Label)gvScanCard.Cells[0].FindControl("lblOrder");

                txtCode.Text = lblCode.Text;
                txtCode.Enabled = false;
                txtName.Text = lblName.Text;
                drpType.SelectedValue = lblType.Text;
                chkStatus.Checked = Convert.ToBoolean(lblStatus.Text);
                txtOrder.Text = lblOrder.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Dept Code";
                    goto FunEnd;
                }

                if (txtName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Dept Name";
                    goto FunEnd;
                }


                DataSet DS = new DataSet();
                string Criteria = " 1=1 AND AM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND AM_TYPE ='" + drpType.SelectedValue + "' ";
                Criteria += " AND  (  AM_CODE = '" + txtCode.Text.Trim() + "'  ) ";


                if (txtCode.Text.Trim() != "" && txtCode.Text.Trim() != null)
                {
                    Criteria += " AND  AM_CODE != '" + txtCode.Text.Trim() + "'";
                }

                MastersBAL objMast = new MastersBAL();
                DS = objMast.MastersGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Same master Dtls Already Exists";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                objMast.BranchID = Convert.ToString(Session["Branch_ID"]);
                objMast.AM_CODE = txtCode.Text.Trim();
                objMast.AM_NAME = txtName.Text.Trim();
                objMast.AM_TYPE = drpType.SelectedValue;
                objMast.AM_ACTIVE = Convert.ToString(chkStatus.Checked);
                objMast.AM_ORDER = txtOrder.Text.Trim();
                objMast.MastersAdd();

                Clear();
                BindCommonMaster();
                lblStatus.Text = "Saved Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;




               Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
               Label lblType = (Label)gvScanCard.Cells[0].FindControl("lblType");
                eAuthorizationBAL eAuth = new eAuthorizationBAL();


                ProductMasterBAL   objProd = new ProductMasterBAL();
                DataSet DSPay = new DataSet();
                string Criteria1 = " 1=1 ";
                Criteria1 += " AND ( APM_LOCATION='" + lblCode.Text + "' OR  APM_CATEGORY='" + lblCode.Text + "'  ) AND APM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSPay = objProd.ProductMasterGet(Criteria1);

                if (DSPay.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This Master Dtls is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                //ReceiptEntryBAL objRec = new ReceiptEntryBAL();
                //DataSet DSRec = new DataSet();

                //string Criteria2 = " 1=1 ";
                //Criteria2 += " AND  ARE_ACC_CODE='" + lblDeptCode.Text.Trim() + "' AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                //DSRec = objRec.ReceiptEntryGet(Criteria2);

                //if (DSRec.Tables[0].Rows.Count > 0)
                //{

                //    lblStatus.Text = "This Account Code is Involved another transaction ";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;

                //}


                CommonBAL objCom = new CommonBAL();
                string Criteria = " AM_CODE='" + lblCode.Text + "' AND AM_TYPE='" + lblType.Text + "'";
                objCom.fnDeleteTableData("AC_MASTERS", Criteria);



                lblStatus.Text = " Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindCommonMaster();


            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            BindCommonMaster();
        }
     
        protected void drpType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtOrder.Text = "1";
           
           DataSet DS = new DataSet();
           string Criteria = " 1=1  AND AM_TYPE ='" + drpType.SelectedValue + "'";

           CommonBAL objCom = new CommonBAL();

          DS = objCom.fnGetFieldValue(" MAX(ISNULL(AM_ORDER,0)) + 1  as OrderNo", "AC_MASTERS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtOrder.Text =  Convert.ToString( DS.Tables[0].Rows[0]["OrderNo"]);
            }

            BindCommonMaster();
        }


   #endregion

    }
}