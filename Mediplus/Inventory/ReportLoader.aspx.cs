﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Inventory
{
    public partial class ReportLoader : System.Web.UI.Page
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='PROD_CATEGORY'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpPropCategory.DataSource = DS;
                drpPropCategory.DataTextField = "AM_NAME";
                drpPropCategory.DataValueField = "AM_CODE";
                drpPropCategory.DataBind();
            }
            drpPropCategory.Items.Insert(0, "--- All ---");
            drpPropCategory.Items[0].Value = "";


        }


        void BindDepartment()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HDM_STATUS='A'  AND HDM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            CommonBAL objMast = new CommonBAL();
            DS = objMast.DepMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = DS;
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
            }
            drpDepartment.Items.Insert(0, "--- Select ---");
            drpDepartment.Items[0].Value = "";


        }

        void BindReport()
        {
            string Criteria = " 1=1  and type  ='" + hidPageName.Value + "'";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.MediplusReportGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpReport.DataSource = ds;
                drpReport.DataValueField = "Code";
                drpReport.DataTextField = "Name";
                drpReport.DataBind();

            }

            drpReport.Items.Insert(0, "--- Select --");
            drpReport.Items[0].Value = "";

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='" + hidPageName.Value + "' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {


            }


            if (strPermission == "7")
            {





            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");

            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);

               // txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
               // txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                BindCategory();
                BindReport();
                BindDepartment();
            }

        }
    }
}