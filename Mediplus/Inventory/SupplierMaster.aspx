﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="SupplierMaster.aspx.cs" Inherits="Mediplus.Inventory.SupplierMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

       
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }

    </script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>
      <script type="text/javascript">
          function ShowMessage() {
              $("#myMessage").show();
              setTimeout(function () {
                  var selectedEffect = 'blind';
                  var options = {};
                  $("#myMessage").hide();
              }, 2000);
              return true;
          }

          function ShowErrorMessage(vMessage, vColor) {

              document.getElementById("divMessage").style.display = 'block';
              document.getElementById("divMessageClose").style.display = 'block';
              document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
            document.getElementById("<%=lblMessage.ClientID%>").style.color = vColor;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("divMessageClose").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
    }

    function ShowMasterPopup() {

        document.getElementById("divSupplierPopup").style.display = 'block';


    }

    function HideMasterPopup() {

      
        document.getElementById("<%=divSupplierPopup.ClientID%>").style.display = 'none';

    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div>
             <div id="divMessageClose" style="display: none; position: absolute; top: 278px; left: 712px;">
                <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" />
            </div>
            <div style="padding-left: 60%; width: 100%;">
                <div id="divMessage" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 200px;">

                    <table cellpadding="0" cellspacing="0" width="100%">

                        <tr>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" CssClass="label" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                    </table>


                    <br />

                </div>
            </div>
            <table style="width: 80%">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Supplier Master"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                   <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 80%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">


                <div style="width: 100%; text-align: right; float: right;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>

                            <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>


                <table cellpadding="5" cellspacing="5" style="width: 100%">
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Supplier Code
                             <asp:ImageButton ID="imgSupplierZoom"   runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Receipt Entry" OnClick="imgSupplierZoom_Click" />

                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSupplierCode" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                         <td class="lblCaption1">Status
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                        <asp:ListItem Value="I">InActive</asp:ListItem>

                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                         <td class="lblCaption1" style="width: 100px;">Supplier Name
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSupplierName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                       <td class="lblCaption1" style="width: 100px;">Cont. Person
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtContPerson" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"  ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1" style="width: 100px;">Mobile
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtMobile" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"  ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                     </tr>
                    <tr>
                         <td class="lblCaption1" style="width: 100px;">Phone
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                         <td class="lblCaption1" style="width: 100px;">Address
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="TextBoxStyle" Width="100%" Height="50px"  TextMode="MultiLine"  ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                         <td class="lblCaption1" style="width: 100px;">Credit Limit
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCreditLimit" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                         <td class="lblCaption1" style="width: 100px;">Credit Days
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCreditDays" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"   onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Supplier A/C
                        </td>
                        <td colspan="5">
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAccountName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>
                                    <div id="divwidth" style="visibility: hidden;"></div>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtAccountName" MinimumPrefixLength="1" ServiceMethod="GetAccountList"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                    </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                         <td class="lblCaption1" style="width: 100px;">TRN Number
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtTRNNumber" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"  ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>

            </div>

              <div id="divSupplierPopup"  runat="server"  visible="false" style="overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 150px;">
                <div style="width: 100%; text-align: right; float: right;position:absolute; top:0px;right:0px;">
                            
                            <img src="../Images/Close.png"  style="height: 25px; width: 25px;"   onclick="HideMasterPopup()" />
                             
                        </div>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                             <asp:gridview id="gvSupplier" runat="server" allowsorting="True" autogeneratecolumns="False"
                                enablemodelvalidation="True" width="100%">
                                                                 <HeaderStyle CssClass="GridHeader_Gray" />
                                                                    <RowStyle CssClass="GridRow" />
                                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                                <Columns>
                                                                    
                                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="LinkButton11" CssClass="lblCaption1"  runat="server"  OnClick="SupplierEdit_Click"   >
                                                                            <asp:Label ID="lblgvSuppCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ASM_CODE") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkgvProdName" CssClass="lblCaption1"  runat="server"  OnClick="SupplierEdit_Click"   >
                                                                            <asp:Label ID="lblgvSuppName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ASM_NAME") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Contact Person" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkgvType" CssClass="lblCaption1"  runat="server"  OnClick="SupplierEdit_Click"   >
                                                                            <asp:Label ID="lblgvType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ASM_CONT_PERSON") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkgvCategory" CssClass="lblCaption1"  runat="server"  OnClick="SupplierEdit_Click"   >
                                                                            <asp:Label ID="lblgvCategory" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ASM_MOBILE") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     
                                                                      <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkgvStatus" CssClass="lblCaption1"  runat="server"  OnClick="SupplierEdit_Click"   >
                                                                            <asp:Label ID="lblgvStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ASM_ACTIVEDesc") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:gridview>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
                   

   </asp:Content>