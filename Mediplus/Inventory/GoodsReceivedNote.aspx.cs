﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Inventory
{
    public partial class GoodsReceivedNote : System.Web.UI.Page
    {

        MastersBAL objMast = new MastersBAL();
        CommonBAL objCom = new CommonBAL();
        ProductMasterBAL objProd = new ProductMasterBAL();

        GoodsReceivedNoteBAL objGRN = new GoodsReceivedNoteBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUnitType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='UNITS_MEASUREMENT'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpUnitType.DataSource = DS;
                drpUnitType.DataTextField = "AM_NAME";
                drpUnitType.DataValueField = "AM_CODE";
                drpUnitType.DataBind();
            }
            //drpUnitType.Items.Insert(0, "Select");
            //drpUnitType.Items[0].Value = "";


        }

        void BindStore()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='PROD_STORE'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpStore.DataSource = DS;
                drpStore.DataTextField = "AM_NAME";
                drpStore.DataValueField = "AM_CODE";
                drpStore.DataBind();
            }
            drpStore.Items.Insert(0, "--- Select ---");
            drpStore.Items[0].Value = "";


        }

        void BindGRNMaster()
        {
            objGRN = new GoodsReceivedNoteBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND AGRM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            DS = objGRN.GoodsReceivedNoteMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["SaveMode"] = "M";


                    txtTransDate.Text = Convert.ToString(DR["AGRM_TRANS_DATEDesc"]);
                    txtReferanceNo.Text = Convert.ToString(DR["AGRM_REF_NO"]);
                    drpPaymentType.SelectedValue = Convert.ToString(DR["AGRM_PAYMENT_TYPE"]);
                    txtSupplier.Text = Convert.ToString(DR["AGRM_SUPPLIER_CODE"]) + "~" + Convert.ToString(DR["AGRM_SUPPLIER_NAME"]);

                    drpStore.SelectedValue = Convert.ToString(DR["AGRM_STORE"]);
                    drpStatus.SelectedValue = Convert.ToString(DR["AGRM_STATUS"]);
                    txtTotalAmount.Text = Convert.ToString(DR["AGRM_TOTAL_AMOUNT"]);
                    drpDiscountType.SelectedValue = Convert.ToString(DR["AGRM_DISC_TYPE"]);
                    txtTotalDisc.Text = Convert.ToString(DR["AGRM_DISC_AMOUNT"]);
                    txtNetAmount.Text = Convert.ToString(DR["AGRM_NET_AMOUNT"]);
                    txtPONo.Text = Convert.ToString(DR["AGRM_PO_NO"]);

                }
            }
        }

        void BindGRNMasterGrid()
        {
            objGRN = new GoodsReceivedNoteBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";


            DS = objGRN.GoodsReceivedNoteMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGRNMaster.DataSource = DS;
                gvGRNMaster.DataBind();
            }
            else
            {
                gvGRNMaster.DataBind();
            }
        }

        void BuildeGRNTransDtls()
        {
            string Criteria = "1=2";
            objGRN = new GoodsReceivedNoteBAL();
            DataSet DS = new DataSet();

            DS = objGRN.GoodsReceivedNoteTransGet(Criteria);

            ViewState["GRNTrans"] = DS.Tables[0];
        }

        void BindGRNTransDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND AGRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND AGRT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            objGRN = new GoodsReceivedNoteBAL();
            DataSet DS = new DataSet();

            DS = objGRN.GoodsReceivedNoteTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["GRNTrans"] = DS.Tables[0];

            }
        }

        void BindTempGRNTransDtls()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["GRNTrans"];
            if (DT.Rows.Count > 0)
            {
                gvGRNTrans.DataSource = DT;
                gvGRNTrans.DataBind();

            }
            else
            {
                gvGRNTrans.DataBind();
            }

        }

        void CalculateTotalAmount()
        {
            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["GRNTrans"];

            decimal decTotalAmount = 0, decDiscount = 0;
            foreach (DataRow DR in DT.Rows)
            {
                decTotalAmount = decTotalAmount + Convert.ToDecimal(DR["AGRT_AMOUNT"]);

            }

            txtTotalAmount.Text = Convert.ToString(decTotalAmount);

            decimal decDiscountAmount = 0, decNetAmount = 0;


            if (decTotalAmount != 0 && txtTotalDisc.Text.Trim() != "")
            {

                decDiscount = Convert.ToDecimal(txtTotalDisc.Text.Trim());


                if (drpDiscountType.SelectedValue == "$")
                {
                    decDiscountAmount = decDiscount;
                }
                else
                {
                    decDiscountAmount = Convert.ToDecimal((decTotalAmount * decDiscount) / 100);//.ToString("#0.00");
                }

                decNetAmount = decTotalAmount - decDiscountAmount;

                txtNetAmount.Text = Convert.ToString(decNetAmount);
            }
            else
            {
                decNetAmount = decTotalAmount;
                txtNetAmount.Text = Convert.ToString(decNetAmount);
            }

        }

        void BindPaymentEntry()
        {
            PaymentEntryBAL objPay = new PaymentEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND  APE_ENTRY_REF_NO='" + txtTransNo.Text.Trim() + "' AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objPay.PaymentEntryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    hidPaymentNo.Value = Convert.ToString(DR["APE_TRANS_NO"]);
                }

            }

        }

        void GetProdAC(string ItemCode, out string ProdACCode, out string ProdACName)
        {
            ProdACCode = "";
            ProdACName = "";
            objProd = new ProductMasterBAL();
            string Criteria = " 1=1 AND APM_ACTIVE = 'A'   ";
            Criteria += " AND APM_CODE='" + ItemCode + "'";

            DataSet DS = new DataSet();

            DS = objProd.ProductMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string ProdType = Convert.ToString(DS.Tables[0].Rows[0]["APM_TYPE"]);
                if (ProdType == "Inventory")
                {
                    ProdACCode = Convert.ToString(DS.Tables[0].Rows[0]["APM_INVENTORY_AC_CODE"]);
                    ProdACName = Convert.ToString(DS.Tables[0].Rows[0]["APM_INVENTORY_AC_NAME"]);
                }
                else
                {
                    ProdACCode = Convert.ToString(DS.Tables[0].Rows[0]["APM_FIXED_ASSET_CODE"]);
                    ProdACName = Convert.ToString(DS.Tables[0].Rows[0]["APM_FIXED_ASSET_NAME"]);
                }
            }



        }

        void GetSupplierAC(string SuppCode, out string SuppACCode, out string SuppACName)
        {
            SuppACCode = "";
            SuppACName = "";

            SuppliertMasterBAL objMast = new SuppliertMasterBAL();
            string Criteria = " 1=1 AND ASM_ACTIVE = 'A'   ";
            Criteria += " AND ASM_CODE='" + SuppCode + "'";

            DataSet DS = new DataSet();

            DS = objMast.SuppliertMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                SuppACCode = Convert.ToString(DS.Tables[0].Rows[0]["ASM_ACCOUNT_CODE"]);
                SuppACName = Convert.ToString(DS.Tables[0].Rows[0]["ASM_ACCOUNT_NAME"]);
            }



        }


        DataSet GetProductDtls(string ProdCode)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND APM_ACTIVE = 'A'   ";
            Criteria += " AND APM_CODE ='" + ProdCode + "'";

            ProductMasterBAL objProd = new ProductMasterBAL();
            DS = objProd.ProductMasterGet(Criteria);


            return DS;
        }


        void SavePayment()
        {

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["GRNTrans"];

            string SuppACCode, SuppACName;
            string strSuppName = txtSupplier.Text.Trim();
            string SuppCode = "";

            string[] arrSuppName = strSuppName.Split('~');
            if (arrSuppName.Length > 0)
            {
                SuppCode = arrSuppName[0];

            }

            objGRN.AGRM_SUPPLIER_CODE = SuppCode;


            GetSupplierAC(SuppCode, out    SuppACCode, out  SuppACName);



            PaymentEntryBAL objPay = new PaymentEntryBAL();
            string transNumber = "";

            if (Convert.ToString(ViewState["SaveMode"]) == "M")
            {
                transNumber = hidPaymentNo.Value;
            }
            else
            {
                transNumber = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
            }

            objPay.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPay.APE_TRANS_NO = transNumber;
            objPay.APE_DATE = txtTransDate.Text.Trim();

            if (drpPaymentType.SelectedValue == "Credit")
            {
                objPay.APE_TYPE = "BANK";
            }
            else
            {
                objPay.APE_TYPE = "CASH";
            }
            objPay.APE_STATUS = "A";
            objPay.APE_ACC_CODE = SuppACCode;
            objPay.APE_ACC_NAME = SuppACName;
            objPay.APE_AMOUNT = txtTotalAmount.Text.Trim();
            objPay.APE_CHEQUE_NO = "";
            objPay.APE_CHEQUE_DATE = "";
            objPay.APE_BANK_NAME = "";
            objPay.APE_CC_TYPE = "";
            objPay.APE_CC_NO = "";
            objPay.APE_CC_HOLDER = "";
            objPay.APE_DESCRIPTION = "Entry From GRN";
            objPay.APE_REF_NO = txtReferanceNo.Text.Trim();
            objPay.APE_ENTRY_FROM = "GRN";
            objPay.APE_ENTRY_REF_NO = txtTransNo.Text.Trim();
            objPay.MODE = Convert.ToString(ViewState["SaveMode"]);
            objPay.UserID = Convert.ToString(Session["User_ID"]);
            objPay.PaymentEntryAdd();


            string strAccName = txtSupplier.Text.Trim();
            string AccCode = "", AccName = "";

            string[] arrAccName = strAccName.Split('~');
            if (arrAccName.Length > 0)
            {
                AccCode = arrAccName[0];
                if (arrAccName.Length > 1)
                {
                    AccName = arrAccName[1];
                }
            }




            if (Convert.ToString(ViewState["SaveMode"]) == "M")
            {
                objCom = new CommonBAL();

                string Criteria = " 1=1 ";

                Criteria += " AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APE_TRANS_NO ='" + transNumber + "'";

                objCom.fnDeleteTableData("AC_PAYMENT_EXPENSES", Criteria);

            }

            foreach (DataRow DR in DT.Rows)
            {
                string ProdACCode, ProdACName;
                GetProdAC(Convert.ToString(DR["AGRT_ITEM_CODE"]), out ProdACCode, out ProdACName);

                objPay = new PaymentEntryBAL();
                objPay.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPay.APE_TRANS_NO = transNumber;
                objPay.APE_ACC_CODE = ProdACCode;
                objPay.APE_ACC_NAME = ProdACName;
                objPay.APE_COST_CENT_CODE = "";
                objPay.APE_COST_CENT_NAME = "";
                objPay.APE_SUPPLIER_CODE = AccCode;
                objPay.APE_SUPPLIER_NAME = AccName;

                objPay.APE_AMOUNT = Convert.ToString(DR["AGRT_AMOUNT"]);
                objPay.PaymentExpensesAdd();

            }

        }

        void Clear()
        {

            objGRN.AGRM_PAYMENT_TYPE = drpPaymentType.SelectedValue;

            txtReferanceNo.Text = "";
            drpPaymentType.SelectedIndex = 0;
            txtSupplier.Text = "";
            drpStore.SelectedIndex = 0;

            drpStatus.SelectedIndex = 0;
            objGRN.AGRM_TOTAL_AMOUNT = txtTotalAmount.Text = "";

            drpDiscountType.SelectedIndex = 0;
            txtTotalDisc.Text = "";
            txtNetAmount.Text = "";

            txtPONo.Text = "";

            BuildeGRNTransDtls();
            BindTempGRNTransDtls();

            ViewState["SaveMode"] = "A";

            divGRNPopup.Visible = false;

            hidPaymentNo.Value = "";
        }

        void ClearTrans()
        {
            txtProduct.Text = "";
            txtExpiryDate.Text = "";
            if (drpUnitType.Items.Count > 0)
                drpUnitType.SelectedIndex = 0;
            txtQty.Text = "";
            txtUnitPrice.Text = "";
            txtAmount.Text = "";

            ViewState["gvServSelectIndex"] = "";
        }


        void BindJournalGrid()
        {
            objGRN = new GoodsReceivedNoteBAL();

            DataSet DS = new DataSet();

            string Criteria = txtTransNo.Text.Trim();


            DS = objGRN.GRNJournalEntryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvJournal.DataSource = DS;
                gvJournal.DataBind();
            }
            else
            {
                gvJournal.DataBind();
            }
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetSupplierList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 AND ASM_ACTIVE = 'A'   ";
            Criteria += " AND (ASM_CODE like '%" + prefixText + "%' OR  ASM_NAME like '%" + prefixText + "%' )";


            string[] Data;
            SuppliertMasterBAL objMast = new SuppliertMasterBAL();
            ds = objMast.SuppliertMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["ASM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["ASM_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }



        [System.Web.Services.WebMethod]
        public static string[] GetProductList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 AND APM_ACTIVE = 'A'   ";
            Criteria += " AND (APM_CODE like '%" + prefixText + "%' OR  APM_NAME like '%" + prefixText + "%' )";

            ProductMasterBAL objProd = new ProductMasterBAL();

            string[] Data;

            ds = objProd.ProductMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["APM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["APM_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            try
            {

                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");



                    //objCom = new CommonBAL();
                    txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GRN");
                    BindUnitType();
                    BindStore();
                    BuildeGRNTransDtls();


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["GRNTrans"];


                //if (txtTransNo.Text.Trim() == "")
                //{
                //    lblStatus.Text = " Please Enter Trans. No.";
                //    goto FunEnd;
                //}

                //if (txtTransDate.Text.Trim() == "")
                //{
                //    lblStatus.Text = " Please Enter Trans. Date";
                //    goto FunEnd;
                //}
                //if (txtSupplier.Text.Trim() == "" || txtSupplier.Text.Length <= 1)
                //{
                //    lblStatus.Text = " Please Select Supplier";
                //    goto FunEnd;
                //}


                string strAccName = txtSupplier.Text.Trim();
                string AccCode = "", AccName = "";

                string[] arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }




                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Pls Add the Item Dtls.";
                    goto FunEnd;
                }



                objGRN = new GoodsReceivedNoteBAL();
                objGRN.BranchID = Convert.ToString(Session["Branch_ID"]);
                objGRN.AGRM_TRANS_NO = txtTransNo.Text.Trim();
                objGRN.AGRM_TRANS_DATE = txtTransDate.Text.Trim();
                objGRN.AGRM_REF_NO = txtReferanceNo.Text;
                objGRN.AGRM_PAYMENT_TYPE = drpPaymentType.SelectedValue;




                objGRN.AGRM_SUPPLIER_CODE = AccCode;
                objGRN.AGRM_SUPPLIER_NAME = AccName;
                objGRN.AGRM_STORE = drpStore.SelectedValue;
                objGRN.AGRM_STATUS = drpStatus.SelectedValue;
                objGRN.AGRM_TOTAL_AMOUNT = txtTotalAmount.Text.Trim();
                objGRN.AGRM_DISC_TYPE = drpDiscountType.SelectedValue;
                objGRN.AGRM_DISC_AMOUNT = txtTotalDisc.Text.Trim();
                objGRN.AGRM_NET_AMOUNT = txtNetAmount.Text.Trim();
                objGRN.AGRM_PO_NO = txtPONo.Text.Trim();
                objGRN.MODE = Convert.ToString(ViewState["SaveMode"]);
                objGRN.UserID = Convert.ToString(Session["User_ID"]);

                string strTransNo = "";
                strTransNo = objGRN.GoodsReceivedNoteMasterAdd();

                txtTransNo.Text = strTransNo;



                string Criteria = " 1=1 ";
                Criteria += " AND AGRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                Criteria += " AND AGRT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

                objGRN = new GoodsReceivedNoteBAL();
                DataSet DS = new DataSet();

                DS = objGRN.GoodsReceivedNoteTransGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        string CriteriaPM = "  APM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APM_CODE = '" + Convert.ToString(DR["AGRT_ITEM_CODE"]) + "'";
                        string FieldNameWithValues = "APM_STOCK_AVAILABLE= APM_STOCK_AVAILABLE - " + Convert.ToString(DR["AGRT_QTY"]);
                        objCom = new CommonBAL();
                        objCom.fnUpdateTableData(FieldNameWithValues, "AC_PRODUCT_MASTER", CriteriaPM);
                    }

                }
                objCom = new CommonBAL();
                string Criteria1 = " AGRT_TRANS_NO = '" + txtTransNo.Text.Trim() + "'";

                objCom.fnDeleteTableData("AC_GOODS_RECEIVED_NOTE_TRANS", Criteria1);



                foreach (DataRow DR in DT.Rows)
                {
                    objGRN.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objGRN.AGRT_TRANS_NO = txtTransNo.Text.Trim();
                    objGRN.AGRT_SL_NO = Convert.ToString(DR["AGRT_SL_NO"]);
                    objGRN.AGRT_ITEM_CODE = Convert.ToString(DR["AGRT_ITEM_CODE"]);
                    objGRN.AGRT_ITEM_NAME = Convert.ToString(DR["AGRT_ITEM_NAME"]);
                    objGRN.AGRT_EXPIRY_DATE = Convert.ToString(DR["AGRT_EXPIRY_DATEDesc"]);
                    objGRN.AGRT_UNIT_TYPE = Convert.ToString(DR["AGRT_UNIT_TYPE"]);
                    objGRN.AGRT_QTY = Convert.ToString(DR["AGRT_QTY"]);
                    objGRN.AGRT_UNIT_PRICE = Convert.ToString(DR["AGRT_UNIT_PRICE"]);
                    objGRN.AGRT_AMOUNT = Convert.ToString(DR["AGRT_AMOUNT"]);
                    objGRN.GoodsReceivedNoteTransAdd();
                }


                SavePayment();


                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GRN");
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('GRN Details','Saved Successfully')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnCtlrRefresh_Click(object sender, EventArgs e)
        {
            txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GRN");
            Clear();
        }

        protected void imgGRNZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindGRNMasterGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            divGRNPopup.Visible = true;
        }

        protected void btnAddItem_Click(object sender, ImageClickEventArgs e)
        {
            try
            {


                string strTestCodeDesc = "", ItemCode = "", ItemDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["GRNTrans"];

                strTestCodeDesc = txtProduct.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Product Details','Please Enter Product')", true);

                    goto FunEnd;
                }
                ItemCode = arrTestCodeDesc[0];
                ItemDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["AGRT_ITEM_CODE"] = ItemCode;
                    DT.Rows[R]["AGRT_ITEM_NAME"] = ItemDesc;
                    DT.Rows[R]["AGRT_EXPIRY_DATEDesc"] = txtExpiryDate.Text.Trim();
                    DT.Rows[R]["AGRT_UNIT_TYPE"] = drpUnitType.SelectedValue;
                    DT.Rows[R]["AGRT_QTY"] = txtQty.Text.Trim();
                    DT.Rows[R]["AGRT_UNIT_PRICE"] = txtUnitPrice.Text.Trim();
                    DT.Rows[R]["AGRT_AMOUNT"] = txtAmount.Text.Trim();
                    DT.AcceptChanges();
                }
                else
                {
                    objrow["AGRT_ITEM_CODE"] = ItemCode;
                    objrow["AGRT_ITEM_NAME"] = ItemDesc;
                    objrow["AGRT_EXPIRY_DATEDesc"] = txtExpiryDate.Text.Trim();
                    objrow["AGRT_UNIT_TYPE"] = drpUnitType.SelectedValue;
                    objrow["AGRT_QTY"] = txtQty.Text.Trim();
                    objrow["AGRT_UNIT_PRICE"] = txtUnitPrice.Text.Trim();
                    objrow["AGRT_AMOUNT"] = txtAmount.Text.Trim();


                    DT.Rows.Add(objrow);
                }


                ViewState["GRNTrans"] = DT;


                BindTempGRNTransDtls();
                ClearTrans();

                CalculateTotalAmount();

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddItem_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void GRNTransEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvItemCode = (Label)gvScanCard.Cells[0].FindControl("lblgvItemCode");
            Label lblgvItemName = (Label)gvScanCard.Cells[0].FindControl("lblgvItemName");
            Label lblgvUnitType = (Label)gvScanCard.Cells[0].FindControl("lblgvUnitType");

            Label lblgvQty = (Label)gvScanCard.Cells[0].FindControl("lblgvQty");
            Label lblgvUnitPrice = (Label)gvScanCard.Cells[0].FindControl("lblgvUnitPrice");
            Label lblgvAmount = (Label)gvScanCard.Cells[0].FindControl("lblgvAmount");
            txtProduct.Text = lblgvItemCode.Text + "~" + lblgvItemName.Text;

            drpUnitType.SelectedValue = lblgvUnitType.Text;
            txtQty.Text = lblgvQty.Text;
            txtUnitPrice.Text = lblgvUnitPrice.Text;
            txtAmount.Text = lblgvAmount.Text;
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["GRNTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["GRNTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["GRNTrans"] = DT;

                }


                BindTempGRNTransDtls();

                CalculateTotalAmount();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void txtTransNo_TextChanged(object sender, EventArgs e)
        {
            Clear();
            BindGRNMaster();
            BindGRNTransDtls();
            BindTempGRNTransDtls();

        }

        protected void GRNEdit_Click(object sender, EventArgs e)
        {
            Clear();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvTransNo = (Label)gvScanCard.Cells[0].FindControl("lblgvTransNo");

            txtTransNo.Text = lblgvTransNo.Text;


            divGRNPopup.Visible = false;
            BindGRNMaster();
            BindGRNTransDtls();
            BindTempGRNTransDtls();

            BindPaymentEntry();

            BindJournalGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideTestMasterPopup()", true);
        }

        protected void txtTotalDisc_TextChanged(object sender, EventArgs e)
        {
            CalculateTotalAmount();
        }

        protected void drpDiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateTotalAmount();
        }

        protected void gvJournal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblgvTransType = (Label)e.Row.FindControl("lblgvTransType");

                TextBox lblgvAmountDr = (TextBox)e.Row.FindControl("lblgvAmountDr");
                TextBox lblgvAmountCr = (TextBox)e.Row.FindControl("lblgvAmountCr");

                if (lblgvTransType.Text == "Dr")
                {
                    lblgvAmountCr.Visible = false;
                    lblgvAmountCr.Text = "";
                }
                else
                {
                    lblgvAmountDr.Visible = false;
                    lblgvAmountDr.Text = "";
                }

            }
        }

        protected void btnJournalRefresh_Click(object sender, ImageClickEventArgs e)
        {
            BindJournalGrid();
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string Criteria = " AGRT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_GOODS_RECEIVED_NOTE_TRANS", Criteria);

                string Criteria1 = " AGCM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_GOODS_CONSUMPTION_VOUCHER_MASTER", Criteria1);


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('GRN Details ','Data Deleted ')", true);
                Clear();
                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GRN");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void txtProduct_TextChanged(object sender, EventArgs e)
        {
            string[] strProd = txtProduct.Text.Trim().Split('~');

            if (strProd.Length > 0)
            {
                string strProdCode = strProd[0];
                DataSet DS = new DataSet();
                DS = GetProductDtls(strProdCode);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    hidTaxType.Value  = Convert.ToString(DS.Tables[0].Rows[0]["APM_TAXTYPE"]);
                    txtTaxPres.Text = Convert.ToString(DS.Tables[0].Rows[0]["APM_TAXVALUE"]);

                    if (hidTaxType.Value == "$")
                    {
                       
                    }
                    else
                    {
                        txtTaxPres.Text = Convert.ToString(DS.Tables[0].Rows[0]["APM_TAXVALUE"]);
                    }
                }


            }
        }

        protected void txtQty_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {

        }

    }
}