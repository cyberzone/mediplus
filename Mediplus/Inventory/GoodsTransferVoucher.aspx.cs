﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Inventory
{
    public partial class GoodsTransferVoucher : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();
        CommonBAL objCom = new CommonBAL();
        ProductMasterBAL objProd = new ProductMasterBAL();
        GoodsTransferVoucherBAL objGTV = new GoodsTransferVoucherBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUnitType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='UNITS_MEASUREMENT'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpUnitType.DataSource = DS;
                drpUnitType.DataTextField = "AM_NAME";
                drpUnitType.DataValueField = "AM_CODE";
                drpUnitType.DataBind();
            }
            //drpUnitType.Items.Insert(0, "Select");
            //drpUnitType.Items[0].Value = "";


        }

        void BindDepartment()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HDM_STATUS='A'  AND HDM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            CommonBAL objMast = new CommonBAL();
            DS = objMast.DepMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = DS;
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataBind();



                drpDepartmentTo.DataSource = DS;
                drpDepartmentTo.DataTextField = "HDM_DEP_NAME";
                drpDepartmentTo.DataValueField = "HDM_DEP_NAME";
                drpDepartmentTo.DataBind();
            }
            drpDepartment.Items.Insert(0, "--- Select ---");
            drpDepartment.Items[0].Value = "";
            
            drpDepartmentTo.Items.Insert(0, "--- Select ---");
            drpDepartmentTo.Items[0].Value = "";

        }


        void BindGTVMaster()
        {
            objGTV = new GoodsTransferVoucherBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND AGTM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            DS = objGTV.GoodsTransferVoucherMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["SaveMode"] = "M";

                    txtTransDate.Text = Convert.ToString(DR["AGTM_TRANS_DATEDesc"]);
                    txtReferanceNo.Text = Convert.ToString(DR["AGTM_REF_NO"]);
                    drpDepartment.SelectedValue = Convert.ToString(DR["AGTM_DEPARTMENT_FROM"]);
                    drpDepartmentTo.SelectedValue = Convert.ToString(DR["AGTM_DEPARTMENT_TO"]);

                    drpStatus.SelectedValue = Convert.ToString(DR["AGTM_STATUS"]);



                }
            }

        }

        void BindGTVMasterGrid()
        {
            objGTV = new GoodsTransferVoucherBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";


            DS = objGTV.GoodsTransferVoucherMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGTVMaster.DataSource = DS;
                gvGTVMaster.DataBind();
            }
            else
            {
                gvGTVMaster.DataBind();
            }
        }

        void BuildGTVTransDtls()
        {
            string Criteria = "1=2";
            objGTV = new GoodsTransferVoucherBAL();
            DataSet DS = new DataSet();

            DS = objGTV.GoodsTransferVoucherTransGet(Criteria);

            ViewState["GTVTrans"] = DS.Tables[0];
        }

        void BindGTVTransDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND AGTT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND AGTT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            objGTV = new GoodsTransferVoucherBAL();
            DataSet DS = new DataSet();

            DS = objGTV.GoodsTransferVoucherTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["GTVTrans"] = DS.Tables[0];

            }
        }

        void BindTempGTVTransDtls()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["GTVTrans"];
            if (DT.Rows.Count > 0)
            {
                gvGTVTrans.DataSource = DT;
                gvGTVTrans.DataBind();

            }
            else
            {
                gvGTVTrans.DataBind();
            }

        }

        Int32 GetProductStock(string ITEM_CODE)
        {
            Int32 intAvailStock = 0;
            string CriteriaPM = "  APM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APM_CODE = '" +  ITEM_CODE + "'";

            objProd = new ProductMasterBAL();

            DataSet DS = new DataSet();

            DS = objProd.ProductMasterGet(CriteriaPM);

            if (DS.Tables[0].Rows.Count > 0)
            {
                intAvailStock = Convert.ToInt32(DS.Tables[0].Rows[0]["APM_STOCK_AVAILABLE"]);
            }
            return intAvailStock;
        }

        void Clear()
        {
            // objGTV.AGTT_REF_NO = "";
            //objGTV.AGTT_PAYMENT_TYPE = drpPaymentType.SelectedValue;

            txtReferanceNo.Text = "";
            //drpPaymentType.SelectedIndex = 0;
            //txtSupplier.Text = "";
            //drpStore.SelectedIndex = 0;

            drpDepartment.SelectedIndex = 0;
            drpStatus.SelectedIndex = 0;
            //objGTV.AGTT_TOTAL_AMOUNT = txtTotalAmount.Text = "";

            //drpDiscountType.SelectedIndex = 0;
            //txtTotalDisc.Text = "";
            //txtNetAmount.Text = "";

            BuildGTVTransDtls();
            BindTempGTVTransDtls();

            ViewState["SaveMode"] = "A";

            divGTVPopup.Visible = false;
        }

        void ClearTrans()
        {
            txtProduct.Text = "";

            if (drpUnitType.Items.Count > 0)
                drpUnitType.SelectedIndex = 0;
            txtQty.Text = "";


            ViewState["gvServSelectIndex"] = "";
        }


        #endregion

        #region AutoExt

        [System.Web.Services.WebMethod]
        public static string[] GetProductList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 AND APM_ACTIVE = 'A'   ";
            Criteria += " AND (APM_CODE like '%" + prefixText + "%' OR  APM_NAME like '%" + prefixText + "%' )";

            ProductMasterBAL objProd = new ProductMasterBAL();

            string[] Data;

            ds = objProd.ProductMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["APM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["APM_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            try
            {

                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");



                    //objCom = new CommonBAL();
                    txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GTV");
                    BuildGTVTransDtls();
                    BindDepartment();
                    BindUnitType();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtTransNo.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. No.','Red')", true);
                    goto FunEnd;
                }

                if (txtTransDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. Date','Red')", true);
                    goto FunEnd;
                }


                objGTV = new GoodsTransferVoucherBAL(); ;
                objGTV.BranchID = Convert.ToString(Session["Branch_ID"]);
                objGTV.AGTM_TRANS_NO = txtTransNo.Text.Trim();
                objGTV.AGTM_TRANS_DATE = txtTransDate.Text.Trim();
                objGTV.AGTM_REF_NO = txtReferanceNo.Text;
                objGTV.AGTM_DEPARTMENT_FROM = drpDepartment.SelectedValue;
                objGTV.AGTM_DEPARTMENT_TO= drpDepartmentTo.SelectedValue;


                objGTV.AGTM_STATUS = drpStatus.SelectedValue;


                objGTV.MODE = Convert.ToString(ViewState["SaveMode"]);
                objGTV.UserID = Convert.ToString(Session["User_ID"]);

                string strTransNo = "";
                strTransNo = objGTV.GoodsTransferVoucherMasterAdd();

                txtTransNo.Text = strTransNo;


                string Criteria = " 1=1 ";
                Criteria += " AND AGTT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                Criteria += " AND AGTT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

                objGTV = new GoodsTransferVoucherBAL();
                DataSet DS = new DataSet();

                DS = objGTV.GoodsTransferVoucherTransGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        string CriteriaPM = "  APM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APM_CODE = '" + Convert.ToString(DR["AGTT_ITEM_CODE"]) + "'";
                        string FieldNameWithValues = "APM_STOCK_AVAILABLE= APM_STOCK_AVAILABLE + " + Convert.ToString(DR["AGTT_QTY"]);
                        objCom = new CommonBAL();
                        objCom.fnUpdateTableData(FieldNameWithValues, "AC_PRODUCT_MASTER", CriteriaPM);
                    }

                }

                objCom = new CommonBAL();
                string Criteria1 = " AGTT_TRANS_NO = '" + txtTransNo.Text.Trim() + "'";

                objCom.fnDeleteTableData("AC_GOODS_Transfer_VOUCHER_TRANS", Criteria1);

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["GTVTrans"];

                foreach (DataRow DR in DT.Rows)
                {
                    objGTV.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objGTV.AGTT_TRANS_NO = txtTransNo.Text.Trim();
                    objGTV.AGTT_SL_NO = Convert.ToString(DR["AGTT_SL_NO"]);
                    objGTV.AGTT_ITEM_CODE = Convert.ToString(DR["AGTT_ITEM_CODE"]);
                    objGTV.AGTT_ITEM_NAME = Convert.ToString(DR["AGTT_ITEM_NAME"]);
                    objGTV.AGTT_UNIT_TYPE = Convert.ToString(DR["AGTT_UNIT_TYPE"]);
                    objGTV.AGTT_QTY = Convert.ToString(DR["AGTT_QTY"]);

                    objGTV.GoodsTransferVoucherTransAdd();
                }





                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GTV");
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('   Saved Successfully','Green')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnCtlrRefresh_Click(object sender, EventArgs e)
        {
            txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GTV");
            Clear();
        }

        protected void imgGTVZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindGTVMasterGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            divGTVPopup.Visible = true;
        }

        protected void btnAddItem_Click(object sender, ImageClickEventArgs e)
        {
            try
            {


                string strTestCodeDesc = "", ItemCode = "", ItemDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["GTVTrans"];

                strTestCodeDesc = txtProduct.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Producte','Red')", true);
                    goto FunEnd;
                }
                ItemCode = arrTestCodeDesc[0];
                ItemDesc = arrTestCodeDesc[1];

                if (txtQty.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter QTY','Red')", true);

                    goto FunEnd;
                }

                if ( txtAvailStock.Text.Trim() =="" )
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Stock not available','Red')", true);

                    goto FunEnd;
                }

                if (Convert.ToInt32(txtAvailStock.Text.Trim()) < Convert.ToInt32(txtQty.Text.Trim()))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Stock not available','Red')", true);

                    goto FunEnd;
                }
                 

                if (Convert.ToInt32(txtAvailStock.Text.Trim()) < Convert.ToInt32(txtQty.Text.Trim()))
                {
                     ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Stock not available','Red')", true);

                    goto FunEnd;
                }

                DataRow objrow;
                objrow = DT.NewRow();




                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["AGTT_ITEM_CODE"] = ItemCode;
                    DT.Rows[R]["AGTT_ITEM_NAME"] = ItemDesc;

                    DT.Rows[R]["AGTT_UNIT_TYPE"] = drpUnitType.SelectedValue;
                    DT.Rows[R]["AGTT_QTY"] = txtQty.Text.Trim();

                    DT.AcceptChanges();
                }
                else
                {
                    objrow["AGTT_ITEM_CODE"] = ItemCode;
                    objrow["AGTT_ITEM_NAME"] = ItemDesc;

                    objrow["AGTT_UNIT_TYPE"] = drpUnitType.SelectedValue;
                    objrow["AGTT_QTY"] = txtQty.Text.Trim();



                    DT.Rows.Add(objrow);
                }


                ViewState["GTVTrans"] = DT;


                BindTempGTVTransDtls();
                ClearTrans();



            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddItem_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void GTVTransEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvItemCode = (Label)gvScanCard.Cells[0].FindControl("lblgvItemCode");
            Label lblgvItemName = (Label)gvScanCard.Cells[0].FindControl("lblgvItemName");
            Label lblgvUnitType = (Label)gvScanCard.Cells[0].FindControl("lblgvUnitType");

            Label lblgvQty = (Label)gvScanCard.Cells[0].FindControl("lblgvQty");
            Label lblgvUnitPrice = (Label)gvScanCard.Cells[0].FindControl("lblgvUnitPrice");
            Label lblgvAmount = (Label)gvScanCard.Cells[0].FindControl("lblgvAmount");
            txtProduct.Text = lblgvItemCode.Text + "~" + lblgvItemName.Text;

            drpUnitType.SelectedValue = lblgvUnitType.Text;
            txtQty.Text = lblgvQty.Text;

        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["GTVTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["GTVTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["GTVTrans"] = DT;

                }


                BindTempGTVTransDtls();


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void txtTransNo_TextChanged(object sender, EventArgs e)
        {
            Clear();
            BindGTVMaster();
            BindGTVTransDtls();
            BindTempGTVTransDtls();

        }

        protected void GTVEdit_Click(object sender, EventArgs e)
        {
            Clear();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvTransNo = (Label)gvScanCard.Cells[0].FindControl("lblgvTransNo");

            txtTransNo.Text = lblgvTransNo.Text;


            divGTVPopup.Visible = false;
            BindGTVMaster();
            BindGTVTransDtls();
            BindTempGTVTransDtls();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideTestMasterPopup()", true);
        }

        protected void txtProduct_TextChanged(object sender, EventArgs e)
        {
            
            string strTestCodeDesc = "", ItemCode = "", ItemDesc = "";

            strTestCodeDesc = txtProduct.Text;
            string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

            
            ItemCode = arrTestCodeDesc[0];
            ItemDesc = arrTestCodeDesc[1];

           
            string CriteriaPM = "  APM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APM_CODE = '" + ItemCode + "'";

            objProd = new ProductMasterBAL();

            DataSet DS = new DataSet();

            DS = objProd.ProductMasterGet(CriteriaPM);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpUnitType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["APM_UNIT_TYPE"]);
                txtAvailStock.Text  = Convert.ToString(DS.Tables[0].Rows[0]["APM_STOCK_AVAILABLE"]);
            }
           
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string Criteria = " AGTT_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_GOODS_TRANSFER_VOUCHER_TRANS", Criteria);

                string Criteria1 = " AGTM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_GOODS_TRANSFER_VOUCHER_MASTER", Criteria1);


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' Data Deleted ','Data Deleted ')", true);
                Clear();
                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_GTV");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

    }
}