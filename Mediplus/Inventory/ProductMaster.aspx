﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="ProductMaster.aspx.cs" Inherits="Mediplus.Inventory.ProductMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }

    </script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .modalPopup
        {
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px 10px 10px 10px;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ValSave() {

            if (document.getElementById('<%=txtProdCode.ClientID%>').value == "") {
                ShowErrorMessage('Product Code', 'Please Enter Product Code')
                return false;
            }


            if (document.getElementById('<%=drpPropCategory.ClientID%>').value == "") {

                ShowErrorMessage('Product Category', 'Please select the Category')
                return false;
            }

            if (document.getElementById('<%=txtDescription.ClientID%>').value == "") {

                ShowErrorMessage('Product Description', 'Please Enter Product Description')
                return false;
            }

        }

        function ShowErrorMessage(vMessage1, vMessage2) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
           document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
           document.getElementById("divMessage").style.display = 'block';

       }

       function HideErrorMessage() {

           document.getElementById("divMessage").style.display = 'none';

           document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

    function ShowMasterPopup() {

        document.getElementById("divProductPopup").style.display = 'block';


    }

    function HideMasterPopup() {

        document.getElementById("<%=divProductPopup.ClientID%>").style.display = 'none';
        }


        function ShowStaffMaser() {

            var win = window.open("../HMS/Masters/StaffProfile.aspx?MenuName=Masters,newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function ShowDepMaser() {

            var win = window.open("../HMS/Masters/DepartmentMaster.aspx?MenuName=Masters,newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }


        function ShowCommonMaster(Type) {

            var win = window.open("../Inventory/CommonMasters.aspx?MenuName=Masters&Type=" + Type, "newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div id="divMessageClose" style="display: none; position: absolute; top: 278px; left: 712px;">
            <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" />
        </div>
        <div style="padding-left: 60%; width: 100%;">
            <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
        </div>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
        <table style="width: 80%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Product Master"></asp:Label>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnDepreProcess" runat="server" CssClass="button gray small" Width="150px" OnClick="btnDepreProcess_Click" Text="Depre. Process" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return window.confirm('Do you want to Delete this data?')" />

                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" OnClientClick="return ValSave();"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 80%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">


            <div style="width: 100%; text-align: right; float: right;">
                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>

                        <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>


            <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Item Code <span style="color: red; font-size: 11px;">*</span>
                        <asp:ImageButton ID="imgReceiptZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Product List" OnClick="imgReceiptZoom_Click" />

                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtProdCode" runat="server" CssClass="TextBoxStyle" Width="70%" Height="25px" AutoPostBack="true" OnTextChanged="txtProdCode_TextChanged"></asp:TextBox>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Type   <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpType" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="drpType_SelectedIndexChanged">
                                    <asp:ListItem Text="Inventory" Value="Inventory"></asp:ListItem>
                                    <asp:ListItem Text="Fixed Asset" Value="Fixed Asset"></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Category   <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpPropCategory" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Description<span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Status
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                    <asp:ListItem Value="A">Active</asp:ListItem>
                                    <asp:ListItem Value="I">InActive</asp:ListItem>

                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Supplier <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpSupplier" runat="server" CssClass="TextBoxStyle" Width="70%" Height="25px">
                                </asp:DropDownList>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Cont. Person
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSupContPerson" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" ReadOnly="true"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Cont. Number
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSupContNumber" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" ReadOnly="true"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Department   <span style="color: red; font-size: 11px;">*</span>
                        <input type="image" src="../Images/Zoom.png" style="height: 20px; width: 20px; cursor: pointer;" onclick="ShowDepMaser()" />

                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle" Width="70%" Height="25px"></asp:DropDownList>

                                <asp:ImageButton ID="imgDeptfRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px; cursor: pointer;" ToolTip="Refresh" OnClick="imgDeptfRef_Click" />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Unit   <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpUnitType" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Avail. Stock 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAvaiStock" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Inventory A/C    <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAccountName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                <div id="divwidth" style="visibility: hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtAccountName" MinimumPrefixLength="1" ServiceMethod="GetAccountList"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:Label ID="Label22" runat="server" CssClass="label"
                            Text="Sales Tax"></asp:Label>
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanelTaxType" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpTaxType" CssClass="TextBoxStyle" runat="server" Width="25%" Enabled="false" Height="25px">
                                    <asp:ListItem Value="$">$</asp:ListItem>
                                    <asp:ListItem Value="%" Selected="True">%</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtTaxValue" CssClass="TextBoxStyle" runat="server" Width="70%" MaxLength="10" Height="25px" Text="0"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Remarks
                    </td>
                    <td colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="TextBoxStyle" Width="100%" Height="30px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                      <td class="lblCaption1">Sub Type   <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpSubType" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" >
                                    <asp:ListItem Text="Regular" Value="Regular"></asp:ListItem>
                                    <asp:ListItem Text="Expiry Date" Value="Expiry Date"></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                
            </table>
            <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                <ContentTemplate>
                    <div id="divFixedAsset" runat="server" style="display: none;">
                        <table cellpadding="5" cellspacing="5" style="width: 100%">
                            <tr>
                                <td class="lblCaption1" style="width: 100px;">Location
                                      <input type="image" src="../Images/Zoom.png" style="height: 20px; width: 20px; cursor: pointer;" onclick="ShowCommonMaster('PROD_LOCATION')" />

                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpLocation" runat="server" CssClass="TextBoxStyle" Width="80%" Height="25px"></asp:DropDownList>
                                            <asp:ImageButton ID="imgCommRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px; cursor: pointer;" ToolTip="Refresh" OnClick="imgCommRef_Click" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1">Custodian
 
                                        <input type="image" src="../Images/Zoom.png" style="height: 20px; width: 20px; cursor: pointer;" onclick="ShowStaffMaser()" />

                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpCustodian" runat="server" CssClass="TextBoxStyle" Width="80%" Height="25px"></asp:DropDownList>
                                            <asp:ImageButton ID="imgStaffRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px; cursor: pointer;" ToolTip="Refresh" OnClick="imgStaffRef_Click" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Depreciation Methods
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpDepreMethod" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                            </tr>
                            <tr>
                                <td class="lblCaption1">Purchase Date
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPurchaseDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                                Enabled="True" TargetControlID="txtPurchaseDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Warranty Expires
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtWarrantyDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                                Enabled="True" TargetControlID="txtWarrantyDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1" style="width: 100px;">SN
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtSN" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="width: 100px;">Fixed Asset A/C
                                </td>
                                <td colspan="5">
                                    <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtFixedAssetAC" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtFixedAssetAC" MinimumPrefixLength="1" ServiceMethod="GetFixedAssetAccountList"
                                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                                            </asp:AutoCompleteExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="width: 100px;">Depreciation  A/C
                                </td>
                                <td colspan="5">
                                    <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtDepreciationAC" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>

                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDepreciationAC" MinimumPrefixLength="1" ServiceMethod="GetDepreciationAccountList"
                                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                                            </asp:AutoCompleteExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                            </tr>
                        </table>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="divProductPopup" runat="server" class="modalPopup" visible="false" style="overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 150px;">
            <div style="width: 100%; text-align: right; float: right; position: absolute; top: 0px; right: 0px;">

                <img src="../Images/Close.png" style="height: 25px; width: 25px;" onclick="HideMasterPopup()" />

            </div>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvProduct" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton11" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvProdCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_CODE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvProdName" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvProdName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvType" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_TYPE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Category" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvCategory" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvCategory" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_CATEGORY") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Supplier" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvSupplier" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvSupplier" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_SUPPLIER_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Department" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvDepartment" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvDepartment" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_DEPARTMENT") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvStatus" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_ACTIVEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Avail. Stock" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvAvailStock" CssClass="lblCaption1" runat="server" OnClick="ProductEdit_Click">
                                            <asp:Label ID="lblgvAvailStock" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APM_STOCK_AVAILABLE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>


