﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Inventory
{
    public partial class SupplierMaster : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();
        CommonBAL objCom = new CommonBAL();
        SuppliertMasterBAL objSup = new SuppliertMasterBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }



        void BindSupplier()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND ASM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ASM_CODE='" + txtSupplierCode.Text.Trim() + "'";

            SuppliertMasterBAL objSup = new SuppliertMasterBAL();
            DS = objSup.SuppliertMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["SaveMode"] = "M";


                    txtSupplierName.Text = Convert.ToString(DR["ASM_NAME"]);
                    drpStatus.SelectedValue = Convert.ToString(DR["ASM_ACTIVE"]);
                    txtContPerson.Text = Convert.ToString(DR["ASM_CONT_PERSON"]);
                    txtMobile.Text = Convert.ToString(DR["ASM_MOBILE"]);
                    txtPhone.Text = Convert.ToString(DR["ASM_PHONE"]);
                    txtAddress.Text = Convert.ToString(DR["ASM_ADDRESS"]);
                    if (DR.IsNull("ASM_ACCOUNT_CODE") == false && Convert.ToString(DR["ASM_ACCOUNT_CODE"]) != "")
                    {
                        txtAccountName.Text = Convert.ToString(DR["ASM_ACCOUNT_CODE"]) + "~" + Convert.ToString(DR["ASM_ACCOUNT_NAME"]);
                    }

                    txtCreditLimit.Text = Convert.ToString(DR["ASM_CREDIT_LIMIT"]);
                    txtCreditDays.Text = Convert.ToString(DR["ASM_CREDIT_DAYS"]);
                    txtTRNNumber.Text = Convert.ToString(DR["ASM_TRN_NUMBER"]);
                }
            }
            else
            {
                gvSupplier.DataBind();
            }



        }


        void BindSuppliertGrid()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND ASM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            SuppliertMasterBAL objSup = new SuppliertMasterBAL();
            DS = objSup.SuppliertMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvSupplier.DataSource = DS;
                gvSupplier.DataBind();
            }
            else
            {
                gvSupplier.DataBind();
            }



        }

        void Clear()
        {
           
            txtSupplierName.Text = "";

            txtSupplierName.Text = "";
            drpStatus.SelectedValue = "";
            txtContPerson.Text = "";
            txtMobile.Text = "";
            txtPhone.Text = "";
            txtAddress.Text = "";
            txtAccountName.Text = "";

            txtCreditLimit.Text = "";
            txtCreditDays.Text = "";
            txtTRNNumber.Text = "";
            ViewState["SaveMode"] = "A";
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            // Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    txtSupplierCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_SUPPLIR");

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSupplierCode.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Supplier Code','Red')", true);
                    goto FunEnd;
                }

                if (txtSupplierName.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Supplier Name','Red')", true);
                    goto FunEnd;
                }
                objSup = new SuppliertMasterBAL();
                objSup.BranchID = Convert.ToString(Session["Branch_ID"]);

                objSup.ASM_CODE = txtSupplierCode.Text.Trim();
                objSup.ASM_NAME = txtSupplierName.Text;
                objSup.ASM_ACTIVE = drpStatus.SelectedValue;
                objSup.ASM_CONT_PERSON = txtContPerson.Text;
                objSup.ASM_MOBILE = txtMobile.Text;
                objSup.ASM_PHONE = txtPhone.Text;
                objSup.ASM_ADDRESS = txtAddress.Text;
                string strAccName = txtAccountName.Text.Trim();
                string AccCode = "", AccName = "";

                string[] arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }


                objSup.ASM_ACCOUNT_CODE = AccCode;
                objSup.ASM_ACCOUNT_NAME = AccName;
                objSup.ASM_CREDIT_LIMIT = txtCreditLimit.Text.Trim();
                objSup.ASM_CREDIT_DAYS = txtCreditDays.Text.Trim();
                objSup.ASM_TRN_NUMBER = txtTRNNumber.Text.Trim();
                objSup.MODE = Convert.ToString(ViewState["SaveMode"]);
                objSup.UserID = Convert.ToString(Session["User_ID"]);
                objSup.SupplierMasterAdd();

                txtSupplierCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_SUPPLIR");
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Supplier Master','Saved Successfully')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnCtlrRefresh_Click(object sender, EventArgs e)
        {
            txtSupplierCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_SUPPLIR");
            Clear();
        }

        protected void imgSupplierZoom_Click(object sender, EventArgs e)
        {
            BindSuppliertGrid();
            divSupplierPopup.Visible = true;
        }


        protected void SupplierEdit_Click(object sender, EventArgs e)
        {
            Clear();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvSuppCode = (Label)gvScanCard.Cells[0].FindControl("lblgvSuppCode");

            txtSupplierCode.Text = lblgvSuppCode.Text;


            divSupplierPopup.Visible = false;
            BindSupplier();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideTestMasterPopup()", true);
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtSupplierCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_SUPPLIR");
            Clear();
        }


        #endregion
    }
}