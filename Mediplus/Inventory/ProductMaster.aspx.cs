﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;


namespace Mediplus.Inventory
{
    public partial class ProductMaster : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();
        CommonBAL objCom = new CommonBAL();
        ProductMasterBAL objProd = new ProductMasterBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='PROD_CATEGORY'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpPropCategory.DataSource = DS;
                drpPropCategory.DataTextField = "AM_NAME";
                drpPropCategory.DataValueField = "AM_CODE";
                drpPropCategory.DataBind();
            }
            drpPropCategory.Items.Insert(0, "--- Select ---");
            drpPropCategory.Items[0].Value = "";


        }

        void BindUnitType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='UNITS_MEASUREMENT'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpUnitType.DataSource = DS;
                drpUnitType.DataTextField = "AM_NAME";
                drpUnitType.DataValueField = "AM_CODE";
                drpUnitType.DataBind();
            }
            drpUnitType.Items.Insert(0, "--- Select ---");
            drpUnitType.Items[0].Value = "";


        }

        void BindDepreMethod()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND ADP_ACTIVE=1 AND ADP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";


            MastersBAL objProd = new MastersBAL();
            DS = objProd.DepreciationPolicyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpDepreMethod.DataSource = DS;
                drpDepreMethod.DataTextField = "ADP_NAME";
                drpDepreMethod.DataValueField = "ADP_CODE";
                drpDepreMethod.DataBind();
            }
            drpDepreMethod.Items.Insert(0, "--- Select ---");
            drpDepreMethod.Items[0].Value = "";


        }

        void BindCustodian()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND  HSFM_CATEGORY IN ('Others','Admin Staff')  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            StaffMasterBAL objMast = new StaffMasterBAL();
            DS = objMast.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCustodian.DataSource = DS;
                drpCustodian.DataTextField = "FullName";
                drpCustodian.DataValueField = "HSFM_STAFF_ID";
                drpCustodian.DataBind();
            }
            drpCustodian.Items.Insert(0, "--- Select ---");
            drpCustodian.Items[0].Value = "";


        }

        void BindDepartment()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HDM_STATUS='A'  AND HDM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            CommonBAL objMast = new CommonBAL();
            DS = objMast.DepMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = DS;
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
            }
            drpDepartment.Items.Insert(0, "--- Select ---");
            drpDepartment.Items[0].Value = "";


        }

        void BindSuppliertMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND ASM_ACTIVE= 'A'  AND ASM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            SuppliertMasterBAL objMast = new SuppliertMasterBAL();
            DS = objMast.SuppliertMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpSupplier.DataSource = DS;
                drpSupplier.DataTextField = "ASM_NAME";
                drpSupplier.DataValueField = "ASM_CODE";
                drpSupplier.DataBind();
            }
            drpSupplier.Items.Insert(0, "--- Select ---");
            drpSupplier.Items[0].Value = "";


        }

        void BindLocation()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AM_ACTIVE=1 ";
            Criteria += " AND AM_TYPE='PROD_LOCATION'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.MastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpLocation.DataSource = DS;
                drpLocation.DataTextField = "AM_NAME";
                drpLocation.DataValueField = "AM_CODE";
                drpLocation.DataBind();
            }
            drpLocation.Items.Insert(0, "--- Select ---");
            drpLocation.Items[0].Value = "";


        }

        void BindProduct()
        {
            objProd = new ProductMasterBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND APM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND APM_CODE='" + txtProdCode.Text.Trim() + "'";
            DS = objProd.ProductMasterGet(Criteria);
            gvProduct.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["SaveMode"] = "M";
                    txtDescription.Text = Convert.ToString(DR["APM_NAME"]);
                    drpStatus.SelectedValue = Convert.ToString(DR["APM_ACTIVE"]);
                    drpType.SelectedValue = Convert.ToString(DR["APM_TYPE"]);

                    drpPropCategory.SelectedValue = Convert.ToString(DR["APM_CATEGORY"]);
                    drpSupplier.SelectedValue = Convert.ToString(DR["APM_SUPPLIER_CODE"]);

                    drpDepartment.SelectedValue = Convert.ToString(DR["APM_DEPARTMENT"]);
                    drpUnitType.SelectedValue = Convert.ToString(DR["APM_UNIT_TYPE"]);
                    txtAvaiStock.Text = Convert.ToString(DR["APM_STOCK_AVAILABLE"]);

                    if (DR.IsNull("APM_INVENTORY_AC_CODE") == false && Convert.ToString(DR["APM_INVENTORY_AC_CODE"]) != "")
                    {
                        txtAccountName.Text = Convert.ToString(DR["APM_INVENTORY_AC_CODE"]) + "~" + Convert.ToString(DR["APM_INVENTORY_AC_NAME"]);
                    }

                    txtRemarks.Text = Convert.ToString(DR["APM_REMARKS"]);
                    drpLocation.SelectedValue = Convert.ToString(DR["APM_LOCATION"]);
                    drpCustodian.SelectedValue = Convert.ToString(DR["APM_CUSTODIAN"]);
                    drpDepreMethod.SelectedValue = Convert.ToString(DR["APM_DEPRECIATION"]);
                    txtWarrantyDate.Text = Convert.ToString(DR["APM_WARRANTY_EXPIRESDesc"]);
                    txtPurchaseDate.Text = Convert.ToString(DR["APM_PURCHASE_DATEDesc"]);
                    txtSN.Text = Convert.ToString(DR["APM_SL_NO"]);


                    if (DR.IsNull("APM_FIXED_ASSET_CODE") == false && Convert.ToString(DR["APM_FIXED_ASSET_CODE"]) != "")
                    {
                        txtFixedAssetAC.Text = Convert.ToString(DR["APM_FIXED_ASSET_CODE"]) + "~" + Convert.ToString(DR["APM_FIXED_ASSET_NAME"]);
                    }

                    if (DR.IsNull("APM_DEPRECIATION_AC_CODE") == false && Convert.ToString(DR["APM_DEPRECIATION_AC_CODE"]) != "")
                    {
                        txtDepreciationAC.Text = Convert.ToString(DR["APM_DEPRECIATION_AC_CODE"]) + "~" + Convert.ToString(DR["APM_DEPRECIATION_AC_NAME"]);
                    }


                    if (DR.IsNull("APM_TAXTYPE") == false && Convert.ToString(DR["APM_TAXTYPE"]) != "")
                    {
                       drpTaxType.SelectedValue = Convert.ToString(DR["APM_TAXTYPE"]);
                    }

                    if (DR.IsNull("APM_TAXVALUE") == false && Convert.ToString(DR["APM_TAXVALUE"]) != "")
                    {
                        txtTaxValue.Text = Convert.ToString(DR["APM_TAXVALUE"]);
                    }
                     

                    if (drpType.SelectedValue == "Fixed Asset")
                    {
                        divFixedAsset.Style.Add("display", "block");
                        txtAccountName.Enabled = false;
                    }

                }

            }
            else
            {

            }

        }

        void BindProductGrid()
        {
            objProd = new ProductMasterBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND APM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DS = objProd.ProductMasterGet(Criteria);
            gvProduct.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProduct.DataSource = DS;
                gvProduct.DataBind();
                gvProduct.Visible = true;
            }
            else
            {
                gvProduct.DataBind();
            }

        }



        void Clear()
        {
            txtAccountName.Enabled = true;
            divFixedAsset.Style.Add("display", "None");
            txtDescription.Text = "";
            drpStatus.SelectedIndex = 0;
            drpType.SelectedIndex = 0;
            drpSubType.SelectedIndex = 0;

            drpPropCategory.SelectedIndex = 0;
            drpSupplier.SelectedIndex = 0;

            drpDepartment.SelectedIndex = 0;
            drpUnitType.SelectedIndex = 0;
            txtAvaiStock.Text = "";

            txtAccountName.Text = "";
            txtRemarks.Text = ""; ;
            drpLocation.SelectedIndex = 0;
            drpCustodian.SelectedIndex = 0;
            drpDepreMethod.SelectedIndex = 0;
            txtWarrantyDate.Text = "";
            txtPurchaseDate.Text = "";
            txtSN.Text = "";



            txtFixedAssetAC.Text = "";

            txtDepreciationAC.Text = "";

            drpTaxType.SelectedIndex = 0;
            txtTaxValue.Text = "0";

            ViewState["SaveMode"] = "A";
        }

        void DepreciationProcess()
        {
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            string TodayDate = strFromDate.ToString("dd/MM/yyyy");

            objProd = new ProductMasterBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND APM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND APM_TYPE='Fixed Asset'";
            DS = objProd.ProductMasterGet(Criteria);
            gvProduct.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    PaymentEntryBAL objPay = new PaymentEntryBAL();
                    objCom = new CommonBAL();
                    String strTrnNumber = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
                    objPay.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objPay.APE_TRANS_NO = strTrnNumber;
                    objPay.APE_DATE = TodayDate;


                    objPay.APE_TYPE = "CASH";

                    objPay.APE_STATUS = "A";
                    objPay.APE_ACC_CODE = Convert.ToString(DS.Tables[0].Rows[0]["APM_DEPRECIATION_AC_CODE"]);
                    objPay.APE_ACC_NAME = Convert.ToString(DS.Tables[0].Rows[0]["APM_DEPRECIATION_AC_NAME"]);
                    objPay.APE_AMOUNT = 
                    objPay.APE_CHEQUE_NO = "";
                    objPay.APE_CHEQUE_DATE = "";
                    objPay.APE_BANK_NAME = "";
                    objPay.APE_CC_TYPE = "";
                    objPay.APE_CC_NO = "";
                    objPay.APE_CC_HOLDER = "";
                    objPay.APE_DESCRIPTION = "Depreciation";
                    objPay.APE_REF_NO = strTrnNumber;
                    objPay.APE_ENTRY_FROM = "PROD_DEPRE";
                    objPay.APE_ENTRY_REF_NO = strTrnNumber;
                    objPay.MODE = "A";
                    objPay.UserID = Convert.ToString(Session["User_ID"]);
                    objPay.PaymentEntryAdd();

                    objPay = new PaymentEntryBAL();
                    objPay.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objPay.APE_TRANS_NO = strTrnNumber;
                    objPay.APE_ACC_CODE = Convert.ToString(DS.Tables[0].Rows[0]["APM_INVENTORY_AC_CODE"]);
                    objPay.APE_ACC_NAME = Convert.ToString(DS.Tables[0].Rows[0]["APM_INVENTORY_AC_NAME"]);
                    objPay.APE_COST_CENT_CODE = "";
                    objPay.APE_COST_CENT_NAME = "";
                    objPay.APE_AMOUNT = "";
                    objPay.PaymentExpensesAdd();
                }

            }
        }



        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            // Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetFixedAssetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            // Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDepreciationAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            // Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtPurchaseDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtWarrantyDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    objCom = new CommonBAL();
                    txtProdCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PRODUCT");
                    BindCategory();
                    BindUnitType();
                    BindDepreMethod();

                    BindCustodian();
                    BindDepartment();
                    BindSuppliertMaster();
                    BindLocation();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                //if (txtProdCode.Text.Trim() == "")
                //{
                //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Product Code','Please Enter Product Code')", true);
                //    goto FunEnd;
                //}

                //if (drpPropCategory.SelectedIndex == 0)
                //{
                //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Product Category','Please select the Category')", true);
                //    goto FunEnd;
                //}

                //if (txtDescription.Text.Trim() == "")
                //{
                //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Product Description','Please Enter Product Description')", true);
                //    goto FunEnd;
                //}


               ProductMasterBAL  objProd = new ProductMasterBAL();
                DataSet DS = new DataSet();
                string Criteria = " 1=1 AND APM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND APM_NAME='" + txtDescription.Text.Trim() + "' AND APM_CODE !='" + txtProdCode.Text.Trim() + "'";
                DS = objProd.ProductMasterGet(Criteria);
                gvProduct.Visible = false;
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Same Product Description Exists','Same Product Description Already Exists')", true);
                    goto FunEnd;


                }
                objProd.BranchID = Convert.ToString(Session["Branch_ID"]);
                objProd.APM_CODE = txtProdCode.Text.Trim();
                objProd.APM_NAME = txtDescription.Text;
                objProd.APM_ACTIVE = drpStatus.SelectedValue;
                objProd.APM_TYPE = drpType.SelectedValue;
                objProd.APM_SUBTYPE = drpSubType.SelectedValue;
                objProd.APM_CATEGORY = drpPropCategory.SelectedValue;
                objProd.APM_SUPPLIER_CODE = drpSupplier.SelectedValue;
                objProd.APM_SUPPLIER_NAME = drpSupplier.SelectedItem.Text;
                objProd.APM_DEPARTMENT = drpDepartment.SelectedValue;
                objProd.APM_UNIT_TYPE = drpUnitType.SelectedValue;
                objProd.APM_STOCK_AVAILABLE = txtAvaiStock.Text.Trim();

                string strAccName = txtAccountName.Text.Trim();
                string AccCode = "", AccName = "";

                string[] arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }



                objProd.APM_INVENTORY_AC_CODE = AccCode;
                objProd.APM_INVENTORY_AC_NAME = AccName;
                objProd.APM_REMARKS = txtRemarks.Text;
                objProd.APM_LOCATION = drpLocation.SelectedValue;
                objProd.APM_CUSTODIAN = drpCustodian.SelectedValue;
                objProd.APM_DEPRECIATION = drpDepreMethod.SelectedValue;
                objProd.APM_WARRANTY_EXPIRES = txtWarrantyDate.Text.Trim();
                objProd.APM_PURCHASE_DATE = txtPurchaseDate.Text.Trim();
                objProd.APM_SL_NO = txtSN.Text.Trim();


                strAccName = txtFixedAssetAC.Text.Trim();
                AccCode = "";
                AccName = "";

                arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }


                objProd.APM_FIXED_ASSET_CODE = AccCode;
                objProd.APM_FIXED_ASSET_NAME = AccName;



                strAccName = txtDepreciationAC.Text.Trim();
                AccCode = "";
                AccName = "";

                arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }


                objProd.APM_DEPRECIATION_AC_CODE = AccCode;
                objProd.APM_DEPRECIATION_AC_NAME = AccName;

                objProd.APM_TAXTYPE = drpTaxType.SelectedValue ;
                objProd.APM_TAXVALUE = txtTaxValue.Text.Trim();



                objProd.MODE = Convert.ToString(ViewState["SaveMode"]);
                objProd.UserID = Convert.ToString(Session["User_ID"]);
                objProd.ProductMasterAdd();

                txtProdCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PRODUCT");
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Product Master','Saved Successfully')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnCtlrRefresh_Click(object sender, EventArgs e)
        {
            txtProdCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PRODUCT");
            Clear();
        }

        protected void drpType_SelectedIndexChanged(object sender, EventArgs e)
        {

            divFixedAsset.Style.Add("display", "None");
            txtAccountName.Enabled = true;

            if (drpType.SelectedValue == "Fixed Asset")
            {
                divFixedAsset.Style.Add("display", "block");
                txtAccountName.Enabled = false;
            }
        }


        #endregion

        protected void imgReceiptZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindProductGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            divProductPopup.Visible = true;
        }

        protected void ProductEdit_Click(object sender, EventArgs e)
        {
            Clear();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvProdCode = (Label)gvScanCard.Cells[0].FindControl("lblgvProdCode");

            txtProdCode.Text = lblgvProdCode.Text;


            divProductPopup.Visible = false;
            BindProduct();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideTestMasterPopup()", true);
        }

        protected void btnDepreProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DepreciationProcess();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnDepreProcess_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                GoodsReceivedNoteBAL objGRN = new GoodsReceivedNoteBAL();
                DataSet DSPay = new DataSet();
                string Criteria1 = " 1=1 ";
                Criteria1 += " AND  AGRT_ITEM_CODE='" + txtProdCode.Text + "' AND AGRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSPay = objGRN.GoodsReceivedNoteTransGet(Criteria1);

                if (DSPay.Tables[0].Rows.Count > 0)
                {

                    lblStatus.Text = "This Product is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                GoodsTransferVoucherBAL objTrans= new GoodsTransferVoucherBAL();
                DataSet DSRec = new DataSet();

                string Criteria2 = " 1=1 ";
                Criteria2 += " AND  AGTT_ITEM_CODE='" + txtProdCode.Text.Trim() + "' AND AGTT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSRec = objTrans.GoodsTransferVoucherTransGet(Criteria2);

                if (DSRec.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This Product is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                GoodsConsumptionVoucherBAL objCon = new GoodsConsumptionVoucherBAL();
                DataSet DSCon = new DataSet();

                string Criteria3 = " 1=1 ";
                Criteria3 += " AND  AGCT_ITEM_CODE='" + txtProdCode.Text.Trim() + "' AND AGCT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSCon = objCon.GoodsConsumptionVoucherTransGet(Criteria3);

                if (DSCon.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This Product is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }





                CommonBAL objCom = new CommonBAL();
                string Criteria = " APM_CODE='" + txtProdCode.Text + "'";
                objCom.fnDeleteTableData("AC_PRODUCT_MASTER", Criteria);



                lblStatus.Text = " Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindProduct();
                Clear();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnDepreProcess_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void imgStaffRef_Click(object sender, EventArgs e)
        {
            BindCustodian();
        }

        protected void imgDeptfRef_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void imgCommRef_Click(object sender, EventArgs e)
        {
            BindLocation();
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtProdCode.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PRODUCT");
            Clear();
        }

        protected void txtProdCode_TextChanged(object sender, EventArgs e)
        {
            BindProduct();
        }

    }
}