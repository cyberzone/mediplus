﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus
{
    public partial class AuditLogDisplay : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("Mediplus.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("AuditLogDisplay " + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";
            //if (txtSearch.Text.Trim() != "")
            //{

            //    Criteria += " AND ( HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%' OR  HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PT_ID like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_IQAMA_NO like '%" + txtSearch.Text.Trim() + "%' OR   HPM_MOBILE like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_PHONE1 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PHONE2 like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_PHONE3 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_NATIONALITY like '%" + txtSearch.Text.Trim() + "%' OR  HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_POLICY_NO like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_BILL_CODE like '%" + txtSearch.Text.Trim() + "%' OR   HPM_INS_COMP_ID like '%" + txtSearch.Text.Trim() + "%' )";

            //}


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAL_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAL_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }



            //if (Convert.ToString(ViewState["DataDisplay"]).ToUpper() != "ALL".ToUpper())
            //{

            if (drpScreen.SelectedIndex != 0)
            {
                Criteria += " AND HAL_SCREEN_ID='" + drpScreen.SelectedValue + "'";

            }
            // }


            //if (Convert.ToString(ViewState["DataDisplay"]).ToUpper() == "ByEMR_ID".ToUpper())
            //{

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HAL_PT_ID='" + txtFileNo.Text + "'";

            }
            // }

            CommonBAL com = new CommonBAL();
            ds = com.AuditLogGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            gvAuditLog.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAuditLog.Visible = true;
                gvAuditLog.DataSource = DV;
                gvAuditLog.DataBind();


            }
            else
            {
                gvAuditLog.DataBind();

            }


            ds.Clear();
            ds.Dispose();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {
                    string strScreenID = "";

                    strScreenID = Request.QueryString["ScreenID"].ToString();
                    ViewState["ScreenID"] = strScreenID;
                    drpScreen.SelectedValue = strScreenID;
                    ViewState["DataDisplay"] = Request.QueryString["DataDisplay"].ToString();
                   // txtFIleNo.Text = Convert.ToString(Session["EMR_ID"]);

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");



                    BindGrid();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvPTList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvPTList_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {

                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = "";
            txtToDate.Text = "";

            if (Convert.ToString(ViewState["ScreenID"]) != null && Convert.ToString(ViewState["ScreenID"]) != "")
            {
                drpScreen.SelectedValue = Convert.ToString(ViewState["ScreenID"]);
            }
            else
            {
                drpScreen.SelectedIndex = 0;
            }

            //if (Convert.ToString(ViewState["EMR_ID"]) != null && Convert.ToString(ViewState["EMR_ID"]) != "")
            //{
            //    txtEMRID.Text = Convert.ToString(Session["EMR_ID"]);
            //}
            //else
            //{
            //    txtEMRID.Text = "";
            //}

        }
    }
}