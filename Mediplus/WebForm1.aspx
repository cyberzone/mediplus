﻿<%@ Page Title="" Language="C#"   AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Mediplus.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <title>Lab Test Profile Master Popup</title>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

     
</head>
<body>
  <form id="form1" runat="server">

      <input type="hidden" id="hidSignaturePath" value="C:\inetpub\wwwroot\HMS\Uploads\Signature\" runat="server" />
          <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
               
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                
            </td>
        </tr>

    </table>
    <div>
        <span class="lblCaption1" style="font-weight:bold;" > Consent Form Signature Update in Database </span> <br /><br /> 

       
        <asp:Button ID="btnUpdatePTSignature" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button orange small" Text=" Update PT Sginature " OnClick="btnUpdatePTSignature_Click" />


        <asp:Button ID="btnUpdateSubSignature" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button orange small" Text=" Update Substitute Sginature " OnClick="btnUpdateSubSignature_Click" />

        <asp:Button ID="btnUpdateTranSignature" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button orange small" Text=" Update Translator Sginature " OnClick="btnUpdateTranSignature_Click" />


        </div>
      </form>
    </body>
    </html>