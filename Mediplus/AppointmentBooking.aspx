﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppointmentBooking.aspx.cs" Inherits="Mediplus.AppointmentBooking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="Styles/Style.css" rel="stylesheet" type="text/css" />
<link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />



</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>

            <div style="padding-left: 60%; width: 100%;">


                <div id="divMessage" style="display: none; border: groove; height: auto; width: 600px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 300px; top: 200px;">

                    <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                            <span style="float: right;">

                                <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; font-size: 15px; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                            </span>
                            <br />
                            <div style="padding-left: 5px;">
                                <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                                    <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                                </div>
                                <br />
                                <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666; font-size: 12px;letter-spacing: 1px;"></asp:Label>
                            </div>
                            <span style="float: right; margin-right: 20px">

                                <input type="button" id="Button5" class="ButtonStyle gray" style="font-weight: bold; cursor: pointer; width: 100px;" value=" OK " onclick="HideErrorMessage()" />

                            </span>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <br />

                </div>
            </div>

           
             
            <div style="padding-left: 5px;">
                <table>
                    <tr>
                        <td class="PageHeader" style="letter-spacing:2px;font-family:Arial;">APPOINTMENT BOOKING
               
                        </td>
                    </tr>
                </table>

                <div style="padding-top: 0px; width:700px; border: 1px solid #005c7b; padding-bottom : 0px; border-radius: 10px;">
                    <table style="width: 100%;" cellpadding="5" cellspacing="5" border="0">
                        <tr>
                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">File No 
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFileNo" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Font-Size="12px" Font-Bold="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/Images/Zoom.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Search" Height="25PX" Width="25PX" OnClick="imgSearch_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">First Name 
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFName" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Font-Size="12px" Font-Bold="true"></asp:TextBox>
                                         <asp:ImageButton ID="imgSearchName" runat="server" ImageUrl="~/Images/Zoom.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Search" Height="25PX" Width="25PX" OnClick="imgSearchName_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">Mobile 
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMobileNo" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Font-Size="12px" Font-Bold="true"></asp:TextBox>
                                        <asp:ImageButton ID="imgSearchMobileNo" runat="server" ImageUrl="~/Images/Zoom.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Search" Height="25PX" Width="25PX" OnClick="imgSearchMobileNo_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>

                        <tr>
                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">Gender 
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpGender" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Font-Size="12px" Font-Bold="true">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>

                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">Date & Time
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="200px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);" Height="30px" Font-Size="12px" Font-Bold="true"></asp:TextBox>
                                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                        <asp:DropDownList ID="drpSTHour" CssClass="TextBoxStyle" runat="server" Width="45px" Height="30px" Font-Size="12px" Font-Bold="true">
                                        </asp:DropDownList>
                                        :
                                         <asp:DropDownList ID="drpSTMin" CssClass="TextBoxStyle" runat="server" Width="45px" Height="30px" Font-Size="12px" Font-Bold="true">
                                         </asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">Department 
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Font-Size="12px" Font-Bold="true" AutoPostBack="true" OnSelectedIndexChanged="drpDepartment_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="font-weight: bold; font-size: 12px;">Doctor 
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDoctor" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Font-Size="12px" Font-Bold="true">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height:50px;"></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="150px" CssClass="button red small" OnClick="btnSave_Click" OnClientClick="return  SaveValidation();" />
                                        <input type="button" id="btnClear" class="button gray small" value="Clear" style="width: 150px" onclick="Clear()" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                        </tr>
                    </table>
                       <img  src= '<%= ResolveUrl("~/Images/Clinic_Logo.png")%>'   alt="" style="width:100%;height:200px;border-bottom-left-radius : 10px;border-bottom-right-radius : 10px;"  >
                </div>
            </div>
    </form>
</body>
<script>
    function ShowErrorMessage(vMessage1, vMessage2, vColor) {
        document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
        document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

        if (vColor != '') {

            document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


        }

        document.getElementById("divMessage").style.display = 'block';



    }

    function HideErrorMessage() {

        document.getElementById("divMessage").style.display = 'none';

        document.getElementById("<%=txtFileNo.ClientID%>").innerHTML = '';
        document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
    }

    function Clear() {
        document.getElementById('<%=txtFName.ClientID%>').value = "";
        document.getElementById('<%=txtFileNo.ClientID%>').value = "";
        document.getElementById('<%=txtMobileNo.ClientID%>').value = "";
        document.getElementById('<%=drpGender.ClientID%>').value = "";
        document.getElementById('<%=drpDepartment.ClientID%>').value = "";
        document.getElementById('<%=drpDoctor.ClientID%>').value = "";

        document.getElementById("<%=txtFName.ClientID%>").style.border = '1px solid #999';
        document.getElementById("<%=txtMobileNo.ClientID%>").style.border = '1px solid #999';
        document.getElementById("<%=drpGender.ClientID%>").style.border = '1px solid #999';
        document.getElementById("<%=txtFromDate.ClientID%>").style.border = '1px solid #999';
        document.getElementById("<%=drpDoctor.ClientID%>").style.border = '1px solid #999';
    }


    function SaveValidation() {
        var IsError = false;
        var ErrorMessage = "";
        var trNewLineChar = "</br>";

        document.getElementById("<%=txtFName.ClientID%>").style.border = '1px solid #999';
             document.getElementById("<%=drpGender.ClientID%>").style.border = '1px solid #999';
             document.getElementById("<%=drpDoctor.ClientID%>").style.border = '1px solid #999';

             if (document.getElementById('<%=txtFName.ClientID%>').value == "") {
                 document.getElementById("<%=txtFName.ClientID%>").style.border = '1px solid red';
                 document.getElementById("<%=txtFName.ClientID%>").focus();
                ErrorMessage += ' Patient Name is Empty' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtMobileNo.ClientID%>').value == "") {
                document.getElementById("<%=txtMobileNo.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById("<%=txtMobileNo.ClientID%>").focus();
                }
                ErrorMessage += ' Patient Mobile is Empty' + trNewLineChar;
                IsError = true;
            }
            if (document.getElementById('<%=drpGender.ClientID%>').value == "") {
                document.getElementById("<%=drpGender.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById("<%=drpGender.ClientID%>").focus();
                }
                ErrorMessage += 'Patient Gender is Empty' + trNewLineChar;
                IsError = true;
            }

        if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
            document.getElementById("<%=txtFromDate.ClientID%>").style.border = '1px solid red';
            if (IsError == false) {
                document.getElementById("<%=txtFromDate.ClientID%>").focus();
            }
            ErrorMessage += ' Patient Mobile is Empty' + trNewLineChar;
            IsError = true;
        }

            if (document.getElementById('<%=drpDoctor.ClientID%>').value == "") {
                document.getElementById("<%=drpDoctor.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById("<%=drpDoctor.ClientID%>").focus();
                }
                ErrorMessage += 'Doctor Dtls. is Empty' + trNewLineChar;
                IsError = true;
            }

            if (IsError == true) {
               // ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }

            return true;
        }

</script>
</html>
