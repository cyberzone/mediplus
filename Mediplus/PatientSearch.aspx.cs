﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus
{
    public partial class PatientSearch : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();
        DataSet ds = new DataSet();

        #region Methods
          void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";

 

            // }
            ds = dbo.retun_patient_details(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            lblTotal.Text = "0";

            gvPatient.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPatient.Visible = true;
                gvPatient.DataSource = DV;
                gvPatient.DataBind();

                lblTotal.Text = ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }


            ds.Clear();
            ds.Dispose();
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {


                if (!IsPostBack)
                {
                    BindGrid();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientSearch.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
        
    }
}