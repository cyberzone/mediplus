﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Mediplus_BAL;
using SampleSAMLLibrary;
using System.Xml.Serialization;
using System.Text;

namespace Mediplus
{
    public partial class PatientMalaffiDtls : System.Web.UI.Page
    {
        #region Malaffi
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        /*
         Example EMR Roles          Role name in SAML       Role level in the solution
         -----------------------    --------------------    ---------------------------------
        • Physician                 Primary Provider        Level 1
        • Consultant
        • Dentist
    
        • Nurse                     Secondary Provider      Level 2   
        • Allied Health
        • Dietician

        • Pharmacist                Tertiary Provider       Level 3      
        • Other
         
        • Contact Centre            Front Desk              Level 4
        • Administration 
        
         */

        void LaunchSSO()
        {
            StringBuilder strData = new StringBuilder();
            string strNewLineChar = "\n";
            strData.Append("<?xml version='1.0' encoding='utf-8' ?>" + strNewLineChar);
            strData.Append("<malaffi>" + strNewLineChar);

            CommonBAL objCom = new CommonBAL();
            string Criteria = "1=1";
            DataSet DSMalaffiConfig = new DataSet();
            DSMalaffiConfig = objCom.fnGetFieldValue("TOP 1 *", "HMS_MALAFFI_CONFIGURATION", Criteria, "");

            if (DSMalaffiConfig.Tables[0].Rows.Count > 0)
            {
                string OID = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_OID"]);
                string strCertPath = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_CERT_PATH"]);
                string strCertPwd = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_CERT_PASSWORD"]);

                string strIssuer = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_ISSUER"]); //SPLMCARE
                string strReceipient = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_RECIPIENT"]); //"https://hie.malaffi.ae"
                string strAudience = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_AUDIENCE"]); //Malaffi
                string strEndPointURL = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_ENDPOINT_URL"]);


                string LicenseNo = "";
                Dictionary<string, string> SAMLAttributes = new Dictionary<string, string>();

                DataSet DS = new DataSet();
                Criteria = " 1=1  ";
                Criteria += "  AND HSFM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";
                DS = objCom.GetStaffMaster(Criteria);

                if (DS.Tables[0].Rows.Count <= 0)
                {
                    goto FunEnd;
                }


                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (DR.IsNull("HSFM_MOH_NO") == false && Convert.ToString(DR["HSFM_MOH_NO"]) != "")
                    {
                        LicenseNo = Convert.ToString(DR["HSFM_MOH_NO"]);
                    }
                    else
                    {
                        LicenseNo = Convert.ToString(Session["Branch_ProviderID"]) + "-" + Convert.ToString(DR["HSFM_STAFF_ID"]);
                    }


                 

                    string ResourceID = Convert.ToString(ViewState["PatientID"]).Trim() + "^^^&" + OID + "&ISO";  //  2.16.840.1.113883.3.8352.5.21328


                    SAMLAttributes.Add("urn:malaffi:firstname", Convert.ToString(DR["HSFM_FNAME"]));
                    SAMLAttributes.Add("urn:malaffi:lastname", Convert.ToString(DR["HSFM_LNAME"])); //Convert.ToString(DR["HSFM_MNAME"]) + " " +
                    SAMLAttributes.Add("urn:malaffi:fullname", Convert.ToString(DR["FullName"]));
                    SAMLAttributes.Add("urn:malaffi:emailaddress", Convert.ToString(DR["HSFM_EMAIL"]));
                    SAMLAttributes.Add("urn:malaffi:workphone", Convert.ToString(DR["HSFM_PHONE1"]));

                    //strData.Append("<firstname>" + Convert.ToString(DR["HSFM_FNAME"]) + "</firstname>" + strNewLineChar);
                    //strData.Append("<lastname>" + Convert.ToString(DR["HSFM_LNAME"]) + "</lastname>" + strNewLineChar);
                    //strData.Append("<fullname>" + Convert.ToString(DR["FullName"]) + "</fullname>" + strNewLineChar);
                    //strData.Append("<emailaddress>" + Convert.ToString(DR["HSFM_EMAIL"]) + "</emailaddress>" + strNewLineChar);
                    //strData.Append("<workphone>" + Convert.ToString(DR["HSFM_PHONE1"]) + "</workphone>" + strNewLineChar);



                    string strCategory = Convert.ToString(DR["HSFM_CATEGORY"]);
                    string strDepartment = Convert.ToString(DR["HSFM_DEPT_ID"]);

                    string Major = "", Department = "";

                    switch (strCategory)
                    {
                        case "Doctors":
                            Major = "Medical Practitioner";
                            break;
                        case "Nurse":
                            Major = "Nursing";
                            break;
                        default:
                            break;
                    }

                    SAMLAttributes.Add("urn:malaffi:major", Major);
                    SAMLAttributes.Add("urn:malaffi:profession", Major);
                    SAMLAttributes.Add("urn:malaffi:category", strDepartment);

                    //strData.Append("<major>" + Major + "</major>" + strNewLineChar);
                    //strData.Append("<profession>" + Major + "</profession>" + strNewLineChar);
                    //strData.Append("<category>" + strDepartment + "</category>" + strNewLineChar);



                    SAMLAttributes.Add("urn:malaffi:facilitylicense", Convert.ToString(Session["Branch_ProviderID"]));
                    SAMLAttributes.Add("urn:malaffi:gender", Convert.ToString(DR["HSFM_SEX"]));
                    SAMLAttributes.Add("urn:malaffi:organization", strIssuer);

                    //strData.Append("<facilitylicense>" + Convert.ToString(Session["Branch_ProviderID"]) + "</facilitylicense>" + strNewLineChar);
                    //strData.Append("<gender>" + Convert.ToString(DR["HSFM_SEX"]) + "</gender>" + strNewLineChar);
                    //strData.Append("<organization>" + strIssuer + "</organization>" + strNewLineChar);



                    if ((Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" || Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTOR"))
                    {
                        SAMLAttributes.Add("urn:oasis:names:tc:xacml:2.0:subject:role", "Primary Provider");

                      //  strData.Append("<role>" + "Primary Provider" + "</role>" + strNewLineChar);
                    }
                    else if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {
                        SAMLAttributes.Add("urn:oasis:names:tc:xacml:2.0:subject:role", "Secondary Provider");
                       // strData.Append("<role>" + "Secondary Provider" + "</role>" + strNewLineChar);

                    }
                    else if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF")
                    {
                        SAMLAttributes.Add("urn:oasis:names:tc:xacml:2.0:subject:role", "Primary Provider");

                      //  strData.Append("<role>" + "Primary Provider" + "</role>" + strNewLineChar);
                    }
                    else if (Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
                    {
                        SAMLAttributes.Add("urn:oasis:names:tc:xacml:2.0:subject:role", "Front Desk");

                        //strData.Append("<role>" + "Front Desk" + "</role>" + strNewLineChar);
                    }
                    else
                    {
                        SAMLAttributes.Add("urn:oasis:names:tc:xacml:2.0:subject:role", "Front Desk");
                        //strData.Append("<role>" + "Front Desk" + "</role>" + strNewLineChar);
                    }



                    SAMLAttributes.Add("urn:oasis:names:tc:xacml:2.0:resource:resource-id", ResourceID);

                   // strData.Append("<resource-id>" + ResourceID + "</resource-id>" + strNewLineChar);
                   // strData.Append("</malaffi>" + strNewLineChar);
                }
                TextFileWriting("SAML Attributes ");
                ////TextFileWriting( Convert.ToString(SAMLAttributes) );
               // TextFileWriting(Convert.ToString(strData));
              

                //// get the certificate
                String CertPath = System.Web.Hosting.HostingEnvironment.MapPath(@strCertPath);//System.Configuration.ConfigurationManager.AppSettings["pfxpath"]);

              // // TextFileWriting("Certificate Path  " + CertPath);

                X509Certificate2 SigningCert = new X509Certificate2(CertPath, strCertPwd);// System.Configuration.ConfigurationManager.AppSettings["password"], X509KeyStorageFlags.MachineKeySet);//System.Configuration.ConfigurationManager.AppSettings["password"]);


                // Add base 64 encoded SAML Response to form for POST
                String NameID = String.Empty;

                NameID = LicenseNo;  // Convert.ToString(ViewState["PatientID"]) + "-" + strTimeStamp; ;// "2.16.840.1.113883.3.8352.5.21328";



                SAMLResponse.Value = SampleSAMLLibrary.SAML20Assertion.CreateSAML20Response(strIssuer, 10, strAudience, NameID, strReceipient, SAMLAttributes, SigningCert);


              ////  TextFileWriting("Response Value  " + SAMLResponse.Value);
                // Set Body page load action
                ////htmlform frmidplauncher = (htmlform)this.page.findcontrol("frmidplauncher");
                frmIdPLauncher.Action = @strEndPointURL;   //"https://clinicalportal-uat.malaffi.ae/login-auth/saml?path=%2fpatient%2fauth-token%2fportal-context";

                // add javascript to HTTP POST to the SSO configured
                // This implements the IdP-initiated HTTP POST use case
                HtmlGenericControl body = (HtmlGenericControl)this.Page.FindControl("bodySSO");
                if (body != null)
                {
                    body.Attributes.Add("onload", "document.forms.frmIdPLauncher.submit();");
                }

                TextFileWriting("document Loaded  ");

            }

        FunEnd: ;
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ViewState["PatientID"] = Convert.ToString(Request.QueryString["PatientID"]);

                if (Convert.ToString(ViewState["PatientID"]) != "" && ViewState["PatientID"] != null)
                {

                    LaunchSSO();
                }
            }


        }
    }
}