﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Mediplus_BAL;
using System.IO;
using System.Drawing.Imaging;
using System.Data.SqlClient;


namespace Mediplus
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }



        void UpdateImageData(string strGC_ID, string SignType, string strImageName)
        {

            //    ../Uploads/Signature

          //  string strPath = @"C:\inetpub\wwwroot\HMS\Uploads\Signature\" + strImageName;
            string strPath = @hidSignaturePath.Value + strImageName;     // @"C:\inetpub\wwwroot\HMS\outlook menu\Uploads\Signature\" + strImageName;

            if(!File.Exists(strPath))
            {

                goto FunEnd;
            }


            Byte[] bytImage1 = null;

            System.Drawing.Image image = System.Drawing.Image.FromFile(strPath);
            bytImage1 = ImageToByte(image);



            string strCon = System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString();
            SqlConnection Con = new SqlConnection(strCon);

            Con.Open();

            String query = "";

            if (SignType == "PT_SIGN")
            {
                query = "UPDATE HMS_GENERAL_CONSENT SET   HGC_PT_SIGNATURE_IMAGE= @SIGNATURE_IMAGE WHERE HGC_ID=" + strGC_ID;

            }
            else if (SignType == "SUBS_SIGN")
            {
                query = "UPDATE HMS_GENERAL_CONSENT SET   HGC_SUBS_SIGNATURE_IMAGE= @SIGNATURE_IMAGE WHERE HGC_ID=" + strGC_ID;
            }
            else if (SignType == "TRAN_SIGN")
            {
                query = "UPDATE HMS_GENERAL_CONSENT SET   HGC_TRAN_SIGNATURE_IMAGE= @SIGNATURE_IMAGE WHERE HGC_ID=" + strGC_ID;
            }

            SqlCommand command = new SqlCommand(query, Con);
            command.Parameters.Add("@SIGNATURE_IMAGE", bytImage1);
            command.ExecuteNonQuery();

            Con.Close();


            FunEnd:;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            lblStatus.Text = "";
        }


        protected void btnUpdatePTSignature_Click(object sender, EventArgs e)
        {


            string Criteria = " 1=1 ";
            Criteria += " AND HGC_PT_SIGNATURE IS NOT NULL  AND HGC_PT_SIGNATURE <>''   AND HGC_PT_SIGNATURE_IMAGE IS NULL  ";

            clsGeneralConsent objGC = new clsGeneralConsent();
            DataSet DS = new DataSet();
            DS = objGC.GeneralConsentGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string strGC_ID = "", strImageName = "";

                    strGC_ID = Convert.ToString(DR["HGC_ID"]);
                    strImageName = Convert.ToString(DR["HGC_PT_SIGNATURE"]);

                    UpdateImageData(strGC_ID, "PT_SIGN", strImageName);

                }


            }

            lblStatus.Text = "Patient Signature Completed";

        }

        protected void btnUpdateSubSignature_Click(object sender, EventArgs e)
        {


            string Criteria = " 1=1 ";
            Criteria += " AND HGC_SUBS_SIGNATURE IS NOT NULL  AND HGC_SUBS_SIGNATURE <>''   AND HGC_SUBS_SIGNATURE_IMAGE IS NULL  ";

            clsGeneralConsent objGC = new clsGeneralConsent();
            DataSet DS = new DataSet();
            DS = objGC.GeneralConsentGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string strGC_ID = "", strImageName = "";

                    strGC_ID = Convert.ToString(DR["HGC_ID"]);
                    strImageName = Convert.ToString(DR["HGC_SUBS_SIGNATURE"]);

                    UpdateImageData(strGC_ID, "SUBS_SIGN", strImageName);

                }


            }

            lblStatus.Text = "Substitute Signature Completed";

        }

        protected void btnUpdateTranSignature_Click(object sender, EventArgs e)
        {


            string Criteria = " 1=1 ";
            Criteria += " AND  HGC_TRAN_SIGNATURE IS NOT NULL  AND HGC_TRAN_SIGNATURE <>''   AND HGC_TRAN_SIGNATURE_IMAGE IS NULL   ";

            clsGeneralConsent objGC = new clsGeneralConsent();
            DataSet DS = new DataSet();
            DS = objGC.GeneralConsentGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string strGC_ID = "", strImageName = "";

                    strGC_ID = Convert.ToString(DR["HGC_ID"]);
                    strImageName = Convert.ToString(DR["HGC_TRAN_SIGNATURE"]);

                    UpdateImageData(strGC_ID, "TRAN_SIGN", strImageName);

                }


            }

            lblStatus.Text = "Translator Signature Completed";

        }


    }
}