﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.Data;
using System.IO;
using System.Text;


namespace Mediplus.UserControls
{
    public partial class TopMenuEti : System.Web.UI.UserControl
    {
        public StringBuilder strData = new StringBuilder();
        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUsersGrid()
        {
            string Criteria = " 1=1  ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID <> 'SUPER_ADMIN'";

            DataSet ds = new DataSet();

            ds = objCom.fnGetFieldValue("*,CASE HUM_STATUS WHEN  'A' THEN 'Active' ELSE 'InActive' END AS HUM_STATUSDesc", "HMS_USER_MASTER", Criteria, "HUM_USER_NAME");
            if (ds.Tables[0].Rows.Count > 0)
            {

                gvUserList.DataSource = ds;
                gvUserList.DataBind();

            }



        }

        void BindModuleName()
        {

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND  HSCR_STATUS='A'  AND HSCR_MAIN_MENUNAME IS NOT NULL and HSCR_MAIN_MENUNAME <> ''   GROUP BY HSCR_MAIN_MENUNAME";

            DS = objCom.fnGetFieldValue(" HSCR_MAIN_MENUNAME ", "HMS_SCREEN_MASTER ", Criteria, "HSCR_MAIN_MENUNAME");


            if (DS.Tables[0].Rows.Count > 0)
            {

                drpModule.DataSource = DS;
                drpModule.DataValueField = "HSCR_MAIN_MENUNAME";
                drpModule.DataTextField = "HSCR_MAIN_MENUNAME";
                drpModule.DataBind();

            }
            drpModule.Items.Insert(0, "--- All ---");
            drpModule.Items[0].Value = "";

        }

        void BindScreenName()
        {

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND  HSCR_STATUS='A' AND HSCR_MAIN_MENUNAME IS NOT NULL AND HSCR_MAIN_MENUNAME <> ''";

            if (drpModule.SelectedIndex != 0)
            {

                Criteria += " AND HSCR_MAIN_MENUNAME='" + drpModule.SelectedValue + "'";
            }

            DS = objCom.fnGetFieldValue(" DISTINCT HSCR_SCREEN_ID, HSCR_SCREEN_NAME,HSCR_MAIN_MENUNAME ", "HMS_SCREEN_MASTER", Criteria, "HSCR_MAIN_MENUNAME,HSCR_SCREEN_NAME");


            if (DS.Tables[0].Rows.Count > 0)
            {

                drpScreen.DataSource = DS;
                drpScreen.DataValueField = "HSCR_SCREEN_ID";
                drpScreen.DataTextField = "HSCR_SCREEN_NAME";
                drpScreen.DataBind();

            }

            drpScreen.Items.Insert(0, "--- All ---");
            drpScreen.Items[0].Value = "";

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["PWD_FORMAT_REQUIRED"] = "0";

            ViewState["PWD_MIN_LENGTH"] = "0";
            ViewState["PWD_MAX_LENGTH"] = "0";
            ViewState["PWD_NUM_MIN_LENGTH"] = "0";
            ViewState["PWD_LETTER_MIN_LENGTH"] = "0";
            ViewState["PWD_MINIMUM_AGE"] = "0";

            ViewState["PWD_PREV_NOTREPEAT"] = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_FORMAT_REQUIRED")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_FORMAT_REQUIRED"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MAX_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_MAX_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_NUM_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_NUM_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_LETTER_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_LETTER_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MINIMUM_AGE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_MINIMUM_AGE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_PREV_NOTREPEAT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_PREV_NOTREPEAT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                }
            }



        }

        private bool CheckPasswordFormat(string passwordText)
        {

            int minimumLength = Convert.ToInt32(ViewState["PWD_MIN_LENGTH"]), maximumLength = Convert.ToInt32(ViewState["PWD_MAX_LENGTH"]),
                minimumNumbers = Convert.ToInt32(ViewState["PWD_NUM_MIN_LENGTH"]), minimumLetters = Convert.ToInt32(ViewState["PWD_LETTER_MIN_LENGTH"]),
                minimumSpecialCharacters = 1, minUpperCharacters = 1;

            //Assumes that special characters are anything except upper and lower case letters and digits
            //Assumes that ASCII is being used (not suitable for many languages)

            int letters = 0;
            int digits = 0;
            int specialCharacters = 0;
            int UpperCharacters = 0;

            //Make sure there are enough total characters
            if (passwordText.Length < minimumLength)
            {
                ShowError("You must have at least " + minimumLength + " characters in your password.");
                return false;
            }
            //Make sure there are enough total characters
            if (passwordText.Length > maximumLength)
            {
                ShowError("You must have no more than " + maximumLength + " characters in your password.");
                return false;
            }

            foreach (var ch in passwordText)
            {
                if (char.IsLetter(ch)) letters++; //increment letters
                if (char.IsDigit(ch)) digits++; //increment digits

                //Test for only letters and numbers...
                if (!((ch > 47 && ch < 58) || (ch > 64 && ch < 91) || (ch > 96 && ch < 123)))
                {
                    specialCharacters++;
                }

                if (Char.IsUpper(ch) == true)
                {
                    UpperCharacters++;
                }
            }
            //Make sure there are enough digits
            if (letters < minimumLetters)
            {
                ShowError("You must have at least " + minimumLetters + " letters in your password.");
                return false;
            }


            //Make sure there are enough digits
            if (digits < minimumNumbers)
            {
                ShowError("You must have at least " + minimumNumbers + " numbers in your password.");
                return false;
            }

            //Make sure there are enough special characters -- !(a-zA-Z0-9)
            if (specialCharacters < minimumSpecialCharacters)
            {
                ShowError("You must have at least " + minimumSpecialCharacters + " special characters (like @,$,%,#) in your password.");
                return false;
            }


            if (UpperCharacters < minUpperCharacters)
            {
                ShowError("You must have at least one Upper characters  in your password.");
                return false;
            }
            return true;
        }

        private void ShowError(string errorMessage)
        {
            lblStatus.Text = errorMessage;
        }

        void Clear()
        {
            txtOldPwd.Text = "";
            txtNewPwd.Text = "";
            txtConPwd.Text = "";
            txtUserID.Text = "";
            txtUserName.Text = "";
            lblStatus.Text = "";
           


        }

        void BindAuditLog()
        {
            string Criteria = " 1=1 ";


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EAL_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EAL_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }



            //if (Convert.ToString(ViewState["DataDisplay"]).ToUpper() != "ALL".ToUpper())
            //{

            if (drpScreen.SelectedIndex != 0)
            {
                Criteria += " AND EAL_SCREEN_ID='" + drpScreen.SelectedValue + "'";

            }
            // }


            //if (Convert.ToString(ViewState["DataDisplay"]).ToUpper() == "ByEMR_ID".ToUpper())
            //{

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND EAL_PT_ID='" + txtFileNo.Text + "'";

            }
            if (txtEmrID.Text.Trim() != "")
            {
                Criteria += " AND EAL_EMR_ID='" + txtEmrID.Text + "'";

            }
            if (txtInvoiceNo.Text.Trim() != "")
            {
                Criteria += " AND EAL_INVOICE_ID='" + txtInvoiceNo.Text + "'";

            }
            // }
            DataSet ds = new DataSet();
            CommonBAL com = new CommonBAL();
            ds = com.EMRAuditLogGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            gvAuditLog.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAuditLog.Visible = true;
                gvAuditLog.DataSource = DV;
                gvAuditLog.DataBind();


            }
            else
            {
                gvAuditLog.Visible = false;
                gvAuditLog.DataBind();

            }


            ds.Clear();
            ds.Dispose();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

                lblLoginUser.Text = "Welcome " + Convert.ToString(Session["User_Name"]) + ", " + Convert.ToString(Session["User_DeptID"]);
                lblStatus.Text = "";
                if (!IsPostBack)
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindModuleName();
                    BindScreenName();
                    papulateMenuTree();
                    BindScreenCustomization();
                  //  BindAuditLog();
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_ID"]).ToUpper() == "SUPER_ADMIN")
                    {
                        BindUsersGrid();
                    }
                    else
                    {
                        txtUserID.Text = Convert.ToString(Session["User_ID"]);
                        txtUserName.Text = Convert.ToString(Session["User_Name"]);
                    }


                    if (Convert.ToString(Session["PREV_LOGIN"]) != "")
                    {
                        lblPrevLogin.Text = " Last Login & Time : " + Convert.ToString(Session["PREV_LOGIN"]);
                    }


                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {

                        if (Convert.ToString(Session["PWD_EXPIRY_DAYS"]) != "0" && Convert.ToInt32(Session["PWD_MINIMUM_AGE"]) <= Convert.ToInt32(Session["UserCreatedDateDiff"]))
                        {
                            lblPwdExpiryDaysMsg.Text = "Password will expire within " + Convert.ToString(Session["UserPasswordExpire"]) + " Days";
                            lblPwdExpiryDaysMsg.Visible = true;


                            // aPwdChange.Visible = false;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       TopMenuEti.Page_Load");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();
                string EncryPassword = "";

                string Criteria = " 1=1 ";

                if (Convert.ToString(ViewState["PWD_PREV_NOTREPEAT"]) == "1")
                {
                    Criteria = " 1=1 ";
                    Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "'";

                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));
                        Criteria += " AND (HUM_USER_PASS='" + EncryPassword + "' OR HUM_PREV_PASS1='" + EncryPassword + "' OR HUM_PREV_PASS2='" + EncryPassword + "')";
                    }
                    else
                    {
                        Criteria += " AND (HUM_USER_PASS='" + txtNewPwd.Text.Trim() + "' OR HUM_PREV_PASS1='" + txtNewPwd.Text.Trim() + "' OR HUM_PREV_PASS2='" + txtNewPwd.Text.Trim() + "')";
                    }


                    DataSet DS = new DataSet();
                    DS = objCom.UserMasterGet(Criteria);
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblStatus.Text = "Password should not match with previous two passwords";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }

                }

                if (Convert.ToString(ViewState["PWD_FORMAT_REQUIRED"]) != "0")
                {

                    if (CheckPasswordFormat(txtNewPwd.Text) == false)
                    {
                        goto FunEnd;
                    }

                }

                if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_ID"]).ToUpper() == "SUPER_ADMIN")
                {
                    Criteria = " 1=1 ";
                    Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "'";

                    string FieldNameWithValues = "";

                    objCom = new CommonBAL();
                    EncryPassword = "";
                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));
                        FieldNameWithValues = " HUM_USER_PASS ='" + EncryPassword + "',HUM_CREATED_DATE=getdate()";
                    }
                    else
                    {
                        FieldNameWithValues = " HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "',HUM_CREATED_DATE=getdate()";
                    }

                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_USER_MASTER", Criteria);

                    lblStatus.Text = "Password Changed Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    if (txtOldPwd.Text.Trim() == "")
                    {
                        lblStatus.Text = "Please Enter the Old Password";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }

                    Criteria = " 1=1 ";

                    EncryPassword = "";
                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtOldPwd.Text.Trim()));
                        Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";

                    }
                    else
                    {
                        Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + txtOldPwd.Text.Trim() + "'";

                    }


                    DataSet ds = new DataSet();



                    ds = objCom.UserMasterGet(Criteria);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string FieldNameWithValues = "";

                        EncryPassword = "";
                        if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                        {
                            EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));

                            FieldNameWithValues = " HUM_PREV_PASS1 = HUM_PREV_PASS2 ,HUM_PREV_PASS2 = HUM_USER_PASS ";
                            FieldNameWithValues += ",HUM_USER_PASS ='" + EncryPassword + "',HUM_CREATED_DATE=getdate()";
                        }
                        else
                        {
                            FieldNameWithValues = " HUM_PREV_PASS1 = HUM_PREV_PASS2 ,HUM_PREV_PASS2 = HUM_USER_PASS ";
                            FieldNameWithValues += " ,HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "',HUM_CREATED_DATE=getdate()";
                        }

                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_USER_MASTER", Criteria);
                        // objDB.ExecuteReader("UPDATE HMS_USER_MASTER SET HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "' WHERE   HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "'");

                        lblStatus.Text = "Password Changed Successfully";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblStatus.Text = "Your Old Password Is Incorrect";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }

        FunEnd: ;
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            Clear();
            TextFileWriting(System.DateTime.Now.ToString() + "Edit_Click Started");

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;


            Label lblUserID = (Label)gvScanCard.Cells[0].FindControl("lblUserID");
            Label lblUserName = (Label)gvScanCard.Cells[0].FindControl("lblUserName");
            Label lblPassword = (Label)gvScanCard.Cells[0].FindControl("lblPassword");
            Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");

            if (Convert.ToString(Session["User_ID"]).ToUpper() == "SUPER_ADMIN")
            {
                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1" )//&& lblPassword.Text.Length > 15
                {
                    txtOldPwd.Text = objCom.SimpleDecrypt(lblPassword.Text.Trim());

                }
                else
                {
                    txtOldPwd.Text = lblPassword.Text.Trim();
                }

            }
            txtUserID.Text = lblUserID.Text.Trim();
            txtUserName.Text = lblUserName.Text.Trim();

        }

        void papulateMenuTree()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria1 = " 1=1 AND  HM_MENU_STATUS=1 AND HM_MENU_TYPE='HEADER_SUBMENU' AND HM_MENU_FOR ='HMS' ";
            //Criteria1 += " AND (HM_DEPT_ID  LIKE'%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR HM_DEPT_ID='' OR HM_DEPT_ID IS NULL)";
            //Criteria1 += " AND (HM_DEPT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR HM_DEPT_ID_HIDE='' OR HM_DEPT_ID_HIDE IS NULL )";


            Int32 i;


            DS = objCom.fnGetFieldValue(" * ", "HMS_MENUS", Criteria1, "HM_MENU_ORDER");


            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow dbRow in DS.Tables[0].Rows)
                {

                    strData.Append("<a class='label' href=#  onclick=openInNewTab('" + Convert.ToString(dbRow["HM_MENU_URL"]) + "')>" + Convert.ToString(dbRow["HM_MENU_NAME"]) + "</a>");



                    //MenuItem L1 = new MenuItem(Convert.ToString(dbRow["HM_MENU_NAME"]), Convert.ToString(dbRow["HM_MENU_ID"]), "", Convert.ToString(dbRow["HM_MENU_URL"]), "_self");
                    //Menu1.Items.Add(L1);
                    ///}
                    ///


                }


            }
        }

        protected void gvPTList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindAuditLog();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvPTList_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {

                BindAuditLog();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

            drpModule.SelectedIndex = 0;
            drpScreen.SelectedIndex = 0;
            txtFileNo.Text = "";
            txtEmrID.Text = "";
            txtInvoiceNo.Text = "";


            if (Convert.ToString(ViewState["ScreenID"]) != null && Convert.ToString(ViewState["ScreenID"]) != "")
            {
                drpScreen.SelectedValue = Convert.ToString(ViewState["ScreenID"]);
            }
            else
            {
                drpScreen.SelectedIndex = 0;
            }

            //if (Convert.ToString(ViewState["EMR_ID"]) != null && Convert.ToString(ViewState["EMR_ID"]) != "")
            //{
            //    txtEMRID.Text = Convert.ToString(Session["EMR_ID"]);
            //}
            //else
            //{
            //    txtEMRID.Text = "";
            //}

        }

        protected void drpModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindScreenName();
        }
    }
}