﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopMenuEti.ascx.cs" Inherits="Mediplus.UserControls.TopMenuEti" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<link rel="stylesheet" type="text/css" href="design/css/home.css">

<script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

<link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .header
    {
        height: 40px;
        width: 100%;
        background: #212121;
    }

    /* Dropdown Button */
    .DotlineImg
    {
        width: 23px;
        height: 23px;
        cursor: pointer;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown
    {
        position: relative;
        display: inline-block;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-content
    {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        overflow: hidden;
    }

        /* Links inside the dropdown */
        .dropdown-content a
        {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

            /* Change color of dropdown links on hover */
            .dropdown-content a:hover
            {
                background-color: #a6a6a6; /*#D64535;*/ /*#f1f1f1*/
                color: #ffffff;
            }

    /* Show the dropdown menu on hover */
    .dropdown:hover .dropdown-content
    {
        display: block;
    }

    /* Change the background color of the dropdown button when the dropdown content is shown */
    .dropdown:hover .DotlineImg
    {
        background-color: rgba(0,0,0,0.2);
    }
</style>

<script language="javascript" type="text/javascript">
    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }

    function PageLoad() {

        var UserCategory = '<%= Convert.ToString(Session["User_Category"]).ToUpper()%>'
            var UserID = '<%= Convert.ToString(Session["User_ID"]).ToUpper()%>'

            if (UserCategory != "ADMIN STAFF" && UserID != 'SUPER_ADMIN') {
                document.getElementById("divUserDetails").style.height = "200px";
                document.getElementById("divUserListGrid").style.display = 'none';
            }
        }


        function ShowUserDetailsPopup() {

            document.getElementById("divUserDetails").style.display = 'block';


        }

        function HideUserDetailsPopup() {

            document.getElementById("divUserDetails").style.display = 'none';

        }


        function ShowAuditLogPopup() {

            document.getElementById("divAuditLog").style.display = 'block';


        }

        function HideAuditLogPopup() {

            document.getElementById("divAuditLog").style.display = 'none';

        }


        function ShowAuditLog() {

            var win = window.open('../../AuditLogDisplay.aspx?ScreenID=HMS_APPOINTMENT&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

        }

</script>
<body onload="PageLoad()"></body>

<div class="header" style="height: 53px;">

    <div id="redline">
    </div>

    <div id="divhead" style="padding-left: 5px;">

        <b>Mediplus HIS</b>
        <div>
            <span style="color: #ffffff; font-family: Segoe UI,Arial; font-size: 12px; float: left;">Version 20.0.5</span>
        </div>

    </div>
    <div id="headdetails">
        <div id="bl">
            <asp:Label ID="lblLoginUser" runat="server" CssClass="label" Style="font-family: BenchNine, sans-serif; text-transform: lowercase; color: #f78d1d; font-size: 15px;"></asp:Label></b></div>
        <asp:Label ID="lblPwdExpiryDaysMsg" runat="server" CssClass="label" Style="color: Yellow;" Text="test" Visible="false"></asp:Label>
        <asp:Label ID="lblPrevLogin" runat="server" CssClass="label" Style="color: #ffffff;"></asp:Label>
    </div>
    <div id="headderDivider2"></div>
    <div id="headderSetting">

        <div class="dropdown">
            <img src='<%= ResolveUrl("~/Images/Dotline.png")%>' alt="" class="DotlineImg">
            <div class="dropdown-content">
                <%=strData%>
                <a id="lnkAuditLog" runat="server" href="javascript:ShowAuditLogPopup();" style="text-decoration: none;" class="label">Audit Log </a>
                <a id="aPwdChange" runat="server" href="javascript:ShowUserDetailsPopup();" class="label" style="text-decoration: none; display: block;">Change Password</a>

            </div>
        </div>

    </div>
    <div id="headderDivider1"></div>
    <div id="headderLogout" class="headderLogout">
        <a href='<%= ResolveUrl("~/Default.aspx")%>'>

            <img src='<%= ResolveUrl("~/Images/Logout.png")%>' alt="" style="width: 23px; height: 23px;" title="Logout"></a>
    </div>


    <div id="redbox">
        <img id="redboxcut" src='<%= ResolveUrl("~/Images/red.png")%>' style="height: 33px;" alt="My company name" />
        <div id="headdetailsOffer" onclick="openInNewTab('http://cyberzonecomputer.com/');" title="cyberzonecomputer.com"><b>Cyberzone</b> </div>

    </div>

    <div id="divUserDetails" style="display: none; overflow: hidden; border: groove; height: 600px; width: 950px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 53px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="PageHeader" style="vertical-align: top; width: 50%;">Change Password
                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideUserDetailsPopup()" />
                </td>
            </tr>

        </table>
        <div style="width: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;"></div>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="height: 10px;">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

        <table class="table" cellpadding="1" cellspacing="1">

            <tr>
                <td class="lblCaption1" style="height: 30px;">User ID 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtUserID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 30px;">User Name 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px;">Old Password : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtOldPwd" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 30px;">New Password : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNewPwd" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px;">Confirm Password : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtConPwd" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 30px;"></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click" OnClientClick="return validatechangepawd();" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>


        <div id="divUserListGrid" style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border-color: #000; border-style: dotted; border-width: 2px;">
            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gvUserList" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" GridLines="None">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>

                            <asp:TemplateField HeaderText="User ID" FooterText="File No" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnUserID" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblPassword" CssClass="lblCaption1" runat="server" Text='<%# Bind("HUM_USER_PASS") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblUserID" CssClass="lblCaption1" runat="server" Text='<%# Bind("HUM_USER_ID") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name" FooterText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkUserName" CssClass="lblCaption1" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblUserName" CssClass="lblCaption1" runat="server" Text='<%# Bind("HUM_USER_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Status" FooterText="Comp Name" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblStatus" CssClass="lblCaption1" runat="server" Text='<%# Bind("HUM_STATUSDesc") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>







                        </Columns>


                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>


    <div id="divAuditLog" style="display: none; overflow: hidden; border: groove; height: 500px; width: 1050px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 200px; top: 53px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="PageHeader" style="vertical-align: top; width: 50%;">Audit Log
                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideAuditLogPopup()" />
                </td>
            </tr>

        </table>
        <div style="width: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;"></div>
        <br />
        <table style="width: 100%">
            <tr>

                <td class="TDStyle">DATE
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFromDate" runat="server" Width="100px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">TO DATE

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtToDate" runat="server" Width="100px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">MODULE 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpModule" runat="server" CssClass="TextBoxStyle" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="drpModule_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">SCREEN 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpScreen" runat="server" Width="150px" CssClass="TextBoxStyle">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


            </tr>
            <tr>
                <td class="TDStyle">FILE NO 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFileNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">EMR ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEmrID" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">INVOICE NO
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInvoiceNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnRefresh" runat="server" CssClass="button red small" Width="80px" Text="Refresh" OnClick="btnRefresh_Click" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="80px" Text="Clear" OnClick="btnClear_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 98%; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvAuditLog" runat="server"
                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvPTList_Sorting"
                        EnableModelValidation="True" Width="100%" GridLines="None">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="DATE" SortExpression="EAL_CREATED_DATE" ItemStyle-Width="120px" HeaderStyle-Width="120px">
                                <ItemTemplate>

                                    <asp:Label ID="lblCreatedDate" CssClass="label" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FILE NO" SortExpression="EAL_EMR_ID" ItemStyle-Width="70px" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblPT_ID" CssClass="label" runat="server" Text='<%# Bind("EAL_PT_ID") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EMR ID" SortExpression="EAL_EMR_ID" ItemStyle-Width="70px" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblEMR_ID" CssClass="label" runat="server" Text='<%# Bind("EAL_EMR_ID") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="INVOICE NO" SortExpression="EAL_EMR_ID" ItemStyle-Width="70px" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblINVOICE_ID" CssClass="label" runat="server" Text='<%# Bind("EAL_INVOICE_ID") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SCREEN" SortExpression="EAL_SCREEN_NAME">
                                <ItemTemplate>

                                    <asp:Label ID="lblScreenName" CssClass="label" runat="server" Text='<%# Bind("EAL_SCREEN_NAME") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ACTION" SortExpression="EAL_ACTION">
                                <ItemTemplate>

                                    <asp:Label ID="lblAction" CssClass="label" runat="server" Text='<%# Bind("EAL_ACTION") %>'></asp:Label>


                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="REMARKS" SortExpression="EAL_REMARKS">
                                <ItemTemplate>

                                    <asp:Label ID="lblRemarks" CssClass="label" runat="server" Text='<%# Bind("EAL_REMARKS") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="USER" SortExpression="CreatedUserName">
                                <ItemTemplate>

                                    <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("CreatedUserName") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
</div>
