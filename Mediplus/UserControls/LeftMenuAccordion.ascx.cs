﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

using Mediplus_BAL;
namespace Mediplus.UserControls
{
    public partial class LeftMenuAccordion : System.Web.UI.UserControl
    {
        public string PatientID { set; get; }
        public string BalanceAmt { set; get; }


        public string HPM_INS_COMP_ID { set; get; }
        public string HPM_INS_COMP_NAME { set; get; }
        public string HPM_POLICY_NO { set; get; }

        public string HPM_POLICY_TYPE { set; get; }
        public string HPM_ID_NO { set; get; }

        public string HPM_PACKAGE_NAME { set; get; }



        public string HPM_DED_TYPE { set; get; }
        public string HPM_DED_AMT { set; get; }
        public string HPM_COINS_TYPE { set; get; }
        public string HPM_COINS_AMT { set; get; }

        CommonBAL objCom = new CommonBAL();

        DataSet GetDoctorMaster()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.GetStaffMaster(Criteria);


            return DS;
        }

        void BindDoctor()
        {
            // DrpList.Items.Add(new ListItem(Convert.ToString(DRDrpData["FullName"]), Convert.ToString(DRDrpData["HSFM_STAFF_ID"])));

            drpDoctor.DataSource = GetDoctorMaster();
            drpDoctor.DataValueField = "HSFM_STAFF_ID";
            drpDoctor.DataTextField = "FullName";
            drpDoctor.DataBind();

            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "";
        }

        DataSet GetSegmentData()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND HLS_ACTIVE=1 AND HLS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "'";// AND HLS_TYPE='" + strType + "'";


            DataSet DS = new DataSet();

            DS = objCom.HomeLeftSegmentGet(Criteria);



            return DS;

        }

        DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND HLSM_STATUS=1 ";
            Criteria += " and HLSM_TYPE='" + SegmentType + "'";


            objCom = new CommonBAL();
            DS = objCom.HomeLeftSegmentMasterGet(Criteria);


            return DS;

        }

        Boolean IsNumber(string strSearch)
        {
            strSearch = strSearch.Replace("-", "");
            for (int i = 0; i <= strSearch.Length -1; i++)
            {
                if (char.IsNumber(strSearch, i) == false)
                {
                    return false;
                }
            }


            return true;
        }

        Boolean IsLetter(string strSearch)
        {
            
            for (int i = 0; i <= strSearch.Length - 1; i++)
            {
                if (char.IsLetter(strSearch, i) == false)
                {
                    return false;
                }
            }


            return true;
        }


        DataSet PatientMasterGet()
        {

            //  txtPolicyNo.Text = "";
            // txtPolicyType.Text = "";
            //  txtIdNo.Text = "";
            //  txtPackage.Text = "";


            DataSet DSpt = new DataSet();
            string Criteria = " 1=1 ";

            //Criteria += " AND (  HPM_PT_ID    like '%" + txtSearch.Text.Trim() + "%' OR HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%' ";
            //Criteria += " AND    HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' OR HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' ";
            //Criteria += " AND    HPM_MOBILE   like '%" + txtSearch.Text.Trim() + "%') ";


            string strSearch = "";
            strSearch = txtSearch.Text.Trim();



            if (Convert.ToString(ViewState["PT_ID"]) != "")
            {
                Criteria += " AND  HPM_PT_ID ='" + Convert.ToString(ViewState["PT_ID"]) + "'";
            }
            else
            {

                if (drpSearchType.SelectedValue == "All")
                {

                    if (IsNumber(strSearch) == true && strSearch.Length < 15 && strSearch.IndexOf("0", 0) == 0)
                    {

                        Criteria += " AND  HPM_MOBILE  like '" + txtSearch.Text.Trim() + "%'";
                    }
                    else if (IsNumber(strSearch) == true && strSearch.Length >= 15)
                    {

                        Criteria += " AND  HPM_IQAMA_NO ='" + txtSearch.Text.Trim() + "'";
                    }
                    else if (IsLetter(strSearch) == true)
                    {
                        Criteria += " AND  ( HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%'   OR    HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' OR HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' )";
                    }
                    else
                    {
                       Criteria += " AND   HPM_PT_ID like '%" + txtSearch.Text.Trim() + "%' " ;
                    //   " OR   HPM_PHONE1 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PHONE2 like '%" + txtSearch.Text.Trim() + "%' " +
                    //   "  OR   HPM_PHONE3 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
                    //   "  OR   HPM_NATIONALITY like '%" + txtSearch.Text.Trim() + "%'  OR   HPM_POLICY_NO like '%" + txtSearch.Text.Trim() + "%' "+
                    //   "  OR   HPM_BILL_CODE like '%" + txtSearch.Text.Trim() + "%' OR   HPM_INS_COMP_ID like '%" + txtSearch.Text.Trim() + "%' )";
                     }

                }
                else if (drpSearchType.SelectedValue == "FileNo")
                {
                    Criteria += " AND  HPM_PT_ID ='" + txtSearch.Text.Trim() + "'";
                }
                else if (drpSearchType.SelectedValue == "Name")
                {
                    Criteria += " AND  ( HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%'   OR    HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' OR HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' )";
                }
                else if (drpSearchType.SelectedValue == "Mobile")
                {
                    Criteria += " AND  HPM_MOBILE  like '" + txtSearch.Text.Trim() + "%'";
                }
                else if (drpSearchType.SelectedValue == "EmiratesID")
                {
                    Criteria += " AND  HPM_IQAMA_NO ='" + txtSearch.Text.Trim() + "'";
                }


            }


            objCom = new CommonBAL();
            DSpt = objCom.PatientMasterGet(Criteria);


            if (DSpt.Tables[0].Rows.Count > 0)
            {
                if (DSpt.Tables[0].Rows.Count == 1)
                {
                    hidTodayDBDate.Value = Convert.ToString(DSpt.Tables[0].Rows[0]["TodayDBDate"]);
                    ViewState["PT_ID"] = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_PT_ID"]);
                    Session["PatientID"] = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_PT_ID"]);

                    if (DSpt.Tables[0].Rows[0].IsNull("HPM_OPENBAL") == false && Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_OPENBAL"]) != "")
                    {
                        ViewState["OpBalance"] = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_OPENBAL"]);
                    }
                    else
                    {
                        ViewState["OpBalance"] = 0;
                    }

                    //  drpDoctor.SelectedValue = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_DR_ID"]);


                    if (Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_PT_TYPE"]).ToUpper() == "CR" || Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_PT_TYPE"]).ToUpper() == "CREDIT")
                    {
                        HPM_INS_COMP_ID = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                        HPM_INS_COMP_NAME = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);

                        HPM_POLICY_NO = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                        HPM_POLICY_TYPE = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                        HPM_ID_NO = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_ID_NO"]);
                        HPM_PACKAGE_NAME = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_PACKAGE_NAME"]);



                        HPM_DED_TYPE = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_DED_TYPE"]);
                        HPM_DED_AMT = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_DED_AMT"]);
                        HPM_COINS_TYPE = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_COINS_TYPE"]);
                        HPM_COINS_AMT = Convert.ToString(DSpt.Tables[0].Rows[0]["HPM_COINS_AMT"]);


                    }

                    GetPatientVisit();
                    BindSegmentData(DSpt);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPTDtlsPopup()", true);
                    gvPTDtls.DataSource = DSpt;
                    gvPTDtls.DataBind();
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('No Data','Patient Dtls. not match with search Dtls')", true);

            }



            return DSpt;
        }

        void GetAppointment()
        {
            if (Convert.ToString(ViewState["PT_ID"]) == "")
            {
                goto FunEnd;
            }

            string Criteria = " 1=1 AND HAS_STATUS!='Cancelled' ";
            // Criteria += "  AND HAM_STATUS ='Waiting'";
            Criteria += "  AND HAM_FILENUMBER ='" + Convert.ToString(ViewState["PT_ID"]) + "'";
            Criteria += "   AND    CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) >=  CONVERT(datetime,convert(varchar(10),GETDATE(),101),101)";


            objCom = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = objCom.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {

                gvAppointment.DataSource = DSAppt;
                gvAppointment.DataBind();

            }
            else
            {
                gvAppointment.DataBind();
            }

        FunEnd: ;

        }

        void GetInvoiceDtls()
        {


            string strInvDays = "";
            strInvDays = "-" + Convert.ToString(Session["RevisitDays"]);

            string Criteria = " 1=1 AND HIT_SERV_TYPE='C' ";
            Criteria += "  AND HIM_PT_ID ='" + Convert.ToString(ViewState["PT_ID"]) + "'";
            Criteria += "   AND    CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >=  CONVERT(datetime,convert(varchar(10),DATEADD(DD," + strInvDays + ",GETDATE()),101),101)";


            objCom = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = objCom.InvoiceTransGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {

                gvInvoice.DataSource = DSAppt;
                gvInvoice.DataBind();

            }
            else
            {
                gvInvoice.DataBind();
            }



        }

        void GetPTVisitDtls()
        {


            string strInvDays = "";
            strInvDays = "-" + Convert.ToString(Session["RevisitDays"]);

            string Criteria = " 1=1 AND HPV_STATUS <>'C'   AND HPV_STATUS <>'D' ";
            Criteria += "  AND HPV_PT_ID ='" + Convert.ToString(ViewState["PT_ID"]) + "'";
            Criteria += "   AND    CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >=  CONVERT(datetime,convert(varchar(10),DATEADD(DD,-30,GETDATE()),101),101)";


            objCom = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = objCom.PatientVisitGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {

                gvPTVisit.DataSource = DSAppt;
                gvPTVisit.DataBind();

            }
            else
            {
                gvPTVisit.DataBind();
            }



        }

        void BindBalanceHistory()
        {
            Decimal decInvoiceAmt = 0, decPadiAmt = 0, decRejectedAmt = 0, decDepositAmt = 0, decBalanceDue = 0;
            decInvoiceAmt = fnCalculatePTAmount("INVOICE") - fnCalculatePTAmount("RETURN");
            decPadiAmt = fnCalculatePTAmount("PAIDAMT") + fnCalculatePTAmount("RETURNPAIDAMT");
            decRejectedAmt = fnCalculatePTAmount("REJECTED");
            decDepositAmt = fnCalculatePTAmount("ADVANCE");



            decBalanceDue = (Convert.ToDecimal(ViewState["OpBalance"]) + decInvoiceAmt - (decPadiAmt + decRejectedAmt));
            BalanceAmt = decBalanceDue.ToString("N2");


        }

        void GetStaffDept()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom1 = new CommonBAL();
            string Criteria = "1=1";
            Criteria += " AND HSFM_STAFF_ID='" + drpDoctor.SelectedValue + "'";
            DS = objCom1.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                lblDrDepartment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]);
            }

        }

        decimal fnCalculatePTAmount(string AmountType)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HPB_PT_ID='" + Convert.ToString(ViewState["PT_ID"]) + "' ";
            switch (AmountType)
            {
                case "INVOICE":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "RETURN":
                    Criteria += " AND HPB_TRANS_TYPE='INV_RTN' ";
                    break;
                case "PAIDAMT":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "RETURNPAIDAMT":
                    Criteria += " AND HPB_TRANS_TYPE='INV_RTN' ";
                    break;
                case "REJECTED":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "ADVANCE":
                    Criteria += " AND HPB_TRANS_TYPE='ADVANCE' ";
                    break;
                case "NPPAID":

                    break;
                default:
                    break;
            }
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.GetPatientBalHisTotal(Criteria);

            decimal Amt = 0;
            if (DS.Tables[0].Rows.Count > 0)
            {
                switch (AmountType)
                {
                    case "INVOICE":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalInvoiceAmout"]);
                        break;
                    case "RETURN":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalInvoiceAmout"]);
                        break;
                    case "PAIDAMT":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "RETURNPAIDAMT":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "REJECTED":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalRejectAmount"]);
                        break;
                    case "ADVANCE":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "NPPAID":

                        break;
                    default:
                        break;
                }
            }

            return Amt;
        }

        public void BindSegmentData(DataSet DSPTDtls)
        {
            string SegmentType = "";
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                SegmentType = Convert.ToString(DR["HLS_TYPE"]);
                Position = Convert.ToString(DR["HLS_POSITION"]);

                Int32 i = 1;
                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["HLS_CODE"]).Trim());

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strColumnName = "";
                    strColumnName = Convert.ToString(DR1["HLSM_DATA"]);


                    string strValueYes = "", _Comment = "";
                    if (DSPTDtls.Tables[0].Rows.Count > 0)
                    {
                        _Comment = Convert.ToString(DSPTDtls.Tables[0].Rows[0][strColumnName]).Trim();
                    }

                    if (Convert.ToString(DR1["HLSM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label txt = new Label();

                        txt = (Label)plhoHomeLeft.FindControl("txt" + Convert.ToString(DR1["HLSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["HLSM_FIELD_ID"]).Trim());


                        txt.Text = _Comment;


                    }

                }
                i = i + 1;
            }
        }

        public void CreateSegCtrls()
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            string SegmentType = "";



            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                SegmentType = Convert.ToString(DR["HLS_TYPE"]);
                Position = Convert.ToString(DR["HLS_POSITION"]);


                //// Panel pnlHeader = new Panel();
                // pnlHeader.ID = "pnl" + Convert.ToString(DR["HLS_CODE"]);
                // pnlHeader.BorderStyle = BorderStyle.Groove;
                // pnlHeader.BorderWidth = 0;
                // //  pnlHeader.Style.Add("display", "none");
                // pnlHeader.Style.Add("width", "99.5%");
                // pnlHeader.Style.Add("border", "thin");
                // pnlHeader.Style.Add("border-color", "#F7F2F2");//F7F2F2
                // pnlHeader.Style.Add("border-style", "groove");
                // // pnlHeader.Style.Add("padding-bottom", " 20px");
                // //pnlHeader.Style.Add("border-radius", "5px");
                // pnlHeader.Style.Add("border-radius", "5px 5px 0px 0px");
                // pnlHeader.Style.Add("heigh", "30px");
                // pnlHeader.Height = 30;
                // Literal lit2 = new Literal();
                // lit2.Text = "&nbsp;";

                Label lblHeader = new Label();
                lblHeader.Text = Convert.ToString(DR["HLS_NAME"]);
                lblHeader.CssClass = "lblCaption1";
                lblHeader.Style.Add("font-family", "Segoe UI,arial");
                lblHeader.Style.Add("font-size", "13px");
                lblHeader.Style.Add("width", "95%");
                // lblHeader.ReadOnly = true;
                lblHeader.BorderStyle = BorderStyle.None;

                ////HtmlAnchor Anc1 = new HtmlAnchor();
                ////Anc1.ID = "anc" + Convert.ToString(DR["HLS_CODE"]);
                ////Anc1.Title = "+";
                ////Anc1.InnerText = "+";
                ////Anc1.Style.Add("color", "#005c7b");
                ////Anc1.Style.Add("font-weight", "bold");
                ////Anc1.Style.Add("font-family", "Arial Black");
                ////Anc1.Style.Add("font-size", "18px");
                ////// Anc1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");
                ////Anc1.Style.Add("width", "5%");


                // lblHeader.Style.Add("color", "#ffffff");
                //// Anc1.Style.Add("color", "#ffffff");
                //  pnlHeader.Style.Add("background-image", "url(../Images/menubar-bg1.PNG)");


                // pnlHeader.Controls.Add(lit2);
                // pnlHeader.Controls.Add(lblHeader);
                //// pnlHeader.Controls.Add(Anc1);

                Panel myFieldSet = new Panel();

                // myFieldSet.GroupingText = Convert.ToString(DR["HLS_NAME"]);
                myFieldSet.ID = "myFieldSet" + Convert.ToString(DR["HLS_CODE"]);
                myFieldSet.Style.Add("text-align", "justify");





                // myFieldSet.Style.Add("display", "none");

                myFieldSet.Style.Add("width", "99.5%");
                //myFieldSet.Style.Add("border", "thin");
                //myFieldSet.Style.Add("border-color", "#0095cd");//F7F2F2//#F7F2F2
                //myFieldSet.Style.Add("border-style", "groove");
                //myFieldSet.Style.Add("padding-bottom", " 20px");
                //myFieldSet.Style.Add("border-radius", "0px 0px 5px 5px");
                //myFieldSet.Style.Add("heigh", "30px");




                myFieldSet.Width = 500;


                // pnlHeader.Attributes.Add("onclick", "DivShow('" + myFieldSet.ID   + "')");
                //    pnlHeader.Attributes.Add("onclick", "DivShow('" + myFieldSet.ID + "')");



                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["HLS_CODE"]));

                Int32 i = 1;


                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {

                    //if (i % 2 == 0)
                    //{
                    //    myfieldset.style.add("background", "#ffffff");
                    //}
                    //else
                    //{
                    //    myfieldset.style.add("background", "#f6f6f6");

                    //}


                    string strComment = "";




                    if (Convert.ToString(DR1["HLSM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();

                        lbl.CssClass = "label";
                        lbl.Style.Add("font-weight", "bold");
                        lbl.Width = 100;
                        
                        lbl.Style.Add("color", "#5c5c5c");

                        lbl.Text = Convert.ToString(DR1["HLSM_FIELD_NAME"]).Trim();// +"&nbsp;&nbsp;&nbsp;";
                        myFieldSet.Controls.Add(lbl);

                        if (lbl.Text.Trim() =="Name")
                        {
                            Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                            myFieldSet.Controls.Add(lit1);
                        }

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["HLSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["HLSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["HLSM_FIELD_NAME"]);
                        txt.CssClass = "lblCaption";
                       // txt.Style.Add("font-weight", "bold");
                        txt.Style.Add("width", "97%");

                        txt.Text = strComment;


                       


                        myFieldSet.Controls.Add(txt);


                    }


                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/><br/>" };
                    myFieldSet.Controls.Add(lit);
                    i = i + 1;
                }

                Literal litGap1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };

                //plhoHomeLeft.Controls.Add(pnlHeader);
                //pnlHeader.Controls.Add(myFieldSet);


                // plhoHomeLeft.Controls.Add(pnlHeader);
                plhoHomeLeft.Controls.Add(myFieldSet);
                //  plhoHomeLeft.Controls.Add(litGap1);








            }
        }

        void GetPatientVisit()
        {

            string Criteria = " 1=1 ";

            // Criteria += " AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPV_PT_ID='" + Convert.ToString(ViewState["PT_ID"]) + "'";

            if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            {
                if (lblDrDepartment.Text != "")
                {
                    Criteria += " AND HPV_DEP_NAME='" + lblDrDepartment.Text + "'";
                }
            }
            //VISIT DATE GRATER THAN 3 YEARS TREAT AS NEW PATIENT
            Criteria += " AND  DATEADD(YY,-3, CONVERT(DATETIME,GETDATE(),101)) <= CONVERT(DATETIME, HPV_DATE,101) ";

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.PatientVisitGet(Criteria);

            lblVisitType.Text = "New";
            if (DS.Tables[0].Rows.Count > 0)
            {
                int intCount = DS.Tables[0].Rows.Count;
                string strLastVisit = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATE"]);

                int intRevisit = Convert.ToInt32(Session["RevisitDays"]);
                if (strLastVisit != "")
                {

                    DateTime dtLastVisit = Convert.ToDateTime(strLastVisit);
                    DateTime Today;
                    if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    {
                        Today = Convert.ToDateTime(hidTodayDBDate.Value);
                    }
                    else
                    {
                        Today = Convert.ToDateTime(Convert.ToString(Session["LocalDate"]));
                    }

                    TimeSpan Diff = Today.Date - dtLastVisit.Date;

                    if (Diff.Days <= intRevisit)
                    {
                        lblVisitType.Text = "Revisit";
                    }
                    else if (Diff.Days >= intRevisit)
                    {
                        lblVisitType.Text = "Old";
                    }

                }
            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='PTREG' ";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["EmiratesIdMandatory"] = "0";
            ViewState["NationalityMandatory"] = "0";
            ViewState["AgeMandatory"] = "0";
            ViewState["DeductibleMandatory"] = "0";
            ViewState["LABEL_PRINT"] = "1";
            ViewState["DATABASE_DATE"] = "1";
            ViewState["DEP_WISE_VISIT_TYPE"] = "0";
            ViewState["PT_SIGNATURE"] = "0";
            ViewState["OTHER_INFO_DISPLAY"] = "0";
            ViewState["PT_COMP_ADD"] = "0";
            ViewState["PT_TITLE_NAME"] = "0";
            ViewState["PLAN_TYPE_STORE_SCAN_CARD"] = "0";
            ViewState["APPOINTMENT_ALERT"] = "0";
            ViewState["CityMandatory"] = "0";
            ViewState["VisaTypeMandatory"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "EMIRATES_ID")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EmiratesIdMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "NATIONALITY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NationalityMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "AGE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["AgeMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DEDUCTIBLE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DeductibleMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "LABEL_PRINT")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["LABEL_PRINT"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DATABASE_DATE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DATABASE_DATE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DEP_WISE_VISIT_TYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEP_WISE_VISIT_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_SIGNATURE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_SIGNATURE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    //if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "SIGNATURE_PAD")
                    //{
                    //    if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                    //    {
                    //        hidSignaturePad.Value = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]).Trim();
                    //    }

                    //}

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "OTHER_INFO_DISPLAY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OTHER_INFO_DISPLAY"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_COMP_ADD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_COMP_ADD"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_TITLE_NAME")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_TITLE_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PLAN_TYPE_STORE_SCAN_CARD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PLAN_TYPE_STORE_SCAN_CARD"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APPOINTMENT_ALERT")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["APPOINTMENT_ALERT"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CITY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CityMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "VISATYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["VisaTypeMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CREDITDTLS_CLEAR")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CREDITDTLS_CLEAR"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }






                }
            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindScreenCustomization();
                    ViewState["PT_ID"] = "";
                    BindDoctor();

                }


            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }

        public void generateDynamicControls()
        {

            CreateSegCtrls();



        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePTDtlsPopup()", true);
            lblVisitType.Text = "";
            ViewState["PT_ID"] = "";
            PatientMasterGet();
            GetAppointment();
            GetInvoiceDtls();
            GetPTVisitDtls();
            BindBalanceHistory();

            //  CreateSegCtrls();

        }

        protected void btnPTRegSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(ViewState["PT_ID"])) == true)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Patient Dtls.','Select Patient Dtls.')", true);

                goto FunEnd;
            }

            if (drpDoctor.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Doctor Name','Select Doctor Name')", true);

                goto FunEnd;
            }
            objCom = new CommonBAL();
            objCom.PT_ID = Convert.ToString(ViewState["PT_ID"]);
            objCom.BranchID = Convert.ToString(Session["BranchID"]);
            objCom.DR_CODE = drpDoctor.SelectedValue;
            objCom.DR_NAME = drpDoctor.SelectedItem.Text;
            objCom.DEP_ID = lblDrDepartment.Text.Trim();
            objCom.VisitType = lblVisitType.Text.Trim();
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.AddToWaiting();
            GetPTVisitDtls();
            BindBalanceHistory();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

        FunEnd: ;
        }

        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetStaffDept();


        }

        protected void PTDtlsEdit_Click(object sender, EventArgs e)
        {

              ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePTDtlsPopup()", true);

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblPTID = (Label)gvScanCard.Cells[0].FindControl("lblPTID");


            txtSearch.Text = lblPTID.Text;
            ViewState["PT_ID"] = lblPTID.Text;
            drpSearchType.SelectedValue = "FileNo";

            PatientMasterGet();
            GetAppointment();
            GetInvoiceDtls();
            GetPTVisitDtls();
            BindBalanceHistory();



        }

        protected void gvPTDtls_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPTDtls.PageIndex = e.NewPageIndex;
            PatientMasterGet();
        }
    }
}