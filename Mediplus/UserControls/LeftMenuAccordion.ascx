﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftMenuAccordion.ascx.cs" Inherits="Mediplus.UserControls.LeftMenuAccordion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
<html lang=''>
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="X-UA-Compatible" content="IE=9" />
    

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />

  
 



    <title>CSS MenuMaker</title>

    <script type="text/javascript">
        var defaultText = "Search";
        function WaterMark(txt, evt) {
            if (txt.value.length == 0 && evt.type == "blur") {
                txt.style.color = "gray";
                txt.value = defaultText;
            }
            if (txt.value == defaultText && evt.type == "focus") {
                txt.style.color = "black";
                txt.value = "";
            }
        }
    </script>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }




        function DivShow(pnlID) {

            if (document.getElementById("ctrlLeftMenu_" + pnlID).style.display == 'none') {
                document.getElementById("ctrlLeftMenu_" + pnlID).style.display = 'block';
            }
            else {

                document.getElementById("ctrlLeftMenu_" + pnlID).style.display = 'none';
            }


        }

        function DivStaticShow(pnlID) {
            if (document.getElementById(pnlID).style.display == 'block') {
                document.getElementById(pnlID).style.display = 'none';
            }
            else {
                document.getElementById(pnlID).style.display = 'block';

            }


        }


        function ShowPTDtlsPopup() {
            document.getElementById("divOverlay").style.display = 'block';
            document.getElementById("divPTDtlsPopup").style.display = 'block';


        }

        function HidePTDtlsPopup() {
            document.getElementById("divOverlay").style.display = 'none';
            document.getElementById("divPTDtlsPopup").style.display = 'none';

        }

       

    </script>

 <script type="text/javascript">
     function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

     function ShowErrorMessage(vMessage1, vMessage2) {

         document.getElementById("divMessage").style.display = 'block';
         
         document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
         document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
    }
 

     $(document).ready(function () {
         $("#nav > li > a").on("click", function (e) {
             if ($(this).parent().has("ul")) {
                 e.preventDefault();
             }

             if (!$(this).hasClass("open")) {
                 // hide any open menus and remove all other classes
                 $("#nav li ul").slideUp(350);
                 $("#nav li a").removeClass("open");

                 // open our new menu and add the open class
                 $(this).next("ul").slideDown(350);
                 $(this).addClass("open");
             } else if ($(this).hasClass("open")) {
                 $(this).removeClass("open");
                 $(this).next("ul").slideUp(350);
             }
         });
         $("#nav > li:first > a").trigger('click');
     });
    </script>

    <style>

    #src{

	    height:30px;
	    float: left;
	    width: 100%;
	    border-radius: 0px 0px 50px 50px;
	    background: #E3E3E3;
	    color: white;
	    display: block;
	    font-size: 18px;
	    font-weight: 500;
	    padding: 0px 5px 5px 5px;
	      -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
    }


        #AccContent
        {
            width: 190px;
            float: left;
            margin: 0px 0px 0px 0px;
        }
         .searchType{
        color:#444;
        border: 0;
        width: 50px;
        height:25px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow:1px 1px 5px rgba(0,0,0,0.65);
        -moz-box-shadow:1px 1px 5px rgba(0,0,0,0.65);
        box-shadow: 1px 1px 5px rgba(0,0,0,0.65);
        outline: 0;
        }

       .searchBtn {
        border: 0;
        position: absolute;
        width: 30px;
        cursor: pointer;
        margin: 2px -30PX;
        border-left: 2px solid skyblue;
        height: 23px;
        font-weight: 900;
        font-size: 12px;
        background-color: #f6f5ef;
        color:#009F8A;
        -webkit-border-radius:0px 5px 5px 0px;
        -moz-border-radius:0px 5px 5px 0px;
        border-radius:0px 5px 5px 0px;
        }
 
    </style>
</head>
<body    style="padding-top:0px;margin-top:0px;border:groove;" >
   
      <div style=" top: 500px; left: 300px;position:fixed;">
                        <div id="myMessage" style="display: none; border: groove; height: 70px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed;">
                             Saved Successfully
                        </div>
      </div>
            <div style="padding-left: 60%; width: 100%;">

               
                <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999;  border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">
               
                      <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                              <span style="float:right;"> 
                      
                                  <input type="button" id="Button1" class="ButtonStyle" style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideErrorMessage()" />

                              </span>
                            <br />
                           <div style="width:100%;height:20px;background:#E3E3E3;">
                                 &nbsp; <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold;color:#02a0e0;font-weight:bold;"></asp:Label>
                           </div>
                                  &nbsp; <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold;color:#666;"></asp:Label>
                            
                    </ContentTemplate>
                                </asp:UpdatePanel>

                    <br />

                </div>
            </div>
   <asp:HiddenField ID="hidTodayDBDate" runat="server" />
    	<section id="AccContent">

   <div id="src">
		      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>      
                        <asp:DropDownList ID="drpSearchType" runat="server"   CssClass="searchType" ForeColor="Brown" >
                            <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                           <asp:ListItem Text="File No" Value="FileNo" >     </asp:ListItem>
                           <asp:ListItem Text="Name" Value="Name">  </asp:ListItem>
                           <asp:ListItem Text="Mobile" Value="Mobile">  </asp:ListItem>
                          <asp:ListItem Text="Emirates ID" Value="EmiratesID">  </asp:ListItem>
                        </asp:DropDownList>
             <asp:TextBox ID="txtSearch" runat="server"  CssClass="searchTerm" Width="120PX"  Text="Search" ForeColor="Gray" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);"  ></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" Text="GO" CssClass="searchBtn"   OnClick="btnSearch_Click" />
	                </ContentTemplate>
            </asp:UpdatePanel>

	</div>
            <br />  
            
    <div id='cssmenu' style="width: 100%;float:left;">
        <ul style="background-color: #FFFFFF;">
            <li id="liptdtls" class='active has-sub'><a href='#'><span>Patient Dtls</span></a>
                <ul >
                     <table style="width: 100%; background-color: #ffffff;">
                            <tr>
                                <td style="vertical-align: top; height: 50px;">
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:PlaceHolder ID="plhoHomeLeft" runat="server"></asp:PlaceHolder>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                     <span class="lblCaption1" style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Visit Type </span>
                           
                          <span class="label"  style="float:left;">
                               <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                    <ContentTemplate>
                                             <asp:Label ID="lblVisitType" CssClass="lblCaption1" runat="server"  ForeColor="Red" Font-Bold="true"></asp:Label>
                                     </ContentTemplate>
                                </asp:UpdatePanel>
                        </span>  <br />
</td>
                                </tr>
                         </table>

                   
                </ul>
            </li>
            <li class='active has-sub'><a href='#'><span>Insurance Dtls</span></a>
                <ul>
                   
                        <table style="width: 100%; background-color: #ffffff;">
                            <tr>
                                <td style="vertical-align: top; height: 50px;">

                                    <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Company </span>
                                    
                                    
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                              <span  class="lblCaption"  style="float:left;"><%=HPM_INS_COMP_NAME %> </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br /> <br />
                                    <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Policy No  </span>
                                                                      
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                             <span class="lblCaption"   style="float:left;"><%=HPM_POLICY_NO %> </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                     <br /><br />

                                     <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Type  </span>
                                    
                                    
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                             <span  class="lblCaption"  style="float:left;"><%=HPM_POLICY_TYPE %> </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                 <br /><br />
                                    <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">ID No  </span>
                                    
                                   
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                             <span  class="lblCaption"  style="float:left;"><%=HPM_ID_NO %> </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                <br /><br />

                                    <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Package  </span>
                                   
                                    
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                        <ContentTemplate>
                                             <span class="lblCaption"   style="float:left;"><%=HPM_PACKAGE_NAME %> </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                <br /><br />

                                    <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Deductible  </span>
                                    
                                  
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                              <span class="lblCaption"   style="float:left;"><%=HPM_DED_AMT %><%=HPM_DED_TYPE %>  </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                <br /><br />

                                     <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Co-Insurance  </span>
                                    
                                    
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <span class="lblCaption"   style="float:left;"><%=HPM_COINS_AMT %> <%=HPM_COINS_TYPE %></span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                            </tr>
                        </table>
                  
                </ul>
            </li>
            <li class='active has-sub'><a href='#'><span>Appointment Dtls</span></a>
                <ul>
                    
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAppointment" runat="server" Width="100%" ShowHeader="false"
                                    AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" GridLines="None">
                                    <HeaderStyle CssClass="GridHeader_Blue" />

                                    <RowStyle CssClass="GridRow" />
                                  
                                    <Columns>

                                        <asp:TemplateField HeaderText="Date&Time" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>

                                                <table style="width: 100%; background-color: #ffffff;">
                                                    <tr>
                                                        <td style="vertical-align: top; height: 50px;">

                                                            <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Date & Time  </span>
                                                            
                                                           <span    style="float:left;">
                                                            <asp:Label ID="lblAppDate" CssClass="lblCaption" runat="server" Text='<%# Bind("AppintmentDate") %>'></asp:Label>&nbsp;&nbsp;
                                                            <asp:Label ID="lblAppTime" CssClass="lblCaption" runat="server" Text='<%# Bind("AppintmentSTTime") %>'></asp:Label>
                                                           </span>
                                                            <br />
                                                            <br />

                                                           <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Doctor   </span>
                                                             
                                                          <span  style="float:left;">
                                                            <asp:Label ID="lblAppDrName" CssClass="lblCaption1" runat="server" Text='<%# Bind("HAM_DR_NAME") %>'></asp:Label>
                                                              </span>
                                                              <br />
                                                            <br />

                                                           <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Status   </span>
                                                            
                                                          <span  style="float:left;">
                                                            <asp:Label ID="lblAppStatus" CssClass="lblCaption1" runat="server" Text='<%# Bind("HAS_STATUS") %>'></asp:Label>
                                                              </span>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </ItemTemplate>

                                        </asp:TemplateField>





                                    </Columns>


                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                   
                    <br /><br />
                </ul>
            </li>

            <li class='active has-sub'><a href='#'><span>Invoice Dtls</span></a>
                <ul>
                   
                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvInvoice" runat="server" Width="100%" ShowHeader="false"
                                    AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" GridLines="None">
                                    <HeaderStyle CssClass="GridHeader_Blue" />

                                    <RowStyle CssClass="GridRow" />
                                   
                                    <Columns>

                                        <asp:TemplateField HeaderText="Date&Time" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>

                                                 <table style="width: 100%; background-color: #ffffff;">
                                                    <tr>
                                                        <td style="vertical-align: top; height: 50px;">

                                                            <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Date    </span>
                                                            
                                                           <span class="lblCaption1"  style="float:left;">
                                                             <asp:Label ID="lblInvDate" CssClass="lblCaption" runat="server" Text='<%# Bind("HIM_DATEDesc") %>'></asp:Label>
                                                               </span>
                                                           <br /> <br />
                                                    

                                                            <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Amount   </span>
                                                            
                                                           <span class="lblCaption1"  style="float:left;">
                                                             <asp:Label ID="lblInvAmt" CssClass="lblCaption" runat="server" Text='<%# Bind("HIM_NET_AMOUNT") %>'></asp:Label>
                                                               </span>
                                                            <br /> <br />
                                                     

                                                            <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Doctor    </span>
                                                            
                                                           <span class="lblCaption1"  style="float:left;">
                                                            <asp:Label ID="lblInvDrName" CssClass="lblCaption" runat="server" Text='<%# Bind("HIM_DR_NAME") %>'></asp:Label>
                                                               </span>
                                                           
                                                        </td>
                                                    </tr>
                                                </table>


                                            </ItemTemplate>

                                        </asp:TemplateField>





                                    </Columns>


                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                      <br /><br />
                </ul>
            </li>
            <li class='active has-sub'><a href='#'><span>Visit Dtls</span></a>
                <ul>
                    
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvPTVisit" runat="server" Width="100%" ShowHeader="false"
                                    AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" GridLines="None">
                                    <HeaderStyle CssClass="GridHeader_Blue" />

                                    <RowStyle CssClass="GridRow" />
                                   
                                    <Columns>

                                        <asp:TemplateField HeaderText="Date&Time" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                  <table style="width: 100%; background-color: #ffffff;">
                                                    <tr>
                                                        <td style="vertical-align: top; height: 50px;width:100%">

                                                            <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;color:#5c5c5c;width:100px;"> Date & Time   </span>
                                                            
                                                             <span class="lblCaption"  style="float:left;">

                                                             <%# Eval("HPV_DateDesc") %> 
                                                               <%# Eval("HPV_DateTimeDesc") %> 
                                                                 </span>
                                                            <br />
                                                         <br />
                                                            <span class="lblCaption1" style="float:left;font-weight:bold;color:#5c5c5c;width:100px;color:#5c5c5c;width:100px;">Doctor  </span>
                                                            
                                                              <span class="lblCaption"  style="float:left;">
                                                                    <%# Eval("HPV_DR_NAME") %> 
                                                                  </span>
                                                               <br />
                                                                 <br />
                                                            <span class="lblCaption1" style="float:left;font-weight:bold;color:#5c5c5c;width:100px;color:#5c5c5c;width:100px;">Type  </span>
                                                            
                                                               <span class="lblCaption"  style="float:left;">
                                                            <%# Eval("HPV_PT_TYPEDesc") %>
                                                                  </span>
                                                        </td>
                                                    </tr>
                                                   
                                                </table>


                                            </ItemTemplate>

                                        </asp:TemplateField>





                                    </Columns>


                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                     
                      <br /><br />
                </ul>
            </li>

            <li class='active has-sub'><a href='#'><span>PT. Balance Dtls.</span></a>
                <ul>
                   
                        <table style="width: 100%; background-color: #ffffff;">
                            <tr>

                                <td  style="vertical-align: top; height: 50px;">


                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                           <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Balance Amount  </span> 
                                           <span class="lblCaption"  style="float:left;"><%=BalanceAmt %> </span>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br /><br />
                                                            <span class="lblCaption1" style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Dept.   </span>
                                                             
                                                              <span class="label"  style="float:left;">
                                                            <asp:Label ID="lblDepartment" CssClass="lblCaption" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                                                  </span>
                                </td>
                            </tr>
                        </table>
                    
                      
                </ul>
            </li>
            <li class='active has-sub' ><a href='#'><span>Quick Registration</span></a>
                <ul>
                    
                    
                        <table style="width: 100%; background-color: #ffffff;text-align:left;float:left;">
                           
                            <tr>
                                <td  style="height: 50px;">
                                      <span class="lblCaption1"  style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Doctor   </span>
                                    <br />
                                   

                                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                        <ContentTemplate>
                                            <span class="lblCaption1"  style="float:left;">
                                            <asp:DropDownList ID="drpDoctor" runat="server" CssClass="lblCaption1" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged" style="border:solid;border-width:thin;"></asp:DropDownList>
                                           </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />  <br />
                                      <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                        <ContentTemplate>
                                                            <span class="lblCaption1" style="float:left;font-weight:bold;color:#5c5c5c;width:100px;">Dept.    &nbsp;&nbsp;</span>
                                                         
                                                              <span class="label"  style="float:left;">
                                                            <asp:Label ID="lblDrDepartment" CssClass="lblCaption" runat="server" ></asp:Label>
                                                                  </span>
                                              <br />  <br />
                                            <asp:Button ID="btnPTRegSave" runat="server" Text="Save" class="orange"  style="width:70px;border-radius:5px 5px 5px 5px; cursor:pointer;"    Width="70px" OnClick="btnPTRegSave_Click" />
                                               

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                            </tr>
                            
                        </table>
                    
                      <br /><br />
                </ul>
            </li>
            <%--   <li class='active has-sub'><a href='#'><span>Products</span></a>
      <ul>
         <li class='has-sub'><a href='#'><span>Product 1</span></a>
            <ul>
               <li><a href='#'><span>Sub Product</span></a></li>
               <li class='last'><a href='#'><span>Sub Product</span></a></li>
            </ul>
         </li>
         <li class='has-sub'><a href='#'><span>Product 2</span></a>
            <ul>
               <li><a href='#'><span>Sub Product</span></a></li>
               <li class='last'><a href='#'><span>Sub Product</span></a></li>
            </ul>
         </li>
      </ul>
   </li>
   <li><a href='#'><span>About</span></a></li>
   <li class='last'><a href='#'><span>Contact</span></a></li>--%>
        </ul>

    </div>
<div id="divOverlay" class="Overlay" style="display:none;">

    <div id="div1"   style="height: 550px; width: 1100px;position: fixed; left: 150px; top: 250px;" > 
        <img src= '<%= ResolveUrl("~/Images/TopMenuIcons/Registration.png")%>'  style="border:none;width: 40px; height: 40px;" />
        <span  style="color:white;font-size:17px;"  class="label">
            Patient
        </span>
    <div id="divPTDtlsPopup" style="display: none; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px;  position: fixed; left: 150px; top: 300px;" >
         <div id="divHeaderLine" style="height: 5px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px;position:absolute; top:0px;left:0px;"  >
            </div>
         <div style="width: 100%; text-align: right; float: right;position:absolute; top:-0px;right:0px;">
               <img src= '<%= ResolveUrl("~/Images/Close.png")%>'   style="height: 25px; width: 25px;cursor:pointer;"   onclick="HidePTDtlsPopup()" />
           </div>      
          <input type="button" class="PageSubHeaderBlue"   style="width:150px;"   value=" Select Patient"   disabled="disabled" />
        <img src= '<%= ResolveUrl("~/Images/TabBlueCornor.png")%>'    style="border:none;height:20px;width:37px;"/>
                    
        <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvPTDtls" runat="server"  AutoGenerateColumns="False" 
                                     EnableModelValidation="True" Width="100%" 
                                      AllowPaging="true" PageSize="10" OnPageIndexChanging="gvPTDtls_PageIndexChanging">
                                    <HeaderStyle CssClass="GridHeader_Gray" />
                                    <RowStyle CssClass="GridRow" />
                                     <AlternatingRowStyle CssClass="GridAlterRow" />
                                     
                                    <Columns>
                                        <asp:TemplateField HeaderText="Action"  HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" >
                                        <ItemTemplate>
                                               <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="PTDtlsEdit_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="File No" HeaderStyle-HorizontalAlign="Left"  >
                                            <ItemTemplate>
                                               
                                                    <asp:Label ID="lblPTID" CssClass="GridRow" style="float:left;" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>
                                                
                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                              
                                                    <span   style="float:left;">
                                                    <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                                    </span>
                                                
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sex" HeaderStyle-HorizontalAlign="Left"  >
                                            <ItemTemplate>
                                                
                                                       <span   style="float:left;">
                                                    <asp:Label ID="lblPTSex" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_SEX") %>'></asp:Label>
                                                           </span>
                                              
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left"  >
                                            <ItemTemplate>
                                                
                                                       <span   style="float:left;">
                                                    <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                                           </span>
                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Pt. Type" HeaderStyle-HorizontalAlign="Left"  >
                                            <ItemTemplate>
                                                
                                                       <span   style="float:left;">
                                                    <asp:Label ID="lblPtType" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                                           </span>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                       
                                              <span   style="float:left;">
                                              <asp:Label ID="lblCompName" CssClass="label" runat="server" Text='<%# Bind("HPM_INS_COMP_NAME") %>'></asp:Label>
                                                    </span>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Policy No." SortExpression="HPM_POLICY_NO">
                                    <ItemTemplate>
                                         
                                              <span   style="float:left;">
                                              <asp:Label ID="lblgvPolicyNo" CssClass="label" runat="server" Text='<%# Bind("HPM_POLICY_NO") %>'></asp:Label>
                                                    </span>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="User" SortExpression="HPM_CREATED_USER">
                                    <ItemTemplate>
                                        
                                              <span   style="float:left;">
                                              <asp:Label ID="lblCrUser" CssClass="label" runat="server" Text='<%# Bind("HPM_CREATED_USER") %>'></asp:Label>
                                                    </span>
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    </Columns>

                                      <PagerStyle CssClass="GridFooterStyle" Font-Bold="true" HorizontalAlign="Right" />
                                </asp:GridView>

                            </ContentTemplate>
                          
                        </asp:UpdatePanel>
                    </td>

                </tr>

            </table>
        </div>

    </div>
    </div>
 </div>
        </section>
</body>
      
                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
 
</html>
