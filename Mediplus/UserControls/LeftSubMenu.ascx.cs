﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;
using System.Text;

namespace Mediplus.UserControls
{
    public partial class LeftSubMenu : System.Web.UI.UserControl
    {
        public string Menu_Header { set; get; }
        public StringBuilder strData = new StringBuilder();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    // papulateMenuTree();


                    TreeNode RootNode = OutputDirectory(Menu_Header, null);
                    // TreeNode treeNode = new TreeNode(Convert.ToString(Session["HPV_DEP_NAME"]));

                    if (RootNode != null)
                    {
                        MyTree.Nodes.Add(RootNode);


                        if (MyTree.Nodes.Count > 0)
                        {
                            MyTree.Nodes[0].Expand();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       LeftMenu.Page_Load");
                TextFileWriting(ex.Message.ToString());

            }
        }


        //void papulateMenuTree()
        //{
        //    //string Criteria = " 1=1 ";
        //    //Criteria += " and DesigID IN (" + Session["DesigId"] + ") AND ( ParentId=" + Session["ParentId"] + " or M.menuid=" + Session["ParentId"] + " )";// + ViewState["EmpId"];


        //    //objRole = new clsRole(strCon);
        //    //DS = new DataSet();
        //    //DS = objRole.GetAllLeftMenu(Criteria);

        //    DataSet DS = new DataSet();
        //    CommonBAL objCom = new CommonBAL();
        //    string Criteria1 = " 1=1 AND  HM_MENU_STATUS=1 AND HM_MENU_TYPE='MENU' AND HM_MENU_FOR ='HMS'  AND HM_MENU_HEADER='"+ Menu_Header +"'";
        //    Criteria1 += " AND  HM_MENU_PARENT_ID = HM_MENU_ID  ";

        //    //Criteria1 += " AND (HM_DEPT_ID  LIKE'%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR HM_DEPT_ID='' OR HM_DEPT_ID IS NULL)";
        //   // Criteria1 += " AND (HM_DEPT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR HM_DEPT_ID_HIDE='' OR HM_DEPT_ID_HIDE IS NULL )";


        //    Int32 i;

        //    //DataTable DT = new DataTable();
        //    //DT = RemoveDuplicateRows(DS.Tables[0], "MenuId");
        //    DS = objCom.fnGetFieldValue(" * ", "HMS_MENUS", Criteria1, "HM_MENU_ORDER");


        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        DS.Relations.Add("NodeRelation1", DS.Tables[0].Columns["HM_MENU_ID"], DS.Tables[0].Columns["HM_MENU_PARENT_ID"]);
        //        foreach (DataRow dbRow in DS.Tables[0].Rows)
        //        {

        //            if (Convert.ToString(dbRow["HM_MENU_ID"]) != Convert.ToString(dbRow["HM_MENU_PARENT_ID"]))
        //            {
        //                // @"../Images/Icons/"+ Convert.ToString(dbRow["HM_MENU_ICON"])
        //                MenuItem L1 = new MenuItem(Convert.ToString(dbRow["HM_MENU_NAME"]), Convert.ToString(dbRow["HM_MENU_ID"]), "", Convert.ToString(dbRow["HM_MENU_URL"]), "_self");
        //               // Menu1.Items.Add(L1);
        //            }



        //        }
        //    }
        //}


        //void papulateMenuTree()
        //{

        //    DataSet DS = new DataSet();
        //    CommonBAL objCom = new CommonBAL();
        //    string Criteria1 = " 1=1 AND  HM_MENU_STATUS=1 AND HM_MENU_TYPE='MENU' AND HM_MENU_FOR ='HMS'  AND HM_MENU_HEADER='" + Menu_Header + "'";
        //    //Criteria1 += " AND (EM_DEPT_ID  LIKE'%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR EM_DEPT_ID='' OR EM_DEPT_ID IS NULL)";
        //    //Criteria1 += " AND (EM_DEPT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR EM_DEPT_ID_HIDE='' OR EM_DEPT_ID_HIDE IS NULL )";


        //    Int32 i;


        //    DS = objCom.fnGetFieldValue(" * ", "HMS_MENUS", Criteria1, "HM_MENU_ORDER");


        //    if (DS.Tables[0].Rows.Count > 0)
        //    {

        //        foreach (DataRow dbRow in DS.Tables[0].Rows)
        //        {

        //            strData.Append("<a  href= '" + Convert.ToString(dbRow["HM_MENU_URL"]) + "'>" + Convert.ToString(dbRow["HM_MENU_NAME"]) + "</a>");



        //            //MenuItem L1 = new MenuItem(Convert.ToString(dbRow["EM_MENU_NAME"]), Convert.ToString(dbRow["EM_MENU_ID"]), "", Convert.ToString(dbRow["EM_MENU_URL"]), "_self");
        //            //Menu1.Items.Add(L1);
        //            ///}
        //            ///


        //        }


        //    }
        //}

        TreeNode OutputDirectory(String strDeptName, TreeNode parentNode)
        {

            // validate param
            if (strDeptName == null) return null;
            // create a node for this directory
            TreeNode DirNode = new TreeNode(strDeptName);




            string Criteria1 = " 1=1 AND  HM_MENU_STATUS=1 AND HM_MENU_TYPE='MENU'  AND HM_MENU_HEADER='" + Menu_Header + "'";
            Criteria1 += "  AND (  HM_DEPT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR HM_DEPT_ID ='' OR HM_DEPT_ID is null ) ";
            Criteria1 += "  AND (  HM_DEPT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR HM_DEPT_ID_HIDE ='' OR HM_DEPT_ID_HIDE is null ) ";

            Criteria1 += " AND HM_MENU_ID != HM_MENU_PARENT_ID ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" * ", "HMS_MENUS", Criteria1, "HM_MENU_ORDER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    TreeNode child = new TreeNode(Convert.ToString(DR["HM_MENU_NAME"]));
                    DirNode.ChildNodes.Add(child);
                    child.ToolTip = Convert.ToString(DR["HM_MENU_URL"]);
                    child.NavigateUrl = Convert.ToString(DR["HM_MENU_URL"]);
                    // child.ImageUrl = @"..\Images\Folder.jpg";

                }
            }

            // otherwise add this node to the parent and return the parent
            if (parentNode == null)
            {
                return DirNode;
            }
            else
            {
                parentNode.ChildNodes.Add(DirNode);
                return parentNode;
            }
        }


        protected void MyTree_SelectedNodeChanged(object sender, EventArgs e)
        {
            string Parent1 = "", Parent2 = "";


            //MyTree.SelectedNode.Parent.Text 
            //MyTree.SelectedNode.Parent.Parent.Text 

            if (MyTree.SelectedNode.ChildNodes.Count == 0)
            {

                //   Parent1 = MyTree.SelectedNode.Parent.Parent.Text;//"03-08-2015"
                //  Parent2 = MyTree.SelectedNode.Parent.Text;//"general"

                //Parent2 = MyTree.SelectedNode.NavigateUrl;
            }


        }


        //private void AddTopMenuItems(DataTable menuData)
        //{
        //    DataView view = null;
        //    try
        //    {
        //        view = new DataView(menuData);
        //        view.RowFilter = "HM_MENU_PARENT_ID = HM_MENU_ID ";
        //        foreach (DataRowView row in view)
        //        {
        //            //Adding the menu item
        //            MenuItem newMenuItem = new MenuItem(row["HM_MENU_NAME"].ToString(), row["HM_MENU_ID"].ToString());
        //            Menu1.Items.Add(newMenuItem);
        //            //Calling the function to add the child menu items
        //            AddChildMenuItems(menuData, newMenuItem); //This function will add child menu items to your menu control.
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Show the error massage here
        //    }
        //    finally
        //    {
        //        view = null;
        //    }
        //}

        //private void AddChildMenuItems(DataTable menuData, MenuItem parentMenuItem)
        //{
        //    DataView view = null;
        //    try
        //    {
        //        view = new DataView(menuData);
        //        view.RowFilter = "HM_MENU_PARENT_ID='" + parentMenuItem.Value +"'";
        //        foreach (DataRowView row in view)
        //        {
        //            MenuItem newMenuItem = new MenuItem(row["HM_MENU_NAME"].ToString(), row["HM_MENU_ID"].ToString());
        //            newMenuItem.NavigateUrl = row["HM_MENU_URL"].ToString();
        //            parentMenuItem.ChildItems.Add(newMenuItem);
        //            // This code is used to recursively add child menu items filtering by ParentID
        //            AddChildMenuItems(menuData, newMenuItem);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Show the error massage here
        //    }
        //    finally
        //    {
        //        view = null;
        //    }
        //}
    }
}