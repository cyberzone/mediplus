﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Accounts
{
    public partial class PDCPosting : System.Web.UI.Page
    {
        PaymentEntryBAL objPay = new PaymentEntryBAL();
        ReceiptEntryBAL objRec = new ReceiptEntryBAL();


        OPDPostingBAL objOPD = new OPDPostingBAL();


        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HBM_BANKNAME", "HMS_BANK_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "HBM_BANKNAME";
                drpBank.DataValueField = "HBM_BANKNAME";
                drpBank.DataBind();
            }
            drpBank.Items.Insert(0, "--- Select ---");
            drpBank.Items[0].Value = "0";
        }

        void BindPaymentEntryGrid()
        {
            objPay = new PaymentEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND APE_TYPE='PDC PAYABLE' AND   (APE_PDC_POSTING IS NULL OR   APE_PDC_POSTING ='')  ";
            Criteria += " AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),APE_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),APE_DATE,101),101) <= '" + strForToDate + "'";
            }



            DS = objPay.PaymentEntryGet(Criteria);
            gvPayment.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPayment.Visible = true;
                gvPayment.DataSource = DS;
                gvPayment.DataBind();

            }
            else
            {
                gvPayment.DataBind();
            }


        }

        void BindReceiptEntryGrid()
        {
            objRec = new ReceiptEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ARE_TYPE='PDC RECEIVABLE' AND (ARE_PDC_POSTING IS NULL OR   ARE_PDC_POSTING ='') ";
            Criteria += " AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),ARE_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),ARE_DATE,101),101) <= '" + strForToDate + "'";
            }
            DS = objRec.ReceiptEntryGet(Criteria);
            gvReceipt.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvReceipt.DataSource = DS;
                gvReceipt.DataBind();
                gvReceipt.Visible = true;
            }
            else
            {
                gvReceipt.DataBind();
            }


        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                string PageName = Convert.ToString(Request.QueryString["PageName"]);
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                BindBank();
                //BindPaymentEntryGrid();


            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 intCount = 0;
                decimal decTotalAmount = 0;


                if (drpBank.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Select the Bank Name','Red')", true);
                    goto FunEnd;
                }


                if (drpType.SelectedValue == "PDCP")
                {

                    foreach (GridViewRow row in gvPayment.Rows)
                    {
                        CheckBox chkPayment = (CheckBox)row.FindControl("chkPayment");

                        Label lblTransNo = (Label)row.FindControl("lblTransNo");
                        Label lblAmount = (Label)row.FindControl("lblAmount");

                        decimal decAmount = 0;

                        if (chkPayment.Checked == true)
                        {
                            objCom = new CommonBAL();

                            string Criteria = " APE_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'   AND APE_TRANS_NO='" + lblTransNo.Text + "'";

                            objCom.fnUpdateTableData(" APE_PDC_POSTING = 1 ", "AC_PAYMENT_ENTRY", Criteria);


                            if (lblAmount.Text.Trim() != "")
                            {
                                decAmount = Convert.ToDecimal(lblAmount.Text.Trim());
                            }
                            decTotalAmount = decTotalAmount + decAmount;

                            intCount = intCount + 1;
                        }

                    }
                }
                else if (drpType.SelectedValue == "PDCR")
                {

                    foreach (GridViewRow row in gvReceipt.Rows)
                    {
                        CheckBox chkReceipt = (CheckBox)row.FindControl("chkReceipt");

                        Label lblReTransNo = (Label)row.FindControl("lblReTransNo");
                        Label lblReAmount = (Label)row.FindControl("lblReAmount");

                        decimal decAmount = 0;

                        if (chkReceipt.Checked == true)
                        {
                            objCom = new CommonBAL();

                            string Criteria = " ARE_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'   AND ARE_TRANS_NO='" + lblReTransNo.Text + "'";

                            objCom.fnUpdateTableData(" ARE_PDC_POSTING = 1 ", "AC_RECEIPT_ENTRY", Criteria);


                            if (lblReAmount.Text.Trim() != "")
                            {
                                decAmount = Convert.ToDecimal(lblReAmount.Text.Trim());
                            }
                            decTotalAmount = decTotalAmount + decAmount;

                            intCount = intCount + 1;
                        }

                    }
                }


                objOPD = new OPDPostingBAL();

                objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                objOPD.APM_TYPE = drpType.SelectedValue;

                objOPD.APM_FROM_DATE = txtFromDate.Text.Trim();
                objOPD.APM_TO_DATE = txtToDate.Text.Trim();
                objOPD.APM_BANK = drpBank.SelectedValue;
                objOPD.APM_COUNT = Convert.ToString(intCount);
                objOPD.APM_AMOUNT = Convert.ToString(decTotalAmount);
                objOPD.UserID = Convert.ToString(Session["User_ID"]);
                objOPD.PDCPostingMasterAdd();


                if (drpType.SelectedValue == "PDCP")
                {
                    BindPaymentEntryGrid();
                }
                else if (drpType.SelectedValue == "PDCR")
                {
                    BindReceiptEntryGrid();
                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('   Saved Successfully','Green')", true);

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void imgPaymentZoom_Click(object sender, ImageClickEventArgs e)
        {
            gvReceipt.Visible = false;
            gvPayment.Visible = false;
            gvPayment.DataBind();
            gvReceipt.DataBind();

            if (drpType.SelectedValue == "PDCP")
            {
                BindPaymentEntryGrid();
               
            }
            else if (drpType.SelectedValue == "PDCR")
            {
                BindReceiptEntryGrid();
            }
        }

        protected void chkPaySelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvPayment.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvPayment.Rows)
                {
                    CheckBox chkPayment = (CheckBox)row.FindControl("chkPayment");
                    if (ChkBoxHeader.Checked == true)
                    {
                        chkPayment.Checked = true;
                    }
                    else
                    {
                        chkPayment.Checked = false;
                    }
                }

                //  CountClaimAmount();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void chkRecSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvReceipt.HeaderRow.FindControl("chkRecSelectAll");
                foreach (GridViewRow row in gvReceipt.Rows)
                {
                    CheckBox chkReceipt = (CheckBox)row.FindControl("chkReceipt");
                    if (ChkBoxHeader.Checked == true)
                    {
                        chkReceipt.Checked = true;
                    }
                    else
                    {
                        chkReceipt.Checked = false;
                    }
                }

                //  CountClaimAmount();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}