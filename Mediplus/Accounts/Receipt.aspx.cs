﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Accounts
{
    public partial class Receipt : System.Web.UI.Page
    {
        Boolean HMS_ENABLE_ADVANCE;
        dboperations dbo = new dboperations();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime12HrsFromDB()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");


            drpSTHour.DataSource = DSHours;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();




            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpSTMin.DataSource = DSMin;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();




        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HBM_BANKNAME", "HMS_BANK_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "HBM_BANKNAME";
                drpBank.DataValueField = "HBM_BANKNAME";
                drpBank.DataBind();
            }

            drpBank.Items.Insert(0, "--- Select ---");
            drpBank.Items[0].Value = "";
        }


        void BIndCCType()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            clsInvoice objInv = new clsInvoice();
            DS = objInv.CCTypeGet(Criteria);
            drpccType.Items.Clear();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpccType.DataSource = DS;
                drpccType.DataTextField = "HCT_CC_TYPE";
                drpccType.DataValueField = "HCT_CC_TYPE";
                drpccType.DataBind();
            }
            drpccType.Items.Insert(0, "--- Select ---");
            drpccType.Items[0].Value = "";


        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            ViewState["HSOM_ENABLE_ADV_PAYMENT"] = "N";

            // ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]


            if (DS.Tables[0].Rows.Count > 0)
            {



                if (DS.Tables[0].Rows[0].IsNull("HSOM_ENABLE_ADV_PAYMENT") == false)
                {
                    ViewState["HSOM_ENABLE_ADV_PAYMENT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ENABLE_ADV_PAYMENT"]);

                }




                if (DS.Tables[0].Rows[0].IsNull("HSOM_ADV_PAYMENT_SERV_CODE") == false)
                {
                    ViewState["HSOM_ADV_PAYMENT_SERV_CODE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ADV_PAYMENT_SERV_CODE"]);

                }






            }


        }

        void fnSetdefaults()
        {


            HMS_ENABLE_ADVANCE = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "Y" ? true : false;
            drpPayType.Items.Insert(0, "Cash");
            drpPayType.Items[0].Value = "Cash";

            drpPayType.Items.Insert(1, "Credit Card");
            drpPayType.Items[1].Value = "Credit Card";

            drpPayType.Items.Insert(2, "Cheque");
            drpPayType.Items[2].Value = "Cheque";

            drpPayType.Items.Insert(3, "PDC");
            drpPayType.Items[3].Value = "PDC";

            drpPayType.Items.Insert(4, "Other");
            drpPayType.Items[4].Value = "Other";

            drpPayType.Items.Insert(5, "Debit Card");
            drpPayType.Items[5].Value = "Debit Card";

            if (HMS_ENABLE_ADVANCE == true && Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]) != "")
            {

                drpPayType.Items.Insert(6, "Advance");
                drpPayType.Items[6].Value = "Advance";

            }

            if (GlobalValues.BLNALLOWADVANCEPAYMENT_INVOICE == true)
            {

                drpPayType.Items.Insert(7, "Utilise Advance");
                drpPayType.Items[7].Value = "Utilise Advance";
            }

            ViewState["NewFlag"] = true;



            BIndCCType();





        }

        void BindPTDetails()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtPTName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();
            }
        }

        void BindPTBalance()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1";

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria = " HPB_PT_ID='" + txtFileNo.Text.Trim() + "'";
            }


            DS = objCom.fnGetFieldValue(" (sum(HPB_INV_AMOUNT) - sum(HPB_PAID_AMT )  ) as PrevBalance", "HMS_PATIENT_BALANCE_HISTORY", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {


                if (DS.Tables[0].Rows[0].IsNull("PrevBalance") == false && Convert.ToString(DS.Tables[0].Rows[0]["PrevBalance"]) != "")
                {
                    decimal decPrevBalance = Convert.ToDecimal(DS.Tables[0].Rows[0]["PrevBalance"]);
                    txtPrevBalance.Text = decPrevBalance.ToString("#0.00");
                }
            }
        }

        void BindReceipt(String SearchOption)
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1";

            if (txtReceiptNo.Text.Trim() != "" && SearchOption == "ReceiptNo")
            {
                Criteria = " HRM_RECEIPTNO='" + txtReceiptNo.Text.Trim() + "'";
            }


            if (txtFileNo.Text.Trim() != "" && SearchOption == "PTFileNo")
            {
                Criteria = " HRM_PT_ID='" + txtFileNo.Text.Trim() + "'";
            }


            DS = objCom.fnGetFieldValue("*,convert(varchar,HRM_DATE,103) as HRM_DATEDesc,convert(varchar,HRM_DATE,108) as HRM_DATETime", "HMS_RECEIPT_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;

                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRM_PT_ID"]);
                txtPTName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRM_PT_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HRM_DATEDesc") == false)
                {
                    txtReceiptDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRM_DATEDesc"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HRM_DATETime") == false)
                {
                    string strTimeUp = Convert.ToString(DS.Tables[0].Rows[0]["HRM_DATETime"]);

                    string[] arrTimeUp1 = strTimeUp.Split(':');

                    if (arrTimeUp1.Length > 1)
                    {
                        string strHour = arrTimeUp1[0];
                        if (strHour.Length == 1)
                        {
                            strHour = "0" + strHour;
                        }

                        drpSTHour.SelectedValue = strHour;
                        drpSTMin.SelectedValue = arrTimeUp1[1];
                    }

                }


                drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HRM_STATUS"]);
                txtDescription.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRM_DESCRIPTION"]);
                txtRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRM_REF_ID"]);
                txtDescription.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRM_DESCRIPTION"]);

                drpPayType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HRM_PAYMENT_TYPE"]);

            }

        }

        void BindInvoiceDtlsGrid()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1";
            Criteria += " AND HIM_PT_ID = '" + txtFileNo.Text.Trim() + "' AND    HIM_NET_AMOUNT > HIM_PAID_AMOUNT ";

            DataSet DSInv = new DataSet();
            DSInv = objCom.fnGetFieldValue("HIM_INVOICE_ID as  InvoiceID, CONVERT(VARCHAR,HIM_DATE,103)AS TransDate ,CONVERT(VARCHAR,HIM_DATE,108)  as TransTime  "
                        + " ,CAST(HIM_NET_AMOUNT as numeric(12, 2))  as  InvoiceAmt,CAST(HIM_PAID_AMOUNT as numeric(12, 2))  PaidAmt,'0.00' CurPay", "HMS_INVOICE_MASTER", Criteria, "");
            if (DSInv.Tables[0].Rows.Count > 0)
            {
                gvInvoiceDtls.Visible = true;
                gvInvoiceDtls.DataSource = DSInv;
                gvInvoiceDtls.DataBind();
            }
            else
            {
                gvInvoiceDtls.Visible = false;
                gvInvoiceDtls.DataBind();
            }
        }

        void BindReceiptDtlsGrid()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1";

            //if (txtReceiptNo.Text.Trim() != "")
            //{
            //    Criteria = " HRM_RECEIPTNO='" + txtReceiptNo.Text.Trim() + "'";
            //}
            //if (txtFileNo.Text.Trim() != "")
            //{
            //    Criteria = " HRT_RECEIPTNO IN ( select HRM_RECEIPTNO from  HMS_RECEIPT_MASTER where  HRM_PT_ID = '" + txtFileNo.Text.Trim() + "')";
            //}
            //DS = objCom.fnGetFieldValue("*,convert(varchar,HRT_DATE,103) as HRT_DATEDesc,convert(varchar,HRT_DATE,108) as HRT_DATETime", "HMS_RECEIPT_TRANSACTION", Criteria, "HRT_RECEIPTNO");

            if (txtReceiptNo.Text.Trim() != "")
            {
                Criteria += " AND  HRT_RECEIPTNO = '" + txtReceiptNo.Text.Trim() + "'";
            }





            DataSet DSInv = new DataSet();
            DSInv = objCom.fnGetFieldValue("HRT_INVOICE_ID as  InvoiceID, CONVERT(VARCHAR,HRT_DATE,103)AS TransDate ,CONVERT(VARCHAR,HRT_DATE,108)  as TransTime  "
                        + " , CAST(HRT_NET_AMOUNT as numeric(12, 2))  as  InvoiceAmt,'0.00' as  PaidAmt,CAST(HRT_PAID_AMOUNT as numeric(12, 2)) as CurPay", "HMS_RECEIPT_TRANSACTION", Criteria, "");
            if (DSInv.Tables[0].Rows.Count > 0)
            {
                gvInvoiceDtls.Visible = true;
                gvInvoiceDtls.DataSource = DSInv;
                gvInvoiceDtls.DataBind();
            }
            else
            {
                gvInvoiceDtls.Visible = false;
                gvInvoiceDtls.DataBind();
            }
        }



        void Clear()
        {
            drpStatus.SelectedIndex = 0;

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtReceiptDate.Text = strFromDate.ToString("dd/MM/yyyy");

            string strHour = Convert.ToString(System.DateTime.Now.Hour);
            if (Convert.ToInt32(strHour) <= 9)
            {
                strHour = "0" + strHour;
            }
            string strMin = Convert.ToString(System.DateTime.Now.Minute);
            if (Convert.ToInt32(strMin) <= 9)
            {
                strMin = "0" + strMin;
            }

            drpSTHour.SelectedValue = strHour;
            drpSTMin.SelectedValue = strMin;

            txtPTName.Text = "";
            txtDescription.Text = "";
            txtRefNo.Text = "";
            txtPrevBalance.Text = "";
            txtCurPay.Text = "0";
            txtCurBalance.Text = "";
            drpPayType.SelectedIndex = 0;
            drpBank.SelectedIndex = 0;
            drpccType.SelectedIndex = 0;

            divCC.Visible = false;
            divCheque.Visible = false;
            ViewState["NewFlag"] = true;

            gvInvoiceDtls.DataBind();
            gvInvoiceDtls.Visible = false;


        }

        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            try
            {

                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtReceiptDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    string strHour = Convert.ToString(System.DateTime.Now.Hour);
                    if (Convert.ToInt32(strHour) <= 9)
                    {
                        strHour = "0" + strHour;
                    }
                    string strMin = Convert.ToString(System.DateTime.Now.Minute);
                    if (Convert.ToInt32(strMin) <= 9)
                    {
                        strMin = "0" + strMin;
                    }


                    BindTime12HrsFromDB();
                    BindSystemOption();
                    fnSetdefaults();
                    BindBank();
                    drpSTHour.SelectedValue = strHour;
                    drpSTMin.SelectedValue = strMin;

                    txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divCC.Visible = false;
                divCheque.Visible = false;

                if (drpPayType.SelectedItem.Text == "Multi Currency")
                {

                }
                else if (drpPayType.SelectedItem.Text == "Credit Card")
                {

                    divCC.Visible = true;
                    BIndCCType();
                    if (txtCCAmt.Text.Trim() == "0" || txtCCAmt.Text.Trim() == "0.00" || txtCCAmt.Text.Trim() == "0.000")
                    {

                        // txtCCAmt.Text = txtPaidAmount.Text;

                    }
                }
                else if (drpPayType.SelectedItem.Text == "Cheque" || drpPayType.SelectedItem.Text == "PDC")
                {

                    divCheque.Visible = true;
                    if (txtChqAmt.Text.Trim() == "0" || txtChqAmt.Text.Trim() == "0.00" || txtChqAmt.Text.Trim() == "0.000")
                    {

                        //  txtChqAmt.Text = txtPaidAmount.Text;

                    }

                }
                else if (drpPayType.SelectedItem.Text == "Other")
                {
                    divCC.Visible = false;
                    divCheque.Visible = false;


                }
                //else if (drpPayType.SelectedItem.Text == "Debit Card")
                //{
                //  //  txtCashAmt.Text = "0.00";
                //    drpccType.Items.Clear();
                //    divCC.Visible = true;


                //    drpccType.Items.Insert(0, "Salary Card");
                //    drpccType.Items[0].Value = "Salary Card";

                //    drpccType.Items.Insert(1, "Knet");
                //    drpccType.Items[1].Value = "Knet";

                //    if (txtCCAmt.Text.Trim() == "0" || txtCCAmt.Text.Trim() == "0.00" || txtCCAmt.Text.Trim() == "0.000")
                //    {
                //        //txtCCAmt.Text = txtPaidAmount.Text;

                //    }
                //}

                else
                {

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpPayType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtFileNo.Text = "";
            Clear();
            txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtReceiptNo.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                if (gvInvoiceDtls.Rows.Count <= 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS Empty.',' Receipt Details is Empty.','Red')", true);
                    goto FunEnd;
                }


                Decimal decTotalAmtReceived = 0, decAmtReceived = 0, decCurentPay = 0, decCurentPayUpdated = 0;

                if (txtCurPay.Text.Trim() != "")
                {
                    decCurentPay = Convert.ToDecimal(txtCurPay.Text.Trim());
                    decCurentPayUpdated = Convert.ToDecimal(txtCurPay.Text.Trim());
                }

                foreach (GridViewRow DR in gvInvoiceDtls.Rows)
                {
                    Label lblgvReceiptDtlsInvoiceNo = (Label)DR.FindControl("lblgvReceiptDtlsInvoiceNo");
                    Label lblgvReceiptDtlsAmount = (Label)DR.FindControl("lblgvReceiptDtlsAmount");
                    Label lblgvReceiptDtlsPaid = (Label)DR.FindControl("lblgvReceiptDtlsPaid");
                    TextBox lblgvReceiptDtlsCurPay = (TextBox)DR.FindControl("lblgvReceiptDtlsCurPay");

                    if (lblgvReceiptDtlsCurPay.Text != "")
                    {
                        decAmtReceived = Convert.ToDecimal(lblgvReceiptDtlsCurPay.Text);
                        decTotalAmtReceived += decAmtReceived;
                    }
                }

                clsReceipt objRec = new clsReceipt();
                objRec.HRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objRec.HRM_RECEIPTNO = txtReceiptNo.Text.Trim();
                objRec.HRM_STATUS = drpStatus.SelectedValue;
                objRec.HRM_DATE = txtReceiptDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
                objRec.HRM_RCPT_TYPE = "Patient";
                objRec.HRM_PT_ID = txtFileNo.Text.Trim();
                objRec.HRM_PT_NAME = txtPTName.Text.Trim();
                objRec.HRM_PT_TYPE = "";
                objRec.HRM_COMP_ID = "";
                objRec.HRM_COMP_NAME = "";
                objRec.HRM_COMP_TYPE = "";
                objRec.HRM_DR_ID = "";
                objRec.HRM_DR_NAME = "";
                objRec.HRM_REF_ID = "";
                objRec.HRM_REF_NAME = "";
                objRec.HRM_DESCRIPTION = txtDescription.Text;
                if (decTotalAmtReceived > 0)
                {
                    objRec.HRM_AMT_RCVD = Convert.ToString(decTotalAmtReceived);
                }
                else
                {
                    objRec.HRM_AMT_RCVD = txtCurPay.Text.Trim();
                }

                objRec.HRM_PAYMENT_TYPE = drpPayType.SelectedValue;
                objRec.HRM_CCNO = txtCCNo.Text.Trim();
                objRec.HRM_CCNAME = drpccType.SelectedValue;
                objRec.HRM_HOLDERNAME = txtCCHolder.Text.Trim();
                objRec.HRM_CCREFNO = txtCCRefNo.Text.Trim();
                objRec.HRM_CHEQUENO = txtCCNo.Text.Trim();
                objRec.HRM_BANKNAME = drpBank.SelectedValue;
                objRec.HRM_CHEQUEDATE = txtDcheqdate.Text.Trim();
                objRec.HRM_CASH_AMT = "";
                objRec.HRM_CC_AMT = txtCCAmt.Text.Trim();
                objRec.HRM_CHEQUE_AMT = txtChqAmt.Text.Trim();
                objRec.HRM_CUR_TYP = "";
                objRec.HRM_CUR_RATE = "";
                objRec.HRM_CUR_VALUE = "";
                objRec.HRM_CUR_RCVD_AMT = "";

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    objRec.NewFlag = "A";
                }
                else
                {
                    objRec.NewFlag = "M";
                }


                objRec.UserID = Convert.ToString(Session["User_ID"]);

                string srtReceptNo = "";
                srtReceptNo = objRec.ReceiptMasterAdd();
                // hidReceipt.Value = srtReceptNo;
                txtReceiptNo.Text = srtReceptNo;

                CommonBAL objCom = new CommonBAL();
                string Criteria = "";
              
                //objCom.fnDeleteTableData("HMS_RECEIPT_TRANSACTION", Criteria);

                foreach (GridViewRow DR in gvInvoiceDtls.Rows)
                {

                    Decimal ReceiptPrevPaid = 0, ReceiptCurPaid = 0;
                    Label lblgvReceiptDtlsInvoiceNo = (Label)DR.FindControl("lblgvReceiptDtlsInvoiceNo");
                    Label lblgvReceiptDtlsAmount = (Label)DR.FindControl("lblgvReceiptDtlsAmount");
                    Label lblgvReceiptDtlsPaid = (Label)DR.FindControl("lblgvReceiptDtlsPaid");
                    Label lblgvReceiptDtlsPrevPaid = (Label)DR.FindControl("lblgvReceiptDtlsPrevPaid");

                    TextBox lblgvReceiptDtlsCurPay = (TextBox)DR.FindControl("lblgvReceiptDtlsCurPay");

                    Criteria = " HRT_RECEIPTNO='" + srtReceptNo + "'  AND HRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


                    if (lblgvReceiptDtlsCurPay.Text != "")
                    {
                        ReceiptCurPaid = Convert.ToDecimal(lblgvReceiptDtlsCurPay.Text.Trim());
                    }


                    if (decCurentPayUpdated > 0)
                    {
                        Decimal decAmtTobePaid = Convert.ToDecimal(lblgvReceiptDtlsAmount.Text) - Convert.ToDecimal(lblgvReceiptDtlsPaid.Text);

                        if (decCurentPayUpdated > decAmtTobePaid)
                        {
                            ReceiptCurPaid = decAmtTobePaid;

                        }
                        else
                        {
                            ReceiptCurPaid = decCurentPayUpdated;
                        }

                        decCurentPayUpdated = decCurentPayUpdated - ReceiptCurPaid;
                    }
                    if (lblgvReceiptDtlsPrevPaid.Text != "")
                    {
                        ReceiptPrevPaid = Convert.ToDecimal(lblgvReceiptDtlsPrevPaid.Text);
                    }


                    if (ReceiptCurPaid != null)
                    {
                        if (Convert.ToBoolean(ViewState["NewFlag"]) == true && ReceiptCurPaid != 0)
                        {
                            objRec.HRT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                            objRec.HRT_RECEIPTNO = srtReceptNo;
                            objRec.HRT_DATE = txtReceiptDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
                            objRec.HRT_INVOICE_ID = lblgvReceiptDtlsInvoiceNo.Text.Trim();
                            objRec.HRT_NET_AMOUNT = lblgvReceiptDtlsAmount.Text.Trim();
                            objRec.HRT_PAID_AMOUNT = Convert.ToString(ReceiptCurPaid);
                            objRec.HRT_DISCOUNT = "";
                            objRec.ReceiptTransAdd();
                        }
                        else
                        {
                            objCom.fnUpdateTableData("HRT_PAID_AMOUNT=" + ReceiptCurPaid, "HMS_RECEIPT_TRANSACTION", Criteria);
                        }

                        clsInvoice objInv = new clsInvoice();
                        Decimal PrevPTReject = 0, PrevInvPaid = 0, InvTotalPaid = 0, PrevInvPTCredit = 0, InvTotalPTCredit=0;  
                        string InvoiceType = "Cash";
                        string InvCriteria = "  HIM_INVOICE_ID='" + lblgvReceiptDtlsInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom = new CommonBAL();
                        DataSet DSInv = new DataSet();
                        DSInv = objCom.fnGetFieldValue("TOP 1 *", "HMS_INVOICE_MASTER", InvCriteria, "");
                        if (DSInv.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_REJECTED"]) != "")
                            {
                                PrevPTReject = DSInv.Tables[0].Rows[0].IsNull("HIM_PT_REJECTED") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PT_REJECTED"]);
                            }

                            if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]) != "")
                            {
                                InvoiceType = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]);
                            }
                            if (DSInv.Tables[0].Rows[0].IsNull("HIM_PAID_AMOUNT") == false && Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]) != "")
                            {
                                PrevInvPaid = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]);
                            }

                            if (DSInv.Tables[0].Rows[0].IsNull("HIM_PT_CREDIT") == false && Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT"]) != "")
                            {
                                PrevInvPTCredit = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT"]);
                            }


                            if (ReceiptCurPaid != null)
                            {
                                InvTotalPaid = (PrevInvPaid - ReceiptPrevPaid) + ReceiptCurPaid;

                                InvTotalPTCredit = (PrevInvPTCredit + ReceiptPrevPaid) - ReceiptCurPaid;
                            }

                            objCom.fnUpdateTableData("HIM_PAID_AMOUNT=" + InvTotalPaid + ", HIM_PT_CREDIT =  " + InvTotalPTCredit, "HMS_INVOICE_MASTER", InvCriteria);
                        }

                        Criteria = "  HPB_TRANS_ID='" + lblgvReceiptDtlsInvoiceNo.Text.Trim() + "' AND HPB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom = new CommonBAL();
                        DataSet DSPTBal = new DataSet();
                        DSPTBal = objCom.fnGetFieldValue("TOP 1 *", "HMS_PATIENT_BALANCE_HISTORY", Criteria, "");

                        if (DSPTBal.Tables[0].Rows.Count < 0)
                        {
                            objInv.HPB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                            objInv.HPB_TRANS_ID = txtReceiptDate.Text.Trim();
                            objInv.HPB_DATE = txtReceiptDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
                            objInv.HPB_PT_ID = txtFileNo.Text.Trim();
                            objInv.HPB_PATIENT_TYPE = InvoiceType;
                            objInv.HPB_INV_AMOUNT = lblgvReceiptDtlsAmount.Text.Trim();
                            objInv.HPB_PAID_AMT = Convert.ToString(ReceiptCurPaid);
                            objInv.HPB_REJECTED_AMT = Convert.ToString(PrevPTReject);
                            objInv.HPB_BALANCE_AMT = Convert.ToString((Convert.ToDecimal(lblgvReceiptDtlsAmount.Text.Trim()) - ReceiptCurPaid) - PrevPTReject);
                            objInv.UserID = Convert.ToString(Session["User_ID"]);
                            objInv.PatientBalHisAdd();
                        }
                        else
                        {
                            Decimal PrevPaid = 0, PTBalTotalPaid = 0, PTBalPreCredit=0 , PTBalTotalCredit=0;

                            if (DSPTBal.Tables[0].Rows[0].IsNull("HPB_PAID_AMT") == false && Convert.ToString(DSPTBal.Tables[0].Rows[0]["HPB_PAID_AMT"]) != "")
                            {
                                PrevPaid = Convert.ToDecimal(DSPTBal.Tables[0].Rows[0]["HPB_PAID_AMT"]);
                            }

                            if (DSPTBal.Tables[0].Rows[0].IsNull("HPB_BALANCE_AMT") == false && Convert.ToString(DSPTBal.Tables[0].Rows[0]["HPB_BALANCE_AMT"]) != "")
                            {
                                PTBalPreCredit = Convert.ToDecimal(DSPTBal.Tables[0].Rows[0]["HPB_BALANCE_AMT"]);
                            }

                            if (ReceiptCurPaid != null)
                            {
                                PTBalTotalPaid = (PrevPaid - ReceiptPrevPaid) + ReceiptCurPaid;
                            }

                            PTBalTotalCredit = (PTBalPreCredit + ReceiptPrevPaid) - ReceiptCurPaid;

                            string FieldNameWithValues = "HPB_PAID_AMT=" + PTBalTotalPaid + ", HPB_BALANCE_AMT = " + PTBalTotalCredit;
                            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_BALANCE_HISTORY", Criteria);
                        }


                    }

                }
                Clear();
                txtFileNo.Text = "";
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS SAVED.',' Receipt Details Saved.','Green')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            Clear();

            BindPTDetails();
            BindPTBalance();
            if (txtFileNo.Text.Trim() != "")
            {
                BindInvoiceDtlsGrid();
            }
        }


        protected void txtReceiptNo_TextChanged(object sender, EventArgs e)
        {
            Clear();
            BindReceipt("ReceiptNo");
            BindReceiptDtlsGrid();
        }

        protected void ReceiptDtlsEdit_Click(object sender, EventArgs e)
        {
            //ClearMicrobiologyDtls();

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvMicrobiologySelectIndex"] = gvScanCard.RowIndex;
            Label lblgvMicroDtlsOrganismTypeCode = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsOrganismTypeCode");
            Label lblgvMicroDtlsAntibioticsTypeCode = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsTypeCode");

            Label lblgvMicroDtlsAntibioticsMethod = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsMethod");
            Label lblgvMicroDtlsAntibioticsUnit = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsUnit");
            Label lblgvMicroDtlsAntibioticsValue = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsValue");
            Label lblgvMicroDtlAntibioticsAbnormalFlag = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlAntibioticsAbnormalFlag");



        }


        protected void txtCurPay_TextChanged(object sender, EventArgs e)
        {
            Decimal decCurPay = 0, prevBalance = 0, decCurBalance = 0;
            if (txtCurPay.Text.Trim() != "")
            {
                decCurPay = Convert.ToDecimal(txtCurPay.Text.Trim());
            }

            if (txtPrevBalance.Text.Trim() != "")
            {
                prevBalance = Convert.ToDecimal(txtPrevBalance.Text.Trim());
            }


            if (txtCurPay.Text.Trim() != "" && txtPrevBalance.Text.Trim() != "")
            {
                decCurBalance = prevBalance - decCurPay;
            }

            txtCurBalance.Text = decCurBalance.ToString("#0.00");
        }

        protected void gvInvoiceDtls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblgvReceiptDtlsInvoiceNo = (Label)e.Row.Cells[0].FindControl("lblgvReceiptDtlsInvoiceNo");
                    TextBox lblgvReceiptDtlsCurPay = (TextBox)e.Row.Cells[0].FindControl("lblgvReceiptDtlsCurPay");


                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {

                    }

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.gvInvTran_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + Convert.ToString(Session["Branch_ID"]) + "','" + txtReceiptNo.Text.Trim() + "' );", true);

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {
                string Criteria = "";
                CommonBAL objCom = new CommonBAL();

                foreach (GridViewRow DR in gvInvoiceDtls.Rows)
                {
                    Decimal ReceiptPrevPaid = 0, ReceiptCurPaid = 0;
                    Label lblgvReceiptDtlsInvoiceNo = (Label)DR.FindControl("lblgvReceiptDtlsInvoiceNo");
                    Label lblgvReceiptDtlsAmount = (Label)DR.FindControl("lblgvReceiptDtlsAmount");
                    Label lblgvReceiptDtlsPaid = (Label)DR.FindControl("lblgvReceiptDtlsPaid");
                    Label lblgvReceiptDtlsPrevPaid = (Label)DR.FindControl("lblgvReceiptDtlsPrevPaid");

                    if (lblgvReceiptDtlsPrevPaid.Text != "")
                    {
                        ReceiptPrevPaid = Convert.ToDecimal(lblgvReceiptDtlsPrevPaid.Text);
                    }


                    string InvCriteria = "  HIM_INVOICE_ID='" + lblgvReceiptDtlsInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    objCom.fnUpdateTableData("HIM_PAID_AMOUNT = HIM_PAID_AMOUNT - " + ReceiptPrevPaid + ", HIM_PT_CREDIT = HIM_PT_CREDIT + " + ReceiptPrevPaid, "HMS_INVOICE_MASTER", InvCriteria);


                    Criteria = "  HPB_TRANS_ID='" + lblgvReceiptDtlsInvoiceNo.Text.Trim() + "' AND HPB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    string FieldNameWithValues = "HPB_PAID_AMT = HPB_PAID_AMT - " + ReceiptPrevPaid + ", HPB_BALANCE_AMT = HPB_BALANCE_AMT + "  + ReceiptPrevPaid ;
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_BALANCE_HISTORY", Criteria);

                }

                Criteria = "1=1 AND HRT_RECEIPTNO='" + txtReceiptNo.Text.Trim() + "'  AND HRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                objCom.fnDeleteTableData("HMS_RECEIPT_TRANSACTION", Criteria);

                Criteria = "1=1 AND HRM_RECEIPTNO='" + txtReceiptNo.Text.Trim() + "' AND  HRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                objCom.fnDeleteTableData("HMS_RECEIPT_MASTER", Criteria);

                Clear();
                txtFileNo.Text = "";
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' RECEIPT DELETED.',' Receipt Details Deleted.','Green')", true);



            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}