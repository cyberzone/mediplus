﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;



namespace Mediplus.Accounts
{
    public partial class AccountMaster : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objMast = new MastersBAL();
            DS = objMast.AccCategorytMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "ACM_CATEGORY";
                drpCategory.DataValueField = "ACM_CATEGORY";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "0";


        }

        void BindAccountMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objMast = new MastersBAL();
            DS = objMast.AccountMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvAccountMaster.DataSource = DS;

                gvAccountMaster.DataBind();
            }
            else
            {
                gvAccountMaster.DataBind();
            }


        }

        void Clear()
        {
            txtAccountName.Text = "";

            if (drpCategory.Items.Count > 0)
                drpCategory.SelectedIndex = 0;

            drpCategory.Enabled = true;

            txtYTD.Text = "";
            txtBalance.Text = "";


            ViewState["AccountCode"] = "";
            ViewState["ProgNotesSelectIndex"] = "";

            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvAccountMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            lblStatus.Text = "";
            if (!IsPostBack)
            {
                ViewState["AccountCode"] = "";
                BindCategory();
                BindAccountMaster();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtAccountName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Account Name";
                    goto FunEnd;
                }

                if (drpCategory.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please select Category";
                    goto FunEnd;
                }
                DataSet DS = new DataSet();
                string Criteria = " 1=1 AND AAM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND AAM_CATEGORY ='"+  drpCategory.SelectedValue +"'";
                Criteria += " AND AAM_ACCOUNT_NAME='" + txtAccountName.Text.Trim() + "'" ;


                if (Convert.ToString(ViewState["AccountCode"]) != "" && Convert.ToString(ViewState["AccountCode"]) != null)
                {
                    Criteria += " AND  AAM_CODE != '" + Convert.ToString(ViewState["AccountCode"]) + "'";
                }

                MastersBAL objMast = new MastersBAL();
                DS = objMast.AccountMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Same Account Name Already Exists";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                objMast.BranchID = Convert.ToString(Session["Branch_ID"]);
                objMast.AAM_CODE = Convert.ToString(ViewState["AccountCode"]);

                objMast.AAM_ACCOUNT_NAME = txtAccountName.Text.Trim();
                objMast.AAM_CATEGORY = drpCategory.SelectedValue;
                objMast.AAM_STATUS = "A";
                objMast.UserID = Convert.ToString(Session["User_ID"]);
                objMast.AccountMasterAdd();

                Clear();
                BindAccountMaster();
                lblStatus.Text = "Saved Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvAccountMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvAccountMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblAccountCode = (Label)gvScanCard.Cells[0].FindControl("lblAccountCode");
                Label lblAccountName = (Label)gvScanCard.Cells[0].FindControl("lblAccountName");

                Label lblCategory = (Label)gvScanCard.Cells[0].FindControl("lblCategory");

                txtAccountName.Text = lblAccountName.Text;
                drpCategory.SelectedValue = lblCategory.Text;
                drpCategory.Enabled = true;
                ViewState["AccountCode"] = lblAccountCode.Text;


                PaymentEntryBAL objPay = new PaymentEntryBAL();
                DataSet DSPay = new DataSet();
                string Criteria1 = " 1=1 ";
                Criteria1 += " AND  APE_ACC_CODE='" + lblAccountCode.Text + "' AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSPay = objPay.PaymentEntryGet(Criteria1);

                if (DSPay.Tables[0].Rows.Count > 0)
                {
                    drpCategory.Enabled = false;
                }

                ReceiptEntryBAL objRec = new ReceiptEntryBAL();
                DataSet DSRec = new DataSet();

                string Criteria2 = " 1=1 ";
                Criteria2 += " AND  ARE_ACC_CODE='" + lblAccountCode.Text.Trim() + "' AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSRec = objRec.ReceiptEntryGet(Criteria2);

                if (DSRec.Tables[0].Rows.Count > 0)
                {

                    drpCategory.Enabled = false;

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void imgACMasRefres_Click(object sender, ImageClickEventArgs e)
        {
            BindAccountMaster();
        }

        protected void btnCtlrRefresh_Click(object sender, ImageClickEventArgs e)
        {
            Clear();
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblAccountCode = (Label)gvScanCard.Cells[0].FindControl("lblAccountCode");
                eAuthorizationBAL eAuth = new eAuthorizationBAL();

               
                DataSet DS = new DataSet();
                string Criteria = " 1=1 ";
                Criteria += " AND  AJT_ACC_CODE='" + lblAccountCode.Text + "' AND AJT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

               JournalEntryBAL   objJour = new JournalEntryBAL();
                DS = objJour.JournalTransactionGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This Account Code is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

               


                CommonBAL objCom = new CommonBAL();
                string Criteria1 = " AAM_CODE='" + lblAccountCode.Text + "'";
                objCom.fnDeleteTableData("AC_ACCOUNT_MASTER", Criteria1);



                lblStatus.Text = " Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindAccountMaster();


            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
             Clear();
        }
        #endregion
    }
}