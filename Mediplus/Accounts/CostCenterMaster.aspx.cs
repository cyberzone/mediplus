﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Accounts
{
    public partial class CostCenterMaster : System.Web.UI.Page
    {

        MastersBAL objMast = new MastersBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCCMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objMast = new MastersBAL();
            DS = objMast.CostCenterMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvCCMaster.DataSource = DS;

                gvCCMaster.DataBind();
            }
            else
            {
                gvCCMaster.DataBind();
            }


        }

        void Clear()
        {
            txtCostCenterNo.Text = "";
            txtCostCenterName.Text = "";

            ViewState["ProgNotesSelectIndex"] = "";

            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvCCMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


                if (!IsPostBack)
                {


                    BindCCMaster();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtCostCenterNo.Text.Trim() == "")
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowCostCentMsg(' Cost Center Number','Please Enter Cost Center Number')", true);
                    lblStatus.Text = "Please Enter Cost Center Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                if (txtCostCenterName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Cost Center Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DataSet DS = new DataSet();
                string Criteria = " 1=1 AND ACCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
                Criteria += " AND ACCM_CC_NAME='" + txtCostCenterName.Text.Trim() + "'";


                if (txtCostCenterNo.Text.Trim() != ""  )
                {
                    Criteria += " AND  ACCM_CODE != '" + txtCostCenterNo.Text.Trim() + "'";
                }

                MastersBAL objMast = new MastersBAL();
                DS = objMast.CostCenterMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Same Cost Center Already Exists";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }




                objMast.BranchID = Convert.ToString(Session["Branch_ID"]);
                objMast.ACCM_CODE = txtCostCenterNo.Text.Trim();

                objMast.ACCM_CC_NAME = txtCostCenterName.Text.Trim();
                objMast.ACCM_STATUS = "A";
                objMast.UserID = Convert.ToString(Session["User_ID"]);
                objMast.CostCenterMasterAdd();

                Clear();
                BindCCMaster();
 
                lblStatus.Text = "Saved Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvCCMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvCCMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblCCCode = (Label)gvScanCard.Cells[0].FindControl("lblCCCode");
                Label lblCCName = (Label)gvScanCard.Cells[0].FindControl("lblCCName");

                txtCostCenterNo.Text = lblCCCode.Text;

                txtCostCenterName.Text = lblCCName.Text;



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void imgACMasRefres_Click(object sender, ImageClickEventArgs e)
        {
            BindCCMaster();
        }

        protected void btnCtlrRefresh_Click(object sender, ImageClickEventArgs e)
        {
            Clear();
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                 ImageButton btnEdit = new ImageButton();
                 btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;




                Label lblCCCode = (Label)gvScanCard.Cells[0].FindControl("lblCCCode");
                eAuthorizationBAL eAuth = new eAuthorizationBAL();


                PaymentEntryBAL objPay = new PaymentEntryBAL();
                DataSet DSPay = new DataSet();
                string Criteria1 = " 1=1 ";
                Criteria1 += " AND  APE_COST_CENT_CODE='" + lblCCCode.Text + "' AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSPay = objPay.PaymentExpensesGet(Criteria1);

                if (DSPay.Tables[0].Rows.Count > 0)
                {
                    
                    lblStatus.Text = "This Cost Center is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                ReceiptEntryBAL objRec = new ReceiptEntryBAL();
                DataSet DSRec = new DataSet();

                string Criteria2 = " 1=1 ";
                Criteria2 += " AND  ARE_COST_CENT_CODE='" + lblCCCode.Text.Trim() + "' AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSRec = objRec.ReceiptExpensesGet(Criteria2);

                if (DSRec.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This Cost Center is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                string Criteria = " ACCM_CODE='" + lblCCCode.Text + "'";
                objCom.fnDeleteTableData("AC_COST_CENTER_MASTER", Criteria);



                lblStatus.Text = " Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindCCMaster();


            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            
            Clear();
        }
        #endregion
    }
}