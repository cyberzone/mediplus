﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;



namespace Mediplus.Accounts
{
    public partial class PaymentEntry : System.Web.UI.Page
    {
        public static string strCategory;

        JournalEntryBAL objJour = new JournalEntryBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BIndCCType()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            clsInvoice objInv = new clsInvoice();
            DS = objInv.CCTypeGet(Criteria);
            drpCCType.Items.Clear();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCCType.DataSource = DS;
                drpCCType.DataTextField = "HCT_CC_TYPE";
                drpCCType.DataValueField = "HCT_CC_TYPE";
                drpCCType.DataBind();
            }

            drpCCType.Items.Insert(0, "--- Select ---");
            drpCCType.Items[0].Value = "";

        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            //string Criteria = " 1=1  AND APB_STATUS='A' ";


            //objCom = new CommonBAL();
            //DS = objCom.fnGetFieldValue("APB_BANK_NAME", "AC_PAYMENT_BANK ", Criteria, "");

            string Criteria = " 1=1  AND AAM_STATUS='A' AND  AAM_CATEGORY = 'BANK' ";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccountMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "AAM_ACCOUNT_NAME";
                drpBank.DataValueField = "AAM_CODE";
                drpBank.DataBind();


                drpChequeBank.DataSource = DS;
                drpChequeBank.DataTextField = "AAM_ACCOUNT_NAME";
                drpChequeBank.DataValueField = "AAM_CODE";
                drpChequeBank.DataBind();

            }
            drpBank.Items.Insert(0, "--- Select ---");
            drpBank.Items[0].Value = "";

            drpChequeBank.Items.Insert(0, "--- Select ---");
            drpChequeBank.Items[0].Value = "";

        }


        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND ACM_STATUS='A'  ";

            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccCategorytMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "ACM_CATEGORY";
                drpCategory.DataValueField = "ACM_CATEGORY";
                drpCategory.DataBind();



            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "";





        }

        void BindAccountName()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";

            Criteria += " AND  AAM_CATEGORY = '" + drpCategory.SelectedValue + "'";

            drpAccountName.Items.Clear();
            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccountMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpAccountName.DataSource = DS;
                drpAccountName.DataTextField = "AAM_ACCOUNT_NAME";
                drpAccountName.DataValueField = "AAM_CODE";
                drpAccountName.DataBind();



            }

            drpAccountName.Items.Insert(0, "--- Select ---");
            drpAccountName.Items[0].Value = "";






        }

        void BindCostCenter()
        {
            DataSet DS = new DataSet();

            drpCostCenter.Items.Clear();

            string Criteria = " 1=1 ";
            MastersBAL objMast = new MastersBAL();
            DS = objMast.CostCenterMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCostCenter.DataSource = DS;
                drpCostCenter.DataTextField = "ACCM_CC_NAME";
                drpCostCenter.DataValueField = "ACCM_CODE";
                drpCostCenter.DataBind();


                drpDrCostCenter.DataSource = DS;
                drpDrCostCenter.DataTextField = "ACCM_CC_NAME";
                drpDrCostCenter.DataValueField = "ACCM_CODE";
                drpDrCostCenter.DataBind();


            }

            drpCostCenter.Items.Insert(0, "--- Select ---");
            drpCostCenter.Items[0].Value = "";

            drpDrCostCenter.Items.Insert(0, "--- Select ---");
            drpDrCostCenter.Items[0].Value = "";



        }

        void BindDrCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND ACM_STATUS='A' AND ACM_CATEGORY IN ('CASH','BANK','CC','PDC PAYABLE')";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccCategorytMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                drpDrCategory.DataSource = DS;
                drpDrCategory.DataTextField = "ACM_CATEGORY";
                drpDrCategory.DataValueField = "ACM_CATEGORY";
                drpDrCategory.DataBind();

            }


            drpDrCategory.Items.Insert(0, "--- Select ---");
            drpDrCategory.Items[0].Value = "";



        }

        void BindDrAccountName()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";

            Criteria += " AND  AAM_CATEGORY = '" + drpDrCategory.SelectedValue + "'";

            drpDrAccountName.Items.Clear();
            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccountMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                drpDrAccountName.DataSource = DS;
                drpDrAccountName.DataTextField = "AAM_ACCOUNT_NAME";
                drpDrAccountName.DataValueField = "AAM_CODE";
                drpDrAccountName.DataBind();



            }



            drpDrAccountName.Items.Insert(0, "--- Select ---");
            drpDrAccountName.Items[0].Value = "";





        }

        void BindJournalMaster()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND AJM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND AJM_JOURNAL_NO='" + txtJournalNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objJour = new JournalEntryBAL();
            DS = objJour.JournalMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SaveMode"] = "M";

                //// txtJournalNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_JOURNAL_NO"]);
                txtTransDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_DATEDesc"]);
                txtRef.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_REF_NO"]);
                drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJM_STATUS"]);
                txtDescription.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_DESCRIPTION"]);


                drpPayType.Value = Convert.ToString(DS.Tables[0].Rows[0]["AJM_PAY_TYPE"]);
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "DisplayPayDtls();", true);

                txtChequeNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CHEQUE_NO"]);
                txtChequeAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CHEQUE_AMOUNT"]);
                txtChequeDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CHEQUE_DATEDesc"]);
                drpChequeBank.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CHEQUE_BANK_NAME"]);

                drpCCType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CC_TYPE"]);
                txtCCNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CC_NO"]);
                txtCCHolder.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CC_HOLDER"]);
                txtCCAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CC_AMOUNT"]);
                txtCCRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_CC_REF_NO"]);

                drpBank.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJM_BANK_NAME"]);
                txtBankPayDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_BANK_PAY_DATEDesc"]);
                drpBank.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJM_BANK_NAME"]);
                drpBankRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_BANK_REF_NO"]);
                txtBankPayAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJM_BANK_AMOUNT"]);

            }

        }


        void BindDrJournalTrans()
        {
            string Criteria = " 1=1 AND AJT_CREDIT_AMOUNT >0 ";
            Criteria += " AND AJT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND AJT_JOURNAL_NO='" + txtJournalNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objJour = new JournalEntryBAL();
            DS = objJour.JournalTransactionGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtDrAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["AJT_CREDIT_AMOUNT"]);
                drpDrCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJT_CATEGORY"]);
                BindDrAccountName();

                drpDrAccountName.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJT_ACC_CODE"]);
                drpDrCostCenter.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["AJT_COST_CENT_CODE"]);


            }

        }

        void BuildJournalTrans()
        {
            string Criteria = "1=2";
            DataTable dt = new DataTable();
            DataSet DS = new DataSet();
            objJour = new JournalEntryBAL();
            DS = objJour.JournalTransactionGet(Criteria);

            ViewState["JournalTransaction"] = DS.Tables[0];
        }

        void BindJournalTrans()
        {
            string Criteria = " 1=1 AND AJT_DEBIT_AMOUNT >0 ";
            Criteria += " AND AJT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND AJT_JOURNAL_NO='" + txtJournalNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objJour = new JournalEntryBAL();
            DS = objJour.JournalTransactionGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["JournalTransaction"] = DS.Tables[0];

            }

        }

        void BindTempJournalTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["JournalTransaction"];

            gvJournalTrans.Visible = false;
            if (DT.Rows.Count > 0)
            {
                gvJournalTrans.Visible = true;
                gvJournalTrans.DataSource = DT;
                gvJournalTrans.DataBind();

            }
            else
            {
                gvJournalTrans.DataBind();
            }

        }

        void BindJournalMasterGrid()
        {
            string Criteria = " 1=1 and AJM_ENTRY_FROM='PAYMENT ENTRY' ";
            Criteria += " AND AJM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            //  Criteria += " AND AJT_JOURNAL_NO='" + txtJournalNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objJour = new JournalEntryBAL();


            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }



            if (txtSrcFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),AJM_DATE,101),101) >= '" + strForStartDate + "'";
            }




            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtSrcToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),AJM_DATE,101),101) <= '" + strForToDate + "'";
            }


            DS = objJour.JournalMasterGet(Criteria);
            gvJournalMaster.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvJournalMaster.DataSource = DS;
                gvJournalMaster.DataBind();
                gvJournalMaster.Visible = true;
            }
            else
            {
                gvJournalMaster.DataBind();
            }


        }

        Boolean   SaveJournal()
        {
            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["JournalTransaction"];

            if (DT.Rows.Count <= 0)
            {
                lblStatus.Text = "Transaction is empty";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return false;

            }

            TextBox txtTotalDRAmount = (TextBox)gvJournalTrans.FooterRow.FindControl("txtTotalDRAmount");
         //   TextBox txtTotalCRAmount = (TextBox)gvJournalTrans.FooterRow.FindControl("txtTotalCRAmount");
            ////if (txtTotalDRAmount.Text != txtDrAmount.Text.Trim())
            ////{
            ////    lblStatus.Text = "Total Amount is not Equal to Transaction Amount";
            ////    lblStatus.ForeColor = System.Drawing.Color.Red ;
            ////    return false;
            ////}
          

            objJour = new JournalEntryBAL();


            objJour.BranchID = Convert.ToString(Session["Branch_ID"]);
            objJour.AJM_JOURNAL_NO = txtJournalNo.Text.Trim();
            objJour.AJM_DATE = txtTransDate.Text.Trim();
            objJour.AJM_STATUS = drpStatus.SelectedValue;
            objJour.AJM_DESCRIPTION = txtDescription.Text;
            objJour.AJM_REF_NO = txtRef.Text.Trim();
            objJour.AJM_DR_TOTAL_AMOUNT = txtTotalDRAmount.Text;
            objJour.AJM_CR_TOTAL_AMOUNT = txtDrAmount.Text;
            objJour.AJM_TOTAL_AMOUNT = "0";
            objJour.AJM_DESCRIPTION = txtDescription.Text.Trim();


            objJour.AJM_ENTRY_FROM = "PAYMENT ENTRY";
            objJour.SCRN_ID = "AC_PAYMENT";

            objJour.AJM_PAY_TYPE = drpPayType.Value;

            objJour.AJM_CHEQUE_NO = txtChequeNo.Text.Trim();
            objJour.AJM_CHEQUE_AMOUNT = txtChequeAmt.Text.Trim();
            objJour.AJM_CHEQUE_DATE = txtChequeDate.Text.Trim();
            objJour.AJM_CHEQUE_BANK_NAME = drpChequeBank.Text.Trim();

            objJour.AJM_CC_TYPE = drpCCType.SelectedValue;
            objJour.AJM_CC_NO = txtCCNo.Text.Trim();
            objJour.AJM_CC_HOLDER = txtCCHolder.Text.Trim();
            objJour.AJM_CC_AMOUNT = txtCCAmt.Text.Trim();
            objJour.AJM_CC_REF_NO = txtCCRefNo.Text.Trim();

            objJour.AJM_BANK_NAME = drpBank.SelectedValue;
            objJour.AJM_BANK_PAY_DATE = txtBankPayDate.Text.Trim();
            objJour.AJM_BANK_NAME = drpBank.SelectedValue;
            objJour.AJM_BANK_REF_NO = drpBankRefNo.Text.Trim();
            objJour.AJM_BANK_AMOUNT = txtBankPayAmount.Text.Trim();

            //          objJour.AJM_SUPPLIER_CODE		 
            //          objJour.AJM_SUPPLIER_NAME		 
            //          objJour.AJM_CUSTOMER_CODE		 
            //          objJour.AJM_CUSTOMER_NAME		 

            objJour.MODE = Convert.ToString(ViewState["SaveMode"]);
            objJour.UserID = Convert.ToString(Session["User_ID"]);

            string strJournalNO = "0";
            strJournalNO = objJour.JournalMasterAdd();

            txtJournalNo.Text = strJournalNO;

          
            if (DT.Rows.Count > 0)
            {
                CommonBAL objCom = new CommonBAL();

                string Criteria = " 1=1 ";


                Criteria += " AND AJT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  AJT_JOURNAL_NO ='" + txtJournalNo.Text.Trim() + "'";

                objCom.fnDeleteTableData("AC_JOURNAL_TRANSACTION", Criteria);

                objJour = new JournalEntryBAL();
                objJour.BranchID = Convert.ToString(Session["Branch_ID"]);
                objJour.AJT_JOURNAL_NO = txtJournalNo.Text.Trim();
                objJour.AJT_TRANS_ID = "0";
                objJour.AJT_CATEGORY = drpDrCategory.SelectedValue;
                objJour.AJT_ACC_CODE = drpDrAccountName.SelectedValue;
                objJour.AJT_ACC_NAME = drpDrAccountName.SelectedItem.Text;
                objJour.AJT_COST_CENT_CODE = drpDrCostCenter.SelectedValue;
                objJour.AJT_COST_CENT_NAME = drpDrCostCenter.SelectedItem.Text;
                objJour.AJT_DEBIT_AMOUNT = "0";
                objJour.AJT_CREDIT_AMOUNT = txtDrAmount.Text.Trim();
                objJour.AJT_DESCRIPTION = txtDescription.Text;
                objJour.JournalTransactionAdd();


                foreach (DataRow DR in DT.Rows)
                {
                    objJour = new JournalEntryBAL();
                    objJour.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objJour.AJT_JOURNAL_NO = txtJournalNo.Text.Trim();
                    objJour.AJT_TRANS_ID = Convert.ToString(DR["AJT_TRANS_ID"]);
                    objJour.AJT_CATEGORY = Convert.ToString(DR["AJT_CATEGORY"]);
                    objJour.AJT_ACC_CODE = Convert.ToString(DR["AJT_ACC_CODE"]);
                    objJour.AJT_ACC_NAME = Convert.ToString(DR["AJT_ACC_NAME"]);
                    objJour.AJT_COST_CENT_CODE = Convert.ToString(DR["AJT_COST_CENT_CODE"]);
                    objJour.AJT_COST_CENT_NAME = Convert.ToString(DR["AJT_COST_CENT_NAME"]);
                    objJour.AJT_DEBIT_AMOUNT = Convert.ToString(DR["AJT_DEBIT_AMOUNT"]);
                    objJour.AJT_CREDIT_AMOUNT = Convert.ToString(DR["AJT_CREDIT_AMOUNT"]);
                    objJour.AJT_DESCRIPTION = Convert.ToString(DR["AJT_DESCRIPTION"]);
                    objJour.JournalTransactionAdd();


                }

            }

            return true;
        }

        void Clear()
        {

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtRef.Text = "";
            drpStatus.SelectedIndex = 0;

            if (drpDrCategory.Items.Count > 0)
                drpDrCategory.SelectedIndex = 0;

            if (drpDrAccountName.Items.Count > 0)
                drpDrAccountName.SelectedIndex = 0;

            if (drpDrCostCenter.Items.Count > 0)
                drpDrCostCenter.SelectedIndex = 0;

            txtDescription.Text = "";
            txtCrAmount.Text = "";



            drpPayType.SelectedIndex = 0;

            txtChequeNo.Text = "";
            txtChequeAmt.Text = "";
            txtChequeDate.Text = "";
            drpChequeBank.Text = "";

            if (drpChequeBank.Items.Count > 0)
                drpChequeBank.SelectedIndex = 0;

            drpCCType.SelectedIndex = 0;
            txtCCNo.Text = "";
            txtCCHolder.Text = "";
            txtCCAmt.Text = "";
            txtCCRefNo.Text = "";

            drpBank.SelectedIndex = 0;
            txtBankPayDate.Text = "";
             drpBankRefNo.Text = "";
            txtBankPayAmount.Text = "";





            ViewState["SaveMode"] = "A";

            BuildJournalTrans();
            gvJournalTrans.DataBind();
            gvJournalTrans.Visible = false;


        }

        void ClearJournalTrans()
        {

            if (drpCategory.Items.Count > 0)
                drpCategory.SelectedIndex = 0;

            if (drpAccountName.Items.Count > 0)
                drpAccountName.SelectedIndex = 0;

            //if (drpCostCenter.Items.Count > 0)
            //    drpCostCenter.SelectedIndex = 0;


            txtCrAmount.Text = "";

        }

        void AmountCalculation()
        {

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["JournalTransaction"];
            if (DT.Rows.Count > 0)
            {

                decimal decDrAmt = 0, decCrAmt = 0, decTotalAmt = 0; ;
                foreach (DataRow DR in DT.Rows)
                {



                    if (Convert.ToString(DR["AJT_DEBIT_AMOUNT"]) != "")
                    {
                        decDrAmt += Convert.ToDecimal(DR["AJT_DEBIT_AMOUNT"]);

                    }
                    else
                    {
                        decDrAmt += 0;
                    }



                    if (Convert.ToString(DR["AJT_CREDIT_AMOUNT"]) != "")
                    {
                        decCrAmt += Convert.ToDecimal(DR["AJT_CREDIT_AMOUNT"]);

                    }
                    else
                    {
                        decCrAmt += 0;
                    }

                    //decTotalAmt += Math.Abs(decDrAmt - decCrAmt) ;
                }
                decTotalAmt = Math.Abs(decDrAmt - decCrAmt);
                 TextBox txtTotalDRAmount = (TextBox)gvJournalTrans.FooterRow.FindControl("txtTotalDRAmount");
               // TextBox txtTotalCRAmount = (TextBox)gvJournalTrans.FooterRow.FindControl("txtTotalCRAmount");

                txtTotalDRAmount.Text = decDrAmt.ToString("#0.00");
              //  txtTotalCRAmount.Text = decCrAmt.ToString("#0.00");

            }
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetExpenseList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";




            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCostCenterList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  ACCM_CC_NAME like '%" + prefixText + "%'";
            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.CostCenterMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["ACCM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["ACCM_CC_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            try
            {

                if (!IsPostBack)
                {
                    ViewState["SaveMode"] = "A";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtSrcFromDate.Text = strFromDate.AddDays(-7).ToString("dd/MM/yyyy");
                    txtSrcToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindBank();
                    BIndCCType();
                    BindCategory();
                    BindAccountName();
                    BindCostCenter();
                    BindDrCategory();
                    BindDrAccountName();
                    BindJournalMasterGrid();
                    CommonBAL objCom = new CommonBAL();
                    txtJournalNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
                    //BindUnitType();
                    //BindStore();
                    BuildJournalTrans();
                    BindJournalTrans();
                    //BindTempJournalTrans();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveJournal() == false)
            {
                goto FunEnd;
            }
            Clear();
            lblStatus.Text = "";
            lblStatus.Text = "Data  Saved";
            lblStatus.ForeColor = System.Drawing.Color.Green;
            CommonBAL objCom = new CommonBAL();
            txtJournalNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");

        FunEnd: ;
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    //  gvJournal.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                //  gvJournal.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvJournal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblgvTransType = (Label)e.Row.FindControl("lblgvTransType");

                TextBox lblgvAmountDr = (TextBox)e.Row.FindControl("lblgvAmountDr");
                TextBox lblgvAmountCr = (TextBox)e.Row.FindControl("lblgvAmountCr");

                if (lblgvTransType.Text == "Dr")
                {
                    lblgvAmountCr.Visible = false;
                    lblgvAmountCr.Text = "";
                }
                else
                {
                    lblgvAmountDr.Visible = false;
                    lblgvAmountDr.Text = "";
                }

            }
        }

        protected void btnJournalRefresh_Click(object sender, ImageClickEventArgs e)
        {
            BuildJournalTrans();
            BindJournalTrans();
        }

        protected void JournalSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblJournalNo = (Label)gvScanCard.Cells[0].FindControl("lblJournalNo");

                txtJournalNo.Text = lblJournalNo.Text;
                txtJournalNo_TextChanged(txtJournalNo, new EventArgs());

                divJournalGridPopup.Visible = false;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PaymentSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void imgPaymentZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindJournalMasterGrid();
            divJournalGridPopup.Visible = true;
        }

        protected void txtJournalNo_TextChanged(object sender, EventArgs e)
        {
            Clear();

            BindJournalMaster();
            BindDrJournalTrans();

            BindJournalTrans();
            BindTempJournalTrans();
            AmountCalculation();


        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();

            CommonBAL objCom = new CommonBAL();
            txtJournalNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
        }

        protected void btnAddTrans_Click(object sender, ImageClickEventArgs e)
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["JournalTransaction"];


            DataRow objrow;
            objrow = DT.NewRow();


            objrow["AJT_BRANCH_ID"] = Convert.ToString(Session["Branch_ID"]);
            objrow["AJT_JOURNAL_NO"] = txtJournalNo.Text.Trim();
            objrow["AJT_TRANS_ID"] = 0;
            objrow["AJT_CATEGORY"] = drpCategory.SelectedValue;
            objrow["AJT_ACC_CODE"] = drpAccountName.SelectedValue;
            objrow["AJT_ACC_NAME"] = drpAccountName.SelectedItem.Text;
            objrow["AJT_COST_CENT_CODE"] = drpCostCenter.SelectedValue;
            objrow["AJT_COST_CENT_NAME"] = drpCostCenter.SelectedItem.Text;
            objrow["AJT_DEBIT_AMOUNT"] = txtCrAmount.Text.Trim();
            objrow["AJT_CREDIT_AMOUNT"] = 0.000;
            objrow["AJT_DESCRIPTION"] = txtDescription.Text.Trim();

            //chk.Checked == true ? "1" : "0";



            ViewState["JournalTransaction"] = DT;
            DT.Rows.Add(objrow);
            BindTempJournalTrans();
            AmountCalculation();
            ClearJournalTrans();
        }

        protected void DeleteJournalTrans_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["JournalTransaction"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["JournalTransaction"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["JournalTransaction"] = DT;

                }



                BindTempJournalTrans();
                AmountCalculation();
                ClearJournalTrans();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindJournalMasterGrid();
        }

        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAccountName();
        }

        protected void drpDrCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDrAccountName();
        }



    }
}