﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;
namespace Mediplus.Accounts
{
    public partial class ReceiptEntry1 : System.Web.UI.Page
    {
        public static string strCategory;
        ReceiptEntryBAL objRec = new ReceiptEntryBAL();

        CommonBAL objCom = new CommonBAL();


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccCategorytMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "ACM_CATEGORY";
                drpCategory.DataValueField = "ACM_CATEGORY";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "0";


        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HBM_BANKNAME", "HMS_BANK_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "HBM_BANKNAME";
                drpBank.DataValueField = "HBM_BANKNAME";
                drpBank.DataBind();
            }
            drpBank.Items.Insert(0, "--- Select ---");
            drpBank.Items[0].Value = "0";
        }

        void BindCompany()
        {
            string Criteria = " 1=1 AND  HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";



            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_inccmp_details(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCustomer.DataSource = DS;
                drpCustomer.DataTextField = "HCM_NAME";
                drpCustomer.DataValueField = "HCM_COMP_ID";
                drpCustomer.DataBind();
            }
            drpCustomer.Items.Insert(0, "--- Select ---");
            drpCustomer.Items[0].Value = "";

        }

        void BIndCCType()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            DS = objCom.fnGetFieldValue("HCT_CC_TYPE", "HMS_CC_TYPE", Criteria, "");
            drpccType.Items.Clear();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpccType.DataSource = DS;
                drpccType.DataTextField = "HCT_CC_TYPE";
                drpccType.DataValueField = "HCT_CC_TYPE";
                drpccType.DataBind();
            }

            drpccType.Items.Insert(0, "--- Select ---");
            drpccType.Items[0].Value = "0";

        }

        void BindReceiptEntry()
        {
            objRec = new ReceiptEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND  ARE_TRANS_NO='" + txtTrnNumber.Text.Trim() + "' AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objRec.ReceiptEntryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtTrnNumber.Text = Convert.ToString(DR["ARE_TRANS_NO"]);
                    txtTransDate.Text = Convert.ToString(DR["ARE_DATEDesc"]);
                    drpCategory.SelectedValue = Convert.ToString(DR["ARE_TYPE"]);
                    drpStatus.SelectedValue = Convert.ToString(DR["ARE_STATUS"]);
                    txtAccountName.Text = Convert.ToString(DR["ARE_ACC_CODE"]) + "~" + Convert.ToString(DR["ARE_ACC_NAME"]);
                    txtAmount.Text = Convert.ToString(DR["ARE_AMOUNT"]);
                    txtChequeNo.Text = Convert.ToString(DR["ARE_CHEQUE_NO"]);
                    txtChequeDate.Text = Convert.ToString(DR["ARE_CHEQUE_DATEDesc"]);
                    drpBank.SelectedValue = Convert.ToString(DR["ARE_BANK_NAME"]);
                    drpccType.SelectedValue = Convert.ToString(DR["ARE_CC_TYPE"]);
                    txtCCNo.Text = Convert.ToString(DR["ARE_CC_NO"]);
                    txtCCHolder.Text = Convert.ToString(DR["ARE_CC_HOLDER"]);
                    txtDescription.Text = Convert.ToString(DR["ARE_DESCRIPTION"]);
                    txtRef.Text = Convert.ToString(DR["ARE_REF_NO"]);
                }

            }


        }

        void BindReceiptEntryGrid()
        {
            objRec = new ReceiptEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }



            if (txtSrcFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),ARE_DATE,101),101) >= '" + strForStartDate + "'";
            }




            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtSrcToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),ARE_DATE,101),101) <= '" + strForToDate + "'";
            }


            DS = objRec.ReceiptEntryGet(Criteria);
            gvReceipt.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvReceipt.DataSource = DS;
                gvReceipt.DataBind();
                gvReceipt.Visible = true;
            }
            else
            {
                gvReceipt.DataBind();
            }


        }

        void BuildeReceiptExpenses()
        {
            string Criteria = "1=2";
            DataTable dt = new DataTable();
            DataSet DS = new DataSet();
            objRec = new ReceiptEntryBAL();
            DS = objRec.ReceiptExpensesGet(Criteria);

            ViewState["ReceiptExpenses"] = DS.Tables[0];
        }

        void BindReceiptExpenses()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND ARE_TRANS_NO='" + txtTrnNumber.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objRec = new ReceiptEntryBAL();
            DS = objRec.ReceiptExpensesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["ReceiptExpenses"] = DS.Tables[0];

            }

        }

        void BindTempReceiptExpenses()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ReceiptExpenses"];

            gvReceiptExpenses.Visible = false;
            if (DT.Rows.Count > 0)
            {
                gvReceiptExpenses.Visible = true;
                gvReceiptExpenses.DataSource = DT;
                gvReceiptExpenses.DataBind();

            }
            else
            {
                gvReceiptExpenses.DataBind();
            }

        }

        void SaveReceipt()
        {
            objRec = new ReceiptEntryBAL();

            objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
            objRec.ARE_TRANS_NO = txtTrnNumber.Text.Trim();
            objRec.ARE_DATE = txtTransDate.Text.Trim();
            objRec.ARE_TYPE = drpCategory.SelectedValue;
            objRec.ARE_STATUS = drpStatus.SelectedValue;

            string strAccName = txtAccountName.Text.Trim();
            string AccCode = "", AccName = "";

            string[] arrAccName = strAccName.Split('~');
            if (arrAccName.Length > 0)
            {
                AccCode = arrAccName[0];
                if (arrAccName.Length > 1)
                {
                    AccName = arrAccName[1];
                }
            }

            objRec.ARE_ACC_CODE = AccCode;
            objRec.ARE_ACC_NAME = AccName;


            objRec.ARE_AMOUNT = txtAmount.Text.Trim();
            objRec.ARE_CHEQUE_NO = txtChequeNo.Text.Trim();
            objRec.ARE_CHEQUE_DATE = txtChequeDate.Text.Trim();
            objRec.ARE_BANK_NAME = drpBank.SelectedValue;
            objRec.ARE_CC_TYPE = drpccType.SelectedValue;
            objRec.ARE_CC_NO = txtCCNo.Text.Trim();
            objRec.ARE_CC_HOLDER = txtCCHolder.Text;
            objRec.ARE_DESCRIPTION = txtDescription.Text;
            objRec.ARE_REF_NO = txtRef.Text.Trim();
            objRec.UserID = Convert.ToString(Session["User_ID"]);
            objRec.ReceiptEntryAdd();


            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ReceiptExpenses"];
            if (DT.Rows.Count > 0)
            {
                objCom = new CommonBAL();

                string Criteria = " 1=1 ";

                Criteria += " AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  ARE_TRANS_NO ='" + txtTrnNumber.Text.Trim() + "'";

                objCom.fnDeleteTableData("AC_Receipt_EXPENSES", Criteria);

                foreach (DataRow DR in DT.Rows)
                {
                    objRec = new ReceiptEntryBAL();
                    objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objRec.ARE_TRANS_NO = txtTrnNumber.Text.Trim();
                    objRec.ARE_ACC_CODE = Convert.ToString(DR["ARE_ACC_CODE"]);
                    objRec.ARE_ACC_NAME = Convert.ToString(DR["ARE_ACC_NAME"]);
                    objRec.ARE_COST_CENT_CODE = Convert.ToString(DR["ARE_COST_CENT_CODE"]);
                    objRec.ARE_COST_CENT_NAME = Convert.ToString(DR["ARE_COST_CENT_NAME"]);
                    objRec.ARE_CUSTOMER_CODE = Convert.ToString(DR["ARE_CUSTOMER_CODE"]);
                    objRec.ARE_CUSTOMER_NAME = Convert.ToString(DR["ARE_CUSTOMER_NAME"]);

                    objRec.ARE_AMOUNT = Convert.ToString(DR["ARE_AMOUNT"]);


                    objRec.ReceiptExpensesAdd();


                }

            }

        }


        void Clear()
        {
            strCategory = "";

            drpCategory.SelectedIndex = 0;
            drpStatus.SelectedIndex = 0;
            txtAccountName.Text = "";
            txtAmount.Text = "";
            txtChequeNo.Text = "";
            txtChequeDate.Text = "";
            drpBank.SelectedIndex = 0;
            drpccType.SelectedIndex = 0;
            txtCCNo.Text = "";
            txtCCHolder.Text = "";
            txtDescription.Text = "";
            txtRef.Text = "";

            txtExpAccount.Text = "";
            txtExpAmount.Text = "";
            txtCostCenter.Text = "";


            if (drpCustomer.Items.Count > 0)
            {
                drpCustomer.SelectedIndex = 0;
            }

            BuildeReceiptExpenses();

            gvReceiptExpenses.DataBind();
            divGridPopup.Visible = false;
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";
            Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";

            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetExpenseList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";




            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCostCenterList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  ACCM_CC_NAME like '%" + prefixText + "%'";
            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.CostCenterMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["ACCM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["ACCM_CC_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                string PageName = Convert.ToString(Request.QueryString["PageName"]);

                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");

                txtSrcFromDate.Text = strFromDate.AddDays(-7).ToString("dd/MM/yyyy");
                txtSrcToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_RECEIPT");
                lblPageHeader.Text = "Receipt Entry";
                imgReceiptZoom.Visible = true;

                BindCategory();
                BindBank();
                BIndCCType();
                BindCompany();

                BuildeReceiptExpenses();
                BindReceiptEntryGrid();
            }
        }

        protected void txtTrnNumber_TextChanged(object sender, EventArgs e)
        {
            Clear();

            BindReceiptEntry();
            BindReceiptExpenses();
            BindTempReceiptExpenses();


        }



        protected void ReDeleteCC_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["ReceiptExpenses"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["ReceiptExpenses"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["ReceiptExpenses"] = DT;

                }


                BindTempReceiptExpenses();


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       DeleteCC_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }




        protected void drpPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            divCheque.Visible = false;
            divCC.Visible = false;
            strCategory = drpCategory.SelectedValue;

            if (drpCategory.SelectedValue.ToUpper() == "PDC PAYABLE".ToUpper() || drpCategory.SelectedValue.ToUpper() == "PDC RECEIVABLE".ToUpper())
            {
                divCheque.Visible = true;
            }
            else if (drpCategory.SelectedValue.ToUpper() == "CC".ToUpper())
            {
                divCC.Visible = true;
            }



        }

        protected void btnAddCost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (txtExpAccount.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter  A/C','Please Enter  A/C')", true);
                    goto FunEnd;
                }


                if (txtExpAmount.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter  Amount','Please Enter  Amount')", true);
                    goto FunEnd;
                }


                DataTable DT = new DataTable();


                DT = (DataTable)ViewState["ReceiptExpenses"];


                DataRow objrow;
                objrow = DT.NewRow();

                string strAccName = txtExpAccount.Text.Trim();
                string AccCode = "", AccName = "";

                string[] arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    AccName = arrAccName[1];
                }

                string strCC = txtCostCenter.Text.Trim();
                string ccCode = "", ccName = "";


                if (txtCostCenter.Text.Trim() != "")
                {


                    string[] arrCC = strCC.Split('~');
                    if (arrAccName.Length > 0)
                    {
                        ccCode = arrCC[0];
                        ccName = arrCC[1];
                    }

                }

                objrow["ARE_BRANCH_ID"] = Convert.ToString(Session["Branch_ID"]);
                objrow["ARE_TRANS_NO"] = txtTrnNumber.Text.Trim();
                objrow["ARE_ACC_CODE"] = AccCode;
                objrow["ARE_ACC_NAME"] = AccName;
                objrow["ARE_COST_CENT_CODE"] = ccCode;
                objrow["ARE_COST_CENT_NAME"] = ccName;
                objrow["ARE_AMOUNT"] = txtExpAmount.Text;
                objrow["ARE_CUSTOMER_CODE"] = drpCustomer.SelectedValue;
                objrow["ARE_CUSTOMER_NAME"] = drpCustomer.SelectedItem.Text;


                ViewState["ReceiptExpenses"] = DT;
                DT.Rows.Add(objrow);
                BindTempReceiptExpenses();



                txtExpAccount.Text = "";
                txtCostCenter.Text = "";


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddCost_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void PaymentSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblReTransNo = (Label)gvScanCard.Cells[0].FindControl("lblReTransNo");

                txtTrnNumber.Text = lblReTransNo.Text;
                txtTrnNumber_TextChanged(txtTrnNumber, new EventArgs());

                divGridPopup.Visible = false;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PaymentSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnReceiptPrint_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Prescription", "ShowPrescriptionPrintPDF('" + Convert.ToString(Session["Branch_ID"]) + "','" + txtTrnNumber.Text.Trim() + "');", true);

        }

        protected void btnCtlrRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_RECEIPT");
            Clear();
        }

        protected void imgReceiptZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindReceiptEntryGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            divGridPopup.Visible = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string Criteria = " ARE_TRANS_NO='" + txtTrnNumber.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_RECEIPT_EXPENSES", Criteria);
                objCom.fnDeleteTableData("AC_RECEIPT_ENTRY", Criteria);


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' Data Deleted ','Data Deleted ')", true);
                Clear();
                txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_RECEIPT");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void drpCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND  HCM_COMP_ID='" + drpCustomer.SelectedValue + "'";

            dboperations dbo = new dboperations();
            DS = dbo.retun_inccmp_details(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtExpAccount.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_CODE"]) + " ~ " + Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_NAME"]);

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (drpCategory.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please select the Type','Please select the Type')", true);
                    goto FunEnd;
                }


                if (txtTrnNumber.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. Number','Please Enter Trans. Number')", true);
                    goto FunEnd;
                }

                if (txtTransDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. Date','Please Enter Trans. Date')", true);
                    goto FunEnd;
                }


                if (txtAmount.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Amount.','Please Enter Amount.')", true);
                    goto FunEnd;
                }





                SaveReceipt();
                Clear();
                txtTrnNumber.Text = "";
                BindReceiptEntryGrid();

                txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_RECEIPT");


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('   Saved Successfully',' Data Saved')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindReceiptEntryGrid();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_RECEIPT");
            Clear();
        }
    }
}