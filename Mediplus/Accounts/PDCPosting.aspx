﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="PDCPosting.aspx.cs" Inherits="Mediplus.Accounts.PDCPosting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

       
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }



    </script>



    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .modalPopup
        {
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px 10px 10px 10px;
        }
    </style>

      <script type="text/javascript">
          function ShowMessage() {
              $("#myMessage").show();
              setTimeout(function () {
                  var selectedEffect = 'blind';
                  var options = {};
                  $("#myMessage").hide();
              }, 2000);
              return true;
          }

          function ShowErrorMessage(vMessage, vColor) {

              document.getElementById("divMessage").style.display = 'block';
              document.getElementById("divMessageClose").style.display = 'block';
              document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
            document.getElementById("<%=lblMessage.ClientID%>").style.color = vColor;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("divMessageClose").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
    }

</script>
     </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div>
            <div id="divMessageClose" style="display: none; position: absolute; top: 275px; left: 712px;">
                <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" />
            </div>
            <div style="padding-left: 60%; width: 80%;">
                <div id="divMessage" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 200px;">

                    <table cellpadding="0" cellspacing="0" width="100%">

                        <tr>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" CssClass="label" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                    </table>


                    <br />

                </div>
            </div>
             
            <table cellpadding="5" cellspacing="5" border="0" style="width: 80%">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Type
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpType" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                   <%-- <asp:ListItem Text="PDC Payable" Value="PDCP"  Selected="True"></asp:ListItem>--%>
                                    <asp:ListItem Text="PDC Receivable" Value="PDCR"></asp:ListItem>


                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">From. Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">To. Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">
                        <asp:ImageButton ID="imgPaymentZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Entry" OnClick="imgPaymentZoom_Click" />

                    </td>
                </tr>

            </table>
            <div style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>

                                    <asp:GridView ID="gvPayment" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30" Visible="false"
                                        EnableModelValidation="True" Width="100%">
                                        <HeaderStyle CssClass="GridHeader_Gray" />
                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>
                                            <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>

                                                    <asp:CheckBox ID="chkPaySelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkPaySelectAll_CheckedChanged" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkPayment" runat="server" CssClass="label" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trn. Number" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_TRANS_NO") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblTransDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_DATEDesc") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_TYPE") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Account Name" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblAccName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_ACC_NAME") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblAmount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_AMOUNT") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                    <asp:GridView ID="gvReceipt" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                        EnableModelValidation="True" Width="100%">
                                        <HeaderStyle CssClass="GridHeader_Gray" />
                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>
                                             <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>

                                                    <asp:CheckBox ID="chkRecSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkRecSelectAll_CheckedChanged" />

                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkReceipt" runat="server" CssClass="label" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trn. Number" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblReTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ARE_TRANS_NO") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblReTransDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ARE_DATEDesc") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblReType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ARE_TYPE") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Account Name" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblReAccName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ARE_ACC_NAME") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblReAmount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ARE_AMOUNT") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>

            <table style="width: 80%;">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Bank
                    </td>
                    <td colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpBank" CssClass="TextBoxStyle" runat="server" Width="100%" Height="25px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Posting" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div> 
   
 
       </asp:Content>