﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PaymentEntry1.aspx.cs" Inherits="Mediplus.Accounts.PaymentEntry1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<%@ Register TagPrefix="UC1" TagName="AccountMasterList" Src="~/Accounts/AccountMasterList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ShowPaytype(vValue) {

            document.getElementById("divCheque").style.visibility = 'visible';

        }


        function ShowPaymentPrintPDF(BranchId, TransNo) {

            var win = '';
            var Report = "Payment.rpt";


            var Criteria = " 1=1 ";
            Criteria += ' AND {AC_VIEW_PAYMENT.APE_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {AC_VIEW_PAYMENT.APE_TRANS_NO}=\'' + TransNo + '\'';
            win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


            win.focus();


        }


        function ShowChequePrintPDF(BranchId, TransNo, vReport) {

            var win = '';

            if (vReport == '') {

                vReport = 'ChequePrint1.rpt';
            }


            var Criteria = " 1=1 ";
            Criteria += ' AND {AC_VIEW_PAYMENT.APE_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {AC_VIEW_PAYMENT.APE_TRANS_NO}=\'' + TransNo + '\'';
            win = window.open('../CReports/ReportViewer.aspx?ReportName=' + vReport + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


            win.focus();


        }

        function ChangeBodyStyle() {

            document.getElementsByTagName("body")[0].style = "modalBackground";
        }

        function ShowAccountMaster(CtrlName) {
            alert(CtrlName);
            var win = window.open("MasterList.aspx?PageName=PaymentEntry&CtrlName" + CtrlName, "newwin", "top=200,left=100,height=570,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }
        function BindAccountDetails(strCtrlName, Code, Name, Category) {

            alert(strCtrlName);
            document.getElementById("<%=txtAccountName.ClientID%>").value = Code + ' ~ ' + Name;

        }



    </script>



    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .modalPopup
        {
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px 10px 10px 10px;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage, vColor) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("divMessageClose").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
            document.getElementById("<%=lblMessage.ClientID%>").style.color = vColor;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("divMessageClose").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
         }

         function ShowMasterPopup() {

             document.getElementById("divGridPopup").style.display = 'block';


         }

         function HideMasterPopup() {


             document.getElementById("<%=divGridPopup.ClientID%>").style.display = 'none';
    }



    function SetExpAmt() {

        var Amt = document.getElementById("<%=txtAmount.ClientID%>").value;

                document.getElementById("<%=txtExpAmount.ClientID%>").value = Amt;
            }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div id="divMessageClose" style="display: none; position: absolute; top: 275px; left: 712px;">
            <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" />
        </div>
        <div style="padding-left: 60%; width: 100%;">
            <div id="divMessage" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 200px;">

                <table cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server" CssClass="label" Style="font-weight: bold;"></asp:Label>
                        </td>
                    </tr>
                </table>


                <br />

            </div>
        </div>
        <table style="width: 80%;">
            <tr>
                <td align="left">
                    <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Payment Entry"></asp:Label>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

                            <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return window.confirm('Do you want to Delete this data?')" />

                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 80%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
            <div style="width: 100%; text-align: right; float: right;">
                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                    <ContentTemplate>

                        <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <table cellpadding="5" cellspacing="5" border="0" style="width: 80%">

                <tr>
                    <td class="lblCaption1" style="width: 100px;">Trn. Number
                       <asp:ImageButton ID="imgPaymentZoom" Visible="false" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Payment Entry" OnClick="imgPaymentZoom_Click" />

                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtTrnNumber" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtTrnNumber_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                    </td>
                    <td class="lblCaption1">Trans. Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtTransDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                    Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>


                    <td class="lblCaption1">Type
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpCategory" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="drpPayType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="drpCategory" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Status
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpStatus" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                    <asp:ListItem Value="Active">Active</asp:ListItem>
                                    <asp:ListItem Value="InActive">InActive</asp:ListItem>

                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                </tr>

                <div id="divCheque" runat="server" visible="false">

                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Cheque No.
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtChequeNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Cheque. Date
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtChequeDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txtChequeDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Bank
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpBank" CssClass="DropDown3DStyle" runat="server" Width="100%" Height="25px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>


                </div>

                <div id="divCC" runat="server" visible="false">

                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Type</td>
                        <td style="width: 100px;">
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpccType" runat="server" CssClass="DropDown3DStyle" Width="100%" BorderWidth="1px" BorderColor="#cccccc">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">C.C No.
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCCNo" runat="server" Width="100%" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Holder
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCCHolder" runat="server" Width="100%" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>

                </div>

                <tr>
                    <td class="lblCaption1" style="width: 100px;">Account Name 
                       <asp:ImageButton ID="ImageButton1" Visible="false" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Payment Entry" OnClick="imgPaymentZoom_Click" />

                    </td>
                    <td colspan="5">
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAccountName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" ondblclick="return ShowAccountMaster('txtAccountName');"></asp:TextBox>
                                <div id="divwidth" style="visibility: hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtAccountName" MinimumPrefixLength="1" ServiceMethod="GetAccountList"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Amount
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);" onkeyup="SetExpAmt();"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>


                </tr>
            </table>


            <div style="overflow: auto; padding-top: 0px; width: 80%; height: 250px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
                <table cellpadding="5" cellspacing="5" style="width: 100%">
                    <tr>
                        <td class="lblCaption1">Supplier
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSupplier" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" AutoPostBack="true" OnSelectedIndexChanged="drpSupplier_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Expenses A/C
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtExpAccount" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtExpAccount" MinimumPrefixLength="1" ServiceMethod="GetExpenseList"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                                    </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>

                        <td class="lblCaption1">Cost Center
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCostCenter" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtCostCenter" MinimumPrefixLength="1" ServiceMethod="GetCostCenterList"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                                    </asp:AutoCompleteExtender>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Amount
                        </td>
                        <td class="lblCaption1">

                            <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                <ContentTemplate>

                                    <asp:TextBox ID="txtExpAmount" runat="server" CssClass="TextBoxStyle" Width="70%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:ImageButton ID="btnAddCost" runat="server" ImageUrl="~/Images/New.png" Style="height: 22px; width: 22px;" ToolTip="Create New Entry" OnClick="btnAddCost_Click" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>


                    </tr>

                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvPaymentExpenses" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="99%" gridline="none">
                                        <HeaderStyle CssClass="GridHeader_Gray" />
                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>
                                            <asp:TemplateField HeaderText="Supplier ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSuppCode" CssClass="label" runat="server" Text='<%# Bind("APE_SUPPLIER_CODE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblSuppName" CssClass="label" runat="server" Text='<%# Bind("APE_SUPPLIER_NAME") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expenses A/C ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAccCode" CssClass="label" runat="server" Text='<%# Bind("APE_ACC_CODE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblAccName" CssClass="label" runat="server" Text='<%# Bind("APE_ACC_NAME") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" CssClass="label" runat="server" Text='<%# Bind("APE_AMOUNT") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cost Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblccCode" CssClass="label" runat="server" Text='<%# Bind("APE_COST_CENT_CODE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblccName" CssClass="label" runat="server" Text='<%# Bind("APE_COST_CENT_NAME") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="DeleteCC" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="12px" Height="12px"
                                                        OnClick="DeleteCC_Click" />&nbsp;&nbsp;
                                                
                                                </ItemTemplate>
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>


                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>


            <table cellpadding="5" cellspacing="5" style="width: 80%">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Description 
                    </td>
                    <td colspan="8">
                        <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" CssClass="TextBoxStyle" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Ref
                    </td>
                    <td colspan="8">
                        <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtRef" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>

            <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                    <td>
                        <asp:Button ID="btnPaymentPrint" runat="server" CssClass="button gray small" Width="120px" OnClick="btnPaymentPrint_Click" Text="Payment Print" />
                        <asp:Button ID="btnChequePrint" runat="server" CssClass="button gray small" Width="120px" OnClick="btnChequePrint_Click" Text="Cheque Print" />
                    </td>

                </tr>
            </table>


            <div id="divGridPopup" runat="server" visible="false" style="overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 150px;">
                <div style="width: 100%; text-align: right; float: right; position: absolute; top: 0px; right: 0px;">

                    <img src="../Images/Close.png" style="height: 25px; width: 25px;" onclick="HideMasterPopup()" />

                </div>

                <table cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td class="lblCaption1" style="height: 400px; vertical-align: top;">
                            <asp:GridView ID="gvPayment" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30" Visible="false"
                                EnableModelValidation="True" Width="100%">
                                <HeaderStyle CssClass="GridHeader_Gray" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <RowStyle CssClass="GridRow" />

                                <Columns>

                                    <asp:TemplateField HeaderText="Trn. Number" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTransNo" runat="server" OnClick="PaymentSelect_Click">
                                                <asp:Label ID="lblTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_TRANS_NO") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTransDate" runat="server" OnClick="PaymentSelect_Click">
                                                <asp:Label ID="lblTransDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkType" runat="server" OnClick="PaymentSelect_Click">
                                                <asp:Label ID="lblType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_TYPE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAccName" runat="server" OnClick="PaymentSelect_Click">
                                                <asp:Label ID="lblAccName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_ACC_NAME") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAmount" runat="server" OnClick="PaymentSelect_Click">
                                                <asp:Label ID="lblAmount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("APE_AMOUNT") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </td>
                    </tr>


                </table>

            </div>



             


        </div>
</asp:Content>
