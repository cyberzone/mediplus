﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Accounts
{
    public partial class ReportLoader : System.Web.UI.Page
    {
       
        public string strPageHeader = "";

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void Clear()
        {
            txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            // Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                try
                {
                    strPageHeader = Convert.ToString(Request.QueryString["PageName"]);
                    hidPageName.Value = strPageHeader;

                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                   


                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

 
    }
}