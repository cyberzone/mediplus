﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Receipt.aspx.cs" Inherits="Mediplus.Accounts.Receipt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .modalPopup
        {
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px 10px 10px 10px;
        }
    </style>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }




        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

            if (vColor != '') {

                document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


            }

            document.getElementById("divMessage").style.display = 'block';



        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

        function ValReceipt() {

            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br></br>";

            if (document.getElementById('<%=txtReceiptNo.ClientID%>').value == "") {
                ErrorMessage += 'Receipt No. is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtReceiptDate.ClientID%>').value == "") {
                ErrorMessage += 'Receipt Date. is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                ErrorMessage += 'Patient File No. is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtCurPay.ClientID%>').value == "" ) {
                ErrorMessage += 'Current Pay is Empty.' + trNewLineChar;
                IsError = true;
            }


            if (IsError == true) {
                ShowErrorMessage('Please check below Errors', ErrorMessage, 'Brown');

                return false;
            }



            return true;

        }

        function DeleteReceiptVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strCompCode = document.getElementById('<%=txtReceiptNo.ClientID%>').value
             if (/\S+/.test(strCompCode) == false) {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Receipt No";
                document.getElementById('<%=txtReceiptNo.ClientID%>').focus;
                return false;
            }

            var isDelCompany = window.confirm('Do you want to Delete Receipt Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }


        function ShowPrintInvoice(BranchID, ReceiptNoNo) {
            var ReportName = "HmsReceiptPt.rpt";
            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_RECEIPT_MASTER.HRM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_RECEIPT_MASTER.HRM_RECEIPTNO}=\'' + ReceiptNoNo + '\'';
             
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: auto; width: 600px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>
                        <br />
                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>
                    </div>
                    <span style="float: right; margin-right: 20px">

                        <input type="button" id="Button5" class="ButtonStyle gray" style="font-weight: bold; cursor: pointer; width: 50px;" value=" OK " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <table style="width: 80%">
        <tr>
            <td align="left">
                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Receipt"></asp:Label>

            </td>
            <td align="right">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" OnClientClick="return ValReceipt();" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return DeleteReceiptVal();"  />
                        <asp:Button ID="btnPrint" runat="server" CssClass="button gray small" Width="70px" OnClick="btnPrint_Click" Text="Print" />
                        <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>

    <div style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

        <table cellpadding="5" cellspacing="5" border="0" style="width: 100%">
            <tr>

                <td class="lblCaption1" style="width: 100px;">Receipt No.
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtReceiptNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" AutoPostBack="true" OnTextChanged="txtReceiptNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td class="lblCaption1" style="width: 100px;">Date
                </td>
                <td style="width: 200px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtReceiptDate" runat="server" CssClass="TextBoxStyle" Width="70px" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtReceiptDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                            :
                                      <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Status
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                <asp:ListItem Value="1">Open</asp:ListItem>
                                <asp:ListItem Value="2">Hold</asp:ListItem>
                                <asp:ListItem Value="3">Void</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>

                <td class="lblCaption1" style="width: 100px;">File No.
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFileNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>

                
                <td class="lblCaption1" style="width: 100px;">Name
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPTName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" AutoPostBack="true" OnTextChanged="txtReceiptNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>


                </tr> 
            <tr>
                <td class="lblCaption1" style="width: 100px;">Description
 
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td class="lblCaption1" style="width: 100px;">Ref. No
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRefNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td class="lblCaption1" style="width: 100px;">Prev. Bala.
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPrevBalance" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" Font-Bold="true"   Enabled="false"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px;">Cur. Pay.
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCurPay" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" Font-Bold="true"  style="text-align:right;" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtCurPay_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td class="lblCaption1" style="width: 100px;">Cur. Bal.
 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCurBalance" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" Font-Bold="true"  Enabled="false" ></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1">Pay. Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpPayType" runat="server" CssClass="TextBoxStyle" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="drpPayType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
            <ContentTemplate>
                <div id="divCC" runat="server" visible="false">
                    <table style="width: 100%;">
                        <tr>
                            <td class="lblCaption1" style="width: 100px;">Type</td>
                            <td style="width: 125px;">
                                <asp:DropDownList ID="drpccType" runat="server" CssClass="TextBoxStyle" Width="120px">
                                </asp:DropDownList>
                            </td>
                            <td class="lblCaption1" style="width: 70px;">C.C Amt.
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel50" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtCCAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">C.C No.
                            </td>
                            <td>
                                <asp:TextBox ID="txtCCNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Holder
                            </td>
                            <td>
                                <asp:TextBox ID="txtCCHolder" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Ref No.
                            </td>
                            <td>
                                <asp:TextBox ID="txtCCRefNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
            <ContentTemplate>

                <div id="divCheque" runat="server" visible="false">
                    <table style="width: 100%;">
                        <tr>
                            <td class="lblCaption1" style="width: 100px;">Cheq. No.
                            </td>
                            <td>
                                <asp:TextBox ID="txtDcheqno" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </td>
                            <td class="lblCaption1" style="width: 70px;">Cheq. Amt.
                            </td>
                            <td>
                                <asp:TextBox ID="txtChqAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Cheq. Date
                            </td>
                            <td>
                                <asp:TextBox ID="txtDcheqdate" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                    Enabled="True" TargetControlID="txtDcheqdate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>

                            </td>
                            <td class="lblCaption1">Bank
                            </td>
                            <td>
                                <asp:DropDownList ID="drpBank" CssClass="TextBoxStyle" runat="server" Width="250px"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div style="width: 80%; height: 300px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <asp:UpdatePanel ID="UpdatePanel60" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvInvoiceDtls" runat="server" AllowSorting="True" AutoGenerateColumns="False"  
                                    EnableModelValidation="True" Width="100%" OnRowDataBound="gvInvoiceDtls_RowDataBound">
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Invoice No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvReceiptDtlsReceiptNo" CssClass="lblCaption1" runat="server" OnClick="ReceiptDtlsEdit_Click">
                                                     <asp:Label ID="lblgvReceiptDtlsInvoiceNo" CssClass="label" Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("InvoiceID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvReceiptDtlsDate" CssClass="label" Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("TransDate") %>'></asp:Label>&nbsp;
                                                <asp:Label ID="lblgvReceiptDtlsTime" CssClass="label" Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("TransTime") %>'></asp:Label>&nbsp;
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left"   ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvReceiptDtlsAmount" CssClass="label"   Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("InvoiceAmt") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Paid" HeaderStyle-HorizontalAlign="Left"   ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvReceiptDtlsPaid" CssClass="label"   Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("PaidAmt") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Cur. Pay" HeaderStyle-HorizontalAlign="Left"   ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvReceiptDtlsPrevPaid" CssClass="label"   Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("CurPay") %>'  Visible="false"   ></asp:Label>

                                                <asp:TextBox ID="lblgvReceiptDtlsCurPay" CssClass="label"   Font-Size="11px" Font-Bold="true"  runat="server" Text='<%# Bind("CurPay") %>' style="text-align:right;"  ></asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         


                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
    </div>

</asp:Content>
