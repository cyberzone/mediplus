﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;

namespace Mediplus.Accounts
{
    public partial class AccountCategory : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindBranch()
        {

            DataSet ds = new DataSet();
            ds = objCom.BranchMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpBranch.DataSource = ds;
                drpBranch.DataValueField = "HBM_ID";
                drpBranch.DataTextField = "HBM_ID";
                drpBranch.DataBind();

            }

        }

        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccCategorytMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvCategoryMaster.DataSource = DS;

                gvCategoryMaster.DataBind();
            }



        }

        void Clear()
        {

            drpBranch.SelectedIndex = 0;
            txtCategoryName.Text = "";
            txtSubCategoryName.Text = "";
            drpMainCategory.SelectedValue = "";
            txtStartNo.Text = "";
            txtEndNo.Text = "";
            txtCurrentNo.Text = "";
            txtPrefix.Text = "";
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            lblStatus.Text = "";
            if (!IsPostBack)
            {

                BindBranch();
                BindCategory();
            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {

                if (txtCategoryName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Category Name";
                    goto FunEnd;
                }

                if (txtSubCategoryName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Sub Category Name";
                    goto FunEnd;
                }


                if (drpMainCategory.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please Select Main Category Name";
                    goto FunEnd;
                }

                if (txtStartNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Start No.";
                    goto FunEnd;
                }

                if (txtStartNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter End No.";
                    goto FunEnd;
                }

                if (txtEndNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter End No.";
                    goto FunEnd;
                }

                if (txtCurrentNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Current No.";
                    goto FunEnd;
                }
                MastersBAL objMast = new MastersBAL();

                objMast.BranchID = drpBranch.SelectedValue;
                objMast.ACM_CATEGORY = txtCategoryName.Text;
                objMast.ACM_SUB_CATEGORY = txtSubCategoryName.Text;
                objMast.ACM_MAIN_CATEGORY = drpMainCategory.SelectedValue;
                objMast.ACM_START_NO = txtStartNo.Text;
                objMast.ACM_END_NO = txtEndNo.Text;
                objMast.ACM_CURRENT_NO = txtCurrentNo.Text;
                objMast.ACM_PREFIX = txtPrefix.Text;
                objMast.ACM_STATUS = "A";
                objMast.CategorytMasterAdd();

                Clear();
                BindCategory();
                lblStatus.Text = "Saved Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvCategoryMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvCategoryMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblBranchID = (Label)gvScanCard.Cells[0].FindControl("lblBranchID");
                Label lblCategory = (Label)gvScanCard.Cells[0].FindControl("lblCategory");



                Label lblSubCategory = (Label)gvScanCard.Cells[0].FindControl("lblSubCategory");
                Label lblMainCategory = (Label)gvScanCard.Cells[0].FindControl("lblMainCategory");


                Label lblStartNo = (Label)gvScanCard.Cells[0].FindControl("lblStartNo");
                Label lblEndNo = (Label)gvScanCard.Cells[0].FindControl("lblEndNo");


                Label lblCuttentNo = (Label)gvScanCard.Cells[0].FindControl("lblCuttentNo");
                Label lblPrefix = (Label)gvScanCard.Cells[0].FindControl("lblPrefix");




                drpBranch.SelectedValue = lblBranchID.Text;
                txtCategoryName.Text = lblCategory.Text;
                txtSubCategoryName.Text = lblSubCategory.Text;
                drpMainCategory.SelectedValue = lblMainCategory.Text;
                txtStartNo.Text = lblStartNo.Text;
                txtEndNo.Text = lblEndNo.Text;
                txtCurrentNo.Text = lblCuttentNo.Text;
                txtPrefix.Text = lblPrefix.Text;



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnClear_Click(object sender, EventArgs e)
        {

            Clear();
        }
    }
}