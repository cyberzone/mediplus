﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;
namespace Mediplus.Accounts
{
    public partial class PaymentEntry1 : System.Web.UI.Page
    {
        public static string strCategory;
        PaymentEntryBAL objPay = new PaymentEntryBAL();
        ReceiptEntryBAL objRec = new ReceiptEntryBAL();

        CommonBAL objCom = new CommonBAL();


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCategory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND ACM_STATUS='A' AND ACM_CATEGORY IN ('CASH','BANK','CC','PDC PAYABLE') ";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccCategorytMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "ACM_CATEGORY";
                drpCategory.DataValueField = "ACM_CATEGORY";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "";


        }

        void BindSuppliertMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND ASM_ACTIVE= 'A'  AND ASM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            SuppliertMasterBAL objMast = new SuppliertMasterBAL();
            DS = objMast.SuppliertMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpSupplier.DataSource = DS;
                drpSupplier.DataTextField = "ASM_NAME";
                drpSupplier.DataValueField = "ASM_CODE";
                drpSupplier.DataBind();
            }
            drpSupplier.Items.Insert(0, "--- Select ---");
            drpSupplier.Items[0].Value = "";


        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            //string Criteria = " 1=1  AND APB_STATUS='A' ";


            //objCom = new CommonBAL();
            //DS = objCom.fnGetFieldValue("APB_BANK_NAME", "AC_PAYMENT_BANK ", Criteria, "");

            string Criteria = " 1=1  AND AAM_STATUS='A' AND  AAM_CATEGORY = 'BANK' ";


            MastersBAL objMast = new MastersBAL();
            DS = objMast.AccountMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "AAM_ACCOUNT_NAME";
                drpBank.DataValueField = "AAM_CODE";
                drpBank.DataBind();
            }
            drpBank.Items.Insert(0, "--- Select ---");
            drpBank.Items[0].Value = "";
        }


        string GetChequeReportName()
        {
            string ReportName = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND APB_STATUS='A' AND APB_BANK_NAME='" + drpBank.SelectedValue + "'  ";


            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("APB_REPORT_NAME", "AC_PAYMENT_BANK ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                ReportName = Convert.ToString(DS.Tables[0].Rows[0]["APB_REPORT_NAME"]);
            }

            return ReportName;

        }


        void BIndCCType()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            DS = objCom.fnGetFieldValue("HCT_CC_TYPE", "HMS_CC_TYPE", Criteria, "");
            drpccType.Items.Clear();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpccType.DataSource = DS;
                drpccType.DataTextField = "HCT_CC_TYPE";
                drpccType.DataValueField = "HCT_CC_TYPE";
                drpccType.DataBind();
            }

            drpccType.Items.Insert(0, "--- Select ---");
            drpccType.Items[0].Value = "";

        }



        void BindPaymentEntry()
        {
            objPay = new PaymentEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND  APE_TRANS_NO='" + txtTrnNumber.Text.Trim() + "' AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objPay.PaymentEntryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["SaveMode"] = "M";
                    txtTrnNumber.Text = Convert.ToString(DR["APE_TRANS_NO"]);
                    txtTransDate.Text = Convert.ToString(DR["APE_DATEDesc"]);
                    drpCategory.SelectedValue = Convert.ToString(DR["APE_TYPE"]);
                    drpStatus.SelectedValue = Convert.ToString(DR["APE_STATUS"]);
                    txtAccountName.Text = Convert.ToString(DR["APE_ACC_CODE"]) + "~" + Convert.ToString(DR["APE_ACC_NAME"]);
                    txtAmount.Text = Convert.ToString(DR["APE_AMOUNT"]);
                    txtChequeNo.Text = Convert.ToString(DR["APE_CHEQUE_NO"]);
                    txtChequeDate.Text = Convert.ToString(DR["APE_CHEQUE_DATEDesc"]);
                    drpBank.SelectedValue = Convert.ToString(DR["APE_BANK_NAME"]);
                    drpccType.SelectedValue = Convert.ToString(DR["APE_CC_TYPE"]);
                    txtCCNo.Text = Convert.ToString(DR["APE_CC_NO"]);
                    txtCCHolder.Text = Convert.ToString(DR["APE_CC_HOLDER"]);
                    txtDescription.Text = Convert.ToString(DR["APE_DESCRIPTION"]);
                    txtRef.Text = Convert.ToString(DR["APE_REF_NO"]);

                    strCategory = drpCategory.SelectedValue;

                    if (drpCategory.SelectedValue.ToUpper() == "BANK".ToUpper() || drpCategory.SelectedValue.ToUpper() == "PDC PAYABLE".ToUpper() || drpCategory.SelectedValue.ToUpper() == "PDC RECEIVABLE".ToUpper())
                    {
                        divCheque.Visible = true;
                    }
                    else if (drpCategory.SelectedValue.ToUpper() == "CC".ToUpper())
                    {
                        divCC.Visible = true;
                    }

                }

            }


        }

        void BindPaymentEntryGrid()
        {
            objPay = new PaymentEntryBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objPay.PaymentEntryGet(Criteria);
            gvPayment.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPayment.Visible = true;
                gvPayment.DataSource = DS;
                gvPayment.DataBind();

            }
            else
            {
                gvPayment.DataBind();
            }


        }

        void BuildePaymentExpenses()
        {
            string Criteria = "1=2";
            DataTable dt = new DataTable();
            DataSet DS = new DataSet();
            objPay = new PaymentEntryBAL();
            DS = objPay.PaymentExpensesGet(Criteria);

            ViewState["PaymentExpenses"] = DS.Tables[0];
        }

        void BindPaymentExpenses()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND APE_TRANS_NO='" + txtTrnNumber.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objPay = new PaymentEntryBAL();
            DS = objPay.PaymentExpensesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["PaymentExpenses"] = DS.Tables[0];

            }

        }

        void BindTempPaymentExpenses()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["PaymentExpenses"];

            // gvPaymentExpenses.Visible = false;
            if (DT.Rows.Count > 0)
            {
                // gvPaymentExpenses.Visible = true;
                gvPaymentExpenses.DataSource = DT;
                gvPaymentExpenses.DataBind();

            }
            else
            {
                gvPaymentExpenses.DataBind();
            }

        }


        void SavePayment()
        {
            objPay = new PaymentEntryBAL();

            objPay.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPay.APE_TRANS_NO = txtTrnNumber.Text.Trim();
            objPay.APE_DATE = txtTransDate.Text.Trim();
            objPay.APE_TYPE = drpCategory.SelectedValue;
            objPay.APE_STATUS = drpStatus.SelectedValue;

            string strAccName = txtAccountName.Text.Trim();
            string AccCode = "", AccName = "";

            string[] arrAccName = strAccName.Split('~');
            if (arrAccName.Length > 0)
            {
                AccCode = arrAccName[0];
                AccName = arrAccName[1];
            }

            objPay.APE_ACC_CODE = AccCode;
            objPay.APE_ACC_NAME = AccName;


            objPay.APE_AMOUNT = txtAmount.Text.Trim();
            objPay.APE_CHEQUE_NO = txtChequeNo.Text.Trim();
            objPay.APE_CHEQUE_DATE = txtChequeDate.Text.Trim();
            objPay.APE_BANK_NAME = drpBank.SelectedValue;
            objPay.APE_CC_TYPE = drpccType.SelectedValue;
            objPay.APE_CC_NO = txtCCNo.Text.Trim();
            objPay.APE_CC_HOLDER = txtCCHolder.Text;
            objPay.APE_DESCRIPTION = txtDescription.Text;
            objPay.APE_REF_NO = txtRef.Text.Trim();

            objPay.MODE = Convert.ToString(ViewState["SaveMode"]);
            objPay.UserID = Convert.ToString(Session["User_ID"]);
            objPay.PaymentEntryAdd();


            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["PaymentExpenses"];
            if (DT.Rows.Count > 0)
            {
                objCom = new CommonBAL();

                string Criteria = " 1=1 ";

                Criteria += " AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APE_TRANS_NO ='" + txtTrnNumber.Text.Trim() + "'";

                objCom.fnDeleteTableData("AC_PAYMENT_EXPENSES", Criteria);

                foreach (DataRow DR in DT.Rows)
                {
                    objPay = new PaymentEntryBAL();
                    objPay.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objPay.APE_TRANS_NO = txtTrnNumber.Text.Trim();
                    objPay.APE_ACC_CODE = Convert.ToString(DR["APE_ACC_CODE"]);
                    objPay.APE_ACC_NAME = Convert.ToString(DR["APE_ACC_NAME"]);
                    objPay.APE_COST_CENT_CODE = Convert.ToString(DR["APE_COST_CENT_CODE"]);
                    objPay.APE_COST_CENT_NAME = Convert.ToString(DR["APE_COST_CENT_NAME"]);
                    objPay.APE_SUPPLIER_CODE = Convert.ToString(DR["APE_SUPPLIER_CODE"]);
                    objPay.APE_SUPPLIER_NAME = Convert.ToString(DR["APE_SUPPLIER_NAME"]);
                    objPay.APE_AMOUNT = Convert.ToString(DR["APE_AMOUNT"]);
                    objPay.PaymentExpensesAdd();


                }

            }

        }




        void Clear()
        {
            strCategory = "";
            divCheque.Visible = false;
            divCC.Visible = false;

            drpCategory.SelectedIndex = 0;
            drpStatus.SelectedIndex = 0;
            txtAccountName.Text = "";
            txtAmount.Text = "";
            txtChequeNo.Text = "";
            txtChequeDate.Text = "";
            drpBank.SelectedIndex = 0;
            drpccType.SelectedIndex = 0;
            txtCCNo.Text = "";
            txtCCHolder.Text = "";
            txtDescription.Text = "";
            txtRef.Text = "";

            if (drpSupplier.Items.Count > 0)
            {
                drpSupplier.SelectedIndex = 0;
            }

            txtExpAccount.Text = "";
            txtExpAmount.Text = "";
            txtCostCenter.Text = "";

            BuildePaymentExpenses();

            gvPaymentExpenses.DataBind();

            divGridPopup.Visible = false;
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetExpenseList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";




            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCostCenterList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  ACCM_CC_NAME like '%" + prefixText + "%'";
            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.CostCenterMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["ACCM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["ACCM_CC_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["SaveMode"] = "A";
                string PageName = Convert.ToString(Request.QueryString["PageName"]);

                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
                objCom = new CommonBAL();
                txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");

                lblPageHeader.Text = "Payment Entry";
                imgPaymentZoom.Visible = true;

                BindCategory();
                BindSuppliertMaster();
                BindBank();
                BIndCCType();

                BuildePaymentExpenses();
                BindPaymentEntryGrid();


            }
        }

        protected void txtTrnNumber_TextChanged(object sender, EventArgs e)
        {
            Clear();

            BindPaymentEntry();
            BindPaymentExpenses();
            BindTempPaymentExpenses();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (drpCategory.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please select the Type','Red')", true);
                    goto FunEnd;
                }


                if (txtTrnNumber.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. Number','Red')", true);
                    goto FunEnd;
                }

                if (txtTransDate.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Trans. Date','Red')", true);
                    goto FunEnd;
                }


                if (txtAmount.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Amount. Date','Red')", true);
                    goto FunEnd;
                }

                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["PaymentExpenses"];
                if (DT.Rows.Count > 0)
                {
                    decimal decTotalAmt = 0;
                    objCom = new CommonBAL();

                    string Criteria = " 1=1 ";

                    Criteria += " AND APE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  APE_TRANS_NO ='" + txtTrnNumber.Text.Trim() + "'";

                    objCom.fnDeleteTableData("AC_PAYMENT_EXPENSES", Criteria);

                    foreach (DataRow DR in DT.Rows)
                    {

                        decTotalAmt = decTotalAmt + Convert.ToDecimal(DR["APE_AMOUNT"]);



                    }


                    if (decTotalAmt != Convert.ToDecimal(txtAmount.Text.Trim()))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Expenses Amount Not Match with Total Amount','Red')", true);
                        goto FunEnd;


                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Expenses Dtls','Red')", true);
                    goto FunEnd;


                }

                SavePayment();
                Clear();
                txtTrnNumber.Text = "";
                BindPaymentEntryGrid();


                txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('   Saved Successfully','Data Saved')", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void DeleteCC_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["PaymentExpenses"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["PaymentExpenses"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["PaymentExpenses"] = DT;

                }


                BindTempPaymentExpenses();


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       DeleteCC_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }




        protected void drpPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            divCheque.Visible = false;
            divCC.Visible = false;
            strCategory = drpCategory.SelectedValue;

            if (drpCategory.SelectedValue.ToUpper() == "BANK".ToUpper() || drpCategory.SelectedValue.ToUpper() == "PDC PAYABLE".ToUpper() || drpCategory.SelectedValue.ToUpper() == "PDC RECEIVABLE".ToUpper())
            {
                divCheque.Visible = true;
            }
            else if (drpCategory.SelectedValue.ToUpper() == "CC".ToUpper())
            {
                divCC.Visible = true;
            }



        }

        protected void btnAddCost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (txtExpAccount.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Expenses A/C','Red')", true);
                    goto FunEnd;
                }


                if (txtExpAmount.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please Enter Expenses  Amount','Red')", true);
                    goto FunEnd;
                }


                DataTable DT = new DataTable();

                if (lblPageHeader.Text == "Payment Entry")
                {
                    DT = (DataTable)ViewState["PaymentExpenses"];
                }
                else if (lblPageHeader.Text == "Receipt Entry")
                {
                    DT = (DataTable)ViewState["ReceiptExpenses"];
                }


                DataRow objrow;
                objrow = DT.NewRow();

                string strAccName = txtExpAccount.Text.Trim();
                string AccCode = "", AccName = "";

                string[] arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }

                string strCC = txtCostCenter.Text.Trim();
                string ccCode = "", ccName = "";

                if (txtCostCenter.Text.Trim() != "")
                {
                    string[] arrCC = strCC.Split('~');
                    if (arrAccName.Length > 0)
                    {
                        ccCode = arrCC[0];
                        if (arrAccName.Length > 1)
                        {
                            ccName = arrCC[1];
                        }
                    }
                }
                objrow["APE_BRANCH_ID"] = Convert.ToString(Session["Branch_ID"]);
                objrow["APE_TRANS_NO"] = txtTrnNumber.Text.Trim();
                objrow["APE_ACC_CODE"] = AccCode;
                objrow["APE_ACC_NAME"] = AccName;
                objrow["APE_COST_CENT_CODE"] = ccCode;
                objrow["APE_COST_CENT_NAME"] = ccName;

                objrow["APE_SUPPLIER_CODE"] = drpSupplier.SelectedValue;
                objrow["APE_SUPPLIER_NAME"] = drpSupplier.SelectedItem.Text;

                objrow["APE_AMOUNT"] = txtExpAmount.Text;

                ViewState["PaymentExpenses"] = DT;
                DT.Rows.Add(objrow);
                BindTempPaymentExpenses();




                txtExpAccount.Text = "";
                txtCostCenter.Text = "";


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddCost_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void PaymentSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTransNo = (Label)gvScanCard.Cells[0].FindControl("lblTransNo");

                txtTrnNumber.Text = lblTransNo.Text;
                txtTrnNumber_TextChanged(txtTrnNumber, new EventArgs());

                divGridPopup.Visible = false;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PaymentSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnPaymentPrint_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Prescription", "ShowPaymentPrintPDF('" + Convert.ToString(Session["Branch_ID"]) + "','" + txtTrnNumber.Text.Trim() + "');", true);

        }

        protected void btnChequePrint_Click(object sender, EventArgs e)
        {

            //string strReportName = "";


            //if (drpBank.SelectedIndex != 0)
            //{
            //    strReportName = GetChequeReportName();
            //}


            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Prescription", "ShowChequePrintPDF('" + Convert.ToString(Session["Branch_ID"]) + "','" + txtTrnNumber.Text.Trim() + "','" + strReportName + "');", true);

            string strRptPath = "../WebReports/ChequePrint.aspx";
            string rptcall = @strRptPath + "?TRANS_NO=" + txtTrnNumber.Text.Trim();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ChequePrint", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);


        }

        protected void btnCtlrRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
            Clear();
        }

        protected void imgPaymentZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindPaymentEntryGrid();
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            divGridPopup.Visible = true;
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string Criteria = " APE_TRANS_NO='" + txtTrnNumber.Text.Trim() + "'";
                objCom.fnDeleteTableData("AC_PAYMENT_EXPENSES", Criteria);
                objCom.fnDeleteTableData("AC_PAYMENT_ENTRY", Criteria);


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' Data Deleted ','Data Deleted ')", true);
                Clear();
                txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void drpSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND  ASM_CODE='" + drpSupplier.SelectedValue + "'";

            SuppliertMasterBAL objMast = new SuppliertMasterBAL();
            DS = objMast.SuppliertMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtExpAccount.Text = Convert.ToString(DS.Tables[0].Rows[0]["ASM_ACCOUNT_CODE"]) + " ~ " + Convert.ToString(DS.Tables[0].Rows[0]["ASM_ACCOUNT_NAME"]);

            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtTrnNumber.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_PAYMENT");
            Clear();
        }
    }
}