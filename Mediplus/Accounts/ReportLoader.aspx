﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"  AutoEventWireup="true" CodeBehind="ReportLoader.aspx.cs" Inherits="Mediplus.Accounts.ReportLoader" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>


    <script src="../../Scripts/Validation.js" type="text/javascript"></script>

     
    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

       <script>
           function ShowReports() {

               var PageName = document.getElementById('<%=hidPageName.ClientID%>').value;
                if (PageName == "Ledger") {
                    Ledger()

                }
                else if (PageName == "PAndLAccount") {
                    PAndLAccount()

                }
                else if (PageName == "BalanceSheet") {
                    BalanceSheet()

                }
                else if (PageName == "TrialBalance") {
                    TrialBalance()

                }
              

            }
           function Ledger() {
               var Report = "";


               var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var AccountName = document.getElementById('<%=txtAccountName.ClientID%>').value
            var AccountCode = '';
            var arrAccountName = AccountName.split('~');

            if (arrAccountName.length > 1) {

                AccountCode = arrAccountName[0]
            }


            Report = "Ledger.rpt";



            var Criteria = " 1=1 ";


            if (AccountCode != "") {

                Criteria += ' AND {AC_JOURNAL_TRANSACTION.AJT_ACC_CODE}=\'' + AccountCode + '\'';
            }


            Criteria += ' AND  {AC_JOURNAL_MASTER.AJM_DATE}>=date(\'' + Date1 + '\') AND  {AC_JOURNAL_MASTER.AJM_DATE}<=date(\'' + Date2 + '\')'


            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }


        function PAndLAccount() {
            var Report = "";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            Report = "ProfitAndLossAccount.rpt";



            var Criteria = " 1=1 ";




            // Criteria += ' AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function BalanceSheet() {
            var Report = "";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            Report = "BalanceSheet.rpt";



            var Criteria = " 1=1 ";




            // Criteria += ' AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }
        function TrialBalance() {
            var Report = "";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            Report = "TrialBalance.rpt";



            var Criteria = " 1=1 ";




            //Criteria += ' AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }
 
    </script>

      <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

     <style type="text/css">
        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .modalPopup
        {
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px 10px 10px 10px;
        }
    </style>

     </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidPageName" runat="server" value="Report" />
    <input type="hidden" id="hidPerCheck" runat="server" value="true" />
      <table style="width: 100%;">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblPageHeader" runat="server" CssClass="lblCaption1" Font-Size="15" Text="Report"></asp:Label>
                    </td>
                    <td align="right">
                       

                    </td>
                </tr>
            </table>
  
      <table width="900px" cellpadding="5" cellspacing="5">
            <tr>
                <td style="width: 100px; height: 30px;"  >
                    <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                        Text="From"></asp:Label><span style="color:red;">* </span>
                </td>
                <td style="width: 400px" colspan="3">
                    <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" CssClass="TextBox3DStyle" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                        Text="To"></asp:Label>
                      &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" CssClass="TextBox3DStyle" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                </td>

            </tr>
          <tr>
                        <td class="lblCaption1" style="width: 100px;">Accounts
                        </td>
                        <td colspan="5">
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAccountName" runat="server" CssClass="TextBox3DStyle" Width="100%" Height="20px"></asp:TextBox>
                                    <div id="divwidth" style="visibility: hidden;"></div>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtAccountName" MinimumPrefixLength="1" ServiceMethod="GetAccountList"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                    </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>

          </table>
    
      <table   cellpadding="5" cellspacing="5"  style="width:80%;"  >

        <tr>

            <td >
                <input type="button" id="btnShowReport" runat="server" style="width: 150px;" class="button red small" value="Show Report" onclick="ShowReports()" />

                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="150px" CssClass="button gray small"  OnClick="btnClear_Click"/>



            </td>

        </tr>
    </table>
 </asp:Content>
  
 
