﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;


namespace Mediplus.Accounts
{
    public partial class MasterList : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindAccountMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


          MastersBAL  objMast = new MastersBAL();
            DS = objMast.AccountMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvAccountMaster.DataSource = DS;

                gvAccountMaster.DataBind();
            }
            else
            {
                gvAccountMaster.DataBind();
            }


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                hidCtrlName.Value =  Convert.ToString(Request.QueryString["CtrlName"]);

                BindAccountMaster();
            }
        }

       
    }
}