﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.Insurance
{
    public partial class RemittanceReverse : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {


            }
        }

      

        protected void btnReceiptReverse_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileLogo.FileName.ToLower() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Receipt", "ShowErrorMessage('  SELECT REMITTANCE FILE .',' Please Select Remittance File ','Red')", true);

                    goto FunEnd;
                }
                CommonBAL objCom = new CommonBAL();
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                SqlCommand cmd1 = new SqlCommand();

                con.Open();
                cmd1 = new SqlCommand();
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.CommandTimeout = 864000;
                cmd1.CommandText = "HMS_SP_RemittanceReverseUpdate";
                cmd1.Parameters.Add(new SqlParameter("@HRM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd1.Parameters.Add(new SqlParameter("@ActualFileName", SqlDbType.VarChar)).Value = fileLogo.FileName.ToLower();
                cmd1.Parameters.Add(new SqlParameter("@CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]).Trim(); ;
                cmd1.ExecuteNonQuery();
                con.Close();



                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Remittance", "ShowErrorMessage('  RECEIPT GENERATED.',' Receipt Reverse  Completed.','Green')", true);


       

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnReceiptReverse_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

    }
}