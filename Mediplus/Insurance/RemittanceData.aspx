﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="RemittanceData.aspx.cs" Inherits="Mediplus.Insurance.RemittanceData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }


        #divDiagServ
        {
            width: 400px !important;
        }

            #divDiagServ div
            {
                width: 400px !important;
            }


        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>
    <script language="javascript" type="text/javascript">
        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function ShowReceiptAddPopup() {

            document.getElementById("divReceiptAdd").style.display = 'block';


        }

        function HideReceiptAddPopup() {

            document.getElementById("divReceiptAdd").style.display = 'none';
           
        }

        function SetPendingAmount() {
            var Payment = document.getElementById("<%=txtRemiPaymentAmount.ClientID%>").value;
            var Received = document.getElementById("<%=txtReceivedAmount.ClientID%>").value;

            var Pending = parseFloat(Payment) - parseFloat(Received)


            document.getElementById("<%=txtPendingAmount.ClientID%>").value = Pending;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table>
        <tr>
            <td class="PageHeader">Bank Reconciliation
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <table width="80%">
        <tr>
            <td>
                <div class="lblCaption1" style="width: 100%; height: 150px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">


                    <table width="100%">
                        <tr>
                            <td class="lblCaption1">From  Date
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txteAuthFrmDt" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                    Enabled="True" TargetControlID="txteAuthFrmDt" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txteAuthFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            </td>

                            <td class="lblCaption1">To Date
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txteAuthToDt" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                    Enabled="True" TargetControlID="txteAuthToDt" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txteAuthToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Company
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" Height="22px" Width="100px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" Height="22px" Width="250px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>
                                        <div id="divComp" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                                        </asp:AutoCompleteExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Reference #
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtReferenceNo" CssClass="TextBoxStyle" runat="server" Height="22px" Width="200px"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                            <td>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button orange small"
                                            OnClick="btnSearch_Click" Text="Search" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td></td>
                        </tr>


                    </table>
                </div>
            </td>

        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvRemittanceData" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" PageSize="200" GridLines="Vertical">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="Select_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SENDER ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                              <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("ActualFileName") %>' Visible="false"></asp:Label>

                                            <asp:Label ID="lblSenderID" CssClass="GridRow" runat="server" Text='<%# Bind("SenderID") %>'></asp:Label>


                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyID") %>'></asp:Label>
                                            <asp:Label ID="lblCompanyName" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PROVIDER" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>

                                            <asp:Label ID="lblProviderID" CssClass="GridRow" runat="server" Text='<%# Bind("ProviderID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REFERENCE NO" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>


                                            <asp:Label ID="lblRefNo" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentReference") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCount" CssClass="GridRow" runat="server" Text='<%# Bind("Count") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYER SHARE" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PATIENT SHARE" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYMENT AMOUNT" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblPaymentAmount" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentAmount") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>




                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

    </div>


    <div id="divReceiptAdd" style="display: none; overflow: hidden; border: groove; height: 420px; width: 950px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 200px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideReceiptAddPopup()" />
                </td>
            </tr>
        </table>

        <table style="width: 100%;" cellpadding="5" cellspacing="5" border="0">
            <tr>
                <td class="lblCaption1" style="width: 100px;"> Remi. File Name 
                </td>
                <td colspan="6">
                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblRemiFileName" CssClass="lblCaption1" runat="server" ></asp:Label>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px;">Sender ID <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRemiSenderID" CssClass="TextBoxStyle" Enabled="false" runat="server" Height="22px" Width="200px"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Company
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRemiCompany" runat="server" Enabled="false" CssClass="TextBoxStyle" Height="22px" Width="100px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtRemiCompanyName" runat="server" Enabled="false" CssClass="TextBoxStyle" Height="22px" Width="200px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px;">Reference No <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRemiReferenceNo" CssClass="TextBoxStyle" Enabled="false" runat="server" Height="22px" Width="200px"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


            </tr>
            <tr>

            <tr>
                <td class="lblCaption1" style="width: 100px;">Trn. Number
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtJournalNo" runat="server" CssClass="TextBoxStyle" Width="200px" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td class="lblCaption1">Trans. Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransDate" runat="server" CssClass="TextBoxStyle" Width="100px" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Pay.Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpPayType" runat="server" CssClass="TextBoxStyle" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="drpPayType_SelectedIndexChanged">
                                <asp:ListItem Text="Cash" Value="Cash" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Bank" Value="Bank"></asp:ListItem>
                                <asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px;">Payment Amt. <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRemiPaymentAmount" CssClass="TextBoxStyle" Enabled="false" runat="server" Style="text-align: right; padding-right: 5px;" Height="22px" Width="200px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 100px;">Received Amt. <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtReceivedAmount" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Height="22px" Width="100px" onkeyup="SetPendingAmount();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 100px;">Pending Amt. <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPendingAmount" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Height="22px" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px;">Description 
                </td>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="TextBoxStyle" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
            <ContentTemplate>
                <div id="divBank" runat="server" visible="false" style="width: 100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td class="lblCaption1" style="width: 105px;">Payment Date</td>
                            <td style="width: 125px;">
                                
                                        <asp:TextBox ID="txtBnkPayDate" runat="server" CssClass="TextBoxStyle" Width="100px" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender5" runat="server"
                                            Enabled="True" TargetControlID="txtBnkPayDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                   
                            </td>
                            <td class="lblCaption1" style="width: 70px;">Amount
                            </td>
                            <td>
                                
                                        <asp:TextBox ID="txtBnkAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                  
                            </td>
                            <td class="lblCaption1">Bank RefNo
                            </td>
                            <td>
                                <asp:TextBox ID="txtBnkRefNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </td>

                        </tr>
                       
                        <tr>
                            <td class="lblCaption1">Bank
                            </td>
                            <td>
                                <asp:DropDownList ID="drpBank" CssClass="TextBoxStyle" runat="server" Width="200px"></asp:DropDownList>
                            </td>
                        </tr>
                        

                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
            <ContentTemplate>
                <div id="divCheque" runat="server" visible="false" style="width: 100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td class="lblCaption1" style="width: 105px;">Cheq. No.
                            </td>
                            <td>
                                <asp:TextBox ID="txtCheqNo" runat="server" Width="200px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </td>
                            <td class="lblCaption1" style="width: 70px;">Cheq. Amt.
                            </td>
                            <td>
                                <asp:TextBox ID="txtCheqAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Cheq. Date
                            </td>
                            <td>
                                <asp:TextBox ID="txtCheqDate" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                    Enabled="True" TargetControlID="txtCheqDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>

                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Bank
                            </td>
                            <td>
                                <asp:DropDownList ID="drpCheqBank" CssClass="TextBoxStyle" runat="server" Width="200px"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <table style="width: 100%;">
            <tr>
                <td style="width: 105px;"></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 100px;" CssClass="orange"
                                OnClick="btnSave_Click" Text="Save" OnClientClick="return ServiceAddVal();" />


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>


    <br />
</asp:Content>
