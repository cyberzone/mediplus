﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="eClaim.aspx.cs" Inherits="Mediplus.Insurance.eClaim" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function BindCompanyDtls(CompanyId, CompanyName) {

            document.getElementById("<%=txtCompany.ClientID%>").value = CompanyId;
            document.getElementById("<%=txtCompanyName.ClientID%>").value = CompanyName;

        }

        function InsurancePopup(PageName, strValue, evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode == 126) {
                var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
                return false;
            }
            return true;
        }

        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];


                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();


                }
            }

            return true;
        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }



        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if ('<%=radGeneratedType.SelectedValue%>' == "DB") {

                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Date";
                    document.getElementById('<%=txtFromDate.ClientID%>').focus;
                    return false;
                }

                if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtFromDate.ClientID%>').focus()
                    return false;
                }


                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Date";
                    document.getElementById('<%=txtToDate.ClientID%>').focus;
                    return false;
                }

                if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtToDate.ClientID%>').focus()
                      return false;
                }

               

                if (InvType == "Credit") {

                    if (document.getElementById('<%=txtCompany.ClientID%>').value == "") {
                        document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company";
                        document.getElementById('<%=txtCompany.ClientID%>').focus;
                        return false;
                    }

                    if (document.getElementById('<%=txtCompanyName.ClientID%>').value == "") {
                        document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company";
                        document.getElementById('<%=txtCompanyName.ClientID%>').focus;
                        return false;
                    }
                }

             }

             return true;
         }



         function ValClaimGenerate() {

             var TotalClaims = document.getElementById('<%=lblTotalClaims.ClientID%>').innerHTML;
            var TotalAmount = document.getElementById('<%=lblTotalAmount.ClientID%>').innerHTML


            var isClaimGenerate = window.confirm('Total No. of claims : ' + TotalClaims + ' Total Amount : ' + TotalAmount + ' Do you want to Continue ? ');
            document.getElementById('<%=hidClaimGenerate.ClientID%>').value = isClaimGenerate;

            return true;
        }



        function ShowExportMsg(strName) {

            alert('eClaim Excel Exported - File Name is eClaimExcel' + strName + '.xls');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <input type="hidden" id="hidPermission" runat="server" value="9" />
        <input type="hidden" id="hidClaimGenerate" runat="server" />
        <input type="hidden" id="hidServCodesNotSendClaim" runat="server" value="OTHERS|1111|0000" />

        <table>
            <tr>
                <td class="PageHeader">eClaim
                </td>
            </tr>
        </table>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>


        <asp:HiddenField ID="hidIsEclaimSub" runat="server" />
        <div class="lblCaption1" style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
            <table width="900px" cellpadding="3" cellspacing="3">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                            Text="From"></asp:Label><span style="color: red;">* </span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                            Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                        <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                            Text="To"></asp:Label>

                        <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                            Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                    </td>
                    <td class="lblCaption1">Invoice Type
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="85px" onChange="ShowInvTypeMessage()">
                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                    <asp:ListItem Value="Credit" Selected="True">Credit</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">

                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                Submi. Status &nbsp;&nbsp;
                                <asp:DropDownList ID="drpSubmissionStatus" runat="server" CssClass="TextBoxStyle" Width="120px">
                                    <asp:ListItem Text="--- All ---" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Pending" Value="Pending" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Submitted" Value="Submitted"></asp:ListItem>
                                </asp:DropDownList>



                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="chkOnlyVerifiedInvoice" runat="server" CssClass="lblCaption1" Text="Verified Invoice" Checked="false" />
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                            Text="Company"></asp:Label><span style="color: red;">* </span>

                    </td>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="10" onKeyUp="DoUpper(this);" onblur="return CompIdSelected()" onkeypress="return InsurancePopup('CompanyId',this.value,event)"></asp:TextBox>
                                <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50" onKeyUp="DoUpper(this);" onblur="return CompNameSelected()" onkeypress="return InsurancePopup('CompanyName',this.value,event)"></asp:TextBox>
                                <div id="divwidth" style="visibility: hidden;"></div>
                                <div id="div1" style="visibility: hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>

                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:Label ID="Label38" runat="server" CssClass="lblCaption1"
                            Text="Invoice No."></asp:Label>

                    </td>
                    <td>
                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" Width="250px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpPanelSearch" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small"
                                    OnClick="btnSearch_Click" Text="Search" />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSearch">
                            <ProgressTemplate>
                                <div id="overlay">
                                    <div id="modalprogress">
                                        <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="200" Width="350" />
                                    </div>
                                </div>

                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>

                </tr>
                <tr>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnClaimExportExcel" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 200px;" CssClass="button gray small"
                                    OnClick="btnClaimExportExcel_Click" Text="Claim Export To Excel" />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>


                <tr id="Tr1" runat="server" visible="false">
                    <td style="width: 120px">
                        <asp:Label ID="Label7" runat="server" CssClass="lblCaption1"
                            Text="Eclaim Generated Type"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="radGeneratedType" runat="server" CssClass="label" Width="150px" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="radGeneratedType_SelectedIndexChanged">
                            <asp:ListItem Text="DB" Value="DB" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="File" Value="File>"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="Panel1" runat="server" Width="300px">
                                    <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                                </asp:Panel>
                            </ContentTemplate>
                            <%--  <Triggers>
                                <asp:PostBackTrigger ControlID="btnSearch" />
                            </Triggers>--%>
                        </asp:UpdatePanel>
                    </td>

                </tr>


            </table>
        </div>
        <div style="height: 5px;"></div>
        <div style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvClaims" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="10" OnRowDataBound="gvClaims_RowDataBound" GridLines="Horizontal">
                        <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                        <RowStyle CssClass="GridRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="SL." SortExpression="HPV_SEQNO" HeaderStyle-Width="30px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                <ItemTemplate>
                                    <asp:Label ID="lblSerial" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkInvResub" runat="server" CssClass="label" AutoPostBack="true" OnCheckedChanged="chkboxSelect_CheckedChanged" />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Show Service Dtls" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                        OnClick="SelectClaim_Click" />
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CLAIM ID">
                                <ItemTemplate>
                                    <asp:Label ID="lnlInvId" CssClass="GridRow" runat="server" Text='<%# Bind("ID") %>' Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lblStart" CssClass="GridRow" runat="server" Text='<%# Bind("Start") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COMPANY CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompId" CssClass="GridRow" runat="server" Text='<%# Bind("CompId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PAYERID">
                                <ItemTemplate>
                                    <asp:Label ID="lblPayerId" CssClass="GridRow" runat="server" Text='<%# Bind("PayerId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MEMBERID">
                                <ItemTemplate>
                                    <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("MemberID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Font-Bold="true" Text='<%# Eval("Net", "{0:0.00}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PT. SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lblPatientShare" CssClass="GridRow" runat="server" Text='<%# Eval("PatientShare", "{0:0.00}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lblGross" CssClass="GridRow" runat="server" Text='<%# Eval("Gross", "{0:0.00}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PATIENTID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblPatientID" CssClass="GridRow" runat="server" Text='<%# Bind("PatientID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PATIENT NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblPatientName" CssClass="GridRow" runat="server" Text='<%# Bind("PatientName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="EMIRATES ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmiratesIDNumber" CssClass="GridRow" runat="server" Text='<%# Bind("EmiratesIDNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubmissionStatus" CssClass="GridRow" Font-Bold="true" runat="server" Text='<%# Bind("ClaimSubmitted") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <table width="80%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpPanelGenerateFile" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnTestEclaim" runat="server" CssClass="button gray small" Width="150px" OnClientClick="return ValClaimGenerate();"
                                OnClick="btnTestEclaim_Click" Text="Test E-Claim" />

                            <asp:Button ID="btnProductionEclaim" runat="server" CssClass="button red small" Width="150px" OnClientClick="return ValClaimGenerate();"
                                OnClick="btnProductionEclaim_Click" Text="Production E-Claim" />

                            <asp:Button ID="btnSaveToDB" runat="server" CssClass="button red small" Width="150px"
                                OnClick="btnSaveToDB_Click" Text="Save to DB" Visible="false" />
                            <asp:CheckBox ID="chkSubmitted" runat="server" Text="UPDATE STATUS AS SUBMITTED" CssClass="lblCaption1" Font-Bold="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelGenerateFile">
                        <ProgressTemplate>
                            <div id="overlay">
                                <div id="modalprogress">
                                    <asp:Image ID="imgLoader1" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="200" Width="350" />
                                </div>
                            </div>

                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td class="style2">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label6" runat="server" CssClass="label" Font-Bold="true"
                                Text="TOTAL NO. OF CLAIMS :"></asp:Label>
                            &nbsp; 
                             <asp:Label ID="lblTotalClaims" runat="server" CssClass="label" Font-Bold="true" ForeColor="Brown"></asp:Label>
                            &nbsp;  &nbsp; &nbsp;  &nbsp;
                            <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                                Text="TOTAL AMOUNT :"></asp:Label>
                            &nbsp;
                            <asp:Label ID="lblTotalAmount" runat="server" CssClass="label" Font-Bold="true" ForeColor="Brown"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


            </tr>

        </table>
        <div style="height: 5px;"></div>
        <div style="padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvClaimTrans" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvGridView_RowDataBound"
                        EnableModelValidation="True" Width="100%">
                        <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                        <RowStyle CssClass="GridRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="CLAIM ID">
                                <ItemTemplate>
                                    <asp:Label ID="lnlInvId" CssClass="GridRow" runat="server" Text='<%# Bind("InvoiceID") %>' Font-Bold="true" Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SERV. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblCode2" CssClass="GridRow" runat="server" Text='<%# Bind("Code2") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="QTY">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuantity" CssClass="GridRow" runat="server" Text='<%# Eval("Quantity", "{0:0}")   %>' Width="50px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Net">
                                <ItemTemplate>
                                    <asp:Label ID="lblNet1" CssClass="GridRow" runat="server" Text='<%# Eval("Net1", "{0:0.00}") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PT. SHARE">
                                <ItemTemplate>
                                    <asp:Label ID="lblPatientShare" CssClass="GridRow" runat="server" Text='<%# Eval("PatientShare", "{0:0.00}") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PAYER SHARE">
                                <ItemTemplate>
                                    <asp:Label ID="lblNet" CssClass="GridRow" runat="server" Text='<%# Eval("Net", "{0:0.00}") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AMOUNT">
                                <ItemTemplate>
                                    <asp:Label ID="lblGross" CssClass="GridRow" runat="server" Text='<%# Eval("Gross", "{0:0.00}") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="START">
                                <ItemTemplate>
                                    <asp:Label ID="lblStart" CssClass="GridRow" runat="server" Text='<%# Bind("Start") %>' Width="130px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End">
                                <ItemTemplate>
                                    <asp:Label ID="lblEnd" CssClass="GridRow" runat="server" Text='<%# Bind("End") %>' Width="130px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="START TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartType" CssClass="GridRow" runat="server" Text='<%# Bind("StartType") %>' Width="200px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="END TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndType" CssClass="GridRow" runat="server" Text='<%# Bind("EndType") %>' Width="200px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" CssClass="GridRow" runat="server" Text='<%# Bind("Type") %>' Width="200px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblType1" CssClass="GridRow" runat="server" Text='<%# Bind("Type1") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" CssClass="GridRow" runat="server" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblType3" CssClass="GridRow" runat="server" Text='<%# Bind("Type3") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblCode3" CssClass="GridRow" runat="server" Text='<%# Bind("Code3") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS2" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS2") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS2" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS2") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS3" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS3") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS3" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS3") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS4" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS4") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS4" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS4") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS5" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS5") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS5" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS5") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS6" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS6") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS6" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS6") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS7" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS7") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS7" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS7") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS8" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS8") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS8" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS8") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS9" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS9") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS9" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS9") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDS10" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS10") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS10" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS10") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblSICDS11" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS11") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS11" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS11") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS12") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS12" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS12") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS13") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeS13" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS13") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS14") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS14") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label14" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS15") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS15") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label16" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS16") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label17" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS16") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label18" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS17") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label19" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS17") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label20" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS18") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label21" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS18") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label22" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS19") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label23" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS19") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label24" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS20") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label25" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS20") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label26" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS21") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label27" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS21") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label28" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS22") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label29" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS22") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label30" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS23") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label31" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS23") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label32" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS24") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label33" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS24") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label34" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS25") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label35" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS25") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="Label36" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS26") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DIG. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="Label37" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS26") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>








                            <asp:TemplateField HeaderText="ADMITTING TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDA" CssClass="GridRow" runat="server" Text='<%# Bind("ICDA") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ADMITTING CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblICDCodeA" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeA") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="ADMITTING START">
                                <ItemTemplate>
                                    <asp:Label ID="lblStart1" CssClass="GridRow" runat="server" Text='<%# Bind("Start1") %>' Width="130px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ACTIVITY TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblType2" CssClass="GridRow" runat="server" Text='<%# Bind("Type2") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PRIORAUTH. ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblPriorAuthorizationId" CssClass="GridRow" runat="server" Text='<%# Bind("PriorAuthorizationId") %>' Width="100px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OBSERV. TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblObservType" CssClass="GridRow" runat="server" Text='<%# Bind("ObservType") %>' Width="130px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OBSERV. CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblObservCode" CssClass="GridRow" runat="server" Text='<%# Bind("ObservCode") %>' Width="130px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OBSERV. VALUE">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("ObservValue") %>' Width="130px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>



                        </Columns>

                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>


    </div>
    <br />
    <br />
    <br />
</asp:Content>
