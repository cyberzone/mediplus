﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Insurance
{
    public partial class BatchSubmission : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindSubmissionData()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND UploadStatus='UPLOADED' ";
            string strStartDate = txtFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EClaimTransDate,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EClaimTransDate,101),101) <= '" + strForToDate + "'";
            }

            if (txtReceiverID.Text.Trim() != "")
            {
                Criteria += " AND  ReceiverID='" + txtReceiverID.Text.Trim() + "'";
            }

            if (txtBatchNo.Text.Trim() != "")
            {
                Criteria += " AND  EClaimTransID='" + txtBatchNo.Text.Trim() + "'";
            }


            if (txtFileName.Text.Trim() != "")
            {
                Criteria += " AND  EClaimFileName like'" + txtFileName.Text.Trim() + "%'";
            }

            if (txtCompany.Text.Trim() != "")
            {
                Criteria += " AND BillToCode='" + txtCompany.Text.Trim() + "'";
            }


            DS = objCom.EclaimXMLDataMasterBatchWise(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvSubmissionData.Visible = true;
                gvSubmissionData.DataSource = DS;
                gvSubmissionData.DataBind();
            }
            else
            {
                gvSubmissionData.Visible = false;
                gvSubmissionData.DataBind();
            }
        }

        void BindSubmissionClaims(string EClaimTransID)
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND UploadStatus='UPLOADED' ";

            Criteria += " AND EClaimTransID='" + EClaimTransID + "'";



            DS = objCom.EclaimXMLDataMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvSubmissionClaims.Visible = true;
                gvSubmissionClaims.DataSource = DS;
                gvSubmissionClaims.DataBind();
            }
            else
            {
                gvSubmissionClaims.Visible = false;
                gvSubmissionClaims.DataBind();
            }
        }

        void Clear()
        {
            gvSubmissionData.Visible = false;
            gvSubmissionData.DataBind();

            gvSubmissionClaims.Visible = false;
            gvSubmissionClaims.DataBind();
        }
        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' ";
            Criteria += " AND HCM_COMP_ID Like '" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A'   ";
            Criteria += " AND HCM_NAME Like '" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            //  lblStatus.Text = "";
            try
            {
                if (!IsPostBack)
                {
                   

                    txtFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    BindSubmissionData();
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "BatchSubmission_Page_Load");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

                BindSubmissionData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "BatchSubmission_btnSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
              
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvSubmissionData.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblTransID = (Label)gvScanCard.Cells[0].FindControl("lblTransID");

                BindSubmissionClaims(lblTransID.Text);



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BatchSubmission.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}