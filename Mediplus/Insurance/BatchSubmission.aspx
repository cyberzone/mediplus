﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="BatchSubmission.aspx.cs" Inherits="Mediplus.Insurance.BatchSubmission" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        .auto-style1
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            height: 25px;
            width: 242px;
        }

        .auto-style2
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            width: 242px;
        }
    </style>


    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                     document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                 }


             }

             return true;
         }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td class="PageHeader">Batch Submission
            </td>
        </tr>
    </table>
    <div class="lblCaption1" style="width: 80%; height: 80px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <table cellspacing="5" cellpadding="5" width="100%">
            <tr>
                <td class="lblCaption1">File Name 

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFileName" runat="server" CssClass="TextBoxStyle" Width="300px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


                <td class="lblCaption1">From Date 
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFrmDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtFrmDt" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1">To Date
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtToDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtToDt" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td></td>
            </tr>
            <tr>
                <td class="lblCaption1">Company
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" Height="22px" Width="100px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" Height="22px" Width="250px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>
                            <div id="divComp" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px; width: 100px;">Receiver ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtReceiverID" runat="server" CssClass="TextBoxStyle" Width="75px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Batch #

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtBatchNo" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpPanelSearch" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small"
                                OnClick="btnSearch_Click" Text="Search" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSearch">
                        <ProgressTemplate>
                            <div id="overlay">
                                <div id="modalprogress">
                                    <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="200" Width="350" />
                                </div>
                            </div>

                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>

    <div style="padding-top: 0px; width: 80%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <asp:UpdatePanel ID="UpPanelSubmissionData" runat="server">
            <ContentTemplate>

                <asp:GridView ID="gvSubmissionData" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" GridLines="Both">
                    <HeaderStyle CssClass="GridHeader_Blue" HorizontalAlign="Center" />
                    <FooterStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                    OnClick="Select_Click" />&nbsp;&nbsp;
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FILE NAME" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblEClaimFileName" CssClass="GridRow" runat="server" Text='<%# Bind("EClaimFileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BATCH #" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblTransID" CssClass="GridRow" runat="server" Text='<%# Bind("EClaimTransID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblApprovalNo" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PROVIDER" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblProviderID" CssClass="GridRow" runat="server" Text='<%# Bind("ProviderID") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EXECUTION DATE" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>


                                <asp:Label ID="lblInsName" CssClass="GridRow" runat="server" Text='<%# Bind("EClaimTransDateDesc") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblTotalQty" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("Count") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>


                                <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PATIENT SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>


                                <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>


                                <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>






                    </Columns>


                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSubmissionData">
            <ProgressTemplate>
                <div id="overlay">
                    <div id="modalprogress">
                        <asp:Image ID="imgLoaderSubmissionData" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="200" Width="350" />
                    </div>
                </div>

            </ProgressTemplate>
        </asp:UpdateProgress>

    </div>
    <br />

    <div style="padding-top: 0px; width: 80%; height: 350px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvSubmissionClaims" runat="server" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" PageSize="200">
                    <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>

                        <asp:TemplateField HeaderText="CLAIM ID" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblEClaimFileName" CssClass="GridRow" runat="server" Text='<%# Bind("EClaimFileName") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblClaimID" CssClass="GridRow" runat="server" Font-Bold="true" Text='<%# Bind("ClaimId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyCode") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblCompanyName" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblCount" CssClass="GridRow" runat="server" Text='<%# Bind("RecordCount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>


                                <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PATIENT SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>


                                <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
