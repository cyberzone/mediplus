﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using Mediplus_BAL;
using System.IO;
using System.Data;

namespace Mediplus.Insurance
{
    public partial class Resubmission : System.Web.UI.Page
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void XMLLogFileWriting(string strContent)
        {
            try
            {
                // string strName = "EClaimErrorLog" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".txt";
                string strName = GlobalValues.Shafafiya_ReSubEClaim_Error_FilePath + Convert.ToString(ViewState["LogFileName"]) + ".txt"; ;

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }
        }

        void BindData()
        {

            string Criteria = "  1=1   ";
            string strStartDate = txtFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) <= '" + strForToDate + "'";
            }


            if (txtFileName.Text.Trim() != "")
            {
                Criteria = "   ActualFileName like'" + txtFileName.Text.Trim() + "%'";
            }


            if (txtCompany.Text.Trim() != "")
            {
                // Criteria += " AND HCM_BILL_CODE='" + txtCompany.Text.Trim() + "'";
                Criteria += " AND SenderID  IN ( SELECT HCM_FIELD5 FROM HMS_COMPANY_MASTER WHERE  HCM_BILL_CODE='" + txtCompany.Text.Trim() + "' )";
            }


            if (txtSrcReferenceNo.Text.Trim() != "")
            {
                Criteria += " AND PaymentReference='" + txtSrcReferenceNo.Text.Trim() + "'";
            }

            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.RemittanceXMLDataGroupByGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceData.Visible = true;
                gvRemittanceData.DataSource = DS;
                gvRemittanceData.DataBind();

            }
            else
            {
                gvRemittanceData.Visible = false;
                gvRemittanceData.DataBind();
                lblStatus.Text = " No data";
            }


        }


        void BindRemittanceClaims(string FileName)
        {

            string Criteria = "  1=1   ";

            // Criteria += " AND PaymentReference='" + RefNo + "'";

            if (FileName != "")
            {
                Criteria += " AND XMLFILENAME='" + FileName + "'";
            }

            string strStartDate = txtClaimFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtClaimFrmDt.Text != "")
            {
                // Criteria += " AND   CONVERT(datetime,convert(varchar(10),Start1,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtClaimToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtClaimToDt.Text != "")
            {
                // Criteria += " AND   CONVERT(datetime,convert(varchar(10),Start1,101),101) <= '" + strForToDate + "'";
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtCompany.Text.Trim() != "")
            {
                Criteria += " AND HIM_BILLTOCODE='" + txtCompany.Text.Trim() + "'";
                // Criteria += " AND SenderID  IN ( SELECT HCM_FIELD5 FROM HMS_COMPANY_MASTER WHERE  HCM_BILL_CODE='" + txtCompany.Text.Trim() + "' )";
            }



            if (txtClaimID.Text != "")
            {
                string strIsuNos = "";
                strIsuNos = txtClaimID.Text.Trim();

                string[] arrIsuNos = strIsuNos.Split(',');

                string strIsuNo = "";
                for (int i = 0; i < arrIsuNos.Length; i++)
                {
                    strIsuNo += "'" + arrIsuNos[i] + "',";
                }
                if (strIsuNo.Length > 1)
                    strIsuNo = strIsuNo.Substring(0, strIsuNo.Length - 1);

                Criteria += " AND ClaimId IN (" + strIsuNo + ")";
            }


            if (drpClaimStatus.SelectedIndex != 0)
            {
                if (drpClaimStatus.SelectedValue == "Pending")
                {
                    Criteria += " AND RESUBSTATUS IS NULL ";
                }
                else
                {
                    Criteria += " AND ReSubStatus='" + drpClaimStatus.SelectedValue + "'";
                }
            }

            if (chkShowRejectInv.Checked == true)
            {
                Criteria += " AND DenialCode IS NOT NULL   ";
            }

            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.RemittanceXMLDataClaimWise(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceClaims.Visible = true;
                gvRemittanceClaims.DataSource = DS;
                gvRemittanceClaims.DataBind();

            }
            else
            {
                gvRemittanceClaims.DataBind();
            }

        }

        void BindRemittanceClaimTrans(string RefNo, string ClaimID)
        {

            string Criteria = "  1=1   ";

            Criteria += " AND PaymentReference='" + RefNo + "' AND ClaimID='" + ClaimID + "'";


            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.RemittanceXMLDataGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceClaimTrans.Visible = true;
                gvRemittanceClaimTrans.DataSource = DS;
                gvRemittanceClaimTrans.DataBind();

            }
            else
            {
                gvRemittanceClaimTrans.DataBind();
            }

        }


        void BindInvoiceTran(string ClaimID)
        {
            string Criteria = " 1=1 ";

            DataColumn TransReComment = new DataColumn();



            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            Criteria = " 1=1 ";
            Criteria += " AND  HIT_INVOICE_ID='" + ClaimID + "'";
            DS = objInv.InvoiceTransResubGet(Criteria, txtResubNo.Text.Trim() == "" ? "0" : txtResubNo.Text.Trim());

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvTran.Visible = true;
                gvInvTran.DataSource = DS;
                gvInvTran.DataBind();

            }
            else
            {
                gvInvTran.DataBind();
            }

        }

        void BindResubTranHistory()
        {
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            DataColumn TransReComment = new DataColumn();
            dboperations dbo = new dboperations();
            Criteria = " 1=1 ";
            Criteria += " AND  HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            DS = dbo.ResubmissionTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvResubTransHistory.Visible = true;
                gvResubTransHistory.DataSource = DS;
                gvResubTransHistory.DataBind();

            }
            else
            {
                gvResubTransHistory.DataBind();
            }

        }

        void BindInvoiceDtls(string ClaimID)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HIM_INVOICE_ID='" + ClaimID + "'";

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" TOP 1 HIM_PT_ID,HIM_PT_NAME,HIM_POLICY_NO, HIM_AUTHID,HIM_ID_NO,HIM_DR_CODE", "HMS_INVOICE_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);

                txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);

                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_AUTHID"]);

                ViewState["INV_EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_EMR_ID"]);
                ViewState["InvDrCode"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);
            }
        }

        void BuildeInvoiceClaims()
        {

            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn HIC_ICD_CODE = new DataColumn();
            HIC_ICD_CODE.ColumnName = "HIC_ICD_CODE";

            DataColumn HIC_ICD_DESC = new DataColumn();
            HIC_ICD_DESC.ColumnName = "HIC_ICD_DESC";


            DataColumn HIC_ICD_TYPE = new DataColumn();
            HIC_ICD_TYPE.ColumnName = "HIC_ICD_TYPE";


            dt.Columns.Add(HIC_ICD_CODE);
            dt.Columns.Add(HIC_ICD_DESC);
            dt.Columns.Add(HIC_ICD_TYPE);

            ViewState["InvoiceClaims"] = dt;
        }

        void BindInvoiceClaimDtls(string ClaimID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  HIC_RESUBNO='" + txtResubNo.Text.Trim() + "'";

            dboperations dbo = new dboperations();

            DataSet DS = new DataSet();
            DS = dbo.ResubmissionClaimDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count <= 0)
            {
                Criteria = " 1=1 ";
                Criteria += " AND  HIC_INVOICE_ID='" + ClaimID + "'";
                DS = new DataSet();
                DS = dbo.InvoiceClaimDtlsGet(Criteria);
            }


            if (DS.Tables[0].Rows.Count > 0)
            {


                drpClmType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_TYPE"]);
                txtClmStartDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTDATEDesc"]);
                txtClmFromTime.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTTimeDesc"]);

                txtClmEndDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATEDesc"]);
                txtClmToTime.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATETimeDesc"]);


                drpClmStartType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTTYPE"]);
                drpClmEndType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDTYPE"]);





                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["InvoiceClaims"];

                    DataRow objrow;
                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();


                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);

                        objrow["HIC_ICD_TYPE"] = "Principal";


                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();



                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HIC_ICD_TYPE"] = "Admitting";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();


                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }




                }

                ViewState["InvoiceClaims"] = DT;

            }
        }

        void BindTempInvoiceClaims()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceClaims"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceDiag.Visible = true;
                gvInvoiceDiag.DataSource = DT;
                gvInvoiceDiag.DataBind();

            }
            else
            {
                gvInvoiceDiag.DataBind();
            }

        }

        void BuildeResubClaimsHistory()
        {

            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn HIC_RESUBNO = new DataColumn();
            HIC_RESUBNO.ColumnName = "HIC_RESUBNO";


            DataColumn HIC_ICD_CODE = new DataColumn();
            HIC_ICD_CODE.ColumnName = "HIC_ICD_CODE";

            DataColumn HIC_ICD_DESC = new DataColumn();
            HIC_ICD_DESC.ColumnName = "HIC_ICD_DESC";


            DataColumn HIC_ICD_TYPE = new DataColumn();
            HIC_ICD_TYPE.ColumnName = "HIC_ICD_TYPE";

            dt.Columns.Add(HIC_RESUBNO);
            dt.Columns.Add(HIC_ICD_CODE);
            dt.Columns.Add(HIC_ICD_DESC);
            dt.Columns.Add(HIC_ICD_TYPE);

            ViewState["ResubClaims"] = dt;
        }

        void BindResubClaimHistory()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND  HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ResubmissionClaimDtlsGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["ResubClaims"];

                    DataRow objrow;
                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);
                        objrow["HIC_RESUBNO"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_RESUBNO"]);
                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);

                        objrow["HIC_ICD_TYPE"] = "Principal";


                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HIC_RESUBNO"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_RESUBNO"]);
                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HIC_ICD_TYPE"] = "Admitting";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();
                                //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);
                                objrow["HIC_RESUBNO"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_RESUBNO"]);
                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }




                }

                ViewState["ResubClaims"] = DT;

            }
        }

        void BindResubClaimHistoryGrid()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ResubClaims"];
            if (DT.Rows.Count > 0)
            {
                gvResubClaimsHistory.Visible = true;
                gvResubClaimsHistory.DataSource = DT;
                gvResubClaimsHistory.DataBind();

            }
            else
            {
                gvResubClaimsHistory.DataBind();
            }

        }



        void BindResubType()
        {


            drpReType.Items.Insert(0, "--- Select ---");
            drpReType.Items[0].Value = "";

            drpReType.Items.Insert(1, "Correction");
            drpReType.Items[1].Value = "correction";

            drpReType.Items.Insert(2, "Internal Complaint");
            drpReType.Items[2].Value = "internal complaint";

            drpReType.Items.Insert(3, "Accepted");
            drpReType.Items[3].Value = "Accepted";


        }

        void ResubmissionGet(string strInvoiceID)
        {
            string Criteria = " 1=1  AND HIM_RESUB_LATEST=1 ";
            Criteria += " AND  HIM_INVOICE_ID='" + strInvoiceID + "'";

            DataSet DS = new DataSet();

            dboperations dbo = new dboperations();
            DS = dbo.ResubmissionGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);
                txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);
                ViewState["INV_EMR_ID"]= Convert.ToString(DS.Tables[0].Rows[0]["HIM_EMR_ID"]);
                ViewState["InvDrCode"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);

                txtResubNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["ReSubNo"]);
                // txtIDPayer.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REMARKS"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_AUTHID"]);

                //txtReComment.Text = Convert.ToString(DS.Tables[0].Rows[0]["ReComment"]);
                if (DS.Tables[0].Rows[0].IsNull("ReType") == false && Convert.ToString(DS.Tables[0].Rows[0]["ReType"]) != "")
                {
                    drpReType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["ReType"]);
                }
                lblFileName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_FILE_NAME"]);
                lblFileNameActual.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_FILE_NAME_ACTUAL"]);
                ViewState["AttachmentData"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ATTACHMENT"]);

                if (lblFileNameActual.Text != "")
                {
                    DeleteAttach.Visible = true;
                }
                else
                {
                    DeleteAttach.Visible = false;
                }
            }
            else
            {
                Criteria = " 1=1 ";

                Criteria += " AND HIM_INVOICE_ID='" + strInvoiceID + "'";

                DS = new DataSet();
                CommonBAL objCom = new CommonBAL();
                DS = objCom.fnGetFieldValue(" TOP 1 HIM_PT_ID,HIM_PT_NAME,HIM_POLICY_NO, HIM_AUTHID,HIM_ID_NO,HIM_EMR_ID,HIM_DR_CODE", "HMS_INVOICE_MASTER", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_ID"]);
                    txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);
                    txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);
                    ViewState["INV_EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_EMR_ID"]);
                    ViewState["InvDrCode"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);

                    txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                    txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_AUTHID"]);
                }
            }

        }

        void Clear()
        {


            gvInvTran.Visible = false;
            gvInvTran.DataBind();

            gvInvoiceDiag.Visible = false;
            gvInvoiceDiag.DataBind();

            gvResubTransHistory.Visible = false;
            gvResubTransHistory.DataBind();

            gvResubClaimsHistory.Visible = false;
            gvResubClaimsHistory.DataBind();

            gvEMR_PTMaster.Visible = false;
            gvEMR_PTMaster.DataBind();

            txtFileNo.Text = "";
            txtName.Text = "";
            txtIdNo.Text = "";
            ViewState["INV_EMR_ID"] = "";
            ViewState["InvDrCode"] = "";

            txtResubNo.Text = "";
            txtIDPayer.Text = "";
            txtPolicyNo.Text = "";
            // txtReComment.Text = "";
            drpReType.SelectedIndex = 0;
            txtPriorAuthorizationID.Text = "";

            txtInvoiceNo.Text = "";

            drpClmType.SelectedIndex = 0;
            txtClmStartDate.Text = "";
            txtClmFromTime.Text = "00:00";
            txtClmEndDate.Text = "";
            txtClmToTime.Text = "00:00";

            lblFileName.Text = "";
            lblFileNameActual.Text = "";

            txtReferenceNo.Text = "";


            ViewState["AttachmentData"] = "";
            DeleteAttach.Visible = false;
            // ViewState["PaymentReference"] = "";

            lblTotalClaims.Text = "0";
            lblTotalAmount.Text = "0";
        }

        void ClearSaveData()
        {
            gvInvTran.Visible = false;
            gvInvTran.DataBind();

            gvInvoiceDiag.Visible = false;
            gvInvoiceDiag.DataBind();


            txtResubNo.Text = "";
            txtIDPayer.Text = "";
            txtPolicyNo.Text = "";
            // txtReComment.Text = "";
            drpReType.SelectedIndex = 0;
            txtPriorAuthorizationID.Text = "";

            txtInvoiceNo.Text = "";

            drpClmType.SelectedIndex = 0;
            txtClmStartDate.Text = "";
            txtClmFromTime.Text = "00:00";
            txtClmEndDate.Text = "";
            txtClmToTime.Text = "00:00";

            lblFileName.Text = "";
            lblFileNameActual.Text = "";
            ViewState["AttachmentData"] = "";
            DeleteAttach.Visible = false;

            btnSaveToDB.Visible = true;


            TabContainer2.ActiveTab = TabServices;

        }

        void BindEMRPTMasterGrid()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            if (Convert.ToString(ViewState["INV_EMR_ID"]) != "" && Convert.ToString(ViewState["INV_EMR_ID"]) != "0")
            {
                Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";


            }
            else
            {
                if (txtEMR_PTMaster_FromDate.Text != "")
                {
                    Criteria += " AND   EPM_DATE >= CONVERT(datetime,'" + txtEMR_PTMaster_FromDate.Text + "',103)";
                }

                if (txtEMR_PTMaster_ToDate.Text != "")
                {
                    Criteria += " AND   EPM_DATE <= CONVERT(datetime,'" + txtEMR_PTMaster_ToDate.Text + "',103)";
                }
            }


            // Criteria += " AND EPM_INS_CODE='" + Convert.ToString(ViewState["SubInsCode"]) + "'";
            Criteria += " AND EPM_DR_CODE='" + Convert.ToString(ViewState["InvDrCode"]) + "'";
            Criteria += " AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "'";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.EMRPTMasterGet(Criteria);
            gvEMR_PTMaster.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvEMR_PTMaster.DataSource = DS;
                gvEMR_PTMaster.DataBind();
                gvEMR_PTMaster.Visible = true;
            }
            else
            {
                gvEMR_PTMaster.DataBind();
            }


        }

        DataSet GetServiceMaster(string ServCode)
        {

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            return DS;

        }

        Boolean TransferResubValidation()
        {
            string Criteria = "";

            dboperations dbo = new dboperations();
            DataSet DSInv = new DataSet();
            Criteria = " HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  ResubNo='" + txtResubNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DSInv = dbo.ResubmissionGet(Criteria);

            if (DSInv.Tables[0].Rows.Count <= 0)
            {
                lblStatus.Text = "Invoice Dtls not available ";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return false;
            }


            if (DSInv.Tables[0].Rows.Count > 0)
            {

                ////if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]).ToUpper() == "CASH" || Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]).ToUpper() == "CA")
                ////{
                ////    lblStatus.Text = " Cash Invoice can not Transfer";
                ////    lblStatus.ForeColor = System.Drawing.Color.Red;
                ////    return false;
                ////}


                if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_RESUB_VERIFY_STATUS"]) == "Completed")
                {
                    lblStatus.Text = " Already Transfered";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    return false;
                }

                ////Decimal decCompAmount = 0;

                ////if (DSInv.Tables[0].Rows[0].IsNull("HIM_CLAIM_AMOUNT") == false && Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]) != "")
                ////{

                ////    decCompAmount = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                ////}

                ////if (decCompAmount <= 0)
                ////{
                ////    lblStatus.Text = " Claim Amount should grater than zero ";
                ////    lblStatus.ForeColor = System.Drawing.Color.Red;
                ////    return false;
                ////}
            }





            if (txtClmStartDate.Text.Trim() == "" || txtClmFromTime.Text.Trim() == "")
            {
                lblStatus.Text = "Encounter Start Date & Time is empty ";

                return false;
            }

            if (txtClmEndDate.Text.Trim() == "" || txtClmToTime.Text.Trim() == "")
            {
                lblStatus.Text = "Encounter End Date & Time is empty";

                return false;
            }

            if (gvInvTran.Rows.Count <= 0)
            {
                lblStatus.Text = "Invoice Services is empty";

                return false;
            }

            Boolean boolPrincipal = false;

            if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
            {



                if (gvInvoiceDiag.Rows.Count <= 0)
                {
                    lblStatus.Text = "Please Add the Diagnosis ";
                    return false;
                }




                for (int i = 0; i < gvInvoiceDiag.Rows.Count; i++)
                {
                    Label lblICDType = (Label)gvInvoiceDiag.Rows[i].Cells[0].FindControl("lblICDType");
                    Label lblICDCode = (Label)gvInvoiceDiag.Rows[i].Cells[0].FindControl("lblICDCode");

                    if (lblICDType.Text == "Principal")
                    {
                        boolPrincipal = true;
                        goto EndForDiag;
                    }


                }
            EndForDiag: ;
                if (boolPrincipal == false)
                {
                    lblStatus.Text = "Please Add the Principal Diagnosis";
                    return false;
                }


            }
            /*
            string strDiagCodes = "";
            if (gvResubClaims.Rows.Count > 0)
            {
                for (int intCurRow = 0; intCurRow < gvResubClaims.Rows.Count; intCurRow++)
                {
                    Label lblResubICDCode = (Label)gvResubClaims.Rows[intCurRow].FindControl("lblResubICDCode");

                    if (lblResubICDCode.Text == "")
                    {
                        lblStatus.Text = "Please Enter Diagnosis Code";

                        return false;
                    }

                    if (strDiagCodes.Contains(lblResubICDCode.Text.Trim() + "|") == true)
                    {
                        lblStatus.Text = "Duplicate Diagnosis Code -" + lblResubICDCode.Text.Trim();

                        return false;
                    }

                    if (strDiagCodes != "")
                    {
                        strDiagCodes = strDiagCodes + lblResubICDCode.Text.Trim() + "|";
                    }
                    else
                    {
                        strDiagCodes = lblResubICDCode.Text.Trim() + "|";
                    }



                }
            }

            */


            for (int intCurRow = 0; intCurRow < gvInvTran.Rows.Count; intCurRow++)
            {
                Label lblServCode = (Label)gvInvTran.Rows[intCurRow].FindControl("lblServCode");
                TextBox txtServeDesc = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtServeDesc");

                TextBox txtStartDt = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtStartDt");

                Label lblHIOType = (Label)gvInvTran.Rows[intCurRow].FindControl("lblHIOType");
                TextBox txtHIOCode = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtHIOCode");

                TextBox txtHIOValue = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtHIOValue");
                TextBox txtHIOValueType = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtHIOValueType");

                DataSet DSServ = new DataSet();

                DSServ = GetServiceMaster(lblServCode.Text);

                String ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "", CodeType = "";

                decimal ObsConversion;
                if (DSServ.Tables[0].Rows.Count > 0)
                {



                    if (lblServCode.Text == "")
                    {
                        lblStatus.Text = "Please Enter Service Code";

                        return false;
                    }

                    if (lblServCode.Text == "")
                    {
                        lblStatus.Text = "Please Enter Service Code";

                        return false;
                    }


                    CodeType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);


                    ObsNeeded = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
                    if (ObsNeeded == "Y")
                    {
                        ObsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);//txtObsCode.Text =
                        ObsValueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                        ObsConversion = DSServ.Tables[0].Rows[0].IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]);

                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "DXB" && CodeType == "3")// && ServTrnt == "L")
                        {
                            ObsType = "Result";
                        }
                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "AUH" && CodeType == "3")// && ServTrnt == "L")
                        {

                            ObsType = "LOINC";

                        }
                        else if (CodeType == "6")
                        {
                            ObsType = "Universal Dental";
                        }


                        lblHIOType.Text = ObsType;
                        txtHIOCode.Text = ObsCode;


                    }
                }


                if (ObsNeeded.ToUpper() == "Y")    /// if (txtObsType.Text.Trim() != "")
                {
                    if (txtHIOValue.Text.Trim() == "")
                    {
                        lblStatus.Text = "Please Enter Observation Dtls for " + lblServCode.Text;

                        return false;

                    }

                }

                if (txtStartDt.Text == "")
                {
                    lblStatus.Text = "Please Enter Service Start Date";

                    return false;
                }



                if (txtClmStartDate.Text.Trim() != "" && txtClmFromTime.Text.Trim() != "")
                {

                    DateTime dtEncStartDate = DateTime.ParseExact(txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);//:ss

                    DateTime dtEncEndDate = DateTime.ParseExact(txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);//:ss

                    DateTime dtServStartDate = DateTime.ParseExact(txtStartDt.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);//



                    TimeSpan Diff = dtServStartDate - dtEncStartDate;

                    if (Diff.TotalMinutes < 0)
                    {
                        lblStatus.Text = "Service Date Between Encounter Start Date and End Date";

                        return false;
                    }

                    TimeSpan Diff1 = dtServStartDate - dtEncEndDate;

                    if (Diff1.TotalMinutes > 0)
                    {
                        lblStatus.Text = "Service Date Between Encounter Start Date and End Date";

                        return false;
                    }


                }
            }


            return true;
        }

        void CountClaimAmount()
        {
            decimal decTotalClaimAmount = 0;
            Int32 intTotalCount = 0;

            for (int i = 0; i < gvRemittanceClaims.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvRemittanceClaims.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblTotalNet = (Label)gvRemittanceClaims.Rows[i].Cells[0].FindControl("lblTotalNet");

                if (chkInvResub.Checked == true)
                {
                    intTotalCount += 1;

                    if (lblTotalNet.Text != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(lblTotalNet.Text);

                    }
                }

            }


            lblTotalClaims.Text = Convert.ToString(intTotalCount); ;
            lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

            //hidTotalClaims.Value = Convert.ToString(intTotalCount);
            //hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");
        }

        void XMLFileWrite(string DispositionFlag)
        {

            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strPath = GlobalValues.Shafafiya_ReSubEClaim_FilePath;

            string Criteria = " 1=1 ";
            Criteria += " AND HMS_RESUBMISSION_MASTER.ReType <> 'Accepted'";
            string strInvoiceID = "";

            for (int intCount = 0; intCount < gvRemittanceClaims.Rows.Count; intCount++)
            {


                CheckBox chkInvResub = (CheckBox)gvRemittanceClaims.Rows[intCount].FindControl("chkInvResub");


                Label lblClaimID = (Label)gvRemittanceClaims.Rows[intCount].FindControl("lblClaimID");

                if (chkInvResub.Checked == true)
                {
                    strInvoiceID += "'" + lblClaimID.Text + "',";
                }


            }

            if (strInvoiceID.Length > 0)
                strInvoiceID = strInvoiceID.Substring(0, strInvoiceID.Length - 1);

            if (strInvoiceID != "")
            {
                Criteria += " AND HIM_INVOICE_ID in( " + strInvoiceID + ")";
            }
            else
            {
                lblStatus.Text = "Please select the invoice ";
                lblStatus.ForeColor = System.Drawing.Color.Red;

                goto ClaimEnd;
            }

            // Criteria = ViewState["Criteria"].ToString();
            dboperations dbo = new dboperations();
            DataSet DS1 = new DataSet();
            DS1 = dbo.GetResubmissionViewGroupBy(Criteria);


            if (DS1.Tables[0].Rows.Count <= 0)
            {
                lblStatus.Text = "No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto ClaimEnd;
            }
            Boolean IsError = false;
            ViewState["LogFileName"] = "ReSubEClaimErrorLog" + strName + ".txt";


            StringBuilder strData = new StringBuilder();

            string strNewLineChar = "\n";

            Int32 ActCount = 0;

            strData.Append("<?xml version='1.0' encoding='US-ASCII'?>" + strNewLineChar);


            if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
            {
                strData.Append("<Claim.Submission xmlns:tns='http://www.eclaimlink.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='http://www.eclaimlink.ae/DataDictionary/CommonTypes/DataDictionary.xsd'>" + strNewLineChar);
            }
            else
            {
                //strData.Append("<Claim.Submission xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/DataDictionary.xsd'>");
                strData.Append("<Claim.Submission xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/ClaimSubmission.xsd' xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" + strNewLineChar);


            }


            // if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
            //{

            //    strData.Append("<Remittance.Advice xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/DataDictionary.xsd' xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' >");
            //}
            //else
            //{
            //    strData.Append("<Remittance.Advice xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/DataDictionary.xsd' xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' >");
            //}

            strData.Append("<Header>" + strNewLineChar);

            strData.Append("<SenderID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</SenderID>" + strNewLineChar); //GlobalValues.FacilityID
            strData.Append("<ReceiverID>" + Convert.ToString(DS1.Tables[0].Rows[0]["ReceiverID"]) + "</ReceiverID>" + strNewLineChar);
            strData.Append("<TransactionDate>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "</TransactionDate>" + strNewLineChar);
            strData.Append("<RecordCount>" + DS1.Tables[0].Rows.Count.ToString() + "</RecordCount>" + strNewLineChar);
            strData.Append("<DispositionFlag>" + DispositionFlag + "</DispositionFlag>" + strNewLineChar);
            strData.Append("</Header>" + strNewLineChar);


            for (Int32 i = 0; i < DS1.Tables[0].Rows.Count; i++)
            {

                strData.Append("<Claim>" + strNewLineChar);
                //if (DS.Tables[0].Rows[0].IsNull("MemberID") == true)
                //{
                //    IsError = true;
                //    TextFileWriting("Invalid or blank MemberID");
                //    // goto ClaimEnd;
                //}

                strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + "</ID>" + strNewLineChar);
                strData.Append("<IDPayer>" + Convert.ToString(DS1.Tables[0].Rows[i]["IDPayer"]) + "</IDPayer>" + strNewLineChar);
                strData.Append("<MemberID>" + Convert.ToString(DS1.Tables[0].Rows[i]["MemberID"]) + "</MemberID>" + strNewLineChar);
                strData.Append("<PayerID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PayerID"]) + "</PayerID>" + strNewLineChar);
                strData.Append("<ProviderID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</ProviderID>" + strNewLineChar);//GlobalValues.FacilityID 
                if (DS1.Tables[0].Rows[i].IsNull("EmiratesIDNumber") == false && Convert.ToString(DS1.Tables[0].Rows[i]["EmiratesIDNumber"]) != "")
                {
                    strData.Append("<EmiratesIDNumber>" + Convert.ToString(DS1.Tables[0].Rows[i]["EmiratesIDNumber"]) + "</EmiratesIDNumber>" + strNewLineChar);
                }
                else
                {
                    strData.Append("<EmiratesIDNumber>000-0000-0000000-0</EmiratesIDNumber>" + strNewLineChar);
                }
                strData.Append("<Gross>" + Convert.ToString(DS1.Tables[0].Rows[i]["Gross"]) + "</Gross>" + strNewLineChar);
                strData.Append("<PatientShare>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientShare"]) + "</PatientShare>" + strNewLineChar);
                strData.Append("<Net>" + Convert.ToString(DS1.Tables[0].Rows[i]["Net"]) + "</Net>" + strNewLineChar);

                strData.Append("<Encounter>");
                strData.Append("<FacilityID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</FacilityID>" + strNewLineChar); //GlobalValues.FacilityID 
                strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type"]) + "</Type>" + strNewLineChar);
                strData.Append("<PatientID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientID"]) + "</PatientID>" + strNewLineChar);

                if (Convert.ToString(Session["ECLAIM_TYPE"]) != "DXB")
                {
                    if (DS1.Tables[0].Rows[i].IsNull("EligibilityID") == false && DS1.Tables[0].Rows[i]["EligibilityID"].ToString() != "")
                    {
                        strData.Append("<EligibilityIDPayer>" + Convert.ToString(DS1.Tables[0].Rows[i]["EligibilityID"]) + "</EligibilityIDPayer>" + strNewLineChar);
                    }
                }
                
                
                if (DS1.Tables[0].Rows[i].IsNull("Start") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Start"]) != "")
                {
                    strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["Start"])).ToString("dd/MM/yyyy HH:mm") + "</Start>" + strNewLineChar);
                }
                else
                {
                    strData.Append("<Start></Start>" + strNewLineChar);
                }
                if (DS1.Tables[0].Rows[i].IsNull("End") == false && Convert.ToString(DS1.Tables[0].Rows[i]["End"]) != "")
                {
                    strData.Append("<End>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["End"])).ToString("dd/MM/yyyy HH:mm") + "</End>" + strNewLineChar);
                }
                else
                {
                    strData.Append("<End></End>" + strNewLineChar);
                }
                strData.Append("<StartType>" + Convert.ToString(DS1.Tables[0].Rows[i]["StartType"]) + "</StartType>" + strNewLineChar);
                strData.Append("<EndType>" + Convert.ToString(DS1.Tables[0].Rows[i]["EndType"]) + "</EndType>" + strNewLineChar);
                strData.Append("</Encounter>" + strNewLineChar);



                Boolean isDiagnosis = false;

                if (DS1.Tables[0].Rows[i].IsNull("Type1") == false && DS1.Tables[0].Rows[i].IsNull("Code") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Code"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type1"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code"]) + "</Code>" + strNewLineChar);

                    if (Convert.ToString(Session["ECLAIM_TYPE"]) != "DXB")
                    {
                        if (DS1.Tables[0].Rows[i].IsNull("HIC_YEAR_OF_ONSET_P") == false && DS1.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"].ToString() != "")
                        {
                            strData.Append("<DxInfo>");
                            strData.Append("<Type>Year of onset</Type>");
                            strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"]) + "</Code>");
                            strData.Append("</DxInfo>");
                        }
                    }


                    strData.Append("</Diagnosis>" + strNewLineChar);

                }



                if (DS1.Tables[0].Rows[i].IsNull("Type3") == false && DS1.Tables[0].Rows[i].IsNull("Code3") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Code3"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type3"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code3"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS2") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS2") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS2"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS2"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS2"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS3") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS3") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS3"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS3"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS3"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS4") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS4") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS4"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS4"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS4"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS5") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS5") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS5"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS5"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS5"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS6") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS6") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS6"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS6"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS6"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS7") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS7") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS7"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS7"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS7"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS8") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS8") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS8"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS8"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS8"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS9") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS9") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS9"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS9"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS9"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS10") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS10") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS10"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS10"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS10"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS11") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS11") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS11"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS11"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS11"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS12") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS12") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS12"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS12"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS12"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS13") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS13") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS13"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS13"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS13"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS14") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS14") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS14"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS14"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS14"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS15") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS15") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS15"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS15"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS15"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS16") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS16") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS16"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS16"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS16"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS17") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS17") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS17"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS17"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS17"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS18") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS18") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS18"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS18"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS18"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS19") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS19") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS19"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS19"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS19"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS20") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS20") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS20"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS20"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS20"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS21") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS21") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS21"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS21"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS21"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS22") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS22") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS22"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS22"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS22"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS23") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS23") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS23"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS23"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS23"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS24") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS24") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS24"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS24"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS24"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS25") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS25") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS25"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS25"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS25"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS26") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS26") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS26"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS26"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS26"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDA") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeA") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeA"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDA"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeA"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");

                }

                if (isDiagnosis == false)
                {
                    IsError = true;
                    XMLLogFileWriting("Invalid or blank Diagnosis  (InvoiceNo=" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + ")");
                    // goto ClaimEnd;
                }


                //   ResubmissionGet(Convert.ToString( DS1.Tables[0].Rows[i]["ID"])) ;
                //Criteria = ViewState["Criteria"].ToString();

                Criteria = " 1=1 ";
                Criteria += " AND HMS_RESUBMISSION_MASTER.ReType <> 'Accepted'";
                // Criteria += " AND HIM_INVOICE_ID=" + DS1.Tables[0].Rows[i]["ID"].ToString();
                // Criteria += " AND HMS_RESUBMISSION_MASTER.ReSubNo='" + Convert.ToString(ViewState["ReSubNo"]) + "'";
                Criteria += " AND HMS_RESUBMISSION_MASTER.ReSubNo='" + Convert.ToString(DS1.Tables[0].Rows[i]["ReSubNo"]) + "'";


                DataSet DS = new DataSet();
                DS = dbo.ResubmissionViewGet(Criteria);


                //DataTable DTActivity = new DataTable();
                //DataRow[] DRActivity;
                //DRActivity = DS1.Tables[0].Select("ID=" + DS.Tables[0].Rows[i]["ID"].ToString());

                string strResubComment = "";

                for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_RESUBMITTED"]) == "True")
                    {
                        ActCount = ActCount + 1;

                        if (DS.Tables[0].Rows[j].IsNull("Clinician") == true)
                        {
                            IsError = true;
                            XMLLogFileWriting("Invalid or blank Clinician  (InvoiceNo=" + Convert.ToString(DS.Tables[0].Rows[j]["ID"]) + ")");
                            // goto ClaimEnd;
                        }

                        if (DS.Tables[0].Rows[j].IsNull("Code2") == true)
                        {
                            IsError = true;
                            XMLLogFileWriting("Invalid or blank  Activity Code (InvoiceNo=" + Convert.ToString(DS.Tables[0].Rows[j]["ID"]) + ")");
                            // goto ClaimEnd;
                        }

                        strData.Append("<Activity>" + strNewLineChar);

                        if (Convert.ToString(DS1.Tables[0].Rows[i]["ReType"]) == "internal complaint")
                        {
                            strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + Convert.ToString(DS.Tables[0].Rows[j]["HIT_SLNO"]) + "</ID>" + strNewLineChar);
                        }
                        else if (Convert.ToString(DS1.Tables[0].Rows[i]["ReType"]) == "correction")
                        {
                            strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ReSubNo"]) + Convert.ToString(DS.Tables[0].Rows[j]["HIT_SLNO"]) + "</ID>" + strNewLineChar);
                        }


                        //strData.Append("<ID>" + ActCount + "</ID>" + strNewLineChar);

                        if (DS1.Tables[0].Rows[i].IsNull("Start") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Start"]) != "")
                        {
                            // strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS.Tables[0].Rows[j]["Start1"])).ToString("dd/MM/yyyy HH:mm") + "</Start>");
                            strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["Start"])).ToString("dd/MM/yyyy HH:mm") + "</Start>" + strNewLineChar);
                        }
                        else
                        {
                            strData.Append("<Start></Start>" + strNewLineChar);
                        }
                        strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["Type2"]) + "</Type>" + strNewLineChar);
                        strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["Code2"]) + "</Code>" + strNewLineChar);
                        strData.Append("<Quantity>" + Convert.ToString(DS.Tables[0].Rows[j]["Quantity"]) + "</Quantity>" + strNewLineChar);
                        strData.Append("<Net>" + Convert.ToString(DS.Tables[0].Rows[j]["Net1"]) + "</Net>" + strNewLineChar);
                        //strData.Append("<PatientShare>" + Convert.ToString(DS.Tables[0].Rows[j]["PatientShare"]) + "</PatientShare>" + strNewLineChar);
                        if (Convert.ToString(Session["ECLAIM_TYPE"]) != "DXB")
                        {
                            if (GlobalValues.FileDescription == "SMCH")
                            {
                                strData.Append("<OrderingClinician>" + DS1.Tables[0].Rows[i]["OrderingClinician"].ToString() + "</OrderingClinician>" + strNewLineChar);
                            }
                            else
                            {
                                strData.Append("<OrderingClinician>" + DS.Tables[0].Rows[j]["OrderingClinician"].ToString() + "</OrderingClinician>" + strNewLineChar);
                            }

                        }
                        strData.Append("<Clinician>" + Convert.ToString(DS.Tables[0].Rows[j]["Clinician"]) + "</Clinician>" + strNewLineChar);





                        if (DS.Tables[0].Rows[j].IsNull("PriorAuthorizationId") == false && Convert.ToString(DS.Tables[0].Rows[j]["PriorAuthorizationId"]) != "")
                        {
                            strData.Append("<PriorAuthorizationID>" + Convert.ToString(DS.Tables[0].Rows[j]["PriorAuthorizationId"]) + "</PriorAuthorizationID>" + strNewLineChar);
                        }
                        else
                        {
                            strData.Append("<PriorAuthorizationID></PriorAuthorizationID>" + strNewLineChar);
                        }

                        if (DS.Tables[0].Rows[j].IsNull("ObservType") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) != "")
                        {
                            bool bolObservDone = false;
                            if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
                            {
                                string strObsValue = Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]);
                                string[] arrObsValue = strObsValue.Split('/');
                                if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80061" && arrObsValue.Length > 2)
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>CHOL</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);

                                    if (arrObsValue.Length > 1)
                                    {
                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>HDL</Code>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }

                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 2)
                                    {
                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>LDL</Code>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }

                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 3)
                                    {
                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>TRG</Code>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }

                                        strData.Append("</Observation>" + strNewLineChar);
                                    }
                                }


                                string[] arrBPObsValue = strObsValue.Split('/');
                                if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_SERV_TYPE"]).Trim() == "C" && arrBPObsValue.Length > 1)
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>");

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>BS</Code>");
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrBPObsValue[0] + "</Value>");
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>");
                                    }


                                    strData.Append("</Observation>");

                                    strData.Append("<Observation>");

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>BD</Code>");
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrBPObsValue[1] + "</Value>");
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>");
                                    }


                                    strData.Append("</Observation>");

                                }


                            }
                            else
                            {
                                string strObsValue = Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]);
                                string[] arrObsValue = strObsValue.Split('/');

                                if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80061" && arrObsValue.Length > 2) //LOINC 80061 CODE START
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>2093-3</Code>" + strNewLineChar); //CHOL
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }


                                    strData.Append("</Observation>" + strNewLineChar);

                                    if (arrObsValue.Length > 1 && arrObsValue[1] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>2085-9</Code>" + strNewLineChar); //HDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 2 && arrObsValue[2] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>2571-8</Code>" + strNewLineChar); //LDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 3 && arrObsValue[3] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>18262-6</Code>" + strNewLineChar);//TRG
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                } //LOINC 80061 CODE END

                                else if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80051" && arrObsValue.Length > 2) //LOINC 80051 CODE START
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>82374</Code>" + strNewLineChar); //CHOL
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }


                                    strData.Append("</Observation>" + strNewLineChar);

                                    if (arrObsValue.Length > 1 && arrObsValue[1] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82435</Code>" + strNewLineChar); //HDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 2 && arrObsValue[2] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84132</Code>" + strNewLineChar); //LDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 3 && arrObsValue[3] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84295</Code>" + strNewLineChar);//TRG
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                } //LOINC 80051 CODE END
                                else if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80053" && arrObsValue.Length > 2) // LOINC 80053 CODE START
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>82040</Code>" + strNewLineChar); //CHOL
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }


                                    strData.Append("</Observation>" + strNewLineChar);

                                    if (arrObsValue.Length > 1 && arrObsValue[1] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>282247</Code>" + strNewLineChar); //HDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 2 && arrObsValue[2] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82310</Code>" + strNewLineChar); //LDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 3 && arrObsValue[3] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82374</Code>" + strNewLineChar);//TRG
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }


                                    if (arrObsValue.Length > 4 && arrObsValue[4] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82435</Code>" + strNewLineChar);//TRG
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[4] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 5 && arrObsValue[5] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82565</Code>" + strNewLineChar);//TRG
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[5] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 6 && arrObsValue[6] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82947</Code>" + strNewLineChar);// Glucose 
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[6] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }


                                    if (arrObsValue.Length > 7 && arrObsValue[7] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84075</Code>" + strNewLineChar);// Alkaline phosphatase 
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[7] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 8 && arrObsValue[8] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84132</Code>" + strNewLineChar);   //Potassium
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[8] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 9 && arrObsValue[9] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84155</Code>" + strNewLineChar);   //Protein, total 
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[9] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 10 && arrObsValue[10] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84295</Code>" + strNewLineChar);   //Sodium
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[10] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 11 && arrObsValue[11] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84460</Code>" + strNewLineChar);   //Alanine aminotransferase.macromolecular
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[11] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 12 && arrObsValue[12] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84450</Code>" + strNewLineChar);   //Aspartate aminotransferase.macromolecular
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[12] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 13 && arrObsValue[13] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84520</Code>" + strNewLineChar);   //Urea nitrogen blood
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[13] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                } // LOINC 80053 CODE END
                                else if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80051" && arrObsValue.Length > 2) //LOINC 80051 CODE START
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>82374</Code>" + strNewLineChar); //CHOL
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }


                                    strData.Append("</Observation>" + strNewLineChar);

                                    if (arrObsValue.Length > 1 && arrObsValue[1] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>82435</Code>" + strNewLineChar); //HDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 2 && arrObsValue[2] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84132</Code>" + strNewLineChar); //LDL
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 3 && arrObsValue[3] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>84295</Code>" + strNewLineChar);//TRG
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                } //LOINC 80051 CODE END

                                else if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "85025" && arrObsValue.Length > 2) // LOINC 85025 CODE START
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>718-7</Code>" + strNewLineChar); //Hemoglobin
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }


                                    strData.Append("</Observation>" + strNewLineChar);

                                    if (arrObsValue.Length > 1 && arrObsValue[1] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>4544-3</Code>" + strNewLineChar); //Hematocrit
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 2 && arrObsValue[2] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>789-8</Code>" + strNewLineChar); //Erythrocytes
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 3 && arrObsValue[3] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>6690-2</Code>" + strNewLineChar);//Leukocytes
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }


                                    if (arrObsValue.Length > 4 && arrObsValue[4] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>777-3</Code>" + strNewLineChar);//Platelets
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[4] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 5 && arrObsValue[5] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>787-2</Code>" + strNewLineChar);//Erythrocyte mean corpuscular volume
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[5] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 6 && arrObsValue[6] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>785-6</Code>" + strNewLineChar);// Erythrocyte mean corpuscular hemoglobin
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[6] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }


                                    if (arrObsValue.Length > 7 && arrObsValue[7] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>786-4</Code>" + strNewLineChar);// Erythrocyte mean corpuscular hemoglobin concentration
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[7] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 8 && arrObsValue[8] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>788-0</Code>" + strNewLineChar);   //Erythrocyte distribution width
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[8] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 9 && arrObsValue[9] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>32623-1</Code>" + strNewLineChar);   //Platelet mean volume
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[9] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 10 && arrObsValue[10] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>751-8</Code>" + strNewLineChar);   //Neutrophils
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[10] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 11 && arrObsValue[11] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>731-0</Code>" + strNewLineChar);   //Lymphocytes
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[11] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }

                                    if (arrObsValue.Length > 12 && arrObsValue[12] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>742-7</Code>" + strNewLineChar);   //Monocytes
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[12] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 13 && arrObsValue[13] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>711-2</Code>" + strNewLineChar);   //Eosinophils
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[13] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }
                                    if (arrObsValue.Length > 14 && arrObsValue[14] != "")
                                    {

                                        strData.Append("<Observation>" + strNewLineChar);

                                        strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                        if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                        {
                                            strData.Append("<Code>704-7</Code>" + strNewLineChar);   //Basophils
                                        }
                                        else
                                        {
                                            strData.Append("<Code></Code>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                        {
                                            strData.Append("<Value>" + arrObsValue[14] + "</Value>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<Value></Value>" + strNewLineChar);
                                        }
                                        if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                        {
                                            strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                        }
                                        else
                                        {
                                            strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                        }


                                        strData.Append("</Observation>" + strNewLineChar);

                                    }











                                } // LOINC 80053 CODE END




                                else
                                {
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]) + "</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) != "")
                                    {
                                        strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                                    }
                                    //else
                                    //{
                                    //    strData.Append("<Value></Value>" + strNewLineChar);
                                    //}
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) != "")
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    //else
                                    //{
                                    //    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    //}


                                    strData.Append("</Observation>" + strNewLineChar);
                                }
                            }


                            string strObservCode = Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]);
                            string[] arrObservCode = strObservCode.Split(',');

                            if (arrObservCode.Length > 1)
                            {

                                for (Int32 intCode = 0; intCode < arrObservCode.Length; intCode++)
                                {

                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>" + arrObservCode[intCode] + "</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);
                                }
                            }
                            else if (bolObservDone == false)
                            {
                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]) + "</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }

                                strData.Append("</Observation>" + strNewLineChar);
                            }

                        }
                        if (Convert.ToString(DS.Tables[0].Rows[j]["TransReComment"]) != "")
                        {
                            strResubComment += Convert.ToString(DS.Tables[0].Rows[j]["Code2"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["TransReComment"]) + strNewLineChar;
                        }
                        strData.Append("</Activity>" + strNewLineChar);

                    }


                }

                strData.Append("<Resubmission>" + strNewLineChar);
                strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ReType"]) + "</Type>" + strNewLineChar);
                strData.Append("<Comment>" + strResubComment.Replace("&", "") + "</Comment>" + strNewLineChar);

                DataSet DSAttach = new DataSet();
                string CriteriaAttach = " 1=1 ";
                CriteriaAttach += " AND HMS_RESUBMISSION_MASTER.ReSubNo='" + Convert.ToString(DS1.Tables[0].Rows[i]["ReSubNo"]) + "'";
                DSAttach = dbo.ResubmissionGet(CriteriaAttach);
                if (DSAttach.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(DSAttach.Tables[0].Rows[0]["HIM_ATTACHMENT"]) != "")
                    {
                        strData.Append("<Attachment>" + Convert.ToString(DSAttach.Tables[0].Rows[0]["HIM_ATTACHMENT"]) + "</Attachment>" + strNewLineChar);
                    }

                }


                strData.Append("</Resubmission>" + strNewLineChar);


                //strData.Append("<Contract>" + strNewLineChar);
                //strData.Append("<PackageName>" + Convert.ToString(DS1.Tables[0].Rows[i]["PackageName"]) + "</PackageName>" + strNewLineChar);
                //strData.Append("</Contract>" + strNewLineChar);



                strData.Append("</Claim>" + strNewLineChar);
            }
            strData.Append("</Claim.Submission>" + strNewLineChar);

            if (IsError == false)
            {
                string strFileName = strPath + "ReSubEClaim" + strName + ".XML";

                StreamWriter oWrite;
                oWrite = File.CreateText(strFileName);
                oWrite.WriteLine(strData);

                oWrite.Close();
                lblStatus.Text = "ReSubmission E-Claim Created - File Name is ReSubEClaim" + strName + ".XML";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('E-Claim Not Created, Check the Log File !');", true);
                lblStatus.Text = "ReSubmission E-Claim Not Created, Check the Log File  - File Name is  ReSubEClaimErrorLog" + strName + ".txt";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        ClaimEnd: ;

        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "ECLARESUB";
            objCom.ScreenName = "eClaim";
            objCom.ScreenType = "Resubmission";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='ECLARESUB' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                // btnClear.Enabled = false;
                btnSearch.Enabled = false;
                filAttachment.Enabled = false;
                btnTestEclaim.Enabled = false;
                btnProductionEclaim.Enabled = false;
                btnSaveToDB.Enabled = false;


                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                btnDispense.Enabled = false;

            }

            if (strPermission == "5")
            {
                btnDelete.Enabled = false;
            }
            if (strPermission == "7")
            {

                // btnClear.Enabled = false;
                btnSearch.Enabled = false;
                filAttachment.Enabled = false;
                btnTestEclaim.Enabled = false;
                btnProductionEclaim.Enabled = false;
                btnSaveToDB.Enabled = false;

                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                btnDispense.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Insurance");
            }
        }

        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' ";
            Criteria += " AND HCM_COMP_ID Like '" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A'   ";
            Criteria += " AND HCM_NAME Like '" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisID(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_ID  like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_DESCRIPTION   like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";


            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                AuditLogAdd("OPEN", "Open Resubmission Page");

                ViewState["SaveMode"] = "A";
                txtFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                //  txtClaimFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                // txtClaimToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

              

                BuildeInvoiceClaims();
                BindResubType();

                BindData();

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Clear();
            BindData();

        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvRemittanceData.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblRefNo = (Label)gvScanCard.Cells[0].FindControl("lblRefNo");
                Label lblXMLFileName = (Label)gvScanCard.Cells[0].FindControl("lblXMLFileName");
                txtReferenceNo.Text = lblRefNo.Text.Trim();
                //  ViewState["PaymentReference"] = lblRefNo.Text;
                ViewState["FileName"] = lblXMLFileName.Text;

                BindRemittanceClaims(lblXMLFileName.Text);



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Resubmission.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void SelectClaim_Click(object sender, EventArgs e)
        {
            try
            {
                ////ViewState["PaymentReference"] = "";
                ////txtInvoiceNo.Text = "";
                ////txtResubNo.Text = "";
                ////txtIDPayer.Text = "";
                ////txtPolicyNo.Text = "";
                ////txtPriorAuthorizationID.Text = "";
                ////drpReType.SelectedIndex = 0;

                Clear();
                BuildeInvoiceClaims();
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                // ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvRemittanceClaims.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblRefNo = (Label)gvScanCard.Cells[0].FindControl("lblRefNo");
                Label lblClaimID = (Label)gvScanCard.Cells[0].FindControl("lblClaimID");
                Label lblIDPayer = (Label)gvScanCard.Cells[0].FindControl("lblIDPayer");
                Label lblReSubStatus = (Label)gvScanCard.Cells[0].FindControl("lblReSubStatus");
                Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");

                txtEMR_PTMaster_FromDate.Text = Convert.ToString(lblDate.Text);
                txtEMR_PTMaster_ToDate.Text = Convert.ToString(lblDate.Text);

                txtIDPayer.Text = lblIDPayer.Text;

                ResubmissionGet(lblClaimID.Text);

                //  ViewState["PaymentReference"] = lblRefNo.Text.Trim();
                txtInvoiceNo.Text = lblClaimID.Text;
                BindRemittanceClaimTrans(lblRefNo.Text, lblClaimID.Text);
                BindInvoiceTran(lblClaimID.Text);
                // BindInvoiceDtls(lblClaimID.Text);
                BindInvoiceClaimDtls(lblClaimID.Text);
                BindTempInvoiceClaims();


                BindResubTranHistory();

                BuildeResubClaimsHistory();
                BindResubClaimHistory();
                BindResubClaimHistoryGrid();
                BindEMRPTMasterGrid();

                if (lblReSubStatus.Text.Trim() == "Pending")
                {
                    btnSaveToDB.Visible = true;

                    btnDispense.Visible = false;
                }
                if (lblReSubStatus.Text.Trim() == "Completed")
                {
                    btnSaveToDB.Visible = false;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    btnDispense.Visible = false;
                }

                else
                {
                    btnSaveToDB.Visible = true;

                    btnDispense.Visible = false;
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Resubmission.SelectClaim_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvInvTran_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drpDiscType, drpCoInsType;
                CheckBox chkSubmit;
                Label lblDiscType, lblCoInsType, lblResubType, lblSubmitted;


                drpDiscType = (DropDownList)e.Row.Cells[0].FindControl("drpDiscType");
                drpCoInsType = (DropDownList)e.Row.Cells[0].FindControl("drpCoInsType");

                chkSubmit = (CheckBox)e.Row.Cells[0].FindControl("chkSubmit");


                lblDiscType = (Label)e.Row.Cells[0].FindControl("lblDiscType");
                lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");
                lblResubType = (Label)e.Row.Cells[0].FindControl("lblResubType");
                lblSubmitted = (Label)e.Row.Cells[0].FindControl("lblSubmitted");

                drpDiscType.SelectedValue = lblDiscType.Text;
                drpCoInsType.SelectedValue = lblCoInsType.Text;
                if (lblSubmitted.Text == "True")
                {
                    chkSubmit.Checked = true;
                }
                else
                {
                    chkSubmit.Checked = false;
                }


            }
        }

        protected void drpReType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < gvInvTran.Rows.Count; i++)
                {
                    CheckBox chkSubmit;

                    chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");

                    if (drpReType.SelectedValue == "correction")
                    {
                        chkSubmit.Checked = true;
                    }
                    else
                    {
                        chkSubmit.Checked = false;
                    }


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.drpReType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAttaAdd_Click(object sender, EventArgs e)
        {

            try
            {

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                string strPath = GlobalValues.AuthorizationAttachmentPath;

                Int32 intLength;
                intLength = filAttachment.PostedFile.ContentLength;
                String ext = System.IO.Path.GetExtension(filAttachment.FileName);
                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                lblFileName.Text = strName + ext;
                lblFileNameActual.Text = filAttachment.FileName;
                ViewState["AttachmentData"] = Convert.ToBase64String(filAttachment.FileBytes);
                DeleteAttach.Visible = true;


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteAttach_Click(object sender, EventArgs e)
        {

            try
            {
                lblFileName.Text = "";
                lblFileNameActual.Text = "";
                ViewState["AttachmentData"] = "";
                DeleteAttach.Visible = false;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.DeleteAttach_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();

                SqlCommand cmd = new SqlCommand();

                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_ResubmissionDelete";
                cmd.Parameters.Add(new SqlParameter("@BranchID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@InvoiceNo", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = txtResubNo.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                cmd.ExecuteNonQuery();
                con.Close();

                AuditLogAdd("DELETE", "Delete the Resubmission, Invoice Number is " + txtInvoiceNo.Text.Trim() + ", Resubmission Number is " + txtResubNo.Text.Trim());


                lblStatus.Text = "Following Resubmission Deleted - " + txtResubNo.Text.Trim();
                lblStatus.ForeColor = System.Drawing.Color.Green;
                ClearSaveData();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {


                if (txtResubNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Resubmission is not available";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;
                }


                SqlCommand cmd = new SqlCommand();

                if (gvRemittanceClaims.Rows.Count > 0 && txtInvoiceNo.Text.Trim() != "")
                {

                    for (int i = 0; i < gvInvTran.Rows.Count; i++)
                    {
                        CheckBox chkSubmit;
                        TextBox txtResubComment;
                        DropDownList drpDiscType, drpCoInsType;
                        Label lblHIOType, lblServCode;
                        TextBox txtServCode, txtServeDesc, txtFee, txtDiscAmt, txtQTY, txtDeduct, txtCoInsAmt, txtHitAmount, txtPTAmount, txtHaadCode, txtHIOCode, txtHIOValue, txtHIOValueType;



                        chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");
                        txtResubComment = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtResubComment");


                        Label lblSlno = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblSlno");

                        TextBox txtgvInvoiceID = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtgvInvoiceID");
                        lblServCode = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblServCode");

                        txtServCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServCode");
                        txtServeDesc = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServeDesc");

                        txtFee = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtFee");
                        txtDiscAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDiscAmt");
                        txtQTY = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtQTY");
                        txtDeduct = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDeduct");
                        txtCoInsAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtCoInsAmt");

                        txtHitAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHitAmount");
                        txtPTAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtPTAmount");
                        txtHaadCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHaadCode");
                        lblHIOType = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblHIOType");
                        txtHIOCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOCode");
                        txtHIOValue = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValue");
                        txtHIOValueType = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValueType");


                        drpDiscType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpDiscType");
                        drpCoInsType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpCoInsType");

                        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con.Open();
                        cmd = new SqlCommand();
                        //   ReSubSaveLog("  ResubmissionTranUpdate ");

                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionTranUpdate";
                        cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = txtgvInvoiceID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIT_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;

                        cmd.Parameters.Add(new SqlParameter("@HIT_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIT_DESCRIPTION", SqlDbType.VarChar)).Value = txtServeDesc.Text.Trim();

                        if (txtFee.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = txtFee.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_DISC_TYPE", SqlDbType.Char)).Value = drpDiscType.SelectedValue;

                        if (txtDiscAmt.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = txtDiscAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtQTY.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = txtQTY.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = "1";

                        if (txtDeduct.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = txtDeduct.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;

                        if (txtCoInsAmt.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = txtCoInsAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtHitAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = txtHitAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        if (txtPTAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = txtPTAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_HAAD_CODE", SqlDbType.VarChar)).Value = txtHaadCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@TransReComment", SqlDbType.VarChar)).Value = txtResubComment.Text.Trim();
                        if (chkSubmit.Checked == true)
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue.Trim();
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = "";
                        }
                        cmd.Parameters.Add(new SqlParameter("@HIT_RESUBMITTED", SqlDbType.VarChar)).Value = chkSubmit.Checked;
                        cmd.Parameters.Add(new SqlParameter("@HIT_SLNO", SqlDbType.Int)).Value = lblSlno.Text;
                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = txtResubNo.Text.Trim();


                        // ReSubSaveLog(" ResubmissionTranUpdate Started ");

                        cmd.ExecuteNonQuery();
                        con.Close();
                        //  ReSubSaveLog(" ResubmissionTranUpdate End ");

                        SqlConnection con1 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con1.Open();
                        cmd = new SqlCommand();

                        cmd.Connection = con1;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionObservUpdate";
                        cmd.Parameters.Add(new SqlParameter("@HIO_INVOICE_ID", SqlDbType.VarChar)).Value = txtgvInvoiceID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIO_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;
                        cmd.Parameters.Add(new SqlParameter("@HIO_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_TYPE", SqlDbType.VarChar)).Value = lblHIOType.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_CODE", SqlDbType.VarChar)).Value = txtHIOCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUE", SqlDbType.VarChar)).Value = txtHIOValue.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUETYPE", SqlDbType.VarChar)).Value = txtHIOValueType.Text.Trim();


                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = txtResubNo.Text.Trim();

                        // ReSubSaveLog(" ResubmissionObservUpdate Start ");
                        cmd.ExecuteNonQuery();
                        con1.Close();
                        //  ReSubSaveLog(" ResubmissionObservUpdate Start ");




                    }

                    SqlConnection con2 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con2.Open();
                    cmd = new SqlCommand();

                    cmd.Connection = con2;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionClaimDtlsUpdate";
                    cmd.Parameters.Add(new SqlParameter("@HIC_INVOICE_ID", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_TYPE", SqlDbType.TinyInt)).Value = drpClmType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTDATE", SqlDbType.VarChar)).Value = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDDATE", SqlDbType.VarChar)).Value = txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTTYPE", SqlDbType.TinyInt)).Value = drpClmStartType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDTYPE", SqlDbType.TinyInt)).Value = drpClmEndType.SelectedValue;


                    for (int i = 0; i < gvInvoiceDiag.Rows.Count; i++)
                    {
                        Label lblICDType = (Label)gvInvoiceDiag.Rows[i].Cells[0].FindControl("lblICDType");
                        Label lblICDCode = (Label)gvInvoiceDiag.Rows[i].Cells[0].FindControl("lblICDCode");


                        string strICDType = "", strICDCode = "";

                        strICDCode = lblICDCode.Text;
                        strICDType = lblICDType.Text;


                        if (strICDType == "Principal")
                        {

                            //  objInvClms.HIC_YEAR_OF_ONSET_P = Convert.ToString(DT.Rows[i]["HIC_YEAR_OF_ONSET_P"]);

                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_P", SqlDbType.VarChar)).Value = strICDCode;


                            goto EndFor;
                        }

                        if (strICDType == "Admitting")
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_A", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }



                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S1") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S1", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S2") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S2", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S3") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S3", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }
                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S4") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S4", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S5") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S5", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S6") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S6", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S7") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S7", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S8") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S8", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S11") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S11", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S12") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S12", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S13") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S13", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S14") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S14", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S15") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S15", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S16") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S16", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S17") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S17", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S18") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S18", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S19") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S19", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S20") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S20", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S21") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S21", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S22") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S22", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S23") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S23", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S24") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S24", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S25") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S25", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S26") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S26", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S27") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S27", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S28") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S28", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }
                    EndFor: ;


                    }




                    cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = txtResubNo.Text.Trim();
                    // ReSubSaveLog(" ResubmissionClaimDtlsUpdate Start ");
                    cmd.ExecuteNonQuery();
                    con2.Close();
                    //  ReSubSaveLog(" ResubmissionClaimDtlsUpdate End ");

                    SqlConnection con3 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con3.Open();
                    cmd = new SqlCommand();

                    cmd.Connection = con3;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionUpdate";
                    cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIM_REMARKS", SqlDbType.VarChar)).Value = txtIDPayer.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@ReComment", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@ReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue;


                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME", SqlDbType.VarChar)).Value = lblFileName.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME_ACTUAL", SqlDbType.VarChar)).Value = lblFileNameActual.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_ATTACHMENT", SqlDbType.Text)).Value = ViewState["AttachmentData"];
                    cmd.Parameters.Add(new SqlParameter("@HIM_AUTHID", SqlDbType.VarChar)).Value = txtPriorAuthorizationID.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = txtResubNo.Text.Trim();

                    // ReSubSaveLog(" ResubmissionUpdate Start ");
                    cmd.ExecuteNonQuery();
                    con3.Close();

                    // ReSubSaveLog(" ResubmissionUpdate End ");


                    CommonBAL objCom = new CommonBAL();

                    string Criteria = " 1=1 ";

                    if (Convert.ToString(ViewState["FileName"]) != "")
                    {
                        Criteria = " ClaimID='" + txtInvoiceNo.Text.Trim() + "' AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";
                    }
                    else
                    {
                        Criteria = " ClaimID='" + txtInvoiceNo.Text.Trim() + "'";
                    }


                    string FieldNameWithValues = "ReSubStatus='Generated'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_REMITTANCE_XML_DATA", Criteria);

                }

                AuditLogAdd("MODIFY", "Modify Existing entry in the Resubmission, Invoice Number is " + txtInvoiceNo.Text.Trim() + ", Resubmission Number is " + txtResubNo.Text.Trim());


                ClearSaveData();
                BindRemittanceClaims(Convert.ToString(ViewState["FileName"]));
                lblStatus.Text = "Details Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnSaveToDB_Click(object sender, EventArgs e)
        {

            try
            {
                Boolean boolIsChecked = false; ;
                for (int i = 0; i < gvInvTran.Rows.Count; i++)
                {
                    CheckBox chkSubmit;
                    chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");

                    if (chkSubmit.Checked == true)
                    {
                        boolIsChecked = true;
                    }

                }

                if (drpReType.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please select Resubmission Type";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;
                }

                if (boolIsChecked == false)
                {
                    lblStatus.Text = "Please select Any of one Bill Details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;

                }

                if (GlobalValues.FileDescription == "SMCH")
                {
                    string Criteria = " 1=1  AND   HIM_RESUB_VERIFY_STATUS IS NULL ";
                    Criteria += " AND  HIM_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";

                    DataSet DS = new DataSet();

                    dboperations dbo = new dboperations();
                    DS = dbo.ResubmissionGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblStatus.Text = "This invoice already Resubmitted";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto SaveEnd;
                    }
                }

                if (gvRemittanceClaims.Rows.Count > 0 && ViewState["InvoiceId"] != "")
                {


                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionAdd";
                    cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIM_REMARKS", SqlDbType.VarChar)).Value = txtIDPayer.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@ReComment", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@ReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue;

                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME", SqlDbType.VarChar)).Value = lblFileName.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME_ACTUAL", SqlDbType.VarChar)).Value = lblFileNameActual.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_ATTACHMENT", SqlDbType.Text)).Value = ViewState["AttachmentData"];
                    cmd.Parameters.Add(new SqlParameter("@HIM_AUTHID", SqlDbType.VarChar)).Value = txtPriorAuthorizationID.Text.Trim();



                    SqlParameter returnValue = new SqlParameter("@ReturnReSubNo", SqlDbType.VarChar, 20);
                    returnValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnValue);

                    cmd.ExecuteNonQuery();
                    string strReturnReSubNo;
                    strReturnReSubNo = Convert.ToString(returnValue.Value);
                    con.Close();


                    for (int i = 0; i < gvInvTran.Rows.Count; i++)
                    {
                        CheckBox chkSubmit;
                        TextBox txtResubComment;
                        DropDownList drpDiscType, drpCoInsType;
                        Label lblHIOType, lblServCode, lblSlno;
                        TextBox txtServCode, txtServeDesc, txtFee, txtDiscAmt, txtQTY, txtDeduct, txtCoInsAmt, txtHitAmount, txtPTAmount, txtHaadCode, txtHIOCode, txtHIOValue, txtHIOValueType;



                        chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");
                        txtResubComment = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtResubComment");

                        TextBox txtgvInvoiceID = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtgvInvoiceID");
                        lblServCode = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblServCode");
                        lblSlno = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblSlno");
                        txtServCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServCode");
                        txtServeDesc = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServeDesc");

                        txtFee = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtFee");
                        txtDiscAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDiscAmt");
                        txtQTY = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtQTY");
                        txtDeduct = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDeduct");
                        txtCoInsAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtCoInsAmt");

                        txtHitAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHitAmount");
                        txtPTAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtPTAmount");
                        txtHaadCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHaadCode");
                        lblHIOType = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblHIOType");
                        txtHIOCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOCode");
                        txtHIOValue = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValue");
                        txtHIOValueType = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValueType");



                        drpDiscType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpDiscType");
                        drpCoInsType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpCoInsType");


                        // if (chkSubmit.Checked == true)
                        // {
                        SqlConnection con1 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con1.Open();
                        cmd = new SqlCommand();

                        cmd.Connection = con1;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionTranAdd";
                        cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIT_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;

                        cmd.Parameters.Add(new SqlParameter("@HIT_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIT_DESCRIPTION", SqlDbType.VarChar)).Value = txtServeDesc.Text.Trim();
                        if (txtFee.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = txtFee.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_DISC_TYPE", SqlDbType.Char)).Value = drpDiscType.SelectedValue;

                        if (txtDiscAmt.Text.Trim() != "")

                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = txtDiscAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtQTY.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = txtQTY.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = "1";

                        if (txtDeduct.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = txtDeduct.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;

                        if (txtCoInsAmt.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = txtCoInsAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtHitAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = txtHitAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        if (txtPTAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = txtPTAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_HAAD_CODE", SqlDbType.VarChar)).Value = txtHaadCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@TransReComment", SqlDbType.VarChar)).Value = txtResubComment.Text.Trim();
                        if (chkSubmit.Checked == true)
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue.Trim();
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = "";
                        }
                        cmd.Parameters.Add(new SqlParameter("@HIT_RESUBMITTED", SqlDbType.VarChar)).Value = chkSubmit.Checked;
                        cmd.Parameters.Add(new SqlParameter("@HIT_SLNO", SqlDbType.Int)).Value = lblSlno.Text;
                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                        cmd.ExecuteNonQuery();
                        con1.Close();

                        SqlConnection con2 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con2.Open();
                        cmd = new SqlCommand();

                        cmd.Connection = con2;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionObservAdd";
                        cmd.Parameters.Add(new SqlParameter("@HIO_INVOICE_ID", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIO_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;
                        cmd.Parameters.Add(new SqlParameter("@HIO_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_TYPE", SqlDbType.VarChar)).Value = lblHIOType.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_CODE", SqlDbType.VarChar)).Value = txtHIOCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUE", SqlDbType.VarChar)).Value = txtHIOValue.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUETYPE", SqlDbType.VarChar)).Value = txtHIOValueType.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_SERV_SLNO", SqlDbType.Int)).Value = lblSlno.Text;

                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                        cmd.ExecuteNonQuery();
                        con2.Close();


                        // }

                    }

                    SqlConnection con3 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con3.Open();
                    cmd = new SqlCommand();

                    cmd.Connection = con3;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionClaimDtlsAdd";
                    cmd.Parameters.Add(new SqlParameter("@HIC_INVOICE_ID", SqlDbType.VarChar)).Value = txtInvoiceNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_TYPE", SqlDbType.TinyInt)).Value = drpClmType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTDATE", SqlDbType.VarChar)).Value = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDDATE", SqlDbType.VarChar)).Value = txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTTYPE", SqlDbType.TinyInt)).Value = drpClmStartType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDTYPE", SqlDbType.TinyInt)).Value = drpClmEndType.SelectedValue;


                    for (int i = 0; i < gvInvoiceDiag.Rows.Count; i++)
                    {
                        Label lblICDType = (Label)gvInvoiceDiag.Rows[i].Cells[0].FindControl("lblICDType");
                        Label lblICDCode = (Label)gvInvoiceDiag.Rows[i].Cells[0].FindControl("lblICDCode");


                        string strICDType = "", strICDCode = "";

                        strICDCode = lblICDCode.Text;
                        strICDType = lblICDType.Text;


                        if (strICDType == "Principal")
                        {

                            //  objInvClms.HIC_YEAR_OF_ONSET_P = Convert.ToString(DT.Rows[i]["HIC_YEAR_OF_ONSET_P"]);

                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_P", SqlDbType.VarChar)).Value = strICDCode;


                            goto EndFor;
                        }

                        if (strICDType == "Admitting")
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_A", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S1") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S1", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S2") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S2", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S3") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S3", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }
                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S4") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S4", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S5") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S5", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S6") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S6", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S7") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S7", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S8") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S8", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S11") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S11", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S12") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S12", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S13") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S13", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S14") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S14", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S15") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S15", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S16") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S16", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S17") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S17", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S18") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S18", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S19") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S19", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S20") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S20", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S21") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S21", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S22") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S22", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S23") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S23", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S24") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S24", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S25") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S25", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S26") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S26", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }

                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S27") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S27", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }


                        if (strICDType == "Secondary" && cmd.Parameters.Contains("@HIC_ICD_CODE_S28") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S28", SqlDbType.VarChar)).Value = strICDCode;
                            goto EndFor;
                        }



                    EndFor: ;
                    }





                    cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                    cmd.ExecuteNonQuery();
                    con3.Close();



                    CommonBAL objCom = new CommonBAL();

                    string Criteria = " ClaimID='" + txtInvoiceNo.Text.Trim() + "' AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";
                    string FieldNameWithValues = "ReSubStatus='Generated'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_REMITTANCE_XML_DATA", Criteria);


                    //c.conopen();
                    //cmd = new SqlCommand();

                    //cmd.Connection = c.con;
                    //cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.CommandText = "HMS_SP_ResubmissionUpdate";
                    //cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                    //cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                    //cmd.ExecuteNonQuery();
                    //c.conclose();

                }

                AuditLogAdd("ADD", "Generate Resubmission, Invoice Number is " + txtInvoiceNo.Text.Trim() + ", Resubmission Number is " + txtResubNo.Text.Trim());

                ClearSaveData();

                BindRemittanceClaims(Convert.ToString(ViewState["FileName"]));
                lblStatus.Text = "Details Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_btnSaveToDB_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnInvDiagAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceClaims"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HIC_ICD_CODE"] = txtDiagCode.Text.Trim();
                objrow["HIC_ICD_DESC"] = txtDiagName.Text.Trim();
                objrow["HIC_ICD_TYPE"] = drpDiagType.SelectedValue;


                DT.Rows.Add(objrow);

                ViewState["InvoiceClaims"] = DT;
                BindTempInvoiceClaims();

                txtDiagCode.Text = "";
                txtDiagName.Text = "";
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnInvDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeletegvDiag_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["InvoiceClaims"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["InvoiceClaims"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["InvoiceClaims"] = DT;

                }


                BindTempInvoiceClaims();



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.DeletegvDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }




        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            ClearSaveData();
        }


        protected void btnDispense_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();
                DataSet DS = new DataSet();
                string Criteria = " ClaimID='" + txtInvoiceNo.Text.Trim() + "'  AND ReSubNo='" + txtResubNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue("*", "HMS_ECLAIM_XML_DATA_MASTER", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = " Already Transfered";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Criteria = " 1=1 ";
                Criteria += " AND  HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND ReSubNo='" + txtResubNo.Text.Trim() + "'";

                dboperations dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.ResubmissionTransGet(Criteria);


                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = " Services not available for this Resubmission";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Criteria = " 1=1 ";
                Criteria += " AND  HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIC_RESUBNO='" + txtResubNo.Text.Trim() + "'";

                dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.ResubmissionClaimDtlsGet(Criteria);


                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = " Diagnosis not available for this Resubmission";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (TransferResubValidation() == false)
                {
                    goto FunEnd;
                }


                // TextFileWriting("");
                // TextFileWriting(Convert.ToString(ViewState["BRANCH_ID"]) + "," + txtInvoiceID.Text.Trim() + "," + txtFileNo.Text + "," + Convert.ToString(ViewState["VisitType"]) + "," + Convert.ToString(Session["User_ID"]));

                clsInvoice objInv = new clsInvoice();

                objInv.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objInv.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim();
                objInv.RESUB_NO = txtResubNo.Text.Trim();
                objInv.HIM_PT_ID = Convert.ToString(ViewState["FileNo"]);
                objInv.UserID = Convert.ToString(Session["User_ID"]);



                objInv.EclaimXMLDataMasterAddFromResub();

                TextFileWriting(" EclaimXMLDataMasterAdd completed");

                AuditLogAdd("DISPENSE", "Dispense the Resubmission, Invoice Number is " + txtInvoiceNo.Text.Trim() + ", Resubmission Number is " + txtResubNo.Text.Trim());


                objCom = new CommonBAL();

                string Criteria1 = " ClaimID='" + txtInvoiceNo.Text.Trim() + "' AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";
                string FieldNameWithValues = "ReSubStatus='Completed'";

                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_REMITTANCE_XML_DATA", Criteria1);




                BindRemittanceClaims(Convert.ToString(ViewState["FileName"]));


                TextFileWriting(" BindVisit completed");

                lblStatus.Text = " Transfer Completed";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnDispense_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindRemittanceClaims(Convert.ToString(ViewState["FileName"]));
             }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnClaimSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvRemittanceClaims.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvRemittanceClaims.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkInvResub");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }
                }

                CountClaimAmount();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void chkboxSelect_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CountClaimAmount();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.chkboxSelect_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }
        protected void btnTestEclaim_Click(object sender, EventArgs e)
        {
            Boolean boolIsChecked = false;
            decimal decTotalClaimAmount = 0;
            Int32 intTotalCount = 0;

            for (int i = 0; i < gvRemittanceClaims.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvRemittanceClaims.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblTotalNet = (Label)gvRemittanceClaims.Rows[i].Cells[0].FindControl("lblTotalNet");

                if (chkInvResub.Checked == true)
                {
                    intTotalCount += 1;
                    boolIsChecked = true;
                    if (lblTotalNet.Text != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(lblTotalNet.Text);

                    }
                }

            }


            lblTotalClaims.Text = Convert.ToString(intTotalCount);
            lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

            //hidTotalClaims.Value = Convert.ToString(intTotalCount);
            //hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");

            if (boolIsChecked == false)
            {
                lblStatus.Text = "Please select Any of one Bill Details";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;

            }



            //  string strClaimGenerate = hidClaimGenerate.Value;

            //if (strClaimGenerate == "true")
            //{
            XMLFileWrite("TEST");

         //  }


       FunEnd: ;

            //hidClaimGenerate.Value = "";
        }

        protected void btnProductionEclaim_Click(object sender, EventArgs e)
        {

            Boolean boolIsChecked = false;
            decimal decTotalClaimAmount = 0;
            Int32 intTotalCount = 0;

            for (int i = 0; i < gvRemittanceClaims.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvRemittanceClaims.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblTotalNet = (Label)gvRemittanceClaims.Rows[i].Cells[0].FindControl("lblTotalNet");

                if (chkInvResub.Checked == true)
                {
                    intTotalCount += 1;
                    boolIsChecked = true;
                    if (lblTotalNet.Text != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(lblTotalNet.Text);

                    }
                }

            }


            lblTotalClaims.Text = Convert.ToString(intTotalCount);
            lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

            //hidTotalClaims.Value = Convert.ToString(intTotalCount);
            //hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");

            if (boolIsChecked == false)
            {
                lblStatus.Text = "Please select Any of one Bill Details";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;

            }



            //  string strClaimGenerate = hidClaimGenerate.Value;

            //if (strClaimGenerate == "true")
            //{
            XMLFileWrite("PRODUCTION");

            //  }

            AuditLogAdd("GENERATE", "Generate the Production Resubmission, " + lblStatus.Text.Trim());

        FunEnd: ;

            // hidClaimGenerate.Value = "";

        }

        protected void gvRemittanceData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                    lblSerial.Text = ((gvRemittanceData.PageIndex * gvRemittanceData.PageSize) + e.Row.RowIndex + 1).ToString();
                }
            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Resubmission.gvRemittanceData_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvRemittanceClaims_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                    lblSerial.Text = ((gvRemittanceClaims.PageIndex * gvRemittanceClaims.PageSize) + e.Row.RowIndex + 1).ToString();
                }
            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Resubmission.gvRemittanceClaims_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnEMR_PTMasterRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                
                BindEMRPTMasterGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnEMR_PTMasterRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvEMR_PTMasterPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["EMRSelectIndex"]) != "")
                {
                    if (gvEMR_PTMaster.Rows.Count > 0)
                        gvEMR_PTMaster.Rows[Convert.ToInt32(ViewState["EMRSelectIndex"])].BackColor = System.Drawing.Color.White;
                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                // ViewState["EMRSelectIndex"] = gvScanCard.RowIndex;

                //   gvEMR_PTMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblgvEMR_PTMaster_Emr_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvEMR_PTMaster_Emr_ID");
                Label lblgvEMR_PTMaster_DrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvEMR_PTMaster_DrCode");

                // ViewState["Selected_Emr_ID"] = lblgvEMR_PTMaster_Emr_ID.Text;


                //string strRptPath = "../WebReport/eClaimCoderClinicalSummary.aspx";
                //string rptcall = @strRptPath + "?INV_EMR_ID=" + lblgvEMR_PTMaster_Emr_ID.Text;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                if (drpEMRReport.SelectedValue == "ClinicalSummary")
                {

                    string strRptPath = "../EMR/WebReports/ClinicalSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblgvEMR_PTMaster_Emr_ID.Text + "&EMR_PT_ID=" + txtFileNo.Text.Trim() + "&DR_ID=" + lblgvEMR_PTMaster_DrCode.Text.Trim();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }
                else if (drpEMRReport.SelectedValue == "AddendumSummary")
                {
                    string strRptPath = "../EMR/WebReports/AddendumSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblgvEMR_PTMaster_Emr_ID.Text + "&EMR_PT_ID=" + txtFileNo.Text.Trim() + "&DR_ID=" + lblgvEMR_PTMaster_DrCode.Text.Trim();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Resubmission.gvEMR_PTMasterPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }
    }
}