﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Resubmission.aspx.cs" Inherits="Mediplus.Insurance.Resubmission" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }


        #divDiagServ
        {
            width: 400px !important;
        }

            #divDiagServ div
            {
                width: 400px !important;
            }


        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>
    <script language="javascript" type="text/javascript">
        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function DiagIdSelected() {
            if (document.getElementById('<%=txtDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DiagNameSelected() {
            if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function ShowReceiptAddPopup() {

            document.getElementById("divReceiptAdd").style.display = 'block';


        }

        function HideReceiptAddPopup() {

            document.getElementById("divReceiptAdd").style.display = 'none';

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <table>
        <tr>
            <td class="PageHeader">Resubmission
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <table width="80%">
        <tr>
            <td>
                <div class="lblCaption1" style="width: 100%; height: 70px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">


                    <table width="100%">
                        <tr>
                            <td class="lblCaption1">File Name 

                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFileName" runat="server" CssClass="TextBoxStyle" Width="300px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                            <td class="lblCaption1">From Date 
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFrmDt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                            Enabled="True" TargetControlID="txtFrmDt" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                            <td class="lblCaption1">To Date
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtToDt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                            Enabled="True" TargetControlID="txtToDt" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Company
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" Height="22px" Width="100px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" Height="22px" Width="250px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>
                                        <div id="divComp" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                                        </asp:AutoCompleteExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Reference #
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcReferenceNo" CssClass="TextBoxStyle" runat="server" Height="22px" Width="200px"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td></td>

                            <td>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small"
                                            OnClick="btnSearch_Click" Text="Search" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>


                    </table>
                </div>
            </td>

        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; height: 200px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvRemittanceData" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvRemittanceData_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200" GridLines="Both" CellPadding="2" CellSpacing="2">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="Select_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SL.No" SortExpression="HPV_SEQNO" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerial" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("XMLFILENAME") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FILE NAME" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("ActualFileName") %>'></asp:Label>
                                            <asp:Label ID="lblXMLFileName" CssClass="GridRow" runat="server" Text='<%# Bind("XMLFILENAME") %>' Visible="false"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SENDER ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSenderID" CssClass="GridRow" runat="server" Text='<%# Bind("SenderID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyID") %>'></asp:Label>
                                            <asp:Label ID="lblCompanyName" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PROVIDER" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="lblProviderID" CssClass="GridRow" runat="server" Text='<%# Bind("ProviderID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REF NO" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>


                                            <asp:Label ID="lblRefNo" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentReference") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TRANS. COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCount" CssClass="GridRow" runat="server" Text='<%# Bind("Count") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PT. SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYMENT AMT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaymentAmount" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

    </div>
    <table width="80%">
        <tr>

            <td class="lblCaption1">From Date 
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtClaimFrmDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender5" runat="server"
                            Enabled="True" TargetControlID="txtClaimFrmDt" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtClaimFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

            <td class="lblCaption1">To Date
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtClaimToDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender6" runat="server"
                            Enabled="True" TargetControlID="txtClaimToDt" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtClaimToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1">Claim ID
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtClaimID" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">Status
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpClaimStatus" runat="server" CssClass="TextBoxStyle" Width="100px">
                            <asp:ListItem Text="--- All ---" Value=""></asp:ListItem>
                            <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                            <asp:ListItem Text="Generated" Value="Generated"></asp:ListItem>
                            <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>


            <td>
                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                    <ContentTemplate>
                        <asp:CheckBox ID="chkShowRejectInv" runat="server" CssClass="lblCaption1" Checked="true" Text="Show only Rejected Invoice" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnClaimSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small"
                            OnClick="btnClaimSearch_Click" Text="Search" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvRemittanceClaims" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvRemittanceClaims_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:CheckBox ID="chkInvResub" runat="server" CssClass="label" AutoPostBack="true" OnCheckedChanged="chkboxSelect_CheckedChanged" />
                                            <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="SelectClaim_Click" />



                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SL.No" SortExpression="HPV_SEQNO" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerial" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("ClaimId") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CLAIM ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("ActualFileName") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblIDPayer" CssClass="GridRow" runat="server" Text='<%# Bind("IDPayer") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblClaimID" CssClass="GridRow" runat="server" Font-Bold="true" Text='<%# Bind("ClaimId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DATE" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("START1") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblCompanyName" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REFERENCE NO" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>


                                            <asp:Label ID="lblRefNo" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentReference") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SERV. COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCount" CssClass="GridRow" runat="server" Text='<%# Bind("Count") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PATIENT SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalPTShare" CssClass="GridRow" Font-Bold="true" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYMENT AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblPaymentAmount" CssClass="GridRow" Font-Bold="true" runat="server" Text='<%# Bind("PaymentAmount") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="RESUB. STATUS" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="lblReSubStatus" CssClass="GridRow" Font-Bold="true" runat="server" Text='<%# Bind("ReSubStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
    </div>

    <br />
    <table width="80%">
        <tr>
            <td style="width: 100px" class="lblCaption1">File No  

            </td>

            <td>
                <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFileNo" runat="server" Font-Bold="true" Font-Size="11" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 100px" class="lblCaption1">Name

            </td>

            <td colspan="3">
                <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxStyle" Width="250px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 100px" class="lblCaption1">Ins.Card#

            </td>

            <td>
                <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtIdNo" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td style="width: 100px" class="lblCaption1">IDPayer <span style="color: red;">* </span>

            </td>

            <td>
                <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtIDPayer" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="30"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">Policy No <span style="color: red;">* </span>

            </td>

            <td>
                <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPolicyNo" runat="server" CssClass="TextBoxStyle" Width="120px" MaxLength="30"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">Resub. Type
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpReType" runat="server" CssClass="TextBoxStyle" Width="150px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpReType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">Resub. #
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtResubNo" runat="server" CssClass="TextBoxStyle" Font-Bold="true" Font-Size="11" Width="150px" MaxLength="30" ReadOnly="true"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Type
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpClmType" runat="server" CssClass="TextBoxStyle" Width="150px">
                            <asp:ListItem Value="1" Selected="True">No Bed + No Emergency Room (OP)</asp:ListItem>
                            <asp:ListItem Value="2">No Bed + Emergency Room (OP)</asp:ListItem>
                            <asp:ListItem Value="3">InPatient Bed + No Emergency Room (IP)</asp:ListItem>
                            <asp:ListItem Value="4">InPatient Bed + Emergency Room (IP)</asp:ListItem>
                            <asp:ListItem Value="5">Daycase Bed + No Emergency Room (Day Care)</asp:ListItem>
                            <asp:ListItem Value="6">Daycase Bed + Emergency Room (Day Care)</asp:ListItem>
                            <asp:ListItem Value="7">National Screening</asp:ListItem>
                            <asp:ListItem Value="12">Homecare</asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1">Start Date  <span style="color: red;">* </span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtClmStartDate" runat="server" Width="75px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                            Enabled="True" TargetControlID="txtClmStartDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtClmStartDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        <asp:TextBox ID="txtClmFromTime" runat="server" Text="00:00" Width="30px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">End Date  <span style="color: red;">* </span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtClmEndDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                            Enabled="True" TargetControlID="txtClmEndDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtClmEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        <asp:TextBox ID="txtClmToTime" runat="server" Text="00:00" Width="30px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">Invoice #
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" Font-Bold="true" Font-Size="11" Width="150px" MaxLength="30" ReadOnly="true"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Authorization ID
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel48" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPriorAuthorizationID" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="30"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <td class="lblCaption1">Start Type
                </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpClmStartType" runat="server" CssClass="TextBoxStyle" Width="120px">
                            <asp:ListItem Value="1" Selected="True">Elective</asp:ListItem>
                            <asp:ListItem Value="2">Emergency</asp:ListItem>
                            <asp:ListItem Value="3">Transfer</asp:ListItem>
                            <asp:ListItem Value="4">Live Birth</asp:ListItem>
                            <asp:ListItem Value="5">Still Birth</asp:ListItem>
                            <asp:ListItem Value="6">Dead on Arrival</asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1">End Type 
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpClmEndType" runat="server" CssClass="TextBoxStyle" Width="150px">
                            <asp:ListItem Value="1" Selected="True">Discharged with approval</asp:ListItem>
                            <asp:ListItem Value="2">Discharged against advice</asp:ListItem>
                            <asp:ListItem Value="3">Discharged absent without leave</asp:ListItem>
                            <asp:ListItem Value="4">Transfer to another facility</asp:ListItem>
                            <asp:ListItem Value="5">Deceased</asp:ListItem>

                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1">Refe. #
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtReferenceNo" CssClass="TextBoxStyle" runat="server" Height="22px" Width="150px"></asp:TextBox>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Attachment
            </td>
            <td colspan="2">
                <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="300px">
                            <asp:FileUpload ID="filAttachment" runat="server" Width="300px" CssClass="ButtonStyle" />
                        </asp:Panel>
                    </ContentTemplate>

                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Button ID="btnAttaAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px; height: 22px;" CssClass="button gray small"
                    OnClick="btnAttaAdd_Click" Text="Add" />
            </td>
            <td style="width: 100px; height: 30px;" class="lblCaption1">File Name
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblFileName" runat="server" CssClass="label" Visible="false"></asp:Label>
                        <asp:Label ID="lblFileNameActual" runat="server" CssClass="lblCaption1"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
 <asp:ImageButton ID="DeleteAttach" runat="server" ToolTip="Delete" Visible="false" ImageUrl="~/Images/icon_delete.png"
     OnClick="DeleteAttach_Click" />

                    </ContentTemplate>
                </asp:UpdatePanel>


            </td>
        </tr>
    </table>
    <asp:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabServices" HeaderText="Services" Width="100%">
            <ContentTemplate>
                <div style="padding-top: 0px; width: 80%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvInvTran" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="99%" PageSize="200" OnRowDataBound="gvInvTran_RowDataBound">
                                            <HeaderStyle CssClass="GridHeader_Gray" />
                                            <RowStyle CssClass="GridRow" />

                                            <PagerStyle CssClass="label" Font-Bold="true" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="CLAIM ID" ItemStyle-VerticalAlign="Top" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtgvInvoiceID" runat="server" CssClass="TextBoxStyle" HeaderStyle-Width="80px" Height="30px" Text='<%# Bind("HIT_INVOICE_ID") %>' ReadOnly="true" Width="50px" MaxLength="10"></asp:TextBox>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="CODE" HeaderStyle-Width="70px">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblSlno" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HIT_SLNO") %>' Width="70px"></asp:Label>
                                                                <asp:Label ID="lblServCode" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HIT_SERV_CODE") %>' Width="70px"></asp:Label>
                                                                <asp:TextBox ID="txtServCode" runat="server" CssClass="TextBoxStyle" HeaderStyle-Width="50px" Height="30px" Text='<%# Bind("HIT_SERV_CODE") %>' ReadOnly="true" Width="50px" MaxLength="10"></asp:TextBox>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="txtServCode" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SERVICE NAME" HeaderStyle-Width="300px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtServeDesc" runat="server" CssClass="TextBoxStyle" Width="300px" Height="30px" Text='<%# Bind("HIT_DESCRIPTION") %>' ReadOnly="true"></asp:TextBox>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="QTY" HeaderStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQTY" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="30px" Height="30px" Text='<%# Bind("HIT_QTY") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CLAIM AMT." ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtgvClaimAmt" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_COMP_TOTAL", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PT. SHARE" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPTAmount" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PAID AMT." ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_CLAIM_AMOUNT_PAID", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="REJECT" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_CLAIM_REJECTED", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="RESUBMIT" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubmitted" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_RESUBMITTED") %>' Visible="false"></asp:Label>
                                                        <asp:CheckBox ID="chkSubmit" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="RESUBMISSION COMMENTS" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtResubComment" runat="server" CssClass="TextBoxStyle" Width="400px" Height="30px" TextMode="MultiLine" Text='<%# Bind("TransReComment") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DENIAL CODE" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDenial" runat="server" CssClass="TextBoxStyle" Style="padding-right: 5px;" Width="70px" Height="30px" Text='<%# Bind("HIT_DENIAL_CODE") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DENIAL DESC." ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>

                                                        <asp:TextBox ID="txtDenialDesc" runat="server" CssClass="TextBoxStyle" Style="padding-right: 5px;" Width="200px" Height="30px" Text='<%# Bind("HIT_DENIAL_DESC") %>' ReadOnly="true"></asp:TextBox>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="FEE" HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtFee" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_FEE", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDiscType" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' Visible="false"></asp:Label>
                                                        <asp:DropDownList ID="drpDiscType" CssClass="TextBoxStyle" runat="server" Width="50px" Height="30px" Enabled="false">
                                                            <asp:ListItem Value="$">$</asp:ListItem>
                                                            <asp:ListItem Value="%">%</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="D.AMT." HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_DISC_AMT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHitAmount" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_AMOUNT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DED." HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDeduct" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CO-%" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCoInsType" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' Visible="false"></asp:Label>
                                                        <asp:DropDownList ID="drpCoInsType" CssClass="TextBoxStyle" runat="server" Width="50px" Height="30px" Enabled="false">
                                                            <asp:ListItem Value="$">$</asp:ListItem>
                                                            <asp:ListItem Value="%">%</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CO-INS" HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCoInsAmt" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="30px" Text='<%# Eval("HIT_CO_INS_AMT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="HAAD CODE" HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHaadCode" runat="server" CssClass="TextBoxStyle" Width="70px" Height="30px" Text='<%# Bind("HIT_HAAD_CODE") %>' ReadOnly="true"></asp:TextBox>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="START DATE" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtStartDt" runat="server" Width="100px" Height="30px" Text='<%# Bind("HIT_STARTDATEDesc") %>' Enabled="false" Style="border: thin; border-color: #CCCCCC; border-style: groove;"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="OBSERV. TYPE" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblHIOType" CssClass="TextBoxStyle" Style="padding-right: 5px;" runat="server" Height="30px" Text='<%# Bind("HIO_TYPE") %>' Width="100px"></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OBSERV. CODE" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHIOCode" runat="server" CssClass="TextBoxStyle" Height="30px" Text='<%# Bind("HIO_CODE") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OBSERV. DESC." HeaderStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHIODesc" runat="server" CssClass="TextBoxStyle" Height="30px" Text='<%# Bind("HIO_DESCRIPTION") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OBSERV. VALUE" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHIOValue" runat="server" CssClass="TextBoxStyle" Height="30px" Text='<%# Bind("HIO_VALUE") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OBSERV. VALUETYPE" HeaderStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHIOValueType" runat="server" CssClass="TextBoxStyle" Height="30px" Text='<%# Bind("HIO_VALUETYPE") %>' ReadOnly="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>

                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabDiagnosis" HeaderText="Diagnosis" Width="100%">
            <ContentTemplate>
                <table>
                    <tr>
                        <td class="lblCaption1" style="width: 105px;">Type <span style="color: red;">* </span>
                        </td>
                        <td class="lblCaption1" style="width: 105px;">Code <span style="color: red;">* </span>
                        </td>
                        <td class="lblCaption1">Description
                        </td>



                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="drpDiagType" runat="server" CssClass="TextBoxStyle" Width="100px">
                                <asp:ListItem Value="Principal" Selected>Principal</asp:ListItem>
                                <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                <asp:ListItem Value="Admitting">Admitting</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                        <td>

                            <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onblur="return DiagIdSelected()"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDiagName" runat="server" Width="400px" Height="22px" CssClass="TextBoxStyle" onblur="return DiagNameSelected()"></asp:TextBox>

                            <div id="divDiagExt" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="Server" TargetControlID="txtDiagCode" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisID"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtDiagName" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt">
                            </asp:AutoCompleteExtender>


                        </td>

                        <td>
                            <asp:Button ID="btnInvDiagAdd" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 50px;" CssClass="button gray small"
                                OnClick="btnInvDiagAdd_Click" Text="Add" OnClientClick="return DiagAddVal();" />

                        </td>
                    </tr>

                </table>
                <div style="padding-top: 0px; width: 80%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvInvoiceDiag" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="99%" GridLines="Both">
                                            <HeaderStyle CssClass="GridHeader_Gray" />
                                            <RowStyle CssClass="GridRow" BorderStyle="Solid" BorderWidth="1px" />

                                            <Columns>
                                                <asp:TemplateField HeaderText="Type">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblICDType" Width="70px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_TYPE") %>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblICDCode" Width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_CODE") %>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblICDDesc" Width="400px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_DESC") %>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                            OnClick="DeletegvDiag_Click" />&nbsp;&nbsp;
                                                
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>





                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="History" Width="100%">
            <ContentTemplate>
                <div runat="server" id="divHistory" style="padding-left: 5px; padding-top: 0px; width: 80%; height: 400px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
                    <asp:UpdatePanel ID="UpdatePanel46" runat="server">
                        <ContentTemplate>
                            <span class="lblCaption1">Services Details</span><br />
                            <asp:GridView ID="gvResubTransHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="99%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader_Gray" />
                                <RowStyle CssClass="GridRow" />

                                <PagerStyle CssClass="label" Font-Bold="true" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Resub. No." ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox24" runat="server" CssClass="TextBoxStyle" Style="border: none;" HeaderStyle-Width="100px" BorderWidth="1px" Text='<%# Bind("ResubNo") %>' ReadOnly="true" Width="100px" MaxLength="10"></asp:TextBox>
                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="TextBoxStyle" Style="border: none;" HeaderStyle-Width="100px" BorderWidth="1px" Text='<%# Bind("HIT_INVOICE_ID") %>' Visible="false" ReadOnly="true" Width="100px" MaxLength="10"></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Service Code" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HIT_SERV_CODE") %>' Width="70px"></asp:Label>
                                            <asp:TextBox ID="TextBox4" runat="server" CssClass="TextBoxStyle" Style="border: none;" HeaderStyle-Width="100px" BorderWidth="1px" Text='<%# Bind("HIT_SERV_CODE") %>' ReadOnly="true" Width="100px" MaxLength="10"></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service Name" HeaderStyle-Width="300px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox5" runat="server" CssClass="TextBoxStyle" Style="border: none;" Width="300px" Text='<%# Bind("HIT_DESCRIPTION") %>' ReadOnly="true"></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QTY" HeaderStyle-Width="30px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="30px" Text='<%# Bind("HIT_QTY") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Claim Amt." ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox7" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_COMP_TOTAL", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Patient Share" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox8" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Amt." ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox9" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_CLAIM_AMOUNT_PAID", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox10" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_CLAIM_REJECTED", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Denial Code" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox11" runat="server" CssClass="TextBoxStyle" Style="padding-right: 5px; border: none;" Width="70px" Text='<%# Bind("HIT_DENIAL_CODE") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Denial Desc" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>

                                            <asp:TextBox ID="TextBox12" runat="server" CssClass="TextBoxStyle" Style="padding-right: 5px; border: none;" Width="200px" Text='<%# Bind("HIT_DENIAL_DESC") %>' ReadOnly="true"></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Resub Type" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" CssClass="GridRow" runat="server" Width="100px" Text='<%# Bind("TransReType") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Resubmission Comments" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox13" runat="server" CssClass="TextBoxStyle" Width="400px" Style="border: none;" TextMode="MultiLine" Text='<%# Bind("TransReComment") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox14" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_FEE", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="70px" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' Width="70px"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="D.Amt" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox15" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_DISC_AMT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox16" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_AMOUNT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ded." HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox17" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Co-%" HeaderStyle-Width="70px" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' Width="70px"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox18" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Text='<%# Eval("HIT_CO_INS_AMT", "{0:0.00}") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox19" runat="server" CssClass="TextBoxStyle" Style="border: none;" Width="70px" Text='<%# Bind("HIT_HAAD_CODE") %>' ReadOnly="true"></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Observ. Type" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>

                                            <asp:Label ID="Label7" CssClass="GridRow" Style="text-align: right; padding-right: 5px;" runat="server" Text='<%# Bind("HIO_TYPE") %>' Width="70px"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observ. Code" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox20" runat="server" CssClass="TextBoxStyle" Style="border: none;" Text='<%# Bind("HIO_CODE") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observ. Description" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox21" runat="server" CssClass="TextBoxStyle" Style="border: none;" Text='<%# Bind("HIO_DESCRIPTION") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observ. Value" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox22" runat="server" CssClass="TextBoxStyle" Style="border: none;" Text='<%# Bind("HIO_VALUE") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observ. ValueType" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox23" runat="server" CssClass="TextBoxStyle" Style="border: none;" Text='<%# Bind("HIO_VALUETYPE") %>' ReadOnly="true"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>

                            </asp:GridView>
                            <br />
                            <span class="lblCaption1">Diagnosis Details</span><br />
                            <asp:GridView ID="gvResubClaimsHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="99%" gridline="none">
                                <HeaderStyle CssClass="GridHeader_Gray" />
                                <RowStyle CssClass="GridRow" />

                                <Columns>
                                    <asp:TemplateField HeaderText="Resub. No." HeaderStyle-Width="100px">
                                        <ItemTemplate>

                                            <asp:Label ID="Label8" Width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_RESUBNO") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="100px">
                                        <ItemTemplate>

                                            <asp:Label ID="lblICDCode" Width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_CODE") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>

                                            <asp:Label ID="lblICDDesc" Width="400px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_DESC") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>

                                            <asp:Label ID="lblICDType" Width="70px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_TYPE") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Medical Records" Width="100%">
            <ContentTemplate>
                <div runat="server" id="div1" style="padding-left: 5px; padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
                    <table style="width: 100%" class="gridspacy">
                        <tr>
                            <td class="lblCaption1" width="100px">From Date: 
                            </td>
                            <td style="width: 400px">
                                <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtEMR_PTMaster_FromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender7" runat="server"
                                            Enabled="True" TargetControlID="txtEMR_PTMaster_FromDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="true" TargetControlID="txtEMR_PTMaster_FromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        &nbsp;

 
                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                                Text="To"></asp:Label>

                                        <asp:TextBox ID="txtEMR_PTMaster_ToDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender8" runat="server"
                                            Enabled="True" TargetControlID="txtEMR_PTMaster_ToDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender8" runat="server" Enabled="true" TargetControlID="txtEMR_PTMaster_ToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        &nbsp;
                         <asp:Button ID="btnEMR_PTMasterRefresh" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button red small"
                             OnClick="btnEMR_PTMasterRefresh_Click" Text="Refresh" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="text-align: right;">
                                <asp:DropDownList ID="drpEMRReport" runat="server" CssClass="TextBoxStyle">
                                    <asp:ListItem Value="ClinicalSummary" Text="Clinical Summary" Selected="True"></asp:ListItem>
                                  
                                </asp:DropDownList>
                            </td>

                        </tr>
                    </table>
                    <table style="width: 100%" class="gridspacy">
                        <tr>
                            <td class="lblCaption1  BoldStyle">
                                <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvEMR_PTMaster" runat="server" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="GridHeader_Blue" Height="20px" Font-Bold="true" />
                                            <RowStyle CssClass="GridRow" Height="20px" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Emr ID">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblGVEmrDeptName" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DEP_NAME") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblgvEMR_PTMaster_Emr_ID" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblgvEMR_PTMaster_Date" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DATEDesc") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Doctor">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblgvEMR_PTMaster_DrCode" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DR_CODE") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblBMI" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DR_NAME") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblgvEMR_PTMaster_InsCode" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_INS_CODE") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_STATUES") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgPrint" runat="server" OnClick="gvEMR_PTMasterPrint_Click" ImageUrl="~/EMR/WebReports/Images/printer.png" Style="height: 25px; width: 30px; border: none;" />
                                                    </ItemTemplate>


                                                </asp:TemplateField>
                                            </Columns>


                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>
    <table width="80%">
        <tr>

            <td class="label" style="width: 100%; font-weight: bold;">
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        Total No. of Claims :&nbsp;<asp:Label ID="lblTotalClaims" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                        &nbsp;&nbsp; &nbsp; &nbsp; Total Claim :  &nbsp;<asp:Label ID="lblTotalAmount" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
    </table>
    <table width="80%" align="center">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnTestEclaim" runat="server" CssClass="button gray small" Width="150px"
                            OnClick="btnTestEclaim_Click" Text="Test E-Claim" />

                        <asp:Button ID="btnProductionEclaim" runat="server" CssClass="button gray small" Width="150px"
                            OnClick="btnProductionEclaim_Click" Text="Production E-Claim" />


                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

            <td style="float: right;">
                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                    <ContentTemplate>

                        <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="150px"
                            OnClick="btnDelete_Click" OnClientClick="return DeleteConfirm();" Text="Delete" />

                        <asp:Button ID="btnUpdate" runat="server" CssClass="button gray small" Width="150px"
                            OnClick="btnUpdate_Click" OnClientClick="return SaveVal1();" Text="Update" />

                        <asp:Button ID="btnSaveToDB" runat="server" CssClass="button red small" Width="150px"
                            OnClick="btnSaveToDB_Click" OnClientClick="return SaveVal();" Text="Resubmssion Generate" />

                        <asp:Button ID="btnDispense" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button red small" OnClick="btnDispense_Click" Text="Dispense" Visible="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td style="height: 50px;" colspan="4"></td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin; display: none;">
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvRemittanceClaimTrans" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>

                                    <asp:TemplateField HeaderText="CLAIM ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("ActualFileName") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblClaimID" CssClass="GridRow" runat="server" Text='<%# Bind("ClaimId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SERV. ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("Code2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QTY" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>


                                            <asp:Label ID="lbQuantity" CssClass="GridRow" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net1") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PATIENT SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYMENT AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblPaymentAmount" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentAmount") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DENIAL CODE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblDenialCode" CssClass="GridRow" runat="server" Text='<%# Bind("DenialCode") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
    </div>
    <br />
    <br />
</asp:Content>
