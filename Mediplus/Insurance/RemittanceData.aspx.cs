﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.IO;
using System.Data;

namespace Mediplus.Insurance
{
    public partial class RemittanceData : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HBM_BANKNAME", "HMS_BANK_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "HBM_BANKNAME";
                drpBank.DataValueField = "HBM_BANKNAME";
                drpBank.DataBind();

                drpCheqBank.DataSource = DS;
                drpCheqBank.DataTextField = "HBM_BANKNAME";
                drpCheqBank.DataValueField = "HBM_BANKNAME";
                drpCheqBank.DataBind();
            }
            drpBank.Items.Insert(0, "--- Select ---");
            drpBank.Items[0].Value = "";

            drpCheqBank.Items.Insert(0, "--- Select ---");
            drpCheqBank.Items[0].Value = "";

        }

        void BindData()
        {

            string Criteria = "  1=1   ";
            string strStartDate = txteAuthFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txteAuthFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txteAuthToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txteAuthToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) <= '" + strForToDate + "'";
            }


            if (txtCompany.Text.Trim() != "")
            {
                Criteria += " AND HCM_BILL_CODE='" + txtCompany.Text.Trim() + "'";
            }

            if (txtReferenceNo.Text.Trim() != "")
            {
                Criteria += " AND PaymentReference='" + txtReferenceNo.Text.Trim() + "'";
            }

            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.RemittanceXMLDataGroupByGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceData.DataSource = DS;
                gvRemittanceData.DataBind();

            }


        }

        //void GetFileName()
        //{

        //    string Criteria = "  1=1   ";


        //    Criteria += " AND PaymentReference='" + txtRemiReferenceNo.Text.Trim() + "'";


        //    dboperations objDp = new dboperations();
        //    DataSet DS = new DataSet();
        //    DS = objDp.RemittanceXMLDataGroupByGet(Criteria);
        //    if (DS.Tables[0].Rows.Count > 0)
        //    { 

        //    }


        //}

        string GetAccountName(string AccCode)
        {
            string AccName = "";
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_CODE = '" + AccCode + "'";

            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {

                AccName = Convert.ToString(ds.Tables[0].Rows[0]["AAM_ACCOUNT_NAME"]).Trim();

            }

            return AccName;
        }

        void GetCompanyDtls(out string CompAcCode, out string CompAcName)
        {
            CompAcCode = "";
            CompAcName = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' ";
            Criteria += " AND HCM_COMP_ID = '" + txtRemiCompany.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_inccmp_details(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                CompAcCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_ID"]);
                CompAcName = GetAccountName(CompAcCode);
            }
        }

        void SaveJournal()
        {
            string CompAcCode, CompAcName;
            GetCompanyDtls(out CompAcCode, out  CompAcName);


            JournalEntryBAL objJour = new JournalEntryBAL();


            objJour = new JournalEntryBAL();


            objJour.BranchID = Convert.ToString(Session["Branch_ID"]);
            objJour.AJM_JOURNAL_NO = txtJournalNo.Text.Trim();
            objJour.AJM_DATE = txtTransDate.Text.Trim();
            objJour.AJM_STATUS = "Active";
            objJour.AJM_DESCRIPTION = txtDescription.Text;
            objJour.AJM_REF_NO = txtRemiReferenceNo.Text.Trim();
            objJour.AJM_DR_TOTAL_AMOUNT = txtReceivedAmount.Text;
            objJour.AJM_CR_TOTAL_AMOUNT = txtReceivedAmount.Text;
            objJour.AJM_TOTAL_AMOUNT = txtReceivedAmount.Text;
            objJour.AJM_DESCRIPTION = txtDescription.Text.Trim();


            objJour.AJM_ENTRY_FROM = "REMITTANCE ENTRY";


            objJour.AJM_CHEQUE_NO = txtCheqNo.Text.Trim();
            objJour.AJM_CHEQUE_AMOUNT = txtCheqAmt.Text.Trim();
            objJour.AJM_CHEQUE_DATE = txtCheqDate.Text.Trim();
            objJour.AJM_CHEQUE_BANK_NAME = drpCheqBank.SelectedValue;

            objJour.AJM_CC_TYPE = "";
            objJour.AJM_CC_NO = "";
            objJour.AJM_CC_HOLDER = "";

            objJour.AJM_BANK_PAY_DATE = txtBnkPayDate.Text.Trim();
            objJour.AJM_BANK_NAME = txtBnkAmt.Text.Trim();
            objJour.AJM_BANK_REF_NO = txtBnkRefNo.Text.Trim();
            objJour.AJM_BANK_AMOUNT = txtBnkAmt.Text.Trim();

            objJour.AJM_SUPPLIER_CODE = "";
            objJour.AJM_SUPPLIER_NAME = "";
            objJour.AJM_CUSTOMER_CODE = txtRemiCompany.Text.Trim();
            objJour.AJM_CUSTOMER_NAME = txtRemiCompanyName.Text.Trim();

            objJour.MODE = Convert.ToString(ViewState["SaveMode"]);
            objJour.UserID = Convert.ToString(Session["User_ID"]);
            objJour.JournalMasterAdd();



            CommonBAL objCom = new CommonBAL();

            string Criteria = " 1=1 ";


            Criteria += " AND AJT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  AJT_JOURNAL_NO ='" + txtJournalNo.Text.Trim() + "'";

            objCom.fnDeleteTableData("AC_JOURNAL_TRANSACTION", Criteria);


            objJour = new JournalEntryBAL();
            objJour.BranchID = Convert.ToString(Session["Branch_ID"]);
            objJour.AJT_JOURNAL_NO = txtJournalNo.Text.Trim();
            objJour.AJT_TRANS_ID = "0";
            objJour.AJT_CATEGORY = "BANK";
            objJour.AJT_ACC_CODE = "";
            objJour.AJT_ACC_NAME = "";
            objJour.AJT_COST_CENT_CODE = "";
            objJour.AJT_COST_CENT_NAME = "";
            objJour.AJT_DEBIT_AMOUNT = txtReceivedAmount.Text;
            objJour.AJT_CREDIT_AMOUNT = "0";

            objJour.JournalTransactionAdd();


            objJour = new JournalEntryBAL();
            objJour.BranchID = Convert.ToString(Session["Branch_ID"]);
            objJour.AJT_JOURNAL_NO = txtJournalNo.Text.Trim();
            objJour.AJT_TRANS_ID = "0";
            objJour.AJT_CATEGORY = "CUSTOMER";
            objJour.AJT_ACC_CODE = CompAcCode;
            objJour.AJT_ACC_NAME = CompAcName;
            objJour.AJT_COST_CENT_CODE = "";
            objJour.AJT_COST_CENT_NAME = "";
            objJour.AJT_DEBIT_AMOUNT = "0";
            objJour.AJT_CREDIT_AMOUNT = txtReceivedAmount.Text;

            objJour.JournalTransactionAdd();





        }

        void Clear()
        {
            lblRemiFileName.Text = "";
            txtRemiSenderID.Text = "";
            txtRemiCompany.Text = "";

            txtRemiCompanyName.Text = "";
            txtRemiReferenceNo.Text = "";
            txtJournalNo.Text = "";
            txtRemiReferenceNo.Text = "";

            drpPayType.SelectedIndex = 0;
            txtRemiPaymentAmount.Text = "";
            txtReceivedAmount.Text = "";
            txtPendingAmount.Text = "";
            txtDescription.Text = "";


            txtBnkAmt.Text = "";
            txtBnkRefNo.Text = "";
            if (drpBank.Items.Count > 0)
            {
                drpBank.SelectedIndex = 0;
            }

            txtCheqNo.Text = "";
            txtCheqAmt.Text = "";
            txtCheqDate.Text = "";

            if (drpBank.Items.Count > 0)
            {
                drpBank.SelectedIndex = 0;
            }
            divBank.Visible = false;
            divCheque.Visible = false;



        }
        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' ";
            Criteria += " AND HCM_COMP_ID Like '" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A'   ";
            Criteria += " AND HCM_NAME Like '" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                ViewState["SaveMode"] = "A";
                txteAuthFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txteAuthToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                txtTransDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                CommonBAL objCom = new CommonBAL();
                txtJournalNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_JOURNAL");
                BindBank();

                BindData();

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                CommonBAL objCom = new CommonBAL();
                txtJournalNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "AC_JOURNAL");

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvRemittanceData.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblRefNo = (Label)gvScanCard.Cells[0].FindControl("lblRefNo");
                Label lblCompanyID = (Label)gvScanCard.Cells[0].FindControl("lblCompanyID");
                Label lblCompanyName = (Label)gvScanCard.Cells[0].FindControl("lblCompanyName");
                Label lblPaymentAmount = (Label)gvScanCard.Cells[0].FindControl("lblPaymentAmount");
                Label lblSenderID = (Label)gvScanCard.Cells[0].FindControl("lblSenderID");
                Label lblActualFileName = (Label)gvScanCard.Cells[0].FindControl("lblActualFileName");


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowReceiptAddPopup()", true);

                txtRemiReferenceNo.Text = lblRefNo.Text;
                txtRemiCompany.Text = lblCompanyID.Text;
                txtRemiCompanyName.Text = lblCompanyName.Text;
                txtRemiSenderID.Text = lblSenderID.Text;
                lblRemiFileName.Text = lblActualFileName.Text;


                txtRemiPaymentAmount.Text = lblPaymentAmount.Text;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RemittanceData.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divBank.Visible = false;
                divCheque.Visible = false;

                if (drpPayType.SelectedItem.Text == "Bank")
                {

                    divBank.Visible = true;

                    if (txtBnkAmt.Text.Trim() == "0" || txtBnkAmt.Text.Trim() == "0.00" || txtBnkAmt.Text.Trim() == "0.000")
                    {

                        txtBnkAmt.Text = txtRemiPaymentAmount.Text;

                    }
                }
                else if (drpPayType.SelectedItem.Text == "Cheque" || drpPayType.SelectedItem.Text == "PDC")
                {
                    // txtCashAmt.Text = "0.00";
                    divCheque.Visible = true;
                    if (txtCheqAmt.Text.Trim() == "0" || txtCheqAmt.Text.Trim() == "0.00" || txtCheqAmt.Text.Trim() == "0.000")
                    {

                        txtCheqAmt.Text = txtRemiPaymentAmount.Text;

                    }

                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpPayType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveJournal();
            Clear();
            lblStatus.Text = "Data  Saved";
            lblStatus.ForeColor = System.Drawing.Color.Green;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideReceiptAddPopup()", true);

        }
    }
}