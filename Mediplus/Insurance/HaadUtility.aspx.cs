﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using Mediplus.ShafafiyaService;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.SqlClient;
using Mediplus.DHAService;


using Mediplus_BAL;


namespace Mediplus.Insurance
{
    public partial class HaadUtility : System.Web.UI.Page
    {

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void LogFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../MediTransLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindProvider()
        {

            DataSet ds = new DataSet();
            CommonBAL objphyCom = new CommonBAL();

            string Criteria = " 1=1 AND HAC_STATUS='A' ";
            ds = objphyCom.AuthorizationCredentialGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpProvider.DataSource = ds;
                drpProvider.DataValueField = "HAC_PROVIDER_ID";
                drpProvider.DataTextField = "HAC_PROVIDER_NAME";
                drpProvider.DataBind();



            }

            drpProvider.Items.Insert(0, "--- Select ---");
            drpProvider.Items[0].Value = "";




            if (drpProvider.Items.Count == 2)
            {
                drpProvider.SelectedIndex = 1;


            }
        }

        void BindSrcProvider()
        {

            DataSet ds = new DataSet();
            CommonBAL objphyCom = new CommonBAL();

            string Criteria = " 1=1  ";
            ds = objphyCom.AuthorizationCredentialGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                drpSrcProvider.DataSource = ds;
                drpSrcProvider.DataValueField = "HAC_PROVIDER_ID";
                drpSrcProvider.DataTextField = "HAC_PROVIDER_NAME";
                drpSrcProvider.DataBind();

            }



            drpSrcProvider.Items.Insert(0, "--- Select ---");
            drpSrcProvider.Items[0].Value = "";



        }

        void BuildUploadTrans()
        {
            DataTable dt = new DataTable();

            DataColumn SenderID = new DataColumn();
            SenderID.ColumnName = "SenderID";

            DataColumn ReceiverID = new DataColumn();
            ReceiverID.ColumnName = "ReceiverID";

            DataColumn FileID = new DataColumn();
            FileID.ColumnName = "FileID";

            DataColumn FileName = new DataColumn();
            FileName.ColumnName = "FileName";

            DataColumn TransactionDate = new DataColumn();
            TransactionDate.ColumnName = "TransactionDate";

            DataColumn RecordCount = new DataColumn();
            RecordCount.ColumnName = "RecordCount";


            DataColumn DispositionFlag = new DataColumn();
            DispositionFlag.ColumnName = "DispositionFlag";

            DataColumn IsUploaded = new DataColumn();
            IsUploaded.ColumnName = "IsUploaded";

            DataColumn ErrorReport = new DataColumn();
            ErrorReport.ColumnName = "ErrorReport";

            DataColumn FileLocation = new DataColumn();
            FileLocation.ColumnName = "FileLocation";

            dt.Columns.Add(SenderID);
            dt.Columns.Add(ReceiverID);
            dt.Columns.Add(FileID);
            dt.Columns.Add(FileName);
            dt.Columns.Add(TransactionDate);
            dt.Columns.Add(RecordCount);
            dt.Columns.Add(DispositionFlag);
            dt.Columns.Add(IsUploaded);
            dt.Columns.Add(ErrorReport);
            dt.Columns.Add(FileLocation);

            ViewState["UploadTrans"] = dt;
        }

        void BindDirection()
        {
            DataTable DT = new DataTable();

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";
            DT.Columns.Add(Name);


            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";
            DT.Columns.Add(Code);


            DataRow objrow;
            objrow = DT.NewRow();
            objrow["Name"] = "Received Only";
            objrow["COde"] = "2";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();


            objrow = DT.NewRow();
            objrow["Name"] = "Sent Only";
            objrow["COde"] = "1";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();

            if (drpPostOffice.SelectedValue == "Shafafiya")
            {
                objrow = DT.NewRow();
                objrow["Name"] = "All";
                objrow["COde"] = "-1";
                DT.Rows.Add(objrow);
                DT.AcceptChanges();
            }

            radDirection.DataSource = DT;
            radDirection.DataTextField = "Name";
            radDirection.DataValueField = "Code";
            radDirection.DataBind();

            if (drpPostOffice.SelectedValue == "Shafafiya")
            {
                radDirection.SelectedIndex = 2;
            }
            else
            {
                radDirection.SelectedIndex = 0;
            }
        }

        void BindTransStatus()
        {
            DataTable DT = new DataTable();

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";
            DT.Columns.Add(Name);


            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";
            DT.Columns.Add(Code);


            DataRow objrow;
            objrow = DT.NewRow();
            objrow["Name"] = "Downloaded Only";
            objrow["COde"] = "2";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();


            objrow = DT.NewRow();
            objrow["Name"] = "New Only";
            objrow["COde"] = "1";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();

            if (drpPostOffice.SelectedValue == "Shafafiya")
            {
                objrow = DT.NewRow();
                objrow["Name"] = "All";
                objrow["COde"] = "-1";
                DT.Rows.Add(objrow);
                DT.AcceptChanges();
            }

            radTransStatus.DataSource = DT;
            radTransStatus.DataTextField = "Name";
            radTransStatus.DataValueField = "Code";
            radTransStatus.DataBind();


            if (drpPostOffice.SelectedValue == "Shafafiya")
            {
                radTransStatus.SelectedIndex = 2;
            }
            else
            {
                radTransStatus.SelectedIndex = 0;
            }
        }

        void BindDownloadFileType()
        {
            DataTable DT = new DataTable();

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";
            DT.Columns.Add(Name);


            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";
            DT.Columns.Add(Code);


            DataRow objrow;
            objrow = DT.NewRow();
            objrow["Name"] = "Claim.Submission";
            objrow["COde"] = "2";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();


            objrow = DT.NewRow();
            objrow["Name"] = "Remittance.Advice";
            objrow["COde"] = "8";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();



            objrow = DT.NewRow();
            objrow["Name"] = "Prior.Request";
            objrow["COde"] = "16";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();


            objrow = DT.NewRow();
            objrow["Name"] = "Prior.Authorization";
            objrow["COde"] = "32";
            DT.Rows.Add(objrow);
            DT.AcceptChanges();




            if (drpPostOffice.SelectedValue == "Shafafiya")
            {
                objrow = DT.NewRow();
                objrow["Name"] = "All";
                objrow["COde"] = "-1";
                DT.Rows.Add(objrow);
                DT.AcceptChanges();
            }

            chkDownloadFileType.DataSource = DT;
            chkDownloadFileType.DataTextField = "Name";
            chkDownloadFileType.DataValueField = "Code";
            chkDownloadFileType.DataBind();



            if (drpPostOffice.SelectedValue == "Shafafiya")
            {
                chkDownloadFileType.SelectedIndex = 4;
            }
            else
            {
                chkDownloadFileType.SelectedIndex = 1;
            }

        }

        void AddUploadTrans(string Path)
        {


            DataTable dt = new DataTable();

            dt = (DataTable)ViewState["UploadTrans"];

            System.IO.DirectoryInfo RootDir = new System.IO.DirectoryInfo(Path);


            System.IO.FileInfo[] Files = RootDir.GetFiles();
            for (int FileCount = 0; FileCount < Files.Length; FileCount++)
            {
                DataSet DS_DownloadFile = new DataSet();

                DS_DownloadFile.ReadXml(Path + Files[FileCount].Name);

                Boolean IsHeader = false, IsAuthorization = false, IsActivity = false;

                foreach (DataTable table in DS_DownloadFile.Tables)
                {

                    if (table.TableName == "Header")
                    {
                        IsHeader = true;
                    }
                    if (table.TableName == "Authorization")
                    {
                        IsAuthorization = true;
                    }

                    if (table.TableName == "Activity")
                    {
                        IsActivity = true;
                    }

                }

                if (IsHeader == true)
                {
                    DataRow objrow;
                    objrow = dt.NewRow();

                    if (DS_DownloadFile.Tables["Header"].Columns.Contains("SenderID") == true)
                        objrow["SenderID"] = Convert.ToString(DS_DownloadFile.Tables["Header"].Rows[0]["SenderID"]);

                    if (DS_DownloadFile.Tables["Header"].Columns.Contains("ReceiverID") == true)
                        objrow["ReceiverID"] = Convert.ToString(DS_DownloadFile.Tables["Header"].Rows[0]["ReceiverID"]);

                    objrow["FileID"] = "";


                    objrow["FileName"] = Files[FileCount].Name;

                    if (DS_DownloadFile.Tables["Header"].Columns.Contains("TransactionDate") == true)
                        objrow["TransactionDate"] = Convert.ToString(DS_DownloadFile.Tables["Header"].Rows[0]["TransactionDate"]);

                    if (DS_DownloadFile.Tables["Header"].Columns.Contains("RecordCount") == true)
                        objrow["RecordCount"] = Convert.ToString(DS_DownloadFile.Tables["Header"].Rows[0]["RecordCount"]);

                    if (DS_DownloadFile.Tables["Header"].Columns.Contains("DispositionFlag") == true)
                        objrow["DispositionFlag"] = Convert.ToString(DS_DownloadFile.Tables["Header"].Rows[0]["DispositionFlag"]);



                    objrow["IsUploaded"] = "";
                    objrow["ErrorReport"] = "";
                    objrow["FileLocation"] = Path + Files[FileCount].Name;



                    dt.Rows.Add(objrow);

                }



            }





        }

        void BindUploadTrans()
        {


            DataTable dt = new DataTable();

            dt = (DataTable)ViewState["UploadTrans"];


            if (dt.Rows.Count > 0)
            {
                gvUploadTrans.Visible = true;
                gvUploadTrans.DataSource = dt;
                gvUploadTrans.DataBind();


            }
            else
            {
                gvUploadTrans.Visible = false;
                gvUploadTrans.DataBind();
            }
        }

        string SaveDataFromXMLFile(string FilePath, string FileName)
        {
            string strXMLFileName = "";


            if (FileName == "")
            {
                lblStatus.Text = "Please select the file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }



            DataSet DS = new DataSet();
            //  LogFileWriting(System.DateTime.Now.ToString() + " Read XML File Start ");
            DS.ReadXml(FilePath);
            // LogFileWriting(System.DateTime.Now.ToString() + " Read XML File End ");

            // LogFileWriting(System.DateTime.Now.ToString() + " Claim  Count " + DS.Tables["Claim"].Rows.Count);


            if (DS.Tables["Claim"].Rows.Count > 0)
            {
                string SenderID = Convert.ToString(DS.Tables["Header"].Rows[0]["SenderID"]);
                string ReceiverID = Convert.ToString(DS.Tables["Header"].Rows[0]["ReceiverID"]);


                string strTimeStamp = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;
                strXMLFileName = SenderID + "-" + ReceiverID + "-" + strTimeStamp + ".XML";

                for (int i = 0; i < DS.Tables["Claim"].Rows.Count; i++)
                {
                    string EClaimTransID = "";

                    string strTimeStamp1 = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;
                    EClaimTransID = SenderID + "-" + ReceiverID + "-" + strTimeStamp1;



                    DataTable DTActivity = new DataTable();
                    DataRow[] DRActivity;
                    DRActivity = DS.Tables["Activity"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                    DataTable DTEncounter = new DataTable();
                    DataRow[] DREncounter;
                    DREncounter = DS.Tables["Encounter"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                    DataTable DTDiagnosis = new DataTable();
                    DataRow[] DRDiagnosis;
                    DRDiagnosis = DS.Tables["Diagnosis"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());


                    for (int j = 0; j < DRActivity.Length; j++)
                    {
                        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con.Open();
                        SqlCommand cmd = new SqlCommand();
                        SqlDataAdapter adpt = new SqlDataAdapter();
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_EclaimxmlDataAdd";
                        cmd.Parameters.Add(new SqlParameter("@BranchID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                        cmd.Parameters.Add(new SqlParameter("@SenderID", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Header"].Rows[0]["SenderID"]);
                        cmd.Parameters.Add(new SqlParameter("@ReceiverID", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Header"].Rows[0]["ReceiverID"]);
                        cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Header"].Rows[0]["TransactionDate"]);
                        cmd.Parameters.Add(new SqlParameter("@RecordCount", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Header"].Rows[0]["RecordCount"]);
                        cmd.Parameters.Add(new SqlParameter("@DispositionFlag", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Header"].Rows[0]["DispositionFlag"]);


                        cmd.Parameters.Add(new SqlParameter("@ClaimID", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]);
                        cmd.Parameters.Add(new SqlParameter("@IDPayer", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["IDPayer"]);
                        cmd.Parameters.Add(new SqlParameter("@MemberID", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["MemberID"]);
                        cmd.Parameters.Add(new SqlParameter("@PayerID", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["PayerID"]);
                        cmd.Parameters.Add(new SqlParameter("@ProviderID", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["ProviderID"]);
                        cmd.Parameters.Add(new SqlParameter("@EmiratesIDNumber", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["EmiratesIDNumber"]);

                        cmd.Parameters.Add(new SqlParameter("@Gross", SqlDbType.Decimal)).Value = Convert.ToDecimal(DS.Tables["Claim"].Rows[i]["Gross"].ToString());
                        cmd.Parameters.Add(new SqlParameter("@PatientShare", SqlDbType.Decimal)).Value = Convert.ToDecimal(DS.Tables["Claim"].Rows[i]["PatientShare"].ToString());
                        cmd.Parameters.Add(new SqlParameter("@Net", SqlDbType.Decimal)).Value = Convert.ToDecimal(DS.Tables["Claim"].Rows[i]["Net"].ToString());


                        //  cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.TinyInt)).Value = Convert.ToInt32(DREncounter[0]["Type"]);
                        if (DS.Tables["Encounter"].Columns.Contains("FacilityID") == true)
                            cmd.Parameters.Add(new SqlParameter("@FacilityID", SqlDbType.VarChar)).Value = Convert.ToString(DREncounter[0]["FacilityID"].ToString());

                        if (DS.Tables["Encounter"].Columns.Contains("PatientID") == true)
                            cmd.Parameters.Add(new SqlParameter("@PatientID", SqlDbType.VarChar)).Value = Convert.ToString(DREncounter[0]["PatientID"]);

                        if (DS.Tables["Encounter"].Columns.Contains("Start") == true)
                            cmd.Parameters.Add(new SqlParameter("@EncounterStart", SqlDbType.VarChar)).Value = Convert.ToString(DREncounter[0]["Start"]);  // Convert.ToDateTime(DREncounter[0]["Start"].ToString()).ToString("dd/MM/yyyy HH:mm");

                        if (DS.Tables["Encounter"].Columns.Contains("End") == true)
                            cmd.Parameters.Add(new SqlParameter("@EncounterEnd", SqlDbType.VarChar)).Value = Convert.ToString(DREncounter[0]["End"]); // Convert.ToDateTime(DREncounter[0]["End"].ToString()).ToString("dd/MM/yyyy HH:mm");

                        if (DS.Tables["Encounter"].Columns.Contains("StartType") == true)
                            cmd.Parameters.Add(new SqlParameter("@EncounterStartType", SqlDbType.TinyInt)).Value = Convert.ToInt32(DREncounter[0]["StartType"]);

                        if (DS.Tables["Encounter"].Columns.Contains("EndType") == true)
                            cmd.Parameters.Add(new SqlParameter("@EncounterEndType", SqlDbType.TinyInt)).Value = Convert.ToInt32(DREncounter[0]["EndType"]);



                        for (int k = 0; k < DRDiagnosis.Length; k++)
                        {
                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Principal")
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDP", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Admitting")
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDA", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }
                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS1") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS1", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }
                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS2") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS2", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS3") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS3", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS4") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS4", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS5") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS5", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }
                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS6") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS6", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS7") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS7", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS8") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS8", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS9") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS9", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS10") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS10", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS11") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS11", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS12") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS12", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS13") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS13", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS14") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS14", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS15") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS15", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS16") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS16", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS17") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS17", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS18") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS18", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }


                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS19") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS19", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }

                            if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary" && cmd.Parameters.Contains("@ICDS20") == false)
                            {
                                cmd.Parameters.Add(new SqlParameter("@ICDS20", SqlDbType.VarChar)).Value = Convert.ToString(DRDiagnosis[k]["Code"]);
                                goto ForDigEnd;
                            }
                        ForDigEnd: ;
                        }



                        if (DS.Tables["Activity"].Columns.Contains("ID") == true && cmd.Parameters.Contains("@ActivityID") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@ActivityID", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["ID"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("Start") == true && cmd.Parameters.Contains("@ActivityStart") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@ActivityStart", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["Start"]); // Convert.ToDateTime(DRActivity[j]["Start"]);
                        }


                        if (DS.Tables["Activity"].Columns.Contains("Type") == true && cmd.Parameters.Contains("@ActivityType") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@ActivityType", SqlDbType.SmallInt)).Value = Convert.ToString(DRActivity[j]["Type"]);
                        }


                        if (DS.Tables["Activity"].Columns.Contains("Code") == true && cmd.Parameters.Contains("@ActivityCode") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@ActivityCode", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["Code"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("Quantity") == true && cmd.Parameters.Contains("@ActivityQty") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@ActivityQty", SqlDbType.Decimal)).Value = Convert.ToDecimal(DRActivity[j]["Quantity"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("Net") == true && cmd.Parameters.Contains("@ActivityNet") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@ActivityNet", SqlDbType.Decimal)).Value = Convert.ToDecimal(DRActivity[j]["Net"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("OrderingClinician") == true && cmd.Parameters.Contains("@OrderingClinician") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@OrderingClinician", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["OrderingClinician"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("Clinician") == true && cmd.Parameters.Contains("@Clinician") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@Clinician", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["Clinician"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("PriorAuthorizationID") == true && cmd.Parameters.Contains("@PriorAuthorizationID") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@PriorAuthorizationID", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["PriorAuthorizationID"]);
                        }


                        if (DS.Tables["Activity"].Columns.Contains("VAT") == true && cmd.Parameters.Contains("@VAT") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@VAT", SqlDbType.Decimal)).Value = Convert.ToDecimal(DRActivity[j]["VAT"]);
                        }

                        if (DS.Tables["Activity"].Columns.Contains("VATPercent") == true && cmd.Parameters.Contains("@VATPercent") == false)
                        {
                            cmd.Parameters.Add(new SqlParameter("@VATPercent", SqlDbType.VarChar)).Value = Convert.ToString(DRActivity[j]["VATPercent"]);
                        }

                        if (DS.Tables.Contains("Observation") == true)
                        {

                            if (DS.Tables["Observation"].Columns.Contains("ObservType") == true)
                                cmd.Parameters.Add(new SqlParameter("@ObservType", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Observation"].Rows[i]["ObservType"]);


                            if (DS.Tables["Observation"].Columns.Contains("ObservCode") == true)
                                cmd.Parameters.Add(new SqlParameter("@ObservCode", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Observation"].Rows[i]["ObservCode"]);


                            if (DS.Tables["Observation"].Columns.Contains("ObservValue") == true)
                                cmd.Parameters.Add(new SqlParameter("@ObservValue", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Observation"].Rows[i]["ObservValue"]);

                            if (DS.Tables["Observation"].Columns.Contains("ObservValueType") == true)
                                cmd.Parameters.Add(new SqlParameter("@ObservValueType", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Observation"].Rows[i]["ObservValueType"]);

                        }

                        if (DS.Tables.Contains("Resubmission") == true)
                        {


                            if (DS.Tables["Resubmission"].Columns.Contains("Type") == true && Convert.ToString(DS.Tables["Resubmission"].Rows[i]["Type"]) != "")
                            {
                                cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]);
                            }

                            if (DS.Tables["Resubmission"].Columns.Contains("Comment") == true)
                                cmd.Parameters.Add(new SqlParameter("@TransReComment", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Resubmission"].Rows[i]["Comment"]);

                            if (DS.Tables["Resubmission"].Columns.Contains("Type") == true)
                                cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Resubmission"].Rows[i]["Type"]);

                            cmd.Parameters.Add(new SqlParameter("@SubmissionType", SqlDbType.VarChar)).Value = "Resubmission";
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@SubmissionType", SqlDbType.VarChar)).Value = "Submission";
                        }


                        if (DS.Tables.Contains("Contract") == true)
                        {
                            if (DS.Tables["Contract"].Columns.Contains("PackageName") == true)
                                cmd.Parameters.Add(new SqlParameter("@PackageName", SqlDbType.VarChar)).Value = Convert.ToString(DS.Tables["Contract"].Rows[i]["PackageName"]);
                        }
                        cmd.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = strXMLFileName;//FileName

                        cmd.Parameters.Add(new SqlParameter("@ActualFileName", SqlDbType.VarChar)).Value = FileName;





                        cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);


                        for (int j1 = 0; j1 < cmd.Parameters.Count; j1++)
                        {
                            if (cmd.Parameters[j1].Value == "")
                            {
                                cmd.Parameters[j1].Value = DBNull.Value;
                            }
                        }

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }


            }


        BindEnd: ;

            return strXMLFileName;
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "MEDITRANSFER";
            objCom.ScreenName = "MediTransfer";
            objCom.ScreenType = "TRANSACTION";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        #endregion

        #region AuthorizationMethods
        void SearchDownloadFiles()
        {
            WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");
            string errorMessage, xmlTransactions = "";
            byte[] prescription, errorReport;
            //string responseMsg = "";
            //int result = soapclient.CheckForNewPriorAuthorizationTransactions(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), out errorMessage);
            //responseMsg += "result = " + result + ", Message: " + errorMessage;
            string foundTransactions = null;
            int transactionStatus = -1;
            for (int i = 0; i <= chkDownloadFileType.Items.Count - 1; i++)
            {

                if (chkDownloadFileType.Items[i].Selected == true)
                {
                    transactionStatus = Convert.ToInt32(chkDownloadFileType.Items[i].Value);
                }
            }
            LogFileWriting(System.DateTime.Now.ToString() + "Search Start ");
            soapclient.SearchTransactions(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), Convert.ToInt32(radDirection.SelectedValue), "", txtePartmenr.Text.Trim(), transactionStatus, Convert.ToInt32(radTransStatus.SelectedValue), txtFileName.Text.Trim(), txtFrmDt.Text.Trim() + " 00:00:00", txtToDt.Text.Trim() + " 23:59:59", -1, -1, out foundTransactions, out errorMessage);

            LogFileWriting(System.DateTime.Now.ToString() + "Search errorMessage :" + errorMessage);
            if (string.IsNullOrEmpty(foundTransactions) == false && !foundTransactions.Equals(""))
            {
                DataSet DS = new DataSet();


                //string strPath = Convert.ToString(ViewState["LOOKUP_FILEPATH"]);
                //StreamWriter oWrite;
                //oWrite = File.CreateText(strPath + "GetTransactionsList.xml");
                //oWrite.WriteLine(Convert.ToString(foundTransactions.Trim()));
                //oWrite.Close();


                Boolean IsFiles = false, IsFile = false;

                DS = ConvertXMLToDataSet(foundTransactions);

                //  LogFileWriting("Read XML File" + srtFileName);

                foreach (DataTable table in DS.Tables)
                {


                    if (table.TableName == "File")
                    {
                        IsFile = true;
                    }



                }
                if (IsFile == true)
                {

                    gvTrans.Visible = true;
                    gvTrans.DataSource = DS;
                    gvTrans.DataBind();

                    lblDownloadCount.Text = Convert.ToString(DS.Tables[0].Rows.Count);
                }
                else
                {
                    lblDownloadCount.Text = "0";
                    gvTrans.Visible = false;
                    gvTrans.DataBind();
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Claim Dtls.','No Data.')", true);


                }


                LogFileWriting(System.DateTime.Now.ToString() + "Search Completed ");
            }
            else
            {
                lblDownloadCount.Text = "0";
                gvTrans.Visible = false;
                gvTrans.DataBind();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Claim Dtls.','No Data.')", true);
            }
        }

        void SearchDownloadFilesDHA()
        {
            // eRxValidateTransactionSoapClient objClient = new eRxValidateTransactionSoapClient("eRxValidateTransactionSoap");
            Mediplus.DHAService.ValidateTransactionsSoapClient objClient = new Mediplus.DHAService.ValidateTransactionsSoapClient("ValidateTransactionSoap");
            // ClaimsAndAuthorizationsArchiveSoapClient objClient = new ClaimsAndAuthorizationsArchiveSoapClient("ClaimsAndAuthorizationsArchiveSoap");
            ///    Mediplus.DHAService.ValidateTransactionsSoapChannel
            string errorMessage, xmlTransactions = "";
            byte[] prescription, errorReport;
            //string responseMsg = "";
            //int result = soapclient.CheckForNewPriorAuthorizationTransactions(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), out errorMessage);
            //responseMsg += "result = " + result + ", Message: " + errorMessage;
            string foundTransactions = null;
            int transactionStatus = -1;
            for (int i = 0; i <= chkDownloadFileType.Items.Count - 1; i++)
            {

                if (chkDownloadFileType.Items[i].Selected == true)
                {
                    transactionStatus = Convert.ToInt32(chkDownloadFileType.Items[i].Value);
                }
            }
            LogFileWriting(System.DateTime.Now.ToString() + "Search Start ");
            //objClient.SearchTransactions(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), Convert.ToInt32(radDirection.SelectedValue), "", txtePartmenr.Text.Trim(), "", 0, Convert.ToInt32(radTransStatus.SelectedValue), txtFrmDt.Text.Trim() + " 00:00:00", txtToDt.Text.Trim() + " 23:59:59", -1, -1, out foundTransactions, out errorMessage);
            objClient.SearchTransactions(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), Convert.ToInt32(radDirection.SelectedValue), "", txtePartmenr.Text.Trim(), transactionStatus, Convert.ToInt32(radTransStatus.SelectedValue), txtFileName.Text, txtFrmDt.Text.Trim() + " 00:00:00", txtToDt.Text.Trim() + " 23:59:59", -1, -1, out foundTransactions, out errorMessage);

            LogFileWriting(System.DateTime.Now.ToString() + "Search errorMessage :" + errorMessage);
            if (string.IsNullOrEmpty(foundTransactions) == false && !foundTransactions.Equals(""))
            {
                DataSet DS = new DataSet();


                //string strPath = Convert.ToString(ViewState["LOOKUP_FILEPATH"]);
                //StreamWriter oWrite;
                //oWrite = File.CreateText(strPath + "GetTransactionsList.xml");
                //oWrite.WriteLine(Convert.ToString(foundTransactions.Trim()));
                //oWrite.Close();


                Boolean IsFiles = false, IsFile = false;

                DS = ConvertXMLToDataSet(foundTransactions);

                //  LogFileWriting("Read XML File" + srtFileName);

                foreach (DataTable table in DS.Tables)
                {


                    if (table.TableName == "File")
                    {
                        IsFile = true;
                    }



                }
                if (IsFile == true)
                {

                    gvTrans.Visible = true;
                    gvTrans.DataSource = DS;
                    gvTrans.DataBind();

                    lblDownloadCount.Text = Convert.ToString(DS.Tables[0].Rows.Count);
                }
                else
                {
                    lblDownloadCount.Text = "0";
                    gvTrans.Visible = false;
                    gvTrans.DataBind();
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Claim Dtls.','No Data.')", true);


                }


                LogFileWriting(System.DateTime.Now.ToString() + "Search Completed ");
            }
            else
            {
                lblDownloadCount.Text = "0";
                gvTrans.Visible = false;
                gvTrans.DataBind();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Claim Dtls.','No Data.')", true);
            }
        }


        public DataSet ConvertXMLToDataSet(string xmlData)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmlData);
                // Load the XmlTextReader from the stream
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);
                return xmlDS;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }// Use this function to get XML string from a dataset

        string DownloadAuthorizaton(string fileId)
        {
            // LogFileWriting("DownloadAuthorizaton() Start");
            WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");
            string errorMessage, xmlTransactions = "";
            byte[] fileContent, errorReport;
            string responseMsg = "", fileName = "";
            string foundTransactions;
            int result;
            string strDownloadPath = Convert.ToString(ViewState["DOWNLOAD_FILEPATH"]);

            if (!fileId.Equals(""))
            {

                result = soapclient.DownloadTransactionFile(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileId, out fileName, out fileContent, out errorMessage);
                //if (File.Exists(strDownloadPath + fileName) == true)
                //{
                //    goto LoopEnd;
                //}

                fileName = fileName.Replace(".XML.XML", ".XML");

                if (fileName.IndexOf("\\") >= 0)
                {
                    fileName = fileName.Substring(fileName.LastIndexOf("\\"), fileName.Length - fileName.LastIndexOf("\\"));
                    fileName = fileName.Replace("\\", "");
                }

                if (fileContent != null && fileContent.Length > 0)
                {

                    string strNewFileName = fileName;

                    if (File.Exists(strDownloadPath + fileName) == true)
                    {
                        string strNewName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                        //if (!Directory.Exists(strDownloadPath + @"\" + strFolderName))
                        //{
                        //    Directory.CreateDirectory(strDownloadPath + @"\" + strFolderName);
                        //}


                        using (FileStream fs = new FileStream(strDownloadPath + @"\" + strNewName + "_" + fileName, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                bw.Write(fileContent);

                            }
                        }

                    }
                    else
                    {
                        using (FileStream fs = new FileStream(strDownloadPath + @"\" + fileName, FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                bw.Write(fileContent);

                            }
                        }

                    }

                }
                responseMsg += (result + " DownloadTransactionFile " + errorMessage + ", fileName = " + fileName);
            }
            else
            {
                responseMsg = "fileId param s empty...";
            }
        LoopEnd: ;
            return fileName;
        }

        string DownloadAuthorizatonDHA(string fileId)
        {
            // LogFileWriting("DownloadAuthorizaton() Start");
            //  eRxValidateTransactionSoapClient objClient = new eRxValidateTransactionSoapClient("eRxValidateTransactionSoap");
            ValidateTransactionsSoapClient objClient = new ValidateTransactionsSoapClient("ValidateTransactionSoap");
            //  ClaimsAndAuthorizationsArchiveSoapClient objClient = new ClaimsAndAuthorizationsArchiveSoapClient("ClaimsAndAuthorizationsArchiveSoap");

            string errorMessage, xmlTransactions = "";
            byte[] fileContent, errorReport;
            string responseMsg = "", fileName = "";
            string foundTransactions;
            int result;
            string strDownloadPath = Convert.ToString(ViewState["DOWNLOAD_FILEPATH"]);

            if (!fileId.Equals(""))
            {

                result = objClient.DownloadTransactionFile(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileId, out fileName, out fileContent, out errorMessage);
                if (File.Exists(strDownloadPath + fileName) == true)
                {
                    goto LoopEnd;
                }

                fileName = fileName.Replace(".XML.XML", ".XML");

                if (fileName.IndexOf("\\") >= 0)
                {
                    fileName = fileName.Substring(fileName.LastIndexOf("\\"), fileName.Length - fileName.LastIndexOf("\\"));
                    fileName = fileName.Replace("\\", "");
                }

                if (fileContent != null && fileContent.Length > 0)
                {

                    using (FileStream fs = new FileStream(strDownloadPath + fileName, FileMode.Create))
                    {
                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {
                            bw.Write(fileContent);

                        }
                    }
                }
                responseMsg += (result + " DownloadTransactionFile " + errorMessage + ", fileName = " + fileName);
            }
            else
            {
                responseMsg = "fileId param s empty...";
            }
        LoopEnd: ;
            return fileName;
        }

        void TransactionDownloadedStatus(string fileId)
        {
            WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");
            string errorMessage, xmlTransactions = "";
            byte[] fileContent, errorReport;
            string responseMsg = "", fileName = "";
            string foundTransactions;
            int result;
            if (!fileId.Equals(""))
            {
                result = soapclient.SetTransactionDownloaded(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileId, out errorMessage);

            }
            else
            {
                responseMsg = "fileId param s empty...";
            }


        }

        void TransactionDownloadedStatusDHA(string fileId)
        {
            // eRxValidateTransactionSoapClient objClient = new eRxValidateTransactionSoapClient("eRxValidateTransactionSoap");
            ValidateTransactionsSoapClient objClient = new ValidateTransactionsSoapClient("ValidateTransactionSoap");
            //  ClaimsAndAuthorizationsArchiveSoapClient objClient = new ClaimsAndAuthorizationsArchiveSoapClient("ClaimsAndAuthorizationsArchiveSoap");

            string errorMessage, xmlTransactions = "";
            byte[] fileContent, errorReport;
            string responseMsg = "", fileName = "";
            string foundTransactions;
            int result;
            if (!fileId.Equals(""))
            {
                result = objClient.SetTransactionDownloaded(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileId, out errorMessage);

            }
            else
            {
                responseMsg = "fileId param s empty...";
            }


        }

        void UnzipAuthorization(string FileName, string directoryName)
        {
            using (ZipInputStream s = new ZipInputStream(File.OpenRead(FileName)))
            {

                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {

                    //  Console.WriteLine(theEntry.Name);

                    //string directoryName = Path.GetDirectoryName(theEntry.Name);
                    //  Console.WriteLine(directoryName);
                    string fileName = Path.GetFileName(theEntry.Name);

                    //   Console.WriteLine(fileName);

                    /* create directory
                    if (directoryName.Length > 0)
                    {
                        Directory.CreateDirectory(directoryName);
                    }*/

                    if (fileName != String.Empty)
                    {
                        using (FileStream streamWriter = File.Create(directoryName + theEntry.Name))
                        {

                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }


        void ReadDownLoadFileData(string FIleID, string FileName)
        {
            LogFileWriting("ReadDownLoadFileData() Start");
            decimal TotalPaymentAmount = 0;
            Int32 intActivityCount = 0, intApprovedCount = 0;

            DataSet DS_DownloadFile = new DataSet();
            string strPath = Convert.ToString(ViewState["DOWNLOAD_FILEPATH"]);
            string srtFileName = strPath + FileName;

            //  LogFileWriting("Raw File Name"+ FileName);

            if (srtFileName.Contains(".xml") == false && srtFileName.Contains(".XML") == false)
            {
                srtFileName += ".xml";
            }

            if (File.Exists(srtFileName) == false)
            {
                // LogFileWriting("File not available"+srtFileName);
                goto FunEnd;
            }

            LogFileWriting(srtFileName);

            Boolean IsAuthorization = false, IsActivity = false;

            DS_DownloadFile.ReadXml(srtFileName);

            //  LogFileWriting("Read XML File" + srtFileName);

            foreach (DataTable table in DS_DownloadFile.Tables)
            {
                if (table.TableName == "Authorization")
                {
                    IsAuthorization = true;
                }

                if (table.TableName == "Activity")
                {
                    IsActivity = true;
                }



            }

            if (IsAuthorization == true)
            {


                if (DS_DownloadFile.Tables["Authorization"].Rows.Count > 0)
                {
                    LogFileWriting("DS_DownloadFile.Tables[Authorization].Rows.Count " + DS_DownloadFile.Tables["Authorization"].Rows.Count.ToString());
                    string strResult = "", str_eAuthId = "", strStartDate = "", strEndDate = "", strComments = "", strIDPayer = "", strApprovalStatus = "";


                    if (DS_DownloadFile.Tables[1].Columns.Contains("Result") == true)
                        strResult = Convert.ToString(DS_DownloadFile.Tables["Authorization"].Rows[0]["Result"]);

                    if (DS_DownloadFile.Tables[1].Columns.Contains("ID") == true)
                        str_eAuthId = Convert.ToString(DS_DownloadFile.Tables["Authorization"].Rows[0]["ID"]);

                    if (DS_DownloadFile.Tables[1].Columns.Contains("Start") == true)
                        strStartDate = Convert.ToString(DS_DownloadFile.Tables["Authorization"].Rows[0]["Start"]);

                    if (DS_DownloadFile.Tables[1].Columns.Contains("End") == true)
                        strEndDate = Convert.ToString(DS_DownloadFile.Tables["Authorization"].Rows[0]["End"]);

                    if (DS_DownloadFile.Tables[1].Columns.Contains("IDPayer") == true)
                        strIDPayer = Convert.ToString(DS_DownloadFile.Tables["Authorization"].Rows[0]["IDPayer"]);


                    if (DS_DownloadFile.Tables[1].Columns.Contains("Comments") == true)
                        strComments = Convert.ToString(DS_DownloadFile.Tables["Authorization"].Rows[0]["Comments"]);

                    eAuthorizationBAL objeAuth;

                    objeAuth = new eAuthorizationBAL();

                    DataSet DS_eAuth = new DataSet();
                    string Criteria = " 1=1 ";
                    Criteria += " AND HAR_TRANS_ID='" + str_eAuthId + "'";
                    DS_eAuth = objeAuth.AuthorizationRequestGet(Criteria);



                    if (DS_DownloadFile.Tables[0].Rows.Count > 0)
                    {

                        if (IsActivity == true)
                        {
                            intActivityCount = DS_DownloadFile.Tables["Activity"].Rows.Count;


                            LogFileWriting(" DS_DownloadFile.Tables[Activity].Rows.Count " + DS_DownloadFile.Tables["Activity"].Rows.Count.ToString());

                            for (int i = 0; i < DS_DownloadFile.Tables["Activity"].Rows.Count; i++)
                            {
                                string strServCode = "", strApprovedQty = "", strList = "", strDenialCode = "", strPatientShare = "";
                                decimal decPaymentAmount = 0;

                                if (DS_DownloadFile.Tables["Activity"].Columns.Contains("Code") == true)
                                    strServCode = Convert.ToString(DS_DownloadFile.Tables["Activity"].Rows[i]["Code"]);

                                if (DS_DownloadFile.Tables["Activity"].Columns.Contains("Quantity") == true)
                                    strApprovedQty = Convert.ToString(DS_DownloadFile.Tables["Activity"].Rows[i]["Quantity"]);

                                if (DS_DownloadFile.Tables["Activity"].Columns.Contains("List") == true)
                                    strList = Convert.ToString(DS_DownloadFile.Tables["Activity"].Rows[i]["List"]);

                                //if (DS_DownloadFile.Tables["Activity"].Columns.Contains("Net") == true)
                                //    decPaymentAmount = Convert.ToDecimal(DS_DownloadFile.Tables["Activity"].Rows[i]["Net"]);

                                if (DS_DownloadFile.Tables["Activity"].Columns.Contains("PatientShare") == true)
                                    strPatientShare = Convert.ToString(DS_DownloadFile.Tables["Activity"].Rows[i]["PatientShare"]);

                                if (DS_DownloadFile.Tables["Activity"].Columns.Contains("PaymentAmount") == true)
                                    decPaymentAmount = Convert.ToDecimal(DS_DownloadFile.Tables["Activity"].Rows[i]["PaymentAmount"]);


                                if (DS_DownloadFile.Tables["Activity"].Columns.Contains("DenialCode") == true)
                                    strDenialCode = Convert.ToString(DS_DownloadFile.Tables["Activity"].Rows[i]["DenialCode"]);

                                if (strDenialCode == "" && decPaymentAmount != 0)
                                {
                                    intApprovedCount = intApprovedCount + 1;
                                }

                                ////objeAuth = new eAuthorizationBAL();
                                ////objeAuth.HAR_TRANS_ID = str_eAuthId;
                                ////objeAuth.SERV_CODE = strServCode;
                                ////objeAuth.HAA_APPROVED_QTY = strApprovedQty;
                                ////objeAuth.HAA_LIST = strList;
                                ////objeAuth.HAA_PAYMENT_AMOUNT = Convert.ToString(decPaymentAmount);
                                ////objeAuth.HAA_DENIAL_CODE = strDenialCode;
                                ////objeAuth.HAA_PATIENT_SHARE = strPatientShare;
                                ////objeAuth.UpdateAuthActivitesResponse();



                                ////TotalPaymentAmount = TotalPaymentAmount + decPaymentAmount;
                                /*

                                objeAuth = new eAuthorizationBAL();
                                objeAuth.HAR_EMR_ID = Convert.ToString(DS_eAuth.Tables[0].Rows[0]["HAR_EMR_ID"]);
                                objeAuth.HAA_APPROVED_QTY = strApprovedQty;
                                objeAuth.HAR_MEMBER_ID = Convert.ToString(DS_eAuth.Tables[0].Rows[0]["HAR_MEMBER_ID"]);
                                objeAuth.HAR_PAYER_ID = Convert.ToString(DS_eAuth.Tables[0].Rows[0]["HAR_PAYER_ID"]);
                                objeAuth.HAR_TRANS_DATE = Convert.ToString(DS_eAuth.Tables[0].Rows[0]["HAR_TRANS_DATE"]);

                                if (strResult == "Yes")     //strList == "Yes"
                                    objeAuth.HAR_STATUS = "Approved";

                                if (strResult == "No")
                                    objeAuth.HAR_STATUS = "Rejected";

                                if (strResult == "")
                                    objeAuth.HAR_STATUS = "Pending";


                                objeAuth.HAA_PAYMENT_AMOUNT = Convert.ToString(decPaymentAmount);
                                objeAuth.HAA_PATIENT_SHARE = strPatientShare;
                                objeAuth.SERV_CODE = strServCode;
                                objeAuth.PBM_DRUGS_Add();

                                */
                            }
                        }
                    }

                    ////strApprovalStatus = GlobalValues.ApprovalStatus_Pending;

                    ////if (intApprovedCount == 0)
                    ////{
                    ////    strApprovalStatus = GlobalValues.ApprovalStatus_Rejected;

                    ////}
                    ////else if (intApprovedCount < intActivityCount)
                    ////{
                    ////    strApprovalStatus = GlobalValues.ApprovalStatus_PartialyApproved;

                    ////}
                    ////else if (intApprovedCount == intActivityCount)
                    ////{
                    ////    strApprovalStatus = GlobalValues.ApprovalStatus_Approved;
                    ////}

                    ////LogFileWriting(" Approval Count " + intApprovedCount);


                    ////objeAuth = new eAuthorizationBAL();
                    ////objeAuth.HAR_STATUS = Convert.ToString(GlobalValues.Authorization_DownloadStatus);
                    ////objeAuth.HAR_START_DATE = strStartDate;
                    ////objeAuth.HAR_END_DATE = strEndDate;
                    ////objeAuth.HAR_FILE_ID = FIleID;
                    ////objeAuth.HAR_FILE_NAME = FileName;
                    ////objeAuth.HAR_FILE_COMMENTS = strComments;
                    ////objeAuth.HAR_TRANS_ID = str_eAuthId;
                    ////objeAuth.HAR_ID_PAYER = strIDPayer;
                    ////objeAuth.HAR_APPROVED_AMOUNT = Convert.ToString(TotalPaymentAmount);
                    ////objeAuth.HAR_RESULT = strResult;
                    ////objeAuth.HAR_APPROVAL_STATUS = strApprovalStatus;
                    ////objeAuth.UpdateAuthorizationDownload();

                }
            }

        FunEnd: ;
            LogFileWriting("ReadDownLoadFileData() End");
        }

        void ReadLookupAndDownloadFiles()
        {
            DataSet DS_AuthLookup = new DataSet();

            string strPath = Convert.ToString(ViewState["LOOKUP_FILEPATH"]);
            string srtFileName = strPath + "GetNewRemittanceTransactions.xml";

            if (File.Exists(srtFileName) == false)
            {
                goto FunEnd;
            }


            DS_AuthLookup.ReadXml(srtFileName);


            if (DS_AuthLookup.Tables.Count == 0)
            {
                goto FunEnd;
            }

            if (DS_AuthLookup.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS_AuthLookup.Tables[0].Rows.Count; i++)
                {
                    string strDownloadPath = Convert.ToString(ViewState["DOWNLOAD_FILEPATH"]);
                    string strFileId = "", strFileName = "";

                    if (DS_AuthLookup.Tables[0].Columns.Contains("FileID") == true)
                        strFileId = Convert.ToString(DS_AuthLookup.Tables[0].Rows[i]["FileID"]);

                    if (DS_AuthLookup.Tables[0].Columns.Contains("FileName") == true)
                        strFileName = Convert.ToString(DS_AuthLookup.Tables[0].Rows[i]["FileName"]);  //"MF464_E001_2014020510391.XML.From.D001.To.MF464.File.1.xml.zip"; //  //"C001_2_39f68bdd-acce-4e84-a9f1-4ec5d8cb36ba.xml";


                    //////checking for HAR_FILE_ID is available this id come from GetNewPriorAuthorizationTransactions.xml
                    ////string Criteria = " 1=1 ";
                    ////Criteria += " AND HAR_FILE_ID='" + strFileId + "'";

                    ////eAuthorizationBAL objeAuth = new eAuthorizationBAL();
                    ////DataSet DSeAuth = new DataSet();
                    ////DSeAuth = objeAuth.AuthorizationRequestGet(Criteria);
                    ////if (DSeAuth.Tables[0].Rows.Count > 0)
                    ////{
                    ////    goto LoopEnd;

                    ////}

                    string strProviderID = Convert.ToString(ViewState["PROVIDER_ID"]);

                    if (strFileName.Trim().LastIndexOf(strProviderID) < 0)
                    {
                        goto LoopEnd;
                    }


                    strFileName = DownloadAuthorizaton(strFileId);// DownloadAuthorizaton("1a0440bb-071a-4094-8b55-b320de66bb9a");//

                    if (strFileName != "")
                    {
                        if (Convert.ToString(ViewState["TRANS_DOWNLOADED"]).ToLower() == "true")
                        {
                            if (drpPostOffice.SelectedValue == "Shafafiya")
                            {
                                //FOR UPDATE THE IsDownloaded FILE STATSU AS TRUE
                                TransactionDownloadedStatus(strFileId);
                            }
                            else
                            {
                                TransactionDownloadedStatusDHA(strFileId);
                            }
                        }


                        if (strFileName.Substring(strFileName.Length - 4, 4) == ".zip")
                        {
                            UnzipAuthorization(strDownloadPath + strFileName, strDownloadPath);

                            string strXMLFileName = "";
                            strXMLFileName = strFileName.Substring(0, strFileName.Length - 4);

                            ReadDownLoadFileData(strFileId, strXMLFileName);
                        }
                        else
                        {
                            ReadDownLoadFileData(strFileId, strFileName);
                        }
                    }

                LoopEnd: ;

                }
            }
        FunEnd: ;

        }


        void SearchClaims()
        {
            DataSet DS = new DataSet();
            CommonBAL objphyCom = new CommonBAL();

            string Criteria = " 1=1 AND HAC_STATUS='A'  AND  HAC_PROVIDER_ID='" + drpProvider.SelectedValue + "'";
            DS = objphyCom.AuthorizationCredentialGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (DR.IsNull("HAC_PROVIDER_ID") == false)
                    {
                        ViewState["PROVIDER_ID"] = Convert.ToString(DR["HAC_PROVIDER_ID"]);

                    }

                    if (DR.IsNull("HAC_ECLAIM_FILEPATH") == false)
                    {
                        ViewState["ECLAIM_FILEPATH"] = Convert.ToString(DR["HAC_ECLAIM_FILEPATH"]);
                        if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                        {
                            if (!Directory.Exists(Convert.ToString(ViewState["ECLAIM_FILEPATH"])))
                            {
                                Directory.CreateDirectory(Convert.ToString(ViewState["ECLAIM_FILEPATH"]));
                            }
                        }
                    }

                    if (DR.IsNull("HAC_DOWNLOAD_FILEPATH") == false)
                    {
                        ViewState["DOWNLOAD_FILEPATH"] = Convert.ToString(DR["HAC_DOWNLOAD_FILEPATH"]);
                        if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                        {
                            if (!Directory.Exists(Convert.ToString(ViewState["DOWNLOAD_FILEPATH"])))
                            {
                                Directory.CreateDirectory(Convert.ToString(ViewState["DOWNLOAD_FILEPATH"]));
                            }
                        }
                    }

                    if (DR.IsNull("HAC_LOOKUP_FILEPATH") == false)
                    {
                        ViewState["LOOKUP_FILEPATH"] = Convert.ToString(DR["HAC_LOOKUP_FILEPATH"]);
                        if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                        {
                            if (!Directory.Exists(Convert.ToString(ViewState["LOOKUP_FILEPATH"])))
                            {
                                Directory.CreateDirectory(Convert.ToString(ViewState["LOOKUP_FILEPATH"]));
                            }
                        }
                    }

                    if (DR.IsNull("HAC_ECLAIM_ERROR_FILEPATH") == false)
                    {
                        ViewState["ECLAIM_ERROR_FILEPATH"] = Convert.ToString(DR["HAC_ECLAIM_ERROR_FILEPATH"]);
                        if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                        {
                            if (!Directory.Exists(Convert.ToString(ViewState["ECLAIM_ERROR_FILEPATH"])))
                            {
                                Directory.CreateDirectory(Convert.ToString(ViewState["ECLAIM_ERROR_FILEPATH"]));
                            }
                        }
                    }





                    if (DR.IsNull("HAC_LOGIN_ID") == false)
                    {
                        ViewState["LOGIN_ID"] = Convert.ToString(DR["HAC_LOGIN_ID"]);
                    }
                    if (DR.IsNull("HAC_LOGIN_PASSWORD") == false)
                    {
                        ViewState["LOGIN_PASSWORD"] = Convert.ToString(DR["HAC_LOGIN_PASSWORD"]);
                    }

                    if (DR.IsNull("HAC_SET_TRANS_DOWNLOADED") == false)
                    {
                        ViewState["TRANS_DOWNLOADED"] = Convert.ToString(DR["HAC_SET_TRANS_DOWNLOADED"]);
                    }

                    if (DR.IsNull("HAC_ECLAIM_ERROR_FILEPATH") == false)
                    {
                        ViewState["ECLAIM_ERROR_FILEPATH"] = Convert.ToString(DR["HAC_ECLAIM_ERROR_FILEPATH"]);
                    }




                    LogFileWriting("LookupAuthorization() END");

                    // ReadLookupAndDownloadFiles();

                    // LogFileWriting("ReadLookupAndDownloadFiles() END");

                }
            }
        }


        void LoadProviderDtls()
        {
            DataSet DS = new DataSet();
            CommonBAL objphyCom = new CommonBAL();

            string Criteria = " 1=1 AND  HAC_PROVIDER_ID='" + drpSrcProvider.SelectedValue + "'";
            DS = objphyCom.AuthorizationCredentialGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (DR.IsNull("HAC_PROVIDER_ID") == false)
                    {
                        txtSrcProviderID.Text = Convert.ToString(DR["HAC_PROVIDER_ID"]);

                    }

                    if (DR.IsNull("HAC_PROVIDER_NAME") == false)
                    {
                        txtSrcProviderName.Text = Convert.ToString(DR["HAC_PROVIDER_NAME"]);

                    }

                    if (DR.IsNull("HAC_ECLAIM_FILEPATH") == false)
                    {
                        txtSrcEClaimFilePath.Text = Convert.ToString(DR["HAC_ECLAIM_FILEPATH"]);

                    }

                    if (DR.IsNull("HAC_DOWNLOAD_FILEPATH") == false)
                    {
                        txtSrcDownloadFilePath.Text = Convert.ToString(DR["HAC_DOWNLOAD_FILEPATH"]);

                    }

                    if (DR.IsNull("HAC_LOOKUP_FILEPATH") == false)
                    {
                        txtSrcLookupFilePath.Text = Convert.ToString(DR["HAC_LOOKUP_FILEPATH"]);

                    }

                    if (DR.IsNull("HAC_ECLAIM_ERROR_FILEPATH") == false)
                    {
                        txtSrcEClaimErrorFilePath.Text = Convert.ToString(DR["HAC_ECLAIM_ERROR_FILEPATH"]);

                    }


                    if (DR.IsNull("HAC_LOGIN_ID") == false)
                    {
                        txtSrcLoginID.Text = Convert.ToString(DR["HAC_LOGIN_ID"]);
                    }
                    if (DR.IsNull("HAC_LOGIN_PASSWORD") == false)
                    {
                        txtSrcLoginPwd.Text = Convert.ToString(DR["HAC_LOGIN_PASSWORD"]);
                    }

                    if (DR.IsNull("HAC_DISPOSITION_FLAG") == false)
                    {
                        drpScrDispositionFlag.SelectedValue = Convert.ToString(DR["HAC_DISPOSITION_FLAG"]);
                    }

                    if (DR.IsNull("HAC_SET_TRANS_DOWNLOADED") == false)
                    {
                        drpSrcSetTransDownload.SelectedValue = Convert.ToString(DR["HAC_SET_TRANS_DOWNLOADED"]).ToUpper();
                    }



                    if (DR.IsNull("HAC_STATUS") == false)
                    {

                        if (Convert.ToString(DR["HAC_STATUS"]).Trim() == "A")
                        {
                            chkSrcStatus.Checked = true;
                        }
                        else
                        {
                            chkSrcStatus.Checked = false;
                        }
                    }




                }
            }
        }

        void ClearProviderDtls()
        {

            txtSrcProviderID.Text = "";


            txtSrcProviderName.Text = "";


            txtSrcEClaimFilePath.Text = "";

            txtSrcDownloadFilePath.Text = "";

            txtSrcLookupFilePath.Text = "";

            txtSrcEClaimErrorFilePath.Text = "";

            txtSrcLoginID.Text = "";

            txtSrcLoginPwd.Text = "";

            drpScrDispositionFlag.SelectedIndex = 0;

            drpSrcSetTransDownload.SelectedIndex = 0;

            chkSrcStatus.Checked = false;


        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            try
            {
                if (!IsPostBack)
                {
                    //if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                    //{
                    //    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    //    {
                    //        SetPermission();
                    //    }
                    //}
                    AuditLogAdd("OPEN", "Open MediTransfer Page");

                    if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
                    {
                        if (drpPostOffice.Items.Count > 1)
                        {
                            drpPostOffice.SelectedIndex = 1;
                        }
                    }
                    else
                    {
                        drpPostOffice.SelectedIndex = 0;
                    }

                    BuildUploadTrans();

                    txtFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                    BindProvider();
                    BindSrcProvider();
                    BindDirection();
                    BindTransStatus();
                    BindDownloadFileType();

                    SearchClaims();


                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpProvider_SelectedIndexChanged(object sender, EventArgs e)
        {


            SearchClaims();

        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpProvider.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Provider Name','Select Provider Name')", true);

                    goto FunEnd;
                }

                SearchClaims();
                LogFileWriting(System.DateTime.Now.ToString() + "Search Start ");
                if (drpPostOffice.SelectedValue == "Shafafiya")
                {
                    SearchDownloadFiles();
                }
                else
                {
                    SearchDownloadFilesDHA();
                }
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDownlaodTrans_Click(object sender, EventArgs e)
        {
            if (drpProvider.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Provider Name','Select Provider Name')", true);

                goto FunEnd;
            }

            for (int intCount = 0; intCount < gvTrans.Rows.Count; intCount++)
            {


                CheckBox chkDownload = (CheckBox)gvTrans.Rows[intCount].FindControl("chkDownload");


                Label LablblFIleID = (Label)gvTrans.Rows[intCount].FindControl("LablblFIleID");
                Label lblFileName = (Label)gvTrans.Rows[intCount].FindControl("lblFileName");

                if (chkDownload.Checked == true)
                {
                    if (drpPostOffice.SelectedValue == "Shafafiya")
                    {
                        DownloadAuthorizaton(LablblFIleID.Text);
                    }
                    else
                    {
                        DownloadAuthorizatonDHA(LablblFIleID.Text);
                    }
                }


            }

            AuditLogAdd("DOWNLOAD", "Download files from MediTransfer");

            lblStatus.Text = "Download Completed";
        FunEnd: ;
        }


        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            if (drpProvider.SelectedIndex == 0)
            {
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Provider Name','Select Provider Name')", true);

                lblStatus.Text = "Select Provider Name";
                goto FunEnd;
            }

            if (FileUpload1.PostedFiles == null || FileUpload1.FileName == "")
            {
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Upload Files','Select Upload Files')", true);
                lblStatus.Text = "Select Upload Files";
                goto FunEnd;
            }

            string strPath = Convert.ToString(ViewState["ECLAIM_FILEPATH"]);

            string strName = "Temp_" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


            strPath = strPath + strName + @"\";



            if (!Directory.Exists(strPath))
            {
                Directory.CreateDirectory(strPath);
            }


            foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
            {
                string fileName = Path.GetFileName(postedFile.FileName);
                postedFile.SaveAs(@strPath + fileName);

                ViewState["UploadedFilePath"] = strPath + fileName;
                //  ViewState["FileName"] = fileName;
            }

            BuildUploadTrans();
            AddUploadTrans(@strPath);
            BindUploadTrans();



        FunEnd: ;
        }

        protected void btnUploadTrans_Click(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();




            //-----------------------------------------------------------------------
            if (drpProvider.SelectedIndex == 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Provider Name','Select Provider Name')", true);

                goto FunEnd;
            }

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["UploadTrans"];

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {


                Label lblFileLocation = (Label)gvUploadTrans.Rows[i].FindControl("lblFileLocation");
                Label lblFileName = (Label)gvUploadTrans.Rows[i].FindControl("lblFileName");
                Label lblDispositionFlag = (Label)gvUploadTrans.Rows[i].FindControl("lblDispositionFlag");

                LinkButton lnkErrorReport = (LinkButton)gvUploadTrans.Rows[i].FindControl("lnkErrorReport");




                XmlDocument doc = new XmlDocument();
                doc.Load(lblFileLocation.Text.Trim());

                StringBuilder sb = new StringBuilder();
                //foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                //{
                //    sb.Append(char.ToUpper(node.Name[0]));
                //    sb.Append(node.Name.Substring(1));
                //    sb.Append(' ');
                //    sb.AppendLine(node.InnerText);
                //}

                sb.Append(doc.InnerXml);

                string DamanPrescriptionRequest = Convert.ToString(sb.Replace("&", " "));

                string errorMessage;
                byte[] fileContent, errorReport;
                if (drpPostOffice.SelectedValue == "Shafafiya")
                {
                    WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");

                    fileContent = System.Text.Encoding.UTF8.GetBytes(DamanPrescriptionRequest);

                    soapclient.UploadTransaction(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileContent, lblFileName.Text.Trim(), out errorMessage, out errorReport);

                }
                else
                {
                    ValidateTransactionsSoapClient objClient = new ValidateTransactionsSoapClient("ValidateTransactionSoap");

                    fileContent = System.Text.Encoding.UTF8.GetBytes(DamanPrescriptionRequest);

                    objClient.UploadTransaction(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileContent, lblFileName.Text.Trim(), out errorMessage, out errorReport);

                }


                if (!errorMessage.Equals("") && errorReport != null && errorReport.Length > 0)
                {

                    string strErrorPath = Convert.ToString(ViewState["ECLAIM_ERROR_FILEPATH"]);
                    string strErrorFileName = "";
                    if (drpPostOffice.SelectedValue == "Shafafiya")
                    {
                        strErrorFileName = strErrorPath + "Error_" + lblFileName.Text.Trim() + ".zip";
                    }
                    else
                    {
                        strErrorFileName = strErrorPath + "Error_" + lblFileName.Text.Trim().Replace(".XML", ".csv").Replace(".xml", ".csv");
                    }

                    ViewState["ErrorFileFullPath"] = strErrorFileName;
                    using (FileStream fs = new FileStream(strErrorFileName, FileMode.Create))
                    {
                        fs.Write(errorReport, 0, errorReport.Length);
                    }

                    dt.Rows[i]["IsUploaded"] = "Failed";
                    dt.Rows[i]["ErrorReport"] = "";
                    lnkErrorReport.Visible = true;
                    gvUploadTrans.Rows[i].Style.Add("Color", "red");

                }
                LogFileWriting(System.DateTime.Now.ToString() + " Upload errorMessage :" + errorMessage);
                if (errorMessage.Contains("successfully") == true || errorMessage.Contains("SUCCESSFULLY") == true || errorMessage.Contains("successful") == true)
                {

                    dt.Rows[i]["IsUploaded"] = "Sucess";
                    dt.Rows[i]["ErrorReport"] = "";

                    if (Convert.ToString(Session["MEDITRANS_FILE_STORE_IN_DB"]) == "1" && lblDispositionFlag.Text == "PRODUCTION")// 
                    {
                        DataSet DS = new DataSet();

                        ////string Criteria = "  ActualFileName='" + lblFileName.Text + "'";
                        ////DS = objCom.fnGetFieldValue("*", "HMS_ECLAIM_XML_DATA", Criteria, "");
                        ////if (DS.Tables[0].Rows.Count > 0)
                        ////{
                        ////    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('File Not Uploaded','Some files already Uplaoded, Pls Check error file')", true);
                        ////    LogFileWriting("Following file already Uploaded :" + lblFileName.Text);
                        ////    goto LoopEnd;
                        ////}

                        try
                        {


                            LogFileWriting(System.DateTime.Now.ToString() + " SaveDataFromXMLFile Start, File Name is " + lblFileName.Text);
                            string strXMLFileName = "";
                            strXMLFileName = SaveDataFromXMLFile(lblFileLocation.Text, lblFileName.Text);

                            LogFileWriting(System.DateTime.Now.ToString() + " SaveDataFromXMLFile End");

                            LogFileWriting(System.DateTime.Now.ToString() + " EclaimXMLDataMasterAddFromFiles Start " + lblFileName.Text + " XMLFileName " + strXMLFileName);

                            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                            con.Open();
                            SqlCommand cmd = new SqlCommand();
                            SqlDataAdapter adpt = new SqlDataAdapter();
                            cmd.Connection = con;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "HMS_SP_EclaimXMLDataMasterAddFromFiles";
                            //  cmd.Parameters.Add(new SqlParameter("@ActualFileName", SqlDbType.VarChar)).Value = lblFileName.Text;
                            cmd.Parameters.Add(new SqlParameter("@XMLFileName", SqlDbType.VarChar)).Value = strXMLFileName;
                            cmd.ExecuteNonQuery();
                            con.Close();
                            LogFileWriting(System.DateTime.Now.ToString() + " EclaimXMLDataMasterAddFromFiles End ");
                            LogFileWriting(" ");
                        }
                        catch (Exception ex)
                        {
                            LogFileWriting(System.DateTime.Now.ToString() + " " + ex.Message);
                        }
                    }

                }

                dt.AcceptChanges();

            LoopEnd: ;
            }

            BindUploadTrans();
            AuditLogAdd("UPLOAD", "Upload files Using MediTransfer");

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Upload','Upload Completed')", true);

        FunEnd: ;
        }

        protected void lnkErrorReport_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;



        }



        protected void drpScrProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearProviderDtls();
            LoadProviderDtls();
        }

        protected void btnSaveProviderDtls_Click(object sender, EventArgs e)
        {
            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidedivProviderPopup()", true);


            if (txtSrcEClaimFilePath.Text.Substring(txtSrcEClaimFilePath.Text.Length - 1, 1) != @"\")
            {
                txtSrcEClaimFilePath.Text = txtSrcEClaimFilePath.Text.Trim() + @"\";
            }


            if (txtSrcDownloadFilePath.Text.Substring(txtSrcDownloadFilePath.Text.Length - 1, 1) != @"\")
            {
                txtSrcDownloadFilePath.Text = txtSrcDownloadFilePath.Text.Trim() + @"\";
            }


            if (txtSrcEClaimErrorFilePath.Text.Substring(txtSrcEClaimErrorFilePath.Text.Length - 1, 1) != @"\")
            {
                txtSrcEClaimErrorFilePath.Text = txtSrcEClaimErrorFilePath.Text.Trim() + @"\";
            }

            if (txtSrcLookupFilePath.Text.Substring(txtSrcLookupFilePath.Text.Length - 1, 1) != @"\")
            {
                txtSrcLookupFilePath.Text = txtSrcLookupFilePath.Text.Trim() + @"\";
            }




            CommonBAL objCom = new CommonBAL();
            objCom.HAC_PROVIDER_ID = txtSrcProviderID.Text.Trim();
            objCom.HAC_PROVIDER_NAME = txtSrcProviderName.Text.Trim();
            objCom.HAC_LOGIN_ID = txtSrcLoginID.Text.Trim();
            objCom.HAC_LOGIN_PASSWORD = txtSrcLoginPwd.Text.Trim();
            objCom.HAC_UPLOAD_FILEPATH = "";
            objCom.HAC_DOWNLOAD_FILEPATH = txtSrcDownloadFilePath.Text.Trim();
            objCom.HAC_ERROR_FILEPATH = "";
            objCom.HAC_LOOKUP_FILEPATH = txtSrcLookupFilePath.Text.Trim();
            objCom.HAC_ECLAIM_FILEPATH = txtSrcEClaimFilePath.Text.Trim();
            objCom.HAC_ECLAIM_ERROR_FILEPATH = txtSrcEClaimErrorFilePath.Text.Trim();
            objCom.HAC_DISPOSITION_FLAG = drpScrDispositionFlag.SelectedValue;
            objCom.HAC_SET_TRANS_DOWNLOADED = drpSrcSetTransDownload.SelectedValue;

            if (chkSrcStatus.Checked == true)
            {
                objCom.HAC_STATUS = "A";
            }
            else
            {
                objCom.HAC_STATUS = "I";
            }
            objCom.AuthorizationcredentialAdd();

            BindSrcProvider();
            BindSrcProvider();
            ClearProviderDtls();
        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                Int64 intSelected = 0;
                CheckBox ChkBoxHeader = (CheckBox)gvTrans.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvTrans.Rows)
                {
                    CheckBox chkDownload = (CheckBox)row.FindControl("chkDownload");
                    if (ChkBoxHeader.Checked == true)
                    {
                        chkDownload.Checked = true;

                        intSelected = intSelected + 1;
                    }
                    else
                    {
                        chkDownload.Checked = false;
                    }
                }


                lblDownloadCount.Text = Convert.ToString(intSelected);
                //  CountClaimAmount();

            }
            catch (Exception ex)
            {

            }

        }

        protected void drpPostOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDirection();
            BindTransStatus();
            BindDownloadFileType();
        }

        protected void chkDownloadFileType_SelectedIndexChanged(object sender, EventArgs e)
        {

            //objrow["Name"] = "Claim.Submission";
            //objrow["COde"] = "2";


            //objrow["Name"] = "Remittance.Advice";
            //objrow["COde"] = "8";



            //objrow["Name"] = "Prior.Request";
            //objrow["COde"] = "16";



            //objrow["Name"] = "Prior.Authorization";
            //objrow["COde"] = "32";



            if (chkDownloadFileType.SelectedValue == "2" || chkDownloadFileType.SelectedValue == "16")
            {
                //Sent Only
                radDirection.SelectedValue = "1";
            }


            if (chkDownloadFileType.SelectedValue == "8" || chkDownloadFileType.SelectedValue == "32")
            {
                //Received Only
                radDirection.SelectedValue = "2";
            }

        }



    }
}