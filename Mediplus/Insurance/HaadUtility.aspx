﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="HaadUtility.aspx.cs" Inherits="Mediplus.Insurance.HaadUtility" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        .auto-style1
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            height: 25px;
            width: 242px;
        }

        .auto-style2
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            width: 242px;
        }
    </style>


    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowErrorMessage(vMessage1, vMessage2) {

            document.getElementById("divMessage").style.display = 'block';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }


        function ShowUploadErrorRptPopup() {
            document.getElementById("divUploadErrorRptOverlay").style.display = 'block';
            document.getElementById("divUploadErrorRptPopup").style.display = 'block';


        }

        function HideUploadErrorRptPopup() {
            document.getElementById("divUploadErrorRptOverlay").style.display = 'none';
            document.getElementById("divUploadErrorRptPopup").style.display = 'none';

        }



        function ShowdivProviderPopup() {
            document.getElementById("divProviderOverlay").style.display = 'block';
            document.getElementById("divProviderPopup").style.display = 'block';


        }

        function HidedivProviderPopup() {
            document.getElementById("divProviderOverlay").style.display = 'none';
            document.getElementById("divProviderPopup").style.display = 'none';

        }


        function fnTotalDownloads(chk) {


            var gv = document.getElementById("<%= gvTrans.ClientID %>");
            var inputList = gv.getElementsByTagName("input");
            var numChecked = 0;

            for (var i = 0; i < inputList.length; i++) {
                if (inputList[i].type == "checkbox" && inputList[i].checked) {
                    numChecked = numChecked + 1;
                }
            }

            document.getElementById("<%=lblDownloadCount.ClientID%>").innerHTML = numChecked - 1;
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <div style="top: 500px; left: 300px; position: fixed;">
        <div id="myMessage" style="display: none; border: groove; height: 70px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed;">
            Saved Successfully
        </div>
    </div>
    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">


            <span style="float: right;">

                <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

            </span>
            <br />
            <div style="width: 100%; height: 20px; background: #E3E3E3;">
                &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
            </div>
            &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>



            <br />

        </div>
    </div>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <table cellspacing="0" cellpadding="0" width="60%">
        <tr>
            <td class="lblCaption1" style="height: 25px;">Post Office
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpPostOffice" runat="server" Style="width: 152px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpPostOffice_SelectedIndexChanged">
                            <asp:ListItem Value="Shafafiya" Selected="True">Shafafiya</asp:ListItem>
                             <asp:ListItem Value="eClaimLink" >eClaimLink</asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 25px;">Provider Name
              
 
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpProvider" runat="server" Style="width: 200px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpProvider_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:ImageButton ID="imgGRNZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px; cursor: pointer;" ToolTip="Display Provider Dtls" OnClientClick="ShowdivProviderPopup()" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>


    <div style="padding-top: 0px; width: 80%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove;">
        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
            <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Upload" Width="100%">
                <ContentTemplate>
                    <table style="width: 100%">
                        <tr>
                            <td>

                                <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="True" />

                                <asp:Button ID="btnLoadFile" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 200px;" CssClass="button red small" accept="XML"
                                    OnClick="btnLoadFile_Click" Text="Load Files" />





                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnUploadTrans" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 200px;" CssClass="button red small" accept="XML"
                                            OnClick="btnUploadTrans_Click" Text="Upload Transactions" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>


                    <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvUploadTrans" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True">
                                    <HeaderStyle CssClass="GridHeader_Blue" HorizontalAlign="Center" />
                                    <FooterStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="From" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFileLocation" CssClass="GridRow" runat="server" Text='<%# Bind("FileLocation") %>' Visible="false"></asp:Label>

                                                <asp:Label ID="lblSenderID" CssClass="GridRow" runat="server" Text='<%# Bind("SenderID") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>

                                                <asp:Label ID="lblReceiverID" CssClass="GridRow" runat="server" Text='<%# Bind("ReceiverID") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Transaction File " HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="LablblFIleID" CssClass="GridRow" runat="server" Text='<%# Bind("FileID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblFileName" CssClass="GridRow" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transaction Date" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransactionDate" CssClass="GridRow" runat="server" Text='<%# Bind("TransactionDate") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Record Count" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRecordCount" CssClass="GridRow" runat="server" Style="text-align: Center;" Text='<%# Bind("RecordCount") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disposition Flag" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDispositionFlag" CssClass="GridRow" runat="server" Text='<%# Bind("DispositionFlag") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsUploaded" CssClass="GridRow" runat="server" Text='<%# Bind("IsUploaded") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Error Report" HeaderStyle-HorizontalAlign="center" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblErrorReport" CssClass="GridRow" runat="server" Text='<%# Bind("ErrorReport") %>' Visible="false"></asp:Label>
                                                <asp:LinkButton ID="lnkErrorReport" runat="server" Text="Show Reports" OnClick="lnkErrorReport_Click" OnClientClick="ShowUploadErrorRptPopup()"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                    </Columns>


                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </ContentTemplate>
            </asp:TabPanel>

            <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Download" Width="100%">
                <ContentTemplate>




                    <div class="lblCaption1" style="width: 100%; height: 150px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                        <table cellspacing="5" cellpadding="5" width="80%">
                            <tr>
                                <td class="lblCaption1" style="height: 25px; width: 100px;">ePartner
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtePartmenr" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Transaction Date between
                                </td>
                                <td class="lblCaption1">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtFrmDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                                Enabled="True" TargetControlID="txtFrmDt" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1">and
                                </td>
                                <td class="lblCaption1">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtToDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                                Enabled="True" TargetControlID="txtToDt" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1">File Name

                                </td>
                                <td colspan="5">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtFileName" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="radDirection" runat="server" CssClass="TextBoxStyle" Width="305px" RepeatDirection="Horizontal">
                                            </asp:RadioButtonList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td colspan="4">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="radTransStatus" runat="server" CssClass="TextBoxStyle" Width="305px" RepeatDirection="Horizontal">
                                               
                                            </asp:RadioButtonList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="chkDownloadFileType" runat="server" CssClass="TextBoxStyle" RepeatDirection="Horizontal" Width="700px" AutoPostBack="true" OnSelectedIndexChanged="chkDownloadFileType_SelectedIndexChanged">
                                            </asp:RadioButtonList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpPanelSearch" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button red small"
                                                OnClick="btnSearch_Click" Text="Search" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSearch">
                                        <ProgressTemplate>
                                            <div id="overlay">
                                                <div id="modalprogress">
                                                    <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                                                </div>
                                            </div>

                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>


                    </div>


                    <div style="padding-top: 0px; width: 100%; height: 350px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvTrans" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" GridLines="Both">
                                    <HeaderStyle CssClass="GridHeader_Blue" HorizontalAlign="Center" />
                                    <FooterStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Download" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>

                                                <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" Checked="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel44"
                                                    UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                    <ContentTemplate>
                                                        <asp:CheckBox ID="chkDownload" runat="server" CssClass="label" Checked="true" onClick="fnTotalDownloads(this)" />
                                                    </ContentTemplate>

                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FROM" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSenderID" CssClass="GridRow" runat="server" Text='<%# Bind("SenderID") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TO" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>

                                                <asp:Label ID="lblReceiverID" CssClass="GridRow" runat="server" Text='<%# Bind("ReceiverID") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="TRANSACTION FILE " HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="LablblFIleID" CssClass="GridRow" runat="server" Text='<%# Bind("FileID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblFileName" CssClass="GridRow" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TRANSACTION DATE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransactionDate" CssClass="GridRow" runat="server" Text='<%# Bind("TransactionDate") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="RECORD COUNT" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRecordCount" CssClass="GridRow" runat="server" Style="text-align: Center;" Text='<%# Bind("RecordCount") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsDownloaded" CssClass="GridRow" runat="server" Text='<%# Bind("IsDownloaded") %>' Visible="false"></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>



                                    </Columns>


                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                    <table style="width: 100%">
                        <tr>
                            <td style="float: left;">

                                <asp:UpdatePanel ID="UpPanelDownload" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnDownlaodTrans" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button red small"
                                            OnClick="btnDownlaodTrans_Click" Text="Download Transaction" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelDownload">
                                    <ProgressTemplate>
                                        <div id="overlay">
                                            <div id="modalprogress">
                                                <asp:Image ID="imgUDownload" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                                            </div>
                                        </div>

                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                    <ContentTemplate>
                                        Last operation progress (
                                <asp:Label ID="lblDownloadCount" runat="server" CssClass="lblCaption1" Text="0"></asp:Label>
                                        &nbsp;files)
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:TabPanel>

        </asp:TabContainer>
    </div>



    <div id="divUploadErrorRptOverlay" class="Overlay" style="display: none;">

        <div id="div1" style="height: 550px; width: 1100px; position: fixed; left: 150px; top: 250px;">
            <img src='<%= ResolveUrl("~/Images/TopMenuIcons/eAuthorization.png")%>' style="border: none; width: 40px; height: 40px;" />
            <span style="color: white; font-size: 17px;" class="label">Upload Validation Reports
            </span>
            <div id="divUploadErrorRptPopup" style="display: none; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; position: fixed; left: 150px; top: 300px;">
                <div id="divHeaderLine" style="height: 5px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px; position: absolute; top: 0px; left: 0px;">
                </div>
                <div style="width: 100%; text-align: right; float: right; position: absolute; top: -0px; right: 0px;">
                    <img src='<%= ResolveUrl("~/Images/Close.png")%>' style="height: 25px; width: 25px; cursor: pointer;" onclick="HideUploadErrorRptPopup()" />
                </div>
                <input type="button" class="PageSubHeaderBlue" style="width: 150px;" value="Validation Reports" disabled="disabled" />
                <img src='<%= ResolveUrl("~/Images/TabBlueCornor.png")%>' style="border: none; height: 20px; width: 37px;" />

                <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">File Name
                        <asp:DropDownList ID="drpUploadedFiles" runat="server" CssClass="TextBoxStyle" Width="300px"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                    <ContentTemplate>
                                    </ContentTemplate>

                                </asp:UpdatePanel>
                            </td>

                        </tr>

                    </table>
                </div>

            </div>
        </div>
    </div>



    <div id="divProviderOverlay" class="Overlay" style="display: none;">

        <div id="div3" style="height: 550px; width: 1100px; position: fixed; left: 150px; top: 250px;">
            <img src='<%= ResolveUrl("~/Images/TopMenuIcons/eAuthorization.png")%>' style="border: none; width: 30px; height: 30px;" />
            <span style="color: white; font-size: 17px;" class="label">Update Provider Details
            </span>
            <div id="divProviderPopup" style="display: none; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; position: fixed; left: 150px; top: 300px;">
                <div id="div5" style="height: 5px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px; position: absolute; top: 0px; left: 0px;">
                </div>
                <div style="width: 100%; text-align: right; float: right; position: absolute; top: -0px; right: 0px;">
                    <img src='<%= ResolveUrl("~/Images/Close.png")%>' style="height: 25px; width: 25px; cursor: pointer;" onclick="HidedivProviderPopup()" />
                </div>
                <input type="button" class="PageSubHeaderBlue" style="width: 150px;" value=" Provider Details" disabled="disabled" />
                <img src='<%= ResolveUrl("~/Images/TabBlueCornor.png")%>' style="border: none; height: 20px; width: 37px;" />

                <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="lblCaption1">Provider Name
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpSrcProvider" runat="server" Style="width: 200px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpScrProvider_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <br />


                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="lblCaption1">Provider ID
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcProviderID" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Provider Name
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcProviderName" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Status
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="chkSrcStatus" runat="server" Text="Active" Checked="true" CssClass="lblCaption1" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Login ID
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcLoginID" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td class="lblCaption1">Login Password
                            </td>
                            <td colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcLoginPwd" runat="server" CssClass="TextBoxStyle" Width="200px"  ></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Disposition Flag
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpScrDispositionFlag" runat="server" CssClass="TextBoxStyle" Width="200px">
                                            <asp:ListItem Text="Select" Value="" Selected="True"> </asp:ListItem>
                                            <asp:ListItem Text="PRODUCTION" Value="PRODUCTION"> </asp:ListItem>
                                            <asp:ListItem Text="TEST" Value="TEST"> </asp:ListItem>

                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td class="lblCaption1">Set Trans. Download
                            </td>
                            <td colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpSrcSetTransDownload" runat="server" CssClass="TextBoxStyle" Width="200px">
                                            <asp:ListItem Text="Select" Value="" Selected="True"> </asp:ListItem>
                                            <asp:ListItem Text="TRUE" Value="TRUE"> </asp:ListItem>
                                            <asp:ListItem Text="FALSE" Value="FALSE"> </asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Upload Eclaim File Path
                            </td>
                            <td colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcEClaimFilePath" runat="server" CssClass="TextBoxStyle" Width="80%"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Download File Path
                            </td>
                            <td colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcDownloadFilePath" runat="server" CssClass="TextBoxStyle" Width="80%"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">EClaim Error File Path
                            </td>
                            <td colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcEClaimErrorFilePath" runat="server" CssClass="TextBoxStyle" Width="80%"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Lookup File Path
                            </td>
                            <td colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcLookupFilePath" runat="server" CssClass="TextBoxStyle" Width="80%"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSaveProviderDtls" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small" accept="XML"
                                            OnClick="btnSaveProviderDtls_Click" Text="Save" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
