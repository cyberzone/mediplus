﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Insurance
{
    public partial class Remittance : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void RemittanceLogWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../RemittanceLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BuildRemittance()
        {
            DataTable dt = new DataTable();

            DataColumn SenderID = new DataColumn();
            SenderID.ColumnName = "SenderID";

            DataColumn ProviderId = new DataColumn();
            ProviderId.ColumnName = "ProviderId";

            DataColumn TransactionDate = new DataColumn();
            TransactionDate.ColumnName = "TransactionDate";

            DataColumn RecordCount = new DataColumn();
            RecordCount.ColumnName = "RecordCount";

            DataColumn DispositionFlag = new DataColumn();
            DispositionFlag.ColumnName = "DispositionFlag";

            DataColumn ClaimId = new DataColumn();
            ClaimId.ColumnName = "ClaimId";

            DataColumn IDPayer = new DataColumn();
            IDPayer.ColumnName = "IDPayer";

            DataColumn PaymentReference = new DataColumn();
            PaymentReference.ColumnName = "PaymentReference";

            DataColumn DateSettlement = new DataColumn();
            DateSettlement.ColumnName = "DateSettlement";

            DataColumn ActivityId = new DataColumn();
            ActivityId.ColumnName = "ActivityId";

            DataColumn Start1 = new DataColumn();
            Start1.ColumnName = "Start1";

            DataColumn Type2 = new DataColumn();
            Type2.ColumnName = "Type2";

            DataColumn Code2 = new DataColumn();
            Code2.ColumnName = "Code2";

            DataColumn Quantity = new DataColumn();
            Quantity.ColumnName = "Quantity";

            DataColumn Net1 = new DataColumn();
            Net1.ColumnName = "Net1";

            DataColumn List = new DataColumn();
            List.ColumnName = "List";

            DataColumn Clinician = new DataColumn();
            Clinician.ColumnName = "Clinician";

            DataColumn PriorAuthorizationID = new DataColumn();
            PriorAuthorizationID.ColumnName = "PriorAuthorizationID";

            DataColumn Gross = new DataColumn();
            Gross.ColumnName = "Gross";

            DataColumn PatientShare = new DataColumn();
            PatientShare.ColumnName = "PatientShare";

            DataColumn PaymentAmount = new DataColumn();
            PaymentAmount.ColumnName = "PaymentAmount";

            DataColumn DenialCode = new DataColumn();
            DenialCode.ColumnName = "DenialCode";


            dt.Columns.Add(SenderID);
            dt.Columns.Add(ProviderId);
            dt.Columns.Add(TransactionDate);
            dt.Columns.Add(RecordCount);
            dt.Columns.Add(DispositionFlag);
            dt.Columns.Add(ClaimId);
            dt.Columns.Add(IDPayer);
            dt.Columns.Add(PaymentReference);
            dt.Columns.Add(DateSettlement);
            dt.Columns.Add(ActivityId);
            dt.Columns.Add(Start1);
            dt.Columns.Add(Type2);
            dt.Columns.Add(Code2);
            dt.Columns.Add(Quantity);
            dt.Columns.Add(Net1);
            dt.Columns.Add(List);
            dt.Columns.Add(Clinician);
            dt.Columns.Add(PriorAuthorizationID);
            dt.Columns.Add(Gross);
            dt.Columns.Add(PatientShare);
            dt.Columns.Add(PaymentAmount);
            dt.Columns.Add(DenialCode);


            DataSet DS = new DataSet();
            DS.Tables.Add(dt);


            ViewState["TempRemittance"] = DS;


        }

        void BindData()
        {
            string Criteria = "  1=1   ";
            Criteria += " AND XMLFILENAME ='" + Convert.ToString(ViewState["FileName"]) + "'";

            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.TempRemittanceXMLDataGroupByGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceData.DataSource = DS;
                gvRemittanceData.DataBind();

            }


        }

        void BindRemittanceXMLData()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  ";//AND UploadStatus='UPLOADED'
            string strStartDate = txtFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) <= '" + strForToDate + "'";
            }

            if (txtFileName.Text.Trim() != "")
            {
                Criteria = "   ActualFileName like'" + txtFileName.Text.Trim() + "%'";
            }


            if (txtCompany.Text.Trim() != "")
            {
                // Criteria += " AND HCM_BILL_CODE='" + txtCompany.Text.Trim() + "'";
                Criteria += " AND SenderID  IN ( SELECT HCM_FIELD5 FROM HMS_COMPANY_MASTER WHERE  HCM_BILL_CODE='" + txtCompany.Text.Trim() + "' )";
            }




            if (txtSrcReferenceNo.Text.Trim() != "")
            {
                Criteria += " AND PaymentReference='" + txtSrcReferenceNo.Text.Trim() + "'";
            }





            dboperations objDp = new dboperations();
            DS = objDp.RemittanceXMLDataGroupByGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                ViewState["REMITTANCE_XML_DATA"] = true;
                gvRemittanceData.Visible = true;
                gvRemittanceData.DataSource = DS;
                gvRemittanceData.DataBind();
            }
            else
            {
                gvRemittanceData.Visible = false;
                gvRemittanceData.DataBind();
            }
        }

        void BindTempRemittanceClaims(string FileName)
        {

            string Criteria = "  1=1   ";

            // Criteria += " AND PaymentReference='" + RefNo + "'";

            Criteria += " AND XMLFILENAME='" + FileName + "'";

            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.TempRemittanceXMLDataClaimWise(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceClaims.Visible = true;
                gvRemittanceClaims.DataSource = DS;
                gvRemittanceClaims.DataBind();

            }
            else
            {
                gvRemittanceClaims.Visible = false;
                gvRemittanceClaims.DataBind();
            }

        }


        void BindRemittanceClaims(string FileName)
        {

            string Criteria = "  1=1   ";

            // Criteria += " AND PaymentReference='" + RefNo + "'";
            Criteria += " AND XMLFILENAME='" + FileName + "'";


            dboperations objDp = new dboperations();
            DataSet DS = new DataSet();
            DS = objDp.RemittanceXMLDataClaimWise(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRemittanceClaims.Visible = true;
                gvRemittanceClaims.DataSource = DS;
                gvRemittanceClaims.DataBind();

            }
            else
            {
                gvRemittanceClaims.Visible = false;
                gvRemittanceClaims.DataBind();
            }

        }

        void SaveDataFromXMLFile()
        {
            if (fileLogo.FileName == "")
            {
                lblStatus.Text = "Please select the XML  file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }

            DataSet DS = new DataSet();
            Boolean IsHeader = false, IsClaim = false, IsEncounter = false, IsActivity = false;

            string strPath = GlobalValues.Shafafiya_Remittance_FilePath;
            if (Path.GetExtension(fileLogo.FileName.ToLower()) == ".xml")
            {
                ViewState["ActualFileName"] = fileLogo.FileName.ToLower();
                string strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".XML";

                fileLogo.SaveAs(strPath + strFileName);

                ViewState["FileName"] = strFileName;
                DS.ReadXml(strPath + strFileName);
                ViewState["RawXMLData"] = DS;

            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;

            }



            foreach (DataTable table in DS.Tables)
            {
                if (table.TableName == "Header")
                {
                    IsHeader = true;
                }
                if (table.TableName == "Claim")
                {
                    IsClaim = true;
                }
                if (table.TableName == "Encounter")
                {
                    IsEncounter = true;
                }

                if (table.TableName == "Activity")
                {
                    IsActivity = true;
                }



            }


            if (IsHeader == false || IsClaim == false || IsEncounter == false || IsActivity == false)
            {
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }

            DataSet DSView = new DataSet();
            DSView = (DataSet)ViewState["TempRemittance"];


            for (int i = 0; i < DS.Tables["Claim"].Rows.Count; i++)
            {
                DataTable DTActivity = new DataTable();
                DataRow[] DRActivity;
                DRActivity = DS.Tables["Activity"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                DataTable DTEncounter = new DataTable();
                DataRow[] DREncounter;
                DREncounter = DS.Tables["Encounter"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());




                for (int j = 0; j < DRActivity.Length; j++)
                {
                    //DataTable DTObservation = new DataTable();
                    //DataRow[] DRObservation;



                    DataRow objrow;
                    objrow = DSView.Tables[0].NewRow();

                    if (DS.Tables["Header"].Columns.Contains("SenderID") == true)
                        objrow["SenderID"] = Convert.ToString(DS.Tables["Header"].Rows[0]["SenderID"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("ReceiverID") == true)
                        objrow["ProviderID"] = Convert.ToString(DS.Tables["Header"].Rows[0]["ReceiverID"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("TransactionDate") == true)
                        objrow["TransactionDate"] = Convert.ToString(DS.Tables["Header"].Rows[0]["TransactionDate"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("RecordCount") == true)
                        objrow["RecordCount"] = Convert.ToString(DS.Tables["Header"].Rows[0]["RecordCount"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("DispositionFlag") == true)
                        objrow["DispositionFlag"] = Convert.ToString(DS.Tables["Header"].Rows[0]["DispositionFlag"]).Trim();

                    if (DS.Tables["Claim"].Columns.Contains("ID") == true)
                        objrow["ClaimId"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]).Trim();


                    if (DS.Tables["Claim"].Columns.Contains("IDPayer") == true)
                        objrow["IDPayer"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["IDPayer"]).Trim();

                    if (DS.Tables["Claim"].Columns.Contains("PaymentReference") == true)
                        objrow["PaymentReference"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["PaymentReference"]).Trim();


                    if (DS.Tables["Claim"].Columns.Contains("DateSettlement") == true)
                        objrow["DateSettlement"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["DateSettlement"]).Trim();



                    if (DRActivity[0].Table.Columns.Contains("ID") == true)
                        objrow["ActivityId"] = Convert.ToString(DRActivity[j]["ID"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Start") == true)
                        objrow["Start1"] = Convert.ToString(DRActivity[j]["Start"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Type") == true)
                        objrow["Type2"] = Convert.ToString(DRActivity[j]["Type"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Code") == true)
                        objrow["Code2"] = Convert.ToString(DRActivity[j]["Code"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Quantity") == true)
                        objrow["Quantity"] = Convert.ToString(DRActivity[j]["Quantity"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Net") == true)
                        objrow["Net1"] = Convert.ToString(DRActivity[j]["Net"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("List") == true)
                        objrow["List"] = Convert.ToString(DRActivity[j]["List"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Clinician") == true)
                        objrow["Clinician"] = Convert.ToString(DRActivity[j]["Clinician"]).Trim();
                    //if(DRActivity.col

                    if (DRActivity[0].Table.Columns.Contains("PriorAuthorizationID") == true)
                        objrow["PriorAuthorizationID"] = Convert.ToString(DRActivity[j]["PriorAuthorizationID"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Gross") == true)
                        objrow["Gross"] = Convert.ToString(DRActivity[j]["Gross"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("PatientShare") == true)
                        objrow["PatientShare"] = Convert.ToString(DRActivity[j]["PatientShare"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("PaymentAmount") == true)
                        objrow["PaymentAmount"] = Convert.ToString(DRActivity[j]["PaymentAmount"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("DenialCode") == true)
                        objrow["DenialCode"] = Convert.ToString(DRActivity[j]["DenialCode"]).Trim();

                    DSView.Tables[0].Rows.Add(objrow);
                }

            }

            RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 1. XML Data Saved in DataSet");

            TempSaveXMLData();

            RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 2. XML Data Saved in Temp Table ");



        BindEnd: ;

        }

        void TempSaveXMLData()
        {

            if (fileLogo.FileName == "")
            {
                lblStatus.Text = "Please select the XML  file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto funEnd;
            }



            string strPath = GlobalValues.Shafafiya_Remittance_FilePath;

            int intCount = 1;
            for (int intFileList = 0; intFileList < fileLogo.PostedFiles.Count; intFileList++)
            {
                DataSet DS = new DataSet();
                Boolean IsHeader = false, IsClaim = false, IsEncounter = false, IsActivity = false;

                if (Path.GetExtension(fileLogo.PostedFiles[intFileList].FileName.ToLower()) == ".xml")
                {
                    ViewState["ActualFileName"] = fileLogo.PostedFiles[intFileList].FileName;
                    string strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + "_" + intCount + ".XML";

                    fileLogo.PostedFiles[intFileList].SaveAs(strPath + strFileName);

                    ViewState["FileName"] = strFileName;

                    if (lblFileList.Text != "")
                    {
                        lblFileList.Text += ", " + fileLogo.PostedFiles[intFileList].FileName;
                    }
                    else
                    {
                        lblFileList.Text = lblFileList.Text + fileLogo.PostedFiles[intFileList].FileName;
                    }

                    if (Convert.ToString(ViewState["FileNameList"]) != "")
                    {
                        ViewState["FileNameList"] += "|" + strFileName;
                    }
                    else
                    {
                        ViewState["FileNameList"] = strFileName;
                    }

                    DS.ReadXml(strPath + strFileName);
                    ViewState["RawXMLData"] = DS;

                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    lblStatus.Text = "Incorrect File Format!";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto funEnd;

                }




                foreach (DataTable table in DS.Tables)
                {
                    if (table.TableName == "Header")
                    {
                        IsHeader = true;
                    }
                    if (table.TableName == "Claim")
                    {
                        IsClaim = true;
                    }
                    if (table.TableName == "Encounter")
                    {
                        IsEncounter = true;
                    }

                    if (table.TableName == "Activity")
                    {
                        IsActivity = true;
                    }



                }


                if (IsHeader == false || IsClaim == false || IsActivity == false)   //|| IsEncounter == false 
                {
                    lblStatus.Text = "Incorrect File Format!";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto funEnd;
                }

                BuildRemittance();

                DataSet DSView = new DataSet();
                DSView = (DataSet)ViewState["TempRemittance"];


                for (int i = 0; i < DS.Tables["Claim"].Rows.Count; i++)
                {
                    DataTable DTActivity = new DataTable();
                    DataRow[] DRActivity;
                    DRActivity = DS.Tables["Activity"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                    ////DataTable DTEncounter = new DataTable();
                    ////DataRow[] DREncounter;
                    ////DREncounter = DS.Tables["Encounter"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());




                    for (int j = 0; j < DRActivity.Length; j++)
                    {
                        //DataTable DTObservation = new DataTable();
                        //DataRow[] DRObservation;



                        DataRow objrow;
                        objrow = DSView.Tables[0].NewRow();

                        if (DS.Tables["Header"].Columns.Contains("SenderID") == true)
                            objrow["SenderID"] = Convert.ToString(DS.Tables["Header"].Rows[0]["SenderID"]).Trim();

                        if (DS.Tables["Header"].Columns.Contains("ReceiverID") == true)
                            objrow["ProviderID"] = Convert.ToString(DS.Tables["Header"].Rows[0]["ReceiverID"]).Trim();

                        if (DS.Tables["Header"].Columns.Contains("TransactionDate") == true)
                            objrow["TransactionDate"] = Convert.ToString(DS.Tables["Header"].Rows[0]["TransactionDate"]).Trim();

                        if (DS.Tables["Header"].Columns.Contains("RecordCount") == true)
                            objrow["RecordCount"] = Convert.ToString(DS.Tables["Header"].Rows[0]["RecordCount"]).Trim();

                        if (DS.Tables["Header"].Columns.Contains("DispositionFlag") == true)
                            objrow["DispositionFlag"] = Convert.ToString(DS.Tables["Header"].Rows[0]["DispositionFlag"]).Trim();

                        if (DS.Tables["Claim"].Columns.Contains("ID") == true)
                            objrow["ClaimId"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]).Trim();


                        if (DS.Tables["Claim"].Columns.Contains("IDPayer") == true)
                            objrow["IDPayer"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["IDPayer"]).Trim();

                        if (DS.Tables["Claim"].Columns.Contains("PaymentReference") == true)
                            objrow["PaymentReference"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["PaymentReference"]).Trim();


                        if (DS.Tables["Claim"].Columns.Contains("DateSettlement") == true)
                            objrow["DateSettlement"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["DateSettlement"]).Trim();



                        if (DRActivity[0].Table.Columns.Contains("ID") == true)
                            objrow["ActivityId"] = Convert.ToString(DRActivity[j]["ID"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Start") == true)
                            objrow["Start1"] = Convert.ToString(DRActivity[j]["Start"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Type") == true)
                            objrow["Type2"] = Convert.ToString(DRActivity[j]["Type"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Code") == true)
                            objrow["Code2"] = Convert.ToString(DRActivity[j]["Code"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Quantity") == true)
                            objrow["Quantity"] = Convert.ToString(DRActivity[j]["Quantity"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Net") == true)
                            objrow["Net1"] = Convert.ToString(DRActivity[j]["Net"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("List") == true)
                            objrow["List"] = Convert.ToString(DRActivity[j]["List"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Clinician") == true)
                            objrow["Clinician"] = Convert.ToString(DRActivity[j]["Clinician"]).Trim();
                        //if(DRActivity.col

                        if (DRActivity[0].Table.Columns.Contains("PriorAuthorizationID") == true)
                            objrow["PriorAuthorizationID"] = Convert.ToString(DRActivity[j]["PriorAuthorizationID"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("Gross") == true)
                            objrow["Gross"] = Convert.ToString(DRActivity[j]["Gross"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("PatientShare") == true)
                            objrow["PatientShare"] = Convert.ToString(DRActivity[j]["PatientShare"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("PaymentAmount") == true)
                            objrow["PaymentAmount"] = Convert.ToString(DRActivity[j]["PaymentAmount"]).Trim();

                        if (DRActivity[0].Table.Columns.Contains("DenialCode") == true)
                            objrow["DenialCode"] = Convert.ToString(DRActivity[j]["DenialCode"]).Trim();

                        DSView.Tables[0].Rows.Add(objrow);
                    }

                }


                if (DSView.Tables[0].Rows.Count <= 0)
                {

                    lblStatus.Text = "Please select the XML File !";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto funEnd;
                }

                for (int i = 0; i < DSView.Tables[0].Rows.Count; i++)
                {
                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    // cmd.CommandText = "HMS_SP_TempRemittancexmlDataAdd";
                    cmd.CommandText = "HMS_SP_TempRemittancexmlDataUpload";

                    cmd.Parameters.Add(new SqlParameter("@SenderID", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["SenderID"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@ProviderId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ProviderId"].ToString().Trim();

                    cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["TransactionDate"].ToString().Trim();

                    cmd.Parameters.Add(new SqlParameter("@RecordCount", SqlDbType.Int)).Value = DSView.Tables[0].Rows[i]["RecordCount"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@DispositionFlag", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DispositionFlag"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@ClaimId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ClaimId"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@IDPayer", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["IDPayer"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@PaymentReference", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["PaymentReference"].ToString().Trim();




                    cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DateSettlement"].ToString().Trim();

                    cmd.Parameters.Add(new SqlParameter("@ActivityId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ActivityId"].ToString().Trim();


                    cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Start1"].ToString().Trim();

                    cmd.Parameters.Add(new SqlParameter("@Type2", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Type2"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@Code2", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Code2"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Quantity"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@Net1", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Net1"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@List", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["List"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@Clinician", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Clinician"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@PriorAuthorizationID", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["PriorAuthorizationID"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@Gross", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Gross"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@PatientShare", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["PatientShare"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@PaymentAmount", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["PaymentAmount"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("@DenialCode", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DenialCode"].ToString().Trim();
                    cmd.Parameters.Add(new SqlParameter("XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["FileName"]);
                    cmd.Parameters.Add(new SqlParameter("ActualFileName", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ActualFileName"]);

                    for (int j = 0; j < cmd.Parameters.Count; j++)
                    {
                        if (cmd.Parameters[j].Value == "")
                        {
                            cmd.Parameters[j].Value = DBNull.Value;
                        }
                    }




                    cmd.ExecuteNonQuery();
                    con.Close();
                }

                intCount = intCount + 1;

            }
        funEnd: ;
        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='RECEIPT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnShow.Enabled = false;
                fileLogo.Enabled = false;

                btnReceiptGenerate.Enabled = false;
                btnSaveToDB.Enabled = false;


            }


            if (strPermission == "7")
            {



                btnShow.Enabled = false;
                fileLogo.Enabled = false;

                btnReceiptGenerate.Enabled = false;
                btnSaveToDB.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Insurance");

            }
        }

        #endregion


        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' ";
            Criteria += " AND HCM_COMP_ID Like '" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A'   ";
            Criteria += " AND HCM_NAME Like '" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                {
                    SetPermission();
                }
            }

            if (!IsPostBack)
            {
                ViewState["FileNameList"] = "";
                txtFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewState["REMITTANCE_XML_DATA"] = false;
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {

                ViewState["FileNameList"] = "";
                lblFileList.Text = "";
                dboperations dbo = new dboperations();

              

                for (int i = 0; i < fileLogo.PostedFiles.Count; i++)
                {
                    //if (Convert.ToString(ViewState["UploadFiles"]) != "")
                    //{
                    //    ViewState["UploadFiles"] += "|" + fileLogo.PostedFiles[i].FileName  ;
                    //}
                    //else
                    //{
                    //    ViewState["UploadFiles"] =   fileLogo.PostedFiles[i].FileName ;
                    //}


                    string Criteria = " 1=1 ";
                    Criteria += " and ActualFileName = '" + fileLogo.PostedFiles[i].FileName + "'";

                    CommonBAL objCom = new CommonBAL();
                    objCom.fnDeleteTableData("TEMP_HMS_REMITTANCE_XML_DATA", Criteria);


                    DataSet DS = new DataSet();


                    DS = dbo.RemittanceXMLDataGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblStatus.Text = "Following File is already Uploaded " + fileLogo.PostedFiles[i];
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }


                    objCom = new CommonBAL();
                    DS = new DataSet();
                    Criteria = "SUBSTRING(ActualFileName,0,40) = SUBSTRING('" + fileLogo.PostedFiles[i].FileName + "',0,40) ";
                    DS = objCom.fnGetFieldValue(" TOP 1 * ", "HMS_REMITTANCE_XML_DATA", Criteria, "");

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblStatus.Text = "Similar File name is already Exists, File Name is  " + Convert.ToString(DS.Tables[0].Rows[0]["ActualFileName"]);
                        lblStatus.ForeColor = System.Drawing.Color.Red;

                        goto FunEnd;
                    }



                }
                if (fileLogo.PostedFiles.Count == 1)
                {
                    txtFileName.Text = fileLogo.FileName.ToLower();
                    ViewState["FileName"] = fileLogo.FileName.ToLower();
                }


                ViewState["REMITTANCE_XML_DATA"] = false;
                //   Clear();
                //  txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                //  txtRefNo.Text = fileLogo.FileName.ToLower();


                BuildRemittance();
                TempSaveXMLData();
                if (fileLogo.PostedFiles.Count == 1)
                {
                    BindData();
                }


                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");

            //   BindCompanyBalance();
            //  dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "Remittance", "TRANSACTION", "Remittance XML Loaded, File Name is   " + Convert.ToString(ViewState["FileName"]), Convert.ToString(Session["User_ID"]).Trim());
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_btnShow_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvRemittanceData.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblRefNo = (Label)gvScanCard.Cells[0].FindControl("lblRefNo");
                Label lblXMLFileName = (Label)gvScanCard.Cells[0].FindControl("lblXMLFileName");
                // txtReferenceNo.Text = lblRefNo.Text.Trim();
                ViewState["PaymentReference"] = lblRefNo.Text;
                // ViewState["FileName"] = lblXMLFileName.Text;
                ViewState["FileNameList"] = lblXMLFileName.Text;


                if (Convert.ToBoolean(ViewState["REMITTANCE_XML_DATA"]) == true)
                {
                    BindRemittanceClaims(lblXMLFileName.Text);
                }
                else
                {
                    BindTempRemittanceClaims(lblXMLFileName.Text);
                }

                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Resubmission.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (Convert.ToString(Convert.ToString(ViewState["FileName"])) == "")
                if (Convert.ToString(Convert.ToString(ViewState["FileNameList"])) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Receipt", "ShowErrorMessage('  SELECT REMITTANCE FILE .',' Please Select Remittance File ','Red')", true);

                    goto FunEnd;
                }


                string strUploadedFiles = Convert.ToString(ViewState["FileNameList"]);
                string[] arrUploadedFiles = strUploadedFiles.Split('|');

                for (int i = 0; i < arrUploadedFiles.Length; i++)
                {


                    TextFileWriting(System.DateTime.Now.ToString() + "1. XML Data Saving Started");

                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    SqlCommand cmd1 = new SqlCommand();

                    con.Open();
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandTimeout = 864000;
                    cmd1.CommandText = "HMS_SP_RemittancexmlDataUpload";
                    // cmd1.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(Convert.ToString(ViewState["FileName"])).Trim();
                    cmd1.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(arrUploadedFiles[i]).Trim();

                    cmd1.ExecuteNonQuery();
                    con.Close();



                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "2. XML Data saved successfully in HMS_REMITTANCE_XML_DATA File Name" + Convert.ToString(arrUploadedFiles[i]).Trim()); //Convert.ToString(ViewState["FileName"]);
                    dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "Remittance", "TRANSACTION", "XML data saved to HMS_REMITTANCE_XML_DATA table, File Name is " + Convert.ToString(arrUploadedFiles[i]).Trim(), Convert.ToString(Session["User_ID"]).Trim()); //Convert.ToString(ViewState["FileName"])

                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Remittance", "ShowErrorMessage('  DETAILS SAVED.',' Details Saved','Green')", true);


                //lblStatus.Text = "Details Saved";
                //lblStatus.ForeColor = System.Drawing.Color.Green;
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_btnSaveToDB_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Details Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        funEnd: ;

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear();

                BindRemittanceXMLData();
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Remittance_btnSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvRemittanceClaims.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvRemittanceClaims.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkInvResub");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnVerified_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();

                foreach (GridViewRow row in gvRemittanceClaims.Rows)
                {
                    Label lblClaimID = (Label)row.FindControl("lblClaimID");
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkInvResub");
                    if (ChkBoxRows.Checked == true)
                    {
                        string Criteria = " PaymentReference='" + Convert.ToString(ViewState["PaymentReference"]) + "' AND ClaimID='" + lblClaimID.Text + "'";
                        string FieldNameWithValues = " InvoiceStatus='Verified' ";

                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_REMITTANCE_XML_DATA", Criteria);

                    }

                }

                BindRemittanceClaims(Convert.ToString(ViewState["FileName"]));//PaymentReference
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Remittance_btnVerified_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnReceiptGenerated_Click(object sender, EventArgs e)
        {
            try
            {

                string strUploadedFiles = Convert.ToString(ViewState["FileNameList"]);
                string[] arrUploadedFiles = strUploadedFiles.Split('|');

                for (int i = 0; i < arrUploadedFiles.Length; i++)
                {
                    ViewState["FileName"] = Convert.ToString(arrUploadedFiles[i]).Trim();
                    DataSet DS = new DataSet();
                    CommonBAL objCom = new CommonBAL();
                    //// string Criteria = " XMLFILENAME='" + Convert.ToString(Convert.ToString(ViewState["FileName"])) + "'";
                    string Criteria = " XMLFILENAME='" + Convert.ToString(arrUploadedFiles[i]).Trim() + "'";

                    DS = objCom.fnGetFieldValue("top 1 *", "HMS_REMITTANCE_XML_DATA", Criteria, "");

                    if (DS.Tables[0].Rows.Count <= 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Receipt", "ShowErrorMessage('  NO REMITTANCE DATA .',' Remittance Data not Available, Please Save the Remittance then click the Generate Receipt ','Red')", true);

                        goto FunEnd;
                    }

                    if (Convert.ToString(arrUploadedFiles[i]).Trim() == "") //
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Receipt", "ShowErrorMessage('  SELECT REMITTANCE FILE .',' Please Select Remittance File ','Red')", true);

                        goto FunEnd;
                    }

                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    SqlCommand cmd1 = new SqlCommand();

                    con.Open();
                    cmd1 = new SqlCommand();
                    cmd1.Connection = con;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.CommandTimeout = 864000;
                    cmd1.CommandText = "HMS_SP_RemittanceUpdateNew";
                    cmd1.Parameters.Add(new SqlParameter("@HRM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd1.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(arrUploadedFiles[i]).Trim();//  Convert.ToString(ViewState["FileName"]).Trim();
                    cmd1.Parameters.Add(new SqlParameter("@CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]).Trim(); ;
                    cmd1.ExecuteNonQuery();
                    con.Close();

                }

                BindRemittanceClaims(Convert.ToString(ViewState["FileName"]));//PaymentReference

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Remittance", "ShowErrorMessage('  RECEIPT GENERATED.',' Receipt Generated.','Green')", true);


                // lblStatus.Text = " Receipt Generated ";
                // lblStatus.ForeColor = System.Drawing.Color.Green;
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");

                // BindRemittanceClaims(Convert.ToString(ViewState["FileName"]).Trim());

                BindRemittanceXMLData();
                ViewState["FileNameList"] = "";
                ViewState["FileName"] = "";
                lblFileList.Text = "";
                //gvRemittanceData.Visible = false;
                //gvRemittanceData.DataBind();

                gvRemittanceClaims.Visible = false;
                gvRemittanceClaims.DataBind();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Remittance_btnVerified_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvRemittanceData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                    lblSerial.Text = ((gvRemittanceData.PageIndex * gvRemittanceData.PageSize) + e.Row.RowIndex + 1).ToString();
                }
            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Remittance.gvRemittanceData_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvRemittanceClaims_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                    lblSerial.Text = ((gvRemittanceClaims.PageIndex * gvRemittanceClaims.PageSize) + e.Row.RowIndex + 1).ToString();
                }
            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Remittance.gvRemittanceClaims_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

                createDataInExcel();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Remittance.btnExport_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        private void createDataInExcel()
        {

            DataSet DS = new DataSet();

            string Criteria = " 1=1  ";//AND UploadStatus='UPLOADED'
            string strStartDate = txtFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),DateSettlement,101),101) <= '" + strForToDate + "'";
            }

            if (txtFileName.Text.Trim() != "")
            {
                Criteria = "   ActualFileName like'" + txtFileName.Text.Trim() + "%'";
            }


            if (txtCompany.Text.Trim() != "")
            {
                Criteria += " AND SenderID  IN ( SELECT HCM_FIELD5 FROM HMS_COMPANY_MASTER WHERE  HCM_BILL_CODE='" + txtCompany.Text.Trim() + "' )";
            }


            if (txtSrcReferenceNo.Text.Trim() != "")
            {
                Criteria += " AND PaymentReference='" + txtSrcReferenceNo.Text.Trim() + "'";
            }

            dboperations objDp = new dboperations();

            DS = objDp.RemittanceXMLDataGet(Criteria);


            if (DS.Tables[0].Rows.Count <= 0)
            {
                lblStatus.Text = "No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strPath = GlobalValues.Shafafiya_Remittance_FilePath;

            if (Directory.Exists(strPath) == false)
            {
                Directory.CreateDirectory(strPath);
            }


            oXL = new Microsoft.Office.Interop.Excel.Application();
            oXL.Visible = false;
            //Get a new workbook.
            oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(System.Reflection.Missing.Value));
            oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;



            oSheet.Cells[1, 1] = "SL No";
            oSheet.Cells[1, 2] = "Claim ID";
            oSheet.Cells[1, 3] = "Date";


            oSheet.Cells[1, 4] = "CPT Code";
            oSheet.Cells[1, 5] = "Qty.";
            oSheet.Cells[1, 6] = "Price";
            oSheet.Cells[1, 7] = "Total";


            oSheet.Cells[1, 8] = "Net Amt";
            oSheet.Cells[1, 9] = "PT Share";
            oSheet.Cells[1, 10] = "Claim Amt";
            oSheet.Cells[1, 11] = "Paid Amt";
            oSheet.Cells[1, 12] = "Balance";


            oSheet.Cells[1, 13] = "Denial Code";
            oSheet.Cells[1, 14] = "Denial Desc";
            oSheet.Cells[1, 15] = "SenderID";





            int intRow = 2, intSlNo = 1;
            foreach (DataRow DR in DS.Tables[0].Rows)
            {

                oSheet.Cells[intRow, 1] = intSlNo; //"SL No";
                oSheet.Cells[intRow, 2] = Convert.ToString(DR["ClaimId"]); //"Claim ID";
                oSheet.Cells[intRow, 3] = Convert.ToString(DR["Start1Desc"]); //"Date";


                oSheet.Cells[intRow, 4] = Convert.ToString(DR["Code2"]); //"CPT Code";
                oSheet.Cells[intRow, 5] = Convert.ToString(DR["Quantity"]); //"Qty.";
                oSheet.Cells[intRow, 6] = Convert.ToString(DR["Price"]); //"Price";
                oSheet.Cells[intRow, 7] = Convert.ToString(DR["Net1"]); //"Total";


                oSheet.Cells[intRow, 8] = Convert.ToString(DR["Gross"]); //"Net Amt";
                oSheet.Cells[intRow, 9] = Convert.ToString(DR["PatientShare"]); //"PT Share";
                oSheet.Cells[intRow, 10] = Convert.ToString(DR["Net1"]); //"Claim Amt";
                oSheet.Cells[intRow, 11] = Convert.ToString(DR["PaymentAmount"]); //"Paid Amt";
                oSheet.Cells[intRow, 12] = Convert.ToString(DR["Balance"]); //"Balance";


                oSheet.Cells[intRow, 13] = Convert.ToString(DR["DenialCode"]); //"Denial Code";
                oSheet.Cells[intRow, 14] = Convert.ToString(DR["DenialDesc"]); ; //"Denial Desc";
                oSheet.Cells[intRow, 15] = Convert.ToString(DR["SenderID"]); //"SenderID";

                intRow++;
                intSlNo++;


            }



            oRng = oSheet.get_Range("A1", "IV1");
            oRng.EntireColumn.AutoFit();
            oXL.Visible = false;
            oXL.UserControl = false;
            string strFile = "RemittanceFile_" + strName + ".xls"; //"report" + DateTime.Now.Ticks.ToString() + ".xls";//+
            oWB.SaveAs(strPath +
            strFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, null, null, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, false, false, null, null);
            // Need all following code to clean up and remove all references!!!
            oWB.Close(null, null, null);
            oXL.Workbooks.Close();
            oXL.Quit();

            lblStatus.Text = "Remittance File Exported, File Name is - " + strFile;
            lblStatus.ForeColor = System.Drawing.Color.Green;


        FunEnd: ;

        }





    }
}