﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.Data;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
namespace Mediplus
{
    public partial class ChartViewer : System.Web.UI.Page
    {
     /*
      protected void BindChart()
      {
          DataSet ds = new DataSet();
            
         // string cmdstr = "select top 6 Country, COUNT(CompanyName) [Total Suppliers] from [Suppliers] group by Country";

          string Criteria = " 1=1 ";
          /*
          string strStartDate = txtFromDate.Text;
          string[] arrDate = strStartDate.Split('/');
          string strForStartDate = "";

          if (arrDate.Length > 1)
          {
              strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
          }

          if (txtFromDate.Text != "")
          {
              Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
          }



          string strTotDate = txtToDate.Text;
          string[] arrToDate = strTotDate.Split('/');
          string strForToDate = "";

          if (arrToDate.Length > 1)
          {
              strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
          }


          if (txtToDate.Text != "")
          {
              Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
          }

          if (drpDoctor.SelectedIndex != 0)
          {
              Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
          }

          */

      ////    CommonBAL objcom = new CommonBAL();
      ////    DataSet DS = new DataSet();
      ////    DS = objcom.fnGetFieldValue("COUNT(HPV_VISIT_TYPE),COUNT(HPV_PT_ID)","HMS_PATIENT_VISIT  ", Criteria +"  group by HPV_VISIT_TYPE ","");

      ////    string[] x = new string[DS.Tables[0].Rows.Count];
      ////    int[] y = new int[DS.Tables[0].Rows.Count];
      ////    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
      ////    {
      ////        x[i] = DS.Tables[0].Rows[i][0].ToString();
      ////        y[i] = Convert.ToInt32(DS.Tables[0].Rows[i][1]);
      ////    }
      ////    Chart1.Series[0].Points.DataBindXY(x, y);
      ////    Chart1.Series[0].ChartType = SeriesChartType.Column;
      ////    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
      ////    Chart1.Legends[0].Enabled = true;
      ////}

        void GetVisitDtlChartData()
        {

            string Criteria = " 1=1 ";

            //string strStartDate = txtFromDate.Text;
            //string[] arrDate = strStartDate.Split('/');
            //string strForStartDate = "";

            //if (arrDate.Length > 1)
            //{
            //    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            //}

            //if (txtFromDate.Text != "")
            //{
            //    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            //}



            //string strTotDate = txtToDate.Text;
            //string[] arrToDate = strTotDate.Split('/');
            //string strForToDate = "";

            //if (arrToDate.Length > 1)
            //{
            //    strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            //}


            //if (txtToDate.Text != "")
            //{
            //    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            //}

            //if (drpDoctor.SelectedIndex != 0)
            //{
            //    Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            //}



            CommonBAL objcom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objcom.fnGetFieldValue("COUNT(HPV_VISIT_TYPE) as VisitType,COUNT(HPV_PT_ID) as PTCount", "HMS_PATIENT_VISIT", Criteria, "");

            Series series = VisitDtlChart.Series["Series1"];
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                series.Points.AddXY(Convert.ToString(DR["VisitType"]), Convert.ToString(DR["PTCount"]));
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetVisitDtlChartData();
            }

        }
    }
}