﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus
{
    public partial class LoyaltyPrograms : System.Web.UI.Page
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
              dboperations dbo = new dboperations();

            string Criteria = " 1=1 ";


            if (txtSrcName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["PTNameMultiple"]) != "" && Convert.ToString(Session["PTNameMultiple"]) == "N")
                {
                    Criteria += " AND HPM_PT_FNAME like '" + txtSrcName.Text.Trim() + "%' ";
                }
                else
                {

                    if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                    {
                        Criteria += " AND HPM_PT_FNAME like '" + txtSrcName.Text.Trim() + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                    {
                        Criteria += " AND HPM_PT_FNAME + ' ' + ISNULL(HPM_PT_MNAME,'')   like '%" + txtSrcName.Text.Trim() + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                    {
                        Criteria += " AND HPM_PT_FNAME + ' ' + ISNULL(HPM_PT_MNAME,'')  = '" + txtSrcName.Text.Trim() + "' ";
                    }
                }

            }

            if (txtSrcLName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["PTNameMultiple"]) != "" && Convert.ToString(Session["PTNameMultiple"]) == "N")
                {

                    Criteria += " AND  SUBSTRING(HPM_PT_FNAME,CHARINDEX(' ',HPM_PT_FNAME),len(HPM_PT_FNAME))    like '%" + txtSrcLName.Text.Trim() + "%' ";
                }
                else
                {
                    if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                    {
                        Criteria += " AND HPM_PT_LNAME like '" + txtSrcLName.Text + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                    {
                        Criteria += " AND ISNULL(HPM_PT_LNAME,'')   like '%" + txtSrcLName.Text.Trim() + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                    {
                        Criteria += " AND  ISNULL(HPM_PT_LNAME,'')   = '" + txtSrcLName.Text.Trim() + "' ";
                    }
                }

            }


            if (txtSrcFileNo.Text.Trim() != "")
            {


                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_ID like '" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_ID  like '%" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_ID   = '" + txtSrcFileNo.Text.Trim() + "' ";
                }



            }

            if (txtSrcPhone1.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PHONE1 like '" + txtSrcPhone1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PHONE1  like '%" + txtSrcPhone1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PHONE1   = '" + txtSrcPhone1.Text.Trim() + "' ";
                }

            }

            if (txtSrcMobile1.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_MOBILE like '" + txtSrcMobile1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_MOBILE  like '%" + txtSrcMobile1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_MOBILE   = '" + txtSrcMobile1.Text.Trim() + "' ";
                }

            }



            DataSet DS = new DataSet();
            DS = dbo.retun_patient_details(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

         

            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

               
            }
            else
            {
                gvGridView.Visible = false;
                gvGridView.DataBind();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
            }


            
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }

            try
            {


                if (!IsPostBack)
                {
                    BindGrid();
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Firstname_Zoom.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }  
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      LoyaltyPrograms.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Firstname_Zoom.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }
        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ServSelectIndex"] = gvScanCard.RowIndex;
             

              Label  lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
              Label  lblPatientName = (Label)gvScanCard.Cells[0].FindControl("lblPatientName");
              Label  lblMobile = (Label)gvScanCard.Cells[0].FindControl("lblMobile");
              Label  lblEmailId = (Label)gvScanCard.Cells[0].FindControl("lblEmailId");

               Label lblLoyaltyPoint = (Label)gvScanCard.Cells[0].FindControl("lblLoyaltyPoint");


               txtLoyaltyPoint.Text = lblLoyaltyPoint.Text;

             }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      LoyaltyPrograms.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}