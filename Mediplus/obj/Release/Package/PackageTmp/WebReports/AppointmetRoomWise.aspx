﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="AppointmetRoomWise.aspx.cs" Inherits="Mediplus.WebReports.AppointmetRoomWise" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .Header {
            font: bold 18px/100% Arial;
            background-color: #fff;
            color: black;
            text-align: Center;
            height: 30px;
            width: 50px;
            border-color: black;
        }
         .SubHeader {
           font: bold 15px/100% Arial;
           background-color: #fff;
           vertical-align:middle;
           text-align:center;
           border: 1px; 
           border-color:black;
           border: thin; border-color: #CCCCCC; border-style: groove;;
           height: 30px;
        }

        .RowHeader {
            font: bold 11px/100% Arial;
            text-align: center;
            border-color: black;
        }

        .Content {
            font: 12px/100% Arial;
            background-color: #fff;
            vertical-align:middle;
            text-align:center;
            border: 1px; 
            border-color:black;border: thin; border-color: #CCCCCC; border-style: groove;
           
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
      <asp:HiddenField ID="hidStatus" runat="server" />

     <asp:HiddenField ID="hidDisplayRoomCode" runat="server" Value="Room 1|Room 2" />
      <table width="100%" cellpadding="5" cellspacing="5">
          <tr>
              <td style="width:50%;" class="Header" > <%=Convert.ToString(ViewState["DEPT_ID"])%></td>
              <td style="width:50%;" class="Header"> <%=Convert.ToString(ViewState["APP_DATE"])%></td>
          </tr>
            <tr>
                <td colspan="2">
                    <%=strData%>

                </td>
            </tr>

        </table>
     
</asp:Content>
