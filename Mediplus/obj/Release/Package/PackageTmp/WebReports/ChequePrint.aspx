﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="ChequePrint.aspx.cs" Inherits="Mediplus.WebReports.ChequePrint" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
        <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    
      <style type="text/css">
         #site-logo
         {
             display: none;
         }

         .box-header
         {
             display: none;
         }

         table
         {
             border-collapse: collapse;
         }

         /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

         .BoldStyle
         {
             font-weight: bold;
         }
     </style>
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
      <table style="width:100%;text-align:center;vertical-align:top;" >
        <tr>
            <td >
                   <img style="padding:1px;height:400px;height:100px;border:none;" src="images/Report_Logo.PNG"  />
            </td>
        </tr>

    </table>
   
    <table style="width:100%;">
        <tr>
            <td>
                <div id="divChequeDate" runat="server" style="position: absolute; left: 600px; top: 100px;">
                    <asp:Label ID="lblChequeDate" runat="server"  ></asp:Label>
                </div>

                 <div id="divPayAgainst" runat="server" style="position: absolute; left : 100px; top: 150px;">
                    <asp:Label ID="lblPayAgainst" runat="server"  ></asp:Label>
                </div>

                 <div id="divAmountInwords" runat="server" style="position: absolute; left: 100px; top: 150px;">
                    <asp:Label ID="lblAmountInwords" runat="server"   ></asp:Label>
                </div>
                <div id="divAmount" runat="server" style="position: absolute; left: 600px; top: 200px;">
                    <asp:Label ID="lblAmount" runat="server" ></asp:Label>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
