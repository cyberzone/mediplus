﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SampleCollectionPopup.aspx.cs" Inherits="Mediplus.Laboratory.SampleCollectionPopup" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lab Waiting List Popup</title>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />


        <script language="javascript" type="text/javascript">

            function PassValue(PageName, TransID, PatientID, LabSampleID, LabSampleBareCode) {

                if (PageName == 'LabTestReport') {
                    opener.location.href = "../Laboratory/SampleCollection.aspx?PageName=LabWaitList&MenuName=Laboratory&TransID=" + TransID + "&PatientID=" + PatientID + "&LabRegisterID=" + LabRegisterID + "&LabRegisterBareCode=" + LabRegisterBareCode;
                }

                window.parent.close();
                window.parent.parent.close();
                window.close();
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <div>
          <asp:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
            </asp:toolkitscriptmanager>
      <table width="90%">
                <tr>
                    <td style="text-align: left; width: 50%;" class="PageHeader">
                      
                         <asp:label id="lblHeader"  runat="server"  cssclass="PageHeader" text="" ></asp:label>
                    </td>
                    <td style="text-align: right; width: 50%;">
                     
                    </td>
                </tr>
            </table>
    <br />
     <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="90%">
                <tr>
                    <td>
                        <asp:label id="lblStatus" runat="server" forecolor="red" style="letter-spacing: 1px;font-weight:bold;" cssclass="label"></asp:label>
                    </td>
                </tr>

            </table>
    <table width="90%" border="0" cellpadding="3" cellspacing="3">

                    <tr>
                        
                        <td class="lblCaption1" style="height: 25px;">From Date
                        </td>
                        <td >
                            <asp:updatepanel id="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                          <asp:TextBox ID="txtTransFromDate" runat="server" Width="70px" CssClass="TextBoxStyle" height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"  Enabled="True" TargetControlID="txtTransFromDate" Format="dd/MM/yyyy">
                                                                                            </asp:CalendarExtender>    

                                            </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                         <td class="lblCaption1" style="height: 25px;"> To Date
                        </td>
                        <td >
                            <asp:updatepanel id="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                          <asp:TextBox ID="txtTransToDate" runat="server" Width="70px" height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"  Enabled="True" TargetControlID="txtTransToDate" Format="dd/MM/yyyy">
                                                                                            </asp:CalendarExtender>    

                                            </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                         <td class="lblCaption1" style="height: 25px;"> File No
                        </td>
                        <td>
                             <asp:updatepanel id="UpdatePanel17" runat="server">
                                                                <ContentTemplate>
                                                        <asp:TextBox ID="txtFileNo" runat="server" Width="70px" height="20PX" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>
                                                                </ContentTemplate>
                                                         </asp:updatepanel>
                        </td>
                        <td class="lblCaption1" style="height: 25px;">Name
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel20" runat="server">
                                                                <ContentTemplate>
                                                             <asp:TextBox ID="txtFName" runat="server" Width="300px" height="20PX" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                                                 </ContentTemplate>
                                                         </asp:updatepanel>
                                        </td>
                        <td>
                              <asp:Button ID="btnShow" runat="server" CssClass="button orange small" Text="Show" OnClick="btnShow_Click" />
                        </td>
                          </tr>

            </table>

       <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">


                            <asp:gridview id="gvLabSamplePopup" runat="server" allowsorting="True" autogeneratecolumns="False"
                                enablemodelvalidation="True" width="100%">
                                                                 <HeaderStyle CssClass="GridHeader_Blue" />
                                                                    <RowStyle CssClass="GridRow" />
                                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                                <Columns>
                                                                    
                                                                    <asp:TemplateField HeaderText="Sample ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="LinkButton1" CssClass="lblCaption1"  runat="server"  OnClick="LabSampleEdit_Click"   >
                                                                                
                                                                            <asp:Label ID="lblgvLabSampleID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_TEST_SAMPLE_ID") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="LinkButton2" CssClass="lblCaption1"  runat="server"  OnClick="LabSampleEdit_Click"   >
                                                                            <asp:Label ID="lblgvLabSampleDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_DATEDesc") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Patient" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton3" CssClass="lblCaption1"  runat="server"  OnClick="LabSampleEdit_Click"   >
                                                                            <asp:Label ID="lblgvLabSamplePTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_ID") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton4" CssClass="lblCaption1"  runat="server"  OnClick="LabSampleEdit_Click"   >
                                                                            <asp:Label ID="lblgvLabSamplePTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_NAME") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="BarCode" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton5" CssClass="lblCaption1"  runat="server"  OnClick="LabSampleEdit_Click"   >
                                                                            <asp:Label ID="lblgvLabSampleBareCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_BARECODE") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                       
                                                                </Columns>
                                                            </asp:gridview>

                        </td>

                    </tr>

                </table>
            </div>

        <table width="100%" border="0" cellpadding="3" cellspacing="3">

                    <tr>
                        
                        <td class="lblCaption1" style="height: 25px;width:100px;">Status
                        </td>
                        <td style="width:200px;">
                            <asp:updatepanel id="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                            <asp:dropdownlist id="drpWaitListStatus" runat="server"  style="width: 120px" class="lblCaption1" >
                                           <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                                           <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                            
                                   </asp:dropdownlist> 

                                            </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                        <td>
                              <asp:Button ID="btnShowTestReport" runat="server" CssClass="button orange small" Text="Test Report" OnClick="btnShowTestReport_Click" />
                        </td>
                          </tr>

            </table>
    </div>
    </form>
</body>
</html>
