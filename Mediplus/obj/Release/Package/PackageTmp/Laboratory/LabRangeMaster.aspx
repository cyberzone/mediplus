﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="LabRangeMaster.aspx.cs" Inherits="Mediplus.Laboratory.LabRangeMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

     <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function SaveValidate() {
          


            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
                 label.style.color = 'red';
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtRangeID.ClientID%>').value
            if (/\S+/.test(strtxtServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Range Id";
                document.getElementById('<%=txtRangeID.ClientID%>').focus;
                return false;
            }

            var strtxtServDesc = document.getElementById('<%=txtDescription.ClientID%>').value
            if (/\S+/.test(strtxtServDesc) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Range Description";
                document.getElementById('<%=txtDescription.ClientID%>').focus;
                return false;
            }

 
            
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />


    <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="Range Master" Text="Range Master"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpPanalSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Text="Clear" OnClick="btnClear_Click"   />
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" OnClientClick="return SaveValidate();" />
                    </ContentTemplate>

                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanalSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 80%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <table width="100%" border="0" cellpadding="3" cellspacing="3">
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Code
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRangeID" runat="server" Width="150px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtRangeID_TextChanged"></asp:TextBox>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Description
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" Width="300px" CssClass="TextBoxStyle" ondblclick="return ProfileMasterShow('txtDescription',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Status
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" Style="width: 155px" class="TextBoxStyle">
                                <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                <asp:ListItem Value="I">InActive</asp:ListItem>

                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

    </div>
    <div style="padding-top: 0px; width: 80%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvRangeMaster" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>

                        <asp:TemplateField HeaderText="Range Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton9" CssClass="lblCaption1" runat="server" OnClick="TestRangeEdit_Click">

                                    <asp:Label ID="lblgvRangeID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LNRT_RANGE_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton10" CssClass="lblCaption1" runat="server" OnClick="TestRangeEdit_Click">
                                    <asp:Label ID="lblgvRangeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LNRT_DESCRIPTION") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton11" CssClass="lblCaption1" runat="server" OnClick="TestRangeEdit_Click">
                                    <asp:Label ID="lblgvRangeStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LNRT_STATUS") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
