﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LabTestWaitingListPopup.aspx.cs" Inherits="HMS.Laboratory.LabTestWaitingListPopup" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Lab Test Profile Master Popup</title>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


        <script language="javascript" type="text/javascript">

            function PassValue(PageName, TransID, PatientID, DoctorID, TransDate, TransType, ServiceID) {
                if (PageName == 'LabWaitList') {
                    opener.location.href = "../Radiology/TransactionContainer.aspx?PageName=LabWaitList&TransID=" + TransID + "&PatientID=" + PatientID + "&DoctorID=" + DoctorID + "&TransDate=" + TransDate + "&TransType=" + TransType + "&ServiceID=" + ServiceID;

                }
                else if (PageName == 'SampleCollection') {
                    opener.location.href = "../Laboratory/SampleCollection.aspx?PageName=LabWaitList&TransID=" + TransID + "&PatientID=" + PatientID + "&DoctorID=" + DoctorID + "&TransDate=" + TransDate + "&TransType=" + TransType + "&ServiceID=" + ServiceID;
                }
                else if (PageName == 'RadWaitList') {
                    opener.location.href = "../Radiology/TransactionContainer.aspx?PageName=RadWaitList&TransID=" + TransID + "&PatientID=" + PatientID + "&DoctorID=" + DoctorID + "&TransDate=" + TransDate + "&TransType=" + TransType + "&ServiceID=" + ServiceID;
                }

                window.parent.close();
                window.parent.parent.close();
                window.close();
            }
    </script>
</head>
<body>
  <form id="form1" runat="server">
         <asp:hiddenfield id="hidPageName" runat="server" />
    <div>
        <asp:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
            </asp:toolkitscriptmanager>
      <table width="90%">
                <tr>
                    <td style="text-align: left; width: 50%;" class="PageHeader">
                      
                         <asp:label id="lblHeader"  runat="server"  cssclass="PageHeader" text="" ></asp:label>
                    </td>
                    <td style="text-align: right; width: 50%;">
                     
                    </td>
                </tr>
            </table>
    <br />
     <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="90%">
                <tr>
                    <td>
                        <asp:label id="lblStatus" runat="server" forecolor="red" style="letter-spacing: 1px;font-weight:bold;" cssclass="label"></asp:label>
                    </td>
                </tr>

            </table>
    <table width="90%" border="0" cellpadding="3" cellspacing="3">

                    <tr>
                        
                        <td class="lblCaption1" style="height: 25px;">From Date
                        </td>
                        <td >
                            <asp:updatepanel id="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                          <asp:TextBox ID="txtTransFromDate" runat="server" Width="70px" height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"  Enabled="True" TargetControlID="txtTransFromDate" Format="dd/MM/yyyy">
                                                                                            </asp:CalendarExtender>    

                                            </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                         <td class="lblCaption1" style="height: 25px;"> To Date
                        </td>
                        <td >
                            <asp:updatepanel id="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                          <asp:TextBox ID="txtTransToDate" runat="server" Width="70px" height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"  Enabled="True" TargetControlID="txtTransToDate" Format="dd/MM/yyyy">
                                                                                            </asp:CalendarExtender>    

                                            </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                         <td class="lblCaption1" style="height: 25px;"> File No
                        </td>
                        <td>
                             <asp:updatepanel id="UpdatePanel17" runat="server">
                                                                <ContentTemplate>
                                                        <asp:TextBox ID="txtFileNo" runat="server" Width="70px" height="20PX" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>
                                                                </ContentTemplate>
                                                         </asp:updatepanel>
                        </td>
                        <td class="lblCaption1" style="height: 25px;">Name
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel20" runat="server">
                                                                <ContentTemplate>
                                                             <asp:TextBox ID="txtFName" runat="server" Width="300px" height="20PX" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                                                 </ContentTemplate>
                                                         </asp:updatepanel>
                                        </td>
                        <td>
                              <asp:Button ID="btnShow" runat="server" CssClass="button orange small" Text="Show" OnClick="btnShow_Click" />
                        </td>
                          </tr>

            </table>
    <div style="padding-top: 0px; width: 1000PX; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvWaitingList" runat="server"  Width="100%"
                    AllowSorting="True" AutoGenerateColumns="False" 
                    EnableModelValidation="True"    >
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                
                    <Columns>

                        <asp:TemplateField HeaderText="No" SortExpression="ID">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="lblTransDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("TransDate") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("DoctorID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblServiceID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ServiceID") %>' visible="false" ></asp:Label>
                                    <asp:Label ID="lblID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="File No" SortExpression="FileNo" >
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                   
                                    <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("FileNo") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" SortExpression="PatientName">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="Label2" CssClass="GridRow"  Width="250px"  runat="server" Text='<%# Bind("PatientName") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Doctor" SortExpression="Doctor">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkMob" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="Label3" CssClass="GridRow" Width="150px"   runat="server" Text='<%# Bind("Doctor") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Trans Type" SortExpression="TransType">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="lblTransType" CssClass="GridRow" Width="70px"  runat="server" Text='<%# Bind("TransType") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Type" SortExpression="PatientType">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDepName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="Label5" CssClass="GridRow" Width="70px"  runat="server" Text='<%# Bind("PatientType") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Service Requested" SortExpression="ServiceRequested">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkTime" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="Label7" CssClass="GridRow"  runat="server" Text='<%# Bind("ServiceRequested") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          
                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

     <table width="100%" border="0" cellpadding="3" cellspacing="3">

                    <tr>
                        
                        <td class="lblCaption1" style="height: 25px;width:100px;">Status
                        </td>
                        <td style="width:200px;">
                            <asp:updatepanel id="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                            <asp:dropdownlist id="drpWaitListStatus" runat="server"  style="width: 120px" class="lblCaption1" >
                                           <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                                           <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                            
                                   </asp:dropdownlist> 

                                            </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                        <td>
                              <asp:Button ID="btnShowTestReport" runat="server" CssClass="button orange small" Text="Test Report" OnClick="btnShowTestReport_Click" />
                        </td>
                          </tr>

            </table>
    </div>
    </form>
</body>
</html>