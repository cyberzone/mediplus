﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="LabTestWaitingList.aspx.cs" Inherits="HMS.Laboratory.LabTestWaitingList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <script language="javascript" type="text/javascript">
        function ShowRadRequestPopup() {
            document.getElementById("divRadRequestPopup").style.display = 'block';

        }

        function HideRadRequestPopup() {

            document.getElementById("divRadRequestPopup").style.display = 'none';

        }



        function ShowRadRequestPopup(EMR_ID) {

            var win = window.open("../Radiology/EMRRadRequest.aspx?PageName=RadWaitList&MenuName=Laboratory&EMR_ID=" + EMR_ID, "ScanMaster", "top=300,left=370,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }

        function RadResultReport(TransNo) {

            var Report = "RadiologyReport.rpt";
            var Criteria = " 1=1 ";

            if (TransNo != "") {

                Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

                var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
                win.focus();


            }

        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table width="90%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Register Waiting"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;"></td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>
    <table width="80%" border="0" cellpadding="3" cellspacing="3">

        <tr>

            <td class="lblCaption1" style="height: 25px;">From Date
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTransFromDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransFromDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                        :
                         <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1" style="height: 25px;">To Date
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTransToDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtTransToDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpEndHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                        :
                       <asp:DropDownList ID="drpEndMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1" style="height: 25px;">Status
            </td>
            <td style="width: 200px;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpWaitListStatus" runat="server" Style="width: 160px" class="TextBoxStyle">
                             <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                            <asp:ListItem Value="Completed">Completed</asp:ListItem>

                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>

            <td class="lblCaption1" style="height: 25px;">File No
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFileNo" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 25px;">Name
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFName" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td class="lblCaption1" style="height: 25px;">Mobile No
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtMobileNo" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

            
            <td>
                <asp:Button ID="btnShow" runat="server" CssClass="button red small" Text="Show" OnClick="btnShow_Click" />
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvWaitingList" runat="server" Width="100%" 
                    AllowSorting="True" AutoGenerateColumns="False"  OnSorting="gvWaitingList_Sorting" OnRowDataBound="gvWaitingList_RowDataBound"
                    EnableModelValidation="True">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />

                    <Columns>
                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                              <ItemTemplate>
                                      <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                            OnClick="Edit_Click" />&nbsp;&nbsp;
                              </ItemTemplate>
                       </asp:TemplateField>
                        <asp:TemplateField HeaderText="NO" SortExpression="ID">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblTransDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("TransDate") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("DoctorID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblServiceID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ServiceID") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="lblID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                 
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FILE NO" SortExpression="FileNo">
                            <ItemTemplate>
                               

                                    <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("FileNo") %>'></asp:Label>
                                 
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NAME" SortExpression="PatientName">
                            <ItemTemplate>
                                
                                    <asp:Label ID="Label2" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("PatientName") %>'></asp:Label>
                                
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MOBILE NO" SortExpression="MobileNo">
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblMobileNo" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("MobileNo") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DOCTOR" SortExpression="Doctor">
                            <ItemTemplate>
                                
                                    <asp:Label ID="Label3" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("Doctor") %>'></asp:Label>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TRANS TYPE" SortExpression="TransType">
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblTransType" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("TransType") %>'></asp:Label>
                                 
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PATIENT TYPE" SortExpression="PatientType">
                            <ItemTemplate>
                                
                                    <asp:Label ID="Label5" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("PatientType") %>'></asp:Label>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority" SortExpression="Priority">
                            <ItemTemplate>
                                     <asp:Label ID="lblPriority" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("Priority") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="INVOICE STATUS" SortExpression="InvoiceStatus">
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblInvStatus" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("InvoiceStatus") %>'></asp:Label>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <table width="80%" border="0" cellpadding="3" cellspacing="3">

        <tr>


            <td>
                <asp:Button ID="btnEMRRRadRequest" runat="server" CssClass="button gray small" Text="EMR Request" OnClick="btnEMRRRadRequest_Click" />

                <asp:Button ID="btnLabTestRegister" runat="server" CssClass="button red small" Text="Lab Test Register" OnClick="btnLabTestRegister_Click" />
                <asp:Button ID="btnShowTestReport" runat="server" CssClass="button gray small" Text="Lab Test Result" Visible="false" OnClick="btnShowTestReport_Click" />


            </td>

        </tr>

    </table>
     

</asp:Content>
