﻿<%@ Page Language="C#" validaterequest="false"  AutoEventWireup="true" CodeBehind="LabTestInvestProfileUserDefined.aspx.cs" Inherits="HMS.Laboratory.LabTestInvestProfileUserDefined" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
              <asp:hiddenfield id="hidPageName" runat="server" />

    <div>
         <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="90%">
                <tr>
                    <td>
                        <asp:label id="lblStatus" runat="server" forecolor="red" style="letter-spacing: 1px;font-weight:bold;" cssclass="label"></asp:label>
                    </td>
                </tr>

    </table>

        <table width="90%">
                <tr>
                    <td style="text-align: left; width: 50%;" class="PageHeader">
                        

                    </td>
                    <td style="text-align: right; width: 50%;">
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save" OnClick="btnSave_Click" />
                    
                    </td>
                </tr>
     </table>
    <table width="100%" border="0" cellpadding="5" cellspacing="5">
                <tr>
                    <td valign="top" style="width: 47%;">

                        <asp:textbox id="txtContent" height="400" maxlength="8000" textmode="MultiLine" runat="server" class="mceEditor" style="resize: none;" />



                    </td>

                    </tr>

        </table>
    </div>
    </form>
</body>
    <!-- Start for TinyMCE -->

<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="../tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        // General options textareas
        mode: "textareas",
        theme: "advanced",
        editor_selector: "mceEditor",
        editor_deselector: "mceNoEditor",
        relative_urls: false,
        remove_script_host: false,
        // document_base_url : "http://localhost:1803/DIHWebsite/",
        document_base_url: '<% =getURL() %> ',
        //       setup : function(ed) {
        //          ed.onKeyUp.add(function(ed, e) {   
        //               var strip = (tinyMCE.activeEditor.getContent()).replace(/(<([^>]+)>)/ig,"");
        //               var text =   strip.length + " Characters"
        //        tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), text);   
        //         });},

        plugins: "safari,pagebreak,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,

        // Example content CSS (should be your site CSS)
        content_css: "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });

</script>

<!-- End for TinyMCE -->
</html>

