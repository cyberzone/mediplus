﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LabTestProfileMasterPopup.aspx.cs" Inherits="HMS.Laboratory.LabTestProfileMasterPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lab Test Profile Master Popup</title>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
       <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        function passvalue(Code, Name) {

            if (document.getElementById('hidPageName').value == 'LabProfileMaster') {
                opener.location.href = "LabTestProfileMaster.aspx?MenuName=Laboratory&ProfileID=" + Code;
            }

            window.parent.close();
            window.parent.parent.close();
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
            <input type="hidden" id="hidCtrlName" runat="server" />
         <asp:hiddenfield id="hidPageName" runat="server" />
    <div>
              <div style="padding-top: 0px; padding-left: 0px; width: 700px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table width="100%">
                    <tr>
                         <td class="lblCaption1" style="width: 100px;">Search
                        </td>
                        <td>
                           <asp:TextBox ID="txtSearch" runat="server"  CssClass="TextBoxStyle" Width="300px" MaxLength="10"    AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                        </td>
                       
                    </tr>
                </table>
            </div>
            <asp:GridView ID="gvPopupGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="700">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("LTPM_TEST_PROFILE_ID")  %>','<%# Eval("LTPM_DESCRIPTION")%>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("LTPM_TEST_PROFILE_ID") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("LTPM_TEST_PROFILE_ID")  %>','<%# Eval("LTPM_DESCRIPTION")%>')">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("LTPM_DESCRIPTION") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("LTPM_TEST_PROFILE_ID")  %>','<%# Eval("LTPM_DESCRIPTION")%>')">
                                <asp:Label ID="Label3" CssClass="label" runat="server" Width="70px" runat="server" style="text-align: right;padding-right:5px;"  Text='<%# Bind("LTPM_STATUS") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("LTPM_TEST_PROFILE_ID")  %>','<%# Eval("LTPM_DESCRIPTION")%>')">
                                <asp:Label ID="Label4" CssClass="label" runat="server" Text='<%# Bind("LTPM_UNIT") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
    </form>
</body>
</html>
