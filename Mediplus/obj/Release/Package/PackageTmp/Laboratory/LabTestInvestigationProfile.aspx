﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="LabTestInvestigationProfile.aspx.cs" Inherits="HMS.Laboratory.LabTestInvestigationProfile" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>


    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ServicePopup(CtrlName, strValue) {

            var win = window.open("../HMS/Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue + "&Type=L", "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();


            return true;

        }


        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName, Fees) {

            document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
            document.getElementById("<%=txtServName.ClientID%>").value = ServName;
            document.getElementById("<%=txtFee.ClientID%>").value = Fees;



        }
        function ServicePopup(CtrlName, strValue) {

            var win = window.open("../HMS/Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue + "&Type=L", "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");

            win.focus();


            return true;

        }





        function InvestProfileMasterShow(CtrlName, strValue) {


            var win = window.open("LabTestInvestProfilePopup.aspx?PageName=LabInvestProfileMaster&CtrlName=" + CtrlName + "&Value=" + strValue, "InvestProfileMasterShow", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }

        function ShowUserDefined() {

            var InvestProfileID;
            var Description;
            InvestProfileID = document.getElementById("<%=txtInvProfileID.ClientID%>").value;
            Description = document.getElementById("<%=txtDescription.ClientID%>").value;


            var win = window.open("LabTestInvestProfileUserDefined.aspx?PageName=LabInvestProfileMaster&InvestProfileID=" + InvestProfileID + "&Description=" + Description, "ShowUserDefined", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table style="width: 80%;">
        <tr>
            <td align="left">
                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Investigation Profile"></asp:Label>
            </td>
            <td align="right">
                <asp:UpdatePanel ID="UpPanalSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnClear" runat="server" class="button gray small" Text="Clear " OnClick="btnClear_Click" Width="100px" />
                        <asp:Button ID="btnSave" runat="server" class="button red small" Text="Save Record" OnClick="btnSave_Click" Width="100px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanalSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>


    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="90%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 80%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <table width="100%" border="0" cellpadding="3" cellspacing="3">
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Code
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInvProfileID" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtInvProfileID_TextChanged" ondblclick="return InvestProfileMasterShow('txtInvProfileID',this.value);"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="txtInvProfileID" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Description
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" Width="300px" Height="20px" CssClass="TextBoxStyle" ondblclick="return InvestProfileMasterShow('txtDescription',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Status
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" Style="width: 155px" class="TextBoxStyle">
                                <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                <asp:ListItem Value="I">InActive</asp:ListItem>

                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Remarks
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" Width="590px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Color
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpColor" runat="server" Style="width: 155px" class="TextBoxStyle">
                                <asp:ListItem Value="" Selected="True">---Select---</asp:ListItem>
                                <asp:ListItem Value="Gold">Gold</asp:ListItem>
                                <asp:ListItem Value="Gray">Gray</asp:ListItem>
                                <asp:ListItem Value="Green">Green</asp:ListItem>
                                <asp:ListItem Value="Pink">Pink</asp:ListItem>
                                <asp:ListItem Value="Purple">Purple</asp:ListItem>
                                <asp:ListItem Value="Red">Red</asp:ListItem>
                                <asp:ListItem Value="Yellow">Yellow</asp:ListItem>

                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label4" runat="server" CssClass="label" Text="ServiceCode"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServCode" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle" ondblclick="return ServicePopup('txtServCode',this.value);" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label5" runat="server" CssClass="label" Text="Name"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServName" runat="server" Width="300px" Height="20px" CssClass="TextBoxStyle" ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label6" runat="server" CssClass="label" Text="Fee"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFee" runat="server" Width="153px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>

                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label7" runat="server" CssClass="label" Text="Caption"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCaption" runat="server" Width="300px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Usage
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpUsage" runat="server" CssClass="TextBoxStyle" Width="155px" BorderWidth="1px">
                                <asp:ListItem Value="N" Selected="True">Group</asp:ListItem>
                                <asp:ListItem Value="U">Individual</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelType" runat="server" CssClass="lblCaption1" Text="Report Type"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpReportType" runat="server" CssClass="TextBoxStyle" Width="155px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpReportType_SelectedIndexChanged">
                                <asp:ListItem Value="N" Selected="True">Normal</asp:ListItem>
                                <asp:ListItem Value="U">User Defiend</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td></td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <input type="button" id="btnShowUserDefined" runat="server" style="display: none;" class="button red small" onclick="return ShowUserDefined();" value="User Defined Format" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Diagnostic Serv
                                   
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpDiagnosticService" runat="server" Style="width: 150px" class="TextBoxStyle">
                            </asp:DropDownList>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Method
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMethod" runat="server" Width="615px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtMethod" MinimumPrefixLength="1" ServiceMethod="GetMethod"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px; width: 120px;">Test Category
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpReportCategory" runat="server" CssClass="TextBoxStyle" Width="155px" BorderWidth="1px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px; width: 100px; vertical-align: top;">Additional Reference
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtAddReference" runat="server" Width="615px" Height="50px" CssClass="TextBoxStyle" TextMode="MultiLine"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px; width: 120px;">Test Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpTestType" runat="server" CssClass="TextBoxStyle" Width="155px" BorderWidth="1px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <table width="80%" border="0" cellpadding="3" cellspacing="3">
        <tr>
            <td class="lblCaption1">Test Code & Description
            </td>
            <td></td>

        </tr>
        <tr>
            <td width="620px">

                <asp:TextBox ID="txtTestName" runat="server" Width="600px" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtTestName" MinimumPrefixLength="1" ServiceMethod="GetTestProfMaster"
                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                </asp:AutoCompleteExtender>

            </td>
            <td>

                <asp:Button ID="btnAdd" runat="server" CssClass="button red small" Text="Add" OnClick="btnAdd_Click" />

            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvInvProfileTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" OnRowDataBound="gvInvProfileTrans_RowDataBound">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>

                        <asp:TemplateField HeaderText=" Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton9" CssClass="lblCaption1" runat="server" OnClick="TestProfTransEdit_Click">
                                    <asp:Label ID="lblgvTestProfTransProfID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LIPT_INVESTIGATION_PROFILE_ID") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="LBLgvInvProfileTransCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LIPT_TEST_PROFILE_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton10" CssClass="lblCaption1" runat="server" OnClick="TestProfTransEdit_Click">
                                    <asp:Label ID="lblgvInvProfileTranssDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LIPT_TEST_DETAILS") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                            <ItemTemplate>

                                <asp:TextBox ID="txtgvInvProfileTransMachineCode" CssClass="TextBoxStyle" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LIPT_MACHINE_CODE") %>' AutoPostBack="true" OnTextChanged="gvInvProfileTrans_TextChanged"></asp:TextBox>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                            <ItemTemplate>

                                <asp:TextBox ID="txtgvInvProfileTransUnit" CssClass="TextBoxStyle" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LIPT_UNIT") %>' AutoPostBack="true" OnTextChanged="gvInvProfileTrans_TextChanged"></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Exam Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                            <ItemTemplate>

                                <asp:Label ID="lblgvInvProfileTransExam" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LIPT_EXAM_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpgvInvProfileTransExam" runat="server" Style="width: 155px" class="TextBoxStyle">
                                    <asp:ListItem Value="" Selected="True">---Select---</asp:ListItem>
                                    <asp:ListItem Value="Macroscopic">Macroscopic </asp:ListItem>
                                    <asp:ListItem Value="Microscopic">Microscopic</asp:ListItem>

                                </asp:DropDownList>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                    OnClick="Delete_Click" />&nbsp;&nbsp;
                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>



</asp:Content>

