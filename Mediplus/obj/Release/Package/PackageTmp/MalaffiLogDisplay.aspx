﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="MalaffiLogDisplay.aspx.cs" Inherits="Mediplus.MalaffiLogDisplay" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="Styles/style.css" rel="Stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 85%">
        <table>
            <tr>
                <td class="PageHeader">Malaffi Log Viewer
                </td>
            </tr>
        </table>

        <table>
            <tr>

                <td class="TDStyle">Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFromDate" runat="server" Width="95%" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">To Date

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcToDate" runat="server" Width="95%" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">File No

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFileNo" runat="server" Width="95%" CssClass="TextBoxStyle"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="TDStyle">Status

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpSrcStatus" runat="server" CssClass="TextBoxStyle" Width="95%">
                                <asp:ListItem Text="--- All ---" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="PENDING" Value="PENDING"></asp:ListItem>
                                <asp:ListItem Text="ERROR" Value="ERROR"></asp:ListItem>
                                <asp:ListItem Text="UPLOADED" Value="UPLOADED"></asp:ListItem>
                                <asp:ListItem Text="FILE_CREATED" Value="FILE_CREATED"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSearch" runat="server" CssClass="button red small" OnClick="btnSearch_Click" Text="Search" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
        </table>

        <div style="padding-top: 0px; width: 97%; height: 450px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvMalaffiLog" runat="server"
                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvMalaffiLog_Sorting" OnRowDataBound="gvMalaffiLog_RowDataBound"
                        EnableModelValidation="True" Width="100%" BorderStyle="None" GridLines="Horizontal" CellPadding="2" CellSpacing="0">
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                        OnClick="Edit_Click" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DATE" SortExpression="HMHM_CREATED_DATE" ItemStyle-Width="120px" HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitID" CssClass="label" runat="server" Text='<%# Bind("HMHM_VISIT_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblEMR_ID" CssClass="label" runat="server" Text='<%# Bind("HMHM_EMR_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblMessageID" CssClass="label" runat="server" Text='<%# Bind("HMHM_MESSAGE_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblMessageType" CssClass="label" runat="server" Text='<%# Bind("HMHM_MESSAGE_TYPE") %>' Visible="false"></asp:Label>

                                     <asp:Label ID="lblCreatedDate" CssClass="label" runat="server" Text='<%# Bind("HMHM_CREATED_DATEDesc") %>'></asp:Label> &nbsp;
                                        <asp:Label ID="lblCreatedTime" CssClass="label" runat="server" Text='<%# Bind("HMHM_CREATED_DATETimeDesc") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FILE NO" SortExpression="Hmhm_PT_ID" ItemStyle-Width="70px" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblPatientID" CssClass="label" runat="server" Text='<%# Bind("HMHM_PT_ID") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="HL7 FILE NAME" SortExpression="HMHM_FILE_NAME">
                                <ItemTemplate>

                                    <asp:Label ID="lblFileName" CssClass="label" runat="server" Text='<%# Bind("HMHM_FILE_NAME") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MESSAGE CODE" SortExpression="HMHM_MESSAGE_CODE">
                                <ItemTemplate>

                                    <asp:Label ID="lblMessageCode" CssClass="label" runat="server" Text='<%# Bind("HMHM_MESSAGE_CODE") %>'></asp:Label>


                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MESSAGE DESC" SortExpression="HMHM_MESSAGE_DESC">
                                <ItemTemplate>

                                    <asp:Label ID="lblMessageDesc" CssClass="label" runat="server" Text='<%# Bind("HMHM_MESSAGE_DESC") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="RAD TRANS ID" SortExpression="HMHM_MESSAGE_TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblRadTestReportID" CssClass="label" runat="server" Text='<%# Bind("HMHM_RAD_TEST_REPORT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="LAB TRANS ID" SortExpression="HMHM_MESSAGE_TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblLabTestReportID" CssClass="label" runat="server" Text='<%# Bind("HMHM_LAB_TEST_REPORT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" SortExpression="HMHM_UPLOAD_STATUS">
                                <ItemTemplate>

                                    <asp:Label ID="lblUploadStatus" CssClass="label" runat="server" Text='<%# Bind("HMHM_UPLOAD_STATUS") %>' Visible="false" ></asp:Label>
                                    <asp:DropDownList ID="drpUploadStatus" runat="server" CssClass="TextBoxStyle" Width="95%" AutoPostBack="true" OnSelectedIndexChanged="drpUploadStatus_SelectedIndexChanged">
                                        <asp:ListItem Text="PENDING" Value="PENDING"></asp:ListItem>
                                        <asp:ListItem Text="ERROR" Value="ERROR"></asp:ListItem>
                                        <asp:ListItem Text="UPLOADED" Value="UPLOADED"></asp:ListItem>
                                        <asp:ListItem Text="FILE_CREATED" Value="FILE_CREATED"></asp:ListItem>
                                    </asp:DropDownList>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="RETURN MESSAGE">
                                <ItemTemplate>

                                    <asp:Label ID="lblReturnMessage" CssClass="label" runat="server" Text='<%# Bind("HMHM_RETURN_MESSAGE") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

    </div>
</asp:Content>
