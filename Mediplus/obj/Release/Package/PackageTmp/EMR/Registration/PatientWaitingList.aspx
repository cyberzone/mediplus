﻿<%@ Page Title=""  MasterPageFile="~/Site2.Master"  Language="C#" AutoEventWireup="true" CodeBehind="PatientWaitingList.aspx.cs" Inherits="Mediplus.EMR.Registration.PatientWaitingList" %>


<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register Src="~/EMR/VisitDetails/NursingOrderList.ascx" TagPrefix="UC1" TagName="NursingOrderList" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

     
     
       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

  <script language="javascript" type="text/javascript">


      function OnlyNumeric(evt) {
          var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
          if (chCode >= 48 && chCode <= 57 ||
               chCode == 46) {
              return true;
          }
          else

              return false;
      }

      function ShowStartProcess(vStatus) {

          var EMR_START_PROC_CONFIRM = '<%= Convert.ToString(Session["EMR_START_PROC_CONFIRM"]) %>'
              if (vStatus == 'Waiting' && EMR_START_PROC_CONFIRM != "0") {
                  var isSrtProc = window.confirm('Do you want to start the process? ');
                  document.getElementById('<%=hidSrtProc.ClientID%>').value = isSrtProc;

            }

            if (vStatus == 'Waiting' && EMR_START_PROC_CONFIRM == "0") {
                document.getElementById('<%=hidSrtProc.ClientID%>').value = 'true';
            }
            return true;
        }
        /*
        setInterval(function () {
            window.location.reload();
        }, 30000);
        */
    </script>

    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
            // alert(today);
        });
    </script>

    <script language="javascript" type="text/javascript">
        function ShowDrWiseIncome() {
            var Report = "";
            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            var DrId = '<%= Convert.ToString(Session["User_Code"])%>'

            var FileDesc = document.getElementById('<%=hidFileDesc.ClientID%>').value


            if (FileDesc == "ALNOOR") {
                Report = "HmsDrIncome.rpt";
            }
            else {

                Report = "HmsStaffwiseIncome.rpt";
            }
            //  Report = "HmsDailyDetailSummary.rpt";

            var Criteria = " 1=1 ";
            // Criteria += ' AND {HMS_INVOICE_MASTER.HIM_STATUS}=\'Open\'' ;

            if (DrId != "") {
                if (FileDesc == "ALNOOR") {
                    Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
                }
                else if (FileDesc == "MAMPILLY") {
                    Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
                }
                else {
                    Criteria += ' AND {HMS_STAFF_WISE_INCOME.HIM_DR_CODE}=\'' + DrId + '\'';
                }

            }

            if (FileDesc == "ALNOOR") {
                Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

            }
            else if (FileDesc == "MAMPILLY") {
                Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

            }
            else {
                Criteria += ' AND  {HMS_STAFF_WISE_INCOME.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_STAFF_WISE_INCOME.HIM_DATE}<=date(\'' + Date2 + '\')'
            }

            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function ShowDrDeptWiseIncomeSummary() {
            var Report = "";
            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            var DrId = '<%= Convert.ToString(Session["User_Code"])%>'

         Report = "HmsMonthlyDeptSummary.rpt";


         var Criteria = " 1=1 ";


         if (DrId != "") {

             Criteria += ' AND {VWDEPARTMENTWISESUMMARY.HIM_DR_CODE}=\'' + DrId + '\'';
         }


         Criteria += ' AND  {VWDEPARTMENTWISESUMMARY.HIM_DATE}>=date(\'' + Date1 + '\') AND  {VWDEPARTMENTWISESUMMARY.HIM_DATE}<=date(\'' + Date2 + '\')'


         var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



     }

     function ShowDrIncome() {
         var Report = "";
         var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
         var arrFromDate = dtFrom.split('/');
         var Date1;
         if (arrFromDate.length > 1) {

             Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
         }


         var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
         var arrToDate = dtTo.split('/');
         var Date2;
         if (arrToDate.length > 1) {

             Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
         }



         var DrId = '<%= Convert.ToString(Session["User_Code"])%>'



         Report = "HmsDrIncome.rpt";
         //  Report = "HmsDailyDetailSummary.rpt";

         var Criteria = " 1=1 ";
         // Criteria += ' AND {HMS_INVOICE_MASTER.HIM_STATUS}=\'Open\'' ;

         if (DrId != "") {

             Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
             // Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
         }


         Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

         //   Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'
         var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



     }


     function ShowAppointment() {


         var DrId = '<%= Convert.ToString(Session["User_Code"])%>'


         var win = window.open('AppointmentEMRDr.aspx?PageName=AppointmentEMRDr&DrID=' + DrId, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowReports() {

         var win = window.open('../MedicalReport/Index.aspx?PageName=WaitingLIst', 'MedicalReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowResults() {

         var win = window.open('../VisitDetails/ResultsViewPopup.aspx?PageName=WaitingLIst', 'VisitDetails', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowPatientList() {


         var win = window.open('PatientPopup.aspx?PageName=WaitingLIst', 'PatientList', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

     }

     function ShowDoctorToken() {


         var win = window.open('DoctorTokenPopup.aspx?PageName=WaitingLIst', 'DoctorToken', 'menubar=no,left=100,top=80,height=650,width=1075,scrollbars=1')

     }

     function ShowNursingOrder() {


         var win = window.open('../VisitDetails/NursingOrderPopup.aspx?PageName=WaitingLIst', 'NursingOrder', 'menubar=no,left=100,top=80,height=600,width=850,scrollbars=1')

     }

     function ShowPharmacyInternal() {


         var win = window.open('../Patient/PharmacyInternalPopup.aspx?PageName=WaitingLIst', 'NursingOrder', 'menubar=no,left=100,top=80,height=600,width=950,scrollbars=1')

     }


     function ShowCommomSMS() {


         var win = window.open('CommonSMS.aspx?PageName=WaitingLIst', 'CommonSMS', 'menubar=no,left=100,top=80,height=600,width=850,scrollbars=1')

     }

     function BindTokenNo(TokenNo, PT_ID) {

         var gvcheck = document.getElementById('<%=gvGridView.ClientID%>');
         var i;
         for (i = 2; i < gvcheck.rows.length - 1; i++) {

             var inputs = gvcheck.rows[i].getElementsByTagName('input');
             // inputs[0].checked = false;

         }

         document.getElementById('<%=hidTokenPTID.ClientID%>').value = PT_ID;

         document.getElementById('<%=txtSelectedToken.ClientID%>').value = TokenNo;


     }

     function ShowEMREditDisplayPopup() {

         document.getElementById("divEMREditDisplay").style.display = 'block';


     }

     function HideEMREditDisplayPopup() {

         document.getElementById("divEMREditDisplay").style.display = 'none';

     }

     function ShowNursingListPopup() {
         document.getElementById("divOverlayNurseOrder").style.display = 'block';
     }



    </script>

     <style>
           .Overlay
        {
            position: fixed;
            z-index: 999;
            width: 100%;
            height: 100%;
            background-color: black;
            top: 0;
            left: 0;
            background-color: rgba(0,0,0,.75);
        }
     </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <asp:HiddenField ID="hidLocalDate" runat="server" />

    <input type="hidden" id="hidSrtProc" runat="server" value="false" />
    <input type="hidden" id="hidFileDesc" runat="server" />
    <input type="hidden" id="hidTokenPTID" runat="server" />

 
    <table width="80%">
        <tr>
            <td class="lblCaption1">Date
            </td>
            <td>
                <asp:TextBox ID="txtFromDate" runat="server" Width="150px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

            </td>
            <td class="lblCaption1">To Date

            </td>
            <td>
                <asp:TextBox ID="txtToDate" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


            </td>
            <td class="style2">
                <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle">
                    <asp:ListItem Text="Waiting" Value="W" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Completed" Value="C"></asp:ListItem>
                    <asp:ListItem Text="In Process" Value="P"></asp:ListItem>
                    <asp:ListItem Text="Verified" Value="V"></asp:ListItem>
                    <asp:ListItem Text="Invoiced" Value="I"></asp:ListItem>
                    <asp:ListItem Value="">All</asp:ListItem>

                </asp:DropDownList>
                 <asp:DropDownList ID="drpEMRClinicStatus" runat="server" CssClass="TextBoxStyle"  Visible="false">
            <asp:ListItem Text="Waiting" Value="Waiting" Selected="True"></asp:ListItem>
            <asp:ListItem Text="With Doctor" Value="With Doctor"></asp:ListItem>
            <asp:ListItem Text="Waiting Lab" Value="Waiting Lab"></asp:ListItem>
            <asp:ListItem Text="PT Come Back" Value="PT Come Back"></asp:ListItem>
            <asp:ListItem Text="Investigation TO DO" Value="Investigation TO DO"></asp:ListItem>
            <asp:ListItem Text="Seen" Value="Seen"></asp:ListItem>
            <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
            <asp:ListItem Text="Billed" Value="Billed"></asp:ListItem>
            <asp:ListItem Value="">All</asp:ListItem>
        </asp:DropDownList>
            </td>
            <td>

                <asp:Button ID="btnRefresh" runat="server" CssClass="button red small" Width="80px"
                    Text="Refresh" OnClick="btnRefresh_Click" />
                <asp:Button ID="btnTodayDate" runat="server" CssClass="button gray small" Width="80px"
                    Text="Today" OnClick="btnTodayDate_Click" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">File Number
            </td>
            <td>
                <asp:TextBox ID="txtSrcFileNo" runat="server"  Width="150px" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
            </td>
            <td class="lblCaption1">Patient Name
            </td>
            <td>
                <asp:TextBox ID="txtSrcName" runat="server"   Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
            </td>
            <td class="lblCaption1">Mobile Number
            </td>
            <td>
                <asp:TextBox ID="txtSrcMobile1" runat="server"   Width="200px" CssClass="TextBoxStyle"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Patient Type
            </td>
            <td>
                <asp:DropDownList ID="drpPatientType" runat="server" CssClass="TextBoxStyle" Width="150px" >
                    <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                    <asp:ListItem Value="CA">Cash</asp:ListItem>
                    <asp:ListItem Value="CR">Credit</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="lblCaption1">Company 
            </td>
            <td>
                <asp:DropDownList ID="drpSrcCompany"   CssClass="TextBoxStyle" runat="server" Width="150px" OnSelectedIndexChanged="drpSrcCompany_SelectedIndexChanged">
                </asp:DropDownList>

            </td>
            <td class="lblCaption1">
                <asp:Label ID="lblDoctor" runat="server" CssClass="lblCaption1"
                    Text="Doctor" Visible="false"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpDoctor" CssClass="TextBoxStyle" runat="server"   Width="202px" Visible="false">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table width="80%">
        <tr>
            <td align="right" class="lblCaption1">
                <input type="button" style="width:15px;height:15px;border:none;background-color:#FFA500" />
                >&nbsp;<asp:Label ID="lblColorChangeTime1" runat="server"  class="lblCaption1"  ></asp:Label>&nbsp;Minutes
                <input type="button" style="width:15px;height:15px;border:none;background-color:#FF0000" />
                >&nbsp;<asp:Label ID="lblColorChangeTime2" runat="server"  class="lblCaption1"  ></asp:Label>&nbsp;Minutes&nbsp;
            </td>
        </tr>
    </table>
    <table width="80%">
        <tr>
            <td class="lblCaption1">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" Width="100%" ShowFooter="true"
                            AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting" OnRowDataBound="gvGridView_RowDataBound"
                            EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200" GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <FooterStyle CssClass="GridHeader_Gray" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                               <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemTemplate>
                                        <input type="button" id="btngvToken"  style="width:20px;height:20px;cursor:pointer;"  onclick=<%# "BindTokenNo('" + Eval("HPV_DR_TOKEN") + "','" + Eval("HPV_PT_Id") + "')" %> title="Select Token"  />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File No." SortExpression="HPV_PT_Id" FooterText="File No" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click" OnClientClick='<%# Eval("HPV_EMR_STATUSDesc", "javascript:ShowStartProcess(&#39;{0}&#39;);") %>'>
                                            <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                            <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                            <asp:Label ID="lblDeptName" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                            <asp:Label ID="lblRefDrCode" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_REFERRAL_TO") %>'></asp:Label>
                                            <asp:Label ID="lblgvVisit_PhotoID" CssClass="GridRow" Width="250px" runat="server" Text='<%# Bind("HPV_PHOTO_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblgvPTType" CssClass="GridRow" Width="250px" runat="server" Text='<%# Bind("HPV_PT_TYPE") %>' Visible="false"></asp:Label>
                                              <asp:Label ID="lblStatusValue" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_EMR_STATUS") %>'  Visible="false"></asp:Label>

                                            <asp:Label ID="lblPatientId" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Patient Name" SortExpression="HPV_PT_NAME" FooterText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPTName" CssClass="lblCaption1" runat="server" OnClick="Edit_Click" OnClientClick='<%# Eval("HPV_EMR_STATUSDesc", "javascript:ShowStartProcess(&#39;{0}&#39;);") %>'>
                                            <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Ins. Status" Visible="false"  FooterText="Ins. Status" SortExpression="HPV_INS_VERIFY_STATUS" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblInsStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_INS_VERIFY_STATUS") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sex" FooterText="Sex" SortExpression="HPV_AGE" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblSex" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_SEX") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age" FooterText="Age" SortExpression="HPV_AGE" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAge" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_AGE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblAgeDesc" CssClass="GridRow" runat="server" Text='<%# Bind("AgeDesc") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nationality" FooterText="Nationality" SortExpression="HPV_NATIONALITY" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_NATIONALITY") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date Time" FooterText="Date Time" SortExpression="HPV_Date" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                      
                                        <asp:Label ID="lblVisitDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateDesc") %>'></asp:Label>
                                        <asp:Label ID="lblVisitDateTime" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateTimeDesc") %>'></asp:Label>
                                        <asp:Label ID="lblVisitTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_TIME") %>' Visible="false"></asp:Label>
                                          <asp:Label ID="lblPTWaitingTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("PTWaitingTime") %>' Visible="false"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comp Name" FooterText="Comp Name" SortExpression="HPV_COMP_NAME" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblCompID" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblCompName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Priority" FooterText="Priority" SortExpression="HPV_VISIT_TYPE" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblVisitType" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_VISIT_TYPEDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Token" FooterText="Token" SortExpression="HPV_DR_TOKEN" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblToken" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Status" FooterText="Status" SortExpression="HPV_PT_TYPEDesc" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                          <asp:Label ID="lblType" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_STATUSDesc") %>'  ></asp:Label>
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Clinic Status" FooterText="Clinic Status" SortExpression="HPV_EMR_CLINIC_STATUS" HeaderStyle-HorizontalAlign="Left"  Visible ="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblClinicStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_CLINIC_STATUSDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Doctor" Visible="false" FooterText="Doctor" SortExpression="HPV_DR_NAME" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDrID" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_DR_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                

                                  <asp:TemplateField HeaderText="Appoint.Status" FooterText="Appoint.Status" SortExpression="AppointStatus" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblAppntStatus" CssClass="GridRow" runat="server" Text='<%# Bind("AppointStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderText="User" FooterText="User" SortExpression="HPV_CREATED_USER" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="lblUser" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_CREATED_USER") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" FooterText="" Visible="false" SortExpression="HPV_CREATED_USER" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                         <asp:ImageButton ID="EditEMR" runat="server" ToolTip="Edit" ImageUrl="~/Images/icon_edit.png" Height="20px" Width="20px"
                                        OnClick="EditEMR_Click" />&nbsp;&nbsp;
                                     </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>


                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <table width="80%">
        <tr>
            <td align="left">
                <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                    Text="Selected Records :"></asp:Label>
                &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true" Font-Size="Medium"></asp:Label>
                &nbsp;&nbsp;
                                    <asp:Label ID="Label3" runat="server" CssClass="label" Font-Bold="true"
                                        Text="Completed:"></asp:Label>
                <asp:Label ID="lblTotalCompleted" runat="server" CssClass="label" Font-Bold="true" ForeColor="Green" Font-Size="Medium"></asp:Label>&nbsp;&nbsp;

                                     <asp:Label ID="Label5" runat="server" CssClass="label" Font-Bold="true"
                                         Text="In Process:"></asp:Label>
                <asp:Label ID="lblTotalInProcess" runat="server" CssClass="label" Font-Bold="true" ForeColor="Brown" Font-Size="Medium"> </asp:Label>&nbsp;&nbsp;

                                    <asp:Label ID="Label1" runat="server" CssClass="label" Font-Bold="true"
                                        Text="Waiting:"></asp:Label>

                <asp:Label ID="lblTotalWaiting" runat="server" CssClass="label" Font-Bold="true" ForeColor="Red" Font-Size="Medium"></asp:Label>

            </td>
            <td align="right">
                <asp:Label ID="lblNursingOrder" runat="server" CssClass="label" Font-Bold="true" Visible="true"
                    Text="Nursing Order :"></asp:Label>
                 <a href="#" onclick="ShowNursingListPopup();" style="text-decoration: none;">
                    <asp:Label ID="lnkNursingOrder" runat="server" CssClass="labelTemp" Text="0" ForeColor="Red" Font-Size="Medium"></asp:Label>
                </a>
               
                 <asp:LinkButton ID="lnkPharmacyInternal" runat="server" Text="0" CssClass="label" Visible="true" ForeColor="Red" Font-Size="Medium" OnClientClick="return ShowPharmacyInternal();" Style="text-decoration: none;"></asp:LinkButton>
            </td>
        </tr>

    </table>
    <table style="width: 80%;">
        <tr>
            <td style="width: 10%;">

                <asp:Button ID="btnDrWiseIncome" runat="server" Text="Income Report" Width="140px" Style="display: block;" CssClass="button gray small" OnClientClick="return ShowDrWiseIncome();" Visible="false" />

            </td>
            <td style="width: 10%;">
                <asp:Button ID="btnReport" runat="server" Text="Medical&Leave Rep." Width="140px" Style="display: block;" CssClass="button gray small" OnClientClick="return ShowReports();" Visible="false" />
            </td>
            <td style="width: 10%;">
                <asp:Button ID="btnResult" runat="server" Text="Result Report" Width="140px" Style="display: block;" CssClass="button gray small" OnClientClick="return ShowResults();" Visible="false" />
            </td>
            <td style="width: 10%;">

                <asp:Button ID="btnAppoint" runat="server" Text="Appointment" Width="140px" Style="display: block;" CssClass="button gray small" value="false" OnClientClick="return ShowAppointment();" Visible="false" />

            </td>
            <td style="width: 10%;">

                <asp:Button ID="btnPatientList" runat="server" Text="Patient List" Width="140px" Style="display: block;" CssClass="button gray small" OnClientClick="return ShowPatientList();" Visible="false" />

            </td>
            <td style="width: 10%;">

                <asp:Button ID="btnUpdateToken" runat="server" Text="Next Token" Width="140px" Style="display: block;" CssClass="button gray small" OnClick="btnUpdateToken_Click" Visible="false" />

            </td>
            <td style="width: 10%;">

                <asp:Button ID="btnDoctorToken" runat="server" Text="Dr.Token List" Width="140px" Style="display: block;" CssClass="button gray small" OnClientClick="return ShowDoctorToken();" Visible="false" />

            </td>
            <td style="width: 30%;"></td>
        </tr>
        <tr>
            <td style="width: 10%;">
                <asp:Button ID="btnSendSMS" runat="server" Text="SMS Sending" Width="140px" Visible="false" CssClass="button gray small" OnClientClick="return ShowCommomSMS();" />
            </td>

        </tr>
        <tr   id="trToken"  runat="server"  visible="false"  >
            
            <td class="lblCaption1" colspan="8" style="width:100%;"> 
                Currrent Token &nbsp;&nbsp;

                 <asp:TextBox ID="txtSelectedToken" runat="server" CssClass="TextBoxStyle" Width="100px" Font-Bold="true"></asp:TextBox>
                <asp:Button ID="btnShowToken" runat="server" Text="Show Token" Width="100px"   CssClass="button gray small" OnClick="btnShowToken_Click" />

            </td>
        </tr>

    </table>
      <div id="divEMREditDisplay" class="Overlay" style="display: none;">
        <div id="div3" class="Overlay" style="display: block;">
            <div id="div2" style="height: 550px; width: 1100px; position: fixed; left: 150px; top: 250px;">
                <img src='<%= ResolveUrl("~/Images/Doctor.png")%>' style="border: none; width: 40px; height: 40px;" />
                <span style="color: white; font-size: 17px;" class="label">
                    <asp:Label ID="lblNursingHeader" runat="server" class="label" Text=" EMR Details " Style="color: white; font-size: 17px;"></asp:Label>
                </span>
                <div id="divPTDtlsPopup" style="display: block; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; position: fixed; left: 150px; top: 300px;">
                    <div id="divHeaderLine" style="height: 1px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px; position: absolute; top: 0px; left: 0px;">
                    </div>
                    <div style="width: 100%; text-align: right; float: right; position: absolute; top: -0px; right: 0px;">
                        <img src='<%= ResolveUrl("~/Images/Close.png")%>' style="height: 25px; width: 25px; cursor: pointer;" onclick="HideEMREditDisplayPopup()" />
                    </div>
                    <input id="btnNursingHeader" runat="server" type="button" class="PageSubHeaderBlue" style="width: 150px;" value="  Edit EMR Dtls. " disabled="disabled" />
                    <img src='<%= ResolveUrl("~/Images/TabBlueCornor.png")%>' style="border: none; height: 20px; width: 37px;" />
                    <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

                        <table width="100%" cellpadding="2" cellspacing="2"   >
                            <tr>
                                <td class="TDStyle"  >File No.
                                </td>
                                 <td style="padding:5px;">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblEditFileNo" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="TDStyle"  >Name
                                </td>
                                 <td style="padding:5px;">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblEditName" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                 <td class="TDStyle" > 
                                </td>
                                 <td  > 
                                </td>
                            </tr>

                            <tr>
                                <td class="TDStyle"  >Date
                                </td>
                                <td style="padding:5px;">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEditEMRDate" runat="server" Width="70px" MaxLength="10" CssClass="labelTemp" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                                                Enabled="True" TargetControlID="txtEditEMRDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtEditEMRDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td class="TDStyle" >Start Date
                                </td>
                                <td style="padding:5px;">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEditEMRSt" runat="server" Width="70px" MaxLength="10" CssClass="labelTemp" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Enabled="True" TargetControlID="txtEditEMRSt" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtEditEMRSt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                            <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                            :
                                      <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                            <asp:DropDownList ID="drpSTAM" runat="server" CssClass="label" Style="font-size: 10px" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                                <asp:ListItem Value="PM">PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="TDStyle"  >End Date

                                </td>
                                <td style="padding:5px;">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEditEMREnd" runat="server" Width="70px" MaxLength="10" CssClass="labelTemp" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                                Enabled="True" TargetControlID="txtEditEMREnd" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtEditEMREnd" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                            <asp:DropDownList ID="drpFinHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>:
                                        <asp:DropDownList ID="drpFinMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                            <asp:DropDownList ID="drpFinAM" runat="server" CssClass="label" Style="font-size: 10px" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                                                <asp:ListItem Value="AM">AM</asp:ListItem>
                                                <asp:ListItem Value="PM">PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                        </table>



                    </div>
                    <table width="100%">
                        <tr>
                            <td style="float: right; text-align: right;">
                                <asp:Button ID="btnEMRUpdate" runat="server" Text="Update" Width="100px" CssClass="button orange small" OnClick="btnEMRUpdate_Click" />
                                 <input type="button" value="Cancel" title="Cancel" class="button gray small" onclick="HideEMREditDisplayPopup()" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div id="divOverlayNurseOrder" class="Overlay" style="display: none;">
        <UC1:NursingOrderList ID="pnlNursingOrderList" runat="server"></UC1:NursingOrderList>
    </div>

    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Enabled="false"></asp:Timer>



    </asp:Content>
    
     

 