﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppointmentEMRDr.aspx.cs" Inherits="Mediplus.EMR.Registration.AppointmentEMRDr" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title>Appointment</title>
     <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function PatientPopup1(AppId, strTime, DRId, PatientID) {
            var strDate;
            strDate = document.getElementById('txtFromDate').value;
            var win = window.open("AppointmentPopup.aspx?PageName=ApptEmrDr&AppId=" + AppId + "&Date=" + strDate + "&Time=" + strTime + "&DrID=" + DRId + "&PatientID=" + PatientID, "newwin", "top=200,left=270,height=450,width=750,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function NewAppointmentPopup() {
            var strDate;
            strDate = document.getElementById('txtFromDate').value;
            var DRId;
            DRId = document.getElementById('hidDrID').value;
            var PatientID;
            PatientID = document.getElementById('hidPatientID').value;

            var win = window.open("AppointmentPopup.aspx?PageName=ApptEmrDr&AppId=0&Date=" + strDate + "&Time=0&DrID=" + DRId + "&PatientID=" + PatientID, "newwin", "top=200,left=270,height=450,width=700,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function ShowReport(DrID) {

            var win = window.open("AppointmentReportEMRDr.aspx?PageName=ApptEmrDr&DrID=" + DrID, "newwin", "top=200,left=270,height=370,width=700,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();

        }
    </script>

    <style type="text/css">
        .Header {
            font: bold 11px/100% Arial;
            background-color: #4fbdf0;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            width: 250px;
            border-color: black;
        }
         .Header1 {
            font: bold 11px/100% Arial;
            background-color: #4fbdf0;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            width: 100%;
            border-color: black;
        }
        .RowHeader {
            font: bold 11px/100% Arial;
            height: 15px;
            text-align: center;
            border-color: black;
        }

        .Content {
            font: 11px/100% Arial;
            text-decoration: none;
            color: black;
            text-align: center;
            vertical-align: middle;
        }
    </style>
</head>
<body  >
    <form id="form1" runat="server">
        <div>
              <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
     <input type="hidden" id="hidPermission" runat="server" value="9" />
     <input type="hidden" id="hidDrID" runat="server"  />
     <input type="hidden" id="hidPatientID" runat="server"  />

    <table >
        <tr>
            <td class="PageHeader">Appointment
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="90%">
        <tr>
            <td style="width: 170px;">
                <asp:HiddenField ID="hidStatus" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <contenttemplate>
                 <asp:Button ID="btnNewApp" runat="server" style="padding-left:5px;padding-right:5px;width:50px" Text="New" CssClass="button orange small"     OnClientClick="return NewAppointmentPopup()" />
                     </contenttemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 50px;" class="label">Date:
            </td>
            <td align="left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" height="22px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="red" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <asp:calendarextender id="TextBox1_CalendarExtender" runat="server" 
                    enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                </asp:calendarextender>
            </td>
            
            <td align="right">
                <asp:RadioButtonList ID="radAppStatus" runat="server" Font-Bold="true" CssClass="label" AutoPostBack="true" Width="500px" RepeatDirection="Horizontal" OnSelectedIndexChanged="radAppStatus_SelectedIndexChanged">
                    <asp:ListItem Text="Appointment List" Value="A" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Waiting List" Value="Waiting"></asp:ListItem>
                    <asp:ListItem Text="Cancelled Appointment" Value="Cancelled"></asp:ListItem>
                     <asp:ListItem Text="Not Arrived" Value="Not Arrived"></asp:ListItem>
                </asp:RadioButtonList>

            </td>
            <td>
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <contenttemplate>
                        <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="button orange small" Width="100px"  OnClick="btnReport_Click"   />
                     </contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
   
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <%=strData%>

                </td>
            </tr>

        </table>
     
    
    <br />
  </div>
    </form>
</body>
</html>
