﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientProfile.ascx.cs" Inherits="Mediplus.EMR.Registration.PatientProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">


        function ShowPTDtlsPopup() {
            document.getElementById("divPatientProfile").style.display = 'block';


        }

        function HidePTDtlsPopup() {
            document.getElementById("divPatientProfile").style.display = 'none';


        }

</script>



  <div id="divPatientProfile" class="Overlay" style="display: none;">
        <div id="div3" class="Overlay" style="display: block;">
            <div id="div2" style="height: 550px; width: 1100px; position: fixed; left: 150px; top: 250px;">
                <img src='<%= ResolveUrl("~/Images/Doctor.png")%>' style="border: none; width: 40px; height: 40px;" />
                <span style="color: white; font-size: 17px;" class="label">
                    <asp:Label ID="lblNursingHeader" runat="server" class="label" Text=" EMR Details " Style="color: white; font-size: 17px;"></asp:Label>
                </span>
                <div id="divPTDtlsPopup" style="display: block; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; position: fixed; left: 150px; top: 300px;">
                    <div id="divHeaderLine" style="height: 1px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px; position: absolute; top: 0px; left: 0px;">
                    </div>
                    <div style="width: 100%; text-align: right; float: right; position: absolute; top: -0px; right: 0px;">
                        <img src='<%= ResolveUrl("~/Images/Close.png")%>' style="height: 25px; width: 25px; cursor: pointer;" onclick="HideEMREditDisplayPopup()" />
                    </div>
                    <input id="btnNursingHeader" runat="server" type="button" class="PageSubHeaderBlue" style="width: 150px;" value="  Edit EMR Dtls. " disabled="disabled" />
                    <img src='<%= ResolveUrl("~/Images/TabBlueCornor.png")%>' style="border: none; height: 20px; width: 37px;" />
                    <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

                        <table width="100%" cellpadding="2" cellspacing="2"   >
                            <tr>
                                <td class="TDStyle"  >File No.
                                </td>
                                 <td style="padding:5px;">
                                   
                                            <asp:Label ID="lblFileNo" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                       
                                </td>
                                <td class="TDStyle"  >Name
                                </td>
                                 <td style="padding:5px;">
                                   
                                            <asp:Label ID="lblName" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                    
                                </td>
                                 <td class="TDStyle" > 
                                </td>
                                 <td  > 
                                </td>
                            </tr>

                            <tr>
                                <td class="TDStyle"  >Date
                                </td>
                                <td style="padding:5px;">
                                    
                                </td>

                                <td class="TDStyle" >Start Date
                                </td>
                                <td style="padding:5px;">
                                   
                                </td>
                                <td class="TDStyle"  >End Date

                                </td>
                                <td style="padding:5px;">
                                   

                                </td>
                            </tr>
                        </table>



                    </div>
                    
                </div>
            </div>
        </div>

    </div>