﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageUpload.ascx.cs" Inherits="Mediplus.EMR.Ophthalmology.ImageUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="DisplayScanImage.ascx"  TagName="DisplayScanImage" TagPrefix="UCDisp" %>

<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
<link href="../../Content/themes/base/jquery-ui.css" />

<style>
    .AutoExtender
    {
        font-family: Verdana, Helvetica, sans-serif;
        font-size: .8em;
        font-weight: normal;
        border: solid 1px #006699;
        line-height: 20px;
        padding: 10px;
        background-color: White;
        margin-left: 10px;
    }

    .AutoExtenderList
    {
        border-bottom: dotted 1px #006699;
        cursor: pointer;
        color: Maroon;
    }

    .AutoExtenderHighlight
    {
        color: White;
        background-color: #006699;
        cursor: pointer;
    }

    #divwidth
    {
        width: 400px !important;
    }

        #divwidth div
        {
            width: 400px !important;
        }
</style>
<style type="text/css">
    .box-header
    {
        border-bottom: 4px solid #f4fbfd;
        height: 70px;
    }

    .box-title
    {
        padding-bottom: 5px;
        border-bottom: 4px solid #92c500;
        float: left;
        font-size: 1.5em;
        color: #2078c0;
    }

    h1, h2, h3, h4, h5, h6
    {
        font-size: 100%;
        font-weight: normal;
        font-family: "Segoe UI", Arial, Helvetica, sans-serif;
    }

    .ImageStyle
    {
        height:400px;
        width:400px;
    }
</style>
<script language="javascript" type="text/javascript">

    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }



    function SaveVal() {
        var label;
        label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";




        }

</script>

<script type="text/javascript">
    function ShowMessage() {
        $("#myMessage").show();
        setTimeout(function () {
            var selectedEffect = 'blind';
            var options = {};
            $("#myMessage").hide();
        }, 2000);
        return true;
    }

</script>

<table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
    <tr>

        <td>
            <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<br />
<fieldset>
    <legend class="lblCaption1" style="font-weight: bold;">Select your scan file</legend>
    <table width="1000px" border="0">

        <tr>

            <td class="lblCaption1">Please upload the image scanned for Ophthalmology
            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Select File 
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="300px">
                            <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                        </asp:Panel>
                    </ContentTemplate>

                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
             <td class="lblCaption1">File Name
            </td>
            <td>
                <asp:TextBox ID="txtFileName" runat="server" CssClass="lable" Width="150px"></asp:TextBox>

            </td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Upload" />
                        <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSave" />

                    </Triggers>
                </asp:UpdatePanel>


            </td>
        </tr>
    </table>
    <div style="padding-left: 20%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Uploaded Successfully
        </div>
    </div>

</fieldset>


<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
             <UCDisp:DisplayScanImage id="ctrlDisplayImage" runat="Server"></UCDisp:DisplayScanImage>
        </td>
    </tr>
</table>
 
