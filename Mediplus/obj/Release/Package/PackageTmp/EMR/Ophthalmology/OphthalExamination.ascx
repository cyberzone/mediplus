﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OphthalExamination.ascx.cs" Inherits="Mediplus.EMR.Ophthalmology.OphthalExamination" %>


<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

<link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
<script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

 
 <style type="text/css">
        .AutoExtender
        {
            font-family: "Segoe UI",arial, "lucida grande", "lucida sans unicode", verdana, tahoma, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: #005c7b;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #02a0e0;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

<fieldset style="width: 95%;">
    <legend class="lblCaption1">Examination</legend>
    <table class="table spacy">
        <tr>
            <td style="width: 20%">
                <asp:CheckBox ID="chkAllNormal" runat="server" Text="All Normal" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkAllNormal_CheckedChanged" />
            </td>
            <td class="lblCaption1" style="text-align: center; width: 40%;">Right   
            </td>
            <td class="lblCaption1" style="text-align: center; width: 40%;">Left
            </td>

        </tr>
        <tr>
            <td class="lblCaption1" style="vertical-align: top;">Visual Acuity
            </td>
            <td class="lblCaption1">
                <table>
                    <tr>
                        <td class="lblCaption1">UCVA:
                        </td>
                        <td>
                            <input type="text" id="txtVisualAcuity_UCVA_R" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">PH:
                        </td>
                        <td>
                            <input type="text" id="txtVisualAcuity_PH_R" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">BCVA:
                        </td>
                        <td>
                            <asp:RadioButton ID="radVisualAcuity_BCVA_R_Glass" runat="server" CssClass="lblCaption1" GroupName="radVisualAcuity_BCVA_R_Glass" Text="With Glass" />
                            <asp:RadioButton ID="radVisualAcuity_BCVA_R_ContLen" runat="server" CssClass="lblCaption1" GroupName="radVisualAcuity_BCVA_R_Glass" Text="With Contact Lenses" /><br />
                            <input type="text" id="txtVisualAcuity_BCVA_R" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                     
                     <tr>
                        <td class="lblCaption1">COLOR VISION:
                        </td>
                        <td>
                            <input type="text" id="txtVisualAcuity_COLORVISION_R" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                </table>

            </td>
            <td class="lblCaption1">
                <table>
                    <tr>
                        <td class="lblCaption1">UCVA:
                        </td>
                        <td>
                            <input type="text" id="txtVisualAcuity_UCVA_L" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">PH:
                        </td>
                        <td>
                            <input type="text" id="txtVisualAcuity_PH_L" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">BCVA:
                        </td>
                        <td>
                            <asp:RadioButton ID="radVisualAcuity_BCVA_L_Glass" runat="server" CssClass="lblCaption1" GroupName="radVisualAcuity_BCVA_L_Glass" Text="With Glass" />
                            <asp:RadioButton ID="radVisualAcuity_BCVA_L_ContLen" runat="server" CssClass="lblCaption1" GroupName="radVisualAcuity_BCVA_L_Glass" Text="With Contact Lense" /><br />

                            <input type="text" id="txtVisualAcuity_BCVA_L" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                    
                     <tr>
                        <td class="lblCaption1">COLOR VISION:
                        </td>
                        <td>
                            <input type="text" id="txtVisualAcuity_COLORVISION_L" runat="server" class="lblCaption1" style="width: 100%" />
                        </td>
                    </tr>
                </table>

            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Field(Confrontation)
            </td>
            <td>
                <asp:RadioButton ID="radField_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radField_R_Normal" Text="Normal" />
                <asp:RadioButton ID="radField_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radField_R_Normal" Text="Abnormal" /><br />
                <asp:TextBox ID="txtField_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine" cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender3" runat="Server" targetcontrolid="txtField_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetFieldR"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
            <td>
                 <asp:RadioButton ID="radField_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radField_L_Normal" Text="Normal" />
                <asp:RadioButton ID="radField_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radField_L_Normal" Text="Abnormal" /><br />
               <asp:TextBox ID="txtField_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender1" runat="Server" targetcontrolid="txtField_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetFieldL"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

        </tr>
        <tr>
            <td class="lblCaption1">IOP
            </td>
            <td>
                 <asp:RadioButton ID="radIOP_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radIOP_R" Text="Normal" />
                 <asp:RadioButton ID="radIOP_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radIOP_R" Text="Abnormal" /><br />
                 <asp:TextBox ID="txtIOP_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender2" runat="Server" targetcontrolid="txtIOP_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetIOP_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
            <td>
                  <asp:RadioButton ID="radIOP_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radIOP_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radIOP_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radIOP_L_Normal" Text="Abnormal" /><br />
                <asp:TextBox ID="txtIOP_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender4" runat="Server" targetcontrolid="txtIOP_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetIOP_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>

        </tr>
        <tr>
            <td class="lblCaption1">E.O.M
            </td>
            <td>
                 <asp:RadioButton ID="radEOM_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radEOM_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radEOM_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radEOM_R_Normal" Text="Abnormal" /><br />

                <asp:TextBox ID="txtEOM_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender5" runat="Server" targetcontrolid="txtEOM_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetFOM_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

                 </td>
            <td>
                  <asp:RadioButton ID="radEOM_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radEOM_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radEOM_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radEOM_L_Normal" Text="Abnormal" /><br />
                 <asp:TextBox ID="txtEOM_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender6" runat="Server" targetcontrolid="txtEOM_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetFOM_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Lids
            </td>
            <td>
                 <asp:RadioButton ID="radLids_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radLids_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radLids_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radLids_R_Normal" Text="Abnormal" /><br />

                  <asp:TextBox ID="txtLids_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender7" runat="Server" targetcontrolid="txtLids_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetLids_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
            <td>
                <asp:RadioButton ID="radLids_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radLids_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radLids_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radLids_L_Normal" Text="Abnormal" /><br />
                   <asp:TextBox ID="txtLids_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender8" runat="Server" targetcontrolid="txtLids_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetLids_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Conjunctiva
            </td>
            <td>
                 <asp:RadioButton ID="radConjunctiva_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radConjunctiva_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radConjunctiva_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radConjunctiva_R_Normal" Text="Abnormal" /><br />


                 <asp:TextBox ID="txtConjunctiva_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender9" runat="Server" targetcontrolid="txtConjunctiva_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetConjunctiva_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radConjunctiva_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radConjunctiva_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radConjunctiva_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radConjunctiva_L_Normal" Text="Abnormal" /><br />


                
                                  <asp:TextBox ID="txtConjunctiva_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender10" runat="Server" targetcontrolid="txtConjunctiva_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetConjunctiva_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Sclera
            </td>
            <td>
                 <asp:RadioButton ID="radSclera_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radSclera_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radSclera_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radSclera_R_Normal" Text="Abnormal" /><br />

                   <asp:TextBox ID="txtSclera_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender11" runat="Server" targetcontrolid="txtSclera_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetSclera_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radSclera_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radSclera_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radSclera_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radSclera_L_Normal" Text="Abnormal" /><br />

                
                                   <asp:TextBox ID="txtSclera_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender12" runat="Server" targetcontrolid="txtSclera_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetSclera_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Cornea
            </td>
            <td>
                 <asp:RadioButton ID="radCornea_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radCornea_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radCornea_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radCornea_R_Normal" Text="Abnormal" /><br />


                <asp:TextBox ID="txtCornea_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender13" runat="Server" targetcontrolid="txtCornea_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetCornea_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radCornea_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radCornea_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radCornea_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radCornea_L_Normal" Text="Abnormal" /><br />

               <asp:TextBox ID="txtCornea_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender14" runat="Server" targetcontrolid="txtCornea_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetCornea_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">A.C
            </td>
            <td>
                 <asp:RadioButton ID="radAC_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radAC_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radAC_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radAC_R_Normal" Text="Abnormal" /><br />


               
                          <asp:TextBox ID="txtAC_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender15" runat="Server" targetcontrolid="txtAC_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetAC_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
               <asp:RadioButton ID="radAC_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radAC_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radAC_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radAC_L_Normal" Text="Abnormal" /><br />
                           <asp:TextBox ID="txtAC_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender16" runat="Server" targetcontrolid="txtAC_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetAC_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Angle
            </td>
            <td>
                 <asp:RadioButton ID="radAngle_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radAngle_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radAngle_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radAngle_R_Normal" Text="Abnormal" /><br />

                
                       <asp:TextBox ID="txtAngle_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender17" runat="Server" targetcontrolid="txtAngle_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetAngle_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radAngle_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radAngle_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radAngle_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radAngle_L_Normal" Text="Abnormal" /><br />

                
                                       <asp:TextBox ID="txtAngle_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender18" runat="Server" targetcontrolid="txtAngle_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetAngle_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Iris
            </td>
            <td>
                 <asp:RadioButton ID="radIris_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radIris_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radIris_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radIris_R_Normal" Text="Abnormal" /><br />


                <asp:TextBox ID="txtIris_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender19" runat="Server" targetcontrolid="txtIris_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetIris_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                <asp:RadioButton ID="radIris_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radIris_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radIris_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radIris_L_Normal" Text="Abnormal" /><br />
                    <asp:TextBox ID="txtIris_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender20" runat="Server" targetcontrolid="txtIris_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetIris_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Lens
            </td>
            <td>
                 <asp:RadioButton ID="radLens_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radLens_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radLens_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radLens_R_Normal" Text="Abnormal" /><br />

                   <asp:TextBox ID="txtLens_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender21" runat="Server" targetcontrolid="txtLens_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetLens_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                <asp:RadioButton ID="radLens_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radLens_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radLens_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radLens_L_Normal" Text="Abnormal" /><br />
                                   <asp:TextBox ID="txtLens_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender22" runat="Server" targetcontrolid="txtLens_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetLens_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Vitreous
            </td>
            <td>
                 <asp:RadioButton ID="radVitreous_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radVitreous_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radVitreous_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radVitreous_R_Normal" Text="Abnormal" /><br />

                <asp:TextBox ID="txtVitreous_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender23" runat="Server" targetcontrolid="txtVitreous_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetVitreous_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radVitreous_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radVitreous_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radVitreous_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radVitreous_L_Normal" Text="Abnormal" /><br />

                <asp:TextBox ID="txtVitreous_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender24" runat="Server" targetcontrolid="txtVitreous_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetVitreous_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Optic Disc
            </td>
            <td>
                 <asp:RadioButton ID="radOpticNerve_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radOpticNerve_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radOpticNerve_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radOpticNerve_R_Normal" Text="Abnormal" /><br />

                      <asp:TextBox ID="txtOpticNerve_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender25" runat="Server" targetcontrolid="txtOpticNerve_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetOpticNerve_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radOpticNerve_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radOpticNerve_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radOpticNerve_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radOpticNerve_L_Normal" Text="Abnormal" /><br />

                 <asp:TextBox ID="txtOpticNerve_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender26" runat="Server" targetcontrolid="txtOpticNerve_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetOpticNerve_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

   
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Choroid
            </td>
            <td>
                 <asp:RadioButton ID="radChoroid_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radChoroid_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radChoroid_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radChoroid_R_Normal" Text="Abnormal" /><br />


                 
                <asp:TextBox ID="txtChoroid_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender27" runat="Server" targetcontrolid="txtChoroid_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetChoroid_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radChoroid_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radChoroid_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radChoroid_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radChoroid_L_Normal" Text="Abnormal" /><br />


                <asp:TextBox ID="txtChoroid_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender28" runat="Server" targetcontrolid="txtChoroid_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetChoroid_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>
           <tr>
            <td class="lblCaption1">Gonioscopy
            </td>
            <td>
                 <asp:RadioButton ID="radGonioscopy_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radGonioscopy_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radGonioscopy_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radGonioscopy_R_Normal" Text="Abnormal" /><br />


                 
                <asp:TextBox ID="txtGonioscopy_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender33" runat="Server" targetcontrolid="txtGonioscopy_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetGonioscopy_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radGonioscopy_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radGonioscopy_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radGonioscopy_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radGonioscopy_L_Normal" Text="Abnormal" /><br />


                <asp:TextBox ID="txtGonioscopy_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender34" runat="Server" targetcontrolid="txtGonioscopy_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetGonioscopy_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>
           <tr>
            <td class="lblCaption1">Pupil
            </td>
            <td>
                 <asp:RadioButton ID="radPupil_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radPupil_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radPupil_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radPupil_R_Normal" Text="Abnormal" /><br />


                 
                <asp:TextBox ID="txtPupil_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender35" runat="Server" targetcontrolid="txtPupil_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetPupil_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                 <asp:RadioButton ID="radPupil_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radPupil_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radPupil_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radPupil_L_Normal" Text="Abnormal" /><br />


                <asp:TextBox ID="txtPupil_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender36" runat="Server" targetcontrolid="txtPupil_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetPupil_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
        </tr>

          <tr>
            <td class="lblCaption1" >Retina
            </td>
              </tr>

         <tr>
            <td class="lblCaption1"  style="text-align:right;"> 
                <asp:Label ID="lblNPDR" runat="server" CssClass="lblCaption1"   Text="NPDR" ></asp:Label>
            </td>
            <td>
                 <asp:RadioButton ID="radReinalNPDR_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radReinalNPDR_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_R_Normal" Text="Abnormal" />

                 <asp:RadioButton ID="radReinalNPDR_R_Dilated" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_R_Dilated" Text="Dilated" />
                 <asp:RadioButton ID="radReinalNPDR_R_Undilated" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_R_Dilated" Text="Undilated" />

                <br />


                      <asp:TextBox ID="txtReinalNPDR_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender29" runat="Server" targetcontrolid="txtReinalNPDR_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetReinalNPDR_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
                  <asp:RadioButton ID="radReinalNPDR_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_L_Normal" Text="Normal" />
                 <asp:RadioButton ID="radReinalNPDR_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_L_Normal" Text="Abnormal" />
                 <asp:RadioButton ID="radReinalNPDR_L_Dilated" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_L_Dilated" Text="Dilated" />

                 <asp:RadioButton ID="radReinalNPDR_L_Undilated" runat="server" CssClass="lblCaption1" GroupName="radReinalNPDR_L_Dilated" Text="Undilated" />


                <br />

                                      <asp:TextBox ID="txtReinalNPDR_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender30" runat="Server" targetcontrolid="txtReinalNPDR_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GetReinalNPDR_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>
</td>
        </tr>
          <tr>
            <td class="lblCaption1" style="text-align:right;">
                  <asp:Label ID="lblCSME" runat="server" CssClass="lblCaption1"   Text="CSME" ></asp:Label>
            </td>
            <td>
                 <asp:RadioButton ID="radReinalCSME_R_Normal" runat="server" CssClass="lblCaption1" GroupName="radReinalCSME_R_Normal" Text="Normal" />
                 <asp:RadioButton ID="radReinalCSME_R_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radReinalCSME_R_Normal" Text="Abnormal" /><br />
                
                                      <asp:TextBox ID="txtReinalCSME_R" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender31" runat="Server" targetcontrolid="txtReinalCSME_R" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GettReinalCSME_R"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>


            </td>
            <td>
               <asp:RadioButton ID="radReinalCSME_L_Normal" runat="server" CssClass="lblCaption1" GroupName="radReinalCSME_L_Normal" Text="Normal" />
               <asp:RadioButton ID="radReinalCSME_L_Abnormal" runat="server" CssClass="lblCaption1" GroupName="radReinalCSME_L_Normal" Text="Abnormal" /> 
                                                      <asp:TextBox ID="txtReinalCSME_L" runat="server" style="width: 100%;height:50px;" TextMode="MultiLine"  cssclass="lblCaption1" ></asp:TextBox>
                <asp:autocompleteextender id="AutoCompleteExtender32" runat="Server" targetcontrolid="txtReinalCSME_L" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GettReinalCSME_L"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

            </td>
        </tr>
      
      
    </table>

</fieldset>

<fieldset style="width: 95%;">
    <legend class="lblCaption1"></legend>
    <table class="table spacy">
        <tr>
            <td class="lblCaption1" style="width: 20%">OCT
            </td>
            <td>
                <textarea id="txtOCT" runat="server" name="txtOCT" rows="3" style="width: 100%" class="lblCaption1"></textarea></td>
        </tr>
        <tr>
            <td class="lblCaption1">F.A
            </td>
            <td>
                <textarea id="txtFA" runat="server" name="ProvisionalDiagnosis" rows="3" style="width: 100%" class="lblCaption1"></textarea></td>
        </tr>
        <tr>
            <td class="lblCaption1">Field
            </td>
            <td>
                <textarea id="txtField" runat="server" name="ProvisionalDiagnosis" rows="3" style="width: 100%" class="lblCaption1"></textarea></td>
        </tr>
    </table>
</fieldset>

<br />
