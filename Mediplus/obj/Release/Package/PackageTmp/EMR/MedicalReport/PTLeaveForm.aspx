﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="PTLeaveForm.aspx.cs" Inherits="Mediplus.EMR.MedicalReport.PTLeaveForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
<link href="../Content/themes/base/jquery-ui.css" />

     <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>
<script language="javascript" type="text/javascript">

    function HaadServicessListShow(CtrlName, CategoryType) {

        var win = window.open("../Masters/HaadServicessList.aspx?PageName=Leave&CtrlName=" + CtrlName + "&CategoryType=" + CategoryType, "newwin1", "top=200,left=100,height=650,width=700,toolbar=no,scrollbars=yes,menubar=no");
        win.focus();
        return false;


    }


    function BindServiceDtls(Code, Desc, CtrlName) {


        if (CtrlName == 'Diagnosis') {
            var data = document.getElementById("<%=txtDiagnosis.ClientID%>").value

            if (data != "") {
                document.getElementById("<%=txtDiagnosis.ClientID%>").value = data + "\n" + Code + ' - ' + Desc
            }
            else {
                document.getElementById("<%=txtDiagnosis.ClientID%>").value = Code + ' - ' + Desc
            }

        }




    }

    function SaveVal() {




        var strVisitDate = document.getElementById('<%=txtFromDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                alert("Please enter From Date");
                document.getElementById('<%=txtFromDate.ClientID%>').focus;
                return false;
            }

            var strToDate = document.getElementById('<%=txtToDate.ClientID%>').value
            if (/\S+/.test(strToDate) == false) {
                alert("Please enter To Date");
                document.getElementById('<%=txtToDate.ClientID%>').focus;
                return false;
            }

            return true;
        }


        function ShowLeavePrintPDF(FileNo, BranchId, EPLR_ID, ReportType) {

            var win = '';
            var Report = ""; //"EMRTestReport.rpt";


            if (ReportType == "LeaveReport") {
                Report = "EMRLeaveReport.rpt";
            }
            else if (ReportType == "MedicalCertificate") {
                Report = "EMRMedicalCertificate.rpt";
            }


            var Criteria = " 1=1 ";
            Criteria += ' AND {EMR_PT_LEAVEREPORT.EPLR_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {EMR_PT_LEAVEREPORT.EPLR_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {EMR_PT_LEAVEREPORT.EPLR_ID}=' + EPLR_ID;
            win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, 'LeaveReportrpt', 'menubar=no,left=100,top=100,height=650,width=1075,scrollbars=1')


            win.focus();


        }

        function BindDiagnosis() {



            var ChiefComp = document.getElementById("<%=txtDiagnosis.ClientID%>").value + document.getElementById("<%=txtDiag.ClientID%>").value + "\n";
          if (document.getElementById("<%=txtDiag.ClientID%>").value != "") {
              document.getElementById("<%=txtDiagnosis.ClientID%>").value = ChiefComp;
           }
           document.getElementById("<%=txtDiag.ClientID%>").value = '';


          return false;

      }


</script>

<script type="text/javascript">
    function ShowMessage() {
        $("#myMessage").show();
        setTimeout(function () {
            var selectedEffect = 'blind';
            var options = {};
            $("#myMessage").hide();
        }, 2000);
        return true;
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
<div  >
     <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
   <table width="100%"  >
            <tr>
                <td style="text-align:left;width:50%;" >
                    <h3 class="box-title">Leave Form</h3>
                </td>
                <td style="text-align:right;;width:50%;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                           <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" Text="Save" OnClick="btnSave_Click" OnClientClick="return SaveVal();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>


                <table style="width: 100%;">
                    <tr>
                        <td class="lblCaption1" >
                        Leave Type

                        </td>
                        <td class="lblCaption1" >
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="radLeaveType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px">
                                <asp:ListItem Value="Light Work" Text="Light Work" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Sick Leave" Text="Sick Leave"></asp:ListItem>
                            </asp:RadioButtonList>
                            </ContentTemplate>
                                </asp:UpdatePanel>
                        </td>

                    </tr>
                   
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">Diagnosis
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                             <div id="divwidthDiag" style="visibility: hidden;"></div>
                                    <asp:TextBox ID="txtDiag" runat="server" CssClass="label" Width="60%"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtDiag" MinimumPrefixLength="1" ServiceMethod="GetDiagnosis"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidthPDiag">
                                    </asp:AutoCompleteExtender>
                                    <input type="button" id="btnDiagcAdd" value="Add" class="button orange small" onclick="BindDiagnosis()" />
                            <asp:CheckBox ID="chkLoadDiag" runat="server" Text="Load From Visit Dtls" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkLoadDiag_CheckedChanged"  />
                            <asp:TextBox ID="txtDiagnosis" runat="server" TextMode="MultiLine" CssClass="lblLabel" Height="50px" Width="90%" ondblclick="return HaadServicessListShow('Diagnosis','Diagnosis');"></asp:TextBox>

                        </ContentTemplate>
                                </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">Treatment Desc. 
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTreatmentDesc" runat="server" TextMode="MultiLine" CssClass="lblLabel" Height="50px" Width="90%"></asp:TextBox>
                            </ContentTemplate>
                                </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Date
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFromDate" runat="server" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            &nbsp;&nbsp;&nbsp;
                          <span class="lblCaption1">To Date</span>
                            &nbsp;&nbsp; 
                         
                            <asp:TextBox ID="txtToDate" runat="server" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            </ContentTemplate>
                                </asp:UpdatePanel>

                        </td>
                    </tr>
                    
                </table>

             
                <br />
     <span class="lblCaption1" style="font-weight:bold;" >Leave History</span>
        

      <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1">
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvLeaveReport" runat="server" AllowPaging="True" Width="100%"
                                AutoGenerateColumns="False"
                                EnableModelValidation="True" OnPageIndexChanging="gvLeaveReport_PageIndexChanging" PageSize="200" GridLines="None">
                                <HeaderStyle CssClass="GridHeader_Blue" />

                                <RowStyle CssClass="GridRow" />

                                <Columns>
                                    <asp:TemplateField HeaderText="Issue Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkIssueDate" runat="server" OnClick="Edit_Click">

                                                <asp:Label ID="lblEPLR_ID" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_ID") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lblFileNo" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_PT_ID") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lblPTName" CssClass="lblCaption1" runat="server" Text='<%# Bind("FullName") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_CREATED_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Diagnosis" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDiag" runat="server" OnClick="Edit_Click">
                                                 <asp:Label ID="lblDiag" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_DIAGNOSIS") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Treatment Desc." HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTreatment" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblTreatment" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_TREATMENT") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkFromDate" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblFromDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_FROM_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkToDate" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblToDat" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_TO_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkLeaveType" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblLeaveType" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_LEAVE_TYPE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Created User" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkUser" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblCreatedUser" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_CREATED_USER") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgPrint" runat="server" OnClick="gvbtnPrint_Click" ImageUrl="~/WebReports/Images/printer.png" style="height:25px;width:30px;border:none;" />
                                        </ItemTemplate>


                                    </asp:TemplateField>


                                </Columns>


                            </asp:GridView>
                            </ContentTemplate></asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
          </div>
   
</div>
</asp:Content>

