﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicalReport.ascx.cs" Inherits="Mediplus.EMR.MedicalReport.MedicalReport" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

     <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />


 <script type="text/javascript">
     function ShowMessage() {
         $("#myMessage").show();
         setTimeout(function () {
             var selectedEffect = 'blind';
             var options = {};
             $("#myMessage").hide();
         }, 2000);
         return true;
     }

        </script>


<div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Issue" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1">Date
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                        </td>
                        <td class="lblCaption1">To Date

                        </td>
                        <td>
                            <asp:TextBox ID="txtToDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                        </td>
                        <td class="style2">
                            <asp:Button ID="btnRefresh" runat="server" CssClass="button orange small" Width="80px"
                                Text="Refresh" OnClick="btnRefresh_Click" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">File Number
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcFileNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10"></asp:TextBox>
                        </td>
                        <td class="lblCaption1">Patient Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcName" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                        </td>
                        <td class="lblCaption1">Mobile Number
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcMobile" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                </table>

                <table style="width: 100%">
                    <tr>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" Width="100%"
                                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting"
                                        EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200" GridLines="None">
                                        <HeaderStyle CssClass="GridHeader_Blue" />

                                        <RowStyle CssClass="GridRow" />

                                        <Columns>
                                            <asp:TemplateField HeaderText="File No." SortExpression="HPV_PT_Id" FooterText="File No" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click" OnClientClick='<%# Eval("HPV_EMR_STATUSDesc", "javascript:ShowStartProcess(&#39;{0}&#39;);") %>'>
                                                        <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                                        <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblDeptName" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                                        <asp:Label ID="lblRefDrCode" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_REFERRAL_TO") %>'></asp:Label>
                                                        <asp:Label ID="lblPatientId" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Patient Name" SortExpression="HPV_PT_NAME" FooterText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTName" CssClass="lblCaption1" runat="server" OnClick="Edit_Click" OnClientClick='<%# Eval("HPV_EMR_STATUSDesc", "javascript:ShowStartProcess(&#39;{0}&#39;);") %>'>
                                                        <asp:Label ID="lblPTName" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sex" FooterText="Sex" SortExpression="HPM_Sex" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblSex" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPM_SEX") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Age" FooterText="Age" SortExpression="HPV_AGE" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblAge" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_AGE") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nationality" FooterText="Nationality" SortExpression="HPV_NATIONALITY" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="Label2" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_NATIONALITY") %>'></asp:Label>


                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date Time" FooterText="Date Time" SortExpression="HPV_Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblVisitDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_DateDesc") %>'></asp:Label>
                                                    <asp:Label ID="Label1" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comp Name" FooterText="Comp Name" SortExpression="HPV_COMP_NAME" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblCompID" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_COMP_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCompName" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Priority" FooterText="Priority" SortExpression="HPV_VISIT_TYPE" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblVisitType" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_VISIT_TYPEDesc") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Token" FooterText="Token" SortExpression="HPV_DR_TOKEN" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblToken" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Status" FooterText="Status" SortExpression="HPV_PT_TYPEDesc" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblType" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_EMR_STATUSDesc") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Doctor" Visible="false" FooterText="Doctor" SortExpression="HPV_DR_NAME" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblDrID" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_DR_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblDrName" CssClass="lblCaption1" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>



                                        </Columns>


                                    </asp:GridView>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                </table>
                <br />
                <br />
                <table style="width: 100%">
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">Investigation Remarks
                        </td>
                        <td>
                            <asp:TextBox ID="txtInvstRemarks" runat="server" TextMode="MultiLine" CssClass="lblLabel" Height="30px" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">Recommendation 
                        </td>
                        <td>
                            <asp:TextBox ID="txtRecommendation" runat="server" TextMode="MultiLine" CssClass="lblLabel" Height="30px" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                  <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" Text="Save" OnClick="btnSave_Click" />
                                    </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="History" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1">Date
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromDate1" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                Enabled="True" TargetControlID="txtFromDate1" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromDate1" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                        </td>
                        <td class="lblCaption1">To Date

                        </td>
                        <td>
                            <asp:TextBox ID="txtToDate1" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                Enabled="True" TargetControlID="txtToDate1" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToDate1" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                        </td>
                        <td class="style2">
                            <asp:Button ID="btnMRRefresh" runat="server" CssClass="button orange small" Width="80px"
                                Text="Refresh" OnClick="btnMRRefresh_Click" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">File Number
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcFileNo1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10"></asp:TextBox>
                        </td>
                        <td class="lblCaption1">Patient Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcName1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                        </td>
                        <td class="lblCaption1">Mobile Number
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcMobile1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                  <table style="width: 100%">
                    <tr>
                        <td class="lblCaption1">
                <asp:GridView ID="gvMedicalReport" runat="server" AllowPaging="True" Width="100%"
                    AutoGenerateColumns="False"
                    EnableModelValidation="True" OnPageIndexChanging="gvMedicalReport_PageIndexChanging" PageSize="200" GridLines="None">
                    <HeaderStyle CssClass="GridHeader_Blue" />

                    <RowStyle CssClass="GridRow" />

                    <Columns>
                        <asp:TemplateField HeaderText="Issue Date" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPMR_CREATED_DATEDesc") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMR" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblEMRID" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPMR_EMR_ID") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No." HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblFileNo" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPMR_PT_ID") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblPTName" CssClass="lblCaption1" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Investigation Remarks" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblInvstRemarks" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPMR_INVEST_REMARKS") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Recommendation" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblRecom" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPMR_RECOMMEND") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Print Option">
                                <ItemTemplate>
                                     
                                
                                    <asp:LinkButton ID="lnkPrint" runat="server" OnClick="gvbtnPrint_Click">
                                    <input type="button" name="Print" value="Print" class="print-button button orange small" style="display: inline-block" />
                                </asp:LinkButton>


                                </ItemTemplate>
                           

                            </asp:TemplateField>


                    </Columns>


                </asp:GridView>
                        </td>
                    </tr>
                  </table>


            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>

        <div style="padding-left:10%;width:100%; ">
                    <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                        padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
           </div>
</div>
