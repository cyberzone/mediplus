﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Leave.ascx.cs" Inherits="Mediplus.EMR.MedicalReport.Leave" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
<link href="../Content/themes/base/jquery-ui.css" />

<script language="javascript" type="text/javascript">

    function HaadServicessListShow(CtrlName, CategoryType) {

        var win = window.open("../Masters/HaadServicessList.aspx?PageName=Leave&CtrlName=" + CtrlName + "&CategoryType=" + CategoryType, "newwin1", "top=200,left=100,height=650,width=700,toolbar=no,scrollbars=yes,menubar=no");
        win.focus();
        return false;


    }


    function BindServiceDtls(Code, Desc, CtrlName) {


        if (CtrlName == 'Diagnosis') {
            var data = document.getElementById("<%=txtDiagnosis.ClientID%>").value

            if (data != "") {
                document.getElementById("<%=txtDiagnosis.ClientID%>").value = data + "\n" + Code + ' - ' + Desc
                }
                else {
                    document.getElementById("<%=txtDiagnosis.ClientID%>").value = Code + ' - ' + Desc
                }

            }




        }

        function SaveVal() {



            var strFileNo = document.getElementById('<%=txtFileNo.ClientID%>').value
            if (/\S+/.test(strFileNo) == false) {
                alert("Please enter File No");
                document.getElementById('<%=txtFileNo.ClientID%>').focus;
              return false;
          }

          var strVisitDate = document.getElementById('<%=txtFromDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                alert("Please enter From Date");
                document.getElementById('<%=txtFromDate.ClientID%>').focus;
              return false;
          }

          var strToDate = document.getElementById('<%=txtToDate.ClientID%>').value
            if (/\S+/.test(strToDate) == false) {
                alert("Please enter To Date");
                document.getElementById('<%=txtToDate.ClientID%>').focus;
              return false;
          }

          return true;
      }


      function ShowLeavePrintPDF(FileNo, BranchId, EPLR_ID, ReportType) {

          var win = '';
          var Report = ""; //"EMRTestReport.rpt";


          if (ReportType == "LeaveReport") {
              Report = "EMRLeaveReport.rpt";
          }
          else if (ReportType == "MedicalCertificate") {
              Report = "EMRMedicalCertificate.rpt";
          }


          var Criteria = " 1=1 ";
          Criteria += ' AND {EMR_PT_LEAVEREPORT.EPLR_PT_ID}=\'' + FileNo + '\'';
          Criteria += ' AND {EMR_PT_LEAVEREPORT.EPLR_BRANCH_ID}=\'' + BranchId + '\'';
          Criteria += ' AND {EMR_PT_LEAVEREPORT.EPLR_ID}=' + EPLR_ID;
          win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, 'LeaveReportrpt', 'menubar=no,left=100,top=100,height=650,width=1075,scrollbars=1')


          win.focus();


      }

</script>

<script type="text/javascript">
    function ShowMessage() {
        $("#myMessage").show();
        setTimeout(function () {
            var selectedEffect = 'blind';
            var options = {};
            $("#myMessage").hide();
        }, 2000);
        return true;
    }

</script>

<div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Issue" Width="100%">
            <ContentTemplate>

                <table style="width: 100%;">
                    <tr>
                        <td class="lblCaption1">Leave Type
                        </td>
                        <td>

                            <asp:RadioButtonList ID="radLeaveType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Light Work" Text="Light Work" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Sick Leave" Text="Sick Leave"></asp:ListItem>
                            </asp:RadioButtonList>

                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">File No & Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtFileNo" runat="server" Width="20%" MaxLength="10" CssClass="label" BackColor="#ffffff" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('FileNo',this.value);" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                            &nbsp;
                        <asp:TextBox ID="txtFullName" runat="server" Width="69%" CssClass="label" BackColor="#ffffff" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Name',this.value);"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">Diagnosis
                        </td>
                        <td>
                            <asp:TextBox ID="txtDiagnosis" runat="server" TextMode="MultiLine" CssClass="lblLabel" Height="50px" Width="90%" ondblclick="return HaadServicessListShow('Diagnosis','Diagnosis');"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 15%;">Treatment Desc. 
                        </td>
                        <td>
                            <asp:TextBox ID="txtTreatmentDesc" runat="server" TextMode="MultiLine" CssClass="lblLabel" Height="30px" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Date
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            &nbsp;&nbsp;&nbsp;
                          <span class="lblCaption1">To Date</span>
                            &nbsp;&nbsp; 
                         
                            <asp:TextBox ID="txtToDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" Text="Save" OnClick="btnSave_Click" OnClientClick="return SaveVal();" />
                            <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="100px" Text="Clear" OnClick="btnClear_Click" />


                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanelHistory" HeaderText="History" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1">Date
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromDate1" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                Enabled="True" TargetControlID="txtFromDate1" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromDate1" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                        </td>
                        <td class="lblCaption1">To Date

                        </td>
                        <td>
                            <asp:TextBox ID="txtToDate1" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                Enabled="True" TargetControlID="txtToDate1" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToDate1" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                        </td>
                        <td class="style2">
                            <asp:Button ID="btnMRRefresh" runat="server" CssClass="button orange small" Width="80px"
                                Text="Refresh" OnClick="btnMRRefresh_Click" />
                        </td>
                        <td class="lblCaption1"></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">File Number
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcFileNo1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10"></asp:TextBox>
                        </td>
                        <td class="lblCaption1">Patient Name
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcName1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                        </td>
                        <td class="lblCaption1">Mobile Number
                        </td>
                        <td>
                            <asp:TextBox ID="txtSrcMobile1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1" align="right">Select Report Type: 
                    <asp:DropDownList ID="drpReportType" runat="server" CssClass="lblCaption1">
                        <asp:ListItem Text="Leave Report" Value="LeaveReport"></asp:ListItem>
                        <asp:ListItem Text="Medical Certificate" Value="MedicalCertificate"></asp:ListItem>
                    </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">

                            <asp:GridView ID="gvLeaveReport" runat="server" AllowPaging="True" Width="100%"
                                AutoGenerateColumns="False"
                                EnableModelValidation="True" OnPageIndexChanging="gvLeaveReport_PageIndexChanging" PageSize="200" GridLines="None">
                                <HeaderStyle CssClass="GridHeader_Blue" />

                                <RowStyle CssClass="GridRow" />

                                <Columns>
                                    <asp:TemplateField HeaderText="Issue Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkIssueDate" runat="server" OnClick="Edit_Click">

                                                <asp:Label ID="lblEPLR_ID" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDiag" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_DIAGNOSIS") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblTreatment" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_TREATMENT") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_CREATED_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="File No." HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkFileNo" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblFileNo" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_PT_ID") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblPTName" CssClass="lblCaption1" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkFromDate" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblFromDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_FROM_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkToDate" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblToDat" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_TO_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leave Type" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkLeaveType" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblLeaveType" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_LEAVE_TYPE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Created User" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkUser" runat="server" OnClick="Edit_Click">
                                                <asp:Label ID="lblCreatedUser" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPLR_CREATED_USER") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print Option">
                                        <ItemTemplate>


                                            <asp:LinkButton ID="lnkPrint" runat="server" OnClick="gvbtnPrint_Click">
                                    <input type="button" name="Print" value="Print" class="print-button button orange small" style="display: inline-block" />
                                            </asp:LinkButton>


                                        </ItemTemplate>


                                    </asp:TemplateField>


                                </Columns>


                            </asp:GridView>

                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>
    <div style="padding-left: 10%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>
</div>

