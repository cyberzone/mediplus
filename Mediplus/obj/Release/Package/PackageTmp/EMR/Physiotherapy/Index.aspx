﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.Physiotherapy.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
           <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <%@ Register Src="~/EMR/Physiotherapy/PhysiotherapyEvaluation.ascx" TagPrefix="UC1" TagName="PhyEvalution" %>
    <%@ Register Src="~/EMR/Physiotherapy/TherapyChart.ascx"TagPrefix="UC1" TagName="PhyTherapyChart" %>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>
     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;

        }





    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          
            <div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Physiotherapy </h3>
                </div>
            </div>
            <div style="padding-left:60%;width:100%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
        </div>
      <table style="width:100%" >
          <tr>
            <td align="left">
              </td>
              <td align="right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" Text="Save Record" />
                    </ContentTemplate>
                </asp:UpdatePanel>

              </td>
          </tr>
      </table>
                 <div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                    <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Evaluation" Width="100%">
                        <ContentTemplate>
                            <UC1:PhyEvalution ID="pnlPhyEvalution" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Pediatric Chart" Width="100%">
                        <ContentTemplate>
                            <UC1:PhyTherapyChart ID="pnlPhyTherapyChart" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    
                </asp:TabContainer>
            </div>

        </div>
</asp:Content>
