﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.Referral.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <style type="text/css">
        #ReferralDiv
        {
             width: 90%;
        }

            #ReferralDiv label
            {
                font-weight: bold;
            }

            #ReferralDiv textarea
            {
                height: 60px;
                width: 100%;
            }

        #kinAddress
        {
            height: 40px !important;
        }

        #TopLeftDiv
        {
            float: left;
            width: 450px;
        }

        #TopRightDiv
        {
            float: right;
        }

            #TopRightDiv.KinDetails
            {
            }

        #MiddleDiv
        {
            height: 38%;
            float: left;
        }

        #BottomLeftDiv
        {
            float: left;
            height: 29%;
            width: 50%;
        }

        #BottomRightDiv
        {
            float: left;
            height: 29%;
            width: 50%;
        }

        #ReferralFooter
        {
            float: right;
        }
    </style>
    
<script language="javascript" type="text/javascript">


    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }

    function ShowAlertMessage(msg) {

        alert(msg);
    }

</script>

    <script language="javascript" type="text/javascript">

        function EnableOutsideDr() {

            document.getElementById('<%=OutDoctor.ClientID%>').disabled = false;
            document.getElementById('<%=KinName.ClientID%>').disabled = false;
            document.getElementById('<%=KinAddress.ClientID%>').disabled = false;
            document.getElementById('<%=KinPhone.ClientID%>').disabled = false;

            document.getElementById('<%=BeforeTransfer.ClientID%>').disabled = false;
            document.getElementById('<%=AfterTransfer.ClientID%>').disabled = false;
            document.getElementById('<%=Recommended.ClientID%>').disabled = false;
            document.getElementById('<%=ManagementTransfer.ClientID%>').disabled = false;
            document.getElementById('<%=Currentstatusofpatient.ClientID%>').disabled = false;
            document.getElementById('<%=Specialendorsement.ClientID%>').disabled = false;

        }
        function EnableInsideDr() {

            document.getElementById('<%=OutDoctor.ClientID%>').disabled = true;
            document.getElementById('<%=KinName.ClientID%>').disabled = true;
            document.getElementById('<%=KinAddress.ClientID%>').disabled = true;
            document.getElementById('<%=KinPhone.ClientID%>').disabled = true;

            document.getElementById('<%=BeforeTransfer.ClientID%>').disabled = true;
            document.getElementById('<%=AfterTransfer.ClientID%>').disabled = true;
            document.getElementById('<%=Recommended.ClientID%>').disabled = true;
            document.getElementById('<%=ManagementTransfer.ClientID%>').disabled = true;

            document.getElementById('<%=Currentstatusofpatient.ClientID%>').disabled = true;
            document.getElementById('<%=Specialendorsement.ClientID%>').disabled = true;


        }


        function ShowReferral(ReferalType, FileNo, BranchId, EMRID) {
            var Report;
            if (ReferalType == "IntRef") {
                Report = "IntReferral.rpt";

            }
            else {
                Report = "ExtReferral.rpt";
            }

            var Criteria = " 1=1 ";
            Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    </script>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
      <div id="divAuditDtls" runat="server" visible="false"  style="text-align: right; width: 80%;" class="lblCaption1">
        Creaded By :
                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Modified By
                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
    </div>
    
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>
      <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Patient Transfer Form</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
 <table width="80%"  >
        <tr>
            <td style="width:50%;" >

            </td>
            <td   class="lblCaption1"  style="width:50%;" >  
                              <asp:UpdatePanel runat="server" ID="updatePanel11">
                                    <ContentTemplate>History &nbsp;&nbsp; 
                                 <asp:DropDownList ID="drpHistory" CssClass="TextBoxStyle" runat="server" Width="300px">
                                </asp:DropDownList>
                         
                                  <asp:ImageButton  ID="imgbtnPrescHistView"    runat="server" ToolTip="View selected Diagnosis" ImageUrl="~//Images/zoom.png" 
                                                       OnClick="imgbtnPrescHistView_Click"  style="height:15px;width:25px;"  />
                                

                                        </ContentTemplate>
                                  </asp:UpdatePanel>
                            </td>
                        </tr>
    </table>

    
        <div id="ReferralDiv" style="width:80%">
            <div id="TopLeftDiv">
                <fieldset class="fieldset" style="width:400px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Reference Details</legend>
                    <table class="table">
                        <tr>
                            <td>
                                <label for="ReferenceType" class="lblCaption1">Reference Type</label></td>
                            <td colspan="2">
                                <select id="ReferenceType" runat="server" name="ReferenceType" class="TextBoxStyle" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" id="InType" runat="server" name="Type" value="1" onclick="EnableInsideDr();" checked="true" /></td>
                            <td>
                                <label for="InType" class="lblCaption1">Internal Hospital</label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label for="InDoctorID" class="lblCaption1">Doctor</label></td>
                            <td>
                                <select name="InDoctorID" runat="server" id="InDoctorID" class="TextBoxStyle"  style="width:100%">
                                </select>
                            </td>
                            <td>
                                <!--<input type="button" value="Fix Appointment" />-->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="Type" id="OutType" runat="server" value="2" onclick="EnableOutsideDr();" />
                            </td>
                            <td>
                                <label for="OutType" class="lblCaption1">Outside</label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label for="OutHospital" class="lblCaption1">Clinic/Hospital</label></td>
                            <td>
                                <input type="text" name="OutHospital" id="OutHospital" runat="server" value="" style="width:99%" class="TextBoxStyle"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="OutDoctor" class="lblCaption1">Doctor</label></td>
                            <td>
                                <input type="text" id="OutDoctor" name="OutDoctor" runat="server" class="TextBoxStyle"  disabled="disabled" style="width:99%" /></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="TopRightDiv">
                <fieldset class="KinDetails" style="width:400px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Kin Details</legend>
                    <table class="table spacy">
                        <tr>
                            <td>
                                <label for="KinName" class="lblCaption1">Name</label></td>
                            <td>
                                <input type="text" id="KinName" runat="server" name="KinName" style="width:99%;" class="TextBoxStyle"  disabled="disabled" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="KinAddress" class="lblCaption1">Address</label></td>
                            <td>
                                <textarea id="KinAddress" runat="server" class="TextBoxStyle"  name="KinAddress" disabled="disabled" style="width:99%;resize:none;"></textarea></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="KinPhone" class="lblCaption1">Phone</label></td>
                            <td>
                                <input type="text" id="KinPhone" runat="server" class="TextBoxStyle"  style="width:99%;" name="KinPhone" disabled="disabled" /></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="MiddleDiv">
                <table class="table">
                    <tr>
                        <td>
                            <label for="Remarks" class="lblCaption1">Reason for Referral</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Remarks" name="Remarks" runat="server" rows="3" cols="140" style="resize:none;" class="TextBoxStyle" ></textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="Diagnosis" class="lblCaption1">Provisional Diagnosis</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Diagnosis" name="Diagnosis" runat="server" rows="3" cols="140" style="resize:none;" class="TextBoxStyle" > </textarea></td>
                    </tr>
                   
                </table>
            </div>
            <div id="BottomLeftDiv">
                <table class="table">
                    <tr>
                        <td>
                            <label for="BeforeTransfer" class="lblCaption1">Patient Condition Before Transfer</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="BeforeTransfer" name="BeforeTransfer" runat="server" class="TextBoxStyle"  rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="AfterTransfer" class="lblCaption1">Patient Condition During Transfer</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="AfterTransfer" name="AfterTransfer" runat="server" class="TextBoxStyle"  rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                      <tr>
                        <td>
                            <label for="AfterTransfer" class="lblCaption1">Current status of patient</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Currentstatusofpatient" name="Currentstatusofpatient" runat="server" class="TextBoxStyle"  rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>

                   
                </table>
            </div>
            <div id="BottomRightDiv">
                <table class="table">
                    <tr>
                        <td>
                            <label for="Recommended" class="lblCaption1">Recommended Medications</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Recommended" name="Recommended" runat="server" class="TextBoxStyle"  rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="ManagementTransfer" class="lblCaption1">Management During Transfer</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="ManagementTransfer" name="ManagementTransfer" runat="server" class="TextBoxStyle" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"> </textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="BeforeTransfer" class="lblCaption1">Special endorsement</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Specialendorsement" name="Specialendorsement" runat="server" class="TextBoxStyle"  rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                </table>
            </div>
            <div style="clear: both">&nbsp;</div>

        </div>
        <div id="divPatientCondition" runat="server" visible="false"  style="width:80%">
        <table>
            <tr>
                <td>
                    <label   class="lblCaption1" style="font-weight:bold;">Patient Condition at time of Transfer </label>
                </td>
            </tr>
        </table>
        <table style="width: 100%; border-width: thin; border-color: lightgray;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td class="GridHeader_Blue" style="width: 100px">Org. Function
                </td>
                <td class="GridHeader_Blue" style="width: 400px">Comment
                </td>
                <td class="GridHeader_Blue" style="width: 100px">Time 
                </td>
                <td class="GridHeader_Blue" style="width: 50px">BP
                </td>
                <td class="GridHeader_Blue" style="width: 50px">HR
                </td>
                <td class="GridHeader_Blue" style="width: 50px">SPO2 
                </td>
                <td class="GridHeader_Blue" style="width: 50px">R.R
                </td>
                <td class="GridHeader_Blue" style="width: 50px">Temp.
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">G. Condition
                </td>
                <td>
                    <asp:TextBox ID="txtComment1" runat="server" Style="resize: none;" CssClass="TextBoxStyle" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime1" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP1" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR1" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO21" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR1" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp1" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td class="lblCaption1">Resp. Sys
                </td>
                <td>
                    <asp:TextBox ID="txtComment2" runat="server" Style="resize: none;" CssClass="TextBoxStyle" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime2" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP2" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR2" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO22" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR2" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp2" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">C.V.S
                </td>
                <td>
                    <asp:TextBox ID="txtComment3" runat="server" Style="resize: none;" CssClass="TextBoxStyle" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime3" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP3" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR3" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO23" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR3" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp3" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">CNS
                </td>
                <td>
                    <asp:TextBox ID="txtComment4" runat="server" Style="resize: none;" CssClass="TextBoxStyle" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime4" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px" Width="99%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP4" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR4" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO24" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR4" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp4" runat="server" CssClass="TextBoxStyle" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
            </tr>



        </table>
            </div>
        <br /><br />

</asp:Content>
