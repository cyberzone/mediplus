﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CPTCodeResult.ascx.cs" Inherits="Mediplus.EMR.WebReports.CPTCodeResult" %>


   <table style="width:100%">
                <tr>
                    <td class="lblCaption1" style="font-weight: bold;" colspan="2">EM Result Summary
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 30px; background-image: url('<%= ResolveUrl("~/EMR/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">(1) History</span>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px; width: 300px;">Level of HPI
                    </td>
                    <td>
                        <asp:Label ID="lblHPIResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">No. of ROS
                    </td>
                    <td>
                        <asp:Label ID="lblROSResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">No. of PFSH
                    </td>
                    <td>
                        <asp:Label ID="lblPFSHResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Level of History Documented
                    </td>
                    <td>
                        <asp:Label ID="lblFinalHistoryResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="height: 30px; background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption BoldStyle" style="color: #fff;">(2) Examination Type</span>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Examination
                    </td>
                    <td>
                        <asp:Label ID="lblPEResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 30px; background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption BoldStyle" style="color: #fff;">(3) Medical Decision Making</span>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Problem Point Scored
                    </td>
                    <td>
                        <asp:Label ID="lblPSResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Data Point Scored
                    </td>
                    <td>
                        <asp:Label ID="lblDPResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Risk Management
                    </td>
                    <td>
                        <asp:Label ID="lblRiskResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Final MDM
                    </td>
                    <td>
                        <asp:Label ID="lblMDMResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                
                 <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtENMCode" Visible="false" runat="server" CssClass="lblCaption1" BorderWidth="0px" Font-Size="30" BorderColor="#cccccc" Width="200px" Height="50PX" MaxLength="10" ReadOnly="true"></asp:TextBox>

                        <asp:GridView ID="gvCPT" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Blue" Height="10px" />
                            <RowStyle CssClass="GridRow" Height="10px" />

                            <Columns>

                                <asp:TemplateField HeaderText="EM" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLabReqEPRID" CssClass="label" runat="server" Text='<%# Bind("CPTCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="200px">
                                    <ItemTemplate>
                                        <asp:Label ID="Label27" CssClass="label" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fee">
                                    <ItemTemplate>
                                        <asp:Label ID="Label28" CssClass="label" runat="server" Text='<%# Bind("Fee") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Claim Fee">
                                    <ItemTemplate>

                                        <asp:Label ID="Label29" CssClass="label" runat="server" Text='<%# Bind("ClaimFee") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
               
            </table>