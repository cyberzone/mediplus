﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Procedures.ascx.cs" Inherits="Mediplus.EMR.WebReports.Procedures" %>
 <table style="width: 100%" class="gridspacy" >
                    <tr>
                                      <td class="ReportCaption BoldStyle"  >
                                    Procedure
                                      </td>
                                  </tr>
                      <tr>
                       
                        <td valign="top" class="ReportCaption  BoldStyle">
                          
                        <asp:GridView ID="gvProcedure" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True"  Width="100%">
                            <HeaderStyle CssClass="ReportCaption"   Font-Bold="true"/>
                            <RowStyle CssClass="ReportCaption" />
                            
                            <Columns>
                                 <asp:TemplateField HeaderText="Date"  HeaderStyle-Width="100px"  Visible="false"  >
                                    <ItemTemplate>
                          
                                    <%# Eval("EPM_DATEDesc")  %>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                           
                                            <asp:Label ID="lblDiagCode" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_DIAG_CODE") %>'  ></asp:Label>
                                          
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description"  HeaderStyle-Width="60%">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblDiagName" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_DIAG_NAME") %>'></asp:Label>
                                             
                                    </ItemTemplate>

                                </asp:TemplateField>
                               

                                 <asp:TemplateField HeaderText="Price" HeaderStyle-Width="10%" Visible="false">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblPrice" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_PRICE") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 
                                 <asp:TemplateField HeaderText="Procedure Remarks" HeaderStyle-Width="40%">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="Label3" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_REMARKS") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>

                </table>