﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StandardPrescriptions.ascx.cs" Inherits="Mediplus.EMR.WebReports.StandardPrescriptions" %>

<table width="100%">
    <tr>
        <td class="lblCaption1">Rx
    </tr>
    <tr>
        <td class="lblCaption1  BoldStyle">
            <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="99%" gridline="none">
                <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" />
                <RowStyle CssClass="lblCaption1" />

                <Columns>
                    <asp:TemplateField HeaderText="Name/Strength/Form"  >
                        <ItemTemplate>
                          
                        <%# Eval("EPP_PHY_CODE")  %> &nbsp;  <%# Eval("EPP_PHY_NAME")  %>

                        </ItemTemplate>

                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Dose/Frequency/Route/Admin Strength">
                        <ItemTemplate>

                                <%# Eval("EPP_UNIT") %> &nbsp;
                                <%# Eval("EPP_FREQUENCY") %> &nbsp;
                                <%# Eval("EPP_ROUTE") %> &nbsp;
                                <%# Eval("EPP_REFILL") %>   
                        </ItemTemplate>

                    </asp:TemplateField>
                    
                   

                    <asp:TemplateField HeaderText="Duration">
                        <ItemTemplate>

                               <%# Eval("EPP_DURATION") %> &nbsp; <%# Eval("EPP_DURATION_TYPEDesc") %> 

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Quantity">
                        <ItemTemplate>

                             <%# Eval("EPP_QTY") %> 

                        </ItemTemplate>

                    </asp:TemplateField>


                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
