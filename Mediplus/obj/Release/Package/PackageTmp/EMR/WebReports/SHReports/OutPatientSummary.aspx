﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="OutPatientSummary.aspx.cs" Inherits="MediplusEMR.WebReports.SHReports.OutPatientSummary" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx" TagPrefix="UC1" TagName="Diagnosis" %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY" />
    <table style="width: 100%; text-align: center; vertical-align: top;">
        <tr>
            <td>
                <img style="padding: 1px; height: 100px; border: none;" src="../images/Report_Logo.PNG" />
            </td>
        </tr>

    </table>
    <span class="box-title">Out Patient Summary</span>
    <UC1:PatientReportHeader ID="pnlPatientReportHeader" runat="server"></UC1:PatientReportHeader>

    <div style="clear: both">&nbsp;</div>

    <UC1:Diagnosis ID="pnlDiagnosis" runat="server"></UC1:Diagnosis>


    <table style="width: 100%">
        <tr>
            <td class="lblCaption1 BoldStyle">Allergy
            </td>
        </tr>
        <tr>
            <td>
                <span class="lblCaption1"><%=ALLERGY %></span>
            </td>
        </tr>
        <tr>
            <td style="height: 10PX;"></td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Current Medication:
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPrescription" CssClass="lblCaption1" runat="server"></asp:Label>

            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 10PX;"></td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Past surgical procedure:
            </td>
        </tr>
        <tr>
            <td>
                <span class="lblCaption1"><%=HIST_SURGICAL %></span>
            </td>
        </tr>
        <tr>
            <td style="height: 10PX;"></td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Past Hospitalization:
            </td>
        </tr>
        <tr>
            <td>
                <span class="lblCaption1"><%=HIST_PAST %></span>
            </td>
        </tr>
        <tr>
            <td style="height: 10PX;"></td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Current condition: 
            </td>
        </tr>
        <tr>
            <td>
                <span class="lblCaption1"><%=HPI %></span>
            </td>
        </tr>
        <tr>
            <td style="height: 10PX;"></td>
        </tr>
    </table>
    <br />
    <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"></UC1:DoctorSignature>

</asp:Content>
