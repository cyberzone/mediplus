﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="ReassessmentSheet.aspx.cs" Inherits="Mediplus.EMR.WebReports.SHReports.ReassessmentSheet" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/VitalSignReport.ascx" TagPrefix="UC1" TagName="VitalSignReport" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx" TagPrefix="UC1" TagName="Diagnosis" %>
<%@ Register Src="~/EMR/WebReports/StandardPrescriptions.ascx" TagPrefix="UC1" TagName="StandardPrescriptions" %>
<%@ Register Src="~/EMR/WebReports/Prescriptions.ascx" TagPrefix="UC1" TagName="Prescriptions" %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <table style="width: 100%; text-align: center; vertical-align: top;">
        <tr>
            <td>
                <img style="padding: 1px; height: 100px; border: none;" src="../images/Report_Logo.PNG" />
            </td>
        </tr>

    </table>
    <table style="width: 100%">
        <tr>
            <td style="width:100%;text-align:center;"">
                <span style="width:100%;text-align:center;" class="lblCaption1" > (To be filled by the Nurses)</span>
            </td>
        </tr>
    </table>

    <UC1:PatientReportHeader ID="pnlPatientReportHeader" runat="server"></UC1:PatientReportHeader>

    <UC1:VitalSignReport ID="VitalSignReport" runat="server"></UC1:VitalSignReport>
    <br />
    <%if (CriticalNotes != null) %>
    <%{%>
    <div style="clear: both">&nbsp;</div>
    <label class="lblCaption1">Allergies if any: <%= CriticalNotes != null?CriticalNotes:"" %></label>
    <%}%>



    <table style="width: 100%">
        <tr>
            <td style="width:100%;text-align:center;"">
                <span style="width:100%;text-align:center;" class="lblCaption1" > (To be filled by the Doctors)</span>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Chief Complaints 
            </td>
        </tr>
        <tr>
            <td>

                <span class="lblCaption1"><%=CC %></span>
            </td>
        </tr>
    </table>
    <div style="clear: both">&nbsp;</div>
    
    <UC1:Diagnosis ID="pnlDiagnosis" runat="server"></UC1:Diagnosis>
    <span class="lblCaption1">Medication Prescription: (Details in the system, list only medication names)</span>
    <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lblPrescription" CssClass="lblCaption1" runat="server"></asp:Label>

            </td>
        </tr>
    </table>
    <br />
    <span class="lblCaption1">Medication to be given in the centers when applicable</span>
    <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"   Width="100%"
        EnableModelValidation="True">
        <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
        <RowStyle CssClass="GridRow" />


        <Columns>

            <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                <ItemTemplate>

                    <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_CODE") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_NAME") %>'></asp:Label>



                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DOSAGE") %>'></asp:Label>



                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_ROUTE") %>'></asp:Label>



                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPEDesc") %>'></asp:Label>
                    <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPE") %>' Visible="false"></asp:Label>


                </ItemTemplate>
            </asp:TemplateField>



        </Columns>
    </asp:GridView>
    <br />
    <table style="width: 100%;border-width:thin;border-color:lightgray;background-color:lightgray;" border="1">
        <tr>
            <td style="padding-left:10px;color:black;" class="label" >Reassessment to be completed in the follow up visit <br />
                Please repeat the full assessment incase of the following: <br />
                &nbsp;&nbsp;&nbsp;&nbsp;If the duration of the last visit is more than 90 Days.<br />
                &nbsp;&nbsp;&nbsp;&nbsp;In case of hospitalization after the last visit<br />
                &nbsp;&nbsp;&nbsp;&nbsp;In case of procedure or surgeries done under general anesthesia after the last visit 

            </td>
        </tr>
    </table>
</asp:Content>
