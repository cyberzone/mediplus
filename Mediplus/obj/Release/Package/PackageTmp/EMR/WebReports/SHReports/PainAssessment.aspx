﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="PainAssessment.aspx.cs" Inherits="Mediplus.EMR.WebReports.PainAssessment" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <table style="width: 100%; text-align: center; vertical-align: top;">
        <tr>
            <td>
                <img style="padding: 1px; height: 100px; border: none;" src="../images/Report_Logo.PNG" />
            </td>
        </tr>

    </table>
    <span class="box-title">Pain Assessment</span>
    <UC1:PatientReportHeader ID="pnlPatientReportHeader" runat="server"></UC1:PatientReportHeader>
    <br />
    <asp:GridView ID="gvPainAss" runat="server" AutoGenerateColumns="False"
        EnableModelValidation="True" Width="100%">
        <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
        <RowStyle CssClass="GridRow" />
        <Columns>
            <asp:TemplateField HeaderText="Date">
                <ItemTemplate>
                    <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_DATEDesc") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location">
                <ItemTemplate>
                    <asp:Label ID="lblLocation" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_LOCATION") %>'></asp:Label>


                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Height">
                <ItemTemplate>
                    <asp:Label ID="lblOnSet" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_ONSET") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Rhythm">
                <ItemTemplate>
                    <asp:Label ID="lblRhythm" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_RHYTHM") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Intensity of pain">
                <ItemTemplate>
                    <asp:Label ID="lblPulse" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_INTENSITY") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pain scale used">
                <ItemTemplate>
                    <asp:Label ID="lblRespiration" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_SCALE_USED") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Associated symptoms">
                <ItemTemplate>
                    <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_ASSOCIATED_SYMPTOMS") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pharmacological Manag.">
                <ItemTemplate>
                    <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PHARMACOLOGICAL_MANG") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Non-Pharmacological Manag.">
                <ItemTemplate>
                    <asp:Label ID="Label23" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_NONPHARMACOLOGICAL_MANG") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Comments and Action ">
                <ItemTemplate>
                    <asp:Label ID="Label23" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_COMMENTS") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>

        </Columns>


    </asp:GridView>
    <table style='width: 100%; border-width: thin; border-color: lightgray;' cellpadding='0' cellspacing='0' border='1'>
        <tr>
            <td class="label" valign='Top'>
                Pain intensity to be assessed with  the help of Numerical Intensity Scale 

            </td>
             <td class="label" valign='Top'>
                 Instruction: Pain Reassessment Section  must be completed if: <br />
                   &nbsp; &nbsp; &nbsp; A.	Pain score is above acceptable level (3) of pain by patient.<br />
                    &nbsp; &nbsp; &nbsp; B.	With each report of new or changed pain.<br />
                    &nbsp; &nbsp; &nbsp; C.	Following administration of analgesics:<br />
                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;i.	Within 30 minutes of administration of parenteral analgesics <br />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ii.	Within One hour of administration of oral analgesics.<br />


             </td>

        </tr>
    </table>
</asp:Content>
