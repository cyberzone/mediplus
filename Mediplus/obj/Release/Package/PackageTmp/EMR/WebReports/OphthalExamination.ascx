﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OphthalExamination.ascx.cs" Inherits="Mediplus.EMR.WebReports.OphthalExamination" %>

<span class="lblCaption1" style="font-weight:bold;" >Examination</span>
   
    <table   style="width:100%;" cellpadding="0" cellspacing="0" border="1">
        <tr>
            <td style="width: 20%"></td>
            <td class="lblCaption1" style="text-align: center; width: 40%;">Right   
            </td>
            <td class="lblCaption1" style="text-align: center; width: 40%;">Left
            </td>

        </tr>
        <tr id="trVisualAcuity" runat="server"  >
            <td class="lblCaption1" style="vertical-align: top;">Visual Acuity
            </td>
            <td class="lblCaption1">
                <table style="width:100%">
                    <tr>
                        <td class="lblCaption1" style="width:100px;">UCVA:
                        </td>
                          <td style="border: 1px solid #969393;" class="lblCaption1">
                             <%=EOPE_R_UCVA %>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">PH:
                        </td>
                         <td style="border: 1px solid #969393;" class="lblCaption1">
                                <%=EOPE_R_PH %>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">BCVA:
                        </td>
                        <td style="border: 1px solid #969393;" class="lblCaption1">
                              <%=EOPE_R_BCVA %>

                        </td>
                    </tr>
                      <tr>
                        <td class="lblCaption1">COLOR VISION:
                        </td>
                         <td style="border: 1px solid #969393;" class="lblCaption1">
                             <%=EOPE_R_COLORVISION %>
                        </td>
                    </tr>
                </table>

            </td>
            <td class="lblCaption1">
                <table style="width:100%">
                    <tr>
                        <td class="lblCaption1" style="width:100px;">UCVA:
                        </td>
                         <td style="border: 1px solid #969393;" class="lblCaption1">
                              <%=EOPE_L_UCVA %>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">PH:
                        </td>
                         <td style="border: 1px solid #969393;" class="lblCaption1">
                              <%=EOPE_L_PH %>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">BCVA:
                        </td>
                         <td style="border: 1px solid #969393;" class="lblCaption1">
                            <%=EOPE_L_BCVA %>
                        </td>
                    </tr>
                       <tr>
                        <td class="lblCaption1">COLOR VISION:
                        </td>
                        <td style="border: 1px solid #969393;" class="lblCaption1">
                           <%=EOPE_L_COLORVISION %>
                        </td>
                    </tr>
                </table>

            </td>

        </tr>
        <tr id="trField" runat="server" visible="false">
            <td class="lblCaption1" >Field(Confrontation)
            </td>
            <td style="border: 1px solid #969393;" class="lblCaption1">
               
                <%=EOPE_R_FIELD %>
            </td>
            <td style="border: 1px solid #969393;" class="lblCaption1">
              <%=EOPE_L_FIELD %>
            </td>
        </tr>
        <tr id="trIOP" runat="server" visible="false">
            <td class="lblCaption1">IOP
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                  <%=EOPE_R_IOP %>
             </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_L_IOP %>
            </td>

        </tr>
        <tr id="trEOM" runat="server" visible="false">
            <td class="lblCaption1">E.O.M
            </td>
           <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_R_EOM %>
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
               <%=EOPE_L_EOM %>

            </td>

        </tr>
        <tr id="trLids" runat="server" visible="false">
            <td class="lblCaption1">Lids
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                  <%=EOPE_R_LIDS %>

            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                    <%=EOPE_L_LIDS %>
            </td>
        </tr>
        <tr id="trConjunctiva" runat="server" visible="false">
            <td class="lblCaption1">Conjunctiva
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
               
                <%=EOPE_R_CONJUNCTIVA %>
             </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                <%=EOPE_L_CONJUNCTIVA %>

             </td>
        </tr>
        <tr id="trSclera" runat="server" visible="false">
            <td class="lblCaption1">Sclera
            </td>
            <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_R_SCLERA %>

            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                <%=EOPE_L_SCLERA %>
            </td>
        </tr>
        <tr id="trCornea" runat="server" visible="false">
            <td class="lblCaption1">Cornea
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_R_CORNEA %>

            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
               
                <%=EOPE_L_CORNEA %>
            </td>
        </tr>
        <tr id="trAC" runat="server" visible="false">
            <td class="lblCaption1">A.C
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_R_AC %>
            </td>
            <td style="border: 1px solid #969393;" class="lblCaption1">
             <%=EOPE_L_AC %>
            </td>
        </tr>
        <tr id="trAngle" runat="server" visible="false">
            <td class="lblCaption1">Angle
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
             <%=EOPE_R_ANGLE %>
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                <%=EOPE_L_ANGLE %>

            </td>
        </tr>
        <tr id="trIris" runat="server" visible="false">
            <td class="lblCaption1">Iris
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_R_IRIS %>

            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_L_IRIS %>
            </td>
        </tr>
        <tr id="trLens" runat="server" visible="false">
            <td class="lblCaption1">Lens
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                  <%=EOPE_R_LENS %>

            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                  <%=EOPE_L_LENS %>
            </td>
        </tr>
        <tr id="trVitreous" runat="server" visible="false">
            <td class="lblCaption1">Vitreous
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                  <%=EOPE_R_VITREOUS %>
            </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_L_VITREOUS %>

            </td>
        </tr>
        <tr id="trOpticNerve" runat="server" visible="false">
            <td class="lblCaption1">Optic Nerve
            </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
                <%=EOPE_R_OPTICNERVE %>
            </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
               
                <%=EOPE_L_OPTICNERVE %>
            </td>
        </tr>
        <tr id="trChoroid" runat="server" visible="false">
            <td class="lblCaption1">Choroid
            </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
               <%=EOPE_R_CHOROID %>
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_L_CHOROID %>

            </td>
        </tr>
       <tr id="trGonioscopy" runat="server" visible="false">
            <td class="lblCaption1">Gonioscopy
            </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_R_GONIOSCOPY %>

              </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
                  <%=EOPE_L_GONIOSCOPY %>

              </td>
        </tr>
        <tr id="trPupil" runat="server" visible="false">
            <td class="lblCaption1">Pupil
            </td>
             <td style="border: 1px solid #969393" class="lblCaption1">
                  <%=EOPE_R_PUPIL %>

             </td>
              <td style="border: 1px solid #969393;" class="lblCaption1">
                 <%=EOPE_L_PUPIL %>
              </td>
        </tr>
         <tr>
            <td class="lblCaption1"  style="vertical-align: top;">Retina
            </td>
            <td class="lblCaption1">
                  <table style="width:100%">
                    <tr>
                       
                       <td style="border: 1px solid #969393;" class="lblCaption1">
                            <asp:Label ID="lblNPDR" runat="server" CssClass="lblCaption1"  Width="50px"  Text="NPDR:" ></asp:Label>
                              <%=EOPE_R_NPDR %>
                        </td>

                       
                    </tr>
                    <tr>
                        
                        <td style="border: 1px solid #969393;" class="lblCaption1">
                              <asp:Label ID="lblCSME" runat="server" CssClass="lblCaption1" Width="50px"   Text="CSME:" ></asp:Label>
                               <%=EOPE_R_CSME %>

                        </td>
                    </tr>

                </table>
            </td>
            <td>
                  <table style="width:100%">
                    <tr>
                         
                          <td style="border: 1px solid #969393;" class="lblCaption1">
                              
                              
                             <%=EOPE_L_NPDR %>
                        </td>
                    </tr>
                    <tr>
                      
                          <td style="border: 1px solid #969393;" class="lblCaption1">
                          <%=EOPE_L_CSME %>

                        </td>
                    </tr>

                </table>

            </td>
        </tr>
    
       <tr id="trOCT" runat="server" visible="false">
            <td class="lblCaption1"  style="width: 20%">OCT
            </td>
             <td style="border: 1px solid #969393;" class="lblCaption1" colspan="2">
                 <%=EOPE_OCT %>
            </td>
        </tr>
        <tr id="trFA" runat="server" visible="false">
             <td class="lblCaption1">F.A
            </td>
               <td style="border: 1px solid #969393;" class="lblCaption1" colspan="2">
                 <%=EOPE_FA %>

               </td>
        </tr>
        <tr id="trField1" runat="server" visible="false">
             <td class="lblCaption1">Field
            </td>
                 <td style="border: 1px solid #969393;" class="lblCaption1" colspan="2">
                
                 <%=EOPE_FIELD %>
               </td>
        </tr>
    </table>

