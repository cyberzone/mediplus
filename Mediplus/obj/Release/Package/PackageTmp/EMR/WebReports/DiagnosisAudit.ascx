﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DiagnosisAudit.ascx.cs" Inherits="Mediplus.EMR.WebReports.DiagnosisAudit" %>

<table class="gridspacy" style="width: 100%;">
     <tr>
         <td class="ReportCaption BoldStyle"  >
              Diagnosis
          </td>
      </tr>
    <tr>

        <td class="ReportCaption  BoldStyle">
            <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%" ShowHeader="true">
                  <HeaderStyle CssClass="ReportCaption" Font-Bold="true" />
                <RowStyle CssClass="ReportCaption" />

                <Columns>
                    <asp:TemplateField HeaderText="Type"  >
                        <ItemTemplate>

                            <asp:Label ID="Label16" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPD_TYPE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Code"  >
                        <ItemTemplate>
                            ICD:  
                            <asp:Label ID="Label16" CssClass="ReportCaption" Font-Bold="true" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" >
                        <ItemTemplate>

                            <asp:Label ID="Label17" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Remarks"  >
                        <ItemTemplate>

                            <asp:Label ID="lblRemarks" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPD_REMARKS") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:BoundField  HeaderText="User" DataField="AUDIT_USER_ID"  HeaderStyle-Width="100px" />
                    <asp:BoundField  HeaderText="Date" DataField="AUDIT_DATEDesc"  HeaderStyle-Width="100px" />
                    <asp:BoundField  HeaderText="Reason" DataField="AUDIT_REASON"  HeaderStyle-Width="200px"/>
                    
                    
                  
                </Columns>

            </asp:GridView>

        </td>
    </tr>
</table>