﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientReportHeader.ascx.cs" Inherits="Mediplus.EMR.WebReports.PatientReportHeader" %>

 
  <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Content/themes/base/jquery-ui.css" />

 <input type="hidden" id="hidEMRDeptID" runat="server" />

        <table width="100%; border: 1px solid #dcdcdc"  class="gridspacy">
        <tr>
            <td class="ReportCaption BoldStyle" style=" border: 1px solid #dcdcdc;height:25px;width:30%">
               Patient Full Name:&nbsp;<asp:label id="lblPTFullName" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
            </td>
             <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;width:15%">
              Medical No.:&nbsp;<asp:label id="lblFileNo" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
            </td>
            
            <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;width:20%">
              Phone No:&nbsp;<asp:label id="lblMobile" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
            </td>
             <td class="ReportCaption BoldStyle"style=" border: 1px solid #dcdcdc;height:25px;width:200px">
              Emirates ID:&nbsp;<asp:label id="lblEmiratesID" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
            </td>
            <td class="ReportCaption BoldStyle"style=" border: 1px solid #dcdcdc;height:25px;width:200px">
                Visit No:&nbsp; <%=EMR_ID %>
            </td>
        </tr>
         <tr>
        <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;">
              Age & Sex:&nbsp;
                 <asp:label id="lblDOB" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp;  
                 <asp:label id="lblAge" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp;  
                 <asp:label id="lblAgeType" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp
                 <asp:label id="lblAge1" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp;  
                 <asp:label id="lblAgeType1" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp;  
                 <asp:label id="lblSex" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp;  
            </td>
                <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;">
                Weight:&nbsp;<asp:label id="lblWeight" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp; Kg &nbsp;
                     BMI:&nbsp;<asp:label id="lblBMI" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>&nbsp;  
              </td>
              <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;">
                Address:&nbsp;<asp:label id="lblAddress" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
              </td>
            <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;">
              Nationality:&nbsp;<asp:label id="lblNationality" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
            </td>
             <td class="ReportCaption BoldStyle"style=" border: 1px solid #dcdcdc;height:25px;width:200px">
                   Patient Type:&nbsp; <span  Class="ReportCaption BoldStyle"> <%=VisitType %></span>
             </td>
        </tr>

         <tr>
             <td class="ReportCaption BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              Insurance Company:&nbsp;<asp:label id="lblInsCo" cssclass="ReportCaption BoldStyle" runat="server"></asp:label>
            </td>
            <td class="ReportCaption BoldStyle" style=" border: 1px solid #dcdcdc;height:25px;">
              Policy No:&nbsp;<span  Class="ReportCaption BoldStyle"> <%=PolicyNo %></span>
           
            </td>
              <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;">
              Policy Type:&nbsp; <span  Class="ReportCaption BoldStyle"> <%=PolicyType %></span>
             
            </td>
             <td class="ReportCaption BoldStyle"  style=" border: 1px solid #dcdcdc;height:25px;">
                Card No:&nbsp;<span  Class="ReportCaption BoldStyle"> <%=CardNo %></span>
             </td>
        </tr>
        
    </table>