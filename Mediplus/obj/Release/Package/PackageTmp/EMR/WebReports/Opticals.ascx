﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Opticals.ascx.cs" Inherits="Mediplus.EMR.WebReports.Opticals" %>
<table style="width: 100%;">
    <tr>
        <td>

            <table style="width: 100%; border-left: groove; border-top: groove; border-right: groove; border-bottom: groove; border-width: thin;" border="0" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="GridHeader_Blue"></td>
                    <td colspan="3" class="GridHeader_Blue"><span class="GridHeader_Blue" style="font-weight: bold;">RIGHT EYE</span></td>
                    <td colspan="3" class="GridHeader_Blue"><span class="GridHeader_Blue" style="font-weight: bold;">LEFT EYE</span></td>
                </tr>

                <tr>
                    <td></td>
                    <td class="lblCaption1"><span style="font-weight: bold;">Sph </span></td>
                    <td class="lblCaption1"><span style="font-weight: bold;">Cyl </span></td>
                    <td class="lblCaption1"><span style="font-weight: bold;">Axis </span></td>
                    <td class="lblCaption1"><span style="font-weight: bold;">Sph </span></td>
                    <td class="lblCaption1"><span style="font-weight: bold;">Cyl </span></td>
                    <td class="lblCaption1"><span style="font-weight: bold;">Axis </span></td>

                </tr>


                <tr>
                    <td class="lblCaption1" style="text-align: left;">&nbsp;Distance:</td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=rdsph %></span>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=rdcyl %></span>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=rdaxis %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=ldsph %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>

                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=ldcyl %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=ldaxis %></span>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="text-align: left;">&nbsp;Near:</td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=rnsph %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=rncyl %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=rnaxis %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=lnsph %></span>
                                </td>

                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=lncyl %></span>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <span class="lblCaption1"><%=lnaxis %></span>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>

            </table>


        </td>
    </tr>
    <tr>
        <td style="height:5px;">

        </td>
    </tr>
    <tr>
        <td class="lblCaption1">D.P.P&nbsp; <span class="lblCaption1"><%=remarks %></span>
        </td>
    </tr>

</table>
