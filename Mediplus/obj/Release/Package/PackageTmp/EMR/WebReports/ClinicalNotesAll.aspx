﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="ClinicalNotesAll.aspx.cs" Inherits="Mediplus.EMR.WebReports.ClinicalNotesAll" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 50px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
 
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" runat="server">
    <table style="width: 100%; text-align: center; vertical-align: top;">
        <tr>
            <td>
                <img style="padding: 1px; height: 400px; height: 100px; border: none;" src="images/Report_Logo.PNG" />
            </td>
        </tr>

    </table>
    <div class="box">
        <div>
            <h4 class="box-title">Clinical Notes</h4>
        </div>
    </div>

    <UC1:PatientReportHeader ID="pnlPatientReportHeader" runat="server"></UC1:PatientReportHeader>

    <br />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" style="width: 800px" valign="top">

                <asp:Label ID="lblClinicalNotes1" runat="server" CssClass="label"></asp:Label>

            </td>

        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td align="left" style="width: 800px" valign="top">

                <asp:Label ID="lblClinicalNotes2" runat="server" CssClass="label"></asp:Label>

            </td>

        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td align="left" style="width: 800px" valign="top">

                <asp:Label ID="lblClinicalNotes3" runat="server" CssClass="label"></asp:Label>

            </td>

        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td align="left" style="width: 800px" valign="top">

                <asp:Label ID="lblClinicalNotes4" runat="server" CssClass="label"></asp:Label>

            </td>

        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td align="left" style="width: 800px" valign="top">

                <asp:Label ID="lblClinicalNotes5" runat="server" CssClass="label"></asp:Label>

            </td>

        </tr>


    </table>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>

    <div style="clear: both">&nbsp;</div>
    <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"></UC1:DoctorSignature>
</asp:Content>
