﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Laboratories.ascx.cs" Inherits="Mediplus.EMR.WebReports.Laboratories" %>


 <table   style="width:100%;" >
                                   <tr>
                                      <td class="ReportCaption BoldStyle" >
                                       Laboratory  
                                      </td>
                                  </tr>
                                   <tr>
                                       <td class="ReportCaption  BoldStyle">
                                         <asp:GridView ID="gvLabRequest" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="ReportCaption"  Font-Bold="true"/>
                                            <RowStyle CssClass="ReportCaption" />
                            
                                            <Columns>
                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label27" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPL_LAB_CODE") %>'  ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description"  HeaderStyle-Width="90%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label28" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPL_LAB_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Price" HeaderStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label29" CssClass="ReportCaption" runat="server" Text="0.00"  ></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                 
                                    </Columns>
                                </asp:GridView>
                                   </td>
                               </tr>
                               </table>