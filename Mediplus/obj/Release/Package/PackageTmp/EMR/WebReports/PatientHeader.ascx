﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientHeader.ascx.cs" Inherits="Mediplus.EMR.WebReports.PatientHeader" %>
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
 <table width="100%; border: 1px solid #dcdcdc"  class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;width:40%;">
               Patient Full Name:&nbsp; <%=FullName %>
            </td>
             <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;width:20%;">
              Medical No.:&nbsp; <%=FileNo %>
            </td>
            
            <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;width:20%;">
              Phone No:&nbsp; <%=MobileNo %>
            </td>
             <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;width:20%;">
              Emirates ID:&nbsp; <%=EmiratesID %>
            </td>

        </tr>
         <tr>
        <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              Age:&nbsp;  <%=Age %>&nbsp;&nbsp;  Sex:&nbsp;  <%=Sex %> 
            </td>
                <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
                  Address:&nbsp; <%=Address %>
              </td>
              <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
                Nationality:&nbsp; <%=Nationality %>
              </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              PT Company:&nbsp; <%=PTCompany %>
            </td>
        </tr>

         <tr>
             <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              Insurance Company:&nbsp; <%=CompanyName %>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              Policy No:&nbsp; <%=PolicyNo %>&nbsp;  
                 
            </td>
              <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              Policy Type:&nbsp; <%=PolicyType %>&nbsp;  
              Expiry On:&nbsp; <%=ExpiryOn %>
            </td>
             
            <td class="lblCaption1 BoldStyle" colspan="2" style=" border: 1px solid #dcdcdc;height:25px;">
              Patient Type:&nbsp; <%=PatientType %>&nbsp;  
                 
            </td>
        </tr>
        
    </table>