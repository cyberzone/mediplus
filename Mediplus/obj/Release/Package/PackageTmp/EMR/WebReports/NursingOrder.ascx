﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NursingOrder.ascx.cs" Inherits="Mediplus.EMR.WebReports.NursingOrder" %>
 <table style="width: 100%" class="gridspacy" >
     <tr>
         <td class="ReportCaption BoldStyle"  >
             Nursing Order
          </td>
      </tr>
    <tr>

        <td class="ReportCaption  BoldStyle">
 <asp:GridView ID="gvNursingInsttruction" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                              <HeaderStyle CssClass="ReportCaption"   Font-Bold="true"/>
                            <RowStyle CssClass="ReportCaption" />
                            
                              <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblgvHistDate" CssClass="label" runat="server" Text='<%# Bind("EPC_DATEDesc") %>'></asp:Label>
                                         
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>
                                        
                                      
                                            <asp:Label ID="lblgvHistInsttruction" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblgvHistComment" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>
                                         
                                    </ItemTemplate>

                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Username"  HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblgvHistUser" CssClass="label" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                     
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Start Time"  HeaderStyle-Width="150px"> 
                                    <ItemTemplate>
                                            
                                            <asp:Label ID="lblgvHistDate" CssClass="label" runat="server" Text='<%# Bind("EPC_TIMEStartDate") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblgvHistTime" CssClass="label" runat="server" Text='<%# Bind("EPC_TIMEStartTime") %>'></asp:Label>
                                             
                                    </ItemTemplate>

                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="End Time"  HeaderStyle-Width="150px"> 
                                    <ItemTemplate>
                                            <asp:Label ID="lblgvHisEndtDate" CssClass="label" runat="server" Text='<%# Bind("EPC_END_TIMEDate") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblgvHistEndTime" CssClass="label" runat="server" Text='<%# Bind("EPC_END_TIMETime") %>'></asp:Label>
                                             
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 

                               
                    </Columns>

                </asp:GridView>

        </td>
    </tr>
</table>