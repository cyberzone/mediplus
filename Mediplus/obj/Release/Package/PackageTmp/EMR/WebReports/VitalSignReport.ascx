﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VitalSignReport.ascx.cs" Inherits="Mediplus.EMR.WebReports.VitalSignReport" %>
<div id="VitalSignReportDiv">
  <span class="ReportCaption  BoldStyle" > Vital Sign</span>
	<table class="table grid spacy family-illness"  border="1" style="width:100%">
		<tr style="height:25px;">
			<td class="ReportCaption" style="font-weight:bold;">Weight :</td>
			<td class="ReportCaption"><span ><%= Weight %></span>Kg</td>
			<td class="ReportCaption" style="font-weight:bold;">Height :</td>
			<td class="ReportCaption"><span c><%= Height %></span> cm</td>
			<td class="ReportCaption" style="font-weight:bold;">BMI :</td>
			<td class="ReportCaption"><span ><%= BMI %></span></td>
			<td class="ReportCaption" style="font-weight:bold;">Temp :</td>
			<td class="ReportCaption"><span ><%= Temperature %></span> F</td>
			<td class="ReportCaption" style="font-weight:bold;">Pulse :</td>
			<td class="ReportCaption"><span><%= Pulse %></span> Beats/Min</td>
		</tr>
		<tr style="background-color:#f0f0f0;height:25px;">
			<td class="ReportCaption" style="font-weight:bold;">Respiration</td>
			<td class="ReportCaption"><span ><%= Respiration %></span> /m</td>
			<td class="ReportCaption" style="font-weight:bold;">BP: Systolic :</td>
			<td class="ReportCaption"><span ><%= BpSystolic %></span> mm/Hg</td>
			<td class="ReportCaption" style="font-weight:bold;">BP: Diastolic :</td>
			<td class="ReportCaption" ><span ><%= BpDiastolic %></span> mm/Hg</td>
			<td class="ReportCaption" style="font-weight:bold;">SPO2:</td>
			<td class="ReportCaption"><span class="ReportCaption"><%= Sp02 %></span> %</td>
            <td class="ReportCaption" style="font-weight:bold;">Capillary Blood Sugar</td>
			<td class="ReportCaption"><span class="ReportCaption"><%= CapillaryBloodSugar %></span> mg/dL</td>
		    
		</tr>
	 
		<tr style="background-color:#f0f0f0;height:25px;" id="trJaundice" runat="server">
            <td class="ReportCaption" style="font-weight:bold;">Birth Weight </td>
			<td class="ReportCaption"><span class="ReportCaption"><%= BirthWeight %></span> &nbsp;(Kg)</td>
            <td class="ReportCaption" style="font-weight:bold;">Jaundice Meter :</td>
			<td class="ReportCaption"><span ><%= JaundiceMeter %></span> mg/dL</td>
			<td class="ReportCaption" style="font-weight:bold;" >Abdominal Grith :</td>
			<td class="ReportCaption"><span ><%=  AbdominalGirth %></span>&nbsp;cm</td>
 			<td class="ReportCaption" style="font-weight:bold;">Chest Circumfrence :</td>
			<td class="ReportCaption"><span><%= ChestCircumference %></span>&nbsp; cm</td>
			 
		</tr>
		<tr style="height:25px;" id="trECG" runat="server">
			<td class="ReportCaption" style="font-weight:bold;">ECG :</td>
			<td class="ReportCaption" colspan="9" ><%= EcgReport %></td>
		</tr>
		<tr style="background-color:#f0f0f0;height:25px;">
              <td class="ReportCaption" style="font-weight:bold;">Head Circumfrence :</td>
			<td class="ReportCaption"><span ><%= HeadCircumference %></span>&nbsp;cm</td>
			<td class="ReportCaption" style="font-weight:bold;">Others :</td>
			<td  class="ReportCaption"colspan="7" ><%= Others %></td>
		</tr>
        <tr id="trFemale" runat="server" style="background-color:#f0f0f0;height:25px;" visible="false">
			<td class="ReportCaption" style="font-weight:bold;">LMP Date :</td>
			<td  class="ReportCaption" ><%= LMPDate %></td>
            <td class="ReportCaption" style="font-weight:bold;">Pregnant :</td>
			<td  class="ReportCaption"  ><%= Pregnant %></td>
            	<td class="ReportCaption" style="font-weight:bold;">EDD  :</td>
			<td  class="ReportCaption"   ><%= EDDDate %></td>
             <td class="ReportCaption"></td>
            <td class="ReportCaption"></td>
            <td class="ReportCaption"></td>
            <td class="ReportCaption"></td>
		</tr>
	</table>
</div> 

