﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Diagnosis.ascx.cs" Inherits="Mediplus.EMR.WebReports.Diagnosis" %>


<table class="gridspacy" style="width: 100%;">
     <tr>
         <td class="ReportCaption BoldStyle"  >
              Diagnosis
          </td>
      </tr>
    <tr>

        <td class="ReportCaption  BoldStyle">
            <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%" ShowHeader="false">
                <HeaderStyle CssClass="GridRow" Font-Bold="true" />
                <RowStyle CssClass="ReportCaption" Height="25px" />

                <Columns>
                     <asp:TemplateField HeaderText="Date"  HeaderStyle-Width="100px"   Visible="false" >
                        <ItemTemplate>
                          
                        <%# Eval("EPM_DATEDesc")  %>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                        <ItemTemplate>

                            <asp:Label ID="Label16" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPD_TYPE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            ICD:  
                            <asp:Label ID="Label16" CssClass="ReportCaption" Font-Bold="true" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%">
                        <ItemTemplate>

                            <asp:Label ID="Label17" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="30%">
                        <ItemTemplate>

                            <asp:Label ID="lblRemarks" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPD_REMARKS") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>

                  
                </Columns>

            </asp:GridView>

        </td>
    </tr>
</table>
