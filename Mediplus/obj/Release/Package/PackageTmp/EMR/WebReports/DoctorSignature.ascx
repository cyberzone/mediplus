﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DoctorSignature.ascx.cs" Inherits="Mediplus.EMR.WebReports.DoctorSignature" %>

<table style="width: 100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="ReportCaption BoldStyle"  style="text-align:center;">
                Doctor Name
            </td>
            <td class="ReportCaption BoldStyle" style="text-align:center;">
                Dr.License No.            </td>
            <td class="ReportCaption BoldStyle" style="text-align:center;">
               Day/Month/Year           </td>
            <td class="ReportCaption BoldStyle" style="text-align:center;">
               Signature/Stamp          </td>
       </tr>
        <tr>
              <td class="ReportCaption BoldStyle" style="vertical-align:top;text-align:center;" >
                <asp:label id="lblDrName1" cssclass="ReportCaption" runat="server"></asp:label>
                <asp:label id="lblDrCode" cssclass="ReportCaption" runat="server" Visible="false"></asp:label>
            </td>
            <td class="ReportCaption BoldStyle" style="vertical-align:top;text-align:center;">
                    <asp:label id="lblDrLicenseNo" cssclass="ReportCaption" runat="server"></asp:label>
            </td>        
            <td class="ReportCaption BoldStyle" style="vertical-align:top;text-align:center;">
                     <asp:label id="lblDate" cssclass="ReportCaption" runat="server"></asp:label>      </td>
            <td class="ReportCaption BoldStyle" style="text-align:right;text-align:center;">
              
                         </td>
        </tr>
        
        </table>
    <br />
    <table style="width:100%">
        <tr>
            <td style="text-align:right;">
                   <asp:Image ID="imgStaffSig" runat="server" Height="120px" Width="250px" BorderStyle="None" />
            </td>
        </tr>
    </table>