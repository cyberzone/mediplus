﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="ClinicalSummary.aspx.cs" Inherits="Mediplus.EMR.WebReports.ClinicalSummary" %>
<%@ Register Src="EMR_EducationalForm.ascx" TagName="EducationalForm" TagPrefix="Edu" %>

<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx" TagPrefix="UC1" TagName="Diagnosis" %>
<%@ Register Src="~/EMR/WebReports/VitalSignReport.ascx" TagPrefix="UC1" TagName="VitalSignReport" %>
<%@ Register Src="~/EMR/WebReports/Radiologies.ascx" TagPrefix="UC1" TagName="Radiologies" %>
<%@ Register Src="~/EMR/WebReports/Laboratories.ascx" TagPrefix="UC1" TagName="Laboratories" %>
<%@ Register Src="~/EMR/WebReports/Procedures.ascx" TagPrefix="UC1" TagName="Procedures" %>
<%@ Register Src="~/EMR/WebReports/Prescriptions.ascx" TagPrefix="UC1" TagName="Prescriptions" %>
<%@ Register Src="~/EMR/WebReports/CPTCodeResult.ascx" TagPrefix="UC1" TagName="CPTCodeResult" %>
<%@ Register Src="~/EMR/WebReports/NursingOrder.ascx" TagPrefix="UC1" TagName="NursingOrder" %>
<%@ Register Src="~/EMR/WebReports/CommonRemarks.ascx" TagPrefix="UC1" TagName="CommonRemarks" %>
<%@ Register Src="~/EMR/WebReports/PainScore.ascx" TagPrefix="UC1" TagName="PainScore" %>
<%@ Register Src="~/EMR/WebReports/PharmacyInternal.ascx" TagPrefix="UC1" TagName="PharmacyInternal" %>

<%@ Register Src="~/EMR/WebReports/OphthalExamination.ascx" TagPrefix="UC1" TagName="OphthalExamination" %>
<%@ Register Src="~/EMR/WebReports/Opticals.ascx" TagPrefix="UC1" TagName="Opticals"  %>
 


<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="BoxContent" runat="server">
    <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

        .BoldStyle
        {
            font-weight: bold;
        }

          .ImageStyle
    {
        height:200px;
        width:150px;
    }
    </style>
     

    <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="|HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY|CHRONIC_ILLNESS|" />
    <input type="hidden" id="hidEMRDeptID" runat="server" />
    <input type="hidden" id="hidEMRDeptName" runat="server" />
     
    <div  class="ReportHeaderDiv">
       <span  class="ReportHeader"> Clinical Summary </span>   
    </div>
 <br /><br /> 
    <table style="width: 100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Department:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblDept" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Date:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblEmrDate" CssClass="ReportCaption"  runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Start Time:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblEmrStTime" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">End Time:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblEmrEndTime" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>

        </tr>
    </table>

    <table width="100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="ReportCaption BoldStyle" colspan="6" style="border: 1px solid #dcdcdc; height: 25px;">Patient Full Name:<asp:Label ID="lblPTFullName" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">File No:<asp:Label ID="lblFileNo" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Nationality:<asp:Label ID="lblNationality" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Age:<asp:Label ID="lblAge" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>&nbsp;  
                 <asp:Label ID="lblAgeType" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>&nbsp
                 <asp:Label ID="lblAge1" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>&nbsp;  
                 <asp:Label ID="lblAgeType1" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Phone No:<asp:Label ID="lblMobile" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit No:<asp:Label ID="lblEPMID" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;Invoice No: &nbsp;<span class="ReportCaption BoldStyle" ><%=INVOICE_ID %></span>
                 &nbsp;&nbsp;&nbsp;Amount: &nbsp;<span class="ReportCaption BoldStyle"   > <%=HIM_CLAIM_AMOUNT %> (Cr)&nbsp;  <%=HIM_PT_AMOUNT %>  (Ca)  </span> 
                 &nbsp;&nbsp;&nbsp;E&M Code: &nbsp;<span class="ReportCaption BoldStyle" > <%=EM_Code %></span>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Sex:<asp:Label ID="lblSex" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>

        <tr>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Ins. Co.:<asp:Label ID="lblInsCo" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy Type:<asp:Label ID="lblPolicyType" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy No:<asp:Label ID="lblPolicyNo" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
        <tr>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Doctor Name:<asp:Label ID="lblDrName" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
                <asp:Label ID="lblDrCode" CssClass="ReportCaption BoldStyle" runat="server" Visible="false"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Emirates ID:<asp:Label ID="lblEmiratesID" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="ReportCaption BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit Type:<asp:Label ID="lblVisitType" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
    </table>
     <div id="divTriageDtls" runat="server" style="padding-top: 0px;"  visible="false">
      <table width="100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="ReportCaption BoldStyle" colspan="6" style="border: 1px solid #dcdcdc; height: 25px;"><span class="lblCaption1" style="font-weight:bold;">Nursing Assessment Status:</span>&nbsp;<asp:Label ID="lblTriageStatus" runat="server" CssClass="lblCaption1"  ></asp:Label>
            </td>
             <td class="ReportCaption BoldStyle" colspan="6" style="border: 1px solid #dcdcdc; height: 25px;"><span class="lblCaption1" style="font-weight:bold;">Assessment Start:</span>&nbsp;<asp:Label ID="lblTriageStart" runat="server" CssClass="lblCaption1" ></asp:Label>
             </td>
             <td class="ReportCaption BoldStyle" colspan="6" style="border: 1px solid #dcdcdc; height: 25px;"><span class="lblCaption1" style="font-weight:bold;"> Assessment End:</span>&nbsp;<asp:Label ID="lblTriageEnd" runat="server" CssClass="lblCaption1"   ></asp:Label>
             </td>
             <td class="ReportCaption BoldStyle" colspan="6" style="border: 1px solid #dcdcdc; height: 25px;"><span class="lblCaption1" style="font-weight:bold;">Reason for Assessment Not Required:</span>&nbsp;<asp:Label ID="lblTriageReason" runat="server" CssClass="lblCaption1"   ></asp:Label>
             </td>
             <td class="ReportCaption BoldStyle" colspan="6" style="border: 1px solid #dcdcdc; height: 25px;"><span class="lblCaption1" style="font-weight:bold;"> Done By:</span>&nbsp;<asp:Label ID="lblTriageDoneBy" runat="server" CssClass="lblCaption1"   ></asp:Label>

             </td>

        </tr>

     </table>
         </div>
     <table width="100%">
        <tr>
            <td  style="vertical-align:top;">

            
         <table width="100%">
        <tr>
            <td class="ReportCaption BoldStyle">Chief Complaints 
            </td>
        </tr>
        <tr>
            <td>

                <span class="ReportCaption"><%=SegCC %></span>
                 <asp:TextBox ID="lblCC" Visible="false" CssClass="ReportCaption" runat="server" TextMode="MultiLine" Height="50px" Width="100%" ReadOnly="true" style="resize:both;border:none;" ></asp:TextBox>
            </td>
        </tr>
         
         <tr>
            <td >
                <span class="ReportCaption"><%=EMR_CLSUMMARY_SEGMENTS  %></span>
            </td>
        </tr>
     <tr>
            <td >
                <span class="ReportCaption"><%=EMR_CLSUMMARY_SEGMENTS1  %></span>
            </td>
        </tr>
    </table>
                </td>
            <td style="vertical-align:top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        </table>
      
      <div id="divCriticalNote" runat="server" style="padding-top: 0px;"  visible="true">
          <table style="width: 100%">
         <tr>
            <td class="ReportCaption BoldStyle">Progress Notes
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=CriticalNote %></span>
            </td>
        </tr>
              </table>
    </div>
     


    <div id="divOphthalmology" runat="server" style="padding-top: 0px;"  visible="false">
 <br />
     <UC1:OphthalExamination ID="pnlOphthalExamination" runat="server"></UC1:OphthalExamination>
 <br />
      <UC1:Opticals ID="pnlOpticals" runat="server"   ></UC1:Opticals>
 </div>

    
     <div id="divBloodSugarReading" runat="server" style="padding-top: 0px;"  visible="false">
          <table style="width: 100%">
         <tr>
            <td class="ReportCaption BoldStyle">Blood Sugar Reading
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=BloodSugarReading %></span>
            </td>
        </tr>
              </table>
    </div>
    <div id="divGynaecology" runat="server" style="padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
        <table style="width: 100%">
            <tr>
                <td class="ReportCaption BoldStyle">Antenatal Assessment
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblGynaecAntenatalAsses" CssClass="ReportCaption" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td class="ReportCaption BoldStyle">Prenatal Assessment
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblGynaecPrenatalAsses" CssClass="ReportCaption" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="ReportCaption BoldStyle">Postpartum Assessment
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblGynaecPostpartumlAsses" CssClass="ReportCaption" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td class="ReportCaption BoldStyle">Menstrual&Gynec.History
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblMenstrualGynecHistory" CssClass="ReportCaption" runat="server"></asp:Label>
                </td>
            </tr>

              <tr>
                <td class="ReportCaption BoldStyle">Obstetrics History
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblObstetricsHistory" CssClass="ReportCaption" runat="server"></asp:Label>
                </td>
            </tr>
        </table>




    </div>
   
     <div id="divphysiotherapy" runat="server" style="padding-top: 0px;"  visible="false">
          <table style="width: 100%">
                <tr>
            <td class="ReportCaption BoldStyle">Problems
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=Problems %></span>
            </td>
        </tr>
         <tr>
            <td class="ReportCaption BoldStyle">Progress Notes
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=ProgressNotes %></span>
            </td>
        </tr>
         <tr>
            <td class="ReportCaption BoldStyle">Precautions
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=Precautions %></span>
            </td>
        </tr>

        <tr>
            <td class="ReportCaption BoldStyle">DetailedAssessments
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=DetailedAssessments %></span>
            </td>
        </tr>


        <tr>
            <td class="ReportCaption BoldStyle">Treatment Plans
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=TreatmentPlans %></span>
            </td>
        </tr>


              </table>
    </div>
    <UC1:VitalSignReport ID="VitalSignReport" runat="server"></UC1:VitalSignReport>
   <br />
 

    <UC1:PainScore ID="pnlPainScore" runat="server" ></UC1:PainScore>
    


    <UC1:Diagnosis ID="pnlDiagnosis" runat="server" ></UC1:Diagnosis>
  


    <UC1:Laboratories ID="Laboratories" runat="server"></UC1:Laboratories>
    <UC1:Radiologies ID="Radiologies" runat="server"></UC1:Radiologies>
    <UC1:Procedures ID="Procedures" runat="server"></UC1:Procedures>
   

    <% if (ProcedureNotes != "" && ProcedureNotes != null)
       {  %>
    <div>
        <span class="ReportCaption">Procedure Notes </span>
        <br />
        <span class="ReportCaption"><%=ProcedureNotes %></span>
    </div>
    <%} %>


    <br />
    <span class="ReportCaption" style="font-weight:bold;">Medical Decision Making</span>
      <div id="divUltrasoundFindings" runat="server" visible="false">
        <table class="table spacy"  >
            <tr>
                <td class="ReportCaption" style="font-weight:bold;" ><label for="UltrasoundFindings">Ultrasound Findings</label></td>
            </tr>
            
            <tr>
                <td>
                     <asp:Label ID="lblUltrasoundFindings" CssClass="ReportCaption" runat="server"></asp:Label>

                </td>
            </tr>
        </table>
    </div>
     <div id="divNarrativeDiag" runat="server" style="padding-top: 0px;"  visible="false">
          <table style="width: 100%">
         <tr>
            <td class="ReportCaption BoldStyle">  Narrative Diagnosis
            </td>
        </tr>
         <tr>
            <td >
                <span class="ReportCaption"><%=NarrativeDiagnosis %></span>
            </td>
        </tr>
              </table>
    </div>
    <table width="100%" style="border-collapse: collapse;">
       
        <tr>
            <td class="ReportCaption" style="font-weight:bold;" >
                 <asp:Label ID="lblTreatPlan" runat="server" CssClass="lblCaption1"  Text="Treatment Plan"  style="font-weight:bold;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="lblTreatmentPlan" CssClass="ReportCaption" runat="server" ></asp:Label>
            </td>
        </tr>
       
        <tr>
            <td class="ReportCaption" style="font-weight:bold;" >Followup Notes
            </td>
        </tr>
         
        <tr>
            <td >
                <asp:Label ID="lblFollowupNotes" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="ReportCaption" style="font-weight:bold;" >
                   <asp:Label ID="lblTreatOptionCaption" runat="server" CssClass="lblCaption1"  Text="Treatment Option"   style="font-weight:bold;"></asp:Label>
            </td>
        </tr>
         
        <tr>
            <td >
                <asp:Label ID="lblTreatOption" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
        </tr>

<tr>
            <td class="ReportCaption" style="font-weight:bold;" >
                   <asp:Label ID="lblPTEducationnCaption" runat="server" CssClass="lblCaption1"  Text="PT Education"   style="font-weight:bold;"></asp:Label>
            </td>
        </tr>
         
        <tr>
            <td >
                <asp:Label ID="lblPTEducationn" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
        </tr>

    </table>
 
    <Edu:EducationalForm ID="ctrlEduForm" runat="server" />

    <UC1:Prescriptions ID="Prescriptions" runat="server" ></UC1:Prescriptions>

    <UC1:PharmacyInternal ID="PharmacyInternal" runat="server"  Visible="false"></UC1:PharmacyInternal>

   
    <UC1:NursingOrder ID="ctrlNursingOrder" runat="server"></UC1:NursingOrder>

  <UC1:CommonRemarks ID="pnlCommonRemarks" runat="server"></UC1:CommonRemarks>
 
    <div id="divDental" runat="server" style="padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
        <table style="width: 100%" class="gridspacy">
            <tr>
                <td class="ReportCaption">Treatments
                </td>
            </tr>
            <tr>

                <td valign="top" class="ReportCaption  BoldStyle">
                    <div style="padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;">
                        <asp:GridView ID="gvDentalTreatment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" GridLines="None">
                            <HeaderStyle CssClass="GridRow" Height="20px" Font-Bold="true" />
                            <RowStyle CssClass="GridRow" Height="20px" />

                            <Columns>
                                <asp:TemplateField HeaderText="Teeth" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("DDMR_TOOTH_NO") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Treatment" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="Label5" CssClass="GridRow" runat="server" Text='<%# Bind("Treatment") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ICD Code/Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="Label7" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeDesc") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cost" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                    <ItemTemplate>

                                        <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("DDMR_TRNT_COST") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Comment" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("DDMR_TRNT_COMMENTS") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>
                    </div>
                </td>
            </tr>
             <tr>
                <td class="ReportCaption"> Remarks
                </td>
            </tr>
            <tr>
                <td>
                    <span class="ReportCaption"><%=DentalRemarks %></span>
                </td>
            </tr>
             <tr>
                <td class="ReportCaption"> Follow Notes
                </td>
            </tr>
            <tr>
                <td>
                    <span class="ReportCaption"><%=DentalFollowNotes %></span>
                </td>
            </tr>
        </table>
        <div id="divDentalADL" runat="server" style="padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove; padding: 5px 5px 5px 5px;" visible="false">
            <table class="table spacy" width="100%">
                <thead>
                    <tr>
                        <td class="ReportCaption">Xray</td>
                        <td colspan="2" class="ReportCaption">Pericapical</td>
                        <td colspan="2" class="ReportCaption">Bite-wing</td>
                        <td colspan="2" class="ReportCaption">Occlusal</td>
                        <td colspan="2" class="ReportCaption">Panoramic</td>
                    </tr>
                </thead>
                
                    <tr>
                        <td class="ReportCaption">Date</td>
                        <td>
                            <asp:Label ID="lblPericapical1" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPericapical2" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblBitewing1" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblBitewing2" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblOcclusal1" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblOcclusal2" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPanoramic1" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPanoramic2" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>



                    </tr>
                    <tr>
                        <td class="ReportCaption">Remarks</td>
                        <td>
                            <asp:Label ID="lblPericapical1Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPericapical2Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblBitewing1Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblBitewing2Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblOcclusal1Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblOcclusal2Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPanoramic1Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPanoramic2Remarks" CssClass="ReportCaption BoldStyle" runat="server"></asp:Label></td>


                    </tr>

                
            </table>

        </div>
    </div>
    <br />
    <%-- <UC1:CPTCodeResult id="CPTCodeResult" runat="server"></UC1:CPTCodeResult> --%>

    <div id="divDeptImages" runat="server" visible="false">
        <table   style="width:100%">
            <tr>
                <td>
                    <asp:Image ID="img1" runat="server" Width="470" Height="300" Visible="false" />
                </td>
                <td style="width: 10px;"></td>
                <td>
                    <asp:Image ID="img2" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="img3" runat="server" Width="470" Height="300" Visible="false"  />
                </td>
                <td style="width: 10px;"></td>
                <td>
                    <asp:Image ID="img4" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="img5" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
                <td style="width: 20px;"></td>
                <td>
                    <asp:Image ID="img6" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
            </tr>
              <tr>
                <td>
                    <asp:Image ID="img7" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
                <td style="width: 20px;"></td>
                <td>
                    <asp:Image ID="img8" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="img9" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
                <td style="width: 20px;"></td>
                <td>
                    <asp:Image ID="img10" runat="server" Width="470" Height="300"  Visible="false" />
                </td>
            </tr>
        </table>
    </div>
   
    
    <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"></UC1:DoctorSignature>

</asp:Content>

