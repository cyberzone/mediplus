﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PainScore.ascx.cs" Inherits="Mediplus.EMR.WebReports.PainScore" %>
 <div id="pain-score-left-content">
      <center>
            <div style="border-style:solid;border-width:thin;  border-radius:20px;width:80%;">
              <span class="lblCaption1" style="text-align:center;font-size:15px;">WONG BAKER PAIN ASSESSMENT TOOL</span>
                <table class="table spacy" style="width:70%;">
                    <tr>
                             <td><img src="../Images/smilie-1.png" alt="Pain Score" class="pain-smilie"/></td>
                            <td><img src="../images/smilie-2.png" alt="Pain Score" class="pain-smilie" /></td>
                             <td><img src="../images/smilie-3.png" alt="Pain Score" class="pain-smilie" /></td>
                             <td><img src="../images/smilie-4.png" alt="Pain Score" class="pain-smilie" /></td>
                               <td><img src="../images/smilie-5.png" alt="Pain Score" class="pain-smilie" /></td>
                              <td><img src="../images/smilie-6.png" alt="Pain Score" class="pain-smilie" /></td>
                        </tr>
                        <tr>
                            
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue0" value="0" class="lblCaption1" runat="server" disabled ="disabled" /> 0 
                                <input type="radio" name="PainValue" id="PainValue1" value="1"  class="lblCaption1"  runat="server" disabled ="disabled" /> 1
                            </td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue2" value="2"  class="lblCaption1" runat="server"  disabled ="disabled" /> 2 
                                <input type="radio" name="PainValue" id="PainValue3" value="3"  class="lblCaption1"  runat="server" disabled ="disabled" /> 3
                            </td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue4" value="4" class="lblCaption1"  runat="server" disabled ="disabled" /> 4 
                                <input type="radio" name="PainValue" id="PainValue5" value="5" class="lblCaption1"  runat="server" disabled ="disabled" /> 5
                            </td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue6" value="6" class="lblCaption1"  runat="server" disabled ="disabled" /> 6 
                                <input type="radio" name="PainValue" id="PainValue7" value="7" class="lblCaption1" runat="server"  disabled ="disabled" /> 7
                            </td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue8" value="8" class="lblCaption1"  runat="server" disabled ="disabled" /> 8 
                                <input type="radio" name="PainValue" id="PainValue9" value="9" class="lblCaption1" runat="server" disabled ="disabled" /> 9
                            </td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue10" value="10" class="lblCaption1"  runat="server" disabled ="disabled" /> 10 
                            </td>
                        </tr>
                       <tr>
                           <td class="lblCaption1" colspan="6">
                               No Pain  ---------------------------------------------------------------------------------------------------------> Severe Pain
                           </td>
                       </tr>
                     
                </table>
                   
            </div>
          </center> 
        </div>