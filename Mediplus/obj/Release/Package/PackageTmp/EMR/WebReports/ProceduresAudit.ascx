﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProceduresAudit.ascx.cs" Inherits="Mediplus.EMR.WebReports.ProceduresAudit" %>

<table style="width: 100%" class="gridspacy">
    <tr>
        <td class="ReportCaption BoldStyle">Procedure
        </td>
    </tr>
    <tr>

        <td valign="top" class="ReportCaption  BoldStyle">

            <asp:GridView ID="gvProcedure" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="ReportCaption" Font-Bold="true" />
                <RowStyle CssClass="ReportCaption" />

                <Columns>

                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>

                            <asp:Label ID="lblDiagCode" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_DIAG_CODE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>

                            <asp:Label ID="lblDiagName" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_DIAG_NAME") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Procedure Remarks">
                        <ItemTemplate>

                            <asp:Label ID="Label3" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPP_REMARKS") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:BoundField HeaderText="User" DataField="AUDIT_USER_ID" HeaderStyle-Width="100px" />
                    <asp:BoundField HeaderText="Date" DataField="AUDIT_DATEDesc" HeaderStyle-Width="100px" />
                    <asp:BoundField HeaderText="Reason" DataField="AUDIT_REASON" HeaderStyle-Width="200px" />

                </Columns>

            </asp:GridView>
        </td>
    </tr>

</table>
