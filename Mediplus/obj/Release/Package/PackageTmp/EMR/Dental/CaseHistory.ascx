﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CaseHistory.ascx.cs" Inherits="Mediplus.EMR.Dental.CaseHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

<script src="../../Validation.js" type="text/javascript"></script>

<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />


<script language="javascript" type="text/javascript">


    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }

   
    function BindOralHygieneTotal() {
        var DebrisTotal = 0;
        var vtxtDS16 = document.getElementById("<%=txtDS16.ClientID%>").value;
        var vtxtDS11 = document.getElementById("<%=txtDS11.ClientID%>").value;
        var vtxtDSX = document.getElementById("<%=txtDSX.ClientID%>").value;
        var vtxtDS26 = document.getElementById("<%=txtDS26.ClientID%>").value;
        var vtxtDS46 = document.getElementById("<%=txtDS46.ClientID%>").value;

        var vtxtDSX_1 = document.getElementById("<%=txtDSX_1.ClientID%>").value;
        var vtxtDS31 = document.getElementById("<%=txtDS31.ClientID%>").value;
        var vtxtDS36 = document.getElementById("<%=txtDS36.ClientID%>").value;

        if (vtxtDS16.length <= 0) {
            vtxtDS16 = 0;
        }

        if (vtxtDS11.length <= 0) {
            vtxtDS11 = 0;
        }
        if (vtxtDSX.length <= 0) {
            vtxtDSX = 0;
        }
        if (vtxtDS26.length <= 0) {
            vtxtDS26 = 0;
        }

        if (vtxtDS46.length <= 0) {
            vtxtDS46 = 0;
        }

        if (vtxtDSX_1.length <= 0) {
            vtxtDSX_1 = 0;
        }

        if (vtxtDS31.length <= 0) {
            vtxtDS31 = 0;
        }


        if (vtxtDS36.length <= 0) {
            vtxtDS36 = 0;
        }
       
         DebrisTotal = parseInt(vtxtDS16) + parseInt(vtxtDS11) + parseInt(vtxtDSX) + parseInt(vtxtDS26) + parseInt(vtxtDS46) + parseInt(vtxtDSX_1) + parseInt(vtxtDS31)+ parseInt(vtxtDS36)

        var Calculus = 0;
        var vtxtCA16 = document.getElementById("<%=txtCA16.ClientID%>").value;
        var vtxtCA11 = document.getElementById("<%=txtCA11.ClientID%>").value;
        var vtxtCAX = document.getElementById("<%=txtCAX.ClientID%>").value;
        var vtxtCA26 = document.getElementById("<%=txtCA26.ClientID%>").value;
        var vtxtCA46 = document.getElementById("<%=txtCA46.ClientID%>").value;

        var vtxtCAX_1 = document.getElementById("<%=txtCAX_1.ClientID%>").value;
        var vtxtCA31 = document.getElementById("<%=txtCA31.ClientID%>").value;
        var vtxtCA36 = document.getElementById("<%=txtCA36.ClientID%>").value;

        if (vtxtCA16.length <= 0) {
            vtxtCA16 = 0;
        }

        if (vtxtCA11.length <= 0) {
            vtxtCA11 = 0;
        }

        if (vtxtCAX.length <= 0) {
            vtxtCAX = 0;
        }

        if (vtxtCA26.length <= 0) {
            vtxtCA26 = 0;
        }

        if (vtxtCA46.length <= 0) {
            vtxtCA46 = 0;
        }

        if (vtxtCAX_1.length <= 0) {
            vtxtCAX_1 = 0;
        }


        if (vtxtCA31.length <= 0) {
            vtxtCA31 = 0;
        }

        if (vtxtCA36.length <= 0) {
            vtxtCA36 = 0;
        }

       Calculus = parseInt(vtxtCA16) + parseInt(vtxtCA11) + parseInt(vtxtCAX) + parseInt(vtxtCA26) + parseInt(vtxtCA46) + parseInt(vtxtCAX_1) + parseInt(vtxtCA31) + parseInt(vtxtCA36)

        var TotalScore = 0;
        TotalScore = parseInt(DebrisTotal) + parseInt(Calculus)
        document.getElementById("<%=lblTotalScore.ClientID%>").innerHTML = TotalScore;
        document.getElementById("<%=hidTotalScore.ClientID%>").value = TotalScore;


        

    }

</script>


<table style="width: 80%">
    <asp:HiddenField ID="hidTotalScore" runat="server" />
    <tr>
         <td class="lblCaption1" style="width:250px;">
            Root canal treated
             </td>
             <td>
              <asp:TextBox ID="txtRootCanalTreated" runat="server" CssClass="label"  style="width:150px;" ></asp:TextBox> 
        </td>
         <td class="lblCaption1">
                Periapical lesion  </td>
             <td>
              <asp:TextBox ID="txtPeriapicalLesion" runat="server" CssClass="label" style="width:150px;"></asp:TextBox>
        </td>
    </tr>
    <tr>
         <td class="lblCaption1">
           Remaining roots  </td>
             <td>
              <asp:TextBox ID="txtRemainingRoots" runat="server" CssClass="label" onkeypress="return OnlyNumeric(event);" style="width:150px;"></asp:TextBox>
        </td>
          <td class="lblCaption1">
            DMF Score  </td>
             <td>
              <asp:TextBox ID="txtDMFScore" runat="server" CssClass="label" onkeypress="return OnlyNumeric(event);" style="width:150px;"></asp:TextBox>
          </td>
    </tr>
    
    <tr>
        <td class="lblCaption1" >
            Angle’s Classification:   </td>
             <td colspan="4">
                 <asp:RadioButtonList  ID="radAngleClassifi" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="250px"   >
                 <asp:ListItem Text="Class I" Value="Class I" > </asp:ListItem>
                 <asp:ListItem Text="Class II" Value="Class II"> </asp:ListItem>
                 <asp:ListItem Text="Class III" Value="Class III"> </asp:ListItem>
            </asp:RadioButtonList>

           
           
        </td>
        
    </tr>
    <tr>
         <td class="lblCaption1">
          Overjet  </td>
             <td>
              <asp:TextBox ID="txtOverjet" runat="server" CssClass="label" style="width:150px;"></asp:TextBox>
        </td>
          <td class="lblCaption1">
            Overbite  </td>
             <td>
              <asp:TextBox ID="txtOverbite" runat="server" CssClass="label" style="width:150px;"></asp:TextBox>
          </td>
    </tr>
     <tr>
         <td class="lblCaption1"  >
          Parafunctional Habits; if YES, please specify  </td>
             <td colspan="4">
               <asp:DropDownList ID="drpParafunctionalHabits" runat="server" CssClass="lblCaption1" style="width:155px;" >
                   <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                   <asp:ListItem Text="Nail biting" Value="Nail biting"></asp:ListItem>
                   <asp:ListItem Text="Lip biting" Value="Lip biting"></asp:ListItem>
                   <asp:ListItem Text="Cheek  biting" Value="Cheek  biting"></asp:ListItem>
                   <asp:ListItem Text="Teeth grinding teeth" Value="Teeth grinding teeth"></asp:ListItem>
                   <asp:ListItem Text="Tongue thrust" Value="Tongue thrust"></asp:ListItem>
                   <asp:ListItem Text="Holding pen with teeth" Value="Holding pen with teeth"></asp:ListItem>
               </asp:DropDownList>
               
        </td>
    </tr>
       <tr>
        <td class="lblCaption1" style="height:10px;"> 
            
        </td>
    </tr>
   
      <tr>
        <td class="lblCaption1"  colspan="4" > 
            <span class="lblCaption1">Oral Hygiene Procedures:</span>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Tooth brush  </td> 
        <td>
            <asp:TextBox ID="txtToothBrush" runat="server" CssClass="label" style="width:150px;"></asp:TextBox>
            </td>
        <td  class="lblCaption1">Brushing Technique </td>
         <td>
            <asp:TextBox ID="txtBrushingTechnique" runat="server" CssClass="label" style="width:150px;"></asp:TextBox>&nbsp;
             
           
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">
             Interdental cleaning: 
        </td>
        <td colspan="4" > 
            
            <asp:CheckBox ID="chkDentalfloss" runat="server" CssClass="lblCaption1" Text="Dental floss" />
            <asp:CheckBox ID="chkToothpicks" runat="server" CssClass="lblCaption1" Text="Toothpicks" />
            <asp:CheckBox ID="chkInterproximalBrushes" runat="server" CssClass="lblCaption1" Text="Interproximal brushes " />
        </td>
    </tr>
     <tr>
        <td class="lblCaption1" style="height:10px;"> 
        </td>
    </tr>
     </table>

    <table style="width: 100%">
    <tr>
        <td class="lblCaption1">Oral Hygiene Status:   (Simplified Oral Hygiene Index):   Good/  Fair/  Poor
            <br />

        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 80%;border-width:thin;border-color:lightgray;" cellpadding="0" cellspacing="0" border="1" >
                <tr>
                    <td class="lblCaption1" style="width:150px;"></td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">16
                    </td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">11
                    </td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">X
                    </td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">26
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Debris Score
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDS16" runat="server" CssClass="label" BorderStyle="None"  style="width: 100%;height:25px;text-align:center;"  onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDS11" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDSX" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDS26" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Calculus
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCA16" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCA11" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCAX" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCA26" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1"></td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">46
                    </td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">X
                    </td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">31
                    </td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">36
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Debris Score
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDS46" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDSX_1" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDS31" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtDS36" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Calculus
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCA46" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCAX_1" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCA31" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtCA36" runat="server" CssClass="label"  BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeyup="BindOralHygieneTotal();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Total Score: (Debris Index Score + Calculus Index Score) = &nbsp;
            <asp:Label ID="lblTotalScore" runat="server" CssClass="lblCaption1" ></asp:Label>
        </td>
    </tr>
      <tr>
        <td class="lblCaption1" style="height:10px;"> 
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Gingival Status:
        </td>
    </tr>
    <tr>
        <td>
             <table style="width: 80%;border-width:thin;border-color:lightgray;" cellpadding="0" cellspacing="0" border="1" >
                <tr>
                    <td style="width:150px;"></td>
                    <td class="lblCaption1" style="font-weight:bold;">Clinical Features
                    </td>
                    <td class="lblCaption1" style="font-weight:bold;">Tooth Segment 
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Color</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtColorCF" runat="server" CssClass="label" Width="100%" BorderStyle="None" TextMode="MultiLine" style="width: 99%;height:50px;resize:none;" ></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtColorTS" runat="server" CssClass="label" Width="100%" BorderStyle="None" TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Contour</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtContourCF" runat="server" CssClass="label" Width="100%" BorderStyle="None" TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtContourTS" runat="server" CssClass="label" Width="100%" BorderStyle="None" TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Size</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtSizeCF" runat="server" CssClass="label" Width="100%" BorderStyle="None" style="width: 99%;height:20px;"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtSizeTS" runat="server" CssClass="label" Width="100%" BorderStyle="None" style="width: 99%;height:20px;"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Consistency</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtConsistencyCF" runat="server" CssClass="label" Width="100%" BorderStyle="None"  TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtConsistencyTS" runat="server" CssClass="label" Width="100%" BorderStyle="None"  TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Position</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtPositionCF" runat="server" CssClass="label" Width="100%" BorderStyle="None"  TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtPositionTS" runat="server" CssClass="label" Width="100%" BorderStyle="None"  TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Bleeding on probing</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtBleedingProbingCF" runat="server" CssClass="label" Width="100%" BorderStyle="None"  TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtBleedingProbingTS" runat="server" CssClass="label" Width="100%" BorderStyle="None"  TextMode="MultiLine" style="width: 99%;height:50px;resize:none;"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Periodontal Status:
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">
            <table style="width: 80%;border-width:thin;border-color:lightgray;" cellpadding="0" cellspacing="0" border="1" >
                <tr>
                    <td></td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">18</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">17</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">16</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">15</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">14</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">13</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">12</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">11</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">21</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">22</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">23</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">24</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">25</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">26</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">27</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">28</td>

                </tr>
                <tr>
                    <td class="lblCaption1">Probing depth</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth18" runat="server" CssClass="label" BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth17" runat="server" CssClass="label" BorderStyle="None"  style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth16" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth15" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth14" runat="server" CssClass="label"   BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth13" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth12" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth11" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth21" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth22" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth23" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth24" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth25" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth26" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth27" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth28" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>
                <tr>
                    <td class="lblCaption1">Mobility</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility18" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility17" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility16" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility15" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility14" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility13" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility12" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility11" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility21" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility22" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility23" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility24" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility25" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility26" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility27" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility28" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>

                <tr>
                    <td class="lblCaption1">Furcation</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation18" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation17" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation16" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation15" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation14" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation13" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation12" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation11" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation21" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation22" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation23" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation24" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation25" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation26" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation27" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation28" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>
                <tr>
                    <td class="lblCaption1">Recession</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession18" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession17" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession16" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession15" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession14" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession13" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession12" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession11" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession21" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession22" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession23" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession24" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession25" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession26" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession27" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession28" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>

                <tr>
                    <td class="lblCaption1">Loss Of Att</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt18" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt17" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt16" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt15" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt14" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt13" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt12" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt11" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt21" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt22" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt23" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt24" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt25" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt26" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt27" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt28" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>

                <tr>
                    <td></td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">48</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">47</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">46</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">45</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">44</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">43</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">42</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">41</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">31</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">32</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">33</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">34</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">35</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">36</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">37</td>
                    <td class="lblCaption1" style="text-align:center;font-weight:bold;">38</td>

                </tr>
                <tr>
                    <td class="lblCaption1">Probing depth</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth48" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth47" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth46" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth45" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth44" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth43" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth42" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth41" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth31" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth32" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth33" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth34" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth35" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth36" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth37" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtProbingDepth38" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>
                <tr>
                    <td class="lblCaption1">Mobility</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility48" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility47" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility46" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility45" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility44" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility43" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility42" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility41" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility31" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility32" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility33" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility34" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility35" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility36" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility37" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtMobility38" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>

                <tr>
                    <td class="lblCaption1">Furcation</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation48" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation47" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation46" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation45" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation44" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation43" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation42" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation41" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation31" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation32" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation33" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation34" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation35" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation36" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation37" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtFurcation38" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>
                <tr>
                    <td class="lblCaption1">Recession</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession48" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession47" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession46" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession45" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession44" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession43" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession42" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession41" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession31" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession32" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession33" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession34" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession35" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession36" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession37" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtRecession38" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>

                <tr>
                    <td class="lblCaption1">Loss Of Att</td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt48" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt47" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt46" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt45" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt44" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt43" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt42" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt41" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt31" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt32" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt33" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt34" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt35" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt36" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt37" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>
                    <td class="lblCaption1">
                        <asp:TextBox ID="txtLossOfAtt38" runat="server" CssClass="label"  BorderStyle="None" style="width: 100%;height:25px;text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox></td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Radiographic Findings<br />
            <asp:TextBox ID="txtRadiographicFindings" runat="server" CssClass="label" Width="80%" Height="50px" TextMode="MultiLine" style="resize:none;"></asp:TextBox>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Prognosis:
            <br />
            <asp:TextBox ID="txtPrognosis" runat="server" CssClass="label" Width="80%" Height="50px" TextMode="MultiLine" style="resize:none;"></asp:TextBox>

        </td>
    </tr>
    
</table>
