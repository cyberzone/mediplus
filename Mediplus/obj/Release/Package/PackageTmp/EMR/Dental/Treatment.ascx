﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Treatment.ascx.cs" Inherits="Mediplus.EMR.Dental.Treatment" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />




<style>
    #dashboard ul li .removeService
    {
        display: none;
    }

    #dashboard ul li:hover .removeService
    {
        display: inline;
    }

    #teeth-details
    {
        /*margin-left:35px;*/
    }

    .addmore
    {
        margin: 0px 0px 5px 0px;
    }

    #treatment-image
    {
        width: 200px;
        float: left;
    }

    #dental-treatmentdetails
    {
        height: 420px;
        overflow: auto;
        margin-left: 250px;
        /*border:1px solid #dcdcdc;*/
    }

        #dental-treatmentdetails table tr td
        {
            text-align: center;
        }

    .textarea
    {
        width: 98%;
        height: 70px;
    }

    .ui-helper-hidden-accessible
    {
        display: none;
    }

    .small-text
    {
        width: 60px;
    }

    .big-text
    {
        width: 90%;
    }

    .map
    {
        margin-bottom: 5px;
    }
</style>

<script src="../Scripts/jquery.maphilight.js" type="text/javascript"></script>

<script type="text/javascript">

    function TestClick() {
        alert('Test');

    }

    $(function () {
        $('.dentalmap').maphilight({
            fillColor: '008800'
        });

    });
</script>

<script src="scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type = "text/javascript">
    function ShowCurrentTime() {

        alert('test');
        $.ajax({
            type: "POST",
            url: "Treatment.aspx/ShwoDentalTrans",
            data: '{name: "' + $("#<%=txtRemarks.ClientID%>")[0].value + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    function OnSuccess(response) {
        alert(response.d);
    }



    function DRIdSelected() {
        return true;
    }
</script>

 <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divwidth1
        {
            width: 400px !important;
        }

            #divwidth1 div
            {
                width: 400px !important;
            }


      

       

    </style>

 <style type="text/css">
            #imgMapContainer {
                position: relative;
            }
            
            #rm1Status {
                position: absolute;
                top: 160px;
                left: 14px;
                display: none;
                border-radius:100px;
                text-align:center;
                
            }
            #rm2Status {
                position: absolute;
                top: 135px;
                left: 16px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm3Status {
                position: absolute;
                top: 110px;
                left: 18px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm4Status {
                position: absolute;
                top: 85px;
                left: 24px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
              #rm5Status {
                position: absolute;
                top: 61px;
                left: 34px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
              #rm6Status {
                position: absolute;
                top: 40px;
                left: 49px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm7Status {
                position: absolute;
                top: 22px;
                left: 68px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
              #rm8Status {
                position: absolute;
                top: 10px;
                left: 92px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
               #rm9Status {
                position: absolute;
                top: 10px;
                left: 118px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm10Status {
                position: absolute;
                top: 21px;
                left: 140px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm11Status {
                position: absolute;
                top: 42px;
                left: 160px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm12Status {
                position: absolute;
                top: 65px;
                left: 172px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
               #rm13Status {
                position: absolute;
                top: 87px;
                left: 181px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm14Status {
                position: absolute;
                top: 112px;
                left: 186px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm15Status {
                position: absolute;
                top: 137px;
                left: 190px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm16Status {
                position: absolute;
                top: 160px;
                left: 190px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
           #rm17Status {
                position: absolute;
                top: 205px;
                left: 190px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm18Status {
                position: absolute;
                top: 230px;
                left: 190px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm19Status {
                position: absolute;
                top: 255px;
                left: 185px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm20Status {
                position: absolute;
                top: 280px;
                left: 178px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm21Status {
                position: absolute;
                top: 305px;
                left: 170px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm22Status {
                position: absolute;
                top: 322px;
                left: 158px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm23Status {
                position: absolute;
                top: 340px;
                left: 140px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             #rm24Status {
                position: absolute;
                top: 350px;
                left: 118px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm25Status {
                position: absolute;
                top: 350px;
                left: 92px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
              #rm26Status {
                 position: absolute;
                top: 340px;
                left: 70px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
            #rm27Status {
                position: absolute;
                top: 325px;
                left: 50px;
                display: none;
                border-radius:100px;
                text-align:center;
            }
             
        </style>
        <script type="text/javascript">
            function doThis(str, DisValue, Color) {

                document.getElementById(str + 'Status').style.display = 'block';
                document.getElementById(str + 'Status').innerText = DisValue;
                document.getElementById(str + 'Status').style.backgroundColor = Color;

            }



        </script>
<body runat="server" id="MyBody">


<table style="width:100%">
 

    <tr>
        <td style="width:30%;vertical-align:top;">
            
                <canvas style="width: 10px; height: 30px; position: absolute; left: 0px; top: 0px; padding: 0px; border: 0px; opacity: 1;"></canvas>
                <div style="position: relative;" id="dentalmap-image">
                    <img src="<%= ResolveUrl("~/EMR/images/DentalFormtreatment.png") %>" usemap="#dentalmap" class="map" />
                    <a href="Index.aspx?PageName=Dental&ToothNo=1" > <div id="rm1Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=2" > <div id="rm2Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=3" > <div id="rm3Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=4" > <div id="rm4Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=5" > <div id="rm5Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=6" > <div id="rm6Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=7" > <div id="rm7Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=8" > <div id="rm8Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=9" > <div id="rm9Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=10" ><div id="rm10Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=11" ><div id="rm11Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=12" ><div id="rm12Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=13" ><div id="rm13Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=14" ><div id="rm14Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=15" ><div id="rm15Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=16" ><div id="rm16Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=17" ><div id="rm17Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=18" ><div id="rm18Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=19" ><div id="rm19Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=20" ><div id="rm20Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=21" ><div id="rm21Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=22" ><div id="rm22Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=23" ><div id="rm23Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=24" ><div id="rm24Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=25" ><div id="rm25Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=26" ><div id="rm26Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=27" ><div id="rm27Status" style="height:20px;width:20px;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=28" >
                        <div id="rm28Status" style="height:20px;width:20px;position: absolute;top: 304px;left: 38px;display: none;border-radius:100px;text-align:center;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=29" >
                         <div id="rm29Status" style="height:20px;width:20px; position: absolute;top: 280px;left: 26px;display: none;border-radius:100px;text-align:center;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=30" >
                         <div id="rm30Status" style="height:20px;width:20px; position: absolute;top: 255px;left: 20px;display: none;border-radius:100px;text-align:center;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=31" >
                         <div id="rm31Status" style="height:20px;width:20px; position: absolute;top: 232px;left: 15px;display: none;border-radius:100px;text-align:center;"></div></a>
                    <a href="Index.aspx?PageName=Dental&ToothNo=32" >
                         <div id="rm32Status" style="height:20px;width:20px; position: absolute;top: 205px;left: 13px;display: none;border-radius:100px;text-align:center;"></div></a>

                   <a href="Index.aspx?PageName=Dental&ToothNo=A" >
                         <div id="rmAStatus" style="height:20px;width:20px; position: absolute;top: 160px;left: 55px;display: none;border-radius:100px;text-align:center;"></div></a>
                   <a href="Index.aspx?PageName=Dental&ToothNo=B" >
                         <div id="rmBStatus" style="height:20px;width:20px; position: absolute;top: 133px;left: 55px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=C" >
                         <div id="rmCStatus" style="height:20px;width:20px; position: absolute;top: 105px;left: 59px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=D" >
                         <div id="rmDStatus" style="height:20px;width:20px; position: absolute;top: 82px;left: 72px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=E" >
                         <div id="rmEStatus" style="height:20px;width:20px; position: absolute;top: 60px;left: 92px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=F" >
                         <div id="rmFStatus" style="height:20px;width:20px; position: absolute;top: 60px;left: 117px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=G" >
                         <div id="rmGStatus" style="height:20px;width:20px; position: absolute;top: 80px;left: 140px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=H" >
                         <div id="rmHStatus" style="height:20px;width:20px; position: absolute;top: 109px;left: 150px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=I" >
                         <div id="rmIStatus" style="height:20px;width:20px; position: absolute;top: 132px;left: 155px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=J" >
                         <div id="rmJStatus" style="height:20px;width:20px; position: absolute;top: 160px;left: 153px;display: none;border-radius:100px;text-align:center;"></div></a>
                  <a href="Index.aspx?PageName=Dental&ToothNo=K" >
                         <div id="rmKStatus" style="height:20px;width:20px; position: absolute;top: 202px;left: 147px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=L" >
                         <div id="rmLStatus" style="height:20px;width:20px; position: absolute;top: 230px;left: 147px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=M" >
                         <div id="rmMStatus" style="height:20px;width:20px; position: absolute;top: 256px;left: 142px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=N" >
                         <div id="rmNStatus" style="height:20px;width:20px; position: absolute;top: 285px;left: 135px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=O" >
                         <div id="rmOStatus" style="height:20px;width:20px; position: absolute;top: 306px;left: 120px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=P" >
                         <div id="rmPStatus" style="height:20px;width:20px; position: absolute;top: 306px;left: 90px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=Q" >
                         <div id="rmQStatus" style="height:20px;width:20px; position: absolute;top: 284px;left: 75px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=R" >
                         <div id="rmRStatus" style="height:20px;width:20px; position: absolute;top: 259px;left: 65px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=S" >
                         <div id="rmSStatus" style="height:20px;width:20px; position: absolute;top: 230px;left: 59px;display: none;border-radius:100px;text-align:center;"></div></a>
                 <a href="Index.aspx?PageName=Dental&ToothNo=T" >
                         <div id="rmTStatus" style="height:20px;width:20px; position: absolute;top: 205px;left: 55px;display: none;border-radius:100px;text-align:center;"></div></a>


                </div>
                <map id="dentalmap" name="dentalmap">
                    <area shape="circle"   coords="23,168,12" href="Index.aspx?PageName=Dental&ToothNo=1"  alt="1"   />
                    <area shape="circle"  coords="26,143,12"  href="Index.aspx?PageName=Dental&ToothNo=2" alt="2"    />  
                    <area shape="circle" coords="30,116,12" href="Index.aspx?PageName=Dental&ToothNo=3"  alt="3" />
                    <area shape="circle" coords="26,94,12" href="Index.aspx?PageName=Dental&ToothNo=4"   alt="4" />
                    <area shape="circle" coords="46,69,12" href="Index.aspx?PageName=Dental&ToothNo=5"  alt="5" />
                    <area shape="circle" coords="65,46,12" href="Index.aspx?PageName=Dental&ToothNo=6"   alt="6" />
                    <area shape="circle" coords="84,29,12" href="Index.aspx?PageName=Dental&ToothNo=7"   alt="7" />
                    <area shape="circle" coords="92,11,12" href="Index.aspx?PageName=Dental&ToothNo=8"  alt="8" />
                    <area shape="circle" coords="134,19,12" href="Index.aspx?PageName=Dental&ToothNo=9"  alt="9" />
                    <area shape="circle" coords="151,32,12" href="Index.aspx?PageName=Dental&ToothNo=10"  alt="10" />
                    <area shape="circle" coords="175,50,12" href="Index.aspx?PageName=Dental&ToothNo=11"  alt="11" />
                    <area shape="circle" coords="188,63,12" href="Index.aspx?PageName=Dental&ToothNo=12"  alt="12" />
                    <area shape="circle" coords="192,96,12" href="Index.aspx?PageName=Dental&ToothNo=13"  alt="13" />
                    <area shape="circle" coords="198,122,12" href="Index.aspx?PageName=Dental&ToothNo=14"   alt="14" />
                    <area shape="circle" coords="202,146,12" href="Index.aspx?PageName=Dental&ToothNo=15"   alt="15" />
                    <area shape="circle" coords="204,170,12" href="Index.aspx?PageName=Dental&ToothNo=16"   alt="16" />
                    <area shape="circle" coords="206,211,12" href="Index.aspx?PageName=Dental&ToothNo=17"   alt="17" />
                    <area shape="circle" coords="191,237,12" href="Index.aspx?PageName=Dental&ToothNo=18"   alt="18" />
                    <area shape="circle" coords="200,265,12" href="Index.aspx?PageName=Dental&ToothNo=19"   alt="19" />
                    <area shape="circle" coords="194,288,12" href="Index.aspx?PageName=Dental&ToothNo=20"   alt="20" />
                    <area shape="circle" coords="184,309,12" href="Index.aspx?PageName=Dental&ToothNo=21"  alt="21" />
                    <area shape="circle" coords="174,329,12" href="Index.aspx?PageName=Dental&ToothNo=22"   alt="22" />
                    <area shape="circle" coords="155,347,12" href="Index.aspx?PageName=Dental&ToothNo=23" onclick="teeth(155,347,'23')" alt="23" />
                    <area shape="circle" coords="129,359,12" href="Index.aspx?PageName=Dental&ToothNo=24" onclick="teeth(129,359,'24')" alt="24" />
                    <area shape="circle" coords="105,359,12" href="Index.aspx?PageName=Dental&ToothNo=25" onclick="teeth(105,359,'25')" alt="25" />
                    <area shape="circle" coords="80,351,12" href="Index.aspx?PageName=Dental&ToothNo=26" onclick="teeth(80,351,'26')" alt="26" />
                    <area shape="circle" coords="62,334,12" href="Index.aspx?PageName=Dental&ToothNo=27" onclick="teeth(62,334,'27')" alt="27" />
                    <area shape="circle" coords="48,317,12" href="Index.aspx?PageName=Dental&ToothNo=28" onclick="teeth(48,317,'28')" alt="28" />
                    <area shape="circle" coords="35,289,12" href="Index.aspx?PageName=Dental&ToothNo=29" onclick="teeth(35,289,'29')" alt="29" />
                    <area shape="circle" coords="28,267,12" href="Index.aspx?PageName=Dental&ToothNo=30" onclick="teeth(28,267,'30')" alt="30" />
                    <area shape="circle" coords="24,244,12" href="Index.aspx?PageName=Dental&ToothNo=31" onclick="teeth(24,244,'31')" alt="31" />
                    <area shape="circle" coords="25,214,12" href="Index.aspx?PageName=Dental&ToothNo=32" onclick="teeth(25,214,'32')" alt="32" />
                    <area shape="circle" coords="67,172,12" href="Index.aspx?PageName=Dental&ToothNo=A" onclick="teeth(67,172,'A')" alt="A" />
                    <area shape="circle" coords="63,143,12" href="Index.aspx?PageName=Dental&ToothNo=B" onclick="teeth(63,143,'B')" alt="B" />
                    <area shape="circle" coords="72,112,12" href="Index.aspx?PageName=Dental&ToothNo=C" onclick="teeth(72,112,'C')" alt="C" />
                    <area shape="circle" coords="85,89,12" href="Index.aspx?PageName=Dental&ToothNo=D" onclick="teeth(85,89,'D')" alt="D" />
                    <area shape="circle" coords="105,69,12" href="Index.aspx?PageName=Dental&ToothNo=E" onclick="teeth(105,69,'E')" alt="E" />
                    <area shape="circle" coords="129,68,12" href="Index.aspx?PageName=Dental&ToothNo=F" onclick="teeth(129,68,'F')" alt="F" />
                    <area shape="circle" coords="149,91,12" href="Index.aspx?PageName=Dental&ToothNo=G" onclick="teeth(149,91,'G')" alt="G" />
                    <area shape="circle" coords="163,115,12" href="Index.aspx?PageName=Dental&ToothNo=H" onclick="teeth(163,115,'H')" alt="H" />
                    <area shape="circle" coords="164,143,12" href="Index.aspx?PageName=Dental&ToothNo=I" onclick="teeth(164,143,'I')" alt="I" />
                    <area shape="circle" coords="164,168,12" href="Index.aspx?PageName=Dental&ToothNo=J" onclick="teeth(164,168,'J')" alt="J" />
                    <area shape="circle" coords="160,209,12" href="Index.aspx?PageName=Dental&ToothNo=K" onclick="teeth(160,209,'K')" alt="K" />
                    <area shape="circle" coords="159,239,12" href="Index.aspx?PageName=Dental&ToothNo=L" onclick="teeth(159,239,'L')" alt="L" />
                    <area shape="circle" coords="154,264,12" href="Index.aspx?PageName=Dental&ToothNo=M" onclick="teeth(154,264,'M')" alt="M" />
                    <area shape="circle" coords="149,291,12" href="Index.aspx?PageName=Dental&ToothNo=N" onclick="teeth(149,291,'N')" alt="N" />
                    <area shape="circle" coords="131,314,12" href="Index.aspx?PageName=Dental&ToothNo=O" onclick="teeth(131,314,'O')" alt="O" />
                    <area shape="circle" coords="104,315,12" href="Index.aspx?PageName=Dental&ToothNo=P" onclick="teeth(104,315,'P')" alt="P" />
                    <area shape="circle" coords="86,292,12" href="Index.aspx?PageName=Dental&ToothNo=Q" onclick="teeth(86,292,'Q')" alt="Q" />
                    <area shape="circle" coords="76,268,12" href="Index.aspx?PageName=Dental&ToothNo=R" onclick="teeth(76,268,'R')" alt="R" />
                    <area shape="circle" coords="70,241,12" href="Index.aspx?PageName=Dental&ToothNo=S" onclick="teeth(70,241,'S')" alt="S" />
                    <area shape="circle" coords="69,211,12" href="Index.aspx?PageName=Dental&ToothNo=T" onclick="teeth(69,211,'T')" alt="T" />
                </map>
            

        </td>
        
        <td style="width:70%;vertical-align:top;">
            <asp:Button ID="btnNew" runat="server" CssClass="button orange" Width="70px" OnClick="btnNew_Click" Text="Add New" />
            <br />
             <div style="padding-top: 0px; width:600px; height:400px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                 

                 <table cellpadding="0" cellspacing="0" style="width:650px;">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"  CssClass="label" Height="50px" Width="95%" ></asp:TextBox>
        </td>
    </tr>
     <tr>
        <td class="lblCaption1" colspan="2">
            Follow Notes<br />
            <asp:TextBox ID="txtFollowNotes" runat="server" TextMode="MultiLine"  CssClass="label" Height="50px" Width="95%" ></asp:TextBox>
        </td>
    </tr>
</table>


</body>



