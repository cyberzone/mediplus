﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Scans.aspx.cs" Inherits="Mediplus.EMR.Scans.Scans" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/themes/base/jquery-ui.css" />

     <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

       
    </style>
     <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.5em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        
        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



 


            if (document.getElementById('<%=drpCategory.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                return false;
            }
 


        }

        function ShowUploadMsg() {

            alert('Uploaded Successfully');
        }

        </script>

      <script type="text/javascript">
          function ShowMessage() {
              $("#myMessage").show();
              setTimeout(function () {
                  var selectedEffect = 'blind';
                  var options = {};
                  $("#myMessage").hide();
              }, 2000);
              return true;
          }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
     <fieldset style="width:80%;">
            <legend class="lblCaption1" style="font-weight:bold;" >Select your scan file</legend>
         <table width="1000px" border="0">
                
                 <tr>
            
                        <td class="lblCaption1" >
                            Category
                        </td>
                        <td class="lblCaption1" >
                              <asp:dropdownlist id="drpCategory" cssclass="TextBoxStyle"   runat="server" width="150px">
                                  </asp:dropdownlist>
                          
                        </td>
                </tr>
                <tr>
                     <td class="lblCaption1">
                         Select File 
                     </td>
                 <td >
                     <asp:updatepanel id="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="300px">
                            <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                        </asp:Panel>
                    </ContentTemplate>
                  
                </asp:updatepanel>
                 </td>
             </tr>
             <tr id="trRemarks" runat="server" visible="false">
                   
                 <td class="lblCaption1" >
                      Remarks
                 </td>
                 <td>
                  <asp:TextBox ID="txtRemarks" runat="server" Width="200"   CssClass="TextBoxStyle"          ></asp:TextBox>
                 </td>
             </tr>
             <tr>
                   
                 <td class="lblCaption1" >
                      Comments
                 </td>
                 <td>
                  <asp:TextBox ID="txtComment" runat="server" Width="380px"   CssClass="TextBoxStyle"        Height="30px" TextMode="MultiLine"  ></asp:TextBox>
                 </td>
             </tr>
           
             <tr>
                 <td colspan="2">
                       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                     <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click"   OnClientClick="return SaveVal();" Text="Upload" />
                     <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click"     Text="Clear" />

                         </ContentTemplate>
                           <Triggers>
                               <asp:PostBackTrigger  ControlID="btnSave" />

                           </Triggers>
                </asp:UpdatePanel>


                 </td>
             </tr>
        </table>
          <div style="padding-left:20%;width:100%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Uploaded Successfully </div>
        </div>
  
     </fieldset>
</asp:Content>
