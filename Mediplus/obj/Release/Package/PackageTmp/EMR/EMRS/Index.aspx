﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.EMRS.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

 <%@ Register Src="~/EMR/EMRS/ComplaintHistory.ascx" TagPrefix="UC1" TagName="ComplaintHistory" %>
 <%@ Register Src="~/EMR/EMRS/ProblemAndRisk.ascx" TagPrefix="UC1" TagName="ProblemAndRisk" %>

 <%@ Register Src="~/EMR/EMRS/DataPoints.ascx" TagPrefix="UC1" TagName="DataPoints" %>

 <%@ Register Src="~/EMR/EMRS/CPTCodeResult.ascx" TagPrefix="UC1" TagName="CPTCodeResult" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../../Content/themes/base/jquery-ui.css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.5em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>

     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">EMR Review Sheet </h3>
        </div>
    </div>
    <div style="padding-left:60%;width:100%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
   </div>
      <table style="width:100%" >
          <tr>
              <td align="right">
                   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" Text="Save Record" />
                         </ContentTemplate>
                </asp:UpdatePanel>

              </td>
          </tr>
      </table>
 
    <br />
      <div style="padding-top: 0px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
     <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
     <asp:TabPanel runat="server" ID="TabPanelComplaintHistory" HeaderText="Complaint & History" Width="100%" >
            <ContentTemplate>
             <asp:PlaceHolder ID="PlaceHolderCompHist" runat="server"></asp:PlaceHolder>
                <UC1:ComplaintHistory ID="ComplaintHistory" runat="server" />
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanelDataPoints" HeaderText="Data Review & Points" Width="100%">
            <ContentTemplate>
                  <UC1:DataPoints ID="DataPoints" runat="server" />
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanelProblemRisk" HeaderText="Problem & Risk" Width="100%">
            <ContentTemplate>
                 <UC1:ProblemAndRisk ID="ProblemAndRisk" runat="server" />
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanelCPTCodeResult" HeaderText="CPT Code Result" Width="100%">
            <ContentTemplate>
                 <UC1:CPTCodeResult ID="TabCPTCodeResult" runat="server" />
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>

          </div>
    <br />
    <br />
</asp:Content>
