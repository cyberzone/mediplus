﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataPoints.ascx.cs" Inherits="Mediplus.EMR.EMRS.DataPoints" %>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" >

    function BindPintValue(chkID ,CtrlName, strValue) {
        
        var txtValue="1";

        if (document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanelDataPoints_DataPoints_" + chkID).checked == true) {

            if (strValue != "") {
                txtValue = strValue
            }
            
        }
        else {

            txtValue = "0";
        }

        document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanelDataPoints_DataPoints_" + CtrlName).value = txtValue;

    }
</script>
<div id="DataReviewContainer">
    <fieldset>
        <legend class="lblCaption1" style="font-weight: bold;">Data Reviewed</legend>        
        <table class="table spacy">
            <tr>
                <td><textarea name="DataReviewed" id="txtDataReviewed" runat="server" style="width:100%;height:100px;"></textarea></td>
            </tr>
        </table>
    </fieldset>
    <div id="DataPointsContainer">
    <div id="DataPoints">
        <fieldset>
            <legend class="lblCaption1" style="font-weight: bold;">Data Points</legend>
            <table class="table data-points-fields grid tleft">
                 <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </table>
        </fieldset>
    </div>
</div>
</div>