﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="IndexCPTResult.aspx.cs" Inherits="Mediplus.EMR.EMRS.IndexCPTResult" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
 <%@ Register Src="~/EMR/EMRS/CPTCodeResult.ascx" TagPrefix="UC1" TagName="CPTCodeResult" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
     <style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>

     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

        </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">EMR Review Sheet </h3>
        </div>
    </div>
    <div style="padding-left:60%;width:80%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
   </div>
    
 
    <br />
      <div style="padding-top: 0px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;width:80%;">

         <UC1:CPTCodeResult ID="TabCPTCodeResult" runat="server" />
           

          </div>
    <br />
    <br />
</asp:Content>
