﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.OPDAssessment.Index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
      <div id="divAuditDtls" runat="server" visible="false"  style="text-align: right; width: 80%;" class="lblCaption1">
        Creaded By :
                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Modified By
                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
    </div>
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>

    <table style="width: 80%; display: none;">
        <tr>
            <td align="right">
                <span class="lblCaption1">Template&nbsp;</span>
                <asp:DropDownList ID="drpTemplate" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged"></asp:DropDownList>
                <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="button gray small" Text="Save" Enabled="false" OnClientClick="CreateTempClick('TREATMENTPLAN');"
                    Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                <asp:LinkButton ID="lnkDeleteTemp" runat="server" CssClass="button gray small" Text="Delete" OnClick="lnkDeleteTemp_Click"
                    Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>


            </td>
        </tr>
    </table>
  <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">OPD Assessment </h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <div style="padding-top: 0px; width: 80%; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">

        <asp:GridView ID="gvOPDAssessment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="99%" GridLines="None" OnRowDataBound="gvOPDAssessment_RowDataBound">
            <HeaderStyle CssClass="GridHeader_Blue" />
            <RowStyle CssClass="GridRow" />
            <AlternatingRowStyle CssClass="GridAlterRow" />
            <Columns>
                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%">
                    <ItemTemplate>
                        <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("LevelType") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("FieldID") %>' Visible="false"></asp:Label>

                        <asp:Label ID="lblDescription" CssClass="lblCaption1" runat="server" Text='<%# Bind("FieldName") %>' Width="100%"></asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Patient's Answer" HeaderStyle-Width="25%">
                    <ItemTemplate>
                        <asp:RadioButtonList ID="radPTAnswer" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px">
                            <asp:ListItem Text="No" Value="N"> </asp:ListItem>
                            <asp:ListItem Text="Yes" Value="Y"> </asp:ListItem>
                            <asp:ListItem Text="Don't Know" Value="D"> </asp:ListItem>
                        </asp:RadioButtonList>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Details" HeaderStyle-Width="25%">
                    <ItemTemplate>
                        <asp:TextBox ID="txtDtls" runat="server" CssClass="lblCaption1"></asp:TextBox>
                    </ItemTemplate>

                </asp:TemplateField>
            </Columns>

        </asp:GridView>
    </div>

    <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
        PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
        PopupDragHandleControlID="pnlSaveTemplate">
    </asp:ModalPopupExtender>

    <asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
        <table cellpadding="0" cellspacing="0" width="100%" style="">
            <tr>
                <td align="right">
                    <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
                </td>
            </tr>
            <tr>
                <td class="lblCaption" style="font-weight: bold;">Template List
                </td>
            </tr>

            <tr>
                <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                    <fieldset style="height: 80px;">
                        <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                        <table>
                            <tr>
                                <td>Template Name : 
                                </td>
                                <td>
                                    <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                                </td>
                            </tr>
                            <tr>
                                <td>Other Doctors
                                </td>
                                <td>
                                    <input type="checkbox" id="chkTemplateOtherDr" runat="server" title="Apply" />Apply
                                </td>
                            </tr>

                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">

                    <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />

                    <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

                </td>
            </tr>

        </table>
    </asp:Panel>



    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

</asp:Content>
