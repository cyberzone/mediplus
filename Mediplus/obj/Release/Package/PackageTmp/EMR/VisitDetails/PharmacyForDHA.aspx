﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PharmacyForDHA.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.PharmacyForDHA1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
  <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

     <style>
        .AutoExtender 
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

<script language="javascript" type="text/javascript">


    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }

    function ShowAlertMessage(msg) {

        alert(msg);
    }

</script>

<script language="javascript" type="text/javascript">
    function refreshPanelPha(strValue) {
        // alert(strValue)
        __doPostBack('<%= updatePanelPha.UniqueID %>', "PharmacyForDHA");

    }

    function setCursorPha(el, st, end) {
        if (el.setSelectionRange) {
            el.focus();
            el.setSelectionRange(st, end);
        } else {
            if (el.createTextRange) {
                range = el.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', st);
                range.select();
            }
        }
    }

    function ShowSearchPha() {
        //  SetCaretAtEnd();

        var elem = document.getElementById("<%=txtSearch.ClientID%>")

        var elemLen = elem.value.length;
        //alert(elemLen);
        setCursorPha(elem, elemLen, elemLen)
    }

    function GetSelectedValue(selectItem) {
        var index = document.getElementById(selectItem).selectedIndex;

        //  alert("value =" + document.getElementById(selectItem).value);
        // alert("text =" + document.getElementById(selectItem).options[index].text);

        return document.getElementById(selectItem).options[index].text;
    }

    function BindInstructions() {

        var vUnit = document.getElementById("<%=txtUnit.ClientID%>").value;
        var vUnitType = document.getElementById("<%=drpUnitType.ClientID%>").value;
        var vFrequency = document.getElementById("<%=txtFreqyency.ClientID%>").value;
        var vFrequencyType = document.getElementById("<%=drpFreqType.ClientID%>").value;
        var vDuration = document.getElementById("<%=txtDuration.ClientID%>").value;
        var vDurationType = document.getElementById("<%=drpDurationType.ClientID%>").value;


        var vUnitTypeDesc = GetSelectedValue("<%=drpUnitType.ClientID%>");
        var vFrequencyTypeDesc = GetSelectedValue("<%=drpFreqType.ClientID%>");
        var vDurationTypeDesc = GetSelectedValue("<%=drpDurationType.ClientID%>");

        if (vUnit != "" && vFrequency != "" && vDuration) {

            var strInstructions = "";
            strInstructions = "Take " + vUnit + " " + vUnitTypeDesc + "," + vFrequency + " Time (" + vFrequencyTypeDesc + ") for " + vDuration + " " + vDurationTypeDesc + ".";



            var decTotal = 0, durationMix;

            durationMix = document.getElementById("<%=drpDurationType.ClientID%>").value;

            if (vDurationType == "3") {
                durationMix = 7;

            }
            else if (vDurationType == "1") {
                durationMix = 30;

            }
            else if (vDurationType == "2") {
                durationMix = 365;

            }
            else {
                durationMix = 1;
            }

            if (vFrequencyType == "04") {

                decTotal = ((parseFloat(vUnit) * parseFloat(vFrequency) * 1 * parseFloat(vDuration)) * parseFloat(durationMix));
                //  alert(Total)
            }
            else if (vFrequencyType == "05") {

                decTotal = ((parseFloat(vUnit) * parseFloat(vFrequency) * 24 * parseFloat(vDuration)) * parseFloat(durationMix));
                //  alert(Total)
            }
            else if (vFrequencyType == "06") {

                decTotal = (((parseFloat(vUnit) * parseFloat(vFrequency) * parseFloat(vDuration)) / 7) * parseFloat(durationMix));
                //  alert(Total)
            }

            if (vUnitType == "01") {
                document.getElementById("<%=txtQty.ClientID%>").value = decTotal;

            }
            else {
                document.getElementById("<%=txtQty.ClientID%>").value = "1";

            }


            document.getElementById("<%=txtRemarks.ClientID%>").value = strInstructions;

        }
        else {
            document.getElementById("<%=txtQty.ClientID%>").value = "";
            document.getElementById("<%=txtRemarks.ClientID%>").value = "";
        }

    }

    function BindSplInstruction() {
        var vSplInstruction = document.getElementById("<%=chkSplInstruction.ClientID%>");
        if (vSplInstruction.checked == true) {
            document.getElementById("<%=txtRemarks.ClientID%>").value = "";
        }
        else {
            BindInstructions()
        }
    }


    function ServNameSelected() {
        if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
            var Data = document.getElementById('<%=txtServName.ClientID%>').value;
            var Data1 = Data.split('~');

            if (Data1.length > 1) {
                document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[2];
                }
            }

            return true;
        }


        function ShowNewService() {

            document.getElementById("divNewService").style.display = 'block';
            document.getElementById('<%=lblNewServStatus.ClientID%>').innerHTML = '';

    }

    function HideNewService() {

        document.getElementById("divNewService").style.display = 'none';

    }


    function ShowHistory() {

        document.getElementById("divHistory").style.display = 'block';


    }

    function HideHistory() {

        document.getElementById("divHistory").style.display = 'none';

    }
</script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <div>
     <div>
            <div style="padding-left: 40%; width: 100%;">
                   <div id="divNewService" style="display: none; border: groove; height: 250px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:50px;">
                            <table cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <td style="vertical-align: top;">
                                              
                                            </td>
                                            <td align="right" style="vertical-align: top;">

                                                <input type="button" id="Button1" class="ButtonStyle"  color: black; border: none;" value=" X " onclick="HideNewService()" />
                                            </td>
                                        </tr>
                                  </table>
                       <fieldset>
                           <legend  class="lblCaption1" >Add New Drug</legend>
                       
                          <table cellpadding="5" cellspacing="5" width="100%" >
                                <tr>
                                    <td colspan="2">
                                          <asp:UpdatePanel ID="updatePanel27" runat="server" >
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblNewServStatus" runat="server" CssClass="lblCaption" ForeColor="Red"  Visible="false"></asp:Label>
                                                    </ContentTemplate>
                                          </asp:UpdatePanel>
                                    </td>
                                </tr>
                                       <tr>
                                           <td class="lblCaption1" style="width:100px;"  >
                                               Drug Code

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel28" runat="server">
                                                    <ContentTemplate>
                                                <asp:TextBox ID="txtNewServCode" runat="server" Width="150px" MaxLength="15" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                       <tr>
                                            <td class="lblCaption1"  style="width:100px;"  >
                                             Drug Name

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel29" runat="server">
                                                    <ContentTemplate>
                                               <asp:TextBox ID="txtNewServDesc" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"    ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                        <tr>
                                            <td class="lblCaption1"  style="width:100px;"  >
                                             Product Name

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel31" runat="server">
                                                    <ContentTemplate>
                                               <asp:TextBox ID="txtNewProdName" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"    ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                      <tr>
                                                <td></td>
                                                <td>
                                                     <asp:UpdatePanel ID="updatePanel30" runat="server">
                                                    <ContentTemplate>
                                                    <asp:Button ID="btnAddNewService" runat="server" Text="Add" OnClick="btnAddNewService_Click" CssClass="button orange small"  />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                </td>
                                       </tr>
                       </table>

                           </fieldset>

                    </div>

            </div>
         <table style="width:100%;" border="0">
            <tr>
                <td style="width: 50%; vertical-align: top;">
<table style="width: 50%">
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel18">
                <ContentTemplate>
                    <asp:Button ID="btnAddFav" runat="server" CssClass="orange" Style="width: 70px; border-radius: 5px 5px 5px 5px" Text="Add Fav." OnClick="btnAddFav_Click" />
                    <asp:Button ID="btnDeleteFav" runat="server" CssClass="orange" Style="width: 70px; border-radius: 5px 5px 5px 5px" Text="Delete Fav." OnClick="btnDeleteFav_Click" Visible="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel17">
                <ContentTemplate>
                    <asp:DropDownList ID="drpSrcType" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpSrcType_SelectedIndexChanged">
                        <asp:ListItem Selected="True">Full List</asp:ListItem>
                        <asp:ListItem>Favorite Only</asp:ListItem>
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanelPha">
                <ContentTemplate>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="lblCaption1" OnKeyUp="refreshPanelPha(this.value);" onfocus="ShowSearchPha();"></asp:TextBox>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="txtSearch" />

                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>

</table>
</td>

                <td style="width: 5px;"></td>
                  <td style="width: 50%; vertical-align: top;">

                        <input type="button" id="btnNewService" value="Add New Drug" class="orange" runat="server" visible="false" title="Add New Drug" style="width:120px;border-radius:5px 5px 5px 5px"  onclick="ShowNewService()" />

                  </td>
            </tr>
        </table>
<table>
    <tr>
        <td style="width: 50%; vertical-align: top;">

            <div style="float: left;">
                <table style="width: 100%">

                    <tr>
                        <td colspan="3">
                            <div style="padding-top: 0px; width: 100%; height: 600px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                <asp:UpdatePanel runat="server" ID="updatePanel1">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                            EnableModelValidation="True" OnPageIndexChanging="gvServicess_PageIndexChanging" GridLines="None" Width="100%">
                                            <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                            <RowStyle CssClass="GridRow" />
                                            <AlternatingRowStyle CssClass="GridAlterRow" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="lnkAdd" runat="server" ToolTip="Add" ImageUrl="~/EMR/Images/AddButton.jpg" Height="18" Width="18"
                                                            OnClick="Add_Click" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top" ItemStyle-VerticalAlign ="Top">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click">
                                                            <asp:Label ID="lblPrice" CssClass="label" BorderStyle="None" Visible="false" runat="server" Font-Size="11px" Width="70px" Style="text-align: right; padding-right: 5px;" Text='<%# Bind("Price") %>'></asp:Label>

                                                            <asp:Label ID="lblCode" CssClass="label" BorderStyle="None" runat="server" Font-Size="11px" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDesc" runat="server" OnClick="Edit_Click"  ToolTip='<%# Bind("Description") %>'>
                                                            <asp:Label ID="lblDesc" CssClass="label" BorderStyle="None" Width="95%" runat="server" Font-Size="11px" Text='<%# Bind("Description") %>' MaxLength="10" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblProdName" CssClass="label" BorderStyle="None" Width="95%" runat="server" Font-Size="11px" Text='<%# Bind("ProductName") %>' MaxLength="10" Visible="false"></asp:Label>

                                                          <%# Eval("ProductName") %>

                                                          <%#  Convert.ToString(Eval("Description")).Length > 100 ? Convert.ToString(Eval("Description")).Substring(0,100) : Eval("Description") %>
                                                           
                                                        </asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerStyle CssClass="PagerStyle" />

                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>

        <td style="width: 10px;"></td>

        <td style="width: 50%; vertical-align: top;">
            <div>
                <table style="width: 100%">
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel runat="server" ID="updatePanel3">
                                <ContentTemplate>
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Description
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel2">
                                <ContentTemplate>
                                    
                                    <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                                    <asp:TextBox ID="txtServName" runat="server" Width="97%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>
                                        <div id="divwidth" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                       
                    </tr>
                    <tr>
                         <td class="lblCaption1">Taken
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel10">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpTaken" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1"> 
                            <asp:Label ID="lblDosage" runat="server" CssClass="=lblCaption1" Text="Dosage" ></asp:Label>
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel8">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDossage1" runat="server" CssClass="label" Width="120px" TextMode="MultiLine" Height="30px" style="resize:none;" ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>


                       

                    </tr>
                   
                    <tr>
                        <td class="lblCaption1">Units
                        </td>
                        <td>
                              <asp:UpdatePanel runat="server" ID="updatePanel9">
                                    <ContentTemplate>
                            <asp:TextBox ID="txtUnit" runat="server" Width="48px" Height="19px" Text="" CssClass="label" MaxLength="4" BorderWidth="1px" BorderColor="#cccccc"  onkeyup="BindInstructions();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:DropDownList ID="drpUnitType" CssClass="label" runat="server" Width="70px" BorderColor="#cccccc" Style="font-size: 10px;" onchange="BindInstructions();" >
                                <asp:ListItem Text="Tablet(s)" Value="01" Selected="True"></asp:ListItem>
                                  <asp:ListItem Text="ML" Value="04"></asp:ListItem>
                                <asp:ListItem Text="Pcs" Value="02"></asp:ListItem>
                                <asp:ListItem Text="Strip" Value="03" Enabled="false"></asp:ListItem>
                                <asp:ListItem Text="Drop" Value="05"></asp:ListItem>
                                 <asp:ListItem Text="Spray" Value="06"></asp:ListItem>
                            </asp:DropDownList>
                                        </ContentTemplate>
                                  </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Freqyency
                        </td>
                        <td>
                              <asp:UpdatePanel runat="server" ID="updatePanel12">
                                    <ContentTemplate>
                            <asp:TextBox ID="txtFreqyency" runat="server" Width="48px" Height="19px" Text="" CssClass="label" MaxLength="4" BorderWidth="1px" BorderColor="#cccccc"  onkeyup="BindInstructions();" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:DropDownList ID="drpFreqType" CssClass="label" runat="server" Width="70px" BorderColor="#cccccc" Style="font-size: 10px;" AutoPostBack="True"  onchange="BindInstructions();">
                                <asp:ListItem Text="Per Day" Value="04" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Per Hour" Value="05"></asp:ListItem>
                                <asp:ListItem Text="Per Week" Value="06"></asp:ListItem>
                                <asp:ListItem Text="" Value="8"></asp:ListItem>
                            </asp:DropDownList>
                                        </ContentTemplate>
                                  </asp:UpdatePanel>
                        </td>
                        </tr>
                    <tr>
                         <td class="lblCaption1">Duration
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel6">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDuration" runat="server" CssClass="label" Text="" Width="48px" onkeyup="BindInstructions();"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                     &nbsp; &nbsp; &nbsp; &nbsp;
                                    <asp:DropDownList ID="drpDurationType" CssClass="label" runat="server" Width="70px" BorderColor="#cccccc" Style="font-size: 10px;"   onchange="BindInstructions();">
                                        <asp:ListItem Text="" Value="8"> </asp:ListItem>
                                        <asp:ListItem Text="Day(s)" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Week(s)" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Month(s)" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Year(s)" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                         <td class="lblCaption1">QTY
                        </td>

                         <td>
                              <asp:UpdatePanel runat="server" ID="updatePanel7">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtQty" runat="server" CssClass="label" Width="120px" Text="1" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                         </td>
                    </tr>
                     <tr>
                         <td class="lblCaption1">Route of Admin
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel11">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpRoute" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1"> 
                        </td>
                         <td>
                             <asp:UpdatePanel runat="server" ID="updatePanel20">
                                <ContentTemplate>
                               <asp:CheckBox ID="chkSplInstruction" runat="server"  CssClass="label" Text="Special Instructions"  onchange="BindSplInstruction();"/>
                                      </ContentTemplate>
                            </asp:UpdatePanel>
                         </td>
                       
                         </tr>

                        <tr>
                            <td class="lblCaption1">Instructions 
                             
                            </td>
                            <td colspan="3">
                                <asp:UpdatePanel runat="server" ID="updatePanel13">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="label" TextMode="MultiLine" Width="97%" Height="30px" style="resize:none;"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    <tr>
                        <td  colspan="4">
                            <asp:UpdatePanel runat="server" ID="updatePanel4">
                                <ContentTemplate>
                                    <asp:Button ID="btnAddPhar" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAddPhar_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                    </table>
                 <table style="width: 100%" runat="server" id="tblAuth" visible="false">
                    <tr>
                        <td colspan="4" class="lblCaption1">
                             <asp:UpdatePanel runat="server" ID="updatePanel19">
                                <ContentTemplate>
                           eRx Referance No: &nbsp;
                           
                            <asp:Label ID="lbleAuthSRefNo" runat="server" CssClass="label"  Font-Bold="true" ></asp:Label>&nbsp;&nbsp;&nbsp;
                                  
                             Status: &nbsp;
                          
                            <asp:Label ID="lbleAuthStatus" runat="server" CssClass="label"   Font-Bold="true" ></asp:Label>
                                       </ContentTemplate>
                                  </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="lblCaption1">
                            Comment: &nbsp;
                             <asp:UpdatePanel runat="server" ID="updatePanel21">
                                <ContentTemplate>
                            <asp:Label ID="lbleAuthComment" runat="server" CssClass="label" ></asp:Label> 
                               </ContentTemplate>
                                  </asp:UpdatePanel>
                            
                        </td>
                    </tr>
                    <tr>
                        <td  >
                        <asp:UpdatePanel runat="server" ID="updatePanel15">
                                <ContentTemplate>
                                    <asp:Button ID="btnReqeAuth" runat="server" CssClass="orange" Width="120px" Text="Req. Authorization" OnClick="btnReqeAuth_Click" Style="width: 120px; border-radius: 5px 5px 5px 5px" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                         <td >
                        <asp:UpdatePanel runat="server" ID="updatePanel14">
                                <ContentTemplate>
                                    <asp:Button ID="btnReqhResubmit" runat="server" CssClass="orange" Width="120px" Text="Req. ReSubmit" OnClick="btnReqhResubmit_Click" onclientclick="return window.confirm('Do you want to Resubmit?')"  Style="width: 120px; border-radius: 5px 5px 5px 5px" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td colspan="2"  >
                        <asp:UpdatePanel runat="server" ID="updatePanel16">
                                <ContentTemplate>
                                    <asp:Button ID="btnRefresheAuth" runat="server" CssClass="orange" Width="50px" Text="Refresh" OnClick="btnRefresheAuth_Click" Style="width: 120px; border-radius: 5px 5px 5px 5px" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                     </table>
                   <table style="width: 100%">
                    <tr>
                        <td colspan="4" class="lblCaption1">
                            <div id="divHistoryDrp" runat="server" visible="false">
                            History
                             <asp:UpdatePanel runat="server" ID="updatePanel35">
                                    <ContentTemplate>

                             <asp:DropDownList ID="drpHistory" CssClass="label" runat="server" Width="300px" BorderColor="#cccccc">
                            </asp:DropDownList>
                         
                              <asp:ImageButton  ID="imgbtnPrescHistView"    runat="server" ToolTip="View selected Prescription" ImageUrl="~//Images/zoom.png" 
                                                   OnClick="imgbtnPrescHistView_Click"  style="height:15px;width:25px;"  />
                              &nbsp;&nbsp; 
                              <asp:ImageButton  ID="imgbtnDown"    runat="server" ToolTip="Click here to Import selected Prescription" ImageUrl="~/EMR/Images/Down.gif" 
                                                   OnClick="imgbtnDown_Click"  />

                                        </ContentTemplate>
                                 </asp:UpdatePanel>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="lblCaption1">
                             <div id="divTemplate" runat="server" visible="false">
                                 Template
                        
                            <asp:DropDownList ID="drpTemplate" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>

                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label orange" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px" CssClass="orange" />
                           </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                <asp:UpdatePanel runat="server" ID="updatePanel5">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                            EnableModelValidation="True" GridLines="None">
                                            <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                            <RowStyle CssClass="GridRow" />
                                            <AlternatingRowStyle CssClass="GridAlterRow" />

                                            <Columns>
                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                            OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Code & Description" HeaderStyle-Width="50%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPhyDesc" runat="server" OnClick="PhySelect_Click">
                                                            <asp:Label ID="lblPhyLifeLong" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_LIFE_LONG") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyStDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_START_DATEDesc") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyEndDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_END_DATEDesc") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REMARKS") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_TIME") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyRefil" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REFILL") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyDossage1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE1") %>'  Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_ROUTE") %>'></asp:Label>

                                                          
                                                            <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_NAME") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_CODE") %>'></asp:Label> <br />
                                                           <asp:Label ID="lblPhyDescDisplay" CssClass="label" BorderStyle="None" Width="95%" runat="server" Font-Size="11px"  Text='<%# Convert.ToString(Eval("EPP_PHY_NAME")).Length > 100 ? Convert.ToString(Eval("EPP_PHY_NAME")).Substring(0,100) : Eval("EPP_PHY_NAME") %>' ></asp:Label>

                                                        </asp:LinkButton>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPhyQTY" runat="server" OnClick="PhySelect_Click">
                                                            <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

             
                                                
                                              
                                                <asp:TemplateField HeaderText="Units" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkUnit" runat="server" OnClick="PhySelect_Click">
                                                            <asp:Label ID="lblPhyUnit" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_UNIT") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyUnitTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_UNITTYPEDesc") %>'></asp:Label>
                                                            <asp:Label ID="lblPhyUnitType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_UNITTYPE") %>' Visible="false"></asp:Label>

                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Frequency" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkFrequency" runat="server" OnClick="PhySelect_Click">
                                                            <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_FREQUENCYTYPEDesc") %>'></asp:Label>
                                                            <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_FREQUENCYTYPE") %>' Visible="false"></asp:Label>

                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPhyDuration" runat="server" OnClick="PhySelect_Click">
                                                            <asp:Label ID="lblPhyDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION") %>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPEDesc") %>'></asp:Label>
                                                            <asp:Label ID="lblPhyDurationType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPE") %>' Visible="false"></asp:Label>

                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>

                    </tr>
                </table>
            </div>

        </td>
    </tr>
</table>

          <div style="padding-left: 40%; width: 100%;">
               <div id="divHistory" style=" display: none;border: groove; height: 350px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:250px;">
                     <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">
                                     <asp:UpdatePanel runat="server" ID="updatePanel33">
                                    <ContentTemplate>
                                    <input type="button" id="btnHistoryClose" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideHistory()" />
                                        </ContentTemplate>
                                         </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                         <asp:UpdatePanel runat="server" ID="updatePanel34">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   GridLines="None"   Width="100%"  >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
 <Columns>
                   <asp:TemplateField HeaderText="Code & Description" HeaderStyle-Width="50%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                       
                                                            <asp:Label ID="lblPhyLifeLong" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_LIFE_LONG") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyStDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_START_DATEDesc") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyEndDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_END_DATEDesc") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REMARKS") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_TIME") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyRefil" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REFILL") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyDossage1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE1") %>'  Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_ROUTE") %>'></asp:Label>

                                                          
                                                            <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_NAME") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_CODE") %>'></asp:Label> <br />
                                                           <asp:Label ID="lblPhyDescDisplay" CssClass="label" BorderStyle="None" Width="95%" runat="server" Font-Size="11px"  Text='<%# Convert.ToString(Eval("EPP_PHY_NAME")).Length > 100 ? Convert.ToString(Eval("EPP_PHY_NAME")).Substring(0,100) : Eval("EPP_PHY_NAME") %>' ></asp:Label>

                                                        

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        
                                                            <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

             
                                                
                                              
                                                <asp:TemplateField HeaderText="Units" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        
                                                            <asp:Label ID="lblPhyUnit" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_UNIT") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyUnitTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_UNITTYPEDesc") %>'></asp:Label>
                                                            <asp:Label ID="lblPhyUnitType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_UNITTYPE") %>' Visible="false"></asp:Label>

                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Frequency" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        
                                                            <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_FREQUENCYTYPEDesc") %>'></asp:Label>
                                                            <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_FREQUENCYTYPE") %>' Visible="false"></asp:Label>

                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        
                                                            <asp:Label ID="lblPhyDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION") %>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPEDesc") %>'></asp:Label>
                                                            <asp:Label ID="lblPhyDurationType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPE") %>' Visible="false"></asp:Label>

                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                           </Columns>
    </asp:GridView>
                                    </ContentTemplate>
                                        
                                        </asp:UpdatePanel>
                                         

                                  
               </div>
           
           </div>
<asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td>Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
<style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>

    </div>
    </form>
</body>
</html>
