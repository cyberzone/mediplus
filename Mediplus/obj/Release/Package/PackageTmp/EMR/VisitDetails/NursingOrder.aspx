﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NursingOrder.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.NursingOrder1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
  <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">


        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


</script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <input type="hidden" id="hidCreateTempType" runat="server" />
<div id="NursingOrderContainer">
    
        <fieldset>
            <legend class="lblCaption1" style="font-weight:bold;" >Nurse Instruction</legend>
            <table style="width:100%" >   
                 <tr>
                <td colspan="2" >
                     <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="button orange small" Text="Create Template" Enabled="false"   
                                Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                             <asp:LinkButton ID="lnkDisplayTemp" runat="server" CssClass="button orange small" Text="Select Template" Enabled="false"   
                                Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                </td>
            </tr>             
                <tr>
                    <td class="lblCaption1"><label for="Instruction">Instruction</label></td>
                    <td><input type="text" name="Instruction" id="Instruction" runat="server" style="width:90%;" /></td>
                </tr>
                <tr>
                     <td class="lblCaption1"><label for="Instruction">Date</label></td>
                    <td   class="lblCaption1"> 
                    <asp:TextBox ID="txtFromDate" runat="server" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                   
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                    &nbsp Next 
                <asp:TextBox ID="txtNextDays" runat="server" Width="30px" MaxLength="2"   onKeypress="return OnlyNumeric(event);" Text="0"></asp:TextBox>
                         &nbsp; days
                    </td>
                </tr>
                
                <tr id="trNurseComment" runat="server" visible="false" >
                    <td class="lblCaption1"><label for="Comments">Comments</label></td>
                    <td><input type="text" name="Comments" id="Comments" runat="server" style="width:90%" /></td>
                </tr>
                 <tr id="trNurseTime" runat="server" visible="false"  >
                    <td class="lblCaption1"><label for="Comments">Date & Time</label></td>
                    <td> 
                         <asp:textbox id="txtTreatmentDate" runat="server" width="95px" height="22px" cssclass="label" borderwidth="1px" bordercolor="#cccccc" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                        <asp:textbox id="txtTreatmentTime" runat="server" readonly="false" width="30px" height="22px" cssclass="label" borderwidth="1px" bordercolor="#cccccc" maxlength="5"></asp:textbox>
                        <asp:calendarextender id="Calendarextender3" runat="server"
                        enabled="True" targetcontrolid="txtTreatmentDate" format="dd/MM/yyyy">
                        </asp:calendarextender>
                       <asp:maskededitextender id="MaskedEditExtender3" runat="server" enabled="true" targetcontrolid="txtTreatmentTime" mask="99:99" masktype="Time"></asp:maskededitextender>
                          <asp:DropDownList ID="drpAM" runat="server" CssClass="label"  height="22px" >
                            <asp:ListItem Text="AM" Value="AM" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
               
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" Text="Save" OnClick="btnSave_Click" />
                         
                    </td>
                </tr>
            </table>
        </fieldset>
    
                        <asp:GridView ID="gvNursingInsttruction" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                             <HeaderStyle CssClass="GridHeader" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                          <ItemTemplate>
                                                  <%   if (Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE")   { %>
                                                <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                      OnClick="DeletegInst_Click" />&nbsp;&nbsp;
                                                 <%} %>
                                          </ItemTemplate>
                                  <HeaderStyle Width="50px" />
                                  <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPC_DATEDesc") %>'></asp:Label>
                                          </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkEMRID" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblEMRID" CssClass="label" runat="server" Text='<%# Bind("EPC_ID") %>' Visible="false"  ></asp:Label>
                                        <asp:Label ID="lblInstructionID" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTRUCTION_ID") %>' Visible="false"  ></asp:Label>
                                            <asp:Label ID="lblInsttruction" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkComment" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblComment" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>
                                          </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Username"  HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkuser" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="Label19" CssClass="label" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                      </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Time"  HeaderStyle-Width="150px"> 
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkTime" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblTime" CssClass="label" runat="server" Text='<%# Bind("EPC_TIME") %>'></asp:Label>
                                             </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                               
                                 

                               
                    </Columns>

                </asp:GridView>

      <br />
                <br />
                <span class="lblCaption1" style="font-weight:bold;">History</span><br />
                <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

                     <asp:GridView ID="gvNursingOrderHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                             <HeaderStyle CssClass="GridHeader" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblgvHistDate" CssClass="label" runat="server" Text='<%# Bind("EPC_DATEDesc") %>'></asp:Label>
                                         
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>
                                        
                                      
                                            <asp:Label ID="lblgvHistInsttruction" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblgvHistComment" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>
                                         
                                    </ItemTemplate>

                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Username"  HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblgvHistUser" CssClass="label" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                     
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Time"  HeaderStyle-Width="150px"> 
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblgvHistTime" CssClass="label" runat="server" Text='<%# Bind("EPC_TIME") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                               
                                 

                               
                    </Columns>

                </asp:GridView>
                 </div>
</div>

        <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
 
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Template List
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" style="width:250px;" />
                            </td>
                        </tr>
                        
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtender6" runat="server" TargetControlID="lnkDisplayTemp"
    PopupControlID="pnlDisplayTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnDisplayClose"
    PopupDragHandleControlID="pnlDisplayTemplate">
</asp:ModalPopupExtender>

<asp:Panel ID="pnlDisplayTemplate" runat="server" Height="500px" Width="300px" CssClass="modalPopup">
 <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnDisplayClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Template List
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 400px; vertical-align: top;">
                  <div style="padding-top: 0px; width: 100%;height:390px;   overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <asp:GridView ID="gvTemplates" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                    EnableModelValidation="True" Width="290px" ShowHeader="false"  >
                                    <HeaderStyle CssClass="GridHeader"   />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />

                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Width="90%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkName" runat="server" OnClick="TempSelect_Click">
                                                <asp:Label ID="lblTempCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_CODE") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblTempData" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_TEMPLATE") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lbTempName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=""  HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvDeleteTemp1" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="15" Width="15"
                                                OnClick="gvDeleteTemp1_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                      </div>
           </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnDisplayClose1" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>

    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    </form>
</body>
</html>