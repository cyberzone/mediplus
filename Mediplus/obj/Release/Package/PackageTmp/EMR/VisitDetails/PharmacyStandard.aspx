﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PharmacyStandard.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.PharmacyStandard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
     
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>





</head>

<script language="javascript" type="text/javascript">
    function refreshPanelPha1(strValue) {
        // alert(strValue)
        __doPostBack(strValue, "Pharmacy1");

    }

    function setCursorPha1(el, st, end) {
        if (el.setSelectionRange) {
            el.focus();
            el.setSelectionRange(st, end);
        } else {
            if (el.createTextRange) {
                range = el.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', st);
                range.select();
            }
        }
    }

    function ShowSearchPha1(strName) {
        var elem = strName;

        var elemLen = elem.value.length;
        //alert(elemLen);
        setCursorPha1(elem, elemLen, elemLen)
    }

</script>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server"></asp:ToolkitScriptManager>
        <div>
            <div style="padding-left: 40%; width: 100%;">
                   <div id="divNewService" style="display: none; border: groove; height: 250px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:50px;">
                            <table cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <td style="vertical-align: top;">
                                              
                                            </td>
                                            <td align="right" style="vertical-align: top;">

                                                <input type="button" id="Button1" class="ButtonStyle"  color: black; border: none;" value=" X " onclick="HideNewService()" />
                                            </td>
                                        </tr>
                                  </table>
                       <fieldset>
                           <legend  class="lblCaption1" >Add New Drug</legend>
                       
                          <table cellpadding="5" cellspacing="5" width="100%" >
                                <tr>
                                    <td colspan="2">
                                          <asp:UpdatePanel ID="updatePanel27" runat="server" >
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblNewServStatus" runat="server" CssClass="lblCaption" ForeColor="Red"  Visible="false"></asp:Label>
                                                    </ContentTemplate>
                                          </asp:UpdatePanel>
                                    </td>
                                </tr>
                                       <tr>
                                           <td class="lblCaption1" style="width:100px;"  >
                                               Drug Code

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel28" runat="server">
                                                    <ContentTemplate>
                                                <asp:TextBox ID="txtNewServCode" runat="server" Width="150px" MaxLength="15" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                       <tr>
                                            <td class="lblCaption1"  style="width:100px;"  >
                                             Drug Name

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel29" runat="server">
                                                    <ContentTemplate>
                                               <asp:TextBox ID="txtNewServDesc" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"    ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                        <tr>
                                            <td class="lblCaption1"  style="width:100px;"  >
                                             Product Name

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel31" runat="server">
                                                    <ContentTemplate>
                                               <asp:TextBox ID="txtNewProdName" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"    ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                      <tr>
                                                <td></td>
                                                <td>
                                                     <asp:UpdatePanel ID="updatePanel30" runat="server">
                                                    <ContentTemplate>
                                                    <asp:Button ID="btnAddNewService" runat="server" Text="Add" OnClick="btnAddNewService_Click" CssClass="button orange small"  />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                </td>
                                       </tr>
                       </table>

                           </fieldset>

                    </div>

            </div>
            <table style="width:100%;" border="0">
            <tr>
                <td style="width: 50%; vertical-align: top;">
                    <table style="width: 50%">
            <tr>
                <td>
                      <asp:UpdatePanel runat="server" ID="updatePanel18">
                        <ContentTemplate>
                    <asp:Button ID="btnAddFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px" Text="Add Fav." OnClick="btnAddFav_Click" />
                    <asp:Button ID="btnDeleteFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px" Text="Delete Fav." OnClick="btnDeleteFav_Click" Visible="false" />
                              </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
                <td>
                      <asp:UpdatePanel runat="server" ID="updatePanel17">
                        <ContentTemplate>
                    <asp:DropDownList ID="drpSrcType" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpSrcType_SelectedIndexChanged">
                        <asp:ListItem Selected="True">Full List</asp:ListItem>
                        <asp:ListItem>Favorite Only</asp:ListItem>
                    </asp:DropDownList>
                              </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
                <td>
                     <asp:UpdatePanel runat="server" ID="updatePanelPha1">
                        <ContentTemplate>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="lblCaption1" Width="300px"  OnKeyUp="refreshPanelPha1(this.value);"  onfocus="ShowSearchPha1(this);"  ></asp:TextBox>
                            </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="txtSearch" />
                   
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
                </td>

                <td style="width: 5px;"></td>
                  <td style="width: 50%; vertical-align: top;">

                        <input type="button" id="btnNewService" value="Add New Drug" class="orange" runat="server" visible="false" title="Add New Drug" style="width:120px;border-radius:5px 5px 5px 5px"  onclick="ShowNewService()" />
                      <asp:UpdatePanel runat="server" ID="updatePanel32">
                                <ContentTemplate>

                      <asp:RadioButtonList ID="radDrugSrcType" runat="server" RepeatDirection="Horizontal" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="radDrugSrcType_SelectedIndexChanged"  >
                          <asp:ListItem Text="All" Value="A" ></asp:ListItem>
                          <asp:ListItem Text="Generic Name" Value="G"></asp:ListItem>
                          <asp:ListItem Text="Trade Name" Value="T"></asp:ListItem>
                      </asp:RadioButtonList>
                                   </ContentTemplate>
                          </asp:UpdatePanel>
                  </td>
            </tr>
        </table>
<table>
    <tr>
        <td style="width: 50%; vertical-align: top;">

            <div style="float: left;">
                <table style="width: 100%">

                    <tr>
                        <td colspan="3">
                            <div style="padding-top: 0px; width: 100%;height:600px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                 <asp:UpdatePanel runat="server" ID="updatePanel1">
                                    <ContentTemplate>
                                <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                    EnableModelValidation="True"   OnPageIndexChanging="gvServicess_PageIndexChanging" GridLines="None"   Width="100%" >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText=""  HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                             <asp:ImageButton ID="lnkAdd" runat="server" ToolTip="Add" ImageUrl="~/EMR/Images/AddButton.jpg" Height="18" Width="18"
                                                OnClick="Add_Click" />
                                        </ItemTemplate>
                                           <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left"  ItemStyle-VerticalAlign ="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click">
                                                      <asp:Label ID="lblPrice" CssClass="label" BorderStyle="None" Visible="false"  runat="server" Font-Size="11px" Width="70px" Style="text-align: right; padding-right: 5px;" Text='<%# Bind("Price") %>'></asp:Label>

                                                    <asp:Label ID="lblCode" CssClass="label" BorderStyle="None" runat="server" Font-Size="11px" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="100%" HeaderStyle-HorizontalAlign="Left"  ItemStyle-VerticalAlign ="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDesc" runat="server" OnClick="Edit_Click">
                                                    <asp:Label ID="lblDesc" CssClass="label" BorderStyle="None"  Width="95%" runat="server" Font-Size="11px" Text='<%# Bind("Description") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblProdName" CssClass="label" BorderStyle="None" Width="95%" runat="server" Font-Size="11px" Text='<%# Bind("ProductName") %>' MaxLength="10" Visible="false"></asp:Label>
                                                     <%#  Convert.ToString(Eval("Description")).Length > 100 ? Convert.ToString(Eval("Description")).Substring(0,100) : Eval("Description") %>
                                                           

                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        
                                    </Columns>
                                    <PagerStyle CssClass="PagerStyle" /> 

                                </asp:GridView>
                                          </ContentTemplate>
                                 </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>

        <td style="width: 5px;"></td>

        <td style="width: 50%; vertical-align: top;">
            <div>
                <table style="width: 100%">
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel runat="server" ID="updatePanel3" >
                                  <ContentTemplate>
                                     <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label" ></asp:Label>
                                  </ContentTemplate>
                              </asp:UpdatePanel>
                        </td>
                    </tr>
                     <tr>
                        <td class="lblCaption1">Description
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel19">
                                <ContentTemplate>
                                    
                                    <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" ></asp:TextBox>
                                    <asp:TextBox ID="txtServName" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>
                                        <div id="divwidth" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                       
                    </tr>
                    <tr>
                        <td class="lblCaption1">Duration
                        </td>
                        <td>
                             <asp:UpdatePanel runat="server" ID="updatePanel6" >
                                  <ContentTemplate>
                            <asp:TextBox ID="txtDuration" runat="server" CssClass="label" Width="55px" onkeypress="return OnlyNumeric(event);" AutoPostBack="true"  OnTextChanged="txtDuration_TextChanged"></asp:TextBox>
                           <span class="lblCaption1">Type</span> 
                            <asp:DropDownList ID="drpDurationType" CssClass="label" runat="server" Width="60px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpDurationType_SelectedIndexChanged" >
                                <asp:ListItem Text="" Value="8" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Day(s)" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Week(s)" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Month(s)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Year(s)" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                                      </ContentTemplate>
                                 </asp:UpdatePanel>
                        </td>

                            <td class="lblCaption1" style="vertical-align:top;" rowspan="2"  >Dosage
                        </td>
                        <td style="vertical-align:top;"  rowspan="2">
                              <asp:UpdatePanel runat="server" ID="updatePanel8" >
                                  <ContentTemplate>
                            <asp:TextBox ID="txtDossage1" runat="server" CssClass="label" Width="200px" TextMode="MultiLine" Height="40px" style="resize:none;" ></asp:TextBox>
                                    </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Quantity
                        </td>
                        <td>
                             <asp:UpdatePanel runat="server" ID="updatePanel7" >
                                  <ContentTemplate>
                          <asp:TextBox ID="txtQty" runat="server" CssClass="label" Width="150px" Text="1" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                     </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                       <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Time
                        </td>
                        <td>
                              <asp:UpdatePanel runat="server" ID="updatePanel9" >
                                  <ContentTemplate>
                            <asp:DropDownList ID="drpDossage" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                            </asp:DropDownList>
                                        </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Taken
                        </td>
                        <td>
                              <asp:UpdatePanel runat="server" ID="updatePanel10" >
                                  <ContentTemplate>
                            <asp:DropDownList ID="drpTaken" CssClass="label" runat="server" Width="205px" BorderColor="#cccccc">
                            </asp:DropDownList>
                              </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Route
                        </td>
                        <td>
                              <asp:UpdatePanel runat="server" ID="updatePanel11" >
                                  <ContentTemplate>
                            <asp:DropDownList ID="drpRoute" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                            </asp:DropDownList>
                                    </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Refil
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel12" >
                                  <ContentTemplate>
                            <asp:TextBox ID="txtRefil" runat="server" CssClass="label" Width="200px"></asp:TextBox>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Start Date
                        </td>
                        <td>
                             <asp:UpdatePanel runat="server" ID="updatePanel15" >
                                  <ContentTemplate>
                            <asp:TextBox ID="txtFromDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                   
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
 </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">End Date

                        </td>
                        <td>
                             <asp:UpdatePanel runat="server" ID="updatePanel16" >
                                  <ContentTemplate>
                            <asp:TextBox ID="txtToDate" runat="server" Width="200px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
  </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Remarks 
                             
                        </td>
                        <td colspan="3">
                             <asp:UpdatePanel runat="server" ID="updatePanel13" >
                                  <ContentTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="label" TextMode="MultiLine" Width="98%" Height="30px" style="resize:none;" ></asp:TextBox>
                                      </ContentTemplate>
                                 </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">  
                             
                        </td>
                        <td  class="lblCaption1" >
                             <asp:UpdatePanel runat="server" ID="updatePanel14" >
                                  <ContentTemplate>
                                             <asp:CheckBox ID="chkLifeLong" runat="server" Text="Life Long"  CssClass="lblCaption1" AutoPostBack="true"  OnCheckedChanged="chkLifeLong_CheckedChanged" />
                                   </ContentTemplate>
                                 </asp:UpdatePanel>
                        </td>
                        <td></td>
                        <td >
                             <asp:Button ID="btnPrescriptioTODB" runat="server" Text="Save Prescription To DB" OnClick="btnPrescriptioTODB_Click" style="width:150px;border-radius:5px 5px 5px 5px" CssClass="orange" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                             <asp:UpdatePanel runat="server" ID="updatePanel4" >
                                  <ContentTemplate>
                            <asp:Button ID="btnAddPhar" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAddPhar_Click" style="width:50px;border-radius:5px 5px 5px 5px"   />
                                  </ContentTemplate>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                      
                        <td colspan="4" class="lblCaption1">
                            <div id="divHistoryDrp" runat="server" visible="false">
                            History
                             <asp:UpdatePanel runat="server" ID="updatePanel35">
                                    <ContentTemplate>

                             <asp:DropDownList ID="drpHistory" CssClass="label" runat="server" Width="300px" BorderColor="#cccccc">
                            </asp:DropDownList>
                         
                              <asp:ImageButton  ID="imgbtnPrescHistView"    runat="server" ToolTip="View selected Prescription" ImageUrl="~//Images/zoom.png" 
                                                   OnClick="imgbtnPrescHistView_Click"  style="height:15px;width:25px;"  />
                              &nbsp;&nbsp; 
                              <asp:ImageButton  ID="imgbtnDown"    runat="server" ToolTip="Click here to Import selected Prescription" ImageUrl="~/EMR/Images/Down.gif" 
                                                   OnClick="imgbtnDown_Click"  />

                                        </ContentTemplate>
                                 </asp:UpdatePanel>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="lblCaption1">
                             <div id="divTemplate" runat="server" visible="false">
                                 Template
                        
                            <asp:DropDownList ID="drpTemplate" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>

                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label orange" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px" CssClass="orange" />
                           </div>
                        </td>
                    </tr>
                    </table>
                <div style="padding-left: 40%; width: 100%;">
                                <div id="divPhyScedule" style="display: none; border: groove; height: 350px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:250px;">

                                    <table cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <asp:UpdatePanel ID="updatePanel22" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblSheduleMsg" runat="server" CssClass="lblCaption" ForeColor="Red"  Visible="false"></asp:Label>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td align="right" style="vertical-align: top;">

                                                <input type="button" id="btnMsgClose" class="ButtonStyle"  style="color: black; border: none;" value=" X " onclick="HidePhyScedule()" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                   <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                                                        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Shedule" Width="100%">
                                                            <ContentTemplate>
                                              
                                                    <table width="100%">

                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend class="lblCaption1" ">Pharmacy Schedule</legend>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                              <td class="lblCaption1" style="width: 100px;vertical-align:top;" >Description 
                                                                            </td>
                                                                            <td>
                                                                                 <asp:UpdatePanel ID="updatePanel24" runat="server">
                                                                                    <ContentTemplate>
                                                                                <asp:Label ID="lblSheMedicinCode" runat="server"  CssClass="label" ForeColor="Black"  ></asp:Label><br />
                                                                                <asp:Label ID="lblSheMedicinDesc" runat="server"  CssClass="label" ForeColor="Black" ></asp:Label>
                                                                                        </ContentTemplate>
                                                                                     </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td class="lblCaption1" style="width: 100px; ">Name  
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel20" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="drpWardNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="lblCaption1" style="width: 150px; ">Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel21" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtWardNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="lblCaption1" ">Date & Time
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel2" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtSheduleDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                                                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                                                                            Enabled="True" TargetControlID="txtSheduleDate" Format="dd/MM/yyyy">
                                                                                        </asp:CalendarExtender>
                                                                                        <asp:DropDownList ID="drpSheduleHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                                        <asp:DropDownList ID="drpSheduleMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td class="lblCaption1" >Remarks
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel23" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtSheduleComment" runat="server" Width="300px" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel25" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:Button ID="btnPhyScheduleAdd" runat="server" CssClass="orange" Text="Add" Width="50px" Style="width: 50px; border-radius: 5px 5px 5px 5px" OnClick="btnPhyScheduleAdd_Click" />
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                
                                                              </ContentTemplate>
                                                    </asp:TabPanel>
                                                    <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="History" Width="100%">
                                                        <ContentTemplate>
                                                             <div style="padding-top: 0px; width: 100%;height:300px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                                    <asp:UpdatePanel runat="server" ID="updatePanel26">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvPhySchedule" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
                                                                EnableModelValidation="True" GridLines="None" Width="100%">
                                                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                                <RowStyle CssClass="GridRow" />

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" >
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPhySheDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPS_DATEDesc") %>'></asp:Label>
                                                                            <asp:Label ID="lblPhySheTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPS_DATETime") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblPhySheGivenBy" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPS_GIVENBYDesc") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblPhySheRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPS_REMARKS") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                              </div>
                                                          </ContentTemplate>
                                                </asp:TabPanel>
                                              </asp:TabContainer>
                                            </td>
                                        </tr>
                                        
                                    </table>

                                </div>
                            </div>
                   <table style="width: 100%">
                    <tr>
                        <td >
                            <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                <asp:UpdatePanel runat="server" ID="updatePanel5" >
                                  <ContentTemplate>
                                <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                    EnableModelValidation="True"  GridLines="None"  OnRowDataBound="gvPharmacy_RowDataBound"  >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />

                                    <Columns>
                                       <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Code & Description" HeaderStyle-Width="50%"   HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhyDesc" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblPhyLifeLong" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_LIFE_LONG") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblPhyStDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_START_DATEDesc") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblPhyEndDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_END_DATEDesc") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REMARKS") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_TIME") %>' Visible="false"></asp:Label>
                                            

                                                <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_CODE") %>'></asp:Label><br />
                                                    <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_NAME") %>'></asp:Label>
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhyQTY" runat="server" OnClick="PhySelect_Click">
                                                    <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Dosage" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhyDosage" runat="server" OnClick="PhySelect_Click">
                                                      <asp:Label ID="lblPhyDossage1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE1") %>'></asp:Label>
                                                    <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE") %>' Visible="false"></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Route" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhyRoute" runat="server" OnClick="PhySelect_Click">
                                                    <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_ROUTE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblPhyRouteDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_ROUTEDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="Refil" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhyRefil" runat="server" OnClick="PhySelect_Click">
                                                    <asp:Label ID="lblPhyRefil" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REFILL") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhyDuration" runat="server" OnClick="PhySelect_Click">
                                                    <asp:Label ID="lblPhyDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION") %>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPEDesc") %>'></asp:Label>
                                                    <asp:Label ID="lblPhyDurationType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPE") %>' Visible="false"></asp:Label>                                              

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Don't Print" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left" >
                                            <ItemTemplate>
                                                 <asp:Label ID="lblPhyNoPrint" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_NOT_PRINT") %>' Visible="false"></asp:Label>  
                                                <asp:CheckBox ID="chkPrintNo" runat="server" AutoPostBack="true" OnCheckedChanged="chkPrintNo_CheckedChanged"   />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField>
                                               <ItemTemplate>
                                                 <asp:LinkButton ID="lnkTask" runat="server" OnClick="PhySchedule_Click" Text="Schedule">
                                                   </asp:LinkButton>
                                               </ItemTemplate>
                                          </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                                      </ContentTemplate>
                                 </asp:UpdatePanel>
                            </div>
                        </td>

                    </tr>
                </table>
            </div>

        </td>
    </tr>
</table>
  <div style="padding-left: 40%; width: 100%;">
               <div id="divHistory" style=" display: none;border: groove; height: 350px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:250px;">
                     <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">
                                     <asp:UpdatePanel runat="server" ID="updatePanel33">
                                    <ContentTemplate>
                                    <input type="button" id="btnHistoryClose" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideHistory()" />
                                        </ContentTemplate>
                                         </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                         <asp:UpdatePanel runat="server" ID="updatePanel34">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   GridLines="None"   Width="100%"  >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
 <Columns>
                                        <asp:TemplateField HeaderText="Code & Description" HeaderStyle-Width="50%"   HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                               <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_CODE") %>'></asp:Label><br />
                                               <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_PHY_NAME") %>'></asp:Label>
                                             

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                
                                                    <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>
                                               
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Dosage" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                               
                                                      <asp:Label ID="lblPhyDossage1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DOSAGE1") %>'></asp:Label>
                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Route" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                              
                                                    <asp:Label ID="lblPhyRouteDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_ROUTEDesc") %>'></asp:Label>
                                              
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="Refil" HeaderStyle-Width="5%" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                            <ItemTemplate>
                                              
                                                    <asp:Label ID="lblPhyRefil" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_REFILL") %>'></asp:Label>
                                             
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                
                                                    <asp:Label ID="lblPhyDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION") %>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPP_DURATION_TYPEDesc") %>'></asp:Label>
                                                    

                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        

                                    </Columns>
    </asp:GridView>
                                    </ContentTemplate>
                                        
                                        </asp:UpdatePanel>
                                         

                                  
               </div>
           
           </div>




<asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td>Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
   
    </div>
    </form>
</body>
<style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>

<script language="javascript" type="text/javascript">


    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }


</script>

<script language="javascript" type="text/javascript">

    function ServNameSelected() {
        if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
            var Data = document.getElementById('<%=txtServName.ClientID%>').value;
            var Data1 = Data.split('~');
            if (Data1.length > 0) {
                document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];

                 if (Data1.length == 2) {
                     document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }

                if (Data1.length == 3) {
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[2];
                }

            }
        }

        return true;
    }


    function ServNameSelected1() {

        var radiolist = document.getElementById('<%= radDrugSrcType.ClientID %>');
        var rblName = radiolist.name;
        var radio = document.getElementsByName(rblName);

        var strdDrugSrcType;


        strdDrugSrcType = $('#<%=radDrugSrcType.ClientID %> input[type=radio]:checked').val()


        if (document.getElementById('<%=drpSrcType.ClientID%>').value == 'Full List') {

            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                // alert(document.getElementById('<%=radDrugSrcType.ClientID%>').innerText);

                if (strdDrugSrcType == "A") {
                    if (Data1.length > 0) {
                        document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    }

                    if (Data1.length > 2) {
                        document.getElementById('<%=txtServName.ClientID%>').value = Data1[1] + "~" + Data1[2];
                    }
                }
                else {
                    if (Data1.length > 0) {
                        document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    }

                    if (Data1.length > 1) {
                        document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                    }


                }
            }
        }
        else {
            var Data = document.getElementById('<%=txtServName.ClientID%>').value;
            var Data1 = Data.split('~');

            if (Data1.length > 0) {
                document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
            }
            if (Data1.length > 1) {
                document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
            }
            if (Data1.length > 2) {
                document.getElementById('<%=txtServName.ClientID%>').value = Data1[1] + Data1[2];
            }

        }


        return true;
    }

    function ShowPhySchedule() {

        document.getElementById("divPhyScedule").style.display = 'block';
        HidegvPhyShedule();

    }

    function HidePhyScedule() {

        document.getElementById("divPhyScedule").style.display = 'none';

    }

    function ShowNewService() {

        document.getElementById("divNewService").style.display = 'block';
        document.getElementById('<%=lblNewServStatus.ClientID%>').innerHTML = '';

    }

    function HideNewService() {

        document.getElementById("divNewService").style.display = 'none';

    }

    function ShowHistory() {

        document.getElementById("divHistory").style.display = 'block';


    }

    function HideHistory() {

        document.getElementById("divHistory").style.display = 'none';

    }

</script>

</html>

