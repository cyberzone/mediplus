﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RadiologyResult.ascx.cs" Inherits="Mediplus.EMR.VisitDetails.RadiologyResult" %>
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
  <script language="javascript" type="text/javascript">


      function RadResultReport(TransNo) {

          var Report = "RadiologyReport.rpt";
          var Criteria = " 1=1 ";

          if (TransNo != "") {

              Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

              var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, 'RadiologyReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
              win.focus();


          }

      }
    </script>

 <asp:GridView ID="gvRadResult" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" AllowPaging="true" OnPageIndexChanging="gvRadResult_PageIndexChanging" PageSize="30">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />

                    <Columns>

                        <asp:TemplateField HeaderText="TransNo" HeaderStyle-Width="110px">
                            <ItemTemplate>
                                 <asp:LinkButton ID="lnkRadTransNo" runat="server"  OnClick="RadEdit_Click"      >
                                   <asp:Label ID="lblRadTransNo" CssClass="label" runat="server" Text='<%# Bind("RTR_TRANS_NO") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                 <asp:LinkButton ID="lnkRadTransDate" runat="server"  OnClick="RadEdit_Click"      >
                                <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("RTR_DATEDesc") %>'></asp:Label>
                                     </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                 <asp:LinkButton ID="lnkDesc" runat="server"  OnClick="RadEdit_Click"      >
                                <asp:Label ID="lblDesc" CssClass="label" runat="server" Text='<%# Bind("RTR_RAD_DESCRIPTION") %>'></asp:Label>
                                     </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Doctor">
                            <ItemTemplate>
                                 <asp:LinkButton ID="lnkRadTransDr" runat="server"  OnClick="RadEdit_Click"      >
                                <asp:Label ID="Label21" CssClass="label" runat="server" Text='<%# Bind("RTR_DR_NAME") %>'></asp:Label>
                                     </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>