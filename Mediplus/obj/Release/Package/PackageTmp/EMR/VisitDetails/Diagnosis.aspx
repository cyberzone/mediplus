﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Diagnosis.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.Diagnosis1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
 <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    
<script type="text/javascript" >

    function __DoPostBack(Code, Desc) {
        alert(Code);
        alert(Desc);

    }

    function ShowAlertMessage() {

        alert('Principal diagnosis already added');
    }


    function SaveEMRS() {
        var EMRS = {
            BranchID: $("#BranchID").val(),
            PatientMasterID: $("#PatientMasterID").val(),
            ExaminationType: $("#ExaminationType").val(),
            PageFrom: 'PatientDashboard'
        };
        $.ajax({
            url: getBaseURL() + 'EMRS/SaveEMRS',
            data: JSON.stringify(EMRS),
            type: 'POST',
            success: function (result) {
                if (result == "success") {
                    console.log("EMRS saved successfully...");
                } else {
                    console.log("Problem in saving emrs, due to result = " + result);
                }
            }
        });
    }

    function refreshPanel(strValue) {
        // alert(strValue)
        __doPostBack('<%= updatePanel.UniqueID %>', "TextData");

        //  SetCaretAtEnd();
        // var elem = document.getElementById("<%=txtSearch.ClientID%>")

        // var elemLen = elem.value.length;
        // alert(elemLen);
        // setCursor(elem, elemLen, elemLen)

        // $('#<%=txtSearch.ClientID%>').setCursorToTextEnd();
    }

    function SetCaretAtEnd() {

        var elem = document.getElementById("<%=txtSearch.ClientID%>")

        var elemLen = elem.value.length;

        // For IE Only
        if (document.selection) {
            // Set focus
            elem.focus();
            // Use IE Ranges
            var oSel = document.selection.createRange();
            // Reset position to 0 & then set at end
            oSel.moveStart('character', -elemLen);
            oSel.moveStart('character', elemLen);
            oSel.moveEnd('character', 0);
            oSel.select();
        }
        else if (elem.selectionStart || elem.selectionStart == '0') {
            // Firefox/Chrome
            // alert(elem.selectionStart);

            elem.selectionStart = elemLen;
            elem.selectionEnd = elemLen;
            elem.focus();


            //alert(elem.selectionStart);
            // alert(elem.selectionEnd);
        } // if
    } // SetCaretAtEnd(


    (function ($) {
        $.fn.setCursorToTextEnd = function () {
            var $initialVal = this.val();
            this.val($initialVal);
        };
    })(jQuery);



</script>
<script type="text/javascript">
    window.onload = function () {
        //  setCursor(document.getElementById('input1'), 13, 13)
        //setCursor(document.getElementById("<%=txtSearch.ClientID%>"), 6, 6)

    }

    function setCursor(el, st, end) {
        if (el.setSelectionRange) {
            el.focus();
            el.setSelectionRange(st, end);
        } else {
            if (el.createTextRange) {
                range = el.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', st);
                range.select();
            }
        }
    }

    function ShowSearch() {
        //  SetCaretAtEnd();

        var elem = document.getElementById("<%=txtSearch.ClientID%>")

        var elemLen = elem.value.length;
        //alert(elemLen);
        setCursor(elem, elemLen, elemLen)
    }

    function ShowNewService() {

        document.getElementById("divNewService").style.display = 'block';
        document.getElementById('<%=lblNewServStatus.ClientID%>').innerHTML = '';

    }

    function HideNewService() {

        document.getElementById("divNewService").style.display = 'none';

    }

    function ShowHistory() {

        document.getElementById("divHistory").style.display = 'block';


    }

    function HideHistory() {

        document.getElementById("divHistory").style.display = 'none';

    }



    function ShowDiagCrosswalkPopup() {

        document.getElementById("divDiagCrosswalk").style.display = 'block';


    }

    function HideDiagCrosswalkPopup() {

        document.getElementById("divDiagCrosswalk").style.display = 'none';

    }


    function DiagNameSelected() {
        if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
            var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
            var Data1 = Data.split('~');
            if (Data1.length > 0) {
                document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];

                if (Data1.length == 2) {
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }

                if (Data1.length == 3) {
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[2];
                 }

             }
         }

         return true;
     }

    function UpdateInvoiceConfirm() {


        var isMsgConfirm = window.confirm('Do you want to Replace the Diagnosis in Existing Invoice ?');
        if (isMsgConfirm == true) {
            return true;
        }

        return false;
    }
</script>
 
      <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color:#005c7b;   
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

 <style type="text/css">
     .WebContainer{
    width: 100%;
    height: auto;
}

 </style>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
     
    <div>
    <input type="hidden" id="hidSeleCode" runat="server" />
<input type="hidden" id="hidSeleDesc" runat="server" />

         <div style="padding-left: 40%; width: 100%;">
                   <div id="divNewService" style="display: none; border: groove; height: 250px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:50px;">
                            <table cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <td style="vertical-align: top;">
                                              
                                            </td>
                                            <td align="right" style="vertical-align: top;">

                                                <input type="button" id="Button1" class="ButtonStyle"  style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideNewService()" />
                                            </td>
                                        </tr>
                                  </table>
                       <fieldset>
                           <legend  class="lblCaption1" >Add New ICD</legend>
                       
                          <table cellpadding="5" cellspacing="5" width="100%" >
                                <tr>
                                    <td colspan="2">
                                          <asp:UpdatePanel ID="updatePanel27" runat="server" >
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblNewServStatus" runat="server" CssClass="lblCaption" ForeColor="Red"  Visible="false"></asp:Label>
                                                    </ContentTemplate>
                                          </asp:UpdatePanel>
                                    </td>
                                </tr>
                                       <tr>
                                           <td class="lblCaption1" style="width:100px;"  >
                                               ICD Code

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel28" runat="server">
                                                    <ContentTemplate>
                                                <asp:TextBox ID="txtNewServCode" runat="server" Width="150px" MaxLength="15" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                       <tr>
                                            <td class="lblCaption1"  style="width:100px;"  >
                                             ICD Name

                                           </td>
                                           <td>
                                                <asp:UpdatePanel ID="updatePanel29" runat="server">
                                                    <ContentTemplate>
                                               <asp:TextBox ID="txtNewServDesc" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"    ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                           </td>
                                       </tr>
                                        
                                      <tr>
                                                <td></td>
                                                <td>
                                                     <asp:UpdatePanel ID="updatePanel30" runat="server">
                                                    <ContentTemplate>
                                                    <asp:Button ID="btnAddNewService" runat="server" Text="Add" OnClick="btnAddNewService_Click" CssClass="button orange small"  />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                </td>
                                       </tr>
                       </table>

                           </fieldset>

                    </div>

            </div>
        <table  style="width:100%">
    <tr>
        <td style="width: 50%; vertical-align: top;">
<table style="width: 100%;">
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel5" >
                      <ContentTemplate>
            <asp:Button ID="btnAddFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px"  Text="Add Fav."  OnClick="btnAddFav_Click"/>
             <asp:Button ID="btnDeleteFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px"   Text="Delete Fav." OnClick="btnDeleteFav_Click" Visible="false" />
                    </ContentTemplate>
            </asp:UpdatePanel>

                          
        </td>
        <td>
              <asp:UpdatePanel runat="server" ID="updatePanel4" >
                      <ContentTemplate>
            <asp:DropDownList ID="drpSrcType" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpSrcType_SelectedIndexChanged">
                <asp:ListItem Selected="True">Full List</asp:ListItem>
                <asp:ListItem>Favorite Only</asp:ListItem>
            </asp:DropDownList>
               </ContentTemplate>
              </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel">
                <ContentTemplate>
            <asp:TextBox ID="txtSearch" runat="server" CssClass="lblCaption1" OnKeyUp="refreshPanel(this.value);"  onfocus="ShowSearch();"  ></asp:TextBox>
                    </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="txtSearch" />
                   
                </Triggers>
            </asp:UpdatePanel>
        </td>
         <td>
             <asp:CheckBox ID="chkAnySearch" runat="server" Text="Any Search" CssClass="lblCaption1" Checked="true"  Visible="false"/>
         </td>
    </tr>
</table>
             <td style="width: 10px;"></td>

        <td style="width: 50%; vertical-align: top;">
             <input type="button" id="btnNewService"  value="Add New ICD" class="orange" runat="server" visible="false" title="Add New ICD" style="width:120px;border-radius:5px 5px 5px 5px"  onclick="ShowNewService()" />
             <input type="button" id="btnCrosswalk"  value="View ICD Crosswalk" class="orange" runat="server"  visible="false"   title="View ICD Crosswalk" style="width:120px;border-radius:5px 5px 5px 5px"  onclick="ShowDiagCrosswalkPopup()" />

         </td>
            </tr>
        </table>
<table style="width:100%"  >
    <tr>
        <td style="width: 50%; vertical-align: top;">

            <div style="float: left;">
                <table style="width:100%">

                    <tr>
                        <td>
                            <div style="padding-top: 0px; width: 100%;height:500px;   overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                           <asp:UpdatePanel runat="server" ID="updatePanel1" >
                                                 <ContentTemplate>
                            <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"   
                                EnableModelValidation="True"     OnPageIndexChanging="gvServicess_PageIndexChanging"  GridLines="None"    >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                 <FooterStyle CssClass="FooterStyle" />
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                           
                                                        <asp:ImageButton ID="lnkAdd" runat="server" ToolTip="Add" ImageUrl="~/EMR/Images/AddButton.jpg" Height="18" Width="18"
                                                                                                        OnClick="Add_Click" />
                                               
                                        </ItemTemplate>
                                           <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click"  >
                                                    <asp:Label ID="lblPrice" CssClass="label" BorderStyle="None" Visible="false" runat="server" Font-Size="11px" Width="70px" Style="text-align: right; padding-right: 5px;" Text='<%# Bind("Price")' ></asp:Label>

                                                <asp:Label ID="lblCode" CssClass="label" BorderStyle="None"  runat="server" Font-Size="11px" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                             </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="85%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDesc" runat="server" OnClick="Edit_Click"  >
                                                <asp:Label ID="lblDesc" CssClass="label" BorderStyle="None" runat="server" Font-Size="11px" Text='<%# Bind("Description") %>'  Width="95%"   ></asp:Label>
                                                
                                                
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                   
                                </Columns>
                                 <PagerStyle CssClass="PagerStyle"  /> 

                               
                                
                            </asp:GridView>
                                     </ContentTemplate>
                                            </asp:UpdatePanel>
                    
                                
                             </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>

        

        <td style="width: 50%; vertical-align: top;">
            <div>
                <table style="width:100%">
                    <tr>
                        <td class="lblCaption1">
                            
                          
                           <asp:UpdatePanel runat="server" ID="updatePanel3" >
                                  <ContentTemplate>
                                        Description
                                     <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label" ></asp:Label>
                                  </ContentTemplate>
                              </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server" ID="updatePanel19">
                                <ContentTemplate>
                                    
                                    <asp:TextBox ID="txtDiagCode" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"   ></asp:TextBox>
                                    <asp:TextBox ID="txtDiagName" runat="server" Width="87%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return DiagNameSelected()"></asp:TextBox>
                                    <asp:Button ID="btnAddDiag" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAddDiag_Click" style="width:50px;border-radius:5px 5px 5px 5px"   />
                                        <div id="divwidth" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtDiagCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtDiagName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                       
                    </tr>
                    <tr>
                        <td class="lblCaption1">
                            
                            Remarks<br />
                              <asp:UpdatePanel runat="server" ID="updatePanel6" >
                                  <ContentTemplate>
                                         <asp:TextBox ID="txtRemarks" runat="server" CssClass="lblCaption1" Width="65%" AutoPostBack="true" OnTextChanged="txtRemarks_TextChanged"></asp:TextBox>
&nbsp;
                            <asp:dropdownlist id="drpDiagType" runat="server" cssclass="lblCaption1" width="30%" borderwidth="1px" bordercolor="#cccccc" >
                                            <asp:ListItem Value="Principal" selected="True">Principal</asp:ListItem>
                                            <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                            <asp:ListItem Value="Admitting">Admitting</asp:ListItem>
                                           
                       </asp:dropdownlist>

                                 </ContentTemplate>
                             </asp:UpdatePanel>
                           
                        </td>
                    </tr>
                      <tr>
                           <td class="lblCaption1"  colspan="3">
                                  <div id="divHistoryDrp" runat="server" visible="false">
                               History   &nbsp;&nbsp; 
                              <asp:UpdatePanel runat="server" ID="updatePanel11">
                                    <ContentTemplate>
                                 <asp:DropDownList ID="drpHistory" CssClass="label" runat="server" Width="300px" BorderColor="#cccccc">
                                </asp:DropDownList>
                         
                                  <asp:ImageButton  ID="imgbtnPrescHistView"    runat="server" ToolTip="View selected Diagnosis" ImageUrl="~//Images/zoom.png" 
                                                       OnClick="imgbtnPrescHistView_Click"  style="height:15px;width:25px;"  />
                                  &nbsp;&nbsp; 
                                  <asp:ImageButton  ID="imgbtnDown"    runat="server" ToolTip="Click here to Import selected Diagnosis" ImageUrl="~/EMR/Images/Down.gif" 
                                                       OnClick="imgbtnDown_Click"  />

                                        </ContentTemplate>
                                  </asp:UpdatePanel>

                                      </div>
                            </td>
                        </tr>
                       <tr>
                        <td class="lblCaption1">
                              <div id="divTemplate" runat="server" visible="false">
                            Template&nbsp;
                        
                            <asp:DropDownList ID="drpTemplate" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>

                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label orange" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px" CssClass="orange" />
</div>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                             <div style="padding-top: 0px; width: 100%;  height:300px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                            <asp:UpdatePanel runat="server" ID="updatePanel2">
                <ContentTemplate>
                                  <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"    OnRowDataBound="gvDiagnosis_RowDataBound" OnRowCommand="gvDiagnosis_RowCommand"  GridLines="None"   >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                          

                                <Columns>
                                     <asp:TemplateField HeaderText=""  HeaderStyle-Width="50px" >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                        
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiagType" CssClass="label" Font-Size="11px"  Visible="false"  runat="server" Text='<%# Bind("EPD_TYPE") %>'></asp:Label>
                                             <asp:Label ID="lblPosition" CssClass="label" Font-Size="11px"  Visible="false"  runat="server" Text='<%# Bind("EPD_POSITION") %>'></asp:Label>
                                            <asp:Button ID="btnColor" runat="server" Width="10px" Enabled="false" BorderStyle="None"  />&nbsp
                                            <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="600px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px" Width="100%"  runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField   HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                                 <asp:ImageButton  ID="imgbtnUp"  runat="server" ToolTip="Click here to move this row up" ImageUrl="~/EMR/Images/Up.gif"
                                                     CommandArgument='<%# Eval("EPD_POSITION")%>' CommandName="Up"    />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField  HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                              <asp:ImageButton  ID="imgbtnDown"    runat="server" ToolTip="Click here to move this row down" ImageUrl="~/EMR/Images/Down.gif" 
                                                   CommandArgument='<%# Eval("EPD_POSITION")%>' CommandName="Down" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                            </asp:GridView>
                  </ContentTemplate>
                                </asp:UpdatePanel>
                    
                             </div>
                             <asp:Button ID="btnColor1" runat="server" Width="10px" Height="10px" Enabled="false" BorderStyle="None" style="background-color:#ff0000;" />&nbsp; <span class="lblCaption1">Principal</span>&nbsp;
                            <asp:Button ID="btnColor2" runat="server" Width="10px" Height="10px" Enabled="false" BorderStyle="None" style="background-color:#9ACD32;" />&nbsp; <span class="lblCaption1">Secondary</span>&nbsp;
                           <asp:Button ID="btnColor3" runat="server" Width="10px" Height="10px" Enabled="false" BorderStyle="None" style="background-color:#FFFF00;" />&nbsp; <span class="lblCaption1">Admitting</span>
                             <asp:UpdatePanel runat="server" ID="updatePanel8">
                                    <ContentTemplate>
                            <asp:CheckBox ID="chkDiagUpdateInvoice" runat="server" Visible="false" Text="Diagnosis Update in to Invoice" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkDiagUpdateInvoice_CheckedChanged"  />
                                       </ContentTemplate>
                             </asp:UpdatePanel>
                        </td>

                    </tr>
                </table>
            </div>

        </td>
    </tr>
</table>


        <div style="padding-left: 40%; width: 100%;">
               <div id="divHistory" style=" display: none;border: groove; height: 350px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:250px;">
                     <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">
                                     <asp:UpdatePanel runat="server" ID="updatePanel12">
                                    <ContentTemplate>
                                    <input type="button" id="btnHistoryClose" class="ButtonStyle" style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideHistory()" />
                                        </ContentTemplate>
                                         </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                         <asp:UpdatePanel runat="server" ID="updatePanel14">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"  GridLines="Horizontal"  Width="100%"  >
                               <HeaderStyle CssClass="GridHeader_Blue" />
                              <RowStyle CssClass="GridRow" />

                                             <Columns>
                                      <asp:TemplateField HeaderText="Type" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                           
                                            <asp:Label ID="lblType" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("EPD_TYPE") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                           
                                            <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="600px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px" Width="100%"  runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    
                                </Columns>
                                  </asp:GridView>
                                    </ContentTemplate>
                                        
                                        </asp:UpdatePanel>
                                         

                                  
               </div>
           
           </div>

         <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
        <asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td class="lblCaption1">Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>


          <div id="divDiagCrosswalk" style="display: none; overflow: hidden; border: groove; height: 450px; width: 900px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 100px; top: 100px;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="vertical-align: top;width:50%;">
                               <asp:updatepanel id="UpdatePanel7" runat="server">
                             <ContentTemplate>
                             <asp:RadioButtonList id="radDiagCrosswalkType" runat="server"  RepeatDirection="Horizontal" Font-Bold="true" CssClass="lblCaption1" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="radDiagCrosswalkType_SelectedIndexChanged" >
                                 <asp:ListItem Selected="True" Text="ICD-9 to ICD-10" Value="ICD9"> </asp:ListItem>
                                 <asp:ListItem Text="ICD-10 to ICD-9" Value="ICD10"> </asp:ListItem>
                             </asp:RadioButtonList>
                                 </ContentTemplate>
                                   </asp:updatepanel>
                        </td>
                        <td align="right" style="vertical-align: top;width:50%;">

                            <input type="button" id="Button3" class="ButtonStyle" style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideDiagCrosswalkPopup()" />
                        </td>
                    </tr>
                </table>
                  <table width="90%" border="0" cellpadding="0" cellspacing="0">

                <tr>
                    <td  style="vertical-align:bottom;height:30px;">
                        <asp:updatepanel id="UpdatePanel43" runat="server">
                             <ContentTemplate>
                              <asp:TextBox ID="txtSrcDiagCrosswalk" runat="server" CssClass="searchTerm"  ></asp:TextBox> 
                              <asp:ImageButton  ID="ImgDiagCrosswalkShow"    runat="server" ToolTip="View selected Diagnosis" ImageUrl="~//Images/zoom.png" 
                                                       OnClick="ImgDiagCrosswalkShow_Click"  style="height:20px;width:35px;border:none ;"  />
                            </ContentTemplate>
                        </asp:updatepanel>
                    </td>
                    
                </tr>

            </table>
              <br />
                <div style="padding-top: 0px; width: 98%; height: 370px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;border: 1px solid #dbdbdb;
                       padding :5px 5px;  margin-bottom: 10px;    border-radius: 8px;    box-shadow: 1px 1px 10px 1px #dbdbdb inset;">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">
                                <asp:updatepanel id="UpdatePanel39" runat="server">
                                        <ContentTemplate>
                                   <asp:GridView ID="gvDiagCrosswalk" runat="server"  Width="100%" Visible="false"  
                                        AllowSorting="True" AutoGenerateColumns="False" 
                                        EnableModelValidation="True"    GridLines="Horizontal" >
                                        <HeaderStyle CssClass="GridHeader_Blue" />
                                        <RowStyle CssClass="GridRow" />
                
                                        <Columns>

                                            <asp:TemplateField HeaderText="ICD-9" SortExpression="DIM_ICD_ID_9" >
                                                <ItemTemplate>
                                                        <asp:Label ID="lblCode_9" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("DIM_ICD_ID_9") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" SortExpression="DIM_ICD_DESCRIPTION_9">
                                                <ItemTemplate>
                                                        <asp:Label ID="lblDesc_9" CssClass="GridRow"  Width="250px"  runat="server" Text='<%# Bind("DIM_ICD_DESCRIPTION_9") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ICD-10" SortExpression="DIM_ICD_ID_10">
                                                <ItemTemplate>
                                                        <asp:Label ID="lblCode_10" CssClass="GridRow" Width="100px"   runat="server" Text='<%# Bind("DIM_ICD_ID_10") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" SortExpression="DIM_ICD_DESCRIPTION_10">
                                                <ItemTemplate>
                                                        <asp:Label ID="lblDesc_10" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("DIM_ICD_DESCRIPTION_10") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                             <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                                   <ItemTemplate>
                                                         <asp:Button ID="btnFavDiagCrosswalk" runat="server" Text="f"  CssClass="orange" style="font-size:15px;font-weight:bold;width:18px;height:18px;border-radius:10px;cursor:pointer;" OnClick="btnFavDiagCrosswalk_Click"   />
                                                       <asp:ImageButton ID="imgAddDiagCrosswalk" runat="server" ToolTip="Add" ImageUrl="~/EMR/Images/AddButton.jpg" Height="18" Width="18"  OnClick="imgAddDiagCrosswalk_Click" />
                                                    </ItemTemplate>
                                                 <HeaderStyle Width="50px" />
                                                     <ItemStyle HorizontalAlign="Center" />
                                             </asp:TemplateField>
                          
                                        </Columns>

                                    </asp:GridView>

                                  <asp:GridView ID="gvDiagCrosswalk1" runat="server"  Width="100%"  Visible="false"
                                    AllowSorting="True" AutoGenerateColumns="False" 
                                    EnableModelValidation="True"   GridLines="Horizontal" >
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                
                                    <Columns>
                                        <asp:TemplateField HeaderText="ICD-10" SortExpression="DIM_ICD_ID_10">
                                            <ItemTemplate>
                                                    <asp:Label ID="lblCode_10" CssClass="GridRow" Width="100px"   runat="server" Text='<%# Bind("DIM_ICD_ID_10") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="DIM_ICD_DESCRIPTION_10">
                                            <ItemTemplate>
                                                    <asp:Label ID="lblDesc_10" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("DIM_ICD_DESCRIPTION_10") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="ICD-9" SortExpression="DIM_ICD_ID_9" >
                                            <ItemTemplate>
                                                    <asp:Label ID="lblCode_9" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("DIM_ICD_ID_9") %>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="DIM_ICD_DESCRIPTION_9">
                                            <ItemTemplate>
                                                    <asp:Label ID="lblDesc_9" CssClass="GridRow"  Width="250px"  runat="server" Text='<%# Bind("DIM_ICD_DESCRIPTION_9") %>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                               <ItemTemplate>
                                                    <asp:Button ID="btnFavDiagCrosswalk" runat="server" Text="f"  CssClass="orange" style="font-size:15px;font-weight:bold;width:18px;height:18px;border-radius:10px;cursor:pointer;" OnClick="btnFavDiagCrosswalk_Click"   />
                                                   <asp:ImageButton ID="imgAddDiagCrosswalk" runat="server" ToolTip="Add" ImageUrl="~/EMR/Images/AddButton.jpg" Height="18" Width="18"  OnClick="imgAddDiagCrosswalk_Click" />
                                                </ItemTemplate>
                                             <HeaderStyle Width="50px" />
                                                 <ItemStyle HorizontalAlign="Center" />
                                         </asp:TemplateField>
                          
                                    </Columns>

                                </asp:GridView>
                                        </ContentTemplate>
                                       
                                 </asp:updatepanel> 
                            </td>

                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
     <style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>
</html>
