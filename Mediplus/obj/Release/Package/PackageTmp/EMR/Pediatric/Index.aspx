﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.Pediatric.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/EMR/Pediatric/Pediatric.ascx" TagPrefix="UC1" TagName="Pediatric" %>
<%@ Register Src="~/EMR/Pediatric/PeriodonticChart.ascx" TagPrefix="UC1" TagName="PeriodonticChart" %>
<%@ Register Src="~/EMR/Pediatric/VaccinationChart.ascx" TagPrefix="UC1" TagName="VaccinationChart" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

      
    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

       <%-- <script type="text/javascript" src="../Scripts/Patient/PatientMaster.js" ></script>--%>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
     <div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Pediatrics </h3>
                </div>
            </div>
            <div style="padding-left:60%;width:100%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
        </div>
      <table style="width:100%" >
          <tr>
            <td align="left">
                  <asp:DropDownList ID="drpSegment" runat="server" CssClass="label" AutoPostBack="true" OnSelectedIndexChanged="drpSegment_SelectedIndexChanged" ></asp:DropDownList>
              </td>
              <td align="right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" Text="Save Record" />
                    </ContentTemplate>
                </asp:UpdatePanel>

              </td>
          </tr>
      </table>
  
            <div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
                    <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Pediatric Details" Width="100%">
                        <ContentTemplate>
                            <UC1:Pediatric ID="pnlPediatric" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Pediatric Chart" Width="100%">
                        <ContentTemplate>
                             <UC1:PeriodonticChart ID="pnlPeriodonticChart" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Vaccination Chart" Width="100%">
                        <ContentTemplate>
                             <UC1:VaccinationChart ID="pnlVaccinationChart" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </div>
        </div>
    <br />
    <br />
</asp:Content>
