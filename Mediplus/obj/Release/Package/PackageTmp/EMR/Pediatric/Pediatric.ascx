﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pediatric.ascx.cs" Inherits="Mediplus.EMR.Pediatric.Pediatric" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

     <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />

     <link href="../Styles/style.css" rel="Maincontrols" type="text/css" />
 <script language="javascript" type="text/javascript">


     function OnlyNumeric(evt) {
         var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
         if (chCode >= 48 && chCode <= 57 ||
              chCode == 46) {
             return true;
         }
         else

             return false;
     }


     function BMI(Wt, Ht) {
         if (Wt == 0 || Ht == 0)
             return 0;
         return ((parseFloat(Wt) / parseFloat(Ht) / parseFloat(Ht)) * 10000).toFixed(2)
     }

     function SetCalculation() {
         var wt = document.getElementById("<%=weight.ClientID%>").value;
         var ht = document.getElementById("<%=length.ClientID%>").value;

         var varBmi = BMI(wt, ht)
         document.getElementById("<%=bmi.ClientID%>").value = varBmi;
            Calculation1(varBmi);
        }

        function Calculation1(BMIValue) {
            var vGender = '<%=Gender%>';
         var BMITABLE = jQuery.parseJSON('[{"Gender":"Male","BMIfrom":0,"BMIto":18.5,"BMIresult":"Under Weight"},' +
             '{"Gender":"Male","BMIfrom":18.6,"BMIto":24.9,"BMIresult":"Normal"},' +
             '{"Gender":"Male","BMIfrom":25,"BMIto":29.9,"BMIresult":"Over weight"},' +
             '{"Gender":"Male","BMIfrom":30,"BMIto":30000,"BMIresult":"Obese"},' +
             '{"Gender":"Male","BMIfrom":35,"BMIto":39.9,"BMIresult":"Extreme Obesity"},' +
             '{"Gender":"Male","BMIfrom":40,"BMIto":12000,"BMIresult":"Morbid Obesity"},' +
             '{"Gender":"Female","BMIfrom":0,"BMIto":18.9,"BMIresult":"UnderWeight"},' +
             '{"Gender":"Female","BMIfrom":19.0,"BMIto":24.9,"BMIresult":"Normal"},' +
             '{"Gender":"Female","BMIfrom":25,"BMIto":29.9,"BMIresult":"Over weight"},' +
             '{"Gender":"Female","BMIfrom":30,"BMIto":30000,"BMIresult":"Obese"},' +
             '{"Gender":"Female","BMIfrom":35,"BMIto":39.9,"BMIresult":"Extreme Obesity"},' +
             '{"Gender":"Female","BMIfrom":40,"BMIto":12000,"BMIresult":"Morbid Obesity"}]');

         // alert(JSON.stringify(BMITABLE));
         $.each(BMITABLE, function (i, item) {
             var gender = vGender == undefined ? 'Male' : vGender;


             if ((BMIValue > item.BMIfrom) && (BMIValue <= item.BMIto) && item.Gender.toLowerCase() == gender.toLowerCase()) {
                 // alert(item.BMIresult);
                 $("#BMIMessage").text(item.BMIresult)
                 return;
             }
         });
     }
    </script>
 <script type="text/javascript">
     function ShowMessage() {
         $("#myMessage").show();
         setTimeout(function () {
             var selectedEffect = 'blind';
             var options = {};
             $("#myMessage").hide();
         }, 2000);
         return true;
     }

        </script>
 


 <div id="Pediatric1Div" style="width:100%;">
        <div class="content" style="border:0px solid #dcdcdc;margin:20px 0px;">
            <table class="table spacy"   style="width:100%;">
                    <tr >
                        <td  style="width:30%;" class="lblCaption1" >Date :</td>
                        <td>  
                            <asp:TextBox ID="txtFromDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                    </tr>
                    <tr >
                        <td class="lblCaption1">Age :</td>
                        <td><input type="text"  runat="server" class="age numeric" id="age" value="" /></td>
                    </tr>
                    <tr >
                        <td class="lblCaption1">Weight :</td>
                        <td class="lblCaption1" ><input type="text"  runat="server" class="weight numeric" id="weight" value="" onkeypress="return OnlyNumeric(event);"  onkeyup="SetCalculation();" />&nbsp;Kg</td>
                    </tr>
                    <tr >
                        <td class="lblCaption1">Length/Height :</td>
                        <td class="lblCaption1"><input type="text"  runat="server" class="length numeric" id="length" value="" onkeypress="return OnlyNumeric(event);"  onkeyup="SetCalculation();" />&nbsp;Cm</td>
                    </tr>
                  <tr >
                        <td class="lblCaption1">BMI :</td>
                        <td><input type="text"  runat="server" class="bmi numeric" id="bmi" value="" onkeypress="return OnlyNumeric(event);"/><span id="BMIMessage"></span></td>
                    </tr>
                     <tr >
                        <td class="lblCaption1">Head Circumference. :</td>
                        <td class="lblCaption1"><input type="text"  runat="server" class="headcirc numeric" id="headcirc" value="" onkeypress="return OnlyNumeric(event);"/>&nbsp;Cm</td>
                    </tr>
                    <tr  style="display:none;">
                        <td class="lblCaption1">Stature :</td>
                        <td><input type="text"  runat="server" class="stature numeric" id="stature" value="" onkeypress="return OnlyNumeric(event);"/>&nbsp;Cm</td>
                    </tr>
                  
                    <tr >
                        <td class="lblCaption1">Comments :</td>
                        <td> 
                            <textarea  runat="server" class="comments" id="comments"   style="height:50px;width:300px;" >

                            </textarea>

                        </td>
                    </tr>
            </table>
        </div>
    </div>
<div id="content-panel">
 
    <asp:gridview id="gvPediatric" runat="server" autogeneratecolumns="False"  
                            enablemodelvalidation="True" width="100%" >
                                 <HeaderStyle CssClass="GridHeader"  Font-Bold="true"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                              <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                         
                                             <asp:Label ID="lblSegment" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_SEGMENT") %>'  Visible="false" ></asp:Label>
                                            <asp:Label ID="lblDate" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_DATEDesc") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblAge" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_AGE") %>'  ></asp:Label>
                                     
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Weight">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblWeight" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_WEIGHT") %>'  ></asp:Label>
                                    
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Length">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkLength" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblLength" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_LENGTH") %>'  ></asp:Label>
                                     </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  
                                 <asp:TemplateField HeaderText="Headcircumfrence" >
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblHeadcircumfrence" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_HEADCIRC") %>'  ></asp:Label>
                                    
                                    </ItemTemplate>
                                </asp:TemplateField>
                            
                                <asp:TemplateField HeaderText="Stature">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblStature" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_STATURE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BMI">
                                    <ItemTemplate>
                                          
                                            <asp:Label ID="lblBMI" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_BMI") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblComment" CssClass="GridRow"   runat="server" Text='<%# Bind("EGD_COMMENTS") %>'  ></asp:Label>
                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg"  Height="16px" Width="16px"
                                                OnClick="Delete_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                
                            </Columns>
                              

    </asp:gridview>
                   
    </div>