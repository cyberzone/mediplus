﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VaccinationChart.ascx.cs" Inherits="Mediplus.EMR.Pediatric.VaccinationChart" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div style="padding-top: 0px; width: 100%; height: auto; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">

    <asp:GridView ID="gvVaccinationChart" runat="server" AllowSorting="True" AutoGenerateColumns="False"
        EnableModelValidation="True" Width="100%" OnRowDataBound="gvOPDAssessment_RowDataBound">
        <HeaderStyle CssClass="GridHeader_Blue" />

        <RowStyle CssClass="GridRow"  HorizontalAlign="Center" />
        <AlternatingRowStyle CssClass="GridAlterRow" />
        <Columns>
            <asp:TemplateField HeaderText="Description" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("SEGM_TYPE") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("SEGM_FIELD_ID") %>' Visible="false"></asp:Label>

                    <asp:Label ID="lblDescription" CssClass="lblCaption1" runat="server" Text='<%# Bind("SEGM_FIELD_NAME") %>' Width="100%"></asp:Label>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="at birth">
                <ItemTemplate>
                    <asp:CheckBox ID="chkAtBirth" runat="server" ToolTip="ATBIRTH" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="1 month">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth1" runat="server" ToolTip="MONTH1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="2 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth2" runat="server" ToolTip="MONTH2" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="3 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth3" runat="server" ToolTip="MONTH3" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="4 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth4" runat="server" ToolTip="MONTH4" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="5 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth5" runat="server" ToolTip="MONTH5" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="6 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth6" runat="server" ToolTip="MONTH6" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="9 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth9" runat="server" ToolTip="MONTH9" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="12 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth12" runat="server" ToolTip="MONTH12" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="15-18 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth15_18" runat="server" ToolTip="MONTH15_18" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="24 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth24" runat="server" ToolTip="MONTH24" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="30 months">
                <ItemTemplate>
                    <asp:CheckBox ID="chkMonth30" runat="server" ToolTip="MONTH30" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="4-6 years">
                <ItemTemplate>
                    <asp:CheckBox ID="chkYears4_6" runat="server" ToolTip="YEAR4_6" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="11-16 years">
                <ItemTemplate>
                    <asp:CheckBox ID="chkYears11_16" runat="server" ToolTip="YEAR11_16" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>

    </asp:GridView>

   
</div>

 <br />
    <table style="width: 100%;border-style:solid;border-width:thin;padding:5px;"  >
        <tr>
            <td class="lblCaption1" style="font-weight:bolder;" colspan="4">
                Other Vaccines
            </td>
             <td class="lblCaption1" style="font-weight:bolder;" colspan="4">
                Date of Vaccination
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Typhoid
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate1" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate1" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate2" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate2" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate3" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender3" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate3" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td>
                <asp:UpdatePanel ID="updatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate4" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender4" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate4" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate5" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender5" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate5" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate6" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender6" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate6" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel7" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTyphoidDate7" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender7" runat="server"
                            Enabled="True" TargetControlID="txtTyphoidDate7" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">PPD / Mantoux Test
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel8" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate1" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender8" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate1" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel9" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate2" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender9" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate2" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate3" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender10" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate3" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td>
                <asp:UpdatePanel ID="updatePanel11" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate4" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender11" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate4" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel12" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate5" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender12" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate5" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel13" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate6" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender13" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate6" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel14" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPPD_MantouxDate7" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender14" runat="server"
                            Enabled="True" TargetControlID="txtPPD_MantouxDate7" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">HPV Series
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel15" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate1" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender15" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate1" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel16" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate2" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender16" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate2" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate3" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender17" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate3" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td>
                <asp:UpdatePanel ID="updatePanel18" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate4" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender18" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate4" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel19" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate5" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender19" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate5" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel20" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate6" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender20" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate6" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel21" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHPVSeriesDate7" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender21" runat="server"
                            Enabled="True" TargetControlID="txtHPVSeriesDate7" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Flu Vaccine
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel22" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate1" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender22" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate1" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel23" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate2" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender23" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate2" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel24" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate3" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender24" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate3" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td>
                <asp:UpdatePanel ID="updatePanel25" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate4" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender25" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate4" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel26" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate5" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender26" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate5" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel27" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate6" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender27" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate6" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel28" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFluVaccineDate7" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender28" runat="server"
                            Enabled="True" TargetControlID="txtFluVaccineDate7" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Yellow Fever Vaccine
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel29" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate1" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender29" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate1" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel30" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate2" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender30" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate2" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel31" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate3" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender31" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate3" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td>
                <asp:UpdatePanel ID="updatePanel32" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate4" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender32" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate4" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel33" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate5" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender33" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate5" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel34" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate6" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender34" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate6" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel35" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtYellowFeverVaccineDate7" runat="server" Width="100%" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender35" runat="server"
                            Enabled="True" TargetControlID="txtYellowFeverVaccineDate7" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

<br />
