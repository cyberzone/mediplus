﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.ENT.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="EARChart.ascx" TagPrefix="UC1" TagName="Ear" %>
<%@ Register Src="NoseChart.ascx" TagPrefix="UC1" TagName="Nose" %>
<%@ Register Src="ThroatChart.ascx" TagPrefix="UC1" TagName="Throat" %>
<%@ Register Src="NeckChart.ascx" TagPrefix="UC1" TagName="Neck" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

     
    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">


    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

       <%-- <script type="text/javascript" src="../Scripts/Patient/PatientMaster.js" ></script>--%>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">ENT </h3>
                </div>
            </div>
         <br />
          <div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                    <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Ear Chart" Width="100%">
                        <ContentTemplate>
                              <UC1:Ear ID="pnlEar" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Nose Chart" Width="100%">
                        <ContentTemplate>
                              <UC1:Nose ID="pnlNose" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Throat Chart" Width="100%">
                        <ContentTemplate>
                               <UC1:Throat ID="pnlThroat" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Neck Chart" Width="100%">
                        <ContentTemplate>
                               <UC1:Neck ID="pnlNeck" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </div>

         </div>
</asp:Content>
