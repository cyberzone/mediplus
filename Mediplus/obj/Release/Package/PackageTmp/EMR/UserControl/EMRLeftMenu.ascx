﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMRLeftMenu.ascx.cs" Inherits="Mediplus.EMR.UserControl.EMRLeftMenu" %>

<script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
<script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
<script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<script>
    function ConfirmProcessComplete1() {

        var vEPM_STATUS = '<%= Convert.ToString(Session["EPM_STATUS"]) %>'
        // alert(vEPM_STATUS);

        if (vEPM_STATUS != 'Y') {
            var isSrtProc = window.confirm('Do you want to complete the process ? ');

        }
        window.location.assign('../Billing/Invoice.aspx?CompleteProc=' + isSrtProc)
        return true;

    }

    function readyFn(jQuery) {

        var DeptID = '<%=Convert.ToString(Session["HPV_DEP_NAME"])%>'

        if (DeptID == 'PEDIATRIC' || DeptID == 'PAED' || DeptID == 'PAEDIATRIC') {
            document.getElementById('liPediatric').style.display = 'block';

        }


        if (DeptID == 'PHYSIOTHERAPY') {
            document.getElementById('liTherapy').style.display = 'block';

        }

        if (DeptID == 'ENT') {
            document.getElementById('liENT').style.display = 'block';

        }



        if (DeptID == 'DENTAL' || DeptID == 'DENT') {
            document.getElementById('liDental').style.display = 'block';

        }

        if (DeptID == 'ORTHOPEDIC') {
            document.getElementById('liOrthodontic').style.display = 'block';

        }
        if (DeptID == 'OPHTHALMOLOGY' || DeptID == 'OPTHALMOLOGY' || DeptID == 'OPTHAL') {
            document.getElementById('liOphthalmology').style.display = 'block';

        }



    }


    $(document).ready(readyFn);

</script>
<div style="padding-left: 0px; width: 100%">
    <ul id="patient-nav-list" style="list-style: none; padding-left: 0px;">
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/PatientDashboard.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/chart-area-up_blue.png" /><span class="patient-nav-text">Patient Dashboard</span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/PatientScreening.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/user-card_blue.png" /><span class="patient-nav-text">Patient Screening</span></a>
        </li>


        <li id="liPediatric" class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI'; display: none;">
            <a href='<%= ResolveUrl("~/EMR/Pediatric/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Pediatric </span></a>
        </li>

        <li id="liTherapy" class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI'; display: none;">
            <a href='<%= ResolveUrl("~/EMR/Department/TherapyChart.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/wallet_blue.png" /><span class="patient-nav-text">Therapy </span></a>
        </li>
        <li id="liENT" class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI'; display: none;">
            <a href='<%= ResolveUrl("~/EMR/ENT/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/wallet_blue.png" /><span class="patient-nav-text">ENT </span></a>
        </li>
        <li id="liDental" class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI'; display: none;">
            <a href='<%= ResolveUrl("~/EMR/Dental/Index.aspx?PageName=Dental")%>' class="patient-nav-link">
                <img src="../Images/Icons/wallet_blue.png" /><span class="patient-nav-text">Dental </span></a>
        </li>
        <li id="liOrthodontic" class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI'; display: none;">
            <a href='<%= ResolveUrl("~/EMR/Dental/Index.aspx?PageName=Orthodontic")%>' class="patient-nav-link">
                <img src="../Images/Icons/wallet_blue.png" /><span class="patient-nav-text">Orthodontic </span></a>
        </li>
        <li id="liOphthalmology" class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI'; display: none;">
            <a href='<%= ResolveUrl("~/EMR/Ophthalmology/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/wallet_blue.png" /><span class="patient-nav-text">Ophthalmology </span></a>
        </li>


        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/VisitDetails/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/address-book_blue.png" /><span class="patient-nav-text">Visit Details</span></a>
        </li>

        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Scans/Scans.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/laptop_blue.png" /><span class="patient-nav-text">Scans</span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Scans/ScanList.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/laptop_blue.png" /><span class="patient-nav-text">Scanning</span></a>
        </li>
         <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Scans/ScannedImagesList.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/laptop_blue.png" /><span class="patient-nav-text">Xray Images</span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/History.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/wallet_blue.png" /><span class="patient-nav-text">History</span></a>
        </li>

        <%   if (Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE")
             { %>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/CriticalNotes.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Critical Notes</span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/OPDAssessment/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">OPD Assessment</span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/EducationalForm/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/safe_blue.png" /><span class="patient-nav-text">Educational Form</span></a>
        </li>

        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <%--  <a href='<%= ResolveUrl("~/EMR/EMRS/Index.aspx")%>' class="patient-nav-link" > 
            <img src="../Images/Icons/image_blue.png"     /><span class="patient-nav-text" >EM Review Sheet </span></a>--%>

            <a href='<%= ResolveUrl("~/EMR/EMRS/IndexCPTResult.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">EM Review Sheet </span></a>

        </li>

        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/VisitDetails/ResultsView.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Results </span></a>
        </li>

        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Referral/Index.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/safe_blue.png" /><span class="patient-nav-text">Reference </span></a>
        </li>
       <%-- <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/ClinicalNotesContainer.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Clinical Notes  </span></a>
        </li>--%>

        <%-- <li class="patient-nav" style="font-size:12px;font-family:'Segoe UI';">
        <a href='<%= ResolveUrl("~/EMR/Billing/Invoice.aspx")%>' class="patient-nav-link" onclick="return ConfirmProcessComplete()" > 
            <img src="../Images/Icons/image_blue.png"     /><span class="patient-nav-text" >Invoice </span></a>
    </li>--%>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='#' class="patient-nav-link" onclick="return ConfirmProcessComplete1()">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Invoice </span></a>
        </li>
        <%  } %>
       <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/InPatient/AdmissionSummary.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Admission Summary </span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/CashPatientSummary.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Cash Summary </span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';display:none;">
            <a href='<%= ResolveUrl("~/EMR/Patient/TimeOutProcedureCheckList.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Time out Procedure </span></a>
        </li>
        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/PharmacyInternal.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Pharmacy Internal </span></a>
        </li>
      <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/FallPreventionAssessment.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">FallPreventionAss. </span></a>
        </li>

     <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/InitialOPAssessment.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Initial OP Assessment. </span></a>
        </li>
          <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/DischargeSummary.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Discharge Summary </span></a>
        </li>

              <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/OperationNotes.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Operation Notes </span></a>
        </li>

         

        <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/MedicalReport/PTLeaveForm.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Leave Form </span></a>
        </li>

 <li class="patient-nav" style="font-size: 12px; font-family: 'Segoe UI';">
            <a href='<%= ResolveUrl("~/EMR/Patient/Anesthesia.aspx")%>' class="patient-nav-link">
                <img src="../Images/Icons/image_blue.png" /><span class="patient-nav-text">Anesthesia Record </span></a>
        </li>
    </ul>
   
</div>
