﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SegmentDtlsPopup.aspx.cs" Inherits=" Mediplus.EMR.Patient.SegmentDtlsPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />


      <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

        .BoldStyle
        {
            font-weight: bold;
        }

          .ImageStyle
    {
        height:200px;
        width:150px;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
          <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY" />
    <div>
    <table style="width: 100%; border: 1px solid #dcdcdc" class="gridspacy">
         <tr>
            <td >
                <span class="lblCaption1"><%=SegmentDtslValue %></span>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
