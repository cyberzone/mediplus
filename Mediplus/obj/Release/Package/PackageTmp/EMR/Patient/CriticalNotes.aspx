﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CriticalNotes.aspx.cs" Inherits=" Mediplus.EMR.Patient.CriticalNotes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.5em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
      <div id="divAuditDtls" runat="server" visible="false"  style="text-align: right; width: 80%;" class="lblCaption1">
        Creaded By :
                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Modified By
                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
    </div>

     <div class="box">
        <div class="box-header">
            <h4 class="box-title">Critical Notes</h4>
        </div>
    </div>
 
      <table class="table spacy" style="width:80%;">
            
            <tr>
                <td class="lblCaption1" ><label for="TreatmentPlan">Critical Notes</label></td>                
            </tr>
           
            
            <tr>
                <td>
                    
                    <asp:TextBox ID="txtCriticalNotes" runat="server" CssClass="TextBoxStyle" TextMode="MultiLine" style="width:100%;height:100px;" AutoPostBack="true" OnTextChanged="txtCriticalNotes_TextChanged"></asp:TextBox>
                </td>
            </tr>
        </table>
     <span class="lblCaption1" >History</span><br />
      <div style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
            <asp:GridView ID="gvCriticalNotesHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
                EnableModelValidation="True" GridLines="None">
                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />

                <Columns>
                   
                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            
                                <asp:Label ID="lblHisPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPM_DATEDesc") %>'></asp:Label>
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Critical Notes" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="90%">
                        <ItemTemplate>
                             
                                <asp:Label ID="lblHisPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPM_CRITICAL_NOTES") %>'></asp:Label>
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                    
 


                </Columns>
            </asp:GridView>
      </div>

</asp:Content>
