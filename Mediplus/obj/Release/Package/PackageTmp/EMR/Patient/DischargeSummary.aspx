﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="DischargeSummary.aspx.cs" Inherits="Mediplus.EMR.Patient.DischargeSummary" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />


    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Discharge Summary </h3>
            </div>
        </div>
        <div style="padding-left: 60%; width: 100%;">
            <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
                Saved Successfully
            </div>
        </div>
        <table style="width: 100%">
            <tr>
                <td align="left"></td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" Text="Save Record" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>
        <table style="width:80%">
            <tr>
                <td class="lblCaption1" style="width: 200px;">Admission Date & Time
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtAdmissionDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtAdmissionDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:DropDownList ID="drpAdmissionHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                            <asp:DropDownList ID="drpAdmissionMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 200px;">Discharge Date & Time
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDischargeDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtDischargeDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:DropDownList ID="drpDisHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                            <asp:DropDownList ID="drpDisMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td>
                    <label for="AttendingPhysician" class="lblCaption1">Attending Physician</label></td>
                <td>
                    <select id="AttendingPhysician" runat="server" style="width: 200px;" class="lblCaption1">
                        <option value="">Select</option>
                    </select></td>

                <td  >
                    <label for="Dictatedby" class="lblCaption1">Dictated by </label>
                </td>
                <td>
                    <select id="Dictatedby" runat="server" style="width: 200px;" class="lblCaption1">
                        <option value="">Select</option>
                    </select></td>

            </tr>
            <tr>
                <td>
                    <label for="PrimaryCarePhysician" class="lblCaption1">Primary Care Physician</label></td>
                <td>
                    <select id="PrimaryCarePhysician" runat="server" style="width: 200px;" class="lblCaption1">
                        <option value="">Select</option>
                    </select></td>

                <td  >
                    <label for="ReferringPhysician" class="lblCaption1">Referring Physician</label></td>
                <td>
                    <select id="ReferringPhysician" runat="server" style="width: 200px;" class="lblCaption1">
                        <option value="">Select</option>
                    </select></td>

            </tr>
            <tr>
                <td>
                    <label for="ReferralPhysician1" class="lblCaption1">Consulting Physician</label></td>
                <td colspan="3">
                    <select id="ConsultingPhysician" runat="server" style="width: 200px;" class="lblCaption1">
                        <option value="">Select</option>
                    </select></td>

                <td  ></td>
                <td></td>

            </tr>
            <tr>
                <td  >
                    <label for="ConditionOnDischarge" class="lblCaption1">Condition on Discharge</label></td>
                <td colspan="3">
                    <textarea id="ConditionOnDischarge" runat="server" name="ReasonforAdmission" rows="3" cols="100" class="lblCaption1" ></textarea>
                </td>
            </tr>
            <tr>
                <td  >
                    <label for="FinalDiagnosis" class="lblCaption1">Final Diagnosis</label></td>
                <td colspan="3">
                    <textarea id="FinalDiagnosis" runat="server" name="ReasonforAdmission" rows="3" cols="100" class="lblCaption1" ></textarea>
                </td>
            </tr>
            <tr>
                <td  >
                    <label for="HistoryofPresentIllness" class="lblCaption1">History of Present Illness</label></td>
                <td colspan="3">
                    <textarea id="HistoryofPresentIllness" runat="server" name="ReasonforAdmission" rows="3" cols="100" class="lblCaption1" ></textarea>
                </td>
            </tr>
            <tr>
                <td style="width: 227px;">
                    <label for="Laboratory" class="lblCaption1">Laboratory</label></td>
                 <td colspan="3">
                    <textarea id="Laboratory" runat="server" name="Laboratory" rows="3" cols="100" class="lblCaption1"></textarea></td>
            </tr>
            
              <tr>
                    <td style="width:227px;">
                        <label for="HospitalCourse" class="lblCaption1">Hospital Course</label></td>
                     <td colspan="3">
                        <textarea id="HospitalCourse" runat="server" name="Laboratory" rows="3" cols="100" class="lblCaption1"></textarea></td>
                </tr>
             <tr>
                    <td style="width:227px;">
                        <label for="DischargeMedications" class="lblCaption1">Discharge Medications</label></td>
                     <td colspan="3">
                        <textarea id="DischargeMedications" runat="server" name="Laboratory" rows="3" cols="100" class="lblCaption1"></textarea></td>
                </tr>
              <tr>
                    <td style="width:227px;">
                        <label for="DischargeInstruction" class="lblCaption1">Discharge Instructions</label></td>
                     <td colspan="3">
                        <textarea id="DischargeInstruction" runat="server" name="Laboratory" rows="3" cols="100" class="lblCaption1"></textarea></td>
                </tr>
             <tr>
                    <td style="width:227px;">
                        <label for="FollowUpAppointments" class="lblCaption1">Follow up Appointments</label></td>
                    <td colspan="3">
                        <textarea id="FollowUpAppointments" runat="server" name="Laboratory" rows="3" cols="100" class="lblCaption1"></textarea></td>
                </tr>
        </table>


    </div>

    <br />

</asp:Content>
