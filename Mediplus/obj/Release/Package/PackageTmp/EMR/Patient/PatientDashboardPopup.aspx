﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientDashboardPopup.aspx.cs" Inherits=" Mediplus.EMR.Patient.PatientDashboardPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        function passvalue(TempData) {

            var CtrlName = document.getElementById('hidCtrlName').value;

            window.opener.BindTemplate(TempData, CtrlName);
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="hidCtrlName" runat="server" />
        <div>
            <table>
                <tr>
                    <td class="lblCaption">Segment List
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 300px; height: 500px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <asp:GridView ID="gvTemplates" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                    EnableModelValidation="True" Width="290px" ShowHeader="false" GridLines="None">
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />

                    <Columns>
                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Width="20px">
                            <ItemTemplate >
                                <asp:CheckBox ID="chk1" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" >
                            <ItemTemplate>

                                <asp:Label ID="lblTempCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_CODE") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lbTempName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_NAME") %>' Visible="false"></asp:Label>
                                
                                <asp:Label ID="lblTempData" CssClass="label"  Font-Size="11px" Width="100%" runat="server" Text='<%# Bind("ET_TEMPLATE") %>'></asp:Label>
                                 
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
            <table style="width:100%">
                <tr>
                    <td class="lblCaption" style="float:right">
                        <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="button blue small" OnClick="btnOK_Click" />
                         <asp:Button ID="Button2" runat="server" Text="Close" CssClass="button blue small" OnClientClick=" window.close();" />
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
