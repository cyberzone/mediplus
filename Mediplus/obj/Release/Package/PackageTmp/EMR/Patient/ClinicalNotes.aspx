﻿<%@ Page Title="" ValidateRequest="false" Language="C#" AutoEventWireup="true" CodeBehind="ClinicalNotes.aspx.cs" Inherits=" Mediplus.EMR.Patient.ClinicalNotes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>


    <!-- Start for TinyMCE -->

    <script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

    <script type="text/javascript">
        tinyMCE.init({
            // General options textareas
            mode: "textareas",
            theme: "advanced",
            editor_selector: "mceEditor",
            editor_deselector: "mceNoEditor",
            relative_urls: false,
            remove_script_host: false,
            // document_base_url : "http://localhost:1803/DIHWebsite/",
            document_base_url: '<% =getURL() %> ',
            //       setup : function(ed) {
            //          ed.onKeyUp.add(function(ed, e) {   
            //               var strip = (tinyMCE.activeEditor.getContent()).replace(/(<([^>]+)>)/ig,"");
            //               var text =   strip.length + " Characters"
            //        tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), text);   
            //         });},

            plugins: "safari,pagebreak,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: true,

            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "lists/template_list.js",
            external_link_list_url: "lists/link_list.js",
            external_image_list_url: "lists/image_list.js",
            media_external_list_url: "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

    </script>

    <!-- End for TinyMCE -->

</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="hidCreateTempType" runat="server" value="CLINICALNOTES" />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>

        <div style="float: right;">


            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save" OnClick="btnSave_Click" OnClientClick="ShowMessage();" />
            <asp:Button ID="btnModify" runat="server" CssClass="button orange small" Text="Modify" OnClick="btnModify_Click" OnClientClick="ShowMessage();" />

        </div>
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="button orange small" Text="Create Template" Enabled="false" OnClientClick="CreateTempClick('TREATMENTPLAN');"
                        Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                    <asp:LinkButton ID="lnkDisplayTemp" runat="server" CssClass="button orange small" Text="Select Template" Enabled="false" OnClientClick="CreateTempClick('TREATMENTPLAN');"
                        Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                     <asp:Button ID="btnPasteNotes" runat="server" Text="Paste"  CssClass="button orange small" OnClick="btnPasteNotes_Click" />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">

                    <asp:TextBox ID="txtClinicalNotes" Height="400" MaxLength="8000" TextMode="MultiLine" runat="server" class="mceEditor" />



                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="height: 10px;"></td>
            </tr>
            <tr>
                <td align="left" style="width: 800px" valign="top">
                    <div id="div1" runat="server" style="width: 800px; padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
                        <asp:Label ID="lblClinicalNotes1" runat="server" CssClass="label"></asp:Label>
                    </div>
                </td>
                <td style="width: 20%" valign="top">
                    <asp:Button ID="btnModify1" runat="server" CssClass="button orange small" Text="Modify" OnClick="btnModify1_Click" Visible="false" />
                </td>
            </tr>
              <tr>
                <td align="left" colspan="2" style="height: 10px;"></td>
            </tr>
            <tr>
                <td align="left" style="width: 800px" valign="top">
                    <div id="div2" runat="server" style="width: 800px; padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
                        <asp:Label ID="lblClinicalNotes2" runat="server" CssClass="label"></asp:Label>
                    </div>
                </td>
                <td style="width: 20%" valign="top">
                    <asp:Button ID="btnModify2" runat="server" CssClass="button orange small" Text="Modify" OnClick="btnModify2_Click" Visible="false" />
                </td>
            </tr>
              <tr>
                <td align="left" colspan="2" style="height: 10px;"></td>
            </tr>
            <tr>
                <td align="left" style="width: 800px" valign="top">
                    <div id="div3" runat="server" style="width: 800px; padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
                        <asp:Label ID="lblClinicalNotes3" runat="server" CssClass="label"></asp:Label>
                    </div>
                </td>
                <td style="width: 20%" valign="top">
                    <asp:Button ID="btnModify3" runat="server" CssClass="button orange small" Text="Modify" OnClick="btnModify3_Click" Visible="false" />
                </td>
            </tr>
              <tr>
                <td align="left" colspan="2" style="height: 10px;"></td>
            </tr>
            <tr>
                <td align="left" style="width: 800px" valign="top">
                    <div id="div4" runat="server" style="width: 800px; padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
                        <asp:Label ID="lblClinicalNotes4" runat="server" CssClass="label"></asp:Label>
                    </div>
                </td>
                <td style="width: 20%" valign="top">
                    <asp:Button ID="btnModify4" runat="server" CssClass="button orange small" Text="Modify" OnClick="btnModify4_Click" Visible="false" />
                </td>
            </tr>
              <tr>
                <td align="left" colspan="2" style="height: 10px;"></td>
            </tr>
            <tr>
                <td align="left" style="width: 800px" valign="top">
                    <div id="div5" runat="server" style="width: 800px; padding-top: 0px; border: 1px; border-color: #dcdcdc; border-style: groove;" visible="false">
                        <asp:Label ID="lblClinicalNotes5" runat="server" CssClass="label"></asp:Label>
                    </div>
                </td>
                <td style="width: 20%" valign="top">
                    <asp:Button ID="btnModify5" runat="server" CssClass="button orange small" Text="Modify" OnClick="btnModify5_Click" Visible="false" />
                </td>
            </tr>


        </table>


        <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
            PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
            PopupDragHandleControlID="pnlSaveTemplate">
        </asp:ModalPopupExtender>

        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
            <table cellpadding="0" cellspacing="0" width="100%" style="">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption" style="font-weight: bold;">Template List
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                        <fieldset style="height: 50px;">
                            <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                            <table>
                                <tr>
                                    <td>Template Name : 
                                    </td>
                                    <td>
                                        <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                        <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

                    </td>
                </tr>

            </table>
        </asp:Panel>
        <asp:ModalPopupExtender ID="ModalPopupExtender6" runat="server" TargetControlID="lnkDisplayTemp"
            PopupControlID="pnlDisplayTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnDisplayClose"
            PopupDragHandleControlID="pnlDisplayTemplate">
        </asp:ModalPopupExtender>

        <asp:Panel ID="pnlDisplayTemplate" runat="server" Height="500px" Width="300px" CssClass="modalPopup">
            <table cellpadding="0" cellspacing="0" width="100%" style="">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnDisplayClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption" style="font-weight: bold;">Template List
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1" style="height: 400px; vertical-align: top;">
                        <asp:GridView ID="gvTemplates" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                            EnableModelValidation="True" Width="290px" ShowHeader="false">
                            <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />

                            <Columns>

                                <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkName" runat="server" OnClick="TempSelect_Click">
                                            <asp:Label ID="lblTempCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblTempData" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_TEMPLATE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lbTempName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <asp:Button ID="btnDisplayClose1" runat="server" Text="Close" CssClass="button blue small" />

                    </td>
                </tr>

            </table>
        </asp:Panel>
    </form>
</body>

<style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>

</html>


 