﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Remittance.aspx.cs" Inherits="Mediplus.Insurance.Remittance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        .auto-style1
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            height: 25px;
            width: 242px;
        }

        .auto-style2
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            width: 242px;
        }
    </style>


    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                     document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                 }


             }

             return true;
        }

        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
             document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

             if (vColor != '') {

                 document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


            }

            document.getElementById("divMessage").style.display = 'block';



        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }


       
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
  
    <table>
        <tr>
            <td class="PageHeader">Remittance
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
      <div style="padding-left: 60%; width: 100%;">


       <div id="divMessage" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>

                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div class="lblCaption1" style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
        <table cellspacing="5" cellpadding="5" width="100%">
            <tr>
                <td style="width: 150px; height: 30px;">
                    <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                        Text="Brows XML or Excel File"></asp:Label>
                </td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="300px">
                                <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" AllowMultiple="true"  />
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnShow" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpPanelSearch" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnShow" runat="server" CssClass="button orange small" Width="100px"
                                OnClick="btnShow_Click" Text="Load Details" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
            <tr>
                <td>
                      File Name(s)
                <asp:Label ID="lblFileList" runat="server" CssClass="lblCaption1" ></asp:Label>
                </td>
               
            </tr>
        </table>
    </div>
    <br />
    <div class="lblCaption1" style="width: 80%; height: 80px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <table cellspacing="5" cellpadding="5" width="100%">
            <tr>
                <td class="lblCaption1">File Name 

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFileName" runat="server" CssClass="TextBoxStyle" Width="300px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


                <td class="lblCaption1">From Date 
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFrmDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtFrmDt" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1">To Date
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtToDt" runat="server" Width="75px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtToDt" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td></td>
            </tr>
            <tr>
                <td class="lblCaption1">Company
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" Height="22px" Width="100px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" Height="22px" Width="250px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>
                            <div id="divComp" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px; width: 100px;">Reference #
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcReferenceNo" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small"
                                OnClick="btnSearch_Click" Text="Search" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSearch">
                        <ProgressTemplate>
                            <div id="overlay">
                                <div id="modalprogress">
                                    <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="200" Width="350" />
                                </div>
                            </div>

                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnExport" runat="server" CssClass="button gray small" Width="130px"
                                OnClick="btnExport_Click" Text="Export To Excel" ToolTip="Above Selected data export to Excel sheet" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpPanelSubmissionData" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvRemittanceData" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvRemittanceData_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200" GridLines="Both" CellPadding="2" CellSpacing="2">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="Select_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SL.No" SortExpression="HPV_SEQNO" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerial" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("XMLFILENAME") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FILE NAME" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("ActualFileName") %>'></asp:Label>
                                            <asp:Label ID="lblXMLFileName" CssClass="GridRow" runat="server" Text='<%# Bind("XMLFILENAME") %>' Visible="false"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SENDER ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSenderID" CssClass="GridRow" runat="server" Text='<%# Bind("SenderID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyID") %>'></asp:Label>
                                            <asp:Label ID="lblCompanyName" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PROVIDER" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="lblProviderID" CssClass="GridRow" runat="server" Text='<%# Bind("ProviderID") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REF NO" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>


                                            <asp:Label ID="lblRefNo" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentReference") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TRANS. COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCount" CssClass="GridRow" runat="server" Text='<%# Bind("Count") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PT. SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYMENT AMT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaymentAmount" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSubmissionData">
                        <ProgressTemplate>
                            <div id="overlay">
                                <div id="modalprogress">
                                    <asp:Image ID="imgLoaderSubmissionData" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="200" Width="350" />
                                </div>
                            </div>

                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>

        </table>

    </div>
    <div style="padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvRemittanceClaims" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvRemittanceClaims_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>

                                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:UpdatePanel runat="server" ID="UpdatePanel44"
                                                UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                <ContentTemplate>
                                                    <asp:CheckBox ID="chkInvResub" runat="server" CssClass="label" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="chkInvResub" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SL.No" SortExpression="HPV_SEQNO" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerial" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("ClaimId") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CLAIM ID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("ActualFileName") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblIDPayer" CssClass="GridRow" runat="server" Text='<%# Bind("IDPayer") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblClaimID" CssClass="GridRow" runat="server" Font-Bold="true" Text='<%# Bind("ClaimId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DATE" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("START1") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="COMPANY" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyID" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblCompanyName" CssClass="GridRow" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="REFERENCE NO" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>


                                            <asp:Label ID="lblRefNo" CssClass="GridRow" runat="server" Text='<%# Bind("PaymentReference") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SERV. COUNT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCount" CssClass="GridRow" runat="server" Text='<%# Bind("Count") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYER SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalNet" CssClass="GridRow" runat="server" Text='<%# Bind("Net") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PATIENT SHARE" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblTotalPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("PatientShare") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("Gross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PAYMENT AMOUNT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>


                                            <asp:Label ID="lblPaymentAmount" CssClass="GridRow" Font-Bold="true" runat="server" Text='<%# Bind("PaymentAmount") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" CssClass="GridRow" runat="server" Font-Bold="true" Text='<%# Bind("InvoiceStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
    </div>

    <table width="80%" align="center">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanelSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSaveToDB" runat="server" CssClass="button red small" Width="100px" OnClientClick="return Val();" OnClick="btnSave_Click" Text="Save" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                  <asp:UpdateProgress ID="UpdateProgress4" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpdatePanelSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoaderGenerateSave" runat="server" ImageUrl="~/Images/loading.gif"  AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>

            <td style="float: right;">
                <asp:UpdatePanel ID="UpPanelGenerateReceipt" runat="server">
                    <ContentTemplate>

                        <asp:Button ID="btnVerified" runat="server" CssClass="button red small" Width="100px" OnClick="btnVerified_Click" Text="Verified" Visible="false" />
                        <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                            Text="Receipt No"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtReceiptNo" runat="server" CssClass="TextBoxStyle" Width="135px" ReadOnly="true"></asp:TextBox>

                        <asp:Button ID="btnReceiptGenerate" runat="server" CssClass="button red small" Width="120px" OnClick="btnReceiptGenerated_Click" Text="Generate Receipt" />

                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress3" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelGenerateReceipt">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoaderGenerateReceipt" runat="server" ImageUrl="~/Images/loading.gif"  AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
