﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="RemittanceReverse.aspx.cs" Inherits="Mediplus.Insurance.RemittanceReverse" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        .auto-style1
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            height: 25px;
            width: 242px;
        }

        .auto-style2
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            width: 242px;
        }
    </style>


    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

     <script language="javascript" type="text/javascript">

        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

            if (vColor != '') {

                document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


             }

             document.getElementById("divMessage").style.display = 'block';



         }

         function HideErrorMessage() {

             document.getElementById("divMessage").style.display = 'none';

             document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <table>
        <tr>
            <td class="PageHeader">Remittance Reverse
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
      <div style="padding-left: 60%; width: 100%;">


       <div id="divMessage" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>

                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div class="lblCaption1" style="width: 80%; height: 50px; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
        <table cellspacing="5" cellpadding="5" width="100%">
            <tr>
                <td style="width: 150px; height: 30px;">
                    <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                        Text="Brows XML or Excel File"></asp:Label>
                </td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="300px">
                                <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnReceiptReverse" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td>
                      <asp:UpdatePanel ID="UpPanelGenerateReceipt" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnReceiptReverse" runat="server" CssClass="button red small" Width="120px" OnClick="btnReceiptReverse_Click" Text="Receipt Reverse" OnClientClick="return window.confirm('Do you want to Reverse the Receipt?')" />

                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress3" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelGenerateReceipt">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoaderGenerateReceipt" runat="server" ImageUrl="~/Images/loading.gif"  AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>

                </td>
            </tr>
        </table>
    </div>
    
    <br />
</asp:Content>
