﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="PaymentEntry.aspx.cs" Inherits="Mediplus.Accounts.PaymentEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

     
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .modalPopup
        {
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px 10px 10px 10px;
        }
    </style>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowJournalMasterPopup() {

            document.getElementById("divJournalGridPopup").style.display = 'block';


        }

        function HideJournalMasterPopup() {


            document.getElementById("<%=divJournalGridPopup.ClientID%>").style.display = 'none';
        }

        function DisplayPayDtls() {
            document.getElementById("divCC").style.display = 'none';
            document.getElementById("divCheque").style.display = 'none';
            document.getElementById("divBank").style.display = 'none';

            var vValue = document.getElementById("<%=drpPayType.ClientID%>").value;

            if (vValue == "Credit Card") {
                document.getElementById("divCC").style.display = 'block';
            }
            if (vValue == "Cheque") {
                document.getElementById("divCheque").style.display = 'block';
            }

            if (vValue == "Bank") {
                document.getElementById("divBank").style.display = 'block';
            }



        }

        
        function SaveValidate() {


            var TrNo = document.getElementById("<%=txtJournalNo.ClientID%>").value;
            var TrDate = document.getElementById("<%=txtTransDate.ClientID%>").value;
            var DrAmt = document.getElementById("<%=txtDrAmount.ClientID%>").value;
           

            if (TrNo == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Trans. Bumber";
                 document.getElementById('<%=txtJournalNo.ClientID%>').focus;
                return false;
            }


            if (TrDate == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Trans. Date";
                 document.getElementById('<%=txtTransDate.ClientID%>').focus;
                return false;
            }

            if (DrAmt == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Amount";
                 document.getElementById('<%=txtDrAmount.ClientID%>').focus;
                return false;
            }

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <table style="width: 75%">
        <tr>
            <td align="left">
                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Payment Entry"></asp:Label>

            </td>
            <td align="right">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" OnClientClick="SaveValidate()" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>

    <div style="padding-top: 0px; width: 75%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

        <table cellpadding="5" cellspacing="5" border="0" style="width: 100%">

            <tr>
                <td class="lblCaption1" style="width: 100px;">Trans. No.
                       <asp:ImageButton ID="imgPaymentZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Payment Entry" OnClick="imgPaymentZoom_Click" />

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtJournalNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtJournalNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td class="lblCaption1">Trans. Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1">Ref
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRef" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


                <td class="lblCaption1">Status
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                <asp:ListItem Value="Active">Active</asp:ListItem>
                                <asp:ListItem Value="InActive">InActive</asp:ListItem>

                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Category  </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpDrCategory" runat="server" CssClass="TextBoxStyle" Width="100%" Height="22px" AutoPostBack="true" OnSelectedIndexChanged="drpDrCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">A/C Name  </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpDrAccountName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="22px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Cost Center </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpDrCostCenter" runat="server" CssClass="TextBoxStyle" Width="100%" Height="22px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Amount  </td>
                <td style="vertical-align: top;">

                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDrAmount" runat="server" CssClass="TextBoxStyle" Style="text-align: right;" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 50px;">Pay.Type</td>
                <td style="width: 125px;">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <select id="drpPayType" runat="server" style="width: 200px;" class="TextBoxStyle" onchange="DisplayPayDtls()">
                                <option value="Cash">Cash</option>
                                <option value="Credit Card">Credit Card</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Bank">Bank</option>
                            </select>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="width: 100px;">Description 
                </td>
                <td colspan="6">
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="TextBoxStyle" TextMode="MultiLine" Width="100%" Height="30px" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
        <div id="divCC" style="display: none;">
            <table style="width: 100%;">
                <tr>
                    <td class="lblCaption1" style="width: 105px;">Type</td>
                    <td style="width: 125px;">
                        <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpCCType" runat="server" CssClass="TextBoxStyle" Width="120px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 70px;">C.C Amt.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel50" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCCAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">C.C No.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCCNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Holder
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCCHolder" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Ref No.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCCRefNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divCheque" style="display: none;">
            <table style="width: 100%;">
                <tr>
                    <td class="lblCaption1" style="width: 105px;">Cheq. Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtChequeDate" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                    Enabled="True" TargetControlID="txtChequeDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                    <td class="lblCaption1" style="width: 70px;">Cheq. Amt.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtChequeAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Cheq. No.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtChequeNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>


                    <td class="lblCaption1">Bank
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpChequeBank" CssClass="TextBoxStyle" runat="server" Width="250px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>

        <div id="divBank" style="display: none;">
            <table style="width: 100%;">
                <tr>
                    <td class="lblCaption1" style="width: 105px;">Payment Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtBankPayDate" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender6" runat="server"
                                    Enabled="True" TargetControlID="txtBankPayDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 70px;">Amount.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtBankPayAmount" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Ref No.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="drpBankRefNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Bank
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpBank" CssClass="TextBoxStyle" runat="server" Width="250px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <table style="padding-left: 0px; border: 1PX; border-style: groove;" border="1" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="lblCaption1">Category  </td>
                <td class="lblCaption1">A/C Name  </td>
                <td class="lblCaption1">Cost Center </td>
                <td class="lblCaption1">Amount  </td>
                <td></td>
            </tr>

            <tr>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpCategory" runat="server" CssClass="TextBoxStyle" Width="100%" Height="22px" AutoPostBack="true" OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpAccountName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="22px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpCostCenter" runat="server" CssClass="TextBoxStyle" Width="100%" Height="22px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCrAmount" runat="server" CssClass="TextBoxStyle" Style="text-align: right;" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:ImageButton ID="btnAddTrans" runat="server" ImageUrl="~/Images/New.png" Style="height: 22px; width: 22px;" ToolTip="Create New Entry" OnClick="btnAddTrans_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">

            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvJournalTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False" ShowFooter="true"
                                EnableModelValidation="True" Width="99%" gridline="none">
                                <HeaderStyle CssClass="GridHeader_Gray" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <FooterStyle CssClass="GridHeader_Blue" />
                                <RowStyle CssClass="GridRow" />

                                <Columns>

                                    <asp:TemplateField HeaderText="CATEGORY">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJourNoe" CssClass="label" runat="server" Text='<%# Bind("AJT_JOURNAL_NO") %>' Visible="false"></asp:Label>

                                            <asp:Label ID="lblTransID" CssClass="label" runat="server" Text='<%# Bind("AJT_TRANS_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblCategory" CssClass="label" runat="server" Text='<%# Bind("AJT_CATEGORY") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ACCOUNT NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccCode" CssClass="label" runat="server" Text='<%# Bind("AJT_ACC_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblAccName" CssClass="label" runat="server" Text='<%# Bind("AJT_ACC_NAME") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="COST CENTER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblccCode" CssClass="label" runat="server" Text='<%# Bind("AJT_COST_CENT_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblccName" CssClass="label" runat="server" Text='<%# Bind("AJT_COST_CENT_NAME") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DESCRIPTION">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" CssClass="label" runat="server" Text='<%# Bind("AJT_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="AMOUNT" HeaderStyle-Width="100px" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDrAmount" CssClass="label" Style="text-align: right; padding-right: 5px;" runat="server" Text='<%# Bind("AJT_DEBIT_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtTotalDRAmount" runat="server" CssClass="TextBoxStyle" Style="text-align: right; color: white; font-weight: bold;" Enabled="false" Width="100%"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ACTION" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteJournalTrans" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="12px" Height="12px"
                                                OnClick="DeleteJournalTrans_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <br />

        <asp:UpdatePanel ID="UpdatePanel26" runat="server">
            <ContentTemplate>
                <div id="divJournalGridPopup" runat="server" visible="false" style="overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 250px;">
                    <div style="width: 100%; text-align: right; float: right; position: absolute; top: 0px; right: 0px;">

                        <img src="../Images/Close.png" style="height: 25px; width: 25px;" onclick="HideJournalMasterPopup()" />

                    </div>

                    <table cellpadding="5" cellspacing="5" style="width: 100%">

                        <tr>
                            <td class="lblCaption1">From Date
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcFromDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                            Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">To Date
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSrcToDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                            Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button red small" Width="70px" OnClick="btnSearch_Click" Text="Search" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>

                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="lblCaption1" style="height: 400px; vertical-align: top;">
                                <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gvJournalMaster" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30" Visible="false"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="GridHeader_Gray" />
                                            <AlternatingRowStyle CssClass="GridAlterRow" />
                                            <RowStyle CssClass="GridRow" />

                                            <Columns>

                                                <asp:TemplateField HeaderText="Trn. Number" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkJournalNo" runat="server" OnClick="JournalSelect_Click">
                                                            <asp:Label ID="lblJournalNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AJM_JOURNAL_NO") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkReTransDate" runat="server" OnClick="JournalSelect_Click">
                                                            <asp:Label ID="lblReTransDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AJM_DATEDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="RefNo" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkRefNo" runat="server" OnClick="JournalSelect_Click">
                                                            <asp:Label ID="lblRefNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AJM_REF_NO") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DR Amount" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkReAccName" runat="server" OnClick="JournalSelect_Click">
                                                            <asp:Label ID="lblReAccName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AJM_DR_TOTAL_AMOUNT") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CR Amount" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkReAmount" runat="server" OnClick="JournalSelect_Click">
                                                            <asp:Label ID="lblReAmount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AJM_CR_TOTAL_AMOUNT") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>


                    </table>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
