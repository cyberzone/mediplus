﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="CostCenterMaster.aspx.cs" Inherits="Mediplus.Accounts.CostCenterMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

       
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }

    </script>
     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

         function ShowCostCentMsg(vMessage1, vMessage2) {

             document.getElementById("divCostCentMessage").style.display = 'block';

             document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
         document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
       }

       function HideCostCentMsg() {

           document.getElementById("divCostCentMessage").style.display = 'none';

           document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div>
             <div style="padding-left: 60%; width: 100%;">

               
                <div id="divCostCentMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999;  border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">
               
                      <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                              <span style="float:right;"> 
                      
                                  <input type="button" id="Button1" class="ButtonStyle" style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideCostCentMsg()" />

                              </span>
                            <br />
                           <div style="width:100%;height:20px;background:#E3E3E3;">
                                 &nbsp; <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold;color:#02a0e0;font-weight:bold;"></asp:Label>
                           </div>
                                  &nbsp; <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold;color:#666;"></asp:Label>
                            
                    </ContentTemplate>
                                </asp:UpdatePanel>

                    <br />

                </div>
            </div>
        <div>
            <table style="width: 75%">
                <tr>
                    <td align="left">
                         <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Cost Center Master"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                 <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click" Text="Save" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 70%; height: 400px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 20px;">

                <div style="width: 100%; text-align: right; float: right;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>

                            <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
               <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
                <table cellpadding="5" cellspacing="5" style="width: 100%">
                    <tr>
                        <td class="lblCaption1" style="width: 120px;">Cost Center Number
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCostCenterNo" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                                    <span style="color: red; font-size: 11px;">*</span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Cots Center Name
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCostCenterName" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                                    <span style="color: red; font-size: 11px;">*</span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                </table>
                <div style="width: 100%; text-align: right; float: right;">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>

                            <asp:ImageButton ID="imgACMasRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="imgACMasRefres_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvCCMaster" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader_Gray" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>

                                    <asp:TemplateField HeaderText="Cost Center Number">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAccountCode" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblCCCode" CssClass="GridRow" runat="server" Text='<%# Bind("ACCM_CODE") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAccountName" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblCCName" CssClass="GridRow" runat="server" Text='<%# Bind("ACCM_CC_NAME") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Actions" Visible="true" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="../Images/icon_delete.png" AlternateText="Delete"
                                            OnClick="Delete_Click" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="gvCCMaster" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
            </div>


        </div>
            
</div>
    

   </asp:Content>