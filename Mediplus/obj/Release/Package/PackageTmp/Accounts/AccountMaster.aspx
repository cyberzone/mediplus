﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="AccountMaster.aspx.cs" Inherits="Mediplus.Accounts.AccountMaster" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="width: 75%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Account Master"></asp:Label>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                             <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 70%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">


            <div style="width: 100%; text-align: right; float: right;">
                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>

                        <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
            <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Account Name 
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtAccountName" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>

                    <td class="lblCaption1">Category
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpCategory" runat="server" CssClass="TextBoxStyle" Width="90.5%" Height="25px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">YTD
                    </td>
                    <td style="vertical-align: bottom">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>


                                <asp:TextBox ID="txtYTD" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" ToolTip="Balance of current Year"></asp:TextBox>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="text-align: left;">
                        <asp:ImageButton ID="btnHistory" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px; border-radius: 50%" ToolTip="Create New Entry" />

                    </td>
                    <td class="lblCaption1">Balance
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtBalance" runat="server" CssClass="TextBoxStyle" Width="75%" Height="20px" ToolTip="Balance Amount"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                </tr>
            </table>
            <div style="width: 100%; text-align: right; float: right;">
                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                    <ContentTemplate>

                        <asp:ImageButton ID="imgACMasRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="imgACMasRefres_Click" />

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>

                        <asp:GridView ID="gvAccountMaster" runat="server" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" PageSize="200">
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkAccountCode" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblAccountCode" CssClass="GridRow" runat="server" Text='<%# Bind("AAM_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblAccountName" CssClass="GridRow" runat="server" Text='<%# Bind("AAM_ACCOUNT_NAME") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Category">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkCategory" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblCategory" CssClass="GridRow" runat="server" Text='<%# Bind("AAM_CATEGORY") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Actions" Visible="true" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="../Images/icon_delete.png" AlternateText="Delete"
                                            OnClick="Delete_Click" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvAccountMaster" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>


    </div>
</asp:Content>
