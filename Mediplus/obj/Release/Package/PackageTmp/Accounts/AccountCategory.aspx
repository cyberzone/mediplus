﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="AccountCategory.aspx.cs" Inherits="Mediplus.Accounts.AccountCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script language="javascript" type="text/javascript">

         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }

         function ShowTypeDive() {

             alter('ter');
         }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <table style="width: 75%">
            <tr>
                <td align="left">
                     <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Account Category"></asp:Label>
                    
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                             <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

         <div style="padding-top: 0px; width: 75%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;"> 
               <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>

            <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                     <td class="lblCaption1" style="width: 100px;">Branch
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                    <asp:DropDownList ID="drpBranch" runat="server" CssClass="TextBoxStyle" Width="90.5%" Height="25px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Category 
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCategoryName" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Sub Category  
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSubCategoryName" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Main Category  
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>
                                     <asp:DropDownList ID="drpMainCategory" runat="server" CssClass="TextBoxStyle" Width="90.5%" Height="25px">
                                          <asp:ListItem Text="--- Select ---" Value="" ></asp:ListItem>
                                         <asp:ListItem Text="ASSET" Value="ASSET" ></asp:ListItem>
                                         <asp:ListItem Text="LIABILITY" Value="LIABILITY" ></asp:ListItem>
                                          <asp:ListItem Text="EXPENSE" Value="EXPENSE" ></asp:ListItem>
                                          <asp:ListItem Text="INCOME" Value="INCOME" ></asp:ListItem>

                                  </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    
                </tr>
                <tr>
                     <td class="lblCaption1" style="width: 100px;">Start No
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                       <asp:TextBox ID="txtStartNo" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                      <td class="lblCaption1" style="width: 100px;">End No
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                       <asp:TextBox ID="txtEndNo" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 100px;">Current No
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                       <asp:TextBox ID="txtCurrentNo" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"    onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                      <td class="lblCaption1" style="width: 100px;">Prefix
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                       <asp:TextBox ID="txtPrefix" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"   ></asp:TextBox>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                  
                    </tr>

                </table>

            <br />
              <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="gvCategoryMaster" runat="server" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%" PageSize="200">
                                    <HeaderStyle CssClass="GridHeader_Gray" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <RowStyle CssClass="GridRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="Category">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCategory" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblBranchID" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_BRANCH_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCategory" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_CATEGORY") %>'></asp:Label>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sub Category">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSubCategory" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblSubCategory" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_SUB_CATEGORY") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Main Category">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMainCategory" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblMainCategory" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_MAIN_CATEGORY") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Start No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkStartNo" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblStartNo" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_START_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="End No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEndNo" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblEndNo" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_END_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Current No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCuttentNo" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblCuttentNo" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_CURRENT_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Prefix">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPrefix" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblPrefix" CssClass="GridRow" runat="server" Text='<%# Bind("ACM_PREFIX") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        
                                         
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                                </asp:GridView>
                            </ContentTemplate>
                   
                        </asp:UpdatePanel>
                    </div>


             </div>
</asp:Content>
