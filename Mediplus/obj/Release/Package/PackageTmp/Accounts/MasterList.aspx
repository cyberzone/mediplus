﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MasterList.aspx.cs" Inherits="Mediplus.Accounts.MasterList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

       <script language="javascript" type="text/javascript">
       

           function passvalue(Code, Name, Category) {
             
               var strCtrlName = document.getElementById('hidCtrlName').value;
                  
               window.opener.BindAccountDetails(strCtrlName, Code, Name, Category);

                   opener.focus();
                   window.close();
              
             }

        </script>
</head>
<body>
    <form id="form1" runat="server">
           <asp:hiddenfield id="hidPageName" runat="server" />
           <asp:hiddenfield id="hidCtrlName" runat="server" />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <div>
       <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>

                        <asp:GridView ID="gvAccountMaster" runat="server" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" PageSize="200">
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                     <a href="#" onclick="passvalue('<%# Eval("AAM_CODE") %>', '<%# Eval("AAM_ACCOUNT_NAME") %>', '<%# Eval("AAM_CATEGORY") %>')">
                                            <asp:Label ID="lblAccountCode" CssClass="GridRow" runat="server" Text='<%# Bind("AAM_CODE") %>' ></asp:Label>
 
                                       </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                     <a href="#" onclick="passvalue('<%# Eval("AAM_CODE") %>', '<%# Eval("AAM_ACCOUNT_NAME") %>', '<%# Eval("AAM_CATEGORY") %>')">
                                             <asp:Label ID="lblAccountName" CssClass="GridRow" runat="server" Text='<%# Bind("AAM_ACCOUNT_NAME") %>'></asp:Label>

                                       </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Category">
                                    <ItemTemplate>
                                       <a href="#" onclick="passvalue('<%# Eval("AAM_CODE") %>', '<%# Eval("AAM_ACCOUNT_NAME") %>', '<%# Eval("AAM_CATEGORY") %>')">
                                            <asp:Label ID="lblCategory" CssClass="GridRow" runat="server" Text='<%# Bind("AAM_CATEGORY") %>'></asp:Label>
                                      </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                            </Columns>
                            <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvAccountMaster" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
    </div>
    </form>
</body>
</html>
