﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Mediplus.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">


        function ShowForgotPasswordPopup() {

            document.getElementById("divForgotPassword").style.display = 'block';


        }

        function HideForgotPasswordsPopup() {

            document.getElementById("divForgotPassword").style.display = 'none';

        }

        function ShowUserDetailsPopup() {
            document.getElementById("divUserDetails").style.display = 'block';
        }

        function HideUserDetailsPopup() {

            document.getElementById("divUserDetails").style.display = 'none';

        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="table" cellpadding="5" cellspacing="5">
        <tr>
            <td>
                <label for="BranchID" style="font-family: BenchNine, sans-serif; color: rgb(92, 92, 92);">Branch Name</label></td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpBranch" runat="server" Width="250px" Height="25px">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td>
                <label for="UserID" style="font-family: BenchNine, sans-serif; color: rgb(92, 92, 92);">User ID</label></td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpUsers" runat="server" Width="250px" AutoPostBack="false">
                        </asp:DropDownList>

                        <asp:TextBox ID="txtUserID" runat="server" Width="245px" Visible="false" AutoPostBack="false"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td>
                <label for="Password" style="font-family: BenchNine, sans-serif; color: rgb(92, 92, 92);">Password</label></td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="245px" Height="20px" MaxLength="15"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a id="aPwdChange" runat="server" href="javascript:ShowForgotPasswordPopup();" class="label" style="text-decoration: none; color: #666666; display: block; width: 120px;">[Forgot Password]</a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnLogin" runat="server" CssClass="button red" Height="30px" Width="250px"
                            OnClick="btnLogin_Click" Text="Login" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div id="divForgotPassword" style="display: none; overflow: hidden; border: groove; height: 400px; width: 700px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 400px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="PageHeader" style="vertical-align: top; width: 50%;">Forgot Password
                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideForgotPasswordsPopup()" />
                </td>
            </tr>

        </table>
        <div style="width: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;"></div>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="height: 10px;">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

        <table class="table" cellpadding="1" cellspacing="1">

            <tr>
                <td class="lblCaption1" style="height: 30px;">Mail ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMailID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="400px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>

                <td class="lblCaption1" style="height: 30px;"></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnForgotpassword" runat="server" CssClass="button red small" Width="100px" OnClick="btnForgotpassword_Click"   Text="Proceed" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

    </div>

    <div id="divUserDetails" style="display: none; overflow: hidden; border: groove; height: 300px; width: 700px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 400px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="PageHeader" style="vertical-align: top; width: 50%;">Change Password
                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideUserDetailsPopup()" />
                </td>
            </tr>

        </table>
        <div style="width: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;"></div>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="height: 10px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <asp:Label ID="lblChangePwdStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

        <table class="table" cellpadding="1" cellspacing="1">

            <tr>
                <td class="lblCaption1" style="height: 30px;">User ID 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtChangePwdUserID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px;">Old Password : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtOldPwd" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px" TextMode="Password">></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px;">New Password : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNewPwd" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px" TextMode="Password"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px;">Confirm Password : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtConPwd" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px" TextMode="Password">></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px;"></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" OnClientClick="return validatechangepawd();" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
    </div>

    <script type="text/javascript" language="Javascript">


        function validatechangepawd() {

            if (document.getElementById('<%=txtOldPwd.ClientID%>').value == "") {
                                    alert("Please Enter the Old Password");
                                    document.getElementById('<%=txtOldPwd.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtNewPwd.ClientID%>').value == "") {
                                    alert("Please Enter the New Password");
                                    document.getElementById('<%=txtNewPwd.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtConPwd.ClientID%>').value == "") {
                                    alert("Please Enter the Confirm Password");
                                    document.getElementById('<%=txtConPwd.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtNewPwd.ClientID%>').value != document.getElementById('<%=txtConPwd.ClientID%>').value) {
                                    alert("Please Enter the Same Confirm Password");
                                    document.getElementById('<%=txtConPwd.ClientID%>').value = "";
                document.getElementById('<%=txtConPwd.ClientID%>').focus();
            return false;
        }

        if (txtPassword(document.getElementById('<%=txtNewPwd.ClientID%>').value) == false) {
                                    alert('Password ! Single Code Characters are not Allowed');
                                    document.getElementById('<%=txtNewPwd.ClientID%>').value = "";
                document.getElementById('<%=txtConPwd.ClientID%>').value = "";
            document.getElementById('<%=txtNewPwd.ClientID%>').focus();
            return false;
        }

        if (txtPassword(document.getElementById('<%=txtOldPwd.ClientID%>').value) == false) {
                                    alert('The Old Password is incorrect');
                                    document.getElementById('<%=txtOldPwd.ClientID%>').value = "";
                document.getElementById('<%=txtOldPwd.ClientID%>').focus();
            return false;
        }
                                //return true;
        return ConfirmMsg();

    }

    </script>
</asp:Content>
