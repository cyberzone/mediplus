﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="LoyaltyPrograms.aspx.cs" Inherits="Mediplus.LoyaltyPrograms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="padding-top: 0px; padding-left: 5px; width: 80%; height: 650px; border: thin; border-color: #CCCCCC; border-style: groove;">
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="text-align: left; vertical-align: top; width: 15%;">

                    <img src="Images/kmc_logo.png" height="150" />
                </td>

                <td>
                    <table style="width: 100%">
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Loyalty Programs"></asp:Label>
                            </td>

                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="style2">
                                <asp:Label ID="Label18" runat="server" CssClass="lblCaption1"
                                    Text="Name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcName" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" Height="25px"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Last Name

                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcLName" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" Height="25px"></asp:TextBox>
                            </td>
                            <td>

                                <asp:Label ID="Label31" runat="server" CssClass="lblCaption1" Text="File No"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcFileNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" Height="25px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="Label39" runat="server" CssClass="lblCaption1"
                                    Text="Mobile"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcMobile1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" Height="25px"></asp:TextBox>

                            </td>
                            <td>
                                <asp:Label ID="Label101" runat="server" CssClass="lblCaption1"
                                    Text="PH.No"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcPhone1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" Height="25px"></asp:TextBox>
                            </td>

                            <td align="right" style="padding-right: 20px;">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnFind" runat="server" CssClass="button red small"
                                            OnClick="btnFind_Click" Text=" Find " />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>
                    <div style="padding-top: 0px; padding-left: 5px; width: 100%; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvGridView" runat="server" AllowPaging="True"
                                    AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting"
                                    EnableModelValidation="True" Width="100%" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="50" GridLines="None">
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="FILE NO" SortExpression="HPM_PT_ID">
                                            <ItemTemplate>

                                                <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblPatientId" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NAME" SortExpression="HPM_PT_FNAME">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFullName" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblPatientName" CssClass="label" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MOBILE NO." SortExpression="HPM_MOBILE">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMobile" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PHONE NO." SortExpression="HPM_PHONE1">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhone" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblPhone" CssClass="label" runat="server" Text='<%# Bind("HPM_PHONE1") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="PT TYPE" SortExpression="HPM_PT_TYPEEDesc">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPTType" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblPTType" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EARN POINT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkLoyaltyPoint" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblLoyaltyPoint" CssClass="label" runat="server" Text="50" ReadOnly="true"></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>

                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>

                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: left; vertical-align: top;"></td>
                            <td style="text-align: right; vertical-align: top;">
                                <table style="width: 100%;" cellpadding="5" cellspacing="5">
                                    <tr>
                                        <td style="text-align: right; font-weight: bolder;" class="lblCaption1">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    Earn Point  :
                <asp:TextBox ID="txtLoyaltyPoint" runat="server" CssClass="TextBoxStyle" Font-Bold="true" Style="height: 25px; text-align: right;"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="text-align: right; font-weight: bolder;" class="lblCaption1">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    Redeem Point  :
                <asp:TextBox ID="txtRedeemPoint" runat="server" CssClass="TextBoxStyle" Font-Bold="true" Style="height: 25px; text-align: right;"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; font-weight: bolder;" class="lblCaption1">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    Balance Point  :
                <asp:TextBox ID="txtBalancePoint" runat="server" CssClass="TextBoxStyle" Font-Bold="true" Style="height: 25px; text-align: right;"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="Button1" runat="server" CssClass="button red small"
                                                        Text=" Save " />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </td>
        </table>
    </div>
</asp:Content>
