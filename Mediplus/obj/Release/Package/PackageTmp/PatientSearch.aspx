﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PatientSearch.aspx.cs" Inherits="Mediplus.PatientSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div style="padding-top: 0px; width: 1070px; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:updatepanel id="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvPatient" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False"  OnSorting="gvGridView_Sorting"
                            EnableModelValidation="True" Width="1050px" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200"  gridlines="None">
                             <HeaderStyle CssClass="GridHeader_Gray" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="File No" SortExpression="HPM_PT_ID">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEmailId" CssClass="label" runat="server" Text='<%# Bind("HPM_EMAIL") %>'  visible="false"></asp:Label>
                                          <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click"  >
                                                <asp:Label ID="lblPatientId" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>
                                          </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HPM_PT_FNAME">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFullName" runat="server" OnClick="Select_Click">
                                              <asp:Label ID="lblPatientName" CssClass="label" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB" SortExpression="HPM_DOBDesc">
                                    <ItemTemplate>
                                       <asp:LinkButton ID="lnkDOB" runat="server" OnClick="Select_Click" >
                                            <asp:Label ID="lbldbo" CssClass="label" runat="server" Text='<%# Bind("HPM_DOBDesc") %>'></asp:Label>
                                       </asp:LinkButton>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone No" SortExpression="HPM_PHONE1">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkPhone" runat="server" OnClick="Select_Click"   >
                                              <asp:Label ID="lblPhone" CssClass="label" runat="server" Text='<%# Bind("HPM_PHONE1") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile No" SortExpression="HPM_MOBILE">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkMobile" runat="server" OnClick="Select_Click"  >
                                              <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderText="PT Type" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="inlPTType" runat="server" OnClick="Select_Click"  >
                                                <asp:Label ID="lblPTType" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkCompany" runat="server" OnClick="Select_Click" >
                                              <asp:Label ID="lblCompName" CssClass="label" runat="server" Text='<%# Bind("HPM_INS_COMP_NAME") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Policy No." SortExpression="HPM_POLICY_NO">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkgvPolicyNo" runat="server" OnClick="Select_Click"  >
                                              <asp:Label ID="lblgvPolicyNo" CssClass="label" runat="server" Text='<%# Bind("HPM_POLICY_NO") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Created User" SortExpression="HPM_CREATED_USER">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click"  >
                                              <asp:Label ID="lblCrUser" CssClass="label" runat="server" Text='<%# Bind("HPM_CREATED_USER") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:updatepanel>

            </div>

            <div>
                <asp:updatepanel id="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="70%">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                                        Text="Selected Records :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                                <td class="style2">
                                    
                                </td>

                                <td class="style2">
                                    
                                </td>

                                <td class="style2">
                                    
                                </td>

                            </tr>

                        </table>

                    </ContentTemplate>
                </asp:updatepanel>

            </div>
</asp:Content>
