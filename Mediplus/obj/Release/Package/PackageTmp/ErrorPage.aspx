﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Mediplus.ErrorPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />


    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <table cellpadding="0" cellspacing="0" border="0" 
          style="height:600px">
            <tr>
                <td valign="middle" align="center">
                    <table width="1000" class="TabTableStyle"  style="height:300px;">
                    <tr>
                        <td style="height:100px;">
                            
                        </td>
                    </tr>
                        <tr>
                            <td  style="height: 10px;text-align:center;" >
                                <asp:Label ID="lblError" runat="server" ForeColor="red" Font-Size="15"  CssClass="LabelStyleBold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="bottom">
                               
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
    </asp:Content>