﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditLogDisplay.aspx.cs" Inherits="Mediplus.AuditLogDisplay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Aidit Log</title>
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="Styles/style.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table>
                <tr>
                    <td class="PageHeader">Audit Log
                    </td>
                </tr>
            </table>

            <table>
                <tr>

                    <td class="TDStyle">Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFromDate" runat="server" Width="95%" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" CssClass="labelTemp" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">To Date

                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtToDate" runat="server" Width="95%" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" CssClass="labelTemp" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">Screen 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpScreen" runat="server" CssClass="lblCaption1">
                                    <asp:ListItem Text="All" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Patient Appointment" Value="HMS_APPOINTMENT"></asp:ListItem>
                                     

                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">File No 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFileNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" CssClass="lblCaption1"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnRefresh" runat="server" CssClass="button orange small" Width="80px" Text="Refresh" OnClick="btnRefresh_Click" />
                                <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="80px" Text="Clear" OnClick="btnClear_Click" />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 98%; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvAuditLog" runat="server"
                            AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvPTList_Sorting"
                            EnableModelValidation="True" Width="100%" GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="Date" SortExpression="HAL_CREATED_DATE" ItemStyle-Width="120px" HeaderStyle-Width="120px">
                                    <ItemTemplate>

                                        <asp:Label ID="lblCreatedDate" CssClass="label" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File No" SortExpression="HAL_EMR_ID" ItemStyle-Width="70px" HeaderStyle-Width="70px">
                                    <ItemTemplate>

                                        <asp:Label ID="lblemr_id" CssClass="label" runat="server" Text='<%# Bind("HAL_PT_ID") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Screen" SortExpression="HAL_SCREEN_NAME">
                                    <ItemTemplate>

                                        <asp:Label ID="lblScreenName" CssClass="label" runat="server" Text='<%# Bind("HAL_SCREEN_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" SortExpression="HAL_ACTION">
                                    <ItemTemplate>

                                        <asp:Label ID="lblAction" CssClass="label" runat="server" Text='<%# Bind("HAL_ACTION") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remarks" SortExpression="HAL_REMARKS">
                                    <ItemTemplate>

                                        <asp:Label ID="lblRemarks" CssClass="label" runat="server" Text='<%# Bind("HAL_REMARKS") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User" SortExpression="CreatedUserName">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("CreatedUserName") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </form>
</body>
</html>
