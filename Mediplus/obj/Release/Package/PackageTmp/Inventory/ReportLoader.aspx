﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="ReportLoader.aspx.cs" Inherits="Mediplus.Inventory.ReportLoader" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <title></title>
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>


    <script src="../../Scripts/Validation.js" type="text/javascript"></script>

    
    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script src="ReportScript.js"></script>


    <script type="text/javascript">

        function ShowProductDtls() {
            //   var Report = "";
            //   Report = "HmsCompReceipts.rpt";
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";
              var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
              var arrFromDate = dtFrom.split('/');
              var Date1;
              if (arrFromDate.length > 1) {

                  Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
              }


              var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
                var arrToDate = dtTo.split('/');
                var Date2;
                if (arrToDate.length > 1) {

                    Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
                }
              
        var Type = document.getElementById('<%=drpType.ClientID%>').value
        var PropCategory = document.getElementById('<%=drpPropCategory.ClientID%>').value
            var  Department = document.getElementById('<%=drpDepartment.ClientID%>').value


              var Criteria = " 1=1 ";
              if (Type != "") {

                  Criteria += ' AND {AC_PRODUCT_MASTER.APM_TYPE}=\'' + Type + '\'';
              }

              if (PropCategory != "") {

                  Criteria += ' AND {AC_PRODUCT_MASTER.APM_CATEGORY}=\'' + PropCategory + '\'';
              }



              if (Department != "") {

                  Criteria += ' AND {AC_PRODUCT_MASTER.APM_DEPARTMENT}=\'' + Department + '\'';
              }


              if (dtFrom != "" && dtTo != "") {
                  Criteria += ' AND  {AC_PRODUCT_MASTER.APM_PURCHASE_DATE}>=date(\'' + Date1 + '\') AND  {AC_PRODUCT_MASTER.APM_PURCHASE_DATE}<=date(\'' + Date2 + '\')'
              }

              var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



          }
     function ShowReportFromDrp(ShowReport) {
            var Report = document.getElementById('<%=drpReport.ClientID%>').value  + ".rpt";

                   
            var PageName;
            PageName = '<%=Convert.ToString(Request.QueryString["PageName"]) %>'

            var Criteria = " 1=1 ";
            if (PageName == "HMS_RPT_INVENTORY") {
                ShowProductDtls()
             }
          
         
          
     }

          </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidPageName" runat="server" value="Report" />
    <input type="hidden" id="hidPerCheck" runat="server" value="true" />

     <table>
        <tr>
            <td class="PageHeader">
                <asp:Label ID="lblReportHeader" runat="server" CssClass="PageHeader" Text="Report"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>

    <table width="900px" cellpadding="5" cellspacing="5">
        <tr>
            <td style="width: 150px;">
                <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                    Text="From"></asp:Label><span style="color: red;">* </span>
            </td>
            <td >
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                &nbsp;
                    <asp:Label ID="Label2" runat="server" CssClass="label"
                        Text="To"></asp:Label>

                <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10"  CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
            </td>
             <td class="lblCaption1">Type   
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpType" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" >
                                         <asp:ListItem Text="--- All ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Inventory" Value="Inventory"></asp:ListItem>
                                        <asp:ListItem Text="Fixed Asset" Value="Fixed Asset"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Category   
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPropCategory" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px">
                                     
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
              <td class="lblCaption1">Department   
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                   <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle" Width="70%" Height="25px"> </asp:DropDownList>
                                  
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
        </tr>

        </table> 

    <table cellpadding="5" cellspacing="5" style="width: 80%;">
        <tr>
            <td style="width: 150px; "></td>
            <td>
                <asp:DropDownList ID="drpReport" runat="server" CssClass="TextBoxStyle">
                </asp:DropDownList>
                <input type="button" style="width: 150px;" class="button red small" value="Show" onclick="ShowReportFromDrp('true');" />

            </td>
        </tr>
        
      <tr>

            <td>
                <input type="button" id="btnShowReport" runat="server" style="display: none;width: 150px;" class="button red small" value="Show Report" onclick="ShowReports()" />

                <input type="button" id="btnShowClaimTransferDtls" visible="false" runat="server" class="button red small" style="display: none;width: 150px;" value="Transfer Details" onclick="ClaimTransferDtls()" />
                <input type="button" id="btnShowClaimTransferSummary" visible="false" runat="server" class="button red small" style="display: none;width: 150px;" value="Transfer Summary" onclick="ClaimTransferSummary()" />


            </td>

        </tr>
    </table>
</asp:Content>
