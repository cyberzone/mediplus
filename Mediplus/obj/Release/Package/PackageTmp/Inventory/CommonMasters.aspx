﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CommonMasters.aspx.cs" Inherits="Mediplus.Inventory.CommonMasters" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

       <script language="javascript" type="text/javascript">


           function OnlyNumeric(evt) {
               var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
               if (chCode >= 48 && chCode <= 57 ||
                    chCode == 46) {
                   return true;
               }
               else

                   return false;
           }

       </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div>
        <table style="width: 70%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Common Master"></asp:Label>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                               <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

          <div style="padding-top: 0px; width: 70%; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
              <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
            <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Code
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCode" runat="server" CssClass="TextBoxStyle" Width="50%" Height="20px" MaxLength="50"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                 <tr>
                    <td class="lblCaption1" style="width: 100px;">Name
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxStyle" Width="50%" Height="20px"  MaxLength="100"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 100px;"> 
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="chkStatus" runat="server"  Text="Active" CssClass="TextBoxStyle" Checked="true" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                 <tr>
                    <td class="lblCaption1" style="width: 100px;">Type 
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                  <asp:DropDownList ID="drpType" runat="server" CssClass="TextBoxStyle" Width="50%" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="drpType_SelectedIndexChanged"  >
                                        <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Product Category" Value="PROD_CATEGORY"></asp:ListItem>
                                       <asp:ListItem Text="Product Location" Value="PROD_LOCATION"></asp:ListItem>
                                        <asp:ListItem Text="Unit & Measurment" Value="UNITS_MEASUREMENT"></asp:ListItem>
                                      <asp:ListItem Text="Depreciation Methods" Value="DEPRECIATION_METHODS"></asp:ListItem>
                                      <asp:ListItem Text="Product Store" Value="PROD_STORE"></asp:ListItem>
                                    </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Order
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtOrder" runat="server" CssClass="TextBoxStyle" Width="50%" Height="20px"  MaxLength="3" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                </table>
           </div>
          <br />
             <div style="padding-top: 0px; width: 70%; height: 300px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>

                        <asp:GridView ID="gvCommonMaster" runat="server" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" PageSize="200">
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkCode" runat="server" OnClick="Select_Click">
                                         
                                            <asp:Label ID="lblCode" CssClass="GridRow" runat="server" Text='<%# Bind("AM_CODE") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkMame" runat="server" OnClick="Select_Click">
                                            <asp:Label  ID="lblName" CssClass="GridRow" runat="server" Text='<%# Bind("AM_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkType" runat="server" OnClick="Select_Click">
                                            <asp:Label  ID="lblType" CssClass="GridRow" runat="server" Text='<%# Bind("AM_TYPE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Select_Click">
                                            <asp:Label  ID="lblStatus" CssClass="GridRow" runat="server" Text='<%# Bind("AM_ACTIVE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkOrder" runat="server" OnClick="Select_Click">
                                            <asp:Label  ID="lblOrder" CssClass="GridRow" runat="server" Text='<%# Bind("AM_ORDER") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Actions" Visible="true" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="../Images/icon_delete.png" AlternateText="Delete"
                                            OnClick="Delete_Click" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                        </asp:GridView>
                    </ContentTemplate>
                   
                </asp:UpdatePanel>
            </div>
          </div>
    <br />
    <br />


</asp:Content>
