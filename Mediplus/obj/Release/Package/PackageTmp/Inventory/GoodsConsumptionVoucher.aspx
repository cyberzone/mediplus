﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="GoodsConsumptionVoucher.aspx.cs" Inherits="Mediplus.Inventory.GoodsConsumptionVoucher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

       
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowTypeDive() {

            alter('ter');
        }

    </script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage, vColor) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("divMessageClose").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
            document.getElementById("<%=lblMessage.ClientID%>").style.color = vColor;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("divMessageClose").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
    }




    function ShowMasterPopup() {

        document.getElementById("divGCVPopup").style.display = 'block';


    }

    function HideMasterPopup() {

       
        document.getElementById("<%=divGCVPopup.ClientID%>").style.display = 'none';
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div>

            <div id="divMessageClose" style="display: none; position: absolute; top: 278px; left: 712px;">
                <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" />
            </div>
            <div style="padding-left: 60%; width: 80%;">
                <div id="divMessage" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 200px;">

                    <table cellpadding="0" cellspacing="0" width="100%">

                        <tr>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" CssClass="label" Style="font-weight: bold;"></asp:Label>
                            </td>
                        </tr>
                    </table>


                    <br />

                </div>
            </div>
            <table style="width: 80%">
                <tr>
                    <td align="left">
                       <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Goods Consumption "></asp:Label>
                    </td>
                    <td align="right">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete"   OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                </tr>
            </table>

            <div style="padding-top: 0px; width: 80%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">


                <div style="width: 100%; text-align: right; float: right;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>

                            <asp:ImageButton ID="btnCtlrRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="btnCtlrRefresh_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <table cellpadding="5" cellspacing="5" style="width: 80%">
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Trans. No.
                               <asp:ImageButton ID="imgGCVZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display GRN Entry" OnClick="imgGCVZoom_Click" />

                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtTransNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" AutoPostBack="true" OnTextChanged="txtTransNo_TextChanged"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Date
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtTransDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                        Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1" style="width: 100px;">Ref. No.
 
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtReferanceNo" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Status
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpStatus" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                        <asp:ListItem Value="I">InActive</asp:ListItem>

                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Department
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpDepartment" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Remarks
                        </td>
                        <td colspan="7">
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                     <asp:TextBox ID="txtRemarks" runat="server" CssClass="TextBoxStyle" TextMode="MultiLine" Width="100%" Height="50px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>

                <div style="padding: 10px;">

                    <table cellpadding="3" cellspacing="3" style="width: 100%">
                        <tr>
                            <td class="lblCaption1">Item Dtls
                            </td>
                            <td class="lblCaption1">U/M
                            </td>
                            <td class="lblCaption1">QTY
                            </td>
                             <td class="lblCaption1">Description
                            </td>
                        </tr>
                        <tr>

                            <td style="vertical-align:top;" >
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProduct" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                        <div id="div1" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtProduct" MinimumPrefixLength="1" ServiceMethod="GetProductList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                                        </asp:AutoCompleteExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="vertical-align:top;" >
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpUnitType" runat="server" CssClass="DropDown3DStyle" Width="100%" Height="25px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                            <td style="vertical-align:top;" >
                                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtQty" runat="server" CssClass="TextBoxStyle" Width="100%" Height="25px" Text="" onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                             <td style="vertical-align:top;" >
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtDesc" runat="server" CssClass="TextBoxStyle" TextMode="MultiLine" Width="100%" Height="50px" Text=""  ></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="vertical-align:top;" >
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:ImageButton ID="btnAddItem" runat="server" ImageUrl="~/Images/New.png" Style="height: 25px; width: 25px;" ToolTip="Create New Entry" OnClick="btnAddItem_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                    <div style="padding-top: 0px; width: 100%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvGCVTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%">
                                    <HeaderStyle CssClass="GridHeader_Gray" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvItemCode" CssClass="lblCaption1" runat="server" OnClick="GCVTransEdit_Click">
                                                    <asp:Label ID="lblgvTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCT_TRANS_NO") %>' Visible="false"></asp:Label>

                                                    <asp:Label ID="lblgvItemCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCT_ITEM_CODE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvItemName" CssClass="lblCaption1" runat="server" OnClick="GCVTransEdit_Click">
                                                    <asp:Label ID="lblgvItemName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCT_ITEM_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="U/M" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvUnitType" CssClass="lblCaption1" runat="server" OnClick="GCVTransEdit_Click">
                                                    <asp:Label ID="lblgvUnitType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCT_UNIT_TYPE") %>'></asp:Label>
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvQty" CssClass="lblCaption1" runat="server" OnClick="GCVTransEdit_Click">
                                                    <asp:Label ID="lblgvQty" CssClass="label" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("AGCT_QTY") %>'></asp:Label>
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Desc" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvDesc" CssClass="lblCaption1" runat="server" OnClick="GCVTransEdit_Click">
                                                    <asp:Label ID="lblgvDesc" CssClass="label" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("AGCT_DESC") %>'></asp:Label>
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="15px" Width="15px"
                                                    OnClick="Delete_Click" />&nbsp;&nbsp;
                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                 <div id="divGCVPopup"  runat="server"  visible="false" style="overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 150px;">
                  <div style="width: 100%; text-align: right; float: right;position:absolute; top:0px;right:0px;">
                            
                            <img src="../Images/Close.png"  style="height: 25px; width: 25px;"   onclick="HideMasterPopup()" />
                             
                        </div>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                             <asp:gridview id="gvGCVMaster" runat="server" allowsorting="True" autogeneratecolumns="False"
                                enablemodelvalidation="True" width="100%">
                                                                 <HeaderStyle CssClass="GridHeader_Gray" />
                                                                    <RowStyle CssClass="GridRow" />
                                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                                <Columns>
                                                                    
                                                                    <asp:TemplateField HeaderText="Trans. No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="LinkButton11" CssClass="lblCaption1"  runat="server"  OnClick="GCVEdit_Click"   >
                                                                            <asp:Label ID="lblgvTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCM_TRANS_NO") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkgvProdName" CssClass="lblCaption1"  runat="server"  OnClick="GCVEdit_Click"   >
                                                                            <asp:Label ID="lblgvDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCM_TRANS_DATEDesc") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     
                                                                     <asp:TemplateField HeaderText="Department" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkgvCategory" CssClass="lblCaption1"  runat="server"  OnClick="GCVEdit_Click"   >
                                                                            <asp:Label ID="lblgvSupplier" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("AGCM_DEPARTMENT") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                   
                                                                </Columns>
                                                            </asp:gridview>

                        </td>
                    </tr>
                </table>
            </div>
            </div>
           
    

   </asp:Content>
