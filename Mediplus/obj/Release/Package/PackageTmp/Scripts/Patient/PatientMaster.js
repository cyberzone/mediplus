﻿
/**
 * PaintWeb intitialization code
 */
 
    (function () {
        // Function called when the user clicks the "Edit image" button.
        function pwStart () {
            document.body.insertBefore(loadp, btn.parentNode);

            timeStart = (new Date()).getTime();
            pw.init(pwInit);
        };

        // Function called when the PaintWeb application fires the "appInit" event.
        function pwInit (ev) {
            var initTime = (new Date()).getTime() - timeStart,
                str = 'Demo: Yay, PaintWeb loaded in ' + initTime + ' ms! ' +
                      pw.toString();

            document.body.removeChild(loadp);

            if (ev.state === PaintWeb.INIT_ERROR) {
                alert('Demo: PaintWeb initialization failed.');
                return;

            } else if (ev.state === PaintWeb.INIT_DONE) {
                if (window.console && console.log) {
                    console.log(str);
                } else if (window.opera) {
                    opera.postError(str);
                }

            } else {
                alert('Demo: Unrecognized PaintWeb initialization state ' + ev.state);

                return;
            }

            img.style.display = 'none';
            btn.style.display = 'none';
        };

        var img    = document.getElementById('editableImage'),
            btn    = document.getElementById('buttonEditImage'),
            target = document.getElementById('PaintWebTarget'),
            loadp  = document.createElement('p'),
            timeStart = null,

            // Create a PaintWeb instance.
            pw = new PaintWeb();

        pw.config.guiPlaceholder = target;
        pw.config.imageLoad      = img;
        pw.config.configFile     = 'config-example.js';
        loadp.appendChild(document.createTextNode('Loading, please wait...'));

        if (btn.addEventListener) {
            btn.addEventListener('click', pwStart, false);
        } else if (btn.attachEvent) {
            btn.attachEvent('onclick', pwStart);
        } else {
            btn.onclick = pwStart;
        }

    })();
 
    function SaveDepartmentImage() {
        if ($("#ImageEditorPDC").contents().find("#PaintWebTarget")[0]) {
            var imageData = $("#ImageEditorPDC").contents().find("#PaintWebTarget")[0].toDataURL("image/jpeg");
            alert(imageData);

        }
    }
    jQuery(document).ready(function ($) {
       
        $("#TherapySaveBtn").click(function () {
            alert('test');
        });

       
    });