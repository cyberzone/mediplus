﻿<%@ page language="C#" autoeventwireup="true" codebehind="BenefitsExeclusions.aspx.cs" inherits="Mediplus.HMS.Masters.BenefitsExeclusions" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />
    <title></title>
     <script language="javascript" type="text/javascript">
        

         function passvalue(PlanType) {
            
             window.opener.BindSelectedPlanType(PlanType);
             window.close();

         }

         function DoUpper(Cont) {
             var strValue;
             strValue = Cont.value;
             Cont.value = strValue.toUpperCase();
         }


         function SaveBenefitsVal() {
             var label;
             label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


             if (document.getElementById('<%=txtCategory.ClientID%>').value == "" && document.getElementById('<%=drpCatType.ClientID%>').value == "0") {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the  Category";
                 
                    return false;
                }

             if (document.getElementById('<%=drpCompany.ClientID%>').value == "0") {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Company";
                  document.getElementById('<%=drpCompany.ClientID%>').focus;
                  return false;
              }




            return true
        }

        function DeleteBenefitsVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            if (document.getElementById('<%=drpCatType.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                document.getElementById('<%=drpCatType.ClientID%>').focus;
                return false;
            }

            var isDelBenefit = window.confirm('Do you want to delete Category Information?');

            if (isDelBenefit == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="hidPageName" runat="server" />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <div>

            <table>
                <tr>
                    <td class="PageHeader"> Benefits Execlusions
               
                    </td>
                </tr>
            </table>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>

                    <td>
                        <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="lblCaption1">Category
                    </td>
                    <td>
                        <asp:TextBox ID="txtCategory" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="190px" MaxLength="50" AutoPostBack="true" OnTextChanged="txtCategory_TextChanged"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCategory" MinimumPrefixLength="1" ServiceMethod="GetCategory">
                        </asp:AutoCompleteExtender>
                    </td>
                    <td  class="lblCaption1">
                        Company
                     
                          <asp:DropDownList ID="drpCompany" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#CCCCCC" class="label" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="drpCompany_SelectedIndexChanged"></asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 30px;">Added Category
                    </td>
                    <td>
                        <asp:DropDownList ID="drpCatType" runat="server" CssClass="label" Width="190px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpCatType_SelectedIndexChanged" BorderColor="#CCCCCC">
                        </asp:DropDownList>
                    </td>
                    <td class="lblCaption1">Treatment Type
                     
                      
                            <asp:DropDownList ID="drpTreatmentType" runat="server" CssClass="label" Width="157px" BorderWidth="1px" BorderColor="#CCCCCC">
                                <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                <asp:ListItem Value="Credit">Credit</asp:ListItem>
                            </asp:DropDownList>

                    </td>
                    <td class="lblCaption1">Benefits Type
                        
                            <asp:DropDownList ID="drpBenefitsType" runat="server" CssClass="label" Width="157px" BorderWidth="1px" BorderColor="#CCCCCC" AutoPostBack="true" OnSelectedIndexChanged="drpBenefitsType_SelectedIndexChanged">
                                <asp:ListItem Value="I">Insurance Type</asp:ListItem>
                                <asp:ListItem Value="S">Service Type</asp:ListItem>
                            </asp:DropDownList>
                    </td>


                </tr>

                <tr>
                    <td class="lblCaption1" style="height: 30px;">Hospital Discount
                    </td>
                    <td class="lblCaption1">
                        <asp:DropDownList ID="drpHospDiscType" CssClass="label" runat="server" Width="75px">
                            <asp:ListItem Text="$" Value="$"></asp:ListItem>
                            <asp:ListItem Text="%" Value="%"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtHospDiscAmt" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        max.Claim
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="ChkConsultation" runat="server" Enabled="false" CssClass="lblCaption1" Text="Consultation Included in Hospital Discount" Checked="false" />
                        <asp:CheckBox ID="ChkDedMaxConsAmt" runat="server" Enabled="false" CssClass="lblCaption1" Text="Deductible to the max of Consultation Amt" Checked="false" />

                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1" style="height: 30px;">Deductible
                    </td>
                    <td>
                        <asp:DropDownList ID="drpDedType" runat="server" CssClass="label" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                            <asp:ListItem Text="$" Value="$"></asp:ListItem>
                            <asp:ListItem Text="%" Value="%"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtDeductible" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtDeductible_TextChanged"></asp:TextBox>
                        <asp:TextBox ID="TxtMaxDed" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="ChkDedCoIns" runat="server" Enabled="false" CssClass="lblCaption1" Text="Deductible & Co Insurance applicable for Consultation" Checked="false" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 30px;">Co Insurance
                    </td>
                    <td>
                        <asp:DropDownList ID="drpCoInsType" runat="server" CssClass="label" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                            <asp:ListItem Text="$" Value="$"></asp:ListItem>
                            <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtCoIns" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtCoIns_TextChanged"></asp:TextBox>
                        <asp:TextBox ID="TxtMaxCoIns" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="ChkCoInsFromGrossAmt" runat="server" Enabled="false" CssClass="lblCaption1" Text="Co-Insurance From Gross Amount" Checked="false" />
                        <asp:CheckBox ID="ChkSplCodeNeeded" runat="server" Enabled="false" CssClass="lblCaption1" Text="Special Code Needed" Checked="false" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 30px;">Pharmacy Disc.
                    </td>
                    <td>
                        <asp:DropDownList ID="drpPharType" runat="server" CssClass="label" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                            <asp:ListItem Text="$" Value="$"></asp:ListItem>
                            <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtPharDisc" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="ChkCoInsFromGrossandClaimfromNetAmt" runat="server" Enabled="false" CssClass="lblCaption1" Text="Co-Insurance From Gross Amt and  Claim Amt from  Net Amt" Checked="false" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 30px;">Pharmacy Co.Ins
                    </td>
                    <td>
                        <asp:DropDownList ID="drpPharCoInsType" runat="server" CssClass="label" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                            <asp:ListItem Text="$" Value="$"></asp:ListItem>
                            <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtPharCoInsDisc" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:TextBox ID="TxtMaxPhy" runat="server" Style="text-align: right;" Width="50px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="ChkDedBenefitsType" runat="server" Enabled="false" CssClass="lblCaption1" Text="Calculate Deductible as Benefits Type" Checked="false" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 30px;">Price Factor
                    </td>
                    <td >
                        <asp:TextBox ID="txtPriceFactor" CssClass="label" runat="server" Width="130px" Style="text-align: right;" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                    </td>
                    <td class="lblCaption1">Perday Limit of the Patient
                     
                        <asp:TextBox ID="txtPerDayPTLimit" CssClass="label" runat="server" Width="70px" Style="text-align: right;" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:Button ID="btnUpdate" runat="server" CssClass="button orange small" Width="70px" OnClick="btnUpdate_Click" Text="Update" ToolTip="Update Perday Limit of the Patient" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">Remarks
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtRemarks" CssClass="label" runat="server" Width="480px" MaxLength="50" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>

                    </td>
                </tr>

            </table>



            <asp:Button ID="btnBenefitSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnBenefitSave_Click" OnClientClick="return SaveBenefitsVal();" Text="Save" />
            <asp:Button ID="btnBenefitDelete" runat="server" Visible="false" CssClass="button orange small" Width="100px" OnClick="btnBenefitDelete_Click" OnClientClick="return DeleteBenefitsVal();" Text="Delete" />
            <asp:Button ID="btnBenefitClear" runat="server" CssClass="button orange small" Width="100px" OnClick="btnBenefitClear_Click" Text="Clear" />
             <asp:Button ID="btnSelect" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSelect_Click" Text="Select" />
            <br />

            <div style="padding-top: 0px; padding-left: 0px; width: 900px; height: 350px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:GridView ID="gvHCB" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowDataBound="gvHCB_RowDataBound"
                    EnableModelValidation="True" Width="99%">
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <Columns>


                        <asp:TemplateField HeaderText="Category" HeaderStyle-Width="250px">
                            <ItemTemplate>
                                <asp:Label ID="lblCatId" runat="server" Text='<%# Bind("HCBD_CATEGORY_ID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCBD_CATEGORY") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Category Type" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblCatType" runat="server" Text='<%# Bind("HCBD_CATEGORY_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpCategoryType" runat="server" CssClass="label" Style="border: none;" Width="100%" BorderWidth="1px">
                                    <asp:ListItem Text="Covered" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Un Covered" Value="U"></asp:ListItem>
                                    <asp:ListItem Text="PreApproval" Value="P"></asp:ListItem>

                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Limit" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtLimit" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HCBD_LIMIT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblCoInsType" CssClass="GridRow" runat="server" Text='<%# Bind("HCBD_CO_INS_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpGVCoInsType" CssClass="LabelStyle" runat="server" Width="100%" Height="30px">
                                    <asp:ListItem Value="$">$</asp:ListItem>
                                    <asp:ListItem Value="%">%</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCoInsAmt" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HCBD_CO_INS_AMT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Ded Type" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblDedType" CssClass="GridRow" runat="server" Text='<%# Bind("HCBD_DED_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpGVDedType" CssClass="LabelStyle" runat="server" Width="100%" Height="30px">
                                    <asp:ListItem Value="$">$</asp:ListItem>
                                    <asp:ListItem Value="%">%</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deductible" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDedAmt" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HCBD_DED_AMT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>


            </div>
        </div>
    </form>
</body>
</html>
