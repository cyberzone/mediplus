﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="HAADServiceMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.HAADServiceMaster" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function ShowErrorMessage(vMessage1, vMessage2, vColor) {

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
           document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

           if (vColor != '') {
               document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;
            }

            document.getElementById("divMessage").style.display = 'block';
        }


        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ValService() {

            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br></br>";

            if (document.getElementById('<%=txtServCode.ClientID%>').value == "") {
                ErrorMessage += 'Haad Code is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtServName.ClientID%>').value == "") {
                ErrorMessage += 'Haad Descripton is Empty.' + trNewLineChar;
                IsError = true;
            }


            if (document.getElementById('<%=drpCategory.ClientID%>').value == "") {
                ErrorMessage += 'Haad Category is Empty.' + trNewLineChar;
                IsError = true;
            }



            if (IsError == true) {
                ShowErrorMessage('Please check below Errors', ErrorMessage, 'Brown');

                return false;
            }



            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: auto; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>

                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="width: 85%">
        <table>
            <tr>
                <td class="PageHeader">HAAD Service Master
                </td>
            </tr>
        </table>
        <input type="hidden" id="hidPermission" runat="server" value="9" />


        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
            <tr>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div class="lblCaption1" style="width: 95%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
            <table width="100%" cellpadding="5" cellspacing="5">
                <tr>
                    <td style="width: 100px; height: 30px" class="lblCaption1">Code 
                    <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtServCode" CssClass="TextBoxStyle" runat="server" Width="60%" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged"></asp:TextBox>
                                &nbsp;
                             <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="Active" Checked="true" />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="width: 100px; height: 30px" class="lblCaption1">Description 
                    <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtServName" CssClass="TextBoxStyle" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="width: 100px; height: 30px" class="lblCaption1">Short Name 
                    
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtShortName" CssClass="TextBoxStyle" runat="server" Width="90%" MaxLength="100"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px" class="lblCaption1">Category 
                    <span style="color: red; font-size: 11px;">*</span>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpCategory" CssClass="TextBoxStyle" runat="server" Width="90%">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Serv.Fee 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtServFee" CssClass="TextBoxStyle" runat="server" Width="90%" MaxLength="10" Style="text-align: right;" onblur="return BindCompFee()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="5">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click"   Text="Save"  OnClientClick="return ValService();" />
                                <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="100px" OnClick="btnClear_Click" Text="Clear" />


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>

        </div>
        <br />
        <div class="lblCaption1" style="width: 95%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
            <span class="lblCaption1" style="font-weight:bold;"> Search</span>
            <table width="100%" cellpadding="5" cellspacing="5">
                <tr>
                    <td style="width: 150px; height: 30px" class="lblCaption1">Code & Description  
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcName" CssClass="TextBoxStyle" runat="server" Width="60%"></asp:TextBox>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="height: 30px" class="lblCaption1">Category 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpSrcCategory" CssClass="TextBoxStyle" runat="server" Width="90%">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSearch" runat="server" CssClass="button red small" Width="100px" OnClick="btnSearch_Click" Text="Search" />


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>

        </div>
        <div style="padding-top: 0px; width: 97%; height: 430px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvHaadService" runat="server"
                        AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%" BorderStyle="None" GridLines="Horizontal" CellPadding="2" CellSpacing="2">
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                        OnClick="Edit_Click" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CATEGORY">
                                <ItemTemplate>
                                    <asp:Label ID="lblCategory" CssClass="label" runat="server" Text='<%# Bind("HHS_TYPE") %>'></asp:Label>
                                    <asp:Label ID="lblCodeType" CssClass="label" runat="server" Text='<%# Bind("HHS_TYPE_VALUE") %>' Visible="false"></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblServCode" CssClass="label" runat="server" Text='<%# Bind("HHS_CODE") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DESCRIPTION" HeaderStyle-Width="400px">
                                <ItemTemplate>

                                    <asp:Label ID="lblServDesc" CssClass="label" runat="server" Text='<%# Bind("HHS_DESCRIPTION") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SHORT NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lblShortName" CssClass="label" runat="server" Text='<%# Bind("HHS_SHORT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="FEES">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrice" CssClass="label" runat="server" Text='<%# Eval("HHS_PRICE","{0:0.00}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="STATUS">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" CssClass="label" runat="server" Text='<%# Bind("HHS_STATUS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

    </div>
</asp:Content>
