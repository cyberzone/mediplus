﻿<%@ page language="C#" autoeventwireup="true" codebehind="AgreementService.aspx.cs" inherits="Mediplus.HMS.Masters.AgreementService" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />

    <title></title>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
        
        function ShowAgreementAlert() {

            if (document.getElementById('<%=hidAgreementCategory.ClientID%>').value == 'true') {
                var isAgreementAlert = window.confirm('You cannot make two types of agreement for the same company.In order to create Service wise agreement, you have to delete the previous agreement.Do you want to delete the Category wise agreement.');
                return isAgreementAlert;
            }
            }
       

        function CompIdSelected() {
            if (document.getElementById('<%=txtCompanyId.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyId.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyId.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Name = Data1[0];
                var Code = Data1[1];


                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                    document.getElementById('<%=txtCompanyId.ClientID%>').value = Code.trim();


                }
            }

            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>

             <asp:HiddenField ID="hidAgreementCategory" runat="server"  value="false" />

            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
                <tr>

                    <td>
                        <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; padding-left: 0px; width: 99%; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table width="100%">
                    <tr>
                        <td class="label" style="width:100px;">Comp.Type 
                        </td>
                        <td>
                            <asp:DropDownList ID="drpCompanyType" runat="server" Enabled="false" CssClass="label" Width="157px" BorderWidth="1px" BorderColor="red">
                                <asp:ListItem Value="I">Insurance</asp:ListItem>
                                <asp:ListItem Value="N">Non Insurance</asp:ListItem>
                                <asp:ListItem Value="P">Personal</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="label">Comp. ID
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCompanyId" runat="server" Enabled="false" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="70px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                                    <asp:TextBox ID="txtCompanyName" runat="server" Enabled="false" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="350px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompanyId" MinimumPrefixLength="1" ServiceMethod="GetCompany"></asp:AutoCompleteExtender>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"></asp:AutoCompleteExtender>
                                </ContentTemplate>

                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
            <table width="99%">
                <tr>
                    <td class="label" style="width:100px;">Description
                    </td>
                    <td>
                        <asp:TextBox ID="txtServName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="150px"></asp:TextBox>
                       
                      
                    </td>
                    <td>
                         <asp:Button ID="btnFind" runat="server" CssClass="button orange small" Width="130px" OnClick="btnFind_Click" Text="Find" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAnySearch" runat="server" CssClass="label" Text="Any Search" Checked="false" />
                    </td>
                    <td class="label">
                        Sort By
                        
                    </td>
                    <td>
                        <asp:DropDownList ID="drpSort" CssClass="LabelStyle" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="drpSort_SelectedIndexChanged" >
                               <asp:ListItem Value="%">Serv.Id</asp:ListItem>
                             <asp:ListItem Value="D">Description</asp:ListItem>
                             <asp:ListItem Value="C">Covering</asp:ListItem>
                         </asp:DropDownList>

                    </td>
                    <td>
                         <asp:Button ID="btnUpdate" runat="server" CssClass="button orange small" Width="50px" OnClick="btnUpdate_Click" Text="U" Tooltip="Policy type convert as Uncover" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 30px">
                        <asp:Label ID="Label1" runat="server" CssClass="label"
                            Text="Category"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpCategory" CssClass="label" runat="server" Width="150px" BorderWidth="1px" BorderColor="Red" AutoPostBack="true" OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                                </asp:DropDownList>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                     <td>
                         <asp:Button ID="btnAddNew" runat="server" CssClass="button orange small" Width="130px" OnClick="btnAddNew_Click" OnClientClick="return ShowAgreementAlert();"  Text="Add New Services" />
                    </td>
                     <td>
                        <asp:CheckBox ID="chkRefServOnly" runat="server" CssClass="label" Text="Referenced Services Only" Checked="false" />
                    </td>
                    <td>
                         <asp:Label ID="Label9" runat="server" CssClass="label" style="color:darkblue;"
                            Text="Selected Records:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true" ForeColor="DarkBlue"></asp:Label>
                    </td>
                    <td></td>
                </tr>
               
            </table>

            <br />
            <div runat="server" id="divTran" style="padding-top: 0px; width: 99%; height: 400px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvServices" runat="server"  AllowPaging="True" AutoGenerateColumns="False" OnRowDataBound="gvServices_RowDataBound"
                            EnableModelValidation="True" Width="99%" PageSize="200" OnPageIndexChanging="gvServices_PageIndexChanging">
                            <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Serv Id">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceId" runat="server" Text='<%# Bind("HAS_SERV_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ref No" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtRefNo" runat="server"   MaxLength="10" Style="border: none;" Width="95%" Height="30px" Text='<%# Bind("HAS_REFNO") %>' ></asp:TextBox>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtServiceName" runat="server" MaxLength="1000" Style="border: none;" Width="95%" Height="30px" Text='<%# Bind("HAS_SERV_NAME") %>' ></asp:TextBox>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Comp.Fee" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCompFee"  runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="95%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HAS_COMP_FEE"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiscType" CssClass="GridRow" runat="server" Text='<%# Bind("HAS_DISC_TYPE") %>' Visible="false"></asp:Label>
                                        <asp:DropDownList ID="drpDisctype" CssClass="LabelStyle" runat="server" Width="100%" Height="30px">
                                            <asp:ListItem Value="%">%</asp:ListItem>
                                             <asp:ListItem Value="$">$</asp:ListItem>
                                           
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Discount" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDiscount" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="95%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HAS_DISC_AMT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"  ></asp:TextBox>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Net Amount" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtNetAmt" runat="server" Readonly="true" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="95%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HAS_NETAMOUNT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Coverage" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCoveringType" runat="server" Text='<%# Bind("HAS_COVERING_TYPE") %>' Visible="false"></asp:Label>
                                        <asp:DropDownList ID="drpCoveringType" runat="server" CssClass="label" Style="border: none;" Width="100%" BorderWidth="1px">
                                            <asp:ListItem Text="Covered" Value="C" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Un Covered" Value="U"></asp:ListItem>
                                            <asp:ListItem Text="PreApproval" Value="P"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="50px" OnClick="gvSave_Click"    Text="Save" />

                                    </ItemTemplate>
                                    </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
