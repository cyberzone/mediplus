﻿<%@ page language="C#" autoeventwireup="true" codebehind="AgreementCategory.aspx.cs" inherits="Mediplus.HMS.Masters.AgreementCategory" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />

    <title></title>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var strCompCode = document.getElementById('<%=txtCompanyId.ClientID%>').value
              if (/\S+/.test(strCompCode) == false) {
                  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company Code";
                document.getElementById('<%=txtCompanyId.ClientID%>').focus;
                return false;
            }

          


            var strCompName = document.getElementById('<%=txtCompanyName.ClientID%>').value
              if (/\S+/.test(strCompName) == false) {
                  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company Name";
                document.getElementById('<%=txtCompanyName.ClientID%>').focus;
                return false;
            }




            return true
        }


        function CompIdSelected() {
            if (document.getElementById('<%=txtCompanyId.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyId.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyId.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Name = Data1[0];
                var Code = Data1[1];


                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                    document.getElementById('<%=txtCompanyId.ClientID%>').value = Code.trim();


                }
            }

            return true;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
                <tr>

                    <td>
                        <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; padding-left: 0px; width: 99%; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table>
                    <tr>
                        <td class="label">Comp.Type 
                        </td>
                        <td>
                            <asp:DropDownList ID="drpCompanyType" runat="server" Enabled="false" CssClass="label" Width="157px" BorderWidth="1px" BorderColor="red">
                                <asp:ListItem Value="I">Insurance</asp:ListItem>
                                <asp:ListItem Value="N">Non Insurance</asp:ListItem>
                                <asp:ListItem Value="P">Personal</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="label">Comp. ID
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCompanyId" runat="server" Enabled="false" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="70px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                                    <asp:TextBox ID="txtCompanyName" runat="server" Enabled="false" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="350px" MaxLength="50" onblur="return CompNameSelected()"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompanyId" MinimumPrefixLength="1" ServiceMethod="GetCompany"></asp:AutoCompleteExtender>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"></asp:AutoCompleteExtender>
                                </ContentTemplate>

                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
            <br />


            <div style="padding-top: 0px; width: 99%; height: 440PX; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
                <table align="center">
                    <tr>
                        <td style="font-family: 'Arial', Times, serif; font-size: 15px; font-weight: bold; text-align: center;">Service Category wise Agreement
               
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gvAgreementCat" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowDataBound="gvAgreementCat_RowDataBound"
                    EnableModelValidation="True" Width="99%">
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Category Name" HeaderStyle-Width="250px">
                            <ItemTemplate>
                                <asp:Label ID="lblCatId" runat="server" Text='<%# Bind("HAC_SERVICE_CAT_ID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblCatName" runat="server" Text='<%# Bind("HAC_SERVICE_CAT_NAME") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblDiscType" CssClass="GridRow" runat="server" Text='<%# Bind("HAC_DISC_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpDisctype" CssClass="LabelStyle" runat="server" Width="100%" Height="30px">
                                    <asp:ListItem Value="$">$</asp:ListItem>
                                    <asp:ListItem Value="%">%</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Discount" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDiscount" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HAC_DISC_AMT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Coverage" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblCoveringType" runat="server" Text='<%# Bind("HAC_COVERING_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpCoveringType" runat="server" CssClass="label" Style="border: none;" Width="100%" BorderWidth="1px">
                                    <asp:ListItem Text="Covered" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Un Covered" Value="U"></asp:ListItem>
                                    <asp:ListItem Text="PreApproval" Value="P"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>
            </div>

            <table width="99%">
                <tr>
                    <td align="right">
                        <asp:CheckBox ID="chkNewCategory" runat="server" CssClass="label" Text="New Category" Checked="false"  AutoPostBack="true" OnCheckedChanged="chkNewCategory_CheckedChanged"/>
                        <asp:CheckBox ID="chkDeftDiscount" runat="server" CssClass="label" Text="Default Discount" Checked="false" AutoPostBack="true" OnCheckedChanged="chkDeftDiscount_CheckedChanged" />
                    </td>
                </tr>
            </table>
            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
            <asp:Button ID="btnDelete" runat="server" CssClass="button orange small" Width="100px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return  window.confirm('Do you want to delete Category Information?');" />
            <asp:Button ID="btnCancel" runat="server" CssClass="button orange small" Width="100px" Text="Cancel" OnClientClick="window.close();" />

        </div>
    </form>
</body>
</html>
