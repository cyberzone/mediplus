﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PackageServiceMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.PackageServiceMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>



    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>



    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ValSave() {

            if (document.getElementById('<%=txtPackageID.ClientID%>').value == "") {
                ShowErrorMessage('Package ID', 'Please enter Package ID')
                return false;
            }


            if (document.getElementById('<%=txtPackageName.ClientID%>').value == "") {

                ShowErrorMessage('Package Name Empty', 'Please enter Package Name')
                return false;
            }

            if (document.getElementById('<%=txtEffectiveDate.ClientID%>').value == "") {
                ShowErrorMessage('Effective Date Empty', 'Please enter Effective Date')
                return false;
            }


            if (document.getElementById('<%=txtExpiryDate.ClientID%>').value == "") {

                ShowErrorMessage('Expiry Date Empty', 'Please enter Expiry Date')
                return false;
            }
            return true;

        }
        function ValTransAdd() {

            if (document.getElementById('<%=txtServiceName.ClientID%>').value == "") {
                ShowErrorMessage('Service Details Empty', 'Please enter Service Details')
                return false;
            }


            if (document.getElementById('<%=txtSessionCount.ClientID%>').value == "") {
                ShowErrorMessage('Quantity Empty', 'Please enter Quantity')
                return false;
            }
            return true;

        }
        function ShowErrorMessage(vMessage1, vMessage2) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
            document.getElementById("divMessage").style.display = 'block';

        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }


        function ShowPackageDetailsPopup() {

            document.getElementById("divPackageDetails").style.display = 'block';


        }

        function HidePackageDetailsPopup() {

            document.getElementById("divPackageDetails").style.display = 'none';

        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 400px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="padding-top: 0px; width: 85%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
        <table style="width: 100%">
            <tr>
                <td align="left">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Package Service Master"></asp:Label>
                            <asp:ImageButton ID="imgPackageMasterZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Package Entry" OnClick="imgPackageMasterZoom_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" OnClientClick="return ValSave();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

        <table runat="server" id="tblEnqCtrls" cellpadding="5" cellspacing="5" style="width: 100%">
            <tr>
                <td class="lblCaption1" style="width: 150px;">Package ID <span style="color: red; font-size: 11px;">*</span>


                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPackageID" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" AutoPostBack="true" OnTextChanged="txtPackageID_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1">Name <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPackageName" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:CheckBox ID="chkActive" Text="Active" runat="server" CssClass="lblCaption1" Checked="true" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr style="display:none;">
                <td class="lblCaption1" style="width: 100px;">Effective Date <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtEffectiveDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 100px;">Expiry Date <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtExpiryDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 150px;">Package Service <span style="color: red; font-size: 11px;">*</span>


                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPackageServiceName" runat="server" Width="100%" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtPackageServiceName" MinimumPrefixLength="1" ServiceMethod="GetPackageServiceName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>




            </tr>
        </table>
      

        <table width="100%" border="0" cellpadding="3" cellspacing="3">
            <tr>
                <td class="lblCaption1" style="width: 80%">Service Code & Description
                </td>
                <td class="lblCaption1" style="width: 10%">Quantity</td>
                <td style="width: 10%"></td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServiceName" runat="server" Width="100%" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServiceName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSessionCount" runat="server" Width="100%" Style="height: 20px;" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnAdd" runat="server" CssClass="button red small" Text="Add" OnClick="btnAdd_Click" OnClientClick="return ValTransAdd();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 100%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvPackageTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>

                            <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvPakTransServCode" CssClass="lblCaption1" runat="server" OnClick="PackageTransEdit_Click">
                                        <asp:Label ID="lblgvTestProfTransProfID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPST_PACKAGE_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="Label1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPST_SL_NO") %>' Visible="false"></asp:Label>

                                        <asp:Label ID="lblgvPakTransServCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPST_SERV_CODE") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvPakTransServDesc" CssClass="lblCaption1" runat="server" OnClick="PackageTransEdit_Click">
                                        <asp:Label ID="lblgvPakTransServDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPST_SERV_DESC") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvPakTransSessionCount" CssClass="lblCaption1" runat="server" OnClick="PackageTransEdit_Click">
                                        <asp:Label ID="lblgvPakTransSessionCount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPST_QTY") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:ImageButton ID="DeleteTrans" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                        OnClick="DeleteTrans_Click" />&nbsp;&nbsp;
                                </ItemTemplate>

                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divPackageDetails" style="display: none; overflow: hidden; border: groove; height: 600px; width: 950px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 53px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="PageHeader" style="vertical-align: top; width: 50%;">Package Master List
                    </td>
                    <td align="right" style="vertical-align: top; width: 50%;">

                        <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HidePackageDetailsPopup()" />
                    </td>
                </tr>

            </table>
            <div style="width: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;">
                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvPackageMaster" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Package ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPakMasPackageID" CssClass="lblCaption1" runat="server" OnClick="PackageMasEdit_Click">
                                            <asp:Label ID="lblgvPakMasPackageID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_PACKAGE_ID") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPakMasName" CssClass="lblCaption1" runat="server" OnClick="PackageMasEdit_Click">
                                            <asp:Label ID="lblgvPakMasName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_PACKAGE_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPakMasStatus" CssClass="lblCaption1" runat="server" OnClick="PackageMasEdit_Click">
                                            <asp:Label ID="lblgvPakMasStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_STATUS") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Effective Date" HeaderStyle-HorizontalAlign="Left" Visible="false" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPakMasEffectiveDate" CssClass="lblCaption1" runat="server" OnClick="PackageMasEdit_Click">
                                            <asp:Label ID="lblgvPakMasEffectiveDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_EFFECTIVE_DATEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date" HeaderStyle-HorizontalAlign="Left" Visible="false" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPakMasExpiryDate" CssClass="lblCaption1" runat="server" OnClick="PackageMasEdit_Click">
                                            <asp:Label ID="lblgvPakMasExpiryDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_EXPIRY_DATEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Package Service" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPakMasPackServCode" CssClass="lblCaption1" runat="server" OnClick="PackageMasEdit_Click">
                                            <asp:Label ID="lblgvPakMasPackServCodee" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_PACK_SERV_CODE") %>'></asp:Label>
                                            ~ 
                                             <asp:Label ID="lblgvPakMasPackServName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HPSM_PACK_SERV_DESC") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>


            </div>



        </div>

    </div>
</asp:Content>
