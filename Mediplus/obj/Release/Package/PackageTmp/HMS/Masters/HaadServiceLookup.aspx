﻿<%@ page language="C#" autoeventwireup="true" codebehind="HaadServiceLookup.aspx.cs" inherits="Mediplus.HMS.Masters.HaadServiceLookup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
        <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script language="javascript" type="text/javascript">

        function passvalue(HaadCode, HaadName) {
            var CtrlName = document.getElementById('hidCtrlName').value;
            window.opener.BindHaadDtls(HaadCode, HaadName, CtrlName);
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="hidCtrlName" runat="server" />
        <div>
              <div style="padding-top: 0px; padding-left: 0px; width: 700px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table width="100%">
                    <tr>
                         <td class="lblCaption1" style="width: 100px;">Search
                        </td>
                        <td>
                           <asp:TextBox ID="txtSearch" runat="server"  CssClass="label" Width="300px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                        </td>
                       
                    </tr>
                </table>
            </div>
            <asp:GridView ID="gvHaadService" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="700">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHS_CODE")  %>','<%# Eval("HHS_DESCRIPTION")%>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHS_CODE") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Service Name">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHS_CODE")  %>','<%# Eval("HHS_DESCRIPTION")%>')">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("HHS_DESCRIPTION") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Price">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHS_CODE")  %>','<%# Eval("HHS_DESCRIPTION")%>')">
                                <asp:Label ID="Label3" CssClass="label" runat="server" Width="70px" runat="server" style="text-align: right;padding-right:5px;"  Text='<%# Bind("HHS_PRICE") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHS_CODE")  %>','<%# Eval("HHS_DESCRIPTION")%>')">
                                <asp:Label ID="Label4" CssClass="label" runat="server" Text='<%# Bind("HHS_TYPE") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
    </form>
</body>
</html>
