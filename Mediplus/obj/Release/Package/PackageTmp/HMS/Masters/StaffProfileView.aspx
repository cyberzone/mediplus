﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StaffProfileView.aspx.cs" Inherits="Mediplus.HMS.Masters.StaffProfileView" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <table width="100%" border="0">
                       <tr>
                        <td class="label" style="height:30px;width:15%">

                        </td>
                         <td style="width:20%" >
                        </td>
                           <td  style="width:15%"></td>
                           <td style="width:20%">
                           </td>
                        <td  style="width:15%"></td>
                            <td valign="middle" class="auto-style5" rowspan="4" style="width:15%">
                                  <asp:UpdatePanel ID="UpdatePanel16" runat="server"><ContentTemplate>
                                            <asp:Image ID="imgStaffPhoto" runat="server" Height="120px" Width="150px" />
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                               
                                       
                          </td>
                           </tr>
                       <tr>
                          <td class="label" style="height:30px;">
                          Staff ID
                        </td>
                         <td >
                            <asp:TextBox ID="txtStaffId"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"     ></asp:TextBox>

                        </td>
                        <td class="label">
                             Category
                        </td>
                        <td>
                            <asp:TextBox ID="txtCategory"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"     ></asp:TextBox>

                        </td>
                         
                       </tr>
                       <tr>
                          <td class="label" style="height:30px;">
                          Serv.Category
                        </td>
                         <td >
                            <asp:TextBox ID="txtServCategory"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>

                        </td>
                        <td class="label">
                            Department
                        </td>
                        <td>
                             <asp:TextBox ID="txtDepartment"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>

                        </td>
                       </tr>
                       <tr>

                       <td class="label" style="height:30px;">
                        Status
                        </td>
                         <td >
                            <asp:TextBox ID="txtStatus"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>


                        </td>
                           <td class="label">
                           Designation
                        </td>
                        <td>
                              <asp:TextBox ID="txtDesignation"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>

                        </td>
                       </tr>
                       <tr>
                        <td class="label" style="height:30px;">
                         First Name
                        </td>
                           <td>
                        <asp:TextBox ID="txtFName"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                           </td>
                           <td class="label" style="height:30px;">
                                 Middle Name
                             </td>
                           <td>
                                <asp:TextBox ID="txtMName"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"   ></asp:TextBox>
                           </td>
                            <td class="label" style="height:30px;">
                                 Last Name
                             </td>
                           <td>
                                <asp:TextBox ID="txtLName"   CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>
                           </td>
                       </tr>
                       <tr>
                          <td class="label" style="height:30px;">
                            DOB
                          </td>
                           <td>
                             <asp:TextBox ID="txtDOB" runat="server" Width="150px" CssClass="label" ReadOnly="true" BorderWidth="1px" BorderColor="#CCCCCC" MaxLength="10" onblur="AgeCalculation()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                     </td>
                            <td class="label" style="height:30px;">
                               Age
                           </td>
                           <td>
                             <asp:TextBox ID="txtAge"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>

                           </td>
                            <td class="label" style="height:30px;">
                               Sex
                           </td>
                             <td  >
                                 
                                <asp:TextBox ID="txtSex"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                            </td>
                       </tr>
                       <tr>
                            <td class="label" style="height:30px;">
                               Martiial Status
                           </td>
                             <td class="auto-style36">
                        
                             <asp:TextBox ID="txtMStatus"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>

                            </td>
                           
                            <td class="label" style="height:30px;">
                              No. of Children
                           </td>
                           <td>
                             <asp:TextBox ID="txtChildren" CssClass="label" ReadOnly="true"  runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);" ></asp:TextBox>

                           </td>
                            <td class="label" style="height:30px;">
                             Religion
                           </td>
                           <td>
                            <asp:TextBox ID="txtReligion"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>


                           </td>
                       </tr>
                       <tr>
                           <td class="label" style="height:30px;">
                             Blood Group
                           </td>
                            <td>
                             <asp:TextBox ID="txtBlood"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>



                           </td>  
                           <td class="label" style="height:30px;">
                             Edu. Quali
                           </td>
                           <td>
                             <asp:TextBox ID="txtEduQuali" CssClass="label" ReadOnly="true"  runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>

                           </td>
                            <td class="label" style="height:30px;">
                            PO Box
                           </td>
                           <td>
                             <asp:TextBox ID="txtPOBox" CssClass="label" ReadOnly="true"  runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>


                           </td>

                       </tr>
                       <tr>
                           <td class="label" valign="top" style="height:30px;" rowspan="3">
                            Address
                           </td>
                           <td rowspan="3">
                             <asp:TextBox ID="txtAddress" CssClass="label" ReadOnly="true"  runat="server" TextMode="MultiLine" Width="155px" Height="120px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>


                           </td>
                           <td class="label" style="height:30px;">
                               Country
                           </td>
                           <td>
                         <asp:TextBox ID="txtCountry"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>

                           </td>
                           <td class="label" style="height:30px;">
                               State
                           </td>
                           <td>
                            <asp:TextBox ID="txtState"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>

                           </td>
                       </tr>
                       <tr>
                            <td class="label" style="height:30px;">
                            City
                           </td>
                           <td>
                                <asp:TextBox ID="txtCity"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>


                           </td>
                            <td class="label" style="height:30px;">
                            Nationality
                           </td>
                           <td>
                            <asp:TextBox ID="txtNationality"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>

                           </td>
                       </tr>
                       <tr>
                           <td class="label" style="height:30px;">
                           Phone (R)
                           </td>
                           <td>
                             <asp:TextBox ID="txtPhone1"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>

                           </td>
                            <td class="label" style="height:30px;">
                           Phone-2
                           </td>
                           <td>
                             <asp:TextBox ID="txtPhone2"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>


                           </td>
                       </tr>
                       <tr>
                          <td class="label" style="height:30px;">
                         Fax
                           </td>
                           <td>
                             <asp:TextBox ID="txtFax"   CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>

                           </td>
                            <td class="label" style="height:30px;">
                          Mobile
                           </td>
                           <td>
                             <asp:TextBox ID="txtMobile"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>

                           </td>
                            <td class="label" style="height:30px;">
                          Email
                           </td>
                           <td>
                             <asp:TextBox ID="txtEmail"  CssClass="label" ReadOnly="true" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC"  ></asp:TextBox>

                           </td>

                       </tr>
                       </table>
    </div>
    </form>
</body>
</html>
