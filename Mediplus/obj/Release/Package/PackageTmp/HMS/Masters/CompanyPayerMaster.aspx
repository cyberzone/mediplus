﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CompanyPayerMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.CompanyPayerMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

     <script language="javascript" type="text/javascript">

         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }

         function SaveVal() {
             var label;
             label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


             var strPayerName = document.getElementById('<%=txtPayerName.ClientID%>').value
             if (/\S+/.test(strPayerName) == false) {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Payer Name";
                document.getElementById('<%=txtPayerName.ClientID%>').focus;
                return false;
             }
          

        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidPayerID" runat="server" />
    <table>
        <tr>
            <td class="PageHeader">Company Payer ID Master
               
            </td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />

     <div style="padding: 5px; width: 900px; ">

          <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">
                 <asp:GridView ID="gvPayerID" runat="server"   AutoGenerateColumns="False"   
                                EnableModelValidation="True" Width="100%" PageSize="200"     >
                    <HeaderStyle CssClass="GridHeader_Blue" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Payer Name">
                                    <ItemTemplate>
                                             <asp:Label ID="lblPayerName" CssClass="GridRow"  runat="server" Text='<%# Bind("HPC_PATIENT_COMPANY") %>'   ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Delete" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="Delete_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                        </asp:GridView>

                    </div>
     <br />



         <table width="100%">
             <tr>
                   <td class="lblCaption1" style="height: 30px;" > Payer ID &nbsp;   :<span style="color:red;">* </span>
                     <asp:textbox id="txtPayerName" cssclass="label" runat="server" width="350px" borderwidth="1px" bordercolor="#CCCCCC" Maxlength="50"></asp:textbox>

                   </td>
             </tr>
              
         </table>
         <br />
          <table width="50%">
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
                <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

            </td>
        </tr>
    </table>
     </div>

</asp:Content>
