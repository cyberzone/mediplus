﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Doctor_Name_Zoom.aspx.cs" Inherits="Mediplus.HMS.Masters.Doctor_Name_Zoom" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
            <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
        <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script language="javascript" type="text/javascript">
        function passvalue(StaffId) {
            if (document.getElementById('hidPageName').value == 'StaffProfile') {
                opener.location.href = "StaffProfile.aspx?MenuName=Masters&StaffId=" + StaffId;
            }

            if (document.getElementById('hidPageName').value == 'PTDocScan') {
                opener.location.href = "../Registration/PatientDocScans.aspx?StaffId=" + StaffId;
            }
            opener.focus();
            window.close();

        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }
        
    </script>
  
    
</head>
<body >
    <form id="form1" runat="server">
        <div style="background-color: #FFFFFF; height: 485px; width: 100%;">


            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <div style="padding-top: 5px;">
                 <asp:hiddenfield id="hidPageName" runat="server" />
                <table align="center" class="style1" cellpadding="3" cellspacing="3">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                                Text="Staff ID:"></asp:Label>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDoctorId" CssClass="label" runat="server" Width="70px" BorderWidth="1px" Bordercolor="#CCCCCC"  ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>

                            <asp:Label ID="Label4" runat="server" CssClass="lblCaption1"
                                Text="Name:"></asp:Label>

                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDoctorName" CssClass="label"  runat="server" Width="250px" BorderWidth="1px"  Bordercolor="#CCCCCC"  ></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>

                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                                Text="Department:"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpDepartment" CssClass="lblCaption1"  runat="server" Width="200px" AutoPostBack="true" Bordercolor="#CCCCCC"  OnSelectedIndexChanged="drpDepartment_SelectedIndexChanged"></asp:DropDownList>
                        </td>

                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnFind" runat="server" CssClass="button orange small" Height="25px"
                                        OnClick="btnFind_Click" Text="Find" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="padding-top: 10px; width: 100%; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvGridView" runat="server"
                            AllowSorting="True" AutoGenerateColumns="False" CssClass="Grid"
                            EnableModelValidation="True" OnSorting="gvGridView_Sorting"
                            Width="100%">
                            <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                               <asp:TemplateField HeaderText="Staff ID" SortExpression="HSFM_STAFF_ID">
                                    <ItemTemplate>
                                         <a href="javascript:passvalue('<%# Eval("HSFM_STAFF_ID")  %>')">
                                               <asp:Label ID="Label3" CssClass="label" runat="server" Text='<%# Bind("HSFM_STAFF_ID") %>' Width="100px"></asp:Label>
                                                     </a>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="FullName">
                                    <ItemTemplate>
                                              <a href="javascript:passvalue('<%# Eval("HSFM_STAFF_ID")  %>')">
                                               <asp:Label ID="Label5" CssClass="label" runat="server" Text='<%# Bind("FullName") %>' Width="100px"></asp:Label>
                                                     </a>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Department" SortExpression="HSFM_DEPT_ID">
                                    <ItemTemplate>
                                         <a href="javascript:passvalue('<%# Eval("HSFM_STAFF_ID")  %>')">
                                               <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("HSFM_DEPT_ID") %>' Width="100px"></asp:Label>
                                                     </a>
                                    </ItemTemplate>

                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Category" SortExpression="HSFM_CATEGORY">
                                    <ItemTemplate>
                                        
                                         <a href="javascript:passvalue('<%# Eval("HSFM_STAFF_ID")  %>')">
                                               <asp:Label ID="Label7" CssClass="label" runat="server" Text='<%# Bind("HSFM_CATEGORY") %>' Width="100px"></asp:Label>
                                                     </a>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Status" SortExpression="HSFM_SF_STATUS">
                                    <ItemTemplate>
                                        
                                         <a href="javascript:passvalue('<%# Eval("HSFM_STAFF_ID")  %>')">
                                               <asp:Label ID="Label8" CssClass="label" runat="server" Text='<%# Bind("HSFM_SF_STATUS") %>' Width="100px"></asp:Label>
                                                     </a>
                                    </ItemTemplate>

                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
           

        </div>
    </form>
</body>
</html>
