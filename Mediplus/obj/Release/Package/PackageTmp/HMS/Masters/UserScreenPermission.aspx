﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="UserScreenPermission.aspx.cs" Inherits="Mediplus.HMS.Masters.UserScreenPermission" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script src="../Validation.js" type="text/javascript"></script>


    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlightvisit
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }

        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }


        #divRefDr
        {
            width: 400px !important;
        }

            #divRefDr div
            {
                width: 400px !important;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="width: 80%;">
        <table width="80%">
            <tr>
                <td style="text-align: left; width: 20%;" class="PageHeader">

                    <asp:Label ID="lblPageHeader" runat="server" CssClass="Profile Master" Text="User Screen Permission"></asp:Label>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td class="lblCaption1">User ID & Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div id="divRefDr" style="visibility: hidden;"></div>
                            <asp:TextBox ID="txtRefDoctorName" runat="server" CssClass="TextBoxStyle" Width="250px" MaxLength="50"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtRefDoctorName" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divRefDr">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                 <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Text="Clear" OnClick="btnClear_Click" />
                                  <asp:Button ID="btnShow" runat="server" CssClass="button red small" Text="Show" OnClick="btnShow_Click" />
                                  
                              
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
            </tr>
        </table>
        <div style="padding-top: 0px;padding-left:5px; height:600px; width: 100%;   overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvRollTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="5" CellSpacing="0"
                            EnableModelValidation="True" Width="100%" GridLines="None" OnRowDataBound="gvRollTrans_RowDataBound">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="MAIN MENU NAME" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                         <asp:Label ID="lblMainMenuName" CssClass="GridRow" runat="server" Text='<%# Bind("HSCR_MAIN_MENUNAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SCREEN NAME" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScreenID" CssClass="GridRow" runat="server" Text='<%# Bind("HSCR_SCREEN_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblPermission" CssClass="GridRow" runat="server" Text='<%# Bind("HRT_PERMISSION") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblScreenName" CssClass="GridRow" runat="server" Text='<%# Bind("HSCR_SCREEN_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField  HeaderText="NO PERMISSION" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:CheckBox ID="chkNoPermission"  runat="server" onClick="fnSelectPermission(this)" />
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="READ ONLY" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkReadOnly" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CREATE" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCreate" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MODIFY" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkModify" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DELETE" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkDelete" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FULL PERMISSION" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkFullPermission" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
    </div>


</asp:Content>
