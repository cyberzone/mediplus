﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CustomerEnquiryMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.CustomerEnquiryMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>



    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        
        function ValSave() {

            if (document.getElementById('<%=txtEnquiryID.ClientID%>').value == "") {
                ShowErrorMessage('Enquiry ID', 'Please enter Enquiry ID')
                return false;
            }

           
            if (document.getElementById('<%=txtFName.ClientID%>').value == "") {

                ShowErrorMessage('Customer Name Empty', 'Please enter Customer Name')
                return false;
            }

            if (document.getElementById('<%=txtEnquiryDate.ClientID%>').value == "") {

                ShowErrorMessage('nquiry  Datee Empty', 'Please enter Enquiry  Date')
                return false;
            }


            if (document.getElementById('<%=txtMobile.ClientID%>').value == "") {

                ShowErrorMessage('Mobile No Empty', 'Please enter Mobile No.')
                return false;
            }

            return true;

        }

        function ShowErrorMessage(vMessage1, vMessage2) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
              document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
              document.getElementById("divMessage").style.display = 'block';

          }

          function HideErrorMessage() {

              document.getElementById("divMessage").style.display = 'none';

              document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />

     <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="padding-top: 0px; width: 70%; height: 600px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">

        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
            <tr>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

        <table style="width: 100%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Customer Enquiry Master"></asp:Label>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save"  OnClientClick="return ValSave();"  />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>
        <table runat="server" id="tblEnqCtrls" cellpadding="5" cellspacing="5" style="width: 100%">
            <tr>
                <td class="lblCaption1" style="width: 100px;">Enquiry ID <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEnquiryID" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Date <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEnquiryDate" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtEnquiryDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Name <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFName" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Phone
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPhone" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>

                <td class="lblCaption1">Mobile <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMobile" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Email
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEmail" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>

                <td class="lblCaption1">Know From
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpKnowFrom" runat="server" CssClass="TextBoxStyle" Width="90%"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

        <br /><br />

       <table runat="server" id="Table1" cellpadding="5" cellspacing="5" style="width: 100%">
            <tr>
                <td class="lblCaption1" style="width: 100px;">Search
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEnquirSearch" runat="server" Width="90%" CssClass="TextBoxStyle"  ></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                  <td class="lblCaption1">Date  
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEnqFromDate" runat="server" CssClass="TextBoxStyle" Width="45%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtEnqFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>

                             <asp:TextBox ID="txtEnqToDate" runat="server" CssClass="TextBoxStyle" Width="45%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtEnqToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Status
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle" Width="60%">
                                <asp:ListItem Text="---All---" Value=""></asp:ListItem>
                                <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                <asp:ListItem Text="Closed" Value="Closed"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ImageButton ID="imgACMasRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="imgACMasRefres_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                
            </tr>

        </table>
        <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gvEnquiryMaster" runat="server" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%" PageSize="200">
                        <HeaderStyle CssClass="GridHeader_Gray" />

                        <RowStyle CssClass="GridRow" />
                        <Columns>

                            <asp:TemplateField HeaderText="Enquiry ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEnquiryID" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblEnquiryID" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_ID") %>'></asp:Label>

                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEnquiryDate" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblEnquiryDate" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_DATEDesc") %>'></asp:Label>

                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkName" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblName" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_FNAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPhone" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblPhone" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_PHONE") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mobile">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkMobile" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_MOBILE") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EMail">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEMail" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblEMail" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_EMAIL") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Know From">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkKnowFrom" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblKnowFrom" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_KNOW_FROM") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblKnowFromDesc" CssClass="GridRow" runat="server" Text='<%# Bind("Description") %>'></asp:Label>

                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Select_Click">
                                        <asp:Label ID="lblStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_STATUS") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Actions" Visible="true" HeaderStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="../Images/icon_delete.png" AlternateText="Delete"
                                        OnClick="Delete_Click" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                    </asp:GridView>
                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
