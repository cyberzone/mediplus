﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="DepartmentMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.DepartmentMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div>
        <table style="width: 75%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Department Master"></asp:Label>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                                <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

          <div style="padding-top: 0px; width: 70%; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
              <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
            <table cellpadding="5" cellspacing="5" style="width: 100%">
                <tr>
                    <td class="lblCaption1" style="width: 100px;">Code
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtDeptCode" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                 <tr>
                    <td class="lblCaption1" style="width: 100px;">Name
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtDeptName" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 100px;"> 
                    </td>
                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="chkStatus" runat="server"  Text="Active" CssClass="TextBoxStyle" Checked="true" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

                </table>
           </div>
          <br />
             <div style="padding-top: 0px; width: 70%; height: 300px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>

                        <asp:GridView ID="gvDepttMaster" runat="server" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" PageSize="200">
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDeptCode" runat="server" OnClick="Select_Click">
                                         
                                            <asp:Label ID="lblDeptCode" CssClass="GridRow" runat="server" Text='<%# Bind("HDM_DEP_ID") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDeptMame" runat="server" OnClick="Select_Click">
                                            <asp:Label  ID="lblDeptName" CssClass="GridRow" runat="server" Text='<%# Bind("HDM_DEP_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Select_Click">
                                            <asp:Label  ID="lblStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HDM_STATUS") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Actions" Visible="true" HeaderStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="../Images/icon_delete.png" AlternateText="Delete"
                                            OnClick="Delete_Click" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                        </asp:GridView>
                    </ContentTemplate>
                   
                </asp:UpdatePanel>
            </div>
          </div>
    <br />
    <br />

</asp:Content>
