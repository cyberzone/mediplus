﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="UserMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.UserMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script src="../Validation.js" type="text/javascript"></script>


    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }



        function ShowErrorMessage(vMessage1, vMessage2) {

            document.getElementById("divMessage").style.display = 'block';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
              document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
          }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>
    <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="Range Master" Text="User Master"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpPanalSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" OnClientClick="return SaveValidate();"   />
                        <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Text="Clear" OnClick="btnClear_Click" />
                    </ContentTemplate>

                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table class="table" cellpadding="1" cellspacing="1" style="width: 80%;">

        <tr>
            <td class="lblCaption1" style="height: 30px;">User ID <span style="color: red;">* </span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtUserID" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>


            <td class="lblCaption1" style="height: 30px;">Password : <span style="color: red;">* </span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">User Name <span style="color: red;">* </span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtUserName" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

            <td class="lblCaption1" style="height: 30px;">Confirm Password : <span style="color: red;">* </span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtConPwd" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Staff ID  
            </td>
            <td  >
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtStaffID" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 30px;"> Active
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
 
    <div style="padding-top: 0px;padding-left:5px; height:600px; width: 80%;   overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:GridView ID="gvUserList" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" GridLines="None">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>

                        <asp:TemplateField HeaderText="USER ID"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnUserID" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblPassword" CssClass="GridRow" runat="server" Text='<%# Bind("HUM_USER_PASS") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblUserID" CssClass="GridRow" runat="server" Text='<%# Bind("HUM_USER_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="USER NAME"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkUserName" CssClass="GridRow" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblUserName" CssClass="GridRow" runat="server" Text='<%# Bind("HUM_USER_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="STAFF ID"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkStaffID" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblStaffID" CssClass="GridRow" runat="server" Text='<%# Bind("HUM_REMARK") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="STATUS"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HUM_STATUS") %>' Visible="false" ></asp:Label>
                                    <asp:Label ID="lblStatusDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HUM_STATUSDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>






                    </Columns>


                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <br />
    <br />

    <script type="text/javascript" language="Javascript">


        function SaveValidate() {



            if (document.getElementById('<%=txtPassword.ClientID%>').value == "") {
                alert("Please Enter the New Password");
                document.getElementById('<%=txtPassword.ClientID%>').focus();
                 return false;
             }

             if (document.getElementById('<%=txtConPwd.ClientID%>').value == "") {
                alert("Please Enter the Confirm Password");
                document.getElementById('<%=txtConPwd.ClientID%>').focus();
                 return false;
             }

             if (document.getElementById('<%=txtPassword.ClientID%>').value != document.getElementById('<%=txtConPwd.ClientID%>').value) {
                alert("Please Enter the Same Confirm Password");
                document.getElementById('<%=txtConPwd.ClientID%>').value = "";
                 document.getElementById('<%=txtConPwd.ClientID%>').focus();
                return false;
            }

            if (txtPassword(document.getElementById('<%=txtPassword.ClientID%>').value) == false) {
                alert('Password ! Single Code Characters are not Allowed');
                document.getElementById('<%=txtPassword.ClientID%>').value = "";
                 document.getElementById('<%=txtConPwd.ClientID%>').value = "";
                                    document.getElementById('<%=txtPassword.ClientID%>').focus();
                                    return false;
                                }


            //return true;
                                return ConfirmMsg();

                            }

    </script>
</asp:Content>
