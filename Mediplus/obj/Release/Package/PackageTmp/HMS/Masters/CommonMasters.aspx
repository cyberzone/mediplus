﻿<%@ page title="" language="C#" masterpagefile="~/Site2.Master" autoeventwireup="true" codebehind="CommonMasters.aspx.cs" inherits="Mediplus.HMS.Masters.CommonMasters" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function Val() {


            if (document.getElementById('<%=drpType.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Type";
                document.getElementById('<%=drpType.ClientID%>').focus;
                return false;
            }

            var strCode = document.getElementById('<%=txtCode.ClientID%>').value
            if (/\S+/.test(strCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Code";
                document.getElementById('<%=txtCode.ClientID%>').focus;
                return false;
            }

            var strDesc = document.getElementById('<%=txtDesc.ClientID%>').value
            if (/\S+/.test(strDesc) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Description";
                document.getElementById('<%=txtDesc.ClientID%>').focus;
                return false;
            }

        }

      
      

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 
    <table>
        <tr>
            <td class="PageHeader">Masters
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:label id="lblStatus" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
            </td>
        </tr>
    </table>
    
          <input type="hidden" id="hidHCM_ID" runat="server" />
      <div style="height: 200px;width:900px; overflow: auto"  >
                <asp:GridView ID="gvCommMaster" runat="server" AutoGenerateColumns="False" 
                    HeaderStyle-CssClass="FixedHeader"    gridlines="None"
                    EnableModelValidation="True" Width="100%"   PageSize="200" OnRowDataBound="gvCommMaster_RowDataBound"   >
                    <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true"   />
                    <RowStyle CssClass="GridRow"  />
                  
                    <Columns>
                          <asp:TemplateField HeaderText="Code" HeaderStyle-Width="120px" ItemStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEmergencyNotes" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblHcmID" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblHcmCode" CssClass="GridRow" runat="server"   Text='<%# Bind("HCM_CODE") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="120px" ItemStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEpmDate" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblHcmDesc" CssClass="GridRow" runat="server"  Text='<%# Bind("HCM_DESC") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="120px" ItemStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkInsName" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblHcmType" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_TYPE") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Status" HeaderStyle-Width="120px" ItemStyle-Width="120px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkActicve" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblActive" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_STATUS") %>' visible="false" ></asp:Label>
                                    <asp:Label ID="lblActiveDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_STATUSDesc") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    
                       
                    </Columns>
                    <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />


                </asp:GridView>

           </div>
      
            <div style="height: 200px;width:820px; overflow: auto;display:none;"  >
                <asp:GridView ID="gvDistricts" runat="server" HeaderStyle-CssClass="FixedHeader" HeaderStyle-BackColor="YellowGreen" 
                    AutoGenerateColumns="false" width="800px" AlternatingRowStyle-BackColor="WhiteSmoke" OnRowDataBound="gvDistricts_RowDataBound">
                    <RowStyle CssClass="GridRow"  />
                      <Columns>
                          <asp:TemplateField HeaderText="Code" HeaderStyle-Width="195px" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEmergencyNotes" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblHcmID" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblHcmCode" CssClass="GridRow" runat="server"   Text='<%# Bind("HCM_CODE") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="300px" ItemStyle-Width="300px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEpmDate" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblHcmDesc" CssClass="GridRow" runat="server"  Text='<%# Bind("HCM_DESC") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="200px" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkInsName" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblHcmType" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_TYPE") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkActicve" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblActive" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_STATUS") %>' visible="false" ></asp:Label>
                                    <asp:Label ID="lblActiveDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_STATUSDesc") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    
                       
                    </Columns>
                    
                </asp:GridView>
            </div>


    <div style="width:800px;display:none;">
        <div id="GHead">

        </div>
        <div style="height:200px; overflow:auto;">
             <asp:GridView ID="GridView1" runat="server"  HeaderStyle-BackColor="YellowGreen" 
                    AutoGenerateColumns="false" width="800px" AlternatingRowStyle-BackColor="WhiteSmoke"  >
                    <RowStyle CssClass="GridRow"  />
                      <Columns>
                          <asp:TemplateField HeaderText="Code" HeaderStyle-Width="195px" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="Label2" CssClass="GridRow" runat="server"   Text='<%# Bind("HCM_CODE") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="300px" ItemStyle-Width="300px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label3" CssClass="GridRow" runat="server"  Text='<%# Bind("HCM_DESC") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" HeaderStyle-Width="200px" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_TYPE") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label5" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_STATUS") %>' visible="false" ></asp:Label>
                                    <asp:Label ID="Label6" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_STATUSDesc") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    
                       
                    </Columns>
                    
                </asp:GridView>

        </div>
    </div>
             
    <br />
    <table width="100%">
          <tr>

            <td class="lblCaption1" style="width:100px;height:30px;">Type<span style="color: red;">* </span>
            </td>
            <td>
                <asp:dropdownlist id="drpType" cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="200px">
                     <asp:ListItem Value="0" Selected>--- Select ---</asp:ListItem>
                     <asp:ListItem Value="ApptRooms"  >Appointment Rooms</asp:ListItem>
                     <asp:ListItem Value="ApptTypeService" >Appointment Service Type</asp:ListItem>
                </asp:dropdownlist>
            </td>
        </tr>
        <tr>

            <td class="lblCaption1" style=" height:30px;">Code <span style="color: red;">* </span>
            </td>
            <td>
                <asp:textbox id="txtCode" runat="server" cssclass="label" borderwidth="1px"  bordercolor="#cccccc" width="200px"  ></asp:textbox>

            </td>

        </tr>
        <tr>
            <td class="lblCaption1" style=" height:30px;">Description <span style="color: red;">* </span>
            </td>
            <td>
                <asp:textbox id="txtDesc" runat="server" cssclass="label" borderwidth="1px"   bordercolor="#cccccc" width="200px"  ></asp:textbox>

            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                   <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="Active" Checked="true" />
            </td>
        </tr>
      
        <tr>
                       <td style=" height:30px;"></td>
                        <td >
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return Val();" />
                             <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button orange small" Width="90px" OnClick="btnClear_Click"  />
                             
                             <asp:Button ID="btnShow" runat="server" Text="Show"   visible="false"   />
                        </td>
                    </tr>

    </table>
         <style type="text/css">
        .FixedHeader {
            position: absolute;
            font-weight: bold;
            vertical-align:text-bottom;
        }      
    </style>
</asp:Content>
