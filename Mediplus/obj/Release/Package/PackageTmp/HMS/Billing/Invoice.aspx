﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="Mediplus.HMS.Billing.Invoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script src="../Validation.js" type="text/javascript"></script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }

        #divOrdClin
        {
            width: 400px !important;
        }

            #divOrdClin div
            {
                width: 400px !important;
            }


        #divServDR
        {
            width: 400px !important;
        }

            #divServDR div
            {
                width: 400px !important;
            }


        #divServOrdDR
        {
            width: 400px !important;
        }

            #divServOrdDR div
            {
                width: 400px !important;
            }
    </style>
    <script language="javascript" type="text/javascript">


        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=Invoice&CtrlName=" + CtrlName + "&Value=" + strValue, "Firstname_Zoom", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }



        function ShowPrintInvoice(BranchID, InvoiceNo, ReportName) {

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';


            if (ReportName == "HmsCreditBill1.rpt") {
                Criteria += ' AND {HMS_INVOICE_TRANSACTION.HIT_SERV_CODE} <> \'1111\' AND  {HMS_INVOICE_TRANSACTION.HIT_SERV_CODE} <> \'others\'';
            }

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    </script>


    <style type="text/css">
        .modal
        {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }
    </style>

    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function PTPopup() {
            var PtId = document.getElementById("<%=txtFileNo.ClientID%>").value;
            if (PtId != "") {
                var win = window.open("../Registration/PTRegistration.aspx?PatientId=" + PtId, "PTRegistration", "top=200,left=100,height=700,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
            }
        }


        function ServicePopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
                win.focus();
            }

            return true;

        }
        function CheckInvType(msg1, msg2) {
            var isDelCompany = window.confirm('This patient is registered as ' + msg1 + ' Do you want Invoice this Patient as ' + msg2);

            return true

        }

        function CompIdSelected() {
            if (document.getElementById('<%=txtSubCompanyID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtSubCompanyID.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }
            }

            return true;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function ServDRIdSelected() {
            if (document.getElementById('<%=txtServDrCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServDrCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServDrCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServDrName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function ServDRNameSelected() {
            if (document.getElementById('<%=txtServDrName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServDrName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServDrCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServDrName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function ServOrdClinCodeSelected() {
            if (document.getElementById('<%=txtServOrdClinCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServOrdClinCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServOrdClinName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function ServOrdClinNameSelected() {
            if (document.getElementById('<%=txtServOrdClinName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServOrdClinName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServOrdClinName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function OrdClinIdSelected() {
            if (document.getElementById('<%=txtOrdClinCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtOrdClinCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtOrdClinName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function OrdClinNameSelected() {
            if (document.getElementById('<%=txtOrdClinName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtOrdClinName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtOrdClinName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function ServIdSelected() {
            if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                    document.getElementById('<%=txtServType.ClientID%>').value = Data1[2];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                    document.getElementById('<%=txtServType.ClientID%>').value = Data1[2];
                }
            }

            return true;
        }


        function DiagIdSelected() {
            if (document.getElementById('<%=txtDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DiagNameSelected() {
            if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

      

        
        function ConfirmYes() {

            document.getElementById('<%=hidMsgConfirm.ClientID%>').value = "true";

        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }


        function ServiceAddVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strtxtServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }


            var strtxtServName = document.getElementById('<%=txtServName.ClientID%>').value
            if (/\S+/.test(strtxtServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Name";
                document.getElementById('<%=txtServName.ClientID%>').focus;
                return false;
            }



        }

        function DiagAddVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
             label.style.color = 'red';
             document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtDiagCode.ClientID%>').value
             if (/\S+/.test(strtxtServCode) == false) {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Diagnosis  Code";
                document.getElementById('<%=txtDiagCode.ClientID%>').focus;
                return false;
            }

            var strtxtServName = document.getElementById('<%=txtDiagName.ClientID%>').value
             if (/\S+/.test(strtxtServName) == false) {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Name";
                document.getElementById('<%=txtDiagName.ClientID%>').focus;
                return false;
            }

        }

        function DeleteInvoiceVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strCompCode = document.getElementById('<%=txtInvoiceNo.ClientID%>').value
            if (/\S+/.test(strCompCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Invoice No";
                document.getElementById('<%=txtInvoiceNo.ClientID%>').focus;
                return false;
            }

            var isDelCompany = window.confirm('Do you want to delete Invoice Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }



        function InvSaveVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br>";


            var strtxtInvoiceNo = document.getElementById('<%=txtInvoiceNo.ClientID%>').value
            if (/\S+/.test(strtxtInvoiceNo) == false) {
                ErrorMessage += 'Invoice No. is Empty.' + trNewLineChar;
                IsError = true;
            }

            var strtxtInvDate = document.getElementById('<%=txtInvDate.ClientID%>').value
            if (/\S+/.test(strtxtInvDate) == false) {
                ErrorMessage += 'Invoice Date is Empty.' + trNewLineChar;
                IsError = true;
            }


            var strtxtInvTime = document.getElementById('<%=txtInvTime.ClientID%>').value
            if (/\S+/.test(strtxtInvTime) == false) {
                ErrorMessage += 'Invoice Time is Empty.' + trNewLineChar;
                IsError = true;
            }

            var strtxtFileNo = document.getElementById('<%=txtFileNo.ClientID%>').value
            if (/\S+/.test(strtxtFileNo) == false) {
                ErrorMessage += 'File No. is Empty.' + trNewLineChar;
                IsError = true;
            }


            var strtxtName = document.getElementById('<%=txtName.ClientID%>').value
            if (/\S+/.test(strtxtName) == false) {
                ErrorMessage += 'Patient Name is Empty.' + trNewLineChar;
                IsError = true;
            }




            if (document.getElementById('<%=drpInvType.ClientID%>').value == "Credit") {


                var strtxtSubCompanyID = document.getElementById('<%=txtSubCompanyID.ClientID%>').value
                if (/\S+/.test(strtxtSubCompanyID) == false) {
                    ErrorMessage += 'Company ID is Empty.' + trNewLineChar;
                    IsError = true;
                }


                var strtxtCompanyName = document.getElementById('<%=txtCompanyName.ClientID%>').value
                if (/\S+/.test(strtxtCompanyName) == false) {
                    ErrorMessage += 'Company Name is Empty.' + trNewLineChar;
                    IsError = true;
                }

                var strtxtPolicyNo = document.getElementById('<%=txtPolicyNo.ClientID%>').value
                if (/\S+/.test(strtxtPolicyNo) == false) {
                    ErrorMessage += 'Policy No. is Empty.' + trNewLineChar;
                    IsError = true;
                }


                var strtxtPolicyExp = document.getElementById('<%=txtPolicyExp.ClientID%>').value
                if (/\S+/.test(strtxtPolicyExp) == false) {
                    ErrorMessage += 'Policy Expiry Date is Empty.' + trNewLineChar;
                    IsError = true;
                }



            }


            var strtxtDoctorID = document.getElementById('<%=txtDoctorID.ClientID%>').value
            if (/\S+/.test(strtxtDoctorID) == false) {
                ErrorMessage += 'Doctor Code is Empty.' + trNewLineChar;
                IsError = true;
            }

            var strtxtDoctorName = document.getElementById('<%=txtDoctorName.ClientID%>').value
            if (/\S+/.test(strtxtDoctorName) == false) {
                ErrorMessage += 'Doctor Name is Empty.' + trNewLineChar;
                IsError = true;
            }

            var PaidAmount = document.getElementById('<%=TxtRcvdAmt.ClientID%>').value;
            var PaidAmtCheck = document.getElementById('<%=hidPaidAmtCheck.ClientID%>').value;
            var PaidAmtChange = '<%=Convert.ToString(ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"]) %>';
            var NewFlag = '<%=Convert.ToString(ViewState["NewFlag"]) %>';


            if (PaidAmtChange == "Y" && NewFlag == "False") {
                if (PaidAmount != PaidAmtCheck) {

                    var isMsgConfirm = window.confirm('Paid Amount changed, Do you want to Continue? ');
                    document.getElementById('<%=hidMsgConfirm.ClientID%>').value = isMsgConfirm;
                }

            }

            if (IsError == true) {
                ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }

            return true
        }


        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName, Fees, ServType) {

            document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
            document.getElementById("<%=txtServName.ClientID%>").value = ServName;

            document.getElementById("<%=txtServType.ClientID%>").value = ServType;

            document.getElementById("<%=txtServHaadCode.ClientID%>").value = HaadCode;


        }
    </script>
    <script language="javascript" type="text/javascript">

        function ShowPrintCrBill(InvoiceNo, ReportName) {
            var BranchID = '<%=Convert.ToString(Session["Branch_ID"]) %>';

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';


            //  var win = window.open('../CReports/HMSReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'PrintCrBill', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintCrBill', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }


        function ShowTestMsg() {

            alert('test');
        }



        function ShowMergeReport(BranchID, InvoiceNo, InvDate, FileNo, DrId, SubCompId, PolicyNo, NoMergePrint) {

            var arrFromDate = InvDate.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }



            var Report = "HmsMergeCreditBill.rpt";
            var Criteria = " 1=1 "
            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_PT_ID}= \'' + FileNo + '\''

            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'';

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DR_CODE}= \'' + DrId + '\''



            if (FileNo == "OPDCR" || FileNo == "OPD") {

                Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_POLICY_NO}= \'' + PolicyNo + '\''
            }

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_SUB_INS_CODE}=\'' + SubCompId + '\''

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}=date(\'' + Date1 + '\')'



            if (NoMergePrint == 'Y') {

                Criteria += '  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\''
            }


            //var win = window.open('../CReports/HMSReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, 'PrintMergeInv', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintMergeInv', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


    </script>

    <script language="javascript">

        function ShowAttachment(PTID, FileName, InvoiceID) {
            var win = window.open('../../EMR/Scans/DisplayAttachments.aspx?PT_ID=' + PTID + '&FileName=' + FileName + '&InvoiceID=' + InvoiceID, 'Attach', 'menubar=no,left=100,top=80,height=400,width=500,scrollbars=1')
            win.blur();
            win.focus();
            return true;

        }

        function ShowInvTypeMessage() {



            var vInvType = document.getElementById("<%=hidInvoiceType.ClientID%>").value;


            var vdrpInvType = document.getElementById("<%=drpInvType.ClientID%>").value;


            if (vInvType == 'Credit' && vdrpInvType == 'Cash') {



                document.getElementById("<%=divInvTypeMessage.ClientID%>").style.display = 'block';
                document.getElementById("<%=lblInvTypeMessage.ClientID%>").innerHTML = 'This patient is registered as Credit. Do you want Invoice this Patient as Cash.';
            }
            if (vInvType == 'Cash' && vdrpInvType == 'Credit') {

                document.getElementById("<%=divInvTypeMessage.ClientID%>").style.display = 'block';
                document.getElementById("<%=lblInvTypeMessage.ClientID%>").innerHTML = 'This patient is registered as Cash. Do you want Invoice this Patient as Credit.';
            }

        }



        function HideInvTypeMessage() {


            document.getElementById("<%=divInvTypeMessage.ClientID%>").style.display = 'none';
            document.getElementById("<%=lblInvTypeMessage.ClientID%>").innerHTML = '';
        }




        function ShowErrorMessage(vMessage1, vMessage2, vColor) {

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

            if (vColor != '') {
                document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;
            }

            document.getElementById("divMessage").style.display = 'block';
        }


        function HideErrorMessage() {

             document.getElementById("<%=divMessage.ClientID%>").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }


        function ShowServiceAddPopup() {

            document.getElementById("divServiceAdd").style.display = 'block';


        }

        function HideServiceAddPopup() {

            document.getElementById("divServiceAdd").style.display = 'none';
            ClearDrugAddPopup();
        }


        function ShowTransAttchmentPopup() {

            document.getElementById("divTransAttchment").style.display = 'block';


        }

        function HideTransAttchmentPopup() {

            document.getElementById("divTransAttchment").style.display = 'none';
            ClearDrugAddPopup();
        }


        function ShowMailPreview() {

            document.getElementById("divMailPreview").style.display = 'block';


        }

        function HideMailPreview() {

            document.getElementById("divMailPreview").style.display = 'none';

        }


        function ShowVisitDetailsPopup() {

            document.getElementById("divVisitDetails").style.display = 'block';


        }

        function HideVisitDetailsPopup() {

            document.getElementById("divVisitDetails").style.display = 'none';

        }



        function SetBalCalculation() {

            var PaidAmount = document.getElementById("<%=TxtRcvdAmt.ClientID%>").value;
            var PTCredit = document.getElementById("<%=txtPTCredit.ClientID%>").value;
            var BillAmt4 = document.getElementById("<%=txtBillAmt4.ClientID%>").value;
            var BalAmt = 0;
            BalAmt = parseFloat(PaidAmount) + parseFloat(PTCredit) - parseFloat(BillAmt4);

            document.getElementById("<%=txtBalAmt.ClientID%>").value = BalAmt.toFixed(2);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" runat="server" id="hidMsgConfirm" />

    <input type="hidden" runat="server" id="hidTaxApplicableYear" value="2018" />

    <input type="hidden" runat="server" id="hidDrDept" />
    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" runat="server"   style="display: none; border: groove; height: 150px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>

                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: Red;letter-spacing: 1px;font-size:15px;"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="padding-top: 0px; width: 80%; height: auto; border: thin; border-color: #cccccc; border-style: groove;">
        <div style="text-align: left; float: left; vertical-align: top;">
            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            &nbsp;&nbsp;<asp:Label ID="lblBalanceMsg" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

        </div>


        <div style="text-align: Right; float: right; vertical-align: top;">
            <asp:Button  id="btnVisitDtlsShow" runat="server"   style="padding-left: 5px; padding-right: 5px; width: 70px;" Text="Visit Dtls" class="button gray small"    onclick="btnVisitDtlsShow_Click" />
            &nbsp; 
             <asp:DropDownList ID="drpSMSTemplate" runat="server" CssClass="TextBoxStyle" Width="120px" > </asp:DropDownList>
            &nbsp; 
            <asp:Button ID="btnSaveAndEmail" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small" OnClick="btnSaveAndEmail_Click" Text="Save & Email" Visible="false" />
            <asp:Button ID="btnMailPreview" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnMailPreview_Click" Text="Mail Preview" Visible="false" />

            <asp:Button ID="btnSave" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small" OnClick="btnSave_Click" Text="Save" OnClientClick="return InvSaveVal();" />
            <asp:Button ID="btnDelete" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return DeleteInvoiceVal();" />
            <asp:Button ID="btnClear" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnClear_Click" Text="Clear" />

        </div>


        <input type="hidden" runat="server" id="hidVisitID" />
        <input type="hidden" runat="server" id="hidCompanyID" />
        <input type="hidden" runat="server" id="hidCmpRefCode" />
        <input type="hidden" runat="server" id="hidInsType" />
        <input type="hidden" runat="server" id="hidIDNo" />
        <input type="hidden" runat="server" id="hidInsTrtnt" />

        <input type="hidden" runat="server" id="hidPTVisitType" />
        <input type="hidden" runat="server" id="hidUpdateCodeType" />
        <input type="hidden" runat="server" id="hidCompAgrmtType" />
        <input type="hidden" runat="server" id="hidCoInsType" />
        <input type="hidden" runat="server" id="hidReceipt" />
        <input type="hidden" runat="server" id="hidType" />

        <input type="hidden" runat="server" id="hidccText" />
        <input type="hidden" runat="server" id="hidmasterinvoicenumber" />

        <input type="hidden" runat="server" id="hidPaidAmtCheck" />

        <input type="hidden" runat="server" id="hidInvoiceType" />

        <table width="100%" border="0">
            <tr>
                <td class="lblCaption1">Invoice Type
                </td>
                <td>
                    <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="85px" onChange="ShowInvTypeMessage()">
                        <asp:ListItem Value="Cash">Cash</asp:ListItem>
                        <asp:ListItem Value="Credit" Selected="True">Credit</asp:ListItem>
                        <asp:ListItem Value="Customer">Customer</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="lblCaption1">Inv. No
                    <asp:Button ID="btnNew" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 30px;" CssClass="button gray small"
                        OnClick="btnNew_Click" Text="New" />
                </td>
                <td>
                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22PX" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                </td>

                <td class="lblCaption1">Date & Time
                </td>
                <td>
                    <asp:TextBox ID="txtInvDate" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:TextBox ID="txtInvTime" runat="server" ReadOnly="false" Width="98px" Height="22px" CssClass="TextBoxStyle" MaxLength="10"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                        Enabled="True" TargetControlID="txtInvDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>


                </td>
                <td class="lblCaption1">
                    <asp:Label ID="lblInsCard" runat="server" Style="letter-spacing: 1px;" CssClass="lblCaption1" Text="Ins.Card#"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="txtIdNo" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                    <asp:DropDownList ID="drpStatus" CssClass="TextBoxStyle" runat="server" Width="150px" Visible="false">
                        <asp:ListItem Value="Open">Open</asp:ListItem>
                        <asp:ListItem Value="Void">Void</asp:ListItem>
                        <asp:ListItem Value="Hold">Hold</asp:ListItem>
                    </asp:DropDownList>

                    <asp:TextBox ID="txtJobnumber" runat="server" CssClass="TextBoxStyle" Width="50px" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Token No
                </td>
                <td>
                    <asp:TextBox ID="txtTokenNo" runat="server" CssClass="TextBoxStyle" Width="80px"></asp:TextBox>

                </td>
                <td class="lblCaption1">File No
                    <asp:ImageButton ID="imgFlieNoReload" runat="server" ImageUrl="~/Images/Refresh.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Reload Patient Details" Height="20PX" Width="24PX" Onclick="RefereshPatientDetails" />

                </td>
                <td>
                    <asp:TextBox ID="txtFileNo" runat="server" CssClass="TextBoxStyle" Width="100px" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>

                </td>
                <td class="lblCaption1">Name
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:TextBox>

                </td>
                <td class="lblCaption1">PT Comp.
                </td>
                <td>
                    <asp:TextBox ID="txtPTComp" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>

                </td>
            </tr>

            <tr>
                <td class="lblCaption1">Comp. ID</td>
                <td>
                    <asp:TextBox ID="txtSubCompanyID" runat="server" CssClass="TextBoxStyle" Width="80px"  onblur="return CompIdSelected()" AutoPostBack="true"  OnTextChanged="txtSubCompanyID_TextChanged"></asp:TextBox>
                </td>
                <td class="lblCaption1">Ins. Name</td>
                <td style="width: 400px" colspan="3">
                    <asp:TextBox ID="txtSubCompanyName" runat="server" CssClass="TextBoxStyle" Width="225px" ></asp:TextBox>
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" Width="200px" onblur="return CompNameSelected()"  AutoPostBack="true"  OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>
                    <div id="divComp" style="visibility: hidden;"></div>
                    <asp:AutoCompleteExtender ID="AuExtCompany" runat="Server" TargetControlID="txtSubCompanyID" MinimumPrefixLength="1" ServiceMethod="GetCompany" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp"></asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AuExtCompanyName" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp"></asp:AutoCompleteExtender>
                </td>
                <td class="lblCaption1" style="width: 105px;">Policy Type
                </td>
                <td>
                    <asp:TextBox ID="txtPlanType" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Policy No</td>
                <td colspan="2" class="lblCaption1">
                    <asp:TextBox ID="txtPolicyNo" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                    Exp.
                </td>
                <td>
                    <asp:TextBox ID="txtPolicyExp" runat="server" CssClass="TextBoxStyle" Width="100px"></asp:TextBox>
                </td>
                <td class="lblCaption1" style="width: 105px;">Treat. Type
                </td>
                <td>
                    <asp:DropDownList ID="drpTreatmentType" CssClass="TextBoxStyle" runat="server" Width="200px">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtICdCode" runat="server" CssClass="TextBoxStyle" Width="50px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtICDDesc" runat="server" CssClass="TextBoxStyle" Width="150px" Visible="false"></asp:TextBox>
                </td>
                <td class="lblCaption1">Claim No 
                </td>
                <td>
                    <asp:TextBox ID="TxtVoucherNo" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Doctor 
                </td>
                <td class="auto-style28" colspan="3">
                    <div id="divDr" style="visibility: hidden;"></div>
                    <asp:TextBox ID="txtDoctorID" runat="server" CssClass="TextBoxStyle" Width="80px"      onblur="return DRIdSelected()"></asp:TextBox>
                    <asp:TextBox ID="txtDoctorName" runat="server" CssClass="TextBoxStyle" Width="230px" MaxLength="50"   onblur="return DRNameSelected()"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                    </asp:AutoCompleteExtender>

                </td>

                <td class="lblCaption1">Ref Dr.

                </td>
                <td>
                  

                    <asp:TextBox ID="txtRefDrName" runat="server" CssClass="TextBoxStyle" Width="200px"   ></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtRefDrName" MinimumPrefixLength="1" ServiceMethod="GetRefDoctorName"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </asp:AutoCompleteExtender>

                </td>
                <td class="lblCaption1">EMR ID

                </td>
                <td>
                    <asp:TextBox ID="txtEMRID" runat="server" CssClass="TextBoxStyle" Width="150px" Visible="true"></asp:TextBox>
                </td>
            </tr>  
            <tr>
                <td class="lblCaption1">Ord. Clinician 
                </td>
                <td class="auto-style28" colspan="3">

                    <div id="divOrdClin" style="visibility: hidden;"></div>
                    <asp:TextBox ID="txtOrdClinCode" runat="server" CssClass="TextBoxStyle" Width="80px"  onblur="return OrdClinIdSelected()"></asp:TextBox>
                    <asp:TextBox ID="txtOrdClinName" runat="server" CssClass="TextBoxStyle" Width="230px" MaxLength="50"   onblur="return OrdClinNameSelected()"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtOrdClinCode" MinimumPrefixLength="1" ServiceMethod="GetOrderingDoctorID"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divOrdClin">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender9" runat="Server" TargetControlID="txtOrdClinName" MinimumPrefixLength="1" ServiceMethod="GetOrderingDoctorName"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divOrdClin">
                    </asp:AutoCompleteExtender>

                </td>
                <td></td>
                <td class="lblCaption1">
                    <asp:CheckBox ID="chkOutsidePT" Text="Outside Patient" runat="server" CssClass="lblCaption1" />
                    &nbsp;
                    Visit Type :&nbsp;
                    <asp:Label ID="lblVisitType" runat="server" CssClass="lblCaption1" Text="" ForeColor="Maroon"></asp:Label>
                </td>
                <td class="lblCaption1">Remarks

                </td>
                <td>
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="TextBoxStyle" Width="150px" Visible="true"></asp:TextBox>
                    <asp:DropDownList ID="drpRemarks" CssClass="TextBoxStyle" runat="server" Width="150px" Visible="false">
                    </asp:DropDownList>
                    <asp:DropDownList ID="drpCommissionType" CssClass="TextBoxStyle" runat="server" Width="150px" Visible="false"></asp:DropDownList>

                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Payer ID

                </td>
                <td>
                    <asp:TextBox ID="txtPayerID" runat="server" CssClass="TextBoxStyle" Width="80px" Visible="true" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="lblCaption1">Reciver ID

                </td>
                <td>
                    <asp:TextBox ID="txtReciverID" runat="server" CssClass="TextBoxStyle" Width="100px" Visible="true" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="lblCaption1">Eligibility ID

                </td>
                <td>
                    <asp:TextBox ID="txtEligibilityID" runat="server" CssClass="TextBoxStyle" Width="200px" Visible="true"></asp:TextBox>
                </td>


                <td class="lblCaption1">Email ID

                </td>
                <td>
                    <asp:Label ID="lblEmailID" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Total Session  

                </td>
                <td>
                    <asp:TextBox ID="txtTotalSession" runat="server" CssClass="TextBoxStyle" Width="80px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </td>
                <td class="lblCaption1">Avail. Session  

                </td>
                <td>
                    <asp:TextBox ID="txtAvailableSession" runat="server" CssClass="TextBoxStyle" Width="80px" onkeypress="return OnlyNumeric(event);" ReadOnly="true"></asp:TextBox>
                </td>

                <td class="lblCaption1">Nurse
                </td>
                <td class="lblCaption1">
                    <asp:DropDownList ID="drpNurse" runat="server" CssClass="TextBoxStyle" Width="200px">
                    </asp:DropDownList>
                </td>
                <td class="lblCaption1">Mobile

                </td>
                <td>
                    <asp:Label ID="lblMobileNo" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div style="padding: 5px; width: 80%; border: thin; border-color: #cccccc; border-style: groove;">

        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%" Height="500px">

            <asp:TabPanel runat="server" ID="tabService" HeaderText="Services" Width="100%">
                <ContentTemplate>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnImportSO" runat="server" OnClick="btnImportSO_Click" Text=" Import Sales Order Trans. " CssClass="lblCaption1 gray" />
                                &nbsp;
                                <asp:Button ID="btnImportObservation" runat="server" OnClick="btnImportObservation_Click" Text=" Import Observation Values " CssClass="lblCaption1 gray" />
                                <asp:Button ID="btnServUpdateToEMR" runat="server" OnClick="btnServUpdateToEMR_Click" Text="Update Diag. and Services to EMR ." Visible="false" CssClass="button Orange small" OnClientClick="return window.confirm('Do you want to replace the Diagnosis and Services to the EMR ? ')" />

                            </td>
                            <td style="float: right;">
                                <input type="button" id="btnServiceAddShow" value="Add" class="orange" runat="server" title="Add New Drug" style="width: 100px; border-radius: 5px 5px 5px 5px" onclick="ShowServiceAddPopup()" />

                            </td>
                        </tr>
                    </table>


                    <div style="padding-top: 0px; width: 99%; height: 250px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvInvoiceTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                EnableModelValidation="True" Width="99%" GridLines="Both" OnRowDataBound="gvInvTran_RowDataBound">
                                                <HeaderStyle CssClass="GridHeader_Gray" />
                                                <RowStyle CssClass="GridRow" />

                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="EditgvInvTrans" runat="server" ToolTip="Edit" ImageUrl="~/Images/icon_edit.png" Height="20px" Width="20px"
                                                                OnClick="EditAuthActivity_Click" Style="cursor: pointer; border: none;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="AttachgvInvTrans" runat="server" ToolTip="Attachment" ImageUrl="~/Images/Paperclip.png" Height="15px" Width="18px"
                                                                OnClick="AttachgvInvTrans_Click" Style="cursor: pointer; border: none;" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Code">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgvServCost" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_SERVCOST") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblgvAdjDedAmt" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_ADJ_DEDUCT_AMOUNT") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblgvTranClaimPaid" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_CLAIM_AMOUNT_PAID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblgvTranClaimRej" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_CLAIM_REJECTED") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblgvTranClaimBal" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_CLAIM_BALANCE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblgvTranDenialCode" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_DENIAL_CODE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblgvTranDenialDesc" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_DENIAL_DESC") %>' Visible="false"></asp:Label>



                                                            <asp:UpdatePanel runat="server" ID="UpdatePanel6"
                                                                UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                <ContentTemplate>


                                                                    <asp:TextBox ID="txtgvServCode" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_SERV_CODE") %>' Height="22px" Enabled="false" Style="padding-left: 5px;"></asp:TextBox>


                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="txtgvServCode" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtServDesc" Width="300px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_Description") %>' Height="22px" Enabled="false" Style="padding-left: 5px;"></asp:TextBox>



                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Fee">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtFee" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Text='<%# Bind("HIT_FEE") %>' ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="D.%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDiscType" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' Visible="false"></asp:Label>

                                                            <asp:DropDownList ID="drpgvDiscType" CssClass="TextBoxStyle" runat="server" Width="50px" Height="22px" Enabled="false">
                                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                                <asp:ListItem Value="%">%</asp:ListItem>
                                                            </asp:DropDownList>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="D.Amt">
                                                        <ItemTemplate>


                                                            <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Text='<%# Bind("HIT_DISC_AMT") %>' ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Qty">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvQty" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="30px" Height="22px" Text='<%# Bind("HIT_QTY") %>' ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtHitAmount" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_AMOUNT") %>'></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ded.">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtDeduct" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Text='<%# Bind("HIT_DEDUCT") %>' ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Co-%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCoInsType" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' Visible="false"></asp:Label>

                                                            <asp:DropDownList ID="drpCoInsType" CssClass="TextBoxStyle" runat="server" Width="50px" Height="22px" Enabled="false">
                                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                                <asp:ListItem Value="%">%</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Co-Ins">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtCoInsAmt" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_CO_INS_AMT")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Co-Total">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvCoInsTotal" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Enabled="false" Text='<%# Bind("HIT_CO_TOTAL") %>'></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comp. Type">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvCompType" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Enabled="false" Text='<%# Bind("HIT_COMP_TYPE") %>'></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comp. Amount">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvCompAmount" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Enabled="false" Text='<%# Bind("HIT_COMP_AMT") %>'></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comp Total">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvCompTotal" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Enabled="false" Text='<%# Bind("HIT_COMP_TOTAL") %>'></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Haad Code">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtHaadCode" runat="server" CssClass="TextBoxStyle" Width="70px" Height="22px" Text='<%# Bind("HIT_HAAD_CODE") %>'></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Haad Desc" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtHaadDesc" runat="server" CssClass="TextBoxStyle" Width="150px" Height="22px"></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>




                                                    <asp:TemplateField HeaderText="Serv. Category">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvCatID" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_CAT_ID") %>'></asp:TextBox>
                                                            <asp:Label ID="lblgvCatID" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_CAT_ID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Serv. Treatment Type" ShowHeader="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblServTreatType" Width="100px" CssClass="TextBoxStyle" runat="server" Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Covered" ShowHeader="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCoverd" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_COVERED") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Serv. Type" ShowHeader="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblServType" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_SERV_TYPE") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RefNo." ShowHeader="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgvRefNo" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_REFNO") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>




                                                    <asp:TemplateField HeaderText="TeethNo." ShowHeader="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblgvTeethNo" Width="100px" CssClass="TextBoxStyle" runat="server" Text='<%# Bind("HIT_TEETHNO") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Dr.Code">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtgvDrCode" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" Text='<%# Bind("HIT_DR_CODE") %>' ReadOnly="true"></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Dr.Name">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvDrName" runat="server" CssClass="TextBoxStyle" Width="150px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_DR_NAME") %>'></asp:TextBox>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                      <asp:TemplateField HeaderText="Ord. Clinician">
                                                        <ItemTemplate>
                                                             <asp:TextBox ID="lblgvTranOrderDrCode"   CssClass="TextBoxStyle" Width="150px" Height="22px" ReadOnly="true" runat="server" Text='<%# Bind("HIT_CLINICIAN_CODE") %>' Visible="false"></asp:TextBox>
                                                            <asp:TextBox ID="lblgvTranOrderDrName"   CssClass="TextBoxStyle" Width="150px" Height="22px" ReadOnly="true"  runat="server" Text='<%# Bind("HIT_CLINICIAN_NAME") %>' ></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Haad Type">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTypeValue" runat="server" CssClass="TextBoxStyle" Style="text-align: center" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_TYPE_VALUE") %>'></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Cost Price" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvCostPrice" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Enabled="false" Text='<%# Bind("HIT_COSTPRICE") %>'></asp:TextBox>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Authorization ID">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtgvAuthID" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" Text='<%# Bind("HIT_AUTHORIZATIONID") %>'></asp:TextBox>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Start Date">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtgcStartDt" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" Text='<%# Bind("HIT_STARTDATEDesc") %>' ReadOnly="true"></asp:TextBox>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Observ.Type">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtObsType" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" Text='<%# Bind("HIO_TYPE") %>' ReadOnly="true"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Observ.Code">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtObsCode" runat="server" CssClass="TextBoxStyle" Width="70px" Height="22px" Text='<%# Bind("HIO_CODE") %>' ReadOnly="true"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Observ.Description">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtObsDesc" runat="server" CssClass="TextBoxStyle" Width="120px" Height="22px" Text='<%# Bind("HIO_DESCRIPTION") %>' ReadOnly="true"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Observ.Value">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtObsValue" runat="server" CssClass="TextBoxStyle" Width="70px" Height="22px" Text='<%# Bind("HIO_VALUE") %>' ReadOnly="true"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Observ.ValueType">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtObsValueType" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" Text='<%# Bind("HIO_VALUETYPE") %>' ReadOnly="true"></asp:TextBox>



                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tax Amt">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTaxAmount" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_TAXAMOUNT") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Claim Tax">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtClaimTaxAmount" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_CLAIMAMOUNTTAX") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Tax %">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTaxPercent" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_TAXPERCENT") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Attachment">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAttachment" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_ATTACHMENT") %>' Visible="false"></asp:Label>
                                                            <asp:TextBox ID="lblAttachmentFileName" CssClass="TextBoxStyle" runat="server" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" ReadOnly="true" Text='<%# Bind("HIT_ATTACHMENT_NAME") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>

                                                            <asp:ImageButton ID="DeletegvInvTrans" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                OnClick="DeletegvInvTrans_Click" Style="cursor: pointer; border: none;" />

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                        </table>
                    </div>

                    <div style="padding-top: 0px; width: 99%; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%" border="0" cellpadding="3" cellspacing="3">
                            <tr>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(1)
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtBillAmt1" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="TextBoxStyle" Width="100px" Text="0.00" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(2)
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtBillAmt2" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="TextBoxStyle" Width="100px" Text="0.00" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(3)
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtBillAmt3" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="TextBoxStyle" Width="100px" Text="0.00" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Claim Amt
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel48" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtClaimAmt" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="TextBoxStyle" Width="100px" Text="0.00" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(4)
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtBillAmt4" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="TextBoxStyle" Width="100px" Text="0.00" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" rowspan="3" style="vertical-align: top; text-align: center; border-radius: 4px; border: thin; border-width: 1px; border: groove;">
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                            Bal. Amt 
                                            <br />
                                            <asp:TextBox ID="txtBalAmt" runat="server" CssClass="lblCaption1" Style="text-align: center; font-size: 15px; color: brown;" Width="100px" Height="30px" ReadOnly="true" Font-Bold="true" Font-Size="Larger"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="width: 80px;">Hosp.Disc
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpHospDiscType" CssClass="TextBoxStyle" runat="server" Width="50px">
                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                <asp:ListItem Value="%">%</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtHospDisc" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" Width="45px" MaxLength="10" Text="0.00" AutoPostBack="true" OnTextChanged="txtHospDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:TextBox ID="txtHospDiscAmt" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" Width="45px" MaxLength="10" Text="0.00" Visible="false"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Deductible
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtDeductible" runat="server" Height="22px" Enabled="false" Style="text-align: right;" CssClass="TextBoxStyle" Width="100px" Text="0.00" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Co-Ins.Total
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtCoInsTotal" runat="server" Height="22px" Enabled="false" Style="text-align: right;" CssClass="TextBoxStyle" Text="0.00" Width="100px" MaxLength="10"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Spl.Disc
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtSplDisc" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" Width="100px" MaxLength="10" Text="0.00" AutoPostBack="true" OnTextChanged="txtSplDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">PT Credit
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTCredit" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" Width="100px" MaxLength="10" Text="0.00" AutoPostBack="true" OnTextChanged="txtSplDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="width: 90px;">Tax Amt.
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtTotalTaxAmt" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" Text="0.00" Enabled="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="width: 80px;">Claim Tax Amt.
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel46" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtTotalClaimTaxAmt" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" Text="0.00" Enabled="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td></td>
                                <td class="lblCaption1" style="width: 80px;"></td>
                                <td></td>
                                <td class="lblCaption1" style="width: 60px;"></td>

                                <td class="lblCaption1" style="width: 80px;">Paid Amt
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel47" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="TxtRcvdAmt" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" Width="100px" MaxLength="10" Text="0.00" onkeypress="return OnlyNumeric(event);" onkeyup="SetBalCalculation();"></asp:TextBox>

                                            <asp:TextBox ID="txtPaidAmount" runat="server" Height="22px" Style="text-align: right;" CssClass="TextBoxStyle" Width="100px" MaxLength="10" Text="0.00" Visible="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CheckBox ID="chkPrintCrBill" runat="server" CssClass="lblCaption1" Text="Print Credit Bill" Checked="false" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkOnlySave" runat="server" CssClass="lblCaption1" Text="Only Save" Checked="true" />

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Print Type
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpPrintType" CssClass="TextBoxStyle" runat="server" Width="100px">
                                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="Gross">Gross</asp:ListItem>
                                        <asp:ListItem Value="Net">Net</asp:ListItem>
                                        <asp:ListItem Value="Print1">Print1</asp:ListItem>
                                        <asp:ListItem Value="Print2">Print2</asp:ListItem>
                                        <asp:ListItem Value="Print3">Print3</asp:ListItem>
                                    </asp:DropDownList>


                                </td>

                                <td>

                                    <asp:Button ID="btnEditPTPopup" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 80px;" CssClass="lblCaption1 gray " Text="Edit Patient" OnClientClick="PTPopup()" />

                                </td>
                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            &nbsp;<asp:Button ID="btnPrntDeductible" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 80px;" CssClass="lblCaption1 gray " Visible="true" OnClick="btnPrntDeductible_Click" Text="Deductible" />
                                            <asp:CheckBox ID="chkNoMergePrint" runat="server" CssClass="lblCaption1" Text="No Merge Print" Checked="false" Visible="false" />&nbsp;&nbsp;
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td>
                                    <asp:UpdatePanel ID="UPprint" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 80px;" CssClass="lblCaption1 gray" Visible="true" OnClick="btnPrint_Click" Text="Print" />

                                        </ContentTemplate>


                                    </asp:UpdatePanel>

                                    </updatepanel>

                                </td>

                                <td class="lblCaption1" style="width: 70px;"></td>
                                <td></td>
                                <td></td>
                            </tr>

                        </table>
                    </div>
                    <br />
                    <div style="padding-top: 0px; width: 99%; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <asp:Label ID="Label1" runat="server" CssClass="lblCaption1" Text="Payment" Style="font-weight: bold;"></asp:Label>
                        &nbsp;
                <table style="width: 100%;">
                    <tr>
                        <td class="lblCaption1" style="width: 50px;">Pay.Type</td>
                        <td style="width: 125px;">
                            <asp:DropDownList ID="drpPayType" runat="server" CssClass="TextBoxStyle" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="drpPayType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>

                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="UpdatePanel49" runat="server">
                                <ContentTemplate>
                                    Cash Amt.&nbsp;
                                    <asp:TextBox ID="txtCashAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" Text="0.00" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                </table>
                        <div id="divCC" runat="server" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1" style="width: 50px;">Type</td>
                                    <td style="width: 125px;">
                                        <asp:DropDownList ID="drpccType" runat="server" CssClass="TextBoxStyle" Width="120px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="lblCaption1" style="width: 70px;">C.C Amt.
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel50" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtCCAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtCCAmt_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">C.C No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Holder
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCHolder" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Ref No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCRefNo" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divCheque" runat="server" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1" style="width: 50px;">Cheq. No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDcheqno" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1" style="width: 70px;">Cheq. Amt.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtChqAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtChqAmt_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Cheq. Date
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDcheqdate" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                            Enabled="True" TargetControlID="txtDcheqdate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>

                                    </td>
                                    <td class="lblCaption1">Bank
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpBank" CssClass="TextBoxStyle" runat="server" Width="250px"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divBank" runat="server" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1" style="width: 50px;">Cheq. No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1" style="width: 70px;">Cheq. Amt.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox2" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtChqAmt_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Cheq. Date
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox3" runat="server" Width="95px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender6" runat="server"
                                            Enabled="True" TargetControlID="txtDcheqdate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>

                                    </td>
                                    <td class="lblCaption1">Bank
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownList1" CssClass="TextBoxStyle" runat="server" Width="250px"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>



                    <div id="divAdvane" runat="server" visible="false">
                        <fieldset style="width: 910px;">
                            <legend class="lblCaption1">Advance Details</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1">
                                        <asp:ListBox ID="lstAdvDtls" runat="server" CssClass="TextBoxStyle" Height="50px" Width="250px"></asp:ListBox>
                                        Avail. Adv
                             <asp:TextBox ID="txtAvailAdv" runat="server" Width="100px" CssClass="TextBoxStyle" Visible="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        Advance Amt.
                             <asp:TextBox ID="txtAdvAmt" runat="server" Width="100px" CssClass="TextBoxStyle" Visible="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="divMergeInvoiceNo" runat="server" visible="false">
                        <table style="width: 300px;">

                            <tr>
                                <td class="lblCaption1">Merge Inv. No
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMergeInvoices" runat="server" Width="100px" CssClass="TextBoxStyle"></asp:TextBox>

                                </td>
                            </tr>
                        </table>

                    </div>
                    <div id="divMultiCurrency" runat="server" visible="false">
                        <fieldset style="width: 910px;">
                            <legend class="lblCaption1">Multi Currency</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1">Cur. Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpCurType" runat="server" CssClass="TextBoxStyle" Width="120px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="lblCaption1">Rcvd. Amt
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRcvdAmount" runat="server" Width="100px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </td>
                                    <td class="lblCaption1">Conv. Rate
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtConvRate" runat="server" Width="100px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtConvRate_TextChanged"></asp:TextBox>
                                    </td>

                                    <td class="lblCaption1">Cur. Value
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcurValue" runat="server" Width="100px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Bal. Amt.
                                    </td>
                                    <td>
                                        <asp:Label ID="LblMBalAmt" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:Label>
                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </div>


                    <table cellpadding="5" cellspacing="5" width="100%">
                        <tr>

                            <td valign="top" class="lblCaption1" style="font-weight: bold;">

                                <asp:GridView ID="gvAttachments" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%">
                                    <HeaderStyle CssClass="GridHeader_Gray" Height="10px" />
                                    <RowStyle CssClass="GridRow" Height="10px" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="Date" HeaderStyle-Width="110px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton17" runat="server" OnClick="SelectAttachment_Click">
                                                    <asp:Label ID="lblgvAttaEMRID" CssClass="label" runat="server" Text='<%# Bind("ESF_EMR_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvAttaPTID" CssClass="label" runat="server" Text='<%# Bind("ESF_PT_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvAttaDate" CssClass="label" runat="server" Text='<%# Bind("ESF_CREATED_DATEDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category" HeaderStyle-Width="200px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton18" runat="server" OnClick="SelectAttachment_Click">
                                                    <asp:Label ID="lblgvAttaCategory" CssClass="label" runat="server" Text='<%# Bind("ESF_CAT_ID") %>'></asp:Label>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="File Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton19" runat="server" OnClick="SelectAttachment_Click">
                                                    <asp:Label ID="lblgvAttaFileName" CssClass="label" runat="server" Text='<%# Bind("ESF_FILENAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>


                </ContentTemplate>

            </asp:TabPanel>

            <asp:TabPanel runat="server" ID="TabPanel13" HeaderText="Diagnosis" Width="100%">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td style="width: 70px; height: 30px;" class="lblCaption1">Type
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpClmType" runat="server" CssClass="TextBoxStyle" Width="300px">
                                            <asp:ListItem Value="1" Selected="True">No Bed + No Emergency Room (OP)</asp:ListItem>
                                            <asp:ListItem Value="2">No Bed + Emergency Room (OP)</asp:ListItem>
                                            <asp:ListItem Value="3">InPatient Bed + No Emergency Room (IP)</asp:ListItem>
                                            <asp:ListItem Value="4">InPatient Bed + Emergency Room (IP)</asp:ListItem>
                                            <asp:ListItem Value="5">Daycase Bed + No Emergency Room (Day Care)</asp:ListItem>
                                            <asp:ListItem Value="6">Daycase Bed + Emergency Room (Day Care)</asp:ListItem>
                                            <asp:ListItem Value="7">National Screening</asp:ListItem>
                                            <asp:ListItem Value="12">Home</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td style="height: 40px;" class="lblCaption1">Start Date  <span style="color: red;">* </span>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtClmStartDate" runat="server" CssClass="TextBoxStyle" Width="75px" Height="22PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtClmStartDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtClmStartDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        <asp:TextBox ID="txtClmFromTime" runat="server" CssClass="TextBoxStyle" Width="60px" Height="22PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">End Date  <span style="color: red;">* </span>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtClmEndDate" runat="server" CssClass="TextBoxStyle" Width="75px" Height="22PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtClmEndDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtClmEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        <asp:TextBox ID="txtClmToTime" runat="server" CssClass="TextBoxStyle" Width="60px" Height="22PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td style="height: 30px;" class="lblCaption1">Start Type
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpClmStartType" runat="server" CssClass="TextBoxStyle" Width="300px">
                                            <asp:ListItem Value="1" Selected="True">Elective</asp:ListItem>
                                            <asp:ListItem Value="2">Emergency</asp:ListItem>
                                            <asp:ListItem Value="3">Transfer</asp:ListItem>
                                            <asp:ListItem Value="4">Live Birth</asp:ListItem>
                                            <asp:ListItem Value="5">Still Birth</asp:ListItem>
                                            <asp:ListItem Value="6">Dead on Arrival</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">End Type 
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpClmEndType" runat="server" CssClass="TextBoxStyle" Width="200px">
                                            <asp:ListItem Value="1" Selected="True">Discharged with approval</asp:ListItem>
                                            <asp:ListItem Value="2">Discharged against advice</asp:ListItem>
                                            <asp:ListItem Value="3">Discharged absent without leave</asp:ListItem>
                                            <asp:ListItem Value="4">Transfer to another facility</asp:ListItem>
                                            <asp:ListItem Value="5">Deceased</asp:ListItem>

                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td class="lblCaption1" style="width: 105px;">Type <span style="color: red;">* </span>
                            </td>
                            <td class="lblCaption1" style="width: 105px;">Code <span style="color: red;">* </span>
                            </td>
                            <td class="lblCaption1">Description
                            </td>
                            <td class="lblCaption1">Year of Onset
                            </td>


                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="drpDiagType" runat="server" CssClass="TextBoxStyle" Width="100px">
                                    <asp:ListItem Value="Principal" Selected>Principal</asp:ListItem>
                                    <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                    <asp:ListItem Value="ReasonForVisit">ReasonForVisit</asp:ListItem>
                                    <asp:ListItem Value="Admitting">Admitting</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                            <td>

                                <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" onblur="return DiagIdSelected()"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiagName" runat="server" Width="400px" Height="22px" CssClass="TextBoxStyle" onblur="return DiagNameSelected()"></asp:TextBox>

                                <div id="divDiagExt" style="visibility: hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="Server" TargetControlID="txtDiagCode" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisID"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt">
                                </asp:AutoCompleteExtender>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtDiagName" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisName"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt">
                                </asp:AutoCompleteExtender>


                            </td>
                            <td>
                                <asp:DropDownList ID="drpYearOfOnset" runat="server" CssClass="label" Width="100px" BorderWidth="1px" BorderColor="#cccccc" ToolTip="Year of first time the Patient had the Diagnosis "></asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnInvDiagAdd" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 50px;" CssClass="lblCaption1 gray"
                                    OnClick="btnInvDiagAdd_Click" Text="Add" OnClientClick="return DiagAddVal();" />

                            </td>
                        </tr>

                    </table>
                    <div style="padding-top: 0px; width: 99%; height: 300px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvInvoiceClaims" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="100%" GridLines="Both">
                                        <HeaderStyle CssClass="GridHeader_Gray" />
                                        <RowStyle CssClass="GridRow" BorderStyle="Solid" BorderWidth="1px" />

                                        <Columns>
                                            <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblICDCode" Width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_CODE") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblICDDesc" Width="400px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_DESC") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblICDType" Width="70px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_TYPE") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Year of Onset">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblYearofOnset" Width="70px" CssClass="label" runat="server" Text='<%# Bind("HIC_YEAR_OF_ONSET_P") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                        OnClick="DeletegvDiag_Click" />&nbsp;&nbsp;
                                                
                                                </ItemTemplate>
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>





                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>

                    </div>
                </ContentTemplate>
            </asp:TabPanel>


        </asp:TabContainer>
        <asp:LinkButton ID="lnkVisitMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
            ForeColor="Blue"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModPopExtVisits" runat="server" TargetControlID="lnkVisitMessage"
            PopupControlID="pnlVisit" BackgroundCssClass="modalBackground" CancelControlID="btnVisitClose"
            PopupDragHandleControlID="pnlVisit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlVisit" runat="server" Height="250px" Width="680px" CssClass="modalPopup">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnVisitClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="height: 205px;">


                        <div style="padding-top: 0px; width: 650px; height: 200px; overflow: auto; border-color: #e3f7ef; border-style: groove;">

                            <asp:GridView ID="gvVisit" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="99%" PageSize="50">
                                <HeaderStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <Columns>


                                    <asp:TemplateField HeaderText="File No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SelectVisit_Click">
                                                <asp:Label ID="lblVisitID" runat="server" Text='<%# Bind("HPV_SEQNO") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblVisitEMR_ID" runat="server" Text='<%# Bind("HPV_EMR_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblEligibilityID" runat="server" Text='<%# Bind("HPV_ELIGIBILITY_ID") %>' Visible="false"></asp:Label>
                                              

                                                <asp:Label ID="lblPTId" runat="server" Text='<%# Bind("HPV_PT_ID") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="SelectVisit_Click">
                                                <asp:Label ID="lblPTName" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dr.Coe">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="SelectVisit_Click">
                                                <asp:Label ID="lblgvhpvRefDrId" runat="server" Text='<%# Bind("HPV_REFERRAL_TO") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDrID" runat="server" Text='<%# Bind("HPV_DR_ID") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doctor Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="SelectVisit_Click">
                                                <asp:Label ID="lblDRName" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VISIT TYPE" SortExpression="HPV_VISIT_TYPE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblgvVisitVisitType" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_VISIT_TYPE") %>'></asp:Label>

                                    </ItemTemplate>
                                   </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Patient TYPE" SortExpression="HPV_PT_TYPE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                           <asp:Label ID="lblgvVisitPTType" runat="server" Text='<%# Bind("HPV_PT_TYPE") %>' ></asp:Label>

                                    </ItemTemplate>
                                   </asp:TemplateField>

                                </Columns>

                            </asp:GridView>


                        </div>


                    </td>
                </tr>

            </table>
        </asp:Panel>

        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1" style="width: 30%;">PriorAuthorization ID 
                  <asp:Button ID="btnAuthorIDGet" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="lblCaption1 gray"
                      OnClick="btnAuthorIDGet_Click" Text="Get Author. ID " />
                </td>
                <td style="text-align: left; width: 20%;"></td>
                <td class="lblCaption1" style="width: 50%;">Chief Complaint  </td>

            </tr>
            <tr>
                <td style="vertical-align: top;">
                    <asp:TextBox ID="txtPriorAuthorizationID" runat="server" Width="210px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>

                </td>
                <td style="vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel51" runat="server">
                        <ContentTemplate>
                            <asp:CheckBox ID="ChkTopupCard" runat="server" CssClass="lblCaption1" Text="Topup Card" Checked="false" Enabled="true" />
                            <asp:TextBox ID="TxtTopupAmt" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" Text="0.00" Enabled="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="vertical-align: top;">

                    <asp:TextBox ID="txtChiefComplaints" runat="server" TextMode="MultiLine" Height="50px" Width="100%" CssClass="TextBoxStyle"></asp:TextBox>

                </td>
            </tr>
            <tr>
                <td class="lblCaption1" colspan="3">
                    <asp:CheckBox ID="chkReadyforEclaim" runat="server" CssClass="lblCaption1" Text="Ready for Eclaim" Checked="true" Visible="false" />
                </td>

            </tr>
        </table>


        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Attachment 
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="Panel1" runat="server" Width="300px">
                                <asp:FileUpload ID="filAttachment" runat="server" Width="300px" CssClass="ButtonStyle" />
                            </asp:Panel>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:Button ID="btnAttachAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 120px;" CssClass="lblCaption1 gray"
                        OnClick="btnAttachAdd_Click" Text="Add Attachment" />
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;" class="lblCaption1">File Name
                </td>
                <td colspan="4">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <asp:Label ID="lblFileNameActual" runat="server" CssClass="lblCaption1"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                                 <asp:ImageButton ID="DeleteAttach" runat="server" ToolTip="Delete" Visible="false" ImageUrl="~/Images/icon_delete.png"
                                     OnClick="DeleteAttach_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
            </tr>
        </table>


        <table style="width: 100%; background-color: #D64535; color: #fff;">
            <tr>
                <td class="lblCaption1" style="color: #fff; width: 500px;">Last Invoice No - Date:
                            &nbsp;
                                    <asp:Label ID="lblLstInvNoDate" runat="server" Style="color: #fff;" CssClass="lblCaption1"></asp:Label>
                    &nbsp;
                           First Invoice Date:
                                   <asp:Label ID="lblFstInvDate" runat="server" Style="color: #fff;" CssClass="lblCaption1"></asp:Label>
                </td>

                <td class="lblCaption1" style="color: #fff;">Last Modified User:
                            &nbsp;
                                    <asp:Label ID="lblModifiedUser" runat="server" Style="color: #fff;" CssClass="lblCaption1"></asp:Label>
                </td>

                <td class="lblCaption1" style="color: #fff;">Created User :
                            &nbsp;
                                    <asp:Label ID="lblCreatedUser" runat="server" Style="color: #fff;" CssClass="lblCaption1"></asp:Label>
                </td>



            </tr>

        </table>




    </div>



    <asp:LinkButton ID="lnkMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
        ForeColor="Blue"></asp:LinkButton>
    <asp:ModalPopupExtender ID="MPExtMessage" runat="server" TargetControlID="lnkMessage"
        PopupControlID="pnlPrevPrice" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
        PopupDragHandleControlID="pnlPrevPrice">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlPrevPrice" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
        <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
            <tr>
                <td align="right">
                    <asp:Button ID="btnClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                </td>
            </tr>
            <tr>
                <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                    <%=strMessage %>
                </td>
            </tr>
        </table>
    </asp:Panel>



    <div style="padding-left: 60%; width: 100%;">

        <div id="divInvTypeMessage" runat="server" style="display: none; border: groove; height: 120px; width: 550px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 400px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top; width: 50%;"></td>
                    <td align="right" style="vertical-align: top; width: 50%;">

                        <input type="button" id="Button2" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideInvTypeMessage()" />
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%">

                <tr>
                    <td>
                        <asp:Label ID="lblInvTypeMessage" runat="server" CssClass="lblCaption" Style="font-weight: bold; color: red;"></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td align="center" valign="bottom" style="height: 50px;">
                        <asp:Button ID="btnInvTypeYes" runat="server" Text=" Yes " CssClass="button blue small" Style="width: 70px;" OnClick="btnInvTypeYes_Click" />
                        <asp:Button ID="btnInvTypeNo" runat="server" Text=" No " CssClass="button blue small" Style="width: 70px;" OnClick="btnInvTypeNo_Click" />

                    </td>
                </tr>
            </table>


            <br />

        </div>
    </div>



    <div id="divServiceAdd" style="display: none; overflow: hidden; border: groove; height: 700px; width: 900px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 170px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideServiceAddPopup()" />
                </td>
            </tr>
        </table>
        <table style="width: 100%;" cellpadding="5" cellspacing="5" border="0">
            <tr>
                <td class="lblCaption1" style="width: 100px;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            Service  
                            <asp:ImageButton ID="imgGetServDtls" runat="server" Style="width: 20px; height: 20px;" ImageUrl="~/Images/Refresh.png" ToolTip="Refresh" OnClick="imgGetServDtls_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" colspan="5">
                    <input type="hidden" id="hidHaadID" runat="server" />
                    <input type="hidden" id="hidCatID" runat="server" />
                    <input type="hidden" id="hidCatType" runat="server" />

                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServCode" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" onblur="ServIdSelected()" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" ondblclick="return ServicePopup('txtServCode',this.value);"></asp:TextBox>

                            <%--<asp:TextBox ID="txtServName" runat="server" Width="350px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()" AutoPostBack="true" OnTextChanged="txtServName_TextChanged" ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>--%>
                            <asp:TextBox ID="txtServName" runat="server" Width="350px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()" AutoPostBack="true"  ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>
                            &nbsp;
                            <asp:TextBox ID="txtServType" runat="server"   CssClass="lblCaption1"  Width="50px"  ForeColor="Brown" Text="" BorderStyle="None" ></asp:TextBox>


                            <div id="divwidth" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1">Autho. ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServAuthID" runat="server" CssClass="TextBoxStyle" Width="150px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Unit Price
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServPrice" runat="server" Width="100px" Height="22PX" CssClass="TextBoxStyle" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Text="0.00" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtServPrice_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 100px;">Disc.
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpServDiscType" CssClass="TextBoxStyle" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="50px" Height="22px" AutoPostBack="true" OnSelectedIndexChanged="drpServDiscType_SelectedIndexChanged">
                                <asp:ListItem Value="$">$</asp:ListItem>
                                <asp:ListItem Value="%">%</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtServDiscAmt" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Width="50px" Height="22px" Text="0.00" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtServDiscAmt_TextChanged"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Qty
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServQty" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Text="1" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtServQty_TextChanged"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Amount
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServAmount" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Text="0.00" ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Dedect.
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServDedect" runat="server" Width="100px" Height="22PX" CssClass="TextBoxStyle" MaxLength="10" BorderWidth="1px" Style="text-align: right; padding-right: 5px;" BorderColor="#cccccc" Text="0.00" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtServDedect_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 100px;">Co. Ins %
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpServCoInsPres" CssClass="TextBoxStyle" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="50px" Height="22px" AutoPostBack="true" OnSelectedIndexChanged="drpServCoInsPres_SelectedIndexChanged">
                                <asp:ListItem Value="$">$</asp:ListItem>
                                <asp:ListItem Value="%">%</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtServCoInsAmount" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Width="50px" Height="22px" Text="0.00" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtServCoInsAmount_TextChanged"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Tax Amount
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServTaxAmount" runat="server" Width="70px" Height="22PX" CssClass="TextBoxStyle" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Text="0.00" ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:TextBox ID="txtServTaxPresent" runat="server" Width="50px" Height="22PX" CssClass="TextBoxStyle" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Text="0.00" ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            %
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Claim Tax 
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServClaimTaxAmount" runat="server" Width="70px" Height="22PX" CssClass="TextBoxStyle" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" Style="text-align: right; padding-right: 5px;" Text="0.00" ReadOnly="true" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Haad Code
                </td>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServHaadCode" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                            <asp:TextBox ID="txtServHaadDesc" runat="server" CssClass="TextBoxStyle" Width="400px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                            <asp:TextBox ID="txtServCostPrice" runat="server" CssClass="TextBoxStyle" Style="text-align: right; padding-right: 5px;" Width="70px" Height="22px" Enabled="false" Visible="false"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Start Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServStartDt" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender5" runat="server"
                                Enabled="True" TargetControlID="txtServStartDt" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtServStartDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            <asp:TextBox ID="txtServStartTime" runat="server" CssClass="TextBoxStyle" Width="60px" Height="22PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Doctor
                </td>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                        <ContentTemplate>
                            <div id="divServDR" style="visibility: hidden;"></div>

                            <asp:TextBox ID="txtServDrCode" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServDRIdSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtServDrName" runat="server" CssClass="TextBoxStyle" Width="400px" Height="22px" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServDRNameSelected()"></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender10" runat="Server" TargetControlID="txtServDrCode" MinimumPrefixLength="1" ServiceMethod="GetDoctorNurseId"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divServDR">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender11" runat="Server" TargetControlID="txtServDrName" MinimumPrefixLength="1" ServiceMethod="GetDoctorNurseName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divServDR">
                            </asp:AutoCompleteExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

            </tr>
              <tr>
                <td class="lblCaption1">Ord. Clinician
                </td>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <div id="divServOrdDR" style="visibility: hidden;"></div>

                            <asp:TextBox ID="txtServOrdClinCode" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServOrdClinCodeSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtServOrdClinName" runat="server" CssClass="TextBoxStyle" Width="400px" Height="22px" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServOrdClinNameSelected()"></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender12" runat="Server" TargetControlID="txtServOrdClinCode" MinimumPrefixLength="1" ServiceMethod="GetDoctorNurseId"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divServOrdDR">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender13" runat="Server" TargetControlID="txtServOrdClinName" MinimumPrefixLength="1" ServiceMethod="GetDoctorNurseName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divServOrdDR">
                            </asp:AutoCompleteExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Obs.  Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServObsType" runat="server" CssClass="TextBoxStyle" Width="100px" Height="22px" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Obs.  Code
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServObsCode" runat="server" CssClass="TextBoxStyle" Width="70px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Obs.  Desc.
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServObsDesc" runat="server" CssClass="TextBoxStyle" Width="120px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
              
            </tr>
            <tr>
                  <td class="lblCaption1">Obs.   Value
                </td>
                <td colspan="7">
                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServObsValue" runat="server" CssClass="TextBoxStyle" Width="520px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                            <asp:TextBox ID="txtServObsValueType" runat="server" CssClass="TextBoxStyle" Width="70px" Height="22px" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnServiceAdd" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 100px;" CssClass="orange"
                                OnClick="btnServiceAdd_Click" Text="Save & Add" OnClientClick="return ServiceAddVal();" />
                            <asp:Button ID="btnServiceAddClose" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 100px;" CssClass="orange"
                                OnClick="btnServiceAddClose_Click" Text="Save & Close" OnClientClick="return ServiceAddVal();" />
                            <asp:Button ID="btnServiceClear" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 100px; font-weight: normal;" CssClass="gray"
                                OnClick="btnServiceClear_Click" Text="Clear" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

    </div>


    <div id="divTransAttchment" style="display: none; overflow: hidden; border: groove; height: 200px; width: 700px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 300px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button4" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideTransAttchmentPopup()" />
                </td>
            </tr>
        </table>

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="lblCaption1">Code  
                </td>
                <td class="lblCaption1" colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>

                            <asp:TextBox ID="txtTransAttachServCode" runat="server" Width="100px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Attachment 
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="Panel2" runat="server" Width="300px">
                                <asp:FileUpload ID="TransFilAttachment" runat="server" Width="300px" CssClass="ButtonStyle" />
                            </asp:Panel>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td>

                    <asp:Button ID="btnTransAttachAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 120px;" CssClass="button gray small"
                        OnClick="btnTransAttachAdd_Click" Text="Add Attachment" />
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;" class="lblCaption1">File Name
                </td>
                <td colspan="4">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblTransFileNameActual" runat="server" CssClass="lblCaption1"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                                 <asp:ImageButton ID="DeleteTransAttach" runat="server" ToolTip="Delete" Visible="false" ImageUrl="~/Images/icon_delete.png"
                                     OnClick="DeleteTransAttach_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
            </tr>
        </table>
    </div>

    <div id="divMailPreview" style="display: none; overflow: hidden; border: groove; height: 600px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 200px; top: 100px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button5" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideMailPreview()" />
                </td>
            </tr>
        </table>
        <span class="lblCaption1"><%=MailContent %></span>

    </div>


    <div id="divVisitDetails" style="display: none; overflow: hidden; border: groove; height: 500px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 220px; top: 140px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;">
                  <span class="lblCaption1" style="font-weight:bold;"> Visit Details </span> 

                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button6" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;font-size:15px;" value=" X " onclick="HideVisitDetailsPopup()" />
                </td>
            </tr>

        </table>
          <div style="height:5px;border-bottom-color:red;border-width:1px;border-bottom-style:groove;"></div>
               <div style="height:5px;"></div>
        <table style="width: 100%">
            <tr>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel52" runat="server">
                        <ContentTemplate>
                            File No:&nbsp;
                            <asp:TextBox ID="txtSrcVisitFileNo" CssClass="TextBoxStyle" runat="server" Height="22px" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            Invoice Status:&nbsp;
                            <asp:DropDownList ID="drpSrcVisitInvoiceStatus" runat="server" CssClass="TextBoxStyle" Width="100px">
                                <asp:ListItem Text="--- All ---" Value=""></asp:ListItem>
                                <asp:ListItem Text="Invoiced" Value="Invoiced"></asp:ListItem>
                                <asp:ListItem Text="Not Invoiced" Value="NotInvoiced" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


                <td style="text-align:right;">
                    <asp:UpdatePanel ID="UpdatePanel54" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnVisitDtlsSrc" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button red small"
                                OnClick="btnVisitDtlsSrc_Click" Text="Search" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>


        <div style="padding-top: 0px; width: 100%; height: 420px; overflow: auto; border-color: #02a0e0; border-style: groove; border-width: 1px;">
            <asp:UpdatePanel ID="UpdatePanel55" runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gvVisitDtlsAll" runat="server"   AutoGenerateColumns="False" BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True" Width="100%"  CellPadding="2" CellSpacing="0" >
                         <HeaderStyle CssClass="GridHeader_Gray" />
                         <RowStyle CssClass="GridRow" />
                        
                        <Columns>
                            <asp:TemplateField HeaderText="ACTION" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:UpdatePanel runat="server" ID="UpdatePanel6"
                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                        <ContentTemplate>


                                            <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="SelectVisitDtlsAll_Click" />&nbsp;&nbsp;

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="imgSelect" />
                                        </Triggers>
                                    </asp:UpdatePanel>



                                </ItemTemplate>


                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Date"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:Label ID="lblgvVisitDtlsAllID" runat="server" Text='<%# Bind("HPV_SEQNO") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllEMR_ID" runat="server" Text='<%# Bind("HPV_EMR_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllEligibilityID" runat="server" Text='<%# Bind("HPV_ELIGIBILITY_ID") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="lblgvVisitDtlsAllPolicyType" runat="server" Text='<%# Bind("HPV_POLICY_TYPE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllIDNo" runat="server" Text='<%# Bind("HPV_ID_NO") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllPolicyNo" runat="server" Text='<%# Bind("HPV_POLICY_NO") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllPolicyExp" runat="server" Text='<%# Bind("HPV_POLICY_EXP") %>' Visible="false"></asp:Label>
                                      <asp:Label ID="lblgvVisitDtlsAllPolicyExpDesc" runat="server" Text='<%# Bind("HPV_POLICY_EXPDesc") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="lblgvVisitDtlsAllDate" runat="server" Text='<%# Bind("HPV_DATEDesc") %>'></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllTime" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateTimeDesc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="File No"  HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                               <%--<ItemTemplate>
                                   <asp:Image ID="imgPlanRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Plan Type Refresh" OnClick="imgPlanRefresh_Click" />
                                   <asp:ImageButton ID="btnPTVisitRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnPTVisitRef_Click" />
                               </ItemTemplate>--%>
                                <ItemTemplate>
                                    <asp:Image ID="FileNoRefresh" runat="server" ImageUrl="~/Images/Reload.png" />
                                </ItemTemplate>
                                <ItemTemplate>
                                     <%--<asp:ImageButton ID="imgFlieNoReload" runat="server" ImageUrl="~/Images/Reload.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Reload Patient Details" Height="15PX" Width="20PX"  />--%>
                                    <asp:Label ID="lblgvVisitDtlsAllPTId" runat="server" Text='<%# Bind("HPV_PT_ID") %>'></asp:Label>                                    
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvVisitDtlsAllPTName" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Doctor Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                      <asp:Label ID="lblgvVisitDtlsAllRefDrId" runat="server" Text='<%# Bind("HPV_REFERRAL_TO") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllDrID" runat="server" Text='<%# Bind("HPV_DR_ID") %>'></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllDRName" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Vitit Type" SortExpression="HPV_VISIT_TYPE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblgvVisitDtlsAllVisitType" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_VISIT_TYPE") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                             <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                                    <ItemTemplate>
                                      <asp:Label ID="lblgvVisitDtlsAllCompID" runat="server" Text='<%# Bind("HPV_COMP_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisitDtlsAllCompName" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'  ></asp:Label>

                                         

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PT Type" SortExpression="HPV_PT_TYPEDesc">
                                    <ItemTemplate>

                                        <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>


    </div>
    <br />
    <br />
    <br />

</asp:Content>


