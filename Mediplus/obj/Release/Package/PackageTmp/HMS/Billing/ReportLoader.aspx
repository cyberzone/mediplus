﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master"
    AutoEventWireup="true" CodeBehind="ReportLoader.aspx.cs" Inherits="Mediplus.HMS.Billing.ReportLoader" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>


    <script src="../../Scripts/Validation.js" type="text/javascript"></script>


    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script src="ReportScript.js"></script>


    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlightvisit
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }

        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }



        function PatientPopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;

            var MenuName = document.getElementById("<%=hidPageName.ClientID%>").value;

            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=ReportLoader&MenuName=" + MenuName + "&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
                win.focus();
            }
            return true;

        }



        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');


                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];


                }
            }

            return true;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                 var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                 var Data1 = Data.split('~');

                 if (Data1.length > 1) {
                     document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
            }
        }

        return true;
    }


    function ServIdSelected() {
        if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
            var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
            var Data1 = Data.split('~');
            if (Data1.length > 1) {
                document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                      document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                  }
              }
              return true;
          }


          function ServNameSelected() {
              if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the From Date";
                 document.getElementById('<%=txtFromDate.ClientID%>').focus;
                 return false;

             }
             if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtFromDate.ClientID%>').focus()
                return false;
            }


            if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the To Date";
                document.getElementById('<%=txtToDate.ClientID%>').focus;
                return false;

            }

            if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtToDate.ClientID%>').focus()
                return false;
            }



            if (document.getElementById('<%=drpReport.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Report";
                document.getElementById('<%=drpReport.ClientID%>').focus;
                return false;

            }




            return true;
        }





        function Ledger() {
            var Report = "";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            Report = "Ledger.rpt";



            var Criteria = " 1=1 ";




            Criteria += ' AND  {AC_VIEW_LEDGER.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_LEDGER.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }


        function PAndLAccount() {
            var Report = "";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            Report = "ProfitAndLossAccount.rpt";



            var Criteria = " 1=1 ";




            // Criteria += ' AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function BalanceSheet() {
            var Report = "";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            Report = "BalanceSheet.rpt";



            var Criteria = " 1=1 ";




            // Criteria += ' AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }
        function TrialBalance() {
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            //  Report = "TrialBalance.rpt";



            var Criteria = " 1=1 ";




            //Criteria += ' AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}>=date(\'' + Date1 + '\') AND  {AC_VIEW_PROFILTLOSS_ACCOUNT.TransDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }
        function ShowEClaimReport(ShowReport) {
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
        var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

        var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
        var arrFromDate = dtFrom.split('/');
        var Date1;
        if (arrFromDate.length > 1) {

            Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
        }


        var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
        var arrToDate = dtTo.split('/');
        var Date2;
        if (arrToDate.length > 1) {

            Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
        }




        var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


        //var Criteria = " 1=1 ";

        //if (CompanyCode != "") {

        //    Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
        //}

        //if (DrId != "") {

        //    Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
        //}


        //Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'



        var Criteria = " 1=1 ";

        if (CompanyCode != "") {

            Criteria += ' AND {HMS_ECLAIM_XML_DATA_MASTER.BillToCode}=\'' + CompanyCode + '\'';
        }

        if (DrId != "") {

            Criteria += ' AND {HMS_ECLAIM_XML_DATA_MASTER.DoctorCode}=\'' + DrId + '\'';
        }


        Criteria += ' AND  {HMS_ECLAIM_XML_DATA_MASTER.AuthRequestDate}>=date(\'' + Date1 + '\') AND  {HMS_ECLAIM_XML_DATA_MASTER.AuthRequestDate}<=date(\'' + Date2 + '\')'

        var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
        var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

        if (ShowReport == 'true') {
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        }
        else {
            //exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
            exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');


        }


    }


    function ShowRemittance(ShowReport) {

        //  var Report = "";
        //  Report = "rptRemittance.rpt";
        var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
        var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";
        if (Val() == false) {
            return false;
        }

        var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


        var Criteria = " 1=1 ";

        if (CompanyCode != "") {

            Criteria += ' AND {HMS_REMITTANCE_XML_DATA.SenderID}=\'' + CompanyCode + '\'';
        }

        if (DrId != "") {

            Criteria += ' AND {HMS_REMITTANCE_XML_DATA.Clinician}=\'' + DrId + '\'';
        }


        Criteria += ' AND  date({HMS_REMITTANCE_XML_DATA.Start1})>=date(\'' + Date1 + '\') AND  date({HMS_REMITTANCE_XML_DATA.Start1})<=date(\'' + Date2 + '\')'
        document.getElementById('<%=hidRptFormula.ClientID%>').value = Criteria;

            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
        var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

        if (ShowReport == 'true') {
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        }
        else {
            // exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
            exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

        }

    }

    function ShowResubmission(ShowReport) {

        //  var Report = "";
        //  Report = "Resubmission.rpt";
        var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";
            if (Val() == false) {
                return false;
            }

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {HMS_RESUBMISSION_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_RESUBMISSION_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
            }


            Criteria += ' AND  {HMS_RESUBMISSION_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_RESUBMISSION_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                // exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');


            }

        }

        function ShowDeptWiseIncome(ShowReport) {

            //   var Report = "";
            //   Report = "HmsStaffwiseIncome.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            if (Val() == false) {
                return false;
            }

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {HMS_STAFF_WISE_INCOME.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_STAFF_WISE_INCOME.HIM_DR_CODE}=\'' + DrId + '\'';
            }



            Criteria += ' AND  {HMS_STAFF_WISE_INCOME.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_STAFF_WISE_INCOME.HIM_DATE}<=date(\'' + Date2 + '\')'

            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                //exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

            }
            //  var win = window.open('../CReports/HMSReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        }

        function ShowDailySummary(ShowReport) {
            //  var Report = "";
            //  Report = "HmsDailySummary.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
            }

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'


            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                //exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

            }

        }

        function ShowServwiseIncome(ShowReport) {
            //  var Report = "";
            //  Report = "HmsServwiseIncome.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
            }


            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                //  exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

            }


        }
        function ShowServwiseIncome(ShowReport) {
            //  var Report = "";
            //  Report = "HmsServwiseIncome.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
            }


            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                // exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');


            }

        }


        function ShowAppointment(ShowReport) {
            //  var Report = "";
            //  Report = "NatAgeGenderDiagStatistics.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value
            var Room = document.getElementById('<%=drpRoom.ClientID%>').value
            var FileNo = document.getElementById('<%=txtFileNo.ClientID%>').value

            var MobileNo = document.getElementById('<%=txtMobile.ClientID%>').value

            var AppStatus = document.getElementById('<%=drpAppStatus.ClientID%>').value

            var Criteria = " 1=1  "; //AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS} <> '3'  //AND {HMS_APPOINTMENT_MASTER.HAM_APNT_TYPE}='Consultation' 

            if (DrId != "") {

                Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_DR_CODE}=\'' + DrId + '\'';
            }
            if (Room != "") {

                Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_ROOM}=\'' + Room + '\'';
            }

            if (FileNo != "") {

                Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_FILENUMBER}=\'' + FileNo + '\'';
            }

            if (MobileNo != "") {

                Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_MOBILENO}=\'' + MobileNo + '\'';
            }

            if (AppStatus != "") {

                Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS}=\'' + AppStatus + '\'';
            }

            Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS} <> \'2\'';

            Criteria += ' AND  {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date1 + '\') AND  {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}<=date(\'' + Date2 + '\')'


            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                // exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

            }


        }

        function ShowEMRReports(ShowReport) {
            //  var Report = "";
            //  Report = "NatAgeGenderDiagStatistics.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value







            var Criteria = " 1=1  "; //AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS} <> '3'  //AND {HMS_APPOINTMENT_MASTER.HAM_APNT_TYPE}='Consultation' 

            if (DrId != "") {

                Criteria += ' AND {EMR_PT_MASTER.EPM_DR_CODE}=\'' + DrId + '\'';
            }







            Criteria += ' AND  {EMR_PT_MASTER.EPM_DATE}>=date(\'' + Date1 + '\') AND  {EMR_PT_MASTER.EPM_DATE}<=date(\'' + Date2 + '\')'


            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
             var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                //exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

            }


        }








        function ShowCompReceipts(ShowReport) {
            //   var Report = "";
            //   Report = "HmsCompReceipts.rpt";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";
            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value



            var Criteria = " 1=1 ";
            if (CompanyCode != "") {

                Criteria += ' AND {HMS_RECEIPT_MASTER.HRM_COMP_ID}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_RECEIPT_MASTER.HRM_DR_ID}=\'' + DrId + '\'';
            }

            Criteria += ' AND  {HMS_RECEIPT_MASTER.HRM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_RECEIPT_MASTER.HRM_DATE}<=date(\'' + Date2 + '\')'


            var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
            var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

            if (ShowReport == 'true') {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            }
            else {
                //  exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

            }


        }

        function ShowCompanyReceiptsDtls() {
            // var Report = "";
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value



            //   Report = "HmsCompReceiptDtlsWeb.rpt";



            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
            }



            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }
    </script>



    <script type="text/javascript">

        $(document).ready(function () {
            PageStart();
        });





        function ClaimTransferDtls() {
            // var Report = "";
            //  Report = "ClaimsTransferDetailed.rpt";
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";

            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {ECLAIM_XML_DATA_MASTER.BillToCode}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {ECLAIM_XML_DATA_MASTER.DoctorCode}=\'' + DrId + '\'';
            }


            Criteria += ' AND  {ECLAIM_XML_DATA_MASTER.AuthRequestDate}>=date(\'' + Date1 + '\') AND  {ECLAIM_XML_DATA_MASTER.AuthRequestDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/AccuReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function ClaimTransferSummary() {
            //   var Report = "";
            // Report = "ClaimsTransferSummary.rpt";
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";
            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
            var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


            var Criteria = " 1=1 ";

            if (CompanyCode != "") {

                Criteria += ' AND {ECLAIM_XML_DATA_MASTER.BillToCode}=\'' + CompanyCode + '\'';
            }

            if (DrId != "") {

                Criteria += ' AND {ECLAIM_XML_DATA_MASTER.DoctorCode}=\'' + DrId + '\'';
            }


            Criteria += ' AND  {ECLAIM_XML_DATA_MASTER.AuthRequestDate}>=date(\'' + Date1 + '\') AND  {ECLAIM_XML_DATA_MASTER.AuthRequestDate}<=date(\'' + Date2 + '\')'


            var win = window.open('../../CReports/AccuReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }



        function ShowReportFromDrp(ShowReport) {

            if (Val() == false) {
                return false;
            }

            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'
            var Report = document.getElementById('<%=drpReport.ClientID%>').value + ".rpt";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }





            var PageName;
            PageName = '<%=Convert.ToString(Request.QueryString["PageName"]) %>'

            var Criteria = " 1=1 ";
            if (PageName == "HMS_RPT_ECLAIM") {
                ShowEClaimReport(ShowReport)
            }
            else if (PageName == 'HMS_RPT_REMIT') {
                ShowRemittance(ShowReport);
            }
            else if (PageName == 'HMS_RPT_RESUB') {
                ShowResubmission(ShowReport);
            }
            else if (PageName == 'DrWiseIncome') {
                ShowDeptWiseIncome(ShowReport);
            }

            else if (PageName == 'DailySummary') {
                ShowDailySummary(ShowReport);
            }

            else if (PageName == 'CompReceipts') {
                ShowCompReceipts(ShowReport);
            }

            else if (PageName == 'HMS_RPT_STATISTICS') {
                //  ShowStatistics();
                var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
        var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value
        var vServCode = document.getElementById('<%=txtServCode.ClientID%>').value
        var vInvType = document.getElementById('<%=drpInvType.ClientID%>').value


        var vYear = document.getElementById('<%=drpYear.ClientID%>').value
        var vQuater = document.getElementById('<%=drpQuater.ClientID%>').value

        if (CompanyCode != "") {

            Criteria += ' AND {HMS_PATIENT_VISIT.HPV_COMP_ID}=\'' + CompanyCode + '\'';
        }

        if (DrId != "") {

            Criteria += ' AND {HMS_PATIENT_VISIT.HPV_DR_ID}=\'' + DrId + '\'';
        }



        if (vInvType != "") {
            Criteria += ' AND {HMS_PATIENT_VISIT.HPV_PT_TYPE}=\'' + vInvType + '\'';
        }

        if (vYear != "" && vQuater != "") {
            if (vQuater == "1") {
                Date1 = vYear + "-01-01";
                Date2 = vYear + "-03-31";
            }

            if (vQuater == "2") {
                Date1 = vYear + "-04-01";
                Date2 = vYear + "-06-30";
            }
            if (vQuater == "3") {
                Date1 = vYear + "-07-01";
                Date2 = vYear + "-09-30";
            }
            if (vQuater == "4") {
                Date1 = vYear + "-10-01";
                Date2 = vYear + "-12-31";
            }
        }


        Criteria += ' AND  {HMS_PATIENT_VISIT.HPV_DATE}>=date(\'' + Date1 + '\') AND  {HMS_PATIENT_VISIT.HPV_DATE}<=date(\'' + Date2 + '\')'

        var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
                    var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

        if (ShowReport == 'true') {
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        }
        else {
            // exec('C:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
            exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');


        }
    }

    else if (PageName == 'ClaimTransfer') {
        ClaimTransferDtls();
    }
    else if (PageName == "ServwiseIncome") {
        ShowServwiseIncome(ShowReport)

    }
    else if (PageName == "HMS_APPOINTMENT") {

        ShowAppointment(ShowReport)
    }
    else if (PageName == "EMR_REPORTS") {

        ShowEMRReports(ShowReport)
    }

    else if (PageName == 'HMS_RPT_BILLING') {

        var FileNo = document.getElementById('<%=txtFileNo.ClientID%>').value
                    var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
                    var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value
                    var vServCode = document.getElementById('<%=txtServCode.ClientID%>').value
                    var vInvType = document.getElementById('<%=drpInvType.ClientID%>').value
                    var InvoiceNos = document.getElementById('<%=txtInvoiceNo.ClientID%>').value

                    var IsGraterthanZero = document.getElementById('<%=chkGreaterThanZero.ClientID%>').checked

                    var ReType = document.getElementById('<%=drpReType.ClientID%>').value

                    var UserID = document.getElementById('<%=drpUsers.ClientID%>').value

                    var ServType = document.getElementById('<%=drpServType.ClientID%>').value

                    var vYear = document.getElementById('<%=drpYear.ClientID%>').value
                    var vQuater = document.getElementById('<%=drpQuater.ClientID%>').value
                    if (FileNo != "") {

                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_PT_ID}=\'' + FileNo + '\'';
                    }

                    if (CompanyCode != "") {

                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
                    }

                    if (DrId != "") {

                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
                    }

                    if (vServCode != "") {

                        Criteria += ' AND {HMS_INVOICE_TRANSACTION.HIT_SERV_CODE}=\'' + vServCode + '\'';
                    }

                    if (vInvType != "") {
                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_INVOICE_TYPE}=\'' + vInvType + '\'';
                    }

                    if (UserID != "") {
                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_CREATED_USER}=\'' + UserID + '\'';
                    }


                    if (InvoiceNos != "") {


                        var InvoiceNosFormated = "";
                        var arrInvoiceNos = InvoiceNos.split(",")

                        var i;
                        for (i = 0; i < arrInvoiceNos.length; i++) {

                            if (InvoiceNosFormated != '') {
                                InvoiceNosFormated += ',\'' + arrInvoiceNos[i] + '\'';
                            }
                            else {
                                InvoiceNosFormated = '\'' + arrInvoiceNos[i] + '\'';
                            }
                        }

                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID} IN [ ' + InvoiceNosFormated + ']';

                    }

                    if (IsGraterthanZero == 'true') {
                        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_CLAIM_AMOUNT} > 0 ';
                    }


                    //if (ReType == "Paid") {

                    //    Criteria += ' AND   {HMS_INVOICE_TRANSACTION.HIT_CLAIM_AMOUNT_PAID} >1 ';
                    //}
                    //else if (ReType == "Rejected") {

                    //    Criteria += ' AND   {HMS_INVOICE_TRANSACTION.HIT_CLAIM_REJECTED} >1 ';
                    //}




                    if (vServCode != "") {

                        Criteria += ' AND {HMS_INVOICE_TRANSACTION.HIT_SERV_CODE}=\'' + vServCode + '\'';
                    }

                    if (ServType != "") {

                        Criteria += ' AND {HMS_INVOICE_TRANSACTION.HIT_SERV_TYPE}=\'' + ServType + '\'';
                    }

                    if (vYear != "" && vQuater != "") {
                        if (vQuater == "1") {
                            Date1 = vYear + "-01-01";
                            Date2 = vYear + "-03-31";
                        }

                        if (vQuater == "2") {
                            Date1 = vYear + "-04-01";
                            Date2 = vYear + "-06-30";
                        }
                        if (vQuater == "3") {
                            Date1 = vYear + "-07-01";
                            Date2 = vYear + "-09-30";
                        }
                        if (vQuater == "4") {
                            Date1 = vYear + "-10-01";
                            Date2 = vYear + "-12-31";
                        }
                    }


                    //BELOW HIDE FOR REENA BEEGUM 
                    //  Criteria += ' AND {HMS_INVOICE_MASTER.HIM_INVOICE_TYPE}=\'Credit\''

                    Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

                    document.getElementById('<%=hidRptFormula.ClientID%>').value = Criteria;

                var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
                    var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

                    if (ShowReport == 'true') {
                        var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                    }
                    else {
                        //exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                        exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');

                    }

                }

                else if (PageName == 'HMS_RPT_VISIT') {

                    var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
                var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value
                var vServCode = document.getElementById('<%=txtServCode.ClientID%>').value
                var vInvType = document.getElementById('<%=drpInvType.ClientID%>').value


                if (CompanyCode != "") {

                    Criteria += ' AND {HMS_PATIENT_VISIT.HPV_COMP_ID}=\'' + CompanyCode + '\'';
                }

                if (DrId != "") {

                    Criteria += ' AND {HMS_PATIENT_VISIT.HPV_DR_ID}=\'' + DrId + '\'';
                }



                if (vInvType != "") {
                    Criteria += ' AND {HMS_PATIENT_VISIT.HPV_PT_TYPE}=\'' + vInvType + '\'';
                }



                Criteria += ' AND  {HMS_PATIENT_VISIT.HPV_DATE}>=date(\'' + Date1 + '\') AND  {HMS_PATIENT_VISIT.HPV_DATE}<=date(\'' + Date2 + '\')'


                document.getElementById('<%=hidRptFormula.ClientID%>').value = Criteria;

                var DBString = document.getElementById('<%=hidDBString.ClientID%>').value
                var LocalRptPath = document.getElementById('<%=LocalRptPath.ClientID%>').value

                if (ShowReport == 'true') {
                    var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                }
                else {
                    //exec('c:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$' + LocalRptPath + Report + '$' + Criteria + '$SCREEN$$$1');
                    exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$SCREEN$$$1');


                }

            }


}


function ExportReportFromDrp() {
    var Report = document.getElementById('<%=drpReport.ClientID%>').value


        var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
        var arrFromDate = dtFrom.split('/');
        var Date1;
        if (arrFromDate.length > 1) {

            Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
        }


        var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
    var arrToDate = dtTo.split('/');
    var Date2;
    if (arrToDate.length > 1) {

        Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
    }



    var CompanyCode = document.getElementById('<%=txtCompany.ClientID%>').value
    var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


        var PageName;
        PageName = '<%=Convert.ToString(Request.QueryString["PageName"]) %>'

    if (PageName == 'EClaim') {

        ShoweClaim(Report, dtFrom, dtTo, CompanyCode, DrId);
    }
    else if (PageName == 'ClaimTransfer') {
        ShowClaimTransfer(Report, dtFrom, dtTo, CompanyCode, DrId);
    }
    else if (PageName == 'Billing') {
        ShowBillingRpt(Report, dtFrom, dtTo, CompanyCode, DrId);
    }


}

function AppointmentRoomWiseyPopup() {

    var strDate;
    strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;

        var DrId = document.getElementById('<%=txtDoctorID.ClientID%>').value


        var strDeptID;
        strDeptID = document.getElementById("<%=drpDepartment.ClientID%>").value;


    var win = window.open("../../WebReports/AppointmetRoomWise.aspx?PageName=ApptDay;&DR_ID=" + DrId + "&DEPT_ID=" + strDeptID + "&APP_DATE=" + strDate + "&Time=0&DrId=0", "newwin", "menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1");
    win.focus();

}

function exec(cmdline, params) {

    var shell = new ActiveXObject("WScript.Shell");
    if (params) {
        params = ' ' + params;
    }
    else {
        params = '';
    }
    shell.Run('"' + cmdline + '"' + params);
    shell.Quit();
    //}
}
function ClientItemSelected(sender, e) {
    $get("<%=hfCustomerId.ClientID %>").value = e.get_value();
}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidPageName" runat="server" value="Reports" />
    <input type="hidden" id="hidPerCheck" runat="server" value="true" />

    <input type="hidden" id="hidRptFormula" runat="server" value="true" />
    <input type="hidden" id="hidDBString" runat="server" value="true" />

    <input type="hidden" id="LocalRptPath" runat="server" value="D:\\HMS\\Reports\\" />
    <table>
        <tr>
            <td class="PageHeader">
                <asp:Label ID="lblReportHeader" runat="server" CssClass="PageHeader" Text="Report"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>

    <table width="900px" cellpadding="5" cellspacing="5">
        <tr>
            <td style="width: 150px; height: 30px;">
                <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                    Text="From"></asp:Label><span style="color: red;">* </span>
            </td>
            <td style="width: 400px" colspan="3">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                &nbsp;
                    <asp:Label ID="Label2" runat="server" CssClass="label"
                        Text="To"></asp:Label>

                <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
            </td>

        </tr>
        <tr>
            <td style="height: 10px;" class="lblCaption1">TPA/Insurance </td>
            <td colspan="2">
                <asp:DropDownList ID="drpParentCompany" runat="server" CssClass="TextBoxStyle" Width="430px" AutoPostBack="true" OnSelectedIndexChanged="drpParentCompany_SelectedIndexChanged"></asp:DropDownList>
            </td>
        </tr>
        <tr id="trCompany" runat="server">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label8" runat="server" CssClass="lblCaption1"
                    Text="Company"></asp:Label>

            </td>
            <td colspan="2">
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Height="22px" onKeyUp="DoUpper(this);" onblur="return CompIdSelected()" OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                        <input type="button" name="close" value="Close the Popup" onclick="hideOptionList();" />
                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="350px" Height="22px" onKeyUp="DoUpper(this);" onblur="return CompNameSelected()" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>
                        <div id="divComp" style="visibility: hidden;"></div>
<asp:AutoCompleteExtender ServiceMethod="SearchCompany" MinimumPrefixLength="2"
CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="txtCompany" ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false" OnClientItemSelected = "ClientItemSelected">
</asp:AutoCompleteExtender>
<asp:HiddenField ID="hfCustomerId" runat="server" />
<asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" />
                        <%--<asp:AutoCompleteExtender ID="AuExtCompany" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp" FirstRowSelected="false"></asp:AutoCompleteExtender>--%>
                        <%--<asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" ServiceMethod="SearchCompany" BehaviorID="ACE"
TargetControlID="txtCompany" MinimumPrefixLength="1" EnableCaching="true" FirstRowSelected="false">
</asp:AutoCompleteExtender>--%>
                        <asp:AutoCompleteExtender ID="AuExtCompanyName" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp" DelimiterCharacters="," ShowOnlyCurrentWordInCompletionListItem="true"></asp:AutoCompleteExtender>

                    </ContentTemplate>

                </asp:UpdatePanel>
            </td>

        </tr>
        <tr>
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label20" runat="server" CssClass="lblCaption1" Text="Doctor"></asp:Label>
            </td>
            <td class="auto-style28" colspan="2">
                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtDoctorID" runat="server" CssClass="TextBoxStyle" BorderColor="#CCCCCC" BorderWidth="1" Width="70px" Height="22px" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()" onKeyUp="DoUpper(this);"></asp:TextBox>
                        <asp:TextBox ID="txtDoctorName" runat="server" CssClass="TextBoxStyle" BorderColor="#CCCCCC" BorderWidth="1" AutoPostBack="true" OnTextChanged="txtDoctorName_TextChanged" Width="350px" Height="22px" onblur="return DRNameSelected()" onKeyUp="DoUpper(this);"></asp:TextBox>
                        <div id="divDr" style="visibility: hidden;"></div>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                        </asp:AutoCompleteExtender>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                        </asp:AutoCompleteExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>

        </tr>
        <tr id="trService" runat="server" visible="true">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label10" runat="server" CssClass="lblCaption1" Text="Service"></asp:Label>
            </td>
            <td class="auto-style28" colspan="2">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtServCode" runat="server" Width="70px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" onblur="ServIdSelected()" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" ondblclick="return ServicePopup('txtServCode',this.value);"></asp:TextBox>
                        <asp:TextBox ID="txtServName" runat="server" Width="350px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()" AutoPostBack="true" OnTextChanged="txtServName_TextChanged" ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>

                        <div id="divwidth" style="visibility: hidden;"></div>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                        </asp:AutoCompleteExtender>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                        </asp:AutoCompleteExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>

        </tr>
        <tr id="trRoom" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label11" runat="server" CssClass="lblCaption1" Text="Room"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpRoom" runat="server" CssClass="TextBoxStyle" Width="100px"></asp:DropDownList>

            </td>
        </tr>
        <tr id="trNationality" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label5" runat="server" CssClass="lblCaption1" Text="Nationality"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="155px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trInvTYpe" runat="server">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="lblType" runat="server" CssClass="lblCaption1" Text="Invoice Type"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" BorderColor="#cccccc">
                    <asp:ListItem Value="" Selected="True">--- All ---</asp:ListItem>
                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                    <asp:ListItem Value="Customer">Customer</asp:ListItem>
                </asp:DropDownList>
                &nbsp; 
                <asp:CheckBox ID="chkGreaterThanZero" runat="server" CssClass="lblCaption1" Text="Claim Amount Greater Than Zero" Checked="true" />
            </td>
        </tr>
        <tr id="trTreatType" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label4" runat="server" CssClass="lblCaption1" Text="Treat. Type"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpTreatType" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" BorderColor="#cccccc">
                    <asp:ListItem Value="" Selected="True">--- All ---</asp:ListItem>
                    <asp:ListItem Value="HC">HomeCare</asp:ListItem>
                    <asp:ListItem Value="IP">IP</asp:ListItem>
                    <asp:ListItem Value="OP">OP</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:Label ID="Label13" runat="server" CssClass="lblCaption1" Text="Serv. Type"></asp:Label>

                <asp:DropDownList ID="drpServType" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" BorderColor="#cccccc">
                    <asp:ListItem Value="" Selected="True">--- All ---</asp:ListItem>
                    <asp:ListItem Value="S">Procedure</asp:ListItem>
                    <asp:ListItem Value="L">Lab</asp:ListItem>
                    <asp:ListItem Value="R">Radiology</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trUsers" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label3" runat="server" CssClass="lblCaption1" Text="User Name"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpUsers" runat="server" CssClass="TextBoxStyle" Width="250px" BorderWidth="1px" BorderColor="#cccccc">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trFileNo" runat="server" visible="false">
            <td class="lblCaption1" style="height: 30px; width: 150px;">File No

            </td>
            <td style="height: 30px; width: 170px;">
                <asp:TextBox ID="txtFileNo" runat="server" Width="70px" CssClass="TextBoxStyle" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('FileNo',this.value);" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                <asp:TextBox ID="txtFName" runat="server" Width="350px" CssClass="TextBoxStyle" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Name',this.value);"></asp:TextBox>
            </td>
        </tr>
        <tr id="trMobile" runat="server" visible="false">
            <td class="lblCaption1" style="height: 30px; width: 150px;">Mobile

            </td>
            <td style="height: 30px; width: 170px;">
                <asp:TextBox ID="txtMobile" runat="server" Width="100px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Invoice No (s)
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" Font-Bold="true" Font-Size="11" Width="420px"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 30px;" class="lblCaption1">Type
            </td>
            <td class="lblCaption1">

                <asp:DropDownList ID="drpReType" runat="server" CssClass="label" Width="150px" BorderWidth="1px">
                    <asp:ListItem Value="" Selected="True">--- All---</asp:ListItem>
                    <asp:ListItem Value="Paid">Paid</asp:ListItem>
                    <asp:ListItem Value="Rejected">Rejected</asp:ListItem>

                </asp:DropDownList>
                &nbsp; Year
                <asp:DropDownList ID="drpYear" runat="server" CssClass="label" Width="100px" BorderWidth="1px">
                    <asp:ListItem Value="" Selected="True">--- Select---</asp:ListItem>
                    <asp:ListItem Value="2010">2010</asp:ListItem>
                    <asp:ListItem Value="2011">2011</asp:ListItem>
                    <asp:ListItem Value="2012">2012</asp:ListItem>
                    <asp:ListItem Value="2013">2013</asp:ListItem>
                    <asp:ListItem Value="2014">2014</asp:ListItem>
                    <asp:ListItem Value="2015">2015</asp:ListItem>
                    <asp:ListItem Value="2016">2016</asp:ListItem>
                    <asp:ListItem Value="2017">2017</asp:ListItem>
                    <asp:ListItem Value="2018">2018</asp:ListItem>
                    <asp:ListItem Value="2019">2019</asp:ListItem>
                    <asp:ListItem Value="2020">2020</asp:ListItem>
                    <asp:ListItem Value="2021">2021</asp:ListItem>
                    <asp:ListItem Value="2022">2022</asp:ListItem>
                    <asp:ListItem Value="2023">2023</asp:ListItem>
                    <asp:ListItem Value="2024">2024</asp:ListItem>
                    <asp:ListItem Value="2025">2025</asp:ListItem>
                    <asp:ListItem Value="2026">2026</asp:ListItem>
                    <asp:ListItem Value="2027">2027</asp:ListItem>
                    <asp:ListItem Value="2028">2028</asp:ListItem>
                    <asp:ListItem Value="2029">2029</asp:ListItem>
                    <asp:ListItem Value="2030">2030</asp:ListItem>


                </asp:DropDownList>
                &nbsp; Quater
                <asp:DropDownList ID="drpQuater" runat="server" CssClass="label" Width="100px" BorderWidth="1px">
                    <asp:ListItem Value="" Selected="True">--- Select---</asp:ListItem>
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trSexAge" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label6" runat="server" CssClass="lblCaption1" Text="Sex"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle" Width="130px" BorderWidth="1px" BorderColor="#cccccc">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                    <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                    <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="Label7" runat="server" CssClass="lblCaption1" Text="Age"></asp:Label>
                <asp:DropDownList ID="drpAge" runat="server" Width="50px">
                </asp:DropDownList>
                <asp:TextBox ID="txtAge" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Height="22px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


            </td>
        </tr>

        <tr id="trDepartment" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label9" runat="server" CssClass="lblCaption1" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="155px">
                </asp:DropDownList>
            </td>
        </tr>

        <tr id="trAppStatus" runat="server" visible="false">
            <td style="width: 100px; height: 30px;">
                <asp:Label ID="Label12" runat="server" CssClass="lblCaption1" Text="Appointment Status"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpAppStatus" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" Width="155px">
                </asp:DropDownList>
            </td>
        </tr>

    </table>
    <table cellpadding="5" cellspacing="5" style="width: 100%;">
        <tr>
            <td>
                <asp:DropDownList ID="drpReport" runat="server" CssClass="TextBoxStyle">
                </asp:DropDownList>

                <input type="button" id="btnShowReport" runat="server" style="width: 100px; display: none;" class="button red small" value="Show" onclick="ShowReportFromDrp('true');" />

                <asp:Button ID="btnShowDrpReport" runat="server" CssClass="button red small" Text="Show" OnClick="btnShowDrpReport_Click" />

                <input type="button" id="btnReportExport" runat="server" style="width: 100px;" class="button red small" value="Export Report" onclick="ShowReportFromDrp('false');" />

                <asp:Button ID="btnExcelExport" runat="server" Visible="false" Text="Export" Width="150px" CssClass="button gray small" OnClick="btnExcelExport_Click" OnClientClick="ShowReportFromDrp('false');" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="150px" CssClass="button gray small" OnClick="btnClear_Click" />
                <input type="button" id="btnAppRoomWise" runat="server" class="button gray small" value="Appt. Room Wise" onclick="AppointmentRoomWiseyPopup()" />

            </td>
        </tr>

        <tr>

            <td>

                <input type="button" id="btnShowClaimTransferDtls" visible="false" runat="server" class="button red small" style="display: none; width: 150px;" value="Transfer Details" onclick="ClaimTransferDtls()" />
                <input type="button" id="btnShowClaimTransferSummary" visible="false" runat="server" class="button red small" style="display: none; width: 150px;" value="Transfer Summary" onclick="ClaimTransferSummary()" />


            </td>

        </tr>
    </table>
    <table width="800px" cellpadding="5" cellspacing="5">
        <tr>
            <td width="100px;"></td>
            <td width="200px;">
                <asp:Button ID="btnPTVisit" runat="server" Text="PT Visit" Width="100px" Style="display: none;" CssClass="button orange small" OnClientClick="return HC_PtVisit();" />
            </td>
            <td width="200px;"></td>
            <td width="200px;"></td>

        </tr>
        <tr>
            <td></td>
            <td width="200px;">
                <asp:Button ID="btnInvoice" runat="server" Text="Invoice" Width="100px" Style="display: none;" CssClass="button orange small" OnClientClick="return Invoice();" />
            </td>
            <td width="200px;">
                <asp:Button ID="btnCreditBIll" runat="server" Text="Credit Bill" Width="100px" Style="display: none;" CssClass="button orange small" OnClientClick="return ShowCreditBillStatement();" />
            </td>
            <td width="200px;"></td>
        </tr>
        <tr>
            <td></td>
            <td width="200px;">
                <asp:Button ID="btnIncomeSummary" runat="server" Text="Income Summary" Width="150px" Style="display: none;" CssClass="button orange small" OnClientClick="return ShowIncomeSummary();" />
            </td>
            <td width="200px;">
                <asp:Button ID="btnDrWiseIncome" runat="server" Text="DrWiseIncome" Width="150px" Style="display: none;" CssClass="button orange small" OnClientClick="return ShowDrWiseIncome();" />
            </td>
            <td width="200px;">
                <asp:Button ID="bntDeptWiseIncome" runat="server" Text="DeptWiseIncome" Width="150px" Style="display: none;" CssClass="button orange small" OnClientClick="return ShowDeptWiseIncome();" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td width="200px;">
                <input type="button" id="btnShiftoutSummary" runat="server" class="button orange" style="display: none;" value="Shiftout Summary" onclick="ShiftoutSummary()" />
            </td>
            <td width="200px;"></td>
            <td width="200px;"></td>
        </tr>

        <tr>
            <td></td>
            <td width="200px;">
                <input type="button" id="btnCompanyReceipts" runat="server" class="button orange" style="display: none;" value="Company Receipts Dtls" onclick="CompanyReceiptsDtls()" />
                <input type="button" id="btnInsuranceStatment" runat="server" class="button orange" style="display: none;" value="Insurance Statment" onclick="InsuranceStatment()" />
            </td>
            <td width="200px;"></td>
            <td width="200px;"></td>
        </tr>
    </table>




</asp:Content>
