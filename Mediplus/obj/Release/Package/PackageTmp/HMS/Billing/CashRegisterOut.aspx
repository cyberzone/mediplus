﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="CashRegisterOut.aspx.cs" Inherits="Mediplus.HMS.Billing.CashRegisterOut" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
 
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />
    <script src="../../Validation.js" language="javascript" type="text/javascript"></script>

     
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

     <script language="javascript" type="text/javascript">
         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }

         function CalculateAmt(Amt, Count) {

             var Total = 0;

             if (Amt != '' && Count != '') {
                 Total = Amt * Count;
             }



             if (Amt == "1000") {
                 document.getElementById('<%=txt1000Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "500") {
                document.getElementById('<%=txt500Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "200") {
                document.getElementById('<%=txt200Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "100") {
                document.getElementById('<%=txt100Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "50") {
                document.getElementById('<%=txt50Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "20") {
                document.getElementById('<%=txt20Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "10") {
                document.getElementById('<%=txt10Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "5") {
                document.getElementById('<%=txt5Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "1") {
                document.getElementById('<%=txt1Amt.ClientID%>').value = Total + ".00";
            }
            if (Amt == "0.50") {
                document.getElementById('<%=txt0_50Amt.ClientID%>').value = Total;
            }
            if (Amt == "0.25") {
                document.getElementById('<%=txt0_25Amt.ClientID%>').value = Total;
            }

            if (Amt == "1.0") {
                document.getElementById('<%=txtOtherAmt.ClientID%>').value = Count + ".00";
            }

            CalculateGrandTotal();
        }

        function CalculateGrandTotal() {
            var GrandTotal = 0;
            var var1000 = 0, var500 = 0, var200 = 0, var100 = 0, var50 = 0, var20 = 0, var10 = 0, var5 = 0, var1 = 0, var050 = 0, var025 = 0, varOther = 0;

            if (document.getElementById('<%=txt1000Amt.ClientID%>').value != '')
                var1000 = document.getElementById('<%=txt1000Amt.ClientID%>').value;

            if (document.getElementById('<%=txt500Amt.ClientID%>').value != '')
                var500 = document.getElementById('<%=txt500Amt.ClientID%>').value;

            if (document.getElementById('<%=txt200Amt.ClientID%>').value != '')
                var200 = document.getElementById('<%=txt200Amt.ClientID%>').value;

            if (document.getElementById('<%=txt100Amt.ClientID%>').value != '')
                var100 = document.getElementById('<%=txt100Amt.ClientID%>').value;

            if (document.getElementById('<%=txt50Amt.ClientID%>').value != '')
                var50 = document.getElementById('<%=txt50Amt.ClientID%>').value;

            if (document.getElementById('<%=txt20Amt.ClientID%>').value != '')
                var20 = document.getElementById('<%=txt20Amt.ClientID%>').value;

            if (document.getElementById('<%=txt10Amt.ClientID%>').value != '')
                var10 = document.getElementById('<%=txt10Amt.ClientID%>').value;

            if (document.getElementById('<%=txt5Amt.ClientID%>').value != '')
                var5 = document.getElementById('<%=txt5Amt.ClientID%>').value;

            if (document.getElementById('<%=txt1Amt.ClientID%>').value != '')
                var1 = document.getElementById('<%=txt1Amt.ClientID%>').value;

            if (document.getElementById('<%=txt0_50Amt.ClientID%>').value != '')
                var050 = document.getElementById('<%=txt0_50Amt.ClientID%>').value;

            if (document.getElementById('<%=txt0_25Amt.ClientID%>').value != '')
                var025 = document.getElementById('<%=txt0_25Amt.ClientID%>').value;

            if (document.getElementById('<%=txtOtherAmt.ClientID%>').value != '')
                varOther = document.getElementById('<%=txtOtherAmt.ClientID%>').value;


            GrandTotal = eval(var1000) + eval(var500) + eval(var200) + eval(var100) + eval(var50) + eval(var20) + eval(var10) + eval(var5) + eval(var1) + eval(var050) + eval(var025) + eval(varOther)

            document.getElementById('<%=txtInvoiceCash.ClientID%>').value = GrandTotal;

        }


        function ShowPrintCA() {

            var dtFrom = document.getElementById('<%=txtDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }

            var BranchID = '<%=Convert.ToString(Session["Branch_ID"])  %>'
            var UserID = document.getElementById('<%=txtUserCode.ClientID%>').value;
            var Report = "ShiftoutSummaryCash.rpt";

            var Criteria = " 1=1 ";

            Criteria += ' AND ( {HMS_INVOICE_MASTER.HIM_INVOICE_TYPE} =\'Cash\' OR {HMS_INVOICE_MASTER.HIM_INVOICE_TYPE} =\'CA\')'

            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'';
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_CREATED_USER}=\'' + UserID + '\'';



            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') '
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function ShowPrintCR() {

            var dtFrom = document.getElementById('<%=txtDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }

            var BranchID = '<%=Convert.ToString(Session["Branch_ID"])  %>'
            var UserID = document.getElementById('<%=txtUserCode.ClientID%>').value;
            var Report = "ShiftoutSummaryCredit.rpt";

            var Criteria = " 1=1 ";
            Criteria += ' AND ( {HMS_INVOICE_MASTER.HIM_INVOICE_TYPE} =\'Credit\' OR {HMS_INVOICE_MASTER.HIM_INVOICE_TYPE} =\'CR\')'
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'';
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_CREATED_USER}=\'' + UserID + '\'';



            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') '
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function ConfirmClossing() {

            var varConfirm = window.confirm('Are you sure you want to close the shift ? ');

            return varConfirm;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:label id="lblStatus" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
            </td>
        </tr>

    </table>
     <div  style="padding:5px; width: 750px;   overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
    <table width="100%" cellpadding="5" cellspacing="5">
         <tr>
             <td  style="height:30px;"></td>
             <td    >
                  <input type="text" id="txtDate" runat="server"  class="TextBoxStyle" style="width:310px;" maxlength="10"   readonly="readonly"  />
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <label id="lblShiftStatus" runat="server"   class="lblCaption1" style="font-size:large;" ></label> 
             </td>
         </tr>
         <tr>
             <td align="right" class="lblCaption1" style="height:30px;">
                   User Name  &nbsp;&nbsp;
             </td>
             <td >
                <input type="text" id="txtUserCode"  runat="server"  class="TextBoxStyle"   style="width:100px;"    readonly="readonly"  />
                  <input type="text" id="txtUserName"  runat="server"  class="TextBoxStyle"   style="width:200px;"    readonly="readonly"  />
             </td>
         </tr>
        </table>
    <span class="lblCaption1" style="font-weight:bold;"> Cash In Hand </span>
     <div  style="padding:5px; width: 700px;   overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
     <table width="100%" cellpadding="5" cellspacing="5">
            <tr>
                <td style="width: 50%" valign="top">
                     <table width="100%" cellpadding="5" cellspacing="5">
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                1000 &nbsp;X&nbsp;
                            </td>
                            <td style="width: 50px;">
                                <input type="text" id="txt1000"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('1000',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt1000Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"  />
                            </td>
                        </tr>
                          <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                500 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt500"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('500',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt500Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"   />
                            </td>
                        </tr>
                          <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                200 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt200"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('200',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt200Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"   />
                            </td>
                        </tr>
                          <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                100 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt100"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('100',this.value);"/>
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt100Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"    />
                            </td>
                        </tr>
                           <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                50 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt50"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"   onkeyup="CalculateAmt('50',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt50Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"  />
                            </td>
                        </tr>
                           <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                20 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt20"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('20',this.value);"/>
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt20Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"    />
                            </td>
                        </tr>
                           <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                10 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt10"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"   onkeyup="CalculateAmt('10',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt10Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);" />
                            </td>
                        </tr>
                           <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                5 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt5"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('5',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt5Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"   />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                1 &nbsp;X&nbsp;
                            </td>
                            <td style="width: 50px;">
                                <input type="text" id="txt1"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('1',this.value);"/>
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt1Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" disabled="disabled"   maxlength="10" onkeypress="return OnlyNumeric(event);"    />
                            </td>
                        </tr>
                          <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                0.50 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt0_50"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"   onkeyup="CalculateAmt('0.50',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt0_50Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"  />
                            </td>
                        </tr>
                          <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                0.25 &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txt0_25"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);"  onkeyup="CalculateAmt('0.25',this.value);" />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txt0_25Amt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"  />
                            </td>
                        </tr>
                         <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                Other &nbsp;X&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtOther"  runat="server"  class="TextBoxStyle"   style="width:40px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                            <td class="lblCaption1">
                                 =&nbsp;<input type="text" id="txtOtherAmt"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);"  />
                            </td>
                        </tr>
                     </table>
                </td>
                <td style="width: 50%; " valign="top">
                    <table width="100%" cellpadding="5" cellspacing="5">
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                               Opening Cash&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtOpeningCash"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                                 Cash&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtInvoiceCash"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;"  readonly="readonly"  maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                               Credit Card&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtInvoiceCC"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>
                         <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                              Cheque&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtInvoiceChq"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" disabled="disabled"  maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                              Expense&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtExpense"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>
                          <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                              Mag. Paid&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtMagazinePaid"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>
                        <tr>
                            <td style="width: 100px;height:30px;text-align:right;" class="lblCaption1">
                              CounterWithDrawel&nbsp;
                            </td>
                            <td>
                                <input type="text" id="txtCounterWithDrawel"  runat="server"  class="TextBoxStyle"   style="width:100px;text-align:right;" maxlength="10" onkeypress="return OnlyNumeric(event);" onkeyup="CalculateAmt('1.0',this.value);"  />
                            </td>
                           
                        </tr>


                        </table>
                </td>
                </tr>

      </table>
         </div>
      <table width="100%" cellpadding="5" cellspacing="5">
          <tr>
              <td>
                <asp:Button ID="btnShiftClosing" runat="server"  CssClass="button red small"   Height="25px" Text="Shift Closing" OnClick="btnShiftClosing_Click"   OnClientClick="return ConfirmClossing();"  />
                  <input type="button" id="btnPrint" runat="server"  class="button gray small"  value="Shiftout Summary - Cash" onclick="ShowPrintCA()" />
                 <input type="button" id="Button1" runat="server"  class="button gray small"  value="Shiftout Summary - Credit" onclick="ShowPrintCR()" />

              </td>
          </tr>

          </table>

         <br /><br /><br />
         </div>
    </div>
                  
    
   </asp:Content>

