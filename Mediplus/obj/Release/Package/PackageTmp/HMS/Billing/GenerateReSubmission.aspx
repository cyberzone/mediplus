﻿<%@ Page Title=""  MasterPageFile="~/Site2.Master"  Language="C#" AutoEventWireup="true" CodeBehind="GenerateReSubmission.aspx.cs" Inherits="Mediplus.HMS.Billing.GenerateReSubmission" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>


 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

     
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
   <style>
       .AutoExtender
       {
           font-family: Verdana, Helvetica, sans-serif;
           font-size: .8em;
           font-weight: normal;
           border: solid 1px #006699;
           line-height: 20px;
           padding: 10px;
           background-color: White;
           margin-left: 10px;
       }

       .AutoExtenderList
       {
           border-bottom: dotted 1px #006699;
           cursor: pointer;
           color: Maroon;
       }

       .AutoExtenderHighlight
       {
           color: White;
           background-color: #006699;
           cursor: pointer;
       }

       #divwidth
       {
           width: 400px !important;
       }

           #divwidth div
           {
               width: 400px !important;
           }
   </style>
     <script language="javascript" type="text/javascript">

         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }


         function BindCompanyDtls(CompanyId, CompanyName) {

             document.getElementById("<%=txtCompany.ClientID%>").value = CompanyId;
             document.getElementById("<%=txtCompanyName.ClientID%>").value = CompanyName;

         }

         function InsurancePopup(PageName, strValue, evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode == 126) {
                 var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                 win.focus();
                 return false;
             }
             return true;
         }

         function CompIdSelected() {
             if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');


                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];


                }
            }

            return true;
        }



        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }



        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



            if (document.getElementById('<%=txtInvoiceNo.ClientID%>').value == "") {
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Date";
                    document.getElementById('<%=txtFromDate.ClientID%>').focus;
                    return false;
                }
                if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtFromDate.ClientID%>').focus()
                    return false;
                }

                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Date";
                    document.getElementById('<%=txtToDate.ClientID%>').focus;
                    return false;
                }
                if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtToDate.ClientID%>').focus()
                    return false;
                }

                if (document.getElementById('<%=txtCompany.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company";
                    document.getElementById('<%=txtCompany.ClientID%>').focus;
                    return false;
                }

                if (document.getElementById('<%=txtCompanyName.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company";
                    document.getElementById('<%=txtCompanyName.ClientID%>').focus;
                    return false;
                }

            }


            return true;
        }


        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";




            if (document.getElementById('<%=txtIDPayer.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter IDPayer";
                document.getElementById('<%=txtIDPayer.ClientID%>').focus;
                return false;
            }



            if (document.getElementById('<%=txtClmStartDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Start Date";
                document.getElementById('<%=txtClmStartDate.ClientID%>').focus;
                return false;
            }
            if (isDate(document.getElementById('<%=txtClmStartDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtClmStartDate.ClientID%>').focus()
                return false;
            }

            if (document.getElementById('<%=txtClmFromTime.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Start Time";
                document.getElementById('<%=txtClmFromTime.ClientID%>').focus;
                return false;
            }


            if (document.getElementById('<%=txtClmEndDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Ende Date";
                document.getElementById('<%=txtClmEndDate.ClientID%>').focus;
                return false;
            }
            if (isDate(document.getElementById('<%=txtClmEndDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtClmEndDate.ClientID%>').focus()
                return false;
            }

            if (document.getElementById('<%=txtClmToTime.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter End Time";
                document.getElementById('<%=txtClmToTime.ClientID%>').focus;
                return false;
            }

            if (document.getElementById('<%=drpReType.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Resubmission Type";
               document.getElementById('<%=drpReType.ClientID%>').focus;
               return false;
           }

           return true;
       }

       function SaveVal1() {
           var label;
           label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";





            if (document.getElementById('<%=txtClmStartDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Start Date";
                document.getElementById('<%=txtClmStartDate.ClientID%>').focus;
                return false;
            }
            if (isDate(document.getElementById('<%=txtClmStartDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtClmStartDate.ClientID%>').focus()
                return false;
            }

            if (document.getElementById('<%=txtClmFromTime.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Start Time";
                document.getElementById('<%=txtClmFromTime.ClientID%>').focus;
                return false;
            }


            if (document.getElementById('<%=txtClmEndDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Ende Date";
                document.getElementById('<%=txtClmEndDate.ClientID%>').focus;
                return false;
            }
            if (isDate(document.getElementById('<%=txtClmEndDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtClmEndDate.ClientID%>').focus()
                return false;
            }

            if (document.getElementById('<%=txtClmToTime.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter End Time";
                document.getElementById('<%=txtClmToTime.ClientID%>').focus;
                return false;
            }


            if (document.getElementById('<%=drpReType.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Resubmission Type";
                document.getElementById('<%=drpReType.ClientID%>').focus;
                return false;
            }
            return true;
        }

        function DeleteConfirm() {
            var ResubNo = document.getElementById('<%=txtResubNo.ClientID%>').value
           var Res = confirm('Do you want to Delete the following Resubmission ? ' + ResubNo);

           return Res;
       }


       function PrincipalCodeSel() {
           if (document.getElementById('<%=txtPrincipalCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtPrincipalCode.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtPrincipalCode.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtPrincipalName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function PrincipalNameSel() {
            if (document.getElementById('<%=txtPrincipalName.ClientID%>').value != "") {
                 var Data = document.getElementById('<%=txtPrincipalName.ClientID%>').value;
                 var Data1 = Data.split('~');

                 var Code = Data1[0];
                 var Name = Data1[1];

                 if (Data1.length > 1) {

                     document.getElementById('<%=txtPrincipalCode.ClientID%>').value = Code.trim();
                     document.getElementById('<%=txtPrincipalName.ClientID%>').value = Name.trim();
                 }


             }

             return true;
         }

         function S1CodeSel() {
             if (document.getElementById('<%=txtS1Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS1Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS1Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS1Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S1NameSel() {
            if (document.getElementById('<%=txtS1Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS1Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS1Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS1Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S2CodeSel() {
            if (document.getElementById('<%=txtS2Code.ClientID%>').value != "") {
                   var Data = document.getElementById('<%=txtS2Code.ClientID%>').value;
                   var Data1 = Data.split('~');

                   var Code = Data1[0];
                   var Name = Data1[1];

                   if (Data1.length > 1) {
                       document.getElementById('<%=txtS2Code.ClientID%>').value = Code.trim();
                        document.getElementById('<%=txtS2Name.ClientID%>').value = Name.trim();
                    }


                }

                return true;
            }

            function S2NameSel() {
                if (document.getElementById('<%=txtS2Name.ClientID%>').value != "") {
                    var Data = document.getElementById('<%=txtS2Name.ClientID%>').value;
                    var Data1 = Data.split('~');

                    var Code = Data1[0];
                    var Name = Data1[1];

                    if (Data1.length > 1) {
                        document.getElementById('<%=txtS2Code.ClientID%>').value = Code.trim();
                        document.getElementById('<%=txtS2Name.ClientID%>').value = Name.trim();
                    }


                }

                return true;
            }


            function S3CodeSel() {
                if (document.getElementById('<%=txtS3Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS3Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS3Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS3Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S3NameSel() {
            if (document.getElementById('<%=txtS3Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS3Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS3Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS3Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S4CodeSel() {
            if (document.getElementById('<%=txtS4Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS4Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS4Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS4Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S4NameSel() {
            if (document.getElementById('<%=txtS4Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS4Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS4Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS4Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S5CodeSel() {
            if (document.getElementById('<%=txtS5Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS5Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS5Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS5Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S5NameSel() {
            if (document.getElementById('<%=txtS5Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS5Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS5Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS5Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S6CodeSel() {
            if (document.getElementById('<%=txtS6Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS6Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS6Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS6Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S6NameSel() {
            if (document.getElementById('<%=txtS6Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS6Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS6Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS6Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S7CodeSel() {
            if (document.getElementById('<%=txtS7Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS7Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS7Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS7Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S7NameSel() {
            if (document.getElementById('<%=txtS7Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS7Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS7Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS7Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S8CodeSel() {
            if (document.getElementById('<%=txtS8Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS8Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS8Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS8Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S8NameSel() {
            if (document.getElementById('<%=txtS8Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS8Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS8Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS8Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S11CodeSel() {
            if (document.getElementById('<%=txtS11Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS11Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS11Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS11Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S11NameSel() {
            if (document.getElementById('<%=txtS11Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS11Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS11Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS11Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S12CodeSel() {
            if (document.getElementById('<%=txtS12Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS12Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS12Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS12Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S12NameSel() {
            if (document.getElementById('<%=txtS12Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS12Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS12Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS12Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S13CodeSel() {
            if (document.getElementById('<%=txtS13Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS13Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS13Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS13Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S13NameSel() {
            if (document.getElementById('<%=txtS13Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS13Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS13Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS13Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S14CodeSel() {
            if (document.getElementById('<%=txtS14Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS14Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS14Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS14Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S14NameSel() {
            if (document.getElementById('<%=txtS14Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS14Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS14Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS14Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S15CodeSel() {
            if (document.getElementById('<%=txtS15Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS15Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS15Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS15Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S15NameSel() {
            if (document.getElementById('<%=txtS15Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS15Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS15Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS15Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }





        function S16CodeSel() {
            if (document.getElementById('<%=txtS16Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS16Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS16Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS16Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S16NameSel() {
            if (document.getElementById('<%=txtS16Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS16Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS16Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS16Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }






        function S17CodeSel() {
            if (document.getElementById('<%=txtS17Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS17Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS17Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS17Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S17NameSel() {
            if (document.getElementById('<%=txtS17Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS17Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS17Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS17Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S18CodeSel() {
            if (document.getElementById('<%=txtS18Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS18Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS18Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS18Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S18NameSel() {
            if (document.getElementById('<%=txtS18Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS18Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS18Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS18Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S19CodeSel() {
            if (document.getElementById('<%=txtS19Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS19Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS19Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS19Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S19NameSel() {
            if (document.getElementById('<%=txtS19Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS19Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS19Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS19Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S20CodeSel() {
            if (document.getElementById('<%=txtS20Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS20Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS20Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS20Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S20NameSel() {
            if (document.getElementById('<%=txtS20Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS20Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS20Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS20Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S21CodeSel() {
            if (document.getElementById('<%=txtS21Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS21Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS21Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS21Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S21NameSel() {
            if (document.getElementById('<%=txtS21Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS21Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS21Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS21Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S22CodeSel() {
            if (document.getElementById('<%=txtS22Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS22Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS22Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS22Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S22NameSel() {
            if (document.getElementById('<%=txtS22Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS22Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS22Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS22Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function S23CodeSel() {
            if (document.getElementById('<%=txtS23Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS23Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS23Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS23Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S23NameSel() {
            if (document.getElementById('<%=txtS23Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS23Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS23Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS23Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S24CodeSel() {
            if (document.getElementById('<%=txtS24Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS24Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS24Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS24Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S24NameSel() {
            if (document.getElementById('<%=txtS24Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS24Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS24Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS24Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S25CodeSel() {
            if (document.getElementById('<%=txtS25Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS25Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS25Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS25Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S25NameSel() {
            if (document.getElementById('<%=txtS25Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS25Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS25Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS25Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }




        function S26CodeSel() {
            if (document.getElementById('<%=txtS26Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS26Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS26Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS26Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S26NameSel() {
            if (document.getElementById('<%=txtS26Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS26Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS26Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS26Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S27CodeSel() {
            if (document.getElementById('<%=txtS27Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS27Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS27Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS27Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S27NameSel() {
            if (document.getElementById('<%=txtS27Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS27Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS27Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS27Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }



        function S28CodeSel() {
            if (document.getElementById('<%=txtS28Code.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS28Code.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS28Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS28Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function S28NameSel() {
            if (document.getElementById('<%=txtS28Name.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtS28Name.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtS28Code.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtS28Name.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function AdmitCodeSel() {
            if (document.getElementById('<%=txtAdmitCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtAdmitCode.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtAdmitCode.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtAdmitName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function AdmitNameSel() {
            if (document.getElementById('<%=txtAdmitName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtAdmitName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtAdmitCode.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtAdmitName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        //onblur="return ServCodeSel(this)" 

        function ServCodeSel(txt) {
            var varVal;
            var id = txt.id;
            varVal = txt.value;
            if (varVal != "") {
                var Data = varVal;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                // id.value = Code;
                var serviceCode = jQuery("#" + txt.id);
                serviceCode.val(Code);
                serviceCode.closest("tr").find('input:eq(1)').val(Name);

            }

            return true;
        }
        //onblur = "return ServNameSel(this)"
        function ServNameSel(txt) {
            var varVal;
            var id = txt.id;
            varVal = txt.value;
            if (varVal != "") {
                var Data = varVal;
                var Data1 = Data.split('~');

                var Name = Data1[0];
                var Code = Data1[1];


                var serviceCode = jQuery("#" + txt.id);
                serviceCode.val(Name);
                serviceCode.closest("tr").find('input:eq(0)').val(Code);

            }

            return true;
        }

        function ValClaimGenerate() {

            var TotalClaims = document.getElementById('<%=hidTotalClaims.ClientID%>').value
            var TotalAmount = document.getElementById('<%=hidTotalAmount.ClientID%>').value


            var isClaimGenerate = window.confirm('Total No. of claims : ' + TotalClaims + ' Total Amount : ' + TotalAmount + ' Do you want to Continue ? ');
            document.getElementById('<%=hidClaimGenerate.ClientID%>').value = isClaimGenerate;

            return true;
        }

        function ShowExportMsg(strName) {

            alert('ReSubmission Excel Exported - File Name is ReSubExcel' + strName + '.xls');
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />

     <input type="hidden" id="hidClaimGenerate" runat="server"  value="true"  />

     <input type="hidden" id="hidTotalClaims" runat="server"   />
      <input type="hidden" id="hidTotalAmount" runat="server"   />

     
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:updatepanel id="UpdatePanel50" runat="server">
                        <contenttemplate>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </contenttemplate>
               </asp:updatepanel>
            </td>
        </tr>

    </table>
    <div style="padding-top: 5px;">

        <table width="1000px" cellpadding="3" cellspacing="3">
            <tr>
                <td style="width: 70px" class="lblCaption1">
                     From  <span style="color:red;">* </span>

                </td>
                <td   >
                    <asp:TextBox ID="txtFromDate" runat="server" style="width:75px;"  CssClass="TextBoxStyle"  MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                        enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                    </asp:calendarextender>
                    <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                    <asp:TextBox ID="txtFromTime" runat="server" Width="30px" MaxLength="5" CssClass="TextBoxStyle"    onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:maskededitextender id="MaskedEditExtender3" runat="server" enabled="true" targetcontrolid="txtFromTime" mask="99:99" masktype="Time"></asp:maskededitextender>
                    &nbsp;

 
                    <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                        Text="To"></asp:Label>

                    <asp:TextBox ID="txtToDate" runat="server"  style="width:75px;" CssClass="TextBoxStyle" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:calendarextender id="CalendarExtender1" runat="server"
                        enabled="True" targetcontrolid="txtToDate" format="dd/MM/yyyy">
                    </asp:calendarextender>
                    <asp:maskededitextender id="MaskedEditExtender2" runat="server" enabled="true" targetcontrolid="txtToDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                    <asp:TextBox ID="txtToTime" runat="server" Width="30px" MaxLength="5" CssClass="TextBoxStyle"    onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:maskededitextender id="MaskedEditExtender4" runat="server" enabled="true" targetcontrolid="txtToTime" mask="99:99" masktype="Time"></asp:maskededitextender>


                </td>
              
                 <td style="width: 150px;height:30px" class="lblCaption1">
                   Doctors
                </td>
                <td >
                    <asp:DropDownList ID="drpSrcDoctor" CssClass="TextBoxStyle" runat="server" Width="250px">
                    </asp:DropDownList>

                </td>

                <td style="width: 100px" class="lblCaption1">
                   Treatment Type

                </td>
                <td >
                    <asp:DropDownList ID="drpType" CssClass="TextBoxStyle" runat="server" Width="100px">
                        <asp:ListItem Text="--- All ---" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="OP" Value="OP"></asp:ListItem>
                        <asp:ListItem Text="IP" Value="IP"></asp:ListItem>
                        <asp:ListItem Text="DAYCARE" Value="DAYCARE"></asp:ListItem>
                        <asp:ListItem Text="IP/DAYCARE" Value="IP/DAYCARE"></asp:ListItem>
                    </asp:DropDownList>



                </td>

            </tr>
            <tr>
                <td style="width: 70px" class="lblCaption1">
                   Company <span style="color:red;">* </span>

                </td>
                <td  >
                    <asp:updatepanel id="UpdatePanel7" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle"    Width="70px" MaxLength="10"   onblur="return CompIdSelected()"   AutoPostBack="true"   OnTextChanged="txtCompany_TextChanged" ></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle"   Width="220px" MaxLength="50"   onblur="return CompNameSelected()"  ></asp:TextBox>
                               <div ID="divwidth" style="visibility:hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany" 
                                 CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth" ></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName" 
                                 CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth" ></asp:AutoCompleteExtender>

                                 

                        </contenttemplate>

                    </asp:updatepanel>
                </td>
 
                <td style="width: 150px;height:30px" class="lblCaption1">
                    Invoice No 
                </td>

                <td     >
                   <asp:TextBox ID="txtInvoiceNo" runat="server"      CssClass="TextBoxStyle" Width="250px"     ></asp:TextBox>
                </td>
               
           
              <td  colspan="2" >

                    <asp:Button ID="btnShow" runat="server" CssClass="button red small" Width="70px" OnClientClick="return Val();"
                        OnClick="btnShow_Click" Text="Show" />

                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" />
                </td>
                 </tr>
            <tr>
                <td>

                </td>
                <td colspan="5">
                      <asp:CheckBox ID="chkShowRejectInv" runat="server" CssClass="lblCaption1"  checked="true" Text="Show only Rejected Invoice" />
                     <asp:CheckBox ID="chkShowSubmit" runat="server" CssClass="lblCaption1"  Text="Show only Resubmitted E-Claims" />
                </td>
                 
            </tr>
        </table>

    </div>


    <div style="padding-top: 0px; width: 1000px; height: 375px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
        <asp:updatepanel id="UpdatePanel2" runat="server">
            <contenttemplate>
                <asp:GridView ID="gvInvoice" runat="server"   AutoGenerateColumns="False"   gridlines="None"
                    EnableModelValidation="True" Width="100%"   >
                    <HeaderStyle CssClass="GridHeader_Gray" />
                    <RowStyle CssClass="GridRow" />
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>
                      <asp:TemplateField  ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                           <HeaderTemplate>
                                 
                            <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                                
                        </HeaderTemplate>
                            <ItemTemplate>
                                  <asp:UpdatePanel runat="server" ID="UpdatePanel44" 
                                         UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                     <asp:CheckBox ID="chkInvResub" runat="server" CssClass="label" autopostback="true" OnCheckedChanged="InvoiceSelect_Click"   />
                                     </ContentTemplate>
                                     <Triggers>
                                            <asp:PostBackTrigger ControlID="chkInvResub" />
                                     </Triggers>
                                     </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                          <asp:TemplateField HeaderText="Inv Code">
                            <ItemTemplate>
                                <asp:Label ID="lblPolicyNo" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_POLICY_NO") %>' Width="70px" visible="false" ></asp:Label>
                                <asp:Label ID="lblInvoiceId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>' Width="70px" visible="false" ></asp:Label>
                                  <asp:Label ID="lblAuthorizationID" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_AUTHID") %>'  visible="false" ></asp:Label>
                                <asp:LinkButton ID="lnkCompName" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_INVOICE_ID") %>'></asp:LinkButton>
                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click" >

                                     <asp:Label ID="lblgvDate" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_DATEDESC") %>' ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Claim Amt">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClick="Select_Click"   >

                                <asp:Label ID="lblgvClaimAmount" CssClass="GridRow"  runat="server" Text='<%# Eval("HIM_CLAIM_AMOUNT", "{0:0.00}") %>'  ></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Paid Amt.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPaid" runat="server" OnClick="Select_Click"   >
                                       <asp:Label ID="lblgvPaidAmt" CssClass="GridRow"  runat="server" Text='<%# Eval("HIM_CLAIM_AMOUNT_PAID", "{0:0.00}") %>' style="text-align:right;padding:5px;"  ></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Blance Amt">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvBlanceAmt" runat="server" OnClick="Select_Click"  >

                                    <asp:Label ID="lblgvBlanceAmt" CssClass="GridRow"  runat="server" Text='<%# Eval("BlanceAmt", "{0:0.00}") %>'  style="text-align:right;padding:5px;" ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClick="Select_Click"  >

                                    <asp:Label ID="lblgvPatientShare" CssClass="GridRow"  runat="server" Text='<%# Eval("HIM_PT_AMOUNT", "{0:0.00}") %>'  style="text-align:right;padding:5px;" ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Reject">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClick="Select_Click"   >

                                     <asp:Label ID="lblgvRejectedAmt" CssClass="GridRow"  runat="server" Text='<%# Eval("HIM_CLAIM_REJECTED", "{0:0.00}") %>'  style="text-align:right;padding:5px;" ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClick="Select_Click"  >

                                    <asp:Label ID="lblgvFileNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_PT_ID") %>'>  </asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click"  >
                                     <asp:Label ID="lblgvPTName" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_PT_NAME") %>'>  </asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dr Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click"  >
                                      <asp:Label ID="lblgvDrName" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_DR_NAME") %>'>  </asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                        <asp:TemplateField HeaderText="Policy No">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click"  >
                                     <asp:Label ID="lblgvPolicyNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_POLICY_NO") %>'>  </asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Claim No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCoucherNo" runat="server" OnClick="Select_Click"  > 
                                     <asp:Label ID="lblgvVoucherNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_VOUCHER_NO") %>'>  </asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                   <asp:TemplateField HeaderText="Transfer Status"  ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                             
                             <asp:LinkButton ID="lnkResubVerify" runat="server" OnClick="Select_Click"  >
                                    <asp:Label ID="lblResubVerify" CssClass="GridRow"  runat="server" Text='<%# Bind("HIM_RESUB_VERIFY_STATUSDesc") %>' ></asp:Label>
                             </asp:LinkButton>
                            </ItemTemplate>
                          </asp:TemplateField>
                    </Columns>

                </asp:GridView>

            </contenttemplate>
        </asp:updatepanel>

    </div>    
     <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                    <ContentTemplate>
    <table width="70%">
        <tr>
            
               <td class="label" style="width:100%;font-weight:bold;">
                      Total No. of Claims :&nbsp;<asp:Label ID="lblTotalClaims" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    &nbsp;&nbsp; &nbsp; &nbsp; Total Claim :  &nbsp;<asp:Label ID="lblTotalAmount" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                     &nbsp;&nbsp; &nbsp;&nbsp;Total Paid : &nbsp; <asp:Label ID="lblTotalPaid" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    &nbsp;&nbsp; &nbsp; &nbsp;Total Rejected :&nbsp;<asp:Label ID="lblTotalRejected" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    &nbsp;&nbsp; &nbsp; &nbsp;Total Balance :&nbsp;<asp:Label ID="lblTotalBalance" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:updatepanel id="UpdatePanel47" runat="server">
                        <contenttemplate>
                 <asp:Button ID="btnExport" runat="server" CssClass="button red small" Width="130px" visible="false"
                        OnClick="btnExport_Click"   Text="Export To Excel" Tooltip="Above Selected data export to Excel sheet" />

                  <asp:Button ID="btnDispense"   runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button red small" OnClick="btnDispense_Click" Text="Dispense" Visible="false"  />
                            </contenttemplate>
                     </asp:updatepanel>
            </td>


        </tr>

    </table>
                             </contenttemplate>
        </asp:updatepanel>
    <br />
    <table width="900px">
       <tr>
            <td style="width: 100px" class="lblCaption1">
                IDPayer <span style="color:red;">* </span>

            </td>

            <td  style="width: 200px">
                <asp:updatepanel id="UpdatePanel38" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtIDPayer" runat="server" CssClass="TextBoxStyle"   Width="120px" MaxLength="30"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>
              <td style="width: 100px" class="lblCaption1">
                Policy No <span style="color:red;">* </span>

            </td>

            <td   >
                <asp:updatepanel id="UpdatePanel40" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtPolicyNo" runat="server" CssClass="TextBoxStyle"   Width="120px" MaxLength="30"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 70px" class="lblCaption1">
                Resub. No
            </td>
            <td>
                  <asp:updatepanel id="UpdatePanel41" runat="server">
                    <contenttemplate>
                  <asp:TextBox ID="txtResubNo" runat="server" CssClass="TextBoxStyle"   Width="120px" MaxLength="30" ReadOnly="true"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>

        </tr>

        <tr>
            <td style="width: 100px; height: 30px;" class="lblCaption1" >
                 Type
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel37" runat="server">
                    <contenttemplate>
                <asp:DropDownList ID="drpClmType" runat="server" CssClass="TextBoxStyle" Width="300px">
                    <asp:ListItem Value="1" Selected="True">No Bed + No Emergency Room (OP)</asp:ListItem>
                    <asp:ListItem Value="2">No Bed + Emergency Room (OP)</asp:ListItem>
                    <asp:ListItem Value="3">InPatient Bed + No Emergency Room (IP)</asp:ListItem>
                    <asp:ListItem Value="4">InPatient Bed + Emergency Room (IP)</asp:ListItem>
                    <asp:ListItem Value="5">Daycase Bed + No Emergency Room (Day Care)</asp:ListItem>
                    <asp:ListItem Value="6">Daycase Bed + Emergency Room (Day Care)</asp:ListItem>
                    <asp:ListItem Value="7">National Screening</asp:ListItem>
                      <asp:ListItem Value="12">Homecare</asp:ListItem>
                </asp:DropDownList>
                 </contenttemplate>
                </asp:updatepanel>

            </td>
            <td style="width: 100px; height: 40px;" class="lblCaption1">
               Start Date  <span style="color:red;">* </span>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel33" runat="server">
                    <contenttemplate>
                    <asp:TextBox ID="txtClmStartDate" runat="server" Width="75px" CssClass="TextBoxStyle" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:calendarextender id="CalendarExtender2" runat="server"
                        enabled="True" targetcontrolid="txtClmStartDate" format="dd/MM/yyyy">
                    </asp:calendarextender>
                    <asp:maskededitextender id="MaskedEditExtender5" runat="server" enabled="true" targetcontrolid="txtClmStartDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                    <asp:TextBox ID="txtClmFromTime" runat="server" Text="00:00" Width="30px" MaxLength="10"  CssClass="TextBoxStyle"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                 
            </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 100px" class="lblCaption1" >
               End Date  <span style="color:red;">* </span>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel34" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtClmEndDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:calendarextender id="CalendarExtender3" runat="server"
                    enabled="True" targetcontrolid="txtClmEndDate" format="dd/MM/yyyy">
                </asp:calendarextender>
                <asp:maskededitextender id="MaskedEditExtender6" runat="server" enabled="true" targetcontrolid="txtClmEndDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                 <asp:TextBox ID="txtClmToTime" runat="server" Text="00:00" Width="30px" MaxLength="10"  CssClass="TextBoxStyle"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>


        </tr>
        <tr>
            <td style="width: 100px; height: 30px;" class="lblCaption1" >
               Start Type
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel35" runat="server">
                    <contenttemplate>
                <asp:DropDownList ID="drpClmStartType" runat="server" CssClass="TextBoxStyle" Width="150px">
                    <asp:ListItem Value="1" Selected="True">Elective</asp:ListItem>
                    <asp:ListItem Value="2">Emergency</asp:ListItem>
                    <asp:ListItem Value="3">Transfer</asp:ListItem>
                    <asp:ListItem Value="4">Live Birth</asp:ListItem>
                    <asp:ListItem Value="5">Still Birth</asp:ListItem>
                    <asp:ListItem Value="6">Dead on Arrival</asp:ListItem>
                </asp:DropDownList>
                         </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 100px" class="lblCaption1" >
              End Type 
            </td>
            <td colspan="3">
                <asp:updatepanel id="UpdatePanel36" runat="server">
                    <contenttemplate>
                <asp:DropDownList ID="drpClmEndType" runat="server" CssClass="TextBoxStyle" Width="200px">
                    <asp:ListItem Value="1" Selected="True">Discharged with approval</asp:ListItem>
                    <asp:ListItem Value="2">Discharged against advice</asp:ListItem>
                    <asp:ListItem Value="3">Discharged absent without leave</asp:ListItem>
                    <asp:ListItem Value="4">Transfer to another facility</asp:ListItem>
                    <asp:ListItem Value="5">Deceased</asp:ListItem>

                </asp:DropDownList>
                         </contenttemplate>
                </asp:updatepanel>

            </td>
        </tr>
          <tr>
            <td style="width: 100px; height: 30px;" class="lblCaption1" >Resub. Type <span style="color:red;">* </span>
            </td>
            <td>   
                 <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpReType" runat="server" CssClass="TextBoxStyle"   Width="150px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpReType_SelectedIndexChanged">
                          
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
              <td style="width: 100px; height: 30px;" class="lblCaption1" >
                 Authorization ID
              </td>
              <td>
                   <asp:updatepanel id="UpdatePanel48" runat="server">
                    <contenttemplate>
                  <asp:TextBox ID="txtPriorAuthorizationID" runat="server" CssClass="TextBoxStyle"   Width="150px" MaxLength="30" ></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
              </td>
              <td></td>
              <td>
                    <asp:updatepanel id="UpdatePanel49" runat="server">
                    <contenttemplate>
                 <asp:CheckBox ID="chkResubVerified" runat="server" Text="Resubmission Verified"  Visible="false"   class="lblCaption1" Checked="false" AutoPostBack="true" OnCheckedChanged="chkResubVerified_CheckedChanged"   />
                          </contenttemplate>
                </asp:updatepanel>
              </td>
        </tr>
       <tr>
            <td style="width: 100px; height: 30px;" class="lblCaption1" >
                Attachment
            </td>
           <td >
                 <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="300px">
                                <asp:FileUpload ID="filAttachment" runat="server" Width="300px" CssClass="ButtonStyle" />
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnShow" />
                        </Triggers>
                    </asp:UpdatePanel>
           </td>
           <td colspan="2">
                 <asp:Button ID="btnAttaAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button gray small"
                                                            OnClick="btnAttaAdd_Click" Text="Add"   />
           </td>
       </tr>
        <tr>
             <td style="width: 100px; height: 30px;" class="lblCaption1" >
                 File Name
            </td>
            <td colspan="3">
                 <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblFileName" runat="server"    CssClass="label" visible="false"></asp:Label>
                          <asp:Label ID="lblFileNameActual" runat="server"    CssClass="lblCaption1"></asp:Label>
 &nbsp;&nbsp;&nbsp;&nbsp;
 <asp:ImageButton ID="DeleteAttach" runat="server" ToolTip="Delete" visible="false" ImageUrl="~/Images/icon_delete.png"
                                                OnClick="DeleteAttach_Click" />

                         </ContentTemplate>
                </asp:UpdatePanel>
              
                
            </td>
        </tr>
    </table>
    <br />
        <div runat="server" id="div1" style="padding-top: 0px; width: 1000px; height: 450px;   border: thin; border-color: #000; border-style: groove;">
        <asp:tabcontainer id="TabContainer2" runat="server" activetabindex="0" cssclass="AjaxTabStyle" width="100%">
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Services" Width="100%">
                <contenttemplate>
    <div runat="server" id="divTran" style="padding-top: 0px; width: 980px; height: 375px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
        <asp:updatepanel id="UpdatePanel5" runat="server"><contenttemplate>
                <asp:GridView ID="gvInvTran" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                    EnableModelValidation="True" Width="99%"   PageSize="200" OnRowDataBound="gvInvTran_RowDataBound">
                    <HeaderStyle CssClass="GridHeader_Gray" />
                    <RowStyle CssClass="GridRow" />
                   
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>
                        
                          <asp:TemplateField HeaderText="Invoice#" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="txtgvInvoiceID" runat="server" CssClass="TextBoxStyle"   HeaderStyle-Width="70px"  Height="30px"   Text='<%# Bind("HIT_INVOICE_ID") %>'  Readonly="true"  Width="50px" MaxLength="10"    ></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                           
                         <asp:TemplateField HeaderText="Code" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                   
                                  <asp:updatepanel id="UpdatePanel1" runat="server">
                                    <contenttemplate>
                                         <asp:Label ID="lblSlno" CssClass="GridRow"  visible="false"   runat="server" Text='<%# Bind("HIT_SLNO") %>' Width="70px"></asp:Label>
                                        <asp:Label ID="lblServCode" CssClass="GridRow"  visible="false"   runat="server" Text='<%# Bind("HIT_SERV_CODE") %>' Width="70px"></asp:Label>
                                        <asp:TextBox ID="txtServCode" runat="server" CssClass="TextBoxStyle"   HeaderStyle-Width="50px"  Height="30px"   Text='<%# Bind("HIT_SERV_CODE") %>'  Readonly="true"  Width="50px" MaxLength="10"    ></asp:TextBox>
                                    </contenttemplate>
                                       <Triggers>
                                                <asp:PostBackTrigger ControlID="txtServCode" />
                                            </Triggers>
                                 </asp:updatepanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Service Name" HeaderStyle-Width="300px">
                            <ItemTemplate>
                                 <asp:TextBox ID="txtServeDesc" runat="server" CssClass="TextBoxStyle"   Width="300px"   Height="30px"  Text='<%# Bind("HIT_DESCRIPTION") %>'  Readonly="true" ></asp:TextBox>
                                   
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="QTY" HeaderStyle-Width="30px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtQTY" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;" Width="30px"  Height="30px"  Text='<%# Bind("HIT_QTY") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Claim Amt." ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="txtgvClaimAmt" CssClass="TextBoxStyle"  runat="server" style="text-align:right;padding-right:5px;"  Width="70px"  Height="30px"  Text='<%# Eval("HIT_COMP_TOTAL", "{0:0.00}") %>'    Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PT. Share"  ItemStyle-VerticalAlign="Top" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtPTAmount" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;"  Width="70px"  Height="30px"  Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>'   Readonly="true"></asp:TextBox>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Paid Amt." ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="TextBox2" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;"  Width="70px"  Height="30px"   Text='<%# Eval("HIT_CLAIM_AMOUNT_PAID", "{0:0.00}") %>'     Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                               <asp:TextBox ID="TextBox1" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;"  Width="70px"  Height="30px" Text='<%# Eval("HIT_CLAIM_REJECTED", "{0:0.00}") %>'   Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Denial Code" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                  <asp:TextBox ID="txtDenial" runat="server"  CssClass="TextBoxStyle"  style="padding-right:5px;"  Width="70px"  Height="30px"   Text='<%# Bind("HIT_DENIAL_CODE") %>'  Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Denial Desc" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                
                               <asp:TextBox ID="txtDenialDesc" runat="server"  CssClass="TextBoxStyle"  style="padding-right:5px;"  Width="200px"  Height="30px"   Text='<%# Bind("HIT_DENIAL_DESC") %>'  Readonly="true"></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resubmit"  ItemStyle-VerticalAlign="Middle"  ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                  <asp:Label ID="lblSubmitted" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_RESUBMITTED") %>' visible="false" ></asp:Label>
                               <asp:CheckBox ID="chkSubmit" runat="server"    />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resubmission Comments" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:TextBox ID="txtResubComment" runat="server" CssClass="TextBoxStyle" Width="400px"   Height="30px"   TextMode="MultiLine"  Text='<%# Bind("TransReComment") %>'   ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtFee" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;" Width="70px"  Height="30px"  Text='<%# Eval("HIT_FEE", "{0:0.00}") %>'     Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblDiscType" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' visible="false" ></asp:Label>
                                  <asp:DropDownList ID="drpDiscType" CssClass="TextBoxStyle" runat="server" Width="50px" Height="30px" enabled="false">
                                        <asp:ListItem Value="$">$</asp:ListItem>
                                        <asp:ListItem Value="%">%</asp:ListItem>
                                  </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="D.Amt" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;" Width="70px"  Height="30px" Text='<%# Eval("HIT_DISC_AMT", "{0:0.00}") %>'    Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtHitAmount" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;" Width="70px"  Height="30px" Text='<%# Eval("HIT_AMOUNT", "{0:0.00}") %>'     Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ded." HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtDeduct" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;" Width="70px"  Height="30px" Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>'     Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Co-%" HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblCoInsType" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' visible="false" ></asp:Label>
                                  <asp:DropDownList ID="drpCoInsType" CssClass="TextBoxStyle" runat="server" Width="50px" Height="30px" enabled="false">
                                        <asp:ListItem Value="$">$</asp:ListItem>
                                        <asp:ListItem Value="%">%</asp:ListItem>
                                  </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtCoInsAmt" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;" Width="70px"  Height="30px" Text='<%# Eval("HIT_CO_INS_AMT", "{0:0.00}") %>'   Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                      
                         <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="txtHaadCode" runat="server" CssClass="TextBoxStyle"    Width="70px"  Height="30px"   Text='<%# Bind("HIT_HAAD_CODE") %>'  Readonly="true"></asp:TextBox>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Start Date" HeaderStyle-Width="100px">
                                   <ItemTemplate>
                                     <asp:TextBox ID="txtStartDt" runat="server"   Width="100px"  Height="30px"  Text='<%# Bind("HIT_STARTDATEDesc") %>'  Enabled="false"  style="border: thin; border-color: #CCCCCC; border-style: groove;"  ></asp:TextBox>
                                   </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Observ. Type" ItemStyle-VerticalAlign="Top" >
                            <ItemTemplate>
                                
                                <asp:Label ID="lblHIOType" CssClass="TextBoxStyle" style="padding-right:5px;" runat="server" Height="30px" Text='<%# Bind("HIO_TYPE") %>' Width="100px"></asp:Label>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Code" HeaderStyle-Width="100px"  >
                            <ItemTemplate>
                                      <asp:TextBox ID="txtHIOCode" runat="server" CssClass="TextBoxStyle" Height="30px"   Text='<%# Bind("HIO_CODE") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Description"  HeaderStyle-Width="150px" >
                            <ItemTemplate>
                                      <asp:TextBox ID="txtHIODesc" runat="server" CssClass="TextBoxStyle"  Height="30px"     Text='<%# Bind("HIO_DESCRIPTION") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Value" HeaderStyle-Width="100px" >
                            <ItemTemplate>
                                      <asp:TextBox ID="txtHIOValue" runat="server" CssClass="TextBoxStyle" Height="30px"   Text='<%# Bind("HIO_VALUE") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Observ. ValueType"  HeaderStyle-Width="150px">
                            <ItemTemplate>
                                      <asp:TextBox ID="txtHIOValueType" runat="server"  CssClass="TextBoxStyle"  Height="30px"   Text='<%# Bind("HIO_VALUETYPE") %>' Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                       
                    </Columns>

                </asp:GridView>
            
</contenttemplate>
</asp:updatepanel>



    </div>


                    
</contenttemplate>
                    

</asp:TabPanel>
               
                <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Diagnosis" Width="100%">
                <contenttemplate>

    <div runat="server" id="divICDDiagnosis" style="padding-left: 5px; padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #000; border-style: groove;">

        <br />
        <table width="100%">
            <tr>
                <td style="width: 100px" class="lblCaption1">
                   Principal 

                </td>
                <td colspan="3">
                    <asp:updatepanel id="UpdatePanel3" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtPrincipalCode" runat="server" CssClass="TextBoxStyle"   Width="100px" MaxLength="10"  onblur="return PrincipalCodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtPrincipalName" runat="server" CssClass="TextBoxStyle"  Width="772px" MaxLength="50"  onblur="return PrincipalNameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtPrincipalCode" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtPrincipalName" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                   Secondary1 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel4" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS1Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S1CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS1Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S1NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtS1Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtS1Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                    Secondary2 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel6" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS2Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S2CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS2Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S2NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="Server" TargetControlID="txtS2Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtS2Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>

            <tr>
                <td style="width: 100px" class="lblCaption1">
                    Secondary3 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel8" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS3Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S3CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS3Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S3NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender9" runat="Server" TargetControlID="txtS3Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender10" runat="Server" TargetControlID="txtS3Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                   Secondary4 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel9" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS4Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S4CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS4Name" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50"  onblur="return S4NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender11" runat="Server" TargetControlID="txtS4Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender12" runat="Server" TargetControlID="txtS4Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>

            <tr>
                <td style="width: 100px" class="lblCaption1">
                    Secondary5 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel10" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS5Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S5CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS5Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S5NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender13" runat="Server" TargetControlID="txtS5Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender14" runat="Server" TargetControlID="txtS5Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                     Secondary6 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel11" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS6Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S6CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS6Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S6NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender15" runat="Server" TargetControlID="txtS6Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender16" runat="Server" TargetControlID="txtS6Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                    Secondary7 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel12" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS7Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S7CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS7Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S7NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender17" runat="Server" TargetControlID="txtS7Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender18" runat="Server" TargetControlID="txtS7Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                   Secondary8 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel13" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS8Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S8CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS8Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S8NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender19" runat="Server" TargetControlID="txtS8Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender20" runat="Server" TargetControlID="txtS8Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>

            <tr>
                <td style="width: 100px" class="lblCaption1">
                     Secondary9 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel14" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS11Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S11CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS11Name" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50"  onblur="return S11NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender21" runat="Server" TargetControlID="txtS11Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender22" runat="Server" TargetControlID="txtS11Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                   Secondary10 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel15" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS12Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S12CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS12Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S12NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender23" runat="Server" TargetControlID="txtS12Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender24" runat="Server" TargetControlID="txtS12Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>


            <tr>
                <td style="width: 100px" class="lblCaption1">
                    Secondary11 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel16" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS13Code" runat="server" CssClass="TextBoxStyle" BorderWidth="1px"   Width="100px" MaxLength="10"  onblur="return S13CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS13Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S13NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender25" runat="Server" TargetControlID="txtS13Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender26" runat="Server" TargetControlID="txtS13Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                    Secondary12 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel17" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS14Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S14CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS14Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S14NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender27" runat="Server" TargetControlID="txtS14Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender28" runat="Server" TargetControlID="txtS14Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>


            <tr>
                <td style="width: 100px" class="lblCaption1">
                   Secondary13 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel18" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS15Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S15CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS15Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S15NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender29" runat="Server" TargetControlID="txtS15Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender30" runat="Server" TargetControlID="txtS15Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                   Secondary14 
                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel20" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS16Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S16CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS16Name" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50"  onblur="return S16NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender35" runat="Server" TargetControlID="txtS16Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender36" runat="Server" TargetControlID="txtS16Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                    econdary15 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel21" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS17Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S17CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS17Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S17NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender37" runat="Server" TargetControlID="txtS17Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender38" runat="Server" TargetControlID="txtS17Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                     Secondary16 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel22" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS18Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S18CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS18Name" runat="server" CssClass="TextBoxStyle" BorderWidth="1px"   Width="270px" MaxLength="50"  onblur="return S18NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender39" runat="Server" TargetControlID="txtS18Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender40" runat="Server" TargetControlID="txtS18Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                    Secondary17 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel23" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS19Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S19CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS19Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S19NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender41" runat="Server" TargetControlID="txtS19Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender42" runat="Server" TargetControlID="txtS19Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                   Secondary18 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel24" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS20Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S20CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS20Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S20NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender43" runat="Server" TargetControlID="txtS20Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender44" runat="Server" TargetControlID="txtS20Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>

            <tr>
                <td style="width: 100px" class="lblCaption1">
                   Secondary19 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel25" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS21Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S21CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS21Name" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50"  onblur="return S21NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender45" runat="Server" TargetControlID="txtS21Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender46" runat="Server" TargetControlID="txtS21Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                     Secondary20 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel26" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS22Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S22CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS22Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S22NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender47" runat="Server" TargetControlID="txtS22Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender48" runat="Server" TargetControlID="txtS22Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>

            <tr>
                <td style="width: 100px" class="lblCaption1">
                     Secondary21

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel27" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS23Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S23CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS23Name" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50"  onblur="return S23NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender49" runat="Server" TargetControlID="txtS23Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender50" runat="Server" TargetControlID="txtS23Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                  Secondary22 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel28" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS24Code" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return S24CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS24Name" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50"  onblur="return S24NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender51" runat="Server" TargetControlID="txtS24Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender52" runat="Server" TargetControlID="txtS24Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                    Secondary23 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel29" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS25Code" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10"  onblur="return S25CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS25Name" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50"  onblur="return S25NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender53" runat="Server" TargetControlID="txtS25Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender54" runat="Server" TargetControlID="txtS25Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                    Secondary24 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel30" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS26Code" runat="server" CssClass="TextBoxStyle"    Width="100px" MaxLength="10"  onblur="return S26CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS26Name" runat="server" CssClass="TextBoxStyle"    Width="270px" MaxLength="50"  onblur="return S26NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender55" runat="Server" TargetControlID="txtS26Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender56" runat="Server" TargetControlID="txtS26Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                     Secondary25 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel31" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS27Code" runat="server" CssClass="TextBoxStyle"    Width="100px" MaxLength="10"  onblur="return S27CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS27Name" runat="server" CssClass="TextBoxStyle" BorderWidth="1px"   Width="270px" MaxLength="50"  onblur="return S27NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender57" runat="Server" TargetControlID="txtS27Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender58" runat="Server" TargetControlID="txtS27Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
                <td style="width: 100px" class="lblCaption1">
                     Secondary26 

                </td>
                <td>
                    <asp:updatepanel id="UpdatePanel32" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtS28Code" runat="server" CssClass="TextBoxStyle"    Width="100px" MaxLength="10"  onblur="return S28CodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtS28Name" runat="server" CssClass="TextBoxStyle" BorderWidth="1px"   Width="270px" MaxLength="50"  onblur="return S28NameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender59" runat="Server" TargetControlID="txtS28Code" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender60" runat="Server" TargetControlID="txtS28Name" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px" class="lblCaption1">
                   Admitting 

                </td>
                <td colspan="3">
                    <asp:updatepanel id="UpdatePanel19" runat="server">
                        <contenttemplate>
                            <asp:TextBox ID="txtAdmitCode" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10"  onblur="return AdmitCodeSel()"  ></asp:TextBox>
                            <asp:TextBox ID="txtAdmitName" runat="server" CssClass="TextBoxStyle"    Width="772px" MaxLength="50"  onblur="return AdmitNameSel()"   ></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender31" runat="Server" TargetControlID="txtAdmitCode" MinimumPrefixLength="1" ServiceMethod="GetICDCode"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender32" runat="Server" TargetControlID="txtAdmitName" MinimumPrefixLength="1" ServiceMethod="GetICDName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                    </asp:updatepanel>
                </td>
            </tr>

        </table>

    </div>

                      
</contenttemplate>
                    

</asp:TabPanel>
             <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="History" Width="100%">
                <contenttemplate>
                    <div runat="server" id="divHistory" style="padding-left: 5px; padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
<asp:updatepanel id="UpdatePanel46" runat="server">
            <contenttemplate>
                <span class="lblCaption1">Services Details</span><br />
        <asp:GridView ID="gvResubTransHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                    EnableModelValidation="True" Width="99%"   PageSize="200" >
                    <HeaderStyle CssClass="GridHeader_Gray" />
                    <RowStyle CssClass="GridRow" />
                   
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="Resub. No." ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="TextBox24" runat="server" CssClass="TextBoxStyle"  style="border:none;" HeaderStyle-Width="100px"    BorderWidth="1px"  Text='<%# Bind("ResubNo") %>'  Readonly="true"  Width="100px" MaxLength="10"    ></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Inv Code" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="TextBox3" runat="server" CssClass="TextBoxStyle"  style="border:none;" HeaderStyle-Width="100px"   BorderWidth="1px"  Text='<%# Bind("HIT_INVOICE_ID") %>'  Readonly="true"  Width="100px" MaxLength="10"    ></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                           
                         <asp:TemplateField HeaderText="Service Code" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                        <asp:Label ID="Label1" CssClass="GridRow"  visible="false"   runat="server" Text='<%# Bind("HIT_SERV_CODE") %>' Width="70px"></asp:Label>
                                        <asp:TextBox ID="TextBox4" runat="server" CssClass="TextBoxStyle"  style="border:none;" HeaderStyle-Width="100px"  BorderWidth="1px"  Text='<%# Bind("HIT_SERV_CODE") %>'  Readonly="true"  Width="100px" MaxLength="10"    ></asp:TextBox>
                                     
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Service Name" HeaderStyle-Width="300px">
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox5" runat="server" CssClass="TextBoxStyle"  style="border:none;" Width="300px"    Text='<%# Bind("HIT_DESCRIPTION") %>'  Readonly="true" ></asp:TextBox>
                                   
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="QTY" HeaderStyle-Width="30px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox6" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;border:none;" Width="30px"   Text='<%# Bind("HIT_QTY") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Claim Amt." ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="TextBox7" CssClass="TextBoxStyle"  runat="server" style="text-align:right;padding-right:5px;border:none;"  Width="70px"   Text='<%# Eval("HIT_COMP_TOTAL", "{0:0.00}") %>'    Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share"  ItemStyle-VerticalAlign="Top" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox8" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;border:none;"  Width="70px"   Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>'   Readonly="true"></asp:TextBox>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Paid Amt." ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                   <asp:TextBox ID="TextBox9" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;border:none;"  Width="70px"    Text='<%# Eval("HIT_CLAIM_AMOUNT_PAID", "{0:0.00}") %>'     Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                               <asp:TextBox ID="TextBox10" runat="server" CssClass="TextBoxStyle"  style="text-align:right;padding-right:5px;border:none;"  Width="70px"  Text='<%# Eval("HIT_CLAIM_REJECTED", "{0:0.00}") %>'   Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Denial Code" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                  <asp:TextBox ID="TextBox11" runat="server"  CssClass="TextBoxStyle"  style="padding-right:5px;border:none;"  Width="70px"    Text='<%# Bind("HIT_DENIAL_CODE") %>'  Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Denial Desc" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                
                               <asp:TextBox ID="TextBox12" runat="server"  CssClass="TextBoxStyle"  style="padding-right:5px;border:none;"  Width="200px"    Text='<%# Bind("HIT_DENIAL_DESC") %>'  Readonly="true"></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resub Type"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                  <asp:Label ID="Label3" CssClass="GridRow"  runat="server"   Width="100px" Text='<%# Bind("TransReType") %>'   ></asp:Label>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resubmission Comments" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox13" runat="server" CssClass="TextBoxStyle" Width="400px"   style="border:none;"   TextMode="MultiLine"  Text='<%# Bind("TransReComment") %>'  Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox14" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;border:none;" Width="70px"   Text='<%# Eval("HIT_FEE", "{0:0.00}") %>'     Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="Label4" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>'  Width="70px" ></asp:Label>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="D.Amt" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox15" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Text='<%# Eval("HIT_DISC_AMT", "{0:0.00}") %>'    Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox16" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Text='<%# Eval("HIT_AMOUNT", "{0:0.00}") %>'     Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ded." HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox17" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Text='<%# Eval("HIT_DEDUCT", "{0:0.00}") %>'     Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Co-%" HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="Label5" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' Width="70px"  ></asp:Label>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox18" runat="server" CssClass="TextBoxStyle" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Text='<%# Eval("HIT_CO_INS_AMT", "{0:0.00}") %>'   Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                      
                         <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:TextBox ID="TextBox19" runat="server" CssClass="TextBoxStyle" style=";border:none;"  Width="70px"    Text='<%# Bind("HIT_HAAD_CODE") %>'  Readonly="true"></asp:TextBox>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Observ. Type" ItemStyle-VerticalAlign="Top" >
                            <ItemTemplate>
                                
                                <asp:Label ID="Label7" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("HIO_TYPE") %>' Width="70px"></asp:Label>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Code" HeaderStyle-Width="100px"  >
                            <ItemTemplate>
                                      <asp:TextBox ID="TextBox20" runat="server" CssClass="TextBoxStyle" style="border:none;"   Text='<%# Bind("HIO_CODE") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Description"  HeaderStyle-Width="150px" >
                            <ItemTemplate>
                                      <asp:TextBox ID="TextBox21" runat="server" CssClass="TextBoxStyle"  style="border:none;"     Text='<%# Bind("HIO_DESCRIPTION") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Value" HeaderStyle-Width="100px" >
                            <ItemTemplate>
                                      <asp:TextBox ID="TextBox22" runat="server" CssClass="TextBoxStyle"  style="border:none;"   Text='<%# Bind("HIO_VALUE") %>' Readonly="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Observ. ValueType"  HeaderStyle-Width="150px">
                            <ItemTemplate>
                                      <asp:TextBox ID="TextBox23" runat="server"  CssClass="TextBoxStyle"  style="border:none;"   Text='<%# Bind("HIO_VALUETYPE") %>' Readonly="true" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                       
                    </Columns>

                </asp:GridView>
                <br />
         <span class="lblCaption1">Diagnosis Details</span><br />
                                            <asp:GridView ID="gvResubClaimsHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    EnableModelValidation="True" Width="99%" gridline="none">
                                                                    <HeaderStyle CssClass="GridHeader_Gray" />
                                                                    <RowStyle CssClass="GridRow" />
                                                
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Resub. No." HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                        
                                                                                    <asp:Label ID="Label8" width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_RESUBNO") %>'  ></asp:Label>
                                        
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Code" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                        
                                                                                    <asp:Label ID="lblICDCode" width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_CODE") %>'  ></asp:Label>
                                        
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Description">
                                                                            <ItemTemplate>
                                        
                                                                                    <asp:Label ID="lblICDDesc" width="400px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_DESC") %>'></asp:Label>
                                      
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                                         <asp:TemplateField HeaderText="Type">
                                                                            <ItemTemplate>
                                                                          
                                                                                    <asp:Label ID="lblICDType" width="70px" CssClass="GridRow" runat="server" Text='<%# Bind("HIC_ICD_TYPE") %>'></asp:Label>
                                      
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                               
                                                            </Columns>
                                                        </asp:GridView>
                                 </contenttemplate>
        </asp:updatepanel>
                    </div>
                     
                
</contenttemplate>
                    

</asp:TabPanel>
            </asp:tabcontainer>
</div>
    
    <div>
        <table width="800px" align="center">
            <tr>
                <td>
                    <asp:Button ID="btnTestEclaim" runat="server" CssClass="button gray small" Width="150px"  
                        OnClick="btnTestEclaim_Click" Text="Test E-Claim" />
                </td>
                <td>
                    <asp:Button ID="btnProductionEclaim" runat="server" CssClass="button gray small" Width="150px"  
                        OnClick="btnProductionEclaim_Click" Text="Production E-Claim" />
                </td>
                 <td>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="150px"  
                        OnClick="btnDelete_Click" OnClientClick="return DeleteConfirm();" Text="Delete" />

                </td>
                <td>
                    <asp:Button ID="btnUpdate" runat="server" CssClass="button gray small" Width="150px"  
                        OnClick="btnUpdate_Click" OnClientClick="return SaveVal1();" Text="Update" />

                </td>

                <td>
                    <asp:Button ID="btnSaveToDB" runat="server" CssClass="button red small" Width="150px"
                        OnClick="btnSaveToDB_Click" OnClientClick="return SaveVal();" Text="Resubmssion Generate" />

                </td>
            </tr>
            <tr>
                <td style="height:50px;" colspan="4"></td>
            </tr>
        </table>

    </div>
   
    
     </asp:Content>

