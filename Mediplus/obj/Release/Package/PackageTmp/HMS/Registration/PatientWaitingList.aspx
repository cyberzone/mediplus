﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PatientWaitingList.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientWaitingList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script language="javascript" type="text/javascript">
        function ShowDelete() {
            window.confirm('Do you want to delete?')
        }

    </script>

    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
            // alert(today);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div>
        <input type="hidden" id="hidPermission" runat="server" value="9" />
        <asp:HiddenField ID="hidLocalDate" runat="server" />
        <table>
            <tr>
                <td class="PageHeader">Patient Waiting List
               
                </td>
            </tr>
        </table>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="600px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>


        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table width="85%">
                        <tr>


                            <td class="lblCaption1">Status &nbsp;:&nbsp;
                             <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged">
                                 <asp:ListItem Value="">--- All ---</asp:ListItem>
                                 <asp:ListItem Text="Waiting" Value="W" Selected="true"></asp:ListItem>
                                 <asp:ListItem Text="Invoiced" Value="I"></asp:ListItem>
                                 <asp:ListItem Text="Completed" Value="C"></asp:ListItem>
                             </asp:DropDownList>
                            </td>

                            <td class="lblCaption1">Doctor  &nbsp;:&nbsp;
                            <asp:DropDownList ID="drpDoctor" runat="server" CssClass="TextBoxStyle" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                            <td class="lblCaption1">Patient Type  &nbsp;:&nbsp;
                             <asp:DropDownList ID="drpPatientType" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC" AutoPostBack="true" OnSelectedIndexChanged="drpPatientType_SelectedIndexChanged">
                                 <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                                 <asp:ListItem Value="CA">Cash</asp:ListItem>
                                 <asp:ListItem Value="CR">Credit</asp:ListItem>
                             </asp:DropDownList>
                            </td>
                            <td   >
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnRefresh" runat="server" CssClass="button red small" Width="70px"
                                            Text="Refresh" OnClick="btnRefresh_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            
                        </tr>

                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <div style="padding-top: 0px; width: 82%; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting" OnRowDataBound="gvGridView_RowDataBound"
                        EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200" GridLines="None">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />

                        <Columns>
                              <asp:TemplateField HeaderText="Actions" Visible="true" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                        OnClick="Select_Click" />&nbsp;&nbsp;
                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="../Images/icon_delete.png" AlternateText="Delete"
                                            OnClick="Delete_Click" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="No"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                   
                                        <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                  
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                  
                                        <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                        <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                 </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="HPV_PT_NAME">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="Label2" CssClass="GridRow" Width="250px" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                    
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Mobile" SortExpression="HPV_MOBILE">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="Label3" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("HPV_MOBILE") %>'></asp:Label>
                                     
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Doctor" SortExpression="HPV_DR_NAME">
                                <ItemTemplate>
                                  
                                        <asp:Label ID="Label4" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dept." SortExpression="HPV_DR_NAME">
                                <ItemTemplate>
                                    
                                        <asp:Label ID="Label5" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time" SortExpression="HPV_TIME">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="Label7" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="HPV_PT_TYPEDesc">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="lblType" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                                <ItemTemplate>
                                    
                                        <asp:Label ID="Label8" CssClass="GridRow" Width="250px" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>
                                    
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Token" SortExpression="HPV_DR_ID">
                                <ItemTemplate>
                                    
                                        <asp:Label ID="Label1" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Inv.No." SortExpression="HPV_INV_NO">
                                <ItemTemplate>
                                    
                                        <asp:Label ID="Label10" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_INV_NO") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Inv.Amt." SortExpression="HPV_INV_AMT">
                                <ItemTemplate>
                                    
                                        <asp:Label ID="Label11" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_INV_AMT") %>'></asp:Label>
                                    
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Appnt.Time" SortExpression="HPV_APPOINT_TIME">
                                <ItemTemplate>
                                    
                                        <asp:Label ID="Label12" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_APPOINT_TIME") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Date" SortExpression="HPV_DATE">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="Label13" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_DATEDesc") %>'></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job No." SortExpression="HPV_JOBNUMBER" Visible="false">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="Label14" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_JOBNUMBER") %>'></asp:Label>
                                     
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User" SortExpression="HPV_CREATED_USER">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="lblUser" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("HPV_CREATED_USER") %>'></asp:Label>
                                     
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

    </div>

    <table width="1000PX">
        <tr>
            <td class="style2">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="Label9" runat="server" CssClass="lblCaption1" Text="Total No. Patient in Waiting List:"></asp:Label>
                        &nbsp;
                <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

        </tr>
    </table>
</asp:Content>

