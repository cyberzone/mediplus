﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="PatientDocScans.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientDocScans" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />

       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

     <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

       
    </style>

   <script language="javascript" type="text/javascript">

       function OnlyNumeric(evt) {
           var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
           if (chCode >= 48 && chCode <= 57 ||
                chCode == 46) {
               return true;
           }
           else

               return false;
       }



       function PatientPopup(CtrlName, strValue) {

           var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=PTDocScan&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
           win.focus();

           return true;

       }

       function StaffPopup(CtrlName, strValue) {
           var win = window.open("../Masters/Doctor_Name_Zoom.aspx?PageName=PTDocScan&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=570,width=1100,toolbar=no,scrollbars==yes,menubar=no");
           win.focus();

           return true;

       }

       function DRIdSelected() {
           if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                  var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                  var Data1 = Data.split('~');
                  if (Data1.length > 1) {
                      document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the File No";
                return false;
            }

            if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Date";
                return false;
            }


            if (document.getElementById('<%=txtFromDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtFromDate.ClientID%>').focus()
                    return false;
                }
            }


            if (document.getElementById('<%=drpCategory.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                return false;
            }

            if (document.getElementById('<%=drpDepartment.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Department";
                return false;
            }



        }

        </script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
     <table>
        <tr>
            <td class="PageHeader">EMR Scan Screen
               
            </td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
     <div style="padding: 5px; width: 900px; border: thin; border-color: #cccccc; border-style: groove;">
         <table width="1000px" border="0">
                 <tr>
                         <td class="lblCaption1" style="width:105px;height:20px;" >
                              File No
                          </td>
                          <td>
                               <asp:TextBox ID="txtFileNo" runat="server" Width="100px" MaxLength="10" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="Red" ondblclick="return PatientPopup('FileNo',this.value);" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"      ></asp:TextBox>
                          </td>
                         <td class="lblCaption1" >
                             Name
                          </td>
                         <td>
                          <asp:TextBox ID="txtName" runat="server" Width="375px" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>
                        </td>

                </tr>
                 <tr>
                        <td class="lblCaption1" style="height:20px;" >
                             Date
                        </td>
                        <td>
                             <asp:TextBox ID="txtFromDate" runat="server"   Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                            enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                        </asp:calendarextender>
                        <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                        </td>
                        <td class="lblCaption1" >
                            Category
                        </td>
                        <td class="lblCaption1" >
                              <asp:dropdownlist id="drpCategory" cssclass="label" bordercolor="#e3f7ef" borderstyle="Groove" runat="server" width="150px">
                                  </asp:dropdownlist>
                          &nbsp;&nbsp;Department
                             <asp:DropDownList ID="drpDepartment" runat="server" CssClass="label" Width="150px"  bordercolor="#e3f7ef" borderstyle="Groove" ></asp:DropDownList>

                        </td>
                </tr>
                 <tr>
                  <td class="lblCaption1" style="height:20px;" >
                      Doctor
                 </td>
                 <td colspan="3">
                      <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDoctorID" runat="server" CssClass="label"  BorderColor="#CCCCCC" BorderWidth="1" Width="100px"   AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()" onKeyUp="DoUpper(this);"  ></asp:TextBox>
                            <asp:TextBox ID="txtDoctorName" runat="server" CssClass="label" BorderColor="#CCCCCC" BorderWidth="1" AutoPostBack="true" OnTextChanged="txtDoctorName_TextChanged" Width="535px" MaxLength="50" onblur="return DRNameSelected()" onKeyUp="DoUpper(this);" ></asp:TextBox>
                           <div ID="divwidth" style="visibility:hidden;"></div>
                             <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"
                                  CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                                  CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth"></asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                 </td>
             </tr>
             <tr>
                  <td class="lblCaption1" style="height:40px;" >
                      EMR ID
                 </td>
                 <td>
                  <asp:TextBox ID="txtEMRId" runat="server" Width="100px"   CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>
                 </td>
                 <td class="lblCaption1" >
                      Comments
                 </td>
                 <td>
                  <asp:TextBox ID="txtComment" runat="server" Width="380px"   CssClass="label"       BorderWidth="1px" BorderColor="#cccccc"  Height="30px" TextMode="MultiLine"  ></asp:TextBox>
                 </td>
             </tr>
               <tr>
                  <td class="lblCaption1" style="height:40px;" >
                      Invoice ID
                 </td>
                 <td>
                  <asp:TextBox ID="txtInvoiceID" runat="server" Width="100px"   CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>
                 </td>
                 <td class="lblCaption1" >
                 </td>
                 <td>
                 </td>
             </tr>
             <tr>
                 <td>

                 </td>
                 <td colspan="3">
                     <asp:updatepanel id="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="300px">
                            <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                        </asp:Panel>
                    </ContentTemplate>
                  
                </asp:updatepanel>
                 </td>
             </tr>
             <tr>
                 <td colspan="4">
                     <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSave_Click"   OnClientClick="return SaveVal();" Text="Save" />
                     <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click"     Text="Clear" />

                 </td>
             </tr>
        </table>

    </div>
    </div>
                 
     

   </asp:Content>
