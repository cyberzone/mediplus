﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="AppointmentDoctor.aspx.cs" Inherits="Mediplus.HMS.Registration.AppointmentDoctor" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">

        function PatientPopup1(AppId, strTime, DRId, DeptId) {
            var strDate;
            strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
            var win = window.open("AppointmentDrPopup.aspx?PageName=ApptDr&AppId=" + AppId + "&Date=" + strDate + "&Time=" + strTime + "&DrId=" + DRId + "&DeptID=" + DeptId, "newwin", "top=200,left=270,height=450,width=750,toolbar=no,scrollbars=no,menubar=no");
            win.focus();
        }

        function NewAppointmentPopup() {
            var strDate;
            strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
            var win = window.open("AppointmentDrPopup.aspx?PageName=ApptDay&AppId=0&Date=" + strDate + "&Time=0&DrId=0", "newwin", "top=200,left=270,height=450,width=750,toolbar=no,scrollbars=no,menubar=no");
            win.focus();
        }
    </script>

    <style type="text/css">
        .Header {
            font: bold 11px/100% Arial;
            background-color: #4fbdf0;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            border-color: black;
          
        }

        .RowHeader {
            font: bold 11px/100% Arial;
            height: 15px;
            width:100px;
            text-align: center;
            border-color: black;
        }

        .Content {
            font: 11px/100% Arial;
            text-decoration: none;
            color: black;
            text-align: center;
            vertical-align: middle;
        }

         .AppBox
        {
            width:300px;
        }

        .DrBox
        {
            font: bold 11px/100% Arial;
            background-color: #4fbdf0;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            border-color: black;
             width:305px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table >
        <tr>
            <td class="PageHeader">Appointment - Doctor
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="80%">
        <tr>
            <td style="width: 170px;">
                <asp:HiddenField ID="hidStatus" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <contenttemplate>
                 <asp:Button ID="btnNewApp" runat="server" style="padding-left:5px;padding-right:5px;width:50px" Text="New" CssClass="button orange small"     OnClientClick="return NewAppointmentPopup()" />
                     </contenttemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 50px;" class="lblCaption1">Date:
            </td>
            <td align="left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" height="22px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <asp:calendarextender id="TextBox1_CalendarExtender" runat="server" 
                    enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                </asp:calendarextender>
            </td>
             <td style="width: 100px;" class="lblCaption1"> 
                
                   <asp:Label ID="lblFilterCaption" runat="server"   CssClass="lblCaption1" Text="Department"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drpDepartment" runat="server" Width="200px" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpDepartment_SelectedIndexChanged"></asp:DropDownList>
               <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged" BorderColor="#cccccc" Width="300px" visible="false">
                </asp:DropDownList>
            </td>
            <td align="right">
              

            </td>

        </tr>
    </table>
         
   <table style="float: left; width: 90%;" cellpadding="5" cellspacing="5" >
            <tr>
                <td>
                    <Table border='1' style='border-color:black;' >
                        
                        <%=strDataDr%>
                    </Table>

                </td>
            </tr>

        </table>
        
    <div style="padding-top: 0px; width: 90%; height: 600px;  border: thin; border-color: #CCCCCC; border-style: solid;overflow-x: scroll;
    overflow-y: scroll;">
        <table   cellpadding="5" cellspacing="5">
            <tr class="grid_header">
                <td>
                    <Table border='1' style='border-color:black;' >
                        
                        <%=strData%>
                    </Table>

                </td>
            </tr>

        </table>
     </div>
    <br />  <br />  <br />  <br />
</asp:Content>
