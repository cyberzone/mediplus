﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="PatientFilingRequest.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientFilingRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>


 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />
    
     
       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <script language="javascript" type="text/javascript">
        function ShowFilingRequest() {
            window.confirm('Are you sure to remove the from Filing Request')
        }

        function HistoryPopup(PtId) {
            var win = window.open("PatientVisitHistory.aspx?Value=" + PtId, "newwin1", "top=200,left=300,height=500,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();


        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <div>
      <table>
        <tr>
            <td class="PageHeader">Filing Request
               
            </td>
        </tr>
    </table>
    <br />
    <table width="1000PX">
         <tr>
            <td class="style2">
                 <asp:Label ID="Label9" runat="server" CssClass="lblCaption1"  Text="Total No. Patient in Filing Request:"></asp:Label>
                            &nbsp;
                <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
            </td>
             <td  align="right">
                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnRefresh" runat="server" CssClass="button orange small" Width="70px"
                                        Text="Refresh" OnClick="btnRefresh_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
             </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 1000PX; height: 500px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                 <input type="hidden" id="hidPermission" runat="server" value="9" />
                <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" Width="100%"
                    AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting" OnRowDataBound="gvGridView_RowDataBound"
                    EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200"  gridlines="None">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>
                       <asp:TemplateField HeaderText="" >
                            <ItemTemplate>
                                    <a  href="javascript:HistoryPopup('<%# Eval("HPV_PT_Id") %>')" >
                                    <asp:Label ID="lblHistory" CssClass="GridRow" Width="60px"  style="text-align:center;padding-right:2px;" runat="server" Text='HISTORY' ></asp:Label>
                                     </a>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No" SortExpression="HPV_SEQNO"  ItemStyle-HorizontalAlign="Center"  >
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                      <asp:Label ID="lblTriageReason" CssClass="GridRow" Width="40px" style="text-align:center;padding-right:2px;" runat="server" Text='<%# Bind("HPV_TRIAGE_REASON") %>'  Visible="false" ></asp:Label>
                                    <asp:Label ID="lblDeptName" CssClass="GridRow" Width="40px" style="text-align:center;padding-right:2px;" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'  Visible="false" ></asp:Label>

                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" style="text-align:center;padding-right:2px;" runat="server" Text='<%# Bind("HPV_SEQNO") %>' ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id">
                            <ItemTemplate>
                                 <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" SortExpression="HPV_PT_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="Label2" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Doctor" SortExpression="HPV_DR_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="Label4" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time" SortExpression="HPV_TIME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkTime" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="Label7" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Mobile No" SortExpression="HPV_MOBILE">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkMob" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="Label3" CssClass="GridRow" Width="150px"   runat="server" Text='<%# Bind("HPV_MOBILE") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="PT Type" SortExpression="HPV_PT_TYPEDesc">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkType" runat="server" OnClick="Edit_Click" OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="Label5" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCompName" runat="server" OnClick="Edit_Click"  OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="Label8" CssClass="GridRow" Width="250px"   runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Created User" SortExpression="HPV_CREATED_USER">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkUser" runat="server" OnClick="Edit_Click" OnClientClick="javascript:return confirm('Are you sure to remove from Filing Request')">
                                    <asp:Label ID="lblUser" CssClass="GridRow" Width="150px"  runat="server" Text='<%# Bind("CreatedUser") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table width="900PX">
                    <tr>
                       
                        <td class="lblCaption1">
                                Doctory 
                        </td>
                        <td class="lblCaption1">
                            Dept 
                        </td>
                        <td class="lblCaption1">
                            Patient Type
                        </td>
                       
                    </tr>
                    <tr>
                        <td class="style2">
                           
                             <asp:DropDownList ID="drpDoctor" runat="server" CssClass="label" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                          <td class="style2">
                           
                            <asp:DropDownList ID="drpDepartment" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="drpDepartment_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td>
                               <asp:DropDownList ID="drpPatientType" runat="server" CssClass="label" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC"  AutoPostBack="true" OnSelectedIndexChanged="drpPatientType_SelectedIndexChanged" >
                                 <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                             </asp:DropDownList>
                        </td>
                                              

                    </tr>

                </table>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    </div>
                </asp:Content>