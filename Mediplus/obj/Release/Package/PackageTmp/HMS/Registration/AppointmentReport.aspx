﻿<%@ page title="" language="C#" masterpagefile="~/Site2.Master" autoeventwireup="true" codebehind="AppointmentReport.aspx.cs" inherits="Mediplus.HMS.Registration.AppointmentReport" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the From Date";
                 document.getElementById('<%=txtFromDate.ClientID%>').focus;
                 return false;

             }
             if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtFromDate.ClientID%>').focus()
                return false;
            }


            if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the To Date";
                document.getElementById('<%=txtToDate.ClientID%>').focus;
                return false;

            }

            if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtToDate.ClientID%>').focus()
                return false;
            }





            return true;
        }


        function ShowAppointment(DBString, FileNo, Status, DrId, Date1, Date2, ReportType) {
            var Report;
            
            if (ReportType=='OTAppointment') {

                Report = "OTAppointmentReport.rpt"; 
            }
            else {

                Report = "AppointmentReport.rpt";
            }

            var Formula = "";

            if (ReportType == 'OTAppointment') {

                Formula = Formula + '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_OTAPPOINTMENT}=true';
            }


            if (Date1 != "") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date1 + '\')';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date1 + '\')';
                }
            }


            if (Date2 != "") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date2 + '\')';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}<=date(\'' + Date2 + '\')';
                }
            }


           
            if (FileNo != "") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_FILENUMBER}=\'' + FileNo + '\'';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_FILENUMBER}=\'' + FileNo + '\'';
                }
            }
           
            if (Status != "0") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS}=\'' + Status + '\'';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS}=\'' + Status + '\'';
                }
            }

            if (DrId != "0") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_DR_CODE}=\'' + DrId + '\'';
                }
                else {
                    Formula = Formula + 'AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_DR_CODE}=\'' + DrId + '\'';
                }
            }

           // alert(Formula);
           
           // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '$' + Formula + '$SCREEN$$$1');


            var win = window.open('../CReports/HMSReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Formula, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }



        function ShowAppointmentMaster() {
            if (Val() == false) {
                return false;
            }

            var Report = "HmsApointmt.rpt";


            var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1 = "";

            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2 = "";
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            var FileNo = document.getElementById('<%=txtFileNo.ClientID%>').value

            var DrId = document.getElementById('<%=drpDoctor.ClientID%>').value

           var Type = document.getElementById('<%=drpType.ClientID%>').value



           if (Type == "DR") {
               Report = "HmsApointmt.rpt";

           }
           else {
               Report = "HmsApointmtSrv.rpt";
           }

           var Criteria = " 1=1 ";

           if (FileNo != "") {
               if (Type == "DR") {
                   Criteria += ' AND {HMS_APPOINTMENT_MASTER.HAM_FILE_NO}=\'' + FileNo + '\'';
               }
               else {

                   Criteria += ' AND {HMS_APPOINTMENT_MASTER_SRV.HAMS_FILE_NO}=\'' + FileNo + '\'';
               }
           }



           if (DrId != "0") {
               if (Type == "DR") {
                   Criteria += 'AND {HMS_APPOINTMENT_MASTER.HAM_STAFF_ID}=\'' + DrId + '\'';
               }
               else {
                   Criteria += 'AND {HMS_APPOINTMENT_MASTER_SRV.HAMS_STAFF_ID}=\'' + DrId + '\'';
               }
           }


           if (Type == "DR") {
               Criteria += ' AND  {HMS_APPOINTMENT_MASTER.HAM_APNT_DATE}>=date(\'' + Date1 + '\') AND  {HMS_APPOINTMENT_MASTER.HAM_APNT_DATE}<=date(\'' + Date2 + '\')'

           }
           else {
               Criteria += ' AND  {HMS_APPOINTMENT_MASTER_SRV.HAMS_APNT_DATE}>=date(\'' + Date1 + '\') AND  {HMS_APPOINTMENT_MASTER_SRV.HAMS_APNT_DATE}<=date(\'' + Date2 + '\')'
           }





           var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')


           win.focus();

       }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center">
        <tr>
            <td class="PageHeader">Appointment Report
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>

    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>

            <td style="width: 150px;height:40px;">
                <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                    Text="From"></asp:Label>
            </td>
            <td style="width: 400px">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                &nbsp;

 
                    <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                        Text="To"></asp:Label>

                <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


            </td>
            <td   >
                <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                    Text="File No"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtFileNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="150px" MaxLength="10" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="height:30px;">
                <asp:Label ID="Label6" runat="server" CssClass="lblCaption1"
                    Text="Doctor"></asp:Label>

            </td>
            <td>
                 <asp:DropDownList ID="drpType"  CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px">
                      <asp:ListItem Value="DR">Doctor Wise</asp:ListItem>
                       <asp:ListItem Value="SER">Service Wise</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="300px">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblCaption1" runat="server" CssClass="lblCaption1"
                    Text="Status"></asp:Label>

            </td>
            <td>
                <asp:DropDownList ID="drpStatus" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="height:30px;"></td>
            <td colspan="3">
               <asp:CheckBox ID="chkOTAppointment"   runat="server" CssClass="lblCaption1" Text="OT Appointment"   Checked="false"  />

            </td>
        </tr>
        <tr>
            <td colspan="4">

                <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="button orange small" Width="100px" OnClick="btnReport_Click"  OnClientClick="return Val();"/>

                <asp:Button ID="btnReportMaster" runat="server" Text="Report" CssClass="button orange small" Width="100px" OnClientClick="return ShowAppointmentMaster();"/>


            </td>
        </tr>
    </table>
</asp:Content>
