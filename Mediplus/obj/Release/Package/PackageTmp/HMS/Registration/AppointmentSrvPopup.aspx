﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppointmentSrvPopup.aspx.cs" Inherits="Mediplus.HMS.Registration.AppointmentSrvPopup" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />
    <title>Appointment</title>
     <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function passvalue(strDate, strPageName, DrId) {
            if (strPageName == "ApptSrv") {
                opener.location.href = "AppointmentService.aspx?Date=" + strDate;
            }
            
            opener.focus();
           // self.close();

        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }

        function val() {
            var label;
            label = document.getElementById('lblStatus');
            label.style.color = 'red';
            document.getElementById('lblStatus').innerHTML = " ";


            if (document.getElementById('txtFileNo').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the File No";
                document.getElementById('txtFileNo').focus();
                return false;
            }


            if (document.getElementById('txtFName').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the  Name";
                document.getElementById('txtFName').focus();
                return false;
            }

            if (document.getElementById('drpReptMode').value == "0") {
                document.getElementById('lblStatus').innerHTML = "Please select the Mode";
                document.getElementById('drpReptMode').focus();
                return false;
            }


            if (document.getElementById('txtFromDate').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the Date";
                document.getElementById('txtFromDate').focus();
                return false;
            }


            if (document.getElementById('txtFromDate').value != "") {
                if (isDate(document.getElementById('txtFromDate').value) == false) {
                    document.getElementById('txtFromDate').focus()
                    return false;
                }
            }





            return true



        }


        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=Appointment&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin1", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            return true;

        }

        function BindPTDetails(HPM_PT_ID, PatientName, Mobile, Email) {
            document.getElementById('txtFileNo').value = HPM_PT_ID;
            document.getElementById('txtFName').value = PatientName;
            document.getElementById('txtMobile').value = Mobile;

        }

    </script>
</head>
<body  >
    <form id="form1" runat="server">
        <div>
              <input type="hidden" id="hidPermission" runat="server" value="9" />
                <input type="hidden" id="hidPTType" runat="server"  />
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
             <asp:HiddenField ID="hidStatus" runat="server" />
            <div style="padding-top: 0px;">
               
                <table >
                <tr>
                    <td class="PageHeader">Appointment - Service
                    </td>
                </tr>
            </table>
                <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%"">
                    <tr>
                        <td>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </td>
                    </tr>

                </table>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1">
                            Doctor
                        </td>
                        <td>
                             <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="260px">
                            </asp:DropDownList>
                           <asp:ImageButton ID="btnDrAppMastShow" runat="server" ImageUrl="~/Images/zOOM.PNG" ToolTip="Doctor wise Appointment" Height="20PX" Width="25PX"   OnClick="btnDrAppMastShow_Click" />
                        </td>
                    </tr>
                    <tr>
                        
                         <td class="lblCaption1">
                            File No <span style="color:red;">* </span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFileNo" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="205px" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>
                             <asp:CheckBox ID="chkStatus" runat="server" Checked="true" CssClass="lblCaption1" Text="Active" />
                            <asp:ImageButton ID="btnPTAppMastShow" runat="server" ImageUrl="~/Images/zOOM.PNG" ToolTip="Patient wise Appointment" Height="20PX" Width="25PX"   OnClick="btnPTAppMastShow_Click" />

                        </td>
                         <td class="lblCaption1">
                            Appo. Type

                        </td>
                        <td>
                            <asp:DropDownList ID="drpService" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                       <td class="lblCaption1">
                           Name <span style="color:red;">* </span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFName" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="290px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                        </td>
                         <td class="lblCaption1">
                             Mode<span style="color:red;">* </span>
                        </td>
                        <td>
                             <asp:DropDownList ID="drpReptMode" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="250px">
                                
                                 <asp:ListItem Value="Phone">Phone</asp:ListItem>
                                 <asp:ListItem Value="Direct">Direct</asp:ListItem>
                                 <asp:ListItem Value="Email">Email</asp:ListItem>
                                   <asp:ListItem Value="SMS">SMS</asp:ListItem>
                                   <asp:ListItem Value="Doctor Reference">Doctor Reference</asp:ListItem>
                                   <asp:ListItem Value="Fax">Fax</asp:ListItem>
                                 <asp:ListItem Value="Others">Others</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">
                            PO Box&nbsp;&nbsp;&nbsp;
                        </td>
                         <td class="lblCaption1">
                            <asp:TextBox ID="txtPOBox" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="120px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                                 Res.No
                            <asp:TextBox ID="txtResNo" runat="server" CssClass="label" BorderWidth="1px"   BorderColor="#cccccc" Width="120px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>

                        </td>
                          <td class="lblCaption1">
                           
                        Date<span style="color:red;">* </span>
                        </td>
                         <td class="lblCaption1">
                             <asp:TextBox ID="txtFromDate" runat="server" Width="72px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                                enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                            </asp:calendarextender>
                            <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                           &nbsp; Time
                            <asp:DropDownList ID="drpSTHour" CssClass="label" style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpSTMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>            
                        </td>
                    </tr>
                    <tr >
                         <td class="lblCaption1">
                           Off.No&nbsp;&nbsp;&nbsp;
                        </td>
                         <td class="lblCaption1">
                            <asp:TextBox ID="txtOffNo" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="120px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                              Mobile &nbsp;
                           <asp:TextBox ID="txtMobile" runat="server" CssClass="label"  BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" Width="120px" AutoPostBack="true" OnTextChanged="txtMobile_TextChanged" ondblclick="return PatientPopup1('Mobile1',this.value);"></asp:TextBox>

                        </td>
                         <td class="lblCaption1">
                                 Type Of Service
                        </td>
                        <td class="lblCaption1">
                                                   <asp:DropDownList ID="drpTypeOfService" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="250px"></asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                         <td class="lblCaption1">
                           Insurance
                        </td>
                         <td class="lblCaption1">
                            <asp:TextBox ID="txtInsurance" runat="server" CssClass="label" BorderWidth="1px"   BorderColor="#cccccc" Width="290px" ></asp:TextBox>
                         
                        </td>
                          <td class="lblCaption1">
                            Room
                         
                        </td>
                        <td>
                            <asp:DropDownList ID="drpRoom" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="250px"></asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                         <td class="lblCaption1">
                          Remarks
                        </td>
                        <td  rowspan="2" valign="top">
                          <asp:TextBox ID="txtRemarks" runat="server" Width="285px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" TextMode="MultiLine" Height="50px"></asp:TextBox>

                        </td>
                         <td class="lblCaption1">
                           
                        </td>
                        <td>
                        </td>
                    </tr>
                    
                  
                    <tr>
                       <td></td>
                        <td colspan="4">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return val();" />
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="button orange small" Width="70px"  OnClick="btnDelete_Click" />
                           <asp:Button ID="btnSendSMS" runat="server" Text="Send SMS" CssClass="button orange small" Width="90px"  OnClick="btnSendSMS_Click"/>
                           <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button orange small" Width="90px" OnClick="btnClear_Click" />
  

                        </td>
                    </tr>
                    <tr>
                        <td  colspan="2"></td>
                        <td  >
                             <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button orange small" Width="70px" OnClientClick="window.close()" />

                        </td>
                    </tr>
                </table>
                <div style="padding-left: 0px; padding-top: 0px; width: 100%; height: 50px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
                <table style="width: 100%; border: 0px;" cellpadding="0px" cellspacing="0px">
                    <tr style="background-color: #4fbdf0;">
                        <td class="label" style="width: 200px; color: #ffffff;">Created User</td>
                        <td class="label" style="width: 100px; color: #ffffff;">Created Date</td>
                        <td class="label" style="width: 200px; color: #ffffff;">Modified User</td>
                        <td class="label" style="width: 100px; color: #ffffff;">Modified Date</td>

                    </tr>
                    <tr>
                        <td colspan="4" style="height: 5px;"></td>
                    </tr>
                    <tr>

                        <td class="label">
                            <asp:Label ID="lblCreatedUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="label">
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="label">
                            <asp:Label ID="lblModifiedUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                        </td>

                    </tr>
                </table>
                </div>
            </div>

            <asp:LinkButton ID="lnkAppMasterMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModPopExtAppMasters" runat="server" TargetControlID="lnkAppMasterMessage"
                                PopupControlID="pnlAppMaster" BackgroundCssClass="modalBackground" CancelControlID="btnAppMasterClose"
                                PopupDragHandleControlID="pnlAppMaster">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlAppMaster" runat="server" Height="400px" Width="680px" CssClass="modalPopup"  >
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnAppMasterClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 205px;">
                                            

                                            <div style="padding-top: 0px; width: 650px; height: 350px;  overflow: auto; border: thin; border-color:#cccccc;border-style:groove;" >

                                                <asp:GridView ID="gvAppMaster" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"  
                                                    EnableModelValidation="True" Width="99%"    PageSize="50">
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <RowStyle CssClass="GridRow" />
                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                    <Columns>


                                                        <asp:TemplateField HeaderText="Date" >
                                                            <ItemTemplate>
                                                                  
                                                                    <asp:Label ID="lblPTId" runat="server" Text='<%# Bind("AppintmentDate") %>'></asp:Label>
                                                                    
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Time" >
                                                            <ItemTemplate>
                                                                  
                                                                    <asp:Label ID="lblPTName" runat="server" Text='<%# Bind("HAMS_APNT_TIME") %>'></asp:Label>
                                                               
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                          <asp:TemplateField HeaderText="File No" >
                                                            <ItemTemplate>
                                                                    
                                                                    <asp:Label ID="lblFileNo" runat="server" Text='<%# Bind("HAMS_FILE_NO") %>'></asp:Label>
                                                                  
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Patient Name" >
                                                            <ItemTemplate>
                                                                  
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("HAMS_PT_NAME") %>'></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="User Name" >
                                                            <ItemTemplate>
                                                                  
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("HAMS_CREATED_USER") %>'></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Created Date" >
                                                            <ItemTemplate>
                                                                  
                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("HAMS_CREATED_DATEDesc") %>'></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                    </Columns>

                                                </asp:GridView>


                                            </div>

                                            
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
        </div>
         <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: #4fbdf0;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    </form>
</body>
</html>