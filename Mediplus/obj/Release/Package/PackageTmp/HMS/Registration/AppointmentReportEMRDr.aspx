﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppointmentReportEMRDr.aspx.cs" Inherits="Mediplus.HMS.Registration.AppointmentReportEMRDr" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />
     <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function Val() {
            var label;
            label = document.getElementById('lblStatus');
            label.style.color = 'red';
            document.getElementById('lblStatus').innerHTML = " ";


            if (document.getElementById('txtFromDate').value != "") {
                if (isDate(document.getElementById('txtFromDate').value) == false) {
                    document.getElementById('txtFromDate').focus()
                    return false;
                }
              

            }
            

            if (document.getElementById('txtToDate').value != "") {

                if (isDate(document.getElementById('txtToDate').value) == false) {
                    document.getElementById('txtToDate').focus()
                    return false;
                }
            }

          
        


            return true;
        }

        function ShowAppointment(DBString, FileNo, Status, DrId, Date1, Date2) {
            var Report;
            Report = "appointment_OL.rpt";

            var Formula = "";
            

            if (Date1 != "") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date1 + '\')';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date1 + '\')';
                }
            }


            if (Date2 != "") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}>=date(\'' + Date2 + '\')';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STARTTIME}<=date(\'' + Date2 + '\')';
                }
            }


           
            if (FileNo != "") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_FILENUMBER}=\'' + FileNo + '\'';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_FILENUMBER}=\'' + FileNo + '\'';
                }
            }
           
            if (Status != "0") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS}=\'' + Status + '\'';
                }
                else {
                    Formula = Formula + ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_STATUS}=\'' + Status + '\'';
                }
            }

            if (DrId != "0") {
                if (Formula == "") {
                    Formula = '{HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_DR_CODE}=\'' + DrId + '\'';
                }
                else {
                    Formula = Formula + 'AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_DR_CODE}=\'' + DrId + '\'';
                }
            }

            // alert(Formula);
           
            // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '$' + Formula + '$SCREEN$$$1');


            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Formula, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

          </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
    <table align="center">
        <tr>
            <td class="PageHeader">Appointment Report
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>

    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>

            <td style="width: 150px;height:40px;">
                <asp:Label ID="Label1" runat="server" CssClass="label"
                    Text="From"></asp:Label>
            </td>
            <td style="width: 300px">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                &nbsp;

 
                    <asp:Label ID="Label2" runat="server" CssClass="label"
                        Text="To"></asp:Label>

                <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


            </td>
            <td  style="width: 100px" >
                <asp:Label ID="Label3" runat="server" CssClass="label"
                    Text="File No"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtFileNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="150px" MaxLength="10" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" CssClass="label"
                    Text="Status"></asp:Label>

            </td>
            <td>
                <asp:DropDownList ID="drpStatus" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="4">

                <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="button orange small" Width="100px" OnClick="btnReport_Click"  OnClientClick="return Val();"/>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
