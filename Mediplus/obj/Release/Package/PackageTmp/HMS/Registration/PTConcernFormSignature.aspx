﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PTConcernFormSignature.aspx.cs" Inherits="Mediplus.HMS.Registration.PTConcernFormSignature" %>

<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>PaintWeb simple demo</title>
    <link rel="author" type="text/html" href="http://www.robodesign.ro" 
    title="Marius and Mihai Şucan, ROBO Design">
       <link href="../../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />

          <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>
   


    <!-- $URL: http://code.google.com/p/paintweb $
         $Date: 2010-06-26 22:58:27 +0300 $ -->
    
      <script  type="text/javascript">

          function SaveDepartment() {
              SaveDepartmentImage();

          }

          function ShowImage(ImageType) {

              //alert($("#ImageEditorPDC").contents().find("#PaintWebTarget"));

              //var imageData = $("#ImageEditorPDC").contents().find("#PaintWebTarget .paintweb_layerCanvas")[0].toDataURL("image/jpeg",1.0);

              // var canvas = $("#ImageEditorPDC").contents().find("#PaintWebTarget .paintweb_layerCanvas");
              var canvas = document.getElementsByClassName("paintweb_layerCanvas");
              var lowQuality = canvas[0].toDataURL("image/jpeg", 1.0);
              lowQuality = lowQuality.replace('data:image/jpeg;base64,', '');
              //  alert(lowQuality);


              document.getElementById("<%=hidImgData.ClientID%>").value = lowQuality


          }



          function passvalue(PI_ID) {
              // window.opener.BindCompanyDtls(CompanyId, CompanyName);
              opener.location.href = "PTConcernFormDisplay.aspx?PT_ID=" + PI_ID;
              opener.focus();
              window.close();

          }


      </script>
  </head>
  <body>

     <p><button id="buttonEditImage"  Class="orange"   Style="width:120px;border-radius:5px 5px 5px 5px;">Signature</button></p>

    <p> <img  runat="server"  id="editableImage" src="../Uploads/Signature/notsaved.jpg" alt="Freshalicious"> </p>

  

    <div id="PaintWebTarget"></div>

    

    <!--[if lte IE 8]>
    <p>Please note that this application does not work in Internet Explorer 
    8. Please try this in Internet Explorer 9, or use any other modern web 
       browser: <a href="http://www.google.com/chrome">Chrome</a>, <a 
         href="http://www.mozilla.com">Firefox</a>, <a 
         href="http://www.apple.com/safari">Safari</a>, <a 
         href="http://www.opera.com">Opera</a> or <a 
         href="http://www.konqueror.org">Konqueror</a>.</p>
    <![endif]-->

    <script type="text/javascript" src="../../Scripts/paintweb/paintweb.js"></script>

    <script type="text/javascript" src="../../Scripts/Patient/PatientMaster.js" ></script>

       <form id="form1" runat="server">
           <input type="hidden" id="hidImgData" runat="server" />
             <asp:Button ID="btnAdd" runat="server" Text="Save Signature"   CssClass="orange"   Style="width:120px;border-radius:5px 5px 5px 5px;" OnClick="btnAdd_Click"  OnClientClick="ShowImage('PBMI')" />
 
            <asp:Button ID="btnCancel" runat="server" Text="Cancel"   CssClass="orange"   Style="width:120px;border-radius:5px 5px 5px 5px;" OnClick="btnCancel_Click"  />

      </form>
    

      

  </body>
  <!-- vim:set spell spl=en fo=tcroqwanl1 tw=80 ts=2 sw=2 sts=2 sta et ai cin fenc=utf-8 ff=unix: -->

</html>
