﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientVisitHistory.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientVisitHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Patient History</title>
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowConsentFormReportPDF(FileNo, SeqNo, DrId, DateDesc) {


            var Report = document.getElementById('drpConsentForm').value + "_Visit.rpt"

            var Criteria = " 1=1 ";

            Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'';
            if (SeqNo != "") {

                Criteria += ' AND {HMS_PATIENT_VISIT.HPV_SEQNO}=\'' + SeqNo + '\'';
            }
            else {
                Criteria += ' AND {HMS_PATIENT_VISIT.HPV_DR_ID}=\'' + DrId + '\'';
                Criteria += ' AND {HMS_PATIENT_VISIT.HPV_DATE}=date(\'' + DateDesc + '\')';
            }

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
                <tr>
                    <td>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </td>
                </tr>

            </table>
            <div style="padding-top: 5px;">

                <table width="100%">
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                                Text="From"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            <asp:TextBox ID="txtFromTime" runat="server" Width="30px" MaxLength="5" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>

                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                                Text="To"></asp:Label>

                        </td>
                        <td>
                            <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            <asp:TextBox ID="txtToTime" runat="server" Width="30px" MaxLength="5" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>


                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                                Text="Doctor"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" Width="155px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" CssClass="lblCaption1"
                                Text="Type"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpType" CssClass="label" runat="server" Width="70px">
                                <asp:ListItem Text="--- All ---" Value="0"></asp:ListItem>
                                <asp:ListItem Text="OP" Value="OP"></asp:ListItem>
                                <asp:ListItem Text="IP" Value="IP"></asp:ListItem>
                            </asp:DropDownList>

                        </td>
                        <td>
                            <asp:Label ID="Label5" runat="server" CssClass="lblCaption1"
                                Text="User"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpUser" CssClass="label" runat="server" Width="155px">
                            </asp:DropDownList>

                        </td>

                        <td align="right">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <contenttemplate>
                                    <asp:Button ID="btnFind" runat="server" CssClass="button orange small"
                                        OnClick="btnFind_Click" Text="Find" />
                                </contenttemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                </table>
            </div>


            <div style="padding-top: 0px; width: 100%; height: 375px; overflow: auto;border-color:#e3f7ef;border-style:groove;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <contenttemplate>
                        <asp:GridView ID="gvGridView" runat="server" AllowPaging="false"
                            AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                 <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center">
                                     <ItemTemplate>
                                         <asp:CheckBox ID="chkSelect" runat="server" CssClass="label"  />
                                     </ItemTemplate>
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Consent Report" >
                                      <ItemTemplate>
                                           <asp:LinkButton ID="lnkReport" runat="server" OnClick="SelectPrint_Click">
                                                <asp:Label ID="lblHistory" CssClass="GridRow" Width="50px" Style="text-align: center; padding-right: 2px;" runat="server" Text='REPORT'></asp:Label>
                                          </asp:LinkButton>
                                   </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_SEQNO") %>'  visible="false" ></asp:Label>
                                         <asp:Label ID="lblDrId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_ID") %>'  visible="false" ></asp:Label>
                                         <asp:Label ID="lblEMRId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_ID") %>'  visible="false" ></asp:Label>
                                         <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_Date") %>'  visible="false" ></asp:Label>

                                        <asp:Label ID="lblPatientId" CssClass="GridRow" width="100px" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Token" SortExpression="HPV_DR_TOKEN">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDRToken" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HPV_PT_NAME">
                                    <ItemTemplate>

                                        <asp:Label ID="lblPTName" CssClass="GridRow" width="150px" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Mobile No" SortExpression="HPV_MOBILE">
                                    <ItemTemplate>

                                        <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_MOBILE") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dr.Name" SortExpression="HPV_DR_NAME">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDRName" CssClass="GridRow" width="150px"  runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dept." SortExpression="HPV_DR_NAME">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDepName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="HPV_DATE">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDateDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time" SortExpression="HPV_TIME">
                                    <ItemTemplate>

                                        <asp:Label ID="lblTime" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="PT Type" SortExpression="HPV_PT_TYPEDesc">
                                     <ItemTemplate>
                                
                                    <asp:Label ID="lblPTType" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>
                                
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                                 <ItemTemplate>
                                
                                    <asp:Label ID="lblCompName" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>
                                
                                 </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Id" SortExpression="HPV_CREATED_USER">
                                    <ItemTemplate>

                                        <asp:Label ID="lbluser" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_CREATED_USER") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Created Date" SortExpression="HPV_CREATED_DATEDesc">
                                   <ItemTemplate>
                                      <asp:Label ID="lblCreatedDate" CssClass="GridRow" Width="150px"  runat="server" Text='<%# Bind("HPV_CREATED_DATEDesc") %>'></asp:Label>
                                   </ItemTemplate>
                              </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </contenttemplate>
                </asp:UpdatePanel>

            </div>
            <table width="90%" >
                <tr>
                    <td align="left" class="lblCaption1">
                        Consent Form :
                         <asp:DropDownList ID="drpConsentForm"  BorderColor="#e3f7ef" BorderStyle="Groove" CssClass="label" runat="server" Width="100px" Visible="true">
                                                            </asp:DropDownList>
                         <asp:Button ID="btnConsentFormPrint" runat="server" style="padding-left:5px;padding-right:5px;width:100px;"  Visible="false" CssClass="button orange small"   Text="Consent Form" OnClick="btnConsentFormPrint_Click" />

                    </td>
                    <td  align="right">
                        <asp:Button ID="btnDelete" runat="server" CssClass="button orange small" Visible="false"
                            OnClick="btnDelete_Click" Text="Delete" ToolTip="Delete the Selected Visit." />

                    </td>
                </tr>
            </table>
           
            <div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <contenttemplate>
                        <table width="70%">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label6" runat="server" CssClass="lblCaption1"
                                        Text="Total :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                                <td class="style2">
                                    <asp:Label ID="Label7" runat="server" CssClass="lblCaption1"
                                        Text="New :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblNew" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                                <td class="style2">
                                    <asp:Label ID="Label8" runat="server" CssClass="lblCaption1"
                                        Text="Followup(Revisit)  :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblRevisit" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                                <td class="style2">
                                    <asp:Label ID="Label9" runat="server" CssClass="lblCaption1"
                                        Text="Established(Old) :"></asp:Label>
                                    &nbsp;<asp:Label ID="lblOld" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                            </tr>

                        </table>

                    </contenttemplate>
                </asp:UpdatePanel>

            </div>

        </div>
    </form>
</body>
</html>
