﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PTAndVisitRegistration.aspx.cs" Inherits="Mediplus.HMS.Registration.PTAndVisitRegistration" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />


    <script src="../../Scripts/PatientRegistration.js" type="text/javascript"></script>
    <script src="../../Scripts/Validation.js" type="text/javascript"></script>



    <script type="text/javascript" language="javascript" src="../../Scripts/errors.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/occupations.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/EIDAwebcomponents.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/fingers.js"></script>


    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style type="text/css">
        .modal
        {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }
    </style>


    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    <style type="text/css" media="print">
        #nottoprint
        {
            display: none;
        }

        #printToPrinter
        {
            display: block;
        }

            #printToPrinter ul li
            {
                width: 290px;
                color: #000;
                padding: 48px 5px;
            }
    </style>

    <style type="text/css" media="print">
        #nottoprint1
        {
            display: none;
            vertical-align: top;
        }

        #printToPrinter1
        {
            display: block;
        }
    </style>

    <style type="text/css" media="screen">
        #printToPrinter
        {
            display: none;
        }

        #printToPrinter1
        {
            display: none;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlightvisit
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }

        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }


        #divRefDr
        {
            width: 400px !important;
        }

            #divRefDr div
            {
                width: 400px !important;
            }

        #divPlan
        {
            width: 400px !important;
        }

            #divPlan div
            {
                width: 400px !important;
            }

        #divPTCompany
        {
            width: 400px !important;
        }

            #divPTCompany div
            {
                width: 400px !important;
            }
    </style>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else {
                return false;
            }
        }

        function BindDoctorDtls(StaffId, StaffName, Dept, TokenNo) {

            document.getElementById("<%=txtDoctorID.ClientID%>").value = StaffId;
            document.getElementById("<%=txtDoctorName.ClientID%>").value = StaffName;
            document.getElementById("<%=txtDepName.ClientID%>").value = Dept;
            document.getElementById("<%=txtTokenNo.ClientID%>").value = TokenNo;


        }
        function BindCompanyDtls(CompanyId, CompanyName) {

            document.getElementById("<%=txtCompany.ClientID%>").value = CompanyId;
            document.getElementById("<%=txtCompanyName.ClientID%>").value = CompanyName;

        }


        function disableinc() {

            var e = document.getElementById("<%=drpPatientType.ClientID%>");
            var ty = e.options[e.selectedIndex].value;
            if (ty == "CA") {
                document.getElementById("<%=txtCompany.ClientID%>").disabled = true;
                document.getElementById("<%=txtCompanyName.ClientID%>").disabled = true;
                document.getElementById("<%=txtPolicyNo.ClientID%>").disabled = true;
                document.getElementById("<%=txtPolicyId.ClientID%>").disabled = true;
                document.getElementById("<%=txtIDNo.ClientID%>").disabled = true;
                document.getElementById("<%=txtExpiryDate.ClientID%>").disabled = true;
                document.getElementById("<%=drpPlanType.ClientID%>").disabled = true;
                document.getElementById("<%=txtCompany.ClientID%>").value = '';
                document.getElementById("<%=txtCompanyName.ClientID%>").value = '';
                document.getElementById("<%=txtPolicyNo.ClientID%>").value = '';
                document.getElementById("<%=txtPolicyId.ClientID%>").value = '';
                document.getElementById("<%=txtIDNo.ClientID%>").value = '';
                document.getElementById("<%=txtExpiryDate.ClientID%>").value = '';
                document.getElementById("<%=drpPlanType.ClientID%>").value = 'Select';



            }
            else {

                document.getElementById("<%=txtCompany.ClientID%>").disabled = false;
                document.getElementById("<%=txtCompanyName.ClientID%>").disabled = false;
                document.getElementById("<%=txtPolicyNo.ClientID%>").disabled = false;
                document.getElementById("<%=txtPolicyId.ClientID%>").disabled = false;
                document.getElementById("<%=txtIDNo.ClientID%>").disabled = false;
                document.getElementById("<%=txtExpiryDate.ClientID%>").disabled = false;
                document.getElementById("<%=drpPlanType.ClientID%>").disabled = false;


            }
        }

        function GetDays(year, month) {
            return new Date(year, month, 0).getDate();

        }

        function AgeCalculation() {
            if (document.getElementById("<%=txtDOB.ClientID%>").value != "__/__/____") {
                var dob = document.getElementById("<%=txtDOB.ClientID%>").value;
                document.getElementById("<%=txtAge.ClientID%>").value = 0;
                var arrDOB = dob.split('/');
                var currentyear = document.getElementById("<%=Year.ClientID%>").value

                var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
                var arrTodayDate1 = TodayDate.split('/');


                if (arrDOB.length == 3 && dob.length == 10) {

                    if (currentyear != arrDOB[2]) {
                        var age = currentyear - arrDOB[2];
                        var Month;

                        if (parseFloat(arrTodayDate1[1]) >= parseFloat(arrDOB[1])) {
                            Month = arrTodayDate1[1] - arrDOB[1];

                        }

                        else {
                            age = age - 1;
                            var diff = 12 - arrDOB[1];
                            Month = parseFloat(arrTodayDate1[1]) + parseFloat(diff);

                        }

                        document.getElementById("<%=txtAge.ClientID%>").value = age;
                        document.getElementById("<%=txtMonth.ClientID%>").value = Month;

                    }
                    else {

                        document.getElementById("<%=txtAge.ClientID%>").value = 0;
                        var Days = date_diff_indays(arrDOB[1] + '/' + arrDOB[0] + '/' + arrDOB[2], arrTodayDate1[1] + '/' + arrTodayDate1[0] + '/' + arrTodayDate1[2]);
                        var Month;

                        if (parseFloat(arrTodayDate1[1]) >= parseFloat(arrDOB[1])) {
                            Month = arrTodayDate1[1] - arrDOB[1];

                            if (Month == 1 && Days <= 30) {
                                document.getElementById("<%=txtAge.ClientID%>").value = Days;
                                document.getElementById("<%=drpAgeType.ClientID%>").value = 'D';
                                document.getElementById("<%=txtMonth.ClientID%>").value = '';
                            } else {
                                document.getElementById("<%=txtMonth.ClientID%>").value = Month;
                            }
                        }

                        else {
                            Month = (parseFloat(arrTodayDate1[1]) + 11) - arrDOB[1];
                            document.getElementById("<%=txtMonth.ClientID%>").value = Month;
                        }
                        //alert(date_diff_indays('09/12/2020', '10/05/2020'))


                    }
                }
                else {

                    document.getElementById("<%=txtAge.ClientID%>").value = 0;

                }


            }
            else {
                document.getElementById("<%=txtAge.ClientID%>").value = 0;

            }
        }

        var date_diff_indays = function (date1, date2) {
            dt1 = new Date(date1);
            dt2 = new Date(date2);
            return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        }

        function DBOcalculation() {

            // if (document.getElementById("<%=txtDOB.ClientID%>").value == "") {
            var age = document.getElementById("<%=txtAge.ClientID%>").value;
            var AgeType = document.getElementById("<%=drpAgeType.ClientID%>").value

            var currentyear = document.getElementById("<%=Year.ClientID%>").value
            var DBOYear = currentyear - age;
            if (age != DBOYear && AgeType == 'Y' && age != 0) {
                document.getElementById("<%=txtDOB.ClientID%>").value = '01/01/' + DBOYear
                document.getElementById("<%=txtMonth.ClientID%>").value = '';
            }


            // }

        }
        function PastDateCheck() {

            var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]


                //alert(ReFromTodayDate)
                //  alert(ReFromExpDate)

                // if (ReFromTodayDate > ReFromExpDate) {
                //  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Expiry Date Should be Grater than Today Date";
                //  return false;

                // }

            }
            return true;


        }

        function PastDateCheck1() {

            var ExpiryDat = document.getElementById("<%=txtCardExpDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]


                // if (ReFromTodayDate > ReFromExpDate) {

                //  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Expiry Date Should be Grater than Today Date";
                //  return false;
                // }

            }
            return true;

        }

        function PolicyExpCheck(ExpiryDat) {

            // var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;

            var currentyear = document.getElementById("<%=Year.ClientID%>").value

            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]
                var CurrMonth = date_array[1];

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]
                var ExpYear = date_array1[2];
                var ExpMonth = date_array1[1];

                if (currentyear > ExpYear) {
                    alert('Insurance Card Expired');
                    return false;

                }
                else if (currentyear == ExpYear && CurrMonth > ExpMonth) {
                    alert('Insurance Card Expired');
                    return false;
                }
                else if (currentyear == ExpYear && CurrMonth == ExpMonth) {
                    var diffDays = date_array1[0] - date_array[0]

                    if (diffDays < 0) {
                        alert('Insurance Card will Expire in ' + diffDays + ' Days');
                        return false;
                    }
                    else if (diffDays <= 7) {
                        alert('Insurance Card will Expire in ' + diffDays + ' Days');
                    }
                }

            }
            return true;

        }



        $(document).ready(function () {
            $('#btnClear').click(function () {
                $(':input[type=text]').val('');
            });
        });
        function AsyncFileUpload1_uploadStarted() {
            //document.getElementById("imageFront").style.display = "none";
        }

        function AsyncFileUpload1_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgFront.ClientID %>");
            document.getElementById("<%=hidImgFront.ClientID%>").value = 'true';

            var InsCardPath = document.getElementById("<%= hidInsCardPath.ClientID %>").value;
            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };

            //  img.src = InsCardPath + "Temp\\" + args.get_fileName();
            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();

        }

        function AsyncFileUpload2_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgBack.ClientID %>");

            var InsCardPath = document.getElementById("<%= hidInsCardPath.ClientID %>").value;

            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };
            //img.src = InsCardPath + "Temp\\" + args.get_fileName();
            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();
        }


        function AsyncFileUpload3_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgFrontDefault.ClientID %>");

            var InsCardPath = document.getElementById("<%= hidInsCardPath.ClientID %>").value;
            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };

            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();
            
        }

        function AsyncFileUpload4_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgBackDefault.ClientID %>");

            var InsCardPath = document.getElementById("<%= hidInsCardPath.ClientID %>").value;
            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };
            // img.src = InsCardPath + "Temp\\" + args.get_fileName();
            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();
        }

        function ValRegister() {

            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br></br>";

            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                ErrorMessage += 'Patient File No. is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtFName.ClientID%>').value == "") {

                ErrorMessage += 'Patient First Name is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=drpSex.ClientID%>').value == "") {
                ErrorMessage += 'Patient Gender is Empty.' + trNewLineChar;
                IsError = true;
            }

            var AgeMandatory = '<%=Convert.ToString(ViewState["AgeMandatory"]) %>'
            if (AgeMandatory == "1" && document.getElementById('<%=txtDOB.ClientID%>').value == "") {
                ErrorMessage += 'Patient DOB is Empty.' + trNewLineChar;

                IsError = true;

            }

            if (document.getElementById('<%=txtDOB.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtDOB.ClientID%>').value) == false) {
                    document.getElementById('<%=txtDOB.ClientID%>').focus()
                    ErrorMessage += 'Patient DOB not Valid' + trNewLineChar;
                    IsError = true;
                }
            }
            
            if (AgeMandatory == "1" && document.getElementById('<%=txtAge.ClientID%>').value == "") {
                ErrorMessage += 'Patient Age is Empty.' + trNewLineChar;
                IsError = true;

            }

            var CityMandatory = '<%=Convert.ToString(ViewState["CityMandatory"]) %>'
            if (CityMandatory == "1" && document.getElementById('<%=drpCity.ClientID%>').value == "0") {
                ErrorMessage += 'Patient City is Empty.' + trNewLineChar;
                IsError = true;

            }

            var NationalityMandatory = '<%=Convert.ToString(ViewState["NationalityMandatory"]) %>'
            if (NationalityMandatory == "1" && document.getElementById('<%=drpNationality.ClientID%>').value == "0") {
                ErrorMessage += 'Patient Nationality is Empty.' + trNewLineChar;
                IsError = true;
            }

            var CountryMandatory = '<%=Convert.ToString(ViewState["CountryMandatory"]) %>'
            if (CountryMandatory == "1" && document.getElementById('<%=drpCountry.ClientID%>').value == "0") {
                ErrorMessage += 'Country Of Residence is Empty.' + trNewLineChar;
                IsError = true;
            }

            var PtRegType = '<%=Convert.ToString(ViewState["ECLAIM"]) %>'
            if (PtRegType == "AUH" && document.getElementById('<%=drpPtRegisteredType.ClientID%>').value == "0") {
                ErrorMessage += 'Patient Registered Type is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtMobile1.ClientID%>').value == "") {
                ErrorMessage += 'Mobile No is Empty' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=txtEmiratesID.ClientID%>').value == "") {
                ErrorMessage += 'Mobile No is Empty' + trNewLineChar;
                IsError = true;
            }


            var EmiratesIdMandatory = '<%=Convert.ToString(ViewState["EMIRATES_ID"]) %>'
            if (EmiratesIdMandatory == "1" && document.getElementById('<%=txtEmiratesID.ClientID%>').value == "") {
                ErrorMessage += 'Patient EmiratesID / Passport is Empty.' + trNewLineChar;
                IsError = true;

            }
            var IDCaption = document.getElementById("<%=drpIDCaption.ClientID%>").value;
            var vEmiratesIDContent = document.getElementById('<%=txtEmiratesID.ClientID%>').value

            if (EmiratesIdMandatory == "1" && IDCaption == 'EmiratesId') {
                if (vEmiratesIDContent.length < 16) {
                    {
                        ErrorMessage += 'EmiratesID Minimum 16 Digit' + trNewLineChar;
                        IsError = true;
                    }
                }
            }


            if (IsError == true) {
                ShowErrorMessage('Please check below Errors', ErrorMessage, 'Brown');

                return false;
            }



            return true;

        }

        function ValVisit() {
            document.getElementById('<%=hidWaitingList.ClientID%>').value = true;

            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br>";

            document.getElementById("<%=txtFileNo.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=txtEligibilityID.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=txtDoctorID.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtDoctorName.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=drpPTVisitCompanyPlan.ClientID%>").style.border = '1px solid #999';


            document.getElementById("<%=txtPTVisitPolicyNo.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtPTVisitCardNo.ClientID%>").style.border = '1px solid #999';



            document.getElementById("<%=txtPTVisitCardExpDate.ClientID%>").style.border = '1px solid #999';



            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById("<%=txtFileNo.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtFileNo.ClientID%>').focus();

                }
                ErrorMessage += 'Patient File No. is Empty.' + trNewLineChar;
                IsError = true;
            }


            if (document.getElementById('<%=txtDoctorID.ClientID%>').value == "") {
                document.getElementById("<%=txtDoctorID.ClientID%>").style.border = '1px solid red';
                document.getElementById("<%=txtDoctorName.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').focus;
                }


                ErrorMessage += ' Doctor Dtls is Empty.' + trNewLineChar;
                IsError = true;
            }



            if (document.getElementById('<%=drpPTVisitType.ClientID%>').value == "CR") {

                var EligibilityID = '<%=Convert.ToString(ViewState["REQ_ELIGIBILITY_ID"]) %>'
                if (EligibilityID == "1" && document.getElementById('<%=txtEligibilityID.ClientID%>').value == "") {

                    document.getElementById("<%=txtEligibilityID.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtEligibilityID.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter EligibilityID' + trNewLineChar;
                    IsError = true;

                }
            }




            if (document.getElementById('<%=drpPTVisitType.ClientID%>').value == "CU") {



                if (document.getElementById('<%=drpPTVisitCompany.ClientID%>').value == "") {
                    document.getElementById("<%=drpPTVisitCompany.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtFileNo.ClientID%>').focus();

                    }
                    ErrorMessage += 'Patient Select Customer Name.' + trNewLineChar;
                    IsError = true;
                }

                var vREQ_POLICY_NO = '<%=Convert.ToString(ViewState["REQ_POLICY_NO"]) %>'
                if (vREQ_POLICY_NO == "1" && document.getElementById('<%=txtPTVisitPolicyNo.ClientID%>').value == "") {

                    document.getElementById("<%=txtPTVisitPolicyNo.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtPTVisitPolicyNo.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Member ID' + trNewLineChar;
                    IsError = true;
                }

                var vREQ_CARD_NO = '<%=Convert.ToString(ViewState["REQ_CARD_NO"]) %>'
                if (vREQ_CARD_NO == "1" && document.getElementById('<%=txtPTVisitCardNo.ClientID%>').value == "") {

                    document.getElementById("<%=txtPTVisitCardNo.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtPTVisitCardNo.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Card No' + trNewLineChar;
                    IsError = true;
                }





                if (document.getElementById('<%=txtPTVisitCardExpDate.ClientID%>').value == "") {

                    document.getElementById("<%=txtPTVisitCardExpDate.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtPTVisitCardExpDate.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Card Expiry Date' + trNewLineChar;
                    IsError = true;
                }

                if (document.getElementById('<%=txtPTVisitCardExpDate.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtPTVisitCardExpDate.ClientID%>').value) == false) {

                        document.getElementById("<%=txtPTVisitCardExpDate.ClientID%>").style.border = '1px solid red';
                        if (IsError == false) {
                            document.getElementById('<%=txtPTVisitCardExpDate.ClientID%>').focus;
                        }

                        ErrorMessage += 'Policy Expir Date not valid' + trNewLineChar;
                        IsError = true;
                    }
                }


            }



            if (IsError == true) {
                ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }



            var TokenPrint = '<%=Convert.ToString(Session["TokenPrint"]) %>'

            if (TokenPrint == "Y") {
                var isTokenPrint = window.confirm('Do you want to print Token ? ');
                document.getElementById('<%=hidTokenPrint.ClientID%>').value = isTokenPrint;
            }
            else {
                document.getElementById('<%=hidTokenPrint.ClientID%>').value = 'false';

            }


            var LabelPrint = '<%= Convert.ToString( ViewState["LABEL_PRINT"])  %>'
            if (LabelPrint == "1") {
                var isLabelPrint = window.confirm('Do you want to print Label ? ');
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = isLabelPrint;
            }
            else {
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = 'false';

            }


            if (document.getElementById('<%=drpPatientType.ClientID%>').value == "CR") {

                var ExpiryDat = document.getElementById("<%=txtPTVisitCardExpDate.ClientID%>").value;
                return PolicyExpCheck(ExpiryDat);

            }
            else {
                return true;
            }


        }




        function ValPTSave() {



            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var hh = today.getHours();
            var min = today.getMinutes();
            var sec = today.getSeconds();

            var yyyy = today.getFullYear();
            if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;



            // alert(today);

            document.getElementById("<%=txtFileNo.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtFName.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtLName.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpSex.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtDOB.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtAge.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtAddress.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpCity.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpCountry.ClientID%>").style.border = '1px solid #999';


            document.getElementById("<%=drpNationality.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpMStatus.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpEmirates.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=txtEmiratesID.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpVisaType.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtMobile1.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=txtEmail.ClientID%>").style.border = '1px solid #999';


            document.getElementById("<%=drpReligion.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=drpKnowFrom.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=drpDefaultCompany.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=drpPlanType.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtNetworkClass.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtPolicyNo.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtIDNo.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtPTCompany.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=txtPolicyStart.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtExpiryDate.ClientID%>").style.border = '1px solid #999';

            document.getElementById("<%=drpKinRelation.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtKinMobile.ClientID%>").style.border = '1px solid #999';


            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br>";
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;

            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById("<%=txtFileNo.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtFileNo.ClientID%>').focus();

                }
                ErrorMessage += 'Patient File No. is Empty.' + trNewLineChar;
                IsError = true;
            }



            if (document.getElementById('<%=txtFName.ClientID%>').value == "") {
                document.getElementById("<%=txtFName.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtFName.ClientID%>').focus;

                }
                ErrorMessage += 'Patient Given Name is Empty.' + trNewLineChar;
                IsError = true;
            }



            if (document.getElementById('<%=txtLName.ClientID%>').value == "") {
                document.getElementById("<%=txtLName.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtLName.ClientID%>').focus;

                }
                ErrorMessage += 'Patient Family Name is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (document.getElementById('<%=drpSex.ClientID%>').value == "") {
                document.getElementById("<%=drpSex.ClientID%>").style.border = '1px solid red';
                ErrorMessage += 'Patient Gender is Empty.' + trNewLineChar;
                if (IsError == false) {
                    document.getElementById('<%=drpSex.ClientID%>').focus;

                }
                IsError = true;
            }




            var AgeMandatory = '<%=Convert.ToString(ViewState["AgeMandatory"]) %>'
            if (AgeMandatory == "1" && document.getElementById('<%=txtDOB.ClientID%>').value == "") {

                document.getElementById("<%=txtDOB.ClientID%>").style.border = '1px solid red';
                ErrorMessage += 'Patient DOB is Empty.' + trNewLineChar;
                if (IsError == false) {
                    document.getElementById('<%=txtDOB.ClientID%>').focus;

                }

                IsError = true;

            }

            if (document.getElementById('<%=txtDOB.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtDOB.ClientID%>').value) == false) {
                    document.getElementById('<%=txtDOB.ClientID%>').focus()
                    ErrorMessage += 'Patient DOB not Valid' + trNewLineChar;
                    if (IsError == false) {
                        document.getElementById('<%=txtDOB.ClientID%>').focus;

                    }
                    IsError = true;
                }
            }



            if (AgeMandatory == "1" && document.getElementById('<%=txtAge.ClientID%>').value == "") {
                document.getElementById("<%=txtAge.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {

                    document.getElementById('<%=txtAge.ClientID%>').focus;

                }
                ErrorMessage += 'Patient Age is Empty.' + trNewLineChar;
                IsError = true;

            }

            var AddressnMandatory = '<%=Convert.ToString(ViewState["REQ_ADDRESS"]) %>'
            if (AddressnMandatory == "1" && document.getElementById('<%=txtAddress.ClientID%>').value == "") {

                document.getElementById("<%=txtAddress.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtAddress.ClientID%>').focus;
                }


                ErrorMessage += 'Patient Address is Empty.' + trNewLineChar;
                IsError = true;

            }


            var CityMandatory = '<%=Convert.ToString(ViewState["CityMandatory"]) %>'
            if (CityMandatory == "1" && document.getElementById('<%=drpCity.ClientID%>').value == "0") {
                document.getElementById("<%=drpCity.ClientID%>").style.border = '1px solid red';

                if (IsError == false) {

                    document.getElementById('<%=drpCity.ClientID%>').focus;

                }
                ErrorMessage += 'Patient City is Empty.' + trNewLineChar;
                IsError = true;

            }

            if (document.getElementById('<%=drpCountry.ClientID%>').value == "0") {

                document.getElementById("<%=drpCountry.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpCountry.ClientID%>').focus;
                }

                ErrorMessage += 'Country Of Residence is Empty.' + trNewLineChar;
                IsError = true;

            }

            var NationalityMandatory = '<%=Convert.ToString(ViewState["NationalityMandatory"]) %>'
            if (NationalityMandatory == "1" && document.getElementById('<%=drpNationality.ClientID%>').value == "0") {
                document.getElementById("<%=drpNationality.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpNationality.ClientID%>').focus;
                }


                ErrorMessage += 'Patient Nationality is Empty.' + trNewLineChar;
                IsError = true;

            }

            var EmiratesnMandatory = '<%=Convert.ToString(ViewState["REQ_EMIRATES"]) %>'
            if (EmiratesnMandatory == "1" && document.getElementById('<%=drpEmirates.ClientID%>').value == "") {

                document.getElementById("<%=drpEmirates.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpEmirates.ClientID%>').focus;
                }


                ErrorMessage += 'Patient Emirates is Empty.' + trNewLineChar;
                IsError = true;

            }



            var MStatusMandatory = '<%=Convert.ToString(ViewState["REQ_MARITAL_STATUS"]) %>'
            if (MStatusMandatory == "1" && document.getElementById('<%=drpMStatus.ClientID%>').value == "") {
                document.getElementById("<%=drpMStatus.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpMStatus.ClientID%>').focus;
                }


                ErrorMessage += 'Patient Marital Status is Empty.' + trNewLineChar;
                IsError = true;

            }


            var EmiratesIdMandatory = '<%=Convert.ToString(ViewState["EMIRATES_ID"]) %>'
            if (EmiratesIdMandatory == "1" && document.getElementById('<%=txtEmiratesID.ClientID%>').value == "") {
                document.getElementById("<%=txtEmiratesID.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtEmiratesID.ClientID%>').focus;
                }

                ErrorMessage += 'Patient EmiratesID / Passport is Empty.' + trNewLineChar;
                IsError = true;

            }
            var IDCaption = document.getElementById("<%=drpIDCaption.ClientID%>").value;
            var vEmiratesIDContent = document.getElementById('<%=txtEmiratesID.ClientID%>').value

            if (EmiratesIdMandatory == "1" && IDCaption == 'EmiratesId') {
                if (vEmiratesIDContent.length < 16) {
                    {
                        ErrorMessage += 'EmiratesID Minimum 16 Digit' + trNewLineChar;
                        IsError = true;
                    }
                }
            }

            var VisaTypeMandatory = '<%=Convert.ToString(ViewState["VisaTypeMandatory"]) %>'
            if (VisaTypeMandatory == "1" && document.getElementById('<%=drpVisaType.ClientID%>').value == "") {
                document.getElementById("<%=drpVisaType.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpVisaType.ClientID%>').focus;
                }

                ErrorMessage += 'Patient Visa Type is Empty.' + trNewLineChar;
                IsError = true;

            }


            var KnowFromMandatory = '<%=Convert.ToString(ViewState["REQ_KNOW_FROM"]) %>'
            if (KnowFromMandatory == "1" && document.getElementById('<%=drpKnowFrom.ClientID%>').value == "") {

                document.getElementById("<%=drpKnowFrom.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpKnowFrom.ClientID%>').focus;
                }


                ErrorMessage += 'Know From is Empty.' + trNewLineChar;
                IsError = true;

            }


            if (document.getElementById('<%=txtMobile1.ClientID%>').value == "") {

                document.getElementById("<%=txtMobile1.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtMobile1.ClientID%>').focus;
                }


                ErrorMessage += 'Mobile No is Empty' + trNewLineChar;
                IsError = true;
            }




            var EMailIDMandatory = '<%=Convert.ToString(ViewState["REQ_EMAIL_ID"]) %>'
            if (EMailIDMandatory == "1" && document.getElementById('<%=txtEmail.ClientID%>').value == "") {

                document.getElementById("<%=txtEmail.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtEmail.ClientID%>').focus;
                }

                ErrorMessage += 'Email ID is Empty' + trNewLineChar;
                IsError = true;

            }

            var strEmail = document.getElementById('<%=txtEmail.ClientID%>').value
            if (/\S+/.test(strEmail)) {
                if (echeck(document.getElementById('<%=txtEmail.ClientID%>').value) == false) {
                    document.getElementById("<%=txtEmail.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtEmail.ClientID%>').focus;
                    }


                    ErrorMessage += 'Email ID. is not Valid' + trNewLineChar;
                    IsError = true;
                }
            }



            var TriageOption = '<%=Convert.ToString(Session["TRIAGE_OPTION"]) %>'
            if (TriageOption == "1" && document.getElementById('<%=chkRequiredTriage.ClientID%>').checked == false && document.getElementById('<%=drpTriageReason.ClientID%>').value == "") {




                ErrorMessage += 'Nursing Assessment Avoid Reason is Empty' + trNewLineChar;
                IsError = true;

            }


            var ReligionMandatory = '<%=Convert.ToString(ViewState["REQ_RELIGION"]) %>'
            if (ReligionMandatory == "1" && document.getElementById('<%=drpReligion.ClientID%>').value == "") {

                document.getElementById("<%=drpReligion.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=drpReligion.ClientID%>').focus;
                }


                ErrorMessage += 'Patient Religion is Empty.' + trNewLineChar;
                IsError = true;

            }


            if (document.getElementById('<%=drpPatientType.ClientID%>').value == "CR") {

                if (document.getElementById('<%=drpDefaultCompany.ClientID%>').value == "0") {

                    document.getElementById("<%=drpDefaultCompany.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=drpDefaultCompany.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please Select Company Dtls.' + trNewLineChar;
                    IsError = true;
                }
                if (document.getElementById('<%=drpPlanType.ClientID%>').value == "0") {

                    document.getElementById("<%=drpPlanType.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=drpPlanType.ClientID%>').focus;
                    }


                    ErrorMessage += 'Please Select Plan Type' + trNewLineChar;
                    IsError = true;
                }

                var PackageManditory = '<%=Convert.ToString(Session["PackageManditory"]) %>'
                if (PackageManditory == "Y" && document.getElementById('<%=txtNetworkClass.ClientID%>').value == "") {

                    document.getElementById("<%=txtNetworkClass.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtNetworkClass.ClientID%>').focus;
                    }


                    ErrorMessage += 'Please Select Network Class' + trNewLineChar;
                    IsError = true;
                }

                var vREQ_POLICY_NO = '<%=Convert.ToString(ViewState["REQ_POLICY_NO"]) %>'
                if (vREQ_POLICY_NO == "1" && document.getElementById('<%=txtPolicyNo.ClientID%>').value == "") {

                    document.getElementById("<%=txtPolicyNo.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtPolicyNo.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Member ID' + trNewLineChar;
                    IsError = true;
                }

                var vREQ_CARD_NO = '<%=Convert.ToString(ViewState["REQ_CARD_NO"]) %>'
                if (vREQ_CARD_NO == "1" && document.getElementById('<%=txtIDNo.ClientID%>').value == "") {

                    document.getElementById("<%=txtIDNo.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtIDNo.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Card No' + trNewLineChar;
                    IsError = true;
                }

                var vREQ_PT_COMPANY = '<%=Convert.ToString(ViewState["REQ_PT_COMPANY"]) %>'
                if (vREQ_PT_COMPANY == "1" && document.getElementById('<%=txtPTCompany.ClientID%>').value == "") {

                    document.getElementById("<%=txtPTCompany.ClientID%>").style.border = '1px solid red';
                    if (IsError == false) {
                        document.getElementById('<%=txtPTCompany.ClientID%>').focus;
                    }
                    ErrorMessage += 'Please enter  PT  Company' + trNewLineChar;
                    IsError = true;
                }


                if (document.getElementById('<%=txtPolicyStart.ClientID%>').value == "") {

                    document.getElementById("<%=txtPolicyStart.ClientID%>").style.border = '1px solid red';
                        if (IsError == false) {
                            document.getElementById('<%=txtPolicyStart.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Card Effective Date' + trNewLineChar;
                    IsError = true;
                }


                if (document.getElementById('<%=txtPolicyStart.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtPolicyStart.ClientID%>').value) == false) {

                        document.getElementById("<%=txtPolicyStart.ClientID%>").style.border = '1px solid red';
                        if (IsError == false) {
                            document.getElementById('<%=txtPolicyStart.ClientID%>').focus()
                        }

                        ErrorMessage += 'Policy Start Date not valid' + trNewLineChar;
                        IsError = true;
                    }
                }


                if (document.getElementById('<%=txtExpiryDate.ClientID%>').value == "") {

                    document.getElementById("<%=txtExpiryDate.ClientID%>").style.border = '1px solid red';
                        if (IsError == false) {
                            document.getElementById('<%=txtExpiryDate.ClientID%>').focus;
                    }

                    ErrorMessage += 'Please enter Card Expiry Date' + trNewLineChar;
                    IsError = true;
                }

                if (document.getElementById('<%=txtExpiryDate.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtExpiryDate.ClientID%>').value) == false) {

                        document.getElementById("<%=txtExpiryDate.ClientID%>").style.border = '1px solid red';
                        if (IsError == false) {
                            document.getElementById('<%=txtExpiryDate.ClientID%>').focus;
                        }

                        ErrorMessage += 'Policy Expir Date not valid' + trNewLineChar;
                        IsError = true;
                    }
                }


            }

            var PriorityDisplay = '<%=Convert.ToString(Session["PriorityDisplay"]) %>'
            if (PriorityDisplay == "Y" && document.getElementById('<%=drpPriority.ClientID%>').value == "0") {
                ErrorMessage += 'Enter Priority' + trNewLineChar;
                IsError = true;

            }

            if (document.getElementById('<%=txtKinName.ClientID%>').value != "") {

                if (document.getElementById('<%=drpKinRelation.ClientID%>').value == "") {
                    document.getElementById("<%=drpKinRelation.ClientID%>").style.border = '1px solid red';
                    ErrorMessage += 'Enter Kin Relation' + trNewLineChar;
                    IsError = true;

                }


                if (document.getElementById('<%=txtKinMobile.ClientID%>').value == "") {
                    document.getElementById("<%=txtKinMobile.ClientID%>").style.border = '1px solid red';
                    ErrorMessage += 'Enter Kin Mobile No' + trNewLineChar;
                    IsError = true;

                }

            }


            if (IsError == true) {
                ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }








            if (document.getElementById('<%=drpPatientType.ClientID%>').value == "CR") {

                var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
                return PolicyExpCheck(ExpiryDat);

            }
            else {
                return true;
            }



        }



        function ValScanCardAdd() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=drpCardName.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please select the Card Name";
                    document.getElementById('<%=drpCardName.ClientID%>').focus();
                    return false;
                }

                if (document.getElementById('<%=txtPolicyCardNo.ClientID%>').value == "") {
                var PTRegChangePolicyNoCaption = '<%=PTRegChangePolicyNoCaption.ToLower()%>'
                if (PTRegChangePolicyNoCaption == 'true') {
                    document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please enter the Card No.";
                }
                else {
                    document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please enter the policy Number";
                }
                document.getElementById('<%=txtPolicyCardNo.ClientID%>').focus();
                return false;
            }



            if (document.getElementById('<%=txtEffectiveDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtEffectiveDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtEffectiveDate.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtCardExpDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please enter the Card Expiry Date";
                    document.getElementById('<%=txtCardExpDate.ClientID%>').focus();
                    return false;
                }

                if (document.getElementById('<%=txtCardExpDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtCardExpDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtCardExpDate.ClientID%>').focus()
                    return false;
                }
            }

            //return PastDateCheck1()
            // return PolicyExpCheck();


        }
        function Val1() {

            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            }



            function DRIdSelected() {
                if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function RefDrIdSelected() {
            if (document.getElementById('<%=txtRefDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function RefDRNameSelected() {
            if (document.getElementById('<%=txtRefDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }




        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');


                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];


                }
            }

            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">


        function BindSelectedPlanType(PlanType) {

            document.getElementById("<%=txtPlanType.ClientID%>").value = PlanType;

        }


        function BenefitsExecPopup(strValue) {

            var CompID = document.getElementById('<%=txtCompany.ClientID%>').value;
            var win = window.open("../Masters/BenefitsExeclusions.aspx?PageName=PatientVisitReg&CompanyID=" + CompID + "&PlanType=" + strValue, "newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }


        function BenefitsExecPopup1() {

            var CompID = document.getElementById('<%=drpDefaultCompany.ClientID%>').value;
            var PlanType = document.getElementById('<%=drpPlanType.ClientID%>').value;

            var win = window.open("../Masters/BenefitsExeclusions.aspx?PageName=PatientVisitReg&CompanyID=" + CompID + "&PlanType=" + PlanType, "newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=PatientVisitReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function PatientPopup(PageName, strValue, evt) {

            // if (PageName == 'FileNo') {
            //  document.getElementById('<%=txtFileNo.ClientID%>').value = strValue.toUpperCase()
            //  }

            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;

            if (chCode == 126) {
                // var win = window.open("Firstname_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                // win.focus();
                document.getElementById('<%=hidPopup.ClientID%>').value = 'true';
                return true;

            }
            if (chCode == 33) {
                // var win = window.open("PatientInfo_Zoom.aspx?PagePath=PATIENTREG&PageName=" + PageName + "&Value=" + strValue, "newwin", "top=200,left=270,height=520,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                // win.focus();
                document.getElementById('<%=hidInfoPopup.ClientID%>').value = 'true';
                return false;

            }

            if (PageName == 'Phone' || PageName == 'Mobile') {
                return OnlyNumeric(evt);
            }

            return true;
        }
        function InsurancePopup(PageName, strValue, evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode == 126) {
                var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
                return false;
            }
            return true;
        }
        function DoctorPopup(PageName, strValue, evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode == 126) {
                var win = window.open("../Masters/Doctor_Name_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
                return false;
            }
            return true;
        }

        function OpenScan() {
            window.open("../EmiratesData.aspx", "newwin", "height=475,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }


        function WebReportPopup(strValue) {

            var win = window.open("../WebReport/DAMANAuthorizationRequest.aspx?PatientId=" + strValue, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }

        function ScandCardPopup(strValue) {
            openInNewTab("../../WebReports/ScandCardPrint.aspx?PT_ID=" + strValue + ",newwin1", "top=80,left=100,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");

        }



        function GeneralConsentPopup(strValue, GCID) {
            var win = window.open("../WebReport/GeneralConsentReport.aspx?PatientId=" + strValue + "&GCID=" + GCID, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }


        function HistoryPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("PatientVisitHistory.aspx?Value=" + PtId, "newwin1", "top=200,left=100,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function InvoicePopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("PatientInvoicePopup.aspx?Value=" + PtId, "newwin1", "top=80,left=80,height=650,width=1100,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function ReportPopup(RptName, Mode, PrintCount) {

            var win = window.open("../../CReports/ReportViewer.aspx?ReportName=" + RptName + "&Mode=" + Mode + "&PrintCount=" + PrintCount, "newwin1", "left=200,top=80,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();


        }

        function ReportPrintPopup(RptName, Mode, PrintCount) {

            var win = window.open("../../CReports/PrintCrystalReports.aspx?ReportName=" + RptName + "&Mode=" + Mode + "&PrintCount=" + PrintCount, "newwin1", "left=200,top=80,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();


        }

        function GCHistoryPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("GeneralConsentHistory.aspx?PatientId=" + PtId, "newwin1", "top=200,left=300,height=550,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }


        function AppointmentPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var DrID = document.getElementById('<%=txtDoctorID.ClientID%>').value
            var win = window.open("AppointmentEMRDr.aspx?DrID=" + DrID + "&PatientID=" + PtId, "Appt", "top=200,left=300,height=650,width=900,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
        }



        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }


        function ShowOnLoad(Report, DBString, Formula, PrintCount) {

            var LabelDirectPrint = '<%= Convert.ToString(Session["HMS_LABEL_DIRECT_PRINT"])%>'
            var LocalReportExePath = '<%= Convert.ToString(Session["HMS_LOCAL_REPORT_EXE_PATH"])%>'

            var Criteria = " 1=1 "
            if (Report == "HmsLabel.rpt") {

                if (LabelDirectPrint == "1") {
                    // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + Formula + '\'$PRINT$$$' + PrintCount);
                    Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + Formula + '\'';
                    exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$PRINT$$$' + PrintCount);

                }
                else {
                    Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + Formula + '\'';

                    //if(PTVisitSQNo !="")
                    //{
                    //    Criteria += ' AND {HMS_PATIENT_VISIT.HPV_SEQNO}=' + PTVisitSQNo;
                    //}

                    var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
                    win.focus();
                }

            }
            else {
                if (LabelDirectPrint == "1") {
                    Criteria += ' AND {HMS_PATIENT_VISIT.HPV_SEQNO}=\'' + Formula + '\'';
                    // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_VISIT.HPV_SEQNO}=\'' + Formula + '\'$PRINT$$$1');
                    exec(LocalReportExePath + 'printRpt.exe', '-nomerge 1' + DBString + '$' + LocalReportExePath + 'Reports\\' + Report + '$' + Criteria + '$PRINT$$$1');
                }
                else {
                    var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_VISIT.HPV_SEQNO}=\'' + Formula + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
                    win.focus();
                }

            }

        }

        function ShowClaimPrintPDF(Report, FileNo, BranchId) {

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


        function ShowMRRPrintPDF(FileNo) {

            var Report = "HmsMRRPrint.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowGCReportPDF(FileNo) {
            var Report = "GeneralTreatmentConsent1.rpt";

            //var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowGridGCReportPDF(GCId) {
            var Report = "GeneralTreatmentConsent1.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HGC_ID}=' + GCId + '', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


        function ShowGCReport1(GCId) {
            window.open('../../CReports/ReportViewer.aspx?ReportName=DamanConscent.rpt&SelectionFormula={GeneralConsentView.HGC_ID}=' + GCId, '_new', 'menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1')

        }

        function ShowDamanGCReportPDF(FileNo) {
            var Report = "MedConsent1.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowConsentFormReportPDF() {
            //var Report = "MedConsent1.rpt";

            var FileNo = document.getElementById('<%=hidActualFileNo.ClientID%>').value
            var Report = document.getElementById('<%=drpConsentForm.ClientID%>').value + ".rpt"
            var FileDescription = '<%= Convert.ToString(ViewState["FileDescription"]).ToUpper() %>'

            if (FileDescription == "SMCH") {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&Type=pdf&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            }
            else {
                var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&Type=pdf&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            }



            win.focus();

        }

        function ShowVisitClaimPrintPDF(Report, FileNo, PTVisitSQNo) {
            var Criteria = " 1=1 ";
            Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'';
            // Criteria += ' AND {HMS_PATIENT_VISIT.HPV_PT_ID}=\'' + FileNo + '\'';
            // Criteria += '  AND  {HMS_PATIENT_VISIT.HPV_SEQNO}=' + PTVisitSQNo;
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

        function ShowSignaturePopup() {
            document.getElementById("divSignaturePopup").style.display = 'block';
        }

        function HideSignaturePopup() {

            document.getElementById("divSignaturePopup").style.display = 'none';

        }


        function ShowSignature(DBString, FileName, BranchId) {
            var LocalSignaturePadtExePath = '<%= Convert.ToString(Session["HMS_LOCAL_SIGNATUREPAD_EXE_PATH"])%>'
            var SignaturePad = document.getElementById('<%=hidSignaturePad.ClientID%>').value
            var FileID = document.getElementById('<%=txtFileNo.ClientID%>').value

            if (SignaturePad == 'GENIUS') {
                // exec('D:\\HMS\\GeniusSignature\\HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
                exec(LocalSignaturePadtExePath + 'HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
            }
            else if (SignaturePad == 'WACOM') {

                // exec('D:\\HMS\\NewWacomSignature\\NewWacomSignature.exe', BranchId + '$' + FileID);
                exec(LocalSignaturePadtExePath + 'NewWacomSignature.exe', BranchId + '$' + FileID);

            }
            else {
                //exec('D:\\HMS\\GeniusSignature\\HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
                exec(LocalSignaturePadtExePath + 'HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
            }

        }


        function GetEmiratesIdData(DBString, FileName, BranchId) {

            // exec('D:\\HMS\\EIDA_CardDisplay\\EIDA_CardDisplay.exe', DBString + '$' + FileName + '$' + BranchId);
            var LocalEmiratesIDExePath = '<%= Convert.ToString(Session["HMS_LOCAL_EMIRATESID_EXE_PATH"])%>'
            exec(LocalEmiratesIDExePath + 'EIDA_CardDisplay.exe', DBString + '$' + FileName + '$' + BranchId);
        }


        function ShowGeneralConsentWacomSign(BranchId, HGC_ID, SignType) {

            var FileID = document.getElementById('<%=hidActualFileNo.ClientID%>').value

            //alert(BranchId);
            //alert(HGC_ID);
            //alert(SignType);
            //alert(FileID);

            exec('D:\\HMS\\GeneralConsentWacomSign\\GeneralConsentWacomSign.exe', BranchId + '$' + FileID + '$' + HGC_ID + '$' + SignType);

        }

        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }



    </script>

    <script language="javascript" type="text/javascript">


        function EmiratesIDFormat(Content) {


            if (Content.length == 3) {

                document.getElementById("<%= txtEmiratesID.ClientID %>").value = Content + '-';
            }
            if (Content.length == 8) {

                document.getElementById("<%= txtEmiratesID.ClientID %>").value = Content + '-';
            }
            if (Content.length == 16) {

                document.getElementById("<%= txtEmiratesID.ClientID %>").value = Content + '-';
            }

        }


        function CheckEmiratesIDFormat() {

            var data = document.getElementById("<%= txtEmiratesID.ClientID %>").value;

            var FormatedData = '';
            var FormatedData1 = FormatedData1 = data.substr(0, 3);
            var FormatedData2 = '', FormatedData3 = '', FormatedData4 = '';

            if (data.substr(3, 1) != '-') {
                FormatedData1 = data.substr(0, 3) + '-'

            }


            if (data.length >= 8) {
                if (data.substr(8, 1) != '-') {

                    FormatedData2 = data.substr(3, 4) + '-';


                }
                else {
                    FormatedData2 = data.substr(3, 4)
                }
            }
            else {

                FormatedData2 = data.substr(3, 4)
            }


            if (data.length >= 13) {
                if (data.substr(13, 1) != '-') {

                    FormatedData3 = data.substr(8, 7) + '-';


                }
            }


            FormatedData = FormatedData1 + FormatedData2 + FormatedData3;

            document.getElementById("<%= txtEmiratesID.ClientID %>").value = FormatedData;


        }
    </script>

    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
            // alert(today);

            // fnPageLoad();

        });
    </script>


    <script language="javascript" type="text/javascript">

        function ShowConfirmMessage(strMessage) {



            document.getElementById("<%=divConfirmMessage.ClientID%>").style.display = 'block';
            document.getElementById("<%=lblConfirmMessage.ClientID%>").innerHTML = strMessage;

        }


        function HideConfirmMessage() {

            document.getElementById("<%=divConfirmMessage.ClientID%>").style.display = 'none';
            document.getElementById("<%=lblConfirmMessage.ClientID%>").innerHTML = '';
        }



        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

            if (vColor != '') {

                document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


            }

            document.getElementById("divMessage").style.display = 'block';



        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }


        function FormatEmiratesID() {
            var EmiratesID = document.getElementById("<%=txtEmiratesID.ClientID%>").value;
            var IDCaption = document.getElementById("<%=drpIDCaption.ClientID%>").value;

            var forEmiratesID

            if (IDCaption == 'EmiratesId') {
                // alert( EmiratesID.substring(4, 2))

                if (EmiratesID.length == 3) {

                    forEmiratesID = EmiratesID + '-';

                    document.getElementById("<%=txtEmiratesID.ClientID%>").value = forEmiratesID;

                }


                if (EmiratesID.length == 8) {

                    forEmiratesID = EmiratesID + '-';

                    document.getElementById("<%=txtEmiratesID.ClientID%>").value = forEmiratesID;


                }


                if (EmiratesID.length == 16) {

                    forEmiratesID = EmiratesID + '-';

                    document.getElementById("<%=txtEmiratesID.ClientID%>").value = forEmiratesID;


                }


            }

        }


        function ShowPlanPopup() {

            document.getElementById("divPlanPopup").style.display = 'block';


        }

        function HidePlanPopup() {

            document.getElementById("divPlanPopup").style.display = 'none';

            document.getElementById("<%=txtSrcPlanName.ClientID%>").value = '';
        }

        function NewAppointmentPopup() {
            var stFileNoe, strDrID;
            stFileNoe = document.getElementById("<%=txtFileNo.ClientID%>").value;
            strDrID = document.getElementById("<%=txtDoctorID.ClientID%>").value;

            var win = window.open("AppointmentPopup.aspx?PageName=PTReg&AppId=0&Date=0&PatientID=" + stFileNoe + "&Time=0&DrId=" + strDrID, "newwin", "top=200,left=270,height=470,width=750,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();

        }

        function ShowMalaffiDtls() {

            var stFileNoe;
            stFileNoe = document.getElementById("<%=txtFileNo.ClientID%>").value;
            if (stFileNoe != '') {
                var win = window.open('../../PatientMalaffiDtls.aspx?PatientID=' + stFileNoe, 'PatientMalaffiDtls', 'menubar=no,left=100,top=150,height=700,width=1075,scrollbars=1')
            }

        }


        function AppointmentListDayPopup() {
            var win = window.open("AppointmentListDay.aspx?PageName=ApptDay", "newwin", "top=100,left=100,height=700,width=1100,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();

        }

        function ShowEnquiryMaster() {

            document.getElementById("divEnquiryMaster").style.display = 'block';


        }

        function HideEnquiryMaster() {

            document.getElementById("divEnquiryMaster").style.display = 'none';

            document.getElementById("<%=txtEnquirSearch.ClientID%>").value = '';
        }

        function ShowReferralList() {

            document.getElementById("divReferralList").style.display = 'block';


        }

        function HideReferralList() {

            document.getElementById("divReferralList").style.display = 'none';

        }


        function ShowVisitHistory() {

            document.getElementById("divVisitHistory").style.display = 'block';


        }

        function HideVisitHistory() {

            document.getElementById("divVisitHistory").style.display = 'none';

        }


        function ShowStaffList() {

            document.getElementById("divStaffList").style.display = 'block';


        }

        function HideStaffList() {

            document.getElementById("divStaffList").style.display = 'none';

        }

        function ShowVisitConsentFormReportPDF(FileNo, SeqNo, DrId, DateDesc) {


            var Report = document.getElementById('drpVisitConsentForm').value + "_Visit.rpt"

            var Criteria = " 1=1 ";

            Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'';
            if (SeqNo != "") {

                Criteria += ' AND {HMS_PATIENT_VISIT.HPV_SEQNO}=\'' + SeqNo + '\'';
            }
            else {
                Criteria += ' AND {HMS_PATIENT_VISIT.HPV_DR_ID}=\'' + DrId + '\'';
                Criteria += ' AND {HMS_PATIENT_VISIT.HPV_DATE}=date(\'' + DateDesc + '\')';
            }

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }



    </script>
    <script type="text/javascript">
        function movetoNext(current, nextFieldID) {
            if (current.value.length >= current.maxLength) {
                document.getElementById(nextFieldIDocus());
            }

        }    </script>

    <script type="text/javascript">
        function toggleV2Data(tab) {
            var tabTr = document.getElementById(tab);

            if (tab != 'v2Data')
                document.getElementById('v2Data').style.display = 'none';
            if (tab != 'homeAddress')
                document.getElementById('homeAddress').style.display = 'none';
            if (tab != 'workAddress')
                document.getElementById('workAddress').style.display = 'none';

            document.getElementById('v2DataIcon').src = "images/expand.png";
            document.getElementById('homeAddressIcon').src = "images/expand.png";
            document.getElementById('workAddressIcon').src = "images/expand.png";

            tabTr.style.display = tabTr.style.display == 'none' ? 'block' : 'none';

            if (tabTr.style.display == 'block')
                document.getElementById(tab + 'Icon').src = "images/collapse.png";
        }

        function toggleFamilyBook(tab) {
            var tabTr = document.getElementById(tab);


            tabTr.style.display = tabTr.style.display == 'none' ? 'block' : 'none';

            if (tabTr.style.display == 'block') {
                document.getElementById(tab + 'Icon').src = "images/collapse.png";
            }
            else {
                document.getElementById(tab + 'Icon').src = "images/expand.png";
            }
        }

        function GetEIDData() {
            Initialize();
            GetPublicData();


            var FullName = GetFullName();

            var EmiratesID = GetIDNumber();
            var DOB = GetDateOfBirth();
            var Sex = GetSex();
            var MaritalStatus = GetMaritalStatus();
            var POBox = GetHomeAddress_POBox();
            var Street = GetHomeAddress_Street();
            var AreaDesc = GetHomeAddress_AreaDesc();

            var City = GetHomeAddress_CityDesc();
            var EmirateDesc = GetHomeAddress_EmirateDesc();
            var Nationality = GetNationality();

            var ResidentPhoneNumber = GetHomeAddress_ResidentPhoneNumber();
            var MobilePhoneNumber = GetHomeAddress_MobilePhoneNumber();
            var Email = GetHomeAddress_Email();
            var CompanyName = GetWorkAddress_CompanyName();


            vImageData = GetPhotography();
            document.getElementById('<%=hidPatientEIDPhoto.ClientID%>').value = vImageData;



             // var x = document.getElementById("EIDAWebComponent");
             //  document.getElementById('<%=imgPatientPhoto.ClientID%>').src = 'data:image/jpeg;base64,' + hexToBase64(document.getElementById('<%=hidPatientEIDPhoto.ClientID%>').value);

             if (EmiratesID != '') {
                 document.getElementById('<%=hidEmirateIDData.ClientID%>').value = EmiratesID + '|' + FullName + '|' + DOB + '|' + Sex + '|' + MaritalStatus + '|' + POBox + '|' + AreaDesc + '|' + City + '|' + EmirateDesc + '|' + Nationality + '|' + ResidentPhoneNumber + '|' + MobilePhoneNumber + '|' + Email + '|' + CompanyName;
             }



         }

         function hexToBase64(str) {
             return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
         }
    </script>
    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: auto; width: 600px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>
                        <br />
                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>
                    </div>
                    <span style="float: right; margin-right: 20px">

                        <input type="button" id="Button5" class="ButtonStyle gray" style="font-weight: bold; cursor: pointer; width: 50px;" value=" OK " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="padding-top: 0px; padding-left: 2px; width: 99%; height: 800px; border-color: #D64535; border-style: solid; border-width: thin;">

        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
            <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Patient Information " Width="100%">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <input type="hidden" id="hidPermission" runat="server" value="9" />
                            <asp:HiddenField ID="hidTokenPrint" runat="server" />
                            <asp:HiddenField ID="hidWaitingList" runat="server" />
                            <asp:HiddenField ID="hidLabelPrint" runat="server" />
                            <asp:HiddenField ID="Year" runat="server" />
                            <asp:HiddenField ID="hidTodayDate" runat="server" />
                            <asp:HiddenField ID="hidTodayTime" runat="server" />
                            <asp:HiddenField ID="hidTodayDBDate" runat="server" />
                            <asp:HiddenField ID="hidLatestVisit" runat="server" />
                            <asp:HiddenField ID="hidLatestVisitHours" runat="server" />
                            <asp:HiddenField ID="hidLastVisitDrId" runat="server" />
                            <asp:HiddenField ID="hidMultipleVisitADay" runat="server" />
                            <asp:HiddenField ID="hidPopup" runat="server" />
                            <asp:HiddenField ID="hidInfoPopup" runat="server" />
                            <asp:HiddenField ID="hidImgFront" runat="server" />
                            <asp:HiddenField ID="hidHospitalName" runat="server" />

                            <asp:HiddenField ID="hidPTVisitSQNo" runat="server" />
                            <asp:HiddenField ID="hidActualFileNo" runat="server" />
                            <asp:HiddenField ID="hidLocalDate" runat="server" />
                            <asp:HiddenField ID="hidSignaturePad" runat="server" Value="WACOM" />

                            <asp:HiddenField ID="hidPateName" runat="server" Value="" />

                            <asp:HiddenField ID="hidInsCardPath" runat="server" Value="" />

                            <input type="hidden" id="hidErrorChecking" runat="server" value="true" />

                            <input type="hidden" id="hidEmirateIDData" runat="server" value="" />
                            <input type="hidden" id="hidPatientEIDPhoto" runat="server" value="" />



                            <table cellpadding="0" cellspacing="0" width="85%">
                                <tr>
                                    <td style="width: 25%">
                                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                        <a id="lnkMalaffi" runat="server" class="lblCaption1" title="Malaffi" onclick="ShowMalaffiDtls()" style="cursor: pointer; color: #005c7b; font-weight: bold;" visible="false">
                                            <img src='<%= ResolveUrl("~/Images/MalaffiLogo.png")%>' style="width: 60px; height: 60px;">
                                        </a>
                                        &nbsp; 
                                        <a id="lnkEnquiryMaster" runat="server" class="lblCaption1" onclick="ShowEnquiryMaster()" style="cursor: pointer; color: #005c7b; font-weight: bold;" visible="false">Enquiry Master</a>

                                    </td>
                                    <td style="width: 25%">
                                        <asp:Label ID="lblEFilling" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                        <span style="float: right;">
                                            <asp:UpdatePanel ID="UpdatePanel61" runat="server">
                                                <ContentTemplate>
                                                    <asp:Image ID="imgPatientPhoto" runat="server" Height="60px" Width="60px" ImageUrl="~/Images/NoImage.png" BorderStyle="Groove"></asp:Image>
                                                    <object id="EIDAWebComponent" style="border: solid 1px gray; display: none;" classid="CLSID:A4B3BB86-4A99-3BAE-B211-DB93E8BA008B"
                                                        width="60" height="60">
                                                    </object>
                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </span>
                                    </td>

                                    <td style="width: 10%; text-align: right;">
                                        <asp:UpdatePanel ID="UpPanelSave" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" Text="Save" OnClientClick="return  ValPTSave();"
                                                    OnClick="btnSave_Click"
                                                    Enabled="true" />

                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSave">
                                            <ProgressTemplate>
                                                <div id="overlay">
                                                    <div id="modalprogress">
                                                        <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                                                    </div>
                                                </div>

                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                    <td style="width: 40%">
                                        <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="btnClear" runat="server" class="button gray small" Width="70px" Text="Cancel" OnClick="btnClear_Click" OnClientClick="Val1();" />
                                                <asp:Button ID="btnNewApp" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px" Text="Book Appt." CssClass="button gray small" OnClientClick="return NewAppointmentPopup()" />
                                                <asp:Button ID="btAppList" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px" Text="App. List" CssClass="button gray small" OnClientClick="return AppointmentListDayPopup()" />
                                                <asp:Button ID="btnHaadRegister" runat="server" class="button gray small" Style="padding-left: 5px; padding-right: 5px; width: 70px;" Visible="false" Text="DOH Reg." OnClick="btnHaadRegister_Click" OnClientClick="return ValRegister();" />
                                                <asp:Button ID="btnIDPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button gray small" Text="ID Print" OnClick="btnIDPrint_Click" />
                                                <asp:Button ID="btnReadEID" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button gray small" Text="Read EID" Visible="false" OnClick="btnReadEID_Click" OnClientClick="GetEIDData();" />

                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>




                                </tr>
                            </table>
                            <asp:Timer ID="Timer1" runat="server" Enabled="false" Interval="8000"></asp:Timer>

                            <table border="0" cellpadding="0" cellspacing="0" style="width: 85%">
                                <tr>
                                    <td style="width: 50%; vertical-align: top; padding-top: 5px; padding-left: 5px;">
                                        <fieldset style="width: 98%; border: 1px solid #999; border-style: solid;">
                                            <legend class="lblCaption1">General Information</legend>
                                            <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Button ID="btnNew" runat="server" Style="padding-left: 5px; padding-right: 5px;" Text="New" Visible="false" CssClass="button gray small" Width="50px" OnClick="btnNew_Click" />

                                                            </ContentTemplate>

                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblHomeCarePT" runat="server" CssClass="lblCaption1" Style="color: red; font-weight: bold;" Text="Home Care Patient" Visible="false"></asp:Label>
                                                        <asp:TextBox ID="txtEmirateIDData" runat="server" Width="50px" Style="position: absolute; left: 120px; top: 170px" AutoPostBack="true" OnTextChanged="txtEmirateIDData_TextChanged" TabIndex="1" Visible="false"></asp:TextBox>

                                                    </td>
                                                    <div id="divDept" runat="server" visible="false">
                                                        <td class="lblCaption1" style="height: 40px;">Department
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:DropDownList>
                                                        </td>
                                                    </div>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblVisitType" runat="server" CssClass="lblCaption1" Text="New" ForeColor="Maroon"></asp:Label>
                                                        <asp:Label ID="Label2" runat="server" CssClass="lblCaption1" Text="File No"></asp:Label>
                                                        <span style="color: red; font-size: 11px;">*</span>

                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtFileNo" runat="server" MaxLength="15" CssClass="TextBoxStyle" Width="150px" AutoPostBack="true" onkeypress="return PatientPopup('FileNo',this.value,event)" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>
                                                                <asp:CheckBox ID="chkManual" runat="server" CssClass="lblCaption1" OnCheckedChanged="chkManual_CheckedChanged"
                                                                    Text="Manual#" AutoPostBack="True" Checked="false" />

                                                            </ContentTemplate>

                                                        </asp:UpdatePanel>


                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" CssClass="lblCaption1" Text="PT.Type"></asp:Label>
                                                        <span style="color: red; font-size: 11px;">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpPatientType" runat="server" CssClass="TextBoxStyle" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="drpPatientType_SelectedIndexChanged">
                                                            <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                            <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="Active" Checked="true" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label4" runat="server" CssClass="lblCaption1" Text="Name"></asp:Label>
                                                        <span style="color: red; font-size: 11px;">*</span>

                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="drpTitle" runat="server" CssClass="label" Width="70px" Visible="false">
                                                        </asp:DropDownList>

                                                        <asp:TextBox ID="txtFName" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="150px" onkeypress="return PatientPopup('Name',this.value,event)" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>


                                                        <asp:TextBox ID="txtMName" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="100px" onkeypress="return PatientPopup('Name',this.value,event)" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>



                                                        <asp:TextBox ID="txtLName" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="120px" onkeypress="return PatientPopup('Name',this.value,event)" ondblclick="return PatientPopup1('LName',this.value);"></asp:TextBox>


                                                    </td>



                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label7" runat="server" CssClass="lblCaption1" Text="DOB"></asp:Label>
                                                        <asp:Label ID="lblDBOMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="lblCaption1" colspan="3">
                                                        <asp:TextBox ID="txtDOB" runat="server" Width="75px" CssClass="TextBoxStyle" MaxLength="10" onblur="AgeCalculation()"></asp:TextBox>
                                                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                                            Enabled="True" TargetControlID="txtDOB" Format="dd/MM/yyyy">
                                                        </asp:CalendarExtender>

                                                        <asp:Label ID="Label120" runat="server" CssClass="label" Text="Age"></asp:Label>
                                                        <asp:TextBox ID="txtAge" runat="server" Width="22px" MaxLength="3" Style="text-align: center;" CssClass="TextBoxStyle" onblur="DBOcalculation()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        <asp:DropDownList ID="drpAgeType" runat="server" CssClass="lblCaption1" Width="55px">
                                                            <asp:ListItem Text="Year" Value="Y" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Month" Value="M"></asp:ListItem>
                                                            <asp:ListItem Text="Days" Value="D"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="txtMonth" runat="server" Width="22px" MaxLength="2" Style="text-align: center;" CssClass="TextBoxStyle"></asp:TextBox>
                                                        <asp:DropDownList ID="drpAgeType1" runat="server" CssClass="lblCaption1" Width="55px">
                                                            <asp:ListItem Text="Month" Value="M"></asp:ListItem>
                                                            <asp:ListItem Text="Days" Value="D"></asp:ListItem>
                                                        </asp:DropDownList>&nbsp; 
                                                          Gender <span style="color: red; font-size: 11px;">*</span>
                                                        <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle" Width="75px">
                                                            <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                                            <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label14" runat="server" CssClass="lblCaption1" Text="Marital"></asp:Label>
                                                        <asp:Label ID="lblMStatusMand" runat="server" Style="color: red; font-size: 11px;" Text="*"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpMStatus" CssClass="TextBoxStyle" runat="server" Width="150px">
                                                            <asp:ListItem Selected="True" Text="--- Select ---" Value="" />
                                                            <asp:ListItem>Single</asp:ListItem>
                                                            <asp:ListItem>Married</asp:ListItem>
                                                            <asp:ListItem>Widowed</asp:ListItem>
                                                            <asp:ListItem>Divorced</asp:ListItem>
                                                            <asp:ListItem>Not Specified</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="lblCaption1">
                                                        <asp:Label ID="lblPOBox" runat="server" CssClass="lblCaption1" Text="P O Box"></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:TextBox ID="txtPoBox" runat="server" MaxLength="50" CssClass="TextBoxStyle"
                                                            onkeypress="return PatientPopup('POBox',this.value,event)" Width="150px" ondblclick="return PatientPopup1('POBox',this.value);"></asp:TextBox>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label8" runat="server" CssClass="lblCaption1" Text="Address"></asp:Label>
                                                        <asp:Label ID="lblAddressMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtAddress" CssClass="TextBoxStyle" Style="resize: none;" TextMode="MultiLine" runat="server" Height="30px" Width="98%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label9" runat="server" CssClass="lblCaption1" Text="City"></asp:Label>
                                                        <asp:Label ID="lblCityMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpCity" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList></td>
                                                    <td>
                                                        <asp:Label ID="Label12" runat="server" CssClass="lblCaption1" Text="Area"></asp:Label>
                                                    </td>
                                                    <td class="auto-style43">
                                                        <asp:DropDownList ID="drpArea" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                        <asp:Label ID="Label10" runat="server" CssClass="lblCaption1" Text="Country Of Residence"></asp:Label>
                                                        <asp:Label ID="lblCountryMand" runat="server" Style="color: red; font-size: 11px;" Text="*"  Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpCountry" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label13" runat="server" CssClass="lblCaption1" Text="Nationality"></asp:Label><span style="color: red; font-size: 11px;">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblBlood" runat="server" CssClass="lblCaption1" Text="Blood"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpBlood" CssClass="TextBoxStyle" runat="server" Width="150px">
                                                            <asp:ListItem Selected="True" Text="--- Select ---" Value="" />
                                                            <asp:ListItem>A +ve</asp:ListItem>
                                                            <asp:ListItem>A -ve</asp:ListItem>
                                                            <asp:ListItem>B +ve</asp:ListItem>
                                                            <asp:ListItem>B -ve</asp:ListItem>
                                                            <asp:ListItem>O +ve</asp:ListItem>
                                                            <asp:ListItem>O -ve</asp:ListItem>
                                                            <asp:ListItem>AB +ve</asp:ListItem>
                                                            <asp:ListItem>AB -ve</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>

                                                    <td>
                                                        <asp:Label ID="Label26" runat="server" CssClass="lblCaption1" Text="Emirates"></asp:Label>
                                                        <asp:Label ID="lblEmiratesMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpEmirates" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>
                                                <tr>

                                                    <td>
                                                        <asp:DropDownList ID="drpIDCaption" runat="server" CssClass="TextBoxStyle" Width="100px">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lblEmiratesIDMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtEmiratesID" runat="server" CssClass="TextBoxStyle" MaxLength="18" Width="150px" onkeypress="return PatientPopup('EmiratesID',this.value,event)" ondblclick="return PatientPopup1('EmiratesID',this.value);" onkeyup="FormatEmiratesID();"></asp:TextBox>
                                                        <%--<asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtEmiratesID" Mask="999-9999-9999999-9" MaskType="None"   CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>--%>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label23" runat="server" CssClass="lblCaption1" Text="Religion"></asp:Label>
                                                        <asp:Label ID="lblReligionMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpReligion" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label15" runat="server" CssClass="lblCaption1" Text="Home PH. No."></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPhone1" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Phone',this.value,event)" ondblclick="return PatientPopup1('Phone1',this.value);"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMobile1" runat="server" CssClass="lblCaption1" Text="Mobile1"></asp:Label>
                                                        <span style="color: red; font-size: 11px;">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtMobile1" runat="server" CssClass="TextBoxStyle" CausesValidation="True" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Mobile',this.value,event)" AutoPostBack="true" OnTextChanged="txtMobile1_TextChanged" ondblclick="return PatientPopup1('Mobile1',this.value);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMobile2" runat="server" CssClass="lblCaption1" Text="Mobile2"></asp:Label>
                                                    </td>
                                                    <td class="auto-style49">
                                                        <asp:TextBox ID="txtMobile2" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Mobile',this.value,event)" ondblclick="return PatientPopup1('Mobile2',this.value);"></asp:TextBox>
                                                    </td>

                                                    <td>
                                                        <asp:Label ID="lblOfficePhNo" runat="server" CssClass="lblCaption1" Text="Office PH"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPhone3" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label44" runat="server" CssClass="lblCaption1" Text="Email"></asp:Label>
                                                        <asp:Label ID="lblEmailMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtEmail" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFax" runat="server" CssClass="lblCaption1" Text="Fax"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFax" runat="server" CssClass="TextBoxStyle" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                        <asp:Label ID="lblOccupation" runat="server" CssClass="lblCaption1" Text="Occupation"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtOccupation" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>

                                                    </td>

                                                    <td>
                                                        <asp:Label ID="lblKnowFrom" runat="server" CssClass="lblCaption1"
                                                            Text="Know From"></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:DropDownList ID="drpKnowFrom" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:DropDownList>
                                                    </td>


                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label17" runat="server" CssClass="lblCaption1" Text="Patient Status"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpPTStatus" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                            <asp:ListItem Value="Open">Open</asp:ListItem>
                                                            <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>

                                                    <td>
                                                        <asp:Label ID="Label11" runat="server" CssClass="lblCaption1" Text="Visa Type"></asp:Label>
                                                        <asp:Label ID="lblVisaTypeMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpVisaType" CssClass="TextBoxStyle" runat="server" Width="150px">
                                                            <asp:ListItem Selected="True" Text="--- Select ---" Value="" />
                                                            <asp:ListItem>Resident</asp:ListItem>
                                                            <asp:ListItem>Tourist</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </td>

                                                </tr>
                                                <tr>
                                                     <td>
                                                        <asp:Label ID="Label34" runat="server" CssClass="lblCaption1" Text="PT Registered Type"></asp:Label>
                                                         <asp:Label ID="lblPtRegType" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpPtRegisteredType" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label18" runat="server" CssClass="lblCaption1" Text="Remarks"></asp:Label>
                                                        <asp:Label ID="Label30" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtRemarks" CssClass="TextBoxStyle" Style="resize: none;" TextMode="MultiLine" runat="server" Height="30px" Width="98%"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label33" runat="server" CssClass="lblCaption1" Text="Kin Details" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr>

                                                    <td>
                                                        <asp:Label ID="Label27" runat="server" CssClass="lblCaption1" Text="Kin Name"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKinName" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label24" runat="server" CssClass="lblCaption1" Text="Relation"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpKinRelation" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label28" runat="server" CssClass="lblCaption1" Text="Phone.No."></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKinPhone" CssClass="TextBoxStyle" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label25" runat="server" CssClass="lblCaption1" Text="Mobile No."></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKinMobile" CssClass="TextBoxStyle" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label29" runat="server" CssClass="lblCaption1" Text=" No Of Children"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtNoOfChildren" CssClass="TextBoxStyle" MaxLength="2" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMobile3" runat="server" CssClass="lblCaption1" Text="Other  Mob.No."></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtKinMobile1" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>




                                            <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">


                                                <tr>


                                                    <div id="divCunsult" runat="server" visible="false">
                                                        <td colspan="5" class="lblCaption1" style="color: #0094ff">Last Consultation Date:&nbsp;
                                                         <asp:Label ID="lblLastCunsultDate" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                            &nbsp;&nbsp;&nbsp; Amount:&nbsp;
                                                         <asp:Label ID="lblLastCunsultAmount" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>

                                                        </td>
                                                    </div>
                                                </tr>

                                            </table>


                                        </fieldset>
                                    </td>

                                    <td style="width: 50%; vertical-align: top; padding-top: 12px; text-align: left;">
                                        <fieldset style="width: 88%; border: 1px solid #999; border-style: solid;">

                                            <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                                            <ContentTemplate>

                                                                <asp:CheckBox ID="chkFamilyMember" runat="server" Width="120px" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkFamilyMember_CheckedChanged" Text="Family Member" />

                                                                <asp:ImageButton ID="ImgbtnSig1" runat="server" ImageUrl="~/HMS/Images/Sing1.jpg" ToolTip="Signature Scan" Height="20PX" Width="25pX" OnClick="ImgbtnSig1_Click" />

                                                                &nbsp;
                                                        <asp:LinkButton ID="lnkViewSig1" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="lnkViewSig1_Click"></asp:LinkButton>
                                                                &nbsp;&nbsp;&nbsp;
                                               
                                                    <asp:ImageButton ID="btnScan" runat="server" ImageUrl="~/HMS/Images/EmiratesID.Png" ToolTip="Emirate Id Scan" Height="20PX" Width="25pX" OnClick="btnScan_Click" Visible="false" />
                                                                &nbsp; 
                                                        <asp:LinkButton ID="lnkEmIdShow" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="lnkEmIdShow_Click" Visible="false"></asp:LinkButton>

                                                                <asp:Button ID="bntFamilyApt" runat="server" CssClass="button gray small" Style="padding-left: 5px; padding-right: 5px; width: 120px;" Text="Check Family Appt." OnClick="bntFamilyApt_Click" />

                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkEmIdShow" />

                                                            </Triggers>
                                                        </asp:UpdatePanel>


                                                    </td>

                                                </tr>



                                            </table>
                                        </fieldset>

                                        <div id="divPriority" runat="server">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label86" runat="server" CssClass="lblCaption1" Text="Priority"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:DropDownList ID="drpPriority" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:DropDownList>
                                                </td>
                                            </tr>
                                        </div>

                                        <fieldset style="width: 88%; border: 1px solid #999; border-style: solid;">
                                            <legend class="lblCaption1">Insurance Details</legend>
                                            <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                                <tr runat="server" id="trTPA">
                                                    <td style="height: 10px;" class="lblCaption1">TPA/Insurance
                                                       
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="drpParentCompany" runat="server" CssClass="TextBoxStyle" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="drpParentCompany_SelectedIndexChanged"></asp:DropDownList>


                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 10px;" class="lblCaption1">Company / NW <span style="color: red; font-size: 11px;">*</span>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="drpDefaultCompany" CssClass="TextBoxStyle" runat="server" class="TextBoxStyle" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="drpDefaultCompany_SelectedIndexChanged"></asp:DropDownList>

                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="lblCaption1" runat="server" id="tdCaptionNetwork">Network 
                                                    </td>
                                                    <td runat="server" id="tdNetwork" colspan="3">
                                                        <asp:DropDownList ID="drpNetworkName" CssClass="TextBoxStyle" runat="server" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="drpNetworkName_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="lblCaption1">
                                                        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                                            <ContentTemplate>
                                                                Plan    <span style="color: red; font-size: 11px;">*</span>
                                                                <asp:ImageButton ID="imgPlanPopup" runat="server" ImageUrl="~/Images/Zoom.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Show Plan Type" Height="15PX" Width="20PX" OnClick="imgPlanPopup_Click" />

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>



                                                    </td>
                                                    <td colspan="3">

                                                        <asp:DropDownList ID="drpPlanType" CssClass="TextBoxStyle" runat="server" Width="250px" Enabled="true" AutoPostBack="True" OnSelectedIndexChanged="drpPlanType_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton ID="btnHCBShow" runat="server" ImageUrl="~/Images/Zoom.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Category Lookup" Height="15PX" Width="20PX" OnClick="btnHCBShow_Click" />

                                                        <asp:ImageButton ID="imgPlanRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Plan Type Refresh" OnClick="imgPlanRefresh_Click" />
                                                        <asp:Image ID="ImageButton1" runat="server" ImageUrl="~/Images/New.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Plan Type Add" Height="20PX" Width="22PX" onclick="return BenefitsExecPopup1();" />

                                                    </td>

                                                </tr>
                                                <tr>


                                                    <td>
                                                        <asp:Label ID="lblID" runat="server" CssClass="lblCaption1" Text="ID"></asp:Label>



                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPolicyId" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>

                                                        <asp:Label ID="lblNetworkClass" runat="server" CssClass="lblCaption1" Text="Package"></asp:Label>
                                                        <asp:Label ID="lblNetworkClassMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                                            <ContentTemplate>
                                                                <div id="divPlan" style="visibility: hidden;"></div>
                                                                <asp:TextBox ID="txtNetworkClass" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="50" AutoPostBack="true" OnTextChanged="txtNetworkClass_TextChanged"></asp:TextBox>

                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtNetworkClass" MinimumPrefixLength="1" ServiceMethod="GetGeneralClass"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divPlan">
                                                                </asp:AutoCompleteExtender>
                                                            </ContentTemplate>

                                                        </asp:UpdatePanel>

                                                    </td>

                                                </tr>


                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1" Text="Policy No."></asp:Label>
                                                        <asp:Label ID="lblPolicyNoMand" runat="server" Style="color: red; font-size: 11px;" Text="*"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPolicyNo" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="30" onkeypress="return PatientPopup('PolicyNo',this.value,event)" ondblclick="return PatientPopup1('PolicyNo',this.value);"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCardNo1" runat="server" CssClass="lblCaption1" Text=" Card No."></asp:Label>
                                                        <asp:Label ID="lblCardNo1Mand" runat="server" Style="color: red; font-size: 11px;" Text="*"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIDNo" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                        <asp:Label ID="Label100" runat="server" CssClass="lblCaption1" Text="Effective Date"></asp:Label>
                                                        <span style="color: red; font-size: 11px;">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPolicyStart" runat="server" CssClass="TextBoxStyle" Width="150px" onblur="PastDateCheck()"></asp:TextBox>
                                                        <asp:CalendarExtender ID="CalendarExtender7" runat="server"
                                                            Enabled="True" TargetControlID="txtPolicyStart" Format="dd/MM/yyyy">
                                                        </asp:CalendarExtender>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label35" runat="server" CssClass="lblCaption1" Text="Exp Date"></asp:Label>
                                                        <span style="color: red; font-size: 11px;">*</span>

                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="TextBoxStyle" Width="150px" onblur="PastDateCheck()"></asp:TextBox>
                                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                                            Enabled="True" TargetControlID="txtExpiryDate" Format="dd/MM/yyyy">
                                                        </asp:CalendarExtender>
                                                    </td>


                                                </tr>

                                            </table>
                                        </fieldset>
                                        <table border="0" cellpadding="2" cellspacing="2" style="width: 85%">
                                            <tr>
                                                <td>&nbsp;
                                                        <asp:Label ID="lblPTCompany" runat="server" CssClass="lblCaption1"
                                                            Text="Payer "></asp:Label>
                                                    <asp:Label ID="lblPTCompanyMand" runat="server" Style="color: red; font-size: 11px;" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="drpPayer" CssClass="TextBoxStyle" runat="server" Width="155px">
                                                            </asp:DropDownList>
                                                            <div id="divPTCompany" style="visibility: hidden;"></div>
                                                            <asp:TextBox ID="txtPTCompany" CssClass="TextBoxStyle" runat="server" Width="200px" Visible="false"></asp:TextBox>
                                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtPTCompany" MinimumPrefixLength="1" ServiceMethod="GetPatientCompany" CompletionListElementID="divPTCompany"></asp:AutoCompleteExtender>

                                                        </ContentTemplate>

                                                    </asp:UpdatePanel>
                                                </td>
                                                <td>

                                                    <asp:DropDownList ID="drpConsentForm" CssClass="TextBoxStyle" runat="server" Width="70px" Visible="true">
                                                    </asp:DropDownList>
                                                    <asp:Button ID="btnConsentFormPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" Visible="true" CssClass="button gray small" Text="Con. Form" OnClientClick="ShowConsentFormReportPDF()" />
                                                    <asp:Button ID="btnDamanGC" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" Visible="false" CssClass="button gray small" Text="Daman Consent" OnClick="btnDamanGC_Click" />
                                                    <asp:Button ID="btnGeneConTreatment" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" Visible="false" CssClass="button gray small" Text="Gen. Consent" OnClick="btnGeneConTreatment_Click" />

                                                </td>
                                            </tr>
                                        </table>

                                        <div id="divDeductCoIns" runat="server">
                                            <fieldset style="width: 90%; border: 1px solid #999; border-style: solid;">

                                                <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label47" runat="server" CssClass="lblCaption1" Text="Deduct"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpDedType" runat="server" CssClass="TextBoxStyle" Width="50px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtDeductible" runat="server" Width="50px" Height="23px" ToolTip="Deductible & Co-pay" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label81" runat="server" CssClass="lblCaption1" Text="Co-Ins"></asp:Label>
                                                        </td>
                                                        <td>

                                                            <asp:DropDownList ID="drpCoInsType" runat="server" CssClass="TextBoxStyle" Width="50px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtCoIns" runat="server" Width="50px" Height="23px" ToolTip="Co-Insurance" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label6" runat="server" CssClass="lblCaption1" Text="Dental"></asp:Label>
                                                        </td>
                                                        <td>

                                                            <asp:DropDownList ID="drpCoInsDental" runat="server" CssClass="TextBoxStyle" Width="50px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtCoInsDental" runat="server" Width="50px" Height="23px" ToolTip="Dental Co-Insurance" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label16" runat="server" CssClass="lblCaption1" Text="Pharma"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpCoInsPharmacy" runat="server" CssClass="TextBoxStyle" Width="50px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtCoInsPharmacy" runat="server" Width="50px" Height="23px" ToolTip="Deductible & Co-pay" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label20" runat="server" CssClass="lblCaption1" Text="Lab"></asp:Label>
                                                        </td>
                                                        <td>

                                                            <asp:DropDownList ID="drpCoInsLab" runat="server" CssClass="TextBoxStyle" Width="50px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtCoInsLab" runat="server" Width="50px" Height="23px" ToolTip="Co-Insurance" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label22" runat="server" CssClass="lblCaption1" Text="Radio"></asp:Label>
                                                        </td>
                                                        <td>

                                                            <asp:DropDownList ID="drpCoInsRad" runat="server" CssClass="TextBoxStyle" Width="50px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtCoInsRad" runat="server" Width="50px" Height="23px" ToolTip="Dental Co-Insurance" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </fieldset>
                                        </div>

                                        <fieldset style="width: 85%; border: 1px solid #999; border-style: solid;">
                                            <table cellpadding="3" cellspacing="3" border="0">

                                                <tr>

                                                    <td class="lblCaption1">Card  Image - Front
                                                        <br>

                                                        <asp:Image ID="imgFrontDefault" runat="server" Height="150px" Width="230px" />
                                                        <asp:AsyncFileUpload ID="AsyncFileUpload3" runat="server"
                                                            OnUploadedComplete="AsyncFileUpload3_UploadedComplete"
                                                            OnClientUploadComplete="AsyncFileUpload3_uploadComplete"
                                                            Width="225px" FailedValidation="False" />


                                                    </td>

                                                    <td class="lblCaption1">Card  Image - Back
                                                        <br>
                                                        <asp:Image ID="imgBackDefault" runat="server" Height="150px" Width="230px" />
                                                        <asp:AsyncFileUpload ID="AsyncFileUpload4" runat="server"
                                                            OnUploadedComplete="AsyncFileUpload4_UploadedComplete"
                                                            OnClientUploadComplete="AsyncFileUpload4_uploadComplete"
                                                            Width="225px" FailedValidation="False" />

                                                    </td>
                                                </tr>

                                            </table>

                                        </fieldset>

                                        <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                            <tr>
                                                <td class="lblCaption1" style="vertical-align: top;">Hold Message &nbsp;
                                                   
                                                        <asp:TextBox ID="txtHoldMsg" CssClass="TextBoxStyle" TextMode="MultiLine" runat="server" Height="30px" Width="70%" Style="resize: none;"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="div1" runat="server">
                                            <asp:Button ID="btnHistory" runat="server" Style="width: 100px;" CssClass="button gray small" Text="Prev.Visit" OnClientClick="HistoryPopup();" />

                                            <asp:Button ID="btnBalance" runat="server" Style="width: 100px;" CssClass="button gray small" Text="Inv. Dtls" OnClientClick="InvoicePopup();" />
                                        </div>




                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="width: 100%; height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="width: 100%">
                                        <table style="width: 100%; background-color: #D64535; color: #fff;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label42" runat="server" Style="color: #fff;" CssClass="lblCaption1" Text="Latest Visit&nbsp;:"></asp:Label>
                                                </td>
                                                <td>

                                                    <asp:Label ID="lblLastVisit" runat="server" Style="color: #fff;" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:Label ID="Label40" runat="server" Style="color: #fff;" CssClass="lblCaption1" Text="Branch &nbsp;&nbsp; &nbsp;:"></asp:Label>
                                                </td>
                                                <td style="width: 200px;">
                                                    <asp:Label ID="lblVisitedBranch" runat="server" Style="color: #fff;" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td class="lblCaption1" style="width: 100px; color: #fff">Doctor:&nbsp;
                                                            <asp:Label ID="lblPrevDrId" runat="server" Style="color: #fff;" CssClass="lblCaption1" Width="100px" Font-Bold="true" Visible="false"></asp:Label>

                                                </td>
                                                <td colspan="2">
                                                    <asp:Label ID="lblPrevDrName" runat="server" Style="color: #fff;" CssClass="lblCaption1" Width="200px" Font-Bold="true"></asp:Label>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>


                                                <td style="width: 100px;">
                                                    <asp:Label ID="Label82" runat="server" Style="color: #fff;" CssClass="lblCaption1" Text="Created User:"></asp:Label>
                                                </td>
                                                <td style="width: 150px;">
                                                    <asp:Label ID="lblCreadedUser" runat="server" Style="color: #fff;" CssClass="lblCaption1" Width="150px" Font-Bold="true"></asp:Label>

                                                </td>
                                                <td style="width: 100px;">
                                                    <asp:Label ID="Label41" runat="server" Style="color: #fff;" CssClass="lblCaption1" Text="First Visit :"></asp:Label>
                                                </td>
                                                <td style="width: 100px;">

                                                    <asp:Label ID="lblFirstVisit" runat="server" Style="color: #fff;" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                </td>

                                                <td style="width: 100px;">
                                                    <asp:Label ID="Label38" runat="server" Style="color: #fff;" CssClass="lblCaption1" Text="Modified User:"></asp:Label>
                                                </td>
                                                <td style="width: 150px;">
                                                    <asp:Label ID="lblModifiedUser" runat="server" Style="color: #fff;" CssClass="lblCaption1" Width="150px" Font-Bold="true"></asp:Label>

                                                </td>

                                                <td style="width: 100px;">
                                                    <asp:Label ID="Label46" runat="server" Style="color: #fff;" CssClass="lblCaption1" Text="Previous Visit :"></asp:Label>
                                                </td>
                                                <td style="width: 100px;">

                                                    <asp:Label ID="lblPrevVisit" runat="server" Style="color: #fff;" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                </td>



                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            </td>


                                 </tr>
                          </table>

                            <asp:LinkButton ID="lnkRemMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkRemMessage"
                                PopupControlID="pnlRemMessage" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
                                PopupDragHandleControlID="pnlRemMessage">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlRemMessage" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                            <%=strReminderMessage %>
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>


                            <asp:LinkButton ID="lnkHoldMsg" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="lnkHoldMsg"
                                PopupControlID="pnlHoldMsg" BackgroundCssClass="modalBackground" CancelControlID="btnHoldClose"
                                PopupDragHandleControlID="pnlHoldMsg">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlHoldMsg" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnHoldClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                            <%=strHoldMessage %>
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>




                            <asp:LinkButton ID="lnkMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkMessage"
                                PopupControlID="pnlPrevPrice" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
                                PopupDragHandleControlID="pnlPrevPrice">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlPrevPrice" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                            <%=strMessage %>
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>





                            <asp:LinkButton ID="lnkHCBMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender7" runat="server" TargetControlID="lnkHCBMessage"
                                PopupControlID="pnlHCB" BackgroundCssClass="modalBackground" CancelControlID="btnHCBClose"
                                PopupDragHandleControlID="pnlHCB">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlHCB" runat="server" Height="330px" Width="680px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnHCBClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 230px;">


                                            <div style="padding-top: 0px; width: 650px; height: 275px; overflow: auto; border-color: #e3f7ef; border-style: groove;">

                                                <asp:GridView ID="gvHCB" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                                    EnableModelValidation="True" Width="99%" PageSize="50">
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <RowStyle CssClass="GridRow" />
                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                    <Columns>


                                                        <asp:TemplateField HeaderText="Category">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCBD_CATEGORY") %>'></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Category Type">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCB_CATEGORY_TYPEDesc") %>'></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Limit">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLimit" runat="server" Text='<%# Bind("HCBD_LIMIT") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInsType" runat="server" Text='<%# Bind("HCBD_CO_INS_TYPE") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Co-Ins.">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInsType" runat="server" Text='<%# Bind("HCBD_CO_INS_AMT") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ded Type">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lblDedType" runat="server" Text='<%# Bind("HCBD_DED_TYPE") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Deductible">
                                                            <ItemTemplate>


                                                                <asp:Label ID="lblDedAmt" runat="server" Text='<%# Bind("HCBD_DED_AMT") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>


                                            </div>


                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>







                        </ContentTemplate>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </asp:TabPanel>

            <asp:TabPanel runat="server" ID="TabPatientVisitDtls" HeaderText="Visit Information " Width="100%">
                <ContentTemplate>
                    <div class="lblCaption1" style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                        <table width="1000px" cellpadding="5" cellspacing="5">
                            <tr>
                                <td colspan="5" class="lblCaption1">
                                    <asp:UpdatePanel ID="UpdatePanel59" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label31" runat="server" CssClass="lblCaption1" Text="Visit Details" Font-Bold="True"></asp:Label>
                                            <asp:Label ID="lblStatusVisit" runat="server" ForeColor="red" Font-Bold="True" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td style="text-align: right;" colspan="5">
                                    <asp:UpdatePanel ID="UpPanelSaveVisit" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnSaveVisit" runat="server" CssClass="button red small" Width="100px" Text="Save Visit" OnClientClick="return  ValVisit();"
                                                OnClick="btnSaveVisit_Click" />

                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSaveVisit">
                                        <ProgressTemplate>
                                            <div id="overlay">
                                                <div id="modalprogress">
                                                    <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                                                </div>
                                            </div>

                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>

                            </tr>
                            <tr>
                                <td class="lblCaption1">Visit Type   <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpPTVisitType" runat="server" CssClass="TextBoxStyle" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpPTVisitType_SelectedIndexChanged">
                                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                <asp:ListItem Value="CU">Customer</asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>


                                <td class="lblCaption1" colspan="5">

                                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                        <ContentTemplate>

                                            <asp:CheckBox ID="chkDisplayInDrWaitingList" runat="server" CssClass="lblCaption1" Checked="true"
                                                Text="Display In Dr. Waiting List" />
                                            &nbsp; &nbsp;
                                                                     <asp:CheckBox ID="chkCompCashPT" runat="server" CssClass="lblCaption1"
                                                                         Text="Comp. Cash PT" Width="120px" OnCheckedChanged="CheckBox7_CheckedChanged" Checked="false" AutoPostBack="true" />


                                            <asp:Label ID="lblHAADRegisterCaption" runat="server" CssClass="lblCaption1" Visible="false" Text=" Patient Register at DOH :"></asp:Label>&nbsp;
                                                                    <asp:Label ID="lblHAADRegister" runat="server" CssClass="lblCaption1" Visible="false" Font-Bold="true" Text="No"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>


                                <td class="lblCaption1" colspan="2">

                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="chkRequiredTriage" runat="server" CssClass="lblCaption1" Checked="true" Visible="false"
                                                Text="Required Nursing Assessment" AutoPostBack="true" OnCheckedChanged="chkRequiredTriage_CheckedChanged" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <a id="lnkReferralList" runat="server" class="lblCaption1" onclick="ShowReferralList()" style="cursor: pointer; color: #005c7b; font-weight: bold;">Referral  List...</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" CssClass="lblCaption1" Text="Eligibility ID"></asp:Label>
                                    <asp:Label ID="lblEligibilityIDMand" runat="server" Style="color: red; font-size: 11px;" Text="*" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel58" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEligibilityID" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="50"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" CssClass="lblCaption1" Text="Package"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpPTCategory" runat="server" CssClass="TextBoxStyle" Width="250px">
                                    </asp:DropDownList>
                                </td>
                                <td class="lblCaption1" colspan="6" style="height: 5px;">

                                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                        <ContentTemplate>
                                            <div id="trTriageReason" runat="server" visible="false">
                                                Nursing Assessment Avoid Reason &nbsp;

                                                <asp:DropDownList ID="drpTriageReason" runat="server" CssClass="TextBoxStyle" Width="200px">
                                                </asp:DropDownList>

                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>

                            </tr>
                            <tr>
                                <td class="lblCaption1">Patient Class
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel48" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpPatientClass" runat="server" Style="width: 105px;" CssClass="TextBoxStyle">
                                                <asp:ListItem Text="Outpatient" Value="O"></asp:ListItem>
                                                <asp:ListItem Text="Urgent Care" Value="UC"></asp:ListItem>
                                                <asp:ListItem Text="Emergency" Value="E"></asp:ListItem>
                                                <%--   <asp:ListItem Text="Preadmit" Value="P"></asp:ListItem>
                                                 <asp:ListItem Text="Recurring patient" Value="R"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Coupon No
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel50" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtCouponNo" runat="server" CssClass="TextBoxStyle" Width="250px" MaxLength="50"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="lblCaption1">File No    
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitFileNo" CssClass="TextBoxStyle" runat="server" Font-Bold="true" Width="100px" MaxLength="50" BackColor="#e6e6e6" Enabled="false"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Name  
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitFullName" CssClass="TextBoxStyle" runat="server" Font-Bold="true" Width="250px" BackColor="#e6e6e6" Enabled="false"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Mobile  
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitMobile" CssClass="TextBoxStyle" runat="server" Width="100px" BackColor="#e6e6e6" Enabled="false"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Gender  
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitGender" CssClass="TextBoxStyle" runat="server" Width="100px" BackColor="#e6e6e6" Enabled="false"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td></td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel60" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblPTVisitType" runat="server" CssClass="lblCaption1" Text="New" Font-Bold="true" ForeColor="Maroon"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>


                        </table>
                    </div>
                    <div style="height: 5px;"></div>
                    <div class="lblCaption1" style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                        <table width="1000px" cellpadding="5" cellspacing="5">
                            <tr>
                                <td colspan="6">
                                    <asp:Label ID="Label19" runat="server" CssClass="lblCaption1"
                                        Text="Doctor  Information" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1">Doctor    <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td class="auto-style28">
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                            <div id="divDr" style="visibility: hidden;"></div>
                                            <asp:TextBox ID="txtDoctorID" runat="server" CssClass="TextBoxStyle" Width="70px" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()" ondblclick="return ShowStaffList();"></asp:TextBox>
                                            <asp:TextBox ID="txtDoctorName" runat="server" CssClass="TextBoxStyle" Width="270px" AutoPostBack="true" OnTextChanged="txtDoctorName_TextChanged" MaxLength="50" onblur="return DRNameSelected()" ondblclick="return ShowStaffList();"></asp:TextBox>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                                            </asp:AutoCompleteExtender>
                                            <img src='<%= ResolveUrl("../../Images/TopMenuIcons/Appointment.png")%>' onclick="AppointmentPopup()" alt="Check Appointment" title="Check Appointment" style="width: 20px; height: 20px; cursor: pointer; border: none;">
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>

                                <td class="lblCaption1">Department
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtDepName" CssClass="TextBoxStyle" runat="server" Width="150px" Enabled="false" MaxLength="50"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </td>
                                <div id="divToken" runat="server">
                                    <td>
                                        <asp:Label ID="Label21" runat="server" CssClass="lblCaption1" Text="Token No:"></asp:Label>

                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel57" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTokenNo" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="10" Enabled="false"></asp:TextBox>
                                                <asp:CheckBox ID="chkManualToken" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkManualToken_CheckedChanged"
                                                    Text="Manual" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>


                                </div>

                            </tr>
                            <tr>
                                <td class="lblCaption1">Ref. Doctor
                                </td>
                                <td class="lblCaption1">

                                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                        <ContentTemplate>
                                            <div id="divRefDr" style="visibility: hidden;"></div>

                                            <asp:TextBox ID="txtRefDoctorID" runat="server" CssClass="TextBoxStyle" Width="70px" onblur="return RefDRIdSelected()"></asp:TextBox>
                                            <asp:TextBox ID="txtRefDoctorName" runat="server" CssClass="TextBoxStyle" Width="270px" MaxLength="50" onblur="return RefDRNameSelected()"></asp:TextBox>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtRefDoctorName" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorName"
                                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divRefDr">
                                            </asp:AutoCompleteExtender>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Nurse
                                </td>
                                <td class="lblCaption1">
                                    <asp:DropDownList ID="drpNurse" runat="server" CssClass="TextBoxStyle" Width="150px">
                                    </asp:DropDownList>
                                </td>
                                <td class="lblCaption1">

                                    <asp:Label ID="lblSessionInvoiceNo" runat="server" CssClass="TextBoxStyle" Text="Session Inv.No."></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSessionInvoiceNo" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div style="height: 5px;"></div>
                    <div class="lblCaption1" style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                        <table width="1000px" cellpadding="5" cellspacing="5">
                            <tr>
                                <td colspan="6">
                                    <asp:Label ID="Label32" runat="server" CssClass="lblCaption1"
                                        Text="Insurance / Customer Details" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>

                                <td class="lblCaption1">TPA/Insurance   
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel47" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpPTVisitParentCompany" runat="server" CssClass="TextBoxStyle" Enabled="false" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="drpPTVisitParentCompany_SelectedIndexChanged"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>


                                <td class="lblCaption1">Company    <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel46" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpPTVisitCompany" CssClass="TextBoxStyle" runat="server" class="TextBoxStyle" Width="250px" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="drpPTVisitCompany_SelectedIndexChanged"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>



                                <td class="lblCaption1">Plan   <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel51" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpPTVisitCompanyPlan" CssClass="TextBoxStyle" runat="server" class="TextBoxStyle" Enabled="false" Width="250px"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>



                            </tr>
                            <tr>

                                <td class="lblCaption1">Policy No  
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel52" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitPolicyNo" CssClass="TextBoxStyle" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1">Card No  
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel53" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitCardNo" CssClass="TextBoxStyle" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1">Expiry Date   <span style="color: red; font-size: 11px;">*</span></td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel55" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPTVisitCardExpDate" runat="server" CssClass="TextBoxStyle" Enabled="false" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender8" runat="server"
                                                Enabled="True" TargetControlID="txtPTVisitCardExpDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>


                        </table>
                    </div>
                    <div style="height: 5px;"></div>

                    <table width="80%" cellpadding="5" cellspacing="5">
                        <tr>
                            <td colspan="4">
                                <asp:TextBox ID="txtLablePrintCount" CssClass="TextBoxStyle" runat="server" Width="20px" MaxLength="2" Text="3" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CheckBox ID="chkLabelPrint" runat="server" />

                                <asp:Button ID="btnLabelPrint" runat="server" Style="width: 100px;" CssClass="button gray small" Text="Label Print" OnClick="btnLabelPrint_Click" />


                                <asp:Button ID="btnMRRPrint" runat="server" Style="width: 100px;" CssClass="button gray small" Text="MRR Print" OnClick="btnMRRPrint_Click" />


                                <asp:Button ID="btnTokenPrint" runat="server" CssClass="button gray small" Width="100px" Text="Token Print" OnClick="btnTokenPrint_Click" />

                            </td>
                            <td style="text-align: right;">
                                <a id="lnkVisitHistory" runat="server" class="lblCaption1" onclick="ShowVisitHistory()" style="cursor: pointer; color: #005c7b; font-weight: bold;">Visit History...</a>
                            </td>
                        </tr>


                    </table>



                </ContentTemplate>
            </asp:TabPanel>

            <asp:TabPanel runat="server" ID="TabPanelCard" HeaderText=" Card Information ">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblStatusTab2" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <table cellpadding="2" cellspacing="2" style="width: 850px; height: 100px;" border="0">
                            <tr>
                                <td class="lblCaption1" style="height: 30px">Card Type   <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td class="lblCaption1">Card Name   <span style="color: red; font-size: 11px;">*</span>
                                </td>


                                <td class="lblCaption1">Company   <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblPlanType" runat="server" CssClass="lblCaption1" Text="Plan Type" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px">
                                    <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpCardType" CssClass="TextBoxStyle" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="drpCardType_SelectedIndexChanged">
                                                <asp:ListItem Text="Insurance" Value="Insurance"></asp:ListItem>
                                                <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <asp:DropDownList ID="drpCardName" CssClass="TextBoxStyle" BorderWidth="1px" runat="server" Width="100px"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>


                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtCompany" runat="server" Width="70px" MaxLength="10" CssClass="TextBoxStyle" onblur="return CompIdSelected()" onkeypress="return PatientPopup('CompanyId',this.value,event)" AutoPostBack="true" OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                                            <asp:TextBox ID="txtCompanyName" runat="server" Width="250px" MaxLength="50" CssClass="TextBoxStyle" onblur="return CompNameSelected()" onkeypress="return PatientPopup('CompanyName',this.value,event)" AutoPostBack="true" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>
                                            <div id="divComp" style="visibility: hidden;"></div>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany">
                                            </asp:AutoCompleteExtender>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName">
                                            </asp:AutoCompleteExtender>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td>

                                    <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPlanType" runat="server" CssClass="TextBoxStyle" Visible="false" Width="200px" MaxLength="50" ondblclick="return BenefitsExecPopup(this.value);"></asp:TextBox>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtPlanType" MinimumPrefixLength="1" ServiceMethod="GetPlanType">
                                            </asp:AutoCompleteExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>



                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px">
                                    <asp:Label ID="lblPolicyNo1" runat="server" CssClass="lblCaption1" Text="Policy No."></asp:Label>
                                    <span style="color: red; font-size: 11px;">*</span>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPolicyCardNo" CssClass="TextBoxStyle" runat="server" MaxLength="30" Width="100px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="chkIsDefault" runat="server" Text="Default Card" CssClass="lblCaption1" Checked="True" />
                                            &nbsp; 
                                            <asp:CheckBox ID="chkCardActive" runat="server" Text="Active" CssClass="lblCaption1" Checked="True" />


                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td colspan="2" class="lblCaption1"></td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px">Effective Date   <span style="color: red; font-size: 11px;">*</span></td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender6" runat="server"
                                                Enabled="True" TargetControlID="txtEffectiveDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>


                                <td class="lblCaption1" id="tdCardNo" runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblCardNo" runat="server" CssClass="lblCaption1" Text="ID No"></asp:Label>
                                            &nbsp;  &nbsp; &nbsp;<asp:TextBox ID="txtCardNo" CssClass="TextBoxStyle" runat="server" MaxLength="30" Width="100px"></asp:TextBox>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1">Expiry Date   <span style="color: red; font-size: 11px;">*</span></td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtCardExpDate" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                                Enabled="True" TargetControlID="txtCardExpDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>



                                <td></td>
                                <td colspan="2"></td>


                            </tr>


                        </table>

                        <table cellpadding="5" cellspacing="5" style="width: 850px;" border="0">
                            <tr>

                                <td>
                                    <asp:Label ID="Label70" runat="server" CssClass="lblCaption1"
                                        Text="Card Front Image"></asp:Label>

                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <asp:Label ID="Label49" runat="server" CssClass="lblCaption1"
                                        Text="Card Back Image"></asp:Label>
                                </td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>

                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgFront" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>

                                    </asp:UpdatePanel>

                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgBack" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td></td>
                                <td valign="top">
                                    <asp:Button ID="btnClaimPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Claim Form Print" OnClick="btnClaimPrint_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="btnWebRepot" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Claim Form Web" OnClick="btnWebRepot_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="btnCardPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Card Print" OnClick="btnCardPrint_Click" />
                                    <br />
                                    <asp:Button ID="btnDentalWebRepot" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Web Report" OnClick="btnDentalWebRepot_Click" Visible="false" />
                                    <br />
                                    <input type="checkbox" runat="server" id="chkCompletionRpt" name="chkCompletionRpt" />
                                    <label for="chkCompletionRpt" class="lblCaption1">Completion Report</label>


                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:AsyncFileUpload ID="AsyncFileUpload1" runat="server"
                                        OnUploadedComplete="AsyncFileUpload1_UploadedComplete"
                                        OnClientUploadComplete="AsyncFileUpload1_uploadComplete"
                                        Width="225px" FailedValidation="False" />
                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <asp:AsyncFileUpload ID="AsyncFileUpload2" runat="server"
                                        OnUploadedComplete="AsyncFileUpload2_UploadedComplete"
                                        OnClientUploadComplete="AsyncFileUpload2_uploadComplete"
                                        Width="225px" FailedValidation="False" />
                                </td>

                            </tr>

                        </table>
                        <table cellpadding="5" cellspacing="5" width="70%">
                            <tr>
                                <td>

                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnScanCardAdd" runat="server" Width="80px" CssClass="button gray small" OnClientClick="return ValScanCardAdd();"
                                                OnClick="btnScanCardAdd_Click" Text="Add" />
                                            <asp:Button ID="btnScanCardUpdate" runat="server" Width="80px" CssClass="button gray small" OnClientClick="return ValScanCardAdd();" Visible="false"
                                                OnClick="btnScanCardUpdate_Click" Text="Update" />
                                            <asp:Button ID="bntClear" runat="server" Width="80px" CssClass="button gray small" OnClick="bntClear_Click" Text="Clear" />

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnScanCardAdd" />
                                            <asp:PostBackTrigger ControlID="btnScanCardUpdate" />

                                        </Triggers>
                                    </asp:UpdatePanel>

                            </tr>
                        </table>
                    </div>
                    <br />
                    <div style="padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
                        <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvScanCard" runat="server" AllowSorting="True" CssClass="Grid" OnRowDataBound="gvScanCard_RowDataBound"
                                    AutoGenerateColumns="False" Width="100%" EnableModelValidation="True" GridLines="Both">
                                    <HeaderStyle CssClass="GridHeader" Font-Bold="True" />
                                    <RowStyle CssClass="GridRow" />


                                    <Columns>
                                        <asp:TemplateField HeaderText=" Card Name">
                                            <ItemTemplate>

                                                <asp:LinkButton ID="lnkCardName" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblIsNew" runat="server" Text='<%# Bind("HSC_ISNEW") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCardType" runat="server" Text='<%# Bind("HSC_CARD_TYPE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblHSC_PHOTO_ID" runat="server" Text='<%# Bind("HSC_PHOTO_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCardName" runat="server" Text='<%# Bind("HSC_CARD_NAME") %>'></asp:Label>
                                                </asp:LinkButton>


                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText=" Company Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCompId" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblCompId" runat="server" Text='<%# Bind("HSC_INS_COMP_ID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCompName" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblCompName" runat="server" Text='<%# Bind("HSC_INS_COMP_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Policy No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPolicyCardNo" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblPolicyCardNo" runat="server" Text='<%# Bind("HSC_POLICY_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Plan Type" Visible="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPlanType" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblPlanType" runat="server" Text='<%# Bind("HSC_PLAN_TYPE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ID No.">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCardNo" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblgvCardNo" runat="server" Text='<%# Bind("HSC_CARD_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Effective Date">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEffectiveDate" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblEffectiveDate" runat="server" Text='<%# Bind("HSC_POLICY_START") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Expiry Date">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCExpDate" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblExpDate" runat="server" Text='<%# Bind("HSC_EXPIRY_DATE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Default">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkIsDefault" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblIsDefault" runat="server" Text='<%# Bind("HSC_ISDEFAULT") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblIsDefaultDesc" runat="server" Text='<%# Bind("HSC_ISDEFAULTDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkStatus" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("HSC_STATUS") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="Label52" runat="server" Text='<%# Bind("HSC_STATUSDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Card Front Image">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFrontPath" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblFrontPath" runat="server" Text='<%# Bind("HSC_IMAGE_PATH_FRONT") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Card Back Image">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBackPath" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblBackPath" runat="server" Text='<%# Bind("HSC_IMAGE_PATH_BACK") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>

                                                <asp:ImageButton ID="DeleteScanCard" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                    OnClick="DeleteScanCard_Click" OnClientClick="return window.confirm('Do you want to Delete Card Dtls?')" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>



                    </div>



                </ContentTemplate>

            </asp:TabPanel>

            <asp:TabPanel runat="server" ID="TabPanelSignature" HeaderText=" Signature" Width="100%" Visible="false">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                        <ContentTemplate>
                            <div>
                                <table cellpadding="5" cellspacing="5" style="width: 850px;" border="0">
                                    <tr>
                                        <td class="lblCaption1" style="height: 30px;">Substitute Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubstitute" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="TextBoxStyle" Width="240px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Age
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsAge" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="TextBoxStyle" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Nationality
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpSubsNationality" BorderWidth="1" BorderColor="#cccccc" runat="server" CssClass="TextBoxStyle" Width="153px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 30px;">Policy No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsIdNo" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="TextBoxStyle" Width="240px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Type
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsType" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="TextBoxStyle" Width="150px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Capacity
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsCapacity" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="TextBoxStyle" Width="150px" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 30px;">Translator Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTranslator" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="TextBoxStyle" Width="240px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td colspan="3">
                                            <asp:Button ID="btnSaveConsentForm" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button orange small" Text=" Save " OnClick="btnSaveConsentForm_Click" />
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="5" cellspacing="5" style="width: 850px;" border="0">

                                    <tr>

                                        <td style="height: 5px;">
                                            <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label83" runat="server" CssClass="lblCaption1" Font-Bold="true"
                                                        Text="Patient"></asp:Label>
                                                    <asp:ImageButton ID="btnSignature1" runat="server" ImageUrl="~/HMS/Images/Sing1.jpg" ToolTip="Signature Scan" Height="20PX" Width="25pX" OnClick="btnSignature1_Click" />
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <asp:LinkButton ID="btnSigShow1" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="btnSigShow1_Click"></asp:LinkButton>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                                <ContentTemplate>

                                                    <asp:Label ID="Label84" runat="server" CssClass="lblCaption1" Font-Bold="true"
                                                        Text="Substitute "></asp:Label>
                                                    <asp:ImageButton ID="btnSignature2" runat="server" ImageUrl="~/HMS/Images/Sing1.jpg" ToolTip="Signature Scan" Height="20PX" Width="25pX" OnClick="btnSignature2_Click" />
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                             <asp:LinkButton ID="btnSigShow2" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="btnSigShow2_Click"></asp:LinkButton>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label85" runat="server" CssClass="lblCaption1" Font-Bold="true"
                                                        Text="Translator "></asp:Label>
                                                    <asp:ImageButton ID="btnSignature3" runat="server" ImageUrl="~/HMS/Images/Sing1.jpg" ToolTip="Signature Scan" Height="20PX" Width="25pX" OnClick="btnSignature3_Click" />
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                             <asp:LinkButton ID="btnSigShow3" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="btnSigShow3_Click"></asp:LinkButton>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>


                                    </tr>

                                    <tr>

                                        <td style="width: 30px;">

                                            <asp:Image ID="imgSig1" runat="server" Height="120px" BorderWidth="1" BorderColor="#cccccc" Width="285px" />



                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>

                                            <asp:Image ID="imgSig2" runat="server" Height="120px" BorderWidth="1" BorderColor="#cccccc" Width="285px" />

                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>

                                            <asp:Image ID="imgSig3" runat="server" Height="120px" BorderWidth="1" BorderColor="#cccccc" Width="285px" />

                                        </td>
                                    </tr>

                                </table>
                                <br />
                                <table cellpadding="5" cellspacing="5" style="width: 100%;" border="0">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvGCGridView" runat="server" AllowPaging="True" Width="80%"
                                                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGCGridView_Sorting" OnRowDataBound="gvGCGridView_RowDataBound"
                                                        EnableModelValidation="True" OnPageIndexChanging="gvGCGridView_PageIndexChanging" PageSize="200">
                                                        <HeaderStyle CssClass="GridHeader_Gray" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                                        OnClick="GCSelect_Click" />&nbsp;&nbsp;
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Sl.No" SortExpression="HPV_SEQNO" HeaderStyle-Width="50px">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblSerial" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("HGC_ID") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id" HeaderStyle-Width="70px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HGC_ID") %>'></asp:Label>

                                                                    <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_PT_ID") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name" SortExpression="HGC_SUBS_NAME">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblSubstituteName" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_SUBS_NAME") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Age" SortExpression="HGC_SUBS_AGE">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblSubstituteAge" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_SUBS_AGE") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Nationality" SortExpression="HGC_SUBS_NATIONALITY">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblSubstituteNational" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_SUBS_NATIONALITY") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Policy No" SortExpression="HGC_SUBS_IDNO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubstitutePolicy" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_SUBS_IDNO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" SortExpression="HGC_SUBS_TYPE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubstituteType" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_SUBS_TYPE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Capacity" SortExpression="HGC_SUBS_CAPACITY">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSubstituteCapacity" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_SUBS_CAPACITY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Transalator" SortExpression="HGC_TRAN_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTransalator" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_TRAN_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Date" SortExpression="HGC_CREATED_DATEDesc">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGCDate" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_CREATED_DATEDesc") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkReport" runat="server" OnClick="GCReport_Click">
                                                                        <asp:Label ID="lblHistory" CssClass="GridRow" Style="text-align: center;" runat="server" Text='REPORT'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteGC" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                                        OnClick="DeleteGC_Click" OnClientClick="return window.confirm('Do you want to Delete  Dtls?')" />&nbsp;&nbsp;
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <br />
                            </div>

                        </ContentTemplate>

                    </asp:UpdatePanel>
                </ContentTemplate>
            </asp:TabPanel>
        </asp:TabContainer>



    </div>


    <div id="divSignaturePopup" style="display: none; overflow: hidden; border: groove; height: 200px; width: 350px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 520px; top: 170px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top;"></td>
                <td align="right" style="vertical-align: top;">

                    <input type="button" id="Button4" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideSignaturePopup()" />
                </td>
            </tr>
        </table>

        <table width="100%">

            <tr>
                <td style="width: 50%">
                    <fieldset>
                        <legend class="lblCaption1">Signature </legend>
                        <table width="100%">
                            <tr>
                                <td style="width: 30px;">
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgPatientSig1" runat="server" Height="120px" Width="285px" />
                                        </ContentTemplate>

                                    </asp:UpdatePanel>


                                </td>
                            </tr>

                        </table>


                    </fieldset>
                </td>
                <td style="width: 50%"></td>
            </tr>
        </table>

    </div>

    <div style="padding-left: 60%; width: 100%;">
        <div id="divConfirmMessage" runat="server" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 200px;">
            <span style="float: right;">

                <input type="button" id="Button7" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideConfirmMessage()" />

            </span>
            <table cellpadding="0" cellspacing="0" width="100%">

                <tr>
                    <td>
                        <asp:Label ID="lblConfirmMessage" runat="server" CssClass="label" Style="font-weight: bold; color: red;"></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td align="center" valign="bottom" style="height: 50px;">
                        <asp:Button ID="btnConfirmYes" runat="server" Text=" Yes " CssClass="button blue small" Style="width: 70px;" OnClick="btnConfirmYes_Click" />
                        <asp:Button ID="btnConfirmNo" runat="server" Text=" No " CssClass="button blue small" Style="width: 70px;" OnClick="btnConfirmNo_Click" />

                    </td>
                </tr>
            </table>


            <br />

        </div>
    </div>

    <div id="divPlanPopup" style="display: none; overflow: hidden; border: groove; height: 400px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 700px; top: 300px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HidePlanPopup()" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="lblCaption1">Plan Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcPlanName" runat="server" Width="200px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtSrcPlanName_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="2">

                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                        <ContentTemplate>
                            <div style="padding-top: 0px; width: 480px; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                                <asp:GridView ID="gvPlanPopup" runat="server" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%">
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgPlanSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                    OnClick="imgPlanSelect_Click" />&nbsp;&nbsp;
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Company">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompID" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_COMP_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblCompName" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_NAME") %>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Plan Type">
                                            <ItemTemplate>

                                                <asp:Label ID="lblCatType" CssClass="GridRow" runat="server" Text='<%# Bind("HCB_CAT_TYPE") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>


                                    </Columns>

                                </asp:GridView>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>


        </table>
    </div>


    <div id="divEnquiryMaster" style="display: none; overflow: hidden; border: groove; height: 600px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 300px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button2" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideEnquiryMaster()" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="lblCaption1">Search
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEnquirSearch" runat="server" Width="90%" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Date  
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEnqFromDate" runat="server" CssClass="TextBoxStyle" Width="70px" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtEnqFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>

                            <asp:TextBox ID="txtEnqToDate" runat="server" CssClass="TextBoxStyle" Width="70px" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                Enabled="True" TargetControlID="txtEnqToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                        <ContentTemplate>
                            <asp:ImageButton ID="imgEnqRefresh" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Refresh" OnClick="imgEnqRefresh_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6">

                    <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                        <ContentTemplate>
                            <div style="padding-top: 0px; width: 780px; overflow: auto; height: 500px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                                <asp:GridView ID="gvEnquiryMaster" runat="server" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%" PageSize="200">
                                    <HeaderStyle CssClass="GridHeader_Gray" />

                                    <RowStyle CssClass="GridRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="Enquiry ID">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEnquiryID" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblEnquiryID" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_ID") %>'></asp:Label>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEnquiryDate" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblEnquiryDate" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_DATEDesc") %>'></asp:Label>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkName" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblName" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_FNAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPhone" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblPhone" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_PHONE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMobile" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblgvEnqMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_MOBILE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEMail" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblEMail" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_EMAIL") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Select_Click">
                                                    <asp:Label ID="lblStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HCEM_STATUS") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                                </asp:GridView>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>


        </table>
    </div>


    <div id="divReferralList" style="display: none; overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 250px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;">
                    <span class="lblCaption1" style="font-weight: bold;">Referral List </span>

                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button6" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideReferralList()" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">

            <tr>
                <td colspan="6">

                    <asp:UpdatePanel ID="UpdatePanel49" runat="server">
                        <ContentTemplate>
                            <div style="padding-top: 0px; width: 780px; overflow: auto; height: 370px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                                <asp:GridView ID="gvReferral" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%">
                                    <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                                    <RowStyle CssClass="GridRow" Height="20px" />

                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" HeaderStyle-Width="110px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton35" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="lblgvReferralEMR_ID" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvReferralEMR_DR_ID" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DR_CODE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvReferralEMR_DR_Name" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DR_NAME") %>' Visible="false"></asp:Label>

                                                    <asp:Label ID="lblgvReferralDate" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_CREATED_DATEDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="In Dr.">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton36" CssClass="lblCaption1" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="lblgvReferralDR_ID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPR_DR_CODE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="In Dr. Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton37" CssClass="lblCaption1" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="lblgvReferralDR_Name" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPR_DR_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Out Dr.">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton38" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="Label60" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_OUT_DRNAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Hospital Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton39" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="Label61" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_OUT_HOSPITAL") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reason for Referral">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="nklgvReferralRemarks" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="lblgvReferralRemarks" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_REMARKS") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnklgvReferralStatus" runat="server" OnClick="ReferralSelect_Click">
                                                    <asp:Label ID="lblgvReferralStatus" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_STATUS") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                    </Columns>

                                </asp:GridView>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>


        </table>
    </div>

    <div id="divVisitHistory" style="display: none; overflow: hidden; border: groove; height: 400px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 220px; top: 350px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="lblCaption1" style="vertical-align: top; width: 50%;">
                    <asp:UpdatePanel ID="UpdatePanel56" runat="server">
                        <ContentTemplate>
                            <span class="lblCaption1" style="font-weight: bold;">Visit History </span>&nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:CheckBox ID="chkClaimFormPrint" runat="server" CssClass="lblCaption1" Text=" Print Claim Form" />
                            &nbsp; &nbsp; &nbsp;
                            Consent Form :
                         <asp:DropDownList ID="drpVisitConsentForm" BorderColor="#e3f7ef" BorderStyle="Groove" CssClass="label" runat="server" Width="100px" Visible="true">
                         </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button8" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideVisitHistory()" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">

            <tr>
                <td colspan="6">

                    <asp:UpdatePanel ID="UpdatePanel54" runat="server">
                        <ContentTemplate>
                            <div style="padding-top: 0px; width: 980px; overflow: auto; height: 370px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                                <asp:GridView ID="gvVisitHistory" runat="server" AllowPaging="false"
                                    AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%" GridLines="Horizontal">
                                    <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                                    <RowStyle CssClass="GridRow" Height="20px" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkReport" runat="server" OnClick="SelectPrint_Click">
                                                    <asp:Label ID="lblHistory" CssClass="GridRow" Style="text-align: center; padding-right: 2px;" runat="server" Text="Report"></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date & Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_SEQNO") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDrId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblEMRId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_Date") %>' Visible="false"></asp:Label>

                                                <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_Id") %>' Visible="false"></asp:Label>

                                                <asp:Label ID="lblDateDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateDesc") %>'></asp:Label>
                                                <asp:Label ID="lblTime" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Dr.Name" SortExpression="HPV_DR_NAME">
                                            <ItemTemplate>

                                                <asp:Label ID="lblDRName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dept." SortExpression="HPV_DR_NAME">
                                            <ItemTemplate>

                                                <asp:Label ID="lblDepName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Token" SortExpression="HPV_DR_TOKEN">
                                            <ItemTemplate>

                                                <asp:Label ID="lblDRToken" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PT Type" SortExpression="HPV_PT_TYPEDesc">
                                            <ItemTemplate>

                                                <asp:Label ID="lblPTType" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCompId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblCompName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>


        </table>
    </div>

    <div id="divStaffList" style="display: none; overflow: hidden; border: groove; height: 400px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 220px; top: 350px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="lblCaption1" style="vertical-align: top; width: 50%; font-weight: bold;">Doctor Information
                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button9" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideStaffList()" />
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%">

            <tr>
                <td colspan="6">

                    <asp:UpdatePanel ID="UpdatePanel62" runat="server">
                        <ContentTemplate>
                            <div style="padding-top: 0px; width: 780px; overflow: auto; height: 370px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                                <asp:GridView ID="gvStaffList" runat="server" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%" PageSize="200">
                                    <HeaderStyle CssClass="GridHeader_Gray" />

                                    <RowStyle CssClass="GridRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkStaffID" runat="server" OnClick="SelectStaff_Click">
                                                    <asp:Label ID="lblStaffID" CssClass="GridRow" runat="server" Text='<%# Bind("HSFM_STAFF_ID") %>'></asp:Label>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkStaffName" runat="server" OnClick="SelectStaff_Click">
                                                    <asp:Label ID="lblStaffName" CssClass="GridRow" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>

                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Department">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDepartment" runat="server" OnClick="SelectStaff_Click">
                                                    <asp:Label ID="lblDepartment" CssClass="GridRow" runat="server" Text='<%# Bind("HSFM_DEPT_ID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TokenNo">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkTokenNo" runat="server" OnClick="SelectStaff_Click">
                                                    <asp:Label ID="lblTokenNo" CssClass="GridRow" runat="server" Text='<%# Bind("TokenNo") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>


                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>


        </table>
    </div>

</asp:Content>




