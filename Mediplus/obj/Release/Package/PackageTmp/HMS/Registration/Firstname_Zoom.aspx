﻿<%@ page language="C#" autoeventwireup="true" codebehind="Firstname_Zoom.aspx.cs" inherits="Mediplus.HMS.Registration.Firstname_Zoom" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>


    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title></title>
   

    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('hidLocalDate').value = today;
            // alert(today);
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
            </asp:toolkitscriptmanager>

            <div style="padding-top: 5px;">
                <asp:hiddenfield id="hidLocalDate" runat="server" />
                <asp:hiddenfield id="hidPageName" runat="server" />
                <asp:hiddenfield id="hidMenuName" runat="server" />

                <table width="100%">
                    <tr>
                        <td class="style2">
                            <asp:label id="Label18" runat="server" cssclass="lblCaption1"
                                text="Name"></asp:label>
                        </td>
                        <td>
                            <asp:textbox id="txtSrcName" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px"></asp:textbox>
                        </td>
                        <td Class="lblCaption1">
                          Last Name

                        </td>
                        <td>
                            <asp:textbox id="txtSrcLName" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px"></asp:textbox>
                        </td>
                        <td>
                          
                            <asp:label id="Label31" runat="server" cssclass="lblCaption1"  text="File No"></asp:label>
                        </td>
                        <td>
                             <asp:textbox id="txtSrcFileNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px" maxlength="10"></asp:textbox>
                        </td>
                        <td>
                            <asp:label id="Label39" runat="server" cssclass="lblCaption1"
                                text="Mobile No1"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtSrcMobile1" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="250px"></asp:textbox>

                        </td>

                        <td align="right" style="padding-right: 20px;">
                            <asp:updatepanel id="UpdatePanel10" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:Button ID="btnFind" runat="server" CssClass="button orange small"
                                                                        OnClick="btnFind_Click" Text=" Find " />
                                                                </ContentTemplate>
                                                            </asp:updatepanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:label id="Label17" runat="server" cssclass="lblCaption1"
                                text="Mobile No2"></asp:label>
                        </td>
                        <td>
                            <asp:textbox id="txtSrcMobile2" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px"></asp:textbox>
                        </td>
                        <td>
                            <asp:label id="Label101" runat="server" cssclass="lblCaption1"
                                text="Home PH.No"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtSrcPhone1" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px" maxlength="10"></asp:textbox>
                        </td>
                        <td>
                            <asp:label id="Label102" runat="server" cssclass="lblCaption1"
                                text="Office PH.No"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtSrcPhone3" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="153px"></asp:textbox>
                        </td>
                        <td>
                            <asp:label id="Label103" runat="server" cssclass="lblCaption1"
                                text="Company"></asp:label>

                        </td>
                        <td>
                            <asp:dropdownlist id="drpSrcCompany"  BorderWidth="1px" BorderColor="#cccccc" cssclass="label" runat="server" width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpSrcCompany_SelectedIndexChanged">
                                                            </asp:dropdownlist>

                        </td>



                    </tr>
                    <tr>
                         <td class="style2">
                            <asp:label id="Label1" runat="server" cssclass="lblCaption1"
                                text="Network"></asp:label>
                        </td>
                        <td colspan="3">
                             <asp:DropDownList ID="drpNetworkName" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="drpNetworkName_SelectedIndexChanged">
                                        </asp:DropDownList>
                        </td>
                        <td>
                            <asp:label id="Label2" runat="server" cssclass="lblCaption1"
                                text="Plan Type"></asp:label>

                        </td>
                        <td >
                              <asp:DropDownList ID="drpPlanType" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="155px">
                                        </asp:DropDownList>
                        </td>
                         <td>
                            <asp:label id="Label3" runat="server" cssclass="lblCaption1"
                                text="Patient Type"></asp:label>

                        </td>
                        <td>
                             <asp:DropDownList ID="drpPatientType" runat="server" CssClass="label" Width="250px" BorderWidth="1px" BorderColor="#CCCCCC"  >
                                 <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                             </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <asp:label id="Label50" runat="server" cssclass="lblCaption1"
                                text="PO Box"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtSrcPOBox" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px"></asp:textbox>
                        </td>
                        <td>
                            <asp:label id="lblPolicyNo" runat="server" cssclass="lblCaption1"
                                text="Policy No"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtSrcPolicyNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="100px"></asp:textbox>
                        </td>
                        <td>
                            <asp:label id="Label53" runat="server" cssclass="lblCaption1"
                                text="Nationality"></asp:label>

                        </td>
                        <td>
                            <asp:dropdownlist id="drpSrcNationality" BorderWidth="1px" BorderColor="#cccccc" cssclass="label" runat="server" width="155px">
                                                            </asp:dropdownlist>
                        </td>
                        <td>
                            <asp:label id="Label98" runat="server" cssclass="lblCaption1"
                                text="Doctor"></asp:label>

                        </td>
                        <td colspan="2">
                            <asp:dropdownlist id="drpSrcDoctor" cssclass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" width="250px">
                                                            </asp:dropdownlist>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px;">
                            <asp:label id="Label90" runat="server" cssclass="lblCaption1"
                                text="From Date"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtFromDate" runat="server" width="63px" maxlength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                            <asp:calendarextender id="CalendarExtender4" runat="server"
                                enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                                                                </asp:calendarextender>
                            <asp:maskededitextender id="MaskedEditExtender2" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                            <asp:textbox id="txtFromTime" runat="server" width="30px" BorderWidth="1px" BorderColor="#cccccc" maxlength="5" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                            <asp:maskededitextender id="MaskedEditExtender4" runat="server" enabled="true" targetcontrolid="txtFromTime" mask="99:99" masktype="Time"></asp:maskededitextender>
                            &nbsp;
                        </td>
                        <td>
                            <asp:label id="Label97" runat="server" cssclass="lblCaption1"
                                text="To Date"></asp:label>

                        </td>
                        <td>
                            <asp:textbox id="txtToDate" runat="server" width="63px" maxlength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                            <asp:calendarextender id="CalendarExtender5" runat="server"
                                enabled="True" targetcontrolid="txtToDate" format="dd/MM/yyyy">
                                                                </asp:calendarextender>
                            <asp:maskededitextender id="MaskedEditExtender5" runat="server" enabled="true" targetcontrolid="txtToDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                            <asp:textbox id="txtToTime" runat="server" width="30px" maxlength="5" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                            <asp:maskededitextender id="MaskedEditExtender6" runat="server" enabled="true" targetcontrolid="txtToTime" mask="99:99" masktype="Time"></asp:maskededitextender>

                        </td>
                        <td>
                            <asp:label id="Label99" runat="server" cssclass="lblCaption1"
                                text="Invoice ID"></asp:label>
                        </td>
                        <td>
                            <asp:textbox id="txtSrcInvId" runat="server" width="153px" maxlength="20" BorderWidth="1px" BorderColor="#cccccc"></asp:textbox>

                        </td>
                        <td>
                              <asp:label id="Label37" runat="server" cssclass="lblCaption1"
                                text="Emirates ID"></asp:label>
                        </td>
                        <td>
                             <asp:textbox id="txtSrcEmiratesId" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="153px"></asp:textbox>
                        </td>
                    </tr>
                </table>
            </div>


            <div style="padding-top: 0px; width: 1070px; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:updatepanel id="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False"  OnSorting="gvGridView_Sorting"
                            EnableModelValidation="True" Width="1050px" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200"  gridlines="None">
                             <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="File No" SortExpression="HPM_PT_ID">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEmailId" CssClass="label" runat="server" Text='<%# Bind("HPM_EMAIL") %>'  visible="false"></asp:Label>
                                          <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click"  >
                                                <asp:Label ID="lblPatientId" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>
                                          </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HPM_PT_FNAME">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFullName" runat="server" OnClick="Select_Click">
                                              <asp:Label ID="lblPatientName" CssClass="label" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB" SortExpression="HPM_DOBDesc">
                                    <ItemTemplate>
                                       <asp:LinkButton ID="lnkDOB" runat="server" OnClick="Select_Click" >
                                            <asp:Label ID="lbldbo" CssClass="label" runat="server" Text='<%# Bind("HPM_DOBDesc") %>'></asp:Label>
                                       </asp:LinkButton>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone No" SortExpression="HPM_PHONE1">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkPhone" runat="server" OnClick="Select_Click"   >
                                              <asp:Label ID="lblPhone" CssClass="label" runat="server" Text='<%# Bind("HPM_PHONE1") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile No" SortExpression="HPM_MOBILE">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkMobile" runat="server" OnClick="Select_Click"  >
                                              <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderText="PT Type" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="inlPTType" runat="server" OnClick="Select_Click"  >
                                                <asp:Label ID="lblPTType" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkCompany" runat="server" OnClick="Select_Click" >
                                              <asp:Label ID="lblCompName" CssClass="label" runat="server" Text='<%# Bind("HPM_INS_COMP_NAME") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Policy No." SortExpression="HPM_POLICY_NO">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkgvPolicyNo" runat="server" OnClick="Select_Click"  >
                                              <asp:Label ID="lblgvPolicyNo" CssClass="label" runat="server" Text='<%# Bind("HPM_POLICY_NO") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Created User" SortExpression="HPM_CREATED_USER">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click"  >
                                              <asp:Label ID="lblCrUser" CssClass="label" runat="server" Text='<%# Bind("HPM_CREATED_USER") %>'></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:updatepanel>

            </div>

            <div>
                <asp:updatepanel id="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="70%">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                                        Text="Selected Records :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                                <td class="style2">
                                    
                                </td>

                                <td class="style2">
                                    
                                </td>

                                <td class="style2">
                                    
                                </td>

                            </tr>

                        </table>

                    </ContentTemplate>
                </asp:updatepanel>

            </div>
        </div>
    </form>
</body>
     <script language="javascript" type="text/javascript">
         // <![CDATA[

         function passvalue(HPM_PT_ID, PatientName, Mobile, Email) {

             var PageName = document.getElementById("<%=hidMenuName.ClientID%>").value;


            if (document.getElementById('hidPageName').value == 'HomeCareReg') {
                opener.location.href = "../HomeCare/HomeCareRegistration.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }
            else if (document.getElementById('hidPageName').value == 'PhysicalTherapy') {
                opener.location.href = "../HomeCare/PhysicalTherapy.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }
            else if (document.getElementById('hidPageName').value == 'HCStaffAssign') {
                opener.location.href = "../HomeCare/CoordinatorStaffAssign.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }
            else if (document.getElementById('hidPageName').value == 'CoderVerification') {
                opener.location.href = "../Billing/MRDAudit.aspx?MenuName=Insurance&PatientId=" + HPM_PT_ID; //opener.location.href;
            }
            else if (document.getElementById('hidPageName').value == 'Invoice') {
                opener.location.href = "../Billing/Invoice.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }
            else if (document.getElementById('hidPageName').value == 'ReportLoader') {
                opener.location.href = "../Billing/ReportLoader.aspx?PageName=" + PageName + "&MenuName=HMSReports&PatientId=" + HPM_PT_ID; //opener.location.href;
            }


            if (document.getElementById('hidPageName').value == 'PatientReg') {
                opener.location.href = "PTRegistration.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }
            if (document.getElementById('hidPageName').value == 'CashpatientReg') {
                opener.location.href = "CashPatientRegistration.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }

            if (document.getElementById('hidPageName').value == 'PTDocScan') {
                opener.location.href = "PatientDocScans.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }



            if (document.getElementById('hidPageName').value == 'Appointment') {
                window.opener.BindPTDetails(HPM_PT_ID, PatientName, Mobile, Email);

            }

            if (document.getElementById('hidPageName').value == 'PatientVisitReg') {
                opener.location.href = "PTAndVisitRegistration.aspx?PatientId=" + HPM_PT_ID; //opener.location.href;
            }

            window.parent.close();
            window.parent.parent.close();
            window.close();


        }

        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }


    </script>

</html>
