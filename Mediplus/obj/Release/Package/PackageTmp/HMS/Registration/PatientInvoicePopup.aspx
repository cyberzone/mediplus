﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientInvoicePopup.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientInvoicePopup" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Patient Invoice</title>
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowCreditReport(DBString) {
            var InvId = document.getElementById('<%=hidInvoiceId.ClientID%>').value
            var Report = "HmsInvoiceCr.rpt";
            // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvId + '\'  $SCREEN$$$1');

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvId + '\'';

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();


        }


        function ShowCashReport(DBString) {
            var InvId = document.getElementById('<%=hidInvoiceId.ClientID%>').value
            var Report = "HmsInvoiceCa.rpt";
            // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvId + '\'  $SCREEN$$$1');

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvId + '\'';

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
            <contenttemplate>
                            <asp:HiddenField ID="hidInvoiceId" runat="server" />
            </contenttemplate>
        </asp:UpdatePanel>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </td>
            </tr>

        </table>
        <table align="center">
            <tr>
                <td class="PageHeader" style="text-align: center;">INVOICE DETAILS 
               
                </td>
            </tr>
        </table>

       <div style="padding-top: 0px; width: 95%; height: 250px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
            <asp:updatepanel id="UpdatePanel1" runat="server">
                <contenttemplate>
                <asp:GridView ID="gvInvoice" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%"   PageSize="200">
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>
                       
                     
                          <asp:TemplateField HeaderText="Inv Code">
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>' Width="70px" visible="false" ></asp:Label>
                                <asp:Label ID="lblInvType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_TYPE") %>' Width="70px" visible="false" ></asp:Label>

                                <asp:LinkButton ID="lnkInvoiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_INVOICE_ID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_DATEDESC") %>' > </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Net Amt">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_NET_AMOUNT") %>' ></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Paid Amt.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPaid" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_CLAIM_AMOUNT_PAID") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_AMOUNT") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Patient Paid">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTPaid" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PAID_AMOUNT") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Patient Credit">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTCredit" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_CREDIT") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reject">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_CLAIM_REJECTED") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_ID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_NAME") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dr Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_DR_NAME") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                      
                        <asp:TemplateField HeaderText="Policy No">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_POLICY_NO") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Claim No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCoucherNo" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_VOUCHER_NO") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_INVOICE_TYPE") %>' > </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                     

                    </Columns>

                </asp:GridView>

            </contenttemplate>
            </asp:updatepanel>

        </div>
        <table width="1000px;">
            <tr>
                <td class="style2">
                    <asp:Label ID="Label6" runat="server" CssClass="label" Font-Bold="true"
                        Text="Selected Records :"></asp:Label>
                    &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                </td>
                <td  style="text-align:right;">
                     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                         <contenttemplate>
                     <asp:Button ID="btnCInvoicePrint" runat="server" CssClass="button orange small" Width="120px" Text="Print"  OnClick="btnCInvoicePrint_Click"/>
                         </contenttemplate>
                     </asp:UpdatePanel>
                </td>

            </tr>

        </table>

        <div style="padding-top: 0px; width: 95%; height: 250px; overflow: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
            <asp:updatepanel id="UpdatePanel5" runat="server">
                <contenttemplate>
                <asp:GridView ID="gvInvTran" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                    EnableModelValidation="True" Width="100%"   PageSize="200" >
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                   
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>
                       
                          <asp:TemplateField HeaderText="Inv Code"  >
                            <ItemTemplate>
                                    <asp:Label ID="Label12" CssClass="GridRow"    runat="server" Text='<%# Bind("HIT_INVOICE_ID") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Service Code" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                        <asp:Label ID="txtServCode" runat="server" CssClass="label"  style="border:none;" HeaderStyle-Width="70px"    BorderWidth="1px"  Text='<%# Bind("HIT_SERV_CODE") %>'   Width="100px" MaxLength="10"    ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Service Name" HeaderStyle-Width="300px">
                            <ItemTemplate>
                                 <asp:Label ID="txtServeDesc" runat="server" style="border:none;" Width="300px"     Text='<%# Bind("HIT_DESCRIPTION") %>'  ></asp:Label>
                                   
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtFee" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"   Text='<%# Bind("HIT_FEE") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblDiscType" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="D.Amt" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtDiscAmt" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"   Text='<%# Bind("HIT_DISC_AMT") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="QTY" HeaderStyle-Width="30px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtQTY" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="30px"   Text='<%# Bind("HIT_QTY") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtHitAmount" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Text='<%# Bind("HIT_AMOUNT") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ded." HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtDeduct" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"    Text='<%# Bind("HIT_DEDUCT") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Co-%" HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:Label ID="lblCoInsType" CssClass="GridRow"  Width="70px"   runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>'></asp:Label>
                                   </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtCoInsAmt" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"     Text='<%# Bind("HIT_CO_INS_AMT") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Paid Amt." ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                
                                    <asp:Label ID="Label13" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("HIT_CLAIM_AMOUNT_PAID") %>' Width="70px"></asp:Label>
                               
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtPTAmount" runat="server" style="text-align:right;padding-right:5px;border:none;"  Width="70px"    Text='<%# Bind("HIT_PT_AMOUNT") %>' ></asp:Label>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                
                                <asp:Label ID="Label21" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("HIT_CLAIM_REJECTED") %>' Width="70px"></asp:Label>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-Width="70px" >
                            <ItemTemplate>
                                 <asp:Label ID="txtHaadCode" runat="server" style=";border:none;"  Width="70px"    Text='<%# Bind("HIT_HAAD_CODE") %>' ></asp:Label>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Observ. Type" ItemStyle-VerticalAlign="Top" >
                            <ItemTemplate>
                                
                                <asp:Label ID="lblHIOType" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("HIO_TYPE") %>' Width="70px"></asp:Label>
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Code" HeaderStyle-Width="100px"  >
                            <ItemTemplate>
                                      <asp:Label ID="txtHIOCode" runat="server" style="border:none;"   Text='<%# Bind("HIO_CODE") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Desc."  HeaderStyle-Width="150px" >
                            <ItemTemplate>
                                      <asp:Label ID="txtHIODesc" runat="server"    style="border:none;"     Text='<%# Bind("HIO_DESCRIPTION") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Value" HeaderStyle-Width="100px" >
                            <ItemTemplate>
                                      <asp:Label ID="txtHIOValue" runat="server"   style="border:none;"   Text='<%# Bind("HIO_VALUE") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Observ. ValueType"  HeaderStyle-Width="150px">
                            <ItemTemplate>
                                      <asp:Label ID="txtHIOValueType" runat="server"   style="border:none;"   Text='<%# Bind("HIO_VALUETYPE") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                 

                    </Columns>

                </asp:GridView>

            </contenttemplate>
            </asp:updatepanel>

        </div>

    </form>
</body>
</html>
