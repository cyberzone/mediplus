﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Home1.aspx.cs" Inherits="Mediplus.Home1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowErrorMessage(vMessage1, vMessage2) {

            document.getElementById("divMessage").style.display = 'block';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
        document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
    }

    function HideErrorMessage() {

        document.getElementById("divMessage").style.display = 'none';

        document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
        document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
    }


    function load() {
        document.getElementById("divVisitDtls").style.display = 'none';
        var UserCat = "<%= Convert.ToString(Session["User_Category"]).ToUpper() %>"
        if (UserCat == 'ADMIN STAFF') {
            document.getElementById("divVisitDtls").style.display = 'block';
        }

    }


        function ShowPrintInvoice(BranchID, InvoiceNo, ReportName) {
            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';


            if (ReportName == "HmsCreditBill1.rpt") {
                Criteria += ' AND {HMS_INVOICE_TRANSACTION.HIT_SERV_CODE} <> \'1111\' AND  {HMS_INVOICE_TRANSACTION.HIT_SERV_CODE} <> \'others\'';
            }

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&Type=pdf&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    //  window.onload = load();

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="padding: 5px; width: 85%;">
        <div style="padding-left: 60%; width: 100%;">


            <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 300px; top: 300px;">

                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                    <ContentTemplate>
                        <span style="float: right;">

                            <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                        </span>
                        <br />
                        <div style="width: 100%; height: 20px; background: #E3E3E3;">
                            &nbsp;
                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                        </div>
                        &nbsp;
                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <br />

            </div>
        </div>
        <table>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" Font-Bold="true" Style="letter-spacing: 5px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Date
                </td>
                <td>
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                </td>
                <td class="lblCaption1">To Date

                </td>
                <td>
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                </td>
                <td class="lblCaption1">Doctor &nbsp;
                                       <asp:DropDownList ID="drpDoctor" CssClass="TextBoxStyle" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="202px">
                                       </asp:DropDownList>

                </td>
                <td class="lblCaption1">Nurse &nbsp;
                                       <asp:DropDownList ID="drpNurse" CssClass="TextBoxStyle" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="202px">
                                       </asp:DropDownList>

                </td>

                <td>
                    <asp:Button ID="btnRefresh" runat="server" CssClass="button red small" Width="80px"
                        Text="Refresh" OnClick="btnRefresh_Click" OnClientClick="load();" />
                    <asp:Button ID="btnTodayDate" runat="server" CssClass="button gray small" Width="80px"
                        Text="Today" OnClick="btnTodayDate_Click" />
                    <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="80px"
                        Text="Clear" OnClick="btnClear_Click" />
                </td>
            </tr>

        </table>

        <div id="divVisitDtls" runat="server">
            <table width="99%">
                <tr>
                    <td style="width: 210px;">
                        <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Visit Dtls." disabled="disabled" />
                    </td>
                    <td class="lblCaption1">Max. Row Display
                             <asp:TextBox ID="txtVisitMaxRow" runat="server" CssClass="TextBoxStyle" Width="50px" Text="100" style="text-align:center;"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="Label9" runat="server" CssClass="label"
                                    Text="Total Visits :"></asp:Label>
                                &nbsp;
                                            <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label7" runat="server" CssClass="label"
                                        Text="New :"></asp:Label>
                                &nbsp;
                                            <asp:Label ID="lblNew" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label8" runat="server" CssClass="label"
                                        Text="Followup(Revisit) :"></asp:Label>
                                &nbsp;
                                            <asp:Label ID="lblRevisit" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label10" runat="server" CssClass="label"
                                        Text="Established(Old) :"></asp:Label>
                                &nbsp;<asp:Label ID="lblOld" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                &nbsp;&nbsp;&nbsp;
                           <asp:ImageButton ID="btnPTVisitRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnPTVisitRef_Click" />




                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
           <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvPTVisit" runat="server"
                            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" GridLines="Horizontal" OnSorting="gvPTVisit_Sorting"
                            EnableModelValidation="True" Width="100%"  CellPadding="2" CellSpacing="0"  >
                            <HeaderStyle CssClass="GridHeader_Gray" />
                            <RowStyle CssClass="GridRow" />
                           
                            <Columns>
                                <asp:TemplateField HeaderText="ACTION" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                            OnClick="Edit_Click" />&nbsp;&nbsp;
                                    </ItemTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="DATE" SortExpression="HPV_DATE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblVisitDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateDesc") %>'></asp:Label>
                                        <asp:Label ID="lblVisitDateTime" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateTimeDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="FILE NO." SortExpression="HPV_PT_Id" >
                                    <ItemTemplate>

                                        <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                        <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                        <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="NAME" SortExpression="HPV_PT_NAME" >
                                    <ItemTemplate>

                                        <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                             

                                <asp:TemplateField HeaderText="DOCTOR" SortExpression="HPV_DR_NAME" >
                                    <ItemTemplate>

                                        <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="TOKEN NO." SortExpression="HPM_TOKEN_NO"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblTokenNo" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VISIT TYPE" SortExpression="HPV_PT_TYPE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="LBLVisitType" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_VISIT_TYPE") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PT TYPE" SortExpression="HPV_PT_TYPEDesc">
                                    <ItemTemplate>

                                        <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="EMR STATUS" SortExpression="HPV_PT_TYPEDesc"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:Label ID="lblEMRStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_STATUSDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="COMPANY" SortExpression="HPV_COMP_NAME" >
                                    <ItemTemplate>

                                        <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="INV. STATUS" SortExpression="InvoiceStatus" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPTVisitInvStatus" runat="server" OnClick="lnkgvPTVisitInvStatus_Click"  >
                                                  <asp:Label ID="lblInvoiceStatus" CssClass="GridRow" runat="server" Text='<%# Bind("InvoiceStatus") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>

        <table border="0" style="width: 99.5%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 210px;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Invoice Dtls." disabled="disabled" />
                </td>
                <td class="lblCaption1">Max. Row Display
                             <asp:TextBox ID="txtInvoiceMaxRow" runat="server" CssClass="TextBoxStyle" Width="50px" Text="100" style="text-align:center;"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                </td>
                <td  >
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label11" runat="server" CssClass="label"
                                Text="Total Invoice :"></asp:Label>

                            <asp:Label ID="lblInvTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp;
                            <asp:Label ID="Label3" runat="server" CssClass="label"
                                Text="Gross :"></asp:Label>

                            <asp:Label ID="lblInvGross" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp;
                                    <asp:Label ID="Label6" runat="server" CssClass="label"
                                        Text="Net :"></asp:Label>

                            <asp:Label ID="lblInvNet" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp;
                                    <asp:Label ID="Label12" runat="server" CssClass="label"
                                        Text="PT.Share :"></asp:Label>

                            <asp:Label ID="lblInvPTShare" runat="server" CssClass="label" Font-Bold="true"></asp:Label>

                            &nbsp; &nbsp;
                                     <asp:ImageButton ID="btnInvoiceRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnInvoiceRef_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
        </table>
        <div style="padding-top: 0px; width: 99%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">

            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvInvoice" runat="server" AutoGenerateColumns="False" BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True" Width="100%"  CellPadding="2" CellSpacing="0"  >
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                        
                        <Columns>
                             <asp:TemplateField HeaderText="ACTION" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInvSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                            OnClick="imgInvSelect_Click" />&nbsp;&nbsp;
                                         <asp:ImageButton ID="imgPrint" runat="server" OnClick="gvInvPrint_Click" ImageUrl="~/WebReports/Images/printer.png" style="height:15px;width:25px;border:none;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="INV. NO."  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                     <asp:Label ID="lblInvType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_TYPE") %>'  Visible="false"></asp:Label>

                                    <asp:Label ID="lblInvoiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_INVOICE_ID") %>'  ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lnkDate" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_DATEDESC") %>' Enabled="false"> </asp:Label>
                                     <asp:Label ID="LinkButton2" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_DATETimeDesc") %>' Enabled="false"> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FILE NO">
                                <ItemTemplate>
                                    <asp:Label ID="LinkButton5" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_ID") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NAME" >
                                <ItemTemplate>

                                    <asp:Label ID="LinkButton4" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_NAME") %>' Enabled="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                          
                           
                            <asp:TemplateField HeaderText="TYPE" >
                                <ItemTemplate>
                                    
                                        <asp:Label ID="lblgvInvType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_TYPE") %>' ></asp:Label>
                                    
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GORSS AMT." ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvInvGrossAmt" runat="server" OnClick="Select_Click" Enabled="false">
                                        <asp:Label ID="lblgvInvGrossAmt" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_GROSS_TOTAL") %>' ></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="NET AMT." ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                     
                                        <asp:Label ID="lblgvInvNetAmt" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_CLAIM_AMOUNT") %>' ></asp:Label>
                                   
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PT. SHARE" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                     
                                        <asp:Label ID="lblgvInvPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PT_AMOUNT") %>'></asp:Label>
                                     
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PAY TYPE"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   
                                        <asp:Label ID="lblgvPaymentType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PAYMENT_TYPE") %>'></asp:Label>
                                    
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>


        </div>


        <table border="0" style="width: 99.5%;" cellpadding="0" cellspacing="0">

            <tr>
               <td style="width: 210px;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Appointment Dtls." disabled="disabled" />
                </td>
                <td class="lblCaption1">Max. Row Display
                             <asp:TextBox ID="txtAppointMaxRow" runat="server" CssClass="TextBoxStyle" Width="50px" Text="100" style="text-align:center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                               Status
                             <asp:DropDownList ID="drpAppointStatus" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" >
                            </asp:DropDownList>
                            &nbsp; &nbsp;
                            </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
                <td   >
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                          

                            <asp:Label ID="Label5" runat="server" CssClass="label"
                                Text="Total Appointment :"></asp:Label>

                            <asp:Label ID="lblAppointTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp;
                                    <asp:Label ID="lblBookedCaption" runat="server" CssClass="label"
                                        Text="Booked :"></asp:Label>
                            <asp:Label ID="lblBooked" runat="server" CssClass="label" Font-Bold="true"></asp:Label>

                            &nbsp; &nbsp;
                                    <asp:Label ID="lblConfirmedCaption" runat="server" CssClass="label"
                                        Text="Confirmed :"></asp:Label>
                            <asp:Label ID="lblConfirmed" runat="server" CssClass="label" Font-Bold="true"></asp:Label>

                            &nbsp; &nbsp;
                                    <asp:Label ID="lblCompletedCaption" runat="server" CssClass="label"
                                        Text="Completed / Closed :"></asp:Label>
                            <asp:Label ID="lblCompleted" runat="server" CssClass="label" Font-Bold="true"></asp:Label>


                            &nbsp; &nbsp;
                           
                            <asp:ImageButton ID="btnAppointmentRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnAppointmentRef_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                
            </tr>
        </table>

         <div style="padding-top: 0px; width: 99%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvAppointment" runat="server" AutoGenerateColumns="False" BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0"  >
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                             <asp:TemplateField HeaderText="DATE" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lnkDate" runat="server" Text='<%# Bind("AppintmentDate") %>'> </asp:Label>&nbsp;
                                <asp:Label ID="LinkButton8" runat="server" Text='<%# Bind("AppintmentSTTime") %>'></asp:Label>&nbsp;
                                <asp:Label ID="LinkButton1" runat="server" Text='<%# Bind("AppintmentFITime") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="FILE NO." >
                                <ItemTemplate>
                                     <asp:Label ID="lblAppFileNo" runat="server" Text='<%# Bind("HAM_FILENUMBER") %>' ></asp:Label>
                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NAME" >
                                <ItemTemplate>
                                    <asp:Label ID="LinkButton4" runat="server" Text='<%# Bind("HAM_PT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                           


                            <asp:TemplateField HeaderText="DOCTOR" >
                                <ItemTemplate>
                                    <asp:Label ID="lblAppDrName" runat="server" Text='<%# Bind("HAM_DR_NAME") %>'> </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="STATUS" >
                                <ItemTemplate>
                                    <asp:Label ID="lblAppStatus" runat="server" Text='<%# Bind("HAS_STATUS") %>'> </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="USER" >
                                <ItemTemplate>
                                    <asp:Label ID="lblAppUser" runat="server" Text='<%# Bind("HAM_CREATEDUSER") %>'> </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>


        <table border="0" style="width: 99.5%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 50%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Visit Pie Chart" disabled="disabled" /><br />
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Chart ID="VisitDtlChart" runat="server">
                                <Series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="pie">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Visit Type"></AxisX>
                                        <AxisY Title="Visit Count"></AxisY>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>

                        </ContentTemplate>
                    </asp:UpdatePanel>



                </td>

                <td style="width: 50%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Billing Pie Chart" disabled="disabled" /><br />
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>

                            <asp:Chart ID="InvoiceDtlChart" runat="server">
                                <Series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="pie">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Visit Type"></AxisX>
                                        <AxisY Title="Visit Count"></AxisY>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

            </tr>
        </table>

    </div>
</asp:Content>
