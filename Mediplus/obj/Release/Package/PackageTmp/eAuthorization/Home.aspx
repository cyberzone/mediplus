﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Mediplus.eAuthorization.Home" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function eAuthApprovalReport(strValue) {
            var win = window.open("WebReport/eAuthApprovalOP.aspx?HAR_ID=" + strValue, "newwin1", "top=80,left=100,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();

        }

        function ShowVisitDetailsPopup() {

            document.getElementById("divVisitDetails").style.display = 'block';


        }

        function HideVisitDetailsPopup() {

            document.getElementById("divVisitDetails").style.display = 'none';

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table style="width: 100%">
        <tr>
            <td>
                <asp:Button ID="btnNewRequest" runat="server" Style="padding-left: 5px; padding-right: 5px;" CssClass="button red small" Width="120px" OnClick="btnNewRequest_Click" Text="Add New Request" />
                <input type="button" id="btnVisitDtlsShow" style="padding-left: 5px; padding-right: 5px; width: 120px;" value="Waiting List" class="button red small" runat="server" title="Visit Dtls" onclick="ShowVisitDetailsPopup()" />

            </td>

        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>

                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table width="85%">
        <tr>
            <td>


                <div class="lblCaption1" style="width: 90%; height: 150px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">


                    <table width="100%">
                        <tr>
                            <td class="lblCaption1">From  Date
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txteAuthFrmDt" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                    Enabled="True" TargetControlID="txteAuthFrmDt" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txteAuthFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            </td>

                            <td class="lblCaption1">To Date
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txteAuthToDt" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                    Enabled="True" TargetControlID="txteAuthToDt" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txteAuthToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Authorization #
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txteAuthHAR_ID" runat="server" CssClass="TextBoxStyle" Height="22px" BorderColor="#cccccc" Width="150px"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Member ID 
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txtSrcMemberID" runat="server" CssClass="TextBoxStyle" Height="22px" BorderWidth="1px" BorderColor="#CCCCCC" Width="150px"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Clinician ID 
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txtSrcClinicianID" CssClass="TextBoxStyle" runat="server" Height="22px" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Payer ID 
                            </td>
                            <td class="lblCaption1">
                                <asp:TextBox ID="txtSrcPayerID" CssClass="TextBoxStyle" runat="server" Height="22px" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>

                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1">Status
                            </td>
                            <td class="lblCaption1" colspan="2">
                                <asp:DropDownList ID="drpApprovalStatus" CssClass="TextBoxStyle" runat="server" Width="150px">
                                    <asp:ListItem Text="All" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="CANCELLED" Value="CANCELLED"></asp:ListItem>
                                    <asp:ListItem Text="PARTIALY APPROVED" Value="PARTIALY APPROVED"></asp:ListItem>
                                    <asp:ListItem Text="PENDING" Value="PENDING"></asp:ListItem>
                                    <asp:ListItem Text="REJECTED" Value="REJECTED"></asp:ListItem>
                                    <asp:ListItem Text="APPROVED" Value="APPROVED"></asp:ListItem>
                                    <asp:ListItem Text="DISPENSE" Value="DISPENSE"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td>

                                <asp:Button ID="btneAuthFind" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button red small"
                                    OnClick="btneAuthFind_Click" Text="Search" OnClientClick="return ShoweAuthVal();" />

                            </td>
                            <td></td>
                        </tr>


                    </table>
                </div>
            </td>
            <td>
                <div class="lblCaption1" style="width: 90%; height: 150px; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
                    <table width="100%" cellpadding="5" cellspaccing="5">
                        <tr>
                            <td class="lblCaption1">
                                <asp:Image ID="Image1" runat="server" Height="18px" Width="18px" ImageUrl="~/Images/Pending.PNG" />&nbsp;Pending&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
        <asp:Label ID="lblTotalPending" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            </td>

                            <td class="lblCaption1">
                                <asp:Image ID="Image2" runat="server" Height="18px" Width="18px" ImageUrl="~/Images/Approved.PNG" />&nbsp;Approved&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:Label ID="lblTotalApproval" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                <asp:Image ID="Image3" runat="server" Height="18px" Width="18px" ImageUrl="~/Images/PartiallyApproved.PNG" />&nbsp;Partialy Approved&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Label ID="lblTotalParApproved" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            </td>

                            <td class="lblCaption1">
                                <asp:Image ID="Image4" runat="server" Height="18px" Width="18px" ImageUrl="~/Images/Rejected.PNG" />&nbsp;Rejected&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
         <asp:Label ID="lblTotalRejected" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                <asp:Image ID="Image6" runat="server" Height="18px" Width="18px" ImageUrl="~/Images/Canceled.PNG" />&nbsp;Cancelled &nbsp;&nbsp;
        <asp:Label ID="lblTotalCancelled" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>

                            <td class="lblCaption1" colspan="2" style="font-weight: bold; height: 40px; vertical-align: middle;">&nbsp;Total Requests &nbsp;&nbsp;<asp:Label ID="lblTotalRequest" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>

                    </table>


                    <br />
                </div>
            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 85%; height: 400px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
        <table width="100%">
            <tr>
                <td>
                    <asp:GridView ID="gvAuth" runat="server" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%" PageSize="200" GridLines="None" OnRowDataBound="gvAuth_RowDataBound">
                        <HeaderStyle CssClass="GridHeader_Gray" Font-Bold="true" />
                        <RowStyle CssClass="GridRow" />


                        <Columns>
                            <asp:TemplateField HeaderText="Request ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkHarID" runat="server" OnClick="SelectResubmit_Click">
                                        <asp:Label ID="lblApprovalStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_APPROVAL_STATUS") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblHarID" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblEmrID" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_EMR_ID") %>' Visible="false"></asp:Label>

                                        <asp:Label ID="Label10" CssClass="lblCaption" runat="server" Text='<%# Bind("HAR_TRANS_ID") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Authorization #">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnIDPayerk" runat="server" OnClick="SelectResubmit_Click">
                                        <asp:Label ID="lblApprovalNo" CssClass="lblCaption" runat="server" Text='<%# Bind("HAR_ID_PAYER") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trans. Date">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbkTranDate" runat="server" OnClick="SelectResubmit_Click">
                                        <asp:Label ID="lblTransDate" CssClass="lblCaption" runat="server" Text='<%# Bind("HAR_TRANS_DATEDesc") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Company Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="SelectResubmit_Click">
                                        <asp:Label ID="lblInsCode" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_INS_CODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblInsName" CssClass="lblCaption" runat="server" Text='<%# Bind("HAR_INS_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Member ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SelectResubmit_Click">
                                        <asp:Label ID="lblResubMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_MEMBER_ID") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Clinician ID">
                                <ItemTemplate>

                                    <asp:Label ID="lblResubClinicianID" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_CLINICIAN_ID") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>

                                    <asp:Label ID="lblEndDate" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_STATUSDesc") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Actions" Visible="true">
                                <ItemTemplate>
                                    <asp:Image ID="imgApprovalStatus" runat="server" Height="18px" Width="18px" ImageUrl="Images/Pending.PNG" />&nbsp;&nbsp;
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18px" Width="18px"
                        OnClick="DeleteAuth_Click" OnClientClick="return window.confirm('Do you want to Delete eAuthorization?')" />&nbsp;&nbsp;
                                                
                                </ItemTemplate>
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                    </asp:GridView>
                </td>
            </tr>

        </table>

    </div>

    <br />


    <div id="divVisitDetails" style="display: none; overflow: hidden; border: groove; height: 500px; width: 950px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 200px; top: 150px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideVisitDetailsPopup()" />
                </td>
            </tr>

        </table>
        <table style="width: 100%">
            <tr>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            From Date :&nbsp;
                            <asp:TextBox ID="txtSrcFromDate" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtsrcFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            To Date :&nbsp;
                            <asp:TextBox ID="txtSrcToDate" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtsrcToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            File No:&nbsp;
                            <asp:TextBox ID="txtSrcFileNo" CssClass="label" runat="server" Height="22px" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnVisitSrc" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button orange small"
                                OnClick="btnVisitSrc_Click" Text="Search" OnClientClick="return ShoweAuthVal();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>


        <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border-color: #02a0e0; border-style: dotted; border-width: 2px;">
            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gvEMRVisits" runat="server" Width="100%" AutoGenerateColumns="False" EnableModelValidation="True" GridLines="None">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <FooterStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>

                            <asp:TemplateField HeaderText="File No." FooterText="File No" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click">

                                        <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("EPM_ID") %>'></asp:Label>

                                        <asp:Label ID="lblPatientId" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_PT_ID") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Patient Name" FooterText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPTName" CssClass="lblCaption1" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblPTName" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_PT_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Date Time" FooterText="Date Time" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkVisitDate" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblVisitDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_DATEDesc") %>'></asp:Label>
                                    </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comp Name" FooterText="Comp Name" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCompName" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblCompCode" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_INS_CODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblCompName" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_INS_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Doctor" FooterText="Doctor" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Edit_Click">
                                        <asp:Label ID="lblDrID" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_DR_CODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblDrName" CssClass="lblCaption1" runat="server" Text='<%# Bind("EPM_DR_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>





                        </Columns>


                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>


    </div>
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Enabled="true" Interval="120000"></asp:Timer>
</asp:Content>
