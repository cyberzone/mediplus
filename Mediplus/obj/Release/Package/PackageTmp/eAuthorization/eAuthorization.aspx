﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="eAuthorization.aspx.cs" Inherits="Mediplus.eAuthorization.eAuthorization" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }


        #divDiagServ
        {
            width: 400px !important;
        }

            #divDiagServ div
            {
                width: 400px !important;
            }


        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>
      <script language="javascript" type="text/javascript">
     function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


        function ClinicianSelected() {
            if (document.getElementById('<%=txtClinicianID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtClinicianID.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtClinicianID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtClinicianName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }




        function ClinicianNameSelected() {
            if (document.getElementById('<%=txtClinicianName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtClinicianName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {

                    document.getElementById('<%=txtClinicianID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtClinicianName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }


          </script>

        


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="padding: 5px; width: 85%; overflow: auto; border: thin; border-color: #cccccc; border-style: none;">
        <table style="width: 100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>

                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="text-align:right";>
               <asp:ImageButton ID="imgBack" runat="server" style="width:40px;height:40px;" ImageUrl="~/Images/Back.png" ToolTip="Back"  OnClick="imgBack_Click" />
            </td>

        </tr>
    </table>

        <table style="width: 100%" border="0">
            <tr>
                <td class="lblCaption1" style="height: 30px;">Provider Name  : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:DropDownList ID="drpProvider" runat="server" Width="200px" CssClass="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpProvider_SelectedIndexChanged"></asp:DropDownList>
                </td>
                <td class="lblCaption1" style="height: 30px;">PT. ID & Name  : 
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPTID" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="100px" MaxLength="10"></asp:TextBox>
                            <asp:TextBox ID="txtPTName" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px" MaxLength="50"></asp:TextBox>




                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td></td>
                <td>

                </td>
            </tr>
            <tr>

                <td class="lblCaption1" style="height: 30px;">Order Date  : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:TextBox ID="txtOrderDate" runat="server" Width="70px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender5" runat="server"
                        Enabled="True" TargetControlID="txtOrderDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="True" TargetControlID="txtOrderDate" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>

                </td>

                <td class="lblCaption1" style="height: 30px;">Company  : <span style="color: red;">* </span>
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="100px" MaxLength="10" onblur="return CompIdSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px" MaxLength="50" onblur="return CompNameSelected()" AutoPostBack="true" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>
                            <div id="divComp" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divComp">
                            </asp:AutoCompleteExtender>



                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 30px;">Payer ID  : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPayerID" CssClass="TextBoxStyle" runat="server" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>

            </tr>

            <tr>
                <td class="lblCaption1" style="height: 30px;">Member ID  : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:TextBox ID="txtMemberID" runat="server" Width="200px" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>

                </td>
                <td class="lblCaption1" style="height: 30px;">Clinician   : <span style="color: red;">* </span>
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtClinicianID" CssClass="TextBoxStyle" runat="server" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC" onblur="return ClinicianSelected()"></asp:TextBox>
                            <asp:TextBox ID="txtClinicianName" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px" MaxLength="50" onblur="return ClinicianNameSelected()"></asp:TextBox>
                            <div id="divDr" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtClinicianID" MinimumPrefixLength="1" ServiceMethod="GetClinician"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtClinicianName" MinimumPrefixLength="1" ServiceMethod="GetClinicianName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                            </asp:AutoCompleteExtender>



                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 30px;">Receiver ID : <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtReciverID" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                        </ContentTemplate>

                    </asp:UpdatePanel>

                </td>
            </tr>

            <tr id="trResub" runat="server" visible="false">
                <td class="lblCaption1" style="height: 30px; vertical-align: top;">Resub. Type  :
                </td>
                <td style="vertical-align: top;">
                    <asp:DropDownList ID="drpResubType" CssClass="TextBoxStyle" runat="server" Width="200px">
                        <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Correction" Value="correction"></asp:ListItem>
                        <asp:ListItem Text="Internal Complaint" Value="internal complaint"></asp:ListItem>

                    </asp:DropDownList>
                </td>
                <td class="lblCaption1" style="height: 30px; width: 150px; vertical-align: top;">Resub. Comment  : 
                </td>
                <td style="vertical-align: top;" colspan="3">
                    <asp:TextBox ID="txtResubComment" runat="server" Width="350px" CssClass="TextBoxStyle" BorderWidth="1px" Height="40px" TextMode="MultiLine" BorderColor="#cccccc" Style="resize: none;"></asp:TextBox>

                </td>
                <td class="lblCaption1" style="height: 30px;">Authorization No: <span style="color: red;">* </span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtAuthorizationNo" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" BorderColor="#CCCCCC" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>

                    </asp:UpdatePanel>

                </td>
            </tr>



        </table>

        <br />
        <span class="lblCaption1" style="font-weight: bold;">Diagnosis</span>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 50%; vertical-align: top;">
                    <div style="width: 99%; height: 150px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove; padding: 5px;">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="width: 60%;">
                                    <asp:TextBox ID="txtDiagName" runat="server" Width="98%" Height="18PX" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>

                                    <div id="divDiagExt" style="visibility: hidden;"></div>

                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="server" TargetControlID="txtDiagName" MinimumPrefixLength="1" ServiceMethod="GetICDName"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt" DelimiterCharacters="" Enabled="True" ServicePath="">
                                    </asp:AutoCompleteExtender>

                                </td>
                                <td style="">
                                    <asp:DropDownList ID="drpDiagType" runat="server" CssClass="TextBoxStyle" Width="100PX" Height="22PX" BorderColor="#cccccc">
                                        <asp:ListItem Value="Principal" Selected="True">Principal</asp:ListItem>
                                        <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                        <asp:ListItem Value="Admitting">Admitting</asp:ListItem>
                                    </asp:DropDownList>&nbsp;
                                        <asp:Button ID="btnDiagAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 50px; border-radius: 5px 5px 5px 5px" CssClass="orange"
                                            OnClick="btnDiagAdd_Click" Text="Add" OnClientClick="return PTDiagVal();" />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnColor1" runat="server" Width="5px" Height="10px" Enabled="false" BorderStyle="None" Style="background-color: #ff0000;" />&nbsp; <span class="lblCaption1">Principal</span>&nbsp;
                            <asp:Button ID="btnColor2" runat="server" Width="5px" Height="10px" Enabled="false" BorderStyle="None" Style="background-color: #9ACD32;" />&nbsp; <span class="lblCaption1">Secondary</span>&nbsp;
                           <asp:Button ID="btnColor3" runat="server" Width="5px" Height="10px" Enabled="false" BorderStyle="None" Style="background-color: #FFFF00;" />&nbsp; <span class="lblCaption1">Admitting</span>

                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td style="width: 60%; vertical-align: top;">
                    <div style="width: 99%; height: 150px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove; padding: 5px;">
                        <asp:GridView ID="gveAuthDiagnosis" runat="server" AutoGenerateColumns="False" OnRowDataBound="gveAuthDiagnosis_RowDataBound"
                            EnableModelValidation="True" Width="100%" PageSize="200" ShowHeader="false" GridLines="None">
                            <HeaderStyle CssClass="GridHeader" Font-Bold="True" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiagType" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_TYPE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblDiagCode" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>&nbsp;  - 
                                            <asp:Label ID="lblDiagName" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>&nbsp;
                                             <asp:Button ID="btnColor" runat="server" Width="5px" Height="10px" Enabled="false" BorderStyle="None" />
                                        &nbsp;&nbsp;
                                             <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" Height="10px" ImageUrl="~/Images/icon_delete.png"
                                                 OnClick="DeleteDiag_Click" />&nbsp;&nbsp;

                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                            <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>



        <br />
         

        </div> 
</asp:Content>
