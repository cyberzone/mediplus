﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace HMS.Laboratory
{
    public partial class LabTestReport : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();




        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void LogFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../LabTestReportLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();

            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSigUser.DataSource = ds;
                drpSigUser.DataValueField = "HUM_USER_ID";
                drpSigUser.DataTextField = "HUM_USER_NAME";
                drpSigUser.DataBind();

            }

        }


        void BindReportType()
        {

            DataSet ds = new DataSet();
            objLab = new LaboratoryBAL();

            string Criteria = " 1=1 AND LIPM_STATUS='A' ";
            ds = objLab.InvProfileMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpReportType.DataSource = ds;
                drpReportType.DataTextField = "LIPM_DESCRIPTION";
                drpReportType.DataValueField = "LIPM_INVESTIGATION_PROFILE_ID";
                drpReportType.DataBind();
            }

            drpReportType.Items.Insert(0, "--- Select ---");
            drpReportType.Items[0].Value = "0";
        }

        void BindNationality()
        {
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "Select");
            drpNationality.Items[0].Value = "0";



        }

        void BindDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";// AND HSFM_DEPT_ID='RADIOLOGY' 

            if (GlobalValues.FileDescription == "SMCH")
            {
                Criteria += "  AND HSFM_DEPT_ID='PATHOLOGY' ";
            }

            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDoctors.DataSource = ds;
                drpDoctors.DataTextField = "FullName";
                drpDoctors.DataValueField = "HSFM_STAFF_ID";
                drpDoctors.DataBind();


                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;



            }

            drpDoctors.Items.Insert(0, "--- Select ---");
            drpDoctors.Items[0].Value = "";


        }


        void BinRefdDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpRefDoctor.DataSource = ds;
                drpRefDoctor.DataTextField = "FullName";
                drpRefDoctor.DataValueField = "HSFM_STAFF_ID";
                drpRefDoctor.DataBind();

            }



            drpRefDoctor.Items.Insert(0, "--- Select ---");
            drpRefDoctor.Items[0].Value = "";
        }

        void BindReportCategory()
        {

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 AND LTC_STATUS='A' ";
            ds = objLab.TestCategoryGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpReportCategory.DataSource = ds;
                drpReportCategory.DataTextField = "LTC_DESCRIPTION";
                drpReportCategory.DataValueField = "LTC_CODE";
                drpReportCategory.DataBind();
            }

            drpReportCategory.Items.Insert(0, "--- Select ---");
            drpReportCategory.Items[0].Value = "0";
        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                //  lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                lblAgeType.Text   = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                lblAgeType1.Text  = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);


                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                //txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                //txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                //txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                //txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();

                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]).ToUpper())
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNationality;
                        }
                    }

                }

            ForNationality: ;

                //txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                //txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                //txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                // if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                //   lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                // txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                txtProviderID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtIDNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_NO"]);
                //txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);



                if (ds.Tables[0].Rows[0].IsNull("HPM_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]) != "")
                {
                    for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                    {
                        if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]).ToUpper())
                        {
                            drpDoctors.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);
                            goto ForDoctors;
                        }
                    }

                }

            ForDoctors: ;


                if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "")
                {
                    for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                    {
                        if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]).ToUpper())
                        {
                            drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                            goto ForRefDoctors;
                        }
                    }

                }

            ForRefDoctors: ;
                // GetPatientVisit();

                drpInvType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";


            }

        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";
            if (strType == "ResultStatus")
            {
                Criteria += "  AND HCM_CODE IN ('F','C','X') ";
            }

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "HCM_DESC");

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (strType == "LabDiagnosticService")
                {
                    drpDiagnosticService.DataSource = DS;
                    drpDiagnosticService.DataTextField = "HCM_DESC";
                    drpDiagnosticService.DataValueField = "HCM_CODE";
                    drpDiagnosticService.DataBind();
                }
                if (strType == "ResultStatus")
                {
                    drpResultStatus.DataSource = DS;
                    drpResultStatus.DataTextField = "HCM_DESC";
                    drpResultStatus.DataValueField = "HCM_CODE";
                    drpResultStatus.DataBind();
                }

                if (strType == "DiagnosticCenter")
                {
                    drpExternalLab.DataSource = DS;
                    drpExternalLab.DataTextField = "HCM_DESC";
                    drpExternalLab.DataValueField = "HCM_DESC";
                    drpExternalLab.DataBind();
                }


                if (strType == "OrganismType")
                {
                    drpOrganismType.DataSource = DS;
                    drpOrganismType.DataTextField = "HCM_DESC";
                    drpOrganismType.DataValueField = "HCM_CODE";
                    drpOrganismType.DataBind();
                }
                if (strType == "SpecimenCollectionMethod")
                {
                    drpCollectionMethod.DataSource = DS;
                    drpCollectionMethod.DataTextField = "HCM_DESC";
                    drpCollectionMethod.DataValueField = "HCM_CODE";
                    drpCollectionMethod.DataBind();
                }


            }


            if (strType == "LabDiagnosticService")
            {
                drpDiagnosticService.Items.Insert(0, "--- Select ---");
                drpDiagnosticService.Items[0].Value = "";
            }

            if (strType == "ResultStatus")
            {
                drpResultStatus.Items.Insert(0, "--- Select ---");
                drpResultStatus.Items[0].Value = "";
            }

            if (strType == "DiagnosticCenter")
            {
                drpExternalLab.Items.Insert(0, "--- Select ---");
                drpExternalLab.Items[0].Value = "";
            }


            if (strType == "OrganismType")
            {
                drpOrganismType.Items.Insert(0, "--- Select ---");
                drpOrganismType.Items[0].Value = "";
            }

            if (strType == "SpecimenCollectionMethod")
            {
                drpCollectionMethod.Items.Insert(0, "--- Select ---");
                drpCollectionMethod.Items[0].Value = "";
            }

        }

        void BindAntibioticsType()
        {

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "LAM_STATUS = 'A' ";
            DS = objCommBal.fnGetFieldValue("*", "LAB_ANTIBIOTIC_MASTER", Criteria, "LAM_DESCRIPTION");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpAntibioticsType.DataSource = DS;
                drpAntibioticsType.DataTextField = "LAM_DESCRIPTION";
                drpAntibioticsType.DataValueField = "LAM_CODE";
                drpAntibioticsType.DataBind();
            }

            drpAntibioticsType.Items.Insert(0, "--- Select ---");
            drpAntibioticsType.Items[0].Value = "";


        }


        void PTDtlsClear()
        {

            txtFName.Text = "";
            lblStatus.Text = "";

            txtDOB.Text = "";
            txtAge.Text = "";
            txtAge1.Text = "";
            lblAgeType.Text = "";
            lblAgeType1.Text = "";
            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            //txtAddress.Text = "";
            //txtPoBox.Text = "";

            //txtArea.Text = "";
            //txtCity.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";


            txtProviderID.Text = "";
            txtProviderName.Text = "";
            txtPolicyNo.Text = "";
            txtIDNo.Text = "";

            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";




            drpDoctors.SelectedIndex = 0;


        }

        void BindServiceMasterGrid()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='L' ";


            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_SERV_ID LIKE '" + txtServCode.Text.Trim() + "%'";
            //}

            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_NAME LIKE '" + txtServName.Text.Trim() + "%'";
            //}
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvServMasterPopup.DataSource = DS;
                gvServMasterPopup.DataBind();
            }

        }

        void BindTestSampleMasterGrid()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            DS = objLab.TestSampleMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvSampleMasterPopup.DataSource = DS;
                gvSampleMasterPopup.DataBind();
            }

        }



        void BindLabTestReportMaster()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";
            DS = objLab.TestReportMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["NewFlag"] = false;
                    txtTransNo.Text = Convert.ToString(DR["LTRM_TEST_REPORT_ID"]);


                    txtTestSampleID.Text = Convert.ToString(DR["LTRM_SAMPLE_ID"]);
                    if (DR.IsNull("LTRM_STATUS") == false && Convert.ToString(DR["LTRM_STATUS"]) != "")
                    {
                        drpStatus.SelectedValue = Convert.ToString(DR["LTRM_STATUS"]);
                    }
                    txtFileNo.Text = Convert.ToString(DR["LTRM_PATIENT_ID"]);
                    txtFName.Text = Convert.ToString(DR["LTRM_PATIENT_NAME"]);
                    txtAge.Text = Convert.ToString(DR["LTRM_PATIENT_AGE"]);
                    if (DR.IsNull("LTRM_PATIENT_AGETYPE") == false && Convert.ToString(DR["LTRM_PATIENT_AGETYPE"]) != "")
                    {
                        lblAgeType.Text = Convert.ToString(DR["LTRM_PATIENT_AGETYPE"]);
                    }
                    txtSex.Text = Convert.ToString(DR["LTRM_PATIENT_SEX"]);



                    if (DR.IsNull("LTRM_PATIENT_NATIONALITY") == false && Convert.ToString(DR["LTRM_PATIENT_NATIONALITY"]) != "")
                    {
                        for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                        {
                            if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTRM_PATIENT_NATIONALITY"]).ToUpper())
                            {
                                drpNationality.SelectedValue = Convert.ToString(DR["LTRM_PATIENT_NATIONALITY"]);
                                goto ForNationality;
                            }
                        }

                    }
                ForNationality: ;

                    //= Convert.ToString(DR["LTRM_RANGE"]);
                    if (DR.IsNull("LTRM_INVESTIGATION_PROFILE_ID") == false && Convert.ToString(DR["LTRM_INVESTIGATION_PROFILE_ID"]) != "")
                    {
                        drpReportType.SelectedValue = Convert.ToString(DR["LTRM_INVESTIGATION_PROFILE_ID"]);
                    }
                    //drpReportType.= Convert.ToString(DR["LTRM_INVESTIGATION_DESCRIPTION"]);
                    if (DR.IsNull("LTRM_REF_DR") == false && Convert.ToString(DR["LTRM_REF_DR"]) != "")
                    {
                        drpRefDoctor.SelectedValue = Convert.ToString(DR["LTRM_REF_DR"]);
                    }
                    drpDoctors.SelectedValue = Convert.ToString(DR["LTRM_DR_CODE"]);

                    txtServCode.Text = Convert.ToString(DR["LTRM_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DR["LTRM_SERV_DESCRIPTION"]);
                    txtFee.Text = Convert.ToString(DR["LTRM_FEE"]);
                    txtCaption.Text = Convert.ToString(DR["LTRM_CAPTION"]);
                    txtRemarks.Text = Convert.ToString(DR["LTRM_REMARKS"]);


                    txtProviderID.Text = Convert.ToString(DR["LTRM_COMP_ID"]);
                    txtProviderName.Text = Convert.ToString(DR["LTRM_COMP_NAME"]);
                    txtPolicyNo.Text = Convert.ToString(DR["LTRM_POLICY_NO"]);



                    if (DR.IsNull("LTRM_REPORT_SOURCE") == false && Convert.ToString(DR["LTRM_REPORT_SOURCE"]) != "")
                    {
                        drpSource.SelectedValue = Convert.ToString(DR["LTRM_REPORT_SOURCE"]);
                    }
                    txtSourceName.Text = Convert.ToString(DR["LTRM_REPORT_SOURCENAME"]);


                    txtAdminID.Text = Convert.ToString(DR["LTRM_ADMN"]);
                    txtWard.Text = Convert.ToString(DR["LTRM_WARD"]);
                    txtRoom.Text = Convert.ToString(DR["LTRM_ROOM"]);
                    txtBed.Text = Convert.ToString(DR["LTRM_BED"]);

                    drpInvType.SelectedValue = Convert.ToString(DR["LTRM_INV_TYPE"]);
                    drpPTType.SelectedValue = Convert.ToString(DR["LTRM_PT_TYPE"]);


                    txtInvoiceRefNo.Text = Convert.ToString(DR["LTRM_INVOICE_ID"]);
                    txtEMRID.Text = Convert.ToString(DR["LTRM_EMR_ID"]);







                    if (DR.IsNull("LTRM_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DR["LTRM_DIAGNOSTIC_SERVICE"]) != "")
                    {
                        drpDiagnosticService.SelectedValue = Convert.ToString(DR["LTRM_DIAGNOSTIC_SERVICE"]);
                    }

                    if (DR.IsNull("LTRM_RESULT_STATUS") == false && Convert.ToString(DR["LTRM_RESULT_STATUS"]) != "")
                    {
                        drpResultStatus.SelectedValue = Convert.ToString(DR["LTRM_RESULT_STATUS"]);
                    }


                    if (DR.IsNull("LTRM_EXTERNAL_LAB_NAME") == false && Convert.ToString(DR["LTRM_EXTERNAL_LAB_NAME"]) != "")
                    {
                        drpExternalLab.SelectedValue = Convert.ToString(DR["LTRM_EXTERNAL_LAB_NAME"]);
                    }

                    txtExtLabReferenceNo.Text = Convert.ToString(DR["LTRM_EXTERNAL_LAB_REF_NO"]);

                    if (DR.IsNull("LTRM_MICROORGANISM_IDENTIFIED") == false && Convert.ToString(DR["LTRM_MICROORGANISM_IDENTIFIED"]) != "")
                    {

                        chkMicroorganismIdentified.Checked = Convert.ToBoolean(DR["LTRM_MICROORGANISM_IDENTIFIED"]);
                    }




                    if (DR.IsNull("LTRM_COLLECTION_METHOD_CODE") == false && Convert.ToString(DR["LTRM_COLLECTION_METHOD_CODE"]) != "")
                    {
                        drpCollectionMethod.SelectedValue = Convert.ToString(DR["LTRM_COLLECTION_METHOD_CODE"]);
                    }



                    txtBarecodeID.Text = Convert.ToString(DR["LTRM_BARECODE"]);

                    if (Convert.ToString(DR["LTRM_AUTHORIZE_STATUS"]) == "AUTHORIZED")
                    {

                        lblAuthorizeStatus.Text = "Authorized";
                    }
                    else
                    {
                        lblAuthorizeStatus.Text = "Not Authorized";
                    }

                    if (DR.IsNull("LTRM_TEST_DATEDesc") == false)
                    {
                        txtTransDate.Text = Convert.ToString(DR["LTRM_TEST_DATEDesc"]);
                    }

                    if (DR.IsNull("LTRM_TEST_DATETimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["LTRM_TEST_DATETimeDesc"]);

                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpTransHour.SelectedValue = strHour;
                            drpTransMin.SelectedValue = arrTimeUp1[1];
                        }

                    }




                    if (DR.IsNull("LTRM_COLLECTION_DATEDesc") == false)
                    {
                        txtSampleCollDate.Text = Convert.ToString(DR["LTRM_COLLECTION_DATEDesc"]);
                    }

                    if (DR.IsNull("LTRM_COLLECTION_DATE") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["LTRM_COLLECTION_DATETimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpCollHour.SelectedValue = strHour;
                            drpCollMin.SelectedValue = arrTimeUp1[1];
                        }

                    }

                    if (DR.IsNull("LTRM_DELIVERY_DATEDesc") == false)
                    {
                        txtDeliveryDate.Text = Convert.ToString(DR["LTRM_DELIVERY_DATEDesc"]);
                    }

                    if (DR.IsNull("LTRM_DELIVERY_DATETimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["LTRM_DELIVERY_DATETimeDesc"]);

                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpDelivHour.SelectedValue = strHour;
                            drpDelivMin.SelectedValue = arrTimeUp1[1];
                        }

                    }


                    if (DR.IsNull("LTRM_CRITICAL_RESULT") == false && Convert.ToString(DR["LTRM_CRITICAL_RESULT"]) != "")
                    {

                        chkCriticalResult.Checked = Convert.ToBoolean(DR["LTRM_CRITICAL_RESULT"]);
                    }
                }



                if (drpResultStatus.SelectedValue == "X")
                {
                    lblStatus.Text = "Order Already canceled.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                }

            }

        }

        void BindTestReportMasterGrid()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1  ";

            //if (GlobalValues.FileDescription == "SMCH")
            //{
            //    Criteria += "   AND  LTRM_AUTHORIZE_STATUS='AUTHORIZED' ";
            //}

            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND LTRM_PATIENT_ID = '" + txtFileNo.Text.Trim() + "'";
            }

            DS = objLab.TestReportMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTestMasterPopup.DataSource = DS;
                gvTestMasterPopup.DataBind();
            }


        }

        void BuildeTestReportDtls()
        {
            string Criteria = "1=2";
            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestReportDtlsGet(Criteria);

            ViewState["TestReportDtls"] = DS.Tables[0];
        }

        void BindTestReportDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTRD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTRD_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestReportDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["TestReportDtls"] = DS.Tables[0];

            }
        }

        void BindTempTestReportDtls()
        {

            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["TestReportDtls"];
            if (DT.Rows.Count > 0)
            {
                gvTestReportDtls.DataSource = DT;
                gvTestReportDtls.DataBind();

                ViewState["EditedRow"] = "";

                if (GlobalValues.FileDescription != "SMCH")
                {
                    gvTestReportDtls.Columns[9].Visible = false;
                    gvTestReportDtls.Columns[10].Visible = false;
                    gvTestReportDtls.Columns[11].Visible = false;
                }

            }
            else
            {
                gvTestReportDtls.DataBind();
            }

        }



        void BindTestSampleMaster()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND LTSM_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            DS = objLab.TestSampleMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    txtFileNo.Text = Convert.ToString(DR["LTSM_PATIENT_ID"]);
                    txtFileNo_TextChanged(txtFileNo, new EventArgs());

                    if (DR.IsNull("LTSM_DATEDesc") == false && Convert.ToString(DR["LTSM_DATEDesc"]) != "")
                    {
                        txtSampleCollDate.Text = Convert.ToString(DR["LTSM_DATEDesc"]);
                    }

                    if (DR.IsNull("LTSM_DATETimeDesc") == false && Convert.ToString(DR["LTSM_DATETimeDesc"]) != "")
                    {

                        string strTimeUp = Convert.ToString(DR["LTSM_DATETimeDesc"]);


                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpCollHour.SelectedValue = strHour;
                            drpCollMin.SelectedValue = arrTimeUp1[1];
                        }

                    }


                    if (DR.IsNull("LTSM_DELIVERY_DATEDesc") == false && Convert.ToString(DR["LTSM_DELIVERY_DATEDesc"]) != "")
                    {

                        txtDeliveryDate.Text = Convert.ToString(DR["LTSM_DELIVERY_DATEDesc"]);
                    }
                    if (DR.IsNull("LTSM_DELIVERY_DATETimeDesc") == false && Convert.ToString(DR["LTSM_DELIVERY_DATETimeDesc"]) != "")
                    {

                        string strTimeUp = Convert.ToString(DR["LTSM_DELIVERY_DATETimeDesc"]);

                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpDelivHour.SelectedValue = strHour;
                            drpDelivMin.SelectedValue = arrTimeUp1[1];
                        }

                    }

                    if (DR.IsNull("LTSM_REF_DR") == false && Convert.ToString(DR["LTSM_REF_DR"]) != "")
                    {
                        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                        {
                            if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_REF_DR"]).ToUpper())
                            {
                                drpRefDoctor.SelectedValue = Convert.ToString(DR["LTSM_REF_DR"]);
                                goto FordrpRefDoctor;
                            }
                        }

                    }
                FordrpRefDoctor: ;


                    if (DR.IsNull("LTSM_DR_ID") == false && Convert.ToString(DR["LTSM_DR_ID"]) != "")
                    {
                        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                        {
                            if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_DR_ID"]).ToUpper())
                            {
                                drpDoctors.SelectedValue = Convert.ToString(DR["LTSM_DR_ID"]);
                                goto FordrpDoctors;
                            }
                        }

                    }
                FordrpDoctors: ;

                    /*
                    txtFName.Text = Convert.ToString(DR["LTSM_PATIENT_NAME"]);
                    txtAge.Text = Convert.ToString(DR["LTSM_PATIENT_AGE"]);
                    if (DR.IsNull("LTSM_PATIENT_AGETYPE") == false && Convert.ToString(DR["LTSM_PATIENT_AGETYPE"]) != "")
                    {
                        drpAgeType.SelectedValue = Convert.ToString(DR["LTSM_PATIENT_AGETYPE"]);
                    }
                    txtSex.Text = Convert.ToString(DR["LTSM_PATIENT_SEX"]);


                    if (DR.IsNull("LTSM_PATIENT_NATIONALITY") == false && Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]) != "")
                    {
                        for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                        {
                            if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]).ToUpper())
                            {
                                drpNationality.SelectedValue = Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]);
                                goto ForNationality;
                            }
                        }

                    }
                ForNationality: ;




                    //= Convert.ToString(DR["LTSM_RANGE"]);
                    if (DR.IsNull("LTSM_INVESTIGATION_PROFILE_ID") == false && Convert.ToString(DR["LTSM_INVESTIGATION_PROFILE_ID"]) != "")
                    {
                        drpReportType.SelectedValue = Convert.ToString(DR["LTSM_INVESTIGATION_PROFILE_ID"]);
                    }
                    //drpReportType.= Convert.ToString(DR["LTRM_INVESTIGATION_DESCRIPTION"]);
                    if (DR.IsNull("LTSM_REF_DR") == false && Convert.ToString(DR["LTSM_REF_DR"]) != "")
                    {
                        drpRefDoctor.SelectedValue = Convert.ToString(DR["LTSM_REF_DR"]);
                    }
                    drpDoctors.SelectedValue = Convert.ToString(DR["LTSM_DR_ID"]);


                    //txtProviderID.Text = Convert.ToString(DR["LTRM_COMP_ID"]);
                    //txtProviderName.Text = Convert.ToString(DR["LTRM_COMP_NAME"]);
                    //txtPolicyNo.Text = Convert.ToString(DR["LTRM_POLICY_NO"]);
                     * 
                     * */
                }

            }

        }

        void BindTestSampleDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestSampleDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestReportDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTRD_TEST_PROFILE_ID"] = Convert.ToString(DR["LTSD_TEST_PROFILE_ID"]);
                    objrow["LTRD_TEST_DESCRIPTION"] = Convert.ToString(DR["LTSD_TEST_DESCRIPTION"]);

                    DT.Rows.Add(objrow);

                }

                ViewState["TestReportDtls"] = DT;

            }
        }

        DataSet PatientMasterGridGet()
        {

            //  txtPolicyNo.Text = "";
            // txtPolicyType.Text = "";
            //  txtIdNo.Text = "";
            //  txtPackage.Text = "";


            DataSet DSpt = new DataSet();
            string Criteria = " 1=1 ";

            //Criteria += " AND (  HPM_PT_ID    like '%" + txtSearch.Text.Trim() + "%' OR HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%' ";
            //Criteria += " AND    HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' OR HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' ";
            //Criteria += " AND    HPM_MOBILE   like '%" + txtSearch.Text.Trim() + "%') ";


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND  HPM_PT_ID ='" + txtFileNo.Text.Trim() + "'";
            }
            else if (txtFName.Text.Trim() != "")
            {
                Criteria += " AND  ( HPM_PT_FNAME like '%" + txtFName.Text.Trim() + "%'   OR    HPM_PT_MNAME like '%" + txtFName.Text.Trim() + "%' OR HPM_PT_LNAME like '%" + txtFName.Text.Trim() + "%' )";
            }

            CommonBAL objCom = new CommonBAL();
            DSpt = objCom.PatientMasterGet(Criteria);


            if (DSpt.Tables[0].Rows.Count > 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPTDtlsPopupTestRpt()", true);
                gvPTDtlsTestRpt.Visible = true;
                gvPTDtlsTestRpt.DataSource = DSpt;
                gvPTDtlsTestRpt.DataBind();


            }




            return DSpt;
        }

        Boolean CheckSampleDtlsResultStatus(string PROFILE_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSD_TEST_RESULT='Completed'   ";
            Criteria += " AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "' AND LTSD_TEST_PROFILE_ID='" + PROFILE_ID + "'";


            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestSampleDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                return true;
            }

            return false;
        }

        void BindInvProfileTrans()
        {
            CommonBAL objCom = new CommonBAL();
            string CriteriaServMas = "HSM_STATUS='A' AND HSM_OBSERVATION_NEEDED='Y' AND HSM_HAAD_CODE='" + txtServCode.Text + "'";
            DataSet DSServMas = new DataSet();

            DSServMas = objCom.fnGetFieldValue("*", "HMS_SERVICE_MASTER", CriteriaServMas, "");
            string ObsvNeeded = "N", ObsvType = "Result", ObsvCode = "";
            if (DSServMas.Tables[0].Rows.Count > 0)
            {
                ObsvNeeded = Convert.ToString(DSServMas.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
                ObsvType = Convert.ToString(DSServMas.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);
                ObsvCode = Convert.ToString(DSServMas.Tables[0].Rows[0]["HSM_OBS_CODE"]);
            }

            string Criteria = " 1=1 ";
            Criteria += " AND LIPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LIPT_INVESTIGATION_PROFILE_ID='" + drpReportType.SelectedValue + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.InvProfileTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestReportDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string UNIT = "", ADDITIONAL_REFERENCE = "", SERV_ID = "";
                    GetTestProfileMasterDtls(Convert.ToString(DR["LIPT_TEST_PROFILE_ID"]), out UNIT, out  ADDITIONAL_REFERENCE, out SERV_ID);

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTRD_TEST_PROFILE_ID"] = Convert.ToString(DR["LIPT_TEST_PROFILE_ID"]);
                    objrow["LTRD_TEST_DESCRIPTION"] = Convert.ToString(DR["LIPT_TEST_DETAILS"]);
                    objrow["LTRD_TEST_UNIT"] = Convert.ToString(DR["LIPT_UNIT"]);
                    objrow["LTRD_INVESTIGATION_PROFILE_ID"] = drpReportType.SelectedValue;
                    objrow["LTRD_SERV_CODE"] = SERV_ID; // txtServCode.Text.Trim();
                    objrow["LTRD_ADDITIONAL_REFERENCE"] = ADDITIONAL_REFERENCE;
                    objrow["LTRD_EXAM_TYPE"] = Convert.ToString(DR["LIPT_EXAM_TYPE"]);

                    objrow["LTRD_OBSV_NEEDED"] = ObsvNeeded;
                    objrow["LTRD_OBSV_TYPE"] = ObsvType;
                    objrow["LTRD_OBSV_CODE"] = ObsvCode;

                    DT.Rows.Add(objrow);

                }

                ViewState["TestReportDtls"] = DT;

            }
        }

        DataSet BindTestProfileTrans(string PROFILE_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTPT_TEST_PROFILE_ID='" + PROFILE_ID + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestProfileTransGet(Criteria);
            return DS;
        }

        String GetRangeNormalValue(string PROFILE_ID, string NORMAL_TYPE)
        {
            String NORMAL_RANGE = "";

            string Criteria = " 1=1 ";
            Criteria += " AND LTPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTPT_TEST_PROFILE_ID='" + PROFILE_ID + "' AND  LTPT_NORMAL_TYPE ='" + NORMAL_TYPE + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestProfileTransGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                NORMAL_RANGE = Convert.ToString(DS.Tables[0].Rows[0]["LTPT_NORMAL_RANGE"]);

            }
            return NORMAL_RANGE;
        }

        void GetTestProfileMasterDtls(string ProfileID, out string UNIT, out string ADDITIONAL_REFERENCE, out string SERV_ID)
        {
            UNIT = "";
            ADDITIONAL_REFERENCE = "";
            SERV_ID = "";
            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND LTPM_STATUS='A' ";

            Criteria += " AND LTPM_TEST_PROFILE_ID  in('" + ProfileID + "')";

            DS = objLab.TestProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                UNIT = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_UNIT"]);
                ADDITIONAL_REFERENCE = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_ADDITIONAL_REFERENCE"]);
                SERV_ID = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_SERV_ID"]);
            }



        }


        string GetRangeAbnormalValue(string ProfileID)
        {
            string strAbrValue = "0";
            string Criteria = " 1=1 ";
            Criteria += " AND LTPT_TEST_PROFILE_ID='" + ProfileID + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestProfileTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Columns.Contains("LTPT_ABNORMAL_RANGE") == true)
                {
                    if (DS.Tables[0].Rows[0].IsNull("LTPT_ABNORMAL_RANGE") == false)
                    {
                        strAbrValue = Convert.ToString(DS.Tables[0].Rows[0]["LTPT_ABNORMAL_RANGE"]);

                    }
                }
            }

            return strAbrValue;
        }



        void Clear()
        {
            ViewState["NewFlag"] = true;
            txtFileNo.Text = "";

            drpStatus.SelectedIndex = 0;
            txtTestSampleID.Text = "";
            txtRequestID.Text = "";

            if (drpReportType.Items.Count > 0)
            {
                drpReportType.SelectedIndex = 0;
            }

            txtServCode.Text = "";
            txtServName.Text = "";
            txtFee.Text = "";
            txtCaption.Text = "";
            drpSource.SelectedIndex = 0;

            if (drpRefDoctor.Items.Count > 0)
            {
                drpRefDoctor.SelectedIndex = 0;
            }
            txtTestName.Text = "";
            txtRemarks.Text = "";


            if (drpDiagnosticService.Items.Count > 0)
            {
                drpDiagnosticService.SelectedIndex = 0;
            }
            drpDiagnosticService.Attributes.CssStyle.Add("border-color", "black");

            if (drpResultStatus.Items.Count > 0)
            {
                drpResultStatus.SelectedIndex = 0;
            }

            if (drpExternalLab.Items.Count > 0)
            {
                drpExternalLab.SelectedIndex = 0;
            }

            if (drpCollectionMethod.Items.Count > 0)
            {
                drpCollectionMethod.SelectedIndex = 0;
            }

            chkMicroorganismIdentified.Checked = false;
            chkCriticalResult .Checked = false;

            drpResultStatus.Attributes.CssStyle.Add("border-color", "black");



            string strDate = "", strTime = "", strAM = "";
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            string strHour = Convert.ToString(System.DateTime.Now.Hour);
            if (Convert.ToInt32(strHour) <= 9)
            {
                strHour = "0" + strHour;
            }

            string strMin = Convert.ToString(System.DateTime.Now.Minute);
            if (Convert.ToInt32(strMin) <= 9)
            {
                strMin = "0" + strMin;
            }

            txtSampleCollDate.Text = strDate;
            txtDeliveryDate.Text = strDate;

            if (drpCollHour.Items.Count > 0)
            {

                for (int i = 0; i < drpCollHour.Items.Count; i++)
                {

                    if (drpCollHour.Items[i].Value == strHour)
                    {
                        drpCollHour.SelectedValue = strHour;
                        drpDelivHour.SelectedValue = strHour;
                        goto HourLoopEnd;
                    }
                }

            HourLoopEnd: ;
            }


            if (drpCollMin.Items.Count > 0)
            {

                for (int i = 0; i < drpCollMin.Items.Count; i++)
                {

                    if (drpCollMin.Items[i].Value == strMin)
                    {
                        drpCollMin.SelectedValue = strMin;
                        drpDelivMin.SelectedValue = strMin;

                        goto MinLoopEnd;
                    }
                }

            MinLoopEnd: ;
            }


            if (drpTransHour.Items.Count > 0)
            {

                for (int i = 0; i < drpTransHour.Items.Count; i++)
                {

                    if (drpTransHour.Items[i].Value == strHour)
                    {
                        drpTransHour.SelectedValue = strHour;

                        goto HourLoopEnd1;
                    }
                }

            HourLoopEnd1: ;
            }


            if (drpTransMin.Items.Count > 0)
            {

                for (int i = 0; i < drpTransMin.Items.Count; i++)
                {

                    if (drpTransMin.Items[i].Value == strMin)
                    {
                        drpTransMin.SelectedValue = strMin;

                        goto MinLoopEnd1;
                    }
                }

            MinLoopEnd1: ;
            }

            txtInvoiceRefNo.Text = "";
            txtEMRID.Text = "";
            txtBarecodeID.Text = "";

            btnSave.Visible = true;

            if (drpReportCategory.Items.Count > 0)
            {
                drpReportCategory.SelectedIndex = 0;
            }




            ViewState["gvServSelectIndex"] = "";


            ViewState["InvoiceID"] = "";
            ViewState["EMRID"] = "";
            ViewState["PatientID"] = "";
            ViewState["DoctorID"] = "";
            ViewState["TransDate"] = "";
            ViewState["TransType"] = "";
            ViewState["ServiceID"] = "";

            BuildeTestReportDtls();
            BindTempTestReportDtls();

            BuildeTestReportMicrobiologyDtls();
            BindTempMicrobiologyDtls();

            lblAuthorizeStatus.Text = "";

            ViewState["EditedRow"] = "";




        }

        void New()
        {
            ViewState["NewFlag"] = true;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");

            txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "LABREPORT");
            txtRemarks.Text = "";
        }

        void GetProfileTransValues(string PROFILE_ID, string strResult, out string NORMAL_TYPE, out string NORMAL_RANGE, out string RANGE_TYPE)
        {

            NORMAL_TYPE = "";
            NORMAL_RANGE = "";
            RANGE_TYPE = "A";


            int vNu;
            Decimal vDec;
            bool isNumeric = int.TryParse(strResult, out vNu);
            bool isDec = Decimal.TryParse(strResult, out vDec);

            ////if (isNumeric == false && isDec == false)
            ////{
            ////    goto FunEnd;
            ////}
            ////else
            ////{
            ////    RANGE_TYPE = "A";
            ////}



            decimal Result = 0;

            //if (strResult != "")
            //{
            //    Result = Convert.ToDecimal(strResult);
            //}

            string Criteria = " 1=1 ";
            Criteria += " AND LTPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTPT_TEST_PROFILE_ID='" + PROFILE_ID + "'";



            if (txtSex.Text.Trim().ToUpper() == "MALE")
            {
                Criteria += " AND ( LTPT_SEX='M' OR LTPT_SEX='B' )";
            }
            else if (txtSex.Text.Trim().ToUpper() == "FEMALE")
            {
                Criteria += " AND ( LTPT_SEX='F' OR LTPT_SEX='B' )";
            }




            Decimal decAge = 0;
            if (txtAge.Text.Trim() != "")
            {
                if (lblAgeType.Text  == "Y")
                {
                    decAge = Convert.ToDecimal(txtAge.Text.Trim());
                }
                else if (lblAgeType.Text == "M")
                {
                    decAge = Convert.ToDecimal(txtAge.Text.Trim());
                    decAge = decAge / 12;
                }
            }
            else if (txtAge1.Text.Trim() != "")
            {

                if (lblAgeType1.Text == "Y")
                {
                    decAge = Convert.ToDecimal(txtAge1.Text.Trim());
                    decAge = decAge / 12;
                }

            }

            Criteria += " AND LTPT_AGE_TO   >= " + decAge + " AND   LTPT_AGE_FROM  <=  " + decAge;

            if (isNumeric == true || isDec == true)
            {

                Criteria += " AND  CAST( LTPT_RANGE_VALUE_TO AS DECIMAL(9,2))   >= '" + strResult + "'  AND   CAST( LTPT_RANGE_VALUE_FROM AS DECIMAL(9,2))  <=  '" + strResult + "'";
            }
            else
            {
                Criteria += " AND  LTPT_RANGE_VALUE_FROM  ='" + strResult + "'";
            }

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestProfileTransGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                NORMAL_TYPE = Convert.ToString(DS.Tables[0].Rows[0]["LTPT_NORMAL_TYPE"]);
                NORMAL_RANGE = Convert.ToString(DS.Tables[0].Rows[0]["LTPT_NORMAL_RANGE"]);
                RANGE_TYPE = Convert.ToString(DS.Tables[0].Rows[0]["LTPT_RANGE_TYPE"]);

            }

        FunEnd: ;

        }

        void GridRowRefresh(Int32 intCurRow)
        {

            Label lblgvTestReportDtlsAddReference = (Label)gvTestReportDtls.Rows[intCurRow].FindControl("lblgvTestReportDtlsAddReference");
            //  DropDownList drpgvTestReportDtlsResult = (DropDownList)gvTestReportDtls.Rows[intCurRow].FindControl("drpgvTestReportDtlsResult");
            TextBox txtgvTestReportDtlsResult = (TextBox)gvTestReportDtls.Rows[intCurRow].FindControl("txtgvTestReportDtlsResult");

            DropDownList drpgvTestReportDtlsRange = (DropDownList)gvTestReportDtls.Rows[intCurRow].FindControl("drpgvTestReportDtlsRange");


            TextBox txtgvTestReportDtlsNorValue = (TextBox)gvTestReportDtls.Rows[intCurRow].FindControl("txtgvTestReportDtlsNorValue");

            TextBox txtgvTestReportDtlsUnit = (TextBox)gvTestReportDtls.Rows[intCurRow].FindControl("txtgvTestReportDtlsUnit");




            DropDownList drpgvTestReportDtlsRangeType = (DropDownList)gvTestReportDtls.Rows[intCurRow].FindControl("drpgvTestReportDtlsRangeType");

            Label lblgvTestReportDtlsProfilID = (Label)gvTestReportDtls.Rows[intCurRow].FindControl("lblgvTestReportDtlsProfilID");

            string NORMAL_TYPE = "", NORMAL_RANGE = "", RANGE_TYPE = "";
            if (txtgvTestReportDtlsResult.Text.Trim() != "")
            {
                GetProfileTransValues(lblgvTestReportDtlsProfilID.Text, txtgvTestReportDtlsResult.Text.Trim(), out NORMAL_TYPE, out NORMAL_RANGE, out RANGE_TYPE);

                if (NORMAL_TYPE != "")
                {
                    drpgvTestReportDtlsRange.SelectedValue = NORMAL_TYPE;
                }

                if (NORMAL_RANGE != "")
                {
                    txtgvTestReportDtlsNorValue.Text = NORMAL_RANGE;
                }

                if (RANGE_TYPE != "")
                {

                    drpgvTestReportDtlsRangeType.SelectedValue = RANGE_TYPE;
                }
            }

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TestReportDtls"];


            DT.Rows[intCurRow]["LTRD_TEST_RESULT"] = txtgvTestReportDtlsResult.Text;
            DT.Rows[intCurRow]["LTRD_RANGE"] = drpgvTestReportDtlsRange.SelectedValue;
            DT.Rows[intCurRow]["LTRD_TEST_NORMALVALUE"] = txtgvTestReportDtlsNorValue.Text.Trim();
            DT.Rows[intCurRow]["LTRD_TEST_UNIT"] = txtgvTestReportDtlsUnit.Text.Trim();
            DT.Rows[intCurRow]["LTRD_ADDITIONAL_REFERENCE"] = lblgvTestReportDtlsAddReference.Text.Trim();
            DT.Rows[intCurRow]["LTRD_RANGE_TYPE"] = drpgvTestReportDtlsRangeType.SelectedValue;

            DT.AcceptChanges();
            ViewState["TestReportDtls"] = DT;
            BindTempTestReportDtls();


        }

        /*
        void BindLabTestDtlsInvoiceWise()
        {
            string strServiceID = "";
            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            string Criteria = " 1=1";
            Criteria += " AND  HIT_LAB_STATUS = 'LAB_SAMPLE_COLLECTED' AND HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceID"]) + "'";



            DS = objInv.InvoiceTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (strServiceID != "")
                    {
                        strServiceID += ",'" + Convert.ToString(DR["HIT_SERV_CODE"]) + "'";
                    }
                    else
                    {
                        strServiceID = "'" + Convert.ToString(DR["HIT_SERV_CODE"]) + "'";
                    }
                }
            }

            if (strServiceID != "")
            {
                BindLabTestDtlsServiceWise(strServiceID);
            }


        }

        */
        void BindLabTestDtlsServiceWise(string strServiceID)
        {
            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND LTPM_STATUS='A' ";

            if (Convert.ToString(ViewState["TransType"]).ToUpper() == "INVOICE")
            {
                Criteria += " AND LTPM_SERV_ID  in(" + strServiceID + ")";
            }
            else
            {
                Criteria += " AND LTPM_CPT_CODE  in(" + strServiceID + ")";
            }
            DS = objLab.TestProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestReportDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTRD_TEST_PROFILE_ID"] = Convert.ToString(DR["LTPM_TEST_PROFILE_ID"]);
                    objrow["LTRD_TEST_DESCRIPTION"] = Convert.ToString(DR["LTPM_DESCRIPTION"]);
                    objrow["LTRD_TEST_UNIT"] = Convert.ToString(DR["LTPM_UNIT"]);
                    objrow["LTRD_ADDITIONAL_REFERENCE"] = Convert.ToString(DR["LTPM_ADDITIONAL_REFERENCE"]);
                    DT.Rows.Add(objrow);

                }

                ViewState["TestReportDtls"] = DT;
            }

        }

        Boolean CheckPassword(string UserID, string Password)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        Boolean fnSave()
        {
            LogFileWriting(System.DateTime.Now.ToString() + " fnSave() Start ");
            string FieldNameWithValues = "", Criteria = "";
            Boolean isError = false;


            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter File No";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }


            if (txtTransDate.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Trans Date";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }

            if (Convert.ToString(Session["LAB_TEST_REPORT_WITHOUT_SAMPLE_COLLECTION"]) == "0")
            {
                if (txtTestSampleID.Text.Trim() == "")
                {
                    lblStatus.Text = "Sample ID is empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }

                if (txtSampleCollDate.Text.Trim() == "")
                {
                    lblStatus.Text = "Sample Collection Date is empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }

                if (txtDeliveryDate.Text.Trim() == "")
                {
                    lblStatus.Text = "Sample Delivery Date is empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }



                txtServCode.Attributes.CssStyle.Add("border-color", "black");
                if (txtServCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Service Code";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtServCode.Attributes.CssStyle.Add("border-color", "red");
                    isError = true;
                    goto FunEnd;
                }

                txtServName.Attributes.CssStyle.Add("border-color", "black");
                if (txtServName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Service Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtServName.Attributes.CssStyle.Add("border-color", "red");
                    isError = true;
                    goto FunEnd;
                }

            }





            if (Convert.ToString(Session["REQ_INVOICE_NO_LAB_TESTREPORT"]) == "1")
            {
                if (txtInvoiceRefNo.Text == "")
                {
                    lblStatus.Text = "Please Enter Invoice Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }

                DataSet DS = new DataSet();
                objCom = new CommonBAL();
                string Criteria1 = " 1=1  ";
                Criteria1 += " AND HIM_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "' AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue("HIM_INVOICE_ID", "HMS_INVOICE_MASTER", Criteria1, "");

                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = "Please Enter correct Invoice Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }
            }

            if (gvTestReportDtls.Rows.Count == 0)
            {
                lblStatus.Text = "Please Enter Lab Test Details";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }

            if (chkMicroorganismIdentified.Checked == true)
            {
                if (gvMicrobiologyDtls.Rows.Count == 0)
                {
                    lblStatus.Text = "Please Enter Microorganism Test Details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }
            }




            if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
            {

                if (drpDoctors.Text.Trim() == "")
                {
                    lblStatus.Text = "Doctor Code is empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }

                if (drpRefDoctor.Text.Trim() == "")
                {
                    lblStatus.Text = "Ref. Doctor Code is empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }


                txtEMRID.Attributes.CssStyle.Add("border-color", "black");
                if (txtEMRID.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter EMR ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtEMRID.Attributes.CssStyle.Add("border-color", "red");
                    isError = true;
                    goto FunEnd;
                }


                drpCollectionMethod.Attributes.CssStyle.Add("border-color", "black");
                if (drpCollectionMethod.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please select Collection Method";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    drpCollectionMethod.Attributes.CssStyle.Add("border-color", "red");
                    isError = true;
                    goto FunEnd;
                }


                drpDiagnosticService.Attributes.CssStyle.Add("border-color", "black");
                if (drpDiagnosticService.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please select Diagnostic Service";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    drpDiagnosticService.Attributes.CssStyle.Add("border-color", "red");
                    isError = true;
                    goto FunEnd;
                }

                drpResultStatus.Attributes.CssStyle.Add("border-color", "black");
                if (drpResultStatus.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please select Result Status";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                    isError = true;
                    goto FunEnd;
                }

                if (drpResultStatus.SelectedValue == "C" || drpResultStatus.SelectedValue == "X")
                {
                    string Criteria2 = " 1=1 ";
                    Criteria2 += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria2 += " AND LTRM_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";

                    DataSet DS = new DataSet();
                    CommonBAL objCom1 = new CommonBAL();
                    DS = objCom1.fnGetFieldValue(" TOP 1  *", "LAB_TEST_REPORT_MASTER", Criteria2, "LTRM_CREATED_DATE desc ");


                    if (DS.Tables[0].Rows.Count <= 0)
                    {

                        lblStatus.Text = drpResultStatus.SelectedItem.Text + " - only for update the Test Details";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                        isError = true;
                        goto FunEnd;
                    }

                }

                LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 1 ");

                if (drpDoctors.Text.Trim() != "")
                {

                    string DR_ID = drpDoctors.SelectedValue;

                    string DrFirstName, DrMiddleName, LicenseNo, SpecialityName;
                    GetStaffDetails(DR_ID, out   DrFirstName, out   DrMiddleName, out   LicenseNo, out SpecialityName);


                    if (LicenseNo == "")
                    {
                        lblStatus.Text = "Doctor License No is empty, ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;

                    }

                    if (DrFirstName == "")
                    {

                        lblStatus.Text = "Doctor Name is empty ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;
                    }

                    if (SpecialityName == "")
                    {

                        lblStatus.Text = "Speciality Name is empty ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;
                    }


                }

                LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 2 ");



                if (drpRefDoctor.Text.Trim() == "")
                {


                    string RefDR_ID = drpRefDoctor.Text.Trim();

                    string RefDrFirstName, RefDrMiddleName, RefLicenseNo, RefSpecialityName;
                    GetStaffDetails(RefDR_ID, out   RefDrFirstName, out   RefDrMiddleName, out   RefLicenseNo, out RefSpecialityName);

                    if (RefLicenseNo == "")
                    {

                        lblStatus.Text = "Ref Doctor License No is empty ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;
                    }

                    if (RefDrFirstName == "")
                    {

                        lblStatus.Text = "Ref Doctor Name is empty ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;
                    }


                    if (RefSpecialityName == "")
                    {

                        lblStatus.Text = "Ref Speciality Name is empty ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;
                    }


                    string ErrorMessagePTDtls = "";
                    bool IsNoErrorPTDtls = CheckPatientDetails(txtFileNo.Text.Trim(), out ErrorMessagePTDtls);


                    if (ValidateNL7Message(txtFileNo.Text.Trim()) == false)
                    {
                        lblStatus.Text = ErrorMessagePTDtls;
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        isError = true;
                        goto FunEnd;
                    }

                }
                LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 3 ");

                if (txtSampleCollDate.Text.Trim() != "" && txtDeliveryDate.Text.Trim() != "" && txtTransDate.Text.Trim() != "")
                {
                    LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 4 ");

                    DateTime dtSampleCollDate = DateTime.ParseExact(txtSampleCollDate.Text.Trim() + " " + drpCollHour.SelectedValue + ":" + drpCollMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

                    LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 5 ");

                    DateTime dtDeliveryDate = DateTime.ParseExact(txtDeliveryDate.Text.Trim() + " " + drpDelivHour.SelectedValue + ":" + drpDelivMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

                    LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 6 ");

                    txtDeliveryDate.Attributes.CssStyle.Add("border-color", "black");
                    if (DateTime.Compare(dtDeliveryDate, dtSampleCollDate) == -1)
                    {

                        lblStatus.Text = "Delivery date Should grater than Sample Coll. Date";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtDeliveryDate.Attributes.CssStyle.Add("border-color", "red");
                        isError = true;
                        goto FunEnd;
                    }

                    LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 7 ");

                    DateTime dtTransDate = DateTime.ParseExact(txtTransDate.Text.Trim() + " " + drpTransHour.SelectedValue + ":" + drpTransMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);


                    LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 8 ");
                    txtTransDate.Attributes.CssStyle.Add("border-color", "black");
                    if (DateTime.Compare(dtTransDate, dtDeliveryDate) == -1)
                    {

                        lblStatus.Text = "Trans date Should grater than Delivery Date";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtTransDate.Attributes.CssStyle.Add("border-color", "red");
                        isError = true;
                        goto FunEnd;
                    }





                }








                LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 9 ");








                if (chkMicroorganismIdentified.Checked == false)
                {
                    foreach (GridViewRow DR in gvTestReportDtls.Rows)
                    {
                        Label lblgvTestReportDtlsInvProfilID = (Label)DR.FindControl("lblgvTestReportDtlsInvProfilID");
                        Label lblgvTestReportDtlsProfilID = (Label)DR.FindControl("lblgvTestReportDtlsProfilID");
                        Label lblgvTestReportDtlsDesc = (Label)DR.FindControl("lblgvTestReportDtlsDesc");

                        TextBox txtgvTestReportDtlsResult = (TextBox)DR.FindControl("txtgvTestReportDtlsResult");
                        DropDownList drpgvTestReportDtlsRange = (DropDownList)DR.FindControl("drpgvTestReportDtlsRange");
                        TextBox txtgvTestReportDtlsNorValue = (TextBox)DR.FindControl("txtgvTestReportDtlsNorValue");
                        TextBox txtgvTestReportDtlsUnit = (TextBox)DR.FindControl("txtgvTestReportDtlsUnit");

                        DropDownList drpgvTestReportDtlsRangeType = (DropDownList)DR.FindControl("drpgvTestReportDtlsRangeType");


                        if (txtgvTestReportDtlsResult.Text.Trim() == "" || txtgvTestReportDtlsUnit.Text.Trim() == "")
                        {
                            lblStatus.Text = "Please Enter Lab Test Details";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            isError = true;
                            goto FunEnd;
                        }

                        if (drpgvTestReportDtlsRangeType.SelectedIndex == 0)
                        {
                            lblStatus.Text = "Please Enter Abnormal Flag";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            isError = true;
                            goto FunEnd;
                        }

                    }
                }
            }
            LogFileWriting(System.DateTime.Now.ToString() + " fnSave() 10 ");

            objLab = new LaboratoryBAL();
            objLab.LTRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objLab.LTRM_TEST_REPORT_ID = txtTransNo.Text.Trim();
            objLab.LTRM_TEST_DATE = txtTransDate.Text.Trim() + " " + drpTransHour.SelectedValue + ":" + drpTransMin.SelectedValue + ":00";
            objLab.LTRM_SAMPLE_ID = txtTestSampleID.Text.Trim();

            if (drpResultStatus.SelectedValue == "X")
            {
                objLab.LTRM_STATUS = "Cancelled";
            }
            else
            {
                objLab.LTRM_STATUS = drpStatus.SelectedValue;
            }

            objLab.LTRM_PATIENT_ID = txtFileNo.Text.Trim();
            objLab.LTRM_PATIENT_NAME = txtFName.Text.Trim();
            objLab.LTRM_PATIENT_AGE = txtAge.Text.Trim();
            objLab.LTRM_PATIENT_AGETYPE = lblAgeType.Text.Trim();
            objLab.LTRM_PATIENT_SEX = txtSex.Text.Trim();
            objLab.LTRM_PATIENT_NATIONALITY = drpNationality.SelectedValue;
            objLab.LTRM_RANGE = "";
            objLab.LTRM_INVESTIGATION_PROFILE_ID = drpReportType.SelectedValue;
            objLab.LTRM_INVESTIGATION_DESCRIPTION = drpReportType.Text;
            objLab.LTRM_REF_DR = drpRefDoctor.SelectedValue;
            objLab.LTRM_DR_CODE = drpDoctors.SelectedValue;
            objLab.LTRM_DR_NAME = drpDoctors.SelectedItem.Text;
            objLab.LTRM_SERV_ID = txtServCode.Text.Trim();
            objLab.LTRM_SERV_DESCRIPTION = txtServName.Text.Trim();
            objLab.LTRM_FEE = txtFee.Text.Trim();
            objLab.LTRM_CAPTION = txtCaption.Text.Trim();
            objLab.LTRM_REMARKS = txtRemarks.Text.Trim();
            objLab.LTRM_REFERENCE = "";
            objLab.LTRM_REF_TYPE = "";
            objLab.LTRM_REPORT_TYPE = "";// drpReportCategory.SelectedValue;
            objLab.LTRM_COMP_ID = txtProviderID.Text.Trim();
            objLab.LTRM_COMP_NAME = txtProviderName.Text.Trim();
            objLab.LTRM_POLICY_NO = txtPolicyNo.Text.Trim();
            objLab.LTRM_PT_COMPANY =
            objLab.LTRM_REPORT_SOURCE = drpSource.SelectedValue;
            objLab.LTRM_REPORT_SOURCENAME = txtSourceName.Text.Trim();
            objLab.LTRM_ADMN = txtAdminID.Text.Trim();
            objLab.LTRM_WARD = txtWard.Text.Trim();
            objLab.LTRM_ROOM = txtRoom.Text.Trim();
            objLab.LTRM_BED = txtBed.Text.Trim();
            objLab.LTRM_INV_TYPE = drpInvType.SelectedValue;
            objLab.LTRM_PT_TYPE = drpPTType.SelectedValue;
            objLab.LTRM_METHOD = "";
            objLab.LTRM_Ref_Hospital = "";
            objLab.LTRM_INVOICE_ID = txtInvoiceRefNo.Text.Trim();
            objLab.LTRM_ALERTED = "";
            objLab.LTRM_COLLECTION_TIME = drpCollHour.SelectedValue + ":" + drpCollMin.SelectedValue + ":00";
            objLab.LTRM_INVOICENO = "";
            objLab.BARECODE = txtBarecodeID.Text.Trim();
            objLab.EMR_ID = txtEMRID.Text.Trim();
            objLab.LTRM_COLLECTION_DATE = txtSampleCollDate.Text.Trim() + " " + drpCollHour.SelectedValue + ":" + drpCollMin.SelectedValue + ":00";
            objLab.LTRM_DELIVERY_DATE = txtDeliveryDate.Text.Trim() + " " + drpDelivHour.SelectedValue + ":" + drpDelivMin.SelectedValue + ":00";
            objLab.LTRM_DIAGNOSTIC_SERVICE = drpDiagnosticService.SelectedValue;
            objLab.LTRM_RESULT_STATUS = drpResultStatus.SelectedValue;

            objLab.LTRM_EXTERNAL_LAB_NAME = drpExternalLab.SelectedValue;
            objLab.LTRM_EXTERNAL_LAB_REF_NO = txtExtLabReferenceNo.Text;

            objLab.LTRM_MICROORGANISM_IDENTIFIED = Convert.ToString(chkMicroorganismIdentified.Checked);

            if (drpCollectionMethod.SelectedIndex != 0)
            {
                objLab.COLLECTION_METHOD_CODE = drpCollectionMethod.SelectedValue;
                objLab.COLLECTION_METHOD_NAME = drpCollectionMethod.SelectedItem.Text;
            }
            else
            {
                objLab.COLLECTION_METHOD_CODE = "";
                objLab.COLLECTION_METHOD_NAME = "";
            }

            objLab.LTRM_CRITICAL_RESULT = Convert.ToString(chkCriticalResult.Checked);

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }

            objLab.SCRN_ID = "LABREPORT";

            objLab.UserID = Convert.ToString(Session["User_ID"]);
            string REPORT_ID = "";
            REPORT_ID = objLab.TestReportMasterAdd();
            txtTransNo.Text = REPORT_ID;


            objCom = new CommonBAL();
            Criteria = " LTRD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRD_TEST_REPORT_ID='" + txtTransNo.Text + "'";
            objCom.fnDeleteTableData("LAB_TEST_REPORT_DETAIL", Criteria);


            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TestReportDtls"];

            Int32 intCurRow = 1;

            /** 
                     foreach (DataRow DR in DT.Rows)
                     {

                         objLab.LTRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                         objLab.LTRM_TEST_REPORT_ID = txtTransNo.Text.Trim();
                         objLab.LTRD_TEST_PROFILE_ID = Convert.ToString(DR["LTRD_TEST_PROFILE_ID"]);
                         objLab.LTRD_TEST_DESCRIPTION = Convert.ToString(DR["LTRD_TEST_DESCRIPTION"]);
                         objLab.LTRD_TEST_RESULT = Convert.ToString(DR["LTRD_TEST_RESULT"]);
                         objLab.LTRD_RANGE = Convert.ToString(DR["LTRD_RANGE"]);
                         objLab.LTRD_TEST_NORMALVALUE = Convert.ToString(DR["LTRD_TEST_NORMALVALUE"]);
                         objLab.LTRD_TEST_UNIT = Convert.ToString(DR["LTRD_TEST_UNIT"]);
                         objLab.LTRD_SLNO = Convert.ToString(intCurRow);
                         objLab.LTRD_TEST_CATEGORY = drpReportCategory.SelectedValue;
                         objLab.LTRD_METHOD = "";
                         objLab.LTRD_SERV_CODE = txtServCode.Text.Trim();
                         objLab.LTRD_INVESTIGATION_PROFILE_ID = Convert.ToString(DR["LTRD_INVESTIGATION_PROFILE_ID"]);
                         objLab.LTRD_ADDITIONAL_REFERENCE = Convert.ToString(DR["LTRD_ADDITIONAL_REFERENCE"]);
                         objLab.LTRD_EXAM_TYPE = Convert.ToString(DR["LTRD_EXAM_TYPE"]);
                         objLab.UserID = Convert.ToString(Session["User_ID"]);
                         objLab.TestReportDtlsAdd();


                         string strTestType = "";

                         strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

                         if (strTestType == "EMR")
                         {
                             FieldNameWithValues = " Completed='LAB_TEST_COMPLETED' , EPL_DATE=GETDATE() ";
                             string Criteria1 = "  EPL_ID ='" + Convert.ToString(ViewState["EMRID"]) + "' AND EPL_LAB_CODE='" + Convert.ToString(DR["LTRD_SERV_CODE"]) + "'";
                             objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                         }
                         else
                         {

                             FieldNameWithValues = " HIT_LAB_STATUS='LAB_TEST_COMPLETED'";
                             string Criteria1 = " HIT_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "' AND HIT_SERV_CODE='" + Convert.ToString(DR["LTRD_SERV_CODE"]) + "'";

                             objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);
                         }

                         intCurRow = intCurRow + 1;
                     }

                      * 
                      * */


            foreach (GridViewRow DR in gvTestReportDtls.Rows)
            {
                Label lblgvTestReportDtlsInvProfilID = (Label)DR.FindControl("lblgvTestReportDtlsInvProfilID");
                Label lblgvTestReportDtlsProfilID = (Label)DR.FindControl("lblgvTestReportDtlsProfilID");
                Label lblgvTestReportDtlsDesc = (Label)DR.FindControl("lblgvTestReportDtlsDesc");

                Label lblObsvNeeded = (Label)DR.FindControl("lblObsvNeeded");
                Label lblObsvType = (Label)DR.FindControl("lblObsvType");
                Label lblObsvCode = (Label)DR.FindControl("lblObsvCode");



                TextBox txtgvTestReportDtlsResult = (TextBox)DR.FindControl("txtgvTestReportDtlsResult");
                DropDownList drpgvTestReportDtlsRange = (DropDownList)DR.FindControl("drpgvTestReportDtlsRange");
                TextBox txtgvTestReportDtlsNorValue = (TextBox)DR.FindControl("txtgvTestReportDtlsNorValue");
                TextBox txtgvTestReportDtlsUnit = (TextBox)DR.FindControl("txtgvTestReportDtlsUnit");

                DropDownList drpgvTestReportDtlsRangeType = (DropDownList)DR.FindControl("drpgvTestReportDtlsRangeType");

                string UNIT = "", ADDITIONAL_REFERENCE = "", SERV_ID = "";
                GetTestProfileMasterDtls(Convert.ToString(lblgvTestReportDtlsProfilID.Text), out UNIT, out  ADDITIONAL_REFERENCE, out SERV_ID);

                objLab.LTRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objLab.LTRM_TEST_REPORT_ID = txtTransNo.Text.Trim();
                objLab.LTRD_TEST_PROFILE_ID = lblgvTestReportDtlsProfilID.Text;
                objLab.LTRD_TEST_DESCRIPTION = lblgvTestReportDtlsDesc.Text;
                objLab.LTRD_TEST_RESULT = txtgvTestReportDtlsResult.Text;
                objLab.LTRD_RANGE = drpgvTestReportDtlsRange.SelectedValue;
                objLab.LTRD_TEST_NORMALVALUE = txtgvTestReportDtlsNorValue.Text;
                objLab.LTRD_TEST_UNIT = txtgvTestReportDtlsUnit.Text;
                objLab.LTRD_SLNO = Convert.ToString(intCurRow);
                objLab.LTRD_TEST_CATEGORY = drpReportCategory.SelectedValue;
                objLab.LTRD_METHOD = "";
                objLab.LTRD_SERV_CODE = SERV_ID;// txtServCode.Text.Trim();
                objLab.LTRD_INVESTIGATION_PROFILE_ID = lblgvTestReportDtlsInvProfilID.Text;// Convert.ToString(DR["LTRD_INVESTIGATION_PROFILE_ID"]);

                if (txtSex.Text.ToLower() == "male" && lblgvTestReportDtlsProfilID.Text == "84443")//"7099"
                {
                    objLab.LTRD_ADDITIONAL_REFERENCE = "";
                }
                else
                {
                    objLab.LTRD_ADDITIONAL_REFERENCE = ADDITIONAL_REFERENCE;
                }
                objLab.LTRD_EXAM_TYPE = "";// Convert.ToString(DR["LTRD_EXAM_TYPE"]);
                objLab.LTRD_RANGE_TYPE = drpgvTestReportDtlsRangeType.SelectedValue;


                objLab.LTRD_OBSV_NEEDED = lblObsvNeeded.Text;
                objLab.LTRD_OBSV_TYPE = lblObsvType.Text;
                objLab.LTRD_OBSV_CODE = lblObsvCode.Text;


                objLab.UserID = Convert.ToString(Session["User_ID"]);
                objLab.TestReportDtlsAdd();

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    if (Convert.ToString(Session["LAB_RESULT_DATAUPLOAD"]) == "1")
                    {
                        FieldNameWithValues = "LTRM_VERIFIED_BY='" + Convert.ToString(Session["User_ID"]) + "',LTRM_VERIFIED_DATE=GETDATE()";
                        string Criteria1 = " LTRM_TEST_REPORT_ID ='" + txtTransNo.Text.Trim() + "'";

                        objCom.fnUpdateTableData(FieldNameWithValues, "LAB_TEST_REPORT_MASTER", Criteria1);
                    }

                }
                string strTestType = "";

                strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

                if (strTestType == "EMR")
                {
                    FieldNameWithValues = " Completed='LAB_TEST_COMPLETED' , EPL_DATE=GETDATE() ";
                    string Criteria1 = "  EPL_ID ='" + Convert.ToString(ViewState["EMRID"]) + "' AND EPL_LAB_CODE='" + txtServCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                }
                else
                {

                    FieldNameWithValues = " HIT_LAB_STATUS='LAB_TEST_COMPLETED'";
                    string Criteria1 = " HIT_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "' AND HIT_SERV_CODE='" + txtServCode.Text.Trim() + "'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);
                }

                intCurRow = intCurRow + 1;
            }


            objCom = new CommonBAL();
            Criteria = " LTMD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTMD_TEST_REPORT_ID='" + txtTransNo.Text + "'";
            objCom.fnDeleteTableData("LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria);


            Int32 intMicroCurRow = 1;

            foreach (GridViewRow DR in gvMicrobiologyDtls.Rows)
            {
                Label lblgvMicroDtlsOrganismTypeCode = (Label)DR.FindControl("lblgvMicroDtlsOrganismTypeCode");
                Label lblgvMicroDtlsOrganismTypeName = (Label)DR.FindControl("lblgvMicroDtlsOrganismTypeName");

                Label lblgvMicroDtlsAntibioticsTypeCode = (Label)DR.FindControl("lblgvMicroDtlsAntibioticsTypeCode");
                Label lblgvMicroDtlsAntibioticsTypeName = (Label)DR.FindControl("lblgvMicroDtlsAntibioticsTypeName");

                Label lblgvMicroDtlsAntibioticsMethod = (Label)DR.FindControl("lblgvMicroDtlsAntibioticsMethod");
                Label lblgvMicroDtlsAntibioticsUnit = (Label)DR.FindControl("lblgvMicroDtlsAntibioticsUnit");
                Label lblgvMicroDtlsAntibioticsValue = (Label)DR.FindControl("lblgvMicroDtlsAntibioticsValue");

                Label lblgvMicroDtlAntibioticsAbnormalFlag = (Label)DR.FindControl("lblgvMicroDtlAntibioticsAbnormalFlag");
                Label lblgvMicroDtlAntibioticsAbnormalFlagName = (Label)DR.FindControl("lblgvMicroDtlAntibioticsAbnormalFlagName");
                Label lnkgvMicroDtlsStatus = (Label)DR.FindControl("lnkgvMicroDtlsStatus");

                objLab.BranchID = Convert.ToString(Session["Branch_ID"]);
                objLab.TEST_REPORT_ID = txtTransNo.Text.Trim();

                objLab.LTMD_SLNO = Convert.ToString(intMicroCurRow);
                objLab.LTMD_ORGANISM_TYPE_CODE = lblgvMicroDtlsOrganismTypeCode.Text;
                objLab.LTMD_ORGANISM_TYPE_NAME = lblgvMicroDtlsOrganismTypeName.Text;
                objLab.LTMD_ANTIBIOTICS_TYPE_CODE = lblgvMicroDtlsAntibioticsTypeCode.Text;
                objLab.LTMD_ANTIBIOTICS_TYPE_NAME = lblgvMicroDtlsAntibioticsTypeName.Text;
                objLab.LTMD_ANTIBIOTICS_METHOD = lblgvMicroDtlsAntibioticsMethod.Text;
                objLab.LTMD_ANTIBIOTICS_UNIT = lblgvMicroDtlsAntibioticsUnit.Text;
                objLab.LTMD_ANTIBIOTICS_VALUE = lblgvMicroDtlsAntibioticsValue.Text;

                objLab.LTMD_ABNORMAL_FLAG = lblgvMicroDtlAntibioticsAbnormalFlag.Text;
                objLab.LTMD_ABNORMAL_FLAG_NAME = lblgvMicroDtlAntibioticsAbnormalFlagName.Text;
                objLab.LTMD_STATUS = lnkgvMicroDtlsStatus.Text;



                objLab.TestReportMicrobiologyDtlsAdd();


                ++intMicroCurRow;
            }




            if (txtTestSampleID.Text.Trim() != "")
            {

                objCom = new CommonBAL();
                FieldNameWithValues = "LTSD_TEST_RESULT='Completed' , LTSD_STATUS='Completed'";
                Criteria = " LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "' AND LTSD_TEST_PROFILE_ID='" + txtServCode.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "LAB_TEST_SAMPLE_DETAIL", Criteria);

            }


        FunEnd: ;
            return isError;
        }

        void BindInvoice()
        {
            string Criteria = " 1=1  ";

            Criteria += " AND HIM_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "'";


            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            DS = objInv.InvoiceMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                string strRefDr = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_CODE"]);
                for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                {
                    if (drpRefDoctor.Items[intCount].Value == strRefDr)
                    {
                        drpRefDoctor.SelectedValue = strRefDr;
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;
                drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]) == "CA" ? "Cash" : "Credit";
                drpPTType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_TYPE"]);


            }

        }

        void BindEMRDtls()
        {
            string Criteria = "EPM_ID='" + txtEMRID.Text.Trim() + "'";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DataSet DS = new DataSet();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtProviderID.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_INS_CODE"]);
                txtProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_INS_NAME"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_POLICY_NO"]);
                txtIDNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID_NO"]);
            }
        }

        void BuildServiceList()
        {
            DataTable dt = new DataTable();

            DataColumn ID = new DataColumn();
            ID.ColumnName = "ID";

            DataColumn ServiceID = new DataColumn();
            ServiceID.ColumnName = "ServiceID";

            DataColumn Description = new DataColumn();
            Description.ColumnName = "Description";

            DataColumn TestResult = new DataColumn();
            TestResult.ColumnName = "TestResult";

            DataColumn CollectionMethod = new DataColumn();
            CollectionMethod.ColumnName = "CollectionMethod";

            dt.Columns.Add(ID);
            dt.Columns.Add(ServiceID);
            dt.Columns.Add(Description);
            dt.Columns.Add(TestResult);
            dt.Columns.Add(CollectionMethod);


            ViewState["ServiceList"] = dt;
        }

        /*
        void BindServiceList()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED' ";
            Criteria += " AND HIT_SERV_TYPE='L'  ";

            Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceRefNo.Text + "'";

            dboperations dbo = new dboperations();
            clsInvoice objInv = new clsInvoice();
            DataSet DS = new DataSet();
            DS = objInv.InvoiceTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["ServiceList"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["ID"] = Convert.ToString(ViewState["InvoiceID"]);
                    objrow["ServiceID"] = Convert.ToString(DR["HIT_SERV_CODE"]);
                    objrow["Description"] = Convert.ToString(DR["HIT_DESCRIPTION"]);
                    objrow["TestResult"] = "";

                    DT.Rows.Add(objrow);

                }

                ViewState["ServiceList"] = DT;

            }
        }

        */
        void BindTestSampleList()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = "  ( ISNULL(LTSD_STATUS,'') = 'Open' OR  ISNULL(LTSD_STATUS,'') = '' OR   LTSD_STATUS  IS NULL ) ";
            Criteria += " AND LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";



            DS = objLab.TestSampleDtlsGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["ServiceList"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["ID"] = Convert.ToString(ViewState["InvoiceID"]);
                    objrow["ServiceID"] = Convert.ToString(DR["LTSD_TEST_PROFILE_ID"]);
                    objrow["Description"] = Convert.ToString(DR["LTSD_TEST_DESCRIPTION"]);
                    objrow["CollectionMethod"] = Convert.ToString(DR["LTSD_COLLECTION_METHOD_CODE"]);


                    DT.Rows.Add(objrow);

                }

                ViewState["ServiceList"] = DT;

            }
        }



        void BindTempServiceList()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ServiceList"];
            if (DT.Rows.Count > 0)
            {
                gvServiceList.DataSource = DT;
                gvServiceList.DataBind();

            }
            else
            {
                gvServiceList.DataBind();
            }

        }

        void BindServiceMasterName()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='L' ";


            if (txtServCode.Text.Trim() != "")
            {
                Criteria += " AND HSM_SERV_ID = '" + txtServCode.Text.Trim() + "'";
            }


            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
            }

        }

        void BindInvProfileMaster()
        {
            txtCaption.Text = "";
            drpDiagnosticService.SelectedIndex = 0;

            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LIPM_INVESTIGATION_PROFILE_ID='" + drpReportType.SelectedValue + "'";


            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtCaption.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_CAPTION"]);


                if (DS.Tables[0].Rows[0].IsNull("LIPM_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_DIAGNOSTIC_SERVICE"]) != "")
                {
                    drpDiagnosticService.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_DIAGNOSTIC_SERVICE"]);
                }
            }

        }

        void BindTime12HrsFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");


            drpCollHour.DataSource = DSHours;
            drpCollHour.DataTextField = "Name";
            drpCollHour.DataValueField = "Code";
            drpCollHour.DataBind();


            drpDelivHour.DataSource = DSHours;
            drpDelivHour.DataTextField = "Name";
            drpDelivHour.DataValueField = "Code";
            drpDelivHour.DataBind();

            drpTransHour.DataSource = DSHours;
            drpTransHour.DataTextField = "Name";
            drpTransHour.DataValueField = "Code";
            drpTransHour.DataBind();






            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpCollMin.DataSource = DSMin;
            drpCollMin.DataTextField = "Name";
            drpCollMin.DataValueField = "Code";
            drpCollMin.DataBind();

            drpDelivMin.DataSource = DSMin;
            drpDelivMin.DataTextField = "Name";
            drpDelivMin.DataValueField = "Code";
            drpDelivMin.DataBind();

            drpTransMin.DataSource = DSMin;
            drpTransMin.DataTextField = "Name";
            drpTransMin.DataValueField = "Code";
            drpTransMin.DataBind();


        }




        void BuildeTestReportMicrobiologyDtls()
        {
            string Criteria = "1=2";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria, "LTMD_ORGANISM_TYPE_NAME,LTMD_ANTIBIOTICS_TYPE_NAME");

            ViewState["TestReportMicrobiologyDtls"] = DS.Tables[0];
        }

        void BindTestReportMicrobiologyDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTMD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTMD_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria, "LTMD_ORGANISM_TYPE_NAME,LTMD_ANTIBIOTICS_TYPE_NAME");
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["TestReportMicrobiologyDtls"] = DS.Tables[0];

            }
        }

        void BindTempMicrobiologyDtls()
        {

            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["TestReportMicrobiologyDtls"];
            DataView DV = new DataView();
            DV.Table = DT;
            DV.Sort = "LTMD_ORGANISM_TYPE_NAME,LTMD_ANTIBIOTICS_TYPE_NAME";
            //SORTING CODING - END
            if (DT.Rows.Count > 0)
            {
                gvMicrobiologyDtls.DataSource = DV;
                gvMicrobiologyDtls.DataBind();

                // ViewState["EditedRow"] = "";

                if (lblAuthorizeStatus.Text == "Authorized")
                {
                    gvMicrobiologyDtls.Columns[6].Visible = false;
                }
                else
                {
                    gvMicrobiologyDtls.Columns[6].Visible = true;
                }
            }
            else
            {
                gvMicrobiologyDtls.DataBind();
            }

        }




        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "LAB_RESULT";
            objCom.ScreenName = "Lab Test Report";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_RESULT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnAuthorize.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                btnAuthorize.Visible = false;

            }
            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }

        #endregion


        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetTestProfileMaster(string prefixText)
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string[] Data;

            string Criteria = " 1=1 ";
            Criteria += " AND LTPM_STATUS='A' ";

            Criteria += " AND ( LTPM_TEST_PROFILE_ID LIKE '%" + prefixText + "%' OR LTPM_DESCRIPTION LIKE'%" + prefixText + "%' )";

            ds = obLab.TestProfileMasterGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["LTPM_TEST_PROFILE_ID"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["LTPM_DESCRIPTION"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND ( HRDM_REF_ID Like '%" + prefixText + "%' OR HRDM_FNAME + ' ' +isnull(HRDM_MNAME,'') + ' '  + isnull(HRDM_LNAME,'')   like '%" + prefixText + "%' ) ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.RefDoctorMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HRDM_REF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            lblAntibioticsValidation.Text = "";
            if (!IsPostBack)
            {

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Lab Test Report Page");

                try
                {
                    ViewState["NewFlag"] = true;

                    string strDate = "", strTime = "", strAM = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm");
                    strAM = objCom.fnGetDate("tt");

                    string strHour = Convert.ToString(System.DateTime.Now.Hour);
                    if (Convert.ToInt32(strHour) <= 9)
                    {
                        strHour = "0" + strHour;
                    }
                    string strMin = Convert.ToString(System.DateTime.Now.Minute);
                    if (Convert.ToInt32(strMin) <= 9)
                    {
                        strMin = "0" + strMin;
                    }

                    BindTime12HrsFromDB();

                    txtTransDate.Text = strDate;
                    txtSrcFromDate.Text = strDate;
                    txtSrcToDate.Text = strDate;


                    drpTransHour.SelectedValue = strHour;
                    drpTransMin.SelectedValue = strMin;


                    //if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    //{
                    //    drpDoctors.Enabled = false;
                    //    drpRefDoctor.Enabled = false;
                    //}

                    txtSampleCollDate.Text = strDate;
                    drpCollHour.SelectedValue = strHour;
                    drpCollMin.SelectedValue = strMin;

                    txtDeliveryDate.Text = strDate;
                    drpDelivHour.SelectedValue = strHour;
                    drpDelivMin.SelectedValue = strMin;


                    txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "LABREPORT");

                    BindUsers();
                    BindReportType();

                    BindNationality();
                    BindDoctor();
                    BinRefdDoctor();

                    BindReportCategory();


                    BindCommonMaster("LabDiagnosticService");
                    BindCommonMaster("ResultStatus");
                    BindCommonMaster("DiagnosticCenter");
                    BindCommonMaster("OrganismType");
                    BindCommonMaster("SpecimenCollectionMethod");

                    BindAntibioticsType();

                    BindTestSampleMasterGrid();
                    BindTestReportMasterGrid();

                    BindServiceMasterGrid();

                    BuildeTestReportDtls();

                    BuildeTestReportMicrobiologyDtls();

                    BuildServiceList();

                    string PageName = "", LabTestReportID = "";



                    ViewState["InvoiceID"] = Request.QueryString["InvoiceID"];
                    ViewState["EMRID"] = Request.QueryString["EMRID"];

                    txtInvoiceRefNo.Text = Request.QueryString["InvoiceID"];
                    txtEMRID.Text = Request.QueryString["EMRID"];

                    ViewState["PatientID"] = Request.QueryString["PatientID"];
                    ViewState["DoctorID"] = Request.QueryString["DoctorID"];
                    ViewState["TransDate"] = Request.QueryString["TransDate"];
                    ViewState["TransType"] = Request.QueryString["TransType"];
                    ViewState["ServiceID"] = Request.QueryString["ServiceID"];
                    txtTestSampleID.Text = Request.QueryString["LabSampleID"];
                    txtBarecodeID.Text = Request.QueryString["LabSampleBareCode"];
                    LabTestReportID = Request.QueryString["LabTestReportID"];



                    PageName = (string)Request.QueryString["PageName"];


                    if (PageName == "LabWaitList" || PageName == "MalaffiLog")
                    {

                        if (LabTestReportID != "")
                        {
                            txtTransNo.Text = LabTestReportID;
                            txtTransNo_TextChanged(txtTransNo, new EventArgs());

                        }
                        else
                        {
                            //txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);
                            //txtFileNo_TextChanged(txtFileNo, new EventArgs());

                            // LogFileWriting(System.DateTime.Now.ToString() + " BindTestSampleMaster() Start ");
                            BindTestSampleMaster();
                            //  LogFileWriting(System.DateTime.Now.ToString() + "    BindTestSampleMaster() Completed ");


                            //  if (GlobelValues.FileDescription.ToUpper() == "SMCH" && Convert.ToString(ViewState["TransType"]).ToUpper() == "INVOICE")
                            //{
                            txtInvoiceRefNo.Text = Convert.ToString(ViewState["InvoiceID"]);
                            //  txtServCode.Text = Convert.ToString(ViewState["ServiceID"]);


                            BindInvoice();
                            //  LogFileWriting(System.DateTime.Now.ToString() + "    BindInvoice() Completed ");
                            //BindLabTestDtlsInvoiceWise();

                            // BindServiceList();

                            BindTestSampleList();
                            //  LogFileWriting(System.DateTime.Now.ToString() + "    BindTestSampleList() Completed ");
                            BindTempServiceList();
                            //  LogFileWriting(System.DateTime.Now.ToString() + "    BindTempServiceList() Completed ");
                            // }
                            //  else
                            //  {
                            //  BindLabTestDtlsServiceWise("'" + Convert.ToString(ViewState["ServiceID"]) + "'");
                            //  }
                            BindTempTestReportDtls();

                            BuildeTestReportMicrobiologyDtls();
                            BindTempMicrobiologyDtls();
                            BindEMRDtls();
                            // LogFileWriting(System.DateTime.Now.ToString() + "    BindTempServiceList() Completed ");
                            //
                        }

                    }


                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                PTDtlsClear();

                New();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtTransNo_TextChanged(object sender, EventArgs e)
        {
            Clear();
            PTDtlsClear();
            BindLabTestReportMaster();
            BindTestReportDtls();
            BindTempTestReportDtls();

            BindTestReportMicrobiologyDtls();
            BindTempMicrobiologyDtls();

            BindEMRDtls();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {
                    CommonBAL objCom = new CommonBAL();

                    if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                    {

                        if (drpResultStatus.SelectedValue == "X")
                        {

                            string FieldNameWithValues = " LTRM_AUTHORIZE_STATUS='NOT AUTHORIZED' , LTRM_AUTHORIZE_USER_ID=NULL , LTRM_AUTHORIZE_DATE=NULL ";
                            string CriteriaAuth = " LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";
                            objCom.fnUpdateTableData(FieldNameWithValues, "LAB_TEST_REPORT_MASTER", CriteriaAuth);


                        }


                    }


                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Test Report, Test Report ID is " + txtTransNo.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Lab Test Report, Test Report ID is " + txtTransNo.Text);
                    }


                    // Clear();
                    //  PTDtlsClear();


                    DataSet DS = new DataSet();
                    objCom = new CommonBAL();
                    string Criteria = "AT_TRANSTYPE='LABTESTREPORT' AND AT_TRANSID='" + txtTransNo.Text.Trim() + "'";
                    DS = objCom.fnGetFieldValue("AT_TRANSID", "HMS_AUTHORISATION", Criteria, "");

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        string FieldNameWithValues = "AT_MODIFIED_USER_ID='" + Convert.ToString(Session["User_ID"]) + "',AT_MODIFIED_DATE = getdate()";

                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_AUTHORISATION", Criteria);

                    }
                    else
                    {

                        objCom.fnInsertTableData("AT_BRANCH_ID,AT_TRANSID,AT_TRANSTYPE,AT_CREATED_USER_ID,AT_CREATED_DATE,AT_STAFF_ID",
                          "'" + Convert.ToString(Session["Branch_ID"]) + "','" + txtTransNo.Text.Trim() + "','LABTESTREPORT','" + Convert.ToString(Session["User_ID"]) + "',getdate(),'" + Convert.ToString(Session["User_Code"]) + "'", "HMS_AUTHORISATION");

                    }


                    BuildeTestReportDtls();
                    BindTempTestReportDtls();


                    ClearMicrobiologyDtls();

                    BuildeTestReportMicrobiologyDtls();
                    BindTempMicrobiologyDtls();


                    New();

                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        string GetCommnMasterCode(string Name, string Type)
        {
            string strSpecialtyCode = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = " HCM_TYPE='" + Type + "' AND HCM_DESC='" + Name + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strSpecialtyCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CODE"]);
            }

            return strSpecialtyCode;
        }

        string GetNationalityCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HNM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HNM_MALAFFI_CODE", "HMS_NATIONALITY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HNM_MALAFFI_CODE"]);
            }

            return strCode;
        }

        string GetCountryCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_MALAFFI_CODE", "HMS_COUNTRY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_MALAFFI_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]) != "")
                {
                    strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]).Trim();
                }
            }

            return strCode;
        }

        string GetKinCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_TYPE='Relationship' and HCM_DESC='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_CODE", "HMS_COMMON_MASTERS", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CODE"]);
            }

            return strCode;
        }

        void GetStaffDetails(string StaffCode, out string DrFirstName, out string DrLastName, out string LicenseNo, out string strSpecialityName)
        {
            DrFirstName = "";
            DrLastName = "";
            LicenseNo = "";
            strSpecialityName = "";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HSFM_STAFF_ID='" + StaffCode + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_STAFF_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                DrFirstName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FNAME"]);
                if (DS.Tables[0].Rows[0].IsNull("HSFM_LNAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LNAME"]) != "")
                {
                    DrLastName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LNAME"]);
                }
                LicenseNo = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SPECIALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]) != "")
                {
                    strSpecialityName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]);
                }

            }


        }

        void GetCompanyDtls(string CompanyID, out string PayerID)
        {
            PayerID = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HCM_PAYERID   NOT LIKE '@%' AND HCM_COMP_ID='" + CompanyID + "'";


            DS = objCom.fnGetFieldValue(" TOP 1 *", "HMS_COMPANY_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                PayerID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);

            }
        }



        Boolean CheckPatientDetails(string PT_ID, out string ErrorMessagePTDtls)
        {
            ErrorMessagePTDtls = "";
            Boolean IsNoError = true;

            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = "HPM_PT_ID='" + PT_ID + "'";

            DS = objCom.fnGetFieldValue("TOP 1 *, CONVERT(VARCHAR, HPM_DOB,103) AS HPM_DOBDesc", "HMS_PATIENT_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count <= 0)
            {

                ErrorMessagePTDtls += "Patient Details is empty ,  ";

                IsNoError = false;


            }



            foreach (DataRow DR in DS.Tables[0].Rows)
            {


                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]) == "")
                {
                    ErrorMessagePTDtls += " Patient ID is empty ,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_FNAME") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_FNAME"]) == "")
                {
                    ErrorMessagePTDtls += " Patient First Name is empty ,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_LNAME") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_LNAME"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Family Name is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_DOB") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_DOB"]) == "")
                {
                    ErrorMessagePTDtls += "Patient DOB is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_SEX") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Gender is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_COUNTRY") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_COUNTRY"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Country is empty,  ";

                    IsNoError = false;
                }


                string strCountryCode = GetCountryCode(Convert.ToString(DR["HPM_COUNTRY"]));

                if (strCountryCode == "")
                {
                    ErrorMessagePTDtls += "Patient Country Code is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_ADDR") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_ADDR"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Address is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_CITY") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_CITY"]) == "")
                {
                    ErrorMessagePTDtls += "Patient City is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_EMIRATES_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_EMIRATES_CODE"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Emirates Code is empty,  ";

                    IsNoError = false;
                }


                //if (DS.Tables[0].Rows[0].IsNull("HPM_PHONE1") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PHONE1"]) == "")
                //{
                //    ErrorMessagePTDtls += "Patient Home Phone is empty,  ";

                //    IsNoError = false;
                //}

                if (DS.Tables[0].Rows[0].IsNull("HPM_MOBILE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Mobile No is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_MARITAL") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_MARITAL"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Marital Status is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_RELIGION_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_RELIGION_CODE"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Religion is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Emirates ID is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_NATIONALITY"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Nationality is empty,  ";

                    IsNoError = false;
                }



                if (DR.IsNull("HPM_KIN_NAME") == false && Convert.ToString(DR["HPM_KIN_NAME"]) != "")
                {


                    if (DS.Tables[0].Rows[0].IsNull("HPM_KIN_RELATION") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_KIN_RELATION"]) == "")
                    {
                        ErrorMessagePTDtls += "Patient Kin Relation is empty,  ";

                        IsNoError = false;
                    }

                    if (DS.Tables[0].Rows[0].IsNull("HPM_KIN_MOBILE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_KIN_MOBILE"]) == "")
                    {
                        ErrorMessagePTDtls += "Patient Kin Mobile is empty,  ";

                        IsNoError = false;
                    }

                }



                //if (DS.Tables[0].Rows[0].IsNull("HPM_KNOW_FROM") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_KNOW_FROM"]) == "")
                //{
                //    ErrorMessagePTDtls += "Patient Know From  is empty,  ";

                //    IsNoError = false;
                //}



                string PT_TYPE = "", COMP_ID = "";


                if (DR.IsNull("HPM_PT_TYPE") == false && Convert.ToString(DR["HPM_PT_TYPE"]) != "")
                {
                    PT_TYPE = Convert.ToString(DR["HPM_PT_TYPE"]).ToUpper();
                }
                else
                {
                    ErrorMessagePTDtls += "Patient Type  is empty,  ";
                    IsNoError = false;
                }

                if (PT_TYPE != "")
                {

                    if (PT_TYPE.ToUpper() == "CR" || PT_TYPE.ToUpper() == "CREDIT")
                    {

                        if (DR.IsNull("HPM_INS_COMP_ID") == false && Convert.ToString(DR["HPM_INS_COMP_ID"]) != "")
                        {
                            COMP_ID = Convert.ToString(DR["HPM_INS_COMP_ID"]).ToUpper();
                        }


                        if (DR.IsNull("HPM_POLICY_NO") == true && Convert.ToString(DR["HPM_POLICY_NO"]) == "")
                        {
                            ErrorMessagePTDtls += "Patient Policy No is empty,  ";

                            IsNoError = false;
                        }


                        string PayerID = "";
                        GetCompanyDtls(COMP_ID, out PayerID);

                        if (PayerID == "")
                        {
                            ErrorMessagePTDtls += " Company Payer ID is empty ,  ";

                            IsNoError = false;
                        }

                    }
                }


            }

            return IsNoError;

        }

        Boolean CheckPatientVisitDtls(string PT_ID, string VISIT_ID, out string ErrorMessagePTDVisitDls)
        {
            ErrorMessagePTDVisitDls = "";

            Boolean IsNoError = true;
            string DR_ID;
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "HPV_PT_ID='" + PT_ID + "' ";
            Criteria += " AND HPV_SEQNO='" + VISIT_ID + "' ";




            DS = objCom.fnGetFieldValue(" TOP 1 *", "HMS_PATIENT_VISIT", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HPV_DR_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]) == "")
                {
                    ErrorMessagePTDVisitDls += "Doctor ID is empty ,  ";

                    IsNoError = false;
                }


                DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]).Trim();

                string DrFirstName, DrMiddleName, LicenseNo, SpecialityName;
                GetStaffDetails(DR_ID, out   DrFirstName, out   DrMiddleName, out   LicenseNo, out SpecialityName);


                if (LicenseNo == "")
                {
                    ErrorMessagePTDVisitDls += "Doctor License No is empty,  ";

                    IsNoError = false;
                }



                if (SpecialityName == "")
                {
                    ErrorMessagePTDVisitDls += "Patient Specialty Name is Empty,  ";

                    IsNoError = false;
                }


                string PT_TYPE = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PT_TYPE"]).ToUpper();




                string Specialty = GetCommnMasterCode(SpecialityName, "Specialty");

                if (Specialty == "")
                {
                    ErrorMessagePTDVisitDls += "Patient Specialty is Empty,  ";

                    IsNoError = false;
                }



                if (DS.Tables[0].Rows[0].IsNull("HPV_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATE"]) == "")
                {
                    ErrorMessagePTDVisitDls += "Patient Visit ID is Empty,  ";

                    IsNoError = false;

                }



                if (PT_TYPE == "CR" || PT_TYPE == "CREDIT")
                {

                    if (DS.Tables[0].Rows[0].IsNull("HPV_COMP_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_ID"]) == "")
                    {

                        ErrorMessagePTDVisitDls += "Patient Company ID is Empty,  ";

                        IsNoError = false;

                    }
                    if (DS.Tables[0].Rows[0].IsNull("HPV_COMP_NAME") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]) == "")
                    {
                        ErrorMessagePTDVisitDls += "Patient Company Name is Empty,  ";

                        IsNoError = false;
                    }


                }



            }
            else
            {

                ErrorMessagePTDVisitDls += "Patient Visit is empty ,  ";

                IsNoError = false;


            }

            return IsNoError;

        }

        Boolean CheckLabTestReport(string PT_ID, out string ErrorMessageTestReport)
        {
            ErrorMessageTestReport = "";
            Boolean IsNoError = true;

            string Criteria = " 1=1 ";

            Criteria += " AND  LTRM_PATIENT_ID = '" + PT_ID + "' AND LTRM_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";

            DataSet DS = new DataSet();


            DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("LTRM_SERV_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_SERV_ID"]) == "")
                {
                    ErrorMessageTestReport += " Service Code is empty ,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_SERV_DESCRIPTION") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_SERV_DESCRIPTION"]) == "")
                {
                    ErrorMessageTestReport += "Service Desc is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_SAMPLE_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_SAMPLE_ID"]) == "")
                {
                    ErrorMessageTestReport += "Sample ID is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_RESULT_STATUS") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_RESULT_STATUS"]) == "")
                {
                    ErrorMessageTestReport += "Sample Result Status is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_DIAGNOSTIC_SERVICE") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DIAGNOSTIC_SERVICE"]) == "")
                {
                    ErrorMessageTestReport += "Sample Diagnostic Service is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_COLLECTION_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_COLLECTION_DATE"]) == "")
                {
                    ErrorMessageTestReport += "Sample Collection Date is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_DELIVERY_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DELIVERY_DATE"]) == "")
                {
                    ErrorMessageTestReport += "Sample Delivery Date is empty,  ";

                    IsNoError = false;
                }



                if (DS.Tables[0].Rows[0].IsNull("LTRM_TEST_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_TEST_DATE"]) == "")
                {
                    ErrorMessageTestReport += "Test Date is empty,  ";

                    IsNoError = false;
                }





                ////if (DS.Tables[0].Rows[0].IsNull("LTRM_AUTHORIZE_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_AUTHORIZE_DATE"]) == "")
                ////{
                ////    ErrorMessageTestReport += "Authorize Date is empty,  ";

                ////    IsNoError = false;
                ////}

                if (DS.Tables[0].Rows[0].IsNull("LTRM_DR_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]) == "" || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]) == "0")
                {
                    ErrorMessageTestReport += "Doctor Code is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_DR_CODE") == false || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]) != "")
                {

                    string DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]).Trim();

                    string DrFirstName, DrMiddleName, LicenseNo, SpecialityName;
                    GetStaffDetails(DR_ID, out   DrFirstName, out   DrMiddleName, out   LicenseNo, out SpecialityName);



                    if (LicenseNo == "")
                    {
                        ErrorMessageTestReport += "Doctor License No is empty,  ";

                        IsNoError = false;
                    }

                    if (DrFirstName == "")
                    {
                        ErrorMessageTestReport += "Doctor Name is empty,  ";

                        IsNoError = false;
                    }

                    if (SpecialityName == "")
                    {
                        ErrorMessageTestReport += "Speciality Name is empty,  ";

                        IsNoError = false;
                    }



                }





                if (DS.Tables[0].Rows[0].IsNull("LTRM_REF_DR") == true || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]) == "" || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]) == "0")
                {
                    ErrorMessageTestReport += "Ref. Doctor Code is empty,  ";

                    IsNoError = false;
                }



                if (DS.Tables[0].Rows[0].IsNull("LTRM_REF_DR") == false || Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]) != "")
                {

                    string RefDR_ID = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]).Trim();

                    string RefDrFirstName, RefDrMiddleName, RefLicenseNo, RefSpecialityName;
                    GetStaffDetails(RefDR_ID, out   RefDrFirstName, out   RefDrMiddleName, out   RefLicenseNo, out RefSpecialityName);



                    if (RefLicenseNo == "")
                    {
                        ErrorMessageTestReport += "Ref Doctor License No is empty,  ";

                        IsNoError = false;
                    }

                    if (RefDrFirstName == "")
                    {
                        ErrorMessageTestReport += "Ref Doctor Name is empty,  ";

                        IsNoError = false;
                    }

                    if (RefSpecialityName == "")
                    {
                        ErrorMessageTestReport += "Ref Speciality Name is empty,  ";

                        IsNoError = false;
                    }

                }


            }
            //else
            //{

            //    ErrorMessageTestReport += "Test Report Details is empty ,  ";

            //    IsNoError = false;


            //}
            return IsNoError;
        }


        Boolean CheckLabTestReportDtls(string PT_ID, out string ErrorMessageTestReportDtls)
        {
            ErrorMessageTestReportDtls = "";
            Boolean IsNoError = true;

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND LTRD_TEST_REPORT_ID = '" + txtTransNo.Text.Trim() + "'";

            DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_DETAIL", Criteria, "LTRD_SLNO");
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TestCode = "";


                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (DR.IsNull("LTRD_TEST_PROFILE_ID") == true || Convert.ToString(DR["LTRD_TEST_PROFILE_ID"]) == "")
                    {
                        ErrorMessageTestReportDtls += "Test Code is empty for ,  ";

                        IsNoError = false;
                    }


                    if (DR.IsNull("LTRD_TEST_PROFILE_ID") == false || Convert.ToString(DR["LTRD_TEST_PROFILE_ID"]) != "")
                    {
                        TestCode = Convert.ToString(DS.Tables[0].Rows[0]["LTRD_TEST_PROFILE_ID"]);
                    }

                    if (DR.IsNull("LTRD_TEST_RESULT") == true || Convert.ToString(DR["LTRD_TEST_RESULT"]) == "")
                    {
                        ErrorMessageTestReportDtls += "Result is empty , Test Code is  " + TestCode + ",  ";

                        IsNoError = false;
                    }

                    if (DR.IsNull("LTRD_TEST_UNIT") == true || Convert.ToString(DR["LTRD_TEST_UNIT"]) == "")
                    {
                        ErrorMessageTestReportDtls += "Unit is empty  , Test Code is  " + TestCode + ",  ";

                        IsNoError = false;
                    }


                    if (DR.IsNull("LTRD_RANGE_TYPE") == true || Convert.ToString(DR["LTRD_RANGE_TYPE"]) == "")
                    {
                        ErrorMessageTestReportDtls += "Range Type is empty  , Test Code is  " + TestCode + ",  ";

                        IsNoError = false;
                    }

                    if (DR.IsNull("LTRD_TEST_NORMALVALUE") == true || Convert.ToString(DR["LTRD_TEST_NORMALVALUE"]) == "")
                    {
                        ErrorMessageTestReportDtls += "Normal is empty  , Test Code is  " + TestCode + ",  ";

                        IsNoError = false;
                    }




                }

            }
            //else
            //{

            //    ErrorMessageTestReportDtls += "Test Report Service Details is empty ,  ";

            //    IsNoError = false;


            //}


            return IsNoError;
        }

        Boolean ValidateNL7Message(string PT_ID)
        {
            Boolean IsNoError = true;
            Boolean IsNoErrorPTDtls = true, IsNoErrorPTDVisitDls = true, IsNoErrorTestReport = true, IsNoErrorTestReportDtls = true;

            string ErrorMessagePTDtls = "", ErrorMessagePTDVisitDls = "", ErrorMessageTestReport = "", ErrorMessageTestReportDtls = "";

            IsNoErrorPTDtls = CheckPatientDetails(PT_ID, out ErrorMessagePTDtls);

            // IsNoErrorPTDVisitDls = CheckPatientVisitDtls(PT_ID, VISIT_ID, out ErrorMessagePTDVisitDls);

            IsNoErrorTestReport = CheckLabTestReport(PT_ID, out ErrorMessageTestReport);

            IsNoErrorTestReportDtls = CheckLabTestReportDtls(PT_ID, out  ErrorMessageTestReportDtls);

            if (IsNoErrorPTDtls == false || IsNoErrorTestReport == false || IsNoErrorTestReportDtls == false)  ///|| IsNoErrorPTDVisitDls == false
            {

                lblStatus.Text = ErrorMessagePTDtls + ErrorMessagePTDVisitDls + ErrorMessageTestReport + ErrorMessageTestReportDtls;
                lblStatus.ForeColor = System.Drawing.Color.Red;


                IsNoError = false;
            }
            return IsNoError;
        }


        Boolean CheckServiceSendToMalaffi()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='L' AND ( HSM_SEND_TO_MALAFFI IS NULL OR HSM_SEND_TO_MALAFFI = 1)  ";
            Criteria += " AND HSM_SERV_ID = '" + txtServCode.Text.Trim() + "'";

            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "1");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;

        }

        Boolean CheckEMR_ID()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "' AND EPM_ID=" + txtEMRID.Text.Trim();
             DataSet DS = new DataSet();
            EMR_PTMasterBAL obj = new EMR_PTMasterBAL();
            DS = obj.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
     

            return false;
        }

        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();
                DataSet DS = new DataSet();

                if (drpResultStatus.SelectedValue == "C" || drpResultStatus.SelectedValue == "X")
                {
                    string Criteria1 = " 1=1 AND LTRM_AUTHORIZE_STATUS='AUTHORIZED' ";
                    Criteria1 += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria1 += " AND LTRM_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";

                    DS = objCom.fnGetFieldValue(" TOP 1  *", "LAB_TEST_REPORT_MASTER", Criteria1, "LTRM_CREATED_DATE desc ");


                    if (DS.Tables[0].Rows.Count <= 0)
                    {

                        lblStatus.Text = drpResultStatus.SelectedItem.Text + " - only for update the Test Details";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }

                }


                LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() Start ");

                objCom = new CommonBAL();
                string FieldNameWithValues = " LTRM_AUTHORIZE_STATUS='AUTHORIZED' , LTRM_AUTHORIZE_USER_ID='" + Convert.ToString(Session["User_ID"]) + "' , LTRM_AUTHORIZE_DATE=GETDATE() ";
                string Criteria = " LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "LAB_TEST_REPORT_MASTER", Criteria);
                lblAuthorizeStatus.Text = "Authorized";


                DS = new DataSet();
                objCom = new CommonBAL();
                Criteria = "AT_TRANSTYPE='LABTESTREPORT' AND AT_TRANSID='" + txtTransNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue("AT_TRANSID", "HMS_AUTHORISATION", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    FieldNameWithValues = "AT_AUTHORISED_USER_ID='" + Convert.ToString(Session["User_ID"]) + "',AT_AUTHORISED_DATE = getdate(),AT_AUTHORISATION_REQUIRED='Y' ";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_AUTHORISATION", Criteria);

                    AuditLogAdd("MODIFY", "Modify Existing entry in the Authorization,Test Report ID is " + txtTransNo.Text + " and Authorized by " + Convert.ToString(Session["User_ID"]));
                }
                else
                {

                    objCom.fnInsertTableData("AT_BRANCH_ID,AT_TRANSID,AT_TRANSTYPE,AT_CREATED_USER_ID,AT_CREATED_DATE,AT_STAFF_ID,AT_AUTHORISED_USER_ID,AT_AUTHORISED_DATE,AT_AUTHORISATION_REQUIRED",
                      "'" + Convert.ToString(Session["Branch_ID"]) + "','" + txtTransNo.Text.Trim() + "','LABTESTREPORT','" + Convert.ToString(Session["User_ID"]) + "',getdate(),'" + Convert.ToString(Session["User_Code"])
                  + "','" + Convert.ToString(Session["User_ID"]) + "', getdate(),'Y' ", "HMS_AUTHORISATION");

                    AuditLogAdd("ADD", "Add new entry in the Authorization,Test Report ID is " + txtTransNo.Text + " and Authorized by " + Convert.ToString(Session["User_ID"]));
                }


                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1" && txtEMRID.Text.Trim() !="0")
                {

                    if (CheckServiceSendToMalaffi() == false)
                    {
                        lblStatus.Text = "Data  Authorized";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        goto FunEnd;
                    }

                    if (CheckEMR_ID() == false)
                    {
                        goto FunEnd;
                    }

                    LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() 1 ");
                   // string strHPV_SEQNo = "";
                    ////objCom = new CommonBAL();
                    ////DS = new DataSet();
                    ////Criteria = "HPV_EMR_ID='" + txtEMRID.Text.Trim() + "'";
                    ////DS = objCom.fnGetFieldValue("TOP 1 HPV_SEQNO ", "HMS_PATIENT_VISIT", Criteria, "");

                    ////if (DS.Tables[0].Rows.Count > 0)
                    ////{
                    ////    strHPV_SEQNo = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);

                    ////}

                    ////else
                    ////{
                    ////    LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() Visit ID is missing ");

                    ////    lblStatus.Text = "Visit ID is missing";
                    ////    lblStatus.ForeColor = System.Drawing.Color.Red;
                    ////    goto FunEnd;
                    ////}


                    //if (ValidateNL7Message(txtFileNo.Text.Trim(), strHPV_SEQNo, txtEMRID.Text.Trim()) == false)
                    //{


                    //    goto FunEnd;
                    //}



                    HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                    objHL7Mess.PT_ID = txtFileNo.Text.Trim();
                    objHL7Mess.EMR_ID = txtEMRID.Text.Trim();
                    objHL7Mess.VISIT_ID = "";
                    objHL7Mess.LAB_TEST_REPORT_ID = txtTransNo.Text.Trim();

                    objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                    objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);

                    objHL7Mess.MESSAGE_TYPE = "RESULT";
                    objHL7Mess.UPLOAD_STATUS = "PENDING";
                    objHL7Mess.MessageCode = "ORU-R01";

                    string MessageDesc = "Add Result ORU Lab";
                    if (drpResultStatus.SelectedValue == "F")
                    {
                        MessageDesc = "Add Result ORU Lab";
                    }
                    else if (drpResultStatus.SelectedValue == "C")
                    {
                        MessageDesc = "Update Result ORU Lab";
                    }
                    else if (drpResultStatus.SelectedValue == "X")
                    {
                        MessageDesc = "Delete Result ORU Lab";
                    }

                    objHL7Mess.MessageDesc = MessageDesc;

                    LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() 2 ");

                    objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);
                    objHL7Mess.MalaffiHL7MessageMasterAdd();

                    LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() 3 ");

                    ////Boolean IsError = false;
                    ////LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() 2 ");
                    ////IsError = objHL7Mess.LabMessageGenerate_ORU_R01();

                    ////LogFileWriting(System.DateTime.Now.ToString() + " btnAuthorize_Click() 3 ");

                }



                lblStatus.Text = "Data  Authorized";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAuthorize_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            if (drpResultStatus.SelectedValue == "X")
            {
                lblStatus.Text = "Order Already canceled.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                goto FunEnd;
            }

            if (Convert.ToString(Session["REQ_INVOICE_NO_LAB_TESTREPORT"]) == "1")
            {
                if (txtInvoiceRefNo.Text == "")
                {
                    lblStatus.Text = "Please Enter Invoice Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DataSet DS = new DataSet();
                CommonBAL objCom = new CommonBAL();
                string Criteria = " 1=1  ";
                Criteria += " AND HIM_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "' AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue("HIM_INVOICE_ID", "HMS_INVOICE_MASTER", Criteria, "");

                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = "Please Enter correct Invoice Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
            }


            string ReportName = "LabReport.rpt";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "RadResult", "LabResultReport('" + ReportName + "','" + txtTransNo.Text.Trim() + "');", true);

            try
            {
                DataSet DS = new DataSet();
                objCom = new CommonBAL();
                string Criteria = "AT_TRANSTYPE='LABTESTREPORT' AND AT_TRANSID='" + txtTransNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue("*", "HMS_AUTHORISATION", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    string FieldNameWithValues = "";

                    if (DS.Tables[0].Rows[0].IsNull("AT_PRINTED_ON") == true || Convert.ToString(DS.Tables[0].Rows[0]["AT_PRINTED_ON"]) == "")
                    {
                        FieldNameWithValues = "AT_PRINTED_USER_ID='" + Convert.ToString(Session["User_ID"]) + "',AT_PRINTED_ON = getdate()";
                    }
                    else
                    {
                        FieldNameWithValues = "AT_REPRINTED_USER_ID='" + Convert.ToString(Session["User_ID"]) + "',AT_REPRINTED_ON = getdate()";

                    }
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_AUTHORISATION", Criteria);

                }
                else
                {

                    objCom.fnInsertTableData("AT_BRANCH_ID,AT_TRANSID,AT_TRANSTYPE,AT_CREATED_USER_ID,AT_CREATED_DATE,AT_STAFF_ID,AT_PRINTED_USER_ID,AT_PRINTED_ON",
                      "'" + Convert.ToString(Session["Branch_ID"]) + "','" + txtTransNo.Text.Trim() + "','LABTESTREPORT','" + Convert.ToString(Session["User_ID"]) + "',getdate(),'" + Convert.ToString(Session["User_Code"])
                  + "','" + Convert.ToString(Session["User_ID"]) + "',' getdate(),", "HMS_AUTHORISATION");

                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnReport_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnLabHistory_Click(object sender, EventArgs e)
        {
            try
            {
                BindTestReportMasterGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnLabHistory_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void ServMasterEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblServID = (Label)gvScanCard.Cells[0].FindControl("lblServID");
            Label lblServName = (Label)gvScanCard.Cells[0].FindControl("lblServName");
            Label lblServFee = (Label)gvScanCard.Cells[0].FindControl("lblServFee");




            txtServCode.Text = lblServID.Text;
            txtServName.Text = lblServName.Text;

            if (lblServName.Text != "")
            {
                string str = lblServName.Text;
                if (str.Length > 50)
                {
                    str = str.Substring(0, 50);
                }
                txtServName.Text = str;

            }


            txtFee.Text = Server.HtmlDecode(lblServFee.Text);



            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideServMasterPopup()", true);
        }

        protected void SampleMasterEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvSampMasSampleID = (Label)gvScanCard.Cells[0].FindControl("lblgvSampMasSampleID");
             


            //txtFee.Text = Server.HtmlDecode(lblServFee.Text);
            txtTestSampleID.Text = lblgvSampMasSampleID.Text;

            BindTestSampleMaster();

            BuildeTestReportDtls();
            BindTestSampleDtls();
            BindTempTestReportDtls();

            BuildeTestReportMicrobiologyDtls();
            BindTempMicrobiologyDtls();


            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideSampleMasterPopup()", true);
        }

        protected void TestMasterEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvTestMasReportID = (Label)gvScanCard.Cells[0].FindControl("lblgvTestMasReportID");

            txtTransNo.Text = lblgvTestMasReportID.Text;

            Clear();
            PTDtlsClear();
            BindLabTestReportMaster();
            BindTestReportDtls();
            BindTempTestReportDtls();

            BindTestReportMicrobiologyDtls();
            BindTempMicrobiologyDtls();


            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideTestMasterPopup()", true);
        }

        protected void TestReportDtlsEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvTestReportDtlsProfilID = (Label)gvScanCard.Cells[0].FindControl("lblgvTestReportDtlsProfilID");
            Label lblgvTestReportDtlsDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvTestReportDtlsDesc");

            txtTestName.Text = lblgvTestReportDtlsProfilID.Text + "~" + lblgvTestReportDtlsDesc.Text;
        }

        protected void drpReportType_SelectedIndexChanged(object sender, EventArgs e)
        {

            //foreach (GridViewRow gvRow in gvTestReportDtls.Rows)
            //{
            //    Label lblgvTestReportDtlsInvProfilID = (Label)gvRow.Cells[0].FindControl("lblgvTestReportDtlsInvProfilID");

            //    if (drpReportType.SelectedValue == lblgvTestReportDtlsInvProfilID.Text )
            //    {
            //        lblStatus.Text = "Report Type Already Added";
            //        goto FunEnd;

            //    }

            //}


            //if (chkMultyReptType.Checked == false)
            //{
            //    BuildeTestReportDtls();
            //}
            BindInvProfileMaster();
            BuildeTestReportDtls();
            BindInvProfileTrans();
            BindTempTestReportDtls();

        FunEnd: ;

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();
                string CriteriaServMas = "HSM_STATUS='A' AND HSM_OBSERVATION_NEEDED='Y' AND HSM_HAAD_CODE='" + txtServCode.Text + "'";
                DataSet DSServMas = new DataSet();

                DSServMas = objCom.fnGetFieldValue("*", "HMS_SERVICE_MASTER", CriteriaServMas, "");
                string ObsvNeeded = "N", ObsvType = "Result", ObsvCode = "";
                if (DSServMas.Tables[0].Rows.Count > 0)
                {
                    ObsvNeeded = Convert.ToString(DSServMas.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
                    ObsvType = Convert.ToString(DSServMas.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);
                    ObsvCode = Convert.ToString(DSServMas.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                }



                if (txtTestName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestReportDtls"];

                strTestCodeDesc = txtTestName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();

                string UNIT = "", ADDITIONAL_REFERENCE = "", SERV_ID = "";
                GetTestProfileMasterDtls(TestProfileID, out UNIT, out  ADDITIONAL_REFERENCE, out SERV_ID);


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "" && DT.Rows.Count > 0)
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["LTRD_TEST_PROFILE_ID"] = TestProfileID;
                    DT.Rows[R]["LTRD_TEST_DESCRIPTION"] = TestProfileDesc;
                    DT.Rows[R]["LTRD_RANGE"] = "";
                    DT.Rows[R]["LTRD_TEST_NORMALVALUE"] = "";

                    DT.Rows[R]["LTRD_TEST_UNIT"] = UNIT;
                    //   DT.Rows[R]["LTRD_INVESTIGATION_PROFILE_ID"] = drpReportType.SelectedValue;
                    DT.Rows[R]["LTRD_SERV_CODE"] = SERV_ID;// txtServCode.Text.Trim();
                    DT.Rows[R]["LTRD_ADDITIONAL_REFERENCE"] = ADDITIONAL_REFERENCE;

                    DT.Rows[R]["LTRD_OBSV_NEEDED"] = ObsvNeeded;
                    DT.Rows[R]["LTRD_OBSV_TYPE"] = ObsvType;
                    DT.Rows[R]["LTRD_OBSV_CODE"] = ObsvCode;


                    DT.AcceptChanges();
                }
                else
                {

                    objrow["LTRD_TEST_PROFILE_ID"] = TestProfileID;
                    objrow["LTRD_TEST_DESCRIPTION"] = TestProfileDesc;

                    objrow["LTRD_TEST_UNIT"] = UNIT;
                    //  objrow["LTRD_INVESTIGATION_PROFILE_ID"] = drpReportType.SelectedValue;
                    objrow["LTRD_SERV_CODE"] = SERV_ID;// txtServCode.Text.Trim();
                    objrow["LTRD_ADDITIONAL_REFERENCE"] = ADDITIONAL_REFERENCE;


                    objrow["LTRD_OBSV_NEEDED"] = ObsvNeeded;
                    objrow["LTRD_OBSV_TYPE"] = ObsvType;
                    objrow["LTRD_OBSV_CODE"] = ObsvCode;

                    DT.Rows.Add(objrow);
                }



                ViewState["TestReportDtls"] = DT;


                BindTempTestReportDtls();
                txtTestName.Text = "";

                ViewState["gvServSelectIndex"] = "";

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        void GetTransferTestResultData(string ProfileID, out string RESULT_VALUE, out string NORMAL_TYPE, out string NORMAL_RANGE, out string RANGE_TYPE)
        {
            RESULT_VALUE = "";
            NORMAL_TYPE = "";
            NORMAL_RANGE = "";
            RANGE_TYPE = ""; ;

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = "  ID='" + txtTestSampleID.Text + "' AND ResultUploadedtoHIS=1 ";
            Criteria += " AND HIS_ParamCode='" + ProfileID + "'";

            DS = objCom.fnGetFieldValue("*", "LAB_TEST_RESULT_DATA", Criteria, "HIS_TestName");


            if (DS.Tables[0].Rows.Count > 0)
            {
                RESULT_VALUE = Convert.ToString(DS.Tables[0].Rows[0]["Analyzer_Result"]);

                GetProfileTransValues(ProfileID, RESULT_VALUE, out NORMAL_TYPE, out NORMAL_RANGE, out RANGE_TYPE);



            }

        }

        protected void gvTestReportDtls_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblgvTestReportDtlsProfilID = (Label)e.Row.Cells[0].FindControl("lblgvTestReportDtlsProfilID");

                    Label lblgvTestReportDtlsRange = (Label)e.Row.Cells[0].FindControl("lblgvTestReportDtlsRange");
                    DropDownList drpgvTestReportDtlsRange = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestReportDtlsRange");
                    TextBox txtgvTestReportDtlsNorValue = (TextBox)e.Row.Cells[0].FindControl("txtgvTestReportDtlsNorValue");
                    //Label lblgvTestReportDtlsAbnorm = (Label)e.Row.Cells[0].FindControl("lblgvTestReportDtlsAbnorm");
                    TextBox txtgvTestReportDtlsResult = (TextBox)e.Row.Cells[0].FindControl("txtgvTestReportDtlsResult");

                    Label lblgvTestReportDtlsRangeType = (Label)e.Row.Cells[0].FindControl("lblgvTestReportDtlsRangeType");
                    DropDownList drpgvTestReportDtlsRangeType = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestReportDtlsRangeType");

                    Label lnkgvMicroDtlsStatus = (Label)e.Row.Cells[0].FindControl("lnkgvMicroDtlsStatus");

                    drpgvTestReportDtlsRange.DataSource = BindTestProfileTrans(lblgvTestReportDtlsProfilID.Text);
                    drpgvTestReportDtlsRange.DataTextField = "LTPT_RANGE_DESCRIPTION";
                    drpgvTestReportDtlsRange.DataValueField = "LTPT_NORMAL_TYPE"; //"LTPT_NORMAL_RANGE";
                    drpgvTestReportDtlsRange.DataBind();

                    //drpgvTestReportDtlsRange.Items.Insert(0, "--- Select ---");
                    //drpgvTestReportDtlsRange.Items[0].Value = "0";

                    if (lblgvTestReportDtlsRange.Text != "")
                    {
                        drpgvTestReportDtlsRange.SelectedValue = lblgvTestReportDtlsRange.Text;
                    }

                    string NORMAL_TYPE = "", NORMAL_RANGE = "", RANGE_TYPE = "";
                    if (txtgvTestReportDtlsResult.Text.Trim() != "")
                    {
                        GetProfileTransValues(lblgvTestReportDtlsProfilID.Text, txtgvTestReportDtlsResult.Text.Trim(), out NORMAL_TYPE, out NORMAL_RANGE, out RANGE_TYPE);

                        if (NORMAL_TYPE != "")
                        {
                            drpgvTestReportDtlsRange.SelectedValue = NORMAL_TYPE;
                        }

                        if (NORMAL_RANGE != "")
                        {
                            txtgvTestReportDtlsNorValue.Text = NORMAL_RANGE;
                        }

                        if (RANGE_TYPE != "")
                        {

                            drpgvTestReportDtlsRangeType.SelectedValue = RANGE_TYPE;
                        }
                    }
                    else
                    {
                        txtgvTestReportDtlsNorValue.Text = GetRangeNormalValue(lblgvTestReportDtlsProfilID.Text, drpgvTestReportDtlsRange.SelectedValue);  // drpgvTestReportDtlsRange.SelectedValue;

                    }


                    if (Convert.ToString(Session["LAB_RESULT_DATAUPLOAD"]) == "1")
                    {
                        if (txtgvTestReportDtlsResult.Text == "")
                        {
                            string RESULT_VALUE;

                            GetTransferTestResultData(lblgvTestReportDtlsProfilID.Text, out RESULT_VALUE, out  NORMAL_TYPE, out NORMAL_RANGE, out RANGE_TYPE);


                            if (RESULT_VALUE != "")
                            {

                                txtgvTestReportDtlsResult.Text = RESULT_VALUE;

                                if (NORMAL_TYPE != "")
                                {
                                    drpgvTestReportDtlsRange.SelectedValue = NORMAL_TYPE;
                                }

                                if (NORMAL_RANGE != "")
                                {
                                    txtgvTestReportDtlsNorValue.Text = NORMAL_RANGE;
                                }

                                if (RANGE_TYPE != "" && drpgvTestReportDtlsRangeType.SelectedIndex == 0)
                                {
                                    drpgvTestReportDtlsRangeType.SelectedValue = RANGE_TYPE;

                                }

                            }

                        }
                        else
                        {
                            if (lblgvTestReportDtlsRangeType.Text != "")
                            {
                                drpgvTestReportDtlsRangeType.SelectedValue = lblgvTestReportDtlsRangeType.Text;
                            }
                        }


                    }
                    else
                    {
                        if (lblgvTestReportDtlsRangeType.Text != "")
                        {
                            drpgvTestReportDtlsRangeType.SelectedValue = lblgvTestReportDtlsRangeType.Text;
                        }
                    }









                    ////string abnormaldecAbrValue = "";

                    ////if (txtgvTestReportDtlsNorValue.Text != "")
                    ////{
                    ////    abnormaldecAbrValue = GetRangeAbnormalValue(lblgvTestReportDtlsProfilID.Text);
                    ////}

                    //if (abnormaldecAbrValue != "" && abnormaldecAbrValue != "0")
                    //{
                    //    decimal decResult = 0, decAbnormal = 0;

                    //    decimal myDec;

                    //    if (txtgvTestReportDtlsResult.Text != "" && abnormaldecAbrValue != "")
                    //    {
                    //        if (decimal.TryParse(txtgvTestReportDtlsResult.Text, out myDec) == true)
                    //        {
                    //            decResult = Convert.ToDecimal(txtgvTestReportDtlsResult.Text);
                    //        }

                    //        if (decimal.TryParse(abnormaldecAbrValue, out myDec) == true)
                    //        {
                    //            decAbnormal = Convert.ToDecimal(abnormaldecAbrValue);
                    //        }

                    //        if (decResult >= decAbnormal)
                    //        {
                    //            lblgvTestReportDtlsAbnorm.Text = "Abnormal";
                    //        }

                    //        else
                    //        {
                    //            lblgvTestReportDtlsAbnorm.Text = "Normal";
                    //        }

                    //    }





                    //}

                    if (Convert.ToString(ViewState["EditedRow"]) != "")
                    {
                        if (e.Row.RowIndex != Convert.ToInt32(ViewState["EditedRow"]))
                        {

                            if (e.Row.RowIndex == Convert.ToInt32(ViewState["EditedRow"]) + 1)
                            {
                                txtgvTestReportDtlsResult.Focus();
                            }
                        }
                    }
                    else
                    {
                        if (e.Row.RowIndex == 0)
                        {
                            txtgvTestReportDtlsResult.Focus();
                        }
                    }



                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvTestReportDtls_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void drpgvTestReportDtlsRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;

                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent;
                Int32 R = currentRow.RowIndex;

                Label lblgvTestReportDtlsProfilID = (Label)currentRow.FindControl("lblgvTestReportDtlsProfilID");

                DropDownList drpgvTestReportDtlsRange = (DropDownList)currentRow.FindControl("drpgvTestReportDtlsRange");
                TextBox txtgvTestReportDtlsNorValue = (TextBox)currentRow.FindControl("txtgvTestReportDtlsNorValue");

                Label lblgvTestReportDtlsRange = (Label)currentRow.FindControl("lblgvTestReportDtlsRange");

                lblgvTestReportDtlsRange.Text = drpgvTestReportDtlsRange.SelectedValue;
                if (drpgvTestReportDtlsRange.SelectedValue != "")
                {
                    txtgvTestReportDtlsNorValue.Text = GetRangeNormalValue(lblgvTestReportDtlsProfilID.Text, drpgvTestReportDtlsRange.SelectedValue);  // drpgvTestReportDtlsRange.SelectedValue;
                }

                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.drpgvTestReportDtlsRange_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void drpgvTestReportDtlsRangeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;

                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent;
                Int32 R = currentRow.RowIndex;

                Label lblgvTestReportDtlsProfilID = (Label)currentRow.FindControl("lblgvTestReportDtlsProfilID");


                DropDownList drpgvTestReportDtlsRangeType = (DropDownList)currentRow.FindControl("drpgvTestReportDtlsRangeType");
                Label lblgvTestReportDtlsRangeType = (Label)currentRow.FindControl("lblgvTestReportDtlsRangeType");

                lblgvTestReportDtlsRangeType.Text = drpgvTestReportDtlsRangeType.Text;


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestReportDtls"];


                DT.Rows[R]["LTRD_RANGE_TYPE"] = drpgvTestReportDtlsRangeType.SelectedValue;

                DT.AcceptChanges();
                ViewState["TestReportDtls"] = DT;


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.drpgvTestReportDtlsRange_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void btnTestMasterSrc_Click(object sender, EventArgs e)
        {
            try
            {
                BindTestReportMasterGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnTestMasterSrc_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSignature_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckPassword(drpSigUser.SelectedValue, txtSigPass.Text.Trim()) == false)
                {

                    lblStatus.Text = "Password wrong";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }

                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {

                    objCom = new CommonBAL();
                    objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objCom.UserID = Convert.ToString(Session["User_ID"]);
                    objCom.AuthorisationAdd(txtTransNo.Text.Trim(), "LABTESTREPORT", drpSigUser.SelectedValue);

                    // Clear();
                    // PTDtlsClear();
                    //  New();
                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSignature_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["TestReportDtls"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["TestReportDtls"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["TestReportDtls"] = DT;

                }


                BindTempTestReportDtls();



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "   Delete_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void gvServSelect_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblServiceID = (Label)gvScanCard.Cells[0].FindControl("lblServiceID");
            Label lblDescription = (Label)gvScanCard.Cells[0].FindControl("lblDescription");
            Label lblTestResult = (Label)gvScanCard.Cells[0].FindControl("lblTestResult");
            Label lblCollectionMethod = (Label)gvScanCard.Cells[0].FindControl("lblCollectionMethod");


            if (lblTestResult.Text.Trim() != "")
            {
                lblStatus.Text = "Test Report already Completed for this Service ID " + lblServiceID.Text;
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            if (CheckSampleDtlsResultStatus(lblServiceID.Text) == true)
            {
                lblStatus.Text = "Test Report already Completed for this Service ID " + lblServiceID.Text;
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }



            txtServCode.Text = lblServiceID.Text;
            txtServName.Text = lblDescription.Text;

            for (int intCount = 0; intCount < drpCollectionMethod.Items.Count; intCount++)
            {
                if (drpCollectionMethod.Items[intCount].Value == lblCollectionMethod.Text.Trim())
                {
                    drpCollectionMethod.SelectedValue = lblCollectionMethod.Text.Trim();

                    goto CollectionMethod;
                }
            }
        CollectionMethod: ;


            BindServiceMasterName();

            DataSet ds = new DataSet();
            objLab = new LaboratoryBAL();

            string Criteria = " 1=1 AND LIPM_STATUS='A' ";
            Criteria += "  AND LIPM_SERV_ID='" + lblServiceID.Text + "'";

            ds = objLab.InvProfileMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                for (int intCount = 0; intCount < drpReportType.Items.Count; intCount++)
                {
                    if (drpReportType.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["LIPM_INVESTIGATION_PROFILE_ID"]))
                    {


                        drpReportType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["LIPM_INVESTIGATION_PROFILE_ID"]);
                        drpReportType_SelectedIndexChanged(drpReportType, new EventArgs());
                        goto ForReportTyp;
                    }
                }
            ForReportTyp: ;




            }
            else
            {

                string strServiceID = "'" + lblServiceID.Text + "'";
                BindLabTestDtlsServiceWise(strServiceID);
                BindTempTestReportDtls();

            }
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideSampleCollServiceList()", true);
        FunEnd: ;

        }

        protected void gvTestReportDtls_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
                Int32 R = currentRow.RowIndex;
                ViewState["EditedRow"] = R;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       gvTestReportDtls_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnLoadResult_Click(object sender, EventArgs e)
        {
            if (txtTestSampleID.Text.Trim() == "")
            {
                lblStatus.Text = "Enter sample ID";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }

            BuildeTestReportDtls();

            Boolean IsLabResult = false, IsActivity = false;
            DataSet DS = new DataSet();

            DS.ReadXml(@GlobalValues.Lab_Result_XMLFilePath + "\\" + txtTestSampleID.Text.Trim() + ".xml");

            if (DS.Tables[0].Rows.Count > 0)
            {


                foreach (DataTable table in DS.Tables)
                {
                    if (table.TableName == "LabResult")
                    {
                        IsLabResult = true;
                    }
                    if (table.TableName == "Activity")
                    {
                        IsActivity = true;
                    }

                }



                if (IsLabResult == false || IsActivity == false)
                {
                    lblStatus.Text = "Incorrect File Format!";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto BindEnd;
                }



                if (DS.Tables["Activity"].Rows.Count > 0)
                {

                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["TestReportDtls"];

                    foreach (DataRow DR in DS.Tables["Activity"].Rows)
                    {

                        DataRow objrow;
                        objrow = DT.NewRow();
                        objrow["LTRD_TEST_PROFILE_ID"] = Convert.ToString(DR["Code"]);
                        objrow["LTRD_TEST_DESCRIPTION"] = Convert.ToString(DR["Description"]);
                        objrow["LTRD_TEST_RESULT"] = Convert.ToString(DR["Result"]);
                        objrow["LTRD_TEST_UNIT"] = Convert.ToString(DR["Unit"]);
                        objrow["LTRD_RANGE"] = Convert.ToString(DR["ReferenceRange"]);


                        DT.Rows.Add(objrow);

                    }

                    ViewState["TestReportDtls"] = DT;


                    BindTempTestReportDtls();
                }


            }

        BindEnd: ;
        }

        protected void txtTestSampleID_TextChanged(object sender, EventArgs e)
        {
            BindTestSampleMaster();

            BuildeTestReportDtls();
            BindTestSampleDtls();
            BindTempTestReportDtls();
        }

        protected void PTDtlsEdit_Click(object sender, EventArgs e)
        {

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePTDtlsPopupTestRpt()", true);

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblPTID = (Label)gvScanCard.Cells[0].FindControl("lblPTID");

            txtFileNo.Text = lblPTID.Text;
            PTDtlsClear();
            PatientDataBind();



        }

        protected void gvPTDtlsTestRpt_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPTDtlsTestRpt.PageIndex = e.NewPageIndex;
            PatientMasterGridGet();
        }

        protected void btnPTSearch_Click(object sender, ImageClickEventArgs e)
        {
            PatientMasterGridGet();
        }

        protected void btnTestRptSearch_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestMasterPopup()", true);
            BindTestReportMasterGrid();
        }



        protected void drpAntibioticsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtAntibioticsMethod.Text = "";
            txtAntibioticsUnit.Text = "";
            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "LAM_STATUS = 'A' and  LAM_CODE='" + drpAntibioticsType.SelectedValue + "'";
            DS = objCommBal.fnGetFieldValue("*", "LAB_ANTIBIOTIC_MASTER", Criteria, "LAM_DESCRIPTION");

            if (DS.Tables[0].Rows.Count > 0)
            {

                txtAntibioticsMethod.Text = Convert.ToString(DS.Tables[0].Rows[0]["LAM_METHOD"]);
                txtAntibioticsUnit.Text = Convert.ToString(DS.Tables[0].Rows[0]["LAM_UNIT"]);
            }
        }


        void ClearMicrobiologyDtls()
        {


            if (drpOrganismType.Items.Count > 0)
                drpOrganismType.SelectedIndex = 0;

            if (drpAntibioticsType.Items.Count > 0)
                drpAntibioticsType.SelectedIndex = 0;

            txtAntibioticsMethod.Text = "";
            txtAntibioticsUnit.Text = "";
            txtAntibioticsValue.Text = "";

            if (drpAntibioticsAbnormalFlag.Items.Count > 0)
                drpAntibioticsAbnormalFlag.SelectedIndex = 0;

            ViewState["gvMicrobiologySelectIndex"] = "";
            lblAntibioticsValidation.Text = "";
        }



        protected void btnMicrobiologyDtlsAdd_Click(object sender, EventArgs e)
        {
            try
            {


                if (drpOrganismType.SelectedIndex == 0)
                {
                    lblAntibioticsValidation.Text = "Select Organism Type";
                    lblAntibioticsValidation.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (drpAntibioticsType.SelectedIndex == 0)
                {
                    lblAntibioticsValidation.Text = "Select Antibiotics Type";
                    lblAntibioticsValidation.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (txtAntibioticsMethod.Text.Trim() == "")
                {
                    lblAntibioticsValidation.Text = "Enter Antibiotics Method.";
                    lblAntibioticsValidation.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (txtAntibioticsUnit.Text.Trim() == "")
                {
                    lblAntibioticsValidation.Text = "Enter Antibiotics Unit.";
                    lblAntibioticsValidation.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (drpAntibioticsAbnormalFlag.Text.Trim() == "")
                {
                    lblAntibioticsValidation.Text = "Enter Antibiotics Value";
                    lblAntibioticsValidation.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (drpAntibioticsAbnormalFlag.SelectedIndex == 0)
                {
                    lblAntibioticsValidation.Text = "Select Abnormal Flag";
                    lblAntibioticsValidation.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestReportMicrobiologyDtls"];


                DataRow objrow;
                objrow = DT.NewRow();

                if (Convert.ToString(ViewState["gvMicrobiologySelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvMicrobiologySelectIndex"]);

                    DT.Rows[R]["LTMD_ORGANISM_TYPE_CODE"] = drpOrganismType.SelectedValue;
                    DT.Rows[R]["LTMD_ORGANISM_TYPE_NAME"] = drpOrganismType.SelectedItem.Text;
                    DT.Rows[R]["LTMD_ANTIBIOTICS_TYPE_CODE"] = drpAntibioticsType.SelectedValue;
                    DT.Rows[R]["LTMD_ANTIBIOTICS_TYPE_NAME"] = drpAntibioticsType.SelectedItem.Text;

                    DT.Rows[R]["LTMD_ANTIBIOTICS_METHOD"] = txtAntibioticsMethod.Text.Trim();
                    DT.Rows[R]["LTMD_ANTIBIOTICS_UNIT"] = txtAntibioticsUnit.Text.Trim();
                    DT.Rows[R]["LTMD_ANTIBIOTICS_VALUE"] = txtAntibioticsValue.Text.Trim();

                    DT.Rows[R]["LTMD_ABNORMAL_FLAG"] = drpAntibioticsAbnormalFlag.SelectedValue;
                    DT.Rows[R]["LTMD_ABNORMAL_FLAG_NAME"] = drpAntibioticsAbnormalFlag.SelectedItem.Text;
                    DT.Rows[R]["LTMD_STATUS"] = drpResultStatus.SelectedValue;

                    DT.AcceptChanges();
                }
                else
                {


                    objrow["LTMD_ORGANISM_TYPE_CODE"] = drpOrganismType.SelectedValue;
                    objrow["LTMD_ORGANISM_TYPE_NAME"] = drpOrganismType.SelectedItem.Text;
                    objrow["LTMD_ANTIBIOTICS_TYPE_CODE"] = drpAntibioticsType.SelectedValue;
                    objrow["LTMD_ANTIBIOTICS_TYPE_NAME"] = drpAntibioticsType.SelectedItem.Text;

                    objrow["LTMD_ANTIBIOTICS_METHOD"] = txtAntibioticsMethod.Text.Trim();
                    objrow["LTMD_ANTIBIOTICS_UNIT"] = txtAntibioticsUnit.Text.Trim();
                    objrow["LTMD_ANTIBIOTICS_VALUE"] = txtAntibioticsValue.Text.Trim();

                    objrow["LTMD_ABNORMAL_FLAG"] = drpAntibioticsAbnormalFlag.SelectedValue;
                    objrow["LTMD_ABNORMAL_FLAG_NAME"] = drpAntibioticsAbnormalFlag.SelectedItem.Text;
                    objrow["LTMD_STATUS"] = drpResultStatus.SelectedValue;


                    DT.Rows.Add(objrow);
                }



                ViewState["TestReportMicrobiologyDtls"] = DT;


                BindTempMicrobiologyDtls();
                txtTestName.Text = "";


                ClearMicrobiologyDtls();

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnMicrobiologyDtlsAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void MicrobiologyDtlsEdit_Click(object sender, EventArgs e)
        {
            ClearMicrobiologyDtls();

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvMicrobiologySelectIndex"] = gvScanCard.RowIndex;
            Label lblgvMicroDtlsOrganismTypeCode = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsOrganismTypeCode");
            Label lblgvMicroDtlsAntibioticsTypeCode = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsTypeCode");

            Label lblgvMicroDtlsAntibioticsMethod = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsMethod");
            Label lblgvMicroDtlsAntibioticsUnit = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsUnit");
            Label lblgvMicroDtlsAntibioticsValue = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlsAntibioticsValue");
            Label lblgvMicroDtlAntibioticsAbnormalFlag = (Label)gvScanCard.Cells[0].FindControl("lblgvMicroDtlAntibioticsAbnormalFlag");

            drpOrganismType.SelectedValue = lblgvMicroDtlsOrganismTypeCode.Text;
            drpAntibioticsType.SelectedValue = lblgvMicroDtlsAntibioticsTypeCode.Text;
            txtAntibioticsMethod.Text = lblgvMicroDtlsAntibioticsMethod.Text;
            txtAntibioticsUnit.Text = lblgvMicroDtlsAntibioticsUnit.Text;
            txtAntibioticsValue.Text = lblgvMicroDtlsAntibioticsValue.Text;
            drpAntibioticsAbnormalFlag.SelectedValue = lblgvMicroDtlAntibioticsAbnormalFlag.Text;

        }

        protected void DeleteeMicroDtls_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton DeleteeMicroDtls = new ImageButton();
                DeleteeMicroDtls = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)DeleteeMicroDtls.Parent.Parent;

                Label lnkgvMicroDtlsSlNo = (Label)gvScanCard.Cells[0].FindControl("lnkgvMicroDtlsSlNo");



                if (Convert.ToString(ViewState["TestReportMicrobiologyDtls"]) != null)
                {

                    ////if (lblAuthorizeStatus.Text == "Authorized" && lnkgvMicroDtlsSlNo.Text != "" && lnkgvMicroDtlsSlNo.Text != "0")
                    ////{
                    ////    //CommonBAL objCom = new CommonBAL();
                    ////    //string Criteria = "LTMD_TEST_REPORT_ID='" + txtTransNo.Text.Trim() + "' AND LTMD_SLNO=" + lnkgvMicroDtlsSlNo.Text.Trim();

                    ////    //objCom.fnUpdateTableData("LTMD_STATUS='D'", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria);

                    ////    Int32 R = gvScanCard.RowIndex;

                    ////    DataTable DT = new DataTable();
                    ////    DT = (DataTable)ViewState["TestReportMicrobiologyDtls"];
                    ////    DT.Rows[R]["LTMD_STATUS"] = "X";
                    ////    DT.AcceptChanges();
                    ////    ViewState["TestReportMicrobiologyDtls"] = DT;
                    ////}
                    ////else
                    ////{
                    ////    DataTable DT = new DataTable();
                    ////    DT = (DataTable)ViewState["TestReportMicrobiologyDtls"];
                    ////    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    ////    DT.AcceptChanges();
                    ////    ViewState["TestReportMicrobiologyDtls"] = DT;
                    ////}


                    if (lblAuthorizeStatus.Text != "Authorized")
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["TestReportMicrobiologyDtls"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["TestReportMicrobiologyDtls"] = DT;
                    }

                }


                BindTempMicrobiologyDtls();
                ClearMicrobiologyDtls();


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "   DeleteeMicroDtls_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnMicrobiologyDtlsClear_Click(object sender, EventArgs e)
        {
            ClearMicrobiologyDtls();
        }



        protected void btnResultDtlsAutoFill_Click(object sender, EventArgs e)
        {
            Int32 intCurRow = 0;
            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TestReportDtls"];
            foreach (GridViewRow DR in gvTestReportDtls.Rows)
            {
                Label lblgvTestReportDtlsInvProfilID = (Label)DR.FindControl("lblgvTestReportDtlsInvProfilID");
                Label lblgvTestReportDtlsAddReference = (Label)DR.FindControl("lblgvTestReportDtlsAddReference");

                TextBox txtgvTestReportDtlsResult = (TextBox)DR.FindControl("txtgvTestReportDtlsResult");
                DropDownList drpgvTestReportDtlsRange = (DropDownList)DR.FindControl("drpgvTestReportDtlsRange");
                TextBox txtgvTestReportDtlsNorValue = (TextBox)DR.FindControl("txtgvTestReportDtlsNorValue");
                TextBox txtgvTestReportDtlsUnit = (TextBox)DR.FindControl("txtgvTestReportDtlsUnit");

                DropDownList drpgvTestReportDtlsRangeType = (DropDownList)DR.FindControl("drpgvTestReportDtlsRangeType");

                Label lblgvTestReportDtlsProfilID = (Label)DR.FindControl("lblgvTestReportDtlsProfilID");

                string NORMAL_TYPE = "", NORMAL_RANGE = "", RANGE_TYPE = "";
                if (txtgvTestReportDtlsResult.Text.Trim() != "")
                {
                    GetProfileTransValues(lblgvTestReportDtlsProfilID.Text, txtgvTestReportDtlsResult.Text.Trim(), out NORMAL_TYPE, out NORMAL_RANGE, out RANGE_TYPE);

                    if (NORMAL_TYPE != "")
                    {
                        drpgvTestReportDtlsRange.SelectedValue = NORMAL_TYPE;
                    }

                    if (NORMAL_RANGE != "")
                    {
                        txtgvTestReportDtlsNorValue.Text = NORMAL_RANGE;
                    }

                    if (RANGE_TYPE != "")
                    {

                        drpgvTestReportDtlsRangeType.SelectedValue = RANGE_TYPE;
                    }
                }




                //   DT.Rows[intCurRow]["LTRD_TEST_RESULT"] = txtgvTestReportDtlsResult.Text;
                DT.Rows[intCurRow]["LTRD_RANGE"] = drpgvTestReportDtlsRange.SelectedValue;
                DT.Rows[intCurRow]["LTRD_TEST_NORMALVALUE"] = txtgvTestReportDtlsNorValue.Text.Trim();
                DT.Rows[intCurRow]["LTRD_TEST_UNIT"] = txtgvTestReportDtlsUnit.Text.Trim();
                DT.Rows[intCurRow]["LTRD_ADDITIONAL_REFERENCE"] = lblgvTestReportDtlsAddReference.Text.Trim();
                DT.Rows[intCurRow]["LTRD_RANGE_TYPE"] = drpgvTestReportDtlsRangeType.SelectedValue;

                DT.AcceptChanges();
                intCurRow += 1;
            }
            ViewState["TestReportDtls"] = DT;
            BindTempTestReportDtls();
        }



        #endregion

        protected void gvMicrobiologyDtls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lnkgvMicroDtlsStatus = (Label)e.Row.Cells[0].FindControl("lnkgvMicroDtlsStatus");

                    Label lblgvMicroDtlsOrganismTypeName = (Label)e.Row.Cells[0].FindControl("lblgvMicroDtlsOrganismTypeName");

                    Label lblgvMicroDtlsAntibioticsTypeName = (Label)e.Row.Cells[0].FindControl("lblgvMicroDtlsAntibioticsTypeName");
                    Label lblgvMicroDtlsAntibioticsMethod = (Label)e.Row.Cells[0].FindControl("lblgvMicroDtlsAntibioticsMethod");
                    Label lblgvMicroDtlsAntibioticsUnit = (Label)e.Row.Cells[0].FindControl("lblgvMicroDtlsAntibioticsUnit");
                    Label lblgvMicroDtlsAntibioticsValue = (Label)e.Row.Cells[0].FindControl("lblgvMicroDtlsAntibioticsValue");
                    Label lblgvMicroDtlAntibioticsAbnormalFlagName = (Label)e.Row.Cells[0].FindControl("lblgvMicroDtlAntibioticsAbnormalFlagName");
                    ImageButton DeleteeMicroDtls = (ImageButton)e.Row.Cells[0].FindControl("DeleteeMicroDtls");

                    if (lnkgvMicroDtlsStatus.Text == "X")
                    {
                        lblgvMicroDtlsOrganismTypeName.Style.Add("text-decoration", "line-through");
                        lblgvMicroDtlsAntibioticsTypeName.Style.Add("text-decoration", "line-through");
                        lblgvMicroDtlsAntibioticsMethod.Style.Add("text-decoration", "line-through");
                        lblgvMicroDtlsAntibioticsUnit.Style.Add("text-decoration", "line-through");
                        lblgvMicroDtlsAntibioticsValue.Style.Add("text-decoration", "line-through");
                        lblgvMicroDtlAntibioticsAbnormalFlagName.Style.Add("text-decoration", "line-through");


                    }



                }


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "   gvMicrobiologyDtls_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}