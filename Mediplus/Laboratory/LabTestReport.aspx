﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="LabTestReport.aspx.cs" Inherits="HMS.Laboratory.LabTestReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        .auto-style1
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            height: 25px;
            width: 242px;
        }

        .auto-style2
        {
            color: #005c7b;
            font-weight: normal;
            font-size: 12px;
            font-family: Arial;
            border-radius: 0px;
            width: 242px;
        }
    </style>


    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ShowServMasterPopup() {

            document.getElementById("divSerMasterPopup").style.display = 'block';
            document.getElementById("divOverlaySerMasterPopup").style.display = 'block';

        }

        function HideServMasterPopup() {

            document.getElementById("divSerMasterPopup").style.display = 'none';
            document.getElementById("divOverlaySerMasterPopup").style.display = 'none';

        }


        function ShowSampleMasterPopup() {

            document.getElementById("divSampleMasterPopup").style.display = 'block';


        }

        function HideSampleMasterPopup() {

            document.getElementById("divSampleMasterPopup").style.display = 'none';

        }



        function ShowSampleCollServiceList() {

            document.getElementById("divSampleCollServiceList").style.display = 'block';
            document.getElementById("divOverlaySampleCollServiceList").style.display = 'block';

        }

        function HideSampleCollServiceList() {
            document.getElementById("divSampleCollServiceList").style.display = 'none';
            document.getElementById("divOverlaySampleCollServiceList").style.display = 'none';
        }





        function ShowTestMasterPopup() {

            document.getElementById("divTestMasterPopup").style.display = 'block';
            document.getElementById("divOverlayeTestMaster").style.display = 'block';


        }

        function HideTestMasterPopup() {

            document.getElementById("divTestMasterPopup").style.display = 'none';
            document.getElementById("divOverlayeTestMaster").style.display = 'none';
        }

        function ShowPTDtlsPopupTestRpt() {
            document.getElementById("divOverlayTestRpt").style.display = 'block';
            document.getElementById("divPTDtlsPopupTestRpt").style.display = 'block';


        }

        function HidePTDtlsPopupTestRpt() {
            document.getElementById("divOverlayTestRpt").style.display = 'none';
            document.getElementById("divPTDtlsPopupTestRpt").style.display = 'none';

        }

        function ShowSignaturePopup() {

            var res = window.confirm('Do you want to Approve?')

            if (res == true) {
                document.getElementById("divSignaturePopup").style.display = 'block';

                return tru
            }

            return false;
        }

        function HideSignaturePopup() {

            document.getElementById("divSignaturePopup").style.display = 'none';

        }
        function LabResultReport(ReportName, TransNo) {


            var Criteria = " 1=1 ";

            if (TransNo != "") {
                Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';
                var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'RadReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                win.focus();
            }

        }
    </script>


    <script language="javascript" type="text/javascript">


        function ShowUserDefined() {

            var TestReportID;

            TestReportID = document.getElementById("<%=txtTransNo.ClientID%>").value;



            var win = window.open("LabTestReportUserDefined.aspx?PageName=LabTestReport&TestReportID=" + TestReportID, "LabTestReportUserDefined", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:UpdatePanel ID="UpdatePanel47" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnNew" runat="server" CssClass="button gray small" Text="New" OnClick="btnNew_Click" />
                        &nbsp;
                 <asp:Label ID="lblPageHeader" runat="server" CssClass="Profile Master" Text="Test Report"></asp:Label>
                        <asp:Button ID="btnLabHistory" runat="server" CssClass="button gray small" Text="History" OnClick="btnLabHistory_Click" Width="70px" />


                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="text-align: right; width: 50%;">

                <asp:UpdatePanel ID="UpPanelSave" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblAuthorizeStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>

                        <asp:Button ID="btnAuthorize" runat="server" CssClass="button gray small" Text="Authorized" OnClick="btnAuthorize_Click" />
                        &nbsp;
                         <asp:Button ID="btnReport" runat="server" CssClass="button gray small" Text="Report" OnClick="btnReport_Click" />
                        &nbsp;
                         <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanelSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel46" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 100%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <div style="padding-top: 0px; width: 80%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                    <td class="lblCaption1" style="width: 150PX;">File No
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFileNo" runat="server" Width="130px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="btnPTSearch" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px; border-radius: 50%; cursor: pointer;" ToolTip="Patient Search" OnClick="btnPTSearch_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" ">Name
                    </td>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFName" runat="server" Width="300px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                            <ContentTemplate>
                                Gender  &nbsp; &nbsp;
                                <asp:TextBox ID="txtSex" runat="server" Width="75px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                            </ContentTemplate>
                         </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1"  >DOB   Age
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtDOB" runat="server"  CssClass="TextBoxStyle" ReadOnly="true" Width="75px"   ></asp:TextBox>&nbsp;
                                  <asp:Label   ID ="txtAge" runat="server"  CssClass="lblCaption1" ></asp:Label> &nbsp;
                                <asp:Label   ID ="lblAgeType" runat="server"  CssClass="lblCaption1" ></asp:Label>&nbsp;
                                <asp:Label  ID ="txtAge1" runat="server"   CssClass="lblCaption1"  ></asp:Label>&nbsp;
                                 <asp:Label ID="lblAgeType1" runat="server"   CssClass="lblCaption1"  ></asp:Label>

                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                </tr>
                   <tr>
                        <td class="lblCaption1" style="width: 150PX;">Nationality</td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" Width="157px" ReadOnly="true"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1"  style="width: 150PX;">Company
                                </td>
                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtProviderID" runat="server" CssClass="TextBoxStyle" ReadOnly="true" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtProviderName" runat="server" Width="300px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                               
                                <td  class="lblCaption1" >
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            Policy No
                                            <asp:TextBox ID="txtPolicyNo" runat="server" Width="75px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            
                                <td class="lblCaption1"  >ID No
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtIDNo" runat="server" Width="155px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                         <tr>
                    
                                 <td class="lblCaption1"  ">Admin ID
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtAdminID" runat="server" Width="155px" CssClass="TextBoxStyle"></asp:TextBox>


                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </td>
                              <td class="lblCaption1"  >Ward
                                </td>
                                <td class="lblCaption1" colspan="2" >
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtWard" runat="server" Width="75px" CssClass="TextBoxStyle"></asp:TextBox>
                                            Room
                                              <asp:TextBox ID="txtRoom" runat="server" Width="75px" CssClass="TextBoxStyle"></asp:TextBox> 
                                              Bed
                                             <asp:TextBox ID="txtBed" runat="server" Width="75px" CssClass="TextBoxStyle"></asp:TextBox>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </td>
                </tr>
                       
                 <tr>
                <td colspan="8" style="height:10PX;">
                    <span class="lblCaption1" style="font-weight: bold;"> Transaction Details </span>
                </td>
            </tr>
                <tr>
                    <td class="lblCaption1" style="width: 150PX;">Trans No
                    </td>
                    <td style="width: 200px;">
                        <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtTransNo" runat="server" Width="130px" Font-Bold="true"  CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtTransNo_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="btnTestRptSearch" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px; border-radius: 50%; cursor: pointer;" ToolTip="Test REport Search" OnClick="btnTestRptSearch_Click" />

                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="width: 120PX">Sample ID
                    </td>
                    <td style="width: 200px;">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtTestSampleID" runat="server" Width="150px" Height="20PX" BackColor="#e3f7ef" CssClass="TextBoxStyle" ondblclick="ShowSampleMasterPopup();" AutoPostBack="true" OnTextChanged="txtTestSampleID_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtTestSampleID" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1" style="width: 120PX">Request ID
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtRequestID" runat="server" Width="150px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1" style="width: 120PX">
                        <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                            <ContentTemplate>
                                Rept Type
                                                <asp:CheckBox ID="chkMultyReptType" Visible="false" runat="server" CssClass="lblCaption1" Text="Multy" Width="120px" Checked="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpReportType" runat="server" Style="width: 155px" class="TextBoxStyle" AutoPostBack="True" OnSelectedIndexChanged="drpReportType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>

                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 25px;">Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtTransDate" runat="server" Width="72px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:DropDownList ID="drpTransHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="36px"></asp:DropDownList>
                                :
                                             <asp:DropDownList ID="drpTransMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="36px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>

                    <td class="lblCaption1" style="height: 25px;">
                        <asp:Label ID="Label4" runat="server" CssClass="label" Text="ServiceCode"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtServCode" runat="server" Width="150px" BackColor="#e3f7ef" Style="height: 20px;" CssClass="TextBoxStyle" ondblclick="ShowServMasterPopup();"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">
                        <asp:Label ID="Label5" runat="server" CssClass="label" Text="Name"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtServName" runat="server" Width="150px" BackColor="#e3f7ef" Style="height: 20px;" CssClass="TextBoxStyle" ondblclick="ShowServMasterPopup();"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="auto-style2">Invoice Ref #
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtInvoiceRefNo" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 25px;">Status
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpStatus" runat="server" Style="width: 167px" class="TextBoxStyle">
                                    <asp:ListItem Value="Process" Selected="True">Process</asp:ListItem>
                                    <asp:ListItem Value="Outside">Outside</asp:ListItem>
                                    <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                    <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                                    <asp:ListItem Value="Retest">Retest</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1" style="height: 25px;">
                        <asp:Label ID="Label1" runat="server" CssClass="label" Text="Caption"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCaption" runat="server" Width="150px" Style="height: 20px;" CssClass="TextBoxStyle" ondblclick="ShowServMasterPopup();"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>


                    <td class="lblCaption1" style="height: 25px;">Barcode ID
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtBarecodeID" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle" Enabled="False"></asp:TextBox>

                                <asp:TextBox ID="txtFee" Visible="false" runat="server" Width="90%" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">EMR #
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel48" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtEMRID" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 25px;">Type
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="167px">
                                    <asp:ListItem Value="Cash" Selected="True">Cash</asp:ListItem>
                                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>

                    <td>
                        <asp:Label ID="Label2" runat="server" CssClass="lblCaption1" Text="Source"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpSource" runat="server" Style="width: 153px" class="TextBoxStyle">
                                    <asp:ListItem Value="Inside" Selected="True">Inside</asp:ListItem>
                                    <asp:ListItem Value="Outside">Outside</asp:ListItem>

                                </asp:DropDownList>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSourceName" runat="server" Width="100%" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 25px;">PT. Type
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpPTType" runat="server" CssClass="TextBoxStyle" Width="167px" BorderWidth="1px">
                                    <asp:ListItem Value="OP">OP</asp:ListItem>
                                    <asp:ListItem Value="IP">IP</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Sample Coll.
                                    
                                                        
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSampleCollDate" runat="server" Width="72px" Height="20px" Enabled="false" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtSampleCollDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:DropDownList ID="drpCollHour" Style="font-size: 11px;" Enabled="false" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                <asp:DropDownList ID="drpCollMin" Style="font-size: 11px;" Enabled="false" CssClass="TextBoxStyle" runat="server" Width="35px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="height: 25px;">Deliv. Date 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel61" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtDeliveryDate" runat="server" Width="65px" CssClass="TextBoxStyle" Enabled="false" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" TargetControlID="txtDeliveryDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:DropDownList ID="drpDelivHour" Style="font-size: 11px;" Enabled="false" CssClass="TextBoxStyle" runat="server" Width="35px"></asp:DropDownList>

                                <asp:DropDownList ID="drpDelivMin" Style="font-size: 11px;" Enabled="false" CssClass="TextBoxStyle" runat="server" Width="35px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                    <td class="lblCaption1" style="height: 25px;">Collection Method
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel49" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpCollectionMethod" runat="server" CssClass="TextBoxStyle" Width="150px" BorderWidth="1px">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 25px;" valign="top">Dr. Name
                    </td>


                    <td valign="top">
                        <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpDoctors" runat="server" Style="width: 167px" CssClass="TextBoxStyle"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="height: 25px;">Diagnostic Serv
                                   
                    </td>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpDiagnosticService" runat="server" Style="width: 305px" CssClass="TextBoxStyle">
                                </asp:DropDownList>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                   
                                    
                                                        
                    
                      <td class="lblCaption1" colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel50" runat="server">
                            <ContentTemplate>
                                Result Status
                                <asp:DropDownList ID="drpResultStatus" runat="server" Style="width: 305px" CssClass="TextBoxStyle">
                                </asp:DropDownList>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 25px;">Ref. Doctor
                    </td>

                    <td>
                        <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpRefDoctor" runat="server" Style="width: 167px" CssClass="TextBoxStyle">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">External Lab </td>
                    <td colspan="1">
                        <asp:UpdatePanel ID="UpdatePanel51" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpExternalLab" runat="server" Style="width: 150px" class="TextBoxStyle">
                                </asp:DropDownList>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Reference #
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel52" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtExtLabReferenceNo" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1"> 
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="chkCriticalResult" runat="server"  CssClass="lblCaption1"  Text="Critical Result" Checked="false"  />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>

        <table style="padding: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="width: 85%; vertical-align: top;">
                    <table width="85%" border="0" cellpadding="3" cellspacing="3">
                         <tr>
                            <td class="lblCaption1" colspan="2"> Test Code & Description
                                <span class="lblCaption1" style="height: 25px; width: 100px; text-align: center; letter-spacing: 1px;padding-left:400px">
                                    LAB TEST DETAILS
                                </span>

                            </td>
                        </tr>
                          <tr>
                            
                            <td  >
                                <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                                    <ContentTemplate>
                                          <asp:TextBox ID="txtTestName" runat="server" Width="400px" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtTestName" MinimumPrefixLength="1" ServiceMethod="GetTestProfileMaster"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>

                                        <asp:Button ID="btnLoadResult" runat="server" CssClass="button gray small" Text="Load Result" OnClick="btnLoadResult_Click" Width="100px" />
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button red small" Text="Add" OnClick="btnAdd_Click" Width="70px" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td >

                                <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                    <ContentTemplate>
                                         <a id="lnkSampleCollServiceList" runat="server" class="lblCaption1" onclick="ShowSampleCollServiceList()" style="cursor: pointer; color: #005c7b; font-weight: bold;" >Sample Collection  Serv, List...</a>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>

                    <div style="padding-top: 0px; width: 80%; overflow: auto; height: 400px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                        <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvTestReportDtls" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowDataBound="gvTestReportDtls_RowDataBound"
                                    EnableModelValidation="True" Width="100%">
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="Test Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton9" CssClass="lblCaption1" runat="server" OnClick="TestReportDtlsEdit_Click">
                                                    <asp:Label ID="Label3" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_TEST_REPORT_ID") %>' Visible="false"></asp:Label>



                                                    <asp:Label ID="lblgvTestReportDtlsInvProfilID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_INVESTIGATION_PROFILE_ID") %>' Visible="false"></asp:Label>

                                                    <asp:Label ID="lblgvTestReportDtlsProfilID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_TEST_PROFILE_ID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton10" CssClass="lblCaption1" runat="server" OnClick="TestReportDtlsEdit_Click">
                                                    <asp:Label ID="lblgvTestReportDtlsDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_TEST_DESCRIPTION") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Addi. Refe." HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvTestReportDtlsAddReference" CssClass="lblCaption1" runat="server" OnClick="TestReportDtlsEdit_Click">
                                                    <asp:Label ID="lblgvTestReportDtlsAddReference" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_ADDITIONAL_REFERENCE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Result" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>

                                                <asp:TextBox ID="txtgvTestReportDtlsResult" CssClass="TextBoxStyle" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_TEST_RESULT") %>' AutoPostBack="true" OnTextChanged="gvTestReportDtls_TextChanged"></asp:TextBox>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ref:Range" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>

                                                <asp:Label ID="lblgvTestReportDtlsRange" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_RANGE") %>' Visible="false"></asp:Label>
                                                <asp:DropDownList ID="drpgvTestReportDtlsRange" runat="server" CssClass="TextBoxStyle" Width="70px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpgvTestReportDtlsRange_SelectedIndexChanged">
                                                </asp:DropDownList>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Normal Value" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>

                                                <asp:TextBox ID="txtgvTestReportDtlsNorValue" CssClass="TextBoxStyle" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_TEST_NORMALVALUE") %>'></asp:TextBox>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>

                                                <asp:TextBox ID="txtgvTestReportDtlsUnit" CssClass="TextBoxStyle" Width="70px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_TEST_UNIT") %>'></asp:TextBox>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Abnormal Flag" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>

                                                <asp:Label ID="lblgvTestReportDtlsRangeType" CssClass="lblCaption1" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_RANGE_TYPE") %>' Visible="false"></asp:Label>

                                                <asp:DropDownList ID="drpgvTestReportDtlsRangeType" runat="server" CssClass="TextBoxStyle" Width="70px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpgvTestReportDtlsRangeType_SelectedIndexChanged">
                                                    <asp:ListItem Text="---Select---" Value="" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="L" Text="Low"></asp:ListItem>
                                                    <asp:ListItem Value="H" Text="High"></asp:ListItem>
                                                    <asp:ListItem Value="LL" Text="Below lower panic limits"></asp:ListItem>
                                                    <asp:ListItem Value="HH" Text="Above upper panic limits"></asp:ListItem>
                                                    <asp:ListItem Value="N" Text="Normal"></asp:ListItem>
                                                    <asp:ListItem Value="A" Text="Abnormal"></asp:ListItem>
                                                    <asp:ListItem Value="AA" Text="Very abnormal"></asp:ListItem>
                                                    <asp:ListItem Value="U" Text="Significant change up"></asp:ListItem>
                                                    <asp:ListItem Value="D" Text="Significant change down"></asp:ListItem>
                                                    <asp:ListItem Value="B" Text="Better"></asp:ListItem>
                                                    <asp:ListItem Value="W" Text="Worse"></asp:ListItem>
                                                    <asp:ListItem Value="S" Text="Susceptible"></asp:ListItem>
                                                    <asp:ListItem Value="R" Text="Resistant"></asp:ListItem>
                                                    <asp:ListItem Value="I" Text="Intermediate"></asp:ListItem>
                                                    <asp:ListItem Value="MS" Text="Moderately susceptible"></asp:ListItem>
                                                    <asp:ListItem Value="VS" Text="Very susceptible"></asp:ListItem>
                                                    <asp:ListItem Value="C" Text="Critical"></asp:ListItem>


                                                </asp:DropDownList>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                    OnClick="Delete_Click" />&nbsp;&nbsp;
                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Obsev Nedded" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblObsvNeeded" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_OBSV_NEEDED") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Obsev Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblObsvType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_OBSV_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Obsev Code" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblObsvCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRD_OBSV_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <br />
                    <table width="85%" border="0" cellpadding="5" cellspacing="5">
                        <tr>

                            <td class="lblCaption1" colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel53" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="chkMicroorganismIdentified" runat="server" CssClass="lblCaption1" Font-Bold="true" Checked="false" Text="Microorganism Identified" />
                                        &nbsp;&nbsp;
                                         <asp:Label ID="lblAntibioticsValidation" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                    </table>

                    <table width="85%" border="0" cellpadding="5" cellspacing="5" st>
                        <tr>
                            <td class="lblCaption1">Organism Type
                            </td>
                            <td class="lblCaption1">Antibiotics Type
                            </td>
                            <td class="lblCaption1">Method
                            </td>
                            <td class="lblCaption1">Unit
                            </td>
                            <td class="lblCaption1">Value
                            </td>
                            <td class="lblCaption1">Abnormal Flag
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel54" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpOrganismType" runat="server" Style="width: 150px" class="TextBoxStyle">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel55" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpAntibioticsType" runat="server" Style="width: 150px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpAntibioticsType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel56" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAntibioticsMethod" runat="server" Enabled="false" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel57" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAntibioticsUnit" runat="server" Enabled="false" CssClass="TextBoxStyle" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel58" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAntibioticsValue" runat="server" CssClass="TextBoxStyle" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel62" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpAntibioticsAbnormalFlag" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px">
                                            <asp:ListItem Text="---Select---" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="S" Text="Susceptible"></asp:ListItem>
                                            <asp:ListItem Value="R" Text="Resistant"></asp:ListItem>
                                            <asp:ListItem Value="I" Text="Intermediate"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel59" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnMicrobiologyDtlsAdd" runat="server" Text="Add" CssClass="button red" Width="50px" OnClick="btnMicrobiologyDtlsAdd_Click" />
                                        <asp:Button ID="btnMicrobiologyDtlsClear" runat="server" Text="Clear" CssClass="button gray" Width="50px" OnClick="btnMicrobiologyDtlsClear_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <div style="padding-top: 0px; width: 80%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                        <asp:UpdatePanel ID="UpdatePanel60" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvMicrobiologyDtls" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%">
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>

                                        <asp:TemplateField HeaderText="Organism Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvMicroDtlsOrganismTypeCode" CssClass="lblCaption1" runat="server" OnClick="MicrobiologyDtlsEdit_Click">
                                                    <asp:Label ID="lblgvMicroDtlsReportID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_TEST_REPORT_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lnkgvMicroDtlsSlNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_SLNO") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lnkgvMicroDtlsStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_STATUS") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvMicroDtlsOrganismTypeCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ORGANISM_TYPE_CODE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvMicroDtlsOrganismTypeName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ORGANISM_TYPE_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Antibiotics Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvMicroDtlsAntibioticsTypeCode" CssClass="lblCaption1" runat="server" OnClick="MicrobiologyDtlsEdit_Click">
                                                    <asp:Label ID="lblgvMicroDtlsAntibioticsTypeCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ANTIBIOTICS_TYPE_CODE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvMicroDtlsAntibioticsTypeName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ANTIBIOTICS_TYPE_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Method" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvMicroDtlsAntibioticsMethod" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ANTIBIOTICS_METHOD") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvMicroDtlsAntibioticsUnit" CssClass="label" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ANTIBIOTICS_UNIT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Value" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvMicroDtlsAntibioticsValue" CssClass="label" Width="70px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ANTIBIOTICS_VALUE") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Abnormal Flag" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvMicroDtlAntibioticsAbnormalFlag" CssClass="label" Width="70px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ABNORMAL_FLAG") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvMicroDtlAntibioticsAbnormalFlagName" CssClass="label" Width="70px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTMD_ABNORMAL_FLAG_NAME") %>'></asp:Label>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteeMicroDtls" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                    OnClick="DeleteeMicroDtls_Click" />&nbsp;&nbsp;
                                            </ItemTemplate>

                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <br />
                    <table width="85%" border="0" cellpadding="5" cellspacing="5">
                        <tr>
                            <td class="lblCaption1" colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                    <ContentTemplate>
                                        Remarks
                                        <br />
                                        <asp:TextBox ID="txtRemarks" runat="server" Width="96%" Height="50px" Style="resize: none;" CssClass="TextBoxStyle" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1" style="display: none;">Report Type
                            </td>
                            <td style="display: none;">
                                <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpReportCategory" runat="server" CssClass="TextBoxStyle" Width="200px" BorderWidth="1px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                            <td class="lblCaption1" style="height: 30px;"></td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtTotalAmount" Visible="false" runat="server" Width="100px" CssClass="TextBoxStyle" Style="text-Align: right;" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="display: none;">
                                <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="chkInlineEditing" runat="server" Text="Inline Editing" class="lblCaption1"></asp:CheckBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                    </table>
                </td>
                
            </tr>
        </table>

    </div>
     <div id="divOverlaySampleCollServiceList" class="Overlay" style="display: none;">
     <div id="divSampleCollServiceList" style="display: none; overflow: hidden; border: groove; height: 370px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 420px; top: 370px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                 <td style="vertical-align: top; width: 50%;">
                     <span class="lblCaption1" style="font-weight:bold;"> Sample Collection Service List </span> 

                </td>
                 <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button6" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideSampleCollServiceList()" />
                </td>
           </tr>
        </table>
          <div style="height:5px;border-bottom-color:red;border-width:1px;border-bottom-style:groove;"></div>
           <div style="height:5px;"></div>

          <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvServiceList" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="100%">
                                <HeaderStyle CssClass="GridHeader_Blue" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <Columns>
                                     <asp:TemplateField HeaderText="CODE " HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="gvServSelectCode" CssClass="lblCaption1" runat="server" OnClick="gvServSelect_Click">
                                                <asp:Label ID="lblID" CssClass="GridRow" runat="server" Text='<%# Bind("ID") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lblTestResult" CssClass="GridRow" runat="server" Text='<%# Bind("TestResult") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblCollectionMethod" CssClass="GridRow" runat="server" Text='<%# Bind("CollectionMethod") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lblServiceID" CssClass="GridRow" runat="server" Text='<%# Bind("ServiceID") %>'  ></asp:Label> &nbsp;

                                            </asp:LinkButton>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DESCRIPTION ">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="gvServSelect" CssClass="lblCaption1" runat="server" OnClick="gvServSelect_Click">
                                                 <asp:Label ID="lblDescription" CssClass="GridRow" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
 
                                            </asp:LinkButton>

                                        </ItemTemplate>

                                    </asp:TemplateField>



                                </Columns>

                            </asp:GridView>

                        </ContentTemplate>

                    </asp:UpdatePanel>

         </div>

         </div>

    <div id="divOverlaySerMasterPopup" class="Overlay" style="display: none;">
    <div id="divSerMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 320px; top: 270px;">
        
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                 <td style="vertical-align: top; width: 50%;">
                     <span class="lblCaption1" style="font-weight:bold;"> Service Master List </span> 

                </td>
                 <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideServMasterPopup()" />
                </td>
           </tr>
        </table>
          <div style="height:5px;border-bottom-color:red;border-width:1px;border-bottom-style:groove;"></div>
           <div style="height:5px;"></div>
        <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">


                        <asp:GridView ID="gvServMasterPopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Service ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkServID" CssClass="lblCaption1" runat="server" OnClick="ServMasterEdit_Click">
                                            <asp:Label ID="lblServID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkServName" CssClass="lblCaption1" runat="server" OnClick="ServMasterEdit_Click">
                                            <asp:Label ID="lblServName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fee" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkServFee" CssClass="lblCaption1" runat="server" OnClick="ServMasterEdit_Click">
                                            <asp:Label ID="lblServFee" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_FEE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkServType" CssClass="lblCaption1" runat="server" OnClick="ServMasterEdit_Click">
                                            <asp:Label ID="lblServType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_TYPE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </td>

                </tr>

            </table>
        </div>
    </div>
        </div>
    
    <div id="divSampleMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 420px; top: 170px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top;"></td>
                <td align="right" style="vertical-align: top;">

                    <input type="button" id="Button2" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideSampleMasterPopup()" />
                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">


                        <asp:GridView ID="gvSampleMasterPopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Sample ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="lblgvSampMasSampleID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_TEST_SAMPLE_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton2" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_DATEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Patient" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasPTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton4" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasPTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton5" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasRepType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_REP_TYPE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton6" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasAge" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_AGE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton7" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasAgeType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_AGETYPE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Age Type" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" CssClass="lblCaption1" runat="server" OnClick="SampleMasterEdit_Click">
                                            <asp:Label ID="gvSampMasSex" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_SEX") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </td>

                </tr>

            </table>
        </div>
    </div>
          

    <div id="divOverlayeTestMaster" class="Overlay" style="display: none;">

        <div id="div3" style="height: 550px; width: 1100px; position: fixed; left: 150px; top: 250px;">
            <img src='<%= ResolveUrl("~/Images/TopMenuIcons/Registration.png")%>' style="border: none; width: 40px; height: 40px;" />
            <span style="color: white; font-size: 17px;" class="label">Test Report  
            </span>
            <div id="divTestMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; position: fixed; left: 150px; top: 300px;">
                <div id="div5" style="height: 5px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px; position: absolute; top: 0px; left: 0px;">
                </div>
                <div style="width: 100%; text-align: right; float: right; position: absolute; top: -0px; right: 0px;">
                    <img src='<%= ResolveUrl("~/Images/Close.png")%>' style="height: 25px; width: 25px; cursor: pointer;" onclick="HideTestMasterPopup()" />
                </div>
                <input type="button" class="PageSubHeaderBlue" style="width: 150px;" value=" Select Test Result" disabled="disabled" />
                <img src='<%= ResolveUrl("~/Images/TabBlueCornor.png")%>' style="border: none; height: 20px; width: 37px;" />

                <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto;">

                    <table cellpadding="5" cellspacing="5" width="100%" style="display: none;">
                        <tr>
                            <td class="lblCaption1" style="height: 30px;">Date
                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcFromDate" runat="server" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                    Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtSrcFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                            </td>
                            <td class="lblCaption1">To Date

                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcToDate" runat="server" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                    Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtSrcToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                            </td>
                            <td class="lblCaption1">Age From 
                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcAgeFrom" runat="server" Width="30px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">To
                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcAgeTo" runat="server" Width="30px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 30px;">File Number
                            </td>
                            <td>
                                <asp:TextBox ID="txtSrcFileNo" runat="server" Width="100px" MaxLength="10" CssClass="label"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Patient Name
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="txtSrcName" runat="server" Width="400px" CssClass="label"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 30px;">Test Name
                            </td>
                            <td colspan="6">
                                <asp:TextBox ID="txtSrcTestName" runat="server" Width="90%" MaxLength="10" CssClass="label"></asp:TextBox>
                            </td>
                            <td>

                                <asp:Button ID="btnTestMasterSrc" runat="server" CssClass="button gray small" Text="Display" OnClick="btnTestMasterSrc_Click" />
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <div style="padding-top: 0px; width: 100%; height: 350px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                        <ContentTemplate>

                                            <asp:GridView ID="gvTestMasterPopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                EnableModelValidation="True" Width="100%">
                                                <HeaderStyle CssClass="GridHeader_Blue" />
                                                <RowStyle CssClass="GridRow" />
                                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Report ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton11" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasReportID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_TEST_REPORT_ID") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton12" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_TEST_DATEDesc") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Patient" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton13" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasPTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_ID") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton14" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasPTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_NAME") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Caption" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton15" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasCaption" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_CAPTION") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Age" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton16" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasPTAge" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_AGE") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Age Type" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton17" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasPTAgeType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_AGETYPE") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sex" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton18" CssClass="lblCaption1" runat="server" OnClick="TestMasterEdit_Click">
                                                                <asp:Label ID="lblgvTestMasPTSex" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_SEX") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="gvTestMasterPopup" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>

                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divSignaturePopup" style="display: none; overflow: hidden; border: groove; height: 200px; width: 300px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 320px; top: 170px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top;"></td>
                <td align="right" style="vertical-align: top;">

                    <input type="button" id="Button4" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideSignaturePopup()" />
                </td>
            </tr>
        </table>

        <table width="100%">

            <tr>
                <td style="width: 50%">
                    <fieldset>
                        <legend class="lblCaption1">Signature </legend>
                        <table width="100%">
                            <tr>

                                <td class="lblCaption1" style="width: 150px; height: 30px;">Name  
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpSigUser" runat="server" CssClass="TextBoxStyle" Width="205px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="width: 150px; height: 30px;">Password
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSigPass" runat="server" CssClass="label" TextMode="Password" Width="195px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1"></td>
                                <td>
                                    <asp:Button ID="btnSignature" runat="server" CssClass="button gray small" Text="Authorise" OnClick="btnSignature_Click" />

                                </td>
                            </tr>

                        </table>


                    </fieldset>
                </td>
                <td style="width: 50%"></td>
            </tr>
        </table>

    </div>

    <div id="divOverlayTestRpt" class="Overlay" style="display: none;">

        <div id="div1" style="height: 550px; width: 1100px; position: fixed; left: 150px; top: 250px;">
            <img src='<%= ResolveUrl("~/Images/TopMenuIcons/Registration.png")%>' style="border: none; width: 40px; height: 40px;" />
            <span style="color: white; font-size: 17px;" class="label">Patient  
            </span>
            <div id="divPTDtlsPopupTestRpt" style="display: none; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; position: fixed; left: 150px; top: 300px;">
                <div id="divHeaderLine" style="height: 5px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px; position: absolute; top: 0px; left: 0px;">
                </div>
                <div style="width: 100%; text-align: right; float: right; position: absolute; top: -0px; right: 0px;">
                    <img src='<%= ResolveUrl("~/Images/Close.png")%>' style="height: 25px; width: 25px; cursor: pointer;" onclick="HidePTDtlsPopupTestRpt()" />
                </div>
                <input type="button" class="PageSubHeaderBlue" style="width: 150px;" value=" Select Patient" disabled="disabled" />
                <img src='<%= ResolveUrl("~/Images/TabBlueCornor.png")%>' style="border: none; height: 20px; width: 37px;" />

                <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">

                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                                    <ContentTemplate>

                                        <asp:GridView ID="gvPTDtlsTestRpt" runat="server" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%"
                                            AllowPaging="true" PageSize="10" OnPageIndexChanging="gvPTDtlsTestRpt_PageIndexChanging">
                                            <HeaderStyle CssClass="GridHeader_Gray" />
                                            <RowStyle CssClass="GridRow" />
                                            <AlternatingRowStyle CssClass="GridAlterRow" />

                                            <Columns>
                                                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                            OnClick="PTDtlsEdit_Click" />&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="File No" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblPTID" CssClass="GridRow" Style="float: left;" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sex" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblPTSex" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_SEX") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pt. Type" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblPtType" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Company" SortExpression="HPM_PT_TYPEEDesc">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblCompName" CssClass="label" runat="server" Text='<%# Bind("HPM_INS_COMP_NAME") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Policy No." SortExpression="HPM_POLICY_NO">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblgvPolicyNo" CssClass="label" runat="server" Text='<%# Bind("HPM_POLICY_NO") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User" SortExpression="HPM_CREATED_USER">
                                                    <ItemTemplate>

                                                        <span style="float: left;">
                                                            <asp:Label ID="lblCrUser" CssClass="label" runat="server" Text='<%# Bind("HPM_CREATED_USER") %>'></asp:Label>
                                                        </span>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <PagerStyle CssClass="GridFooterStyle" Font-Bold="true" HorizontalAlign="Right" />
                                        </asp:GridView>

                                    </ContentTemplate>

                                </asp:UpdatePanel>
                            </td>

                        </tr>

                    </table>
                </div>

            </div>
        </div>
    </div>


</asp:Content>
