﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;


namespace HMS.Laboratory
{
    public partial class LabTestInvestProfileUserDefined : System.Web.UI.Page
    {
        LaboratoryBAL objLab = new LaboratoryBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public string getURL()
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl("~/");
        }

        void BindData()
        {
            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1  and LUDF_DATA_LOAD_FROM='WEB' ";
            Criteria += " AND  LUDF_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LUDF_CODE ='" + Convert.ToString(ViewState["InvestProfileID"]) + "'";

            DS = objLab.UserDefinedFormatGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;
                txtContent.Text = Server.HtmlDecode(Convert.ToString(DS.Tables[0].Rows[0]["LUDF_TEST_RESULT"]));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    ViewState["NewFlag"] = true;

                    hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                    ViewState["InvestProfileID"] = Convert.ToString(Request.QueryString["InvestProfileID"]);
                    ViewState["Description"] = Convert.ToString(Request.QueryString["Description"]);

                    BindData();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      LabTestInvestProfilePopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(ViewState["InvestProfileID"]) == "")
            {
                lblStatus.Text = "Profile Not Selected";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            objLab = new LaboratoryBAL();


            IDictionary<string, string> Param = new Dictionary<string, string>();
            objLab.LUDF_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objLab.LUDF_CODE = Convert.ToString(ViewState["InvestProfileID"]);
            objLab.LUDF_DESCRIPTION = Convert.ToString(ViewState["Description"]);

            string tempStr = "";
            tempStr = Server.HtmlEncode(txtContent.Text);


            objLab.LUDF_TEST_RESULT = tempStr;


            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }



            objLab.UserID = Convert.ToString(Session["User_ID"]);

            objLab.UserDefinedFormatAdd();

            lblStatus.Text = "Data Saved";
            lblStatus.ForeColor = System.Drawing.Color.Green;

        FunEnd: ;
        }


    }
}