﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="LabTestSampleWaitingList.aspx.cs" Inherits="Mediplus.Laboratory.LabTestSampleWaitingList" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>
    <table width="90%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Test Waiting List"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;"></td>
        </tr>
    </table>
    <table width="80%" border="0" cellpadding="3" cellspacing="3">

        <tr>

            <td class="lblCaption1" style="height: 25px;">From Date
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTransFromDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransFromDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1" style="height: 25px;">To Date
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTransToDate" runat="server" Width="70px" Height="20PX" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtTransToDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1" style="height: 25px;">File No
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFileNo" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 25px;">Name
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFName" runat="server" Width="150px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 25px;">Status
            </td>
            <td style="width: 200px;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpWaitListStatus" runat="server" Style="width: 120px" class="TextBoxStyle">
                            <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                            <asp:ListItem Value="Authorized">Authorized</asp:ListItem>
                            <asp:ListItem Value="Not Authorized">Not Authorized</asp:ListItem>
                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td>

                <asp:Button ID="btnShow" runat="server" CssClass="button red small" Text="Show" OnClick="btnShow_Click" />
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvLabSamplePopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" Visible="false">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />

                    <Columns>

                        <asp:TemplateField HeaderText="Sample ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSampleID" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblDoctorID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_DR_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvLabSampleID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_TEST_SAMPLE_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSampleDate" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblgvLabSampleDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_DATEDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSamplePTID" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblgvLabSamplePTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSamplePTName" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblgvLabSamplePTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_PATIENT_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ref. Doctor" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSampleDrName" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblgvLabSampleDrName" CssClass="GridRow" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_DR_NAME") %>' Visible="false" ></asp:Label>
                                     <asp:Label ID="lblgvLabSamplRefDrName" CssClass="GridRow" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_REF_DR_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BarCode" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSampleBareCode" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblgvLabSampleBareCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_BARECODE") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Created User" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabSampleUser" CssClass="lblCaption1" runat="server" OnClick="LabSampleEdit_Click">
                                    <asp:Label ID="lblgvLabSampleUser" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSM_CREATED_USER") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>


                <asp:GridView ID="gvLabTestReport" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" Visible="false">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />

                    <Columns>
                        <asp:TemplateField HeaderText="Report ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton12" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">

                                    <asp:Label ID="lblgvLabTestReportID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_TEST_REPORT_ID") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sample ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestDRCode" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestDRCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_DR_CODE") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="lblgvLabTestSampleID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_SAMPLE_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Caption" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestCaption" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestCaption" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_CAPTION") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestDate" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_TEST_DATEDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestPTID" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestPTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestPTName" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestPTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ref. Doctor" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestDRName" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestDRName" CssClass="GridRow" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_DR_NAME") %>' Visible="false" ></asp:Label>
                                    <asp:Label ID="lblgvLabTestRefDRName" CssClass="GridRow" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_REF_DR_NAME") %>'  ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BarCode" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvLabTestBareCode" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblgvLabTestBareCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_BARECODE") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Verified By" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkVerifiedBy" CssClass="lblCaption1" runat="server" OnClick="LabTestReportEdit_Click">
                                    <asp:Label ID="lblVerifiedBy" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_VERIFIED_BY") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                    </Columns>
                </asp:GridView>



            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <table width="100%" border="0" cellpadding="3" cellspacing="3">

        <tr>


            <td>
                <asp:Button ID="btnEMRRRadRequest" runat="server" CssClass="button gray small" Text="EMR Request" OnClick="btnEMRRRadRequest_Click" />

                <asp:Button ID="btnLabTestRegister" runat="server" CssClass="button red small" Text="Lab Test Result" OnClick="btnLabTestRegister_Click" />


            </td>

        </tr>

    </table>

     
    <div id="divTestResultDataPopup" style="display: none; overflow:auto ; border: groove; height: 450px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 420px; top: 200px;">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;"></td>
                <td align="right" style="vertical-align: top; width: 50%;">
                    <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideTestResultDataPopup()" />
                </td>
            </tr>
        </table>
         <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>
        <asp:GridView ID="gvTestResultData" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="100%" Visible="false">
            <HeaderStyle CssClass="GridHeader_Blue" />
            <RowStyle CssClass="GridRow" />

            <Columns>
                <asp:TemplateField HeaderText="Test Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                          <%# Eval("HIS_TestCode")  %>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Test Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                          <%# Eval("HIS_TestName")  %>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Param Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                          <%# Eval("HIS_ParamCode")  %>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Param Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                          <%# Eval("HIS_ParamName")  %>
                    </ItemTemplate>
                </asp:TemplateField>

                   <asp:TemplateField HeaderText="Result" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                          <%# Eval("Analyzer_Result")  %>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
                  </ContentTemplate>
        </asp:UpdatePanel>

    </div>
               
    <script language="javascript" type="text/javascript">
        function ShowTestResultDataPopup() {

            document.getElementById("divTestResultDataPopup").style.display = 'block';


        }

        function HideTestResultDataPopup() {

            document.getElementById("divTestResultDataPopup").style.display = 'none';

        }

    </script>

</asp:Content>

