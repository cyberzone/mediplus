﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="LabTestRegister.aspx.cs" Inherits="Mediplus.Laboratory.LabTestRegister" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

      
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>
      <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

      <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ShowServMasterPopup() {

            document.getElementById("divSerMasterPopup").style.display = 'block';


        }

        function HideServMasterPopup() {

            document.getElementById("divSerMasterPopup").style.display = 'none';

        }


        function ShowSampleMasterPopup() {

            document.getElementById("divSampleMasterPopup").style.display = 'block';


        }

        function HideSampleMasterPopup() {

            document.getElementById("divSampleMasterPopup").style.display = 'none';

        }



        function ShowTestMasterPopup() {

            document.getElementById("divTestMasterPopup").style.display = 'block';


        }

        function HideTestMasterPopup() {

            document.getElementById("divTestMasterPopup").style.display = 'none';

        }








    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <input type="hidden" id="hidPermission" runat="server" value="9" />
          <table width="80%">
                <tr>
                    <td style="text-align: left; width: 50%;" class="PageHeader"> 
                       <asp:Button ID="btnNew" runat="server" CssClass="button gray small" Text="New" OnClick="btnNew_Click"     /> &nbsp;
                        &nbsp;
                 <asp:Label ID="lblPageHeader" runat="server" CssClass="Profile Master" Text="Test Register"></asp:Label>
                    </td>
                    <td style="text-align: right; width: 50%;">
                           <asp:UpdatePanel ID="UpPanalSave" runat="server">
                    <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                       </ContentTemplate>

                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanalSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
    <br />
     <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
                <tr>
                    <td>
                         <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
          <div style="padding-top: 0px; width: 80%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="290px">
                    <tr>
                        <td style="width: 20%;vertical-align:top;">

                            <fieldset style="padding:5px; ">
                                <legend class="lblCaption1" style="font-weight: bold;">Transaction</legend>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                    <td class="lblCaption1" style="height: 25px;">Reg ID
                                    </td>
                                    <td>
                                      <asp:updatepanel id="UpdatePanel18" runat="server">
                                                        <ContentTemplate>
                                                                <asp:TextBox ID="txtTransID" runat="server" Width="160px" height="20PX"   CssClass="TextBoxStyle"  AutoPostBack="true" OnTextChanged="txtTransID_TextChanged" ondblclick="ShowSampleMasterPopup();"  ></asp:TextBox>
                                                        </ContentTemplate>
                                           <Triggers>
                                                       <asp:PostBackTrigger ControlID="txtTransID" />                                     
                                                                                        </Triggers>
                                                 </asp:updatepanel> 
                                    </td>

                                        </tr>
                                 
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;"> Date
                                        </td>
                                        <td>
                                              <asp:updatepanel id="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                          <asp:TextBox ID="txtTransDate" runat="server" Width="160px"  CssClass="TextBoxStyle"  MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"  Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                                                                            </asp:CalendarExtender>    
                                            </ContentTemplate>
                                     </asp:updatepanel>

                                        </td>
                                    </tr>
                                    
                                   
                                    
                                        <tr>
                        <td class="lblCaption1" style="height: 25px;">Type
                        </td>
                        <td>
                          <asp:updatepanel id="UpdatePanel15" runat="server">
                                            <ContentTemplate>
                                <asp:dropdownlist id="drpInvType" runat="server"  CssClass="TextBoxStyle" width="167px"     >
                                            <asp:ListItem Value="Cash" selected="True">Cash</asp:ListItem>
                                            <asp:ListItem Value="Credit" >Credit</asp:ListItem>
                       </asp:dropdownlist>
                                            </ContentTemplate>
                             
                                     </asp:updatepanel> 
                        </td>
                                           

                                            </tr>
                                     <tr>
                         <td class="lblCaption1" style="height: 25px;">PT. Type
                        </td>
                        <td>
                          <asp:updatepanel id="UpdatePanel16" runat="server">
                                            <ContentTemplate>
                                  <asp:DropDownList ID="drpPTType" runat="server"  CssClass="TextBoxStyle" Width="167px" BorderWidth="1px" >
                                            <asp:ListItem Value="OP">OP</asp:ListItem>
                                            <asp:ListItem Value="IP">IP</asp:ListItem>
                                        </asp:DropDownList>
                                            </ContentTemplate>
                             
                                     </asp:updatepanel> 
                        </td>
                                    </tr>
                                    </table>
                                </fieldset>
                            <fieldset style="padding:5px; ">
                                <legend class="lblCaption1" style="font-weight: bold;">Patient Detals</legend>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                     <tr>
                                        <td class="lblCaption1" style="height: 25px;">File No
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel17" runat="server">
                                                                <ContentTemplate>
                                                        <asp:TextBox ID="txtFileNo" runat="server" Width="150px"  CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                                                                </ContentTemplate>
                                                         </asp:updatepanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Name
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel20" runat="server">
                                                                <ContentTemplate>
                                                             <asp:TextBox ID="txtFName" runat="server" Width="150px"  CssClass="TextBoxStyle"></asp:TextBox>
                                                                 </ContentTemplate>
                                                         </asp:updatepanel>
                                        </td>


                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Sex
                                        </td>
                                        <td class="lblCaption1">
                                            <asp:updatepanel id="UpdatePanel24" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtSex" runat="server" Width="150px" CssClass="TextBoxStyle" MaxLength="10"  ReadOnly="true"></asp:TextBox>
                                                 </ContentTemplate>
                                     </asp:updatepanel>

                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">  DOB
                                        </td>
                                      <td>
                                         <asp:updatepanel id="UpdatePanel23" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtDOB" runat="server" Width="150px" CssClass="TextBoxStyle" MaxLength="10"  ReadOnly="true"></asp:TextBox>
                                    
                                             </ContentTemplate>
                                     </asp:updatepanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Age
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel31" runat="server">
                                            <ContentTemplate>
                                            <asp:TextBox ID="txtAge" runat="server" Width="15px" height="15PX" CssClass="TextBoxStyle" MaxLength="10"  ReadOnly="true"></asp:TextBox>
                                            <asp:dropdownlist id="drpAgeType" runat="server"  style="width: 50px;font-size:9px;" class="TextBoxStyle"  ReadOnly="true">
                                                   <asp:ListItem Value="Y">Years</asp:ListItem>
                                                   <asp:ListItem Value="M">Months</asp:ListItem>
                                                    <asp:ListItem Value="D"  >Days</asp:ListItem>
                                           </asp:dropdownlist>
                                              <asp:TextBox ID="txtAge1" runat="server" Width="15px" height="15PX" CssClass="TextBoxStyle" MaxLength="10"  ReadOnly="true"></asp:TextBox>
                                                         <asp:dropdownlist id="drpAgeType1" runat="server"  style="width: 50px;font-size:9px;" class="TextBoxStyle"  ReadOnly="true">
                                                   <asp:ListItem Value="Y">Years</asp:ListItem>
                                                   <asp:ListItem Value="M">Months</asp:ListItem>
                                                    <asp:ListItem Value="D"  >Days</asp:ListItem>
                                           </asp:dropdownlist>
                                             </ContentTemplate>
                                     </asp:updatepanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Nationality</td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel32" runat="server">
                                            <ContentTemplate>
                                  <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle"  Width="157px"  ReadOnly="true">  </asp:DropDownList>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td class="lblCaption1" style="height: 25px;">Admin ID
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                               <asp:TextBox ID="txtAdminID" runat="server" Width="150px" CssClass="TextBoxStyle"  ></asp:TextBox>
                                            </ContentTemplate>
                             
                                     </asp:updatepanel>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Ward
                                        </td>
                                        <td colspan="lblCaption1">
                                            <asp:updatepanel id="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtWard" runat="server" Width="150px"  CssClass="TextBoxStyle"  ></asp:TextBox>
                                            </ContentTemplate>
                             
                                     </asp:updatepanel>
                                           </td>
                                            </tr>
                                             <tr>
                                                   <td class="lblCaption1" style="height: 25px;">Room
                                        </td>
                                             <td colspan="lblCaption1">
                        
                        
                          <asp:updatepanel id="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                               <asp:TextBox ID="txtRoom" runat="server" Width="150px"  CssClass="TextBoxStyle"  ></asp:TextBox>
                                            </ContentTemplate>
                             
                                     </asp:updatepanel>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Bed
                                        </td>
                                        <td>
                                            <asp:updatepanel id="UpdatePanel33" runat="server">
                                                            <ContentTemplate>
                                               <asp:TextBox ID="txtBed" runat="server" Width="150px"   CssClass="TextBoxStyle"  ></asp:TextBox>
                                                            </ContentTemplate>
                             
                                                     </asp:updatepanel>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                             
                            <fieldset style="padding:5px; ">
                                <legend class="lblCaption1" style="font-weight: bold;">Insurance Detals</legend>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                          <td class="lblCaption1" style="height: 25px;">Company
                        </td>
                        <td>
                              <asp:updatepanel id="UpdatePanel12" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtProviderID" runat="server"  CssClass="TextBoxStyle"  ReadOnly="true" visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtProviderName" runat="server" Width="150px"   CssClass="TextBoxStyle"  ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                                    </tr>
                                    <tr>
                                          <td class="lblCaption1" style="height: 25px;"> Policy No
                        </td>
                        <td>
                             <asp:updatepanel id="UpdatePanel13" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtPolicyNo" runat="server" Width="150px"  CssClass="TextBoxStyle"  ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                                    </tr>
                                    </table>
                                    </fieldset>
                            <fieldset style="padding:5px; ">
                                <legend class="lblCaption1" style="font-weight: bold;"></legend>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">


                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;" valign="top">Doctor Name
                                        </td>


                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:updatepanel id="UpdatePanel35" runat="server">
                                            <ContentTemplate>
                                                      <asp:dropdownlist id="drpDoctors" runat="server"  style="width:225px" class="TextBoxStyle" ></asp:dropdownlist>
                                            </ContentTemplate>
                                          </asp:updatepanel>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 25px;">Ref. Doctor
                                           </td>
                                          </tr>
                                    <tr>
                        <td>
                            <asp:updatepanel id="UpdatePanel28" runat="server">
                                            <ContentTemplate>
                                  <asp:dropdownlist id="drpRefDoctor" runat="server"  style="width: 225px" class="TextBoxStyle" >
                                   </asp:dropdownlist>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>

                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                          <td style="width: 80%;vertical-align:top;padding-left:10px;">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                      <td class="lblCaption1" >
                                          Referal Type
                                      </td>
                                      <td>
                                            <asp:updatepanel id="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                            <asp:DropDownList ID="drpReferalType" runat="server" CssClass="TextBoxStyle" Width="167px" BorderWidth="1px" >
                                                <asp:ListItem Value="Internal">Internal</asp:ListItem>
                                                <asp:ListItem Value="External">External</asp:ListItem>
                                            </asp:DropDownList>
                                                  </ContentTemplate>
                                     </asp:updatepanel>
                                      </td>
                                       
                                    <td class="lblCaption1" >
                                      Invoice Ref #
                                    </td>
                                    <td >
                                         <asp:updatepanel id="UpdatePanel25" runat="server">
                                               <ContentTemplate>
                                         <asp:TextBox ID="txtInvoiceRefNo" runat="server" Width="150px" height="20px" CssClass="TextBoxStyle"  ></asp:TextBox>
                                               </ContentTemplate>
                                          </asp:updatepanel> 
                                    </td>
                                   

                                  </tr>
                                    <tr>
                                         <td style=" height: 30px;" class="lblCaption1" >
                                            Attachment 1
                                        </td>
                                       <td >
                                             <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel1" runat="server" Width="300px">
                                                            <asp:FileUpload ID="filAttachment1" runat="server" Width="300px" CssClass="ButtonStyle" />
                                                        </asp:Panel>
                                                    </ContentTemplate>
                        
                                                </asp:UpdatePanel>
                                       </td>
                                         <td class="lblCaption1" >
                                      EMR #
                                    </td>
                                    <td >
                                         <asp:updatepanel id="UpdatePanel8" runat="server">
                                               <ContentTemplate>
                                         <asp:TextBox ID="txtEMRID" runat="server" Width="150px" height="20px" CssClass="TextBoxStyle"   ></asp:TextBox>
                                               </ContentTemplate>
                                          </asp:updatepanel> 
                                    </td>
                                    </tr>
                                     <tr>
                                         <td style=" height: 30px;" class="lblCaption1" >
                                            Attachment 2
                                        </td>
                                       <td >
                                             <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Panel2" runat="server" Width="300px">
                                                            <asp:FileUpload ID="filAttachment2" runat="server" Width="300px" CssClass="ButtonStyle" />
                                                        </asp:Panel>
                                                    </ContentTemplate>
                        
                                                </asp:UpdatePanel>
                                       </td>
 <td class="lblCaption1" >
                                      Barcode ID
                                    </td>
                                    <td >
                                         <asp:updatepanel id="UpdatePanel9" runat="server">
                                               <ContentTemplate>
                                         <asp:TextBox ID="txtBarecodeID" runat="server" Width="150px" height="20px" CssClass="TextBoxStyle"   Enabled="False"  ></asp:TextBox>
                                               </ContentTemplate>
                                          </asp:updatepanel> 
                                    </td>

                                    </tr>
                              </table>
                               <br />
                               <div style="padding-top: 0px; width: 700px;overflow:auto;height:300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvServiceList" runat="server"  autogeneratecolumns="False"  
                                                        enablemodelvalidation="True" width="100%">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                 <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>
                         <asp:TemplateField     ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Width="30px"   >
                            <HeaderTemplate>
                                 <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged"  />
                                 </HeaderTemplate>
                            <ItemTemplate>
                                
                                     <asp:CheckBox ID="chkSelService" runat="server" CssClass="label"    />
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Service ID"  HeaderStyle-Width="100px"   >
                            <ItemTemplate>
                                 <asp:Label ID="lblID" CssClass="GridRow"   runat="server" Text='<%# Bind("ID") %>' visible="false"></asp:Label>
                                <asp:Label ID="lblServiceID" CssClass="GridRow"  runat="server" Text='<%# Bind("ServiceID") %>'  ></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description"  >
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblDescription" CssClass="GridRow"     runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                
                            </ItemTemplate>

                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Type"  >
                            <ItemTemplate>
                                
                                 <asp:Label ID="lblTestTypeCode" CssClass="GridRow"     runat="server" Text='<%# Bind("TEST_TYPE_CODE") %>' Visible="false" ></asp:Label>
                                 <asp:Label ID="lblTestTypeDesc" CssClass="GridRow"     runat="server" Text='<%# Bind("TEST_TYPE_DESC") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                          
                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
                                       
                            <br />
                          </td>
                    </tr>
                    </table>
             
            


            </div>
 
    

</asp:Content>
