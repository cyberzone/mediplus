﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace HMS.Laboratory
{
    public partial class LabTestWaitingList : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindWaitngList()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);


            DataSet DS = new DataSet();

            objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (strTestType == "EMR")
            {
                Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                if (drpWaitListStatus.SelectedValue == "Waiting")
                {
                    Criteria += " AND Completed IS NULL ";
                }
                if (drpWaitListStatus.SelectedValue == "Completed")
                {
                    Criteria += " AND Completed IS NOT NULL ";
                }

                //if (drpWaitListStatus.SelectedValue != "")
                //{
                //    if (drpWaitListStatus.SelectedValue == "Waiting")
                //    {
                //        Criteria += " AND Completed IS NULL ";
                //    }
                //    else
                //    {
                //        Criteria += " AND Completed='" + drpWaitListStatus.SelectedValue + "'";

                //    }

                //}


                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00 '";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + " " + drpEndHour.SelectedValue + ":" + drpEndMin.SelectedValue + ":00 '";
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }

                if (txtMobileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HPM_MOBILE LIKE '%" + txtMobileNo.Text.Trim() + "%'";
                }


            }
            else
            {
                // Criteria = " 1=1  and  AND HIT_SERV_TYPE='L'   ";
                if (drpWaitListStatus.SelectedValue == "Waiting")
                {

                    Criteria += " AND HIT_LAB_STATUS IS NULL ";
                    Criteria += " AND HIT_SERV_TYPE='L'  ";
                }
                if (drpWaitListStatus.SelectedValue == "Completed")
                {

                    Criteria += " AND HIT_LAB_STATUS IS NOT NULL ";
                    Criteria += " AND HIT_SERV_TYPE='L'  ";
                }

                //if (drpWaitListStatus.SelectedValue != "")
                //{

                //    if (drpWaitListStatus.SelectedValue == "Waiting")
                //    {
                //        Criteria += " AND HIT_LAB_STATUS IS NULL ";

                //    }
                //    else
                //    {
                //        Criteria += " AND HIT_LAB_STATUS='" + drpWaitListStatus.SelectedValue + "'";

                //    }

                //}



                //else if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" && drpWaitListStatus.SelectedValue == "Waiting")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}
                //else if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" && drpWaitListStatus.SelectedValue == "Completed")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_TEST_COMPLETED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}

                Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00 '";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + " " + drpEndHour.SelectedValue + ":" + drpEndMin.SelectedValue + ":00 '";
                }
                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }

                if (txtMobileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HPM_MOBILE LIKE '%" + txtMobileNo.Text.Trim() + "%'";
                }
            }

            string strServiceType = "";

            strServiceType = "L";

            // if (Convert.ToString(ViewState["PageName"]) == "LabRegWaitList")
            // {
            DS = objLab.LabTestRegisterWaitingList(Criteria);
            //}
            //else
            //{
            //    DS = objLab.LabWaitingListShow(drpWaitListStatus.SelectedValue, GlobelValues.FileDescription, strServiceType, Criteria);

            //}

            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvWaitingList.DataSource = DV;
                gvWaitingList.DataBind();
            }
            else
            {
                gvWaitingList.DataBind();
            }

        }


        string GetEMRID()
        {
            string strEMR_ID = "0";
            DataSet DS = new DataSet();
            String Criteria = " 1=1  ";

            string strStartDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";

            Criteria += " AND  EPM_PT_ID = '" + Convert.ToString(ViewState["PatientID"]) + "'";
            Criteria += " AND  EPM_DR_CODE = '" + Convert.ToString(ViewState["DoctorID"]) + "'";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strEMR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);
            }


            return strEMR_ID;
        }


        void BindTimeFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");


            drpSTHour.DataSource = DSHours;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();

            drpEndHour.DataSource = DSHours;
            drpEndHour.DataTextField = "Name";
            drpEndHour.DataValueField = "Code";
            drpEndHour.DataBind();


            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpSTMin.DataSource = DSMin;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();

            drpEndMin.DataSource = DSMin;
            drpEndMin.DataTextField = "Name";
            drpEndMin.DataValueField = "Code";
            drpEndMin.DataBind();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_RESULT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                btnLabTestRegister.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnLabTestRegister.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "ID";
                ViewState["PageName"] = (string)Request.QueryString["PageName"];

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }


                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTransToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTimeFromDB();




                    BindWaitngList();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {


            try
            {
                BindWaitngList();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            gvWaitingList.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

            Label lblID = (Label)gvScanCard.Cells[0].FindControl("lblID");
            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblDoctorID");
            Label lblTransDate = (Label)gvScanCard.Cells[0].FindControl("lblTransDate");
            Label lblTransType = (Label)gvScanCard.Cells[0].FindControl("lblTransType");
            Label lblServiceID = (Label)gvScanCard.Cells[0].FindControl("lblServiceID");

            ViewState["TransID"] = lblID.Text;
            ViewState["PatientID"] = lblPatientId.Text;
            ViewState["DoctorID"] = lblDoctorID.Text;
            ViewState["TransDate"] = lblTransDate.Text;
            ViewState["TransType"] = lblTransType.Text;
            ViewState["ServiceID"] = lblServiceID.Text;



        }

        protected void btnShowTestReport_Click(object sender, EventArgs e)
        {
            string strRptPath = "";
            if (Convert.ToString(ViewState["TransID"]) == "" || Convert.ToString(ViewState["TransID"]) == null)
            {
                goto FunEnd;
            }

            if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" || Convert.ToString(ViewState["PageName"]) == "LabRegWaitList")
            {
                Response.Redirect("LabTestReport.aspx?PageName=LabWaitList&TransID=" + Convert.ToString(ViewState["TransID"]) + "&PatientID=" + Convert.ToString(ViewState["PatientID"]) + "&DoctorID=" + Convert.ToString(ViewState["DoctorID"]) + "&TransDate=" + Convert.ToString(ViewState["TransDate"]) + "&TransType=" + Convert.ToString(ViewState["TransType"]) + "&ServiceID=" + Convert.ToString(ViewState["ServiceID"]));
            }
            else
            {
                Response.Redirect("../Radiology/TransactionContainer.aspx?PageName=RadWaitList&TransID=" + Convert.ToString(ViewState["TransID"]) + "&PatientID=" + Convert.ToString(ViewState["PatientID"]) + "&DoctorID=" + Convert.ToString(ViewState["DoctorID"]) + "&TransDate=" + Convert.ToString(ViewState["TransDate"]) + "&TransType=" + Convert.ToString(ViewState["TransType"]) + "&ServiceID=" + Convert.ToString(ViewState["ServiceID"]));
            }

        FunEnd: ;

        }

        protected void btnEMRRRadRequest_Click(object sender, EventArgs e)
        {
            string strPath = "";
            string EMR_ID = "0";
            EMR_ID = GetEMRID();

            string strPath1 = "&EMR_ID=" + EMR_ID + "&EMR_PT_ID=" + Convert.ToString(ViewState["PatientID"]) + "&DR_ID=" + Convert.ToString(ViewState["DoctorID"]);


            //strPath = GlobelValues.EMR_Path + "/CommonPageLoader.aspx?BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + strPath1 + "&PageName=Laboratory&LoginFrom=CommonPage";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Summary", "window.open('" + strPath + "','_new','top=200,left=100,height=800,width=900,toolbar=no,scrollbars=yes,menubar=no');", true);

            strPath = "../EMR/WebReports/Laboratory.aspx";
            string rptcall = @strPath + "?EMR_ID=" + EMR_ID + "&EMR_PT_ID=" + Convert.ToString(ViewState["PatientID"]) + "&DR_ID=" + Convert.ToString(ViewState["DoctorID"]);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Rad", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);



            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowRadRequestPopup(" + EMR_ID + ")", true);
        }

        protected void btnLabTestRegister_Click(object sender, EventArgs e)
        {

            if (Convert.ToString(ViewState["TransID"]) == "" || Convert.ToString(ViewState["TransID"]) == null)
            {
                goto FunEnd;
            }


            Response.Redirect("LabTestRegister.aspx?PageName=LabWaitList&MenuName=Laboratory&TransID=" + Convert.ToString(ViewState["TransID"]) + "&PatientID=" + Convert.ToString(ViewState["PatientID"]) + "&DoctorID=" + Convert.ToString(ViewState["DoctorID"]) + "&TransDate=" + Convert.ToString(ViewState["TransDate"]) + "&TransType=" + Convert.ToString(ViewState["TransType"]) + "&ServiceID=" + Convert.ToString(ViewState["ServiceID"]));


        FunEnd: ;

        }

        protected void gvWaitingList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindWaitngList();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvWaitingList_Sorting");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvWaitingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblPriority = (Label)e.Row.FindControl("lblPriority");



                if (lblPriority.Text.ToUpper() == "URGENT".ToUpper())
                {
                    e.Row.BackColor = System.Drawing.Color.FromName("#ffb3b3");
                }
            }
        }

      

    }
}