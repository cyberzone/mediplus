﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

using System.Data.SqlClient;

using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;

using System.Drawing;
using System.Drawing.Design;

using System.Drawing.Printing;

namespace Mediplus.Laboratory
{
    public partial class SampleCollection1 : System.Web.UI.Page
    {
        string strDataSource = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"]).Trim();
        string strDBName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"]).Trim();
        string strDBUserId = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"]).Trim();
        string strDBUserPWD = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"]).Trim();
        string strReportPath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["REPORT_PATH"]).Trim();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindNationality()
        {
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "Select");
            drpNationality.Items[0].Value = "0";



        }

        void BindTime12HrsFromDB()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");


            drpSTHour.DataSource = DSHours;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();




            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpSTMin.DataSource = DSMin;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();




        }

        void BindDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";// AND HSFM_DEPT_ID='RADIOLOGY' 
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDoctors.DataSource = ds;
                drpDoctors.DataTextField = "FullName";
                drpDoctors.DataValueField = "HSFM_STAFF_ID";
                drpDoctors.DataBind();

                drpRefDoctor.DataSource = ds;
                drpRefDoctor.DataTextField = "FullName";
                drpRefDoctor.DataValueField = "HSFM_STAFF_ID";
                drpRefDoctor.DataBind();


                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;



            }

            drpDoctors.Items.Insert(0, "--- Select ---");
            drpDoctors.Items[0].Value = "0";

            drpRefDoctor.Items.Insert(0, "--- Select ---");
            drpRefDoctor.Items[0].Value = "0";
        }

        void BuildServiceList()
        {
            DataTable dt = new DataTable();

            DataColumn ID = new DataColumn();
            ID.ColumnName = "ID";

            DataColumn FileNo = new DataColumn();
            FileNo.ColumnName = "FileNo";

            DataColumn Date = new DataColumn();
            Date.ColumnName = "Date";

            DataColumn PTName = new DataColumn();
            PTName.ColumnName = "PTName";

            DataColumn Doctor = new DataColumn();
            Doctor.ColumnName = "Doctor";

            DataColumn Insurance = new DataColumn();
            Insurance.ColumnName = "Insurance";



            DataColumn ServiceID = new DataColumn();
            ServiceID.ColumnName = "ServiceID";

            DataColumn Description = new DataColumn();
            Description.ColumnName = "Description";



            DataColumn TEST_TYPE_CODE = new DataColumn();
            TEST_TYPE_CODE.ColumnName = "TEST_TYPE_CODE";

            DataColumn TEST_TYPE_DESC = new DataColumn();
            TEST_TYPE_DESC.ColumnName = "TEST_TYPE_DESC";

            dt.Columns.Add(ID);
            dt.Columns.Add(FileNo);
            dt.Columns.Add(Date);
            dt.Columns.Add(PTName);
            dt.Columns.Add(Doctor);
            dt.Columns.Add(Insurance);

            dt.Columns.Add(ServiceID);
            dt.Columns.Add(Description);
            dt.Columns.Add(TEST_TYPE_CODE);
            dt.Columns.Add(TEST_TYPE_DESC);


            ViewState["ServiceList"] = dt;
        }

        void BindServiceList()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);



            string Criteria = " 1=1 ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (strTestType == "EMR")
            {
                Criteria += " AND Completed IS NULL ";
                //  Criteria += " AND EPL_ID='" + txtEMRID.Text + "'";
                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "  00:00:00'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "  23:59:59'";
                }

                if (txtSrcFileNo.Text != "")
                {
                    Criteria += " AND  EPM_PT_ID='" + txtSrcFileNo.Text + "'";
                }

                if (txtSrcFName.Text != "")
                {
                    Criteria += " AND  EPM_PT_NAME='" + txtSrcFName.Text + "'";
                }

                if (txtSrcDoctor.Text != "")
                {
                    Criteria += " AND  EPM_DR_NAME like '%" + txtSrcDoctor.Text + "%'";
                }



                if (txtSrcInsurance.Text != "")
                {
                    Criteria += " AND  EPM_INS_NAME like '%" + txtSrcInsurance.Text + "%'";
                }


                CommonBAL objCom = new CommonBAL();
                objCom.LaboratoryGet(Criteria);

                DataSet DS = new DataSet();
                DS = objCom.LaboratoryGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["ServiceList"];

                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {

                        DataRow objrow;
                        objrow = DT.NewRow();
                        objrow["ID"] = Convert.ToString(DR["EPM_ID"]);
                        objrow["FileNo"] = Convert.ToString(DR["EPM_PT_ID"]);
                        objrow["Date"] = Convert.ToString(DR["EPM_DATEDesc"]);
                        objrow["PTName"] = Convert.ToString(DR["EPM_PT_NAME"]);
                        objrow["Doctor"] = Convert.ToString(DR["EPM_DR_NAME"]);
                        objrow["Insurance"] = Convert.ToString(DR["EPM_INS_NAME"]);

                        objrow["ServiceID"] = Convert.ToString(DR["EPL_LAB_CODE"]);
                        objrow["Description"] = Convert.ToString(DR["EPL_LAB_NAME"]);

                        string TestTypeCode, TestTypeDesc;
                        GetInvProfileTestType(Convert.ToString(DR["EPL_LAB_CODE"]), out TestTypeCode, out  TestTypeDesc);

                        if (TestTypeCode == "")
                        {
                            GetProfileMasterTestType(Convert.ToString(DR["EPL_LAB_CODE"]), out TestTypeCode, out  TestTypeDesc);
                        }


                        objrow["TEST_TYPE_CODE"] = TestTypeCode;
                        objrow["TEST_TYPE_DESC"] = TestTypeDesc;

                        DT.Rows.Add(objrow);

                    }

                    ViewState["ServiceList"] = DT;

                }



            }
            else
            {
                ////Criteria += " AND HIT_LAB_STATUS IS NULL ";
                ////Criteria += " AND HIT_SERV_TYPE='L'  ";

                ////Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceRefNo.Text + "'";

                ////dboperations dbo = new dboperations();
                ////clsInvoice objInv = new clsInvoice();
                ////DataSet DS = new DataSet();
                ////DS = objInv.InvoiceTransGet(Criteria);

                ////if (DS.Tables[0].Rows.Count > 0)
                ////{

                ////    DataTable DT = new DataTable();
                ////    DT = (DataTable)ViewState["ServiceList"];

                ////    foreach (DataRow DR in DS.Tables[0].Rows)
                ////    {

                ////        DataRow objrow;
                ////        objrow = DT.NewRow();
                ////        objrow["ID"] = Convert.ToString(ViewState["TransID"]);
                ////        objrow["ServiceID"] = Convert.ToString(DR["HIT_SERV_CODE"]);
                ////        objrow["Description"] = Convert.ToString(DR["HIT_DESCRIPTION"]);

                ////        string TestTypeCode, TestTypeDesc;
                ////        GetInvProfileTestType(Convert.ToString(DR["HIT_SERV_CODE"]), out TestTypeCode, out  TestTypeDesc);


                ////        objrow["TEST_TYPE_CODE"] = TestTypeCode;
                ////        objrow["TEST_TYPE_DESC"] = TestTypeDesc;

                ////        DT.Rows.Add(objrow);

                ////    }

                ////    ViewState["ServiceList"] = DT;

            }
        }

        void BindTempServiceList()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ServiceList"];
            if (DT.Rows.Count > 0)
            {
                gvServiceList.DataSource = DT;
                gvServiceList.DataBind();

            }
            else
            {
                gvServiceList.DataBind();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Sample Collection Search','No Data')", true);

            }

        }

        void PTDtlsClear()
        {

            txtFName.Text = "";
            lblStatus.Text = "";

            txtDOB.Text = "";
            //lblAge.Text = "";

            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            //txtAddress.Text = "";
            //txtPoBox.Text = "";

            //txtArea.Text = "";
            //txtCity.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";




            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";




            //  drpDoctors.SelectedIndex = 0;


        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                //  lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                drpAgeType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                drpAgeType1.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);


                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                //txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                //txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                //txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                //txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();

                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]).ToUpper())
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNationality;
                        }
                    }

                }

            ForNationality: ;

                //txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                //txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                //txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                // if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                //   lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                // txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                txtProviderID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);



                //    if (ds.Tables[0].Rows[0].IsNull("HPM_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]) != "")
                //    {
                //        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                //        {
                //            if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]).ToUpper())
                //            {
                //                drpDoctors.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);
                //                goto ForDoctors;
                //            }
                //        }

                //    }

                //ForDoctors: ;


                //    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "")
                //    {
                //        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                //        {
                //            if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]).ToUpper())
                //            {
                //                drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                //                goto ForRefDoctors;
                //            }
                //        }

                //    }

                //ForRefDoctors: ;
                //    // GetPatientVisit();

                //    drpInvType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";


            }

        }

        void BindLabSampleReportMaster()
        {
        LaboratoryBAL    objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND LTSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSM_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            DS = objLab.TestSampleMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["NewFlag"] = false;
                    txtTestSampleID.Text = Convert.ToString(DR["LTSM_TEST_SAMPLE_ID"]);

                    hidActualSampleID.Value = txtTestSampleID.Text;

                    txtTransDate.Text = Convert.ToString(DR["LTSM_DATEDesc"]);


                    if (DR.IsNull("LTSM_DATE") == false && Convert.ToString(DR["LTSM_DATE"]) != "")
                    {

                        string strTimeUp = Convert.ToString(DR["LTSM_DATE"]);

                        string[] arrTimeUp = strTimeUp.Split(' ');
                        if (arrTimeUp.Length > 1)
                        {


                            string[] arrTimeUp1 = arrTimeUp[1].Split(':');

                            if (arrTimeUp1.Length > 1)
                            {
                                string strHour = arrTimeUp1[0];

                                if (drpSTHour.Items.Count > 0)
                                {

                                    for (int i = 0; i < drpSTHour.Items.Count; i++)
                                    {

                                        if (drpSTHour.Items[i].Value == strHour)
                                        {
                                            drpSTHour.SelectedValue = strHour;

                                            goto HourLoopEnd;
                                        }
                                    }

                                HourLoopEnd: ;
                                }


                                string strMin = arrTimeUp1[1];

                                if (drpSTMin.Items.Count > 0)
                                {

                                    for (int i = 0; i < drpSTMin.Items.Count; i++)
                                    {

                                        if (drpSTMin.Items[i].Value == strMin)
                                        {
                                            drpSTMin.SelectedValue = strMin;

                                            goto MinLoopEnd;
                                        }
                                    }

                                MinLoopEnd: ;
                                }


                            }

                        }


                    }

                    txtDeliveryDate.Text = Convert.ToString(DR["LTSM_DELIVERY_DATEDesc"]);

                    if (DR.IsNull("LTSM_STATUS") == false && Convert.ToString(DR["LTSM_STATUS"]) != "")
                    {
                        drpStatus.SelectedValue = Convert.ToString(DR["LTSM_STATUS"]);
                    }
                    txtFileNo.Text = Convert.ToString(DR["LTSM_PATIENT_ID"]);
                    txtFName.Text = Convert.ToString(DR["LTSM_PATIENT_NAME"]);
                    txtAge.Text = Convert.ToString(DR["LTSM_PATIENT_AGE"]);
                    if (DR.IsNull("LTSM_PATIENT_AGETYPE") == false && Convert.ToString(DR["LTSM_PATIENT_AGETYPE"]) != "")
                    {
                        drpAgeType.SelectedValue = Convert.ToString(DR["LTSM_PATIENT_AGETYPE"]);
                    }
                    txtSex.Text = Convert.ToString(DR["LTSM_PATIENT_SEX"]);



                    if (DR.IsNull("LTSM_PATIENT_NATIONALITY") == false && Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]) != "")
                    {
                        for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                        {
                            if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]).ToUpper())
                            {
                                drpNationality.SelectedValue = Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]);
                                goto ForNationality;
                            }
                        }

                    }
                ForNationality: ;

                    //= Convert.ToString(DR["LTRM_RANGE"]);
                    if (DR.IsNull("LTSM_REP_TYPE") == false && Convert.ToString(DR["LTSM_REP_TYPE"]) != "")
                    {
                        drpReportType.SelectedValue = Convert.ToString(DR["LTSM_REP_TYPE"]);
                    }
                    //drpReportType.= Convert.ToString(DR["LTRM_INVESTIGATION_DESCRIPTION"]);
                    if (DR.IsNull("LTSM_REF_DR") == false && Convert.ToString(DR["LTSM_REF_DR"]) != "")
                    {

                    }

                    if (DR.IsNull("LTSM_REF_DR") == false && Convert.ToString(DR["LTSM_REF_DR"]) != "")
                    {
                        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                        {
                            if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_REF_DR"]).ToUpper())
                            {
                                drpRefDoctor.SelectedValue = Convert.ToString(DR["LTSM_REF_DR"]);
                                goto FordrpRefDoctor;
                            }
                        }

                    }
                FordrpRefDoctor: ;


                    if (DR.IsNull("LTSM_DR_ID") == false && Convert.ToString(DR["LTSM_DR_ID"]) != "")
                    {
                        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                        {
                            if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_DR_ID"]).ToUpper())
                            {
                                drpDoctors.SelectedValue = Convert.ToString(DR["LTSM_DR_ID"]);
                                goto FordrpDoctors;
                            }
                        }

                    }
                FordrpDoctors: ;



                    
                    txtRemarks.Text = Convert.ToString(DR["LTSM_REMARKS"]);
                    txtBarecodeID.Text = Convert.ToString(DR["LTSM_BARECODE"]);

                    txtInvoiceRefNo.Text = Convert.ToString(DR["LTSM_INVOICE_ID"]);
                    txtEMRID.Text = Convert.ToString(DR["LTSM_EMR_ID"]);
 



                }
            }

        }

        void BindDiagnosis(string ERMID)
        {

            string Criteria = "1=1  AND EPD_ID=" + ERMID;
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();

            DS = objCom.DiagnosisGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.Visible = true;
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();
            }
            else
            {
                gvDiagnosis.Visible = false;
                gvDiagnosis.DataBind();
            }

        }


        void BuildeTestSampleDtls()
        {
            string Criteria = "1=2";
          LaboratoryBAL  objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestSampleDtlsGet(Criteria);

            ViewState["TestSampleDtls"] = DS.Tables[0];
        }

        void BindTestSampleDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";

          LaboratoryBAL  objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestSampleDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["TestSampleDtls"] = DS.Tables[0];

            }
        }

        void BindTempTestSampleDtls()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["TestSampleDtls"];
            if (DT.Rows.Count > 0)
            {
                gvTestSampleDtls.DataSource = DT;
                gvTestSampleDtls.DataBind();

            }
            else
            {
                gvTestSampleDtls.DataBind();
            }

        }

        void BindInvProfileTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LIPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LIPT_INVESTIGATION_PROFILE_ID='" + drpReportType.SelectedValue + "'";

        LaboratoryBAL    objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.InvProfileTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestSampleDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTSD_TEST_PROFILE_ID"] = Convert.ToString(DR["LIPT_TEST_PROFILE_ID"]);
                    objrow["LTSD_TEST_DESCRIPTION"] = Convert.ToString(DR["LIPT_TEST_DETAILS"]);
                    //objrow["LTRD_TEST_UNIT"] = Convert.ToString(DR["LIPT_UNIT"]);

                    string TestTypeCode, TestTypeDesc;
                    GetInvProfileTestTypeByProfileID(drpReportType.SelectedValue, out TestTypeCode, out  TestTypeDesc);


                    objrow["LTSD_TEST_TYPE_CODE"] = TestTypeCode;
                    objrow["LTSD_TEST_TYPE_DESC"] = TestTypeDesc;

                    DT.Rows.Add(objrow);

                }

                ViewState["TestSampleDtls"] = DT;

            }
        }

        void BindServiceList(string ERMID)
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);



            string Criteria = " 1=1 ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (strTestType == "EMR")
            {
                Criteria += " AND Completed IS NULL ";
                Criteria += " AND EPM_ID='" + ERMID + "'";


                CommonBAL objCom = new CommonBAL();
                objCom.LaboratoryGet(Criteria);

                DataSet DS = new DataSet();
                DS = objCom.LaboratoryGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["TestSampleDtls"];

                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {

                        DataRow objrow;
                        objrow = DT.NewRow();

                        objrow["LTSD_TEST_PROFILE_ID"] = Convert.ToString(DR["EPL_LAB_CODE"]);
                        objrow["LTSD_TEST_DESCRIPTION"] = Convert.ToString(DR["EPL_LAB_NAME"]);

                        string TestTypeCode, TestTypeDesc;
                        GetInvProfileTestType(Convert.ToString(DR["EPL_LAB_CODE"]), out TestTypeCode, out  TestTypeDesc);

                        if (TestTypeCode == "")
                        {
                            GetProfileMasterTestType(Convert.ToString(DR["EPL_LAB_CODE"]), out TestTypeCode, out  TestTypeDesc);
                        }


                        objrow["LTSD_TEST_TYPE_CODE"] = TestTypeCode;
                        objrow["LTSD_TEST_TYPE_DESC"] = TestTypeDesc;

                        DT.Rows.Add(objrow);

                    }

                    ViewState["TestSampleDtls"] = DT;

                }



            }
           
        }


        void GetInvProfileTestTypeByProfileID(string ProfileID, out string TestTypeCode, out string TestTypeDesc)
        {

            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
          LaboratoryBAL  objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LIPM_INVESTIGATION_PROFILE_ID='" + ProfileID + "'";


            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_DESC"]);

            }

        }

        void UpdateQRCode()
        {
            // Byte[] bytImage1 = null;

            QRCodeEncoder encoder = new QRCodeEncoder();
            encoder.QRCodeScale = 5;
            Bitmap img;
            img = encoder.Encode(txtTestSampleID.Text);
            // bytImage1 = ImageToByte(img);

            byte[] arr;
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                arr = ms.ToArray();
                //img.Save(@"c:\"+ txtTestSampleID.Text.Trim()  + ".jpg");
            }



            //objCom = new CommonBAL();
            //string FieldNameWithValues2 = " LTSM_QRCODE='" + arr + "'";
            //String Criteria = " LTSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSM_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            //objCom.fnUpdateTableData(FieldNameWithValues2, "LAB_TEST_SAMPLE_MASTER", Criteria);

            string strCon = System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString();
            SqlConnection Con = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_LAB_TestSampleMasterQRCodeUpdate";
            cmd.Parameters.Add(new SqlParameter("@LTSM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
            cmd.Parameters.Add(new SqlParameter("@LTSM_TEST_SAMPLE_ID", SqlDbType.VarChar)).Value = txtTestSampleID.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@LTSM_QRCODE", SqlDbType.Image)).Value = arr;

            cmd.ExecuteNonQuery();
            Con.Close();


        }

        Boolean fnSave()
        {
            Boolean isError = false;

            if (txtTransDate.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter  Date";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }


            if (txtDeliveryDate.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Delivery Date";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter File No";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }

          LaboratoryBAL  objLab = new LaboratoryBAL();
            objLab.LTSM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);

            objLab.LTSM_TEST_SAMPLE_ID = txtTestSampleID.Text;
            objLab.LTSM_TEST_SAMPLE_TYPE = "";
            objLab.LTSM_DATE = txtTransDate.Text + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
            objLab.LTSM_STATUS = drpStatus.SelectedValue;
            objLab.LTSM_PATIENT_ID = txtFileNo.Text.Trim();
            objLab.LTSM_PATIENT_NAME = txtFName.Text.Trim();
            objLab.LTSM_PATIENT_AGE = txtAge.Text.Trim();
            objLab.LTSM_PATIENT_AGETYPE = drpAgeType.SelectedValue;
            objLab.LTSM_PATIENT_SEX = txtSex.Text;
            objLab.LTSM_PATIENT_NATIONALITY = drpNationality.SelectedValue;
            objLab.LTSM_RANGE = "";
            objLab.LTSM_INVESTIGATION_PROFILE_ID = "";
            objLab.LTSM_INVESTIGATION_DESCRIPTION = "";
            objLab.LTSM_REF_DR = drpRefDoctor.SelectedValue;
            objLab.LTSM_DR_ID = drpDoctors.SelectedValue;
            objLab.LTSM_DR_NAME = drpDoctors.SelectedItem.Text;
            objLab.LTSM_SERV_ID = "";
            objLab.LTSM_SERV_DESCRIPTION = "";
            objLab.LTSM_FEE = "";
            objLab.LTSM_REMARKS = txtRemarks.Text;
            objLab.LTSM_REP_TYPE = drpReportType.SelectedValue;

            objLab.LTSM_TEST_EPRID = "";
            objLab.LTSM_Posted = "";
            objLab.LTSM_POSTED_NO = "";
            objLab.LTSM_DELIVERY_DATE = txtDeliveryDate.Text;
            objLab.LTSM_LAB_REQ_NO = "";
            objLab.LTSM_Ref_Hospital = "";
            objLab.BARECODE = txtBarecodeID.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }

            objLab.SCRN_ID = "LABSAMPLE";

            objLab.INVOICE_ID = txtInvoiceRefNo.Text.Trim();
            objLab.EMR_ID = txtEMRID.Text.Trim();

            objLab.UserID = Convert.ToString(Session["User_ID"]);
            string SAMPLE_ID = "";
            SAMPLE_ID = objLab.LABTestSampleMasterAdd();
            txtTestSampleID.Text = SAMPLE_ID;
            hidActualSampleID.Value = txtTestSampleID.Text;

          CommonBAL  objCom = new CommonBAL();
            string Criteria = " LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            objCom.fnDeleteTableData("LAB_TEST_SAMPLE_DETAIL", Criteria);


            foreach (GridViewRow row in gvTestSampleDtls.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");

                Label lblgvTestSampleDtlsProfilID = (Label)row.FindControl("lblgvTestSampleDtlsProfilID");
                Label lblgvTestSampleDtlsDesc = (Label)row.FindControl("lblgvTestSampleDtlsDesc");

                Label lblTestTypeCode = (Label)row.FindControl("lblTestTypeCode");
                Label lblTestTypeDesc = (Label)row.FindControl("lblTestTypeDesc");

                DropDownList drpgvTestSampleDtlsStatus = (DropDownList)row.FindControl("drpgvTestSampleDtlsStatus");

                if (chkSelService.Checked == true)
                {
                    objLab.LTSM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objLab.LTSM_TEST_SAMPLE_ID = txtTestSampleID.Text;
                    objLab.LTSD_TEST_PROFILE_ID = lblgvTestSampleDtlsProfilID.Text;
                    objLab.LTSD_TEST_DESCRIPTION = lblgvTestSampleDtlsDesc.Text;

                    objLab.TEST_TYPE_CODE = lblTestTypeCode.Text;
                    objLab.TEST_TYPE_DESC = lblTestTypeDesc.Text;

                    objLab.LTSD_TEST_RESULT = "";
                    objLab.LTSD_TYPE = "";
                    objLab.LTSD_SELECTED = "1";
                    objLab.LTSD_STATUS = drpgvTestSampleDtlsStatus.SelectedValue;

                    objLab.UserID = Convert.ToString(Session["User_ID"]);
                    objLab.LABTestSampleDtlsAdd();
                    string strTestType = "";

                    strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

                    // if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                    // {
                    if (strTestType == "EMR")
                    {
                        string FieldNameWithValues = " Completed='LAB_SAMPLE_COLLECTED' , EPL_DATE=GETDATE() ";
                        string Criteria1 = "  EPL_ID ='" + Convert.ToString(ViewState["EMRID"]) + "' AND EPL_LAB_CODE='" + lblgvTestSampleDtlsProfilID.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                    }
                    else
                    {
                        string FieldNameWithValues = " HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED',HIT_LAB_SAMPLE_DATE=GETDATE() ";
                        string Criteria1 = " HIT_INVOICE_ID ='" + Convert.ToString(ViewState["InvoiceID"]) + "' AND HIT_SERV_CODE='" + lblgvTestSampleDtlsProfilID.Text.Trim() + "'";

                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);
                    }

                    // }
                }
            }


            objCom = new CommonBAL();
            string FieldNameWithValues2 = " LTRM_STATUS='SAMPLE_COLLECTED' ";
            string Criteria2 = " LTRM_REG_ID='" + Convert.ToString(ViewState["LabRegisterID"]) + "'  AND (LTRM_STATUS IS NULL OR LTRM_STATUS ='' )";
            objCom.fnUpdateTableData(FieldNameWithValues2, "LAB_TEST_REGISTER_MASTER", Criteria2);

        FunEnd: ;
            return isError;
        }

        void TransferTestResultData()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = "  ID='" + txtTestSampleID.Text + "' AND UploadedToAnalyzer=1";
            DS = objCom.fnGetFieldValue("*", "LAB_TEST_RESULT_DATA", Criteria, "");


            if (DS.Tables[0].Rows.Count <= 0)
            {

               LaboratoryBAL objLab = new LaboratoryBAL();

                objLab.SAMPLE_ID = txtTestSampleID.Text;
                objLab.INVOICE_ID = Convert.ToString(ViewState["InvoiceID"]);
                objLab.EMR_ID = Convert.ToString(ViewState["EMRID"]);
                objLab.UserID = Convert.ToString(Session["User_ID"]);
                objLab.TestResultDataAdd();

            }

        }

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_REG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                // btnLabTestRegister.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                // btnLabTestRegister.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }

        void GetInvProfileTestType(string ServiceID, out string TestTypeCode, out string TestTypeDesc)
        {
            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            LaboratoryBAL objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  LIPM_SERV_ID='" + ServiceID + "'";

            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_DESC"]);
            }

        }

        void GetProfileMasterTestType(string ServiceID, out string TestTypeCode, out string TestTypeDesc)
        {
            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            LaboratoryBAL objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LTPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  LTPM_SERV_ID='" + ServiceID + "'";

            DS = objLab.TestProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_DESC"]);
            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "ID";
                ViewState["PageName"] = (string)Request.QueryString["PageName"];

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }


                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTransToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");

                    BindNationality();
                    BindTime12HrsFromDB();
                    BindDoctor();

                    BuildServiceList();
                    BindServiceList();
                    BindTempServiceList();

                    BuildeTestSampleDtls();


                    BuildeTestSampleDtls();
                    

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            BuildServiceList();
            BindServiceList();
            BindTempServiceList();
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;



            Label lblID = (Label)gvScanCard.Cells[0].FindControl("lblID");
            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");

            txtFileNo.Text = lblPatientId.Text;
            PTDtlsClear();
            PatientDataBind();

            if (lblID.Text != "") ;
            {
                BindDiagnosis(lblID.Text);
               
            }

            BuildeTestSampleDtls();
            BindServiceList(lblID.Text);
            BindTempTestSampleDtls();

            btnSave.Attributes.Add("display", "block");

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideSearchDiv()", true);

        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (chkMultyReptType.Checked == false)
            //{
            BuildeTestSampleDtls();
            //}
            BindInvProfileTrans();
            BindTempTestSampleDtls();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtTestName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestSampleDtls"];

                strTestCodeDesc = txtTestName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["LTSD_TEST_PROFILE_ID"] = TestProfileID;
                    DT.Rows[R]["LTSD_TEST_DESCRIPTION"] = TestProfileDesc;
                    //  DT.Rows[intCurRow]["LTRD_TEST_RESULT"] = drpgvTestReportDtlsResult.SelectedValue;
                    // DT.Rows[R]["LTRD_RANGE"] = "";
                    // DT.Rows[R]["LTRD_TEST_NORMALVALUE"] = "";
                    //  DT.Rows[R]["LTRD_TEST_UNIT"] = "";
                    DT.AcceptChanges();
                }
                else
                {

                    objrow["LTSD_TEST_PROFILE_ID"] = TestProfileID;
                    objrow["LTSD_TEST_DESCRIPTION"] = TestProfileDesc;

                    DT.Rows.Add(objrow);
                }



                ViewState["TestSampleDtls"] = DT;


                BindTempTestSampleDtls();
                txtTestName.Text = "";

                ViewState["gvServSelectIndex"] = "";

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvTestSampleDtls_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblgvTestSampleDtlsSelected = (Label)e.Row.Cells[0].FindControl("lblgvTestSampleDtlsSelected");
                    CheckBox chkSelService = (CheckBox)e.Row.Cells[0].FindControl("chkSelService");
                    Label lblgvTestSampleDtlsStatus = (Label)e.Row.Cells[0].FindControl("lblgvTestSampleDtlsStatus");
                    DropDownList drpgvTestSampleDtlsStatus = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestSampleDtlsStatus");

                    if (lblgvTestSampleDtlsSelected.Text == "1")
                    {

                        chkSelService.Checked = true;
                    }


                    if (lblgvTestSampleDtlsStatus.Text != "")
                    {
                        drpgvTestSampleDtlsStatus.SelectedValue = lblgvTestSampleDtlsStatus.Text;
                    }


                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvTestReportDtls_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }


        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvTestSampleDtls.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvTestSampleDtls.Rows)
                {
                    CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                    if (ChkBoxHeader.Checked == true)
                    {
                        chkSelService.Checked = true;
                    }
                    else
                    {
                        chkSelService.Checked = false;
                    }
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean boolIsChecked = false; ;
                for (int i = 0; i < gvTestSampleDtls.Rows.Count; i++)
                {
                    CheckBox chkSelService;
                    chkSelService = (CheckBox)gvTestSampleDtls.Rows[i].Cells[0].FindControl("chkSelService");

                    if (chkSelService.Checked == true)
                    {
                        boolIsChecked = true;
                    }

                }



                if (boolIsChecked == false)
                {
                    lblStatus.Text = "Please select Any of one of the Test";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;

                }



                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {

                    if (Convert.ToString(Session["LAB_RESULT_DATAUPLOAD"]) == "1")
                    {
                        TransferTestResultData();
                    }

                    UpdateQRCode();
                


                    if (hidLabelPrint.Value == "true")
                    {
                        btnLabelPrint1_Click(btnLabelPrint, new EventArgs());


                    }


                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }

            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + " btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnLabelPrint_Click(object sender, EventArgs e)
        {
            if (hidActualSampleID.Value == "")
            {
                goto FunEnd;
            }

            Int32 intPrintCount = 1;

            intPrintCount = gvTestSampleDtls.Rows.Count;

            //foreach (GridViewRow row in gvTestSampleDtls.Rows)
            //{
            //    CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");

            //    if (chkSelService.Checked == true)
            //    {
            //        intPrintCount = intPrintCount + 1;
            //    }

            //}



            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(intPrintCount);



            String DBString = "", strFormula = "";

            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualSampleID.Value;

            foreach (GridViewRow row in gvTestSampleDtls.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                Label lblgvTestSampleDtlsProfilID = (Label)row.FindControl("lblgvTestSampleDtlsProfilID");
                Label lblTestTypeDesc = (Label)row.FindControl("lblTestTypeDesc");


                if (chkSelService.Checked == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowLabelPrint('" + DBString + "','" + strFormula + "','" + intCount + "'  ,'" + lblgvTestSampleDtlsProfilID.Text + "' ," + txtPrintCount.Value + ",'" + chkEDTA.Checked + "' ,'" + chkSERUM.Checked + "' ,'" + chkFLUORIDE.Checked + "'  );", true);

                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "CallBareCodeexe('" + txtTestSampleID.Text + "','" + txtFileNo.Text.Trim() + "','" + txtFName.Text.Trim() + "','" + lblgvTestSampleDtlsDesc.Text.Trim() + "' ,'" + txtTransDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "','" + txtBarecodeID.Text.Trim() + "' ," + txtPrintCount.Value + ",'" + chkEDTA.Checked + "' ,'" + chkSERUM.Checked + "' ,'" + chkFLUORIDE.Checked + "');", true);

                }


            }
        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END


       FunEnd: ;
        }
        protected void btnLabelPrint1_Click(object sender, EventArgs e)
        {
            if (hidActualSampleID.Value == "")
            {
                goto FunEnd;
            }

            Int32 intPrintCount = 1;

            intPrintCount = gvTestSampleDtls.Rows.Count;

            Int32 intCount = Convert.ToInt32(intPrintCount);

             foreach (GridViewRow row in gvTestSampleDtls.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                Label lblgvTestSampleDtlsProfilID = (Label)row.FindControl("lblgvTestSampleDtlsProfilID");
                Label lblgvTestSampleDtlsDesc = (Label)row.FindControl("lblgvTestSampleDtlsDesc");

                if (chkSelService.Checked == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "CallBareCodeexe('" + txtTestSampleID.Text + "','" + txtFileNo.Text.Trim() + "','" + txtFName.Text.Trim() + "','" + lblgvTestSampleDtlsDesc.Text.Trim() + "' ,'" + txtTransDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "','" + txtBarecodeID.Text.Trim() + "');", true);
                    
                }

            }

             FunEnd: ;
        }
    }
}