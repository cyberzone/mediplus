﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Laboratory
{
    public partial class LabTestSampleWaitingList : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindWaitngList()
        {

            string strTestType = "";
            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

            DataSet DS = new DataSet();

            objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }




            if (drpWaitListStatus.SelectedIndex == 0)
            {
                Criteria += " AND LTSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            }
            else
            {
                Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            }


            if (txtTransFromDate.Text != "")
            {
                if (drpWaitListStatus.SelectedIndex == 0)
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTSM_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTSM_DATE,101),101) <= '" + strForToDate + "'";
                }
                else
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTRM_TEST_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTRM_TEST_DATE,101),101) <= '" + strForToDate + "'";
                }
            }
            if (txtFileNo.Text.Trim() != "")
            {
                if (drpWaitListStatus.SelectedIndex == 0)
                {
                    Criteria += " AND  LTSM_PATIENT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }
                else
                {
                    Criteria += " AND  LTRM_PATIENT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }
            }

            if (txtFName.Text.Trim() != "")
            {
                if (drpWaitListStatus.SelectedIndex == 0)
                {
                    Criteria += " AND  LTSM_PATIENT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
                else
                {
                    Criteria += " AND  LTRM_PATIENT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
            }

            if (drpWaitListStatus.SelectedValue == "Waiting")
            {
                // Criteria += " AND (LTRM_AUTHORIZE_STATUS IS NULL OR  LTRM_AUTHORIZE_STATUS ='' ) ";
                Criteria += " AND (LTSD_TEST_RESULT IS NULL OR  LTSD_TEST_RESULT ='' ) ";
            }

            else if (drpWaitListStatus.SelectedValue == "Authorized")
            {
                Criteria += " AND  LTRM_AUTHORIZE_STATUS='AUTHORIZED' ";
            }

            else if (drpWaitListStatus.SelectedValue == "Not Authorized")
            {
                Criteria += " AND  LTRM_AUTHORIZE_STATUS='NOT AUTHORIZED' ";
            }



            gvLabSamplePopup.Visible = false;
            gvLabTestReport.Visible = false;

            if (drpWaitListStatus.SelectedIndex == 0)
            {
                DS = objLab.TestSampleTestReportMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvLabSamplePopup.Visible = true;
                    gvLabSamplePopup.DataSource = DS;
                    gvLabSamplePopup.DataBind();
                }
                else
                {
                    gvLabSamplePopup.DataBind();
                }
            }
            else
            {
                DS = objLab.TestReportMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvLabTestReport.Visible = true;
                    gvLabTestReport.DataSource = DS;
                    gvLabTestReport.DataBind();
                }
                else
                {
                    gvLabTestReport.DataBind();
                }
            }

        }

        void BindLabTestRegtMaster()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_BARECODE='" + Convert.ToString(ViewState["LabSampleBareCode"]) + "'";
            DS = objLab.TestRegisterMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["InvoiceID"] = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_INVOICE_ID"]);
                ViewState["EMRID"] = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_EMR_ID"]);


            }

        }

        string GetEMRID()
        {
            string strEMR_ID = "0";
            DataSet DS = new DataSet();
            String Criteria = " 1=1  ";

            string strStartDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";

            Criteria += " AND  EPM_PT_ID = '" + Convert.ToString(ViewState["PatientID"]) + "'";
            Criteria += " AND  EPM_DR_CODE = '" + Convert.ToString(ViewState["DoctorID"]) + "'";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strEMR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);
            }


            return strEMR_ID;
        }


        void BindTestResultData(string  SampleID)
        {
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            string Criteria = "  ID='" + SampleID + "'  ";

            DS = objCom.fnGetFieldValue("*", "LAB_TEST_RESULT_DATA", Criteria, "HIS_TestName");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTestResultData.Visible = true;
                gvTestResultData.DataSource = DS;
                gvTestResultData.DataBind();
            }
            else
            {
                gvTestResultData.DataBind();
            }
        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_RESULT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnLabTestRegister.Visible = false;
                //btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnLabTestRegister.Enabled = false;
                // btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTransToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    ViewState["PageName"] = (string)Request.QueryString["PageName"];




                    BindWaitngList();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                BindWaitngList();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnEMRRRadRequest_Click(object sender, EventArgs e)
        {
            string strPath = "";
            string EMR_ID = "0";
            EMR_ID = GetEMRID();

            string strPath1 = "&EMR_ID=" + EMR_ID + "&EMR_PT_ID=" + Convert.ToString(ViewState["PatientID"]) + "&DR_ID=" + Convert.ToString(ViewState["DoctorID"]);


            strPath = "../EMR/WebReports/Laboratory.aspx";
            string rptcall = @strPath + "?EMR_ID=" + EMR_ID + "&EMR_PT_ID=" + Convert.ToString(ViewState["PatientID"]) + "&DR_ID=" + Convert.ToString(ViewState["DoctorID"]);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Rad", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowRadRequestPopup(" + EMR_ID + ")", true);
        }


        protected void LabSampleEdit_Click(object sender, EventArgs e)
        {

            ViewState["LabTestReportID"] = "";

            ViewState["LabSampleID"] = "";

            ViewState["PatientID"] = "";
            ViewState["LabSampleBareCode"] = "";
            ViewState["DoctorID"] = "";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            gvLabSamplePopup.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


            Label lblgvLabSampleID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSampleID");

            Label lblgvLabSamplePTID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSamplePTID");
            Label lblgvLabSampleBareCode = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSampleBareCode");

            Label lblDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblDoctorID");



            ViewState["LabSampleID"] = lblgvLabSampleID.Text;

            ViewState["PatientID"] = lblgvLabSamplePTID.Text;
            ViewState["LabSampleBareCode"] = lblgvLabSampleBareCode.Text;
            ViewState["DoctorID"] = lblDoctorID.Text;
            BindLabTestRegtMaster();
        }

        protected void LabTestReportEdit_Click(object sender, EventArgs e)
        {

            ViewState["LabTestReportID"] = "";

            ViewState["LabSampleID"] = "";

            ViewState["PatientID"] = "";
            ViewState["LabSampleBareCode"] = "";
            ViewState["DoctorID"] = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            gvLabTestReport.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

            Label lblgvLabTestReportID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabTestReportID");

            Label lblgvLabTestSampleID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabTestSampleID");

            Label lblgvLabTestPTID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabTestPTID");
            Label lblgvLabTestBareCode = (Label)gvScanCard.Cells[0].FindControl("lblgvLabTestBareCode");

            Label lblgvLabTestDRCode = (Label)gvScanCard.Cells[0].FindControl("lblgvLabTestDRCode");

            ViewState["LabTestReportID"] = lblgvLabTestReportID.Text;

            ViewState["LabSampleID"] = lblgvLabTestSampleID.Text;

            ViewState["PatientID"] = lblgvLabTestPTID.Text;
            ViewState["LabSampleBareCode"] = lblgvLabTestBareCode.Text;
            ViewState["DoctorID"] = lblgvLabTestDRCode.Text;
            BindLabTestRegtMaster();
        }



        protected void btnLabTestRegister_Click(object sender, EventArgs e)
        {
            //if (Convert.ToString(ViewState["LabSampleID"]) == "" || Convert.ToString(ViewState["LabSampleID"]) == null)
            //{
            //    goto FunEnd;
            //}

            Response.Redirect("LabTestReport.aspx?MenuName=Laboratory&PageName=LabWaitList&InvoiceID=" + Convert.ToString(ViewState["InvoiceID"]) + "&EMRID=" + Convert.ToString(ViewState["EMRID"]) + "&PatientID=" + Convert.ToString(ViewState["PatientID"]) + "&DoctorID=" + Convert.ToString(ViewState["DoctorID"]) + "&LabSampleID=" + Convert.ToString(ViewState["LabSampleID"]) + "&LabSampleBareCode=" + Convert.ToString(ViewState["LabSampleBareCode"]) + "&LabTestReportID=" + Convert.ToString(ViewState["LabTestReportID"]));


        FunEnd: ;

        }

        protected void LabTestReportShow_Click(object sender, EventArgs e)
        {

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

         

            Label lblgvLabSampleID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSampleID");


            BindTestResultData(lblgvLabSampleID.Text);

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTestResultDataPopup()", true);
        }

    }
}