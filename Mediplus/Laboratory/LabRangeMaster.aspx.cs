﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus.Laboratory
{
    public partial class LabRangeMaster : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindRangeGrid()
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";
            ds = obLab.RangeTypeGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                gvRangeMaster.DataSource = ds;
                gvRangeMaster.DataBind();

            }
        }

        void BindRange()
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";
            Criteria += " AND LNRT_RANGE_ID='" + txtRangeID.Text.Trim() + "'";
            ds = obLab.RangeTypeGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;
                foreach (DataRow DR in ds.Tables[0].Rows)
                {
                    txtDescription.Text = Convert.ToString(DR["LNRT_DESCRIPTION"]);
                    drpStatus.SelectedValue = Convert.ToString(DR["LNRT_STATUS"]);
                }


            }
        }

        Boolean fnSave()
        {
            Boolean isError = false;
            if (txtRangeID.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Code";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtDescription.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Description";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }



            objLab = new LaboratoryBAL();


            objLab.LNRT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objLab.LNRT_RANGE_ID = txtRangeID.Text.Trim();
            objLab.LNRT_STATUS = drpStatus.SelectedValue;
            objLab.LNRT_DESCRIPTION = txtDescription.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }

            objLab.UserID = Convert.ToString(Session["User_ID"]);
            objLab.RangeTypeAdd();


        FunEnd: ;


            return isError;
        }


        void Clear()
        {
            ViewState["NewFlag"] = true;

            txtRangeID.Text = "";
            txtDescription.Text = "";
            drpStatus.SelectedIndex = 0;
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "LAB_PROFILE";
            objCom.ScreenName = "Lab Range Master";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_PROFILE' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Lab Range Master Page");

                try
                {
                    ViewState["NewFlag"] = true;

                    BindRangeGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtRangeID_TextChanged(object sender, EventArgs e)
        {
            txtDescription.Text = "";
            drpStatus.SelectedIndex = 0;
            BindRange();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {
                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Range Master, Range ID is " + txtRangeID.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Lab Range Master, Range ID is " + txtRangeID.Text);
                    }

                    Clear();
                    BindRangeGrid();
                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void TestRangeEdit_Click(object sender, EventArgs e)
        {
            Clear();
            ViewState["NewFlag"] = false;
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvRangeID = (Label)gvScanCard.Cells[0].FindControl("lblgvRangeID");
            Label lblgvRangeDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvRangeDesc");
            Label lblgvRangeStatus = (Label)gvScanCard.Cells[0].FindControl("lblgvRangeStatus");


            txtRangeID.Text = lblgvRangeID.Text;
            txtDescription.Text = lblgvRangeDesc.Text;
            drpStatus.SelectedValue = lblgvRangeStatus.Text;



        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

    }
}