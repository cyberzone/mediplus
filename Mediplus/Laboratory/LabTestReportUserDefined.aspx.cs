﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;


namespace outlook_menu.Laboratory
{
    public partial class LabTestReportUserDefined : System.Web.UI.Page
    {
        LaboratoryBAL objLab = new LaboratoryBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public string getURL()
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl("~/");
        }

        void BindData()
        {
            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1  and LTRDU_DATA_LOAD_FROM='WEB' ";
            Criteria += " AND  LTRDU_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LTRDU_TEST_REPORT_ID ='" + Convert.ToString(ViewState["TestReportID"]) + "'";

            DS = objLab.LABReportDtlsUsrGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;
                txtContent.Text = Server.HtmlDecode(Convert.ToString(DS.Tables[0].Rows[0]["LTRDU_TEST_RESULT"]));
            }
        }

      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    ViewState["NewFlag"] = true;

                    hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                    ViewState["TestReportID"] = Convert.ToString(Request.QueryString["TestReportID"]);


                    BindData();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      LabTestInvestProfilePopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(ViewState["TestReportID"]) == "")
            {
                lblStatus.Text = "Profile Not Selected";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            objLab = new LaboratoryBAL();


            IDictionary<string, string> Param = new Dictionary<string, string>();
            objLab.LTRDU_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objLab.LTRDU_TEST_REPORT_ID = Convert.ToString(ViewState["TestReportID"]);


            string tempStr = "";
            tempStr = Server.HtmlEncode(txtContent.Text);


            objLab.LTRDU_TEST_RESULT = tempStr;


            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }



            objLab.UserID = Convert.ToString(Session["User_ID"]);

            objLab.LABReportDtlsUsrAdd();

            lblStatus.Text = "Data Saved";
            lblStatus.ForeColor = System.Drawing.Color.Green;

        FunEnd: ;
        }

    }
}