﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace HMS.Laboratory
{
    public partial class LabTestProfileMaster : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindReportCategory()
        {

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 AND LTC_STATUS='A' ";
            ds = objLab.TestCategoryGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpReportCategory.DataSource = ds;
                drpReportCategory.DataTextField = "LTC_DESCRIPTION";
                drpReportCategory.DataValueField = "LTC_CODE";
                drpReportCategory.DataBind();
            }

            drpReportCategory.Items.Insert(0, "--- Select ---");
            drpReportCategory.Items[0].Value = "0";
        }


        void BindTestType()
        {

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 AND LTT_STATUS='A' ";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue("LTT_CODE,LTT_DESCRIPTION", "LAB_TEST_TYPE", Criteria, "LTT_CODE");
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpTestType.DataSource = ds;
                drpTestType.DataTextField = "LTT_DESCRIPTION";
                drpTestType.DataValueField = "LTT_CODE";
                drpTestType.DataBind();
            }

            drpTestType.Items.Insert(0, "--- Select ---");
            drpTestType.Items[0].Value = "";
        }


        void BindTestProfileMaster()
        {


            //LTPM_TEST_PROFILE_ID
            //LTPM_DESCRIPTION
            //LTPM_STATUS
            //LTPM_UNIT
            //LTPM_REMARK
            //LTPM_SERV_ID
            //LTPM_SERV_DESCRIPTION
            //LTPM_FEE
            //LTPM_COMMIT_FLAG
            //LTPM_CREATED_USER
            //LTPM_CREATED_DATE
            //LTPM_CATEGORY
            //LTPM_DSPLY_NRML_VALUES
            //LTPM_SAMP_REQ
            //LTPM_MATERIAL
            //LTPM_TURN_AROUND_TIME
            //LTPM_CODE_D
            //LTPM_CPT_CODE
            //LTPM_TEST_CATEGORY
            //LTPM_METHOD



            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LTPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LTPM_TEST_PROFILE_ID='" + txtProfileID.Text.Trim() + "'";


            DS = objLab.TestProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;
                txtDescription.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_DESCRIPTION"]);
                drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_STATUS"]);
                txtUnit.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_UNIT"]);
                txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_SERV_ID"]);
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_SERV_DESCRIPTION"]);
                txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_FEE"]);

                if (DS.Tables[0].Rows[0].IsNull("LTPM_TEST_CATEGORY") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_CATEGORY"]) != "")
                {
                    drpReportCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_CATEGORY"]);
                }

                txtSampleReq.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_SAMP_REQ"]);
                txtMaterial.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_MATERIAL"]);
                // txtMaterial.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TURN_AROUND_TIME"]);

                txtCPTCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_CPT_CODE"]);
                // txtCPTName.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_CPT_CODE"]);

                txtMethod.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_METHOD"]);
                txtAddReference.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_ADDITIONAL_REFERENCE"]);


                if (DS.Tables[0].Rows[0].IsNull("LTPM_TEST_TYPE_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_CODE"]) != "")
                {
                    drpTestType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_CODE"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["LTPM_DSPLY_NRML_VALUES"]) == "Y")
                {
                    chkDisplayNormalValues.Checked = true;
                }
                else
                {
                    chkDisplayNormalValues.Checked = false;
                }
            }


        }


        void BuildeTestProfileTrans()
        {
            string Criteria = "1=2";
            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestProfileTransGet(Criteria);

            ViewState["TestProfileTrans"] = DS.Tables[0];
        }

        void BindTestProfileTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTPT_TEST_PROFILE_ID='" + txtProfileID.Text.Trim() + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestProfileTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["TestProfileTrans"] = DS.Tables[0];

            }
        }

        void BindTempTestProfileTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["TestProfileTrans"];
            if (DT.Rows.Count > 0)
            {
                gvTestProfTrans.DataSource = DT;
                gvTestProfTrans.DataBind();

            }
            else
            {
                gvTestProfTrans.DataBind();
            }

        }

        void GetServiceDtls()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='L' ";
            Criteria += " AND HSM_SERV_ID LIKE '" + txtServCode.Text.Trim() + "%'";


            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_NAME LIKE '" + txtServName.Text.Trim() + "%'";
            //}
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_FEE"]);
            }

        }

        void GridRowRefresh(Int32 intCurRow)
        {


            DropDownList drpgvTestProfTransSex = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransSex");
            DropDownList drpgvTestProfTransAgeFromType = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransAgeFromType");
            DropDownList drpgvTestProfTransAgeToType = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransAgeToType");

            TextBox txtgvTestProfTransNorRange = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransNorRange");
            TextBox txtgvTestProfTransAgeFrom = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransAgeFrom");
            TextBox txtgvTestProfTransAgeTo = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransAgeTo");
            TextBox txtgvTestProfTransAbnorRange = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransAbnorRange");


            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TestProfileTrans"];



            DT.Rows[intCurRow]["LTPT_NORMAL_RANGE"] = txtgvTestProfTransNorRange.Text.Trim();
            DT.Rows[intCurRow]["LTPT_SEX"] = drpgvTestProfTransSex.SelectedValue;
            DT.Rows[intCurRow]["LTPT_AGE_FROM"] = txtgvTestProfTransAgeFrom.Text.Trim() == "" ? "1" : txtgvTestProfTransAgeFrom.Text.Trim();
            DT.Rows[intCurRow]["LTPT_AGE_TO"] = txtgvTestProfTransAgeTo.Text.Trim() == "" ? "100" : txtgvTestProfTransAgeTo.Text.Trim();
            DT.Rows[intCurRow]["LTPT_AGE_TYPE"] = drpgvTestProfTransAgeFromType.SelectedValue;
            DT.Rows[intCurRow]["LTPT_AGETO_TYPE"] = drpgvTestProfTransAgeToType.SelectedValue;
            DT.Rows[intCurRow]["LTPT_ABNORMAL_RANGE"] = txtgvTestProfTransAbnorRange.Text.Trim();

            TextBox txtgvTestProfTransRangeFrom = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransRangeFrom");
            TextBox txtgvTestProfTransRangeTo = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransRangeTo");
            DropDownList drpgvTestProfTransRangeType = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransRangeType");

            DT.Rows[intCurRow]["LTPT_RANGE_VALUE_FROM"] = txtgvTestProfTransRangeFrom.Text.Trim();
            DT.Rows[intCurRow]["LTPT_RANGE_VALUE_TO"] = txtgvTestProfTransRangeTo.Text.Trim();
            DT.Rows[intCurRow]["LTPT_RANGE_TYPE"] = drpgvTestProfTransRangeType.SelectedValue;


            if (txtgvTestProfTransRangeFrom.Text.Trim() != "" && txtgvTestProfTransRangeTo.Text.Trim() != "")
            {
                if (txtgvTestProfTransNorRange.Text.Trim() == "")
                {
                    DT.Rows[intCurRow]["LTPT_NORMAL_RANGE"] = txtgvTestProfTransRangeFrom.Text.Trim() + " - " + txtgvTestProfTransRangeTo.Text.Trim();
                }
            }
            DT.AcceptChanges();
            ViewState["TestProfileTrans"] = DT;
            BindTempTestProfileTrans();


        }


        void GridAllRowRefresh()
        {
            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TestProfileTrans"];

            for (int intCurRow = 0; intCurRow < gvTestProfTrans.Rows.Count; intCurRow++)
            {
                DropDownList drpgvTestProfTransSex = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransSex");
                DropDownList drpgvTestProfTransAgeFromType = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransAgeFromType");
                DropDownList drpgvTestProfTransAgeToType = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransAgeToType");

                TextBox txtgvTestProfTransNorRange = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransNorRange");
                TextBox txtgvTestProfTransAgeFrom = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransAgeFrom");
                TextBox txtgvTestProfTransAgeTo = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransAgeTo");
                TextBox txtgvTestProfTransAbnorRange = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransAbnorRange");


                DT.Rows[intCurRow]["LTPT_NORMAL_RANGE"] = txtgvTestProfTransNorRange.Text.Trim();
                DT.Rows[intCurRow]["LTPT_SEX"] = drpgvTestProfTransSex.SelectedValue;
                DT.Rows[intCurRow]["LTPT_AGE_FROM"] = txtgvTestProfTransAgeFrom.Text.Trim() == "" ? "1" : txtgvTestProfTransAgeFrom.Text.Trim();
                DT.Rows[intCurRow]["LTPT_AGE_TO"] = txtgvTestProfTransAgeTo.Text.Trim() == "" ? "100" : txtgvTestProfTransAgeTo.Text.Trim();
                DT.Rows[intCurRow]["LTPT_AGE_TYPE"] = drpgvTestProfTransAgeFromType.SelectedValue;
                DT.Rows[intCurRow]["LTPT_AGETO_TYPE"] = drpgvTestProfTransAgeToType.SelectedValue;
                DT.Rows[intCurRow]["LTPT_ABNORMAL_RANGE"] = txtgvTestProfTransAbnorRange.Text.Trim();

                TextBox txtgvTestProfTransRangeFrom = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransRangeFrom");
                TextBox txtgvTestProfTransRangeTo = (TextBox)gvTestProfTrans.Rows[intCurRow].FindControl("txtgvTestProfTransRangeTo");
                DropDownList drpgvTestProfTransRangeType = (DropDownList)gvTestProfTrans.Rows[intCurRow].FindControl("drpgvTestProfTransRangeType");

                DT.Rows[intCurRow]["LTPT_RANGE_VALUE_FROM"] = txtgvTestProfTransRangeFrom.Text.Trim();
                DT.Rows[intCurRow]["LTPT_RANGE_VALUE_TO"] = txtgvTestProfTransRangeTo.Text.Trim();
                DT.Rows[intCurRow]["LTPT_RANGE_TYPE"] = drpgvTestProfTransRangeType.SelectedValue;


                DT.AcceptChanges();

            }

            ViewState["TestProfileTrans"] = DT;

        }

        void Clear()
        {
            ViewState["NewFlag"] = true;

            txtDescription.Text = "";
            drpStatus.SelectedIndex = 0;

            txtSampleReq.Text = "";
            txtMaterial.Text = "";
            txtUnit.Text = "";

            txtServCode.Text = "";
            txtServName.Text = "";
            txtFee.Text = "";

            txtCPTCode.Text = "";
            txtCPTName.Text = "";
            txtMethod.Text = "";
            txtAddReference.Text = "";
            chkDisplayNormalValues.Checked = false;

            if (drpReportCategory.Items.Count > 0)
            {
                drpReportCategory.SelectedIndex = 0;
            }

            if( drpTestType.Items.Count >0)
            {
                drpTestType.SelectedIndex = 0;
            }

            ViewState["gvServSelectIndex"] = "";

            BuildeTestProfileTrans();
            BindTempTestProfileTrans();

        }


        Boolean fnSave()
        {
            Boolean isError = false;
            if (txtProfileID.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Code";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtDescription.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Description";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }

            if (txtServCode.Text.Trim() == "")
            {
                lblStatus.Text = "Enter Service Code";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            if (txtServName.Text.Trim() == "")
            {
                lblStatus.Text = "Enter Service Name";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            objLab = new LaboratoryBAL();
            objLab.LTPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);


            objLab.LTPM_TEST_PROFILE_ID = txtProfileID.Text.Trim();
            objLab.LTPM_DESCRIPTION = txtDescription.Text.Trim();
            objLab.LTPM_STATUS = drpStatus.SelectedValue;
            objLab.LTPM_UNIT = txtUnit.Text.Trim();
            objLab.LTPM_REMARK = "";
            objLab.LTPM_SERV_ID = txtServCode.Text.Trim();
            objLab.LTPM_SERV_DESCRIPTION = txtServName.Text.Trim();
            objLab.LTPM_FEE = txtFee.Text.Trim();

            objLab.LTPM_CATEGORY = "";

            string strNormal = "N";
            if (chkDisplayNormalValues.Checked == true)
            {
                strNormal = "Y";
            }
            else
            {
                strNormal = "N";
            }

            objLab.LTPM_DSPLY_NRML_VALUES = strNormal;
            objLab.LTPM_SAMP_REQ = txtSampleReq.Text.Trim();
            objLab.LTPM_MATERIAL = txtMaterial.Text.Trim();
            objLab.LTPM_TURN_AROUND_TIME = "";
            objLab.LTPM_CODE_D = "";
            objLab.LTPM_CPT_CODE = txtCPTCode.Text;
            objLab.LTPM_TEST_CATEGORY = drpReportCategory.SelectedValue;
            objLab.LTPM_METHOD = txtMethod.Text;
            objLab.LTPM_ADDITIONAL_REFERENCE = txtAddReference.Text;
            objLab.TEST_TYPE_CODE = drpTestType.SelectedValue;
            objLab.TEST_TYPE_DESC = drpTestType.SelectedItem.Text;

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }



            objLab.UserID = Convert.ToString(Session["User_ID"]);
            objLab.TestProfileMasterAdd();


            objCom = new CommonBAL();
            string Criteria = " LTPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTPT_TEST_PROFILE_ID='" + txtProfileID.Text.Trim() + "'";
            objCom.fnDeleteTableData("LAB_TEST_PROFILE_TRANS", Criteria);

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TestProfileTrans"];

            Int32 intCurRow = 1;
            foreach (DataRow DR in DT.Rows)
            {

                objLab.LTPT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objLab.LTPT_TEST_PROFILE_ID = txtProfileID.Text.Trim();
                objLab.LTPT_NORMAL_TYPE = Convert.ToString(DR["LTPT_NORMAL_TYPE"]);
                objLab.LTPT_NORMAL_RANGE = Convert.ToString(DR["LTPT_NORMAL_RANGE"]);
                objLab.LTPT_RANGE_DESCRIPTION = Convert.ToString(DR["LTPT_RANGE_DESCRIPTION"]);
                objLab.LTPT_SEX = Convert.ToString(DR["LTPT_SEX"]);
                objLab.LTPT_AGE_FROM = Convert.ToString(DR["LTPT_AGE_FROM"]);

                objLab.LTPT_AGE_TO = Convert.ToString(DR["LTPT_AGE_TO"]);
                objLab.LTPT_AGE_TYPE = Convert.ToString(DR["LTPT_AGE_TYPE"]);
                objLab.LTPT_AGETO_TYPE = Convert.ToString(DR["LTPT_AGETO_TYPE"]);
                // objLab.LTPT_SLNO = Convert.ToString(intCurRow);
                objLab.LTPT_ABNORMAL_RANGE = Convert.ToString(DR["LTPT_ABNORMAL_RANGE"]);

                objLab.LTPT_RANGE_VALUE_FROM = Convert.ToString(DR["LTPT_RANGE_VALUE_FROM"]);
                objLab.LTPT_RANGE_VALUE_TO = Convert.ToString(DR["LTPT_RANGE_VALUE_TO"]);
                objLab.LTPT_RANGE_TYPE = Convert.ToString(DR["LTPT_RANGE_TYPE"]);

                 

                objLab.TestProfileTransAdd();

                intCurRow = intCurRow + 1;
            }

        FunEnd: ;
            return isError;
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "LAB_PROFILE";
            objCom.ScreenName = "Lab Profile Master";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_PROFILE' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetMethod(string prefixText)
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string[] Data;

            string Criteria = " 1=1 ";

            Criteria += " AND ( LTPM_METHOD LIKE '%" + prefixText + "%')";

            ds = obLab.TestProfileMasterGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["LTPM_METHOD"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetRangeType(string prefixText)
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string[] Data;

            string Criteria = " 1=1 and LNRT_STATUS='A' ";


            Criteria += " AND ( LNRT_RANGE_ID LIKE '%" + prefixText + "%' OR LNRT_DESCRIPTION LIKE'%" + prefixText + "%' )";
            ds = obLab.RangeTypeGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["LNRT_RANGE_ID"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["LNRT_DESCRIPTION"]).Trim();



                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Lab Test Profile Master Page");

                try
                {
                    ViewState["NewFlag"] = true;
                    BindReportCategory();
                    BindTestType();

                    BuildeTestProfileTrans();

                    string ProfileID = Convert.ToString(Request.QueryString["ProfileID"]);

                    if (ProfileID != " " && ProfileID != null)
                    {

                        txtProfileID.Text = Convert.ToString(ProfileID);
                        txtProfileID_TextChanged(txtProfileID, new EventArgs());

                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtProfileID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();

                BindTestProfileMaster();
                BindTestProfileTrans();
                BindTempTestProfileTrans();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                GridAllRowRefresh();

                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {

                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Test Profile Master, Profile ID is " + txtProfileID.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Lab Test Profile Master, Profile ID is " + txtProfileID.Text);
                    }

                    Clear();
                    txtProfileID.Text = "";
                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void TestProfTransEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvTestProfTransNorType = (Label)gvScanCard.Cells[0].FindControl("lblgvTestProfTransNorType");
            Label lblgvTestProfTransDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvTestProfTransDesc");

            txtRangeName.Text = lblgvTestProfTransNorType.Text + "~" + lblgvTestProfTransDesc.Text;
        }


        protected void gvTestProfTrans_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {



                    Label lblgvTestProfTransSex = (Label)e.Row.Cells[0].FindControl("lblgvTestProfTransSex");
                    DropDownList drpgvTestProfTransSex = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestProfTransSex");


                    Label lblgvTestProfTransAgeFromType = (Label)e.Row.Cells[0].FindControl("lblgvTestProfTransAgeFromType");
                    DropDownList drpgvTestProfTransAgeFromType = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestProfTransAgeFromType");


                    Label lblgvTestProfTransAgeToType = (Label)e.Row.Cells[0].FindControl("lblgvTestProfTransAgeToType");
                    DropDownList drpgvTestProfTransAgeToType = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestProfTransAgeToType");

                    Label lblgvTestProfTransRangeType = (Label)e.Row.Cells[0].FindControl("lblgvTestProfTransRangeType");
                    DropDownList drpgvTestProfTransRangeType = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestProfTransRangeType");


                    if (lblgvTestProfTransSex.Text != "")
                    {
                        drpgvTestProfTransSex.SelectedValue = lblgvTestProfTransSex.Text;
                    }


                    if (lblgvTestProfTransAgeFromType.Text != "")
                    {
                        drpgvTestProfTransAgeFromType.SelectedValue = lblgvTestProfTransAgeFromType.Text;
                    }

                    if (lblgvTestProfTransAgeToType.Text != "")
                    {
                        drpgvTestProfTransAgeToType.SelectedValue = lblgvTestProfTransAgeToType.Text;
                    }

                    if (lblgvTestProfTransRangeType.Text != "")
                    {
                        drpgvTestProfTransRangeType.SelectedValue = lblgvTestProfTransRangeType.Text;
                    }


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvTestProfTrans_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["TestProfileTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["TestProfileTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["TestProfileTrans"] = DT;

                }


                BindTempTestProfileTrans();



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtRangeName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Range Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestProfileTrans"];

                strTestCodeDesc = txtRangeName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    lblStatus.Text = "Enter Range Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["LTPT_NORMAL_TYPE"] = TestProfileID;
                    DT.Rows[R]["LTPT_RANGE_DESCRIPTION"] = TestProfileDesc;
                    //DT.Rows[R]["LTPT_NORMAL_RANGE"] = "";
                    //DT.Rows[R]["LTPT_SEX"] = "";
                    //DT.Rows[R]["LTPT_AGE_FROM"] = "";
                    //DT.Rows[R]["LTPT_AGE_TO"] = "";
                    //DT.Rows[R]["LTPT_AGE_TYPE"] = "";
                    //DT.Rows[R]["LTPT_AGETO_TYPE"] = "";


                    DT.AcceptChanges();
                }
                else
                {

                    objrow["LTPT_NORMAL_TYPE"] = TestProfileID;
                    objrow["LTPT_RANGE_DESCRIPTION"] = TestProfileDesc;
                    objrow["LTPT_NORMAL_RANGE"] = "";
                    objrow["LTPT_SEX"] = "B";
                    objrow["LTPT_AGE_FROM"] = "1";
                    objrow["LTPT_AGE_TO"] = "100";
                    objrow["LTPT_AGE_TYPE"] = "Years";
                    objrow["LTPT_AGETO_TYPE"] = "Years";



                    DT.Rows.Add(objrow);
                }



                ViewState["TestProfileTrans"] = DT;


                BindTempTestProfileTrans();
                txtRangeName.Text = "";

                ViewState["gvServSelectIndex"] = "";
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void gvTestProfTrans_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;


                Int32 R = currentRow.RowIndex;

                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      LabTestProfileMaster.drpgvDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvTestProfTrans_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;



                Int32 R = currentRow.RowIndex;

                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      LabTestProfileMaster.drpgvDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            GetServiceDtls();
        }

    }
}