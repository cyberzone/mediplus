﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="SampleCollection1.aspx.cs" Inherits="Mediplus.Laboratory.SampleCollection1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ShowServMasterPopup() {

            document.getElementById("divSerMasterPopup").style.display = 'block';


        }

        function HideServMasterPopup() {

            document.getElementById("divSerMasterPopup").style.display = 'none';

        }


        function ShowSampleMasterPopup() {

            document.getElementById("divSampleMasterPopup").style.display = 'block';


        }

        function HideSampleMasterPopup() {

            document.getElementById("divSampleMasterPopup").style.display = 'none';

        }









        function ShowWaitingList() {
            var win = window.open('../Laboratory/LabTestRegisterPopup.aspx?PageName=SampleCollection', 'RadWaitList', 'menubar=no,left=100,top=200,height=650,width=1075,scrollbars=1')

            win.focus();

        }



        function BareCodePrint(ServCode) {

            // w = window.open('C:\inetpub\wwwroot\Mediplus\PrintBar.txt');
            //  w.print();

            exec('"Notepad /p " & C:\inetpub\wwwroot\Mediplus\PrintBar.txt', '');
        }


        function CallBareCodeexe(SampleID, FileNo, PTName, TestDesc, TestDate, BareCodeID) {

            //  BarcodePrint.EXE PatientName%UniqueId%Location%Philebotomist%DateTimePrint

            alert('SampleID:' + SampleID + '  FileNo:' + FileNo + '  Name:' + PTName + '  Test:' + TestDesc + '  Date:' + TestDate + '  BareCodeID: ' + BareCodeID);


            exec('D:\\HMS\\BarcodePrint.exe ', FileNo + '%' + PTName + '%' + BareCodeID + '%OP%' + TestDesc + '%' + TestDate)



        }

        function ShowLabelPrint(DBString, Formula, PrintCount, ServCode, PrintCount, EDTA, SERUM, FLUORIDE) {

            var Report = "LabLabel.rpt"
            if (EDTA == 'true' || EDTA == 'True') {
                // var Report = "LabLabelEDTA.rpt"
                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\'$PRINT$$$1');
            }
            if (SERUM == 'true' || SERUM == 'True') {
                //  var Report = "LabLabelSERUM.rpt"
                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\'$PRINT$$$1');
            }
            if (FLUORIDE == 'true' || FLUORIDE == 'True') {
                // var Report = "LabLabelFLUORIDE.rpt"
                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\'$PRINT$$$1');
            }

            for (i = 0; i < PrintCount ; i++) {
                // var Report = "LabLabel.rpt"
                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\'$PRINT$$$1');

            }


            var Criteria = " 1=1 ";
            Criteria += 'AND {LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\''
            //    var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();
        }

        function ShowQRPrint(DBString, Formula) {
            var Report = "LabLabel_QRCode.rpt"
            // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\'$PRINT$$$' + PrintCount);
            //  var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\' AND {LAB_TEST_SAMPLE_DETAIL.LTSD_TEST_PROFILE_ID}=\'' + ServCode + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            //   win.focus();
            var Criteria = " 1=1 ";
            Criteria += 'AND {LAB_TEST_SAMPLE_MASTER.LTSM_TEST_SAMPLE_ID}=\'' + Formula + '\''
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }


        function ShowErrorMessage(vMessage1, vMessage2) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
            document.getElementById("divMessage").style.display = 'block';

        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }
        function SaveValidation() {


            var LabelPrint = document.getElementById('<%=hidBareCodePrint.ClientID%>').value
            if (LabelPrint == "1") {
                var isLabelPrint = window.confirm('Do you want to print Label ? ');
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = isLabelPrint;
            }
            else {
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = 'false';

            }
            return true;
        }

        function HideSearchDiv() {

            document.getElementById("divSearch").style.display = 'none';
            document.getElementById("divAddSampleColl").style.display = 'block';
            document.getElementById("imgBack").style.display = 'block';

        }

        function HideSampleCollDiv() {
            document.getElementById("divSearch").style.display = 'block';
            document.getElementById("divAddSampleColl").style.display = 'none';
            document.getElementById("imgBack").style.display = 'none';

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidActualSampleID" runat="server" />
    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <input type="hidden" id="hidBareCodePrint" runat="server" value="0" />

    <input type="hidden" id="hidLabelPrint" runat="server" />

    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>
    <table style="width: 85%;">
        <tr>
            <td style="text-align: left;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Sample Collection"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
            </td>

            <td style="text-align: right;">

                <img src="../Images/Back.png" id="imgBack" onclick="HideSampleCollDiv()" style="width: 40px; height: 40px; display: none;" />
            </td>
        </tr>
    </table>
    <div id="divSearch">

        <table width="85%"   cellpadding="3" cellspacing="3" >

            <tr>

                <td class="lblCaption1" style="height: 25px;">From Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransFromDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1" style="height: 25px;">To Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransToDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtTransToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1" style="height: 25px;">Doctor
                </td>
                <td style="width: 200px;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcDoctor" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
            <tr>

                <td class="lblCaption1" style="height: 25px;">File No
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFileNo" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFName" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Insurance
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcInsurance" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnShow" runat="server" CssClass="button red small" Text="Show" OnClick="btnShow_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>

        <div style="padding-top: 0px; width: 85%; overflow: auto; height: 90%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvServiceList" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" CellPadding="5" CellSpacing="5" Width="100%" GridLines="Both">
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="30px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                        OnClick="Edit_Click" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="File No." HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" CssClass="GridRow" runat="server" Text='<%# Bind("ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("FileNo") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" HeaderStyle-Width="70px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Patient Name" HeaderStyle-Width="100px">
                                <ItemTemplate>

                                    <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("PTName") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Doctor" HeaderStyle-Width="100px">
                                <ItemTemplate>

                                    <asp:Label ID="lblDoctor" CssClass="GridRow" runat="server" Text='<%# Bind("Doctor") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Insurance" HeaderStyle-Width="100px">
                                <ItemTemplate>

                                    <asp:Label ID="lblInsurance" CssClass="GridRow" runat="server" Text='<%# Bind("Insurance") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Sample Type" HeaderStyle-Width="100px">
                                <ItemTemplate>

                                    <asp:Label ID="lblTestTypeCode" CssClass="GridRow" runat="server" Text='<%# Bind("TEST_TYPE_CODE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblTestTypeDesc" CssClass="GridRow" runat="server" Text='<%# Bind("TEST_TYPE_DESC") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Test Name" HeaderStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblServiceID" CssClass="GridRow" runat="server" Text='<%# Bind("ServiceID") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="lblServiceDesc" CssClass="GridRow" runat="server" Text='<%# Bind("Description") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>



                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

    </div>

    <div id="divAddSampleColl" style="display: none;">
        <table style="width: 85%;">
            <tr>
                <td style="text-align: right;">
                    <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record"   OnClick="btnSave_Click" OnClientClick="return  SaveValidation();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table style="width: 85%;">
            <tr>
                <td style="vertical-align: top; width: 20%;">

                    <span class="lblCaption1" style="font-weight: bold;">Patient Detals</span>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">File No
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFileNo" runat="server" Width="160px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Name
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFName" runat="server" Width="160px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Sex
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSex" runat="server" Width="160px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">DOB
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtDOB" runat="server" Width="160px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Age
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAge" runat="server" Width="25px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                        <asp:DropDownList ID="drpAgeType" runat="server" Style="width: 50px; font-size: 9px;" class="TextBoxStyle" ReadOnly="true">
                                            <asp:ListItem Value="Y">Years</asp:ListItem>
                                            <asp:ListItem Value="M">Months</asp:ListItem>
                                            <asp:ListItem Value="D">Days</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtAge1" runat="server" Width="25px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                        <asp:DropDownList ID="drpAgeType1" runat="server" Style="width: 50px; font-size: 9px;" class="TextBoxStyle" ReadOnly="true">
                                            <asp:ListItem Value="Y">Years</asp:ListItem>
                                            <asp:ListItem Value="M">Months</asp:ListItem>
                                            <asp:ListItem Value="D">Days</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Nationality</td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" Width="162px" ReadOnly="true"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>

                </td>
                <td style="vertical-align: top; width: 65%;">

                    <span class="lblCaption1" style="font-weight: bold;">Transaction</span>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Sample ID
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtTestSampleID" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle" ondblclick="ShowSampleMasterPopup();"></asp:TextBox>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="txtTestSampleID" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>

                            <td class="lblCaption1" style="height: 25px;">Date
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtTransDate" runat="server" Width="70px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                        :
                                      <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td class="lblCaption1" style="height: 25px;">Status
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpStatus" runat="server" Style="width: 162px" class="TextBoxStyle">
                                            <asp:ListItem Value="Processing" Selected="True">Processing</asp:ListItem>
                                            <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                            <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>

                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Type
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="162px">
                                            <asp:ListItem Value="Cash" Selected="True">Cash</asp:ListItem>
                                            <asp:ListItem Value="Credit">Credit</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>

                                </asp:UpdatePanel>
                            </td>

                            <td class="lblCaption1" style="height: 25px;">PT. Type
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpPTType" runat="server" CssClass="TextBoxStyle" Width="162px">
                                            <asp:ListItem Value="OP">OP</asp:ListItem>
                                            <asp:ListItem Value="IP">IP</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>

                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1" style="height: 25px;">Deliv. Date
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtDeliveryDate" runat="server" Width="160px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" TargetControlID="txtDeliveryDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>

                            <td class="lblCaption1" style="height: 25px;">Report Type

                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpReportType" runat="server" Style="width: 160px" class="TextBoxStyle" AutoPostBack="True" OnSelectedIndexChanged="drpReportType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="drpReportType" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Barcode ID
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtBarecodeID" runat="server" Width="160px" Height="20px" CssClass="TextBoxStyle" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                             <td class="lblCaption1">Invoice Ref #
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtInvoiceRefNo" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Doctor Name
                            </td>


                            <td>
                                <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDoctors" runat="server" Style="width: 160px" class="TextBoxStyle"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                            <td class="lblCaption1" style="height: 25px;">Ref. Doctor
                            </td>

                            <td>
                                <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpRefDoctor" runat="server" Style="width: 160px" class="TextBoxStyle">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                             <td class="lblCaption1">EMR #
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtEMRID" runat="server" Width="150px" Height="20px" CssClass="TextBoxStyle"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="height: 30px;">Remarks
                            </td>
                            <td colspan="5">
                                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtRemarks" runat="server" Width="100%" Height="30px" CssClass="TextBoxStyle" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>


                    <span class="lblCaption1" style="font-weight: bold;">Insurance Detals</span>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="lblCaption1" style="height: 25px;">Company
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProviderID" runat="server" CssClass="TextBoxStyle" ReadOnly="true" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="txtProviderName" runat="server" Width="160px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1" style="height: 25px;">Policy No
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtPolicyNo" runat="server" Width="160px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
        <span class="lblCaption1" style="font-weight: bold;">Diagnosis</span>
        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvDiagnosis" runat="server" Width="85%" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                    EnableModelValidation="True" GridLines="Both">
                    <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                    <RowStyle CssClass="GridRow" />

                    <Columns>

                        <asp:TemplateField HeaderText="ICD Code" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>



                                <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px" Width="100px" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'> </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis" HeaderStyle-Width="600px" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px" Width="100%" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>




                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

        <table width="90%" border="0" cellpadding="3" cellspacing="3">
            <tr>
                <td colspan="3" class="lblCaption1">Test Code & Description  
                             
                </td>


            </tr>
            <tr>
                <td width="620px">

                    <asp:TextBox ID="txtTestName" runat="server" Width="500px" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtTestName" MinimumPrefixLength="1" ServiceMethod="GetTestProfileMaster"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                    </asp:AutoCompleteExtender>

                </td>
                <td>

                    <asp:Button ID="btnAdd" runat="server" CssClass="button red small" Text="Add" OnClick="btnAdd_Click" />

                </td>
                <td>
                    <asp:Button ID="btnLabelPrint" runat="server" CssClass="button gray small" Width="100px" Text="Print Label" ToolTip="Label Print" OnClick="btnLabelPrint_Click" />

                    <asp:Button ID="btnLabelPrint1" runat="server" CssClass="button gray small" Width="100px" Text="Print  Label" ToolTip="BarCode Label Print" Visible="false" OnClick="btnLabelPrint1_Click" />


                </td>
            </tr>
            <tr id="trLabelPrintType" runat="server" visible="false" style="text-align: right;">
                <td colspan="3" class="lblCaption1">Label Print Type:&nbsp;
                                
                                 <input type="checkbox" id="chkEDTA" runat="server" name="chkEDTA" value="EDTA" /><label for="EDTA" class="lblCaption1"> EDTA </label>
                    &nbsp;
                                <input type="checkbox" id="chkSERUM" runat="server" name="chkSERUM" value="SERUM" /><label for="SERUM" class="lblCaption1"> SERUM </label>
                    &nbsp; 
                                <input type="checkbox" id="chkFLUORIDE" runat="server" name="chkFLUORIDE" value="FLUORIDE" /><label for="FLUORIDE" class="lblCaption1"> FLUORIDE </label>
                    &nbsp Print Count :&nbsp
                                 <input type="text" id="txtPrintCount" runat="server" style="width: 30px; text-align: center;" value="1" class="TextBoxStyle" />
                </td>
            </tr>
        </table>

        <span class="lblCaption1" style="font-weight: bold;">Investigation</span>
        <div style="padding-top: 0px; width: 85%; overflow: auto; height: 300px; border-width: 1px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvTestSampleDtls" runat="server" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%" OnRowDataBound="gvTestSampleDtls_RowDataBound">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderStyle-Width="30px">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTestSampleDtlsSelected" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSD_SELECTED") %>' Visible="false"></asp:Label>

                                    <asp:CheckBox ID="chkSelService" runat="server" CssClass="label" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Test Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTestSampleDtlsSampleID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSD_TEST_SAMPLE_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvTestSampleDtlsProfilID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSD_TEST_PROFILE_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Lab Test Name" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTestSampleDtlsDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTSD_TEST_DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTestSampleDtlsStatus" CssClass="lblCaption1" Font-Size="11px" runat="server" Text='<%# Bind("LTSD_STATUS") %>' Visible="false"></asp:Label>

                                    <asp:DropDownList ID="drpgvTestSampleDtlsStatus" runat="server" CssClass="TextBoxStyle" Width="70px" BorderWidth="1px">
                                        <asp:ListItem Text="---Select---" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                                        <asp:ListItem Text="Hold" Value="Hold"></asp:ListItem>
                                        <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>

                                    <asp:Label ID="lblTestTypeCode" CssClass="GridRow" runat="server" Text='<%# Bind("LTSD_TEST_TYPE_CODE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblTestTypeDesc" CssClass="GridRow" runat="server" Text='<%# Bind("LTSD_TEST_TYPE_DESC") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </div>
</asp:Content>
