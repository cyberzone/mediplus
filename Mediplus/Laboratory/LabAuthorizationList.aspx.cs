﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.Laboratory
{
    public partial class LabAuthorizationList : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindAuthorizationList()
        {
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "AT_TRANSTYPE='LABTESTREPORT'";

            if (drpStatus.SelectedIndex != 0)
            {
                Criteria += " AND AT_AUTHORISATION_REQUIRED='" + drpStatus.SelectedValue + "'";
            }
            DS = objCom.AuthorizationGet(Criteria);

            //DS = objCom.fnGetFieldValue("*" +
            //     ",CONVERT(VARCHAR, AT_CREATED_DATE,103) +'  '+ CONVERT(VARCHAR(5), AT_CREATED_DATE,108) AS AT_CREATED_DATEDesc" +
            //     ",CONVERT(VARCHAR, AT_MODIFIED_DATE,103) +'  '+ CONVERT(VARCHAR(5), AT_MODIFIED_DATE,108) AS AT_MODIFIED_DATEDesc" +
            //     ",CONVERT(VARCHAR, AT_AUTHORISED_DATE,103) +'  '+ CONVERT(VARCHAR(5), AT_AUTHORISED_DATE,108)  AS AT_AUTHORISED_DATEDesc" +
            //     ",CONVERT(VARCHAR, AT_PRINTED_ON,103)+'  '+ CONVERT(VARCHAR(5), AT_PRINTED_ON,108) AS AT_PRINTED_ONDesc" +
            //     ",CONVERT(VARCHAR, AT_REPRINTED_ON,103) +'  '+CONVERT(VARCHAR(5), AT_REPRINTED_ON,108 ) AS AT_REPRINTED_ONDesc" +
            //     ",CASE AT_AUTHORISATION_REQUIRED WHEN 'N' THEN 'AUTHORIED' ELSE 'Not AUTHORIED' END  AS AT_AUTHORISATION_REQUIREDDesc "
            //     , "HMS_AUTHORISATION",
            //         Criteria, " AT_CREATED_DATE Desc");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAuthorized.DataSource = DS;
                gvAuthorized.DataBind();
            }
            else
            {
                gvAuthorized.DataBind();
            }

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LABAUTH' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnAuthorize.Visible = false;
                //btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnAuthorize.Enabled = false;
                // btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                BindAuthorizationList();
            }


        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            BindAuthorizationList();
        }

        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvAuthorized.Rows)
            {
                CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
                Label lblReportID = (Label)row.FindControl("lblReportID");
                if (chkSelect.Checked == true)
                {

                    CommonBAL objCom = new CommonBAL();
                    string FieldNameWithValues = " LTRM_AUTHORIZE_STATUS='AUTHORIZED' , LTRM_AUTHORIZE_USER_ID='" + Convert.ToString(Session["User_ID"]) + "' , LTRM_AUTHORIZE_DATE=GETDATE() ";
                    string Criteria = " LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_TEST_REPORT_ID='" + lblReportID.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "LAB_TEST_REPORT_MASTER", Criteria);
                  
                    string Criteria1 = "AT_TRANSTYPE='LABTESTREPORT' AND AT_TRANSID='" + lblReportID.Text.Trim() + "'";
                    string FieldNameWithValues1 = "AT_AUTHORISED_USER_ID='" + Convert.ToString(Session["User_ID"]) + "',AT_AUTHORISED_DATE = getdate(),AT_AUTHORISATION_REQUIRED='N'";
                    objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_AUTHORISATION", Criteria1);

                }

            }

            BindAuthorizationList();

        }
    }
}