﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace HMS.Laboratory
{
    public partial class LabTestInvestigationProfile : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindReportCategory()
        {

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 AND LTC_STATUS='A' ";
            ds = objLab.TestCategoryGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpReportCategory.DataSource = ds;
                drpReportCategory.DataTextField = "LTC_DESCRIPTION";
                drpReportCategory.DataValueField = "LTC_CODE";
                drpReportCategory.DataBind();
            }

            drpReportCategory.Items.Insert(0, "--- Select ---");
            drpReportCategory.Items[0].Value = "0";
        }

        void BindTestType()
        {

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 AND LTT_STATUS='A' ";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue("LTT_CODE,LTT_DESCRIPTION", "LAB_TEST_TYPE", Criteria, "LTT_CODE");
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpTestType.DataSource = ds;
                drpTestType.DataTextField = "LTT_DESCRIPTION";
                drpTestType.DataValueField = "LTT_CODE";
                drpTestType.DataBind();
            }

            drpTestType.Items.Insert(0, "--- Select ---");
            drpTestType.Items[0].Value = "";
        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "HCM_DESC");

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (strType == "LabDiagnosticService")
                {
                    drpDiagnosticService.DataSource = DS;
                    drpDiagnosticService.DataTextField = "HCM_DESC";
                    drpDiagnosticService.DataValueField = "HCM_CODE";
                    drpDiagnosticService.DataBind();
                }
                

            }


            if (strType == "LabDiagnosticService")
            {
                drpDiagnosticService.Items.Insert(0, "--- Select ---");
                drpDiagnosticService.Items[0].Value = "";
            }

             

        }

        void BindInvProfileMaster()
        {



            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LIPM_INVESTIGATION_PROFILE_ID='" + txtInvProfileID.Text.Trim() + "'";


            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;
                txtDescription.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_DESCRIPTION"]);
                drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_STATUS"]);

                txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_SERV_ID"]);
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_SERV_DESCRIPTION"]);
                txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_FEE"]);

                txtCaption.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_CAPTION"]);

                if (DS.Tables[0].Rows[0].IsNull("LIPM_TEST_CATEGORY") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_CATEGORY"]) != "")
                {
                    drpReportCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_CATEGORY"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("LIPM_REPORT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_REPORT_TYPE"]) != "")
                {
                    drpReportType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_REPORT_TYPE"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("LIPM_REMARK") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_REMARK"]) != "")
                {
                    txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_REMARK"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("LIPM_USAGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_USAGE"]) != "")
                {
                    drpUsage.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_USAGE"]);
                }

                txtMethod.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_METHOD"]);

                if (drpReportType.SelectedValue == "N")
                {
                    btnShowUserDefined.Style.Add("display", "None");
                }
                else
                {
                    btnShowUserDefined.Style.Add("display", "block");
                }

                if (DS.Tables[0].Rows[0].IsNull("LIPM_COLOR") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_COLOR"]) != "")
                {
                    drpColor.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_COLOR"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("LIPM_ADDITIONAL_REFERENCE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_ADDITIONAL_REFERENCE"]) != "")
                {
                    txtAddReference.Text = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_ADDITIONAL_REFERENCE"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("LIPM_TEST_TYPE_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]) != "")
                {
                    drpTestType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("LIPM_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LIPM_DIAGNOSTIC_SERVICE"]) != "")
                {
                    drpDiagnosticService.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_DIAGNOSTIC_SERVICE"]);
                }

               
            }


        }


        void BuildeInvProfileTrans()
        {
            string Criteria = "1=2";
            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.InvProfileTransGet(Criteria);

            ViewState["InvProfileTrans"] = DS.Tables[0];
        }

        void BindInvProfileTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LIPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LIPT_INVESTIGATION_PROFILE_ID='" + txtInvProfileID.Text.Trim() + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.InvProfileTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["InvProfileTrans"] = DS.Tables[0];

            }
        }

        void BindTempInvProfileTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvProfileTrans"];
            if (DT.Rows.Count > 0)
            {
                gvInvProfileTrans.DataSource = DT;
                gvInvProfileTrans.DataBind();

            }
            else
            {
                gvInvProfileTrans.DataBind();
            }

        }


        string GetUnitValue(string TestProfileID)
        {

            string strUnitValue = "";
            string Criteria = " 1=1 and LTPM_STATUS='A' ";
            Criteria += " AND   LTPM_TEST_PROFILE_ID='" + TestProfileID + "'";

            DataSet DS = new DataSet();
            DS = objLab.TestProfileMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                strUnitValue = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_UNIT"]);
            }
            return strUnitValue;
        }

        void GetServiceDtls()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='L' ";
            Criteria += " AND HSM_SERV_ID LIKE '" + txtServCode.Text.Trim() + "%'";


            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_NAME LIKE '" + txtServName.Text.Trim() + "%'";
            //}
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_FEE"]);
            }

        }

        void GridAllRowRefresh(Int32 intCurRow)
        {
            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["InvProfileTrans"];

            //for (int intCurRow = 0; intCurRow < gvInvProfileTrans.Rows.Count; intCurRow++)
            //{

            TextBox txtgvInvProfileTransMachineCode = (TextBox)gvInvProfileTrans.Rows[intCurRow].FindControl("txtgvInvProfileTransMachineCode");
            TextBox txtgvInvProfileTransUnit = (TextBox)gvInvProfileTrans.Rows[intCurRow].FindControl("txtgvInvProfileTransUnit");
            DropDownList drpgvInvProfileTransExam = (DropDownList)gvInvProfileTrans.Rows[intCurRow].FindControl("drpgvInvProfileTransExam");


            DT.Rows[intCurRow]["LIPT_MACHINE_CODE"] = txtgvInvProfileTransMachineCode.Text.Trim();
            DT.Rows[intCurRow]["LIPT_UNIT"] = txtgvInvProfileTransUnit.Text.Trim();
            DT.Rows[intCurRow]["LIPT_EXAM_TYPE"] = drpgvInvProfileTransExam.SelectedValue;



            DT.AcceptChanges();

            // }

            ViewState["InvProfileTrans"] = DT;
            BindTempInvProfileTrans();
        }


        void Clear()
        {
            ViewState["NewFlag"] = true;

            txtDescription.Text = "";
            drpStatus.SelectedIndex = 0;

            txtRemarks.Text = "";
            drpReportType.SelectedIndex = 0;
            
            drpColor.SelectedIndex = 0;

            drpUsage.SelectedIndex = 0;

            txtServCode.Text = "";
            txtServName.Text = "";
            txtFee.Text = "";
            txtCaption.Text = "";

            txtMethod.Text = "";
            txtAddReference.Text = "";

            if (drpReportCategory.Items.Count > 0)
            {
                drpReportCategory.SelectedIndex = 0;
            }

            if (drpTestType.Items.Count > 0)
            {
                drpTestType.SelectedIndex = 0;
            }

            if (drpDiagnosticService.Items.Count > 0)
            {
                drpDiagnosticService.SelectedIndex = 0;
            }


            btnShowUserDefined.Style.Add("display", "None");

            ViewState["gvServSelectIndex"] = "";

            BuildeInvProfileTrans();
            BindTempInvProfileTrans();

        }

        Boolean fnSave()
        {
            Boolean isError = false;
            if (txtInvProfileID.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Code";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtDescription.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Description";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtServCode.Text.Trim() == "")
            {
                lblStatus.Text = "Enter Service Code";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            if (txtServName.Text.Trim() == "")
            {
                lblStatus.Text = "Enter Service Name";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }


            objLab = new LaboratoryBAL();
            objLab.LIPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);


            objLab.LIPM_INVESTIGATION_PROFILE_ID = txtInvProfileID.Text.Trim();
            objLab.LIPM_DESCRIPTION = txtDescription.Text.Trim();
            objLab.LIPM_STATUS = drpStatus.SelectedValue;

            objLab.LIPM_REMARK = txtRemarks.Text.Trim();
            objLab.LIPM_SERV_ID = txtServCode.Text.Trim();
            objLab.LIPM_SERV_DESCRIPTION = txtServName.Text.Trim();
            objLab.LIPM_FEE = txtFee.Text.Trim();
            objLab.LIPM_CAPTION = txtCaption.Text.Trim();
            objLab.LIPM_FAVOURITE = "";
            objLab.LIPM_REPORT_TYPE = drpReportType.SelectedValue;
            objLab.LIPM_USAGE = drpUsage.SelectedValue;
            objLab.LIPM_TEST_CATEGORY = drpReportCategory.SelectedValue;
            objLab.LIPM_METHOD = txtMethod.Text.Trim();
            objLab.LIPM_COLOR = drpColor.SelectedValue;
            objLab.LIPM_ADDITIONAL_REFERENCE = txtAddReference.Text.Trim();
            objLab.TEST_TYPE_CODE = drpTestType.SelectedValue;
            objLab.TEST_TYPE_DESC = drpTestType.SelectedItem.Text;

            objLab.LIPM_DIAGNOSTIC_SERVICE = drpDiagnosticService.SelectedValue;


            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }



            objLab.UserID = Convert.ToString(Session["User_ID"]);
            objLab.InvProfileMasterAdd();


            objCom = new CommonBAL();
            string Criteria = " LIPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LIPT_INVESTIGATION_PROFILE_ID='" + txtInvProfileID.Text.Trim() + "'";
            objCom.fnDeleteTableData("LAB_INVESTIGATION_PROFILE_TRANS", Criteria);

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["InvProfileTrans"];

            Int32 intCurRow = 1;
            /*
            foreach (DataRow DR in DT.Rows)
            {

                IDictionary<string, string> Param = new Dictionary<string, string>();

                objLab.LIPT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objLab.LIPT_INVESTIGATION_PROFILE_ID = txtInvProfileID.Text.Trim();
                objLab.LIPT_TEST_PROFILE_ID = Convert.ToString(DR["LIPT_TEST_PROFILE_ID"]);
                objLab.LIPT_TEST_DETAILS = Convert.ToString(DR["LIPT_TEST_DETAILS"]);

                objLab.LIPT_MACHINE_CODE = Convert.ToString(DR["LIPT_MACHINE_CODE"]);
                objLab.LIPT_UNIT = Convert.ToString(DR["LIPT_UNIT"]);
                objLab.LIPT_EXAM_TYPE = Convert.ToString(DR["LIPT_EXAM_TYPE"]);

                objLab.LIPT_SLNO = Convert.ToString(intCurRow);
                objLab.InvProfileTransAdd();

                intCurRow = intCurRow + 1;
            }
             * 
             * */

            foreach (GridViewRow DR in gvInvProfileTrans.Rows)
            {
                Label LBLgvInvProfileTransCode = (Label)DR.FindControl("LBLgvInvProfileTransCode");
                Label lblgvInvProfileTranssDesc = (Label)DR.FindControl("lblgvInvProfileTranssDesc");



                TextBox txtgvInvProfileTransMachineCode = (TextBox)DR.FindControl("txtgvInvProfileTransMachineCode");
                TextBox txtgvInvProfileTransUnit = (TextBox)DR.FindControl("txtgvInvProfileTransUnit");

                DropDownList drpgvInvProfileTransExam = (DropDownList)DR.FindControl("drpgvInvProfileTransExam");

                IDictionary<string, string> Param = new Dictionary<string, string>();

                objLab.LIPT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objLab.LIPT_INVESTIGATION_PROFILE_ID = txtInvProfileID.Text.Trim();

                objLab.LIPT_TEST_PROFILE_ID = LBLgvInvProfileTransCode.Text;
                objLab.LIPT_TEST_DETAILS = lblgvInvProfileTranssDesc.Text;

                objLab.LIPT_MACHINE_CODE = txtgvInvProfileTransMachineCode.Text;
                objLab.LIPT_UNIT = txtgvInvProfileTransUnit.Text;
                objLab.LIPT_EXAM_TYPE = drpgvInvProfileTransExam.SelectedValue;

                objLab.LIPT_SLNO = Convert.ToString(intCurRow);
                objLab.InvProfileTransAdd();

                intCurRow = intCurRow + 1;
            }

        FunEnd: ;
            return isError;
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "LAB_INVPRO";
            objCom.ScreenName = "Lab Investigation Profile";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_INVPRO' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion


        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetMethod(string prefixText)
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string[] Data;

            string Criteria = " 1=1 ";

            Criteria += " AND ( LIPM_METHOD LIKE '%" + prefixText + "%')";



            ds = obLab.InvProfileMasterGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["LIPM_METHOD"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetTestProfMaster(string prefixText)
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string[] Data;

            string Criteria = " 1=1 and LTPM_STATUS='A' ";


            Criteria += " AND ( LTPM_TEST_PROFILE_ID LIKE '%" + prefixText + "%' OR LTPM_DESCRIPTION LIKE'%" + prefixText + "%' )";




            ds = obLab.TestProfileMasterGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["LTPM_TEST_PROFILE_ID"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["LTPM_DESCRIPTION"]).Trim();



                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Lab Investigation Profile Page");

                try
                {
                    ViewState["NewFlag"] = true;
                    BindReportCategory();
                    BindTestType();
                    BindCommonMaster("LabDiagnosticService");

                    BuildeInvProfileTrans();

                    string InvProfileID = Convert.ToString(Request.QueryString["InvProfileID"]);

                    if (InvProfileID != " " && InvProfileID != null)
                    {

                        txtInvProfileID.Text = Convert.ToString(InvProfileID);
                        txtInvProfileID_TextChanged(txtInvProfileID, new EventArgs());

                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtInvProfileID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();

                BindInvProfileMaster();
                BindInvProfileTrans();
                BindTempInvProfileTrans();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtProfileID_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // GridAllRowRefresh();

                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {
                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Investigation Profile, Investigation Profile ID is " + txtInvProfileID.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Lab Investigation Profile, Investigation Profile ID is " + txtInvProfileID.Text);
                    }

                    Clear();
                    txtInvProfileID.Text = "";
                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void TestProfTransEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label LBLgvInvProfileTransCode = (Label)gvScanCard.Cells[0].FindControl("LBLgvInvProfileTransCode");
            Label lblgvInvProfileTranssDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvInvProfileTranssDesc");

            txtTestName.Text = LBLgvInvProfileTransCode.Text + "~" + lblgvInvProfileTranssDesc.Text;
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["InvProfileTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["InvProfileTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["InvProfileTrans"] = DT;

                }


                BindTempInvProfileTrans();



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTestName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["InvProfileTrans"];

                strTestCodeDesc = txtTestName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["LIPT_TEST_PROFILE_ID"] = TestProfileID;
                    DT.Rows[R]["LIPT_TEST_DETAILS"] = TestProfileDesc;


                    DT.AcceptChanges();
                }
                else
                {

                    objrow["LIPT_TEST_PROFILE_ID"] = TestProfileID;
                    objrow["LIPT_TEST_DETAILS"] = TestProfileDesc;
                    objrow["LIPT_UNIT"] = GetUnitValue(TestProfileID);




                    DT.Rows.Add(objrow);
                }



                ViewState["InvProfileTrans"] = DT;


                BindTempInvProfileTrans();
                txtTestName.Text = "";

                ViewState["gvServSelectIndex"] = "";
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpReportType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (drpReportType.SelectedValue == "N")
            {
                btnShowUserDefined.Style.Add("display", "None");
                // btnShowUserDefined.Attributes.Add("Style", "color:#000000");
            }
            else
            {
                btnShowUserDefined.Style.Add("display", "block");
            }

        }



        protected void gvInvProfileTrans_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;


                Int32 R = currentRow.RowIndex;

                GridAllRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      LabTestInvestigationProfile.drpgvDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            GetServiceDtls();
        }

        protected void gvInvProfileTrans_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblgvInvProfileTransExam = (Label)e.Row.Cells[0].FindControl("lblgvInvProfileTransExam");


                    DropDownList drpgvInvProfileTransExam = (DropDownList)e.Row.Cells[0].FindControl("drpgvInvProfileTransExam");


                    //drpgvTestReportDtlsRange.Items.Insert(0, "--- Select ---");
                    //drpgvTestReportDtlsRange.Items[0].Value = "0";

                    if (lblgvInvProfileTransExam.Text != "")
                    {
                        drpgvInvProfileTransExam.SelectedValue = lblgvInvProfileTransExam.Text;
                    }


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvTestReportDtls_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            txtInvProfileID.Text = "";

        }
    }
}