﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="LabTestDtlsSearch.aspx.cs" Inherits="Mediplus.Laboratory.LabTestDtlsSearch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

     <script language="javascript" type="text/javascript">

         function LabResultReport(ReportName, TransNo) {


             var Criteria = " 1=1 ";

             if (TransNo != "") {
                 Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';
                 var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'RadReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                 win.focus();
             }

         }
      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <table width="90%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Test Dtls. Search"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;"></td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>
    <table width="80%" border="0" cellpadding="3" cellspacing="3">

        <tr>

            <td class="lblCaption1" style="height: 25px;">From Date
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTransFromDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransFromDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                        :
                         <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1" style="height: 25px;">To Date
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTransToDate" runat="server" Width="70px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtTransToDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpEndHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                        :
                       <asp:DropDownList ID="drpEndMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="37px"></asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
          
        </tr>
        <tr>

            <td class="lblCaption1" style="height: 25px;">File No
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFileNo" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 25px;">Name
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtFName" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td class="lblCaption1" style="height: 25px;">Mobile No
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtMobileNo" runat="server" Width="160px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>


            <td>
                <asp:Button ID="btnShow" runat="server" CssClass="button red small" Text="Show" OnClick="btnShow_Click" />
            </td>
        </tr>

    </table>
    <span class="lblCaption1" style="font-weight:bold;" >TEST RESULT</span>
    <div style="padding-top: 0px; width: 80%; height: 275px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvLabTestReport" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" Visible="false">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />

                    <Columns>
                        <asp:TemplateField HeaderText="Report ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>


                                <asp:Label ID="lblgvLabTestReportID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_TEST_REPORT_ID") %>'></asp:Label>


                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sample ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                            <ItemTemplate>

                                <asp:Label ID="lblgvLabTestDRCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_DR_CODE") %>' Visible="false"></asp:Label>

                                <asp:Label ID="lblgvLabTestSampleID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_SAMPLE_ID") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblgvLabTestDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_TEST_DATEDesc") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblgvLabTestPTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_ID") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblgvLabTestPTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_PATIENT_NAME") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Doctor" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblgvLabTestDRName" CssClass="GridRow" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_DR_NAME") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BarCode" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>

                                <asp:Label ID="lblgvLabTestBareCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTRM_BARECODE") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgPrint" runat="server" OnClick="gvLabTestReportPrint_Click" ImageUrl="~/EMR/WebReports/Images/printer.png" Style="height: 25px; width: 30px; border: none;" />
                            </ItemTemplate>


                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    
    
    <table style="width:100%">
        <tr>
           
            <td   class="lblCaption1">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                         <span class="lblCaption1" style="font-weight:bold;">TEST SERVICE DTLS</span>
                     &nbsp;&nbsp;&nbsp;   Status
                        <asp:DropDownList ID="drpWaitListStatus" runat="server" Style="width: 160px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpWaitListStatus_SelectedIndexChanged">
                            <asp:ListItem Value="">--- All ---</asp:ListItem>
                            <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                            <asp:ListItem Value="LAB_REGISTERED">Test Registred</asp:ListItem>
                            <asp:ListItem Value="LAB_SAMPLE_COLLECTED">Sample Collected</asp:ListItem>
                            <asp:ListItem Value="LAB_TEST_COMPLETED">Test Completed</asp:ListItem>

                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; height: 300px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvWaitingList" runat="server" Width="100%"
                    AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>

                        <asp:TemplateField HeaderText="ID" SortExpression="ID">
                            <ItemTemplate>

                                <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("DoctorID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ID") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DATE" SortExpression="TransDate">
                            <ItemTemplate>

                                <asp:Label ID="lblTransDate" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("TransDate") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FILE NO" SortExpression="FileNo">
                            <ItemTemplate>

                                <asp:Label ID="lblPatientId" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("FileNo") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NAME" SortExpression="PatientName">
                            <ItemTemplate>

                                <asp:Label ID="Label2" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("PatientName") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MOBILE NO" SortExpression="MobileNo" Visible="false">
                            <ItemTemplate>

                                <asp:Label ID="lblMobileNo" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("MobileNo") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SERVICE CODE " SortExpression="ServiceID">
                            <ItemTemplate>

                                <asp:Label ID="lblServiceID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("ServiceID") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SERVICE DESC. " SortExpression="ServiceDesc" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>

                                <asp:Label ID="lblServiceDesc" CssClass="GridRow" runat="server" Text='<%# Bind("ServiceDesc") %>'></asp:Label>

                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="DOCTOR" SortExpression="Doctor">
                            <ItemTemplate>

                                <asp:Label ID="Label3" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("Doctor") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TRANS TYPE" SortExpression="TransType" Visible="false">
                            <ItemTemplate>

                                <asp:Label ID="lblTransType" CssClass="GridRow" Width="50px" runat="server" Text='<%# Bind("TransType") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PATIENT TYPE" SortExpression="PatientType">
                            <ItemTemplate>

                                <asp:Label ID="Label5" CssClass="GridRow" Width="50px" runat="server" Text='<%# Bind("PatientType") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="STATUS" SortExpression="ServiceRequested">
                            <ItemTemplate>

                                <asp:Label ID="lblStatus" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("Status") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

</asp:Content>
