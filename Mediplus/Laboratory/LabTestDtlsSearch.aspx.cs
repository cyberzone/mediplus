﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.Laboratory
{
    public partial class LabTestDtlsSearch : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindWaitngList()
        {

            string strTestType = "";
            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

            DataSet DS = new DataSet();

            objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }





            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



            if (txtTransFromDate.Text != "")
            {
                
                
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTRM_TEST_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTRM_TEST_DATE,101),101) <= '" + strForToDate + "'";
                
            }
            if (txtFileNo.Text.Trim() != "")
            {
                 
                    Criteria += " AND  LTRM_PATIENT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                
            }

            if (txtFName.Text.Trim() != "")
            {
                 
                    Criteria += " AND  LTRM_PATIENT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                
            }
            if (txtMobileNo.Text.Trim() != "")
            {
               // Criteria += " AND  HPM_MOBILE LIKE '%" + txtMobileNo.Text.Trim() + "%'";

                Criteria += " AND  LTRM_PATIENT_ID IN (SELECT  HPM_PT_ID FROM HMS_PATIENT_MASTER WHERE HPM_MOBILE LIKE '%" + txtMobileNo.Text.Trim() + "%' )";
            }

            gvLabTestReport.Visible = false;


            DS = objLab.TestReportMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLabTestReport.Visible = true;
                gvLabTestReport.DataSource = DS;
                gvLabTestReport.DataBind();
            }
            else
            {
                gvLabTestReport.DataBind();
            }


        }

        void BindWaitngListServiceWise()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);


            DataSet DS = new DataSet();

            objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (strTestType == "EMR")
            {
                Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                //if (drpWaitListStatus.SelectedValue == "Waiting")
                //{
                //    Criteria += " AND Completed IS NULL ";
                //}
                //if (drpWaitListStatus.SelectedValue == "Completed")
                //{
                //    Criteria += " AND Completed IS NOT NULL ";
                //}

                if (drpWaitListStatus.SelectedValue != "")
                {
                    if (drpWaitListStatus.SelectedValue == "Waiting")
                    {
                        Criteria += " AND Completed IS NULL ";
                    }
                    else
                    {
                        Criteria += " AND Completed='" + drpWaitListStatus.SelectedValue + "'";

                    }

                }


                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00 '";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + " " + drpEndHour.SelectedValue + ":" + drpEndMin.SelectedValue + ":00 '";
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }

                if (txtMobileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HPM_MOBILE LIKE '%" + txtMobileNo.Text.Trim() + "%'";
                }


            }
            else
            {
                //Criteria = " 1=1  and  AND HIT_SERV_TYPE='L'   ";
                //if (drpWaitListStatus.SelectedValue == "Waiting")
                //{

                //    Criteria += " AND HIT_LAB_STATUS IS NULL ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}
                //if (drpWaitListStatus.SelectedValue == "Completed")
                //{

                //    Criteria += " AND HIT_LAB_STATUS IS NOT NULL ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}

                if (drpWaitListStatus.SelectedValue != "")
                {

                    if (drpWaitListStatus.SelectedValue == "Waiting")
                    {
                        Criteria += " AND HIT_LAB_STATUS IS NULL ";

                    }
                    else
                    {
                        Criteria += " AND HIT_LAB_STATUS='" + drpWaitListStatus.SelectedValue + "'";

                    }

                }



                //else if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" && drpWaitListStatus.SelectedValue == "Waiting")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}
                //else if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" && drpWaitListStatus.SelectedValue == "Completed")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_TEST_COMPLETED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}

                Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00 '";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + " " + drpEndHour.SelectedValue + ":" + drpEndMin.SelectedValue + ":00 '";
                }
                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }

                if (txtMobileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HPM_MOBILE LIKE '%" + txtMobileNo.Text.Trim() + "%'";
                }
            }

            string strServiceType = "";

            strServiceType = "L";

            // if (Convert.ToString(ViewState["PageName"]) == "LabRegWaitList")
            // {
            DS = objLab.LabTestDtlsList(Criteria);
            //}
            //else
            //{
            //    DS = objLab.LabWaitingListShow(drpWaitListStatus.SelectedValue, GlobelValues.FileDescription, strServiceType, Criteria);

            //}
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvWaitingList.DataSource = DS;
                gvWaitingList.DataBind();
            }
            else
            {
                gvWaitingList.DataBind();
            }

        }

        string GetEMRID()
        {
            string strEMR_ID = "0";
            DataSet DS = new DataSet();
            String Criteria = " 1=1  ";

            string strStartDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";

            Criteria += " AND  EPM_PT_ID = '" + Convert.ToString(ViewState["PatientID"]) + "'";
            Criteria += " AND  EPM_DR_CODE = '" + Convert.ToString(ViewState["DoctorID"]) + "'";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strEMR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);
            }


            return strEMR_ID;
        }

        void BindTimeFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");


            drpSTHour.DataSource = DSHours;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();

            drpEndHour.DataSource = DSHours;
            drpEndHour.DataTextField = "Name";
            drpEndHour.DataValueField = "Code";
            drpEndHour.DataBind();


            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpSTMin.DataSource = DSMin;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();

            drpEndMin.DataSource = DSMin;
            drpEndMin.DataTextField = "Name";
            drpEndMin.DataValueField = "Code";
            drpEndMin.DataBind();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_RESULT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                ///  btnLabTestRegister.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                /// btnLabTestRegister.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                ViewState["PageName"] = (string)Request.QueryString["PageName"];

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }


                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTransToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTimeFromDB();




                    BindWaitngList();

                    BindWaitngListServiceWise();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                BindWaitngList();
                BindWaitngListServiceWise();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvLabTestReportPrint_Click(object sender, EventArgs e)
        {
            try
            {
                

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;



                Label lblgvLabTestReportID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabTestReportID");
                Label lblgvEMR_PTMaster_DrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvEMR_PTMaster_DrCode");
 
 
                    string ReportName = "LabReport.rpt";


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RadResult", "LabResultReport('" + ReportName + "','" + lblgvLabTestReportID.Text.Trim() + "');", true);


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.gvEMR_PTMasterPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void drpWaitListStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindWaitngListServiceWise();
        }

    }
}