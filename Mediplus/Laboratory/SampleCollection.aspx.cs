﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Data.SqlClient;

using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;

using System.Drawing;
using System.Drawing.Design;

using System.Drawing.Printing;

namespace Mediplus.Laboratory
{
    public partial class SampleCollection : System.Web.UI.Page
    {
        string strDataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        string strDBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        string strDBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        string strDBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();
        string strReportPath = System.Configuration.ConfigurationSettings.AppSettings["REPORT_PATH"].ToString().Trim();

        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindReportType()
        {

            DataSet ds = new DataSet();
            objLab = new LaboratoryBAL();

            string Criteria = " 1=1 AND LIPM_STATUS='A' ";
            ds = objLab.InvProfileMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpReportType.DataSource = ds;
                drpReportType.DataTextField = "LIPM_DESCRIPTION";
                drpReportType.DataValueField = "LIPM_INVESTIGATION_PROFILE_ID";
                drpReportType.DataBind();
            }

            drpReportType.Items.Insert(0, "--- Select ---");
            drpReportType.Items[0].Value = "";
        }

        void BindNationality()
        {
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "Select");
            drpNationality.Items[0].Value = "0";



        }

        void BindDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";// AND HSFM_DEPT_ID='RADIOLOGY' 

            if (GlobalValues.FileDescription == "SMCH")
            {
                Criteria += "  AND HSFM_DEPT_ID='PATHOLOGY' ";
            }

            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDoctors.DataSource = ds;
                drpDoctors.DataTextField = "FullName";
                drpDoctors.DataValueField = "HSFM_STAFF_ID";
                drpDoctors.DataBind();


                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;



            }

            drpDoctors.Items.Insert(0, "--- Select ---");
            drpDoctors.Items[0].Value = "";


        }


        void BinRefdDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpRefDoctor.DataSource = ds;
                drpRefDoctor.DataTextField = "FullName";
                drpRefDoctor.DataValueField = "HSFM_STAFF_ID";
                drpRefDoctor.DataBind();

            }



            drpRefDoctor.Items.Insert(0, "--- Select ---");
            drpRefDoctor.Items[0].Value = "";
        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                //  lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                drpAgeType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                drpAgeType1.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);


                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                //txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                //txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                //txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                //txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();

                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]).ToUpper())
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNationality;
                        }
                    }

                }

            ForNationality: ;

                //txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                //txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                //txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                // if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                //   lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                // txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                txtProviderID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "")
                {
                    for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                    {
                        if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]).ToUpper())
                        {
                            drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                            goto ForRefDoctors;
                        }
                    }

                }

            ForRefDoctors: ;
                // GetPatientVisit();

                drpInvType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";


            }

        }

        void BindLabSampleReportMaster()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND LTSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSM_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            DS = objLab.TestSampleMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["NewFlag"] = false;
                    txtTestSampleID.Text = Convert.ToString(DR["LTSM_TEST_SAMPLE_ID"]);

                    hidActualSampleID.Value = txtTestSampleID.Text;

                    if (DR.IsNull("LTSM_DATEDesc") == false && Convert.ToString(DR["LTSM_DATEDesc"]) != "")
                    {
                        txtTransDate.Text = Convert.ToString(DR["LTSM_DATEDesc"]);
                    }


                    if (DR.IsNull("LTSM_DATETimeDesc") == false && Convert.ToString(DR["LTSM_DATETimeDesc"]) != "")
                    {

                        string strTimeUp = Convert.ToString(DR["LTSM_DATETimeDesc"]);


                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpSTHour.SelectedValue = strHour;
                            drpSTMin.SelectedValue = arrTimeUp1[1];
                        }

                    }


                    if (DR.IsNull("LTSM_DELIVERY_DATEDesc") == false && Convert.ToString(DR["LTSM_DELIVERY_DATEDesc"]) != "")
                    {

                        txtDeliveryDate.Text = Convert.ToString(DR["LTSM_DELIVERY_DATEDesc"]);
                    }

                    if (DR.IsNull("LTSM_DELIVERY_DATETimeDesc") == false && Convert.ToString(DR["LTSM_DELIVERY_DATETimeDesc"]) != "")
                    {

                        string strTimeUp = Convert.ToString(DR["LTSM_DELIVERY_DATETimeDesc"]);

                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpDelivHour.SelectedValue = strHour;
                            drpDelivMin.SelectedValue = arrTimeUp1[1];
                        }

                    }




                    if (DR.IsNull("LTSM_STATUS") == false && Convert.ToString(DR["LTSM_STATUS"]) != "")
                    {
                        drpStatus.SelectedValue = Convert.ToString(DR["LTSM_STATUS"]);
                    }
                    txtFileNo.Text = Convert.ToString(DR["LTSM_PATIENT_ID"]);
                    txtFName.Text = Convert.ToString(DR["LTSM_PATIENT_NAME"]);
                    txtAge.Text = Convert.ToString(DR["LTSM_PATIENT_AGE"]);
                    if (DR.IsNull("LTSM_PATIENT_AGETYPE") == false && Convert.ToString(DR["LTSM_PATIENT_AGETYPE"]) != "")
                    {
                        drpAgeType.SelectedValue = Convert.ToString(DR["LTSM_PATIENT_AGETYPE"]);
                    }
                    txtSex.Text = Convert.ToString(DR["LTSM_PATIENT_SEX"]);



                    if (DR.IsNull("LTSM_PATIENT_NATIONALITY") == false && Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]) != "")
                    {
                        for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                        {
                            if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]).ToUpper())
                            {
                                drpNationality.SelectedValue = Convert.ToString(DR["LTSM_PATIENT_NATIONALITY"]);
                                goto ForNationality;
                            }
                        }

                    }
                ForNationality: ;

                    //= Convert.ToString(DR["LTRM_RANGE"]);
                    if (DR.IsNull("LTSM_REP_TYPE") == false && Convert.ToString(DR["LTSM_REP_TYPE"]) != "")
                    {
                        drpReportType.SelectedValue = Convert.ToString(DR["LTSM_REP_TYPE"]);
                    }
                    //drpReportType.= Convert.ToString(DR["LTRM_INVESTIGATION_DESCRIPTION"]);
                    if (DR.IsNull("LTSM_REF_DR") == false && Convert.ToString(DR["LTSM_REF_DR"]) != "")
                    {

                    }

                    if (DR.IsNull("LTSM_REF_DR") == false && Convert.ToString(DR["LTSM_REF_DR"]) != "")
                    {
                        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                        {
                            if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_REF_DR"]).ToUpper())
                            {
                                drpRefDoctor.SelectedValue = Convert.ToString(DR["LTSM_REF_DR"]);
                                goto FordrpRefDoctor;
                            }
                        }

                    }
                FordrpRefDoctor: ;


                    if (DR.IsNull("LTSM_DR_ID") == false && Convert.ToString(DR["LTSM_DR_ID"]) != "")
                    {
                        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                        {
                            if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(DR["LTSM_DR_ID"]).ToUpper())
                            {
                                drpDoctors.SelectedValue = Convert.ToString(DR["LTSM_DR_ID"]);
                                goto FordrpDoctors;
                            }
                        }

                    }
                FordrpDoctors: ;



                    //txtServCode.Text = Convert.ToString(DR["LTRM_SERV_ID"]);
                    //txtServName.Text = Convert.ToString(DR["LTRM_SERV_DESCRIPTION"]);
                    //txtFee.Text = Convert.ToString(DR["LTRM_FEE"]);
                    //txtCaption.Text = Convert.ToString(DR["LTRM_CAPTION"]);
                    txtRemarks.Text = Convert.ToString(DR["LTSM_REMARKS"]);
                    txtBarecodeID.Text = Convert.ToString(DR["LTSM_BARECODE"]);

                    txtInvoiceRefNo.Text = Convert.ToString(DR["LTSM_INVOICE_ID"]);
                    txtEMRID.Text = Convert.ToString(DR["LTSM_EMR_ID"]);

                    //  txtProviderID.Text = Convert.ToString(DR["LTRM_COMP_ID"]);
                    //  txtProviderName.Text = Convert.ToString(DR["LTRM_COMP_NAME"]);
                    //    txtPolicyNo.Text = Convert.ToString(DR["LTRM_POLICY_NO"]);



                    //if (DR.IsNull("LTRM_REPORT_SOURCE") == false && Convert.ToString(DR["LTRM_REPORT_SOURCE"]) != "")
                    //{
                    //    drpSource.SelectedValue = Convert.ToString(DR["LTRM_REPORT_SOURCE"]);
                    //}
                    //txtSourceName.Text = Convert.ToString(DR["LTRM_REPORT_SOURCENAME"]);


                    //txtAdminID.Text = Convert.ToString(DR["LTRM_ADMN"]);
                    //txtWard.Text = Convert.ToString(DR["LTRM_WARD"]);
                    //txtRoom.Text = Convert.ToString(DR["LTRM_ROOM"]);
                    //txtBed.Text = Convert.ToString(DR["LTRM_BED"]);

                    //drpInvType.SelectedValue = Convert.ToString(DR["LTRM_INV_TYPE"]);
                    //drpPTType.SelectedValue = Convert.ToString(DR["LTRM_PT_TYPE"]);

                    //txtSampleCollTime.Text = Convert.ToString(DR["LTRM_COLLECTION_TIME"]);
                    //txtInvoiceRefNo.Text = Convert.ToString(DR["LTRM_INVOICENO"]);



                }
            }

        }

        void BuildeTestSampleDtls()
        {
            string Criteria = "1=2";
            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestSampleDtlsGet(Criteria);

            ViewState["TestSampleDtls"] = DS.Tables[0];
        }

        void BindTestSampleDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.TestSampleDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["TestSampleDtls"] = DS.Tables[0];

            }
        }

        void BindTempTestSampleDtls()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["TestSampleDtls"];
            if (DT.Rows.Count > 0)
            {
                gvTestSampleDtls.DataSource = DT;
                gvTestSampleDtls.DataBind();

            }
            else
            {
                gvTestSampleDtls.DataBind();
            }

        }

        void GetInvProfileTestType(string ServiceID, out string TestTypeCode, out string TestTypeDesc)
        {
            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  LIPM_SERV_ID='" + ServiceID + "'";

            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_DESC"]);
            }

        }

        void GetProfileMasterTestType(string ServiceID, out string TestTypeCode, out string TestTypeDesc)
        {
            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LTPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  LTPM_SERV_ID='" + ServiceID + "'";

            DS = objLab.TestProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_DESC"]);
            }

        }

        void GetInvProfileTestTypeByProfileID(string ProfileID, out string TestTypeCode, out string TestTypeDesc)
        {

            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND LIPM_INVESTIGATION_PROFILE_ID='" + ProfileID + "'";


            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_DESC"]);

            }

        }

        void BindInvProfileTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND LIPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND LIPT_INVESTIGATION_PROFILE_ID='" + drpReportType.SelectedValue + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();

            DS = objLab.InvProfileTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestSampleDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTSD_TEST_PROFILE_ID"] = Convert.ToString(DR["LIPT_TEST_PROFILE_ID"]);
                    objrow["LTSD_TEST_DESCRIPTION"] = Convert.ToString(DR["LIPT_TEST_DETAILS"]);
                    //objrow["LTRD_TEST_UNIT"] = Convert.ToString(DR["LIPT_UNIT"]);

                    string TestTypeCode, TestTypeDesc;
                    GetInvProfileTestTypeByProfileID(drpReportType.SelectedValue, out TestTypeCode, out  TestTypeDesc);


                    objrow["LTSD_TEST_TYPE_CODE"] = TestTypeCode;
                    objrow["LTSD_TEST_TYPE_DESC"] = TestTypeDesc;

                    DT.Rows.Add(objrow);

                }

                ViewState["TestSampleDtls"] = DT;

            }
        }


        void BindServiceTrans()
        {
            string Criteria = " 1=1 AND  HIT_LAB_STATUS='LAB_REGISTERED'  ";

            Criteria += " AND HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceID"]) + "'";

            dboperations dbo = new dboperations();
            clsInvoice objInv = new clsInvoice();
            DataSet DS = new DataSet();
            DS = objInv.InvoiceTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestSampleDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTSD_TEST_PROFILE_ID"] = Convert.ToString(DR["HIT_SERV_CODE"]);
                    objrow["LTSD_TEST_DESCRIPTION"] = Convert.ToString(DR["HIT_DESCRIPTION"]);
                    //objrow["LTRD_TEST_UNIT"] = Convert.ToString(DR["LIPT_UNIT"]); HSM_TYPE


                    string TestTypeCode, TestTypeDesc;
                    GetInvProfileTestType(Convert.ToString(DR["HIT_SERV_CODE"]), out TestTypeCode, out  TestTypeDesc);


                    if (TestTypeCode == "")
                    {
                        GetProfileMasterTestType(Convert.ToString(DR["HIT_SERV_CODE"]), out TestTypeCode, out  TestTypeDesc);
                    }


                    objrow["LTSD_TEST_TYPE_CODE"] = TestTypeCode;
                    objrow["LTSD_TEST_TYPE_DESC"] = TestTypeDesc;


                    DT.Rows.Add(objrow);

                }

                ViewState["TestSampleDtls"] = DT;

            }
        }

        void BindRegisterTrans()
        {
            string Criteria = " 1=1   ";

            Criteria += " AND LTRD_REG_ID='" + Convert.ToString(ViewState["LabRegisterID"]) + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();
            DS = objLab.TestRegisterDetailGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestSampleDtls"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["LTSD_TEST_PROFILE_ID"] = Convert.ToString(DR["LTRD_SERV_CODE"]);
                    objrow["LTSD_TEST_DESCRIPTION"] = Convert.ToString(DR["LTRD_DESCRIPTION"]);

                    //objrow["LTSD_TEST_TYPE_CODE"] = Convert.ToString(DR["LTRD_TEST_TYPE_CODE"]);
                    //objrow["LTSD_TEST_TYPE_DESC"] = Convert.ToString(DR["LTRD_TEST_TYPE_DESC"]);

                    string TestTypeCode, TestTypeDesc;
                    GetInvProfileTestType(Convert.ToString(DR["LTRD_SERV_CODE"]), out TestTypeCode, out  TestTypeDesc);


                    if (TestTypeCode == "")
                    {
                        GetProfileMasterTestType(Convert.ToString(DR["LTRD_SERV_CODE"]), out TestTypeCode, out  TestTypeDesc);
                    }


                    objrow["LTSD_TEST_TYPE_CODE"] = TestTypeCode;
                    objrow["LTSD_TEST_TYPE_DESC"] = TestTypeDesc;



                    DT.Rows.Add(objrow);

                }

                ViewState["TestSampleDtls"] = DT;

            }
        }

        void BindLabTestRegtMaster()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_BARECODE='" + txtBarecodeID.Text.Trim() + "'";
            DS = objLab.TestRegisterMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["InvoiceID"] = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_INVOICE_ID"]);
                ViewState["EMRID"] = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_EMR_ID"]);

                txtInvoiceRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_INVOICE_ID"]);
                txtEMRID.Text = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_EMR_ID"]);



                if (DS.Tables[0].Rows[0].IsNull("LTRM_DR_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]) != "")
                {
                    for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                    {
                        if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]).ToUpper())
                        {
                            drpDoctors.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]);
                            goto ForDoctors;
                        }
                    }

                }

            ForDoctors: ;


                //if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "")
                if (DS.Tables[0].Rows[0].IsNull("LTRM_REF_DR") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]) != "")
                {
                    for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                    {
                        if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]).ToUpper())
                        {
                            drpRefDoctor.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]);
                            goto ForRefDoctors;
                        }
                    }

                }

            ForRefDoctors: ;

            }

        }

        void BindEMRDtls()
        {
            string Criteria = "EPM_ID='" + txtEMRID.Text.Trim() + "'";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DataSet DS = new DataSet();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtProviderID.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_INS_CODE"]);
                txtProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_INS_NAME"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_POLICY_NO"]);
            }
        }

        void PTDtlsClear()
        {

            txtFName.Text = "";
            lblStatus.Text = "";

            txtDOB.Text = "";
            //lblAge.Text = "";

            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            //txtAddress.Text = "";
            //txtPoBox.Text = "";

            //txtArea.Text = "";
            //txtCity.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";




            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";




            drpDoctors.SelectedIndex = 0;


        }

        Boolean fnSave()
        {
            Boolean isError = false;

            if (txtTransDate.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter  Date";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }


            if (txtDeliveryDate.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Delivery Date";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter File No";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }

            objLab = new LaboratoryBAL();
            objLab.LTSM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);

            objLab.LTSM_TEST_SAMPLE_ID = txtTestSampleID.Text;
            objLab.LTSM_TEST_SAMPLE_TYPE = "";
            objLab.LTSM_DATE = txtTransDate.Text + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
            objLab.LTSM_STATUS = drpStatus.SelectedValue;
            objLab.LTSM_PATIENT_ID = txtFileNo.Text.Trim();
            objLab.LTSM_PATIENT_NAME = txtFName.Text.Trim();
            objLab.LTSM_PATIENT_AGE = txtAge.Text.Trim();
            objLab.LTSM_PATIENT_AGETYPE = drpAgeType.SelectedValue;
            objLab.LTSM_PATIENT_SEX = txtSex.Text;
            objLab.LTSM_PATIENT_NATIONALITY = drpNationality.SelectedValue;
            objLab.LTSM_RANGE = "";
            objLab.LTSM_INVESTIGATION_PROFILE_ID = "";
            objLab.LTSM_INVESTIGATION_DESCRIPTION = "";
            objLab.LTSM_REF_DR = drpRefDoctor.SelectedValue;
            objLab.LTSM_DR_ID = drpDoctors.SelectedValue;
            objLab.LTSM_DR_NAME = drpDoctors.SelectedItem.Text;
            objLab.LTSM_SERV_ID = "";
            objLab.LTSM_SERV_DESCRIPTION = "";
            objLab.LTSM_FEE = "";
            objLab.LTSM_REMARKS = txtRemarks.Text;
            objLab.LTSM_REP_TYPE = drpReportType.SelectedValue;

            objLab.LTSM_TEST_EPRID = "";
            objLab.LTSM_Posted = "";
            objLab.LTSM_POSTED_NO = "";
            objLab.LTSM_DELIVERY_DATE = txtDeliveryDate.Text + " " + drpDelivHour.SelectedValue + ":" + drpDelivMin.SelectedValue + ":00";
            objLab.LTSM_LAB_REQ_NO = "";
            objLab.LTSM_Ref_Hospital = "";
            objLab.BARECODE = txtBarecodeID.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }

            objLab.SCRN_ID = "LABSAMPLE";

            objLab.INVOICE_ID = txtInvoiceRefNo.Text.Trim();
            objLab.EMR_ID = txtEMRID.Text.Trim();

            objLab.UserID = Convert.ToString(Session["User_ID"]);
            string SAMPLE_ID = "";
            SAMPLE_ID = objLab.LABTestSampleMasterAdd();
            txtTestSampleID.Text = SAMPLE_ID;
            hidActualSampleID.Value = txtTestSampleID.Text;

            objCom = new CommonBAL();
            string Criteria = " LTSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSD_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            objCom.fnDeleteTableData("LAB_TEST_SAMPLE_DETAIL", Criteria);


            foreach (GridViewRow row in gvTestSampleDtls.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");

                Label lblgvTestSampleDtlsProfilID = (Label)row.FindControl("lblgvTestSampleDtlsProfilID");
                Label lblgvTestSampleDtlsDesc = (Label)row.FindControl("lblgvTestSampleDtlsDesc");

                Label lblTestTypeCode = (Label)row.FindControl("lblTestTypeCode");
                Label lblTestTypeDesc = (Label)row.FindControl("lblTestTypeDesc");

                DropDownList drpgvTestSampleDtlsStatus = (DropDownList)row.FindControl("drpgvTestSampleDtlsStatus");

                DropDownList drpgvTestSampleDtlsCollMethod = (DropDownList)row.FindControl("drpgvTestSampleDtlsCollMethod");

                if (chkSelService.Checked == true)
                {
                    objLab.LTSM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objLab.LTSM_TEST_SAMPLE_ID = txtTestSampleID.Text;
                    objLab.LTSD_TEST_PROFILE_ID = lblgvTestSampleDtlsProfilID.Text;
                    objLab.LTSD_TEST_DESCRIPTION = lblgvTestSampleDtlsDesc.Text;

                    objLab.TEST_TYPE_CODE = lblTestTypeCode.Text;
                    objLab.TEST_TYPE_DESC = lblTestTypeDesc.Text;

                    objLab.LTSD_TEST_RESULT = "";
                    objLab.LTSD_TYPE = "";
                    objLab.LTSD_SELECTED = "1";
                    objLab.LTSD_STATUS = drpgvTestSampleDtlsStatus.SelectedValue;

                    if (drpgvTestSampleDtlsCollMethod.SelectedIndex != 0)
                    {
                        objLab.COLLECTION_METHOD_CODE = drpgvTestSampleDtlsCollMethod.SelectedValue;
                        objLab.COLLECTION_METHOD_NAME = drpgvTestSampleDtlsCollMethod.SelectedItem.Text;
                    }
                    else
                    {
                        objLab.COLLECTION_METHOD_CODE = "";
                        objLab.COLLECTION_METHOD_NAME = "";
                    }


                    objLab.UserID = Convert.ToString(Session["User_ID"]);
                    objLab.LABTestSampleDtlsAdd();
                    string strTestType = "";

                    strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

                    // if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                    // {
                    if (strTestType == "EMR")
                    {
                        string FieldNameWithValues = " Completed='LAB_SAMPLE_COLLECTED' , EPL_DATE=GETDATE() ";
                        string Criteria1 = "  EPL_ID ='" + Convert.ToString(ViewState["EMRID"]) + "' AND EPL_LAB_CODE='" + lblgvTestSampleDtlsProfilID.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                    }
                    else
                    {
                        string FieldNameWithValues = " HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED',HIT_LAB_SAMPLE_DATE=GETDATE() ";
                        string Criteria1 = " HIT_INVOICE_ID ='" + Convert.ToString(ViewState["InvoiceID"]) + "' AND HIT_SERV_CODE='" + lblgvTestSampleDtlsProfilID.Text.Trim() + "'";

                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);
                    }

                    // }
                }
            }


            objCom = new CommonBAL();
            string FieldNameWithValues2 = " LTRM_STATUS='SAMPLE_COLLECTED' ";
            string Criteria2 = " LTRM_REG_ID='" + Convert.ToString(ViewState["LabRegisterID"]) + "'  AND (LTRM_STATUS IS NULL OR LTRM_STATUS ='' )";
            objCom.fnUpdateTableData(FieldNameWithValues2, "LAB_TEST_REGISTER_MASTER", Criteria2);

        FunEnd: ;
            return isError;
        }

        void UpdateQRCode()
        {
            // Byte[] bytImage1 = null;

            QRCodeEncoder encoder = new QRCodeEncoder();
            encoder.QRCodeScale = 5;
            Bitmap img;
            img = encoder.Encode(txtTestSampleID.Text);
            // bytImage1 = ImageToByte(img);

            byte[] arr;
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                arr = ms.ToArray();
                //img.Save(@"c:\"+ txtTestSampleID.Text.Trim()  + ".jpg");
            }



            //objCom = new CommonBAL();
            //string FieldNameWithValues2 = " LTSM_QRCODE='" + arr + "'";
            //String Criteria = " LTSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTSM_TEST_SAMPLE_ID='" + txtTestSampleID.Text.Trim() + "'";
            //objCom.fnUpdateTableData(FieldNameWithValues2, "LAB_TEST_SAMPLE_MASTER", Criteria);

            string strCon = System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString();
            SqlConnection Con = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_LAB_TestSampleMasterQRCodeUpdate";
            cmd.Parameters.Add(new SqlParameter("@LTSM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
            cmd.Parameters.Add(new SqlParameter("@LTSM_TEST_SAMPLE_ID", SqlDbType.VarChar)).Value = txtTestSampleID.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@LTSM_QRCODE", SqlDbType.Image)).Value = arr;

            cmd.ExecuteNonQuery();
            Con.Close();


        }

        void TransferTestResultData()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = "  ID='" + txtTestSampleID.Text + "' AND UploadedToAnalyzer=1";
            DS = objCom.fnGetFieldValue("*", "LAB_TEST_RESULT_DATA", Criteria, "");


            if (DS.Tables[0].Rows.Count <= 0)
            {

                objLab = new LaboratoryBAL();

                objLab.SAMPLE_ID = txtTestSampleID.Text;
                objLab.INVOICE_ID = Convert.ToString(ViewState["InvoiceID"]);
                objLab.EMR_ID = Convert.ToString(ViewState["EMRID"]);
                objLab.UserID = Convert.ToString(Session["User_ID"]);
                objLab.TestResultDataAdd();

            }

        }

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));

        }

        void Clear()
        {
            ViewState["NewFlag"] = true;
            if (drpReportType.Items.Count > 0)
            {
                drpReportType.SelectedIndex = 0;
            }
            txtRemarks.Text = "";
            txtBarecodeID.Text = "";

            txtInvoiceRefNo.Text = "";
            txtEMRID.Text = "";

            ViewState["InvoiceID"] = "";
            ViewState["EMRID"] = "";

            ViewState["PatientID"] = "";
            ViewState["DoctorID"] = "";
            ViewState["TransDate"] = "";
            ViewState["TransType"] = "";
            ViewState["ServiceID"] = "";
            ViewState["LabRegisterID"] = "";
            BuildeTestSampleDtls();
            BindTempTestSampleDtls();
        }

        void New()
        {
            ViewState["NewFlag"] = true;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtDeliveryDate.Text = strFromDate.ToString("dd/MM/yyyy");


            txtTestSampleID.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "LABSAMPLE");
        }

        void BindTime12HrsFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");


            drpSTHour.DataSource = DSHours;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();


            drpDelivHour.DataSource = DSHours;
            drpDelivHour.DataTextField = "Name";
            drpDelivHour.DataValueField = "Code";
            drpDelivHour.DataBind();

            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpSTMin.DataSource = DSMin;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();


            drpDelivMin.DataSource = DSMin;
            drpDelivMin.DataTextField = "Name";
            drpDelivMin.DataValueField = "Code";
            drpDelivMin.DataBind();

        }

        DataSet BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "HCM_DESC");



            return DS;


        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "LAB_SAMPLE";
            objCom.ScreenName = "Lab Sample Collection";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_SAMPLE' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetTestProfileMaster(string prefixText)
        {
            DataSet ds = new DataSet();
            LaboratoryBAL obLab = new LaboratoryBAL();

            string[] Data;

            string Criteria = " 1=1 ";
            Criteria += " AND LTPM_STATUS='A' ";

            Criteria += " AND ( LTPM_TEST_PROFILE_ID LIKE '%" + prefixText + "%' OR LTPM_DESCRIPTION LIKE'%" + prefixText + "%' )";

            ds = obLab.TestProfileMasterGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["LTPM_TEST_PROFILE_ID"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["LTPM_DESCRIPTION"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Lab Sample Collection Page");

                try
                {
                    ViewState["NewFlag"] = true;
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtDeliveryDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    string strHour = Convert.ToString(System.DateTime.Now.Hour);
                    if (Convert.ToInt32(strHour) <= 9)
                    {
                        strHour = "0" + strHour;
                    }
                    string strMin = Convert.ToString(System.DateTime.Now.Minute);
                    if (Convert.ToInt32(strMin) <= 9)
                    {
                        strMin = "0" + strMin;
                    }

                    BindTime12HrsFromDB();


                    if (GlobalValues.FileDescription.ToUpper() == "ACCUCARE" || GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        btnLabelPrint1.Visible = true;
                        btnLabelPrint.Visible = false;
                        hidBareCodePrint.Value = "1";
                    }
                    if (GlobalValues.FileDescription.ToUpper() == "JANSONS")
                    {
                        trLabelPrintType.Visible = true;
                        chkEDTA.Checked = true;
                        chkSERUM.Checked = true;
                        chkFLUORIDE.Checked = true;

                    }



                    if (drpDelivHour.Items.Count > 0)
                    {

                        for (int i = 0; i < drpDelivHour.Items.Count; i++)
                        {

                            if (drpDelivHour.Items[i].Value == strHour)
                            {
                                drpDelivHour.SelectedValue = strHour;

                                goto HourLoopEnd;
                            }
                        }

                    HourLoopEnd: ;
                    }


                    if (drpDelivMin.Items.Count > 0)
                    {

                        for (int i = 0; i < drpDelivMin.Items.Count; i++)
                        {

                            if (drpDelivMin.Items[i].Value == strMin)
                            {
                                drpDelivMin.SelectedValue = strMin;

                                goto MinLoopEnd;
                            }
                        }

                    MinLoopEnd: ;
                    }






                    if (drpSTHour.Items.Count > 0)
                    {

                        for (int i = 0; i < drpSTHour.Items.Count; i++)
                        {

                            if (drpSTHour.Items[i].Value == strHour)
                            {
                                drpSTHour.SelectedValue = strHour;

                                goto HourLoopEnd;
                            }
                        }

                    HourLoopEnd: ;
                    }


                    if (drpSTMin.Items.Count > 0)
                    {

                        for (int i = 0; i < drpSTMin.Items.Count; i++)
                        {

                            if (drpSTMin.Items[i].Value == strMin)
                            {
                                drpSTMin.SelectedValue = strMin;

                                goto MinLoopEnd;
                            }
                        }

                    MinLoopEnd: ;
                    }



                    //if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    //{
                    //    drpDoctors.Enabled = false;
                    //    drpRefDoctor.Enabled = false;
                    //}


                    txtTestSampleID.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "LABSAMPLE");



                    BindReportType();

                    BindNationality();
                    BindDoctor();
                    BinRefdDoctor();
                    BuildeTestSampleDtls();

                    ViewState["InvoiceID"] = Request.QueryString["InvoiceID"];
                    ViewState["EMRID"] = Request.QueryString["EMRID"];

                    txtInvoiceRefNo.Text = Request.QueryString["InvoiceID"];
                    txtEMRID.Text = Request.QueryString["EMRID"];


                    ViewState["PatientID"] = Request.QueryString["PatientID"];
                    ViewState["LabRegisterID"] = Request.QueryString["LabRegisterID"];
                    txtBarecodeID.Text = Request.QueryString["LabRegisterBareCode"];

                    //ViewState["DoctorID"] = Request.QueryString["DoctorID"];
                    //ViewState["TransDate"] = Request.QueryString["TransDate"];
                    //ViewState["TransType"] = Request.QueryString["TransType"];
                    //ViewState["ServiceID"] = Request.QueryString["ServiceID"];


                    string PageName = (string)Request.QueryString["PageName"];
                    if (PageName == "LabWaitList")
                    {
                        txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);
                        txtFileNo_TextChanged(txtFileNo, new EventArgs());

                        //  BindInvProfileTrans();
                        //   BindServiceTrans();
                        BindLabTestRegtMaster();
                        BindRegisterTrans();
                        BindTempTestSampleDtls();
                        BindEMRDtls();
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                PTDtlsClear();

                New();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtTestSampleID_TextChanged(object sender, EventArgs e)
        {
            Clear();
            PTDtlsClear();
            BindLabSampleReportMaster();
            BindTestSampleDtls();
            BindTempTestSampleDtls();
            BindLabTestRegtMaster();
            BindEMRDtls();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                //DateTime dtCollDate= Convert.ToDateTime(txtTransDate.Text + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00");
                //DateTime dtDelDate= Convert.ToDateTime(txtDeliveryDate.Text + " " + drpDelivHour.SelectedValue + ":" + drpDelivMin.SelectedValue + ":00");




                Boolean boolIsChecked = false; ;
                for (int i = 0; i < gvTestSampleDtls.Rows.Count; i++)
                {
                    CheckBox chkSelService;
                    chkSelService = (CheckBox)gvTestSampleDtls.Rows[i].Cells[0].FindControl("chkSelService");

                    if (chkSelService.Checked == true)
                    {
                        boolIsChecked = true;
                    }

                }



                if (boolIsChecked == false)
                {
                    lblStatus.Text = "Please select Any of one of the Test";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;

                }



                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {
                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Sample Collection, Sample ID is " + txtTestSampleID.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Lab Sample Collection, Sample ID is " + txtTestSampleID.Text);
                    }

                     
                        if (Convert.ToString(Session["LAB_RESULT_DATAUPLOAD"]) == "1")
                        {
                            if (GlobalValues.FileDescription == "SMCH")
                            {
                                TransferTestResultData();
                            }
                            else
                            {
                                LAB_HL7MessageGenerator objHL7Mess = new LAB_HL7MessageGenerator();
                                objHL7Mess.PT_ID = txtFileNo.Text.Trim();
                                objHL7Mess.LAB_TEST_SAMPLE_ID = txtTestSampleID.Text.Trim();
                                //objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                                //objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                                //objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);
                                //objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);

                                Boolean IsError = false;
                                IsError = objHL7Mess.LabAnalyzerMessageGenerate_ORU_R01();

                                lblStatus.Text = "Request Send";
                                lblStatus.ForeColor = System.Drawing.Color.Green;


                            }
                        }
                     

                    UpdateQRCode();
                    // Clear();
                    // PTDtlsClear();
                    //New();


                    if (hidLabelPrint.Value == "true")
                    {
                        btnLabelPrint1_Click(btnLabelPrint, new EventArgs());


                    }


                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }

            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + " btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (chkMultyReptType.Checked == false)
            //{
            BuildeTestSampleDtls();
            //}
            BindInvProfileTrans();
            BindTempTestSampleDtls();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtTestName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TestSampleDtls"];

                strTestCodeDesc = txtTestName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    lblStatus.Text = "Enter Test Code And Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);

                    DT.Rows[R]["LTSD_TEST_PROFILE_ID"] = TestProfileID;
                    DT.Rows[R]["LTSD_TEST_DESCRIPTION"] = TestProfileDesc;
                    //  DT.Rows[intCurRow]["LTRD_TEST_RESULT"] = drpgvTestReportDtlsResult.SelectedValue;
                    // DT.Rows[R]["LTRD_RANGE"] = "";
                    // DT.Rows[R]["LTRD_TEST_NORMALVALUE"] = "";
                    //  DT.Rows[R]["LTRD_TEST_UNIT"] = "";
                    DT.AcceptChanges();
                }
                else
                {

                    objrow["LTSD_TEST_PROFILE_ID"] = TestProfileID;
                    objrow["LTSD_TEST_DESCRIPTION"] = TestProfileDesc;

                    DT.Rows.Add(objrow);
                }



                ViewState["TestSampleDtls"] = DT;


                BindTempTestSampleDtls();
                txtTestName.Text = "";

                ViewState["gvServSelectIndex"] = "";

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvTestSampleDtls.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvTestSampleDtls.Rows)
                {
                    CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                    if (ChkBoxHeader.Checked == true)
                    {
                        chkSelService.Checked = true;
                    }
                    else
                    {
                        chkSelService.Checked = false;
                    }
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnLabelPrint_Click(object sender, EventArgs e)
        {
            if (hidActualSampleID.Value == "")
            {
                goto FunEnd;
            }

            Int32 intPrintCount = 1;

            intPrintCount = gvTestSampleDtls.Rows.Count;

            //foreach (GridViewRow row in gvTestSampleDtls.Rows)
            //{
            //    CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");

            //    if (chkSelService.Checked == true)
            //    {
            //        intPrintCount = intPrintCount + 1;
            //    }

            //}



            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(intPrintCount);



            String DBString = "", strFormula = "";

            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualSampleID.Value;

            foreach (GridViewRow row in gvTestSampleDtls.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                Label lblgvTestSampleDtlsProfilID = (Label)row.FindControl("lblgvTestSampleDtlsProfilID");
                Label lblTestTypeDesc = (Label)row.FindControl("lblTestTypeDesc");


                if (chkSelService.Checked == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowLabelPrint('" + DBString + "','" + strFormula + "','" + intCount + "'  ,'" + lblgvTestSampleDtlsProfilID.Text + "' ," + txtPrintCount.Value + ",'" + chkEDTA.Checked + "' ,'" + chkSERUM.Checked + "' ,'" + chkFLUORIDE.Checked + "'  );", true);

                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "CallBareCodeexe('" + txtTestSampleID.Text + "','" + txtFileNo.Text.Trim() + "','" + txtFName.Text.Trim() + "','" + lblgvTestSampleDtlsDesc.Text.Trim() + "' ,'" + txtTransDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "','" + txtBarecodeID.Text.Trim() + "' ," + txtPrintCount.Value + ",'" + chkEDTA.Checked + "' ,'" + chkSERUM.Checked + "' ,'" + chkFLUORIDE.Checked + "');", true);

                }


            }
        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END


        FunEnd: ;
        }




        protected void btnQRPrint_Click(object sender, EventArgs e)
        {
            if (hidActualSampleID.Value == "")
            {
                goto FunEnd;
            }

            Int32 intPrintCount = 1;

            intPrintCount = gvTestSampleDtls.Rows.Count;




            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(intPrintCount);



            String DBString = "", strFormula = "";

            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualSampleID.Value;


            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowQRPrint('" + DBString + "','" + strFormula + "'  );", true);






        FunEnd: ;
        }



        protected void btnLabelPrint1_Click(object sender, EventArgs e)
        {
            if (hidActualSampleID.Value == "")
            {
                goto FunEnd;
            }

            Int32 intPrintCount = 1;

            intPrintCount = gvTestSampleDtls.Rows.Count;

            //foreach (GridViewRow row in gvTestSampleDtls.Rows)
            //{
            //    CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");

            //    if (chkSelService.Checked == true)
            //    {
            //        intPrintCount = intPrintCount + 1;
            //    }

            //}



            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(intPrintCount);



            String DBString = "", strFormula = "";

            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualSampleID.Value;

            //  string ProfileID = "";
            foreach (GridViewRow row in gvTestSampleDtls.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                Label lblgvTestSampleDtlsProfilID = (Label)row.FindControl("lblgvTestSampleDtlsProfilID");
                Label lblgvTestSampleDtlsDesc = (Label)row.FindControl("lblgvTestSampleDtlsDesc");

                if (chkSelService.Checked == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "CallBareCodeexe('" + txtTestSampleID.Text + "','" + txtFileNo.Text.Trim() + "','" + txtFName.Text.Trim() + "','" + lblgvTestSampleDtlsDesc.Text.Trim() + "' ,'" + txtTransDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "','" + txtBarecodeID.Text.Trim() + "');", true);


                    // ProfileID = lblgvTestSampleDtlsProfilID.Text;
                }




            }

            /*
        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END
            string path = @"C:\inetpub\wwwroot\Mediplus\PrintBar.txt";
           // if (!File.Exists(path))
           // {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("");
                    sw.WriteLine("N");
                    sw.WriteLine(@"B25,30,0,1,2,4,47,B, """ + txtTestSampleID.Text + @"""");
                    sw.WriteLine(@"A25,120,0,2,1,1,N,  """ + txtFileNo.Text +@"""");
                    sw.WriteLine(@"A25,160,0,3,1,1,N,  """ + ProfileID +@"""");
                }
           // } 

                if (ProfileID != "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "BareCodePrint('" + ProfileID + "'  );", true);
                }

            */


               FunEnd: ;
        }

        protected void gvTestSampleDtls_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblgvTestSampleDtlsSelected = (Label)e.Row.Cells[0].FindControl("lblgvTestSampleDtlsSelected");
                    CheckBox chkSelService = (CheckBox)e.Row.Cells[0].FindControl("chkSelService");
                    Label lblgvTestSampleDtlsStatus = (Label)e.Row.Cells[0].FindControl("lblgvTestSampleDtlsStatus");
                    DropDownList drpgvTestSampleDtlsStatus = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestSampleDtlsStatus");

                    Label lblgvTestSampleDtlsCollMethod = (Label)e.Row.Cells[0].FindControl("lblgvTestSampleDtlsCollMethod");
                    DropDownList drpgvTestSampleDtlsCollMethod = (DropDownList)e.Row.Cells[0].FindControl("drpgvTestSampleDtlsCollMethod");


                    if (lblgvTestSampleDtlsSelected.Text == "1")
                    {

                        chkSelService.Checked = true;
                    }


                    if (lblgvTestSampleDtlsStatus.Text != "")
                    {
                        drpgvTestSampleDtlsStatus.SelectedValue = lblgvTestSampleDtlsStatus.Text;
                    }

                    drpgvTestSampleDtlsCollMethod.DataSource = BindCommonMaster("SpecimenCollectionMethod");
                    drpgvTestSampleDtlsCollMethod.DataTextField = "HCM_DESC";
                    drpgvTestSampleDtlsCollMethod.DataValueField = "HCM_CODE";
                    drpgvTestSampleDtlsCollMethod.DataBind();


                    drpgvTestSampleDtlsCollMethod.Items.Insert(0, "--- Select ---");

                    drpgvTestSampleDtlsCollMethod.Items[0].Value = "";

                    if (lblgvTestSampleDtlsCollMethod.Text != "")
                    {
                        drpgvTestSampleDtlsCollMethod.SelectedValue = lblgvTestSampleDtlsCollMethod.Text;
                    }
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvTestReportDtls_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }


        }

        #endregion
    }
}