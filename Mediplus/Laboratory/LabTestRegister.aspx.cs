﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Laboratory
{
    public partial class LabTestRegister : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void LabTestRegisterTimLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../LabTestRegisterTimLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindNationality()
        {
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "Select");
            drpNationality.Items[0].Value = "0";



        }

        void BindDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";// AND HSFM_DEPT_ID='RADIOLOGY' 

            if (GlobalValues.FileDescription == "SMCH")
            {
                Criteria += "  AND HSFM_DEPT_ID='PATHOLOGY' ";
            }

            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDoctors.DataSource = ds;
                drpDoctors.DataTextField = "FullName";
                drpDoctors.DataValueField = "HSFM_STAFF_ID";
                drpDoctors.DataBind();


                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;



            }

            drpDoctors.Items.Insert(0, "--- Select ---");
            drpDoctors.Items[0].Value = "";


        }


        void BinRefdDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpRefDoctor.DataSource = ds;
                drpRefDoctor.DataTextField = "FullName";
                drpRefDoctor.DataValueField = "HSFM_STAFF_ID";
                drpRefDoctor.DataBind();

            }



            drpRefDoctor.Items.Insert(0, "--- Select ---");
            drpRefDoctor.Items[0].Value = "";
        }



        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                //  lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                drpAgeType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                drpAgeType1.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);


                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                //txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                //txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                //txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                //txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();

                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]).ToUpper())
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNationality;
                        }
                    }

                }

            ForNationality: ;

                //txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                //txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                //txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                // if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                //   lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                // txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                txtProviderID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);



                //    if (ds.Tables[0].Rows[0].IsNull("HPM_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]) != "")
                //    {
                //        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                //        {
                //            if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]).ToUpper())
                //            {
                //                drpDoctors.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);
                //                goto ForDoctors;
                //            }
                //        }

                //    }

                //ForDoctors: ;


                //if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "")
                if (ds.Tables[0].Rows[0].IsNull("HPM_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]) != "")
                {
                    for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                    {
                        if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]).ToUpper())
                        {
                            drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);
                            goto ForRefDoctors;
                        }
                    }

                }

            ForRefDoctors: ;
                // GetPatientVisit();

                drpInvType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";


            }

        }

        void PTDtlsClear()
        {

            txtFName.Text = "";
            lblStatus.Text = "";

            txtDOB.Text = "";
            //lblAge.Text = "";

            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            //txtAddress.Text = "";
            //txtPoBox.Text = "";

            //txtArea.Text = "";
            //txtCity.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";




            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";




            drpDoctors.SelectedIndex = 0;


        }

        void BindLabTestRegtMaster()
        {
            objLab = new LaboratoryBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRM_REG_ID='" + txtTransID.Text.Trim() + "'";
            DS = objLab.TestRegisterMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["NewFlag"] = false;
                    txtTransID.Text = Convert.ToString(DR["LTRM_REG_ID"]);
                    txtTransDate.Text = Convert.ToString(DR["LTRM_REG_DATEDesc"]);


                    if (DR.IsNull("LTRM_REFERAL_TYPE") == false && Convert.ToString(DR["LTRM_REFERAL_TYPE"]) != "")
                    {
                        drpReferalType.SelectedValue = Convert.ToString(DR["LTRM_REFERAL_TYPE"]);
                    }
                    txtFileNo.Text = Convert.ToString(DR["LTRM_PATIENT_ID"]);
                    txtFileNo_TextChanged(txtFileNo, new EventArgs());


                    txtInvoiceRefNo.Text = Convert.ToString(DR["LTRM_INVOICE_ID"]);

                    txtEMRID.Text = Convert.ToString(DR["LTRM_EMR_ID"]);

                    txtBarecodeID.Text = Convert.ToString(DR["LTRM_BARECODE"]);



                    if (DS.Tables[0].Rows[0].IsNull("LTRM_DR_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]) != "")
                    {
                        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                        {
                            if (drpDoctors.Items[intCount].Value.ToUpper() == Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]).ToUpper())
                            {
                                drpDoctors.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]);
                                goto ForDoctors;
                            }
                        }

                    }

                ForDoctors: ;

                    if (DS.Tables[0].Rows[0].IsNull("LTRM_REF_DR") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]) != "")
                    {
                        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                        {
                            if (drpRefDoctor.Items[intCount].Value.ToUpper() == Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]).ToUpper())
                            {
                                drpRefDoctor.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]);
                                goto ForRefDoctors;
                            }
                        }

                    }

                ForRefDoctors: ;


                }
            }

        }

        void BindLabTestRegtDetail()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND LTRD_REG_ID='" + txtTransID.Text + "'";

            objLab = new LaboratoryBAL();
            DataSet DS = new DataSet();
            DS = objLab.TestRegisterDetailGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["ServiceList"];

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["ID"] = txtInvoiceRefNo.Text;
                    objrow["ServiceID"] = Convert.ToString(DR["LTRD_SERV_CODE"]);
                    objrow["Description"] = Convert.ToString(DR["LTRD_DESCRIPTION"]);
                    objrow["TEST_TYPE_CODE"] = Convert.ToString(DR["LTRD_TEST_TYPE_CODE"]);
                    objrow["TEST_TYPE_DESC"] = Convert.ToString(DR["LTRD_TEST_TYPE_DESC"]);

                    DT.Rows.Add(objrow);

                }

                ViewState["ServiceList"] = DT;

            }
        }

        void BuildServiceList()
        {
            DataTable dt = new DataTable();

            DataColumn ID = new DataColumn();
            ID.ColumnName = "ID";

            DataColumn ServiceID = new DataColumn();
            ServiceID.ColumnName = "ServiceID";

            DataColumn Description = new DataColumn();
            Description.ColumnName = "Description";



            DataColumn TEST_TYPE_CODE = new DataColumn();
            TEST_TYPE_CODE.ColumnName = "TEST_TYPE_CODE";

            DataColumn TEST_TYPE_DESC = new DataColumn();
            TEST_TYPE_DESC.ColumnName = "TEST_TYPE_DESC";

            dt.Columns.Add(ID);
            dt.Columns.Add(ServiceID);
            dt.Columns.Add(Description);
            dt.Columns.Add(TEST_TYPE_CODE);
            dt.Columns.Add(TEST_TYPE_DESC);


            ViewState["ServiceList"] = dt;
        }

        void BindServiceList()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);



            string Criteria = " 1=1 ";

            if (strTestType == "EMR")
            {
                Criteria += " AND Completed IS NULL ";
                Criteria += " AND EPL_ID='" + txtEMRID.Text + "'";

                CommonBAL objCom = new CommonBAL();
                objCom.LaboratoryGet(Criteria);

                DataSet DS = new DataSet();
                DS = objCom.LaboratoryGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["ServiceList"];

                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {

                        DataRow objrow;
                        objrow = DT.NewRow();
                        objrow["ID"] = Convert.ToString(ViewState["TransID"]);
                        objrow["ServiceID"] = Convert.ToString(DR["EPL_LAB_CODE"]);
                        objrow["Description"] = Convert.ToString(DR["EPL_LAB_NAME"]);

                        string TestTypeCode, TestTypeDesc;
                        GetInvProfileTestType(Convert.ToString(DR["EPL_LAB_CODE"]), out TestTypeCode, out  TestTypeDesc);

                        if (TestTypeCode == "")
                        {
                            GetProfileMasterTestType(Convert.ToString(DR["EPL_LAB_CODE"]), out TestTypeCode, out  TestTypeDesc);
                        }


                        objrow["TEST_TYPE_CODE"] = TestTypeCode;
                        objrow["TEST_TYPE_DESC"] = TestTypeDesc;

                        DT.Rows.Add(objrow);

                    }

                    ViewState["ServiceList"] = DT;

                }



            }
            else
            {
                Criteria += " AND HIT_LAB_STATUS IS NULL ";
                Criteria += " AND HIT_SERV_TYPE='L'  ";

                Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceRefNo.Text + "'";

                dboperations dbo = new dboperations();
                clsInvoice objInv = new clsInvoice();
                DataSet DS = new DataSet();
                DS = objInv.InvoiceTransGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["ServiceList"];

                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {

                        DataRow objrow;
                        objrow = DT.NewRow();
                        objrow["ID"] = Convert.ToString(ViewState["TransID"]);
                        objrow["ServiceID"] = Convert.ToString(DR["HIT_SERV_CODE"]);
                        objrow["Description"] = Convert.ToString(DR["HIT_DESCRIPTION"]);

                        string TestTypeCode, TestTypeDesc;
                        GetInvProfileTestType(Convert.ToString(DR["HIT_SERV_CODE"]), out TestTypeCode, out  TestTypeDesc);


                        objrow["TEST_TYPE_CODE"] = TestTypeCode;
                        objrow["TEST_TYPE_DESC"] = TestTypeDesc;

                        DT.Rows.Add(objrow);

                    }

                    ViewState["ServiceList"] = DT;

                }
            }
        }

        void BindTempServiceList()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ServiceList"];
            if (DT.Rows.Count > 0)
            {
                gvServiceList.DataSource = DT;
                gvServiceList.DataBind();

            }
            else
            {
                gvServiceList.DataBind();
            }

        }


        void GetInvProfileTestType(string ServiceID, out string TestTypeCode, out string TestTypeDesc)
        {
            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LIPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  LIPM_SERV_ID='" + ServiceID + "'";

            DS = objLab.InvProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LIPM_TEST_TYPE_DESC"]);
            }

        }

        void GetProfileMasterTestType(string ServiceID, out string TestTypeCode, out string TestTypeDesc)
        {
            TestTypeCode = "";
            TestTypeDesc = "";

            DataSet DS = new DataSet();
            objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  LTPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  LTPM_SERV_ID='" + ServiceID + "'";

            DS = objLab.TestProfileMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                TestTypeCode = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_CODE"]);
                TestTypeDesc = Convert.ToString(DS.Tables[0].Rows[0]["LTPM_TEST_TYPE_DESC"]);
            }

        }

        string GetEMRID()
        {
            string strEMR_ID = "0";
            DataSet DS = new DataSet();
            String Criteria = " 1=1  ";

            string strStartDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";

            Criteria += " AND  EPM_PT_ID LIKE '%" + Convert.ToString(ViewState["PatientID"]) + "%'";
            Criteria += " AND  EPM_DR_CODE LIKE '%" + Convert.ToString(ViewState["RefDoctorID"]) + "%'";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strEMR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);


            }


            return strEMR_ID;
        }

        void BindEMRDtls()
        {
            string Criteria = "EPM_ID='" + txtEMRID.Text.Trim() + "'";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DataSet DS = new DataSet();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtProviderID.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_INS_CODE"]);
                txtProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_INS_NAME"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_POLICY_NO"]);
            }
        }

        Boolean fnSave()
        {
            Boolean isError = false;
            if (txtTransDate.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Trans Date";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter File No";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }
            if (drpReferalType.SelectedValue == "External")
            {
                if (filAttachment1.PostedFile.ContentLength == 0 && filAttachment2.PostedFile.ContentLength == 0)
                {
                    lblStatus.Text = "Please Attach Files";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }
            }

            Boolean IsLabNotSelected = false;
            if (gvServiceList.Rows.Count <= 0)
            {
                lblStatus.Text = "Please Select Services";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }

            foreach (GridViewRow row in gvServiceList.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                Label lblID = (Label)row.FindControl("lblID");

                if (chkSelService.Checked == true)
                {
                    IsLabNotSelected = true;
                }


            }


            if (IsLabNotSelected == false)
            {
                lblStatus.Text = "Please Select Services";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                isError = true;
                goto FunEnd;
            }



            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

            string strPath1 = @GlobalValues.Lab_Reg_AttachmentPath;// Server.MapPath("../Uploads/Temp/");
            string strAttach1 = "", strAttach2 = "";

            if (filAttachment1.PostedFile != null && filAttachment1.FileName != "")
            {
                if (filAttachment1.PostedFile.ContentLength != 0)
                {
                    String ext1 = System.IO.Path.GetExtension(filAttachment1.FileName);
                    strAttach1 = strName + "_1" + ext1;
                    filAttachment1.SaveAs(strPath1 + strAttach1);

                }
            }

            if (filAttachment2.PostedFile != null && filAttachment2.FileName != "")
            {
                if (filAttachment2.PostedFile.ContentLength != 0)
                {
                    String ext2 = System.IO.Path.GetExtension(filAttachment2.FileName);
                    strAttach2 = strName + "_2" + ext2;
                    filAttachment2.SaveAs(strPath1 + strAttach2);
                }
            }

            //   LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---TestRegisterMasterAdd() Start");
            objLab = new LaboratoryBAL();
            objLab.BranchID = Convert.ToString(Session["Branch_ID"]);
            objLab.LTRM_REG_ID = txtTransID.Text.Trim();
            objLab.LTRM_REG_DATE = txtTransDate.Text.Trim();
            objLab.PT_ID = txtFileNo.Text.Trim();
            objLab.PT_NAME = txtFName.Text.Trim();
            objLab.LTRM_REFERAL_TYPE = drpReferalType.SelectedValue;
            objLab.INVOICE_ID = txtInvoiceRefNo.Text;
            objLab.EMR_ID = txtEMRID.Text;

            objLab.LTRM_DR_CODE = drpDoctors.SelectedValue;
            objLab.LTRM_DR_NAME = drpDoctors.SelectedItem.Text;
            objLab.LTRM_REF_DR = drpRefDoctor.SelectedValue;


            objLab.LTRM_ATTACHMENT1 = strAttach1;
            objLab.LTRM_ATTACHMENT2 = strAttach2;
            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objLab.NewFlag = "A";
            }
            else
            {
                objLab.NewFlag = "M";
            }

            objLab.SCRN_ID = "LABREG";

            objLab.UserID = Convert.ToString(Session["User_ID"]);
            objLab.TestRegisterMasterAdd();

            // LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---TestRegisterMasterAdd() completed");

            objCom = new CommonBAL();

            string Criteria = " LTRD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND LTRD_REG_ID='" + txtTransID.Text.Trim() + "'";
            objCom.fnDeleteTableData("LAB_TEST_REGISTER_DETAIL", Criteria);

            //  LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---LAB_TEST_REGISTER_DETAIL delete  completed");

            foreach (GridViewRow row in gvServiceList.Rows)
            {
                CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                Label lblID = (Label)row.FindControl("lblID");
                Label lblServiceID = (Label)row.FindControl("lblServiceID");
                Label lblDescription = (Label)row.FindControl("lblDescription");

                Label lblTestTypeCode = (Label)row.FindControl("lblTestTypeCode");
                Label lblTestTypeDesc = (Label)row.FindControl("lblTestTypeDesc");

                if (chkSelService.Checked == true)
                {
                    objLab = new LaboratoryBAL();
                    objLab.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objLab.LTRM_REG_ID = txtTransID.Text.Trim();
                    objLab.LTRD_SERV_CODE = lblServiceID.Text.Trim();
                    objLab.LTRD_DESCRIPTION = lblDescription.Text.Trim();

                    objLab.TEST_TYPE_CODE = lblServiceID.Text.Trim();
                    objLab.TEST_TYPE_DESC = lblTestTypeDesc.Text.Trim();
                    objLab.TestRegisterDetailAdd();

                    // LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---TestRegisterDetailAdd   completed");
                    string strTestType = "";

                    strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);
                    if (strTestType == "EMR")
                    {
                        string FieldNameWithValues = " Completed='LAB_REGISTERED' , EPL_DATE=GETDATE() ";
                        string Criteria1 = " Completed IS NULL AND EPL_ID ='" + lblID.Text.Trim() + "' AND EPL_LAB_CODE='" + lblServiceID.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                    }
                    else
                    {

                        string FieldNameWithValues = " HIT_LAB_STATUS='LAB_REGISTERED' , HIT_LAB_REG_DATE=GETDATE() ";
                        string Criteria1 = " HIT_LAB_STATUS IS NULL AND HIT_INVOICE_ID ='" + lblID.Text.Trim() + "' AND HIT_SERV_CODE='" + lblServiceID.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);

                        // LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---HMS_INVOICE_TRANSACTION Update  completed");
                    }
                }

            }

           // LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---fnSave() completed");
        FunEnd: ;
            return isError;
        }

        void Clear()
        {
            drpReferalType.SelectedIndex = 0;
            txtInvoiceRefNo.Text = "";
            txtEMRID.Text = "";


            ViewState["NewFlag"] = true;
            ViewState["TransID"] = "";
            ViewState["PatientID"] = "";
            ViewState["RefDoctorID"] = "";
            ViewState["TransDate"] = "";
            ViewState["TransType"] = "";
            ViewState["ServiceID"] = "";

            BuildServiceList(); ;
            BindTempServiceList(); ;
        }

        void New()
        {
            ViewState["NewFlag"] = true;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtTransID.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "LABREG");
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "LAB_REG";
            objCom.ScreenName = "Lab Register Waiting";
            objCom.ScreenType = "TRANSACTION";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='LAB_REG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnAuthorize.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                //btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Laboratory");
            }
        }


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Lab Test Register Page");

                try
                {
                    ViewState["NewFlag"] = true;
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    //txtDeliveryDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    txtTransID.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "LABREG");


                    // BindReportType();

                    BindNationality();
                    BindDoctor();
                    BinRefdDoctor();

                    BuildServiceList();

                    ViewState["TransID"] = Request.QueryString["TransID"];
                    ViewState["PatientID"] = Request.QueryString["PatientID"];
                    ViewState["RefDoctorID"] = Request.QueryString["DoctorID"];
                    ViewState["TransDate"] = Request.QueryString["TransDate"];
                    ViewState["TransType"] = Request.QueryString["TransType"];
                    ViewState["ServiceID"] = Request.QueryString["ServiceID"];


                    string PageName = (string)Request.QueryString["PageName"];

                    string strTestType = "";

                    strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);

                    if (PageName == "LabWaitList")
                    {
                        txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);
                        txtFileNo_TextChanged(txtFileNo, new EventArgs());


                        if (strTestType == "EMR")
                        {
                            txtEMRID.Text = Convert.ToString(ViewState["TransID"]);
                            BindEMRDtls();
                        }
                        else
                        {
                            txtInvoiceRefNo.Text = Convert.ToString(ViewState["TransID"]);
                            string EMR_ID = "0";
                            EMR_ID = GetEMRID();
                            txtEMRID.Text = EMR_ID;


                            for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                            {
                                if (drpRefDoctor.Items[intCount].Value == Convert.ToString(ViewState["RefDoctorID"]))
                                {
                                    drpRefDoctor.SelectedValue = Convert.ToString(ViewState["RefDoctorID"]);
                                    goto ForRefDoctors;
                                }
                            }
                        ForRefDoctors: ;

                        }


                        BindServiceList();
                        BindTempServiceList();
                        //BindInvProfileTrans();
                        //BindServiceTrans();
                        //BindTempTestSampleDtls();
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                PTDtlsClear();

                New();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtTransID_TextChanged(object sender, EventArgs e)
        {
            Clear();
            PTDtlsClear();
            BindLabTestRegtMaster();
            BindLabTestRegtDetail();
            BindTempServiceList();
            BindEMRDtls();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                Boolean isError = false;
                isError = fnSave();
                if (isError == false)
                {
                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Register Waiting, Register ID is " + txtTransID.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Lab Register Waiting, Register ID is " + txtTransID.Text);
                    }

                    // Clear();
                    // PTDtlsClear();
                    //New();
                    lblStatus.Text = "Data  Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }


            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvServiceList.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvServiceList.Rows)
                {
                    CheckBox chkSelService = (CheckBox)row.FindControl("chkSelService");
                    if (ChkBoxHeader.Checked == true)
                    {
                        chkSelService.Checked = true;
                    }
                    else
                    {
                        chkSelService.Checked = false;
                    }
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        #endregion
    }
}