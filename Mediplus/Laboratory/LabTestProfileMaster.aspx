﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="LabTestProfileMaster.aspx.cs" Inherits="HMS.Laboratory.LabTestProfileMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>



    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ServicePopup(CtrlName, strValue) {

            var win = window.open("../HMS/Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue + "&Type=L", "newwin", "top=200,left=270,height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();


            return true;

        }


        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName, Fees) {

            document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
            document.getElementById("<%=txtServName.ClientID%>").value = ServName;
            document.getElementById("<%=txtFee.ClientID%>").value = Fees;



        }


        function HaadShow(CtrlName, strValue) {
            var CodeType = '3';
            var ServCode = '';

            var win = window.open("../HMS/Masters/HaadServiceLookup.aspx?CtrlName=" + CtrlName + "&Value=" + strValue + "&CodeType=" + CodeType + "&CategoryType=All", "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }
        function BindHaadDtls(HaadCode, HaadName, CtrlName) {

            document.getElementById("<%=txtCPTCode.ClientID%>").value = HaadCode;
            document.getElementById("<%=txtCPTName.ClientID%>").value = HaadName;

        }


        function ProfileMasterShow(CtrlName, strValue) {


            var win = window.open("LabTestProfileMasterPopup.aspx?PageName=LabProfileMaster&CtrlName=" + CtrlName + "&Value=" + strValue, "ProfileMasterShow", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />


    <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">
                <asp:Button ID="btnNew" runat="server" CssClass="button gray small" Text="New" OnClick="btnNew_Click" />
                &nbsp;
                 <asp:Label ID="lblPageHeader" runat="server" CssClass="Profile Master" Text="Profile Master"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpPanalSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanalSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 80%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <table width="100%" border="0" cellpadding="3" cellspacing="3">
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">CODE
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtProfileID" runat="server" Width="150px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtProfileID_TextChanged" ondblclick="return ProfileMasterShow('txtProfileID',this.value);"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="txtProfileID" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px; width: 120px;">DESCRIPTION
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" Width="300px" CssClass="TextBoxStyle" ondblclick="return ProfileMasterShow('txtDescription',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">STATUS
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" Style="width: 155px" class="TextBoxStyle">
                                <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                <asp:ListItem Value="I">InActive</asp:ListItem>

                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Sample Req.
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSampleReq" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Material
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMaterial" runat="server" Width="300px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Unit
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtUnit" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label4" runat="server" CssClass="label" Text="ServiceCode"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="TextBoxStyle" ondblclick="return ServicePopup('txtServCode',this.value);" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label5" runat="server" CssClass="label" Text="Name"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtServName" runat="server" Width="300px" CssClass="TextBoxStyle" ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label6" runat="server" CssClass="label" Text="Fee"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFee" runat="server" Width="153px" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label3" runat="server" CssClass="label" Text="CPT Code"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCPTCode" runat="server" Width="150px" CssClass="TextBoxStyle" ondblclick="return HaadShow('txtCPTCode',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">
                    <asp:Label ID="Label7" runat="server" CssClass="label" Text="Name"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCPTName" runat="server" Width="300px" CssClass="TextBoxStyle" ondblclick="return HaadShow('txtCPTName',this.value);"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:CheckBox ID="chkDisplayNormalValues" runat="server" CssClass="lblCaption1" Text="Display all Normal Values in Report"></asp:CheckBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td></td>
                <td></td>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Test Category
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpReportCategory" runat="server" CssClass="TextBoxStyle" Width="155px" BorderWidth="1px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Method
                </td>
                <td  >
                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMethod" runat="server" Width="100%" CssClass="TextBoxStyle"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtMethod" MinimumPrefixLength="1" ServiceMethod="GetMethod"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td></td>
                <td></td>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Test Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpTestType" runat="server" CssClass="TextBoxStyle" Width="155px" BorderWidth="1px">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 30px; width: 100px; vertical-align: top;">Additional Reference
                </td>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtAddReference" runat="server" Width="615px" Height="50px" CssClass="TextBoxStyle" TextMode="MultiLine"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
        </table>
    </div>
    <br />
    <table width="80%" border="0" cellpadding="3" cellspacing="3">
        <tr>
            <td class="lblCaption1">Range Code & Description
            </td>
            <td></td>

        </tr>
        <tr>
            <td width="620px">

                <asp:TextBox ID="txtRangeName" runat="server" Width="600px" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtRangeName" MinimumPrefixLength="1" ServiceMethod="GetRangeType"
                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                </asp:AutoCompleteExtender>

            </td>
            <td>

                <asp:Button ID="btnAdd" runat="server" CssClass="button red small" Text="Add" OnClick="btnAdd_Click" />

            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 80%; overflow: auto; height: 300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
        <asp:GridView ID="gvTestProfTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"  OnRowDataBound="gvTestProfTrans_RowDataBound"
            EnableModelValidation="True" Width="100%">
            <HeaderStyle CssClass="GridHeader_Blue" />
            <RowStyle CssClass="GridRow" />
            <AlternatingRowStyle CssClass="GridAlterRow" />
            <Columns>

                <asp:TemplateField HeaderText="Range Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton9" CssClass="lblCaption1" runat="server" OnClick="TestProfTransEdit_Click">
                            <asp:Label ID="lblgvTestProfTransProfID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_TEST_PROFILE_ID") %>' Visible="false"></asp:Label>

                            <asp:Label ID="lblgvTestProfTransNorType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_NORMAL_TYPE") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton10" CssClass="lblCaption1" runat="server" OnClick="TestProfTransEdit_Click">
                            <asp:Label ID="lblgvTestProfTransDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_RANGE_DESCRIPTION") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Range From" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelRangeFrom"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:TextBox ID="txtgvTestProfTransRangeFrom" CssClass="TextBoxStyle" Width="70px" Height="20px" Font-Size="11px" runat="server" style="text-align:center" Text='<%# Bind("LTPT_RANGE_VALUE_FROM") %>' AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtgvTestProfTransRangeFrom" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Range To" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelRangeTo"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:TextBox ID="txtgvTestProfTransRangeTo" CssClass="TextBoxStyle" Width="70px" Height="20px" Font-Size="11px" runat="server" style="text-align:center" Text='<%# Bind("LTPT_RANGE_VALUE_TO") %>' AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtgvTestProfTransRangeTo" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Normal Value" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel35"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:TextBox ID="txtgvTestProfTransNorRange" CssClass="TextBoxStyle" Width="150px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_NORMAL_RANGE") %>' AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtgvTestProfTransNorRange" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sex" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel15"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:Label ID="lblgvTestProfTransSex" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_SEX") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpgvTestProfTransSex" runat="server" CssClass="TextBoxStyle" Width="100px" BorderWidth="1px" AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged">
                                    <asp:ListItem Value="" Selected="true">--- Select ---</asp:ListItem>
                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                    <asp:ListItem Value="B">Both</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="drpgvTestProfTransSex" />
                            </Triggers>
                        </asp:UpdatePanel>


                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Age From" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel16"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:TextBox ID="txtgvTestProfTransAgeFrom" CssClass="TextBoxStyle" Width="50px" Height="20px" Font-Size="11px" style="text-align:center" runat="server" Text='<%# Bind("LTPT_AGE_FROM") %>' AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtgvTestProfTransAgeFrom" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel17"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:Label ID="lblgvTestProfTransAgeFromType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_AGE_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpgvTestProfTransAgeFromType" runat="server" CssClass="TextBoxStyle" Width="100px" AutoPostBack="true" OnTextChanged="gvTestProfTrans_SelectedIndexChanged">
                                    <asp:ListItem Value="" Selected="true">--- Select ---</asp:ListItem>
                                    <asp:ListItem Value="Years">Years</asp:ListItem>
                                    <asp:ListItem Value="Months">Months</asp:ListItem>
                                    <asp:ListItem Value="Days">Days</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="drpgvTestProfTransAgeFromType" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Age To" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel18"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:TextBox ID="txtgvTestProfTransAgeTo" CssClass="TextBoxStyle" Width="50px" Height="20px" Font-Size="11px" style="text-align:center" runat="server" Text='<%# Bind("LTPT_AGE_TO") %>' AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtgvTestProfTransAgeTo" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel19"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:Label ID="lblgvTestProfTransAgeToType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_AGETO_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpgvTestProfTransAgeToType" runat="server" CssClass="TextBoxStyle" Width="100px" AutoPostBack="true" OnTextChanged="gvTestProfTrans_SelectedIndexChanged">
                                    <asp:ListItem Value="" Selected="true">--- Select ---</asp:ListItem>
                                    <asp:ListItem Value="Years">Years</asp:ListItem>
                                    <asp:ListItem Value="Months">Months</asp:ListItem>
                                    <asp:ListItem Value="Days">Days</asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="drpgvTestProfTransAgeToType" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Range Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="70px">
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelRangeType"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:Label ID="lblgvTestProfTransRangeType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_RANGE_TYPE") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="drpgvTestProfTransRangeType" runat="server" CssClass="TextBoxStyle" Width="70px" AutoPostBack="true" OnTextChanged="gvTestProfTrans_SelectedIndexChanged">
                                    <asp:ListItem Value="" Selected="true">--- Select ---</asp:ListItem>
                                   <asp:ListItem Value="L" Text="Low"></asp:ListItem>
                                    <asp:ListItem Value="H" Text="High"></asp:ListItem>
                                   <asp:ListItem Value="N" Text="Normal"></asp:ListItem>
                                   <asp:ListItem Value="A" Text="Abnormal"></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="drpgvTestProfTransRangeType" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Abnormal Value" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" Visible="false" >
                    <ItemTemplate>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel341"
                            UpdateMode="Conditional" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:TextBox ID="txtgvTestProfTransAbnorRange" CssClass="TextBoxStyle" Width="50px" Height="20px" Font-Size="11px" runat="server" Text='<%# Bind("LTPT_ABNORMAL_RANGE") %>' AutoPostBack="true" OnTextChanged="gvTestProfTrans_TextChanged"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="txtgvTestProfTransAbnorRange" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                    <ItemTemplate>
                        <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                            OnClick="Delete_Click" />&nbsp;&nbsp;
                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
