﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="LabAuthorizationList.aspx.cs" Inherits="Mediplus.Laboratory.LabAuthorizationList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    
    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <table width="90%">
        <tr>
            <td style="text-align: left; width: 50%;" class="PageHeader">

                <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Authorization"></asp:Label>
            </td>
            <td style="text-align: right; width: 50%;"></td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>


    <div style="padding-top: 0px; width: 80%; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: solid;">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvAuthorized" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                                <asp:Label ID="lblReportID" runat="server"  Text='<%# Bind("AT_TRANSID") %>'    Visible="false"  ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AT_TRANSID" HeaderText="REPORT ID" />
                        <asp:BoundField DataField="AT_CREATED_USER_ID" HeaderText="CREATED" />
                        <asp:BoundField DataField="AT_CREATED_DATEDesc" HeaderText="CREATD ON" />
                        <asp:BoundField DataField="AT_MODIFIED_USER_ID" HeaderText="MODIFIED" />
                        <asp:BoundField DataField="AT_MODIFIED_DATEDesc" HeaderText="MODIFIED ON" />

                        <asp:BoundField DataField="AT_PRINTED_ONDesc" HeaderText="PRINTED ON" />
                        <asp:BoundField DataField="AT_REPRINTED_ONDesc" HeaderText="REPRINTED ON" />

                        <asp:BoundField DataField="AT_AUTHORISED_USER_ID" HeaderText="AUTHORIZED BY" />
                        <asp:BoundField DataField="AT_AUTHORISED_DATEDesc" HeaderText="AUTHORIZED ON" />
                         <asp:BoundField DataField="AT_AUTHORISATION_REQUIREDDesc" HeaderText="STATUS" />
                    </Columns>
                </asp:GridView>


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <table width="100%" border="0" cellpadding="3" cellspacing="3">

        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList id="drpStatus" runat="server" CssClass="TextBoxStyle" >
                            <asp:ListItem Text="--- All ---" Value="" Selected="True" > </asp:ListItem>
                            <asp:ListItem Text="Authorized" Value="N" ></asp:ListItem>
                            <asp:ListItem Text="Not Authorized" Value="Y" ></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnShow" runat="server" CssClass="button red small" Text="Show" OnClick="btnShow_Click" />

                        <asp:Button ID="btnAuthorize" runat="server" CssClass="button red small" Text="Authorize Selected" OnClick="btnAuthorize_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>

        </tr>

    </table>

</asp:Content>
