﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Laboratory
{
    public partial class SampleCollectionPopup : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindWaitngList()
        {

            string strTestType = "";

           // if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" || Convert.ToString(ViewState["PageName"]) == "SampleCollection")
            //{
                strTestType = Convert.ToString(Session["HSOM_LAB_LIST"]);
            //}
            //else
            //{
            //    strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);
            //}

            DataSet DS = new DataSet();

            objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (strTestType == "EMR")
            {
                Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
            }
            else
            {
                Criteria = " 1=1   ";
                //if (Convert.ToString(ViewState["PageName"]) == "LabWaitList")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}
                //else if (Convert.ToString(ViewState["PageName"]) == "SampleCollection" && drpWaitListStatus.SelectedValue == "Waiting")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_REGISTERED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}
                //else if (Convert.ToString(ViewState["PageName"]) == "SampleCollection" && drpWaitListStatus.SelectedValue == "Completed")
                //{
                //    Criteria += " AND HIT_LAB_STATUS='LAB_SAMPLE_COLLECTED'  ";
                //    Criteria += " AND HIT_SERV_TYPE='L'  ";
                //}
                //else if (Convert.ToString(ViewState["PageName"]) == "RadWaitList")
                //{

                //    Criteria += " AND HIT_SERV_TYPE='R'  ";
                //}

                Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTSM_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),LTSM_DATE,101),101) <= '" + strForToDate + "'";
                }
                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  LTSM_PATIENT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  LTSM_PATIENT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }

                if (drpWaitListStatus.SelectedValue == "Waiting")
                {
                    Criteria += " AND (LTRM_STATUS ='Processing' )";

                }
                else if (drpWaitListStatus.SelectedValue == "Completed")
                {
                    Criteria += " AND  LTSM_STATUS ='Completed' ";
                }

            }

            //string strServiceType = "";
            //if (Convert.ToString(ViewState["PageName"]) == "LabWaitList" || Convert.ToString(ViewState["PageName"]) == "SampleCollection")
            //{
            //    strServiceType = "L";
            //}
            //else
            //{
            //    strServiceType = "R";
            //}

            //if (Convert.ToString(ViewState["PageName"]) == "SampleCollection")
            //{
            //    DS = objLab.LabTestRegisterWaitingList(Criteria);
            //}
            //else
            //{
            DS = objLab.TestSampleMasterGet(Criteria);

            // }
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLabSamplePopup.DataSource = DS;
                gvLabSamplePopup.DataBind();
            }
            else
            {
                gvLabSamplePopup.DataBind();
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTransToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    ViewState["PageName"] = (string)Request.QueryString["PageName"];

                    lblHeader.Text = "Lab Waiting List";


                    BindWaitngList();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {


            try
            {
                BindWaitngList();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void LabSampleEdit_Click(object sender, EventArgs e)
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            Label lblgvLabSampleID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSampleID");

            Label lblgvLabSamplePTID = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSamplePTID");
            Label lblgvLabSampleBareCode = (Label)gvScanCard.Cells[0].FindControl("lblgvLabSampleBareCode");

            ViewState["lblgvLabSampleID"] = lblgvLabSampleID.Text;

            ViewState["PatientID"] = lblgvLabSamplePTID.Text;
            ViewState["LabSampleBareCode"] = lblgvLabSampleBareCode.Text;


        }

        protected void btnShowTestReport_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(ViewState["LabSampleID"]) == "" || Convert.ToString(ViewState["LabSampleID"]) == null)
            {
                goto FunEnd;
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "PassValue('LabTestReport','" + Convert.ToString(ViewState["PatientID"]) + "','" + Convert.ToString(ViewState["lblgvLabSampleID"]) + "','" + Convert.ToString(ViewState["LabSampleBareCode"]) + "')", true);



        FunEnd: ;

        }
    }
}