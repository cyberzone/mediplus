﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Data.SqlClient;
namespace Mediplus
{
    public partial class AppointmentBooking : System.Web.UI.Page
    {

        static Int32 AppointmentInterval, AppointmentStart, AppointmentEnd, HSOM_APPOINTMENT_DSPLY_FORMAT;
        static string BranchID, BranchName;

        #region Methods

        void TextErrorFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("MediplusLog.txt");


                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("AppointmentBooking.aspx_" + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();
            DS = dbo.fnGetFieldValue("*", "HMS_SYSTEM_OPTIONS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    AppointmentInterval = Convert.ToInt32(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    AppointmentInterval = 30;
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    AppointmentStart = Convert.ToInt32(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    AppointmentStart = 9;
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    AppointmentEnd = Convert.ToInt32(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    AppointmentEnd = 21;
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    HSOM_APPOINTMENT_DSPLY_FORMAT = Convert.ToInt32(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    HSOM_APPOINTMENT_DSPLY_FORMAT = 24;
                }


            }


        }


        void BindBranchDtls()
        {
            string Criteria = " 1=1 ";

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();
            DS = dbo.fnGetFieldValue("*", "HMS_BRANCH_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                BranchID = Convert.ToString(DS.Tables[0].Rows[0]["HBM_ID"]);


                BranchName = Convert.ToString(DS.Tables[0].Rows[0]["HBM_NAME"]);
            }
        }

        void BindTime()
        {

            int index = 0;

            DataTable dt = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            dt.Columns.Add(Code);
            dt.Columns.Add(Name);

            DataRow objrow;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {

                objrow = dt.NewRow();
                objrow["Code"] = Convert.ToInt32(AppointmentStart + index).ToString("D2");
                objrow["Name"] = Convert.ToInt32(AppointmentStart + index).ToString("D2");
                dt.Rows.Add(objrow);


                index = index + 1;

            }


            drpSTHour.DataSource = dt;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();

            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            dt = new DataTable();

            Code = new DataColumn();
            Code.ColumnName = "Code";

            Name = new DataColumn();
            Name.ColumnName = "Name";

            dt.Columns.Add(Code);
            dt.Columns.Add(Name);




            objrow = dt.NewRow();
            Code = new DataColumn();
            Code.ColumnName = "Code";

            Name = new DataColumn();
            Name.ColumnName = "Name";

            objrow["Code"] = "00";
            objrow["Name"] = "00";
            dt.Rows.Add(objrow);

            for (int j = AppointmentInterval; j < 60; j++)
            {
                objrow = dt.NewRow();
                objrow["Code"] = Convert.ToString(j - intCount);
                objrow["Name"] = Convert.ToString(j - intCount);
                dt.Rows.Add(objrow);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

            drpSTMin.DataSource = dt;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();
        }

        void BindDepartment()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' ";
            DS = objCom.fnGetFieldValue("DISTINCT HDM_DEP_NAME", "HMS_DEP_MASTER INNER JOIN HMS_STAFF_MASTER ON HDM_DEP_NAME = HSFM_DEPT_ID ", Criteria, "HDM_DEP_NAME");


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDepartment.DataSource = DS.Tables[0];
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
            }
            drpDepartment.Items.Insert(0, "--- Select ---");
            drpDepartment.Items[0].Value = "";
        }

        void BindDoctor()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = "HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS ='Present'";

            if (drpDepartment.SelectedIndex != 0)
            {
                Criteria += " AND HSFM_DEPT_ID='" + drpDepartment.SelectedValue + "'";

            }
            DS = objCom.fnGetFieldValue("HSFM_STAFF_ID,HSFM_FNAME", "HMS_STAFF_MASTER", Criteria, "HSFM_FNAME");


            if (DS.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = DS.Tables[0];
                drpDoctor.DataTextField = "HSFM_FNAME";
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataBind();
            }

            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "";

        }

        void BindSex()
        {

            DataTable dt = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            dt.Columns.Add(Code);
            dt.Columns.Add(Name);

            DataRow objrow;

            objrow = dt.NewRow();



            objrow = dt.NewRow();
            objrow["Code"] = "";
            objrow["Name"] = "--- Select ---";
            dt.Rows.Add(objrow);

            objrow = dt.NewRow();
            objrow["Code"] = "Male";
            objrow["Name"] = "Male";
            dt.Rows.Add(objrow);

            objrow = dt.NewRow();
            objrow["Code"] = "Female";
            objrow["Name"] = "Female";
            dt.Rows.Add(objrow);

            objrow = dt.NewRow();
            objrow["Code"] = "Not Specified";
            objrow["Name"] = "Not Specified";
            dt.Rows.Add(objrow);


            drpGender.DataSource = dt;
            drpGender.DataTextField = "Name";
            drpGender.DataValueField = "Code";
            drpGender.DataBind();


        }

        void BindPatientDtls(string SearchOption)
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = "1=1 ";



            if (txtFileNo.Text.Trim() != "" && SearchOption == "PTFileNo")
            {
                Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "'";
            }

            if (txtFName.Text.Trim() != "" && SearchOption == "PTName")
            {
                Criteria += " AND HPM_PT_FNAME =  '" + txtFName.Text.Trim() + "'";
            }



            if (txtMobileNo.Text.Trim() != "" && SearchOption == "PTMobileNo")
            {
                Criteria += " AND HPM_MOBILE = '" + txtMobileNo.Text.Trim() + "'";
            }




            DS = objCom.fnGetFieldValue("*", "HMS_PATIENT_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = "";
                txtFName.Text = "";
                txtMobileNo.Text = "";
                drpGender.SelectedIndex = 0;
                txtFileNo.Focus();

                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]);
                txtFName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_FNAME"]);
                txtMobileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]);
                drpGender.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]);


            }

            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('No Data',' Patient Details Not Available.','Brown')", true);

            }

        FunEnd: ;
        }

        Boolean CheckAppointment()
        {

            string Criteria = " 1=1 ";



            // Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";

            Criteria += " AND HAM_FILENUMBER !='" + txtFileNo.Text.Trim() + "'";




            DateTime dtStDate = DateTime.ParseExact(txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);


            DateTime dtFinDate = dtStDate.AddMinutes(AppointmentInterval);



            string strSelectedDate = dtStDate.ToString("dd/MM/yyyy");// System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            string strSTTime = strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
            string strFITime = strForStartDate + " " + dtFinDate.ToString("HH:mm") + ":00";


            // TextErrorFileWriting("Start Time  " + txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00");
            // TextErrorFileWriting("Fnish Time " + txtFromDate.Text.Trim() + " " + dtFinDate.ToString("HH:mm") + ":00");


            //Criteria += "  AND( HAM_STARTTIME BETWEEN  '" + strSTTime + "' AND  '" + strFITime + "'";
            //Criteria += "  AND  HAM_FINISHTIME BETWEEN  '" + strSTTime + "'  AND '" + strFITime + "')";

            string strRawStDate = "";

            if (arrDate.Length > 1)
            {
                strRawStDate = arrDate[2] + arrDate[1] + arrDate[0];
            }


            Criteria += "  AND convert(varchar,HAM_FINISHTIME,112)+''+ replace(convert(VARCHAR(5),HAM_FINISHTIME,108),':','')  >  " + strRawStDate.Trim() + drpSTHour.SelectedValue + drpSTMin.SelectedValue;
            Criteria += " AND convert(varchar,HAM_STARTTIME,112)+''+ replace(convert(VARCHAR(5),HAM_STARTTIME,108),':','')  <  " + strRawStDate.Trim() + dtFinDate.ToString("HH") + dtFinDate.ToString("mm");


            CommonBAL objCom = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = objCom.fnGetFieldValue(" * ", "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria, "");

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;
        }

        void Clear()
        {
            txtFileNo.Text = "";
            txtFName.Text = "";
            txtMobileNo.Text = "";
            drpGender.SelectedIndex = 0;
            txtFileNo.Focus();


        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                txtFileNo.Focus();

                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");


                BindSystemOption();
                BindBranchDtls();
                BindTime();
                BindSex();
                BindDepartment();
                BindDoctor();
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //TextErrorFileWriting("btnSave_Click() 1");

                Boolean IsError = false;
                //if (txtFileNo.Text.Trim() == "")
                //{

                //    IsError = true;
                //}

                DateTime dtStDate = DateTime.ParseExact(txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);
                DateTime dtFinDate = dtStDate.AddMinutes(AppointmentInterval);


                DateTime dtCurrentDate = Convert.ToDateTime(System.DateTime.Now);

              //  TextErrorFileWriting("Start Time  " + dtStDate);
               // TextErrorFileWriting("Current Time  " + dtCurrentDate);

              //  TextErrorFileWriting("Value  " + DateTime.Compare(dtStDate, dtCurrentDate));

                if (DateTime.Compare(dtStDate, dtCurrentDate) == -1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Appointment  Date is Wrong',' Appointment  Date is Wrong ','Brown')", true);

                    goto FunEnd;
                }


                if (CheckAppointment() == true)
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Appointment  exist',' Appointment already exist for this Time','Brown')", true);


                    goto FunEnd;
                }


                //  goto SaveEnd;
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_AppointmentOutlookAdd";
                cmd.Parameters.Add(new SqlParameter("@HAM_BRANCH_ID", SqlDbType.VarChar)).Value = BranchID;
                cmd.Parameters.Add(new SqlParameter("@HAM_DR_CODE", SqlDbType.VarChar)).Value = drpDoctor.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_DR_NAME", SqlDbType.VarChar)).Value = drpDoctor.SelectedItem.Text;
                cmd.Parameters.Add(new SqlParameter("@HAM_PT_CODE", SqlDbType.VarChar)).Value = "Reg:No " + txtFileNo.Text.Trim();// + " Mobile: " + txtMobile.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_PT_NAME", SqlDbType.VarChar)).Value = txtFName.Text.Trim();

                //  DateTime dtStDate, dtFinDate;

                //  dtStDate = Convert.ToDateTime(txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00");

 



                cmd.Parameters.Add(new SqlParameter("@HAM_STARTTIME", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
                cmd.Parameters.Add(new SqlParameter("@HAM_FINISHTIME", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + dtFinDate.ToString("HH:mm") + ":00";// dtFinDate.ToString("dd/MM/yyyy HH:mm") + ":00";


                // TextErrorFileWriting("Start Time  " + txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00");
                // TextErrorFileWriting("Fnish Time " + txtFromDate.Text.Trim() + " " + dtFinDate.ToString("HH:mm") + ":00");



                cmd.Parameters.Add(new SqlParameter("@HAM_REMARKS", SqlDbType.VarChar)).Value = "Appointment Created by Patient";
                cmd.Parameters.Add(new SqlParameter("@HAM_STATUS", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_LASTNAME", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_FILENUMBER", SqlDbType.VarChar)).Value = txtFileNo.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_MOBILENO", SqlDbType.VarChar)).Value = txtMobileNo.Text;


                cmd.Parameters.Add(new SqlParameter("@HAM_SERVICE", SqlDbType.VarChar)).Value = "";

                cmd.Parameters.Add(new SqlParameter("@HAM_EMAIL", SqlDbType.VarChar)).Value = "";



                cmd.Parameters.Add(new SqlParameter("@HAM_OTAPPOINTMENT", SqlDbType.Bit)).Value = 0;
                cmd.Parameters.Add(new SqlParameter("@HSAM_PROCEDURE_CODE", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_HSAM_PROCEDURE_DESC", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_SURGERYTYPE", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_ANESTHESIA_TYPE", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_ANESTHETIST_NAME", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_SPECIAL_REQUEST", SqlDbType.VarChar)).Value = "";


                cmd.Parameters.Add(new SqlParameter("@HAM_TIME_IN", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + "00:00:00";



                cmd.Parameters.Add(new SqlParameter("@HAM_TIME_OUT", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + "00:00:00";



                cmd.Parameters.Add(new SqlParameter("@HAM_APPOINTMENTID", SqlDbType.Int)).Value = 0;
                cmd.Parameters.Add(new SqlParameter("@MODE", SqlDbType.Char)).Value = "A";


                cmd.Parameters.Add(new SqlParameter("@HAM_PT_CATEGORY", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_APP_PT_TYPE", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HAM_SEX", SqlDbType.VarChar)).Value = drpGender.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_ROOM", SqlDbType.VarChar)).Value = "";

                cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_CODE", SqlDbType.VarChar)).Value = "";
                cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_NAME", SqlDbType.VarChar)).Value = "";

                cmd.Parameters.Add(new SqlParameter("@HAM_CREATEDUSER", SqlDbType.VarChar)).Value = "Patient";
                cmd.ExecuteNonQuery();
                con.Close();

                Clear();
                // TextErrorFileWriting("btnSave_Click() 4");

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Appointment  Booked',' Appointment Booked...','Green')", true);



            FunEnd: ;
                if (IsError == true)
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('Please Enter Required Field',' Please Enter Required Field','Brown')", true);

                }


            }
            catch (Exception ex)
            {
                TextErrorFileWriting("-----------------------------------------------");
                TextErrorFileWriting(System.DateTime.Now.ToString() + "      btmBookAppointment_Click()");
                TextErrorFileWriting(ex.Message.ToString());

            }
        }

        protected void drpDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDoctor();
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BindPatientDtls("PTFileNo");
            }
            catch (Exception ex)
            {
                TextErrorFileWriting("-----------------------------------------------");
                TextErrorFileWriting(System.DateTime.Now.ToString() + "      imgSearch_Click()");
                TextErrorFileWriting(ex.Message.ToString());

            }
        }

        protected void imgSearchName_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BindPatientDtls("PTName");
            }
            catch (Exception ex)
            {
                TextErrorFileWriting("-----------------------------------------------");
                TextErrorFileWriting(System.DateTime.Now.ToString() + "      imgSearch_Click()");
                TextErrorFileWriting(ex.Message.ToString());

            }
        }

        protected void imgSearchMobileNo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BindPatientDtls("PTMobileNo");
            }
            catch (Exception ex)
            {
                TextErrorFileWriting("-----------------------------------------------");
                TextErrorFileWriting(System.DateTime.Now.ToString() + "      imgSearch_Click()");
                TextErrorFileWriting(ex.Message.ToString());

            }
        }
    }
}