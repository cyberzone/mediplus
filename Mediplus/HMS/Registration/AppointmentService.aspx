﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="AppointmentService.aspx.cs" Inherits="Mediplus.HMS.Registration.AppointmentService" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function PatientPopup1(AppId, strTime, DRId) {
            var strDate;
            strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
            var win = window.open("AppointmentSrvPopup.aspx?PageName=ApptSrv&AppId=" + AppId + "&Date=" + strDate + "&Time=" + strTime + "&DrId=" + DRId, "newwin", "top=200,left=270,height=450,width=750,toolbar=no,scrollbars=no,menubar=no");
            win.focus();
        }

        function NewAppointmentPopup() {
            var strDate;
            strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
            var win = window.open("AppointmentSrvPopup.aspx?PageName=ApptDay&AppId=0&Date=" + strDate + "&Time=0&DrId=0", "newwin", "top=200,left=270,height=450,width=750,toolbar=no,scrollbars=no,menubar=no");
            win.focus();
        }
    </script>

    <style type="text/css">
        .Header {
            font: bold 11px/100% Arial;
            background-color: #4fbdf0;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            border-color: black;
        }

        .RowHeader {
            font: bold 11px/100% Arial;
            height: 15px;
            width:100px;
            text-align: center;
            border-color: black;
        }

        .Content {
            font: 11px/100% Arial;
            text-decoration: none;
            color: black;
            text-align: center;
            vertical-align: middle;
        }
        .AppBox
        {
            width:300px;
        }

        .DrBox
        {
            font: bold 11px/100% Arial;
            background-color: #4fbdf0;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            border-color: black;
             width:305px;
        }

    </style>

    <script type="text/javascript">// <![CDATA[
        $(function () {
            /* move the table header from the datagrid outside, so it doesn't scroll" */
            $("#MyHeader").append($("#StudentData thead"));
            $("#MyDataRows").css("margin-bottom", "0");

            var ths = $("#MyHeader th");
            var tds = $("#MyDataRows tr:first td");

            /* Reset the padding of the th's to match the td's */
            ths.css("padding-left", tds.first().css("padding-left"));
            ths.css("padding-right", tds.first().css("padding-right"));

            /* Set the widths of all the th's (except the last to acccount for scroll bar) to the corresponding td width */
            for (var i = 0; i < tds.length - 1; i++) {
                ths.eq(i).width(tds.eq(i).width());
            }
        });
        // ]]></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table >
        <tr>
            <td class="PageHeader">Appointment - Service
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="80%">
        <tr>
            <td style="width: 170px;">
                <asp:HiddenField ID="hidStatus" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <contenttemplate>
                 <asp:Button ID="btnNewApp" runat="server" style="padding-left:5px;padding-right:5px;width:50px" Text="New" CssClass="button orange small"     OnClientClick="return NewAppointmentPopup()" />
                     </contenttemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 50px;" class="label">Date:
            </td>
            <td align="left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" height="22px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="red" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <asp:calendarextender id="TextBox1_CalendarExtender" runat="server" 
                    enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                </asp:calendarextender>
            </td>
            

        </tr>
    </table>
    
       
   <table style="float: left; width: 90%;" cellpadding="5" cellspacing="5" >
            <tr>
                <td>
                    <Table border='1' style='border-color:black;' >
                        
                        <%=strDataDr%>
                    </Table>

                </td>
            </tr>

        </table>
        
    <div style="padding-top: 0px; width: 90%; height: 600px;  border: thin; border-color: #CCCCCC; border-style: solid;overflow-x: scroll;
    overflow-y: scroll;">
        <table   cellpadding="5" cellspacing="5">
            <tr class="grid_header">
                <td>
                    <Table border='1' style='border-color:black;' >
                        
                        <%=strData%>
                    </Table>

                </td>
            </tr>

        </table>
     </div>
   
    <br />  <br />  <br />  <br />

    
</asp:Content>
