﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class AppointmentWeek : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindAppointment()
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(Session["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(Session["AppointmentEnd"]);


            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            dboperations dbo = new dboperations();




            //Criteria = " 1=1 AND HROS_TYPE='W' ";
            //DataSet DSRoster = new DataSet();
            //DSRoster = dbo.RosterMasterGet(Criteria);

            string strSelectedDate = hidPrevDate.Value; // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            // string strSelectedDate = txtFromDate.Text;  //System.DateTime.Now.ToString("dd/MM/yyyy");
            DateTime strTSelDate = Convert.ToDateTime(strForStartDate);// Convert.ToDateTime(System.DateTime.Now);

            string strday = strTSelDate.DayOfWeek.ToString();

            strData.Append("<Table border='1' style='border-color:black;width:100%;' ><tr>");
            strData.Append("<td   class='Header' >");
            //  strData.Append(strSelectedDate + "</br>" + strday);
            strData.Append("</td>");

            Int32 intday = 1;

            switch (strday.ToUpper())
            {
                case "SUNDAY":
                    intday = 1;
                    break;
                case "MONDAY":
                    intday = 2;
                    break;
                case "TUESDAY":
                    intday = 3;
                    break;
                case "WEDNESDAY":
                    intday = 4;
                    break;
                case "THURSDAY":
                    intday = 5;
                    break;
                case "FRIDAY":
                    intday = 6;
                    break;
                case "SATURDAY":
                    intday = 7;
                    break;
            }


            //style='font-family:Times New Roman,font-size:5;color:#2078c0;text-align:center'>

            for (Int32 i = 0; i <= 6; i++)
            {
                DateTime dt = strTSelDate.AddDays(i);

                //   hidPrevDate.Value = strTSelDate.AddDays(-intday + 0 + 1);
                // hidNextDate.Value = strTSelDate.AddDays(-intday + 0 + 1);

                strData.Append("<td   class='Header' >");
                strData.Append(dt.ToString("dd/MM/yyyy") + "</br>" + dt.DayOfWeek.ToString());
                strData.Append("</td>");
            }

            strData.Append("</tr>");



            //string strdata;
            //strdata = Convert.ToString(DSRoster.Tables[0].Rows[0]["HROS_DAY" + intday]);

            //string[] arrdata;

            //string strFrom1 = "";
            //string strTo1 = "";
            //string strFrom2 = "";
            //string strTo2 = "";

            //if (DSRoster.Tables[0].Rows.Count > 0)
            //{
            //    arrdata = strdata.Split('|');
            //    strFrom1 = arrdata[0];
            //    strFrom1 = strFrom1.Replace(":", "");
            //    strTo1 = arrdata[1];
            //    strTo1 = strTo1.Replace(":", "");
            //    strFrom2 = arrdata[2];
            //    strFrom2 = strFrom2.Replace(":", "");
            //    strTo2 = arrdata[3];
            //    strTo2 = strTo2.Replace(":", "");
            //}

            //Int32 intStart1 = 0;
            //intStart1 = Convert.ToInt32(strFrom1.Replace(":", ""));

            //Int32 intEnd1 = 0;
            //intEnd1 = Convert.ToInt32(strTo1.Replace(":", ""));

            //Int32 intStart2 = 0;
            //intStart2 = Convert.ToInt32(strFrom2.Replace(":", ""));

            //Int32 intEnd2 = 0;
            //intEnd2 = Convert.ToInt32(strTo2.Replace(":", ""));

            String strlTime;
            String strNewTime1 = "";


            //strrtime = strFrom1.Substring(strFrom1.Length - 2, 2);
            strlTime = Convert.ToString(AppointmentStart); //strFrom1.Substring(0, 2);
            strNewTime1 = "00";// strFrom1.Substring(strFrom1.Length - 2, 2);

            AppointmentStart = AppointmentStart * 100;
            AppointmentEnd = AppointmentEnd * 100;

            int[] arrCount = new int[7]; 

            for (Int32 i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                if (strNewTime1 == "")
                {
                    strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                }

                // i = i + AppointmentInterval;
                string[] arrlTime = strlTime.Split(':');
                if (arrlTime.Length > 1)
                {
                    strlTime = arrlTime[0];
                }

                if (Convert.ToInt32(strNewTime1) >= 60)
                {
                    strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                }
                else
                {
                    strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
                }

                i = Convert.ToInt32(strlTime.Replace(":", ""));


                if (Convert.ToInt32(strlTime.Replace(":", "")) <= AppointmentEnd && strlTime != "00:00")
                {
                    strData.Append("<tr>");
                    strData.Append("<td    class='RowHeader'>");
                    strData.Append(strlTime);
                    strData.Append("</td>");


                    for (int j = 0; j <= 6; j++)
                    {
                        if (arrCount[j] > 0)
                        {
                            arrCount[j] = arrCount[j] - 1;
                            goto ForEnd;
                        }

                        DateTime dt = strTSelDate.AddDays(j);
                        DataSet DSAppt = new DataSet();
                        DSAppt = GetAppointment(dt.ToString("dd/MM/yyyy"), strlTime);
                        if (DSAppt.Tables[0].Rows.Count > 0)
                        {


                            String strlTime2 = "";
                            Int32 AppointmentStart2 = 0, AppointmentEnd2 = 0;

                            string strStartTime = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]);
                            //string[] arrStartTime = strStartTime.Split(':');
                            //strStartTime = arrStartTime[0] +""+ arrStartTime[1];
                            strStartTime = strStartTime.Replace(":", "");


                            string strStartFnish = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]);
                            //string[] arrStartFnish =strStartFnish.Split(':');
                            //strStartFnish = arrStartFnish[0] + "" + arrStartFnish[1];
                            strStartFnish = strStartFnish.Replace(":", "");

                            AppointmentStart2 = Convert.ToInt32(strStartTime);
                            AppointmentEnd2 = Convert.ToInt32(strStartFnish);

                            strlTime2 = strStartTime.Substring(0, 2);
                            Int32 intRowCount = 0;
                            String strNewTime2 = Convert.ToString(AppointmentStart2);
                            strNewTime2 = strNewTime2.Substring(strNewTime2.Length - 2, 2);
                            for (Int32 k = AppointmentStart2; k <= AppointmentEnd2; k++)
                            {


                                if (strNewTime2 == "")
                                {
                                    strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
                                }

                                string[] arrlTime2 = strlTime2.Split(':');
                                if (arrlTime2.Length > 1)
                                {
                                    strlTime2 = arrlTime2[0];
                                }

                                if (Convert.ToInt32(strNewTime2) >= 60)
                                {
                                    strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                                }
                                else
                                {
                                    strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
                                }

                                k = Convert.ToInt32(strlTime2.Replace(":", ""));
                                strNewTime2 = "";

                                intRowCount = intRowCount + 1;

                            }
                            arrCount[j] = intRowCount - 2;

                            string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
                            strData.Append("<td    rowspan=" + Convert.ToString(intRowCount - 1) + "  style=' vertical-align:top;border-color:black;background-color: " + strColor + ";'>");
                            strData.Append("<a class='Content'   href='javascript:PatientPopup1(" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APPOINTMENTID"]) + ",0,0,0)'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"] + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"])) + "</a>");
                            strData.Append("</td>");
                        }
                        else
                        {
                            strData.Append("<td     style='border-color:black;background-color: white;'>");
                            strData.Append("<a style='text-decoration:none;width:100%;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(drpDoctor.SelectedValue + "','" + dt.ToString("dd/MM/yyyy")) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
                            strData.Append("</td>");

                        }
                ForEnd: ;
                    }

                    strData.Append("</tr>");
                }

                strNewTime1 = "";

            }


            //String strrtime2;
            //String strlTime2;
            //String strNewTime2;


            //strrtime2 = strFrom2.Substring(strFrom2.Length - 2, 2);
            //strlTime2 = strFrom2.Substring(0, 2);
            //strNewTime2 = strFrom2.Substring(strFrom2.Length - 2, 2);


            //for (Int32 i = intStart2; i <= intEnd2; i++)
            //{
            //    if (strNewTime2 == "")
            //    {
            //        strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
            //    }

            //    // i = i + AppointmentInterval;
            //    string[] arrlTime = strlTime2.Split(':');
            //    if (arrlTime.Length > 1)
            //    {
            //        strlTime2 = arrlTime[0];
            //    }

            //    if (Convert.ToInt32(strNewTime2) >= 60)
            //    {
            //        strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime2) - 60);
            //    }
            //    else
            //    {
            //        strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
            //    }

            //    i = Convert.ToInt32(strlTime2.Replace(":", ""));


            //    if (Convert.ToInt32(strlTime2.Replace(":", "")) <= Convert.ToInt32(strTo2.Replace(":", "")) && strlTime2 != "00:00")
            //    {
            //        strData.Append("<tr>");
            //        strData.Append("<td   class='RowHeader'>");
            //        strData.Append(strlTime2);
            //        strData.Append("</td>");

            //        for (int j = 0; j <= 6; j++)
            //        {

            //            DateTime dt = strTSelDate.AddDays(j);
            //            DataSet DSAppt = new DataSet();
            //            DSAppt = GetAppointment(dt.ToString("dd/MM/yyyy"), strlTime2);
            //            if (DSAppt.Tables[0].Rows.Count > 0)
            //            {
            //                string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
            //                strData.Append("<td   style=' vertical-align:top;background-color: " + strColor + ";'>");
            //                strData.Append("<a class='Content'   href='javascript:PatientPopup1(" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APPOINTMENTID"]) + ",0,0,0)'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"] + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"])) + "</a>");
            //                strData.Append("</td>");
            //            }
            //            else
            //            {
            //                strData.Append("<td    style='background-color: white;'>");
            //                strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(drpDoctor.SelectedValue + "','" + dt.ToString("dd/MM/yyyy")) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
            //                strData.Append("</td>");

            //            }

            //        }


            //        strData.Append("</tr>");
            //    }

            //    strNewTime2 = "";

           // }









        }

        DataSet GetAppointment(string strDate, string FromTime)
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            string strlTime;
            strlTime = FromTime;

            string strNewTime1 = "";
            if (strNewTime1 == "")
            {
                strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
            }

            // i = i + AppointmentInterval;


            string[] arrlTime = strlTime.Split(':');
            if (arrlTime.Length > 1)
            {
                strlTime = arrlTime[0];
            }

            if (Convert.ToInt32(strNewTime1) >= 60)
            {
                strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
            }
            else
            {
                strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
            }

            string Criteria = " 1=1 ";

            if (radAppStatus.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_STATUS IN (" + hidStatus.Value + ")";
            }
            else
            {
                Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";
            }
            string strSelectedDate = strDate; // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += " AND HAM_DR_CODE='" + drpDoctor.SelectedValue + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            Criteria += " AND   HAM_STARTTIME = '" + strForStartDate + " " + FromTime + ":00'";

            //Criteria += "  AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00' AND   HAM_STARTTIME <= '" + strForStartDate + " " + FromTime + ":00'";
            //Criteria += "  AND HAM_FINISHTIME >= '" + strForStartDate + " " + strlTime + ":00' AND   HAM_FINISHTIME <= '" + strForStartDate + " " + strlTime + ":00'";






            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            string[] arrAppDtls = { " " };

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                //    arrAppDtls[0] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_APPOINTMENTID"]);
                //    arrAppDtls[1] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_FILENUMBER"]);
                //    arrAppDtls[2] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_PT_NAME"]);


            }


            return DSAppt;
        }

        void GetAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1 ";

            if (radAppStatus.SelectedIndex != 0)
            {
                Criteria += " AND HAS_STATUS='" + radAppStatus.SelectedValue + "'";
            }
            else
            {
                Criteria += " AND HAS_STATUS='" + radAppStatus.Items[1].Value + "' OR  HAS_STATUS='" + radAppStatus.Items[2].Value + "'";
            }
            DS = dbo.AppointmentStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }




        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

               




            }


            if (strPermission == "7")
            {

               



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    {
                        //  SetPermission();
                    }

                    DateTime strTSelDate = Convert.ToDateTime(System.DateTime.Now);
                    Int32 intday = Convert.ToInt32(strTSelDate.DayOfWeek);

                    hidPrevDate.Value = Convert.ToString(strTSelDate.AddDays(-intday + 0).ToString("dd/MM/yyyy"));
                    hidNextDate.Value = Convert.ToString(strTSelDate.AddDays(-intday + 6).ToString("dd/MM/yyyy"));

                    ViewState["DrId"] = "0";
                    ViewState["DrId"] = Convert.ToString(Request.QueryString["DrID"]);
                    string strDate = "";
                    strDate = Convert.ToString(Request.QueryString["Date"]);

                    BindDoctor();
                    if (ViewState["DrId"] != "0" && ViewState["DrId"] != "")
                    {
                        drpDoctor.SelectedValue = Convert.ToString(ViewState["DrId"]);
                    }
                    if (strDate != "" && strDate != null)
                    {
                        string strSelectedDate = strDate; // System.DateTime.Now.ToString("dd/MM/yyyy");

                        string strStartDate = strSelectedDate;
                        string[] arrDate = strStartDate.Split('/');
                        string strForStartDate = "";

                        if (arrDate.Length > 1)
                        {
                            strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                        }


                        DateTime strTSelDate1 = Convert.ToDateTime(strForStartDate);
                        Int32 intday1 = Convert.ToInt32(strTSelDate1.DayOfWeek);

                        hidPrevDate.Value = Convert.ToString(strTSelDate1.AddDays(-intday1 + 0).ToString("dd/MM/yyyy"));
                        hidNextDate.Value = Convert.ToString(strTSelDate1.AddDays(-intday1 + 6).ToString("dd/MM/yyyy"));
                    }

                    GetAppointmentStatus();
                    BindAppointment();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentWeek.aspx_Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void radAppStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppointmentStatus();

                BindAppointment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentWeek.aspx_radAppStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppointmentStatus();
                BindAppointment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentWeek.aspx_drpDoctor_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            try
            {
                string strSelectedDate = hidPrevDate.Value; // System.DateTime.Now.ToString("dd/MM/yyyy");
                string strStartDate = strSelectedDate;
                string[] arrDate = strStartDate.Split('/');
                string strForStartDate = "";

                if (arrDate.Length > 1)
                {
                    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }


                DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
                Int32 intday = Convert.ToInt32(strTSelDate.DayOfWeek);

                hidPrevDate.Value = Convert.ToString(strTSelDate.AddDays(-7).ToString("dd/MM/yyyy"));
                hidNextDate.Value = Convert.ToString(strTSelDate.AddDays(-1).ToString("dd/MM/yyyy"));
                GetAppointmentStatus();
                BindAppointment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentWeek.aspx_btnPrev_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                
                string strSelectedDate = hidNextDate.Value; // System.DateTime.Now.ToString("dd/MM/yyyy");
                string strStartDate = strSelectedDate;
                string[] arrDate = strStartDate.Split('/');
                string strForStartDate = "";

                if (arrDate.Length > 1)
                {
                    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }


                DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
                Int32 intday = Convert.ToInt32(strTSelDate.DayOfWeek);


                hidPrevDate.Value = Convert.ToString(strTSelDate.AddDays(+1).ToString("dd/MM/yyyy"));
                hidNextDate.Value = Convert.ToString(strTSelDate.AddDays(+7).ToString("dd/MM/yyyy"));
                GetAppointmentStatus();
                BindAppointment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentWeek.aspx_btnNext_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}