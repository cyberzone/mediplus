﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus.HMS.Registration
{
    public partial class TreatmentAppointmentSearch : System.Web.UI.Page
    {

        dboperations dbo = new dboperations();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDoctorDtls()
        {
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSrcDoctor.DataSource = DS;
                drpSrcDoctor.DataTextField = "FullName";
                drpSrcDoctor.DataValueField = "HSFM_STAFF_ID";
                drpSrcDoctor.DataBind();
            }
            drpSrcDoctor.Items.Insert(0, "--- All ---");
            drpSrcDoctor.Items[0].Value = "0";

        }

        void BindPackageMaster()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPSm_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue(" * ", "HMS_PACKAGE_SERVICE_MASTER", Criteria, "HPSM_PACKAGE_NAME");
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSrcPackages.DataSource = DS;
                drpSrcPackages.DataTextField = "HPSM_PACKAGE_NAME";
                drpSrcPackages.DataValueField = "HPSM_PACKAGE_ID";
                drpSrcPackages.DataBind();
            }
            drpSrcPackages.Items.Insert(0, "--- All ---");
            drpSrcPackages.Items[0].Value = "0";


        }

        void BindTreatmentTrans()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1";

            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }



            if (txtSrcFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HTAT_DATE,101),101) >= '" + strForStartDate + "'";
            }




            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtSrcToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HTAT_DATE,101),101) <= '" + strForToDate + "'";
            }



            if (txtSrcFileNo.Text.Trim() != "")
            {
                Criteria += " AND HTAM_PT_ID = '" + txtSrcFileNo.Text.Trim() + "'";
            }

            if (txtSrcFileName.Text.Trim() != "")
            {
                Criteria += " AND   HTAM_PT_NAME LIKE '%" + txtSrcFileName.Text.Trim() + "%'";
            }



            if (drpSrcDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HTAT_DR_CODE='" + drpSrcDoctor.SelectedValue + "'";
            }



            if (txtInvoiceNo.Text.Trim() != "")
            {
                Criteria += " AND HTAM_INVOICE_ID = '" + txtInvoiceNo.Text.Trim() + "'";
            }


            if (drpSrcTreatTranStatus.SelectedIndex != 0)
            {
                Criteria += " AND HTAT_STATUS ='" + drpSrcTreatTranStatus.SelectedValue + "'";
            }

            if (drpSrcPackages.SelectedIndex != 0)
            {
                Criteria += " AND HTAT_PACKAGE_ID ='" + drpSrcPackages.SelectedValue + "'";
            }


            DS = objCom.fnGetFieldValue("*,CONVERT(VARCHAR,HTAT_DATE,103) AS HTAT_DATEDesc,CONVERT(VARCHAR,HTAT_DATE,108) AS HTAT_DATETimeDesc", "HMS_TREATMENT_APPOINTMENT_MASTER INNER JOIN HMS_TREATMENT_APPOINTMENT_TRANS ON HTAM_TREATMENT_ID = HTAT_TREATMENT_ID", Criteria, "HTAT_TREATMENT_ID Desc,HTAT_PACKAGE_ID");


            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTreatmentTrans.Visible = true;
                gvTreatmentTrans.DataSource = DV;
                gvTreatmentTrans.DataBind();

            }
            else
            {
                gvTreatmentTrans.DataBind();
                gvTreatmentTrans.Visible = false;
            }
            lblTotalApp.Text = "0";
            lblTotalOpen.Text = "0";
            lblTotalCompleted.Text = "0";
            lblTotalInProcess.Text = "0";

            Int32 intTotalWaiting = 0, intTotalCompleted = 0, intTotalInProcess = 0;
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblTotalApp.Text = Convert.ToString(DS.Tables[0].Rows.Count);

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["HTAT_STATUS"]).ToUpper() == "OPEN" || DR.IsNull("HTAT_STATUS") == true)
                    {
                        intTotalWaiting = intTotalWaiting + 1;
                    }

                    if (Convert.ToString(DR["HTAT_STATUS"]).ToUpper() == "COMPLETED")
                    {
                        intTotalCompleted = intTotalCompleted + 1;
                    }

                    if (Convert.ToString(DR["HTAT_STATUS"]).ToUpper() == "INPROCESS")
                    {
                        intTotalInProcess = intTotalInProcess + 1;
                    }
                }

            }
            lblTotalOpen.Text = Convert.ToString(intTotalWaiting);
            lblTotalCompleted.Text = Convert.ToString(intTotalCompleted);
            lblTotalInProcess.Text = Convert.ToString(intTotalInProcess);


        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                txtSrcFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtSrcToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                ViewState["SortOrder"] = "HTAT_TREATMENT_ID Desc";
                BindDoctorDtls();
                BindPackageMaster();
                BindTreatmentTrans();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                BindTreatmentTrans();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      btnSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvTreatmentTrans_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblgvTreatStatus = (Label)e.Row.FindControl("lblgvTreatStatus");
                    Label lblgvTreatSerialNo = (Label)e.Row.FindControl("lblgvTreatSerialNo");
                    DropDownList drpTreatTranStatus = (DropDownList)e.Row.FindControl("drpTreatTranStatus");

                    lblgvTreatSerialNo.Text = ((gvTreatmentTrans.PageIndex * gvTreatmentTrans.PageSize) + e.Row.RowIndex + 1).ToString();

                    drpTreatTranStatus.SelectedValue = lblgvTreatStatus.Text;
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      gvTreatmentTrans_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpTreatTranStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList btnEdit = new DropDownList();
                btnEdit = (DropDownList)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblgvTreatTransID = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatTransID");
                Label lblgvTreatTransSLNO = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatTransSLNO");
                Label lblgvTreatServCode = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatServCode");

                DropDownList drpTreatTranStatus = (DropDownList)gvScanCard.Cells[0].FindControl("drpTreatTranStatus");

                CommonBAL objCom = new CommonBAL();

                string Criteria = "HTAT_TREATMENT_ID='" + lblgvTreatTransID.Text + "' AND HTAT_SL_NO=" + lblgvTreatTransSLNO.Text + " AND HTAT_SERV_CODE='" + lblgvTreatServCode.Text.Trim() + "'";

                string FieldNameWithValues = " HTAT_STATUS='" + drpTreatTranStatus.SelectedValue + "'";
                //if (drpTreatTranStatus.SelectedValue == "")
                //{
                //    FieldNameWithValues += ", HTAT_COMPLETED_DATE= getdate()";
                //}
                //else
                //{
                //    FieldNameWithValues += ", HTAT_COMPLETED_DATE = NULL";
                //}

                objCom.fnUpdateTableData( FieldNameWithValues  , "HMS_TREATMENT_APPOINTMENT_TRANS", Criteria);

                BindTreatmentTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      drpTreatTranStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvTreatmentTrans_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindTreatmentTrans();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      gvTreatmentTrans_Sorting");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}