﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="TreatmentAppointment.aspx.cs" Inherits="Mediplus.HMS.Registration.TreatmentAppointment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>



    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ValSave() {

            if (document.getElementById('<%=txtTreatmentID.ClientID%>').value == "") {
                ShowErrorMessage('Treatment ID', 'Please enter Treatment ID')
                return false;
            }

            if (document.getElementById('<%=txtTreatmentDate.ClientID%>').value == "") {
                ShowErrorMessage('Treatment Date.', 'Please enter Treatment Date')
                return false;
            }


            if (document.getElementById('<%=txtInvoiceNo.ClientID%>').value == "") {
                ShowErrorMessage('Invoice No.', 'Please enter Invoice No.')
                return false;
            }

            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                ShowErrorMessage('File No.', 'Please enter File No.')
                return false;
            }

            if (document.getElementById('<%=txtDoctorID.ClientID%>').value == "") {
                ShowErrorMessage('Doctor Dtls.', 'Please enter Doctor Dtls.')
                return false;
            }
            return true;

        }

        function ShowErrorMessage(vMessage1, vMessage2) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
            document.getElementById("divMessage").style.display = 'block';

        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }


        function ShowTreatmentDetailsPopup() {

            document.getElementById("divTreatmentDetails").style.display = 'block';


        }

        function HideTreatmentDetailsPopup() {

            document.getElementById("divTreatmentDetails").style.display = 'none';

        }

        function ShowInvoicePopup() {

            document.getElementById("divInvoice").style.display = 'block';


        }

        function HideInvoicePopup() {

            document.getElementById("divInvoice").style.display = 'none';

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 400px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="padding-top: 0px; width: 85%; height: 700px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 0px;">
        <table style="width: 100%">
            <tr>
                <td align="left">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Treatment Appointment"></asp:Label>
                            <asp:ImageButton ID="imgTreatmentZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Treatment Entry" OnClick="imgTreatmentZoom_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="70px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return window.confirm('Do you want to Delete this data?')" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="70px" OnClick="btnSave_Click" Text="Save" OnClientClick="return ValSave();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

        <table runat="server" id="tblEnqCtrls" cellpadding="5" cellspacing="5" style="width: 100%">
            <tr>
                <td class="lblCaption1" style="width: 100px;">Treatment ID <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTreatmentID" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px" AutoPostBack="true" OnTextChanged="txtTreatmentID_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Treatment Date <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTreatmentDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                Enabled="True" TargetControlID="txtTreatmentDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="width: 150px;">
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            Invoice No. <span style="color: red; font-size: 11px;">*</span>
                            <asp:ImageButton ID="imgInvoiceZoom" runat="server" ImageUrl="~/Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Treatment Entry" OnClick="imgInvoiceZoom_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 150px;">File No. <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFileNo" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 150px;">Doctor <span style="color: red; font-size: 11px;">*</span>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDoctorID" runat="server" CssClass="TextBoxStyle" Width="90%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDoctorName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnInportInvServ" runat="server" CssClass="button red small" Text="Import Invoice Serv." OnClick="btnInportInvServ_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <table width="100%" border="0" cellpadding="3" cellspacing="3">
            <tr>
                <td class="lblCaption1" style="width: 30%">Date & Time
                </td>
                <td class="lblCaption1" style="width: 50%">Staff </td>
                <td class="lblCaption1" style="width: 20%">Status </td>
                <td style="width: 10%"></td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTreatTransDate" runat="server" Width="70px" Height="20px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtTreatTransDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:DropDownList ID="drpTreatTransHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px" Height="20px"></asp:DropDownList>
                            <asp:DropDownList ID="drpTreatTransMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px" Height="20px"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtStaffName" runat="server" Width="90%" Style="height: 20px;" CssClass="TextBoxStyle"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtStaffName" MinimumPrefixLength="1" ServiceMethod="GetStaffDtls"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpTreatTranStatus" runat="server" CssClass="TextBoxStyle" Width="100%">
                                <asp:ListItem Text="Open" Value="Open" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inprocess" Value="Inprocess"></asp:ListItem>
                                <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                            </asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnAdd" runat="server" CssClass="button red small" Text="Add" OnClick="btnAdd_Click" OnClientClick="return ValTransAdd();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 100%; overflow: auto; height: 500px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvTreatmentTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />

                        <Columns>

                            <asp:TemplateField HeaderText="DATE & TIME" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvTreatTransID" CssClass="lblCaption1" runat="server" OnClick="TreatmentTransEdit_Click">
                                        <asp:Label ID="lblgvTreatTransID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_TREATMENT_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="Label1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_SL_NO") %>' Visible="false"></asp:Label>

                                        <asp:Label ID="lblgvTreatDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_DATEDesc") %>'></asp:Label>
                                        &nbsp;

                                        <asp:Label ID="lblgvTreatTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_DATETimeDesc") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PACKAGE NAME" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvTreatPackageID" CssClass="lblCaption1" runat="server" OnClick="TreatmentTransEdit_Click">
                                        <asp:Label ID="lblgvTreatPackageID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_PACKAGE_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblgvTreatPackageName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_PACKAGE_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Service Coe & Name" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvTreatServCode" CssClass="lblCaption1" runat="server" OnClick="TreatmentTransEdit_Click">
                                        <asp:Label ID="lblgvTreatServCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_SERV_CODE") %>'></asp:Label>
                                        &nbsp; &nbsp;
                                        <asp:Label ID="lblgvTreatServName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_SERV_DESC") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="STAFF DTLS." HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvTreatDrCode" CssClass="lblCaption1" runat="server" OnClick="TreatmentTransEdit_Click">
                                        <asp:Label ID="lblgvTreatDrCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_DR_CODE") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblgvTreatDrName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_DR_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvTreatStatus" CssClass="lblCaption1" runat="server" OnClick="TreatmentTransEdit_Click">

                                        <asp:Label ID="lblgvTreatStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_STATUS") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:ImageButton ID="DeleteTrans" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                        OnClick="DeleteTrans_Click" />&nbsp;&nbsp;
                                </ItemTemplate>

                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="divTreatmentDetails" style="display: none; overflow: hidden; border: groove; height: 600px; width: 950px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 53px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="PageHeader" style="vertical-align: top; width: 50%;">Treatment Master List
                    </td>
                    <td align="right" style="vertical-align: top; width: 50%;">

                        <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideTreatmentDetailsPopup()" />
                    </td>
                </tr>

            </table>
            <div style="width: 100%; overflow: auto; height: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvTreatmentMaster" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Treatment ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvTreatMasID" CssClass="lblCaption1" runat="server" OnClick="TreatmentMasEdit_Click">
                                            <asp:Label ID="lblgvTreatMasID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAM_TREATMENT_ID") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvTreatMasDate" CssClass="lblCaption1" runat="server" OnClick="TreatmentMasEdit_Click">
                                            <asp:Label ID="lblgvTreatMasDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAM_DATEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Invoice ID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvTreatMasInvID" CssClass="lblCaption1" runat="server" OnClick="TreatmentMasEdit_Click">
                                            <asp:Label ID="lblgvTreatMasInvID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAM_INVOICE_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File No." HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvTreatMasFileNo" CssClass="lblCaption1" runat="server" OnClick="TreatmentMasEdit_Click">
                                            <asp:Label ID="lblgvTreatMasFileNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAM_PT_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvTreatMasName" CssClass="lblCaption1" runat="server" OnClick="TreatmentMasEdit_Click">
                                            <asp:Label ID="lblgvTreatMasName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAM_PT_NAME") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <div id="divInvoice" style="display: none; overflow: hidden; border: groove; height: 600px; width: 950px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 250px; top: 53px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="PageHeader" style="vertical-align: top; width: 50%;">Invoice List
                    </td>
                    <td align="right" style="vertical-align: top; width: 50%;">

                        <input type="button" id="Button2" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideInvoicePopup()" />
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>

                    <td class="lblCaption1">From Date  
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcInvFromDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                    Enabled="True" TargetControlID="txtSrcInvFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">To Date  
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcInvToDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                    Enabled="True" TargetControlID="txtSrcInvToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">File No.
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcInvFileNo" runat="server" Width="100%" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Name
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcInvFileName" runat="server" Width="100%" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnInvSearch" runat="server" Text="Search" CssClass="button red small" Width="100%" OnClick="btnInvSearch_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
            <div style="width: 100%; overflow: auto; height: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;">
                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvInvoice" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>

                                <asp:TemplateField HeaderText="Invoice ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvInvoiceID" CssClass="lblCaption1" runat="server" OnClick="InvoiceIDEdit_Click">
                                            <asp:Label ID="lblgvInvoiceID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvInvoiceDate" CssClass="lblCaption1" runat="server" OnClick="InvoiceIDEdit_Click">
                                            <asp:Label ID="lblgvInvoicDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HIM_DATEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="File No." HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvInvoicePtId" CssClass="lblCaption1" runat="server" OnClick="InvoiceIDEdit_Click">
                                            <asp:Label ID="lblgvInvoicePtId" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HIM_PT_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvInvoicePtName" CssClass="lblCaption1" runat="server" OnClick="InvoiceIDEdit_Click">
                                            <asp:Label ID="lblgvInvoicePtName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HIM_PT_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Doctor" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvInvoiceDrCode" CssClass="lblCaption1" runat="server" OnClick="InvoiceIDEdit_Click">
                                            <asp:Label ID="lblgvInvoiceDrCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HIM_DR_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblgvInvoiceDrName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HIM_DR_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

    </div>
    <br />
    <br />
    <br />

</asp:Content>
