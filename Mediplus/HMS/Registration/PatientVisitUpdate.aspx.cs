﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class PatientVisitUpdate : System.Web.UI.Page
    {
        # region Variable Declaration
        dboperations dbo = new dboperations();


        #endregion

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_SCANNING' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["EMR_SCAN_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }


        }


        void BindGrid()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "" && txtFromTime.Text == "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }
            else if (txtFromDate.Text != "" && txtFromTime.Text != "")
            {
                Criteria += " AND HPV_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";

            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }





            if (txtToDate.Text != "" && txtToTime.Text == "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }
            else if (txtToDate.Text != "" && txtToTime.Text != "")
            {
                Criteria += " AND HPV_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";

            }




            //Criteria += " AND  CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >='" + strForStartDate + "' ";
            //Criteria += "  AND CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <='" + txtToDate.Text + "'";




            if (txtSrcName.Text != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPV_PT_NAME like '" + txtSrcName.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPV_PT_NAME  like '%" + txtSrcName.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPV_PT_NAME  = '" + txtSrcName.Text + "' ";
                }

            }
            if (txtSrcFileNo.Text != "")
            {


                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPV_PT_ID like '" + txtSrcFileNo.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPV_PT_ID  like '%" + txtSrcFileNo.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPV_PT_ID   = '" + txtSrcFileNo.Text + "' ";
                }



            }
            if (drpSrcDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID='" + drpSrcDoctor.SelectedValue + "'";
            }


            if (Convert.ToString(ViewState["PatientID"]) != "" && Convert.ToString(ViewState["VisitID"]) !="" )
            {
                Criteria = " 1=1 ";
                Criteria += " AND HPV_PT_ID = '" + Convert.ToString(ViewState["PatientID"]) + "'";
                Criteria += " AND HPV_SEQNO = '" + Convert.ToString(ViewState["VisitID"]) + "'";

            }

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.PatientVisitGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DS;
                gvGridView.DataBind();
            }
            else
            {
                gvGridView.Visible = false;
            }

        }

        void BindModifyData()
        {
            txtPTName.Text = "";
            txtMobileNo.Text = "";
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    txtMobileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]).Trim();
                }
            }

        }

        void BIndDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcDoctor.DataSource = ds;
                drpSrcDoctor.DataValueField = "HSFM_STAFF_ID";
                drpSrcDoctor.DataTextField = "FullName";
                drpSrcDoctor.DataBind();


                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();

            }
            drpSrcDoctor.Items.Insert(0, "--- All ---");
            drpSrcDoctor.Items[0].Value = "0";

            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "0";

        }

        void BindCompany()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";

            //if (drpParentCompany.SelectedIndex != 0)
            //{
            //    Criteria += " AND  (  HCM_COMP_ID !=  HCM_BILL_CODE  AND    HCM_BILL_CODE ='" + drpParentCompany.SelectedValue + "'    OR  HCM_COMP_ID ='" + drpParentCompany.SelectedValue + "' )";
            //}
            if (drpPatientType.SelectedValue == "CR")
            {
                Criteria += " AND HCM_COMP_TYPE ='I' ";
            }
            else if (drpPatientType.SelectedValue == "CU")
            {
                Criteria += " AND HCM_COMP_TYPE ='N' ";
            }

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            drpCompany.Items.Clear();

            if (ds.Tables[0].Rows.Count > 0)
            {
                drpCompany.DataSource = ds;
                drpCompany.DataValueField = "HCM_COMP_ID";
                drpCompany.DataTextField = "HCM_NAME";
                drpCompany.DataBind();
            }


            drpCompany.Items.Insert(0, "--- Select ---");
            drpCompany.Items[0].Value = "0";

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='PTVISIT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                gvGridView.Enabled = false;

            }


            if (strPermission == "7")
            {
                gvGridView.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }

        void ClearSave()
        {
            txtFileNo.Text = "";
            txtPTName.Text = "";
            txtMobileNo.Text = "";
            txtVisitDate.Text = "";
            txtVisitTime.Text = "00:00";
            drpDoctor.SelectedIndex = 0;
            drpCompany.SelectedIndex = 0;
            drpVisitType.SelectedIndex = 0;
            drpPatientType.SelectedIndex = 0;
            hidSeqNo.Value = "";
            hidEMRId.Value = "";
            hidDrDept.Value = "";
            hidCreatedDate.Value = "";
            txtTokenNo.Text = "";

            ViewState["SelectIndex"] = "";
            ViewState["OldVisitDate"] = "";
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (hidLocalDate.Value != "")
            {
                Session["LocalDate"] = hidLocalDate.Value;
            }
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    //  SetPermission();
                }


                if (Convert.ToString(Session["User_ID"]).ToLower().IndexOf("admin", 0) == 0 || Convert.ToString(Session["User_ID"]).ToLower().IndexOf("super_admin", 0) == 0 || Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF")
                {
                    btnCreate.Visible = true;
                    btnUpdate.Visible = true;
                    btnDelete.Visible = true;

                }


                try
                {
                    ViewState["SeqNo"] = "";
                    ViewState["NetworkdIDs"] = "";

                    BindScreenCustomization();

                    BIndDoctor();
                    BindCompany();

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtVisitTime.Text = "00:00";

                    ViewState["PatientID"] = Request.QueryString["PatientID"];
                    ViewState["VisitID"] = Request.QueryString["VisitID"];

                    BindGrid();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }



        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {

                txtFileNo.Text = txtFileNo.Text.ToUpper();

                BindModifyData();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {

                ClearSave();

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;
                gvGridView.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");
                Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
                Label lblPTName = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
                Label lblMobile = (Label)gvScanCard.Cells[0].FindControl("lblMobile");
                Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
                Label lblDrDeptId = (Label)gvScanCard.Cells[0].FindControl("lblDrDeptId");
                Label lblVisitDate = (Label)gvScanCard.Cells[0].FindControl("lblVisitDate");
                Label lblVisitTime = (Label)gvScanCard.Cells[0].FindControl("lblVisitTime");
                Label lblPTType = (Label)gvScanCard.Cells[0].FindControl("lblPTType");
                Label lblVisitType = (Label)gvScanCard.Cells[0].FindControl("lblVisitType");
                Label lblCompanyID = (Label)gvScanCard.Cells[0].FindControl("lblCompanyID");
                Label lblEMRId = (Label)gvScanCard.Cells[0].FindControl("lblEMRId");
                Label lblCreatedDate = (Label)gvScanCard.Cells[0].FindControl("lblCreatedDate");
                Label lblTokenNo = (Label)gvScanCard.Cells[0].FindControl("lblTokenNo");


                hidSeqNo.Value = lblSeqNo.Text;
                txtFileNo.Text = lblPatientId.Text;
                txtPTName.Text = lblPTName.Text;
                txtMobileNo.Text = lblMobile.Text;
                txtVisitDate.Text = lblVisitDate.Text;

                ViewState["OldVisitDate"] = txtVisitDate.Text;


                string strVisitTime = lblVisitTime.Text.Trim();
                string[] arrVisitTime = strVisitTime.Split(' ');
                if (arrVisitTime.Length > 1)
                {
                    string strTimeType = arrVisitTime[1];

                    for (int i = 0; i < drpTime.Items.Count; i++)
                    {
                        if (drpTime.Items[i].Value == strTimeType)
                        {
                            drpTime.SelectedValue = strTimeType;
                        }

                    }
                }


                string[] arrVisitTime1 = strVisitTime.Split(':');
                if (arrVisitTime1.Length > 0)
                {
                    txtVisitTime.Text = arrVisitTime1[0] + ":" + arrVisitTime1[1];

                }

                drpDoctor.SelectedValue = lblDrID.Text;

                drpVisitType.SelectedValue = lblVisitType.Text;

                if (lblPTType.Text == "CA" || lblPTType.Text == "Cash")
                {
                    drpPatientType.SelectedIndex = 0;
                }
                else if (lblPTType.Text == "CR" || lblPTType.Text == "Credit")
                {
                    drpPatientType.SelectedIndex = 1;
                }
                else if (lblPTType.Text == "CU" || lblPTType.Text == "Customer")
                {
                    drpPatientType.SelectedIndex = 2;

                }
                drpPatientType_SelectedIndexChanged(drpPatientType, new EventArgs());


                if (lblCompanyID.Text != "" && lblCompanyID.Text != null)
                {
                    drpCompany.SelectedValue = lblCompanyID.Text;
                }



                txtTokenNo.Text = lblTokenNo.Text;


                hidEMRId.Value = lblEMRId.Text;
                hidDrDept.Value = lblDrDeptId.Text;

                hidOldDrId.Value = lblDrID.Text;
                hidOldVisitDt.Value = txtVisitDate.Text;

                hidCreatedDate.Value = lblCreatedDate.Text;

                //Response.Redirect("Patient_Registration.aspx?PatientId=" + lblPatientId.Text.Trim() + "&HPVSeqNo=" + lblSeqNo.Text.Trim());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnCreate_Click(object sender, EventArgs e)
        {

            try
            {
                dbOperation objDB = new dbOperation();

                IDictionary<string, string> Param = new Dictionary<string, string>();
                Param.Add("HPV_BRANCH_ID", Convert.ToString(Session["Branch_ID"]));
                Param.Add("HPV_PT_Id", txtFileNo.Text.Trim());
                Param.Add("HPV_PT_NAME", txtPTName.Text.Trim());
                Param.Add("HPV_DR_ID", drpDoctor.SelectedValue);
                Param.Add("HPV_DR_NAME", drpDoctor.SelectedItem.Text);
                Param.Add("HPV_DEP_NAME", hidDrDept.Value);
                Param.Add("HPV_DATE", txtVisitDate.Text.Trim());
                Param.Add("HPV_TIME", txtVisitTime.Text.Trim() + " " + drpTime.SelectedValue);
                if (drpCompany.SelectedIndex != 0)
                {
                    Param.Add("HPV_COMP_ID", drpCompany.SelectedValue);
                    Param.Add("HPV_COMP_NAME", drpCompany.SelectedItem.Text);
                }
                else
                {
                    Param.Add("HPV_COMP_ID", "");
                    Param.Add("HPV_COMP_NAME", "");
                }
                Param.Add("HPV_VISIT_TYPE", drpVisitType.SelectedValue);
                Param.Add("HPV_PT_TYPE", drpPatientType.SelectedValue);
                Param.Add("HPV_MOBILE", txtMobileNo.Text.Trim());
                Param.Add("UserID", Convert.ToString(Session["User_ID"]));


                // Param.Add("HPV_DELETED_USER", Convert.ToString(Session["User_ID"]));
                objDB.ExecuteNonQuery("HMS_SP_PatientVisitAdd", Param);



                //  dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "PatientHistory", "TRANSACTION", "Patient Visit Update, File Name is " + Convert.ToString(txtFileNo.Text) + " Dr id is " + drpDoctor.SelectedValue + " Visit Date is " + txtVisitDate.Text + " Seq No is " + hidSeqNo.Value + " EMR Id is " + hidEMRId.Value , Convert.ToString(Session["User_ID"]).Trim());


                ClearSave();

                lblStatus.Text = "Patient Visit Created";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                BindGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            try
            {

                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    lblStatus.Text = "Please select the visit";
                    goto FunEnd;
                }

                dbOperation objDB = new dbOperation();

                IDictionary<string, string> Param = new Dictionary<string, string>();
                Param.Add("HPV_SEQNO", hidSeqNo.Value);
                Param.Add("HPV_PT_Id", txtFileNo.Text.Trim());
                Param.Add("HPV_PT_NAME", txtPTName.Text.Trim());
                Param.Add("HPV_DR_ID", drpDoctor.SelectedValue);
                Param.Add("HPV_DR_NAME", drpDoctor.SelectedItem.Text);
                Param.Add("HPV_DEP_NAME", hidDrDept.Value);
                Param.Add("HPV_DATE", txtVisitDate.Text.Trim());
                Param.Add("HPV_TIME", txtVisitTime.Text.Trim() + " " + drpTime.SelectedValue);
                if (drpCompany.SelectedIndex != 0)
                {
                    Param.Add("HPV_COMP_ID", drpCompany.SelectedValue);
                    Param.Add("HPV_COMP_NAME", drpCompany.SelectedItem.Text);
                }
                else
                {
                    Param.Add("HPV_COMP_ID", "");
                    Param.Add("HPV_COMP_NAME", "");
                }
                Param.Add("HPV_VISIT_TYPE", drpVisitType.SelectedValue);
                Param.Add("HPV_PT_TYPE", drpPatientType.SelectedValue);
                Param.Add("HPV_MOBILE", txtMobileNo.Text.Trim());

                Param.Add("OldHPV_DR_ID", hidOldDrId.Value);
                Param.Add("OldHPV_DATE", hidOldVisitDt.Value);

                Param.Add("HPV_CREATED_DATE", hidCreatedDate.Value);

                Param.Add("HPV_DR_TOKEN", txtTokenNo.Text);

                // Param.Add("HPV_DELETED_USER", Convert.ToString(Session["User_ID"]));
                objDB.ExecuteNonQuery("HMS_SP_PatientVisitUpdate", Param);





                string FieldNameWithValues = "EPM_Date=convert(datetime,'" + txtVisitDate.Text.Trim() + "',103) ";
                FieldNameWithValues += ", EPM_START_DATE=convert(datetime,'" + txtVisitDate.Text.Trim() + "',103)";


                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = " + hidEMRId.Value + " AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "'";

                CommonBAL objCom = new CommonBAL();
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);

                Criteria += " AND EPM_STATUS='Y'";
                string FieldNameWithValues1 = "EPM_END_DATE=convert(datetime,'" + txtVisitDate.Text.Trim() + "',103) ";

                objCom.fnUpdateTableData(FieldNameWithValues1, "EMR_PT_MASTER", Criteria);

                /*

                if (Convert.ToString(ViewState["OldVisitDate"]) != txtVisitDate.Text.Trim())
                {

                    string SCanPath, FileNoFolder = "", OldVisitDateFolder = "", VisitDateFolder = "";

                    SCanPath = Convert.ToString(ViewState["EMR_SCAN_PATH"]);
                    FileNoFolder = txtFileNo.Text.Trim();


                    string strOldVisitDate = Convert.ToString(ViewState["OldVisitDate"]);
                    string[] arrOldVisitDate = strOldVisitDate.Split('/');
                    OldVisitDateFolder = arrOldVisitDate[0] + "-" + arrOldVisitDate[1] + "-" + arrOldVisitDate[2];



                    string strVisitDate = txtVisitDate.Text.Trim();
                    string[] arrDate = strVisitDate.Split('/');

                    VisitDateFolder = arrDate[0] + "-" + arrDate[1] + "-" + arrDate[2];



                    if (Directory.Exists(SCanPath + "\\" + FileNoFolder))
                    {
                        if (Directory.Exists(SCanPath + "\\" + FileNoFolder + "\\" + OldVisitDateFolder))
                        {
                            if (!Directory.Exists(SCanPath + "\\" + FileNoFolder + "\\" + VisitDateFolder))
                            {
                                Directory.CreateDirectory(SCanPath + "\\" + FileNoFolder + "\\" + VisitDateFolder);
                            }

                            Directory.Move(SCanPath + "\\" + FileNoFolder + "\\" + OldVisitDateFolder, SCanPath + "\\" + FileNoFolder + "\\" + VisitDateFolder);

                        }

                    }

                }
                */

                //  dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "PatientHistory", "TRANSACTION", "Patient Visit Update, File Name is " + Convert.ToString(txtFileNo.Text) + " Dr id is " + drpDoctor.SelectedValue + " Visit Date is " + txtVisitDate.Text + " Seq No is " + hidSeqNo.Value + " EMR Id is " + hidEMRId.Value , Convert.ToString(Session["User_ID"]).Trim());


                ClearSave();

                lblStatus.Text = "Patient Visit Updated";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                BindGrid();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        Boolean CheckPTDiagnosis()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND ISNULL(EPD_STATUS,'') <> 'D'   ";

            Criteria += " AND EPD_ID = '" + hidEMRId.Value + "'";

            DS = objCom.fnGetFieldValue(" TOP 1 *", "EMR_PT_DIAGNOSIS", Criteria, "EPD_TYPE");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;


        }

        Boolean CheckPTProcedures()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND ISNULL(EPP_STATUS,'') <> 'D'   ";
            Criteria += " AND EPP_ID = '" + hidEMRId.Value + "'";

            DS = objCom.fnGetFieldValue("  TOP 1 *", "EMR_PT_PROCEDURES", Criteria, "EPP_POSITION");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    lblStatus.Text = "Please select the visit";
                    goto FunEnd;
                }

                if (txtReason.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Deleted Reason";
                    goto FunEnd;
                }


                if (hidEMRId.Value != "")
                {
                    if (CheckPTDiagnosis() == true)
                    {

                        lblStatus.Text = "This visit having Diagnosis";
                        goto FunEnd;
                    }

                    if (CheckPTProcedures() == true)
                    {

                        lblStatus.Text = "This visit having Procedure";
                        goto FunEnd;
                    }
                }


                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {
                    Boolean IsError;


                    HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                    objHL7Mess.PT_ID = txtFileNo.Text;
                    objHL7Mess.VISIT_ID = hidSeqNo.Value;
                    objHL7Mess.EMR_ID = hidEMRId.Value;
                    objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                    objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);
                    objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);



                    /// objHL7Mess.MESSAGE_TYPE = "ADT";
                    /// 
                    ////objHL7Mess.MessageCode = "ADT-A11";
                    ////objHL7Mess.MessageDesc = "Cancel Admit/visit Notification";
                    ////objHL7Mess.MalaffiHL7MessageMasterAdd();



                    ////objHL7Mess.MessageCode = "ADT-A08";
                    ////objHL7Mess.MessageDesc = "Update Patient Encounter Information";
                    ////objHL7Mess.MalaffiHL7MessageMasterAdd();


                    IsError = objHL7Mess.CancelAdmit_visitMessageGenerate_ADT_A11();

                    // IsError = objHL7Mess.UpdatePatientEncounterInfoMessageGenerate_ADT_A08();

                }


                dbOperation objDB = new dbOperation();
                IDictionary<string, string> Param = new Dictionary<string, string>();
                Param.Add("HPV_SEQNO", hidSeqNo.Value);
                Param.Add("HPV_PT_Id", txtFileNo.Text);
                Param.Add("HPV_DR_ID", drpDoctor.SelectedValue);
                Param.Add("HPV_DATE", txtVisitDate.Text);
                Param.Add("HPV_DELETED_USER", Convert.ToString(Session["User_ID"]));
                Param.Add("HPV_CREATED_DATE", hidCreatedDate.Value);
                Param.Add("HPV_DELETED_REASON", txtReason.Text);
                objDB.ExecuteNonQuery("HMS_SP_PatientVisitDelete", Param);

                dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "PatientHistory", "TRANSACTION", "Patient Visit Delete, File Name is " + Convert.ToString(txtFileNo.Text) + " Dr id is " + drpDoctor.SelectedValue + " Visit Date is " + txtVisitDate.Text + " Seq No is " + hidSeqNo.Value + " EMR Id is " + hidEMRId.Value, Convert.ToString(Session["User_ID"]).Trim());




                BindGrid();
                ClearSave();
                lblStatus.Text = "Patient Visit Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearSave();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                ClearSave();
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataSet DS = new DataSet();
                string Criteria = " 1=1 ";
                Criteria += " and HSFM_STAFF_ID='" + drpDoctor.SelectedValue + "' ";
                DS = dbo.retun_doctor_detailss(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    hidDrDept.Value = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]).Trim();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisitUpdate.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpPatientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCompany();
        }

        #endregion


    }
}