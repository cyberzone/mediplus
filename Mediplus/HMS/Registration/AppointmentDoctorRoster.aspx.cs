﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class AppointmentDoctorRoster : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();
        public static string BEAUTICIANCODE = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["BEAUTICIANCODE"].ToString());
        public StringBuilder strDataDr = new StringBuilder();

        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName2";
                drpDoctor.DataBind();
            }


            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "0";

        }

        void BindAppDr()
        {
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string[] arrDrCode = BEAUTICIANCODE.Split(',');

            string strDrCode = "";
            for (int i = 0; i < arrDrCode.Length; i++)
            {
                strDrCode += "'" + arrDrCode[i] + "',";
            }
            if (strDrCode.Length > 1)
                strDrCode = strDrCode.Substring(0, strDrCode.Length - 1);

            Criteria += " AND HSFM_STAFF_ID NOT IN (" + strDrCode + ")";

            Criteria += " AND HSFM_STAFF_ID ='" + drpDoctor.SelectedValue + "'";
           

            dboperations dbo = new dboperations();

            DataSet DSDoctors = new DataSet();
            DSDoctors = dbo.retun_doctor_detailss(Criteria);

            strDataDr.Append("<tr ><td width='100px'  class='Header' >&nbsp;&nbsp;Time&nbsp;&nbsp;</td>");
            // strData.Append(strSelectedDate + "</br>" + strday);
            //  strData.Append("</td>");


            if (DSDoctors.Tables[0].Rows.Count > 0)
            {
                //style='font-family:Times New Roman,font-size:5;color:#2078c0;text-align:center'>

                for (Int32 i = 1; i <= 5; i++)
                {
                    string strHeader = "";
                    switch (i)
                    {
                        case 1:
                            strHeader = "One";
                            break;
                        case 2:
                            strHeader = "Two";
                            break;
                        case 3:
                            strHeader = "Three";
                            break;
                        case 4:
                            strHeader = "Four";
                            break;
                        case 5:
                            strHeader = "Five";
                            break;

                        default:
                            break;
                    }


                    strDataDr.Append("<td    class='DrBox' >");
                    strDataDr.Append(strHeader); //+ " | " + Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_STAFF_ID"]));
                    strDataDr.Append("</td>");
                }
            }
            strDataDr.Append("</tr>");
        }

        DataSet GetAppointment(string DRCode, string FromTime, string Priority)
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["HSFM_APNT_TIME_INT"]);
            string strlTime;
            strlTime = FromTime;

            string strNewTime1 = "";
            if (strNewTime1 == "")
            {
                strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
            }

            // i = i + AppointmentInterval;


            string[] arrlTime = strlTime.Split(':');
            if (arrlTime.Length > 1)
            {
                strlTime = arrlTime[0];
            }

            if (Convert.ToInt32(strNewTime1) >= 60)
            {
                strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
            }
            else
            {
                strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
            }

            string Criteria = " 1=1 ";

            //if (radAppStatus.SelectedIndex != 0)
            //{
            //    Criteria += "  AND HAM_STATUS IN ('" + hidStatus.Value + "')";
            //}
            //else
            //{
            //    Criteria += "  AND HAM_STATUS NOT IN ('" + hidStatus.Value + "')";
            //}


            Criteria += " AND HAM_STAFF_ID='" + DRCode + "'";

            Criteria += "   AND    CONVERT(DATETIME,HAM_APNT_DATE,103) = CONVERT(DATETIME,'" + txtFromDate.Text.Trim() + "',103) ";
            Criteria += "   AND   HAM_APNT_TIME = '" + FromTime + "'";
            Criteria += "   AND   HAM_PRIORITY = '" + Priority + "'";

            clsAppointmentMaster clsAppMas = new clsAppointmentMaster();
            DataSet DSAppt = new DataSet();

            DSAppt = clsAppMas.AppointmentMasterGet(Criteria);

            return DSAppt;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnNewApp.Enabled = false;




            }


            if (strPermission == "7")
            {

                btnNewApp.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }

        void BindAppointmentRoster(string pAppointmentStart, string pAppointmentEnd)
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["HSFM_APNT_TIME_INT"]);

            string strAppointmentStart = pAppointmentStart.Replace(":", "");
            string strAppointmentEnd = pAppointmentEnd.Replace(":", "");

            int AppointmentStart = Convert.ToInt32(strAppointmentStart);
            int AppointmentEnd = Convert.ToInt32(strAppointmentEnd);

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string[] arrDrCode = BEAUTICIANCODE.Split(',');

            string strDrCode = "";
            for (int i = 0; i < arrDrCode.Length; i++)
            {
                strDrCode += "'" + arrDrCode[i] + "',";
            }
            if (strDrCode.Length > 1)
                strDrCode = strDrCode.Substring(0, strDrCode.Length - 1);

            Criteria += " AND HSFM_STAFF_ID NOT IN (" + strDrCode + ")";

             Criteria += " AND HSFM_STAFF_ID  ='" + drpDoctor.SelectedValue + "'";
            


            dboperations dbo = new dboperations();

            DataSet DSDoctors = new DataSet();
            DSDoctors = dbo.retun_doctor_detailss(Criteria);


            //Criteria = " 1=1 AND HROS_TYPE='D' ";

            //DataSet DSRoster = new DataSet();
            //DSRoster = dbo.RosterMasterGet(Criteria);

            string strSelectedDate = txtFromDate.Text;  //System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }





            //strData.Append("<tr style='top:0;'><td width='100px'  class='Header' >&nbsp;&nbsp;Time&nbsp;&nbsp;</td>");


            //if (DSDoctors.Tables[0].Rows.Count > 0)
            //{

            //    for (Int32 i = 0; i <= DSDoctors.Tables[0].Rows.Count - 1; i++)
            //    {
            //        strData.Append("<td class='Header' >");
            //        strData.Append(Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_FNAME"]) + " | " + Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_STAFF_ID"]));
            //        strData.Append("</td>");
            //    }
            //}
            //strData.Append("</tr>");




            String strlTime;
            String strNewTime1 = "";


            //strrtime = strFrom1.Substring(strFrom1.Length - 2, 2);
            string[] strStMin = pAppointmentStart.Split(':');

            strlTime = strStMin[0];// Convert.ToString(AppointmentStart); //strFrom1.Substring(0, 2);


            strNewTime1 = strStMin[1]; //"00";// strFrom1.Substring(strFrom1.Length - 2, 2);

            // //// AppointmentStart = AppointmentStart * 100;
            //// // AppointmentEnd = AppointmentEnd * 100;

            int[] arrCount = new int[DSDoctors.Tables[0].Rows.Count];

            for (Int32 i = AppointmentStart; i <= AppointmentEnd; i++)
            {

                if (strNewTime1 == "")
                {
                    strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                }

                // i = i + AppointmentInterval;
                string[] arrlTime = strlTime.Split(':');
                if (arrlTime.Length > 1)
                {
                    strlTime = arrlTime[0];
                }

                if (Convert.ToInt32(strNewTime1) >= 60)
                {
                    strlTime = Convert.ToString(Convert.ToInt32(Convert.ToInt32(strlTime) + 1).ToString("D2")) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                }
                else
                {
                    strlTime = Convert.ToString(Convert.ToInt32(strlTime).ToString("D2") + ":" + strNewTime1);
                }

                i = Convert.ToInt32(strlTime.Replace(":", ""));

                Boolean isBolcked = false;
                isBolcked = BindCheckBlockedAppointment(i);

                if (Convert.ToInt32(strlTime.Replace(":", "")) <= AppointmentEnd && strlTime != "00:00")
                {
                    strData.Append("<tr>");
                    strData.Append("<td    class='RowHeader'>");
                    strData.Append(strlTime);
                    strData.Append("</td>");




                    for (int j = 1; j <= 5; j++)
                    {
                        //if (arrCount[j] > 0)
                        //{
                        //    arrCount[j] = arrCount[j] - 1;
                        //    goto ForEnd;
                        //}

                        DataSet DSAppt = new DataSet();
                        DSAppt = GetAppointment(drpDoctor.SelectedValue, strlTime, Convert.ToString(j));
                        if (DSAppt.Tables[0].Rows.Count > 0)
                        {
                            String strlTime2 = "";
                            Int32 AppointmentStart2 = 0, AppointmentEnd2 = 0;

                            string strStartTime = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TIME"]);
                            //string[] arrStartTime = strStartTime.Split(':');
                            //strStartTime = arrStartTime[0] +""+ arrStartTime[1];
                            strStartTime = strStartTime.Replace(":", "");


                            string strStartFnish = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TIME"]);
                            //string[] arrStartFnish =strStartFnish.Split(':');
                            //strStartFnish = arrStartFnish[0] + "" + arrStartFnish[1];
                            strStartFnish = strStartFnish.Replace(":", "");

                            AppointmentStart2 = Convert.ToInt32(strStartTime);
                            AppointmentEnd2 = Convert.ToInt32(strStartFnish);

                            strlTime2 = strStartTime.Substring(0, 2);
                            Int32 intRowCount = 0;
                            String strNewTime2 = Convert.ToString(AppointmentStart2);
                            strNewTime2 = strNewTime2.Substring(strNewTime2.Length - 2, 2);

                            for (Int32 k = AppointmentStart2; k <= AppointmentEnd2; k++)
                            {
                                if (strNewTime2 == "")
                                {
                                    strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
                                }

                                string[] arrlTime2 = strlTime2.Split(':');
                                if (arrlTime2.Length > 1)
                                {
                                    strlTime2 = arrlTime2[0];
                                }

                                if (Convert.ToInt32(strNewTime2) >= 60)
                                {
                                    strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                                }
                                else
                                {
                                    strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
                                }

                                k = Convert.ToInt32(strlTime2.Replace(":", ""));
                                strNewTime2 = "";

                                intRowCount = intRowCount + 1;

                            }

                            // arrCount[j] = intRowCount - 2;

                            string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_COLOR_CODE"]);
                            strData.Append("<td ondblclick=PatientPopup1('" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ID"]) + "','0','0','','" + Convert.ToString(j) + "')     height='50px' align='center' class='AppBox'   rowspan=" + Convert.ToString(intRowCount - 1) + " style='vertical-align:middle;border-color:black;background-color: " + strColor + ";'>");
                            strData.Append("<span class=lblCaption1>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILE_NO"]) + " : " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILE"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_TYPEOFSERVICE"]) + "</span>");
                            strData.Append("</td>");
                        }
                        else if (isBolcked == true)
                        {
                            strData.Append("<td ALIGN='CENTER'  height='50px'   class='AppBox' style='border-color:black;background-color: white;'>");
                            strData.Append("<span class=lblCaption1>BLOCKED</span>");

                            strData.Append("</td>");
                        }
                        else
                        {

                            strData.Append("<td ondblclick=PatientPopup1('0','" + strlTime + "','" + drpDoctor.SelectedValue + "','','" + Convert.ToString(j) + "')  height='50px'   class='AppBox' style='border-color:black;background-color: white;'>");
                            // strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
                            strData.Append("</td>");

                        }
                    ForEnd: ;
                    }




                    strData.Append("</tr>");
                }

                strNewTime1 = "";

            }


        }


        Boolean BindRosterMaster()
        {

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strDay = "";

            if (arrDate.Length > 1)
            {
                strDay = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
            }
            //DateTime dtAppointmentDate = Convert.ToDateTime(strDay);

            DateTime dtAppointmentDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            string s = dtAppointmentDate.DayOfWeek.ToString();
            string strweekNo = "1";

            switch (s.ToUpper())
            {
                case "SUNDAY":
                    strweekNo = "1";
                    break;
                case "MONDAY":
                    strweekNo = "2";
                    break;
                case "TUESDAY":
                    strweekNo = "3";
                    break;
                case "WEDNESDAY":
                    strweekNo = "4";
                    break;
                case "THURSDAY":
                    strweekNo = "5";
                    break;
                case "FRIDAY":
                    strweekNo = "6";
                    break;
                case "SATURDAY":
                    strweekNo = "7";
                    break;
                default:
                    break;
            }





            String Criteria = " 1=1  AND HROS_STATUS='A' ";
            Criteria += " AND HROS_ROSTER_ID='" + Convert.ToString(ViewState["RosterID"]) + "'";
            DataSet DSRoster = new DataSet();
            dboperations dbo = new dboperations();
            DSRoster = dbo.RosterMasterGet(Criteria);
            if (DSRoster.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(DSRoster.Tables[0].Rows[0]["HROS_TYPE"]) == "D")
                {
                    ViewState["AppointmentTiming"] = Convert.ToString(DSRoster.Tables[0].Rows[0]["HROS_DAY" + arrDate[0]]);
                }
                else
                {

                    ViewState["AppointmentTiming"] = Convert.ToString(DSRoster.Tables[0].Rows[0]["HROS_DAY" + strweekNo]);
                }
                return true;
            }
            return false;

        }

        void BindStaffData()
        {
            ViewState["HSFM_NO_APNT_DATEFROM1"] = "";
            ViewState["HSFM_NO_APNT_FROM1TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO1"] = "";
            ViewState["HSFM_NO_APNT_TO1TIME"] = 0;


            ViewState["HSFM_NO_APNT_DATEFROM2"] = "";
            ViewState["HSFM_NO_APNT_FROM2TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO2"] = "";
            ViewState["HSFM_NO_APNT_TO2TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATEFROM3"] = "";
            ViewState["HSFM_NO_APNT_FROM3TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO3"] = "";
            ViewState["HSFM_NO_APNT_TO3TIME"] = 0;

            ViewState["HSFM_APNT_TIME_INT"] = 30;

            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + drpDoctor.SelectedValue + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["RosterID"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM1Desc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM1Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM1"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM1Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM1TIME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM1TIME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM1TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM1TIME"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM1TIME"]).Replace(":", "");

                }
                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO1Desc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO1Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO1"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO1Desc"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO1TIME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO1TIME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO1TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO1TIME"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO1TIME"]).Replace(":", "");
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM2Desc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM2Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM2"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM2Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM2TIME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM2TIME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM2TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM2TIME"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM2TIME"]).Replace(":", "");
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO2Desc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO2Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO2"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO2Desc"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO2TIME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO2TIME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO2TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO2TIME"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO2TIME"]).Replace(":", "");
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM3Desc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM3Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM3"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM3Desc"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM3TIME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM3TIME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM3TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM3TIME"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM3TIME"]).Replace(":", "");
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO3Desc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO3Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO3"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO3Desc"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO3TIME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO3TIME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO3TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO3TIME"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO3TIME"]).Replace(":", "");
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_APNT_TIME_INT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_APNT_TIME_INT"]) != "")
                {
                    ViewState["HSFM_APNT_TIME_INT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_APNT_TIME_INT"]).Replace(":", "");
                }


            }
        }


        Boolean BindCheckBlockedAppointment(Int32 intTime)
        {


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strDay = "";

            if (arrDate.Length > 1)
            {
                strDay = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
            }
            //DateTime dtAppointmentDate = Convert.ToDateTime(strDay);

            DateTime dtAppointmentDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);



            DateTime dtHSFM_NO_APNT_DATEFROM1;
            DateTime dtHSFM_NO_APNT_DATETO1;

            DateTime dtHSFM_NO_APNT_DATEFROM2;
            DateTime dtHSFM_NO_APNT_DATETO2;


            DateTime dtHSFM_NO_APNT_DATEFROM3;
            DateTime dtHSFM_NO_APNT_DATETO3;


            int intHSFM_NO_APNT_FROM1TIME = 0;
            int intHSFM_NO_APNT_TO1TIME = 0;

            int intHSFM_NO_APNT_FROM2TIME = 0;
            int intHSFM_NO_APNT_TO2TIME = 0;

            int intHSFM_NO_APNT_FROM3TIME = 0;
            int intHSFM_NO_APNT_TO3TIME = 0;






            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM1TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM1TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM1TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO1TIME"]) != "")
            {
                intHSFM_NO_APNT_TO1TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO1TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM1"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO1"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM1 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM1"]);
                dtHSFM_NO_APNT_DATETO1 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO1"]);

                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM1 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO1)
                {
                    if (intHSFM_NO_APNT_FROM1TIME <= intTime && intHSFM_NO_APNT_TO1TIME >= intTime)
                    {
                        return true;
                    }
                }
            }



            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM2TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM2TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM2TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO2TIME"]) != "")
            {
                intHSFM_NO_APNT_TO2TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO2TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM2"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO2"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM2 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM2"]);
                dtHSFM_NO_APNT_DATETO2 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO2"]);
                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM2 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO2)
                {
                    if (intHSFM_NO_APNT_FROM2TIME <= intTime && intHSFM_NO_APNT_TO2TIME >= intTime)
                    {
                        return true;
                    }
                }
            }


            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM3TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM3TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM3TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO3TIME"]) != "")
            {
                intHSFM_NO_APNT_TO3TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO3TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM3"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO3"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM3 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM3"]);
                dtHSFM_NO_APNT_DATETO3 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO3"]);
                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM3 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO3)
                {
                    if (intHSFM_NO_APNT_FROM3TIME <= intTime && intHSFM_NO_APNT_TO3TIME >= intTime)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    // SetPermission();
                }
                try
                {
                    ViewState["Date"] = "0";
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                    ViewState["DeptID"] = Convert.ToString(Request.QueryString["DeptID"]);
                    ViewState["DrID"] = Convert.ToString(Request.QueryString["DrID"]);



 
                        lblFilterCaption.Text = "Doctor";
                        drpDoctor.Visible = true;
                      

                    if (Convert.ToString(ViewState["Date"]) != "0" && Convert.ToString(ViewState["Date"]) != "")
                    {
                        txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                    }
                   
                    BindDoctor();
                  
                  



                    if (Convert.ToString(ViewState["DrID"]) != "0" && Convert.ToString(ViewState["DrID"]) != "")
                    {

                        drpDoctor.SelectedValue = Convert.ToString(ViewState["DrID"]);
                        drpDoctor_SelectedIndexChanged(drpDoctor, new EventArgs());


                    }


                    // BindAppDr();
                    //  BindAppointment();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString());
                    TextFileWriting("AppointmentDay.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                drpDoctor_SelectedIndexChanged(drpDoctor, new EventArgs());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.aspx_txtFromDate_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDoctor.SelectedIndex != 0)
            {
                BindStaffData();
                if (BindRosterMaster() == true)
                {
                    BindAppDr();
                    string strAppTiming = Convert.ToString(ViewState["AppointmentTiming"]);
                    string[] arrAppTiming = strAppTiming.Split('|');
                    if (arrAppTiming.Length > 1)
                    {
                        string AppStart = arrAppTiming[0];
                        string AppEnd = arrAppTiming[1];

                        string AppStart1 = arrAppTiming[2];
                        string AppEnd1 = arrAppTiming[3];

                        if (arrAppTiming.Length > 2)
                        {
                            BindAppointmentRoster(AppStart, AppEnd);
                            BindAppointmentRoster(AppStart1, AppEnd1);
                        }
                    }
                    //else
                    //{
                    //    BindAppointment();
                    //}
                }
                else
                {
                    strData = new StringBuilder();
                }
            }

        }

    }
}