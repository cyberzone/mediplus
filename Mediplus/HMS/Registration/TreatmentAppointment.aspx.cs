﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class TreatmentAppointment : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }



            }


        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet("false");
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpTreatTransHour.DataSource = DS;
                drpTreatTransHour.DataTextField = "Name";
                drpTreatTransHour.DataValueField = "Code";
                drpTreatTransHour.DataBind();






            }

            drpTreatTransHour.Items.Insert(0, "00");
            drpTreatTransHour.Items[0].Value = "00";




            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(ViewState["AppointmentInterval"]));
            if (DS.Tables[0].Rows.Count > 0)
            {


                drpTreatTransMin.DataSource = DS;
                drpTreatTransMin.DataTextField = "Name";
                drpTreatTransMin.DataValueField = "Code";
                drpTreatTransMin.DataBind();


            }

            drpTreatTransMin.Items.Insert(0, "00");
            drpTreatTransMin.Items[0].Value = "00";




        }

        void BindTreatmentMaster()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND HTAM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HTAM_TREATMENT_ID='" + txtTreatmentID.Text.Trim() + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*,Convert(varchar,HTAM_DATE,103) as HTAM_DATEDesc   ", "HMS_TREATMENT_APPOINTMENT_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtTreatmentDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_TREATMENT_ID"]);
                txtTreatmentDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_DATEDesc"]);

                txtInvoiceNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_INVOICE_ID"]);
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_PT_NAME"]);
                txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_DR_CODE"]);
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTAM_DR_NAME"]);

            }

        }

        void BindTreatmentMasterGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HTAM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            //Criteria += " AND HPSm_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*,Convert(varchar,HTAM_DATE,103) as HTAM_DATEDesc   ", "HMS_TREATMENT_APPOINTMENT_MASTER", Criteria, "HTAM_TREATMENT_ID Desc");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTreatmentMaster.DataSource = DS;
                gvTreatmentMaster.DataBind();
            }
            else
            {
                gvTreatmentMaster.DataBind();
            }
        }

        void BuildTreatmentTrans()
        {
            DataSet DS = new DataSet();
            string Criteria = "1=2";
            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*,CONVERT(VARCHAR,HTAT_DATE,103) AS HTAT_DATEDesc,CONVERT(VARCHAR,HTAT_DATE,108) AS HTAT_DATETimeDesc", "HMS_TREATMENT_APPOINTMENT_TRANS", Criteria, "");

            ViewState["TreatmentTrans"] = DS.Tables[0];
        }

        void BindTreatmentTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HTAT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HTAT_TREATMENT_ID='" + txtTreatmentID.Text.Trim() + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*,CONVERT(VARCHAR,HTAT_DATE,103) AS HTAT_DATEDesc,CONVERT(VARCHAR,HTAT_DATE,108) AS HTAT_DATETimeDesc", "HMS_TREATMENT_APPOINTMENT_TRANS", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["TreatmentTrans"] = DS.Tables[0];

            }
        }

        void BindTempTreatmentTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["TreatmentTrans"];
            if (DT.Rows.Count > 0)
            {
                gvTreatmentTrans.DataSource = DT;
                gvTreatmentTrans.DataBind();

            }
            else
            {
                gvTreatmentTrans.DataBind();
            }

        }

        Boolean CheckPackageService(string ServiceCode)
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HSM_STATUS='A' AND HSM_PACKAGE_SERVICE = 1 AND HSM_SERV_ID='" + ServiceCode + "'";

            DS = objCom.fnGetFieldValue("TOP 1 *", "HMS_SERVICE_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void BindInvoiceMasterGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

          

            string strStartDate = txtSrcInvFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }



            if (txtSrcInvFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }




            string strTotDate = txtSrcInvToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtSrcInvToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
            }



            if (txtSrcInvFileNo.Text.Trim() != "")
            {
                Criteria += " AND HIM_PT_ID = '" + txtSrcInvFileNo.Text.Trim() + "'";
            }

            if (txtSrcInvFileName.Text.Trim() != "")
            {
                Criteria += " AND   HIM_PT_NAME LIKE '%" + txtSrcInvFileName.Text.Trim() + "%'";
            }


 


            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("TOP 500 *,Convert(varchar,HIM_DATE,103) as HIM_DATEDesc   ", "HMS_INVOICE_MASTER", Criteria, "HIM_DATE Desc");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvoice.DataSource = DS;
                gvInvoice.DataBind();
            }
            else
            {
                gvInvoice.DataBind();
            }
        }


        void BindInvoiceServiceDtls()
        {

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["TreatmentTrans"];


            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            DS = objCom.fnGetFieldValue("HIT_SERV_CODE,HIT_DESCRIPTION,HIT_QTY ", "HMS_INVOICE_TRANSACTION", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {


                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (CheckPackageService(Convert.ToString(DR["HIT_SERV_CODE"])) == false)
                    {


                        Int32 intQty = 1;
                        if (DR.IsNull("HIT_QTY") == false && Convert.ToString(DR["HIT_QTY"]) != "")
                        {
                            intQty = Convert.ToInt32(DR["HIT_QTY"]);
                        }

                        for (Int32 i = 0; i < intQty; i++)
                        {
                            DataRow objrow;
                            objrow = DT.NewRow();
                            objrow["HTAT_DATEDesc"] = txtTreatTransDate.Text;
                            objrow["HTAT_DATETimeDesc"] = drpTreatTransHour.SelectedValue + ":" + drpTreatTransMin.SelectedValue + ":00";

                            objrow["HTAT_INVOICE_ID"] = txtInvoiceNo.Text.Trim();

                            objrow["HTAT_SERV_CODE"] = Convert.ToString(DR["HIT_SERV_CODE"]);
                            objrow["HTAT_SERV_DESC"] = Convert.ToString(DR["HIT_DESCRIPTION"]);

                            objrow["HTAT_DR_CODE"] = txtDoctorID.Text.Trim();
                            objrow["HTAT_DR_NAME"] = txtDoctorName.Text.Trim();
                            objrow["HTAT_STATUS"] = "Open";


                            DT.Rows.Add(objrow);
                        }


                    }
                    else
                    {
                        DataSet DS1 = new DataSet();
                        string Criteria1 = " HPSM_STATUS='ACTIVE' AND HPSM_PACK_SERV_CODE='" + Convert.ToString(DR["HIT_SERV_CODE"]) + "'";
                        objCom = new CommonBAL();


                        DS1 = objCom.fnGetFieldValue("*", "HMS_PACKAGE_SERVICE_MASTER INNER JOIN HMS_PACKAGE_SERVICE_TRANS ON  HPSM_PACKAGE_ID=HPST_PACKAGE_ID", Criteria1, "");

                        foreach (DataRow DR1 in DS1.Tables[0].Rows)
                        {
                            Int32 intQty = 1;
                            if (DR1.IsNull("HPST_QTY") == false && Convert.ToString(DR1["HPST_QTY"]) != "")
                            {
                                intQty = Convert.ToInt32(DR1["HPST_QTY"]);
                            }

                            for (Int32 i = 0; i < intQty; i++)
                            {
                                DataRow objrow;
                                objrow = DT.NewRow();
                                objrow["HTAT_DATEDesc"] = txtTreatTransDate.Text;
                                objrow["HTAT_DATETimeDesc"] = drpTreatTransHour.SelectedValue + ":" + drpTreatTransMin.SelectedValue + ":00";

                                objrow["HTAT_INVOICE_ID"] = txtInvoiceNo.Text.Trim();

                                objrow["HTAT_PACKAGE_ID"] = Convert.ToString(DR1["HPSM_PACKAGE_ID"]);
                                objrow["HTAT_PACKAGE_NAME"] = Convert.ToString(DR1["HPSM_PACKAGE_NAME"]);

                                objrow["HTAT_SERV_CODE"] = Convert.ToString(DR1["HPST_SERV_CODE"]);
                                objrow["HTAT_SERV_DESC"] = Convert.ToString(DR1["HPST_SERV_DESC"]);

                                objrow["HTAT_DR_CODE"] = txtDoctorID.Text.Trim();
                                objrow["HTAT_DR_NAME"] = txtDoctorName.Text.Trim();
                                objrow["HTAT_STATUS"] = "Open";


                                DT.Rows.Add(objrow);
                            }
                        }
                    }

                }



                ViewState["TreatmentTrans"] = DT;



            }

        }

        void Clear()
        {
            txtInvoiceNo.Text = "";
            txtFileNo.Text = "";
            txtName.Text = "";
            txtDoctorID.Text = "";
            txtDoctorName.Text = "";

            ViewState["gvServSelectIndex"] = "";

            BuildTreatmentTrans();
            BindTempTreatmentTrans();

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='PATIENTREG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?PageName=Registration");
            }
        }
        #endregion

        [System.Web.Services.WebMethod]
        public static string[] GetStaffDtls(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'  OR HSFM_MNAME Like '%" + prefixText + "%' OR HSFM_LNAME Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }



            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                txtTreatmentID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "TREATAPPNT");
                txtTreatmentDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtTreatTransDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                txtSrcInvFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtSrcInvToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");


                BindSystemOption();
                BindTime();


                BindTreatmentMasterGrid();
                BuildTreatmentTrans();
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtTreatmentID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "TREATAPPNT");

            Clear();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {


                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["TreatmentTrans"];

                strTestCodeDesc = txtStaffName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Staff Details.',' Please enter Staff Details')", true);

                    goto FunEnd;
                }


                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    //  DT.Rows[R]["HTAT_DATE"] = txtTreatTransDate.Text + " " + drpTreatTransHour.SelectedValue + ":" + drpTreatTransMin.SelectedValue + ":00";

                    DT.Rows[R]["HTAT_INVOICE_ID"] = txtInvoiceNo.Text.Trim();

                    DT.Rows[R]["HTAT_DATEDesc"] = txtTreatTransDate.Text;
                    DT.Rows[R]["HTAT_DATETimeDesc"] = drpTreatTransHour.SelectedValue + ":" + drpTreatTransMin.SelectedValue + ":00";
                    DT.Rows[R]["HTAT_DR_CODE"] = TestProfileID;
                    DT.Rows[R]["HTAT_DR_NAME"] = TestProfileDesc;
                    DT.Rows[R]["HTAT_STATUS"] = drpTreatTranStatus.SelectedValue;


                    DT.AcceptChanges();
                }
                else
                {
                    // objrow["HTAT_DATE"] = txtTreatTransDate.Text + " " + drpTreatTransHour.SelectedValue + ":" + drpTreatTransMin.SelectedValue + ":00";

                    objrow["HTAT_INVOICE_ID"] = txtInvoiceNo.Text.Trim();

                    objrow["HTAT_DATEDesc"] = txtTreatTransDate.Text;
                    objrow["HTAT_DATETimeDesc"] = drpTreatTransHour.SelectedValue + ":" + drpTreatTransMin.SelectedValue + ":00";
                    objrow["HTAT_DR_CODE"] = TestProfileID;
                    objrow["HTAT_DR_NAME"] = TestProfileDesc;
                    objrow["HTAT_STATUS"] = drpTreatTranStatus.SelectedValue;


                    DT.Rows.Add(objrow);
                }



                ViewState["TreatmentTrans"] = DT;


                BindTempTreatmentTrans();
                txtStaffName.Text = "";


                ViewState["gvServSelectIndex"] = "";
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void TreatmentTransEdit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            gvTreatmentTrans.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


            Label lblgvTreatDate = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatDate");
            Label lblgvTreatTime = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatTime");

            Label lblgvTreatDrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatDrCode");
            Label lblgvTreatDrName = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatDrName");

            Label lblgvTreatStatus = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatStatus");


            txtTreatTransDate.Text = lblgvTreatDate.Text;

            string strIA_TIME_IN = lblgvTreatTime.Text.Trim();

            string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
            if (arrIA_TIME_IN.Length > 1)
            {
                drpTreatTransHour.SelectedValue = arrIA_TIME_IN[0];
                drpTreatTransMin.SelectedValue = arrIA_TIME_IN[1];

            }


            txtStaffName.Text = lblgvTreatDrCode.Text + "~" + lblgvTreatDrName.Text;

            drpTreatTranStatus.SelectedValue = lblgvTreatStatus.Text;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TreatmentAppointmentBAL objTreat = new TreatmentAppointmentBAL();
                objTreat.BranchID = Convert.ToString(Session["Branch_ID"]);
                objTreat.TreatmentID = txtTreatmentID.Text.Trim();
                objTreat.TreatmentDate = txtTreatmentDate.Text.Trim();
                objTreat.INVOICE_ID = txtInvoiceNo.Text.Trim();
                objTreat.PT_ID = txtFileNo.Text.Trim();
                objTreat.PT_NAME = txtName.Text.Trim();
                objTreat.DR_CODE = txtDoctorID.Text.Trim();
                objTreat.DR_NAME = txtDoctorName.Text.Trim();
                objTreat.TreatmentAppMasterAdd();

                CommonBAL objCom = new CommonBAL();
                string Criteria = " HTAT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HTAT_TREATMENT_ID='" + txtTreatmentID.Text.Trim() + "'";
                objCom.fnDeleteTableData("HMS_TREATMENT_APPOINTMENT_TRANS", Criteria);


                foreach (GridViewRow GR in gvTreatmentTrans.Rows)
                {
                    Label lblgvTreatDate = (Label)GR.FindControl("lblgvTreatDate");
                    Label lblgvTreatTime = (Label)GR.FindControl("lblgvTreatTime");

                    Label lblgvTreatPackageID = (Label)GR.FindControl("lblgvTreatPackageID");
                    Label lblgvTreatPackageName = (Label)GR.FindControl("lblgvTreatPackageName");

                    Label lblgvTreatServCode = (Label)GR.FindControl("lblgvTreatServCode");
                    Label lblgvTreatServName = (Label)GR.FindControl("lblgvTreatServName");

                    Label lblgvTreatDrCode = (Label)GR.FindControl("lblgvTreatDrCode");
                    Label lblgvTreatDrName = (Label)GR.FindControl("lblgvTreatDrName");
                    Label lblgvTreatStatus = (Label)GR.FindControl("lblgvTreatStatus");
                    Label lblgvTreatQty = (Label)GR.FindControl("lblgvTreatQty");

                    // DropDownList drpgvTranStatus = (DropDownList)GR.FindControl("drpgvTranStatus");


                    objTreat.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objTreat.TreatmentID = txtTreatmentID.Text.Trim();
                    objTreat.TreatmentDate = lblgvTreatDate.Text.Trim() + " " + lblgvTreatTime.Text.Trim();

                    objTreat.INVOICE_ID = txtInvoiceNo.Text.Trim();


                    objTreat.PackageID = lblgvTreatPackageID.Text.Trim();
                    objTreat.PackageName = lblgvTreatPackageName.Text.Trim();

                    objTreat.SERV_CODE = lblgvTreatServCode.Text.Trim();
                    objTreat.SERV_DESC = lblgvTreatServName.Text.Trim();

                    objTreat.DR_CODE = lblgvTreatDrCode.Text.Trim();
                    objTreat.DR_NAME = lblgvTreatDrName.Text.Trim();
                    objTreat.Status = lblgvTreatStatus.Text;/// drpgvTranStatus.SelectedValue;



                    objTreat.TreatmentAppTransAdd();
                }

                Clear();
                txtTreatmentID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "TREATAPPNT");
                BindTreatmentTrans();
                BindTempTreatmentTrans();
                BindTreatmentMaster();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  Details Saved.',' Treatmen Details Saved.')", true);

            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TreatmentServiceMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtTreatmentID_TextChanged(object sender, EventArgs e)
        {
            Clear();
            BindTreatmentMaster();
            BindTreatmentTrans();
            BindTempTreatmentTrans();
        }

        protected void txtInvoiceNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  ";

                Criteria += " AND HIM_INVOICE_ID ='" + txtInvoiceNo.Text.Trim() + "'";


                DataSet DS = new DataSet();
                clsInvoice objInv = new clsInvoice();
                DS = objInv.InvoiceMasterGet(Criteria);


                if (DS.Tables[0].Rows.Count > 0)
                {

                    txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_ID"]);
                    txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);

                    txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);
                    txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_NAME"]);

                    BuildTreatmentTrans();

                    BindInvoiceServiceDtls();

                    BindTempTreatmentTrans();

                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       TreatmentAppointment.txtInvoiceNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void imgTreatmentZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindTreatmentMasterGrid();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PTRegistration", "ShowTreatmentDetailsPopup()", true);
        }

        protected void TreatmentMasEdit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvTreatMasID = (Label)gvScanCard.Cells[0].FindControl("lblgvTreatMasID");

            txtTreatmentID.Text = lblgvTreatMasID.Text;

            Clear();
            BindTreatmentMaster();
            BindTreatmentTrans();
            BindTempTreatmentTrans();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "HideTreatmentDetailsPopup()", true);

        }

        protected void DeleteTrans_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["TreatmentTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["TreatmentTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["TreatmentTrans"] = DT;

                }
                BindTempTreatmentTrans();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       TreatmentAppointment.DeleteTrans_Click");
                TextFileWriting(ex.Message.ToString());
            }




        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " HTAM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HTAM_TREATMENT_ID='" + txtTreatmentID.Text.Trim() + "'";
            objCom.fnDeleteTableData("HMS_PACKAGE_SERVICE_MASTER", Criteria);


            Criteria = " HTAM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HTAT_TREATMENT_ID='" + txtTreatmentID.Text.Trim() + "'";
            objCom.fnDeleteTableData("HMS_TREATMENT_APPOINTMENT_TRANS", Criteria);

            txtTreatmentID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "TREATAPPNT");
            Clear();

        }

        protected void btnInportInvServ_Click(object sender, EventArgs e)
        {
            BuildTreatmentTrans();
            BindInvoiceServiceDtls();
            BindTempTreatmentTrans();
        }

        protected void imgInvoiceZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindInvoiceMasterGrid();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PTRegistration", "ShowInvoicePopup()", true);
        }

        protected void InvoiceIDEdit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvInvoiceID = (Label)gvScanCard.Cells[0].FindControl("lblgvInvoiceID");
            Label lblgvInvoicePtId = (Label)gvScanCard.Cells[0].FindControl("lblgvInvoicePtId");
            Label lblgvInvoicePtName = (Label)gvScanCard.Cells[0].FindControl("lblgvInvoicePtName");
            Label lblgvInvoiceDrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvInvoiceDrCode");
            Label lblgvInvoiceDrName = (Label)gvScanCard.Cells[0].FindControl("lblgvInvoiceDrName");


            txtInvoiceNo.Text = lblgvInvoiceID.Text;
            txtFileNo.Text = lblgvInvoicePtId.Text;
            txtName.Text = lblgvInvoicePtName.Text;
            txtDoctorID.Text = lblgvInvoiceDrCode.Text;
            txtDoctorName.Text = lblgvInvoiceDrName.Text;

            BuildTreatmentTrans();

            BindInvoiceServiceDtls();

            BindTempTreatmentTrans();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "HideInvoicePopup()", true);

        }

        protected void btnInvSearch_Click(object sender, EventArgs e)
        {
            BindInvoiceMasterGrid();
        }

    }
}