﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class TokenCalling : System.Web.UI.Page
    {

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindStatus()
        {

            DataTable dt = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            dt.Columns.Add(Code);
            dt.Columns.Add(Name);

            DataRow objrow;

            objrow = dt.NewRow();



            objrow = dt.NewRow();
            objrow["Code"] = "";
            objrow["Name"] = "--- All ---";
            dt.Rows.Add(objrow);

            objrow = dt.NewRow();
            objrow["Code"] = "Current";
            objrow["Name"] = "Current";
            dt.Rows.Add(objrow);

            objrow = dt.NewRow();
            objrow["Code"] = "In Process";
            objrow["Name"] = "In Process";
            dt.Rows.Add(objrow);

            objrow = dt.NewRow();
            objrow["Code"] = "Waiting";
            objrow["Name"] = "Waiting";
            dt.Rows.Add(objrow);


            drpStatus.DataSource = dt;
            drpStatus.DataTextField = "Name";
            drpStatus.DataValueField = "Code";
            drpStatus.DataBind();


        }

        void BindTokenGrid()
        {
            CommonBAL objCom = new CommonBAL();

            Int32 strTokenDisplayHours = 8;

            //if (GlobelValues.TokenDisplayHours != "")
            //{
            //    strTokenDisplayHours = Convert.ToInt32(GlobelValues.TokenDisplayHours);
            //}

            string Criteria = " 1=1  ";//AND (ETD_AUDIO_PLAYED=0 OR ETD_AUDIO_PLAYED IS NULL ) // AND ETD_GENERATED_FROM='Patient' 
            Criteria += " AND  ETD_TOKEN_TIME > DATEADD( HH, -" + strTokenDisplayHours + ", GETDATE()  ) ";
            if (drpStatus.SelectedIndex != 0)
            {
                Criteria += " AND ETD_STATUS='" + drpStatus.SelectedValue + "'";
            }

            if (txtSrcTokkenNo.Text.Trim() != "")
            {
                Criteria += " AND ETD_CURRENT_TOKEN='" + txtSrcTokkenNo.Text.Trim() + "'";
            }

            ////if (Convert.ToString(GlobelValues.DeviceID) != "")
            ////{
            ////    Criteria += "  AND  ETD_DEVICE_ID  ='" + GlobelValues.DeviceID + "'";
            ////}


            CommonBAL objDB = new CommonBAL();
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.fnGetFieldValue("TOP 50  *, convert(varchar(10),ETD_TOKEN_TIME,103) as ETD_TOKEN_DATEDesc , convert(varchar(5),ETD_TOKEN_TIME,108) as ETD_TOKEN_TIMEDesc  ", "EMR_TOKEN_DISPLAY", Criteria, "ETD_TOKEN_TIME  desc");
            gvTokenList.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTokenList.Visible = true;
                gvTokenList.DataSource = DS.Tables[0];
                gvTokenList.DataBind();


            }
            else
            {
                gvTokenList.DataBind();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Token Details ','No Data','Brown')", true);

            }
        }

        void BindTokenDetails()
        {

            CommonBAL objCom = new CommonBAL();
            string Criteria = "  HIN_SCRN_ID='PT_TOKEN'";

            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" * ", "HMS_INITIAL_NO", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtPrefix.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIN_PREFIX"]);
                txtCurrentToken.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIN_LASTNO"]);

            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                BindStatus();
                BindTokenGrid();
                BindTokenDetails();
            }
        }

        protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTokenGrid();
        }

        protected void TokenSelect_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;


                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");

                Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
                Label lblDrName = (Label)gvScanCard.Cells[0].FindControl("lblDrName");

                Label lblToken = (Label)gvScanCard.Cells[0].FindControl("lblToken");
                Label lblTokenDate = (Label)gvScanCard.Cells[0].FindControl("lblTokenDate");
                Label lblETD_ID = (Label)gvScanCard.Cells[0].FindControl("lblETD_ID");
                CommonBAL objCom = new CommonBAL();

                DataSet DS = new DataSet();

                string FieldNameWithValues = " ETD_STATUS='In Process' ";
                String Criteria = " ETD_STATUS='Current' AND ETD_AUDIO_PLAYED=1 ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HOSPITAL.DBO.EMR_TOKEN_DISPLAY", Criteria);

                String RoomNo = "";

                if (Convert.ToString(Session["HSFM_ROOM"]) != "")
                {
                    RoomNo = Convert.ToString(Session["HSFM_ROOM"]);
                }
                else
                {
                    RoomNo = "RECEP";
                }
                string FieldNameWithValues1 = " ETD_AUDIO_PLAYED = 0 , ETD_STATUS = 'Current' , ETD_DR_ROOM_NO='" + RoomNo + "'"; //'RECEP'
                string Criteria1 = " ETD_CURRENT_TOKEN='" + lblToken.Text + "' AND ETD_ID=" + lblETD_ID.Text;// AND  CONVERT(datetime,convert(varchar(10),ETD_TOKEN_TIME,101),101)= CONVERT(datetime,convert(varchar(10),'" + lblTokenDate.Text + "',101),101)"; // ETD_DR_ID='" + lblDrID.Text.Trim() + "'  AND 
                objCom.fnUpdateTableData(FieldNameWithValues1, "HOSPITAL.DBO.EMR_TOKEN_DISPLAY", Criteria1);

                BindTokenGrid();


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.TokenSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void gvTokenList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPatientId = (Label)e.Row.FindControl("lblPatientId");

                Label lblDrID = (Label)e.Row.FindControl("lblDrID");
                Label lblDrName = (Label)e.Row.FindControl("lblDrName");
                Label lblToken = (Label)e.Row.FindControl("lblToken");

                ImageButton imgSpeaker = (ImageButton)e.Row.FindControl("imgSpeaker");

                string Criteria = "  ETD_DR_ID='" + lblDrID.Text.Trim() + "'  AND ETD_CURRENT_TOKEN='" + lblToken.Text.Trim() + "' ";
                Criteria += " AND ETD_AUDIO_PLAYED = 1  AND ETD_STATUS = 'In Process' ";

                CommonBAL objCom = new CommonBAL();

                DataSet DS = new DataSet();

                DS = objCom.fnGetFieldValue("*", "HOSPITAL.DBO.EMR_TOKEN_DISPLAY", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    imgSpeaker.ImageUrl = "~/Images/SpeakerGreen.png";
                }
                else
                {
                    imgSpeaker.ImageUrl = "~/Images/SpeakerOrange.png";
                }

            }
        }


        protected void Timer1_Tick(object sender, EventArgs e)
        {
            BindTokenGrid();
            BindTokenDetails();
        }




        protected void btnTokenReset_Click(object sender, EventArgs e)
        {

            if (txtCurrentToken.Text.Trim() == "")
            {

                goto FunEnd;
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = "  HIN_SCRN_ID='PT_TOKEN'";
            DS = objCom.fnGetFieldValue("*", "HMS_INITIAL_NO", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                string FieldNameWithValues = "HIN_PREFIX='" + txtPrefix.Text.Trim() + "',HIN_LASTNO='" + txtCurrentToken.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INITIAL_NO", Criteria);
            }
            else
            {
                string FieldName = "HIN_BRANCH_ID, HIN_SCRN_ID, HIN_SCRN_NAME, HIN_PREFIX, HIN_LASTNO, HIN_CREATED_USER, HIN_CREATED_DATE";
                string Values = "'MAIN','PT_TOKEN','Token Display','" + txtPrefix.Text.Trim() + "'," + txtCurrentToken.Text.Trim() + ",'admin',getdate()";
                objCom.fnInsertTableData(FieldName, Values, "HMS_INITIAL_NO");
            }

        FunEnd: ;

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindTokenGrid();
        }


    }
}