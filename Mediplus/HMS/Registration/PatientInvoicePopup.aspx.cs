﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class PatientInvoicePopup : System.Web.UI.Page
    {
        # region Variable Declaration

        string strDataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        string strDBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        string strDBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        string strDBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();


        DataSet DS = new DataSet();
        DataSet DS1 = new DataSet();
        dboperations dbo = new dboperations();


        #endregion

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindInvoiceGrid()
        {
            string Criteria = " 1=1 ";



            if (ViewState["Value"] != "")
            {
                Criteria += " AND  HIM_PT_ID = '" + ViewState["Value"] + "'";
            }


            //string strStartDate = txtFromDate.Text;
            //string[] arrDate = strStartDate.Split('/');
            //string strForStartDate = "";

            //if (arrDate.Length > 1)
            //{
            //    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            //}

            //if (txtFromDate.Text != "")
            //{
            //    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            //}

            //if (txtFromTime.Text != "")
            //{
            //    Criteria += " AND HIM_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";
            //}

            //string strTotDate = txtToDate.Text;
            //string[] arrToDate = strTotDate.Split('/');
            //string strForToDate = "";

            //if (arrToDate.Length > 1)
            //{
            //    strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            //}


            //if (txtToDate.Text != "")
            //{
            //    Criteria += " AND    CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101)  <= '" + strForToDate + "'";
            //}

            //if (txtToTime.Text != "")
            //{



            //    Criteria += " AND HIM_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";
            //}




            //if (txtCompany.Text != "")
            //{
            //    Criteria += " AND HIM_BILLTOCODE = '" + txtCompany.Text.Trim() + "'";
            //}


            //if (drpType.SelectedIndex != 0)
            //{
            //    Criteria += " AND HIM_TRTNT_TYPE = '" + drpType.SelectedValue + "'";

            //}



            //if (drpInv.SelectedIndex != 0)
            //{
            //    Criteria += " AND HIM_INVOICE_TYPE = '" + drpInv.SelectedValue + "'";

            //}

            //if (txtInvoiceNo.Text != "")
            //{
            //    Criteria += " AND HIM_INVOICE_ID = '" + txtInvoiceNo.Text.Trim() + "'";
            //}

            // ViewState["Criteria"] = Criteria;
            dbo = new dboperations();
            DS = new DataSet();




            DS = dbo.InvoiceMasterGet(Criteria);

            lblTotal.Text = "0";
            gvInvoice.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvoice.Visible = true;
                gvInvoice.DataSource = DS;
                gvInvoice.DataBind();


                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }





        }

        void BindInvoiceTran()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND  HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";

            DataSet DS1 = new DataSet();
            DS1 = dbo.InvoiceTransGet(Criteria);

            if (DS1.Tables[0].Rows.Count > 0)
            {
                gvInvTran.Visible = true;
                gvInvTran.DataSource = DS1;
                gvInvTran.DataBind();

            }
            else
            {

                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            //{
            //    Session["ErrorMsg"] = "Permission Denied";
            //    Response.Redirect("../../ErrorPage.aspx");
            //}

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                ViewState["Value"] = Request.QueryString["Value"].ToString();
                if (ViewState["Value"] != "")
                {
                    BindInvoiceGrid();
                }

            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                gvInvoice.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;


                gvInvoice.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;

                Label lblInvoiceId, lblInvType;

                lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");
                lblInvType = (Label)gvScanCard.Cells[0].FindControl("lblInvType");


                ViewState["InvoiceId"] = lblInvoiceId.Text;
                ViewState["InvoiceType"] = lblInvType.Text;
                BindInvoiceTran();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnCInvoicePrint_Click(object sender, EventArgs e)
        {
            hidInvoiceId.Value = Convert.ToString(ViewState["InvoiceId"]);
            string strInvType = Convert.ToString(ViewState["InvoiceType"]);

            String DBString = "";
            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;

            if (strInvType == "Credit")
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowCreditReport('" + DBString + "');", true);
            }
            else if (strInvType == "Cash")
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowCashReport('" + DBString + "');", true);

            }
        }
    }
}