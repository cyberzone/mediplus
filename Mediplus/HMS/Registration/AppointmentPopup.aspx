﻿<%@ page language="C#" autoeventwireup="true" codebehind="AppointmentPopup.aspx.cs" inherits="Mediplus.HMS.Registration.AppointmentPopup" %>


<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

      <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title>Appointment</title>
     <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function passvalue(strDate, strPageName, DrId, DeptId) {
            if (strPageName == "ApptDay") {
                opener.location.href = "AppointmentDay.aspx?Date=" + strDate + "&DeptID=" + DeptId;;
            }
            if (strPageName == "ApptWeek") {
                opener.location.href = "AppointmentWeek.aspx?Date=" + strDate + "&DrID=" + DrId;
            }
            if (strPageName == "ApptEmrDr") {
                opener.location.href = "AppointmentEMRDr.aspx?Date=" + strDate + "&DrID=" + DrId;
            }
            opener.focus();
            self.close();

        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }

        function val() {
            var label;
            label = document.getElementById('lblStatus');
            label.style.color = 'red';
            document.getElementById('lblStatus').innerHTML = " ";

            /*
            if (document.getElementById('txtFileNo').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the File No";
                document.getElementById('txtFileNo').focus();
                return false;
            }
            */

            if (document.getElementById('txtFName').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the First Name";
                document.getElementById('txtFName').focus();
                return false;
            }

            if (document.getElementById('txtFromDate').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the Date";
                document.getElementById('txtFromDate').focus();
                return false;
            }


            if (document.getElementById('txtFromDate').value != "") {
                if (isDate(document.getElementById('txtFromDate').value) == false) {
                    document.getElementById('txtFromDate').focus()
                    return false;
                }
            }


            if (document.getElementById('txtEmail').value != "") {
                if (echeck(document.getElementById('txtEmail').value) == false) {
                    document.getElementById('txtEmail').focus();
                    return false;
                }
            }


            return true



        }


        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=Appointment&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin1", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            return true;

        }

        function BindPTDetails(HPM_PT_ID, PatientName, Mobile, Email) {
            document.getElementById('txtFileNo').value = HPM_PT_ID;
            document.getElementById('txtFName').value = PatientName;
            document.getElementById('txtMobile').value = Mobile;
            document.getElementById('txtEmail').value = Email;
        }



        function ServIdSelected() {
            if (document.getElementById('txtServCode').value != "") {
                var Data = document.getElementById('txtServCode').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('txtServCode').value = Data1[0];
                    document.getElementById('txtServName').value = Data1[1];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('txtServName').value != "") {
                var Data = document.getElementById('txtServName').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('txtServCode').value = Data1[0];
                    document.getElementById('txtServName').value = Data1[1];
                }
            }

            return true;
        }



    </script>

      <style>
          .AutoExtender
          {
              font-family: Verdana, Helvetica, sans-serif;
              font-size: .8em;
              font-weight: normal;
              border: solid 1px #006699;
              line-height: 20px;
              padding: 10px;
              background-color: White;
              margin-left: 10px;
          }

          .AutoExtenderList
          {
              border-bottom: dotted 1px #006699;
              cursor: pointer;
              color: Maroon;
          }

          .AutoExtenderHighlight
          {
              color: White;
              background-color: #006699;
              cursor: pointer;
          }

          #divwidth
          {
              width: 400px !important;
          }

              #divwidth div
              {
                  width: 400px !important;
              }
      </style>
</head>
<body  >
    <form id="form1" runat="server">
        <div>
              <input type="hidden" id="hidPermission" runat="server" value="9" />
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
             <asp:HiddenField ID="hidStatus" runat="server" />
            <div style="padding-top: 0px;">
               
                <table width="100%" >
                <tr>
                    <td class="PageHeader">Appointment
                        <asp:CheckBox ID="chkOTAppointment" visible="false" runat="server" CssClass="lblCaption1" Text="OT Appointment" OnCheckedChanged="chkOTAppointment_CheckedChanged" Checked="false" AutoPostBack="true" />

                    </td>
                    <td align="right" style="text-align:right;float:right;" >
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="button gray small" Width="70px"  OnClick="btnDelete_Click" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button gray small" Width="70px" OnClientClick="window.close()" />
                        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" ToolTip="Clear" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel Appt." CssClass="button gray small" Width="100px" OnClick="btnCancel_Click" ToolTip="Cancel the Appointment" />
                         <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button red small" Width="70px" OnClick="btnSave_Click" OnClientClick="return val();" />
                         
                           
                            
                        </td>
                </tr>
            </table>
                <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%"">
                    <tr>
                        <td>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </td>
                    </tr>

                </table>
                <table width="100%">
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                                Text="File No"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFileNo" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="200px" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                                Text="Service"></asp:Label>

                        </td>
                        <td>
                             <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="" Checked="false" />
                            <asp:DropDownList ID="drpService" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="drpService_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="lblDuration" runat="server" CssClass="label"  ></asp:Label>
                            <asp:Label ID="lblDurationType" runat="server" CssClass="label"  ></asp:Label>
                           <asp:ImageButton ID="imgServZoom" runat="server" ImageUrl="../../Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Payment Entry" OnClick="imgServZoom_Click" />

                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                                Text="First Name"></asp:Label>
                        </td>
                        <td>
                                 <asp:Label ID="lblFullName" runat="server" CssClass="label"  visible="false" ></asp:Label>
                            <asp:TextBox ID="txtFName" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="Red" Width="200px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" CssClass="lblCaption1"
                                Text="Status"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpStatus" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="315px" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label5" runat="server" CssClass="lblCaption1"
                                Text="Last Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLName" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="200px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label6" runat="server" CssClass="lblCaption1"
                                Text="Doctor"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="315px"  AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label7" runat="server" CssClass="lblCaption1"
                                Text="Mobile"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMobile" runat="server" CssClass="label"  BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" Width="180px"  ondblclick="return PatientPopup1('Mobile1',this.value);"></asp:TextBox>
                            <asp:ImageButton ID="imgPTMobileZoom" runat="server" ImageUrl="../../Images/Zoom.png" Style="height: 20px; width: 20px;" ToolTip="Display Payment Entry" OnClick="imgPTMobileZoom_Click" />

                        </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" CssClass="lblCaption1"
                                Text="Date"></asp:Label>

                        </td>
                        <td class="lblCaption1">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="72px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="red" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                                enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                            </asp:calendarextender>
                            <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                            Upcoming Days 
                             <asp:TextBox ID="txtUpcomingDays" runat="server" Width="30px" CssClass="label" MaxLength="10" style="text-align:center;" BorderWidth="1px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                          
                        </td>
                      </tr>
                  
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label9" runat="server" CssClass="lblCaption1"
                                Text="Email"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </td>
                       <td class="lblCaption1">
                            Time
                       </td>
                        <td class="lblCaption1">
                        <asp:DropDownList ID="drpSTHour" CssClass="label" style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpSTMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>         
                            <asp:DropDownList ID="drpSTAM" runat="server" visible="false" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                               To
                            <asp:DropDownList ID="drpFIHour" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpFIMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>
                            <asp:DropDownList ID="drpFIAM" runat="server" visible="false"  CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="lblCaption1">Gender 
                       </td>
                        <td class="lblCaption1">
                               <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle" Width="100px">
                                     <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                     <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                     <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                    <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                </asp:DropDownList>
                                Category
                                 <asp:DropDownList ID="drpPTCategory" runat="server" CssClass="TextBoxStyle" Width="100px">   </asp:DropDownList>
                            </td>
                       
                         <td class="lblCaption1"> 
                               PT. Type
                       </td>
                          <td class="lblCaption1"> 
                                <asp:DropDownList ID="drpAppPatientType" runat="server" CssClass="TextBoxStyle" Width="100px">   </asp:DropDownList>
                           Room
                              <asp:DropDownList ID="drpRoom" runat="server" CssClass="TextBoxStyle" Width="100px">   </asp:DropDownList>
                          </td>
                    </tr>
                      
                    <tr>
                        <td class="style2">
                            
                        </td>
                        <td>
                         </td>
                        <td>
                            <asp:Label ID="Label13" runat="server" CssClass="lblCaption1"
                                Text="Ref. Dr."></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpRefDoctor" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="315px"  AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                        <tr>
                         <td>
                            <asp:Label ID="Label10" runat="server" CssClass="lblCaption1"
                                Text="Remarks"></asp:Label>

                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" Width="580px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" TextMode="MultiLine" Height="30px"></asp:TextBox>
                        </td>

                        </tr>
                      <tr>
                       
                        <td>
                            <asp:Label ID="Label12" runat="server" CssClass="lblCaption1"
                                Text="Service"></asp:Label>

                        </td>
                        <td colspan="3">
                           <asp:Label ID="lblServices" runat="server" CssClass="lblCaption1"
                                ></asp:Label>
                        </td>
                    </tr>
                    <div id="divOTAppointment" runat="server" visible="false">
                     <tr>
                         <td class="lblCaption1" style="height: 30px;">Procedure 
                        </td>
                        <td   colspan="3" >
                
                            <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" onblur="return ServIdSelected()"></asp:TextBox>
                        
                        
                            <asp:TextBox ID="txtServName" runat="server" Width="500px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                            <div id="divwidth" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="height: 30px;">Surgery Type 
                        </td>
                        <td>
                                <asp:DropDownList ID="drpSurgeryType" runat="server"  CssClass="label"    BorderWidth="1px"  BorderColor="#cccccc" Width="200px" >
                                     <asp:ListItem Value="" >--- Select ---</asp:ListItem>
                                     <asp:ListItem Value="Elective">Elective</asp:ListItem>
                                     <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                                     <asp:ListItem Value="Normal">Normal</asp:ListItem>
                                </asp:DropDownList>

                         </td>
                         <td class="lblCaption1" style="height: 30px;"> Anesthesia Type
                        </td>
                        <td>
                             <asp:DropDownList ID="drpAnesthesia" runat="server"  CssClass="label"    BorderWidth="1px"  BorderColor="#cccccc" Width="315px" >
                                     <asp:ListItem Value="" >--- Select ---</asp:ListItem>
                                     <asp:ListItem Value="General">General</asp:ListItem>
                                     <asp:ListItem Value="Local">Local</asp:ListItem>
                              </asp:DropDownList>

                         </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="height: 30px;">Anesthetist 
                        </td>
                        <td >
                             <asp:DropDownList ID="drpAnesthetist" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="200px">
                            </asp:DropDownList>
                         </td>
                        <td class="lblCaption1">
                            Time In
                       </td>
                        <td class="lblCaption1">
                        <asp:DropDownList ID="drpTimeInHour" CssClass="label" style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpTimeInMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>         
                            <asp:DropDownList ID="drpTimeInAM" runat="server" visible="false" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                               Out
                            <asp:DropDownList ID="drpTimeOutHour" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpTimeOutMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>
                            <asp:DropDownList ID="drpTimeOutAM" runat="server" visible="false"  CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                        </td>
                    </tr>
                        <tr>
                        <td class="lblCaption1" style="height: 30px;">Special Request
                        </td>
                        <td colspan="3">
                           <asp:TextBox ID="txtSpecialRequest" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" Width="580px"  TextMode="MultiLine" Height="30px" ></asp:TextBox>
                         </td>
                        </tr>
                    </div>
                        <tr> 
                            <td></td>
                        <td colspan="2"></td>
                            <td class="lblCaption1" >
                                SMS Language
                                 <asp:DropDownList ID="drpSMSLanguage" runat="server"  CssClass="label"    BorderWidth="1px"  BorderColor="#cccccc" Width="100px" >
                                          <%--  <asp:ListItem Value="1">English</asp:ListItem>
                                            <asp:ListItem Value="3">Arabic</asp:ListItem>--%>
                                       <asp:ListItem Value="0">English</asp:ListItem>
                                       <asp:ListItem Value="8">Arabic</asp:ListItem>
                                        </asp:DropDownList>
                            </td>
                        </tr>

                    <tr>
                       <td></td>
                        
                        <td  colspan="3">
                       
                             <asp:Button ID="btnSendSMS" runat="server" Text="Send SMS" CssClass="button red small" Width="100px"  OnClick="btnSendSMS_Click"/>
                            <asp:Button ID="btnSendSMSReminder" runat="server" Text="Send SMS Reminder" CssClass="button gray small" Width="155px" OnClick="btnSendSMSReminder_Click"  />
                            <asp:Button ID="btnSendSMSAuth" runat="server" Text="Send SMS Authorization" CssClass="button gray small" Width="155px" OnClick="btnSendSMSAuth_Click"  />
                             <asp:Button ID="btnSendMail" runat="server" Text="Send Mail" CssClass="button red small" Width="100px"  OnClick="btnSendMail_Click"/>
                            <asp:Button ID="btnPreview" runat="server" Text="Preview Mail" CssClass="button gray small" Width="100px"  OnClick="btnPreview_Click"/>
                           
                        </td>




                    </tr>
                    <tr>
                       <td></td>
                        
                        <td  colspan="3">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                     <asp:Button ID="btnLabelTreatAppointPrint" runat="server" CssClass="button gray small" Width="100px" Text="Print Label" ToolTip="Label Print" OnClick="btnLabelTreatAppointPrint_Click" Visible="false"  />
                                 </ContentTemplate> 
                           </asp:UpdatePanel> 
                        </td>
                    </tr>

                    <tr>
                          <td></td>
                        <td style="height: 50px;">
                             <asp:Button ID="btnSendSMSComplement" runat="server" Text="Send Complement SMS" CssClass="button gray small" Width="155px"  Visible="false"    />

                        </td>
                    </tr>
                </table>
                <div style="padding-left: 0px; padding-top: 0px; width: 100%; height: 50px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
                <table style="width: 100%; border: 0px;" cellpadding="0px" cellspacing="0px">
                    <tr  class="GridHeader_Blue">
                        <td class="label" style="width: 200px; color: #ffffff;">Created User</td>
                        <td class="label" style="width: 100px; color: #ffffff;">Created Date</td>
                        <td class="label" style="width: 200px; color: #ffffff;">Modified User</td>
                        <td class="label" style="width: 100px; color: #ffffff;">Modified Date</td>

                    </tr>
                    <tr>
                        <td colspan="4" style="height: 5px;"></td>
                    </tr>
                    <tr>

                        <td class="label">
                            <asp:Label ID="lblCreatedUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="label">
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="label">
                            <asp:Label ID="lblModifiedUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                        </td>

                    </tr>
                </table>
                </div>
            </div>


            <div id="divServicePopup" runat="server" visible="false" style="overflow: auto ; border: groove; height: 350px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 100px; top: 50px;">
                <div style="width: 100%; text-align: right; float: right; position: absolute; top: 0px; right: 0px;">

                     <asp:ImageButton ID="btnMsgClose" runat="server" ImageUrl="~/Images/Close.png" Style="height: 25px; width: 25px;" OnClick="btnMsgClose_Click" />

                </div>

                <table cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td class="lblCaption1" style="height: 300px; vertical-align: top;">
                            <asp:GridView ID="gvService" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"  
                                EnableModelValidation="True" Width="100%">
                                <HeaderStyle CssClass="GridHeader_Gray" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <RowStyle CssClass="GridRow" />

                                <Columns>

                                    <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkName" runat="server" OnClick="ServiceSelect_Click">
                                                <asp:Label ID="lblName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HAS_SERVICENAME") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration (Minutes)" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDuration" runat="server" OnClick="ServiceSelect_Click">
                                                <asp:Label ID="lblDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HAS_SERVICE_DURATION") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    

                                </Columns>
                            </asp:GridView>


                        </td>
                    </tr>


                </table>

            </div>

             <div id="divMailPreview" runat="server"  visible="false"  style="  overflow: auto; border: groove; height: 420px; width: 710px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 10px; top: 10px;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="PageHeader"  style="vertical-align: top; width: 50%;">
                            Mail Preview
                        </td>
                        <td align="right" style="vertical-align: top; width: 50%;">
                             <asp:Button ID="ImageButton1" runat="server" CssClass="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" Text=" X "  OnClick="ImageButton1_Click" />

                        </td>
                    </tr>
                </table>
                 <div style="width: 100%; border-color: #666666; border-bottom-style: solid; border-width: 2px;"></div>
                 <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="lblCaption1" style="height: 300px; vertical-align: top;">
                         <span class="lblCaption1"><%=strMailPreview %></span>
                        </td> 
                    </tr>     
                 </table>
            </div>
        </div>
    </form>
   
</body>

    
    <script>

 

      


        function ShowtLabelTreatAppointPrint(AppointmentID, InvoiceID) {
            var PatientID = document.getElementById('<%=txtFileNo.ClientID%>').value
            var Report = "TreatAppointmentLabel.rpt"
            var Criteria = " 1=1 ";
            Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + PatientID + "'";

            if (AppointmentID != "" && AppointmentID != 0) {
                Criteria += ' AND {HMS_APPOINTMENT_OUTLOOKSTYLE.HAM_APPOINTMENTID}=' + AppointmentID;
            }

            Criteria += ' AND {HMS_VIEW_GetTreatAppointStatus.HTAM_INVOICE_ID}=\'' + InvoiceID + "'";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();
        }

    </script>
</html>
