﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="PTConcernFormDisplay.aspx.cs" Inherits="Mediplus.HMS.Registration.PTConcernFormDisplay" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <title>Appointment</title>
    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript">

        function ShowSignature() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID.Trim()%>').value
            if (PtId == "") {
                document.getElementById("<%=lblStatus.ClientID%>").innerHTML = 'Please enter patient File No'
                return false;
            }

            var win = window.open("PTConcernFormSignature.aspx?PT_ID=" + PtId, "newwin", "top=200,left=100,height=600,width=800,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function Clear() {
            document.getElementById("<%=lblStatus.ClientID%>").innerHTML = '';
            document.getElementById("<%=txtFileNo.ClientID%>").value = '';
            document.getElementById("<%=txtPTName.ClientID%>").value = '';

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="lblCaption1"></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td class="lblCaption1">FIle  No
                    </td>
                    <td>
                        <asp:TextBox ID="txtFileNo" runat="server" Width="100px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtPTName" runat="server" Width="200px" CssClass="TextBoxStyle"></asp:TextBox>
                    </td>
                    <td>

                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="button gray small" OnClick="btnRefresh_Click" />
                        <input type="button" id="btnClear" title="Clear" value="Clear" class="button gray small" onclick="Clear()" />
                        <input type="button" id="btnSignature" title="Signature" value="Signature" class="button orange small" onclick="return ShowSignature()" />
                    </td>
                </tr>
            </table>
            <iframe id="frmVisit" runat="server" style="width: 90%; height: 700px"></iframe>


        </div>
    </form>
</body>
</html>
