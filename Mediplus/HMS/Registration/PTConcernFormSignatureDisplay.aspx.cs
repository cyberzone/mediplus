﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class PTConcernFormSignatureDisplay : System.Web.UI.Page
    {
        void DisplayImage()
        {
            try
            {

                string PBMI_FullPath = "";

                //PBMI_FullPath = @"F:\Projects\Mediplus\Mediplus\HMS\Uploads\Signature\notsaved.jpg";

                PBMI_FullPath = Server.MapPath("../Uploads/Signature/notsaved.jpg");


                //TextFileWriting(PBMI_FullPath);

                FileStream fs = new FileStream(PBMI_FullPath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();

                //Write the file to response Stream
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "image/JPEG";
                Response.AddHeader("content-disposition", "attachment;filename=Signature");
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayImage();
            }
        }
    }
}