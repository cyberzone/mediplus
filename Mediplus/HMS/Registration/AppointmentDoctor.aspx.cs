﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Registration
{
    public partial class AppointmentDoctor : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();
        public static string BEAUTICIANCODE = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["BEAUTICIANCODE"].ToString());
        public StringBuilder strDataDr = new StringBuilder();

        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDrDept()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' ";
            Criteria += " AND  HDM_DEP_NAME NOT IN   ('SURGERY','RECEPTION','PORTERS','PHARMACY','INSURANCE','HOME CARE','CASHIER','CLINIC ASSISTANTS','ADMIN','NURSE','NURSES') ";
            dboperations dbo = new dboperations();
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
               

            }

 drpDepartment.Items.Insert(0, "--- Select ---");
                drpDepartment.Items[0].Value = "0";


        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }


            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "0";

        }
        void BindAppDr()
        {
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string[] arrDrCode = BEAUTICIANCODE.Split(',');

            string strDrCode = "";
            for (int i = 0; i < arrDrCode.Length; i++)
            {
                strDrCode += "'" + arrDrCode[i] + "',";
            }
            if (strDrCode.Length > 1)
                strDrCode = strDrCode.Substring(0, strDrCode.Length - 1);

            Criteria += " AND HSFM_STAFF_ID NOT IN (" + strDrCode + ")";

            if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
            {

                Criteria += " AND HSFM_STAFF_ID ='" + drpDoctor.SelectedValue + "'";
            }
            else
            {

                if (drpDepartment.SelectedIndex != 0)
                    Criteria += " AND HSFM_DEPT_ID='" + drpDepartment.SelectedValue + "'";

            }

            dboperations dbo = new dboperations();

            DataSet DSDoctors = new DataSet();
            DSDoctors = dbo.retun_doctor_detailss(Criteria);

            strDataDr.Append("<tr ><td width='100px'  class='Header' >&nbsp;&nbsp;Time&nbsp;&nbsp;</td>");
            // strData.Append(strSelectedDate + "</br>" + strday);
            //  strData.Append("</td>");


            if (DSDoctors.Tables[0].Rows.Count > 0)
            {
                //style='font-family:Times New Roman,font-size:5;color:#2078c0;text-align:center'>

                for (Int32 i = 0; i <= DSDoctors.Tables[0].Rows.Count - 1; i++)
                {
                    strDataDr.Append("<td    class='DrBox' >");
                    strDataDr.Append(Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_FNAME"])); //+ " | " + Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_STAFF_ID"]));
                    strDataDr.Append("</td>");
                }
            }
            strDataDr.Append("</tr>");
        }

        void BindAppointment()
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(Session["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(Session["AppointmentEnd"]);

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string[] arrDrCode = BEAUTICIANCODE.Split(',');

            string strDrCode = "";
            for (int i = 0; i < arrDrCode.Length; i++)
            {
                strDrCode += "'" + arrDrCode[i] + "',";
            }
            if (strDrCode.Length > 1)
                strDrCode = strDrCode.Substring(0, strDrCode.Length - 1);

            Criteria += " AND HSFM_STAFF_ID NOT IN (" + strDrCode + ")";

            if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
            {

                Criteria += " AND HSFM_STAFF_ID  ='" + drpDoctor.SelectedValue + "'";
            }
            else
            {

                if (drpDepartment.SelectedIndex != 0)
                    Criteria += " AND HSFM_DEPT_ID='" + drpDepartment.SelectedValue + "'";

            }


            dboperations dbo = new dboperations();

            DataSet DSDoctors = new DataSet();
            DSDoctors = dbo.retun_doctor_detailss(Criteria);


            //Criteria = " 1=1 AND HROS_TYPE='D' ";

            //DataSet DSRoster = new DataSet();
            //DSRoster = dbo.RosterMasterGet(Criteria);

            string strSelectedDate = txtFromDate.Text;  //System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
            string strday = strTSelDate.DayOfWeek.ToString();

       
            //strData.Append("<tr style='top:0;'><td width='100px'  class='Header' >&nbsp;&nbsp;Time&nbsp;&nbsp;</td>");
           

            //if (DSDoctors.Tables[0].Rows.Count > 0)
            //{
                
            //    for (Int32 i = 0; i <= DSDoctors.Tables[0].Rows.Count - 1; i++)
            //    {
            //        strData.Append("<td class='Header' >");
            //        strData.Append(Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_FNAME"]) + " | " + Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_STAFF_ID"]));
            //        strData.Append("</td>");
            //    }
            //}
            //strData.Append("</tr>");


            Int32 intday = strTSelDate.Day;
 
            String strlTime;
            String strNewTime1 = "";


            //strrtime = strFrom1.Substring(strFrom1.Length - 2, 2);
            strlTime = Convert.ToString(AppointmentStart); //strFrom1.Substring(0, 2);
            strNewTime1 = "00";// strFrom1.Substring(strFrom1.Length - 2, 2);

            AppointmentStart = AppointmentStart * 100;
            AppointmentEnd = AppointmentEnd * 100;

            int[] arrCount = new int[DSDoctors.Tables[0].Rows.Count];

            for (Int32 i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                if (strNewTime1 == "")
                {
                    strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                }

                // i = i + AppointmentInterval;
                string[] arrlTime = strlTime.Split(':');
                if (arrlTime.Length > 1)
                {
                    strlTime = arrlTime[0];
                }

                if (Convert.ToInt32(strNewTime1) >= 60)
                {
                    strlTime = Convert.ToString(Convert.ToInt32(Convert.ToInt32(strlTime) + 1).ToString("D2")) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                }
                else
                {
                    strlTime = Convert.ToString(Convert.ToInt32(strlTime).ToString("D2") + ":" + strNewTime1);
                }

                i = Convert.ToInt32(strlTime.Replace(":", ""));


                if (Convert.ToInt32(strlTime.Replace(":", "")) <= AppointmentEnd && strlTime != "00:00")
                {
                    strData.Append("<tr>");
                    strData.Append("<td    class='RowHeader'>");
                    strData.Append(strlTime);
                    strData.Append("</td>");




                    for (int j = 0; j <= DSDoctors.Tables[0].Rows.Count - 1; j++)
                    {
                        if (arrCount[j] > 0)
                        {
                            arrCount[j] = arrCount[j] - 1;
                            goto ForEnd;
                        }

                        DataSet DSAppt = new DataSet();
                        DSAppt = GetAppointment(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]), strlTime);
                        if (DSAppt.Tables[0].Rows.Count > 0)
                        {
                            String strlTime2 = "";
                            Int32 AppointmentStart2 = 0, AppointmentEnd2 = 0;

                            string strStartTime = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TIME"]);
                            //string[] arrStartTime = strStartTime.Split(':');
                            //strStartTime = arrStartTime[0] +""+ arrStartTime[1];
                            strStartTime = strStartTime.Replace(":", "");


                            string strStartFnish = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TIME"]);
                            //string[] arrStartFnish =strStartFnish.Split(':');
                            //strStartFnish = arrStartFnish[0] + "" + arrStartFnish[1];
                            strStartFnish = strStartFnish.Replace(":", "");

                            AppointmentStart2 = Convert.ToInt32(strStartTime);
                            AppointmentEnd2 = Convert.ToInt32(strStartFnish);

                            strlTime2 = strStartTime.Substring(0, 2);
                            Int32 intRowCount = 0;
                            String strNewTime2 = Convert.ToString(AppointmentStart2);
                            strNewTime2 = strNewTime2.Substring(strNewTime2.Length - 2, 2);

                            for (Int32 k = AppointmentStart2; k <= AppointmentEnd2; k++)
                            {
                                if (strNewTime2 == "")
                                {
                                    strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
                                }

                                string[] arrlTime2 = strlTime2.Split(':');
                                if (arrlTime2.Length > 1)
                                {
                                    strlTime2 = arrlTime2[0];
                                }

                                if (Convert.ToInt32(strNewTime2) >= 60)
                                {
                                    strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                                }
                                else
                                {
                                    strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
                                }

                                k = Convert.ToInt32(strlTime2.Replace(":", ""));
                                strNewTime2 = "";

                                intRowCount = intRowCount + 1;

                            }

                            arrCount[j] = intRowCount - 2;

                            string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_COLOR_CODE"]);
                            strData.Append("<td ondblclick=PatientPopup1('" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ID"]) + "','0','0','" + drpDepartment.SelectedValue + "')     height='50px' align='center' class='AppBox'   rowspan=" + Convert.ToString(intRowCount - 1) + " style='vertical-align:middle;border-color:black;background-color: " + strColor + ";'>");
                            strData.Append("<span class=lblCaption1>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILE_NO"]) + " : " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILE"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_TYPEOFSERVICE"]) + "</span>");
                            strData.Append("</td>");
                        }
                        else
                        {

                            strData.Append("<td ondblclick=PatientPopup1('0','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "','" + drpDepartment.SelectedValue + "')  height='50px'   class='AppBox' style='border-color:black;background-color: white;'>");
                           // strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
                            strData.Append("</td>");

                        }
                    ForEnd: ;
                    }




                    strData.Append("</tr>");
                }

                strNewTime1 = "";

            }


        }

        DataSet GetAppointment(string DRCode, string FromTime)
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            string strlTime;
            strlTime = FromTime;

            string strNewTime1 = "";
            if (strNewTime1 == "")
            {
                strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
            }

            // i = i + AppointmentInterval;


            string[] arrlTime = strlTime.Split(':');
            if (arrlTime.Length > 1)
            {
                strlTime = arrlTime[0];
            }

            if (Convert.ToInt32(strNewTime1) >= 60)
            {
                strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
            }
            else
            {
                strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
            }

            string Criteria = " 1=1 ";

            //if (radAppStatus.SelectedIndex != 0)
            //{
            //    Criteria += "  AND HAM_STATUS IN ('" + hidStatus.Value + "')";
            //}
            //else
            //{
            //    Criteria += "  AND HAM_STATUS NOT IN ('" + hidStatus.Value + "')";
            //}
           
           
            Criteria += " AND HAM_STAFF_ID='" + DRCode + "'";

            Criteria += "   AND   HAM_APNT_DATE = CONVERT(DATETIME,'" + txtFromDate.Text.Trim() + "',103) ";
            Criteria += "   AND   HAM_APNT_TIME = '" + FromTime  +"'";



            clsAppointmentMaster clsAppMas = new clsAppointmentMaster();
            DataSet DSAppt = new DataSet();

            DSAppt = clsAppMas.AppointmentMasterGet(Criteria);

            return DSAppt;
        }

        void GetAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1 ";

            //if (radAppStatus.SelectedIndex != 0)
            //{
            //    Criteria += " AND HAS_STATUS='" + radAppStatus.SelectedValue + "'";
            //}
            //else
            //{
            //    Criteria += " AND HAS_STATUS='" + radAppStatus.Items[1].Value + "' OR  HAS_STATUS='" + radAppStatus.Items[2].Value + "'";
            //}
            DS = dbo.AppointmentStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }
        
        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnNewApp.Enabled = false;




            }


            if (strPermission == "7")
            {

                btnNewApp.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    // SetPermission();
                }
                try
                {
                    ViewState["Date"] = "0";
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                    ViewState["DeptID"] = Convert.ToString(Request.QueryString["DeptID"]);
                    ViewState["DrID"] = Convert.ToString(Request.QueryString["DrID"]);


                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {
                        lblFilterCaption.Text = "Doctor";
                        drpDoctor.Visible = true;
                        drpDepartment.Visible = false;

                    }

                    if (Convert.ToString(ViewState["Date"]) != "0" && Convert.ToString(ViewState["Date"]) != "")
                    {
                        txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                    }
                    BindDrDept();
                    BindDoctor();
                    GetAppointmentStatus();


                    if (Convert.ToString(ViewState["DeptID"]) != "0" && Convert.ToString(ViewState["DeptID"]) != "")
                    {

                        drpDepartment.SelectedValue = Convert.ToString(ViewState["DeptID"]);
                        drpDepartment_SelectedIndexChanged(drpDepartment, new EventArgs());


                    }



                    if (Convert.ToString(ViewState["DrID"]) != "0" && Convert.ToString(ViewState["DrID"]) != "")
                    {

                        drpDoctor.SelectedValue = Convert.ToString(ViewState["DrID"]);
                        drpDoctor_SelectedIndexChanged(drpDepartment, new EventArgs());


                    }


                   // BindAppDr();
                  //  BindAppointment();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString());
                    TextFileWriting("AppointmentDay.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppointmentStatus();

                BindAppDr();
                BindAppointment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.aspx_txtFromDate_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void radAppStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppointmentStatus();

                BindAppointment();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.aspx_radAppStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDepartment.SelectedIndex != 0)
            {
                GetAppointmentStatus();
                BindAppDr();
                BindAppointment();
            }
        }


        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDoctor.SelectedIndex != 0)
            {
                GetAppointmentStatus();
                BindAppDr();
                BindAppointment();
            }
            
        }



    }
}