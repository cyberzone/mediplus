﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PatientVisitUpdate.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientVisitUpdate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

        <script src="../../Scripts/Validation.js" type="text/javascript"></script>

    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">


    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function Val() {

            if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Date";
                document.getElementById('<%=txtFromDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtFromDate.ClientID%>').focus()
                    return false;
                }


                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Date";
                 document.getElementById('<%=txtToDate.ClientID%>').focus;
                 return false;
             }

             if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtToDate.ClientID%>').focus()
                  return false;
              }


              return true;

          }
          function SaveVal() {

              if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter File No";
                document.getElementById('<%=txtFileNo.ClientID%>').focus;
                return false;
            }

            if (document.getElementById('<%=txtPTName.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Patient Name";
                document.getElementById('<%=txtPTName.ClientID%>').focus;
                return false;
            }


            if (document.getElementById('<%=txtVisitDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Visit Date";
                document.getElementById('<%=txtVisitDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtVisitDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtFromDate.ClientID%>').focus()
                  return false;
              }

              if (document.getElementById('<%=drpDoctor.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Doctor";
                document.getElementById('<%=drpDoctor.ClientID%>').focus;
                return false;
            }


            if (document.getElementById('<%=drpPatientType.ClientID%>').value == "Credit") {
                if (document.getElementById('<%=drpCompany.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Company";
                    document.getElementById('<%=drpCompany.ClientID%>').focus;
                    return false;
                }
            }
        }

        function DeleteVal() {
          
            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter File No";
                document.getElementById('<%=txtFileNo.ClientID%>').focus;
                return false;
            }

            if (document.getElementById('<%=txtVisitDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Visit Date";
                  document.getElementById('<%=txtVisitDate.ClientID%>').focus;
                  return false;
              }

              if (isDate(document.getElementById('<%=txtVisitDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtFromDate.ClientID%>').focus()
                return false;

              }
           
            var isDelCompany = window.confirm('Do you want to delete Visit Dtls.?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }


            return true

        }
    </script>

    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
            // alert(today);
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <input type="hidden" id="hidPermission" runat="server" value="9" />
        <asp:HiddenField ID="hidLocalDate" runat="server" />

        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </td>
            </tr>

        </table>
        <div style="height: 5px;"></div>
        <div style="padding-top: 0px; width: 80%; height: auto; overflow: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
            <table width="1000PX" cellpadding="5" cellspacing="5">
                <tr>
                    <td class="lblCaption1" style="width: 70px; height: 30px;">From
                    </td>
                    <td>
                        <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                            Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        <asp:TextBox ID="txtFromTime" runat="server" Width="30px" MaxLength="5" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                        &nbsp;

 
                    <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                        Text="To"></asp:Label>

                        <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                            Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                        <asp:TextBox ID="txtToTime" runat="server" Width="30px" MaxLength="5" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>


                    </td>
                    <td class="lblCaption1">Doctor
                    </td>
                    <td>
                        <asp:DropDownList ID="drpSrcDoctor" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:DropDownList>
                    </td>




                    <td align="right">

                        <asp:Button ID="btnFind" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                            OnClick="btnFind_Click" Text="Refresh" />

                    </td>

                </tr>
                <tr>
                    <td class="lblCaption1" style="width: 70px; height: 30px;">File No 
                    </td>
                    <td>
                        <asp:TextBox ID="txtSrcFileNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" MaxLength="15" Width="110px"></asp:TextBox>
                    </td>
                    <td class="lblCaption1">Patient Name 
                    </td>
                    <td>
                        <asp:TextBox ID="txtSrcName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:TextBox>
                    </td>

                </tr>


            </table>
        </div>
        
      
        <div style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvGridView" runat="server"
                        AllowSorting="True" AutoGenerateColumns="False" CellPadding="2" CellSpacing="10"
                        EnableModelValidation="True" Width="100%" GridLines="Horizontal"  >
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />

                        <Columns>
                            <asp:TemplateField HeaderText="FILE NO" SortExpression="HPV_PT_Id">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                        <asp:Label ID="lblVisitType" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_VISIT_TYPE") %>'></asp:Label>
                                        <asp:Label ID="lblTokenNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_DR_TOKEN") %>'></asp:Label>

                                        <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NAME" SortExpression="HPV_PT_NAME">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DATE" SortExpression="HPV_DATE">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDtDesc" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblVisitDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DATEDesc") %>'></asp:Label>
                                        <asp:Label ID="lblVisitTime" CssClass="GridRow" Style="text-align: right; padding-right: 5px;" runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>

                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MOBILE NO" SortExpression="HPV_MOBILE">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkMob" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_MOBILE") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DOCTOR" SortExpression="HPV_DR_NAME">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblDrID" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HPV_DR_ID") %>'></asp:Label>
                                        <asp:Label ID="lblDrDeptId" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                        <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DEPT." SortExpression="HPV_DR_NAME">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDepName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="Label5" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PT TYPE" SortExpression="HPV_PT_TYPEDesc">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkType" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblPTType" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HPV_PT_TYPE") %>'></asp:Label>
                                        <asp:Label ID="lblPTTypeDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COMPANY" SortExpression="HPV_COMP_NAME">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCompName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblCompanyID" CssClass="GridRow" Visible="false" runat="server" Text='<%# Bind("HPV_COMP_ID") %>'></asp:Label>
                                        <asp:Label ID="Label9" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EMR ID" SortExpression="HPV_EMR_ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEMRId" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblEMRId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" SortExpression="HPV_COMP_NAME">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEMRStatus" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblEMRStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_EMR_STATUSDesc") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CREATED BY" SortExpression="HPV_CREATED_USER">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkUser" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblUser" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_CREATED_USER") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CREATED DATE" SortExpression="HPV_CREATED_DATEDesc">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                        <asp:Label ID="lblCreatedDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_CREATED_DATEDesc") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <div style="height: 5px;"></div>
        <div style="padding-top: 0px; width: 80%; height: auto; overflow: auto; border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <input type="hidden" id="hidSeqNo" runat="server" />
                    <input type="hidden" id="hidEMRId" runat="server" />
                    <input type="hidden" id="hidDrDept" runat="server" />

                    <input type="hidden" id="hidOldDrId" runat="server" />
                    <input type="hidden" id="hidOldVisitDt" runat="server" />
                    <input type="hidden" id="hidCreatedDate" runat="server" />

                    <table width="1000px">
                        <tr>
                            <td class="lblCaption1" style="width: 70px; height: 30px;">File No 
                            </td>
                            <td>
                                <asp:TextBox ID="txtFileNo" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" MaxLength="15" Width="110px" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Patient Name 
                            </td>
                            <td>
                                <asp:TextBox ID="txtPTName" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:TextBox>
                            </td>
                            <td class="lblCaption1">Mobile No
                            </td>
                            <td>
                                <asp:TextBox ID="txtMobileNo" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" width="100px">Visit Date
                            </td>
                            <td width="200px">
                                <asp:TextBox ID="txtVisitDate" runat="server" Width="75px" Height="20px" MaxLength="10" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                    Enabled="True" TargetControlID="txtVisitDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtVisitDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                <asp:TextBox ID="txtVisitTime" runat="server" Width="30px" Height="20px" MaxLength="5" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtVisitTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                <asp:DropDownList ID="drpTime" runat="server" CssClass="label" Style="font-size: 10px;" Width="50px" BorderWidth="1px" BorderColor="#CCCCCC">
                                    <asp:ListItem Value="AM" Selected>AM</asp:ListItem>
                                    <asp:ListItem Value="PM">PM</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="lblCaption1" width="100px">Doctor
                            </td>
                            <td>
                                <asp:DropDownList ID="drpDoctor" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                           

                              <td class="lblCaption1">Patient Type
                            </td>
                            <td>
                                <asp:DropDownList ID="drpPatientType" runat="server" CssClass="TextBoxStyle" Width="250px" BorderWidth="1px" BorderColor="#CCCCCC" AutoPostBack="true" OnSelectedIndexChanged="drpPatientType_SelectedIndexChanged">
                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                                     <asp:ListItem Value="Customer">Customer</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Visit Type
                            </td>
                            <td>
                                <asp:DropDownList ID="drpVisitType" runat="server" CssClass="TextBoxStyle" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC">
                                    <asp:ListItem Value="New" Selected>New</asp:ListItem>
                                    <asp:ListItem Value="Old">Established</asp:ListItem>
                                    <asp:ListItem Value="Revisit">Followup </asp:ListItem>
                                </asp:DropDownList>
                            </td>
                           <td class="lblCaption1" width="100px">Company / Customer
                            </td>
                            <td>
                                <asp:DropDownList ID="drpCompany" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:DropDownList>
                            </td>
                            <td class="lblCaption1">Token No
                            </td>
                            <td>
                                <asp:TextBox ID="txtTokenNo" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                               <td class="lblCaption1">Reason 
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="txtReason" runat="server" CssClass="TextBoxStyle" BorderWidth="1px" BorderColor="#CCCCCC" Width="600px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br />

                </ContentTemplate>
            </asp:UpdatePanel>
            <table width="1000px">
                <tr>
                    <td>
                        <asp:Button ID="btnCreate" runat="server" CssClass="button red small" Visible="false"
                            OnClick="btnCreate_Click" Text="Create" OnClientClick="return SaveVal();" />

                        <asp:Button ID="btnUpdate" runat="server" CssClass="button red small" Visible="false"
                            OnClick="btnUpdate_Click" Text="Update" ToolTip="Update the Selected Visit." OnClientClick="return SaveVal();" />

                        <asp:Button ID="btnDelete" runat="server" CssClass="button red small" Visible="false" OnClientClick="return DeleteVal();"
                            OnClick="btnDelete_Click" Text="Delete" ToolTip="Delete the Selected Visit." />

                        <asp:Button ID="btnClear" runat="server" CssClass="button gray small"
                            OnClick="btnClear_Click" Text="Clear" />
                    </td>
                </tr>
            </table>

        </div>


    </div>



</asp:Content>
