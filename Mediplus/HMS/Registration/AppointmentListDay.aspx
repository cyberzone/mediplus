﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppointmentListDay.aspx.cs" Inherits="Mediplus.HMS.Registration.AppointmentListDay" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script src="../Validation.js" type="text/javascript"></script>

    <script>

        function ShowAppPopup() {

            document.getElementById("divAppPopup").style.display = 'block';


        }

        function HideAppdPopup() {

            document.getElementById("divAppPopup").style.display = 'none';

        }

    </script>
</head>


<body>
    <form id="form1" runat="server">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:HiddenField ID="hidStatus" runat="server" />
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div style="width: 100%; border: groove; border-width: thin;">
            <table width="100%">
                <tr>

                    <td class="lblCaption1">From Date:
                    </td>
                    <td align="left">
                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFromDate" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">To Date:
                    </td>
                    <td align="left">
                        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtToDate" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Status:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpStatus" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                      
                        <td>
                            <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="250px"   >
                            </asp:DropDownList>
                        </td>

                </tr>
                <tr>
                    <td class="lblCaption1">File No:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFileNo" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Name:
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtPTName" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Mobile:
                    </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtMobile" runat="server" Width="150px" Height="22px" CssClass="TextBoxStyle"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                     <td>
                        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle" Width="250px">
                                    <asp:ListItem Text="--- PT. Gender ---" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                    <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                    <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>

                  
                    <td class="lblCaption1">Room </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpRoom" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                     <td colspan="4">
                         </td>
                    <td  >
                        <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSearch" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small"
                                    OnClick="btnSearch_Click" Text="Search" />
                                <asp:Button ID="btnClear" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small"
                                    OnClick="btnClear_Click" Text="Clear" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-top: 0px; width: 100%; height: 500px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
            <table width="100%">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvAppointmentData" runat="server" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="100%" PageSize="200" CellPadding="2" CellSpacing="10"   GridLines="Horizontal">
                                    <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                    <RowStyle CssClass="GridRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="20px" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                    OnClick="Select_Click" />&nbsp;&nbsp;
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DATE" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGender" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_SEX") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblCategory" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_PT_CATEGORY") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblPTType" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_APP_PT_TYPE") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblAppointmentID" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_APPOINTMENTID") %>' Visible="false"></asp:Label>


                                                <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("AppintmentDate") %>'></asp:Label>
                                                <asp:Label ID="lblTimeSt" CssClass="GridRow" runat="server" Text='<%# Bind("AppintmentSTTime") %>'></asp:Label>
                                                -   
                                            <asp:Label ID="lblTimeEnd" CssClass="GridRow" runat="server" Text='<%# Bind("AppintmentFITime") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FILE #" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>

                                                <asp:Label ID="lblFileNo" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_FILENUMBER") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NAME" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>


                                                <asp:Label ID="lblFName" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_PT_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblLName" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_LASTNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="MOBILE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_MOBILENO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SERVICE">
                                            <ItemTemplate>


                                                <asp:Label ID="lblService" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_SERVICE") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="STATUS ">
                                            <ItemTemplate>

                                                 <asp:Label ID="lblAppStatusValue" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_STATUS") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblAppStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HAS_STATUS") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="REMARKS">
                                            <ItemTemplate>


                                                <asp:Label ID="lblRemarks" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_REMARKS") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="DOCTOR">
                                            <ItemTemplate>


                                                <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_DR_NAME") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Room">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRoom" CssClass="GridRow" runat="server" Text='<%# Bind("HAM_ROOM") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>



                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>

        </div>
        <div id="divAppPopup" style="display: none; overflow: hidden; border: groove; height: 300px; width: 700px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 100px; top: 200px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top; width: 50%;"></td>
                    <td align="right" style="vertical-align: top; width: 50%;">

                        <input type="button" id="Button3" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideAppdPopup()" />
                    </td>
                </tr>
            </table>

            <table style="width: 100%;" cellpadding="5" cellspacing="5" border="0">
                <tr>
                    <td class="TDStyle">Date & Time  
                    </td>

                    <td class="lblCaption1">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppDateTime" CssClass="lblCaption1" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                    <td class="TDStyle">File #  
                    </td>
                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>

                                <asp:TextBox ID="txtAppFileNo" CssClass="lblCaption1" runat="server"></asp:TextBox>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                </tr>
                <tr>
                    <td class="TDStyle">Name 
                    </td>

                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppPTName" CssClass="lblCaption1" runat="server"></asp:Label>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">Service 
                    </td>
                    <td class="lblCaption1">

                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppService" CssClass="lblCaption1" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                </tr>
                <tr>
                    <td class="TDStyle">Doctor
                    </td>
                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppDoctor" CssClass="lblCaption1" runat="server"></asp:Label>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">Status 
                    </td>
                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>

                               <asp:DropDownList ID="drpAppStatus" CssClass="lblCaption1" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" >
                            </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                </tr>
                <tr>
                    <td class="TDStyle">Category
                    </td>
                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppCategory" CssClass="lblCaption1" runat="server"></asp:Label>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">Gender
                    </td>
                    <td class="lblCaption1">

                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppGender" CssClass="lblCaption1" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>

                </tr>
                <tr>
                    <td class="TDStyle">PT. Type
                    </td>
                    <td class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppPTType" CssClass="lblCaption1" runat="server"></asp:Label>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TDStyle">Room
                    </td>
                    <td class="lblCaption1">

                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppRoom" CssClass="lblCaption1" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>

                </tr>
                <tr>

                    <td class="TDStyle">Remarks
                    </td>
                    <td class="lblCaption1" colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>

                                <asp:Label ID="lblAppRemarks" CssClass="lblCaption1" runat="server"></asp:Label>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>


                </tr>
                <tr>
                    <td colspan="4" style="text-align:right;">
                        <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                            <ContentTemplate>
                                 <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" OnClientClick="return val();" />
                         
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>

        <br />
    </form>
</body>
</html>
