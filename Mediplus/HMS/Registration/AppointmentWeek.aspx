﻿<%@ page title="" language="C#" masterpagefile="~/Site2.Master" autoeventwireup="true" codebehind="AppointmentWeek.aspx.cs" inherits="Mediplus.HMS.Registration.AppointmentWeek" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">

        function PatientPopup1(AppId, strTime, DRId, strDate) {
            var win = window.open("AppointmentPopup.aspx?PageName=ApptWeek&AppId=" + AppId + "&Date=" + strDate + "&Time=" + strTime + "&DrId=" + DRId, "newwin", "top=200,left=270,height=350,width=750,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }


    </script>


    <style type="text/css">
        .Header {
            font:bold 11px/100% Arial;background-color:#4fbdf0;color:#ffffff;text-align:Center;height:30px;width:100px;border-color:black;
        }
         .RowHeader {
            font:bold 11px/100% Arial;height:15px; text-align: center;border-color:black;
        }

        .Content {
            font: 11px/100% Arial;
            text-decoration: none;
            color: black;
            text-align: center;
            vertical-align:middle;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <input type="hidden" id="hidPermission" runat="server" value="9" />
    <asp:HiddenField ID="hidStatus" runat="server" />
    <asp:HiddenField ID="hidPrevDate" runat="server" />
    <asp:HiddenField ID="hidNextDate" runat="server" />
    <table >
        <tr>
            <td class="PageHeader">Appointment
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="600px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>

            <td class="label">Doctor
            </td>
            <td>
                <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged" BorderColor="#cccccc" Width="300px">
                </asp:DropDownList>
            </td>
            <td align="right">
                <asp:RadioButtonList ID="radAppStatus" runat="server" Font-Bold="true" CssClass="label" AutoPostBack="true" Width="400px" RepeatDirection="Horizontal" OnSelectedIndexChanged="radAppStatus_SelectedIndexChanged">
                    <asp:ListItem Text="Appointment List" Value="A" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Waiting List" Value="Waiting"></asp:ListItem>
                    <asp:ListItem Text="Cancelled Appointment" Value="Cancelled"></asp:ListItem>
                </asp:RadioButtonList>

            </td>

        </tr>
    </table>

    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <contenttemplate>
                         <asp:Button ID="btnPrev" runat="server" Text="PREV" CssClass="button orange small" Width="100px"    OnClick="btnPrev_Click" />
                   </contenttemplate>
                    <triggers>
                          <asp:PostBackTrigger ControlID="btnPrev" />
                     </triggers>
                </asp:UpdatePanel>
            </td>
            <td align="right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <contenttemplate>
                          <asp:Button ID="btnNext" runat="server" Text="NEXT" CssClass="button orange small" Width="100px"    OnClick="btnNext_Click" />
                    </contenttemplate>
                    <triggers>
                           <asp:PostBackTrigger ControlID="btnNext" />
                    </triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <%=strData%>

            </td>
        </tr>

    </table>
    <br />
</asp:Content>
