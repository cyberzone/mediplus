﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="TreatmentAppointmentSearch.aspx.cs" Inherits="Mediplus.HMS.Registration.TreatmentAppointmentSearch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>



    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ShowPackageDetailReport() {

          
            var Report =   "PackageDetails.rpt";


            var dtFrom = document.getElementById('<%=txtSrcFromDate.ClientID%>').value
            var arrFromDate = dtFrom.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }


            var dtTo = document.getElementById('<%=txtSrcToDate.ClientID%>').value
            var arrToDate = dtTo.split('/');
            var Date2;
            if (arrToDate.length > 1) {

                Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            var FileNo = document.getElementById('<%=txtSrcFileNo.ClientID%>').value

            var Criteria = " 1=1  ";


            if (FileNo != "") {

                Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'';
            }
           
           
            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'

             

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria + '&Type=pdf', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

           
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 400px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>

    <div style="width: 80%">
        <table style="width: 100%">
            <tr>
                <td align="left">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblPageHeader" runat="server" CssClass="PageHeader" Text="Treatment Appointment Search"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right"></td>
            </tr>
        </table>

        <table runat="server" id="Table1" cellpadding="5" cellspacing="5" style="width: 100%">
            <tr>

                <td class="lblCaption1">From Date  
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFromDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">To Date  
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcToDate" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" >File No.
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFileNo" runat="server" Width="100%" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                </tr>
            <tr>
                <td class="lblCaption1">Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFileName" runat="server" Width="100%" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            
                <td class="lblCaption1" >Staff</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpSrcDoctor" runat="server" CssClass="TextBoxStyle" Width="100%"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Status
                </td>
                  <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpSrcTreatTranStatus" runat="server" CssClass="TextBoxStyle" Width="100%">
                                <asp:ListItem Text="--- All ---" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                                <asp:ListItem Text="Inprocess" Value="Inprocess"></asp:ListItem>
                                <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                            </asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                </tr>
            <tr>
                  <td class="lblCaption1"  >Invoice No.
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" Width="100%"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                 <td class="lblCaption1">Package
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpSrcPackages" runat="server" CssClass="TextBoxStyle" Width="100%">
                            </asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td></td>
              
               
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button red small" Width="100%" OnClick="btnSearch_Click" />
                </td>

            </tr>

        </table>
        <table runat="server" id="Table2" cellpadding="5" cellspacing="5" style="width: 100%">
            <tr>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            Total   :
                            <asp:Label ID="lblTotalApp" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp; &nbsp;
                            Completed :
                            <asp:Label ID="lblTotalCompleted" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>&nbsp; &nbsp; &nbsp;
                            Inprocess  :
                            <asp:Label ID="lblTotalInProcess" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>&nbsp; &nbsp; &nbsp;
                            Open :
                            <asp:Label ID="lblTotalOpen" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="text-align:right;">
                     <input type="button" id="btnShowReport" runat="server"  style="width: 150px;" class="button gray small" value="Package Details Report" onclick="ShowPackageDetailReport();" />
                </td>
            </tr>
        </table>
        <div style="padding-top: 0px; width: 100%; overflow: auto; height: 500px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvTreatmentTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0" OnRowDataBound="gvTreatmentTrans_RowDataBound" OnSorting="gvTreatmentTrans_Sorting">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="SL NO." HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_SL_NO">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatSerialNo" CssClass="label" Font-Size="12px" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TREAT. ID" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_TREATMENT_ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatTransSLNO" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_SL_NO") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvTreatTransID" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAT_TREATMENT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="INV. ID" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAM_INVOICE_ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatInvoiceID" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAM_INVOICE_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DATE & TIME" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_DATETimeDesc">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_DATEDesc") %>'></asp:Label>
                                    &nbsp;
                                        <asp:Label ID="lblgvTreatTime" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAT_DATETimeDesc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FILE NO" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAM_PT_ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatFileNo" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAM_PT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NAME" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAM_PT_NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatFileName" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAM_PT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PACKAGE" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_PACKAGE_NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatPackageID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_PACKAGE_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvTreatPackageName" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAT_PACKAGE_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SERVICE" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_SERV_DESC">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatServCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_SERV_CODE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvTreatServName" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAT_SERV_DESC") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STAFF" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_DR_NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatDrCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_DR_CODE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvTreatDrName" CssClass="label" Font-Size="12px" runat="server" Text='<%# Bind("HTAT_DR_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="Left" SortExpression="HTAT_STATUS">
                                <ItemTemplate>
                                    <asp:Label ID="lblgvTreatStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HTAT_STATUS") %>' Visible="false"></asp:Label>
                                    <asp:DropDownList ID="drpTreatTranStatus" runat="server" CssClass="TextBoxStyle" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="drpTreatTranStatus_SelectedIndexChanged">
                                        <asp:ListItem Text="Open" Value="Open" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Inprocess" Value="Inprocess"></asp:ListItem>
                                        <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </div>
</asp:Content>
