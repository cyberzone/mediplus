﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;



namespace Mediplus.HMS.Registration
{
    public partial class PatientDocScans : System.Web.UI.Page
    {

        DataSet DS;
        dboperations dbo = new dboperations();
        CommonBAL objCom = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_SCANNING' ";

            DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);
            ViewState["EMR_SCAN_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }


        }

        void BindDepartment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' ";
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDepartment.DataSource = ds;
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataValueField = "HDM_DEP_ID";
                drpDepartment.DataBind();



            }
            //drpDepartment.Items.Insert(0, "--- Select ---");
            //drpDepartment.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();
        }

        void BindCategory()
        {
            objCom = new CommonBAL();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESC_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' and ESC_ACTIVE='1' ";
            ds = objCom.EMR_SCAN_CATEGORYGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = ds;
                drpCategory.DataValueField = "ESC_NAME";
                drpCategory.DataTextField = "ESC_NAME";
                drpCategory.DataBind();



            }
            //drpCategory.Items.Insert(0, "--- Select ---");
            //drpCategory.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();
        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


            }

        }

        void StaffDataBind()
        {
            DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + txtDoctorID.Text.Trim() + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("FullName") == false)
                    txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);

            }


        }

        void Clear()
        {
            txtFileNo.Text = "";
            txtName.Text = "";
            if (drpCategory.Items.Count > 0)
                drpCategory.SelectedIndex = 0;

            if (drpDepartment.Items.Count > 0)
                drpDepartment.SelectedIndex = 0;

            txtDoctorID.Text = "";
            txtDoctorName.Text = "";
            txtEMRId.Text = "";
            txtComment.Text = "";
            fileLogo.Dispose();
        }
        #endregion


        #region AutoExtender
        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {

                try
                {
                    BindScreenCustomization();
                    BindDepartment();
                    BindCategory();

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    string PatientId = "", StaffId = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    StaffId = Convert.ToString(Request.QueryString["StaffId"]);
                    if (PatientId != " " && PatientId != null)
                    {
                        txtFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();
                    }

                    if (StaffId != " " && StaffId != null)
                    {
                        txtDoctorID.Text = Convert.ToString(StaffId);
                        StaffDataBind();

                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientDocScans.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {

                PatientDataBind();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDocScans.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {

            string strDoctorID = txtDoctorID.Text;
            if (strDoctorID.Length > 1)
            {
                if (strDoctorID.Substring(strDoctorID.Length - 1, 1) == "~")
                {
                    strDoctorID = strDoctorID.Substring(0, strDoctorID.Length - 1);
                }
            }
            txtDoctorID.Text = strDoctorID;

            string strName = txtDoctorID.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0];
                txtDoctorName.Text = arrName[1];

            }

        }

        protected void txtDoctorName_TextChanged(object sender, EventArgs e)
        {
            string strName = txtDoctorName.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0];
                txtDoctorName.Text = arrName[1];

            }


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileLogo.FileName == "")
                {
                    lblStatus.Text = "Please select the file";
                    goto FundEnd;
                }

                string SCanPath, FileNoFolder = "", DateFolder = "", CatFolder = "", FullPath = "", FileExt = "", DeptName = "";

                SCanPath = Convert.ToString(ViewState["EMR_SCAN_PATH"]);
                FileNoFolder = txtFileNo.Text.Trim();

                FileNoFolder = FileNoFolder.Replace("/", "_");
                FileNoFolder = FileNoFolder.Replace(@"\", "_");

                string strDay = System.DateTime.Today.Day.ToString();
                if (strDay.Length == 1)
                {
                    strDay = "0" + strDay;
                }

                string strMonth = System.DateTime.Today.Month.ToString();
                if (strMonth.Length == 1)
                {
                    strMonth = "0" + strMonth;
                }


                DateFolder = strDay + "-" + strMonth + "-" + System.DateTime.Today.Year.ToString();

                CatFolder = drpCategory.SelectedValue;
                DeptName = drpDepartment.SelectedItem.Text.Trim();

                if (!Directory.Exists(SCanPath + @"\" + FileNoFolder))
                {
                    Directory.CreateDirectory(SCanPath + @"\" + FileNoFolder);
                }

                if (!Directory.Exists(SCanPath + @"\" + FileNoFolder + @"\" + DeptName))
                {
                    Directory.CreateDirectory(SCanPath + @"\" + FileNoFolder + @"\" + DeptName);
                }

                if (!Directory.Exists(SCanPath + @"\" + FileNoFolder + @"\" + DeptName + @"\" + DateFolder))
                {
                    Directory.CreateDirectory(SCanPath + @"\" + FileNoFolder + @"\" + DeptName + @"\" + DateFolder);
                }


                if (!Directory.Exists(SCanPath + @"\" + FileNoFolder + @"\" + DeptName + @"\" + DateFolder + @"\" + CatFolder))
                {
                    Directory.CreateDirectory(SCanPath + @"\" + FileNoFolder + @"\" + DeptName + @"\" + DateFolder + @"\" + CatFolder);
                }


                FileExt = Path.GetExtension(fileLogo.FileName);
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

                FullPath = @SCanPath + @"\" + FileNoFolder + @"\" + DeptName + @"\" + DateFolder + @"\" + CatFolder;

                fileLogo.SaveAs(FullPath + "\\" + strName + FileExt);

                objCom = new CommonBAL();
                objCom.ESF_BRANCH = Convert.ToString(Session["Branch_ID"]);
                objCom.ESF_PT_ID = txtFileNo.Text.Trim();
                objCom.ESF_CAT_ID = drpCategory.SelectedValue;
                objCom.ESF_SLNO = "1";
                objCom.ESF_CREATED_USER = txtDoctorID.Text.Trim();
                objCom.ESF_CREATED_DATE = txtFromDate.Text.Trim();
                objCom.ESF_DEP_ID = drpDepartment.SelectedValue;
                objCom.ESF_EMR_ID = txtEMRId.Text.Trim();
                objCom.ESF_FILENAME = strName + FileExt;
                objCom.ESF_FULLPATH = @"\" + FileNoFolder + @"\" + DeptName + @"\" + DateFolder + @"\" + CatFolder + @"\";
                objCom.ESF_EXTENSION = FileExt;
                objCom.ESF_COMMENT = txtComment.Text;
                objCom.ESF_INVOICE_ID = txtInvoiceID.Text;
                objCom.HMS_SP_EMRScanFIleAdd();

                // Clear();
                lblStatus.Text = "Data Saved!";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FundEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDocScans.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}