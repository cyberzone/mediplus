﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class InsuranceCardExpiry : System.Web.UI.Page
    {
        DataSet DS;
        dboperations dbo = new dboperations();


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND ( HPM_PT_TYPE ='CR' OR  HPM_PT_TYPE ='CREDIT') ";

            if (chkExpiryOnly.Checked == false)
            {
                if (txtExpiryDays.Text.Trim() != "")
                {
                    Criteria += " AND DATEDIFF(DAY,GETDATE(),HPM_CONT_EXPDATE) <=" + txtExpiryDays.Text.Trim() + "  AND  DATEDIFF(DAY,GETDATE(),HPM_CONT_EXPDATE) >0 ";
                }
                else
                {
                    Criteria += " AND DATEDIFF(DAY,GETDATE(),HPM_CONT_EXPDATE) <=7 AND  DATEDIFF(DAY,GETDATE(),HPM_CONT_EXPDATE) >0 ";

                }
            }
            else
            {
                Criteria += " AND  DATEDIFF(DAY,GETDATE(),HPM_CONT_EXPDATE) <0 ";
            }

            // }
            DS = new DataSet();
            DS = dbo.retun_patient_details(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            lblTotal.Text = "0";

            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }


        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                BindGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      InsuranceCardExpiry.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      InsuranceCardExpiry.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      InsuranceCardExpiry.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}