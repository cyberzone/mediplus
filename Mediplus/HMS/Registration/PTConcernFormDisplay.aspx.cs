﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Mediplus_BAL;
using System.IO;

namespace Mediplus.HMS.Registration
{
    public partial class PTConcernFormDisplay : System.Web.UI.Page
    {

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblStatus.Text = "";
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {

           
            txtPTName.Text = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";

            //dboperations dbo = new dboperations();
            //ds = dbo.retun_patient_details(Criteria);

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HPM_PT_FNAME", "HMS_PATIENT_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtPTName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_FNAME"]);
            }

            string FileID = txtFileNo.Text.Trim();

            string Report = "GeneralConsentForm.rpt";
            frmVisit.Src = "../../CReports/ReportViewer.aspx?ReportName=" + Report + "&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'" + FileID + "\'";


      
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            string FileID = txtFileNo.Text.Trim();

            string Report = "GeneralConsentForm.rpt";
            frmVisit.Src = "../../CReports/ReportViewer.aspx?ReportName=" + Report + "&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'" + FileID + "\'";

        }
    }
}