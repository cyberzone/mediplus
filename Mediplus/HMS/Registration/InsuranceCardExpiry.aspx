﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsuranceCardExpiry.aspx.cs" Inherits="Mediplus.HMS.Registration.InsuranceCardExpiry" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />

     <script language="javascript" type="text/javascript">

         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }


    </script>
</head>
<body>
    <form id="form1" runat="server">
          <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
         <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                 <td style="padding-right: 0px; padding-top: 0px; width: 150px; vertical-align: top;">
                    <UC1:LeftMenu ID="ucLeftMenu" runat="server" Menu_Header="Insurance" />
                </td>
                <td style="vertical-align:top;">


    <div>
     <table>
        <tr>
            <td class="PageHeader">Insurance Card Expiry List
               
            </td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
     <table width="100%">
                    <tr>
                        <td style="width:100px" class="lblCaption1">
                             Expiry Days
                        </td>
                        <td>
                             <asp:TextBox ID="txtExpiryDays" runat="server" Text="7"   CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="100px"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                             &nbsp;&nbsp;&nbsp;&nbsp; <asp:CheckBox ID="chkExpiryOnly" runat="server" Text="Show Expiry Cards" CssClass="lblCaption1" Checked="False" />
                            &nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btnFind" runat="server" CssClass="button orange small"  OnClick="btnFind_Click" Text="Refresh" />

                        </td>
                </tr>
      </table>
      <div style="padding-top: 0px; width: 1000px; height: 400px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">
         <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False"  OnSorting="gvGridView_Sorting"
                            EnableModelValidation="True" Width="1050px" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200"  gridlines="None">
                             <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="File No" SortExpression="HPM_PT_ID">
                                    <ItemTemplate>
                                          <asp:Label ID="lblPatientId" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HPM_PT_FNAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFullName" runat="server" OnClick="Select_Click" Text='<%# Bind("FullName") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Company" SortExpression="HPM_INS_COMP_NAME">
                                    <ItemTemplate>
                                         <asp:Label ID="lblCompany" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_INS_COMP_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Expiry Date" SortExpression="HPM_CONT_EXPDATE">
                                    <ItemTemplate>
                                         <asp:Label ID="Label2" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_CONT_EXPDATEDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="DOB" SortExpression="HPM_DOBDesc">
                                    <ItemTemplate>
                                       <asp:Label ID="lblDOB" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_DOBDesc") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone No" SortExpression="HPM_PHONE1">
                                    <ItemTemplate>
                                         <asp:Label ID="lblPhone" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_PHONE1") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile No" SortExpression="HPM_MOBILE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMobile" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="P.O Box No" SortExpression="HPM_POBOX">
                                    <ItemTemplate>
                                         <asp:Label ID="lblPOBox" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_POBOX") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PT Type" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                         <asp:Label ID="inlPTType" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                  <asp:TemplateField HeaderText="Created User" SortExpression="HPM_CREATED_USER">
                                    <ItemTemplate>
                                         <asp:Label ID="Label1" runat="server" OnClick="Select_Click" Text='<%# Bind("HPM_CREATED_USER") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>



     </div>
    <div>
                <asp:updatepanel id="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="70%">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                                        Text="Selected Records :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>

                                <td class="style2">
                                    
                                </td>

                                <td class="style2">
                                    
                                </td>

                                <td class="style2">
                                    
                                </td>

                            </tr>

                        </table>

                    </ContentTemplate>
                </asp:updatepanel>

            </div>
    </div>
                    </td>
                     </tr>
        </table>
    </form>
</body>
</html>
