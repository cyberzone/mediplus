﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Registration
{
    public partial class AppointmentDay : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void AppointmentProcessTimLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../../AppointmentProcessTimLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDrDept()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' ";
            Criteria += " AND  HDM_DEP_NAME NOT IN   ('SURGERY','RECEPTION','PORTERS','PHARMACY','INSURANCE','HOME CARE','CASHIER','CLINIC ASSISTANTS','ADMIN','NURSE','NURSES') ";
            dboperations dbo = new dboperations();
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HDM_DEP_ID";
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataBind();


                if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
                {
                    for (int intCount = 0; intCount < drpDepartment.Items.Count; intCount++)
                    {
                        if (drpDepartment.Items[intCount].Text == Convert.ToString(Session["User_DeptID"]))
                        {
                            drpDepartment.SelectedValue = drpDepartment.Items[intCount].Value;
                            goto ForBoold;
                        }
                    }

                ForBoold: ;

                    drpDepartment.Enabled = false;
                }

            }

            drpDepartment.Items.Insert(0, "--- All ----");
            drpDepartment.Items[0].Value = "";

        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.CommonMastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                if (strType == "APP_ROOM")
                {
                    drpRoom.DataSource = DS;
                    drpRoom.DataTextField = "HCM_DESC";
                    drpRoom.DataValueField = "HCM_CODE";
                    drpRoom.DataBind();
                }
            }


            if (strType == "APP_ROOM")
            {
                drpRoom.Items.Insert(0, "--- All ---");
                drpRoom.Items[0].Value = "0";
            }


        }
        
        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }


            }


        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS' ";



            DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.ScreenCustomizationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_BOOKED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Booked"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_CANCELLED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Cancelled"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_CONFIRMED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Confirmed"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_FILE_DRAWN")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["FileDrawn"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_MEETING")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Meeting"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_NOT_ARRIVED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NotArrived"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_NOTE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Note"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OTHERS")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Others"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OUT_OFFICE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OutOfOffice"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_PT_ARRIVED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PTArrived"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_PT_BILLED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PTBilled"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_SEEN_BY_DR")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SeenByDr"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_SEEN_BY_NURSE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SeenByNurse"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_WAITING")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Waiting"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_FONT_SIZE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["FontSize"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_COMPLETED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Completed"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OT_CASES")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OTCases"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                }
            }

        }

        void BindAppointment()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);




            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' and HSFM_SF_STATUS='Present' AND HSFM_APNT_TIME_INT IS NOT NULL AND HSFM_APNT_TIME_INT !=''  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (drpDepartment.SelectedIndex != 0)
            {
                Criteria += " AND HSFM_DEPT_ID='" + drpDepartment.SelectedItem.Text + "'";
            }

            if (drpStaffSex.SelectedIndex != 0)
            {
                Criteria += " AND HSFM_SEX='" + drpStaffSex.SelectedItem.Text + "'";
            }

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";
            }

            dboperations dbo = new dboperations();

            DataSet DSDoctors = new DataSet();
            DSDoctors = dbo.retun_doctor_detailss(Criteria);


            //Criteria = " 1=1 AND HROS_TYPE='D' ";

            //DataSet DSRoster = new DataSet();
            //DSRoster = dbo.RosterMasterGet(Criteria);

            string strSelectedDate = txtFromDate.Text;  //System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
            string strday = strTSelDate.DayOfWeek.ToString();

            strData.Append("<Table border='1' style='border-color:black;width:100%' ><tr>");
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
            {

                strData.Append("<td width='250px'     class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px' style='display:none;'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

            }
            else
            {

                strData.Append("<td width='250px'  style='display:none;'    class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");
            }

            if (DSDoctors.Tables[0].Rows.Count > 0)
            {
                //style='font-family:Times New Roman,font-size:5;color:#2078c0;text-align:center'>

                for (Int32 i = 0; i <= DSDoctors.Tables[0].Rows.Count - 1; i++)
                {
                    strData.Append("<td   class='Header' >");
                    strData.Append(Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "</BR>" + Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_FNAME"]));
                    strData.Append("</td>");
                }
            }
            strData.Append("</tr>");






            Int32 intday = strTSelDate.Day;

            //switch (strday.ToUpper())
            //{
            //    case "SUNDAY":
            //        intday = 1;
            //        break;
            //    case "MONDAY":
            //        intday = 2;
            //        break;
            //    case "TUESDAY":
            //        intday = 3;
            //        break;
            //    case "WEDNESDAY":
            //        intday = 4;
            //        break;
            //    case "THURSDAY":
            //        intday = 5;
            //        break;
            //    case "FRIDAY":
            //        intday = 6;
            //        break;
            //    case "SATURDAY":
            //        intday = 7;
            //        break;
            //}
            //string strdata;
            //strdata = Convert.ToString(DSRoster.Tables[0].Rows[0]["HROS_DAY" + intday]);
            //if (strdata == "" || strdata == "__:__|__:__|__:__|__:__")
            //{
            //    goto FunEnd;
            //}


            //string[] arrdata;

            //string strFrom1 = "";
            //string strTo1 = "";
            //string strFrom2 = "";
            //string strTo2 = "";

            //if (DSRoster.Tables[0].Rows.Count > 0)
            //{
            //    arrdata = strdata.Split('|');
            //    strFrom1 = arrdata[0];
            //    strFrom1 = strFrom1.Replace(":", "");
            //    strTo1 = arrdata[1];
            //    strTo1 = strTo1.Replace(":", "");
            //    strFrom2 = arrdata[2];
            //    strFrom2 = strFrom2.Replace(":", "");
            //    strTo2 = arrdata[3];
            //    strTo2 = strTo2.Replace(":", "");
            //}

            //Int32 intStart1 = 0;
            //intStart1 = Convert.ToInt32(strFrom1.Replace(":", ""));

            //Int32 intEnd1 = 0;
            //intEnd1 = Convert.ToInt32(strTo1.Replace(":", ""));

            //Int32 intStart2 = 0;
            //intStart2 = Convert.ToInt32(strFrom2.Replace(":", ""));

            //Int32 intEnd2 = 0;
            //intEnd2 = Convert.ToInt32(strTo2.Replace(":", ""));

            // String strrtime;
            String strlTime;
            String strNewTime1 = "";


            //strrtime = strFrom1.Substring(strFrom1.Length - 2, 2);
            strlTime = Convert.ToString(AppointmentStart); //strFrom1.Substring(0, 2);
            strNewTime1 = "00";// strFrom1.Substring(strFrom1.Length - 2, 2);

            AppointmentStart = AppointmentStart * 100;
            AppointmentEnd = AppointmentEnd * 100;

            int[] arrCount = new int[DSDoctors.Tables[0].Rows.Count];

            for (Int32 i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                if (strNewTime1 == "")
                {
                    if (strlTime.Length > 1)
                    {
                        strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                    }
                    else
                    {
                        strNewTime1 = Convert.ToString(AppointmentInterval);
                    }
                }

                // i = i + AppointmentInterval;
                string[] arrlTime = strlTime.Split(':');
                if (arrlTime.Length > 1)
                {
                    strlTime = arrlTime[0];
                }

                if (Convert.ToInt32(strNewTime1) >= 60)
                {
                    strlTime = Convert.ToString(Convert.ToInt32(Convert.ToInt32(strlTime) + 1).ToString("D2")) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                }
                else
                {
                    strlTime = Convert.ToString(Convert.ToInt32(strlTime).ToString("D2") + ":" + strNewTime1);
                }

                i = Convert.ToInt32(strlTime.Replace(":", ""));

                Boolean IsBlocked = false;

                if (GlobalValues.FileDescription == "ALTAIF")
                {
                    if (strlTime == "12:30" || strlTime == "13:00" || strlTime == "13:30" || strlTime == "14:00" || strlTime == "14:30" || strlTime == "15:00" || strlTime == "15:30" || strlTime == "16:00" || strlTime == "16:30" || strlTime == "21:00")
                    {
                        IsBlocked = true;
                    }
                }
                //&& strlTime != "00:00"
                if (Convert.ToInt32(strlTime.Replace(":", "")) <= AppointmentEnd && Convert.ToInt32(strlTime.Replace(":", "")) < 2400)
                {
                    DateTime dtstlTime;
                    dtstlTime = Convert.ToDateTime(strlTime);

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
                    {
                        if (IsBlocked == false)
                        {
                            strData.Append("<tr>");
                            strData.Append("<td   width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(strlTime);
                            strData.Append("</td>");


                            strData.Append("<td  style='display:none'  width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(dtstlTime.ToString(@"hh\:mm tt"));
                            strData.Append("</td>");

                        }
                    }
                    else
                    {
                        if (IsBlocked == false)
                        {

                            strData.Append("<tr>");
                            strData.Append("<td style='display:none'   width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(strlTime);
                            strData.Append("</td>");


                            strData.Append("<td   width='250px' height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   class='RowHeader'>");
                            strData.Append(dtstlTime.ToString(@"hh\:mm tt"));
                            strData.Append("</td>"); ;
                        }

                    }



                    for (int j = 0; j <= DSDoctors.Tables[0].Rows.Count - 1; j++)
                    {
                        if (arrCount[j] > 0)
                        {
                            arrCount[j] = arrCount[j] - 1;
                            goto ForEnd;
                        }

                        Boolean isBolcked = false;


                        BindStaffData(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]));
                        isBolcked = BindCheckBlockedAppointment(i);

                        // DataSet DSAppt = new DataSet();
                        DataRow[] DRAppt;
                        DRAppt = GetAppointment(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]), strlTime);
                        if (DRAppt.Count() > 0)
                        {
                            String strlTime2 = "";
                            Int32 AppointmentStart2 = 0, AppointmentEnd2 = 0;

                            string strStartTime = Convert.ToString(DRAppt[0]["AppintmentSTTime"]);
                            //string[] arrStartTime = strStartTime.Split(':');
                            //strStartTime = arrStartTime[0] +""+ arrStartTime[1];
                            strStartTime = strStartTime.Replace(":", "");


                            string strStartFnish = Convert.ToString(DRAppt[0]["AppintmentFITime"]);
                            //string[] arrStartFnish =strStartFnish.Split(':');
                            //strStartFnish = arrStartFnish[0] + "" + arrStartFnish[1];
                            strStartFnish = strStartFnish.Replace(":", "");

                            AppointmentStart2 = Convert.ToInt32(strStartTime);
                            AppointmentEnd2 = Convert.ToInt32(strStartFnish);

                            strlTime2 = strStartTime.Substring(0, 2);
                            Int32 intRowCount = 0;
                            String strNewTime2 = Convert.ToString(AppointmentStart2);

                            if (strNewTime2.Length > 1)
                            {
                                strNewTime2 = strNewTime2.Substring(strNewTime2.Length - 2, 2);
                            }
                            else
                            {
                                strNewTime2 = "00";
                            }
                            for (Int32 k = AppointmentStart2; k <= AppointmentEnd2; k++)
                            {
                                if (strNewTime2 == "")
                                {
                                    if (strlTime2.Length > 1)
                                    {
                                        strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
                                    }
                                    else
                                    {
                                        strNewTime2 = Convert.ToString(AppointmentInterval);
                                    }
                                }

                                string[] arrlTime2 = strlTime2.Split(':');
                                if (arrlTime2.Length > 1)
                                {
                                    strlTime2 = arrlTime2[0];
                                }

                                if (Convert.ToInt32(strNewTime2) >= 60)
                                {
                                    strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                                }
                                else
                                {
                                    strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
                                }

                                k = Convert.ToInt32(strlTime2.Replace(":", ""));
                                strNewTime2 = "";

                                intRowCount = intRowCount + 1;

                            }

                            arrCount[j] = intRowCount - 2;

                            string strCustColor = "";
                            switch (Convert.ToString(DRAppt[0]["HAM_STATUS"]))
                            {
                                case "1":
                                    strCustColor = Convert.ToString(ViewState["Confirmed"]);
                                    break;
                                case "2":
                                    strCustColor = Convert.ToString(ViewState["Cancelled"]);
                                    break;
                                case "3":
                                    strCustColor = Convert.ToString(ViewState["Waiting"]);
                                    break;
                                case "4":
                                    strCustColor = Convert.ToString(ViewState["Booked"]);
                                    break;
                                case "5":
                                    strCustColor = Convert.ToString(ViewState["FileDrawn"]);
                                    break;
                                case "6":
                                    strCustColor = Convert.ToString(ViewState["Meeting"]);
                                    break;
                                case "7":
                                    strCustColor = Convert.ToString(ViewState["Note"]);
                                    break;
                                case "8":
                                    strCustColor = Convert.ToString(ViewState["OutOfOffice"]);
                                    break;
                                case "9":
                                    strCustColor = Convert.ToString(ViewState["PTArrived"]);
                                    break;
                                case "10":
                                    strCustColor = Convert.ToString(ViewState["PTBilled"]);
                                    break;
                                case "11":
                                    strCustColor = Convert.ToString(ViewState["SeenByDr"]);
                                    break;
                                case "12":
                                    strCustColor = Convert.ToString(ViewState["SeenByNurse"]);
                                    break;
                                case "13":
                                    strCustColor = Convert.ToString(ViewState["Others"]);
                                    break;
                                case "14":
                                    strCustColor = Convert.ToString(ViewState["NotArrived"]);
                                    break;
                                case "15":
                                    strCustColor = Convert.ToString(ViewState["Completed"]);
                                    break;
                                case "16":
                                    strCustColor = Convert.ToString(ViewState["OTCases"]);
                                    break;

                                default:
                                    break;

                            }
                            strCustColor = strCustColor.Replace("|", ",");

                            //    string strColor = Convert.ToString(DRAppt[0]["HAC_COLOR"]);
                            //    strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  rowspan=" + Convert.ToString(intRowCount - 1) + " style=' vertical-align:top;border-color:black;background-color: rgb(" + strCustColor + ");'>");
                            //    strData.Append("<a class='Content'   href=javascript:PatientPopup1(" + Convert.ToString(DRAppt[0]["HAM_APPOINTMENTID"]) + ",0,0,'" + drpDepartment.SelectedValue + "')>" + Convert.ToString(DRAppt[0]["AppintmentSTTime"]) + " - " + Convert.ToString(DRAppt[0]["AppintmentFITime"]) + "(File# " + Convert.ToString(DRAppt[0]["HAM_FILENUMBER"]) + ")<br/> " + Convert.ToString(DRAppt[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DRAppt[0]["HAM_LASTNAME"]) + "<br/> Mob:" + Convert.ToString(DRAppt[0]["HAM_MOBILENO"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAM_SERVICE"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAS_STATUS"]) + "</a>");
                            //    strData.Append("</td>");
                            //}
                            //else
                            //{

                            //    strData.Append("<td  width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "   style='border-color:black;background-color: white;'>");
                            //    strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "','" + drpDepartment.SelectedValue + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
                            //    strData.Append("</td>");

                            //}


                            if (IsBlocked == false)
                            {
                                string strFormatSTTime = "";
                                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                                {
                                    DateTime dtstlTime1;
                                    dtstlTime1 = Convert.ToDateTime(Convert.ToString(DRAppt[0]["AppintmentSTTime"]));
                                    dtstlTime1.ToString(@"hh\:mm tt");


                                    string[] arrSTTime = Convert.ToString(dtstlTime1).Split(' ');
                                    string strSTTime = "";
                                    if (arrSTTime.Length > 0)
                                    {
                                        strSTTime = arrSTTime[1];
                                        strSTTime = strSTTime.Substring(0, strSTTime.LastIndexOf(':'));
                                    }

                                    string[] arrSTTime1 = Convert.ToString(dtstlTime1).Split(' ');


                                    if (arrSTTime.Length > 1)
                                    {
                                        strFormatSTTime = strSTTime + "  " + arrSTTime1[2];

                                    }
                                }
                                else
                                {
                                    strFormatSTTime = Convert.ToString(DRAppt[0]["AppintmentSTTime"]);
                                }



                                string strFormatFITime = "";
                                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                                {
                                    DateTime dtstlTime1;
                                    dtstlTime1 = Convert.ToDateTime(Convert.ToString(DRAppt[0]["AppintmentFITime"]));
                                    dtstlTime1.ToString(@"hh\:mm tt");


                                    string[] arrFITime = Convert.ToString(dtstlTime1).Split(' ');
                                    string strFITime = "";
                                    if (arrFITime.Length > 0)
                                    {
                                        strFITime = arrFITime[1];
                                        strFITime = strFITime.Substring(0, strFITime.LastIndexOf(':'));
                                    }

                                    string[] arrFITime1 = Convert.ToString(dtstlTime1).Split(' ');


                                    if (arrFITime.Length > 1)
                                    {
                                        strFormatFITime = strFITime + "  " + arrFITime1[2];

                                    }
                                }
                                else
                                {
                                    strFormatFITime = Convert.ToString(DRAppt[0]["AppintmentFITime"]);
                                }


                                string strColor = Convert.ToString(DRAppt[0]["HAC_COLOR"]);
                                string strStatus = Convert.ToString(DRAppt[0]["HAS_STATUS"]);


                                string strStatusColor = GetAppointmentStatus(Convert.ToString(DRAppt[0]["HAM_STATUS"]));

                                if (strStatusColor != null && strStatusColor != "")
                                {
                                    strColor = strStatusColor;
                                }

                                strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  rowspan=" + Convert.ToString(intRowCount - 1) + " ondblclick=PatientPopup1(" + Convert.ToString(DRAppt[0]["HAM_APPOINTMENTID"]) + ",0,0,'" + drpDepartment.SelectedValue + "')   align='center' class='TextBoxStyle'   rowspan=" + Convert.ToString(intRowCount - 1) + " style='vertical-align:middle;border-color:black;background-color: " + strColor + ";  cursor:pointer;'>");
                                //  strData.Append("<span class='TextBoxStyle' style='font-weight: bold;color:black;text-decoration:none;'>" + Convert.ToString(DRAppt[0]["AppintmentSTTime"]) + " - " + Convert.ToString(DRAppt[0]["AppintmentFITime"]) + "(File# " + Convert.ToString(DRAppt[0]["HAM_FILENUMBER"]) + ")<br/> " + Convert.ToString(DRAppt[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DRAppt[0]["HAM_LASTNAME"]) + "<br/> Mob:" + Convert.ToString(DRAppt[0]["HAM_MOBILENO"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAM_SERVICE"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAS_STATUS"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAM_REMARKS"]) + "</span>");
                                strData.Append("<span class='TextBoxStyle' style='font-weight: bold;color:black;text-decoration:none;'>" + strFormatSTTime + " - " + strFormatFITime + "&nbsp;(File# " + Convert.ToString(DRAppt[0]["HAM_FILENUMBER"]) + ")<br/> " + Convert.ToString(DRAppt[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DRAppt[0]["HAM_LASTNAME"]) + "<br/> Mob:" + Convert.ToString(DRAppt[0]["HAM_MOBILENO"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAM_SERVICE"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAS_STATUS"]) + "<br/>" + Convert.ToString(DRAppt[0]["HAM_REMARKS"]) + "<br/>User: &nbsp;" + Convert.ToString(DRAppt[0]["HAM_CREATEDUSER"]) + "</span>");
                                strData.Append("<span onclick=AppointmentListDayPopup('" + Convert.ToString(DRAppt[0]["HAM_FILENUMBER"]) + "','Previous')  class='TextBoxStyle' style='font-weight: bold;color:black;text-decoration:none;'><br/>  Previous App.</span>");
                                strData.Append("<span onclick=AppointmentListDayPopup('" + Convert.ToString(DRAppt[0]["HAM_FILENUMBER"]) + "','Upcoming')  class='TextBoxStyle' style='font-weight: bold;color:black;text-decoration:none;'>&nbsp;&nbsp;&nbsp; Upcoming App.</span>");
                                strData.Append("</td>");
                            }
                        }
                        else if (isBolcked == true)
                        {
                            // strData.Append("<td ALIGN='CENTER'  height='50px'   class='AppBox' style='border-color:black;background-color: white;'>");
                            strData.Append("<td  ALIGN='CENTER' width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "    class='TextBoxStyle' style='border-color:black;background-color: white;  cursor:pointer;'>");

                            strData.Append("<span class=lblCaption1>BLOCKED</span>");

                            strData.Append("</td>");
                        }
                        else
                        {
                            if (IsBlocked == false)
                            {

                                strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + " ondblclick=PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "','" + drpDepartment.SelectedValue + "')   class='TextBoxStyle' style='border-color:black;background-color: white;  cursor:pointer;'>");
                                strData.Append("</td>");
                            }

                        }
                    ForEnd: ;
                    }


                    if (IsBlocked == false)
                    {

                        strData.Append("</tr>");
                    }
                }

                strNewTime1 = "";

            }


            strData.Append("<tr>");
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
            {

                strData.Append("<td width='250px'     class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px' style='display:none;'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

            }
            else
            {

                strData.Append("<td width='250px'  style='display:none;'    class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");
            }

            if (DSDoctors.Tables[0].Rows.Count > 0)
            {
                //style='font-family:Times New Roman,font-size:5;color:#2078c0;text-align:center'>

                for (Int32 S = 0; S <= DSDoctors.Tables[0].Rows.Count - 1; S++)
                {
                    strData.Append("<td   class='Header' >");
                    strData.Append(Convert.ToString(DSDoctors.Tables[0].Rows[S]["HSFM_STAFF_ID"]) + "</BR>" + Convert.ToString(DSDoctors.Tables[0].Rows[S]["HSFM_FNAME"]));
                    strData.Append("</td>");
                }
            }
            strData.Append("</tr>");
         





      FunEnd: ;



        }

        DataRow[] GetAppointment(string DRCode, string FromTime)
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            string strlTime;
            strlTime = FromTime;

            string strNewTime1 = "";
            if (strNewTime1 == "")
            {
                strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
            }

            // i = i + AppointmentInterval;


            string[] arrlTime = strlTime.Split(':');
            if (arrlTime.Length > 1)
            {
                strlTime = arrlTime[0];
            }

            if (Convert.ToInt32(strNewTime1) >= 60)
            {
                strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
            }
            else
            {
                strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
            }

            string Criteria = " 1=1 ";

            if (drpAppStatus.SelectedIndex != 0)
            {
                if (drpAppStatus.SelectedIndex != 0) //drpAppStatus.SelectedIndex != 0
                {
                    Criteria += "  AND HAM_STATUS IN (" + hidStatus.Value + ")";
                }
                else
                {
                    Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";
                }
            }

            if (drpSex.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_SEX ='" + drpSex.SelectedValue + "'";
            }

            if (drpRoom.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_ROOM ='" + drpRoom.SelectedValue + "'";
            }


            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += " AND HAM_DR_CODE='" + DRCode + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            Criteria += "   AND   HAM_STARTTIME = '" + strForStartDate + " " + FromTime + ":00'";


            //Criteria += "  AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00' AND   HAM_STARTTIME <= '" + strForStartDate + " " + FromTime + ":00'";
            //Criteria += "  AND HAM_FINISHTIME >= '" + strForStartDate + " " + strlTime + ":00' AND   HAM_FINISHTIME <= '" + strForStartDate + " " + strlTime + ":00'";


            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            //  DSAppt = dbo.AppointmentOutlookGet(Criteria);

            DSAppt = (DataSet)ViewState["DSAllAppointment"];

            DataRow[] DRAppt;
            DRAppt = DSAppt.Tables[0].Select(Criteria);


            return DRAppt;
        }

        void GetAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1  ";

            if (drpAppStatus.SelectedIndex != 0)
            {
                if (drpAppStatus.SelectedIndex != 1)//drpAppStatus.SelectedIndex != 0)
                {
                    Criteria += " AND HAS_STATUS='" + drpAppStatus.SelectedValue + "'";
                }
                else
                {
                    // Criteria += " AND  HAS_STATUS IN('" + drpAppStatus.Items[1].Value + "','" + drpAppStatus.Items[2].Value + "','" + drpAppStatus.Items[3].Value + "' )";
                    Criteria += " AND  HAS_STATUS IN('" + drpAppStatus.Items[3].Value + "','" + drpAppStatus.Items[4].Value + "' )";
                }
            }
            DS = dbo.AppointmentStatusGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }

        string GetAppointmentStatus(string strStatusID)
        {
            string strColor = "";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            if (strStatusID != "")
            {
                Criteria += " AND HAS_ID=" + strStatusID;
            }

            DS = dbo.AppointmentStatusGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                strColor = Convert.ToString(DS.Tables[0].Rows[0]["HAS_COLOR"]);

            }
            return strColor;

        }

        void BindStaffData(string DR_ID)
        {
            ViewState["HSFM_NO_APNT_DATEFROM1"] = "";
            ViewState["HSFM_NO_APNT_FROM1TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO1"] = "";
            ViewState["HSFM_NO_APNT_TO1TIME"] = 0;


            ViewState["HSFM_NO_APNT_DATEFROM2"] = "";
            ViewState["HSFM_NO_APNT_FROM2TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO2"] = "";
            ViewState["HSFM_NO_APNT_TO2TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATEFROM3"] = "";
            ViewState["HSFM_NO_APNT_FROM3TIME"] = 0;

            ViewState["HSFM_NO_APNT_DATETO3"] = "";
            ViewState["HSFM_NO_APNT_TO3TIME"] = 0;

            ViewState["HSFM_APNT_TIME_INT"] = 30;

            DataSet DS = new DataSet();

            string Criteria = "  HSFM_STAFF_ID='" + DR_ID + "'";

            DS = (DataSet)ViewState["DSAllStaff"];

            DataRow[] result = DS.Tables[0].Select(Criteria);
            foreach (DataRow DR in result)
            {
                ViewState["RosterID"] = Convert.ToString(DR["HSFM_ROSTER_ID"]);

                if (DR.IsNull("HSFM_NO_APNT_DATEFROM1Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATEFROM1Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM1"] = Convert.ToString(DR["HSFM_NO_APNT_DATEFROM1Desc"]);
                }

                if (DR.IsNull("HSFM_NO_APNT_FROM1TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_FROM1TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_FROM1TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM1TIME"] = Convert.ToString(DR["HSFM_NO_APNT_FROM1TIME"]).Replace(":", "");

                }
                if (DR.IsNull("HSFM_NO_APNT_DATETO1Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATETO1Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO1"] = Convert.ToString(DR["HSFM_NO_APNT_DATETO1Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_TO1TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_TO1TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_TO1TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO1TIME"] = Convert.ToString(DR["HSFM_NO_APNT_TO1TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATEFROM2Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATEFROM2Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM2"] = Convert.ToString(DR["HSFM_NO_APNT_DATEFROM2Desc"]);
                }

                if (DR.IsNull("HSFM_NO_APNT_FROM2TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_FROM2TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_FROM2TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM2TIME"] = Convert.ToString(DR["HSFM_NO_APNT_FROM2TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATETO2Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATETO2Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO2"] = Convert.ToString(DR["HSFM_NO_APNT_DATETO2Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_TO2TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_TO2TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_TO2TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO2TIME"] = Convert.ToString(DR["HSFM_NO_APNT_TO2TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATEFROM3Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATEFROM3Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATEFROM3"] = Convert.ToString(DR["HSFM_NO_APNT_DATEFROM3Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_FROM3TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_FROM3TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_FROM3TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_FROM3TIME"] = Convert.ToString(DR["HSFM_NO_APNT_FROM3TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_NO_APNT_DATETO3Desc") == false && Convert.ToString(DR["HSFM_NO_APNT_DATETO3Desc"]) != "")
                {
                    ViewState["HSFM_NO_APNT_DATETO3"] = Convert.ToString(DR["HSFM_NO_APNT_DATETO3Desc"]);
                }
                if (DR.IsNull("HSFM_NO_APNT_TO3TIME") == false && Convert.ToString(DR["HSFM_NO_APNT_TO3TIME"]) != "" && Convert.ToString(DR["HSFM_NO_APNT_TO3TIME"]) != "__:__")
                {
                    ViewState["HSFM_NO_APNT_TO3TIME"] = Convert.ToString(DR["HSFM_NO_APNT_TO3TIME"]).Replace(":", "");
                }

                if (DR.IsNull("HSFM_APNT_TIME_INT") == false && Convert.ToString(DR["HSFM_APNT_TIME_INT"]) != "")
                {
                    ViewState["HSFM_APNT_TIME_INT"] = Convert.ToString(DR["HSFM_APNT_TIME_INT"]).Replace(":", "");
                }


            }
        }

        Boolean BindCheckBlockedAppointment(Int32 intTime)
        {


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strDay = "";

            if (arrDate.Length > 1)
            {
                strDay = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
            }
            //DateTime dtAppointmentDate = Convert.ToDateTime(strDay);

            DateTime dtAppointmentDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);



            DateTime dtHSFM_NO_APNT_DATEFROM1;
            DateTime dtHSFM_NO_APNT_DATETO1;

            DateTime dtHSFM_NO_APNT_DATEFROM2;
            DateTime dtHSFM_NO_APNT_DATETO2;


            DateTime dtHSFM_NO_APNT_DATEFROM3;
            DateTime dtHSFM_NO_APNT_DATETO3;


            int intHSFM_NO_APNT_FROM1TIME = 0;
            int intHSFM_NO_APNT_TO1TIME = 0;

            int intHSFM_NO_APNT_FROM2TIME = 0;
            int intHSFM_NO_APNT_TO2TIME = 0;

            int intHSFM_NO_APNT_FROM3TIME = 0;
            int intHSFM_NO_APNT_TO3TIME = 0;






            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM1TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM1TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM1TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO1TIME"]) != "")
            {
                intHSFM_NO_APNT_TO1TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO1TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM1"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO1"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM1 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM1"]);
                dtHSFM_NO_APNT_DATETO1 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO1"]);

                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM1 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO1)
                {
                    if (intHSFM_NO_APNT_FROM1TIME <= intTime && intHSFM_NO_APNT_TO1TIME >= intTime)
                    {
                        return true;
                    }
                }
            }



            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM2TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM2TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM2TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO2TIME"]) != "")
            {
                intHSFM_NO_APNT_TO2TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO2TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM2"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO2"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM2 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM2"]);
                dtHSFM_NO_APNT_DATETO2 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO2"]);
                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM2 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO2)
                {
                    if (intHSFM_NO_APNT_FROM2TIME <= intTime && intHSFM_NO_APNT_TO2TIME >= intTime)
                    {
                        return true;
                    }
                }
            }


            if (Convert.ToString(ViewState["HSFM_NO_APNT_FROM3TIME"]) != "")
            {
                intHSFM_NO_APNT_FROM3TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_FROM3TIME"]);
            }
            if (Convert.ToString(ViewState["HSFM_NO_APNT_TO3TIME"]) != "")
            {
                intHSFM_NO_APNT_TO3TIME = Convert.ToInt32(ViewState["HSFM_NO_APNT_TO3TIME"]);
            }

            if (Convert.ToString(ViewState["HSFM_NO_APNT_DATEFROM3"]) != "" && Convert.ToString(ViewState["HSFM_NO_APNT_DATETO3"]) != "")
            {
                dtHSFM_NO_APNT_DATEFROM3 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATEFROM3"]);
                dtHSFM_NO_APNT_DATETO3 = Convert.ToDateTime(ViewState["HSFM_NO_APNT_DATETO3"]);
                if (dtAppointmentDate >= dtHSFM_NO_APNT_DATEFROM3 && dtAppointmentDate <= dtHSFM_NO_APNT_DATETO3)
                {
                    if (intHSFM_NO_APNT_FROM3TIME <= intTime && intHSFM_NO_APNT_TO3TIME >= intTime)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        void BindAllStaffData()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' and HSFM_SF_STATUS='Present' AND HSFM_APNT_TIME_INT IS NOT NULL AND HSFM_APNT_TIME_INT !='' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);

            ViewState["DSAllStaff"] = DS;

        }


        void BindAllAppointment()
        {



            string Criteria = " 1=1 ";

            if (drpAppStatus.SelectedIndex != 0)
            {
                if (drpAppStatus.SelectedIndex != 1) //drpAppStatus.SelectedIndex != 0
                {
                    Criteria += "  AND HAM_STATUS IN (" + hidStatus.Value + ")";
                }
                else
                {
                    Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";
                }
            }

            if (drpSex.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_SEX ='" + drpSex.SelectedValue + "'";
            }

            if (drpRoom.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_ROOM ='" + drpRoom.SelectedValue + "'";
            }


            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            Criteria += "   AND   HAM_STARTTIME  >= '" + strForStartDate + " 00:00:00' AND  HAM_STARTTIME  <= '" + strForStartDate + " 23:59:59'   ";


            //Criteria += "  AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00' AND   HAM_STARTTIME <= '" + strForStartDate + " " + FromTime + ":00'";
            //Criteria += "  AND HAM_FINISHTIME >= '" + strForStartDate + " " + strlTime + ":00' AND   HAM_FINISHTIME <= '" + strForStartDate + " " + strlTime + ":00'";


            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);
            ViewState["DSAllAppointment"] = DSAppt;



        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnNewApp.Enabled = false;




            }


            if (strPermission == "7")
            {

                btnNewApp.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                try
                {
                    if (hidErrorChecking.Value.ToLower() == "true") AppointmentProcessTimLog(System.DateTime.Now.ToString() + " Page_Load() 1");

                    ViewState["Date"] = "0";
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                    ViewState["DeptID"] = Convert.ToString(Request.QueryString["DeptID"]);

                    if (Convert.ToString(ViewState["Date"]) != "0" && Convert.ToString(ViewState["Date"]) != "")
                    {
                        txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                    }
                    if (GlobalValues.FileDescription == "KHALID")
                    {
                        divBC_GC_CP.Visible = true;
                    }


                    if (hidErrorChecking.Value.ToLower() == "true") AppointmentProcessTimLog(System.DateTime.Now.ToString() + " Page_Load() 2");
                    BindSystemOption();
                    BindScreenCustomization();
                    BindCommonMaster("APP_ROOM");
                    BindDrDept();
                    GetAppointmentStatus();
                    BindAllStaffData();
                    if (hidErrorChecking.Value.ToLower() == "true") AppointmentProcessTimLog(System.DateTime.Now.ToString() + " Page_Load() 3");

                    if (Convert.ToString(ViewState["DeptID"]) != "0" && Convert.ToString(ViewState["DeptID"]) != "")
                    {

                        drpDepartment.SelectedValue = Convert.ToString(ViewState["DeptID"]);
                        drpDepartment_SelectedIndexChanged(drpDepartment, new EventArgs());


                    }
                    else
                    {
                        if (hidErrorChecking.Value.ToLower() == "true") AppointmentProcessTimLog(System.DateTime.Now.ToString() + " Page_Load() 4");

                        BindAllAppointment();
                        BindAppointment();
                    }


                    //if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" || GlobalValues.FileDescription == "ALQUDRA")
                    //{

                      

                   // }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString());
                    TextFileWriting("AppointmentDay.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (drpDepartment.SelectedIndex != 0)
                //{
                    GetAppointmentStatus();

                    BindAllAppointment();
                    BindAppointment();
                //}
                //else
                //{
                //    strData = new StringBuilder();
                //}
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.aspx_txtFromDate_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {

            //if (drpDepartment.SelectedIndex != 0)
            //{
                GetAppointmentStatus();

                BindAllAppointment();
                BindAppointment();
            //}
            //else
            //{
            //    strData = new StringBuilder();
            //}
        }

        protected void drpAppStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (drpDepartment.SelectedIndex != 0)
                //{
                    GetAppointmentStatus();

                    BindAllAppointment();
                    BindAppointment();
                //}
                //else
                //{
                //    strData = new StringBuilder();
                //}

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.drpAppStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpSex_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (drpDepartment.SelectedIndex != 0)
                //{
                    GetAppointmentStatus();

                    BindAllAppointment();
                    BindAppointment();
                //}
                //else
                //{
                //    strData = new StringBuilder();
                //}

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.drpSex_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (drpDepartment.SelectedIndex != 0)
                //{
                    GetAppointmentStatus();

                    BindAllAppointment();
                    BindAppointment();
                //}
                //else
                //{
                //    strData = new StringBuilder();
                //}

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.drpSex_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpStaffSex_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                //if (drpDepartment.SelectedIndex != 0)
                //{
                    GetAppointmentStatus();

                    BindAllAppointment();
                    BindAppointment();
                //}
                //else
                //{
                //    strData = new StringBuilder();
                //}

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.drpSex_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnBC_Click(object sender, EventArgs e)
        {
            drpAppStatus.SelectedValue = "BC";
            GetAppointmentStatus();

            BindAllAppointment();
            BindAppointment();
        }

        protected void btnGC_Click(object sender, EventArgs e)
        {
            drpAppStatus.SelectedValue = "GC";
            GetAppointmentStatus();

            BindAllAppointment();
            BindAppointment();
        }

        protected void btnCP_Click(object sender, EventArgs e)
        {
            drpAppStatus.SelectedValue = "CP";
            GetAppointmentStatus();

            BindAllAppointment();
            BindAppointment();
        }



    }
}