﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Registration
{
    public partial class PatientFilingRequest : System.Web.UI.Page
    {
        # region Variable Declaration
        dboperations dbo = new dboperations();


        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";

            //string strStartDate = System.DateTime.Now.ToShortDateString();
            //string[] arrDate = strStartDate.Split('/');
            //string strForStartDate = "";

            //if (arrDate.Length > 1)
            //{
            //    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            //}


            Criteria += " AND  CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= CONVERT(datetime,convert(varchar(10),getdate(),101),101) ";
            Criteria += "  AND CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= CONVERT(datetime,convert(varchar(10),getdate(),101),101)";

            Criteria += " AND HPV_STATUS='F' ";

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            }

            if (drpDepartment.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DEP_NAME LIKE '" + drpDepartment.SelectedItem.Text + "'";
            }

            if (drpPatientType.SelectedIndex != 0)
            {
                if (drpPatientType.SelectedIndex == 1)
                {
                    Criteria += " AND ( HPV_PT_TYPE ='CA' OR  HPV_PT_TYPE ='CASH') ";
                }
                else if (drpPatientType.SelectedIndex == 2)
                {
                    Criteria += " AND ( HPV_PT_TYPE ='CR' OR  HPV_PT_TYPE ='CREDIT') ";
                }

            }

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.PatientVisitGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            lblTotal.Text = "0";
            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();



                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {


            }


        }

        void BindDept()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' ";
            DataSet ds = new DataSet();
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
                drpDepartment.Items.Insert(0, "--- All ---");
                drpDepartment.Items[0].Value = "0";

            }

        }

        void BIndDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }
            drpDoctor.Items.Insert(0, "--- All ---");
            drpDoctor.Items[0].Value = "0";



        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='FILING' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                gvGridView.Enabled = false;

            }


            if (strPermission == "7")
            {
                gvGridView.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();
                try
                {
                    ViewState["SeqNo"] = "";
                    BIndDoctor();
                    BindDept();
                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            try
            {
                gvGridView.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.drpStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;


            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");
            Label lblTriageReason = (Label)gvScanCard.Cells[0].FindControl("lblTriageReason");

            Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");


            string strDepartment = "|" + lblDeptName.Text + "|";
            string strTraigeHideDepts = Convert.ToString(Session["TRIAGE_HIDE_DEPTS"]);
            bool bolHideTriage = false;

            if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1")
            {
                if (strTraigeHideDepts.Contains(strDepartment) == true)
                {
                    bolHideTriage = true;
                }
            }


            if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1")
            {
                if (lblTriageReason.Text.Trim() == "" && bolHideTriage == false)
                {
                    CommonBAL objCom = new CommonBAL();
                    string Criteria = "HPV_SEQNO=" + lblSeqNo.Text.Trim();
                    string FieldNameWithValues = " HPV_STATUS='T', HPV_EMR_STATUS='T' ";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria);
                }
                else
                {
                    CommonBAL objCom = new CommonBAL();
                    string Criteria = "HPV_SEQNO=" + lblSeqNo.Text.Trim();
                    string FieldNameWithValues = " HPV_STATUS='W' ";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria);
                }


            }
            else
            {
                CommonBAL objCom = new CommonBAL();
                string Criteria = "HPV_SEQNO=" + lblSeqNo.Text.Trim();
                string FieldNameWithValues = " HPV_STATUS='W' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria);

                // dbo.PatientVisitUpdate(lblSeqNo.Text, "W");
            }
            BindGrid();
        }



        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;

                Label lblSeqNo;


                lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");
                dbo.PatientVisitUpdate(lblSeqNo.Text, "D");
                BindGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.Delete_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void gvGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvGridView.PageIndex * gvGridView.PageSize) + e.Row.RowIndex + 1).ToString();
            }


        }

        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.drpDoctor_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientFilingRequest.drpDepartment_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpPatientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.drpPatientType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        #endregion
    }
}