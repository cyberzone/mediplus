﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master"  AutoEventWireup="true" CodeBehind="AppointmentDay.aspx.cs" Inherits="Mediplus.HMS.Registration.AppointmentDay" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

      <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

 
   
    <link rel="stylesheet" type="text/css" href="design/css/home.css">
    <script src="design/js/tabcontent.js" type="text/javascript"></script>
   
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    
  

    <style type="text/css">
        .Header {
            font: bold 11px/100% Arial;
            background-color: #a6a6a6;
            color: #ffffff;
            text-align: Center;
            height: 30px;
            width: 50px;
            border-color: black;
        }

        .RowHeader {
            font: bold 11px/100% Arial;
            text-align: center;
            border-color: black;
        }

        .Content {
            font: 11px/100% Arial;
            text-decoration: none;
            color: black;
            text-align: center;
            vertical-align: middle;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function PatientPopup1(AppId, strTime, DRId, DeptId) {
            var strDate;
            strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
               var win = window.open("AppointmentPopup.aspx?PageName=ApptDay&AppId=" + AppId + "&Date=" + strDate + "&Time=" + strTime + "&DrID=" + DRId + "&DeptID=" + DeptId, "newwin", "top=200,left=270,height=470,width=750,toolbar=no,scrollbars=" + scroll + ",menubar=no");
               win.focus();
           }

        function NewAppointmentPopup() {
            var strDate;
            strDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
            var win = window.open("AppointmentPopup.aspx?PageName=ApptDay&AppId=0&Date=" + strDate + "&Time=0&DrId=0", "newwin", "top=200,left=270,height=470,width=750,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();

        }
        function AppointmentListDayPopup(PatientID, DisplayType) {
             if (PatientID != "" && DisplayType != "") {
                var win = window.open("AppointmentListDay.aspx?PageName=ApptDay&AppId=0&PatientID=" + PatientID + "&DisplayType=" + DisplayType, "newwin", "top=100,left=100,height=700,width=1100,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            }
            else {
                var win = window.open("AppointmentListDay.aspx?PageName=ApptDay&AppId=0", "newwin", "top=100,left=100,height=700,width=1100,toolbar=no,scrollbars=" + scroll + ",menubar=no");

            }
                win.focus();
               
            }

            function ShowAuditLog() {

                var win = window.open('../../AuditLogDisplay.aspx?ScreenID=HMS_APPOINTMENT&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

            }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <input type="hidden" id="hidPermission" runat="server" value="9" />

      <input type="hidden" id="hidErrorChecking" runat="server" value="true" />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
             <td class="PageHeader">Appointment
            </td>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
            <td style="width: 10%; text-align: right;">

              


            </td>
        </tr>
    </table>
        
    <table width="80%">
        <tr>
            <td  >
                <asp:HiddenField ID="hidStatus" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <contenttemplate>

                 <asp:Button ID="btnNewApp" runat="server" style="padding-left:5px;padding-right:5px;width:50px" Text="New" CssClass="button orange small"     OnClientClick="return NewAppointmentPopup()" />
                     </contenttemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <contenttemplate>
                    <asp:Button ID="Button1" runat="server" style="padding-left:5px;padding-right:5px;width:60px" Text="App. List" CssClass="button orange small"     OnClientClick="return AppointmentListDayPopup('','')" />
                            </contenttemplate>
                </asp:UpdatePanel>
            </td>
          
            <td align="left">
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" height="22px" CssClass="TextBoxStyle" MaxLength="10"    onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <asp:calendarextender id="TextBox1_CalendarExtender" runat="server" 
                    enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                </asp:calendarextender>
            </td>
            
            <td>
                <asp:DropDownList ID="drpDepartment" runat="server" Width="150px" CssClass="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpDepartment_SelectedIndexChanged"></asp:DropDownList>
            </td>
              <td class="lblCaption1">Dr. Gender </td>
             <td  >
                               <asp:DropDownList ID="drpStaffSex" runat="server" CssClass="TextBoxStyle" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpStaffSex_SelectedIndexChanged" >
                                     <asp:ListItem Text="--- All ---" Value=""></asp:ListItem>
                                     <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                     <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                    <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                </asp:DropDownList>
              </td>
            <td  class="lblCaption1">Status:
            </td>
            <td >

                 <asp:DropDownList ID="drpAppStatus" runat="server" CssClass="TextBoxStyle" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="drpAppStatus_SelectedIndexChanged" >
                    <asp:ListItem Text="--- All ---" Value=""  Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Appointment List" Value="A"></asp:ListItem>
                    <asp:ListItem Text="Waiting List" Value="Waiting"></asp:ListItem>
                    <asp:ListItem Text="Cancelled Appointment" Value="Cancelled"></asp:ListItem>
                     <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                      <asp:ListItem Text="BC" Value="BC"></asp:ListItem>
                         <asp:ListItem Text="GC" Value="GC"></asp:ListItem>
                         <asp:ListItem Text="CP" Value="CP"></asp:ListItem>
                 </asp:DropDownList>
            </td>
              <td class="lblCaption1">PT. Gender </td>
             <td  >
                               <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpSex_SelectedIndexChanged" >
                                     <asp:ListItem Text="--- All ---" Value=""></asp:ListItem>
                                     <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                     <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                    <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                </asp:DropDownList>
              </td>
               <td class="lblCaption1">Room </td>
            <td>
                 <asp:DropDownList ID="drpRoom" runat="server" CssClass="TextBoxStyle" Width="70px" AutoPostBack="true" OnSelectedIndexChanged="drpRoom_SelectedIndexChanged" >   </asp:DropDownList>
            </td>
         
        </tr>
       
    </table>
            <div id="divBC_GC_CP" style="width:100%;" runat="server" visible="false" >
               <asp:Button ID="btnBC" runat="server" style="padding-left:5px;padding-right:5px;width:50px;cursor:pointer;" Text="BC"  BackColor="#80dfff"    OnClick="btnBC_Click" />
                 <asp:Button ID="btnGC" runat="server" style="padding-left:5px;padding-right:5px;width:50px;cursor:pointer;" Text="GC"  BackColor="#80cc33"    OnClick="btnGC_Click"/>
                 <asp:Button ID="btnCP" runat="server" style="padding-left:5px;padding-right:5px;width:50px;cursor:pointer;" Text="CP"  BackColor="#ff00ff"    OnClick="btnCP_Click"/>
  
            
  </div>
     <div style="padding-top: 0px; width: 1000PX; height: auto; overflow:auto;border: 1px solid #005c7b; padding: 5px; border-radius: 10px;">
        <table width="100%" cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    <%=strData%>

                </td>
            </tr>

        </table>
   </div> 
   
  </asp:Content>
      