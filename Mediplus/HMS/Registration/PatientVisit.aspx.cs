﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Registration
{
    public partial class PatientVisit : System.Web.UI.Page
    {
        # region Variable Declaration
        dboperations dbo = new dboperations();


        #endregion

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "" && txtFromTime.Text == "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }
            else if (txtFromDate.Text != "" && txtFromTime.Text != "")
            {
                Criteria += " AND HPV_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";

            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "" && txtToTime.Text == "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }
            else if (txtToDate.Text != "" && txtToTime.Text != "")
            {
                Criteria += " AND HPV_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";

            }




            //Criteria += " AND  CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >='" + strForStartDate + "' ";
            //Criteria += "  AND CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <='" + txtToDate.Text + "'";

            if (drpType.SelectedIndex != 0)
            {
                Criteria += " AND HPV_TYPE ='" + drpType.SelectedValue + "'";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            }


            if (drpUsers.SelectedIndex != 0)
            {
                Criteria += " AND HPV_CREATED_USER ='" + drpUsers.SelectedValue + "'";
            }

            /*
            if (drpSrcCompany.SelectedIndex != 0)
            {
                Criteria += " AND HPM_BILL_CODE = '" + drpSrcCompany.SelectedValue + "'";
            }
            */


            if (drpSrcCompany.SelectedIndex != 0)
            {

                if (Convert.ToString(ViewState["NetworkdIDs"]) != "")
                {
                    Criteria += " AND ( HPV_COMP_ID in(" + Convert.ToString(ViewState["NetworkdIDs"]) + ") OR HPV_COMP_ID = '" + drpSrcCompany.SelectedValue + "')";
                }
                else
                {
                    Criteria += " AND HPV_COMP_ID = '" + drpSrcCompany.SelectedValue + "'";
                }

            }

            if (drpNetworkName.Items.Count > 0)
            {
                if (drpNetworkName.SelectedIndex != 0)
                {
                    Criteria += " AND HPM_INS_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                }
            }

            if (drpPlanType.Items.Count > 0)
            {
                if (drpPlanType.SelectedIndex != 0)
                {
                    Criteria += " AND HPM_POLICY_TYPE = '" + drpPlanType.SelectedValue + "'";
                }

            }

            if (drpPatientType.SelectedIndex != 0)
            {
                if (drpPatientType.SelectedIndex == 1)
                {
                    Criteria += " AND ( HPV_PT_TYPE ='CA' OR  HPV_PT_TYPE ='CASH') ";
                }
                else if (drpPatientType.SelectedIndex == 2)
                {
                    Criteria += " AND ( HPV_PT_TYPE ='CR' OR  HPV_PT_TYPE ='CREDIT') ";
                }

            }


            if (txtSrcName.Text != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPV_PT_NAME like '" + txtSrcName.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPV_PT_NAME  like '%" + txtSrcName.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPV_PT_NAME  = '" + txtSrcName.Text + "' ";
                }

            }
            if (txtSrcFileNo.Text != "")
            {


                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPV_PT_ID like '" + txtSrcFileNo.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPV_PT_ID  like '%" + txtSrcFileNo.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPV_PT_ID   = '" + txtSrcFileNo.Text + "' ";
                }



            }

            if (txtSrcMobile1.Text != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPV_MOBILE like '" + txtSrcMobile1.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPV_MOBILE  like '%" + txtSrcMobile1.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPV_MOBILE   = '" + txtSrcMobile1.Text + "' ";
                }

            }
            if (drpStatus.SelectedIndex != 0)
            {
                if (drpStatus.SelectedValue == "C")
                {
                    Criteria += " AND (HPV_STATUS= '" + drpStatus.SelectedValue + "'  OR HPV_EMR_STATUS='Y') ";
                }
                else
                {
                    Criteria += " AND HPV_STATUS= '" + drpStatus.SelectedValue + "'   ";
                }

            }


            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.Patient_Master_VisitGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END
            lblNew.Text = "0";
            lblRevisit.Text = "0";
            lblOld.Text = "0";
            lblTotal.Text = "0";
            gvGridView.Visible = false;


            if (DS.Tables[0].Rows.Count > 0)
            {

                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

                DataRow[] TempRow;
                TempRow = DS.Tables[0].Select(" HPV_VISIT_TYPE='New'");

                lblNew.Text = TempRow.Length.ToString();

                DataRow[] TempRow1;
                TempRow1 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Revisit'");

                lblRevisit.Text = TempRow1.Length.ToString();

                DataRow[] TempRow2;
                TempRow2 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Old'");

                lblOld.Text = TempRow2.Length.ToString();


                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {

                gvGridView.DataBind();
            }


        }

        void BindCompany()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcCompany.DataSource = ds;
                drpSrcCompany.DataValueField = "HCM_COMP_ID";
                drpSrcCompany.DataTextField = "HCM_NAME";
                drpSrcCompany.DataBind();


            }
            drpSrcCompany.Items.Insert(0, "--- All ---");
            drpSrcCompany.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";


            Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE = '" + drpSrcCompany.SelectedValue + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            drpNetworkName.Items.Clear();
            drpPlanType.Items.Clear();

            string strNetworkdIDs = "";
            ViewState["NetworkdIDs"] = "";

            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    strNetworkdIDs += "'" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "',";
                }

                strNetworkdIDs = strNetworkdIDs.Substring(0, strNetworkdIDs.Length - 1);
                ViewState["NetworkdIDs"] = strNetworkdIDs;

                drpNetworkName.DataSource = ds;
                drpNetworkName.DataValueField = "HCM_COMP_ID";
                drpNetworkName.DataTextField = "HCM_NAME";
                drpNetworkName.DataBind();

            }

            drpNetworkName.Items.Insert(0, "--- All ---");
            drpNetworkName.Items[0].Value = "0";
            drpNetworkName.SelectedIndex = 0;
        }

        void BindPlanType()
        {
            drpPlanType.Items.Clear();

            if (drpSrcCompany.Items.Count > 0)
            {
                if (drpSrcCompany.SelectedItem.Text != "")
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";
                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpSrcCompany.SelectedValue + "'";
                    }
                    DS = dbo.CompBenefitsGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        drpPlanType.DataSource = DS;
                        if (GlobalValues.FileDescription.ToUpper() == "AMAL")
                        {
                            drpPlanType.DataTextField = "HICM_INS_CAT_NAME";
                        }
                        else
                        {
                            drpPlanType.DataTextField = "HCB_CAT_TYPE";
                        }
                        drpPlanType.DataValueField = "HCB_CAT_TYPE";
                        drpPlanType.DataBind();
                    }


                }


            }

            drpPlanType.Items.Insert(0, "--- All ---");
            drpPlanType.Items[0].Value = "0";

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            DataSet ds = new DataSet();
            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpUsers.DataSource = ds;
                drpUsers.DataValueField = "HUM_USER_ID";
                drpUsers.DataTextField = "HUM_USER_ID";
                drpUsers.DataBind();

            }
            drpUsers.Items.Insert(0, "--- All ---");
            drpUsers.Items[0].Value = "0";

        }

        void BIndDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }
            drpDoctor.Items.Insert(0, "--- All ---");
            drpDoctor.Items[0].Value = "0";



        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='PTVISIT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                gvGridView.Enabled = false;

            }


            if (strPermission == "7")
            {
                gvGridView.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }





            if (hidLocalDate.Value != "")
            {
                Session["LocalDate"] = hidLocalDate.Value;
            }
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    //  SetPermission();
                }

                try
                {
                    ViewState["SeqNo"] = "";
                    ViewState["NetworkdIDs"] = "";
                    BindUsers();
                    BIndDoctor();
                    BindCompany();

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    BindGrid();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }



        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvGridView.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.drpStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {

                BindGrid();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");

            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");


            if (lblEMR_ID.Text != "" && lblEMR_ID.Text != null)
            {
                lblStatus.Text = "This visit already Done the EMR, So you can not edit.";
                goto FunEnd;
            }
            else
            {
                Response.Redirect("PTAndVisitRegistration.aspx?PatientId=" + lblPatientId.Text.Trim() + "&HPVSeqNo=" + lblSeqNo.Text.Trim());
            }



        FunEnd: ;


        }

        protected void gvGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvGridView.PageIndex * gvGridView.PageSize) + e.Row.RowIndex + 1).ToString();
            }


        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpSrcCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                BindSubCompany();
                BindPlanType();



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.drpSrcCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpNetworkName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindPlanType();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.drpNetworkName_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
        #endregion


    }
}