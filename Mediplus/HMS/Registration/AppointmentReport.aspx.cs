﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class AppointmentReport : System.Web.UI.Page
    {
        string strDataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        string strDBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        string strDBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        string strDBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();

        dboperations dbo = new dboperations();
        static string strSessionBranchId;


        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }
            drpDoctor.Items.Insert(0, "---  All  ---");
            drpDoctor.Items[0].Value = "0";

                    }

       
        void BindAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentStatusGet("1=1");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpStatus.DataSource = DS;
                drpStatus.DataTextField = "HAS_STATUS";
                drpStatus.DataValueField = "HAS_ID";
                drpStatus.DataBind();

            }
            drpStatus.Items.Insert(0, "---  All  ---");
            drpStatus.Items[0].Value = "0";



        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                BindDoctor();
                BindAppointmentStatus();
            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string strForStartDate = "";
            string strForToDate = "";
            string dtFrom = txtFromDate.Text.Trim();
            string dtTo = txtToDate.Text.Trim();

            try
            {
               
                String DBString = "";
                DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;


                string strStartDate = txtFromDate.Text;
                string[] arrDate = strStartDate.Split('/');


                if (arrDate.Length > 1)
                {
                    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }




                string strTotDate = txtToDate.Text;
                string[] arrToDate = strTotDate.Split('/');

                if (arrToDate.Length > 1)
                {
                    strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
                }
                string ReportType = "";

                if (chkOTAppointment.Checked == true)
                {
                    ReportType = "OTAppointment";
                }
                else
                {
                    ReportType = "Appointment";
                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAppointment('" + DBString + "','" + txtFileNo.Text.Trim() + "','" + drpStatus.SelectedValue + "','" + drpDoctor.SelectedValue + "','" + strForStartDate + "','" + strForToDate + "','" + ReportType + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentReport.aspx_btnReport_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}