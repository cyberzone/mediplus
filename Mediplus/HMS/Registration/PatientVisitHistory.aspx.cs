﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;
 

namespace Mediplus.HMS.Registration
{
    public partial class PatientVisitHistory : System.Web.UI.Page
    {
        # region Variable Declaration


        dboperations dbo = new dboperations();


        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpUser.DataSource = ds;
                drpUser.DataValueField = "HUM_USER_ID";
                drpUser.DataTextField = "HUM_USER_NAME";
                drpUser.DataBind();
                drpUser.Items.Insert(0, "--- All ---");
                drpUser.Items[0].Value = "0";

            }

        }

  


        void BindDoctors()
        {
            string Criteria = " 1=1 ";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "HSFM_FNAME";
                drpDoctor.DataBind();
                drpDoctor.Items.Insert(0, "--- All ---");
                drpDoctor.Items[0].Value = "0";

            }

        }

        void BindConsentForm()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetConsentForm(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpConsentForm.DataSource = ds;
                drpConsentForm.DataValueField = "Code";
                drpConsentForm.DataTextField = "Name";
                drpConsentForm.DataBind();
            }



        }
        void BindGrid()
        {
            if (ViewState["Value"] == "")
            {
                goto FunEnd;
            }

            string Criteria = " 1=1 ";
            if (ViewState["Value"] != "")
            {
                Criteria += " AND  HPV_PT_ID = '" + ViewState["Value"] + "'";
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND  convert(varchar,HPV_Date,103) >= '" + txtFromDate.Text + "'";
            }

            if (txtFromTime.Text != "")
            {
                Criteria += " AND HPV_Time >= '" + txtFromTime.Text + "'";
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND  convert(varchar,HPV_Date,103) <= '" + txtToDate.Text + "'";
            }

            if (txtToTime.Text != "")
            {
                Criteria += " AND HPV_Time <= '" + txtToTime.Text + "'";
            }



            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID = '" + drpDoctor.SelectedValue + "'";
            }

            if (drpUser.SelectedIndex != 0)
            {
                Criteria += " AND HPV_CREATED_USER = '" + drpUser.SelectedValue + "'";
            }

            if (drpType.SelectedIndex != 0)
            {
                Criteria += " AND HPV_Type = '" + drpType.SelectedValue + "'";
            }

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.PatientVisitGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            lblNew.Text = "0";
            lblRevisit.Text = "0";
            lblOld.Text = "0";
            lblTotal.Text = "0";
            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

                DataRow[] TempRow;
                TempRow = DS.Tables[0].Select(" HPV_VISIT_TYPE='New'");

                lblNew.Text = TempRow.Length.ToString();

                DataRow[] TempRow1;
                TempRow1 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Revisit'");

                lblRevisit.Text = TempRow1.Length.ToString();

                DataRow[] TempRow2;
                TempRow2 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Old'");

                lblOld.Text = TempRow2.Length.ToString();


                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }

        FunEnd: ;
        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='PTHISTORY' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                // btnClear.Enabled = false;
                
                btnDelete.Enabled = false;
              

            }

            if (strPermission == "5")
            {
                btnDelete.Enabled = false;
            }
            if (strPermission == "7")
            {

                 
                btnDelete.Enabled = false;
               



            }

           
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Insurance");
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            //{
            //    Session["ErrorMsg"] = "Permission Denied";
            //    Response.Redirect("../../ErrorPage.aspx");
            //}



            lblStatus.Text = "";
            if (!IsPostBack)
            {

                ////if (Convert.ToString(Session["User_ID"]).ToLower() == "admin")
                ////{
                ////    btnDelete.Visible = true;
                ////}
                ////else
                ////{
                ////    RollTransGet();
                ////}

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }


                BindConsentForm();
                BindDoctors();
                BindUsers();
                ViewState["Value"] = Request.QueryString["Value"].ToString();
                if (ViewState["Value"] != "")
                {
                    BindGrid();
                }

            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                Session["ErrorMsg"] = ex.Message;
                Response.Redirect("ErrorPage.aspx");
            }


        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {

              
               
                for (int i = 0; i < gvGridView.Rows.Count; i++)
                {
                    CheckBox chkSelect;

                    chkSelect = (CheckBox)gvGridView.Rows[i].Cells[0].FindControl("chkSelect");
                    Label lblSeqNo = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblSeqNo");
                    Label lblPatientId = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblPatientId");
                    Label lblDrId = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblDrId");
                    Label lblDateDesc = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblDateDesc");
                    Label lblEMRId = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblEMRId");
                    Label lblCreatedDate = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblCreatedDate");

                    if (chkSelect.Checked == true)
                    {

                        dbOperation objDB = new dbOperation();


                        IDictionary<string, string> Param = new Dictionary<string, string>();
                        Param.Add("HPV_SEQNO", lblSeqNo.Text);
                        Param.Add("HPV_PT_Id", lblPatientId.Text);
                        Param.Add("HPV_DR_ID", lblDrId.Text);
                        Param.Add("HPV_DATE", lblDateDesc.Text);
                        Param.Add("HPV_DELETED_USER", Convert.ToString(Session["User_ID"]));
                        Param.Add("HPV_CREATED_DATE", lblCreatedDate.Text);
                        objDB.ExecuteNonQuery("HMS_SP_PatientVisitDelete", Param);

                        dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "PatientHistory", "TRANSACTION", "Patient Visit Delete, File Name is " + Convert.ToString(lblPatientId.Text) + " Dr id is " + lblDrId.Text + " Visit Date is " + lblDateDesc.Text + " Seq No is " + lblSeqNo.Text + " EMR Id is " + lblEMRId.Text, Convert.ToString(Session["User_ID"]).Trim());


                    }

                }
                lblStatus.Text = "Patient Visit Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                BindGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_btnSaveToDB_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void SelectPrint_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
            Label lblPatientId, lblSeqNo, lblDrId, lblDateDesc;
            lblPatientId = (Label)gvGCGridView.Cells[0].FindControl("lblPatientId");
            lblSeqNo = (Label)gvGCGridView.Cells[0].FindControl("lblSeqNo");
            lblDrId = (Label)gvGCGridView.Cells[0].FindControl("lblDrId");
            lblDateDesc = (Label)gvGCGridView.Cells[0].FindControl("lblDateDesc");

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowConsentFormReportPDF('" + lblPatientId.Text + "','" + lblSeqNo.Text + "','" + lblDrId.Text + "','" + lblDateDesc.Text + "');", true);


        FunEnd: ;

        }

        protected void btnConsentFormPrint_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvGridView.Rows.Count; i++)
            {
                CheckBox chkSelect;
                Label lblSeqNo, lblPatientId, lblDrId, lblDateDesc;
                chkSelect = (CheckBox)gvGridView.Rows[i].Cells[0].FindControl("chkSelect");
                lblSeqNo = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblSeqNo");
                lblPatientId = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblPatientId");
                lblDrId = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblDrId");
                lblDateDesc = (Label)gvGridView.Rows[i].Cells[0].FindControl("lblDateDesc");

                if (chkSelect.Checked == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowConsentFormReportPDF('" + lblPatientId.Text + "','" + lblSeqNo.Text + "','" + lblDrId.Text + "','" + lblDateDesc.Text + "');", true);
                    goto FunEnd;
                }

            }



        FunEnd: ;

        }
    }
}