﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"    AutoEventWireup="true" CodeBehind="PatientVisit.aspx.cs" Inherits="Mediplus.HMS.Registration.PatientVisit" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../style.css" rel="Stylesheet" type="text/css" />
      
     
       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

     <script language="javascript" type="text/javascript">
         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }

         function Val() {

             if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Date";
                document.getElementById('<%=txtFromDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtFromDate.ClientID%>').focus()
                return false;
            }


            if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Date";
                document.getElementById('<%=txtToDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtToDate.ClientID%>').focus()
                  return false;
              }


              return true;

          }
    </script>

      <script language="javascript" type="text/javascript">
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1; //January is 0!

          var hh = today.getHours();
          var min = today.getMinutes();
          var sec = today.getSeconds();

          var yyyy = today.getFullYear();
          if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

          jQuery(document).ready(function () {
              document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
              // alert(today);
          });
         </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
     <input type="hidden" id="hidPermission" runat="server" value="9" />
     <asp:HiddenField ID="hidLocalDate" runat="server" />
    <table >
        <tr>
            <td class="PageHeader">Patient Visit
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <br />
    <table width="1000PX" cellpadding="5" cellspacing="5">
        <tr>
            <td  class="lblCaption1"   style="width: 70px;height:30px;">
                From
            </td>
            <td>
                <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                <asp:TextBox ID="txtFromTime" runat="server" Width="30px" MaxLength="5" CssClass="label"  BorderWidth="1px" BorderColor="#CCCCCC"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                &nbsp;

 
                    <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                        Text="To"></asp:Label>

                <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                <asp:TextBox ID="txtToTime" runat="server" Width="30px" MaxLength="5" CssClass="label"  BorderWidth="1px" BorderColor="#CCCCCC"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>


            </td>
            <td class="lblCaption1" > 
                Doctor
            </td>
            <td>
                <asp:DropDownList ID="drpDoctor" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#CCCCCC" Width="250px"></asp:DropDownList>
            </td>

           
          
            
            <td align="right">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnFind" runat="server" style="padding-left:5px;padding-right:5px;width:60px;"  CssClass="button orange small"
                            OnClick="btnFind_Click" Text="Refresh" OnClientClick="return Val();" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

        </tr>
        <tr>
             <td  class="lblCaption1" style="width: 70px;height:30px;">
                File No 
            </td>
            <td>
                 <asp:textbox id="txtSrcFileNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" maxlength="15" width="110px"></asp:textbox>
            </td>
            <td  class="lblCaption1">
                  Patient Name 
            </td>
            <td>
                 <asp:textbox id="txtSrcName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" width="250px"></asp:textbox>
            </td>
           
        </tr>
        <tr>
            <td class="lblCaption1" > 
               Type
            </td>
            <td>
                <asp:DropDownList ID="drpType" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#CCCCCC" Width="110px">
                    <asp:ListItem Text="--- All ---" Value="0"></asp:ListItem>
                    <asp:ListItem Text="OP" Value="OP" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="IP" Value="IP"></asp:ListItem>

                </asp:DropDownList>
            </td>
             <td  class="lblCaption1">
                Mobile No 
            </td>
            <td colspan="3">
                  <asp:textbox id="txtSrcMobile1" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" width="250px"></asp:textbox>
            </td>
        </tr>
        <tr>
         
             <td  class="lblCaption1">
                  Company 
             </td>
             <td>
                  <asp:dropdownlist id="drpSrcCompany"  BorderWidth="1px" BorderColor="#cccccc" cssclass="label" runat="server" width="250px"  AutoPostBack="true" OnSelectedIndexChanged="drpSrcCompany_SelectedIndexChanged" >
                   </asp:dropdownlist>

             </td>
            <td  class="lblCaption1">
                 <asp:label id="Label6" runat="server" cssclass="lblCaption1"
                                text="Network"></asp:label>
            </td>
           <td>
                <asp:DropDownList ID="drpNetworkName" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpNetworkName_SelectedIndexChanged">
                                        </asp:DropDownList>
            </td>
           
           
        </tr>
        <tr>
              <td>
                  <asp:label id="Label11" runat="server" cssclass="lblCaption1"
                                text="Plan Type"></asp:label>

            </td>
            <td >
                 <asp:DropDownList ID="drpPlanType" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="250px">
                                        </asp:DropDownList>
             </td>
            <td  class="lblCaption1"> Patient Type
            </td>
            <td>
                <asp:DropDownList ID="drpPatientType" runat="server" CssClass="label" Width="250px" BorderWidth="1px" BorderColor="#CCCCCC"  >
                     <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                    <asp:ListItem Value="CA">Cash</asp:ListItem>
                    <asp:ListItem Value="CR">Credit</asp:ListItem>
                 </asp:DropDownList>
            </td>
        </tr>
        <tr>
            
             <td  class="lblCaption1">
                Users 
            </td>
            <td >
                <asp:DropDownList ID="drpUsers" runat="server" CssClass="label" Width="250px"  BorderWidth="1px" BorderColor="#CCCCCC"></asp:DropDownList>
            </td>
             <td  class="lblCaption1">
                Status
            </td>
            <td>
              <asp:DropDownList ID="drpStatus" runat="server" CssClass="label"  Width="250px" BorderWidth="1px" BorderColor="#CCCCCC">
                                  <asp:ListItem Value=""  Selected="true">--- All ---</asp:ListItem>
                                 <asp:ListItem Text="Completed" Value="C"></asp:ListItem>
                                
             </asp:DropDownList>

            </td>

           
        </tr>
    </table>
    <div style="padding-top: 0px; width: 1000PX; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvGridView" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting" OnRowDataBound="gvGridView_RowDataBound"
                    EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200" Width="100%" gridlines="None">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>

                        <asp:TemplateField HeaderText="No" SortExpression="HPV_SEQNO" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List">
                                      <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="30px" Style="text-align: center; padding-right: 2px;" runat="server" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                    <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Name" SortExpression="HPV_PT_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label2" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Mobile No" SortExpression="HPV_MOBILE">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkMob" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label3" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("HPV_MOBILE") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Doctor" SortExpression="HPV_DR_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label4" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dept." SortExpression="HPV_DR_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDepName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label5" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_DEP_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" SortExpression="HPV_DATE">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDtDesc" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label13" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_DATEDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time" SortExpression="HPV_TIME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkTime" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label7" CssClass="GridRow" Width="70px"  Style="text-align: right; padding-right: 5px;" runat="server" Text='<%# Bind("HPV_TIME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PT Type" SortExpression="HPV_PT_TYPEDesc">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkType" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label8" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCompName" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="Label1" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Created User" SortExpression="HPV_CREATED_USER">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkUser" runat="server" OnClick="Edit_Click" ToolTip="Update Waiting List"  >
                                    <asp:Label ID="lblUser" CssClass="GridRow" Width="150px"  runat="server" Text='<%# Bind("HPV_CREATED_USER") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table width="900PX">
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label9" runat="server" CssClass="label"
                                Text="Total :"></asp:Label>
                            &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                        </td>

                        <td class="style2">
                            <asp:Label ID="Label7" runat="server" CssClass="label"
                                Text="New :"></asp:Label>
                            &nbsp;
                                    <asp:Label ID="lblNew" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                        </td>

                        <td class="style2">
                            <asp:Label ID="Label8" runat="server" CssClass="label"
                                Text="Followup(Revisit) :"></asp:Label>
                            &nbsp;
                                    <asp:Label ID="lblRevisit" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                        </td>

                        <td class="style2">
                            <asp:Label ID="Label10" runat="server" CssClass="label"
                                Text="Established(Old) :"></asp:Label>
                            &nbsp;<asp:Label ID="lblOld" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                        </td>

                    </tr>

                </table>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    </div>
               
    

   </asp:Content>
