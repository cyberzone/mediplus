﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site3.Master" AutoEventWireup="true" CodeBehind="PTRegistration1.aspx.cs" Inherits="Mediplus.HMS.Registration.PTRegistration1" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />


    <script src="../../Scripts/PatientRegistration.js" type="text/javascript"></script>
    <script src="../../Scripts/Validation.js" type="text/javascript"></script>

        <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
</style>
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

        <div style="padding-top: 0px; padding-left: 2px; width: 99%;  border-color: #D64535; border-style: solid; border-width: thin;">

            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
                <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Patient Details-1 " Width="100%">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <input type="hidden" id="hidPermission" runat="server" value="9" />
                                <asp:HiddenField ID="hidTokenPrint" runat="server" />
                                <asp:HiddenField ID="hidWaitingList" runat="server" />
                                <asp:HiddenField ID="hidLabelPrint" runat="server" />
                                <asp:HiddenField ID="Year" runat="server" />
                                <asp:HiddenField ID="hidTodayDate" runat="server" />
                                <asp:HiddenField ID="hidTodayTime" runat="server" />
                                <asp:HiddenField ID="hidTodayDBDate" runat="server" />
                                <asp:HiddenField ID="hidLatestVisit" runat="server" />
                                <asp:HiddenField ID="hidLatestVisitHours" runat="server" />
                                <asp:HiddenField ID="hidLastVisitDrId" runat="server" />
                                <asp:HiddenField ID="hidMultipleVisitADay" runat="server" />
                                <asp:HiddenField ID="hidPopup" runat="server" />
                                <asp:HiddenField ID="hidInfoPopup" runat="server" />
                                <asp:HiddenField ID="hidImgFront" runat="server" />
                                <asp:HiddenField ID="hidHospitalName" runat="server" />

                                <asp:HiddenField ID="hidPTVisitSQNo" runat="server" />
                                <asp:HiddenField ID="hidActualFileNo" runat="server" />
                                <asp:HiddenField ID="hidLocalDate" runat="server" />
                                <asp:HiddenField ID="hidSignaturePad" runat="server" Value="WACOM" />

                                <asp:HiddenField ID="hidPateName" runat="server" Value="" />

                               

                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="width: 30%">
                                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:Label ID="lblEFilling" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                        </td>
                                        <td style="float: right; width: 50%">
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" Text="Save" OnClientClick="return Val();"
                                                        OnClick="btnSave_Click"
                                                        Enabled="true" />
                                                    <asp:Button ID="btnClear" runat="server" class="button gray small" Width="100px" Text="Cancel" OnClick="btnClear_Click" OnClientClick="Val1();" />

                                                </ContentTemplate>
                                                
                                            </asp:UpdatePanel>
                                        </td>





                                    </tr>
                                </table>


                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style=" width:50%;vertical-align: top; padding-top: 5px;padding-left:5px;">
                                            <fieldset style="width: 90%;   border: 1px solid #999;border-style :solid;">
                                                <legend class="lblCaption1">General Information</legend>
                                                <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:Button ID="btnNew" runat="server" Style="padding-left: 5px; padding-right: 5px;" Text="New" Visible="false" CssClass="button gray small" Width="50px" OnClick="btnNew_Click" />
                                                                </ContentTemplate>
                                                               
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblHomeCarePT" runat="server" CssClass="lblCaption1" Style="color: red; font-weight: bold;" Text="Home Care Patient" Visible="false"></asp:Label>

                                                        </td>
                                                        <div id="divDept" runat="server" visible="false">
                                                            <td class="lblCaption1" style="height: 40px;">Department
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:DropDownList>
                                                            </td>
                                                        </div>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblVisitType" runat="server" CssClass="lblCaption1" Text="New" ForeColor="Maroon"></asp:Label>
                                                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1" Text="File No"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>

                                                        </td>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtFileNo" runat="server" MaxLength="10" CssClass="TextBoxStyle" Width="70px" AutoPostBack="true" onkeypress="return PatientPopup('FileNo',this.value,event)" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>
                                                                    <asp:CheckBox ID="chkManual" runat="server" CssClass="lblCaption1" OnCheckedChanged="chkManual_CheckedChanged"
                                                                        Text="Manual#" AutoPostBack="True" Checked="false" />

                                                                </ContentTemplate>
                                                               
                                                            </asp:UpdatePanel>


                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label3" runat="server" CssClass="lblCaption1" Text="PT.Type"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpPatientType" runat="server" CssClass="TextBoxStyle" Width="70px" AutoPostBack="true" OnSelectedIndexChanged="drpPatientType_SelectedIndexChanged">
                                                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="Active" Checked="true" />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label4" runat="server" CssClass="lblCaption1" Text="Name"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>

                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="drpTitle" runat="server" CssClass="label" Width="70px" Visible="false">
                                                            </asp:DropDownList>

                                                            <asp:TextBox ID="txtFName" MaxLength="50" runat="server" CssClass="TextBoxStyle" Width="150px" onkeypress="return PatientPopup('Name',this.value,event)" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>


                                                            <asp:TextBox ID="txtMName" MaxLength="50" runat="server" CssClass="TextBoxStyle"   Width="70px" onkeypress="return PatientPopup('Name',this.value,event)" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>



                                                            <asp:TextBox ID="txtLName" MaxLength="50" runat="server" CssClass="TextBoxStyle"   Width="140px" onkeypress="return PatientPopup('Name',this.value,event)" ondblclick="return PatientPopup1('LName',this.value);"></asp:TextBox>


                                                        </td>



                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label7" runat="server" CssClass="lblCaption1" Text="DOB"></asp:Label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtDOB" runat="server" Width="75px" CssClass="TextBoxStyle" MaxLength="10" onblur="AgeCalculation()"></asp:TextBox>
                                                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                                                Enabled="True" TargetControlID="txtDOB" Format="dd/MM/yyyy">
                                                            </asp:CalendarExtender>

                                                            <asp:Label ID="Label120" runat="server" CssClass="label" Text="Age"></asp:Label>
                                                            <asp:TextBox ID="txtAge" runat="server" Width="20px" MaxLength="3" CssClass="TextBoxStyle" onblur="DBOcalculation()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            <asp:TextBox ID="txtMonth" runat="server" Width="20px" MaxLength="2" CssClass="TextBoxStyle"></asp:TextBox>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="lblCaption1">Sex <span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                                <asp:ListItem Text="--- Select ---" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                                                <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                                                <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                                            </asp:DropDownList>


                                                        </td>
                                                        <td class="lblCaption1">
                                                            <asp:Label ID="lblPOBox" runat="server" CssClass="lblCaption1" Text="P O Box"></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:TextBox ID="txtPoBox" runat="server" MaxLength="50" CssClass="TextBoxStyle"
                                                                onkeypress="return PatientPopup('POBox',this.value,event)" Width="150px" ondblclick="return PatientPopup1('POBox',this.value);"></asp:TextBox>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label8" runat="server" CssClass="lblCaption1" Text="Address"></asp:Label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtAddress" CssClass="TextBoxStyle" Style="resize: none;" TextMode="MultiLine" runat="server" Height="30px" Width="98%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label9" runat="server" CssClass="lblCaption1" Text="City"></asp:Label><span style="color: red; font-size: 11px;">*</span>

                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpCity" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <asp:Label ID="Label12" runat="server" CssClass="lblCaption1" Text="Area"></asp:Label>
                                                        </td>
                                                        <td class="auto-style43">
                                                            <asp:DropDownList ID="drpArea" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <asp:Label ID="Label10" runat="server" CssClass="lblCaption1" Text="Country"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpCountry" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label13" runat="server" CssClass="lblCaption1" Text="Nationality"></asp:Label><span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                            </asp:DropDownList>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblBlood" runat="server" CssClass="lblCaption1" Text="Blood"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpBlood" CssClass="TextBoxStyle" runat="server" Width="150px">
                                                                <asp:ListItem Selected="True" Text="--- Select ---" Value="" />
                                                                <asp:ListItem>A +ve</asp:ListItem>
                                                                <asp:ListItem>A -ve</asp:ListItem>
                                                                <asp:ListItem>B +ve</asp:ListItem>
                                                                <asp:ListItem>B -ve</asp:ListItem>
                                                                <asp:ListItem>O +ve</asp:ListItem>
                                                                <asp:ListItem>O -ve</asp:ListItem>
                                                                <asp:ListItem>AB +ve</asp:ListItem>
                                                                <asp:ListItem>AB -ve</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label14" runat="server" CssClass="lblCaption1" Text="Marital Status"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpMStatus" CssClass="TextBoxStyle" runat="server" Width="150px">
                                                                <asp:ListItem Selected="True" Text="--- Select ---" Value="" />
                                                                <asp:ListItem>Single</asp:ListItem>
                                                                <asp:ListItem>Married</asp:ListItem>
                                                                <asp:ListItem>Widowed</asp:ListItem>
                                                                <asp:ListItem>Divorced</asp:ListItem>
                                                                <asp:ListItem>Not Specified</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <asp:DropDownList ID="drpIDCaption" runat="server" CssClass="TextBoxStyle" Width="100px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmiratesID" runat="server" CssClass="TextBoxStyle" MaxLength="18" Width="150px" onkeypress="return PatientPopup('EmiratesID',this.value,event)" ondblclick="return PatientPopup1('EmiratesID',this.value);"></asp:TextBox>
                                                            <%--<asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtEmiratesID" Mask="999-9999-9999999-9" MaskType="None"   CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>--%>
                                                        </td>


                                                        <td>
                                                            <asp:Label ID="Label11" runat="server" CssClass="lblCaption1" Text="Visa Type"></asp:Label><span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpVisaType" CssClass="TextBoxStyle" runat="server" Width="150px">
                                                                <asp:ListItem Selected="True" Text="--- Select ---" Value="" />
                                                                <asp:ListItem>Resident</asp:ListItem>
                                                                <asp:ListItem>Tourist</asp:ListItem>

                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label15" runat="server" CssClass="lblCaption1" Text="Home PH. No."></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPhone1" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Phone',this.value,event)" ondblclick="return PatientPopup1('Phone1',this.value);"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMobile1" runat="server" CssClass="lblCaption1" Text="Mobile1"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMobile1" runat="server" CssClass="TextBoxStyle" CausesValidation="True" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Mobile',this.value,event)" AutoPostBack="true" OnTextChanged="txtMobile1_TextChanged" ondblclick="return PatientPopup1('Mobile1',this.value);"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMobile2" runat="server" CssClass="lblCaption1" Text="Mobile2"></asp:Label>
                                                        </td>
                                                        <td class="auto-style49">
                                                            <asp:TextBox ID="txtMobile2" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Mobile',this.value,event)" ondblclick="return PatientPopup1('Mobile2',this.value);"></asp:TextBox>
                                                        </td>

                                                        <td>
                                                            <asp:Label ID="lblOfficePhNo" runat="server" CssClass="lblCaption1" Text="Office PH"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPhone3" runat="server" CssClass="TextBoxStyle"   Width="150px" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label44" runat="server" CssClass="lblCaption1" Text="Email"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmail" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblFax" runat="server" CssClass="lblCaption1" Text="Fax"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFax" runat="server" CssClass="TextBoxStyle" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <asp:Label ID="lblOccupation" runat="server" CssClass="lblCaption1" Text="Occupation"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtOccupation" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>

                                                        </td>

                                                        <td>
                                                            <asp:Label ID="lblKnowFrom" runat="server" CssClass="lblCaption1"
                                                                Text="Know From"></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:DropDownList ID="drpKnowFrom" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:DropDownList>
                                                        </td>


                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label17" runat="server" CssClass="lblCaption1" Text="Patient Status"></asp:Label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="drpPTStatus" runat="server" CssClass="TextBoxStyle" Width="150px">
                                                                <asp:ListItem Value="Open">Open</asp:ListItem>
                                                                <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label18" runat="server" CssClass="lblCaption1" Text="Hold Message"></asp:Label>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtHoldMsg" CssClass="TextBoxStyle" TextMode="MultiLine" runat="server" Height="30px" Width="98%" Style="resize: none;"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <asp:Label ID="Label19" runat="server" CssClass="lblCaption1"
                                                                Text="Doctor  Information" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="auto-style32">
                                                            <asp:Label ID="Label20" runat="server" CssClass="lblCaption1" Text="Doctor"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td class="auto-style28" colspan="3">
                                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                <ContentTemplate>
                                                                    <div id="divDr" style="visibility: hidden;"></div>
                                                                    <asp:TextBox ID="txtDoctorID" runat="server" CssClass="TextBoxStyle" Width="70px" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()"></asp:TextBox>
                                                                    <asp:TextBox ID="txtDoctorName" runat="server" CssClass="TextBoxStyle" Width="300px" AutoPostBack="true" OnTextChanged="txtDoctorName_TextChanged" MaxLength="50" onblur="return DRNameSelected()"></asp:TextBox>
                                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                                                                    </asp:AutoCompleteExtender>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>

                                                        </td>


                                                    </tr>

                                                    <tr>
                                                        <td class="lblCaption1">
                                                            Department
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDepName" CssClass="TextBoxStyle" runat="server" Width="150px" Enabled="false" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <div id="divToken" runat="server">
                                                            <td>
                                                                <asp:Label ID="Label21" runat="server" CssClass="lblCaption1" Text="Token No:"></asp:Label>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtTokenNo" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="5" Enabled="false"></asp:TextBox>
                                                                <asp:CheckBox ID="chkManualToken" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkManualToken_CheckedChanged"
                                                                    Text="Manual" />

                                                            </td>


                                                        </div>

                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="3">
                                                            <asp:Button ID="btnAppointment" runat="server" CssClass="button gray small" Visible="false" Style="padding-left: 5px; padding-right: 5px; width: 150px;" Text="Check Appt." OnClientClick="AppointmentPopup();" />
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">


                                                    <tr>


                                                        <div id="divCunsult" runat="server" visible="false">
                                                            <td colspan="5" class="lblCaption1" style="color: #0094ff">Last Consultation Date:&nbsp;
                                                         <asp:Label ID="lblLastCunsultDate" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                                &nbsp;&nbsp;&nbsp; Amount:&nbsp;
                                                         <asp:Label ID="lblLastCunsultAmount" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>

                                                            </td>
                                                        </div>
                                                    </tr>

                                                </table>


                                                <table width="100%" border="0" cellpadding="2" cellspacing="2" style="background-color: #efefef;">
                                                    <tr>
                                                        <td colspan="4">
                                                            <asp:TextBox ID="txtLablePrintCount" CssClass="TextBoxStyle" runat="server" Width="20px" MaxLength="2" Text="3" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            <asp:CheckBox ID="chkLabelPrint" runat="server" />

                                                            <asp:Button ID="btnLabelPrint" runat="server" Style="width: 100px;" CssClass="button gray small" Text="Label Print" OnClick="btnLabelPrint_Click" />

                                                            <asp:Button ID="btnIDPrint" runat="server" Style="width: 100px;" CssClass="button gray small" Text="ID Print" OnClick="btnIDPrint_Click" />

                                                            <asp:Button ID="btnMRRPrint" runat="server" Style="width: 100px;" CssClass="button gray small" Text="MRR Print" OnClick="btnMRRPrint_Click" />


                                                            <asp:Button ID="btnTokenPrint" runat="server" CssClass="button gray small" Width="100px" Text="Token Print" OnClick="btnTokenPrint_Click" />

                                                        </td>
                                                    </tr>
                                                    

                                                </table>
                                            </fieldset>
                                        </td>

                                        <td style="width:50%; vertical-align: top; padding-top: 12px; text-align: left;">
                                            <fieldset style="width: 88%;  border: 1px solid #999;border-style :solid;">

                                                <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                          <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                                <ContentTemplate>
                                                             
                                                                       <asp:CheckBox ID="chkFamilyMember" runat="server" Width="120px" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkFamilyMember_CheckedChanged" Text="Family Member" />

                                                            <asp:ImageButton ID="ImgbtnSig1" runat="server" ImageUrl="~/HMS/Images/Sing1.jpg" ToolTip="Signature Scan" Height="20PX" Width="25pX" OnClick="ImgbtnSig1_Click" />

                                                            &nbsp;
                                                        <asp:LinkButton ID="lnkViewSig1" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="lnkViewSig1_Click"></asp:LinkButton>
                                                            &nbsp;&nbsp;&nbsp;
                                               
                                                    <asp:ImageButton ID="btnScan" runat="server" ImageUrl="~/HMS/Images/EmiratesID.Png" ToolTip="Emirate Id Scan" Height="20PX" Width="25pX" OnClick="btnScan_Click" />
                                                            &nbsp; 
                                                        <asp:LinkButton ID="lnkEmIdShow" runat="server" CssClass="lblCaption1" Text="Show" ForeColor="Blue" Style="text-decoration: none; color: #005c7b;" OnClick="lnkEmIdShow_Click"></asp:LinkButton>
                                                             &nbsp; 
                                                        <asp:Button ID="bntFamilyApt" runat="server" CssClass="button gray small" Style="padding-left: 5px; padding-right: 5px; width: 120px;" Text="Check Family Appt." OnClick="bntFamilyApt_Click" />

                                                               </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lnkEmIdShow" />

                                                </Triggers>
                                            </asp:UpdatePanel>     
                                                         

                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <div id="divCompCashPT">
                                                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:CheckBox ID="chkUpdateWaitList" runat="server" CssClass="lblCaption1" Checked="true"
                                                                            Text="Update Waiting List" />
                                                                        &nbsp; &nbsp;
                                                            <asp:CheckBox ID="chkCompCashPT" runat="server" CssClass="lblCaption1"
                                                                Text="Comp. Cash PT" Width="120px" OnCheckedChanged="CheckBox7_CheckedChanged" Checked="false" AutoPostBack="true" />


                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="lblCaption1">

                                                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                                                <ContentTemplate>
                                                                    <div id="divRefDr" style="visibility: hidden;"></div>
                                                                    Ref. Doctor
                                                                <asp:TextBox ID="txtRefDoctorID" runat="server" CssClass="TextBoxStyle" Width="70px" onblur="return RefDRIdSelected()"></asp:TextBox>
                                                                    <asp:TextBox ID="txtRefDoctorName" runat="server" CssClass="TextBoxStyle" Width="250px" MaxLength="50" onblur="return RefDRNameSelected()"></asp:TextBox>
                                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtRefDoctorName" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorName"
                                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divRefDr">
                                                                    </asp:AutoCompleteExtender>

                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>


                                                    </tr>

                                                </table>
                                            </fieldset>
                                            <div id="divOtherInfo" runat="server" visible="false">
                                                <fieldset style="width: 88%;  border: 1px solid #999;border-style :solid;">
                                                    <legend class="lblCaption1">Other Information</legend>

                                                    <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">


                                                        <tr>

                                                            <td>
                                                                <asp:Label ID="Label27" runat="server" CssClass="lblCaption1" Text="Kin Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtKinName" runat="server" CssClass="TextBoxStyle" Width="150px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label24" runat="server" CssClass="lblCaption1" Text="Kin's Relation"></asp:Label>
                                                                &nbsp; &nbsp; &nbsp; &nbsp;
                                            <asp:TextBox ID="txtKinRelation" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label28" runat="server" CssClass="lblCaption1" Text="Father Mob.No."></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtKinPhone" CssClass="TextBoxStyle" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label25" runat="server" CssClass="lblCaption1" Text="Mother Mob.No"></asp:Label>
                                                                &nbsp; &nbsp;
                                            <asp:TextBox ID="txtKinMobile" CssClass="label" runat="server" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label29" runat="server" CssClass="lblCaption1" Text=" No Of Children"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNoOfChildren" CssClass="TextBoxStyle" MaxLength="2" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMobile3" runat="server" CssClass="lblCaption1" Text="Other  Mob.No."></asp:Label>
                                                                &nbsp; &nbsp; &nbsp;
                                        <asp:TextBox ID="txtKinMobile1" CssClass="label" runat="server" Width="100px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </fieldset>
                                            </div>
                                            <div id="divPriority" runat="server">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label86" runat="server" CssClass="lblCaption1" Text="Priority"></asp:Label>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:DropDownList ID="drpPriority" runat="server" CssClass="TextBoxStyle" Width="200px"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </div>

                                            <fieldset style="width: 88%;  border: 1px solid #999;border-style :solid;">
                                                <legend class="lblCaption1">Insurance Details</legend>
                                                <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                                    <tr runat="server" id="trTPA">
                                                        <td style="height: 10px;" class="lblCaption1">TPA
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="drpParentCompany" runat="server" CssClass="TextBoxStyle" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="drpParentCompany_SelectedIndexChanged"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 10px;" class="lblCaption1">Company <span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="drpDefaultCompany" CssClass="TextBoxStyle" runat="server" class="TextBoxStyle" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="drpDefaultCompany_SelectedIndexChanged"></asp:DropDownList>

                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td class="lblCaption1" runat="server" id="tdCaptionNetwork">Network 
                                                        </td>
                                                        <td runat="server" id="tdNetwork" colspan="3">
                                                            <asp:DropDownList ID="drpNetworkName" CssClass="TextBoxStyle" runat="server" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="drpNetworkName_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label5" runat="server" CssClass="lblCaption1" Text="Plan Type"></asp:Label>
                                                            <asp:ImageButton ID="btnHCBShow" runat="server" ImageUrl="~/Images/Zoom.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Category Lookup" Height="20PX" Width="25PX" OnClick="btnHCBShow_Click" />
                                                            <span style="color: red; font-size: 11px;">*</span>
                                                        </td>
                                                        <td colspan="3">

                                                            <asp:DropDownList ID="drpPlanType" CssClass="TextBoxStyle" runat="server" Width="300px" Enabled="true" AutoPostBack="True" OnSelectedIndexChanged="drpPlanType_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                             <asp:ImageButton ID="imgPlanRefresh"  runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 20px; width: 20px;" ToolTip="Plan Type Refresh" OnClick="imgPlanRefresh_Click" />
                                                            <asp:Image ID="ImageButton1" runat="server" ImageUrl="~/Images/New.PNG" BorderStyle="None" Style="cursor: pointer;" ToolTip="Plan Type Add" Height="22PX" Width="22PX" onclick="return BenefitsExecPopup1();" />
                                                           
                                                        </td>

                                                    </tr>
                                                    <tr>


                                                        <td>
                                                            <asp:Label ID="lblID" runat="server" CssClass="lblCaption1" Text="ID"></asp:Label>
                                                           


                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPolicyId" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                        <td>

                                                            <asp:Label ID="lblNetworkClass" runat="server" CssClass="lblCaption1" Text="Network"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>

                                                        </td>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                                                <ContentTemplate>
                                                                     <div id="divPlan" style="visibility: hidden;"></div>
                                                                    <asp:TextBox ID="txtNetworkClass" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="50" AutoPostBack="true" OnTextChanged="txtNetworkClass_TextChanged"></asp:TextBox>
                                                                    
                                                                         <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtNetworkClass" MinimumPrefixLength="1" ServiceMethod="GetGeneralClass"
                                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divPlan">
                                                                    </asp:AutoCompleteExtender>
                                                                </ContentTemplate>

                                                            </asp:UpdatePanel>

                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1" Text="Policy No."></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>

                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPolicyNo" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="30" onkeypress="return PatientPopup('PolicyNo',this.value,event)" ondblclick="return PatientPopup1('PolicyNo',this.value);"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCardNo1" runat="server" CssClass="lblCaption1" Text=" ID No."></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtIDNo" CssClass="TextBoxStyle" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <asp:Label ID="Label100" runat="server" CssClass="lblCaption1" Text="Effective Date"></asp:Label>

                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPolicyStart" runat="server" CssClass="TextBoxStyle" Width="150px" onblur="PastDateCheck()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            <asp:CalendarExtender ID="CalendarExtender7" runat="server"
                                                                Enabled="True" TargetControlID="txtPolicyStart" Format="dd/MM/yyyy">
                                                            </asp:CalendarExtender>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label35" runat="server" CssClass="lblCaption1" Text="Exp Date"></asp:Label>
                                                            <span style="color: red; font-size: 11px;">*</span>

                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="TextBoxStyle" Width="150px" onblur="PastDateCheck()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                                                Enabled="True" TargetControlID="txtExpiryDate" Format="dd/MM/yyyy">
                                                            </asp:CalendarExtender>
                                                        </td>


                                                    </tr>

                                                </table>
                                            </fieldset>
                                            <table border="0" cellpadding="2" cellspacing="2" style="width: 80%">
                                                <tr>
                                                    <td>&nbsp;
                                                        <asp:Label ID="lblPTCompany" runat="server" CssClass="lblCaption1"
                                                            Text="Payer "></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="drpPayer" CssClass="TextBoxStyle" runat="server" Width="155px">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtPTCompany" CssClass="TextBoxStyle" runat="server" Width="200px" Visible="false"></asp:TextBox>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtPTCompany" MinimumPrefixLength="1" ServiceMethod="GetPatientCompany"></asp:AutoCompleteExtender>

                                                            </ContentTemplate>

                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>

                                                        <asp:DropDownList ID="drpConsentForm" CssClass="TextBoxStyle" runat="server" Width="70px" Visible="true">
                                                        </asp:DropDownList>
                                                        <asp:Button ID="btnConsentFormPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 90px;" Visible="true" CssClass="button gray small" Text="Consent Form" OnClick="btnConsentFormPrint_Click" />
                                                        <asp:Button ID="btnDamanGC" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" Visible="false" CssClass="button gray small" Text="Daman Consent" OnClick="btnDamanGC_Click" />
                                                        <asp:Button ID="btnGeneConTreatment" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" Visible="false" CssClass="button gray small" Text="Gen. Consent" OnClick="btnGeneConTreatment_Click" />

                                                    </td>
                                                </tr>
                                            </table>
                                          
                                            <div id="divDeductCoIns" runat="server">
                                                            <fieldset style="width: 90%;  border: 1px solid #999;border-style :solid;">

                                                                <table border="0" cellpadding="2" cellspacing="2" style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label47" runat="server" CssClass="lblCaption1" Text="Deductible & Co-pay"></asp:Label>
                                                                        </td>
                                                                        <td>

                                                                            <asp:DropDownList ID="drpDedType" runat="server" CssClass="TextBoxStyle">
                                                                            </asp:DropDownList>
                                                                            <asp:TextBox ID="txtDeductible" runat="server" Width="50px" Height="23px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="Label81" runat="server" CssClass="lblCaption1" Text="Co-Ins."></asp:Label>
                                                                        </td>
                                                                        <td>

                                                                            <asp:DropDownList ID="drpCoInsType" runat="server" CssClass="TextBoxStyle">
                                                                            </asp:DropDownList>
                                                                            <asp:TextBox ID="txtCoIns" runat="server" Width="50px" Height="23px" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                                        </td>

                                                                    </tr>
                                                                </table>

                                                            </fieldset>
                                                        </div>

                                            <fieldset style="width: 90%;  border: 1px solid #999;border-style :solid;">
                                                            <table cellpadding="3" cellspacing="3" border="0">
                                                                
                                                                <tr>

                                                                    <td class="lblCaption1">
                                                                        
                                                                                Card  Image - Front <br>
                                                                                <asp:Image ID="imgFrontDefault" runat="server" Height="150px" Width="250px" />
                                                                                <asp:AsyncFileUpload ID="AsyncFileUpload3" runat="server"
                                                                            OnUploadedComplete="AsyncFileUpload3_UploadedComplete"
                                                                            OnClientUploadComplete="AsyncFileUpload3_uploadComplete"
                                                                            Width="225px" FailedValidation="False" />
                                                                           

                                                                    </td>

                                                                    <td class="lblCaption1">
                                                                      
                                                                                  Card  Image - Back <br>
                                                                                <asp:Image ID="imgBackDefault" runat="server" Height="150px" Width="250px" />
                                                                                 <asp:AsyncFileUpload ID="AsyncFileUpload4" runat="server"
                                                                            OnUploadedComplete="AsyncFileUpload4_UploadedComplete"
                                                                            OnClientUploadComplete="AsyncFileUpload4_uploadComplete"
                                                                            Width="225px" FailedValidation="False" />
                                                                           
                                                                    </td>
                                                                </tr>
                                                                
                                                            </table>

                                                        </fieldset>
                                                <div id="div1" runat="server">
                                                                <asp:Button ID="btnHistory" runat="server" Style="width: 100px;" CssClass="button gray small" Text="Prev.Visit" OnClientClick="HistoryPopup();" />

                                                                <asp:Button ID="btnBalance" runat="server" Style="width: 100px;" CssClass="button gray small" Text="Inv. Dtls" OnClientClick="InvoicePopup();" />
                                                    </div>
                                                      



                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="2" style="width:100%;height:10px; ">

                                            </td>
                                     </tr> 
                                    <tr>
                                        <td colspan="2" style="width:100%">
                                                 <table style="width:100%;background-color: #D64535;color: #fff;">
                <tr>
                                                        <td>
                                                            <asp:Label ID="Label42" runat="server" style="color:#fff;" CssClass="lblCaption1" Text="Latest Visit&nbsp;:"></asp:Label>
                                                        </td>
                                                        <td>

                                                            <asp:Label ID="lblLastVisit" runat="server" style="color:#fff;"  CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:Label ID="Label40" runat="server" style="color:#fff;" CssClass="lblCaption1" Text="Branch &nbsp;&nbsp; &nbsp;:"></asp:Label>
                                                        </td>
                                                        <td style="width: 200px;">
                                                            <asp:Label ID="lblVisitedBranch" runat="server" style="color:#fff;"  CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                        </td>
                                                        <td class="lblCaption1" style="width: 100px;color:#fff" >Doctor:&nbsp;
                                                            <asp:Label ID="lblPrevDrId" runat="server" style="color:#fff;"  CssClass="lblCaption1" Width="100px" Font-Bold="true" Visible="false"></asp:Label>

                                                        </td>
                                                        <td  colspan="2" >
                                                            <asp:Label ID="lblPrevDrName" runat="server" style="color:#fff;"  CssClass="lblCaption1" Width="200px" Font-Bold="true"></asp:Label>

                                                    </tr>
                                                    <tr>
                                                       
                                                    </tr>
                                                    <tr>


                                                        <td style="width: 100px;">
                                                            <asp:Label ID="Label82" runat="server" style="color:#fff;"  CssClass="lblCaption1" Text="Created User:"></asp:Label>
                                                        </td>
                                                        <td style="width: 150px;">
                                                            <asp:Label ID="lblCreadedUser" runat="server" style="color:#fff;"  CssClass="lblCaption1" Width="150px" Font-Bold="true"></asp:Label>

                                                        </td>
                                                        <td style="width: 100px;">
                                                            <asp:Label ID="Label41" runat="server" style="color:#fff;"  CssClass="lblCaption1" Text="First Visit :"></asp:Label>
                                                        </td>
                                                        <td style="width: 100px;">

                                                            <asp:Label ID="lblFirstVisit" runat="server" style="color:#fff;"  CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                        </td>
                                                     
                                                        <td style="width: 100px;">
                                                            <asp:Label ID="Label38" runat="server" style="color:#fff;"  CssClass="lblCaption1" Text="Modified User:"></asp:Label>
                                                        </td>
                                                        <td style="width: 150px;">
                                                            <asp:Label ID="lblModifiedUser" runat="server" style="color:#fff;"  CssClass="lblCaption1" Width="150px" Font-Bold="true"></asp:Label>

                                                        </td>

                                                        <td style="width: 100px;">
                                                            <asp:Label ID="Label46" runat="server" style="color:#fff;"  CssClass="lblCaption1" Text="Previous Visit :"></asp:Label>
                                                        </td>
                                                        <td style="width: 100px;">

                                                            <asp:Label ID="lblPrevVisit" runat="server"  style="color:#fff;"  CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                                        </td>



                                                    </tr>
            </table>
                                        </td>
                                    </tr>
                                </table>

                                </td>


                                 </tr>
                          </table>

                            <asp:LinkButton ID="lnkRemMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                                <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkRemMessage"
                                    PopupControlID="pnlRemMessage" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
                                    PopupDragHandleControlID="pnlRemMessage">
                                </asp:ModalPopupExtender>
                                <asp:Panel ID="pnlRemMessage" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                                <%=strReminderMessage %>
                                            </td>
                                        </tr>

                                    </table>
                                </asp:Panel>


                                <asp:LinkButton ID="lnkHoldMsg" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                    ForeColor="Blue"></asp:LinkButton>
                                <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="lnkHoldMsg"
                                    PopupControlID="pnlHoldMsg" BackgroundCssClass="modalBackground" CancelControlID="btnHoldClose"
                                    PopupDragHandleControlID="pnlHoldMsg">
                                </asp:ModalPopupExtender>
                                <asp:Panel ID="pnlHoldMsg" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="btnHoldClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                                <%=strHoldMessage %>
                                            </td>
                                        </tr>

                                    </table>
                                </asp:Panel>




                                <asp:LinkButton ID="lnkMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                    ForeColor="Blue"></asp:LinkButton>
                                <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkMessage"
                                    PopupControlID="pnlPrevPrice" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
                                    PopupDragHandleControlID="pnlPrevPrice">
                                </asp:ModalPopupExtender>
                                <asp:Panel ID="pnlPrevPrice" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="btnClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                                <%=strMessage %>
                                            </td>
                                        </tr>

                                    </table>
                                </asp:Panel>





                                <asp:LinkButton ID="lnkHCBMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                    ForeColor="Blue"></asp:LinkButton>
                                <asp:ModalPopupExtender ID="ModalPopupExtender7" runat="server" TargetControlID="lnkHCBMessage"
                                    PopupControlID="pnlHCB" BackgroundCssClass="modalBackground" CancelControlID="btnHCBClose"
                                    PopupDragHandleControlID="pnlHCB">
                                </asp:ModalPopupExtender>
                                <asp:Panel ID="pnlHCB" runat="server" Height="330px" Width="680px" CssClass="modalPopup">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="right">
                                                <asp:Button ID="btnHCBClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="height: 230px;">


                                                <div style="padding-top: 0px; width: 650px; height: 275px; overflow: auto; border-color: #e3f7ef; border-style: groove;">

                                                    <asp:GridView ID="gvHCB" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                                        EnableModelValidation="True" Width="99%" PageSize="50">
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                                        <Columns>


                                                            <asp:TemplateField HeaderText="Category">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCBD_CATEGORY") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category Type">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCB_CATEGORY_TYPEDesc") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Limit">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLimit" runat="server" Text='<%# Bind("HCBD_LIMIT") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblInsType" runat="server" Text='<%# Bind("HCBD_CO_INS_TYPE") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Co-Ins.">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblInsType" runat="server" Text='<%# Bind("HCBD_CO_INS_AMT") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ded Type">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblDedType" runat="server" Text='<%# Bind("HCBD_DED_TYPE") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Deductible">
                                                                <ItemTemplate>


                                                                    <asp:Label ID="lblDedAmt" runat="server" Text='<%# Bind("HCBD_DED_AMT") %>' Style="text-align: right; padding-right: 5px;" Width="100%"></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>


                                                </div>


                                            </td>
                                        </tr>

                                    </table>
                                </asp:Panel>







                            </ContentTemplate>
                        </asp:UpdatePanel>



                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanelCard" HeaderText=" Patient Details-2 ">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblStatusTab2" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <table cellpadding="2" cellspacing="2" style="width: 850px; height: 100px;" border="0">
                                <tr>
                                    <td class="lblCaption1" style="height: 30px">Card Type   <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td class="lblCaption1">Card Name   <span style="color: red; font-size: 11px;">*</span>
                                    </td>


                                    <td class="lblCaption1">Company   <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="height: 30px">
                                        <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpCardType" CssClass="TextBoxStyle" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="drpCardType_SelectedIndexChanged">
                                                    <asp:ListItem Text="Insurance" Value="Insurance"></asp:ListItem>
                                                    <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>

                                                <asp:DropDownList ID="drpCardName" CssClass="TextBoxStyle" BorderWidth="1px"  runat="server" Width="100px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>


                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtCompany" runat="server" Width="70px" MaxLength="10" CssClass="TextBoxStyle" onblur="return CompIdSelected()" onkeypress="return PatientPopup('CompanyId',this.value,event)" AutoPostBack="true" OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                                                <asp:TextBox ID="txtCompanyName" runat="server" Width="250px" MaxLength="50" CssClass="TextBoxStyle" onblur="return CompNameSelected()" onkeypress="return PatientPopup('CompanyName',this.value,event)" AutoPostBack="true" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>
                                                <div id="divComp" style="visibility: hidden;"></div>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany">
                                                </asp:AutoCompleteExtender>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName">
                                                </asp:AutoCompleteExtender>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                    <td></td>



                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 30px">
                                        <asp:Label ID="lblPolicyNo1" runat="server" CssClass="lblCaption1" Text="Policy No."></asp:Label>   <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtPolicyCardNo" CssClass="TextBoxStyle" runat="server"   MaxLength="30" Width="100px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkIsDefault" runat="server" Text="Default Card" CssClass="lblCaption1" Checked="True" />
                                                &nbsp; 
                                            <asp:CheckBox ID="chkCardActive" runat="server" Text="Active" CssClass="lblCaption1" Checked="True" />


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>

                                    <td colspan="2" class="lblCaption1">
                                        <asp:Label ID="lblPlanType" runat="server" CssClass="lblCaption1" Text="Plan Type" Visible="false"></asp:Label>
                                        <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtPlanType" runat="server" CssClass="TextBoxStyle" Visible="false" Width="200px" MaxLength="50" ondblclick="return BenefitsExecPopup(this.value);"></asp:TextBox>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtPlanType" MinimumPrefixLength="1" ServiceMethod="GetPlanType">
                                                </asp:AutoCompleteExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 30px">Effective Date   <span style="color: red; font-size: 11px;">*</span></td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender6" runat="server"
                                                    Enabled="True" TargetControlID="txtEffectiveDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="True" TargetControlID="txtEffectiveDate" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>


                                    <td class="lblCaption1" id="tdCardNo" runat="server">
                                        <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lblCardNo" runat="server" CssClass="lblCaption1" Text="ID No"></asp:Label>
                                                &nbsp;  &nbsp; &nbsp;<asp:TextBox ID="txtCardNo" CssClass="TextBoxStyle" runat="server"   MaxLength="30" Width="100px"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Expiry Date   <span style="color: red; font-size: 11px;">*</span></td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtCardExpDate" runat="server" CssClass="TextBoxStyle"     Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                                    Enabled="True" TargetControlID="txtCardExpDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="True" TargetControlID="txtCardExpDate" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>



                                    <td></td>
                                    <td colspan="2"></td>


                                </tr>


                            </table>

                            <table cellpadding="5" cellspacing="5" style="width: 850px;" border="0">
                                <tr>

                                    <td>
                                        <asp:Label ID="Label70" runat="server" CssClass="lblCaption1"
                                            Text="Card Front Image"></asp:Label>

                                    </td>
                                    <td style="width: 50px"></td>
                                    <td>
                                        <asp:Label ID="Label49" runat="server" CssClass="lblCaption1"
                                            Text="Card Back Image"></asp:Label>
                                    </td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>

                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                            <ContentTemplate>
                                                <asp:Image ID="imgFront" runat="server" Height="160px" Width="285px" />
                                            </ContentTemplate>

                                        </asp:UpdatePanel>

                                    </td>
                                    <td style="width: 50px"></td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                            <ContentTemplate>
                                                <asp:Image ID="imgBack" runat="server" Height="160px" Width="285px" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td></td>
                                    <td valign="top">
                                        <asp:Button ID="btnClaimPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Claim Form Print" OnClick="btnClaimPrint_Click" />
                                        <br />
                                        <br />
                                        <asp:Button ID="btnWebRepot" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Claim Form Web" OnClick="btnWebRepot_Click" />
                                        <br />
                                        <br />
                                        <asp:Button ID="btnCardPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Card Print" OnClick="btnCardPrint_Click" />
                                        <br />
                                        <asp:Button ID="btnDentalWebRepot" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" Text="Web Report" OnClick="btnDentalWebRepot_Click" Visible="false" />
                                        <br />
                                        <input type="checkbox" runat="server" id="chkCompletionRpt" name="chkCompletionRpt" />
                                        <label for="chkCompletionRpt" class="lblCaption1">Completion Report</label>


                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:AsyncFileUpload ID="AsyncFileUpload1" runat="server"
                                            OnUploadedComplete="AsyncFileUpload1_UploadedComplete"
                                            OnClientUploadComplete="AsyncFileUpload1_uploadComplete"
                                            Width="225px" FailedValidation="False" />
                                    </td>
                                    <td style="width: 50px"></td>
                                    <td>
                                        <asp:AsyncFileUpload ID="AsyncFileUpload2" runat="server"
                                            OnUploadedComplete="AsyncFileUpload2_UploadedComplete"
                                            OnClientUploadComplete="AsyncFileUpload2_uploadComplete"
                                            Width="225px" FailedValidation="False" />
                                    </td>

                                </tr>

                            </table>
                            <table cellpadding="5" cellspacing="5" width="70%">
                                <tr>
                                    <td>

                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="btnScanCardAdd" runat="server" Width="80px" CssClass="button gray small" OnClientClick="return ValScanCardAdd();"
                                                    OnClick="btnScanCardAdd_Click" Text="Add" />
                                                <asp:Button ID="btnScanCardUpdate" runat="server" Width="80px" CssClass="button gray small" OnClientClick="return ValScanCardAdd();" Visible="false"
                                                    OnClick="btnScanCardUpdate_Click" Text="Update" />
                                                <asp:Button ID="bntClear" runat="server" Width="80px" CssClass="button gray small" OnClick="bntClear_Click" Text="Clear" />

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnScanCardAdd" />
                                                <asp:PostBackTrigger ControlID="btnScanCardUpdate" />

                                            </Triggers>
                                        </asp:UpdatePanel>

                                </tr>
                            </table>
                        </div>
                        <br />
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvScanCard" runat="server" AllowSorting="True" CssClass="Grid" OnRowDataBound="gvScanCard_RowDataBound"
                                        AutoGenerateColumns="False" Width="100%" EnableModelValidation="True">
                                        <HeaderStyle CssClass="GridHeader" Font-Bold="True" />
                                        <RowStyle CssClass="GridRow" />
                                        <AlternatingRowStyle CssClass="GridAlterRow" />

                                        <Columns>
                                            <asp:TemplateField HeaderText=" Card Name">
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="lnkCardName" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblIsNew" runat="server" Text='<%# Bind("HSC_ISNEW") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCardType" runat="server" Text='<%# Bind("HSC_CARD_TYPE") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblHSC_PHOTO_ID" runat="server" Text='<%# Bind("HSC_PHOTO_ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCardName" runat="server" Text='<%# Bind("HSC_CARD_NAME") %>'></asp:Label>
                                                    </asp:LinkButton>


                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText=" Company Id">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCompId" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblCompId" runat="server" Text='<%# Bind("HSC_INS_COMP_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCompName" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblCompName" runat="server" Text='<%# Bind("HSC_INS_COMP_NAME") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Policy No">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPolicyCardNo" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblPolicyCardNo" runat="server" Text='<%# Bind("HSC_POLICY_NO") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Plan Type" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPlanType" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblPlanType" runat="server" Text='<%# Bind("HSC_PLAN_TYPE") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ID No.">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCardNo" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblgvCardNo" runat="server" Text='<%# Bind("HSC_CARD_NO") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Expiry Date">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCExpDate" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblExpDate" runat="server" Text='<%# Bind("HSC_EXPIRY_DATE") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Effective Date">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEffectiveDate" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblEffectiveDate" runat="server" Text='<%# Bind("HSC_POLICY_START") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Default">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkIsDefault" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblIsDefault" runat="server" Text='<%# Bind("HSC_ISDEFAULT") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblIsDefaultDesc" runat="server" Text='<%# Bind("HSC_ISDEFAULTDesc") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkStatus" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("HSC_STATUS") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="Label52" runat="server" Text='<%# Bind("HSC_STATUSDesc") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Card Front Image">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkFrontPath" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblFrontPath" runat="server" Text='<%# Bind("HSC_IMAGE_PATH_FRONT") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Card Back Image">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBackPath" runat="server" OnClick="EditScanCard_Click">
                                                        <asp:Label ID="lblBackPath" runat="server" Text='<%# Bind("HSC_IMAGE_PATH_BACK") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>

                                                    <asp:ImageButton ID="DeleteScanCard" runat="server" ToolTip="Delete" ImageUrl="~/HMS/Images/icon_delete.png"
                                                        OnClick="DeleteScanCard_Click" OnClientClick="return window.confirm('Do you want to Delete Card Dtls?')" />&nbsp;&nbsp;
                                                
                                                </ItemTemplate>
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>



                        </div>



                    </ContentTemplate>

                </asp:TabPanel>

               

            </asp:TabContainer>

       

        </div>


        <div id="divSignaturePopup" style="display: none; overflow: hidden; border: groove; height: 200px; width: 350px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 520px; top: 170px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="Button4" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideSignaturePopup()" />
                    </td>
                </tr>
            </table>

            <table width="100%">

                <tr>
                    <td style="width: 50%">
                        <fieldset>
                            <legend class="lblCaption1">Signature </legend>
                            <table width="100%">
                                <tr>
                                    <td style="width: 30px;">
                                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                            <ContentTemplate>
                                                <asp:Image ID="imgPatientSig1" runat="server" Height="120px" Width="285px" />
                                            </ContentTemplate>

                                        </asp:UpdatePanel>


                                    </td>
                                </tr>

                            </table>


                        </fieldset>
                    </td>
                    <td style="width: 50%"></td>
                </tr>
            </table>

        </div>

        <div style="padding-left: 60%; width: 100%;">


            <div id="divConfirmMessageClose" runat="server" style="display: none; position: absolute; top: 278px; left: 712px;">

                <img src="../Images/Close.png" style="height: 25px; width: 25px;" onclick="HideConfirmMessage()" />
            </div>
            <div id="divConfirmMessage" runat="server" style="display: none; border: groove; height: 100px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 200px;">

                <table cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td>
                            <asp:Label ID="lblConfirmMessage" runat="server" CssClass="label" Style="font-weight: bold; color: red;"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="height: 50px;">
                            <asp:Button ID="btnConfirmYes" runat="server" Text=" Yes " CssClass="button blue small" Style="width: 70px;" OnClick="btnConfirmYes_Click" />
                            <asp:Button ID="btnConfirmNo" runat="server" Text=" No " CssClass="button blue small" Style="width: 70px;" OnClick="btnConfirmNo_Click" />

                        </td>
                    </tr>
                </table>


                <br />

            </div>
        </div>
        <style type="text/css">
            .style1
            {
                width: 20px;
                height: 32px;
            }

            .modalBackground
            {
                background-color: black;
                filter: alpha(opacity=40);
                opacity: 0.5;
            }

            .modalPopup
            {
                background-color: #ffffff;
                border-width: 3px;
                border-style: solid;
                border-color: Gray;
                padding: 3px;
                width: 250px;
                top: -1000px;
                position: absolute;
                padding: 0px10px10px10px;
            }
        </style>

        <style type="text/css" media="print">
            #nottoprint
            {
                display: none;
            }

            #printToPrinter
            {
                display: block;
            }

                #printToPrinter ul li
                {
                    width: 290px;
                    color: #000;
                    padding: 48px 5px;
                }
        </style>


        <style type="text/css" media="print">
            #nottoprint1
            {
                display: none;
                vertical-align: top;
            }

            #printToPrinter1
            {
                display: block;
            }
        </style>


        <style type="text/css" media="screen">
            #printToPrinter
            {
                display: none;
            }

            #printToPrinter1
            {
                display: none;
            }
        </style>



<style>
    .AutoExtender
    {
        font-family: Verdana, Helvetica, sans-serif;
        font-size: .8em;
        font-weight: normal;
        border: solid 1px #006699;
        line-height: 20px;
        padding: 10px;
        background-color: White;
        margin-left: 10px;
    }

    .AutoExtenderList
    {
        border-bottom: dotted 1px #006699;
        cursor: pointer;
        color: Maroon;
    }

    .AutoExtenderHighlightvisit
    {
        color: White;
        background-color: #006699;
        cursor: pointer;
    }

    #divComp
    {
        width: 400px !important;
    }

        #divComp div
        {
            width: 400px !important;
        }

    #divDr
    {
        width: 400px !important;
    }

        #divDr div
        {
            width: 400px !important;
        }


    #divRefDr
    {
        width: 400px !important;
    }

        #divRefDr div
        {
            width: 400px !important;
        }

         #divPlan
    {
        width: 400px !important;
    }

        #divPlan div
        {
            width: 400px !important;
        }
</style>
 
<script language="javascript" type="text/javascript">
    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else {
            return false;
        }
    }

    function BindDoctorDtls(StaffId, StaffName, Dept, TokenNo) {

        document.getElementById("<%=txtDoctorID.ClientID%>").value = StaffId;
        document.getElementById("<%=txtDoctorName.ClientID%>").value = StaffName;
        document.getElementById("<%=txtDepName.ClientID%>").value = Dept;
        document.getElementById("<%=txtTokenNo.ClientID%>").value = TokenNo;


    }
    function BindCompanyDtls(CompanyId, CompanyName) {

        document.getElementById("<%=txtCompany.ClientID%>").value = CompanyId;
        document.getElementById("<%=txtCompanyName.ClientID%>").value = CompanyName;

    }


    function disableinc() {

        var e = document.getElementById("<%=drpPatientType.ClientID%>");
        var ty = e.options[e.selectedIndex].value;
        if (ty == "CA") {
            document.getElementById("<%=txtCompany.ClientID%>").disabled = true;
                document.getElementById("<%=txtCompanyName.ClientID%>").disabled = true;
                document.getElementById("<%=txtPolicyNo.ClientID%>").disabled = true;
                document.getElementById("<%=txtPolicyId.ClientID%>").disabled = true;
                document.getElementById("<%=txtIDNo.ClientID%>").disabled = true;
                document.getElementById("<%=txtExpiryDate.ClientID%>").disabled = true;
                document.getElementById("<%=drpPlanType.ClientID%>").disabled = true;
                document.getElementById("<%=txtCompany.ClientID%>").value = '';
                document.getElementById("<%=txtCompanyName.ClientID%>").value = '';
                document.getElementById("<%=txtPolicyNo.ClientID%>").value = '';
                document.getElementById("<%=txtPolicyId.ClientID%>").value = '';
                document.getElementById("<%=txtIDNo.ClientID%>").value = '';
                document.getElementById("<%=txtExpiryDate.ClientID%>").value = '';
                document.getElementById("<%=drpPlanType.ClientID%>").value = 'Select';



            }
            else {

                document.getElementById("<%=txtCompany.ClientID%>").disabled = false;
                document.getElementById("<%=txtCompanyName.ClientID%>").disabled = false;
                document.getElementById("<%=txtPolicyNo.ClientID%>").disabled = false;
                document.getElementById("<%=txtPolicyId.ClientID%>").disabled = false;
                document.getElementById("<%=txtIDNo.ClientID%>").disabled = false;
                document.getElementById("<%=txtExpiryDate.ClientID%>").disabled = false;
                document.getElementById("<%=drpPlanType.ClientID%>").disabled = false;


            }
        }

        function GetDays(year, month) {
            return new Date(year, month, 0).getDate();

        }

        function AgeCalculation() {
            if (document.getElementById("<%=txtDOB.ClientID%>").value != "__/__/____") {
                var dob = document.getElementById("<%=txtDOB.ClientID%>").value;
                document.getElementById("<%=txtAge.ClientID%>").value = 0;
                var arrDOB = dob.split('/');
                var currentyear = document.getElementById("<%=Year.ClientID%>").value

                var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
                var arrTodayDate1 = TodayDate.split('/');


                if (arrDOB.length == 3 && dob.length == 10) {

                    if (currentyear != arrDOB[2]) {
                        var age = currentyear - arrDOB[2];
                        var Month;

                        if (parseFloat(arrTodayDate1[1]) >= parseFloat(arrDOB[1])) {
                            Month = arrTodayDate1[1] - arrDOB[1];
                        }

                        else {
                            age = age - 1;
                            var diff = 12 - arrDOB[1];
                            Month = parseFloat(arrTodayDate1[1]) + parseFloat(diff);
                        }

                        document.getElementById("<%=txtAge.ClientID%>").value = age;
                        document.getElementById("<%=txtMonth.ClientID%>").value = Month;

                    }
                    else {

                        document.getElementById("<%=txtAge.ClientID%>").value = 0;
                        var Month;

                        if (parseFloat(arrTodayDate1[1]) >= parseFloat(arrDOB[1])) {
                            Month = arrTodayDate1[1] - arrDOB[1];

                        }

                        else {
                            Month = (parseFloat(arrTodayDate1[1]) + 11) - arrDOB[1];
                        }

                        document.getElementById("<%=txtMonth.ClientID%>").value = Month;

                    }
                }
                else {

                    document.getElementById("<%=txtAge.ClientID%>").value = 0;

                }


            }
            else {
                document.getElementById("<%=txtAge.ClientID%>").value = 0;

            }
        }

        function DBOcalculation() {

            // if (document.getElementById("<%=txtDOB.ClientID%>").value == "") {
            var age = document.getElementById("<%=txtAge.ClientID%>").value;

            var currentyear = document.getElementById("<%=Year.ClientID%>").value
            var DBOYear = currentyear - age;
            if (age != DBOYear) {
                document.getElementById("<%=txtDOB.ClientID%>").value = '01/01/' + DBOYear
                document.getElementById("<%=txtMonth.ClientID%>").value = '';
            }


            // }

        }

        function PastDateCheck() {

            var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]


                //alert(ReFromTodayDate)
                //  alert(ReFromExpDate)

                // if (ReFromTodayDate > ReFromExpDate) {
                //  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Expiry Date Should be Grater than Today Date";
                //  return false;

                // }

            }
            return true;


        }

        function PastDateCheck1() {

            var ExpiryDat = document.getElementById("<%=txtCardExpDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]


                // if (ReFromTodayDate > ReFromExpDate) {

                //  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Expiry Date Should be Grater than Today Date";
                //  return false;
                // }

            }
            return true;

        }

        function PolicyExpCheck() {

            var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;

            var currentyear = document.getElementById("<%=Year.ClientID%>").value

            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]
                var CurrMonth = date_array[1];

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]
                var ExpYear = date_array1[2];
                var ExpMonth = date_array1[1];

                if (currentyear > ExpYear) {
                    alert('Insurance Card Expired');
                    //return false;

                }
                else if (currentyear == ExpYear && CurrMonth > ExpMonth) {
                    alert('Insurance Card Expired');
                }
                else if (currentyear == ExpYear && CurrMonth == ExpMonth) {
                    var diffDays = date_array1[0] - date_array[0]
                    if (diffDays <= 7) {
                        alert('Insurance Card will Expire in ' + diffDays + ' Days');
                    }
                }

            }
            return true;

        }



        $(document).ready(function () {
            $('#btnClear').click(function () {
                $(':input[type=text]').val('');
            });
        });
        function AsyncFileUpload1_uploadStarted() {
            //document.getElementById("imageFront").style.display = "none";
        }

        function AsyncFileUpload1_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgFront.ClientID %>");
            document.getElementById("<%=hidImgFront.ClientID%>").value = 'true';
            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };

            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();

        }

        function AsyncFileUpload2_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgBack.ClientID %>");
                var img = new Image();
                img.onload = function () {
                    //imageDisplay.style.cssText = "height:160px;width:285px";
                    imageDisplay.src = img.src;
                    //imageDisplay.style.display = "block";
                };
                img.src = "..\\Uploads\\Temp\\" + args.get_fileName();
            }




            function AsyncFileUpload3_uploadComplete(sender, args) {
                var imageDisplay = document.getElementById("<%= imgFrontDefault.ClientID %>");

                var img = new Image();
                img.onload = function () {
                    //imageDisplay.style.cssText = "height:160px;width:285px";
                    imageDisplay.src = img.src;
                    //imageDisplay.style.display = "block";
                };

                img.src = "..\\Uploads\\Temp\\" + args.get_fileName();

            }

            function AsyncFileUpload4_uploadComplete(sender, args) {
                var imageDisplay = document.getElementById("<%= imgBackDefault.ClientID %>");
                var img = new Image();
                img.onload = function () {
                    //imageDisplay.style.cssText = "height:160px;width:285px";
                    imageDisplay.src = img.src;
                    //imageDisplay.style.display = "block";
                };
                img.src = "..\\Uploads\\Temp\\" + args.get_fileName();
            }
            function Val() {
                document.getElementById('<%=hidWaitingList.ClientID%>').value = true;
                var label;
                label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var hh = today.getHours();
            var min = today.getMinutes();
            var sec = today.getSeconds();

            var yyyy = today.getFullYear();
            if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;



                // alert(today);

            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;

            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the File No";
                return false;
            }

            if (document.getElementById('<%=txtFName.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the First Name";
                return false;
            }

            if (document.getElementById('<%=drpSex.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Sex";
                return false;
            }


            if (document.getElementById('<%=txtDOB.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtDOB.ClientID%>').value) == false) {
                    document.getElementById('<%=txtDOB.ClientID%>').focus()
                    return false;
                }
            }

            var AgeMandatory = '<%=Convert.ToString(ViewState["AgeMandatory"]) %>'
                if (AgeMandatory == "1" && document.getElementById('<%=txtAge.ClientID%>').value == "") {

                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Age";
                return false;

            }




            var CityMandatory = '<%=Convert.ToString(ViewState["CityMandatory"]) %>'
                if (CityMandatory == "1" && document.getElementById('<%=drpCity.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the City";
                return false;

            }


            var NationalityMandatory = '<%=Convert.ToString(ViewState["NationalityMandatory"]) %>'
                if (NationalityMandatory == "1" && document.getElementById('<%=drpNationality.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Nationality";
                return false;

            }

            var EmiratesIdMandatory = '<%=Convert.ToString(ViewState["EmiratesIdMandatory"]) %>'
                if (EmiratesIdMandatory == "1" && document.getElementById('<%=txtEmiratesID.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the EmiratesID / Passport";
                    return false;

                }


                var VisaTypeMandatory = '<%=Convert.ToString(ViewState["VisaTypeMandatory"]) %>'
                if (VisaTypeMandatory == "1" && document.getElementById('<%=drpVisaType.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Visa Type";
                return false;

            }



            if (document.getElementById('<%=txtMobile1.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Mobile No";
                return false;
            }

            if (document.getElementById('<%=hidPateName.ClientID%>').value != "HomeCare") {
                    if (document.getElementById('<%=txtDoctorID.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Doctor Id";
                    return false;
                }
            }

            var strEmail = document.getElementById('<%=txtEmail.ClientID%>').value
                if (/\S+/.test(strEmail)) {
                    if (echeck(document.getElementById('<%=txtEmail.ClientID%>').value) == false) {
                    return false;
                }
            }





            if (document.getElementById('<%=drpPatientType.ClientID%>').value == "CR") {


                    if (document.getElementById('<%=drpDefaultCompany.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company Name";
                    return false;
                }
                if (document.getElementById('<%=drpPlanType.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Plan Type";
                     return false;
                 }

                 var PackageManditory = '<%=Convert.ToString(Session["PackageManditory"]) %>'
                if (PackageManditory == "Y" && document.getElementById('<%=txtNetworkClass.ClientID%>').value == "") {

                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Network Class";
                    return false;
                }

                if (document.getElementById('<%=txtPolicyNo.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Member ID";
                    return false;
                }


                if (document.getElementById('<%=txtPolicyStart.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtPolicyStart.ClientID%>').value) == false) {
                        document.getElementById('<%=txtPolicyStart.ClientID%>').focus()
                        return false;
                    }
                }

                if (document.getElementById('<%=txtExpiryDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Expiry Date";
                    return false;
                }

                if (document.getElementById('<%=txtExpiryDate.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtExpiryDate.ClientID%>').value) == false) {
                        document.getElementById('<%=txtExpiryDate.ClientID%>').focus()
                        return false;
                    }
                }







                var DeductibleMandatory = '<%=Convert.ToString(ViewState["DeductibleMandatory"]) %>'
                if (DeductibleMandatory == "1" && document.getElementById('<%=txtDeductible.ClientID%>').value == "" && document.getElementById('<%=drpDedType.ClientID%>').value != "") {

                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Deductible";
                    return false;

                }



            }

            var LatestVisitHours = document.getElementById('<%=hidLatestVisitHours.ClientID%>').value;
                var MultipleVisitADay = document.getElementById('<%=hidMultipleVisitADay.ClientID%>').value;

                var LastVisitDrId = document.getElementById('<%=hidLastVisitDrId.ClientID%>').value;
                var DoctorID = document.getElementById('<%=txtDoctorID.ClientID%>').value;

                if (document.getElementById('<%=chkUpdateWaitList.ClientID%>').checked == true) {
                    if (LastVisitDrId == DoctorID) {
                        if (LatestVisitHours != "" && MultipleVisitADay != "") {

                            if (parseFloat(LatestVisitHours) < 24) {
                                if (parseFloat(LatestVisitHours) < parseFloat(MultipleVisitADay)) {
                                    var isUpdateWaitList = window.confirm('This patient is already exist in Patient Waiting List, Do you want update waiting List ? ');
                                    document.getElementById('<%=hidWaitingList.ClientID%>').value = isUpdateWaitList;

                            }
                        }
                    }
                }
            }

            var TokenPrint = '<%=Convert.ToString(Session["TokenPrint"]) %>'

                if (TokenPrint == "Y") {
                    var isTokenPrint = window.confirm('Do you want to print Token ? ');
                    document.getElementById('<%=hidTokenPrint.ClientID%>').value = isTokenPrint;
            }
            else {
                document.getElementById('<%=hidTokenPrint.ClientID%>').value = 'false';

            }


            var LabelPrint = '<%= Convert.ToString( ViewState["LABEL_PRINT"])  %>'
                if (LabelPrint == "1") {
                    var isLabelPrint = window.confirm('Do you want to print Label ? ');
                    document.getElementById('<%=hidLabelPrint.ClientID%>').value = isLabelPrint;
            }
            else {
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = 'false';

            }

            var PriorityDisplay = '<%=Convert.ToString(Session["PriorityDisplay"]) %>'
                if (PriorityDisplay == "Y" && document.getElementById('<%=drpPriority.ClientID%>').value == "0") {

                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Priority";
                    return false;

                }


                return PolicyExpCheck();
            }



            function ValScanCardAdd() {
                var label;
                label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=drpCardName.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please select the Card Name";
                document.getElementById('<%=drpCardName.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtPolicyCardNo.ClientID%>').value == "") {
                var PTRegChangePolicyNoCaption = '<%=PTRegChangePolicyNoCaption.ToLower()%>'
                if (PTRegChangePolicyNoCaption == 'true') {
                    document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please enter the Card No.";
                }
                else {
                    document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please enter the policy Number";
                }
                document.getElementById('<%=txtPolicyCardNo.ClientID%>').focus();
                return false;
            }



            if (document.getElementById('<%=txtEffectiveDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtEffectiveDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtEffectiveDate.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtCardExpDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatusTab2.ClientID%>').innerHTML = "Please enter the Card Expiry Date";
                document.getElementById('<%=txtCardExpDate.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtCardExpDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtCardExpDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtCardExpDate.ClientID%>').focus()
                    return false;
                }
            }

            //return PastDateCheck1()
            // return PolicyExpCheck();


        }
        function Val1() {

            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

        }



        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function RefDrIdSelected() {
            if (document.getElementById('<%=txtRefDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function RefDRNameSelected() {
            if (document.getElementById('<%=txtRefDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }




        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');


                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];


                }
            }

            return true;
        }


</script>

<script language="javascript" type="text/javascript">


    function BindSelectedPlanType(PlanType) {

        document.getElementById("<%=txtPlanType.ClientID%>").value = PlanType;

    }


    function BenefitsExecPopup(strValue) {

        var CompID = document.getElementById('<%=txtCompany.ClientID%>').value;
        var win = window.open("../Masters/BenefitsExeclusions.aspx?PageName=PatientReg&CompanyID=" + CompID + "&PlanType=" + strValue, "newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
        win.focus();

        return true;

    }


    function BenefitsExecPopup1() {

        var CompID = document.getElementById('<%=drpDefaultCompany.ClientID%>').value;
        var PlanType = document.getElementById('<%=drpPlanType.ClientID%>').value;

        var win = window.open("../Masters/BenefitsExeclusions.aspx?PageName=PatientReg&CompanyID=" + CompID + "&PlanType=" + PlanType, "newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
        win.focus();

        return true;

    }

    function PatientPopup1(CtrlName, strValue) {
        var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=PatientReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
        win.focus();

        return true;

    }

    function PatientPopup(PageName, strValue, evt) {

        // if (PageName == 'FileNo') {
        //  document.getElementById('<%=txtFileNo.ClientID%>').value = strValue.toUpperCase()
        //  }

        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;

        if (chCode == 126) {
            // var win = window.open("Firstname_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            // win.focus();
            document.getElementById('<%=hidPopup.ClientID%>').value = 'true';
            return true;

        }
        if (chCode == 33) {
            // var win = window.open("PatientInfo_Zoom.aspx?PagePath=PATIENTREG&PageName=" + PageName + "&Value=" + strValue, "newwin", "top=200,left=270,height=520,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            // win.focus();
            document.getElementById('<%=hidInfoPopup.ClientID%>').value = 'true';
            return false;

        }

        if (PageName == 'Phone' || PageName == 'Mobile') {
            return OnlyNumeric(evt);
        }

        return true;
    }
    function InsurancePopup(PageName, strValue, evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode == 126) {
            var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
            return false;
        }
        return true;
    }
    function DoctorPopup(PageName, strValue, evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode == 126) {
            var win = window.open("../Masters/Doctor_Name_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
            return false;
        }
        return true;
    }

    function OpenScan() {
        window.open("../EmiratesData.aspx", "newwin", "height=475,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
        win.focus();
    }


    function WebReportPopup(strValue) {

        var win = window.open("../WebReport/DAMANAuthorizationRequest.aspx?PatientId=" + strValue, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
        win.focus();
    }

    function ScandCardPopup(strValue) {
        var win = window.open("../WebReport/ScandCardPrint.aspx", "newwin1", "top=80,left=100,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
        win.focus();
    }



    function GeneralConsentPopup(strValue, GCID) {
        var win = window.open("../WebReport/GeneralConsentReport.aspx?PatientId=" + strValue + "&GCID=" + GCID, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
        win.focus();
    }


    function HistoryPopup() {
        var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("PatientVisitHistory.aspx?Value=" + PtId, "newwin1", "top=200,left=100,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function InvoicePopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("PatientInvoicePopup.aspx?Value=" + PtId, "newwin1", "top=80,left=80,height=850,width=1100,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function ReportPopup(RptName, Mode, PrintCount) {

            var win = window.open("../../CReports/ReportViewer.aspx?ReportName=" + RptName + "&Mode=" + Mode + "&PrintCount=" + PrintCount, "newwin1", "left=200,top=80,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();


        }

        function ReportPrintPopup(RptName, Mode, PrintCount) {

            var win = window.open("../../CReports/PrintCrystalReports.aspx?ReportName=" + RptName + "&Mode=" + Mode + "&PrintCount=" + PrintCount, "newwin1", "left=200,top=80,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();


        }

        function GCHistoryPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("GeneralConsentHistory.aspx?PatientId=" + PtId, "newwin1", "top=200,left=300,height=550,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }


        function AppointmentPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var DrID = document.getElementById('<%=txtDoctorID.ClientID%>').value
            var win = window.open("AppointmentEMRDr.aspx?DrID=" + DrID + "&PatientID=" + PtId, "Appt", "top=200,left=300,height=650,width=900,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
        }



        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }


        function ShowOnLoad(Report, DBString, Formula, PrintCount) {
            if (Report == "HmsLabel.rpt") {
                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + Formula + '\'$PRINT$$$' + PrintCount);
                //  var win = window.open('../CReports/HMSReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + Formula + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
                //   win.focus();

            }
            else {

                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${hms_patient_visit.HPV_SEQNO}=\'' + Formula + '\'$PRINT$$$1');

            }

        }

        function ShowClaimPrint(Report, DBString, FileNo, BranchId) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\' $SCREEN$$$1');



        }

        function ShowClaimPrintPDF(Report, FileNo, BranchId) {

            //exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\' $SCREEN$$$1');

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }



        function ShowBalance(Report, DBString, FileNo) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' $SCREEN$$$1');

        }


        function ShowMRRPrintPDF(FileNo) {

            var Report = "HmsMRRPrint.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


        function ShowGeneConTreatment(Report, DBString, FileNo, BranchId) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\' $SCREEN$$$1');



        }

        function ShowIDPrint(Report, DBString, FileNo) {

            // exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' $SCREEN$$$1');
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', 'IDPrint', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowGCReport(DBString, GCId) {
            // var Report = "GeneralTreatmentConsent.rpt";
            var Report = "GeneralTreatmentConsent1.rpt";

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${GeneralConsentView.HGC_ID}=' + GCId + ' $SCREEN$$$1');

        }

        function ShowGCReportPDF(FileNo) {
            var Report = "GeneralTreatmentConsent1.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowGridGCReportPDF(GCId) {
            var Report = "GeneralTreatmentConsent1.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HGC_ID}=' + GCId + '', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }
        function ShowDamanGCReport(DBString, GCId) {

            //  var Report = "DamanConscent.rpt";
            var Report = "MedConsent1.rpt";

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${GeneralConsentView.HGC_ID}=' + GCId + ' $SCREEN$$$1');

        }

        function ShowGCReport1(GCId) {
            window.open('../../CReports/ReportViewer.aspx?ReportName=DamanConscent.rpt&SelectionFormula={GeneralConsentView.HGC_ID}=' + GCId, '_new', 'menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1')

        }

        function ShowDamanGCReportPDF(FileNo) {
            var Report = "MedConsent1.rpt";

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowConsentFormReportPDF(FileNo) {
            //var Report = "MedConsent1.rpt";

            var Report = document.getElementById('<%=drpConsentForm.ClientID%>').value + ".rpt"

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


        function ShowSignature(DBString, FileName, BranchId) {
            var SignaturePad = document.getElementById('<%=hidSignaturePad.ClientID%>').value
            if (SignaturePad == 'GENIUS') {
                exec('D:\\HMS\\GeniusSignature\\HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
            }
            else if (SignaturePad == 'WACOM') {
                exec('D:\\HMS\\WacomSignature\\DemoButtons.exe', DBString + '$' + FileName + '$' + BranchId);
            }
            else {
                exec('D:\\HMS\\GeniusSignature\\HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
            }

        }


        function GetEmiratesIdData(DBString, FileName, BranchId) {

            // exec('D:\\HMS\\EmiratesId\\EmiratesID.exe', DBString + '$' + FileName + '$' + BranchId);

            exec('D:\\HMS\\EIDA_CardDisplay\\EIDA_CardDisplay.exe', DBString + '$' + FileName + '$' + BranchId);

        }




        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }



</script>

<script language="javascript" type="text/javascript">


    function EmiratesIDFormat(Content) {


        if (Content.length == 3) {

            document.getElementById("<%= txtEmiratesID.ClientID %>").value = Content + '-';
        }
        if (Content.length == 8) {

            document.getElementById("<%= txtEmiratesID.ClientID %>").value = Content + '-';
        }
        if (Content.length == 16) {

            document.getElementById("<%= txtEmiratesID.ClientID %>").value = Content + '-';
        }

    }


    function CheckEmiratesIDFormat() {

        var data = document.getElementById("<%= txtEmiratesID.ClientID %>").value;

        var FormatedData = '';
        var FormatedData1 = FormatedData1 = data.substr(0, 3);
        var FormatedData2 = '', FormatedData3 = '', FormatedData4 = '';

        if (data.substr(3, 1) != '-') {
            FormatedData1 = data.substr(0, 3) + '-'

        }


        if (data.length >= 8) {
            if (data.substr(8, 1) != '-') {

                FormatedData2 = data.substr(3, 4) + '-';


            }
            else {
                FormatedData2 = data.substr(3, 4)
            }
        }
        else {

            FormatedData2 = data.substr(3, 4)
        }


        if (data.length >= 13) {
            if (data.substr(13, 1) != '-') {

                FormatedData3 = data.substr(8, 7) + '-';


            }
        }


        FormatedData = FormatedData1 + FormatedData2 + FormatedData3;

        document.getElementById("<%= txtEmiratesID.ClientID %>").value = FormatedData;


    }
</script>

<script language="javascript" type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var hh = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();

    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

    jQuery(document).ready(function () {
        document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
        // alert(today);

        // fnPageLoad();

    });
</script>


<script language="javascript" type="text/javascript">

    function ShowConfirmMessage() {


        document.getElementById("<%=divConfirmMessageClose.ClientID%>").style.display = 'block';
        document.getElementById("<%=divConfirmMessage.ClientID%>").style.display = 'block';


    }


    function HideConfirmMessage() {

        document.getElementById("<%=divConfirmMessageClose.ClientID%>").style.display = 'none';
            document.getElementById("<%=divConfirmMessage.ClientID%>").style.display = 'none';
            document.getElementById("<%=lblConfirmMessage.ClientID%>").innerHTML = '';
        }

</script>
 

 </asp:Content>