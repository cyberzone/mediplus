﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class AppointmentDrPopup : System.Web.UI.Page
    {

        public static string APPOINTMENTMSG = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["APPOINTMENTMSG"].ToString());
        public static string BEAUTICIANCODE = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["BEAUTICIANCODE"].ToString());


        
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void GetAppointment()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HAM_ID='" + Convert.ToString(ViewState["AppId"]) + "'";


            clsAppointmentMaster objAppMast = new clsAppointmentMaster();
            DataSet DSAppt = new DataSet();

            DSAppt = objAppMast.AppointmentMasterGet(Criteria);

            string[] arrAppDtls = { " " };

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_FILE_NO") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILE_NO"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILE_NO"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_PT_NAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) != "")
                {
                    txtFName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_POBOX") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_POBOX"]) != "")
                {
                    txtPOBox.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_POBOX"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MOBILE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILE"]) != "")
                {
                    txtMobile.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILE"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_PHONE1") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PHONE1"]) != "")
                {
                    txtResNo.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PHONE1"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_PHONE2") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PHONE2"]) != "")
                {
                    txtOffNo.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PHONE2"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentDate") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentDate"]) != "")
                {
                    txtFromDate.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentDate"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_APNT_TIME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TIME"]) != "")
                {
                    LoadStartTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TIME"]));

                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_REMARK") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARK"]) != "")
                {
                    txtRemarks.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARK"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_STAFF_ID") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STAFF_ID"]) != "")
                {
                    drpDoctor.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STAFF_ID"]);
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_APNT_TYPE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TYPE"]) != "")
                {
                    drpService.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APNT_TYPE"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_STATUS") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]) != "")
                {
                    if (Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]) == "A")
                        chkStatus.Checked = true;
                    else
                        chkStatus.Checked = false;
                     
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_TYPEOFSERVICE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_TYPEOFSERVICE"]) != "")
                {
                    txtTypeOfServ.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_TYPEOFSERVICE"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_REPORTEDMODE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REPORTEDMODE"]) != "")
                {
                    drpReptMode.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REPORTEDMODE"]);

                    for (int intCount = 0; intCount < drpReptMode.Items.Count; intCount++)
                    {
                        if (drpReptMode.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REPORTEDMODE"]))
                        {
                            drpReptMode.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REPORTEDMODE"]);
                            goto ForReptMode;
                        }

                    }

                }
            ForReptMode: ;

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_CREATED_USER") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATED_USER"]) != "")
                {
                    lblCreatedUser.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATED_USER"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_CREATED_DATE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATED_DATE"]) != "")
                {
                    DateTime dtCreatedDate;
                    dtCreatedDate = Convert.ToDateTime(DSAppt.Tables[0].Rows[0]["HAM_CREATED_DATE"]);

                    lblCreatedDate.Text = dtCreatedDate.ToString("dd/MM/yyyy HH:mm");
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MODIFIED_USER") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_USER"]) != "")
                {
                    lblModifiedUser.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_USER"]);
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MODIFIED_DATE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_DATE"]) != "")
                {
                    DateTime dtModifiedDate;
                    dtModifiedDate = Convert.ToDateTime(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_DATE"]);
                    lblModifiedDate.Text = dtModifiedDate.ToString("dd/MM/yyyy HH:mm");
                }

            }

        }

        void BindAppointmentGrid(string Searchby)
        {

            string Criteria = " 1=1 ";

            if (Searchby == "HAM_STAFF_ID")
            {
                Criteria += " AND HAM_STAFF_ID='" + drpDoctor.SelectedValue + "'";
            }
            else if (Searchby == "HAM_FILE_NO")
            {
                Criteria += " AND HAM_FILE_NO='" + txtFileNo.Text.Trim() + "'";
            }


            clsAppointmentMaster objAppMast = new clsAppointmentMaster();
            DataSet DSAppt = new DataSet();

            DSAppt = objAppMast.AppointmentMasterGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                gvAppMaster.DataSource = DSAppt;
                gvAppMaster.DataBind();
                ModPopExtAppMasters.Show();
            }


        }

        Boolean CheckAppointment()
        {

            string Criteria = " 1=1 ";


            Criteria += "  AND HAM_STATUS NOT IN ('A')";

            Criteria += " AND HAM_FILE_NO !='" + txtFileNo.Text.Trim() + "'";

            Criteria += " AND HAM_STAFF_ID='" + drpDoctor.SelectedValue + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            Criteria += "   AND   HAM_APNT_DATE = CONVERT(DATETIME,'" + txtFromDate.Text.Trim() + "',103) ";
            Criteria += "  AND HAM_APNT_TIME ='" + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "'";




            clsAppointmentMaster objAppMast = new clsAppointmentMaster();
            DataSet DSAppt = new DataSet();

            DSAppt = objAppMast.AppointmentMasterGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;
        }



        void BindPatientDtls()
        {
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";

            }
            if (txtMobile.Text.Trim() != "")
            {
                Criteria += " AND HPM_MOBILE = '" + txtMobile.Text + "'";
            }



            // txtFName.Text = "";

            // txtMobile.Text = "";


            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) != "")
                {
                    txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) != "")
                {
                    hidPTType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_POBOX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POBOX"]) != "")
                {
                    txtPOBox.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POBOX"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE1") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]) != "")
                {
                    txtResNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE2") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE2"]) != "")
                {
                    txtOffNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE2"]);
                }


            }
        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            string[] arrDrCode = BEAUTICIANCODE.Split(',');

            string strDrCode = "";
            for (int i = 0; i < arrDrCode.Length; i++)
            {
                strDrCode += "'" + arrDrCode[i] + "',";
            }
            if (strDrCode.Length > 1)
                strDrCode = strDrCode.Substring(0, strDrCode.Length - 1);

            Criteria += " AND HSFM_STAFF_ID NOT IN (" + strDrCode + ")";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName2";
                drpDoctor.DataBind();
            }




        }

        void BindAppointmentServices()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentServicesGet();

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpService.DataSource = DS;
                drpService.DataTextField = "HAS_SERVICENAME";
                drpService.DataValueField = "HAS_SERVICENAME";
                drpService.DataBind();
            }

        }



        void BindTime()
        {
            int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(Session["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(Session["AppointmentEnd"]);
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");


                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");



            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);



                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void LoadStartTime(string strSTTime)
        {

            string[] arrSTTime = strSTTime.Split(':');

            if (arrSTTime.Length > 1)
            {
                for (int intCount = 0; intCount < drpSTHour.Items.Count; intCount++)
                {
                    if (drpSTHour.Items[intCount].Value == arrSTTime[0])
                    {
                        drpSTHour.SelectedValue = arrSTTime[0].Trim();
                        intCount = drpSTHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpSTMin.Items.Count; intCount++)
                {
                    if (drpSTMin.Items[intCount].Value == arrSTTime[1].Trim())
                    {
                        drpSTMin.SelectedValue = arrSTTime[1].Trim();
                        intCount = drpSTMin.Items.Count;
                    }

                }


            }
        }

        void Clear()
        {
            //if (drpDoctor.Items.Count > 0)
            //drpDoctor.SelectedIndex = 0;
            txtFileNo.Text = "";
            if( drpService.Items.Count >0)
                drpService.SelectedIndex = 0;
            txtFName.Text = "";
            txtTypeOfServ.Text = "";
            txtPOBox.Text = "";
            txtResNo.Text = "";
            drpReptMode.SelectedIndex = 0;
            txtOffNo.Text = "";
            txtMobile.Text = "";
            txtRemarks.Text = "";

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                btnSendSMS.Enabled = false;
                btnClose.Enabled = false;

            }


            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                btnSendSMS.Enabled = false;
                btnClose.Enabled = false;
            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();
                lblStatus.Text = "";
                BindTime();

                ViewState["Priority"] = "1";
                ViewState["PageName"] = Convert.ToString(Request.QueryString["PageName"]);
                ViewState["AppId"] = Convert.ToString(Request.QueryString["AppId"]);
                ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                ViewState["Time"] = Convert.ToString(Request.QueryString["Time"]);
                ViewState["DrId"] = Convert.ToString(Request.QueryString["DrId"]);
                ViewState["DeptID"] = Convert.ToString(Request.QueryString["DeptID"]);
                ViewState["Priority"] = Convert.ToString(Request.QueryString["Priority"]);

                BindDoctor();
                BindAppointmentServices();

                GetAppointment();

                if (Convert.ToString(ViewState["Date"]) != null && Convert.ToString(ViewState["Date"]) != "0")
                {
                    txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                }
                if (Convert.ToString(ViewState["Time"]) != null && Convert.ToString(ViewState["Time"]) != "0")
                {

                    LoadStartTime(Convert.ToString(ViewState["Time"]));


                    int AppointmentInterval = Convert.ToInt32(Session["AppointmentInterval"]);
                    string strlTime;
                    strlTime = drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;

                    string strNewTime1 = "";
                    if (strNewTime1 == "")
                    {
                        strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                    }

                    string[] arrlTime = strlTime.Split(':');
                    if (arrlTime.Length > 1)
                    {
                        strlTime = arrlTime[0];
                    }

                    if (Convert.ToInt32(strNewTime1) >= 60)
                    {
                        strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                    }
                    else
                    {
                        strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
                    }




                }

                if (Convert.ToString(ViewState["DrId"]) != null && Convert.ToString(ViewState["DrId"]) != "0")
                {
                    drpDoctor.SelectedValue = Convert.ToString(ViewState["DrId"]);
                }




            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckAppointment() == true)
                {
                    lblStatus.Text = "Appointment already exist for this Doctor";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;
                }

                clsAppointmentMaster objAppMast = new clsAppointmentMaster();

                if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    objAppMast.HAM_ID = Convert.ToString(ViewState["AppId"]);
                    objAppMast.MODE = "M";

                }
                else
                {
                    objAppMast.HAM_ID = "0";
                    objAppMast.MODE = "A";
                }
                objAppMast.HAM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objAppMast.HAM_PRIORITY = Convert.ToString(ViewState["Priority"]);
                objAppMast.HAM_STAFF_ID = drpDoctor.SelectedValue;
                objAppMast.HAM_STAFF_TYPE = "";
                objAppMast.HAM_STAFF_NAME = drpDoctor.SelectedItem.Text;
                objAppMast.HAM_FILE_NO = txtFileNo.Text.Trim();
                objAppMast.HAM_PT_NAME = txtFName.Text.Trim();
                objAppMast.HAM_PT_TYPE = hidPTType.Value;
                objAppMast.HAM_APNT_DATE = txtFromDate.Text.Trim();
                objAppMast.HAM_APNT_TIME = drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;
                objAppMast.HAM_POBOX = txtPOBox.Text.Trim();
                objAppMast.HAM_PHONE1 = txtResNo.Text.Trim();
                objAppMast.HAM_PHONE2 = txtOffNo.Text.Trim();
                objAppMast.HAM_MOBILE = txtMobile.Text.Trim();
                objAppMast.HAM_APNT_TYPE = drpService.SelectedValue;
                objAppMast.HAM_REMARK = txtRemarks.Text;
                if (chkStatus.Checked == true)
                    objAppMast.HAM_STATUS = "A";
                else
                    objAppMast.HAM_STATUS = "I";
                objAppMast.HAM_COLOR_CODE = "0";
                objAppMast.HAM_TYPEOFSERVICE = txtTypeOfServ.Text.Trim();
                objAppMast.HAM_REPORTEDMODE = drpReptMode.SelectedValue;
                objAppMast.UserID = Convert.ToString(Session["User_ID"]);
                objAppMast.AddAppointmentMaster();

                lblStatus.Text = "Details Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "','" + Convert.ToString(ViewState["DeptID"]) + "');", true);

            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Details Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }
        }

      

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindPatientDtls();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup_txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtMobile_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindPatientDtls();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup_txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnSendSMS_Click(object sender, EventArgs e)
        {

            if (txtMobile.Text.Trim() == "")
            {
                lblStatus.Text = "Please enter Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            string MobileNumber = txtMobile.Text.Trim();

            if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
            {
                lblStatus.Text = "Please check the Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            clsSMS objSMS = new clsSMS();
            string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";

            string strContent = "";
           // strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + txtFName.Text.Trim()
             //  + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;


            strContent = APPOINTMENTMSG.Replace("|PatientName|", txtFName.Text.Trim());
            strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());

            objSMS.MobileNumber = txtMobile.Text.Trim(); // "0502213045";
            objSMS.template = strContent;
            Boolean boolResult = objSMS.SendSMS();
            if (boolResult == true)
            {
                lblStatus.Text = "SMS Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblStatus.Text = "SMS Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        FunEnd: ;

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                clsAppointmentMaster objAppMast = new clsAppointmentMaster();
                if (Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    objAppMast.HAM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objAppMast.HAM_ID = Convert.ToString(ViewState["AppId"]);
                    objAppMast.DeleteAppointmentMaster();
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "','" + Convert.ToString(ViewState["DeptID"]) + "');", true);
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnDrAppMastShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpDoctor.Items.Count > 0)
                    BindAppointmentGrid("HAM_STAFF_ID");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup.btnDrAppMastShow_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnPTAppMastShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpDoctor.Items.Count > 0)
                    BindAppointmentGrid("HAM_FILE_NO");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup.btnPTAppMastShow_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                 Clear();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentDrPopup.btnClear_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }
    }
}