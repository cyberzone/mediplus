﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Registration
{
    public partial class PTConcernFormSignature : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }


        }
        void BindImage(string SegmentType)
        {
            string   strFileNo = "" ;

            strFileNo = Convert.ToString(ViewState["PT_ID"]).Trim() ;

            editableImage.Src = "PTConcernFormSignatureDisplay.aspx?PT_ID=" + strFileNo; //   @DentalPath + SegmentType + "\notsaved.jpg";


        }


        protected void Page_Load(object sender, EventArgs e)
        {

            ViewState["PT_ID"] = Convert.ToString(Request.QueryString["PT_ID"]);
            BindImage("");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Byte[] bytImage = null;

            bytImage = Convert.FromBase64String(hidImgData.Value);

            dboperations dbo = new dboperations();

            dbo.PatientSignatureAdd( Convert.ToString(ViewState["PT_ID"]).Trim() , Convert.ToString(Session["Branch_ID"]), bytImage);

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "window.close()", true);
            // BindImage("");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

    }
}