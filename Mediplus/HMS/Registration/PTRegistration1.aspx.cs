﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Drawing;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class PTRegistration1 : System.Web.UI.Page
    {
        # region Variable Declaration

        // string strCon = System.Configuration.ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;

        string strDataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        string strDBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        string strDBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        string strDBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();
        string strReportPath = System.Configuration.ConfigurationSettings.AppSettings["REPORT_PATH"].ToString().Trim();


        //string strIdPrinterName = System.Configuration.ConfigurationSettings.AppSettings["IdPrinterName"].ToString().Trim();
        //string strLabelPrinterName = System.Configuration.ConfigurationSettings.AppSettings["LabelPrinterName"].ToString().Trim();

        string ImageSaveToDB = System.Configuration.ConfigurationSettings.AppSettings["ImageSaveToDB"].ToString().Trim();



        static string strTPA = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["TPA"]).ToLower().Trim();
        public string strNetwork = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Network"]).ToLower().Trim();
        public string PTRegChangePolicyNoCaption = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegChangePolicyNoCaption"]).ToLower().Trim();



        // public string strNetworkClass = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["NetworkClass"]).ToLower().Trim();
        //string strPatientRegisterDate = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PatientRegisterDate"]).ToLower().Trim();

        // public string LabelPrint = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["LabelPrint"]).ToLower().Trim();

        // string PTRegDeductCoIns = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegDeductCoIns"]).ToLower().Trim();
        // string PTRegOtherInfo = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegOtherInfo"]).ToLower().Trim();

        //public string EmiratesIdMandatory="0";// = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["EmiratesIdMandatory"]).ToLower().Trim();
        //public string NationalityMandatory="0";// = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegNationalityMand"]).ToLower().Trim();
        //public string AgeMandatory="0";


        dboperations dbo = new dboperations();

        public static string strDrName = "";
        public static string strCompName = "";
        public static string strCompID = "";

        public string strRemainderRecept = "";
        Int32 intImageSize;

        static string stParentName = "0";
        static string strSessionBranchId;
        static string stParentName1 = "0";

        public string strMessage = "", strReminderMessage = "", strHoldMessage = "";

        public string strLabel = "", strToken = "";

        #endregion

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ProcessTimLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../ProcessTimLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDeductibleType()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetDeductibleType(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDedType.DataSource = ds;
                drpDedType.DataValueField = "Code";
                drpDedType.DataTextField = "Name";
                drpDedType.DataBind();
            }


        }

        void BindCoInsuranceType()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetCoInsuranceType(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCoInsType.DataSource = ds;
                drpCoInsType.DataValueField = "Code";
                drpCoInsType.DataTextField = "Name";
                drpCoInsType.DataBind();
            }


        }

        void BindPatientTitle()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetPatientTitle(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpTitle.DataSource = ds;
                drpTitle.DataValueField = "Code";
                drpTitle.DataTextField = "Name";
                drpTitle.DataBind();
            }

            drpTitle.Items.Insert(0, "");
            drpTitle.Items[0].Value = "";

        }

        void BindConsentForm()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetConsentForm(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpConsentForm.DataSource = ds;
                drpConsentForm.DataValueField = "Code";
                drpConsentForm.DataTextField = "Name";
                drpConsentForm.DataBind();
            }



        }

        void AgeCalculation()
        {

            string dob = txtDOB.Text.Trim();
            txtAge.Text = "0";
            string[] arrDOB = dob.Split('/');
            var currentyear = Year.Value;

            string TodayDate = hidTodayDate.Value;
            string[] arrTodayDate1 = TodayDate.Split('/');


            if (arrDOB.Length == 3 && dob.Length == 10)
            {

                if (currentyear != arrDOB[2])
                {
                    Int32 age = Convert.ToInt32(currentyear) - Convert.ToInt32(arrDOB[2]);
                    Int32 Month;

                    if (Convert.ToInt32(arrTodayDate1[1]) >= Convert.ToInt32(arrDOB[1]))
                    {
                        Month = Convert.ToInt32(arrTodayDate1[1]) - Convert.ToInt32(arrDOB[1]);
                    }

                    else
                    {
                        age = age - 1;
                        Int32 diff = 12 - Convert.ToInt32(arrDOB[1]);
                        Month = Convert.ToInt32(arrTodayDate1[1]) + Convert.ToInt32(diff);
                    }

                    txtAge.Text = Convert.ToString(age);
                    txtMonth.Text = Convert.ToString(Month);

                }
                else
                {

                    txtAge.Text = "0";
                    Int32 Month;

                    if (Convert.ToInt32(arrTodayDate1[1]) >= Convert.ToInt32(arrDOB[1]))
                    {
                        Month = Convert.ToInt32(arrTodayDate1[1]) - Convert.ToInt32(arrDOB[1]);

                    }

                    else
                    {
                        Month = Convert.ToInt32(arrTodayDate1[1]) + 11 - Convert.ToInt32(arrDOB[1]);
                    }

                    txtMonth.Text = Convert.ToString(Month);

                }
            }
            else
            {

                txtAge.Text = "0";
            }


        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='PTREG' ";

            DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);
            ViewState["EmiratesIdMandatory"] = "0";
            ViewState["NationalityMandatory"] = "0";
            ViewState["AgeMandatory"] = "0";
            ViewState["DeductibleMandatory"] = "0";
            ViewState["LABEL_PRINT"] = "1";
            ViewState["DATABASE_DATE"] = "1";
            ViewState["DEP_WISE_VISIT_TYPE"] = "0";
            ViewState["PT_SIGNATURE"] = "0";
            ViewState["OTHER_INFO_DISPLAY"] = "0";
            ViewState["PT_COMP_ADD"] = "0";
            ViewState["PT_TITLE_NAME"] = "0";
            ViewState["PLAN_TYPE_STORE_SCAN_CARD"] = "0";
            ViewState["APPOINTMENT_ALERT"] = "0";
            ViewState["CityMandatory"] = "0";
            ViewState["VisaTypeMandatory"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "EMIRATES_ID")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EmiratesIdMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "NATIONALITY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NationalityMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "AGE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["AgeMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DEDUCTIBLE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DeductibleMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "LABEL_PRINT")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["LABEL_PRINT"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DATABASE_DATE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DATABASE_DATE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DEP_WISE_VISIT_TYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEP_WISE_VISIT_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_SIGNATURE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_SIGNATURE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "SIGNATURE_PAD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            hidSignaturePad.Value = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "OTHER_INFO_DISPLAY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OTHER_INFO_DISPLAY"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_COMP_ADD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_COMP_ADD"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_TITLE_NAME")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_TITLE_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PLAN_TYPE_STORE_SCAN_CARD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PLAN_TYPE_STORE_SCAN_CARD"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APPOINTMENT_ALERT")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["APPOINTMENT_ALERT"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CITY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CityMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "VISATYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["VisaTypeMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CREDITDTLS_CLEAR")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CREDITDTLS_CLEAR"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }






                }
            }


        }

        void BindPatientVisitLabel()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            Criteria += " AND HPV_PT_ID='" + hidActualFileNo.Value + "'";

            //if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            //{
            //    if (txtDepName.Text != "")
            //    {
            //        Criteria += " AND HPV_DEP_NAME='" + txtDepName.Text.Trim() + "'";
            //    }
            //}

            DS = new DataSet();
            DS = dbo.PatientVisitGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                hidPTVisitSQNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);

                int intCount = DS.Tables[0].Rows.Count;

                if (DS.Tables[0].Rows[0].IsNull("HPV_DATEDesc") == false)
                {
                    lblLastVisit.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATEDesc"]);//.Remove(10,12);
                }
                lblPrevDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                lblPrevDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);


                if (DS.Tables[0].Rows.Count > 1)
                {
                    if (DS.Tables[0].Rows[1].IsNull("HPV_DATEDesc") == false)
                    {
                        lblPrevVisit.Text = Convert.ToString(DS.Tables[0].Rows[1]["HPV_DATEDesc"]);//.Remove(10,12);
                    }
                }
                else
                {
                    lblPrevVisit.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATEDesc"]);//.Remove(10,12);
                }

                if (DS.Tables[0].Rows[intCount - 1].IsNull("HPV_DATEDesc") == false)
                {
                    lblFirstVisit.Text = Convert.ToString(DS.Tables[0].Rows[intCount - 1]["HPV_DATEDesc"]);//.Remove(10,12);
                }

            }
        }


        void GetPatientVisit()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            // Criteria += " AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPV_PT_ID='" + hidActualFileNo.Value + "'";

            if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            {
                if (txtDepName.Text != "")
                {
                    Criteria += " AND HPV_DEP_NAME='" + txtDepName.Text.Trim() + "'";
                }
            }
            //VISIT DATE GRATER THAN 3 YEARS TREAT AS NEW PATIENT
            Criteria += " AND  DATEADD(YY,-3, CONVERT(DATETIME,GETDATE(),101)) <= CONVERT(DATETIME, HPV_DATE,101) ";
            DS = new DataSet();
            DS = dbo.PatientVisitGet(Criteria);

            lblVisitType.Text = "New";
            if (DS.Tables[0].Rows.Count > 0)
            {
                int intCount = DS.Tables[0].Rows.Count;
                string strLastVisit = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATE"]);

                int intRevisit = Convert.ToInt32(Session["RevisitDays"]);
                if (strLastVisit != "")
                {

                    DateTime dtLastVisit = Convert.ToDateTime(strLastVisit);
                    DateTime Today;
                    if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    {
                        Today = Convert.ToDateTime(hidTodayDBDate.Value);
                    }
                    else
                    {
                        Today = Convert.ToDateTime(Convert.ToString(Session["LocalDate"]));
                    }

                    TimeSpan Diff = Today.Date - dtLastVisit.Date;

                    if (Diff.Days <= intRevisit)
                    {
                        lblVisitType.Text = "Revisit";
                    }
                    else if (Diff.Days >= intRevisit)
                    {
                        lblVisitType.Text = "Old";
                    }

                }
            }

        }

        void BindDepartment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            ds = dbo.DepartmentGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HD_INITIALNO_ID";
                drpDepartment.DataTextField = "HD_DESC";
                drpDepartment.DataBind();



            }

            ds.Clear();
            ds.Dispose();
        }


        void BindCity()
        {
            DataSet ds = new DataSet();
            ds = dbo.city();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCity.DataSource = ds;
                drpCity.DataValueField = "HCIM_NAME";
                drpCity.DataTextField = "HCIM_NAME";
                drpCity.DataBind();
            }
            drpCity.Items.Insert(0, "--- Select ---");
            drpCity.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindCountry()
        {
            DataSet ds = new DataSet();
            ds = dbo.country();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCountry.DataSource = ds;
                drpCountry.DataValueField = "hcm_name";
                drpCountry.DataTextField = "hcm_name";
                drpCountry.DataBind();

            }
            drpCountry.Items.Insert(0, "--- Select ---");
            drpCountry.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindArea()
        {
            DataSet ds = new DataSet();
            ds = dbo.area();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpArea.DataSource = ds;
                drpArea.DataValueField = "ham_area_name";
                drpArea.DataTextField = "ham_area_name";
                drpArea.DataBind();
            }

            drpArea.Items.Insert(0, "--- Select ---");
            drpArea.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindNationality()
        {
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();




            }
            drpNationality.Items.Insert(0, "--- Select ---");
            drpNationality.Items[0].Value = "0";



            ds.Clear();
            ds.Dispose();

        }

        void BindIDCaptionMaster()
        {
            DataSet ds = new DataSet();
            ds = dbo.IDCaptionMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpIDCaption.DataSource = ds;
                drpIDCaption.DataValueField = "HIC_ID_CAPTION";
                drpIDCaption.DataTextField = "HIC_Name_CAPTION";
                drpIDCaption.DataBind();
            }
            ds.Clear();
            ds.Dispose();

        }


        void BindPayer()
        {
            string Criteria = " 1=1  and HPC_STATUS='A' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.PatientCompanyGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpPayer.DataSource = ds;
                drpPayer.DataValueField = "HPC_PATIENT_COMPANY";
                drpPayer.DataTextField = "HPC_PATIENT_COMPANY";
                drpPayer.DataBind();
            }

            drpPayer.Items.Insert(0, "--- Select ---");
            drpPayer.Items[0].Value = "0";
        }


        void BindDefaultCompany()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (drpParentCompany.SelectedIndex != 0)
            {
                Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE  AND HCM_BILL_CODE ='" + drpParentCompany.SelectedValue + "' ";
            }


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDefaultCompany.DataSource = ds;
                drpDefaultCompany.DataValueField = "HCM_COMP_ID";
                drpDefaultCompany.DataTextField = "HCM_NAME";
                drpDefaultCompany.DataBind();
            }

            drpDefaultCompany.Items.Insert(0, "--- Select ---");
            drpDefaultCompany.Items[0].Value = "0";
        }

        void BindNetworkName()
        {
            //string Criteria = " 1=1 ";
            //Criteria += " AND HICM_STATUS = 'A' AND HICM_PACKAGE='Y'";
            //Criteria += " AND hicm_ins_Cat_id IN ( select hcb_cat_type from HMS_COMP_BENEFITS where HCB_COMP_ID='" + drpDefaultCompany.SelectedValue + "')";

            //drpNetworkName.Items.Clear();
            //DataSet ds = new DataSet();
            //ds = dbo.InsCatMasterGet(Criteria);
            //if (ds.Tables[0].Rows.Count > 0)
            //{

            //    drpNetworkName.DataSource = ds;
            //    drpNetworkName.DataValueField = "HICM_INS_CAT_ID";
            //    drpNetworkName.DataTextField = "HICM_INS_CAT_NAME";
            //    drpNetworkName.DataBind();
            //}
            //drpNetworkName.Items.Insert(0, "--- Select ---");
            //drpNetworkName.Items[0].Value = "0";
            //ds.Clear();
            //ds.Dispose();

            DataSet Ds1 = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
            Ds1 = dbo.retun_inccmp_details(Criteria);

            drpNetworkName.Items.Clear();

            if (drpNetworkName.Items.Count == 0)
            {
                drpNetworkName.Items.Insert(0, "--- Select ---");
                drpNetworkName.Items[0].Value = "0";
            }

            if (Ds1.Tables[0].Rows.Count > 0)
            {
                string strPackage = Convert.ToString(Ds1.Tables[0].Rows[0]["HCM_PACKAGE_DETAILS"]);
                string[] arrPackage = strPackage.Split('|');




                for (int j = 0; j <= arrPackage.Length - 1; j++)
                {

                    drpNetworkName.Items.Insert(j + 1, arrPackage[j]);
                    drpNetworkName.Items[j + 1].Value = arrPackage[j];

                }
            }


        }

        void CompanyParentGet()
        {

            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND HCM_COMP_ID in (select HCM_BILL_CODE from HMS_COMPANY_MASTER where  HCM_COMP_ID !=  HCM_BILL_CODE ) ";
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpParentCompany.DataSource = ds;
                drpParentCompany.DataValueField = "HCM_COMP_ID";
                drpParentCompany.DataTextField = "HCM_NAME";
                drpParentCompany.DataBind();
            }
            drpParentCompany.Items.Insert(0, "All");
            drpParentCompany.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }


        void BindCompanyDetails(out string strRemainderRecept)
        {
            strRemainderRecept = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
            DataSet ds = new DataSet();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HCM_REM_RCPTN") == false)
                    strRemainderRecept = Convert.ToString(ds.Tables[0].Rows[0]["HCM_REM_RCPTN"]);




            }


        }

        void BinScanCardMasterGet()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HSM_Card_Type = '" + drpCardType.SelectedValue + "'";

            DataSet ds = new DataSet();
            ds = dbo.ScanCardMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCardName.DataSource = ds;
                drpCardName.DataValueField = "HSM_Card_Name";
                drpCardName.DataTextField = "HSM_Card_Name";
                drpCardName.DataBind();
            }


            ds.Clear();
            ds.Dispose();

        }

        string ScanCardTypeGet(string CardName)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HSM_Card_Name = '" + CardName + "'";

            DataSet ds = new DataSet();
            ds = dbo.ScanCardMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0]["HSM_Card_Type"]);
            }

            return "";

        }




        decimal fnCalculatePTAmount(string AmountType)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HPB_PT_ID='" + txtFileNo.Text + "' ";
            switch (AmountType)
            {
                case "INVOICE":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "RETURN":
                    Criteria += " AND HPB_TRANS_TYPE='INV_RTN' ";
                    break;
                case "PAIDAMT":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "RETURNPAIDAMT":
                    Criteria += " AND HPB_TRANS_TYPE='INV_RTN' ";
                    break;
                case "REJECTED":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "ADVANCE":
                    Criteria += " AND HPB_TRANS_TYPE='ADVANCE' ";
                    break;
                case "NPPAID":

                    break;
                default:
                    break;
            }
            DataSet DS = new DataSet();
            DS = dbo.GetPatientBalHisTotal(Criteria);

            decimal Amt = 0;
            if (DS.Tables[0].Rows.Count > 0)
            {
                switch (AmountType)
                {
                    case "INVOICE":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalInvoiceAmout"]);
                        break;
                    case "RETURN":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalInvoiceAmout"]);
                        break;
                    case "PAIDAMT":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "RETURNPAIDAMT":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "REJECTED":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalRejectAmount"]);
                        break;
                    case "ADVANCE":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "NPPAID":

                        break;
                    default:
                        break;
                }
            }

            return Amt;
        }


        void BindToken()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();

            Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text.Trim() + "'";
            DS = dbo.retun_doctor_detailss(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["TokenNo"]).Trim();
            }

        }

        void BindPriority()
        {
            string Criteria = " 1=1 ";
            DataSet ds = new DataSet();
            ds = dbo.PriorityGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                drpPriority.DataSource = ds;
                drpPriority.DataValueField = "HVP_SLNO";
                drpPriority.DataTextField = "HVP_DESC";
                drpPriority.DataBind();

            }
            drpPriority.Items.Insert(0, "--- Select ---");
            drpPriority.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindKnowFrom()
        {
            string Criteria = " 1=1 ";
            DataSet ds = new DataSet();
            ds = dbo.KnowFromGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                drpKnowFrom.DataSource = ds;
                drpKnowFrom.DataValueField = "Description";
                drpKnowFrom.DataTextField = "Description";
                drpKnowFrom.DataBind();

            }
            drpKnowFrom.Items.Insert(0, "--- Select ---");
            drpKnowFrom.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }


        void BindLastCunsultDtls()
        {


            DataSet ds = new DataSet();
            ds = dbo.GetLastCunsultDtls(hidActualFileNo.Value);
            divCunsult.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                divCunsult.Visible = true;
                lblLastCunsultDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HIM_DATE"]);
                lblLastCunsultAmount.Text = Convert.ToString(ds.Tables[0].Rows[0]["HIT_AMOUNT"]);

            }

            ds.Clear();
            ds.Dispose();

        }


        string GetDrDeptName(string DrID)
        {
            string strDeptName = "";

            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_STAFF_ID='" + DrID + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strDeptName = Convert.ToString(ds.Tables[0].Rows[0]["HSFM_DEPT_ID"]);
            }

            return strDeptName;
        }

        Boolean GetAppointment()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HAM_FILENUMBER='" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND  convert(varchar(10),HAM_STARTTIME,103)  =   convert(varchar(10),getdate(),103)  ";
            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                strMessage += " This patient have an appointment in ";

                for (int i = 0; i < DSAppt.Tables[0].Rows.Count; i++)
                {
                    string strDeptName = "";

                    strDeptName = GetDrDeptName(Convert.ToString(DSAppt.Tables[0].Rows[i]["HAM_DR_CODE"]));

                    strMessage += strDeptName + "  at " + Convert.ToString(DSAppt.Tables[0].Rows[i]["AppintmentSTTime"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[i]["AppintmentFITime"]) + ",";

                }
                return true;
            }

            return false;
        }

        void BindModifyData()
        {

            Boolean isEFilling;
            if (GlobalValues.FileDescription.ToUpper() != "ALNOOR" && GlobalValues.FileDescription.ToUpper() != "ALTAIF")
            {
                isEFilling = dbo.CheckEFilling(Convert.ToString(Session["ScanSave"]), txtFileNo.Text);

                if (isEFilling == true)
                {
                    lblEFilling.Text = "E-Filling- Found";
                }
                else
                {
                    lblEFilling.Text = "E-Filling- Not Found";
                }
            }

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);

            //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindModifyData() Start");

            if (ds.Tables[0].Rows.Count > 0)
            {
                BindModifyData(ds);


                if (txtDOB.Text.Trim() != "")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                    AgeCalculation();
                }

                if (Convert.ToString(ViewState["APPOINTMENT_ALERT"]) == "1")
                {
                    if (GetAppointment() == true)
                    {
                        ModalPopupExtender1.Show();
                    }
                }
            }
            else
            {
                Clear();
            }
        }


        void BindModifyData(DataSet ds)
        {

            string strPolicyType = "";
            if (ds.Tables[0].Rows.Count > 0)
            {




                ViewState["Patient_ID_VALID"] = "1";
                hidActualFileNo.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]);
                hidTodayDBDate.Value = Convert.ToString(ds.Tables[0].Rows[0]["TodayDBDate"]);
                hidLatestVisit.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LATESTVISIT"]);
                hidLatestVisitHours.Value = Convert.ToString(ds.Tables[0].Rows[0]["LastVisitedHours"]);
                hidLastVisitDrId.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);


                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_STATUS"]) == "A")
                {
                    chkStatus.Checked = true;
                }
                else
                {
                    chkStatus.Checked = false;
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_TITLE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]) != "0")
                {
                    for (int intCount = 0; intCount < drpTitle.Items.Count; intCount++)
                    {
                        if (drpTitle.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]))
                        {
                            drpTitle.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]);
                            goto ForTitle;
                        }

                    }
                }
            ForTitle: ;
                txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]);
                txtMName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_MNAME"]);
                txtLName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_LNAME"]);


                if (ds.Tables[0].Rows[0].IsNull("HPM_BLOOD_GROUP") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]) != "0")
                {
                    for (int intCount = 0; intCount < drpBlood.Items.Count; intCount++)
                    {
                        if (drpBlood.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]))
                        {
                            drpBlood.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]);
                            goto ForBoold;
                        }

                    }
                }
            ForBoold: ;

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPatientType.Items.Count; intCount++)
                    {
                        if (drpPatientType.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]))
                        {
                            drpPatientType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]);
                            goto ForPTType;
                        }

                    }
                }

            ForPTType: ;

                txtPTCompany.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPayer.Items.Count; intCount++)
                    {
                        if (drpPayer.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]))
                        {
                            drpPayer.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                            goto ForPayer;
                        }

                    }
                }

            ForPayer: ;
                string strInsCompId = "", strNetworkName = "";
                if (ds.Tables[0].Rows[0].IsNull("HPM_BILL_CODE") == false)
                {


                    if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_BILL_CODE"]) != Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]) && strNetwork == "true")
                    {
                        strInsCompId = Convert.ToString(ds.Tables[0].Rows[0]["HPM_BILL_CODE"]);
                        stParentName1 = Convert.ToString(ds.Tables[0].Rows[0]["HPM_BILL_CODE"]);
                        strNetworkName = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                    }
                    else
                    {
                        strInsCompId = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                        stParentName1 = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                    }


                }






                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtPolicyId.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAMILY_ID"]);
                strPolicyType = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_PACKAGE_NAME") == false)
                {
                    txtNetworkClass.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PACKAGE_NAME"]);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_CONT_EXPDATEDesc") == false)
                {

                    txtExpiryDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]);



                }
                else
                {
                    txtExpiryDate.Text = "";

                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_STARTDesc") == false)
                {

                    txtPolicyStart.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_STARTDesc"]);



                }
                else
                {
                    txtPolicyStart.Text = "";

                }

                txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ADDR"]);
                txtPoBox.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POBOX"]);
                if (ds.Tables[0].Rows[0].IsNull("HPM_CITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]) != "0")
                {
                    for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                    {
                        if (drpCity.Items[intCityCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]))
                        {
                            drpCity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]);
                            goto ForCity;
                        }

                    }
                }

            ForCity: ;
                if (ds.Tables[0].Rows[0].IsNull("HPM_AREA") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]) != "0")
                {
                    for (int intAreaCount = 0; intAreaCount < drpArea.Items.Count; intAreaCount++)
                    {
                        if (drpArea.Items[intAreaCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]))
                        {
                            drpArea.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]);
                            goto ForArea;
                        }

                    }
                }
            ForArea: ;
                if (ds.Tables[0].Rows[0].IsNull("HPM_COUNTRY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]) != "0")
                {
                    for (int intCountryCount = 0; intCountryCount < drpCountry.Items.Count; intCountryCount++)
                    {
                        if (drpCountry.Items[intCountryCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]))
                        {
                            drpCountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]);
                            goto ForCountry;
                        }

                    }
                }
            ForCountry: ;

                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                {
                    for (int intNatiCount = 0; intNatiCount < drpNationality.Items.Count; intNatiCount++)
                    {
                        if (drpNationality.Items[intNatiCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]))
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNation;
                        }

                    }
                }
            ForNation: ;

                if (ds.Tables[0].Rows[0].IsNull("HPM_VISA_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]) != "0")
                {
                    for (int intVisaType = 0; intVisaType < drpVisaType.Items.Count; intVisaType++)
                    {
                        if (drpVisaType.Items[intVisaType].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]))
                        {
                            drpVisaType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]);
                            goto ForVisaType;
                        }

                    }
                }
            ForVisaType: ;




                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE1") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]) != "")
                {
                    txtPhone1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]).Trim();
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_FAX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]) != "")
                {
                    txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]).Trim();
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    txtMobile1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]).Trim();
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE2") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE2"]) != "")
                {
                    txtMobile2.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE2"]).Trim();
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE3") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE3"]) != "")
                {
                    txtPhone3.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE3"]).Trim();
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_EMAIL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]) != "")
                {
                    txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]).Trim();
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                {
                    txtEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]).Trim();
                }



                if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false)
                {
                    drpIDCaption.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]);
                }

                txtIDNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_NO"]);
                if (ds.Tables[0].Rows[0].IsNull("HPM_DOBDesc") == false)
                {
                    txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                }
                else
                {
                    txtDOB.Text = "";


                }
                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);
                txtMonth.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);



                if (ds.Tables[0].Rows[0].IsNull("HPM_MARITAL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]) != "0")
                {
                    for (int intCount = 0; intCount < drpMStatus.Items.Count; intCount++)
                    {
                        if (drpMStatus.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]))
                        {
                            drpMStatus.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]);
                            goto ForMStatus;
                        }

                    }
                }
            ForMStatus: ;


                if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "0")
                {
                    for (int intCount = 0; intCount < drpSex.Items.Count; intCount++)
                    {
                        if (drpSex.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;
                if (Convert.ToString(Session["DrNameDisplay"]).ToUpper() == "Y")
                {
                    txtDoctorID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]).Trim();
                    txtDoctorName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_NAME"]).Trim();
                    txtDepName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]).Trim();
                }
                //  txtTokenNo.Text = ds.Tables[0].Rows[0]["HPM_TOKEN_NO"].ToString();
                lblVisitedBranch.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LASTVISIT_BR"]);
                //if (ds.Tables[0].Rows[0].IsNull("HPM_FIRSTVISITDesc") == false)
                //{
                //    lblFirstVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FIRSTVISITDesc"]);//.Remove(10,12);
                //}
                //if (ds.Tables[0].Rows[0].IsNull("HPM_PREV_VISITDesc") == false)
                //{
                //    lblPrevVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PREV_VISITDesc"]);//.Remove(10,12);
                //}
                //if (ds.Tables[0].Rows[0].IsNull("HPM_LATESTVISITDesc") == false)
                //{
                //    lblLastVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LATESTVISITDesc"]);//.Remove(10,12);
                //}
                //lblPrevDrId.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PRE_VISIT_DR_ID"]);
                //lblPrevDrName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PRE_VISIT_DR_NAME"]);

                lblCreadedUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CREATED_USER"]);
                lblModifiedUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MODIFIED_USER"]);







                if (Convert.ToString(ViewState["HPVSeqNo"]) != null && Convert.ToString(ViewState["HPVSeqNo"]) != "")
                {
                    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false)
                    {
                        txtRefDoctorID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                        //.Remove(10, 12)
                    }
                    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_NAME") == false)
                    {
                        txtRefDoctorName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                        //.Remove(10, 12)
                    }
                }

                //    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "0")
                //    {
                //        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                //        {
                //            if (drpRefDoctor.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]))
                //            {
                //                drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                //                goto ForRefDoc;
                //            }

                //        }
                //    }
                //ForRefDoc: ;


                txtKinName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_NAME"]);
                txtKinRelation.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_RELATION"]);
                txtKinPhone.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_PHONE"]);
                txtKinMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_MOBILE"]);
                txtKinMobile1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_MOBILE1"]);

                txtNoOfChildren.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NO_CHILDREN"]);

                txtOccupation.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_OCCUPATION"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_PATIENT_STATUS") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPTStatus.Items.Count; intCount++)
                    {
                        if (drpPTStatus.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]))
                        {
                            drpPTStatus.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]);
                        }

                    }
                }


                txtHoldMsg.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_STATUS_MSG"]);

                if (drpPTStatus.SelectedValue == "Hold")
                {
                    strHoldMessage = "Patient Hold Message</br>" + Convert.ToString(ds.Tables[0].Rows[0]["HPM_STATUS_MSG"]);
                    ModalPopupExtender2.Show();
                }

                if (drpPatientType.SelectedValue == "CR")
                {
                    //txtPolicyNo.Enabled = true;
                    txtIDNo.Enabled = true;
                    //txtExpiryDate.Enabled = true;
                    // txtPolicyStart.Enabled = true;
                    txtPolicyId.Enabled = true;
                    // drpPlanType.Enabled = true;
                    drpNetworkName.Enabled = true;
                    drpDefaultCompany.Enabled = true;
                }
                //string strPath = Server.MapPath("~/img/" + txtFileNo.Text + "/InsCardFront/");
                //DirectoryInfo di = new DirectoryInfo(strPath);
                //if (di.Exists)
                //{
                //    FileInfo[] files = di.GetFiles();
                //    string filepath = files[0].Name;
                //    imgFront.ImageUrl = @"~/img/" + txtFileNo.Text + "/InsCardFront/" + filepath;

                //}
                //string strPath1 = Server.MapPath("~/img/" + txtFileNo.Text + "/InsCardBack/");
                //DirectoryInfo di1 = new DirectoryInfo(strPath1);
                //if (di1.Exists)
                //{
                //    FileInfo[] files1 = di1.GetFiles();
                //    string filepath1 = files1[0].Name;
                //    imgBack.ImageUrl = @"~/img/" + txtFileNo.Text + "/InsCardBack/" + filepath1;
                //}

                string strDedType = "", strCoInsType = "";
                if (ds.Tables[0].Rows[0].IsNull("HPM_DED_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DED_TYPE"]) != "")
                {
                    strDedType = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DED_TYPE"]);
                }



                decimal DedAmt = 0;
                if (ds.Tables[0].Rows[0].IsNull("HPM_DED_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DED_AMT"]) != "")
                {
                    DedAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_DED_AMT"]);


                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_COINS_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COINS_TYPE"]) != "")
                {
                    strCoInsType = Convert.ToString(ds.Tables[0].Rows[0]["HPM_COINS_TYPE"]);

                }
                decimal CoInsAmt = 0;
                if (ds.Tables[0].Rows[0].IsNull("HPM_COINS_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COINS_AMT"]) != "")
                {
                    CoInsAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_COINS_AMT"]);

                }


                ds.Clear();
                ds.Dispose();

                PatientPhoto();

                //BELOW FUNCTION FOR STORE THE DB IMAGE IN TO PHYSICAL PATH
                //if (drpPatientType.SelectedValue == "CR")
                // {
                //ProcessTimLog(System.DateTime.Now.ToString() + "      ---ScanCardImageGenerate() Start");
                ScanCardImageGenerate();
                //ProcessTimLog(System.DateTime.Now.ToString() + "      ---UploadDBImage() Start");
                UploadDBImage();
                //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindScanCardImageGet() Start");
                BindScanCardImageGet();
                //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindScanCardImageGet() End");
                // }
                Boolean bolCompSelected = false;
                if (drpPatientType.SelectedValue == "CR" && drpDefaultCompany.Items.Count > 0)
                {

                    if (strInsCompId != "")
                    {
                        for (int intCount = 0; intCount < drpDefaultCompany.Items.Count; intCount++)
                        {
                            if (drpDefaultCompany.Items[intCount].Value == strInsCompId)
                            {
                                drpDefaultCompany.SelectedValue = strInsCompId;
                                bolCompSelected = true;
                            }
                        }
                    }

                    if (bolCompSelected == true)
                    {
                        string strRemainderRecept = "";
                        //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindCompanyDetails() Start");
                        BindCompanyDetails(out strRemainderRecept);
                        //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindCompanyDetails() End");

                        strReminderMessage = "Message from Company Master <br/> " + strRemainderRecept;


                        if (strRemainderRecept != "")
                        {
                            ModalPopupExtender5.Show();
                        }
                    }
                    //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindSubCompany() Start");
                    BindSubCompany();
                    //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindSubCompany() End");
                    if (drpNetworkName.Items.Count > 0)
                    {
                        if (strNetworkName != "")
                        {
                            for (int intCount = 0; intCount < drpNetworkName.Items.Count; intCount++)
                            {
                                if (drpNetworkName.Items[intCount].Value == strNetworkName)
                                {
                                    drpNetworkName.SelectedValue = strNetworkName;
                                }
                            }

                        }
                    }
                    //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindPlanType() Start");
                    BindPlanType();
                    //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindPlanType() End");

                    if (strPolicyType != "")
                    {

                        for (int intCount = 0; intCount < drpPlanType.Items.Count; intCount++)
                        {
                            if (drpPlanType.Items[intCount].Value == strPolicyType)
                            {
                                drpPlanType.SelectedValue = strPolicyType;
                            }
                        }

                    }



                }





                if (strDedType != "")
                {

                    for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                    {
                        if (drpDedType.Items[intCount].Value == strDedType)
                        {
                            drpDedType.SelectedValue = strDedType;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                    {
                        if (drpDedType.Items[intCount].Value == "")
                        {
                            drpDedType.SelectedValue = "";
                        }
                    }
                }


                if (strCoInsType != "")
                {

                    for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                    {
                        if (drpCoInsType.Items[intCount].Value == strCoInsType)
                        {
                            drpCoInsType.SelectedValue = strCoInsType;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                    {
                        if (drpCoInsType.Items[intCount].Value == "")
                        {
                            drpCoInsType.SelectedValue = "";
                        }
                    }
                }

                if (DedAmt != 0)
                {
                    txtDeductible.Text = DedAmt.ToString("N2");

                }


                if (CoInsAmt != 0)
                {
                    txtCoIns.Text = CoInsAmt.ToString("N2");

                }



                ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindToken() Start");
                BindToken();

                ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindLastCunsultDtls() Start");
                // BindLastCunsultDtls(); TODO: This has taking more time so we commented.
                ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---GetPatientVisit() Start");
                GetPatientVisit();
                ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindPatientVisitLabel() Start");
                BindPatientVisitLabel();
                ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindPatientVisitLabel() Complete");

                if (drpPatientType.SelectedValue == "CR" && drpDefaultCompany.Items.Count > 0)
                {
                    if (CheckCompanyActive(strInsCompId) == false)
                    {

                        ModalPopupExtender1.Show();
                        strMessage = "This company is not active.";
                    }
                }
            }

        }

        void BindScanCardImageGet()
        {
            DataSet Ds1 = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";

            Ds1 = dbo.ScanCardImageGet(Criteria);
            ViewState["ScanCard"] = Ds1.Tables[0];

            if (Ds1.Tables[0].Rows.Count > 0)
            {
                gvScanCard.Visible = true;
                gvScanCard.DataSource = Ds1.Tables[0];
                gvScanCard.DataBind();


            }

            /*
            // FOR DEFAULT CARD DATA LODING COMMENTED AS PER CLIENT REQUEST -SMCH

            if (GlobalValues.FileDescription.ToUpper() != "SMCH")
            {
                for (int i = 0; i <= Ds1.Tables[0].Rows.Count - 1; i++)
                {
                    if (Ds1.Tables[0].Rows[i]["HSC_ISDEFAULT"].ToString() == "True" && Ds1.Tables[0].Rows[i]["HSC_CARD_NAME"].ToString() == "Insurance")
                    {

                        btnScanCardAdd.Visible = false;
                        btnScanCardUpdate.Visible = true;


                        ViewState["ScanCardSelectIndex"] = i;


                        drpCardName.SelectedValue = Ds1.Tables[0].Rows[i]["HSC_CARD_NAME"].ToString();
                        txtCompany.Text = Ds1.Tables[0].Rows[i]["HSC_INS_COMP_ID"].ToString();
                        txtCompanyName.Text = Ds1.Tables[0].Rows[i]["HSC_INS_COMP_NAME"].ToString();
                        txtCardNo.Text = Ds1.Tables[0].Rows[i]["HSC_CARD_NO"].ToString();
                        txtPolicyCardNo.Text = Ds1.Tables[0].Rows[i]["HSC_POLICY_NO"].ToString();
                        txtCardExpDate.Text = Convert.ToString(Ds1.Tables[0].Rows[i]["HSC_EXPIRY_DATE"].ToString());
                        chkIsDefault.Checked = Convert.ToBoolean(Ds1.Tables[0].Rows[i]["HSC_ISDEFAULT"].ToString());
                        chkCardActive.Checked = Convert.ToBoolean(Ds1.Tables[0].Rows[i]["HSC_STATUS"].ToString());
                        imgFront.ImageUrl = @"../Uploads/" + Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_FRONT"].ToString();
                        imgBack.ImageUrl = @"../Uploads/" + Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_BACK"].ToString();

                        if (Ds1.Tables[0].Rows[i].IsNull("HSC_IMAGE_PATH_FRONT") == false)
                        {
                            Session["InsCardFrontPath"] = Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_FRONT"].ToString();
                        }
                        if (Ds1.Tables[0].Rows[i].IsNull("HSC_IMAGE_PATH_BACK") == false)
                        {
                            Session["InsCardBackPath"] = Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_BACK"].ToString();
                        }


                    }

                }
            
            }
 */
        }


        void BindScanCardImage()
        {
            DataSet Ds1 = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";

            Ds1 = dbo.ScanCardImageGet(Criteria);
            ViewState["ScanCard"] = Ds1.Tables[0];

            if (Ds1.Tables[0].Rows.Count > 0)
            {
                gvScanCard.Visible = true;
                gvScanCard.DataSource = Ds1.Tables[0];
                gvScanCard.DataBind();


            }


        }
        void BindScanCardImageSelected()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ScanCard"];
            DataRow[] TempRow;
            TempRow = DT.Select("HSC_INS_COMP_ID ='" + drpDefaultCompany.SelectedValue + "' AND HSC_ISDEFAULT=TRUE");

            txtIDNo.Text = "";
            txtPolicyNo.Text = "";
            txtExpiryDate.Text = "";
            txtPolicyStart.Text = "";
            txtNetworkClass.Text = "";

            if (TempRow.Length > 0)
            {

                txtIDNo.Text = Convert.ToString(TempRow[0]["HSC_CARD_NO"]);
                txtPolicyNo.Text = Convert.ToString(TempRow[0]["HSC_POLICY_NO"]);
                txtExpiryDate.Text = Convert.ToString(TempRow[0]["HSC_EXPIRY_DATE"]);
                txtPolicyStart.Text = Convert.ToString(TempRow[0]["HSC_POLICY_START"]);


                if (Convert.ToString(TempRow[0]["HSC_PLAN_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpPlanType.Items.Count; intCount++)
                    {
                        if (drpPlanType.Items[intCount].Value == Convert.ToString(TempRow[0]["HSC_PLAN_TYPE"]))
                        {
                            drpPlanType.SelectedValue = Convert.ToString(TempRow[0]["HSC_PLAN_TYPE"]);
                            goto ForPlan;
                        }

                    }
                }
            ForPlan: ;


            }



        }

        void Clear()
        {
            TabContainer1.ActiveTab = TabPanelGeneral;
            string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);
            switch (AutoSlNo)
            {
                case "A":
                    chkManual.Checked = false;
                    chkManual.Visible = false;
                    // txtFileNo.ReadOnly = true;
                    btnNew.Visible = true;
                    break;
                case "M":
                    chkManual.Checked = true;
                    chkManual.Visible = false;
                    txtFileNo.ReadOnly = false;
                    btnNew.Visible = false;
                    break;
                case "B":
                    // chkManual.Checked = true;
                    chkManual.Visible = true;
                    btnNew.Visible = true;
                    break;
                default:
                    break;

            }
            if (hidPateName.Value != "HomeCare")
            {
                chkUpdateWaitList.Checked = true;
            }
            hidLatestVisitHours.Value = "";
            hidLastVisitDrId.Value = "";
            hidWaitingList.Value = "";
            stParentName = "0";
            stParentName1 = "0";

            lblEFilling.Text = "";
            lblStatus.Text = "";
            // imgPhoto.Visible = false;
            lblVisitType.Text = "New";

            //if (drpDepartment.Items.Count > 0)
            //    drpDepartment.SelectedIndex = 0;
            chkStatus.Checked = true;

            if (drpTitle.Items.Count > 0)
                drpTitle.SelectedIndex = 0;

            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";

            if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
            {
                drpPatientType.SelectedIndex = 1;
            }
            else
            {
                drpPatientType.SelectedIndex = 0;
            }

            txtDOB.Text = "";

            txtAge.Text = "";
            txtMonth.Text = "";
            drpSex.SelectedIndex = 0;



            if (chkFamilyMember.Checked == false)
            {
                txtPoBox.Text = "";
                txtAddress.Text = "";
                if (drpCity.Items.Count > 0)
                    drpCity.SelectedIndex = 0;
                if (drpArea.Items.Count > 0)
                    drpArea.SelectedIndex = 0;
                if (drpCountry.Items.Count > 0)
                    drpCountry.SelectedIndex = 0;
                if (drpNationality.Items.Count > 0)
                    drpNationality.SelectedIndex = 0;

                txtPhone1.Text = "";
                txtMobile1.Text = "";
                txtMobile2.Text = "";
                txtPhone3.Text = "";
                txtFax.Text = "";
                txtEmail.Text = "";
            }

            if (drpBlood.Items.Count > 0)
                drpBlood.SelectedIndex = 0;
            if (drpMStatus.Items.Count > 0)
                drpMStatus.SelectedIndex = 0;
            if (drpNetworkName.Items.Count > 0)
                drpNetworkName.SelectedIndex = 0;





            if (drpPriority.Items.Count > 0)
                drpPriority.SelectedIndex = 0;


            txtEmiratesID.Text = "";
            if (drpIDCaption.Items.Count > 0)
            {
                drpIDCaption.SelectedIndex = 0;
            }
            txtDoctorID.Text = "";
            txtDoctorName.Text = "";
            txtDepName.Text = "";
            txtTokenNo.Text = "";

            //if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
            //{
            //    chkUpdateWaitList.Visible = false;
            //    //chkUpdateWaitList.Checked = false;
            //}
            //else
            //{
            //    chkUpdateWaitList.Visible = true;
            //    // chkUpdateWaitList.Checked = true;
            //}
            chkManualToken.Checked = false;




            txtRefDoctorID.Text = "";
            txtRefDoctorName.Text = "";

            txtKinName.Text = "";
            txtKinRelation.Text = "";
            txtKinPhone.Text = "";
            txtKinMobile.Text = "";
            txtKinMobile1.Text = "";
            txtNoOfChildren.Text = "";
            if (drpDefaultCompany.Items.Count > 0)
            {
                drpDefaultCompany.SelectedIndex = 0;
            }



            if (drpPatientType.SelectedValue == "CR")
            {
                //txtPolicyNo.Enabled = true;
                txtPolicyId.Enabled = true;
                txtIDNo.Enabled = true;
                //  txtExpiryDate.Enabled = true;
                // txtPolicyStart.Enabled = true;
                // drpPlanType.Enabled = true;
                drpDefaultCompany.Enabled = true;
                drpNetworkName.Enabled = true;
            }
            else
            {
                // txtPolicyNo.Enabled = false;
                txtPolicyId.Enabled = false;
                txtIDNo.Enabled = false;
                //  txtExpiryDate.Enabled = false;
                //  txtPolicyStart.Enabled = false;
                drpDefaultCompany.Enabled = false;
                // drpPlanType.Enabled = false;
                drpNetworkName.Enabled = false;
            }



            txtPolicyNo.Text = "";
            txtPolicyId.Text = "";
            drpPlanType.Items.Clear();
            txtIDNo.Text = "";
            txtExpiryDate.Text = "";
            txtPolicyStart.Text = "";
            chkCompCashPT.Checked = false;

            txtPTCompany.Text = "";

            if (drpPayer.Items.Count > 0)
                drpPayer.SelectedIndex = 0;

            drpDedType.SelectedIndex = 0;
            drpCoInsType.SelectedIndex = 1;
            txtDeductible.Text = "";
            txtCoIns.Text = "";

            hidPTVisitSQNo.Value = "";
            lblVisitedBranch.Text = "";
            lblFirstVisit.Text = "";
            lblPrevVisit.Text = "";
            lblLastVisit.Text = "";
            lblPrevDrId.Text = "";
            lblPrevDrName.Text = "";

            lblCreadedUser.Text = "";
            lblModifiedUser.Text = "";

            txtOccupation.Text = "";
            if (drpPTStatus.Items.Count > 0)
                drpPTStatus.SelectedIndex = 0;
            txtHoldMsg.Text = "";


            txtNetworkClass.Text = "";



            chkLabelPrint.Checked = false;

            if (drpCardType.Items.Count > 0)
            {
                drpCardType.SelectedIndex = 0;
                drpCardType_SelectedIndexChanged(drpCardType, new EventArgs());
            }

            //if (drpCardName.Items.Count > 0)
            //    drpCardName.SelectedIndex = 0;

            if (drpParentCompany.Items.Count > 0)
                drpParentCompany.SelectedIndex = 0;
            txtCompany.Text = "";
            txtCompanyName.Text = "";
            txtCardNo.Text = "";
            txtPolicyCardNo.Text = "";
            txtCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();

            //if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
            //{
            txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    txtEffectiveDate.Text = hidLocalDate.Value;
            //}


            chkIsDefault.Checked = true;
            chkCardActive.Checked = true;

            txtTokenNo.Enabled = false;

            DataTable dt = new DataTable();
            BuildScanCard();
            dt = (DataTable)ViewState["ScanCard"];
            gvScanCard.DataSource = dt;
            gvScanCard.DataBind();
            gvScanCard.Visible = false;
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";


            imgFrontDefault.ImageUrl = "";
            imgBackDefault.ImageUrl = "";
            btnScanCardAdd.Visible = true;
            btnScanCardUpdate.Visible = false;


            if (drpVisaType.Items.Count > 0)
                drpVisaType.SelectedIndex = 0;





            divCunsult.Visible = false;

            ViewState["ScanCardSelectIndex"] = "";


            ViewState["Secondary_Ins"] = "";
            Session["InsCardFrontPath"] = "";
            Session["InsCardBackPath"] = "";
            ViewState["Patient_ID_VALID"] = "0";
            ViewState["PatientInfoId"] = "";

            ViewState["Signature1"] = "";
            ViewState["Signature2"] = "";
            ViewState["Signature3"] = "";

            Session["SignatureImg1"] = "";
            Session["SignatureImg2"] = "";
            Session["SignatureImg3"] = "";

            ViewState["HPVSeqNo"] = "";
            ViewState["HGC_ID"] = "";

            ViewState["EmirateIdRef"] = "";
            Session["Photo"] = "";

            ViewState["ConfirmResult"] = "";
            ViewState["EIDConfirmResult"] = "";
            ViewState["ConfirmMsgFor"] = "";
            ViewState["EmiratesIDPT_ID"] = "";
            Session["PatientID"] = "";
            lblConfirmMessage.Text = "";
        }

        void BindSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE = '" + drpDefaultCompany.SelectedValue + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            drpNetworkName.Items.Clear();
            drpPlanType.Items.Clear();
            txtDeductible.Text = "";
            //  txtCoIns.Text = "";

            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNetworkName.DataSource = ds;
                drpNetworkName.DataValueField = "HCM_COMP_ID";
                drpNetworkName.DataTextField = "HCM_NAME";
                drpNetworkName.DataBind();

            }

            drpNetworkName.Items.Insert(0, "--- Select ---");
            drpNetworkName.Items[0].Value = "0";
            drpNetworkName.SelectedIndex = 0;
        }

        void BindPlanType()
        {
            drpPlanType.Items.Clear();
            txtDeductible.Text = "";
            //  txtCoIns.Text = "";
            if (drpDefaultCompany.Items.Count > 0)
            {
                if (drpDefaultCompany.SelectedItem.Text != "")
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";
                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
                    }
                    DS = dbo.CompBenefitsGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        drpPlanType.DataSource = DS;
                        if (GlobalValues.FileDescription.ToUpper() == "AMAL")
                        {
                            drpPlanType.DataTextField = "HICM_INS_CAT_NAME";
                        }
                        else
                        {
                            drpPlanType.DataTextField = "HCB_CAT_TYPE";
                        }
                        drpPlanType.DataValueField = "HCB_CAT_TYPE";
                        drpPlanType.DataBind();
                    }

                    drpPlanType.Items.Insert(0, "--- Select ---");
                    drpPlanType.Items[0].Value = "0";
                }
            }

        }

        void BindDeductCoInsAmt()
        {

            if (drpDefaultCompany.Items.Count > 0)
            {
                if (drpDefaultCompany.SelectedItem.Text != "")
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";

                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "' and HCB_CAT_TYPE='" + drpPlanType.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpDefaultCompany.SelectedValue + "' and HCB_CAT_TYPE='" + drpPlanType.SelectedValue + "'";
                    }
                    DS = dbo.CompBenefitsGet(Criteria);
                    txtDeductible.Text = "";
                    txtCoIns.Text = "";
                    if (DS.Tables[0].Rows.Count > 0)
                    {


                        string strDedType = "", strCoInsType = "";

                        strDedType = Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_TYPE"]);
                        if (strDedType != "")
                        {

                            for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                            {
                                if (drpDedType.Items[intCount].Value == strDedType)
                                {
                                    drpDedType.SelectedValue = strDedType;
                                }
                            }

                        }
                        else
                        {

                            for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                            {
                                if (drpDedType.Items[intCount].Value == "")
                                {
                                    drpDedType.SelectedValue = "";
                                }
                            }
                        }

                        strCoInsType = Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_TYPE"]);
                        if (strCoInsType != "")
                        {

                            for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                            {
                                if (drpCoInsType.Items[intCount].Value == strCoInsType)
                                {
                                    drpCoInsType.SelectedValue = strCoInsType;
                                }
                            }

                        }
                        else
                        {

                            for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                            {
                                if (drpCoInsType.Items[intCount].Value == "")
                                {
                                    drpCoInsType.SelectedValue = "";
                                }
                            }
                        }



                        if (DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]) != "")
                        {
                            decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]);
                            txtDeductible.Text = DedAmt.ToString("N2");
                        }

                        if (DS.Tables[0].Rows[0].IsNull("HCB_COINS_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]) != "")
                        {
                            decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]);
                            txtCoIns.Text = CoInsAmt.ToString("N2");
                        }





                    }

                }
            }

        }


        void BindDeductCoInsGrid()
        {

            if (drpDefaultCompany.Items.Count > 0)
            {
                if (drpDefaultCompany.SelectedItem.Text != "" && drpPlanType.SelectedIndex != 0)
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";

                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCBD_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCBD_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
                    }

                    if (drpPlanType.SelectedIndex != 0)
                    {
                        Criteria += " AND HCBD_CAT_TYPE = '" + drpPlanType.SelectedValue + "'";
                    }

                    DS = dbo.CompBenefitsDtlsGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        gvHCB.Visible = true;
                        gvHCB.DataSource = DS;
                        gvHCB.DataBind();
                    }



                }
            }

        }

        void BuildScanCard()
        {
            DataTable dt = new DataTable();

            DataColumn HSC_CARD_NAME = new DataColumn();
            HSC_CARD_NAME.ColumnName = "HSC_CARD_NAME";

            DataColumn HSC_CARD_TYPE = new DataColumn();
            HSC_CARD_TYPE.ColumnName = "HSC_CARD_TYPE";

            DataColumn HSC_CARD_NO = new DataColumn();
            HSC_CARD_NO.ColumnName = "HSC_CARD_NO";

            DataColumn HSC_POLICY_NO = new DataColumn();
            HSC_POLICY_NO.ColumnName = "HSC_POLICY_NO";

            DataColumn HSC_INS_COMP_ID = new DataColumn();
            HSC_INS_COMP_ID.ColumnName = "HSC_INS_COMP_ID";

            DataColumn HSC_INS_COMP_NAME = new DataColumn();
            HSC_INS_COMP_NAME.ColumnName = "HSC_INS_COMP_NAME";


            DataColumn HSC_EXPIRY_DATE = new DataColumn();
            HSC_EXPIRY_DATE.ColumnName = "HSC_EXPIRY_DATE";

            DataColumn HSC_ISDEFAULT = new DataColumn();
            HSC_ISDEFAULT.ColumnName = "HSC_ISDEFAULT";

            DataColumn HSC_ISDEFAULTDesc = new DataColumn();
            HSC_ISDEFAULTDesc.ColumnName = "HSC_ISDEFAULTDesc";

            DataColumn HSC_STATUS = new DataColumn();
            HSC_STATUS.ColumnName = "HSC_STATUS";

            DataColumn HSC_STATUSDesc = new DataColumn();
            HSC_STATUSDesc.ColumnName = "HSC_STATUSDesc";

            DataColumn HSC_IMAGE_PATH_FRONT = new DataColumn();
            HSC_IMAGE_PATH_FRONT.ColumnName = "HSC_IMAGE_PATH_FRONT";

            DataColumn HSC_IMAGE_PATH_BACK = new DataColumn();
            HSC_IMAGE_PATH_BACK.ColumnName = "HSC_IMAGE_PATH_BACK";

            DataColumn HSC_ISNEW = new DataColumn();
            HSC_ISNEW.ColumnName = "HSC_ISNEW";

            DataColumn HSC_POLICY_START = new DataColumn();
            HSC_POLICY_START.ColumnName = "HSC_POLICY_START";


            DataColumn HSC_PLAN_TYPE = new DataColumn();
            HSC_PLAN_TYPE.ColumnName = "HSC_PLAN_TYPE";

            DataColumn HSC_PHOTO_ID = new DataColumn();
            HSC_PHOTO_ID.ColumnName = "HSC_PHOTO_ID";


            dt.Columns.Add(HSC_CARD_NAME);
            dt.Columns.Add(HSC_CARD_TYPE);
            dt.Columns.Add(HSC_CARD_NO);
            dt.Columns.Add(HSC_POLICY_NO);
            dt.Columns.Add(HSC_INS_COMP_ID);
            dt.Columns.Add(HSC_INS_COMP_NAME);
            dt.Columns.Add(HSC_EXPIRY_DATE);
            dt.Columns.Add(HSC_ISDEFAULT);
            dt.Columns.Add(HSC_ISDEFAULTDesc);
            dt.Columns.Add(HSC_STATUS);
            dt.Columns.Add(HSC_STATUSDesc);
            dt.Columns.Add(HSC_IMAGE_PATH_FRONT);
            dt.Columns.Add(HSC_IMAGE_PATH_BACK);
            dt.Columns.Add(HSC_ISNEW);
            dt.Columns.Add(HSC_POLICY_START);
            dt.Columns.Add(HSC_PLAN_TYPE);
            dt.Columns.Add(HSC_PHOTO_ID);

            ViewState["ScanCard"] = dt;
        }

        void ClearScan()
        {
            btnScanCardAdd.Visible = true;
            btnScanCardUpdate.Visible = false;

            if (drpCardType.Items.Count > 0)
            {
                drpCardType.SelectedIndex = 0;
                drpCardType_SelectedIndexChanged(drpCardType, new EventArgs());
            }

            //if (drpCardName.Items.Count > 0)
            //    drpCardName.SelectedIndex = 0;
            if (drpParentCompany.Items.Count > 0)
                drpParentCompany.SelectedIndex = 0;
            stParentName = "0";
            txtCompany.Text = "";
            txtCompanyName.Text = "";
            txtCardNo.Text = "";
            txtPolicyCardNo.Text = "";
            txtCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();
            //if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
            //{
            txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    txtEffectiveDate.Text = hidLocalDate.Value;
            //}
            chkIsDefault.Checked = true;
            chkCardActive.Checked = true;
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";

            txtPlanType.Text = "";

        }

        void BindXMLData()
        {

            DataSet DS = new DataSet();


            string strEMIdFilePath = System.Configuration.ConfigurationSettings.AppSettings["EMIdFilePath"].ToString();

            DS.ReadXml(strEMIdFilePath + "\\PublicData.xml");

            if (DS.Tables[0].Rows.Count > 0)
            {
                int i = DS.Tables[0].Rows.Count - 1;

                Session["Photo"] = (Byte[])DS.Tables[0].Rows[i]["Photo"];
                //    Session["Signature"] = (Byte[])DS.Tables[0].Rows[i]["Signature"];

                DataSet ds1 = new DataSet();
                string Criteria = " 1=1 ";
                Criteria += " AND HPM_IQAMA_NO  ='" + Convert.ToString(DS.Tables[0].Rows[i]["IDNumber"]).Trim() + "'";
                ds1 = dbo.retun_patient_details(Criteria);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    txtFileNo.Text = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();

                    DS.Dispose();
                    BindModifyData(ds1);

                }

                //chkManual.Checked = false;
                //chkManual_CheckedChanged("chkManual", new EventArgs());
                string strFullName = Convert.ToString(DS.Tables[0].Rows[i]["FullName"]).Trim();
                string[] ArrFullName = strFullName.Split(' ');

                txtLName.Text = "";
                if (ArrFullName.Length > 0)
                {

                    for (int j = 0; j <= ArrFullName.Length - 1; j++)
                    {
                        if (j == 0)
                        {
                            txtFName.Text = ArrFullName[j];
                        }
                        else if (j == 1)
                        {
                            txtMName.Text = ArrFullName[j];
                        }
                        else if (j >= 2)
                        {
                            txtLName.Text += ArrFullName[j] + " ";
                        }
                    }

                }

                txtEmiratesID.Text = Convert.ToString(DS.Tables[0].Rows[i]["IDNumber"]).Trim();
                txtCardNo.Text = Convert.ToString(DS.Tables[0].Rows[i]["CardNo"]).Trim();
                txtDOB.Text = Convert.ToString(DS.Tables[0].Rows[i]["DOB"]).Trim();


                drpSex.SelectedValue = Convert.ToString(DS.Tables[0].Rows[i]["Sex"]).Trim();
                drpMStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[i]["MaritalStatus"]).Trim();
                drpCity.SelectedValue = Convert.ToString(DS.Tables[0].Rows[i]["CityDesc"]).ToUpper().Trim();
                txtPoBox.Text = Convert.ToString(DS.Tables[0].Rows[i]["POBox"]).Trim();
                txtAddress.Text = Convert.ToString(DS.Tables[0].Rows[i]["FlatNo"]) + " " + Convert.ToString(DS.Tables[0].Rows[i]["Building"]) + " " + Convert.ToString(DS.Tables[0].Rows[i]["Street"]);
                txtPhone1.Text = Convert.ToString(DS.Tables[0].Rows[i]["Phone"]).Trim();
                txtMobile1.Text = Convert.ToString(DS.Tables[0].Rows[i]["Mobile"]).Trim();




                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "AgeCalculation();", true);



            }
            BindPhoto();
        }

        void BindPhoto()
        {
            try
            {

                //imgPhoto.ImageUrl = "";
                //imgPhoto.Visible = true;
                //imgPhoto.ImageUrl = "../DisplayImage.aspx";





            }
            catch (Exception e)
            {

                //  imgPhoto.ImageUrl = "../Images/Loginlogo.jpeg";
            }

        }

        //void CheckCity(String City)
        //{
        //    if (drpCity.SelectedIndex != 0)
        //    {
        //        dbo.CityMasterAdd(Convert.ToString(Session["Branch_ID"]), City, Convert.ToString(Session["User_ID"]));

        //    }
        //}



        string fnGetClaimFormName(String mCompId, String Dept)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = dbo.retun_inccmp_details(Criteria);

            string RptName;
            RptName = "HMSInsCardPrint.rpt";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "HMSInsCardPrint.rpt";
                }
                else
                {
                    RptName = (Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) + ".rpt");
                }

                if (Dept.ToUpper() == "DENTAL" && chkCompletionRpt.Checked == true)
                {
                    if (File.Exists(strReportPath + "Completion" + Dept + RptName) == true)
                    {
                        return "Completion" + Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }
                else if (Dept.ToUpper() == "DENTAL" && chkCompletionRpt.Checked == false)
                {
                    if (File.Exists(strReportPath + Dept + RptName) == true)
                    {
                        return Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }

                if (chkCompletionRpt.Checked == true)
                {
                    if (File.Exists(strReportPath + "Completion" + RptName) == true)
                    {
                        return "Completion" + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }

                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        string fnGetWebClaimFormName(String mCompId, String Dept)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = dbo.retun_inccmp_details(Criteria);

            string RptName = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {


                }
                else
                {
                    RptName = ("EMR_" + Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) + "AuthorizationRequest.aspx");
                }

                if (Dept.ToUpper() == "DENTAL")
                {
                    if (File.Exists(Server.MapPath("Report\\" + "EMR_" + Dept + RptName)))
                    {
                        return Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }
                return RptName;
            }
            else
            {
                return RptName;
            }



        }



        Boolean CheckMobileNo()
        {
            Boolean bolMobileNo = false;
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            //Criteria += " AND HPM_PT_ID Like '" + txtFileNo.Text + "'";
            if (ViewState["Patient_ID_VALID"] == "0")
            {
                Criteria += " AND HPM_MOBILE Like '" + txtMobile1.Text + "'";
            }
            else
            {
                Criteria += " AND HPM_PT_ID != '" + txtFileNo.Text + "' AND HPM_MOBILE Like '" + txtMobile1.Text + "'";
            }
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strMessage = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) + " : " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]) + " having this mobile No.";
                return true;

            }
            else
            {
                strMessage = "";
            }
            return bolMobileNo;
        }

        Boolean CheckPatientName()
        {
            Boolean bolMobileNo = false;
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND HPM_PT_ID Like '" + txtFileNo.Text + "'";
            if (ViewState["Patient_ID_VALID"] == "0")
            {
                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_FNAME Like '" + txtFName.Text.Trim() + "'";
                }

                if (txtMName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_MNAME Like '" + txtMName.Text.Trim() + "'";
                }

                if (txtLName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_LNAME Like '" + txtLName.Text.Trim() + "'";
                }

            }
            else
            {
                Criteria += " AND HPM_PT_ID != '" + txtFileNo.Text.Trim() + "'";

                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_FNAME Like '" + txtFName.Text.Trim() + "'";
                }

                if (txtMName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_MNAME Like '" + txtMName.Text.Trim() + "'";
                }

                if (txtLName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_LNAME Like '" + txtLName.Text.Trim() + "'";
                }

            }

            // Criteria += " AND hpm_pt_Fname + ' ' +isnull(hpm_pt_Mname,'') + ' '  + isnull(hpm_pt_Lname,'')   like '%" + txtFName.Text.Trim() + ' '  +txtMName.Text.Trim() + ' '   +txtLName.Text.Trim() + "%' ";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strMessage = "File No: " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) + " also having this Name '" + Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) + "'";
                return true;

            }
            else
            {
                strMessage = "";
            }
            return bolMobileNo;
        }

        Boolean CheckCompanyActive(string strCompanyID)
        {


            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND HCM_COMP_ID ='" + strCompanyID + "'";
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                return true;

            }
            return false;




        }

        void CheckFamilyAppoinment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            string strPTName = txtFileNo.Text.Trim();
            string[] arrPTName = strPTName.Split('/');

            //Criteria += " AND  HPM_PT_ID != '" + strPTName + "'";

            if (txtMobile1.Text.Trim() != "")
            {
                Criteria += " AND ( HPM_PT_ID Like '" + txtFileNo.Text.Trim() + "' OR HPM_MOBILE='" + txtMobile1.Text.Trim() + "')";

            }
            else
            {
                Criteria += " AND  HPM_PT_ID Like '" + txtFileNo.Text.Trim() + "'";
            }
            ds = dbo.retun_patient_details(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["TodayDBDateDesc"]) == Convert.ToString(ds.Tables[0].Rows[i]["HPM_LATESTVISITDesc"]))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This Patient having Appoinment Today');", true);
                        i = ds.Tables[0].Rows.Count;

                    }
                }

            }


        }

        void GetAppoinmentDtls()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += "   AND  CONVERT(DATE,HAM_FINISHTIME,101) = CONVERT(DATE, GETDATE(),101) ";

            if (txtMobile1.Text.Trim() != "")
            {
                Criteria += " AND ( HAM_FILENUMBER Like '" + txtFileNo.Text.Trim() + "' OR HAM_MOBILENO='" + txtMobile1.Text.Trim() + "')";

            }
            else
            {
                Criteria += " AND  HAM_FILENUMBER = '" + txtFileNo.Text.Trim() + "'";
            }
            ds = dbo.AppointmentOutlookGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["AppintmentSTTime"] = Convert.ToString(ds.Tables[0].Rows[0]["AppintmentSTTime"]);
            }


        }


        //STORE THE OLD PATIENT IMAGE FROM DATA BASE TO PHYSICAL FILE
        string byteArrayToImage(byte[] byteArrayIn)
        {
            System.Drawing.Image newImage;

            string strName = "DBF_" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strFileName = Server.MapPath("../Uploads/" + strName + ".jpeg");

            using (MemoryStream stream = new MemoryStream(byteArrayIn))
            {
                newImage = System.Drawing.Image.FromStream(stream);
                newImage.Save(@strFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

                //Bitmap bm = new Bitmap(stream);
                //bm.Save(@strFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            return strName + ".jpeg";
        }

        string byteArrayToImage1(byte[] byteArrayIn)
        {
            System.Drawing.Image newImage;

            string strName1 = "DBB_" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strFileName = Server.MapPath("../Uploads/" + strName1 + ".jpeg");

            using (MemoryStream stream = new MemoryStream(byteArrayIn))
            {
                newImage = System.Drawing.Image.FromStream(stream);
                newImage.Save(@strFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            return strName1 + ".jpeg";
        }

        void UploadDBImage()
        {

            DataSet DS;
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'  AND HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True'   AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = new DataSet();
            DS = dbo.ScanCardImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //for (int i = 0; i <= DS.Tables[0].Rows.Count; i++)
                //{
                if (DS.Tables[0].Rows[0].IsNull("HSC_IMAGE_PATH_FRONT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSC_IMAGE_PATH_FRONT"]) != "")
                {
                    goto FunEnd;
                }

                // }

            }


            byte[] InsCardFront, InsCardBack;


            dboperations objOperations = new dboperations();
            Criteria = " 1=1 ";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HPP_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = new DataSet();
            DS = objOperations.PatientPhotoGet(Criteria);
            string strInsCardFront = "", strInsCardBack = "";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                {
                    InsCardFront = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
                    strInsCardFront = byteArrayToImage(InsCardFront);
                }
                else
                {
                    strInsCardFront = "";

                }

                if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD1") == false)
                {
                    InsCardBack = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD1"];
                    strInsCardBack = byteArrayToImage1(InsCardBack);
                }
                else
                {
                    strInsCardBack = "";
                }

                //UPDATE THE CARD FRIEND FRIEND AND BACK FILE NAME IN DB
                objOperations.ScanCardImageModify(txtFileNo.Text.Trim(), Convert.ToString(Session["Branch_ID"]), strInsCardFront, strInsCardBack);

            }

        FunEnd: ;

        }

        //public byte[] imageToByteArray(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        //    return ms.ToArray();
        //}

        //public byte[] imageToByteArrayPNG(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        //    return ms.ToArray();
        //}

        //public byte[] imageToByteArrayJpeg(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //    return ms.ToArray();
        //}

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        void ScanCardImageGenerate()
        {

            DataSet DS;
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'  AND HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True' AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = new DataSet();
            DS = dbo.ScanCardImageGet(Criteria);
            if (DS.Tables[0].Rows.Count == 0)
            {
                dbo.ScanCardImageUpdateOldCard(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim());
                dbo.ScanCardImageGenerate(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim());
            }


        }


        void PatientPhoto()
        {


            imgFrontDefault.ImageUrl = "";
            imgBackDefault.ImageUrl = "";

            Session["SignatureImg1"] = "";
            Session["SignatureImg2"] = "";
            dbo = new dboperations();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND  CONVERT(varchar,HPP_DATE,103)='" + Convert.ToString(ViewState["EMR_Date"]) + "'";
            //if (Convert.ToString(ViewState["Photo_ID"]) != "" && Convert.ToString(ViewState["Photo_ID"]) != null)
            //{
            //    Criteria += " AND HPP_PHOTO_ID='" + Convert.ToString(ViewState["Photo_ID"]) + "'";
            //}

            ds = dbo.PatientPhotoGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["Photo_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPP_PHOTO_ID"]);

                if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                {

                    Session["SignatureImg1"] = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD"];


                    imgFrontDefault.ImageUrl = "../DisplayCardImage.aspx";
                }

                if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD1") == false)
                {
                    Session["SignatureImg2"] = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD1"];

                    imgBackDefault.ImageUrl = "../DisplayCardImageBack.aspx";
                }

            }
        }

        Boolean CheckPatientPhoto()
        {
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";
            dbo = new dboperations();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            ds = dbo.PatientPhotoGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {

                return true;

            }
            return false;
        }
        void BindPatientInfo()
        {
            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND HPI_ID = '" + Convert.ToString(ViewState["PatientInfoId"]) + "'";

            ds = dbo.PatientInfoGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_PT_FNAME"]);

                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_DOBDesc"]);


                if (txtDOB.Text != "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPI_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]) != "0")
                {
                    for (int intCount = 0; intCount < drpSex.Items.Count; intCount++)
                    {
                        if (drpSex.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]);
                            goto Sex;
                        }

                    }
                }

            Sex: ;
                txtOccupation.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_OCCUPATION"]);

                txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_ADDR"]);
                txtPoBox.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_POBOX"]);//need to add

                if (ds.Tables[0].Rows[0].IsNull("HPI_CITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]) != "0")
                {
                    for (int intCount = 0; intCount < drpCity.Items.Count; intCount++)
                    {
                        if (drpCity.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]))
                        {
                            drpCity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]);
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;
                if (ds.Tables[0].Rows[0].IsNull("HPI_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]) != "0")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]))
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]);
                            goto ForNatio;
                        }

                    }
                }
            ForNatio: ;
                txtPhone1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_PHONE1"]);
                txtMobile1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_MOBILE1"]);
                txtMobile2.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_MOBILE2"]);
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_EMAIL"]).Trim();
                txtKinName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_KIN_NAME"]);
                txtKinPhone.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_KIN_PHONE"]);

            }


        }


        Boolean CheckDoctorAvailable()
        {
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID = '" + txtDoctorID.Text.Trim() + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(ds.Tables[0].Rows[0]["FullName"]).Trim() == txtDoctorName.Text.Trim() || Convert.ToString(ds.Tables[0].Rows[0]["FullName1"]).Trim() == txtDoctorName.Text.Trim())
                {
                    return true;
                }
            }
            return false;
        }

        Boolean CheckPatientNameChanged()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "' AND HPM_PT_FNAME <>'" + txtFName.Text.Trim().Replace("'", "") + "'  AND DATEDIFF(DD, CONVERT(DATETIME, HPM_CREATED_DATE,101),CONVERT(DATETIME,GETDATE(),101)) < 1 ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        Boolean CheckPatientIdAvailable()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        Boolean CheckTempImage(Int64 GCId)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTI_IMAGE_TYPE='GeneralConsent' AND HTI_PT_ID = '" + GCId + "'";
            DS = dbo.TempImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;

        }



        void AddSignatureTempImage(Int64 GCId)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            clsGeneralConsent objGC = new clsGeneralConsent();
            string Criteria = " 1=1 ";

            Criteria += " AND HGC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HGC_PT_ID='" + hidActualFileNo.Value + "'";

            if (GCId != 0)
            {
                Criteria += " AND HGC_ID=" + GCId;
            }

            DS = objGC.GeneralConsentGet(Criteria);

            Byte[] bytImage1 = null;
            Byte[] bytImage2 = null;
            Byte[] bytImage3 = null;
            Byte[] bytImage4 = null;

            string strCreatedUser = "";
            ViewState["HGC_ID"] = "";
            if (DS.Tables[0].Rows.Count > 0)
            {


                //BELOW CODING WILL INSERT THE SCAN CARD IMAGE IN TEMP FILE TABLE FOR SHOWING THE CARD IMAGE IN REPORT

                string strPath = Server.MapPath("../Uploads/Signature/");

                if (DS.Tables[0].Rows[0].IsNull("HGC_PT_SIGNATURE") == false)
                {

                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(DS.Tables[0].Rows[0]["HGC_PT_SIGNATURE"]));
                    bytImage1 = ImageToByte(image);

                    //Session["SignatureImg1"] = bytImage1;
                    //imgSig1.ImageUrl = "~/DisplaySignature1.aspx";
                }

                if (DS.Tables[0].Rows[0].IsNull("HGC_SUBS_SIGNATURE") == false)
                {

                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(DS.Tables[0].Rows[0]["HGC_SUBS_SIGNATURE"]));
                    bytImage2 = ImageToByte(image);
                }

                if (DS.Tables[0].Rows[0].IsNull("HGC_TRAN_SIGNATURE") == false)
                {

                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(DS.Tables[0].Rows[0]["HGC_TRAN_SIGNATURE"]));
                    bytImage3 = ImageToByte(image);
                }

                if (DS.Tables[0].Rows[0].IsNull("HGC_CREATED_USER") == false)
                {
                    strCreatedUser = Convert.ToString(DS.Tables[0].Rows[0]["HGC_CREATED_USER"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("HGC_ID") == false)
                {
                    ViewState["HGC_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HGC_ID"]);
                }



                DataSet DS1 = new DataSet();
                Criteria = " 1=1 ";
                Criteria += " AND HSP_STAFF_ID = '" + Convert.ToString(strCreatedUser) + "'";

                DS1 = dbo.StaffPhotoGet(Criteria);
                if (DS1.Tables[0].Rows.Count > 0)
                {
                    if (DS1.Tables[0].Rows[0].IsNull("HSP_SIGNATURE") == false)
                    {
                        bytImage4 = (byte[])DS1.Tables[0].Rows[0]["HSP_SIGNATURE"];

                    }

                }


                //hidActualFileNo.Value.Trim()
                if (CheckTempImage(GCId) == false)
                {
                    dbo.TempImageAdd1(Convert.ToString(GCId), Convert.ToString(Session["Branch_ID"]), bytImage1, bytImage2, bytImage3, bytImage4, "GeneralConsent");
                }



            }

           // BindTempImage();


        FunEnd: ;
        }




        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='PATIENTREG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                chkUpdateWaitList.Enabled = false;
                btnNew.Enabled = false;
                bntFamilyApt.Enabled = false;
                btnScan.Enabled = false;
                lnkEmIdShow.Enabled = false;
                btnSave.Enabled = false;
                btnClear.Enabled = false;
                btnBalance.Visible = false;

                btnScanCardAdd.Enabled = false;
                btnScanCardUpdate.Enabled = false;

                AsyncFileUpload1.Enabled = false;
                AsyncFileUpload2.Enabled = false;




            }


            if (strPermission == "7")
            {
                chkUpdateWaitList.Enabled = false;
                btnNew.Enabled = false;
                bntFamilyApt.Enabled = false;
                btnScan.Enabled = false;
                lnkEmIdShow.Enabled = false;
                btnSave.Enabled = false;
                btnClear.Enabled = false;
                btnBalance.Visible = false;

                btnScanCardAdd.Enabled = false;
                btnScanCardUpdate.Enabled = false;

                AsyncFileUpload1.Enabled = false;
                AsyncFileUpload2.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }
        # endregion

        #region AutoCompleteExtender

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            strCompName = "";
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            if (stParentName != "0" && stParentName != "")
            {
                Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE like '%" + stParentName + "%' and HCM_COMP_ID Like '%" + prefixText + "%'";
            }
            else if (strTPA == "false")
            {
                Criteria += " AND  HCM_COMP_ID =  HCM_BILL_CODE AND HCM_COMP_ID Like '%" + prefixText + "%'";
            }
            else
            {
                Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";
            }

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_NAME"]);


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            if (stParentName != "0" && stParentName != "")
            {
                // Criteria += " AND HCM_NAME Like '%" + prefixText + "%' AND HCM_COMP_ID in ( select HCM_COMP_ID from HMS_COMPANY_MASTER where HCM_COMP_ID !=  HCM_BILL_CODE AND  HCM_BILL_CODE like '" + stParentName + "%' ) ";
                Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE like '%" + stParentName + "%' and HCM_NAME Like '%" + prefixText + "%'";
            }
            else if (strTPA == "false")
            {
                Criteria += " AND  HCM_COMP_ID =  HCM_BILL_CODE  AND HCM_NAME Like '%" + prefixText + "%'";
            }
            else
            {
                Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";
            }


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_NAME"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            strDrName = "";
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }



        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorId(string prefixText)
        {


            strDrName = "";
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HRDM_REF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.RefDoctorMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HRDM_REF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HRDM_FNAME + ' ' +isnull(HRDM_MNAME,'') + ' '  + isnull(HRDM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.RefDoctorMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HRDM_REF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetPatientCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HPC_PATIENT_COMPANY Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.PatientCompanyGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HPC_PATIENT_COMPANY"]);
                }

                return Data;
            }
            string[] Data1 = { "" };
            return Data1;

        }


        [System.Web.Services.WebMethod]
        public static string[] GetGeneralClass(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            if (stParentName1 != "0" && stParentName1 != "")
            {
                Criteria += " AND HCM_COMP_ID = '" + stParentName1 + "'";
            }

            Criteria += " AND HCM_PACKAGE_DETAILS Like '%" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPackage = Convert.ToString(ds.Tables[0].Rows[0]["HCM_PACKAGE_DETAILS"]);
                string[] arrPackage = strPackage.Split('|');

                Data = new string[arrPackage.Length];
                for (int i = 0; i < arrPackage.Length; i++)
                {
                    Data[i] = arrPackage[i];
                }

                return Data;
            }
            string[] Data1 = { "" };
            return Data1;

        }

        [System.Web.Services.WebMethod]
        public static string[] GetPlanType(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1  AND  HCB_COMP_ID ='" + strCompID + "'";
            Criteria += " AND HCB_CAT_TYPE Like '%" + prefixText + "%'";
            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.CompBenefitsGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HCB_CAT_TYPE"]);


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }
            lblStatus.Text = "";
            lblStatusTab2.Text = "";
            if (hidLocalDate.Value != "")
            {
                Session["LocalDate"] = hidLocalDate.Value;
            }



            if (!IsPostBack)
            {
                // string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
                // ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    // SetPermission();
                }

                try
                {



                    ViewState["Secondary_Ins"] = "";
                    Session["InsCardFrontPath"] = "";
                    Session["InsCardBackPath"] = "";
                    ViewState["Patient_ID_VALID"] = "0";
                    ViewState["PatientInfoId"] = "";

                    ViewState["Signature1"] = "";
                    ViewState["Signature2"] = "";
                    ViewState["Signature3"] = "";

                    Session["SignatureImg1"] = "";
                    Session["SignatureImg2"] = "";
                    Session["SignatureImg3"] = "";

                    ViewState["EmirateIdRef"] = "";
                    Session["Photo"] = "";

                    BindScreenCustomization();

                    hidHospitalName.Value = GlobalValues.HospitalName;

                    Form.DefaultButton = btnNew.UniqueID;
                    stParentName = "0";

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    txtLablePrintCount.Text = Convert.ToString(Session["LabelCopies"]);

                    if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
                    {
                        drpPatientType.SelectedIndex = 1;
                    }
                    else
                    {
                        drpPatientType.SelectedIndex = 0;
                    }


                    if (Convert.ToString(Session["IDPrint"]).ToUpper() == "Y")
                    {
                        btnIDPrint.Visible = true;
                    }
                    else
                    {
                        btnIDPrint.Visible = false;
                    }



                    if (Convert.ToString(Session["ShowToken"]).ToUpper() == "Y")
                    {
                        divToken.Visible = true;
                    }
                    else
                    {
                        divToken.Visible = false;
                    }


                    if (strTPA.ToLower() == "true")
                    {

                        trTPA.Visible = true;
                    }
                    else
                    {

                        trTPA.Visible = false;
                    }


                    if (strNetwork.ToLower() == "true")
                    {
                        tdCaptionNetwork.Visible = true;
                        tdNetwork.Visible = true;
                    }
                    else
                    {
                        tdCaptionNetwork.Visible = false;
                        tdNetwork.Visible = false;
                    }



                    if (PTRegChangePolicyNoCaption.ToLower() == "true")
                    {
                        if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                        {
                            lblPolicyNo.Text = "Member ID";
                            lblPolicyNo1.Text = "Member ID";
                            lblCardNo.Text = "Policy No.";
                            lblCardNo1.Text = "Policy No.";
                        }

                        if (GlobalValues.FileDescription.ToUpper() == "ALAMAL")
                        {
                            lblPolicyNo.Text = "Card No.";
                            lblPolicyNo1.Text = "Card No.";
                            tdCardNo.Visible = false;
                        }

                    }



                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {

                        bntFamilyApt.Visible = false;
                        btnScan.Visible = false;
                        lnkEmIdShow.Visible = false;
                        chkStatus.Visible = false;
                        chkCompCashPT.Visible = false;
                        chkFamilyMember.Visible = false;
                        //lblOfficePhNo.Visible = false;
                        txtPhone3.Enabled = false;

                        //  lblOccupation.Visible = false;
                        txtOccupation.Enabled = false;

                        //  lblFax.Visible = false;
                        txtFax.Enabled = false;
                        //  lblRefDr.Visible = false;
                        txtRefDoctorID.Enabled = false;
                        txtRefDoctorName.Enabled = false;


                        //  lblID.Visible = false;
                        txtPolicyId.Enabled = false;

                        btnMRRPrint.Visible = false;

                        txtLablePrintCount.Visible = false;
                        chkLabelPrint.Visible = false;
                        btnLabelPrint.Visible = false;

                        // lblPOBox.Visible = false;
                        txtPoBox.Enabled = false;
                        ///  lblBlood.Visible = false;
                        drpBlood.Enabled = false;


                        txtPhone3.Enabled = false;
                        txtEmail.Enabled = false;
                        txtFax.Enabled = false;

                        txtOccupation.Enabled = false;
                        drpKnowFrom.Enabled = false;

                        txtRefDoctorID.Enabled = false;
                        txtRefDoctorName.Enabled = false;



                        // trOfficePh.Visible = false;
                        // trOccupation.Visible = false;
                        // trRefDr.Visible = false;
                    }


                    if (Convert.ToString(Session["PackageManditory"]).ToUpper() == "Y")
                    {

                        lblNetworkClass.Visible = true;
                        txtNetworkClass.Visible = true;

                    }
                    else
                    {

                        lblNetworkClass.Visible = false;
                        txtNetworkClass.Visible = false;

                    }


                    if (Convert.ToString(Session["ConInsPTReg"]).ToUpper() == "Y")
                    {
                        divDeductCoIns.Visible = true;
                    }
                    else
                    {
                        divDeductCoIns.Visible = false;
                    }



                    if (Convert.ToString(Session["PriorityDisplay"]).ToUpper() == "Y")
                    {
                        divPriority.Visible = true;
                    }
                    else
                    {
                        divPriority.Visible = false;
                    }

                    if (Convert.ToString(Session["KnowFromDisplay"]).ToUpper() == "Y")
                    {
                        drpKnowFrom.Visible = true;
                        lblKnowFrom.Visible = true;

                    }
                    else
                    {
                        drpKnowFrom.Visible = false;
                        lblKnowFrom.Visible = false;
                    }

                    if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                    {
                        divDept.Visible = true;
                    }
                    else
                    {
                        divDept.Visible = false;
                    }


                    if (Convert.ToString(ViewState["APPOINTMENT_ALERT"]) == "1")
                    {
                        btnAppointment.Visible = true;
                    }
                    else
                    {
                        btnAppointment.Visible = false;
                    }
                    //if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
                    //{
                    //    chkUpdateWaitList.Visible = false;
                    //    // chkUpdateWaitList.Checked = false;
                    //}


                    if (Convert.ToString(Session["PTNameMultiple"]) != "" && Convert.ToString(Session["PTNameMultiple"]) == "N")
                    {
                        txtMName.Visible = false;
                        txtLName.Visible = false;
                        txtFName.Width = 250;

                    }

                    //TabContainer1.ActiveTab = TabPanelGeneral;
                    txtFileNo.Focus();
                    hidTodayDate.Value = System.DateTime.Now.ToString("dd/MM/yyyy");
                    hidTodayTime.Value = System.DateTime.Now.ToShortTimeString();
                    hidMultipleVisitADay.Value = Convert.ToString(Session["MultipleVisitADay"]);

                    txtExpiryDate.Text = "31/12/" + DateTime.Now.Year.ToString();
                    txtCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();
                    //  if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    // {
                    txtPolicyStart.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");


                    // }
                    //else
                    //{
                    //    txtEffectiveDate.Text = hidLocalDate.Value;
                    //}


                    Session["InsCardFrontPath"] = "";
                    Session["InsCardBackPath"] = "";

                    Session["status"] = "A";
                    ViewState["Patient_ID_VALID"] = "0";

                    Session["IDPath"] = null;
                    Session["SignaturePath"] = null;
                    Session["CompcashPt"] = "Y";
                    ViewState["PatientInfoId"] = "";

                    Year.Value = DateTime.Now.Year.ToString();
                    BindDepartment();



                    BindCity();
                    BindCountry();
                    BindArea();
                    BindNationality();

                    CompanyParentGet();
                    BindDefaultCompany();

                    BinScanCardMasterGet();
                    BindIDCaptionMaster();

                    BindPriority();
                    BindKnowFrom();
                    BindDeductibleType();
                    BindCoInsuranceType();
                    BindPatientTitle();
                    BindPayer();
                    BindConsentForm();
                    if (Convert.ToString(Session["PackageManditory"]).ToUpper() == "Y")
                    {

                        //txtNetworkClass.BorderColor = System.Drawing.Color.Red;
                        //txtNetworkClass.BorderWidth = 1;
                    }

                    if (Convert.ToString(Session["PriorityDisplay"]).ToUpper() == "Y")
                    {

                        // drpPriority.BorderColor = System.Drawing.Color.Red;
                        // drpPriority.BorderWidth = 1;
                    }

                    //if (Convert.ToString(ViewState["EmiratesIdMandatory"]) == "1")
                    //{
                    //    txtEmiratesID.BorderColor = System.Drawing.Color.Red;
                    //    txtEmiratesID.BorderWidth = 1;
                    //}

                    ////if (Convert.ToString(ViewState["NationalityMandatory"]) == "1")
                    ////{
                    ////    drpNationality.BorderColor = System.Drawing.Color.Red;
                    ////    drpNationality.BorderWidth = 1;
                    ////}

                    ////if (Convert.ToString(ViewState["CityMandatory"]) == "1")
                    ////{
                    ////    drpCity.BorderColor = System.Drawing.Color.Red;
                    ////    drpCity.BorderWidth = 1;
                    ////}

                    //if (Convert.ToString(ViewState["VisaTypeMandatory"]) == "1")
                    //{
                    //    drpVisaType.BorderColor = System.Drawing.Color.Red;
                    //    drpVisaType.BorderWidth = 1;
                    //}



                    if (Convert.ToString(ViewState["AgeMandatory"]) == "1")
                    {
                        txtAge.BorderColor = System.Drawing.Color.Red;
                        txtAge.BorderWidth = 1;
                    }

                    if (Convert.ToString(ViewState["DeductibleMandatory"]) == "1")
                    {
                        //txtDeductible.BorderColor = System.Drawing.Color.Red;
                        txtDeductible.BorderWidth = 1;
                    }

                    if (Convert.ToString(ViewState["OTHER_INFO_DISPLAY"]) == "1")
                    {
                        divOtherInfo.Visible = true;
                    }

                    if (Convert.ToString(ViewState["PT_COMP_ADD"]) == "1")
                    {
                        lblPTCompany.Text = "PT Company";
                        txtPTCompany.Visible = true;
                        drpPayer.Visible = false;
                    }

                    if (Convert.ToString(ViewState["PT_TITLE_NAME"]) == "1")
                    {
                        drpTitle.Visible = true;
                    }

                    if (Convert.ToString(ViewState["PLAN_TYPE_STORE_SCAN_CARD"]) == "1")
                    {
                        //drpPlanType.Enabled = false;
                        lblPlanType.Visible = true;
                        txtPlanType.Visible = true;
                    }

                    string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);


                    switch (AutoSlNo)
                    {
                        case "A":
                            chkManual.Checked = false;
                            chkManual.Visible = false;
                            btnNew.Visible = true;
                            break;
                        case "M":
                            chkManual.Checked = true;
                            chkManual.Visible = false;
                            txtFileNo.ReadOnly = false;
                            btnNew.Visible = false;
                            break;
                        case "B":
                            //chkManual.Checked = true;
                            chkManual.Visible = true;
                            btnNew.Visible = true;
                            break;
                        default:
                            break;

                    }

                    if (Convert.ToString(Session["AutoSlNo"]) == "A")
                    {

                        chkManual.Enabled = false;
                    }




                    BuildScanCard();

                    Session["Incoming"] = "";
                    // }
                    string PatientId = "", PatientInfoId = "", HPVSeqNo = "";

                    ViewState["HPVSeqNo"] = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    PatientInfoId = Convert.ToString(Request.QueryString["PatientInfoId"]);
                    HPVSeqNo = Convert.ToString(Request.QueryString["HPVSeqNo"]);


                    if (PatientId == "" || PatientId == null)
                    {
                        if (Convert.ToString(Session["PatientID"]) != "")
                        {

                            PatientId = Convert.ToString(Session["PatientID"]);
                        }
                    }


                    hidPateName.Value = Convert.ToString(Request.QueryString["PageName"]);

                    if (hidPateName.Value == "HomeCare")
                    {
                        chkUpdateWaitList.Checked = false;
                        //txtDoctorID.BorderColor = System.Drawing.ColorTranslator.FromHtml("#cccccc");
                        lblHomeCarePT.Visible = true;
                    }
                    else
                    {
                        chkUpdateWaitList.Checked = true;
                        // txtDoctorID.BorderColor = System.Drawing.Color.Red;
                        lblHomeCarePT.Visible = false;
                    }

                    ViewState["PatientId"] = PatientId;
                    ViewState["PatientInfoId"] = PatientInfoId;
                    ViewState["HPVSeqNo"] = HPVSeqNo;

                    if (PatientId != " " && PatientId != null)
                    {

                        txtFileNo.Text = Convert.ToString(PatientId);
                        BindModifyData();

                    }

                    if (PatientInfoId != " " && PatientInfoId != null)
                    {
                        BindPatientInfo();
                    }



                }
                catch (Exception ex)
                {

                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                hidActualFileNo.Value = "";
                ViewState["Patient_ID_VALID"] = "0";
                // FOR ADD THE FILE NO PREFIX YEAR AND MONTH LIKSE "1306"
                string strMonth = "", strYear = "", strPrefix = "";
                if (Convert.ToString(Session["FileNoMonthYear"]).ToUpper() == "Y")
                {
                    strMonth = System.DateTime.Now.Month.ToString();
                    if (strMonth.Length == 1)
                    {
                        strMonth = "0" + strMonth;
                    }
                    strYear = System.DateTime.Now.Year.ToString();
                    strYear = strYear.Substring(2, 2);
                    strPrefix = strYear + strMonth;
                }

                if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                {
                    txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), drpDepartment.SelectedValue);
                }
                else
                {
                    txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PATIENTREG");
                }
                if (Convert.ToString(ViewState["PatientInfoId"]) == "")
                {
                    Clear();
                }
                txtFileNo.Focus();
                chkManual.Checked = false;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void chkManual_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkManual.Checked == true)
                {
                    if (chkFamilyMember.Checked == false)
                    {
                        txtFileNo.Text = "";
                    }
                    txtFileNo.ReadOnly = false;

                }
                else
                {
                    txtFileNo.ReadOnly = true;
                    // FOR ADD THE FILE NO PREFIX YEAR AND MONTH LIKSE "1306"
                    string strMonth = "", strYear = "", strPrefix = "";
                    if (Convert.ToString(Session["FileNoMonthYear"]).ToUpper() == "Y")
                    {
                        strMonth = System.DateTime.Now.Month.ToString();
                        if (strMonth.Length == 1)
                        {
                            strMonth = "0" + strMonth;
                        }
                        strYear = System.DateTime.Now.Year.ToString();
                        strYear = strYear.Substring(2, 2);
                        strPrefix = strYear + strMonth;
                    }

                    if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                    {
                        txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), drpDepartment.SelectedValue);
                    }
                    else
                    {
                        txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PATIENTREG");
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.chkManual_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        bool ReturnValue()
        {
            return false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(5000);
                // TabContainer1.ActiveTab = TabPanelGeneral;

                if (drpPatientType.SelectedValue == "CR")
                {
                    if (drpPlanType.Items.Count != 0)
                    {
                        if (drpDefaultCompany.SelectedIndex == 0 || txtPolicyNo.Text == "" || txtExpiryDate.Text == "")
                        {
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Provide Insurance Details');", true);
                            goto SaveEnd;
                        }
                    }
                    if (Convert.ToString(Session["PackageManditory"]).ToUpper() == "Y")
                    {
                        if (txtNetworkClass.Text == "")
                        {
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Provide Insurance Details');", true);
                            goto SaveEnd;
                        }

                    }

                    if (CheckCompanyActive(drpDefaultCompany.SelectedValue) == false)
                    {

                        ModalPopupExtender1.Show();
                        strMessage = "This company is not active.";
                    }
                }
                if (hidPateName.Value != "HomeCare")// IF PT REGISTRATION COME FROM HOME CARE DOCTOR ID IS NOT MANDATORY
                {
                    if (CheckDoctorAvailable() == false)
                    {

                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Provide Existing Doctors Details');", true);
                        goto SaveEnd;
                    }
                }

                if (Convert.ToString(ViewState["PT_SIGNATURE"]) == "1")
                {
                    if (Convert.ToString(Session["SignatureImg1"]) == "" && Convert.ToString(Session["SignatureImg2"]) == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Provide Patient/Substitute Signature');", true);
                        goto SaveEnd;

                    }
                }


                if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                {
                    if (Convert.ToString(ViewState["ConfirmResult"]) != "Yes")
                    {
                        if (CheckPatientNameChanged() == true)
                        {

                            divConfirmMessageClose.Style.Add("display", "block");
                            divConfirmMessage.Style.Add("display", "block");
                            lblConfirmMessage.Text = "Patient Name is differed from Old name, Do you want to Continue ?";

                            ViewState["ConfirmMsgFor"] = "DuplicateNameSave";

                            ViewState["ConfirmResult"] = "";

                            goto SaveEnd;

                        }

                    }

                    ViewState["ConfirmResult"] = "";
                }


                ClientScriptManager CSM = Page.ClientScript;


                if (Page.IsValid)
                {
                    TextFileWriting("Save Started ");


                    //*********************  PATIENT MASTER INSERT START  **************************
                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter adpt = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_PTMasterAdd";

                    cmd.Parameters.Add(new SqlParameter("@HPM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_ID", SqlDbType.VarChar)).Value = txtFileNo.Text.ToString().ToUpper();
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_TITLE", SqlDbType.VarChar)).Value = drpTitle.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_FNAME", SqlDbType.NVarChar)).Value = txtFName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_MNAME", SqlDbType.NVarChar)).Value = txtMName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_LNAME", SqlDbType.NVarChar)).Value = txtLName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_FNAME_An", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_MNAME_An", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_LNAME_An", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_BLOOD_GROUP", SqlDbType.VarChar)).Value = drpBlood.SelectedValue.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_TYPE", SqlDbType.VarChar)).Value = drpPatientType.SelectedValue.ToString();


                    if (Convert.ToString(ViewState["PT_COMP_ADD"]) == "1")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_COMP_NAME", SqlDbType.VarChar)).Value = txtPTCompany.Text.ToString();
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_COMP_NAME", SqlDbType.VarChar)).Value = drpPayer.SelectedValue.ToString();
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPM_COMP_NAME", SqlDbType.VarChar)).Value = "";


                    cmd.Parameters.Add(new SqlParameter("@HPM_COMP_CASHPT", SqlDbType.VarChar)).Value = "";//checkbox
                    cmd.Parameters.Add(new SqlParameter("@HPM_INS_TYPE_TRMT", SqlDbType.VarChar)).Value = "";//from insurance

                    if (drpPatientType.SelectedValue == "CR")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_TYPE", SqlDbType.VarChar)).Value = "Insurance";

                        if (drpNetworkName.SelectedIndex == 0)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedItem.Text;
                            cmd.Parameters.Add(new SqlParameter("@HPM_BILL_CODE", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpNetworkName.SelectedValue;
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpNetworkName.SelectedItem.Text;
                            cmd.Parameters.Add(new SqlParameter("@HPM_BILL_CODE", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                        }


                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.ToString();
                        cmd.Parameters.Add(new SqlParameter("@HPM_FAMILY_ID", SqlDbType.VarChar)).Value = txtPolicyId.Text.ToString();
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_TYPE", SqlDbType.VarChar)).Value = drpPlanType.SelectedValue;


                        string strExpDate = txtExpiryDate.Text;

                        if (txtExpiryDate.Text != "")
                        {
                            string[] arrDate = strExpDate.Split('/');
                            string strForExpDate = "";
                            if (arrDate.Length > 1)
                            {
                                strForExpDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                            }
                            //DateTime TSDate;
                            //  TSDate = Convert.ToDateTime(txtExpiryDate.Text);
                            cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = strForExpDate;// TSDate.ToString("MM/dd/yyyy");


                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = "";
                        }



                        string strPolicyStart = txtPolicyStart.Text;

                        if (txtPolicyStart.Text != "")
                        {
                            string[] arrPlicyStartDate = strPolicyStart.Split('/');
                            string strForPolicyStart = "";
                            if (arrPlicyStartDate.Length > 1)
                            {
                                strForPolicyStart = arrPlicyStartDate[1] + "/" + arrPlicyStartDate[0] + "/" + arrPlicyStartDate[2];
                            }
                            //DateTime TSDate;
                            //  TSDate = Convert.ToDateTime(txtExpiryDate.Text);
                            cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_START", SqlDbType.VarChar)).Value = strForPolicyStart;// TSDate.ToString("MM/dd/yyyy");


                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_START", SqlDbType.VarChar)).Value = "";
                        }


                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_BILL_CODE", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_TYPE", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_NO", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_FAMILY_ID", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_TYPE", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = "";
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_DISCTYPE", SqlDbType.VarChar)).Value = "";//from insurance ;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DISCAMOUNT", SqlDbType.Decimal)).Value = DBNull.Value;// from insurance;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DEDUCTIBLE", SqlDbType.Decimal)).Value = DBNull.Value;// from insurance;
                    cmd.Parameters.Add(new SqlParameter("@HPM_ADDR", SqlDbType.VarChar)).Value = txtAddress.Text.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HPM_POBOX", SqlDbType.VarChar)).Value = txtPoBox.Text.ToString();
                    if (drpCity.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_CITY", SqlDbType.VarChar)).Value = drpCity.SelectedValue.ToString();
                    }
                    if (drpArea.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_AREA", SqlDbType.VarChar)).Value = drpArea.SelectedValue.ToString();
                    }
                    if (drpCountry.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_COUNTRY", SqlDbType.VarChar)).Value = drpCountry.SelectedValue.ToString();
                    }
                    if (drpNationality.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_NATIONALITY", SqlDbType.VarChar)).Value = drpNationality.SelectedValue.ToString();
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_PHONE1", SqlDbType.VarChar)).Value = txtPhone1.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_PHONE2", SqlDbType.VarChar)).Value = txtMobile2.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_FAX", SqlDbType.VarChar)).Value = txtFax.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_MOBILE", SqlDbType.VarChar)).Value = txtMobile1.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_TYPE", SqlDbType.VarChar)).Value = drpPlanType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_NO", SqlDbType.VarChar)).Value = txtIDNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_ISSUEDBY", SqlDbType.VarChar)).Value = "";





                    string strDOBDate = txtDOB.Text;

                    if (txtDOB.Text != "")
                    {
                        //DateTime DOBDate;
                        // DOBDate = Convert.ToDateTime(txtDOB.Text);

                        string[] arrDOBDate = strDOBDate.Split('/');

                        string strForDOBDate = "";
                        if (arrDOBDate.Length > 1)
                        {
                            strForDOBDate = arrDOBDate[1] + "/" + arrDOBDate[0] + "/" + arrDOBDate[2];
                        }

                        cmd.Parameters.Add(new SqlParameter("@HPM_DOB", SqlDbType.VarChar)).Value = strForDOBDate;// DOBDate.ToString("MM/dd/yyyy");

                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE", SqlDbType.Char)).Value = "Y";
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE", SqlDbType.Int)).Value = txtAge.Text;



                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_DOB", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE", SqlDbType.Char)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE", SqlDbType.Int)).Value = "";
                    }


                    if (txtMonth.Text != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE1", SqlDbType.Int)).Value = txtMonth.Text;
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE1", SqlDbType.VarChar)).Value = "M";

                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE1", SqlDbType.Int)).Value = 0;
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE1", SqlDbType.VarChar)).Value = "";
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_MARITAL", SqlDbType.VarChar)).Value = drpMStatus.SelectedValue;

                    if (drpSex.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_SEX", SqlDbType.VarChar)).Value = drpSex.SelectedValue;
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPM_DR_ID", SqlDbType.VarChar)).Value = txtDoctorID.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_DR_NAME", SqlDbType.VarChar)).Value = txtDoctorName.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_DEP_NAME", SqlDbType.VarChar)).Value = txtDepName.Text.Trim();



                    cmd.Parameters.Add(new SqlParameter("@HPM_TOKEN_NO", SqlDbType.VarChar)).Value = txtTokenNo.Text;

                    if (txtRefDoctorID.Text.Trim() != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_ID", SqlDbType.VarChar)).Value = txtRefDoctorID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_NAME", SqlDbType.VarChar)).Value = txtRefDoctorName.Text.Trim();
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_NAME", SqlDbType.VarChar)).Value = txtKinName.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_RELATION", SqlDbType.VarChar)).Value = txtKinRelation.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_PHONE", SqlDbType.VarChar)).Value = txtKinPhone.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_MOBILE", SqlDbType.VarChar)).Value = txtKinMobile.Text;

                    if (chkStatus.Checked == true)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_STATUS", SqlDbType.Char)).Value = 'A';
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_STATUS", SqlDbType.Char)).Value = 'I';
                    }

                    // cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.Char)).Value = drpPatientStatus.SelectedValue;
                    //IN THE SYSTEM OPTIONS IF FILEING REQUEST IS SELECTED VISIT STATUS IS 'F' ELSE 'W'
                    if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.VarChar)).Value = "F";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.VarChar)).Value = "W";
                    }



                    cmd.Parameters.Add(new SqlParameter("@HPM_COMMIT_FLAG", SqlDbType.Char)).Value = 'N';

                    cmd.Parameters.Add(new SqlParameter("@HPM_MODIFIED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);

                    cmd.Parameters.Add(new SqlParameter("@HPM_IQAMA_NO", SqlDbType.VarChar)).Value = txtEmiratesID.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_NO_CHILDREN", SqlDbType.TinyInt)).Value = txtNoOfChildren.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_OCCUPATION", SqlDbType.VarChar)).Value = txtOccupation.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DED_TYPE", SqlDbType.Char)).Value = drpDedType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DED_AMT", SqlDbType.Decimal)).Value = txtDeductible.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_COINS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_COINS_AMT", SqlDbType.Decimal)).Value = txtCoIns.Text.Trim();
                    if (drpKnowFrom.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_KNOW_FROM", SqlDbType.VarChar)).Value = drpKnowFrom.SelectedValue;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_KNOW_FROM", SqlDbType.VarChar)).Value = "";
                    }
                    cmd.Parameters.Add(new SqlParameter("@HPM_PACKAGE_NAME", SqlDbType.VarChar)).Value = txtNetworkClass.Text;



                    cmd.Parameters.Add(new SqlParameter("@HPM_PATIENT_STATUS", SqlDbType.VarChar)).Value = drpPTStatus.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_STATUS_MSG", SqlDbType.VarChar)).Value = txtHoldMsg.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_IQAMA_PATH", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_PATH", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@BUCODE", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_EMPLOYEE_STATUS", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@PT_DEP_ID", SqlDbType.VarChar)).Value = "";

                    if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "0")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                        cmd.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char)).Value = "A";

                        if (Convert.ToString(ViewState["PatientInfoId"]) != "" && Convert.ToString(ViewState["PatientInfoId"]) != null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@PatientInfoId", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["PatientInfoId"]);
                        }



                    }
                    else if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                    {

                        cmd.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char)).Value = "M";
                    }
                    cmd.Parameters.Add(new SqlParameter("@IsManual", SqlDbType.Bit)).Value = chkManual.Checked;

                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_CAPTION", SqlDbType.VarChar)).Value = drpIDCaption.SelectedValue;


                    if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    {
                        cmd.Parameters.Add(new SqlParameter("@PTRegDateFrom", SqlDbType.Char)).Value = "S";
                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime)).Value = "";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@PTRegDateFrom", SqlDbType.Char)).Value = "L";
                        cmd.Parameters.Add(new SqlParameter("@CreatedDate", SqlDbType.DateTime)).Value = hidLocalDate.Value; //"2013-07-05 15:48:43";
                    }


                    if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = drpDepartment.SelectedValue;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = "PATIENTREG";
                    }


                    if (Convert.ToString(ViewState["HPVSeqNo"]) == null || Convert.ToString(ViewState["HPVSeqNo"]) == "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPVSeqNo", SqlDbType.VarChar)).Value = "0";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPVSeqNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["HPVSeqNo"]);
                    }


                    if (drpKnowFrom.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_PRIORITY_SLNO", SqlDbType.TinyInt)).Value = drpPriority.SelectedValue;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_PRIORITY_SLNO", SqlDbType.TinyInt)).Value = "";
                    }
                    cmd.Parameters.Add(new SqlParameter("@HPV_ENTRY_FROM", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_AGE", SqlDbType.VarChar)).Value = txtAge.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPV_CLAIM_NO", SqlDbType.VarChar)).Value = "";//from invoice
                    cmd.Parameters.Add(new SqlParameter("@HPV_ICD_CODE", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_ICD_DESC", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_DIAGNOSIS", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_TYPE_ILLNESS", SqlDbType.VarChar)).Value = "";
                    if (txtRefDoctorID.Text.Trim() != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL", SqlDbType.Char)).Value = "Y";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL", SqlDbType.Char)).Value = "N";
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL_TO", SqlDbType.VarChar)).Value = txtRefDoctorID.Text.Trim();// drpRefDoctor.SelectedValue.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HPV_ICD_CODE1", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_ICD_DESC1", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_INV_NO", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_APPOINT_TIME", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["AppintmentSTTime"]);
                    cmd.Parameters.Add(new SqlParameter("@HPV_REG_UPDTED_TIME", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_FILE_SEND_TIME", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_DR_SEL_TIME", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_INVOICE_TIME", SqlDbType.VarChar)).Value = ""; //from invoice
                    cmd.Parameters.Add(new SqlParameter("@HPV_FILING_USER", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_WAIT_LIST_USER", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_INVOICE_ID", SqlDbType.VarChar)).Value = "";//from invoice
                    cmd.Parameters.Add(new SqlParameter("@HPV_TYPE", SqlDbType.Char)).Value = "OP";
                    cmd.Parameters.Add(new SqlParameter("@HPV_BUSINESSUNITCODE", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_JOBNUMBER", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_MOBILE", SqlDbType.VarChar)).Value = txtMobile1.Text.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HIM_READYFORECLAIM", SqlDbType.Int)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_VISITTYPE", SqlDbType.VarChar)).Value = " ";
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_NURSE_STATUS", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_VERSION", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_ID", SqlDbType.VarChar)).Value = "";

                    //IN THE SYSTEM OPTIONS IF FILEING REQUEST IS SELECTED VISIT STATUS IS 'F' ELSE 'W'
                    if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_STATUS", SqlDbType.VarChar)).Value = "F";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_STATUS", SqlDbType.VarChar)).Value = "W";
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPV_VISIT_TYPE", SqlDbType.VarChar)).Value = lblVisitType.Text;


                    Boolean UpdateWaitingList = false;
                    string strUpdateWaitingList = hidWaitingList.Value;


                    if (chkUpdateWaitList.Checked == true)
                    {
                        UpdateWaitingList = true;

                        if (hidPateName.Value != "HomeCare")
                        {
                            if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                            {
                                if (strUpdateWaitingList == "true")
                                {
                                    UpdateWaitingList = true;
                                }
                                else
                                {
                                    UpdateWaitingList = false;
                                }
                            }
                            else
                            {
                                UpdateWaitingList = true;
                            }

                        }

                    }
                    else
                    {
                        UpdateWaitingList = false;



                    }



                    // cmd.Parameters.Add(new SqlParameter("@IsAddVisit", SqlDbType.Bit)).Value = UpdateWaitingList;
                    cmd.Parameters.Add(new SqlParameter("@IsUpdateVisit", SqlDbType.Bit)).Value = UpdateWaitingList;

                    cmd.Parameters.Add(new SqlParameter("@IsManualToken", SqlDbType.Bit)).Value = chkManualToken.Checked;



                    //if (txtOpBalanceDate.Text != "")
                    //{
                    //    DateTime OPBDate1;
                    //    OPBDate1 = Convert.ToDateTime(txtOpBalanceDate.Text);
                    //    cmd.Parameters.Add(new SqlParameter("@HPB_DATE", SqlDbType.VarChar)).Value = OPBDate1.ToString("MM/dd/yyyy");
                    //}
                    //else
                    //{
                    cmd.Parameters.Add(new SqlParameter("@HPB_DATE", SqlDbType.VarChar)).Value = "";
                    //  }




                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_MOBILE1", SqlDbType.Decimal)).Value = txtKinMobile1.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_PHONE3", SqlDbType.Decimal)).Value = txtPhone3.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HPM_VISA_TYPE", SqlDbType.VarChar)).Value = drpVisaType.SelectedValue;




                    int k = cmd.Parameters.Count;
                    for (int i = 0; i < k; i++)
                    {
                        if (cmd.Parameters[i].Value == "")
                        {
                            cmd.Parameters[i].Value = DBNull.Value;
                        }
                    }

                    SqlParameter returnValue = new SqlParameter("@ReturnPTId", SqlDbType.VarChar, 10);
                    returnValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnValue);


                    SqlParameter PTVisitID = new SqlParameter("@PTVisitID", SqlDbType.VarChar, 15);
                    PTVisitID.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(PTVisitID);

                    cmd.ExecuteNonQuery();
                    string strReturnFileNo;
                    strReturnFileNo = Convert.ToString(returnValue.Value);
                    hidActualFileNo.Value = strReturnFileNo;

                    string strPTVisitID = "";
                    strPTVisitID = Convert.ToString(PTVisitID.Value);

                    if (strPTVisitID != "")
                    {
                        hidPTVisitSQNo.Value = strPTVisitID;
                    }

                    con.Close();

                    TextFileWriting("Patent Master Insert End ");



                    //*********************  PATIENT MASTER INSERT END  **************************

                    if (drpPatientType.SelectedValue == "CR")
                    {
                        string strPhotoID = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                        string strPath = Server.MapPath("../Uploads/");
                        Byte[] bytImage = null, bytImage1 = null;


                        if (CheckPatientPhoto() == false)
                        {
                            if (Convert.ToString(Session["InsCardFrontDefaultPath"]) != "")
                            {
                                System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontDefaultPath"]));
                                bytImage = ImageToByte(image);
                            }



                            if (Convert.ToString(Session["InsCardBackDefaultPath"]) != "")
                            {
                                System.Drawing.Image image1 = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardBackDefaultPath"]));
                                bytImage1 = ImageToByte(image1);
                            }

                            dbo.PatientPhotoAdd(txtFileNo.Text, Convert.ToString(Session["Branch_ID"]), bytImage, bytImage1, strPhotoID, "Insurance");


                            if (strPhotoID != "" && strPhotoID != null && hidPTVisitSQNo.Value != "" && hidPTVisitSQNo.Value != null)
                            {
                                string FieldWithValue = " HPV_PHOTO_ID='" + strPhotoID + "'";
                                string UpdateCondition = " HPV_PT_ID='" + hidActualFileNo.Value + "' AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_SEQNO=" + hidPTVisitSQNo.Value;


                                dbo.fnUpdatetoDb("HMS_PATIENT_VISIT", FieldWithValue, UpdateCondition);
                            }


                        }
                        else
                        {

                            strPhotoID = Convert.ToString(ViewState["Photo_ID"]);

                            if (Convert.ToString(Session["InsCardFrontDefaultPath"]) != "")
                            {
                                System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontDefaultPath"]));
                                bytImage = ImageToByte(image);
                            }


                            if (Convert.ToString(Session["InsCardBackDefaultPath"]) != "")
                            {
                                System.Drawing.Image image1 = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardBackDefaultPath"]));
                                bytImage1 = ImageToByte(image1);
                            }

                            dbo.PatientPhotoInsUpdate(txtFileNo.Text, Convert.ToString(Session["Branch_ID"]), bytImage, bytImage1, strPhotoID, "Insurance");


                            if (strPhotoID != "" && strPhotoID != null && hidPTVisitSQNo.Value != "" && hidPTVisitSQNo.Value != null)
                            {
                                string FieldWithValue = " HPV_PHOTO_ID='" + strPhotoID + "'";
                                string UpdateCondition = " HPV_PT_ID='" + hidActualFileNo.Value + "' AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_SEQNO=" + hidPTVisitSQNo.Value;


                                dbo.fnUpdatetoDb("HMS_PATIENT_VISIT", FieldWithValue, UpdateCondition);
                            }

                        }

                    }


                    ModalPopupExtender1.Show();
                    strMessage = "Details Saved !";

                    if (hidLabelPrint.Value == "true")
                    {
                        btnLabelPrint_Click(btnLabelPrint, new EventArgs());


                    }


                    if (hidTokenPrint.Value == "true")
                    {
                        btnTokenPrint_Click(btnTokenPrint, new EventArgs());

                    }



                    if (Convert.ToString(Session["ClearAfterSave"]).ToUpper() == "Y")
                    {
                        txtFileNo.Text = "";
                        chkFamilyMember.Checked = false;
                        Clear();
                    }
                    else
                    {
                        ViewState["Patient_ID_VALID"] = "1";
                    }

                }



            SaveEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

            if (hidPateName.Value == "HomeCare")
            {
                Response.Redirect("../HomeCare/HomeCareRegistration.aspx?PatientId=" + hidActualFileNo.Value);
            }

        }

        protected void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCompCashPT.Checked == true)
            {
                Session["CompcashPt"] = "Y";
            }
            else
            {
                Session["CompcashPt"] = "N";
            }
        }

        protected void chkStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStatus.Checked == true)
            {
                Session["status"] = "A";

            }

            else
            {
                Session["status"] = "I";
            }

        }

        protected void chkManualToken_CheckedChanged(object sender, EventArgs e)
        {
            if (chkManualToken.Checked == true)
            {
                txtTokenNo.Enabled = true;
                txtTokenNo.Focus();
            }

            else
            {
                txtTokenNo.Enabled = false;
            }

        }

        protected void drpPatientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpNetworkName.Items.Clear();
            if (drpDefaultCompany.Items.Count > 0)
            {
                drpDefaultCompany.SelectedIndex = 0;
            }
            txtPolicyNo.Text = "";
            txtPolicyId.Text = "";
            txtIDNo.Text = "";
            txtNetworkClass.Text = "";

            txtExpiryDate.Text = "";
            txtPolicyStart.Text = "";
            // drpDefaultCompany.Items.Clear();
            drpPlanType.Items.Clear();


            if (drpPatientType.SelectedValue == "CR")
            {
                drpParentCompany.Enabled = true;

                txtPolicyNo.Enabled = true;
                txtPolicyId.Enabled = true;
                txtIDNo.Enabled = true;
                txtNetworkClass.Enabled = true;
                txtExpiryDate.Enabled = true;
                txtPolicyStart.Enabled = true;
                drpPlanType.Enabled = true;
                drpDefaultCompany.Enabled = true;
                drpNetworkName.Enabled = true;
            }
            else
            {
                drpParentCompany.Enabled = false;
                txtPolicyNo.Enabled = false;
                txtPolicyId.Enabled = false;
                txtIDNo.Enabled = false;
                txtNetworkClass.Enabled = false;
                txtExpiryDate.Enabled = false;
                txtPolicyStart.Enabled = false;
                drpDefaultCompany.Enabled = false;
                drpPlanType.Enabled = false;
                drpNetworkName.Enabled = false;
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtFileNo.Text = "";
            hidActualFileNo.Value = "";
            txtFileNo.Focus();
            chkFamilyMember.Checked = false;
            Clear();
            chkStatus.Checked = true;
            //  chkManual_CheckedChanged(chkManual, new EventArgs());
        }

        protected void chkFamilyMember_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFamilyMember.Checked == true)
            {
                //lblVisitType.Text = "New";
                //txtFName.Text = "";
                //txtMName.Text = "";
                //txtLName.Text = "";
                //txtDOB.Text = "";
                //txtEmiratesID.Text = "";
                //txtFileNo.Focus();
                //txtAge.Text = "";
                //drpSex.SelectedIndex = 0;
                //  ViewState["Patient_ID_VALID"] = "0";
                Clear();
            }

            else
            {

            }

        }

        #region Crystal Report

        protected void btnMRRPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }

            // System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            //// Pro.StartInfo.FileName = "C:\\P1.bat";
            //Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            //Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + Server.MapPath("Reports\\HmsMRRPrint.rpt") + "${HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'$SCREEN$$$1";
            // Pro.StartInfo.RedirectStandardInput = false;
            // Pro.StartInfo.RedirectStandardOutput = true;
            // Pro.StartInfo.CreateNoWindow = true;
            // Pro.StartInfo.UseShellExecute = false;
            // Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            // Pro.Start();

            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            // string strReport = "HmsMRRPrint.rpt";
            //String DBString = "", strFormula = "";
            //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            //strFormula = hidActualFileNo.Value;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowBalance('" + strReport + "','" + DBString + "','" + hidActualFileNo.Value + "');", true);
            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMRRPrintPDF('" + hidActualFileNo.Value + "');", true);


        FunEnd: ;
        }

        protected void btnIDPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }

            //System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            ////Pro.StartInfo.FileName = "c:\\printRpt.exe";
            //Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            //Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + Server.MapPath("Reports\\HmsPtIDCard.rpt") + "${HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'$SCREEN$$" + strIdPrinterName + "$" + txtLablePrintCount.Text;
            //Pro.StartInfo.RedirectStandardInput = false;
            //Pro.StartInfo.RedirectStandardOutput = true;
            //Pro.StartInfo.CreateNoWindow = true;
            //Pro.StartInfo.UseShellExecute = false;
            //Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //Pro.Start();

            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            string strReport = "HmsPtIDCard.rpt";
            String DBString = "", strFormula = "";
            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualFileNo.Value;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowIDPrint('" + strReport + "','" + DBString + "','" + hidActualFileNo.Value + "');", true);
        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END




        FunEnd: ;


        }

        protected void btnTokenPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }
            DataSet ds = new DataSet();
            ds = dbo.GetMaxPatientVisit(hidActualFileNo.Value);
            Int32 intSQNO = 1;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HPVSEQNO") == false)
                {
                    intSQNO = Convert.ToInt32(Convert.ToString(ds.Tables[0].Rows[0]["HPVSEQNO"]));
                }
            }

            //System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            //Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            //Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + Server.MapPath("Reports\\HMSToken.rpt") + "${hms_patient_visit.HPV_SEQNO}='" + intSQNO + "'$PRINT$$$1";
            //Pro.StartInfo.RedirectStandardInput = false;
            //Pro.StartInfo.RedirectStandardOutput = true;
            //Pro.StartInfo.CreateNoWindow = true;
            //Pro.StartInfo.UseShellExecute = false;
            //Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //Pro.Start();


            // string strFormula = " {hms_patient_visit.HPV_SEQNO}='" + intSQNO + "'";
            //Session["ReportFormula"] = strFormula;
            //   string strPrintCount="1";
            //if (txtLablePrintCount.Text.Trim() != "")
            //{
            //    strPrintCount = txtLablePrintCount.Text.Trim();
            //}

            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ReportPrintPopup('HMSToken.rpt','PRINT','" + strPrintCount + "');", true);


            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text.Trim());
            string strReport = "HMSToken.rpt";

            String DBString = "";


            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowOnLoad('" + strReport + "','" + DBString + "','" + intSQNO + "','" + intCount + "');", true);
        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            //strLabel = "";
        //strToken = "";
        //BIndToken();
        //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), " window.print();", true);


FunEnd: ;
        }

        protected void btnLabelPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }

            /*System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text);

            string strReport = Server.MapPath("Reports\\HmsLabel.rpt");
            if (chkLabelPrint.Checked == true)
            {
                strReport = Server.MapPath("Reports\\HmsLabel1.rpt");
            }

            Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + strReport + "${HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'$PRINT$$" + strLabelPrinterName + "$" + txtLablePrintCount.Text;
            Pro.StartInfo.RedirectStandardInput = false;
            Pro.StartInfo.RedirectStandardOutput = true;
            Pro.StartInfo.CreateNoWindow = true;
            Pro.StartInfo.UseShellExecute = false;
            Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            Pro.Start();
            */
            // string rptcall = "CReports/ReportsView.aspx?ReportName=HmsLabel&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=" + txtFileNo.Text.Trim()  ;
            //  string rptcall = "CReports/ReportsView.aspx?ReportName=HmsLabel.rpt&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() +"'" ;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);

            // ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('CReports/ReportsView.aspx?ReportName=HmsLabel.rpt&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=" + txtFileNo.Text.Trim() + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);

            ////string strReport = "HmsLabel.rpt";
            ////if (chkLabelPrint.Checked == true)
            ////{
            ////    strReport = "HmsLabel1.rpt";
            ////}
            ////string strFormula = " {HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'";
            ////Session["ReportFormula"] = strFormula;
            ////string strPrintCount = "1";
            ////if (txtLablePrintCount.Text.Trim() != "")
            ////{
            ////    strPrintCount = txtLablePrintCount.Text.Trim();
            ////}

            //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ReportPrintPopup('" + strReport + "','PRINT','" + strPrintCount + "');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "ReportPopup(+" strReport ,"{HMS_PATIENT_MASTER.HPM_PT_ID}=" + txtFileNo.Text.Trim() + ");


            ////   Response.Redirect("CReports\\ReportsView.aspx?ReportName="+ strReport +"&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'");



            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text);
            string strReport = "HmsLabel.rpt";
            if (chkLabelPrint.Checked == true)
            {
                strReport = "HmsLabel.rpt";
            }

            String DBString = "", strFormula = "";

            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualFileNo.Value;

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowOnLoad('" + strReport + "','" + DBString + "','" + strFormula + "','" + intCount + "');", true);

           // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            //strLabel = "";
        //strToken = "";
        //BindLabel();
        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), " window.print();", true);
        FunEnd: ;
        }

        //protected void btnBalance_Click(object sender, EventArgs e)
        //{
        //    if (hidActualFileNo.Value == "")
        //    {
        //        goto FunEnd;
        //    }
        //    // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
        //    string strReport = "HmsPtBalHist.rpt";
        //    String DBString = "", strFormula = "";
        //    DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
        //    strFormula = hidActualFileNo.Value;
        //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowBalance('" + strReport + "','" + DBString + "','" + hidActualFileNo.Value + "');", true);
        //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END



        //FunEnd: ;

        //}

        protected void btnClaimPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (hidActualFileNo.Value == "")
                {
                    goto FunEnd;
                }
                if (drpDefaultCompany.Items.Count == 0)
                {
                    goto FunEnd;
                }

                //BELOW CODING WILL INSERT THE SCAN CARD IMAGE IN TEMP FILE TABLE FOR SHOWING THE CARD IMAGE IN REPORT
                if (Convert.ToString(Session["InsCardFrontPath"]) != "")
                {
                    string strPath = Server.MapPath("../Uploads/");
                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontPath"]));


                    Byte[] bytImage;
                    bytImage = ImageToByte(image);

                    intImageSize = bytImage.Length;

                    dbo.TempImageAdd(txtFileNo.Text.Trim(), Convert.ToString(Session["Branch_ID"]), bytImage);

                }


                string strRptName = "";
                strRptName = fnGetClaimFormName(drpDefaultCompany.SelectedValue, txtDepName.Text);

                if (File.Exists(strReportPath + strRptName) == false)
                {
                    lblStatus.Text = "File Not Found";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
                //String DBString = "";
                //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowClaimPrint('" + strRptName + "','" + DBString + "','" + hidActualFileNo.Value + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);

                //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowClaimPrintPDF('" + strRptName + "','" + hidActualFileNo.Value + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);



                //BELOW CODING WILL DELETE THE SCAN CARD IMAGE FROM TEMP FILE TABLE AFTER SHOWING THE CARD IMAGE IN REPORT
                //  dbo.TempImageDelete(txtFileNo.Text.Trim());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnClaimPrint_Click");
                TextFileWriting(ex.Message.ToString());

            }
        FunEnd: ;

        }


        protected void btnGeneConTreatment_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")//|| Convert.ToString(ViewState["PTLastGCId"]) == "")
            {
                goto FunEnd;
            }



            //  Int64 GCId = Convert.ToInt64(ViewState["PTLastGCId"]);
            //  AddSignatureTempImage(GCId);

            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            ////  string strReport = "GeneralTreatmentConsent.rpt";
            //String DBString = "", strFormula = "";
            //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            //strFormula = hidActualFileNo.Value;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReport('" + DBString + "'," + Convert.ToString(ViewState["HGC_ID"]) + ");", true);

            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReportPDF('" + hidActualFileNo.Value + "');", true);


            //dbo.TempImageDelete(hidActualFileNo.Value);

            //WEB REPORT COMMENTED AS PER CLIENT REQUEST
        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "GeneralConsentPopup('" + hidActualFileNo.Value + "','0');", true);


        FunEnd: ;


        }

        protected void btnDamanGC_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")// || Convert.ToString(ViewState["PTLastGCId"]) == "")
            {
                goto FunEnd;
            }

            // Int64 GCId = Convert.ToInt64(ViewState["PTLastGCId"]);
            // AddSignatureTempImage(GCId);

            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            //// string strReport = "DamanConscent.rpt";
            //String DBString = "", strFormula = "";
            //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            //strFormula = hidActualFileNo.Value;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowDamanGCReport('" + DBString + "'," + ViewState["HGC_ID"] + ");", true);
            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowDamanGCReportPDF('" + hidActualFileNo.Value + "');", true);

          //  string rptcall = "CReports/ReportsView.aspx?ReportName=DamanConscent.rpt&SelectionFormula={GeneralConsentView.HGC_ID}=" + ViewState["HGC_ID"];
        //  ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);

          // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReport1('" + ViewState["HGC_ID"] + "');", true);


        FunEnd: ;

        }



        protected void btnConsentFormPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")// || Convert.ToString(ViewState["PTLastGCId"]) == "")
            {
                goto FunEnd;
            }

            // Int64 GCId = Convert.ToInt64(ViewState["PTLastGCId"]);
            //  AddSignatureTempImage(GCId);

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowConsentFormReportPDF('" + hidActualFileNo.Value + "');", true);



        FunEnd: ;

        }


        #endregion

        #region Web Report


        protected void btnWebRepot_Click(object sender, EventArgs e)
        {
            //BELOW CODING WILL INSERT THE SCAN CARD IMAGE IN TEMP FILE TABLE FOR SHOWING THE CARD IMAGE IN REPORT
            //if (Convert.ToString(Session["InsCardFrontPath"]) != "")
            //{
            //    string strPath = Server.MapPath("../Uploads/");
            //    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Session["InsCardFrontPath"].ToString());


            //    Byte[] bytImage;
            //    bytImage = imageToByteArray(image);

            //    intImageSize = bytImage.Length;

            //    dbo.TempImageAdd(txtFileNo.Text.Trim(), Convert.ToString(Session["Branch_ID"]), bytImage);

            //}
            //string strRptName = "";
            //strRptName = fnGetWebClaimFormName(drpDefaultCompany.SelectedValue, txtDepName.Text);

            //string rptcall = "WebReport/" + strRptName + "?PatientId=" + txtFileNo.Text + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=1&DRId=" + txtDoctorID.Text + "&DrDepName=" + txtDepName.Text + "&Date=0";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);
            ////  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "WebReportPopup(" + txtFileNo.Text + ");", true);

            ////BELOW CODING WILL DELETE THE SCAN CARD IMAGE FROM TEMP FILE TABLE AFTER SHOWING THE CARD IMAGE IN REPORT
            //dbo.TempImageDelete(txtFileNo.Text.Trim());

            //CompanyId

            string rptcall = "../WebReport/WebReportLoader.aspx?PatientId=" + txtFileNo.Text + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=6&DRId=" + txtDoctorID.Text + "&DrDepName=" + txtDepName.Text + "&Date=0&CompanyId=" + drpDefaultCompany.SelectedValue;// txtCompany.Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);



        }

        protected void btnDentalWebRepot_Click(object sender, EventArgs e)
        {
            string rptcall = "../WebReport/EMR_Barbara_DentalConsentForm.aspx?PatientId=" + txtFileNo.Text + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=6&DRId=" + txtDoctorID.Text + "&DrDepName=" + txtDepName.Text + "&Date=0";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

        }




        protected void btnCardPrint_Click(object sender, EventArgs e)
        {
            //string rptcall = "WebReport/ScandCardPrint.aspx";
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeSheet Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);
            if (Session["InsCardFrontPath"] != "")
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ScandCardPopup('" + txtFileNo.Text + "');", true);
            }


        }
        #endregion

        protected void btnBalance_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }




        FunEnd: ;

        }

        protected void AsyncFileUpload1_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                //if (File.Exists(Server.MapPath("../Uploads/" + Session["InsCardFrontPath"].ToString())))
                //{
                //    goto EvEnd;

                //}
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));
                    string strPath = Server.MapPath("../Uploads/");
                    AsyncFileUpload1.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardFrontPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardFrontPathT"] = strFileNmae;
                    string strPath1 = Server.MapPath("../Uploads/Temp/");
                    AsyncFileUpload1.SaveAs(strPath1 + strFileNmae);


                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardFrontPath"] = null;
                }

                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload1_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AsyncFileUpload2_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                //if (File.Exists(Server.MapPath("../Uploads/" + Session["InsCardBackPath"].ToString())))
                //{
                //    goto EvEnd;

                //}

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));
                    string strPath = Server.MapPath("../Uploads/");
                    AsyncFileUpload2.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardBackPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardBackPathT"] = strFileNmae;
                    string strPath2 = Server.MapPath("../Uploads/Temp/");
                    AsyncFileUpload2.SaveAs(strPath2 + strFileNmae);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardBackPath"] = null;
                }
                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload2_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void btnScanCardAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strPhotoID = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                DataTable dt = new DataTable();

                dt = (DataTable)ViewState["ScanCard"];



                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (Convert.ToString(dt.Rows[i]["HSC_CARD_NAME"]) == drpCardName.SelectedValue && chkIsDefault.Checked == true && Convert.ToString(dt.Rows[i]["HSC_ISDEFAULT"]) == "True")
                    {
                        lblStatusTab2.Text = "Only one Default card can add for Selected Card Name";
                        goto EvEnd;
                    }

                    if (Convert.ToString(dt.Rows[i]["HSC_INS_COMP_ID"]) == txtCompany.Text && Convert.ToString(dt.Rows[i]["HSC_EXPIRY_DATE"]) == txtCardExpDate.Text)
                    {
                        lblStatusTab2.Text = "This card already exits for same Expiry date";
                        goto EvEnd;
                    }


                }



                dboperations objOP = new dboperations();
                objOP.PatienId = hidActualFileNo.Value;
                objOP.BranchId = Convert.ToString(Session["Branch_ID"]);
                objOP.CardName = drpCardName.SelectedValue;
                objOP.CompId = txtCompany.Text.Trim();
                objOP.CompName = txtCompanyName.Text.Trim();
                objOP.CardNo = txtCardNo.Text.Trim();
                objOP.PolicyCardNo = txtPolicyCardNo.Text.Trim();
                objOP.ExpDate = txtCardExpDate.Text;
                objOP.PolicyStart = txtEffectiveDate.Text;


                if (chkIsDefault.Checked == true)
                {
                    objOP.isDeafult = "True";
                }
                else
                {
                    objOP.isDeafult = "False";
                }


                if (chkCardActive.Checked == true)
                {
                    objOP.Status = "True";
                }
                else
                {
                    objOP.Status = "False";
                }


                objOP.ImagePathFront = Convert.ToString(Session["InsCardFrontPath"]);
                objOP.ImagePathBack = Convert.ToString(Session["InsCardBackPath"]);
                objOP.HSC_PLAN_TYPE = "";


                objOP.HSC_PHOTO_ID = strPhotoID;


                objOP.ScanCardImageAdd(objOP);



                ClearScan();

                BindScanCardImage();
            EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnScanCardAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnScanCardUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string strPhotoID = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ScanCard"];
                Int32 R = Convert.ToInt32(ViewState["ScanCardSelectIndex"]);

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (R != i && Convert.ToString(dt.Rows[i]["HSC_CARD_NAME"]) == drpCardName.SelectedValue && chkIsDefault.Checked == true && Convert.ToString(dt.Rows[i]["HSC_ISDEFAULT"]) == "True")
                    {
                        lblStatusTab2.Text = "Only one Default card can add for Selected Card Name";
                        goto EvEnd;
                    }



                    if (R != i && Convert.ToString(dt.Rows[i]["HSC_INS_COMP_ID"]) == txtCompany.Text && Convert.ToString(dt.Rows[i]["HSC_EXPIRY_DATE"]) == txtCardExpDate.Text)
                    {
                        lblStatusTab2.Text = "This card already exits for same Expiry date";
                        goto EvEnd;
                    }

                }


                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "";
                string Criteria = "1=2";

                if (drpCardType.SelectedValue == "Insurance")
                {
                    FieldNameWithValues = " HSC_IMAGE_PATH_FRONT='" + Convert.ToString(Session["InsCardFrontPath"]) + "', HSC_IMAGE_PATH_BACK='" + Convert.ToString(Session["InsCardBackPath"]) + "'";
                    Criteria = " HSC_PT_ID='" + hidActualFileNo.Value + "' AND  HSC_INS_COMP_ID='" + txtCompany.Text.Trim() + "' AND HSC_POLICY_NO='" + txtPolicyCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + txtCardExpDate.Text + "',103)";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_SCANCARD_IMAGES  ", Criteria);

                }
                else
                {
                    FieldNameWithValues = " HSC_IMAGE_PATH_FRONT='" + Convert.ToString(Session["InsCardFrontPath"]) + "', HSC_IMAGE_PATH_BACK='" + Convert.ToString(Session["InsCardBackPath"]) + "'";
                    Criteria = " HSC_PT_ID='" + hidActualFileNo.Value + "' AND  HSC_CARD_NAME='" + drpCardName.SelectedValue + "' AND HSC_CARD_NO='" + txtCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + txtCardExpDate.Text + "',103)";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_SCANCARD_IMAGES  ", Criteria);
                }



                ClearScan();

                BindScanCardImage();
            EvEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnScanCardUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void EditScanCard_Click(object sender, EventArgs e)
        {
            try
            {
                btnScanCardAdd.Visible = false;
                btnScanCardUpdate.Visible = true;

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ScanCardSelectIndex"] = gvScanCard.RowIndex;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblCardType, lblCardName, lblCompId, lblCompName, lblgvCardNo, lblPolicyCardNo, lblExpDate, lblIsDefault, lblStatus, lblFrontPath, lblBackPath, lblEffectiveDate, lblPlanType;
                lblCardType = (Label)gvScanCard.Cells[0].FindControl("lblCardType");
                lblCardName = (Label)gvScanCard.Cells[0].FindControl("lblCardName");


                lblCompId = (Label)gvScanCard.Cells[0].FindControl("lblCompId");
                lblCompName = (Label)gvScanCard.Cells[0].FindControl("lblCompName");
                lblgvCardNo = (Label)gvScanCard.Cells[0].FindControl("lblgvCardNo");
                lblPolicyCardNo = (Label)gvScanCard.Cells[0].FindControl("lblPolicyCardNo");

                lblExpDate = (Label)gvScanCard.Cells[0].FindControl("lblExpDate");
                lblEffectiveDate = (Label)gvScanCard.Cells[0].FindControl("lblEffectiveDate");
                lblIsDefault = (Label)gvScanCard.Cells[0].FindControl("lblIsDefault");
                lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");
                lblFrontPath = (Label)gvScanCard.Cells[0].FindControl("lblFrontPath");
                lblBackPath = (Label)gvScanCard.Cells[0].FindControl("lblBackPath");
                lblPlanType = (Label)gvScanCard.Cells[0].FindControl("lblPlanType");

                string strCardType;

                strCardType = ScanCardTypeGet(lblCardName.Text);
                if (strCardType != "")
                {
                    for (int intCount = 0; intCount < drpCardType.Items.Count; intCount++)
                    {
                        if (drpCardType.Items[intCount].Value == strCardType)
                        {
                            drpCardType.SelectedValue = strCardType;
                        }
                    }
                }


                drpCardType_SelectedIndexChanged(drpCardType, new EventArgs());
                drpCardName.SelectedValue = lblCardName.Text;
                txtCardExpDate.Text = lblExpDate.Text;
                txtEffectiveDate.Text = lblEffectiveDate.Text;
                txtPolicyCardNo.Text = lblPolicyCardNo.Text;
                txtCompany.Text = lblCompId.Text;
                txtCompanyName.Text = lblCompName.Text;
                txtCardNo.Text = lblgvCardNo.Text;

                txtPlanType.Text = lblPlanType.Text;


                chkIsDefault.Checked = Convert.ToBoolean(lblIsDefault.Text);
                chkCardActive.Checked = Convert.ToBoolean(lblStatus.Text);
                imgFront.ImageUrl = @"../Uploads/" + lblFrontPath.Text;
                imgBack.ImageUrl = @"../Uploads/" + lblBackPath.Text;

                Session["InsCardFrontPath"] = lblFrontPath.Text;
                Session["InsCardBackPath"] = lblBackPath.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.EditScanCard_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteScanCard_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                string Criteria = "1=2";
                Label lblCardName = (Label)gvScanCard.Cells[0].FindControl("lblCardName");
                Label lblCompId = (Label)gvScanCard.Cells[0].FindControl("lblCompId");
                Label lblPolicyCardNo = (Label)gvScanCard.Cells[0].FindControl("lblPolicyCardNo");
                Label lblExpDate = (Label)gvScanCard.Cells[0].FindControl("lblExpDate");

                if (lblCardName.Text == "Insurance" || lblCardName.Text == "Topup")
                {
                    Criteria = " HSC_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HSC_INS_COMP_ID='" + lblCompId.Text + "' AND HSC_POLICY_NO='" + lblPolicyCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + lblExpDate.Text + "',103)";
                }
                else
                {
                    Criteria = " HSC_PT_ID='" + txtFileNo.Text.Trim() + "'  AND HSC_POLICY_NO='" + lblPolicyCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + lblExpDate.Text + "',103)";

                }

                CommonBAL objCom = new CommonBAL();

                objCom.fnDeleteTableData("HMS_SCANCARD_IMAGES", Criteria);

                BindScanCardImage();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.DeleteScanCard_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void bntClear_Click(object sender, EventArgs e)
        {
            //btnScanCardAdd.Visible = true;
            //btnScanCardUpdate.Visible = false;
            //ClearScan();

            Response.Redirect("PTRegistration.aspx");
        }


        protected void gvScanCard_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (PTRegChangePolicyNoCaption.ToLower() == "true")
                {
                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        e.Row.Cells[3].Text = "Member ID";

                        e.Row.Cells[5].Text = "Policy No.";
                    }


                    if (GlobalValues.FileDescription.ToUpper() == "ALAMAL")
                    {
                        e.Row.Cells[3].Text = "Card No.";
                        gvScanCard.Columns[5].Visible = false;

                    }

                }
            }
        }


        void BindEmirateIdDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTE_ID Like '" + Convert.ToString(ViewState["EmirateIdRef"]) + "'";
            DS = dbo.TempEmiratesIdGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HTE_Photo") == false)
                    Session["Photo"] = (Byte[])DS.Tables[0].Rows[0]["HTE_PHOTO"];

                //chkManual.Checked = false;
                //chkManual_CheckedChanged("chkManual", new EventArgs());
                string strFullName = Convert.ToString(DS.Tables[0].Rows[0]["HTE_FullName"]).Trim();
                string[] ArrFullName = strFullName.Split(' ');

                txtLName.Text = "";
                if (ArrFullName.Length > 0)
                {

                    for (int j = 0; j <= ArrFullName.Length - 1; j++)
                    {
                        if (j == 0)
                        {
                            txtFName.Text = ArrFullName[j];
                        }
                        else if (j == 1)
                        {
                            txtMName.Text = ArrFullName[j];
                        }
                        else if (j >= 2)
                        {
                            txtLName.Text += ArrFullName[j] + " ";
                        }
                    }

                }

                txtEmiratesID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_IDNumber"]).Trim();
                txtCardNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CardNo"]).Trim();

                string strDOB = Convert.ToString(DS.Tables[0].Rows[0]["HTE_DOB"]).Trim();
                string[] arrDOB = strDOB.Split(' ');

                if (arrDOB.Length > 0)
                {
                    txtDOB.Text = arrDOB[0];
                }

                if (DS.Tables[0].Rows[0].IsNull("HTE_Sex") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]) != "0")
                {
                    for (int intSex = 0; intSex < drpSex.Items.Count; intSex++)
                    {
                        if (drpSex.Items[intSex].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]).Trim();
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;

                if (DS.Tables[0].Rows[0].IsNull("HTE_MaritalStatus") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]) != "0")
                {
                    for (int intMstatus = 0; intMstatus < drpMStatus.Items.Count; intMstatus++)
                    {
                        if (drpMStatus.Items[intMstatus].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]))
                        {
                            drpMStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]).Trim();
                            goto ForMStatus;
                        }

                    }
                }
            ForMStatus: ;




                if (DS.Tables[0].Rows[0].IsNull("HTE_CityDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]) != "0")
                {
                    for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                    {
                        if (drpCity.Items[intCityCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]))
                        {
                            drpCity.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]);
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;


                if (DS.Tables[0].Rows[0].IsNull("HTE_AreaDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]) != "0")
                {
                    for (int intAreaCount = 0; intAreaCount < drpArea.Items.Count; intAreaCount++)
                    {
                        if (drpArea.Items[intAreaCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]))
                        {
                            drpArea.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]);
                            goto ForArea;
                        }

                    }
                }
            ForArea: ;

                txtPoBox.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_POBox"]).Trim();
                txtAddress.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Address"]);
                txtPhone1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Phone"]).Trim();
                if (lblVisitType.Text.Trim() == "New")
                    txtMobile1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Mobile"]).Trim();




                if (txtDOB.Text.Trim() != "")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                    AgeCalculation();
                }

                BindPhoto();


            }
        }

        void BindEmiretsId()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTE_ID Like '" + Convert.ToString(ViewState["EmirateIdRef"]) + "'";
            DS = dbo.TempEmiratesIdGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HTE_Photo") == false)
                    Session["Photo"] = (Byte[])DS.Tables[0].Rows[0]["HTE_PHOTO"];


                string strEmiratesID = (string)DS.Tables[0].Rows[0]["HTE_IDNUMBER"];
                string strFormatedEmiratesID = "";

                string strEIDPart1 = "", strEIDPart2 = "", strEIDPart3 = "", strEIDPart4 = "";

                strEIDPart1 = strEmiratesID.Substring(0, 3);
                strEIDPart2 = strEmiratesID.Substring(3, 4);
                strEIDPart3 = strEmiratesID.Substring(7, 7);
                strEIDPart4 = strEmiratesID.Substring(14, 1);


                strFormatedEmiratesID = strEIDPart1 + "-" + strEIDPart2 + "-" + strEIDPart3 + "-" + strEIDPart4;

                DataSet ds1 = new DataSet();
                Criteria = " ( HPM_IQAMA_NO  = '" + Convert.ToString(DS.Tables[0].Rows[0]["HTE_IDNUMBER"]).Trim() + "'  OR  HPM_IQAMA_NO  = '" + strFormatedEmiratesID + "'  )";
                ds1 = dbo.retun_patient_details(Criteria);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(ViewState["EIDConfirmResult"]) != "Yes")
                    {

                        divConfirmMessageClose.Style.Add("display", "block");
                        divConfirmMessage.Style.Add("display", "block");
                        lblConfirmMessage.Text = "Would you like to replace the patient information from Emirates ID Card?";// "Do you want to update Informationg as per Emirates ID?";
                        ViewState["ConfirmMsgFor"] = "EmiratesIDLoad";
                        ViewState["EmiratesIDPT_ID"] = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();
                        ViewState["EIDConfirmResult"] = "";
                        goto FunEnd;



                    }

                    DS.Dispose();


                }

                BindEmirateIdDtls();

            }

        FunEnd: ;


        }

        protected void btnScan_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                chkFamilyMember.Checked = false;
                // Clear();


                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                ViewState["EmirateIdRef"] = strName;
                String DBString = "";
                DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "GetEmiratesIdData('" + @DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnScan_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpDefaultCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (drpDefaultCompany.SelectedIndex != 0)
                {
                    stParentName1 = drpDefaultCompany.SelectedValue.Trim();
                }


                string strRemainderRecept = "";
                BindCompanyDetails(out strRemainderRecept);

                strReminderMessage = "Message from Company Master <br/> " + strRemainderRecept;



                if (strRemainderRecept != "")
                {

                    ModalPopupExtender5.Show();
                }
                BindSubCompany();
                BindPlanType();


                // BindScanCardImageSelected();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpDefaultCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void drpCardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BinScanCardMasterGet();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpCardType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void bntFamilyApt_Click(object sender, EventArgs e)
        {
            try
            {
                CheckFamilyAppoinment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.bntFamilyApt_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpParentCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDefaultCompany();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpParentCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppoinmentDtls();
                hidActualFileNo.Value = "";
                txtFileNo.Text = txtFileNo.Text.ToUpper();
                if (chkFamilyMember.Checked != true)
                {
                    Clear();
                    BindModifyData();
                    ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindModifyData() completed");
                }

                drpPatientType.Focus();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void txtMobile1_TextChanged(object sender, EventArgs e)
        {
            GetAppoinmentDtls();
            if (CheckMobileNo() == true)
            {
                ModalPopupExtender1.Show();
            }

            txtMobile2.Focus();
        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {


            txtTokenNo.Text = "";
            txtDepName.Text = "";
            string strName = txtDoctorID.Text.Trim();

            string[] arrName = strName.Split('~');
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";

            DataSet DS = new DataSet();


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0].Trim();
                txtDoctorName.Text = arrName[1].Trim();
                Criteria += " and HSFM_STAFF_ID='" + arrName[0].Trim() + "' ";
                DS = dbo.retun_doctor_detailss(Criteria);
            }
            else
            {
                Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text.Trim() + "'";
                DS = dbo.retun_doctor_detailss(Criteria);
            }


            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]).Trim();
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["TokenNo"]).Trim();
                txtDepName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]).Trim();
            }
            if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            {
                GetPatientVisit();
            }






        }

        protected void txtDoctorName_TextChanged(object sender, EventArgs e)
        {

            txtTokenNo.Text = "";
            txtDepName.Text = "";
            string strName = txtDoctorName.Text.Trim();

            string[] arrName = strName.Split('~');
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";

            DataSet DS = new DataSet();


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0].Trim();
                txtDoctorName.Text = arrName[1].Trim();
                Criteria += " and HSFM_STAFF_ID='" + arrName[0].Trim() + "' ";
                DS = dbo.retun_doctor_detailss(Criteria);
            }
            else
            {
                Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text + "'";
                DS = dbo.retun_doctor_detailss(Criteria);
            }


            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]).Trim();
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["TokenNo"]).Trim();
                txtDepName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]).Trim();
            }

            if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            {
                GetPatientVisit();
            }




        }




        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {

            string strName = txtCompany.Text;
            string[] arrName = strName.Split('~');

            strCompID = txtCompany.Text.Trim();
            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];
                strCompID = txtCompany.Text.Trim();
            }

        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {

            string strName = txtCompanyName.Text;
            string[] arrName = strName.Split('~');
            strCompID = txtCompany.Text.Trim();

            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];
                strCompID = txtCompany.Text.Trim();
            }


        }


        protected void drpNetworkName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPlanType();
        }

        protected void drpPlanType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GlobalValues.FileDescription.ToUpper() != "ALAMAL")
            {
                BindDeductCoInsAmt();
            }
        }







        protected void btnHCBShow_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (drpDefaultCompany.Items.Count > 0)
                {
                    if (drpDefaultCompany.SelectedItem.Text != "")
                    {
                        BindDeductCoInsGrid();
                        ModalPopupExtender7.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnHCBShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void lnkEmIdShow_Click(object sender, EventArgs e)
        {
            try
            {

                BindEmiretsId();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.lnkEmIdShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtNetworkClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = "1=1 and  HICM_NOT_COVERED='Y' ";
                Criteria += " AND HICM_INS_CAT_NAME='" + txtNetworkClass.Text.Trim() + "'";

                DataSet DS = new DataSet();

                dbo = new dboperations();

                DS = dbo.InsCatMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strMessage = "Selected Network is not covered.";
                    ModalPopupExtender1.Show();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.txtNetworkClass_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void ImgbtnSig1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                ViewState["Signature1"] = strName;
                String DBString = "";
                DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignature('" + DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.ImgbtnSig1_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void lnkViewSig1_Click(object sender, EventArgs e)
        {
            try
            {
                //  Session["PatientID"] = hidActualFileNo.Value;
                imgPatientSig1.ImageUrl = "../DisplaySignaturePatient.aspx?PatientID=" + hidActualFileNo.Value;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignaturePopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.ImgbtnSig1_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }



        protected void AsyncFileUpload3_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                //if (File.Exists(Server.MapPath("../Uploads/" + Session["InsCardFrontPath"].ToString())))
                //{
                //    goto EvEnd;

                //}
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));
                    string strPath = Server.MapPath("../Uploads/");
                    AsyncFileUpload3.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardFrontDefaultPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardFrontDefaultPathT"] = strFileNmae;
                    string strPath1 = Server.MapPath("../Uploads/Temp/");
                    AsyncFileUpload3.SaveAs(strPath1 + strFileNmae);


                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardFrontDefaultPath"] = null;
                }

                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload1_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AsyncFileUpload4_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                //if (File.Exists(Server.MapPath("../Uploads/" + Session["InsCardBackPath"].ToString())))
                //{
                //    goto EvEnd;

                //}

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));
                    string strPath = Server.MapPath("../Uploads/");
                    AsyncFileUpload4.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardBackDefaultPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardBackDefaultPathT"] = strFileNmae;
                    string strPath2 = Server.MapPath("../Uploads/Temp/");
                    AsyncFileUpload4.SaveAs(strPath2 + strFileNmae);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardBackDefaultPath"] = null;
                }
                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload2_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnConfirmYes_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["EIDConfirmResult"] = "Yes";
                ViewState["ConfirmResult"] = "Yes";
                divConfirmMessageClose.Style.Add("display", "none");
                divConfirmMessage.Style.Add("display", "none");
                if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "EmiratesIDLoad")
                {
                    txtFileNo.Text = Convert.ToString(ViewState["EmiratesIDPT_ID"]);
                    BindModifyData();
                    BindEmirateIdDtls();
                }
                else if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "DuplicateNameSave")
                {
                    btnSave_Click(btnSave, new EventArgs());

                }


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnConfirmNo_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["EIDConfirmResult"] = "No";
                ViewState["ConfirmResult"] = "No";

                divConfirmMessageClose.Style.Add("display", "none");
                divConfirmMessage.Style.Add("display", "none");

                if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "EmiratesIDLoad")
                {
                    txtFileNo.Text = Convert.ToString(ViewState["EmiratesIDPT_ID"]);
                    BindModifyData();

                }
                else if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "DuplicateNameSave")
                {


                }



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void imgPlanRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindPlanType();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.imgPlanRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        #endregion
    }
}