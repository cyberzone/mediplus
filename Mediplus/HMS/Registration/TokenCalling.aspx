﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="TokenCalling.aspx.cs" Inherits="Mediplus.HMS.Registration.TokenCalling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
    <script >
        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
              document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

              if (vColor != '') {

                  document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


            }

            document.getElementById("divMessage").style.display = 'block';



        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

        function Validation() {
            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br>";

            if (document.getElementById('<%=txtCurrentToken.ClientID%>').value == "") {
                ErrorMessage += 'Token Number is Empty.' + trNewLineChar;
                IsError = true;
            }

            if (IsError == true) {
                ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }

            return window.confirm('Do you want to Reset Token ?');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: auto; width: 600px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">
            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;font-size:15px;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div >
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">
                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>
                       
                       <span style="padding-left: 5px;">  <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label></span>
                    </div>
                     <span style="float: right;margin-right:20px">

                        <input type="button" id="Button5" class="ButtonStyle gray"    style="font-weight: bold; cursor: pointer;width:70px;" value=" OK " onclick="HideErrorMessage()" />

                    </span>
                     <br />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>
    <div style="padding-left:5px;">
        <input type="hidden" id="hidPermission" runat="server" value="9" />
        <asp:HiddenField ID="hidLocalDate" runat="server" />
        <table>
            <tr>
                <td class="PageHeader">Token Calling
               
                </td>
            </tr>
        </table>
       
      <div style="padding-top: 0px; width: 82%;    border: 1px solid #005c7b; padding: 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td class="lblCaption1" style="padding-left:5px">Status &nbsp;:&nbsp;
                             <asp:DropDownList ID="drpStatus" runat="server" Width="100px" CssClass="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged">
                             </asp:DropDownList>
                            </td>
                             <td class="lblCaption1">Token No &nbsp;:&nbsp;
                             <asp:TextBox ID="txtSrcTokkenNo" runat="server" Width="100px"  CssClass="TextBoxStyle" ></asp:TextBox>
                            </td>
                            <td style="float:right;">
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Width="100px" CssClass="button red small"  OnClick="btnRefresh_Click" />
                            </td>

                        </tr>

                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
         <div style="height:5px;"></div>
        <div style="padding-top: 0px; width: 82%; height: 500px; overflow: auto; border: 1px solid #005c7b; padding-top : 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvTokenList" runat="server" Width="100%" AutoGenerateColumns="false" BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True"  OnRowDataBound="gvTokenList_RowDataBound" >
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" Font-Bold="true"  />
                        
                        <Columns>
                            <asp:TemplateField HeaderText="ACTIONS" Visible="true" HeaderStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSpeaker" runat="server" ToolTip="Token Calling" ImageUrl="~/Images/SpeakerOrange.png" Height="15" Width="15"
                                        OnClick="TokenSelect_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TOKEN NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:Label ID="lblETD_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("ETD_ID") %> '></asp:Label>
                                   
                                    <asp:Label ID="lblToken" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_CURRENT_TOKEN") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="DATE & TIME"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:Label ID="lblTokenDate" CssClass="GridRow" runat="server"  Font-Bold="true"   Text='<%# Bind("ETD_TOKEN_DATEDesc") %> '></asp:Label>&nbsp;
                                   <asp:Label ID="lblTokentIME" CssClass="GridRow" runat="server" Font-Bold="true"   Text='<%# Bind("ETD_TOKEN_TIMEDesc") %> '></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FILE NO"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>


                                    <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_PT_ID") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PT. NAME"  HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>


                                    <asp:Label ID="lblPatientName" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_PT_NAME") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DOCTOR" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblDrID" CssClass="GridRow" runat="server" Font-Bold="true" Visible="false"   Text='<%# Bind("ETD_DR_ID") %>'></asp:Label>
                                    <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_DR_NAME") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ROOM NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:Label ID="Label5" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_DR_ROOM_NO") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:Label ID="lblStatus" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_STATUS") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="GENERATED BY " HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:Label ID="lblGeneratedBy" CssClass="GridRow" runat="server" Font-Bold="true"  Text='<%# Bind("ETD_GENERATED_FROM") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <div style="height:5px;"></div>
        <div style="padding-top: 0px; width: 82%;    border: 1px solid #005c7b; padding : 0px; border-radius: 10px;">
        <table style="width: 100%">
            <tr>
                <td class="lblCaption1" style="padding-left:5px">Token Prefix
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPrefix" runat="server" Width="100px"  CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Token Current
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCurrentToken" runat="server" Width="100px"  CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="float:right;">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnTokenReset" runat="server" Text="Token Reset" Width="100px" CssClass="button red small" OnClick="btnTokenReset_Click" OnClientClick="return Validation();" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        </div> 
    </div>

    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Enabled="true" Interval="10000"></asp:Timer>
</asp:Content>
