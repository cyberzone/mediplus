﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Registration
{
    public partial class AppointmentListDay : System.Web.UI.Page
    {

        public StringBuilder strData = new StringBuilder();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }


            }


        }



        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.CommonMastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                if (strType == "APP_ROOM")
                {
                    drpRoom.DataSource = DS;
                    drpRoom.DataTextField = "HCM_DESC";
                    drpRoom.DataValueField = "HCM_CODE";
                    drpRoom.DataBind();
                }
            }


            if (strType == "APP_ROOM")
            {
                drpRoom.Items.Insert(0, "--- All ---");
                drpRoom.Items[0].Value = "0";
            }


        }

        void GetAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1  ";

            if (drpStatus.SelectedIndex != 0)
            {
                Criteria += " AND HAS_STATUS='" + drpStatus.SelectedValue + "'";
            }
            else
            {
                Criteria += " AND  HAS_STATUS IN('" + drpStatus.Items[1].Value + "','" + drpStatus.Items[2].Value + "','" + drpStatus.Items[3].Value + "','" + drpStatus.Items[4].Value + "' )";
            }
            DS = dbo.AppointmentStatusGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }

        void BindAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            DS = dbo.AppointmentStatusGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                drpStatus.DataSource = DS;
                drpStatus.DataTextField = "HAS_STATUS";
                drpStatus.DataValueField = "HAS_ID";
                drpStatus.DataBind();

                drpAppStatus.DataSource = DS;
                drpAppStatus.DataTextField = "HAS_STATUS";
                drpAppStatus.DataValueField = "HAS_ID";
                drpAppStatus.DataBind();

            }

            drpStatus.Items.Insert(0, "--- All ---");
            drpStatus.Items[0].Value = "0";

            drpAppStatus.Items.Insert(0, "--- Select ---");
            drpAppStatus.Items[0].Value = "0";

        }

        void BindAppointment()
        {


            string Criteria = " 1=1 ";

            //if (drpAppStatus.SelectedIndex != 0)
            //{
            //    Criteria += "  AND HAM_STATUS IN (" + hidStatus.Value + ")";
            //}
            //else
            //{
            //    Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";
            //}
            if (drpStatus.SelectedIndex != 0)
            {
                Criteria += " AND HAM_STATUS='" + drpStatus.SelectedValue + "'";
            }

            if (drpSex.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_SEX ='" + drpSex.SelectedValue + "'";
            }

            if (drpRoom.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_ROOM ='" + drpRoom.SelectedValue + "'";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_DR_CODE ='" + drpDoctor.SelectedValue + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HAM_FILENUMBER='" + txtFileNo.Text + "'";
            }

            if (txtPTName.Text.Trim() != "")
            {
                Criteria += " AND HAM_PT_NAME LIKE '%" + txtPTName.Text + "%'";
            }


            if (txtMobile.Text.Trim() != "")
            {
                Criteria += " AND HAM_MOBILENO='" + txtMobile.Text + "'";
            }

            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) >= '" + strForStartDate + "'";
            }

            string strTotDate = txtToDate.Text.Trim();
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) <= '" + strForToDate + "'";
            }


            //  Criteria += "   AND   HAM_STARTTIME >= '" + strForStartDate + " 00:00:00'";



            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            gvAppointmentData.Visible = false;
            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                gvAppointmentData.Visible = true;
                gvAppointmentData.DataSource = DSAppt;
                gvAppointmentData.DataBind();

            }
            else
            {
                gvAppointmentData.DataBind();
            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }


            drpDoctor.Items.Insert(0, "--- Doctor ---");
            drpDoctor.Items[0].Value = "";

        }

        void Clear()
        {
            txtFileNo.Text = "";
            txtPTName.Text = "";
            txtMobile.Text = "";
            drpSex.SelectedIndex = 0;
            drpRoom.SelectedIndex = 0;

        }

        void ClearPopup()
        {

            lblAppDateTime.Text = "";
            txtAppFileNo.Text = "";
            lblAppPTName.Text = "";

            lblAppService.Text = "";
        

            if (drpAppStatus.Items.Count > 0)
            {
                drpAppStatus.SelectedIndex = 0;
            }

            lblAppDoctor.Text = "";

            lblAppGender.Text = "";
            lblAppCategory.Text = "";
            lblAppPTType.Text = "";
            lblAppRoom.Text = "";
            lblAppRemarks.Text = "";
            ViewState["AppointmentID"] = "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                BindSystemOption();


                ViewState["PatientID"] = Convert.ToString(Request.QueryString["PatientID"]);
                ViewState["DisplayType"] = Convert.ToString(Request.QueryString["DisplayType"]);


                if (Convert.ToString(ViewState["PatientID"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);

                  

                }

                if (Convert.ToString(ViewState["DisplayType"]) == "Previous")
                {
                    txtFromDate.Text = System.DateTime.Now.AddMonths(-3).ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                }

                if (Convert.ToString(ViewState["DisplayType"]) == "Upcoming")
                {
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.AddMonths(3).ToString("dd/MM/yyyy");
                   
                }
                BindDoctor();
                BindAppointmentStatus();
                BindCommonMaster("APP_ROOM");
                //  GetAppointmentStatus();

                BindAppointment();

            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                // GetAppointmentStatus();


                BindAppointment();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentListDay.btnSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                ClearPopup();


                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvAppointmentData.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
                Label lblTimeSt = (Label)gvScanCard.Cells[0].FindControl("lblTimeSt");
                Label lblTimeEnd = (Label)gvScanCard.Cells[0].FindControl("lblTimeEnd");
                Label lblFileNo = (Label)gvScanCard.Cells[0].FindControl("lblFileNo");
                Label lblFName = (Label)gvScanCard.Cells[0].FindControl("lblFName");
                Label lblLName = (Label)gvScanCard.Cells[0].FindControl("lblLName");

                Label lblService = (Label)gvScanCard.Cells[0].FindControl("lblService");
                Label lblAppStatusValue = (Label)gvScanCard.Cells[0].FindControl("lblAppStatusValue");
                Label lblDrName = (Label)gvScanCard.Cells[0].FindControl("lblDrName");

                Label lblGender = (Label)gvScanCard.Cells[0].FindControl("lblGender");
                Label lblCategory = (Label)gvScanCard.Cells[0].FindControl("lblCategory");
                Label lblPTType = (Label)gvScanCard.Cells[0].FindControl("lblPTType");
                Label lblRoom = (Label)gvScanCard.Cells[0].FindControl("lblRoom");

                Label lblRemarks = (Label)gvScanCard.Cells[0].FindControl("lblRemarks");
                Label lblAppointmentID = (Label)gvScanCard.Cells[0].FindControl("lblAppointmentID");


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAppPopup()", true);

                lblAppDateTime.Text = lblDate.Text + " " + lblTimeSt.Text + "-" + lblTimeEnd.Text;
                txtAppFileNo.Text = lblFileNo.Text;
                lblAppPTName.Text = lblFName.Text + " " + lblLName.Text;

                lblAppService.Text = lblService.Text;
                drpAppStatus.SelectedValue = lblAppStatusValue.Text;
                lblAppDoctor.Text = lblDrName.Text;

                lblAppGender.Text = lblGender.Text;
                lblAppCategory.Text = lblCategory.Text;
                lblAppPTType.Text = lblPTType.Text;
                lblAppRoom.Text = lblRoom.Text;
                lblAppRemarks.Text = lblRemarks.Text;

                ViewState["AppointmentID"] = lblAppointmentID.Text;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentListDay.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCon = new CommonBAL();
                string Criteria = "HAM_APPOINTMENTID=" + Convert.ToString(ViewState["AppointmentID"]);
                objCon.fnUpdateTableData("HAM_FILENUMBER='" + txtAppFileNo.Text + "',HAM_STATUS='" + drpAppStatus.SelectedValue +"'", "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria);
                ClearPopup();
                BindAppointment();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAppPopup()", true);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentListDay.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}