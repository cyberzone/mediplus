﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Drawing;
using Mediplus_BAL;
using System.Text;
using System.Xml;
using Mediplus.ShafafiyaService;
using Mediplus.DHASearchIdentity;




namespace Mediplus.HMS.Registration
{
    public partial class PTAndVisitRegistration : System.Web.UI.Page
    {
        # region Variable Declaration

        // string strCon = System.Configuration.ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;

        StringBuilder strProcessTimLogData = new StringBuilder();

        string strDataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        string strDBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        string strDBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        string strDBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();
        string strReportPath = System.Configuration.ConfigurationSettings.AppSettings["REPORT_PATH"].ToString().Trim();



        //string strIdPrinterName = System.Configuration.ConfigurationSettings.AppSettings["IdPrinterName"].ToString().Trim();
        //string strLabelPrinterName = System.Configuration.ConfigurationSettings.AppSettings["LabelPrinterName"].ToString().Trim();

        string ImageSaveToDB = System.Configuration.ConfigurationSettings.AppSettings["ImageSaveToDB"].ToString().Trim();



        static string strTPA = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["TPA"]).ToLower().Trim();
        public string strNetwork = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Network"]).ToLower().Trim();
        public string PTRegChangePolicyNoCaption = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegChangePolicyNoCaption"]).ToLower().Trim();



        // public string strNetworkClass = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["NetworkClass"]).ToLower().Trim();
        //string strPatientRegisterDate = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PatientRegisterDate"]).ToLower().Trim();

        // public string LabelPrint = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["LabelPrint"]).ToLower().Trim();

        // string PTRegDeductCoIns = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegDeductCoIns"]).ToLower().Trim();
        // string PTRegOtherInfo = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegOtherInfo"]).ToLower().Trim();

        //public string EmiratesIdMandatory="0";// = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["EmiratesIdMandatory"]).ToLower().Trim();
        //public string NationalityMandatory="0";// = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegNationalityMand"]).ToLower().Trim();
        //public string REQ_AGE="0";


        dboperations dbo = new dboperations();

        public static string strDrName = "";
        public static string strCompName = "";
        public static string strCompID = "";

        public string strRemainderRecept = "";
        Int32 intImageSize;

        static string stParentName = "0";
        static string strSessionBranchId;
        static string stParentName1 = "0";

        public string strMessage = "", strReminderMessage = "", strHoldMessage = "";

        public string strLabel = "", strToken = "";

        StringBuilder strPageTimeLog = new StringBuilder();
        string strNewLineChar = "\r\n";



        #endregion

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ProcessTimeLog(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');
                string UserName = Convert.ToString(Session["User_ID"]);
                string strFileName = Server.MapPath("../../Log/ProcessTimeLog_" + UserName + "_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("PatientWaitingList " + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
                strPageTimeLog.Clear();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDeductibleType()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetDeductibleType(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDedType.DataSource = ds;
                drpDedType.DataValueField = "Code";
                drpDedType.DataTextField = "Name";
                drpDedType.DataBind();
            }


        }

        void BindCoInsuranceType()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetCoInsuranceType(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCoInsType.DataSource = ds;
                drpCoInsType.DataValueField = "Code";
                drpCoInsType.DataTextField = "Name";
                drpCoInsType.DataBind();


                drpCoInsDental.DataSource = ds;
                drpCoInsDental.DataValueField = "Code";
                drpCoInsDental.DataTextField = "Name";
                drpCoInsDental.DataBind();


                drpCoInsPharmacy.DataSource = ds;
                drpCoInsPharmacy.DataValueField = "Code";
                drpCoInsPharmacy.DataTextField = "Name";
                drpCoInsPharmacy.DataBind();


                drpCoInsLab.DataSource = ds;
                drpCoInsLab.DataValueField = "Code";
                drpCoInsLab.DataTextField = "Name";
                drpCoInsLab.DataBind();


                drpCoInsRad.DataSource = ds;
                drpCoInsRad.DataValueField = "Code";
                drpCoInsRad.DataTextField = "Name";
                drpCoInsRad.DataBind();


            }


        }

        void BindPatientTitle()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetPatientTitle(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpTitle.DataSource = ds;
                drpTitle.DataValueField = "Code";
                drpTitle.DataTextField = "Name";
                drpTitle.DataBind();
            }

            drpTitle.Items.Insert(0, "");
            drpTitle.Items[0].Value = "";

        }

        void BindConsentForm()
        {
            DataSet ds = new DataSet();
            CommonBAL objComm = new CommonBAL();
            string Criteria = " 1=1 ";
            ds = objComm.GetConsentForm(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpConsentForm.DataSource = ds;
                drpConsentForm.DataValueField = "Code";
                drpConsentForm.DataTextField = "Name";
                drpConsentForm.DataBind();

                drpVisitConsentForm.DataSource = ds;
                drpVisitConsentForm.DataValueField = "Code";
                drpVisitConsentForm.DataTextField = "Name";
                drpVisitConsentForm.DataBind();

            }

            drpVisitConsentForm.Items.Insert(0, "--- Select ---");
            drpVisitConsentForm.Items[0].Value = "";

        }

        void AgeCalculation()
        {

            string dob = txtDOB.Text.Trim();
            txtAge.Text = "0";
            string[] arrDOB = dob.Split('/');
            var currentyear = Year.Value;

            string TodayDate = hidTodayDate.Value;
            string[] arrTodayDate1 = TodayDate.Split('/');


            if (arrDOB.Length == 3 && dob.Length == 10)
            {

                if (currentyear != arrDOB[2])
                {
                    Int32 age = Convert.ToInt32(currentyear) - Convert.ToInt32(arrDOB[2]);
                    Int32 Month;

                    if (Convert.ToInt32(arrTodayDate1[1]) >= Convert.ToInt32(arrDOB[1]))
                    {
                        Month = Convert.ToInt32(arrTodayDate1[1]) - Convert.ToInt32(arrDOB[1]);
                    }

                    else
                    {
                        age = age - 1;
                        Int32 diff = 12 - Convert.ToInt32(arrDOB[1]);
                        Month = Convert.ToInt32(arrTodayDate1[1]) + Convert.ToInt32(diff);
                    }

                    txtAge.Text = Convert.ToString(age);
                    txtMonth.Text = Convert.ToString(Month);

                }
                else
                {

                    txtAge.Text = "0";

                    DateTime dtLastVisit = DateTime.ParseExact(txtDOB.Text.Trim() + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);
                    DateTime Today = DateTime.ParseExact(hidTodayDate.Value + " 00:00:00", "dd/MM/yyyy HH:mm:ss", null);


                    TimeSpan Diff = Today.Date - dtLastVisit.Date;

                    int Days = Diff.Days;

                    Int32 Month;

                    if (Convert.ToInt32(arrTodayDate1[1]) >= Convert.ToInt32(arrDOB[1]))
                    {
                        Month = Convert.ToInt32(arrTodayDate1[1]) - Convert.ToInt32(arrDOB[1]);

                        if (Month == 1 && Days <= 30)
                        {
                            txtAge.Text = Convert.ToString(Days); ;
                            drpAgeType.SelectedValue = "D";
                            txtMonth.Text = "";
                        }
                        else
                        {
                            txtMonth.Text = Convert.ToString(Month);
                        }
                    }

                    else
                    {
                        Month = Convert.ToInt32(arrTodayDate1[1]) + 11 - Convert.ToInt32(arrDOB[1]);
                        txtMonth.Text = Convert.ToString(Month);
                    }



                }
            }
            else
            {

                txtAge.Text = "0";
            }


        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

           // Criteria += " AND SCREENNAME='PTREG' "; Changes done for jira task 49 by naveen

            Criteria += " AND SCREENNAME IN ('PTREG','ECLAIM')";

            DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);
            
            ViewState["EMIRATES_ID"] = "0";
            ViewState["NationalityMandatory"] = "0";
            ViewState["CountryMandatory"] = "0";
            ViewState["AgeMandatory"] = "0";
            ViewState["DeductibleMandatory"] = "0";
            ViewState["LABEL_PRINT"] = "1";
            ViewState["DATABASE_DATE"] = "1";
            ViewState["DEP_WISE_VISIT_TYPE"] = "0";
            ViewState["PT_SIGNATURE"] = "0";
            ViewState["OTHER_INFO_DISPLAY"] = "0";
            ViewState["PT_COMP_ADD"] = "0";
            ViewState["PT_TITLE_NAME"] = "0";
            ViewState["PLAN_TYPE_STORE_SCAN_CARD"] = "0";
            ViewState["APPOINTMENT_ALERT"] = "0";
            ViewState["CityMandatory"] = "0";
            ViewState["VisaTypeMandatory"] = "0";

            ViewState["REQ_POLICY_NO"] = "1";
            ViewState["REQ_CARD_NO"] = "0";
            ViewState["REQ_PT_COMPANY"] = "0";
            ViewState["REQ_ELIGIBILITY_ID"] = "0";
            ViewState["REQ_EMAIL_ID"] = "0";

            ViewState["REQ_MARITAL_STATUS"] = "0";
            ViewState["REQ_RELIGION"] = "0";
            ViewState["REQ_ADDRESS"] = "0";
            ViewState["REQ_EMIRATES"] = "0";

            ViewState["REQ_KNOW_FROM"] = "0";

            ViewState["CUST_ENQUIRY_DISPLAY"] = "0";

            ViewState["REQ_EID_DUPLICATE_CHECK"] = "0";
            ViewState["REQ_PT_ZERO_BALANCE_DUE"] = "0";

            ViewState["PT_DTLS_REPLACE_FROM_EID_DATA"] = "0";
            ViewState["EID_TYPE"] = "WEB";
            ViewState["ECLAIM"] = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "EMIRATES_ID" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_EMIRATES_ID")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMIRATES_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "NATIONALITY" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_NATIONALITY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NationalityMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "COUNTRY" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_COUNTRY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CountryMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }
                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "AGE" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_AGE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["AgeMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DEDUCTIBLE" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_DEDUCTIBLE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DeductibleMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }



                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "LABEL_PRINT")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["LABEL_PRINT"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DATABASE_DATE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DATABASE_DATE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "DEP_WISE_VISIT_TYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEP_WISE_VISIT_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_SIGNATURE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_SIGNATURE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "SIGNATURE_PAD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            hidSignaturePad.Value = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "OTHER_INFO_DISPLAY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OTHER_INFO_DISPLAY"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_COMP_ADD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_COMP_ADD"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_TITLE_NAME")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_TITLE_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PLAN_TYPE_STORE_SCAN_CARD")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PLAN_TYPE_STORE_SCAN_CARD"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APPOINTMENT_ALERT")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["APPOINTMENT_ALERT"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CITY" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_CITY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CityMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }





                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "VISATYPE" || Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_VISATYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["VisaTypeMandatory"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }





                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CREDITDTLS_CLEAR")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CREDITDTLS_CLEAR"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }



                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_POLICY_NO")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_POLICY_NO"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }



                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_CARD_NO")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_CARD_NO"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_PT_COMPANY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_PT_COMPANY"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_ELIGIBILITY_ID")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_ELIGIBILITY_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_EMAIL_ID")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_EMAIL_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "CUST_ENQUIRY_DISPLAY")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CUST_ENQUIRY_DISPLAY"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_MARITAL_STATUS")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_MARITAL_STATUS"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_RELIGION")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_RELIGION"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_ADDRESS")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_ADDRESS"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_EMIRATES")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_EMIRATES"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_DEDUCTIBLE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_DEDUCTIBLE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_KNOW_FROM")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_KNOW_FROM"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_EID_DUPLICATE_CHECK")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_EID_DUPLICATE_CHECK"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "REQ_PT_ZERO_BALANCE_DUE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["REQ_PT_ZERO_BALANCE_DUE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "PT_DTLS_REPLACE_FROM_EID_DATA")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_DTLS_REPLACE_FROM_EID_DATA"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "EID_TYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EID_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "ECLAIM_TYPE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["ECLAIM"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                }
            }


        }

        void BindPatientVisitLabel()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HPV_STATUS <>'D' ";


            Criteria += " AND HPV_PT_ID='" + hidActualFileNo.Value + "'";

            //if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            //{
            //    if (txtDepName.Text != "")
            //    {
            //        Criteria += " AND HPV_DEP_NAME='" + txtDepName.Text.Trim() + "'";
            //    }
            //}

            DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" HPV_SEQNO,convert(varchar,HPV_Date,103) as HPV_DateDesc,HPV_DR_ID,HPV_DR_NAME ", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");


          //  DS = dbo.PatientVisitGetForReg(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                hidPTVisitSQNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);

                int intCount = DS.Tables[0].Rows.Count;

                if (DS.Tables[0].Rows[0].IsNull("HPV_DATEDesc") == false)
                {
                    lblLastVisit.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATEDesc"]);//.Remove(10,12);
                }
                lblPrevDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                lblPrevDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);


                if (DS.Tables[0].Rows.Count > 1)
                {
                    if (DS.Tables[0].Rows[1].IsNull("HPV_DATEDesc") == false)
                    {
                        lblPrevVisit.Text = Convert.ToString(DS.Tables[0].Rows[1]["HPV_DATEDesc"]);//.Remove(10,12);
                    }
                }
                else
                {
                    lblPrevVisit.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATEDesc"]);//.Remove(10,12);
                }

                if (DS.Tables[0].Rows[intCount - 1].IsNull("HPV_DATEDesc") == false)
                {
                    lblFirstVisit.Text = Convert.ToString(DS.Tables[0].Rows[intCount - 1]["HPV_DATEDesc"]);//.Remove(10,12);
                }

            }
        }

        void BindPackageServices()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " HSM_STATUS='A' AND HSM_PACKAGE_SERVICE IS NOT NULL  AND HSM_PACKAGE_SERVICE <>''";
            DS = objCom.fnGetFieldValue(" top 100 HSM_SERV_ID,HSM_NAME", "HMS_SERVICE_MASTER", Criteria, "HSM_NAME");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpPTCategory.DataSource = DS;
                drpPTCategory.DataTextField = "HSM_NAME";
                drpPTCategory.DataValueField = "HSM_SERV_ID";
                drpPTCategory.DataBind();

            }
            drpPTCategory.Items.Insert(0, "--- Select ---");
            drpPTCategory.Items[0].Value = "";

        }

        void GetPatientVisit()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HPV_STATUS <>'D'  ";

            // Criteria += " AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPV_PT_ID='" + hidActualFileNo.Value + "'";



            if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            {
                if (txtDepName.Text != "")
                {
                    Criteria += " AND HPV_DEP_NAME='" + txtDepName.Text.Trim() + "'";
                }
            }
            else
            {
                if (txtDoctorID.Text.Trim() != "")
                {
                    Criteria += " AND HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";
                }
            }
            //VISIT DATE GRATER THAN 3 YEARS TREAT AS NEW PATIENT
            Criteria += " AND  DATEADD(YY,-3, CONVERT(DATETIME,GETDATE(),101)) <= CONVERT(DATETIME, HPV_DATE,101) ";
            DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" HPV_DATE,DATEDIFF(mi,HPV_DATE,getdate()) as LastVisitedMinutes,HPV_DR_ID ", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE desc");
            //   DS = dbo.PatientVisitGetForReg(Criteria);

            lblVisitType.Text = "New";
            lblPTVisitType.Text = "New";
            if (DS.Tables[0].Rows.Count > 0)
            {
                int intCount = DS.Tables[0].Rows.Count;
                string strLastVisit = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATE"]);

                hidLatestVisitHours.Value = Convert.ToString(DS.Tables[0].Rows[0]["LastVisitedMinutes"]); //LastVisitedHours
                hidLastVisitDrId.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);

                int intRevisit = Convert.ToInt32(Session["RevisitDays"]);
                if (strLastVisit != "")
                {

                    DateTime dtLastVisit = Convert.ToDateTime(strLastVisit);
                    DateTime Today;

                    Today = Convert.ToDateTime(hidTodayDBDate.Value);


                    TimeSpan Diff = Today.Date - dtLastVisit.Date;

                    if (Diff.Days <= intRevisit)
                    {
                        lblVisitType.Text = "Revisit";
                        lblPTVisitType.Text = "Revisit";
                    }
                    else if (Diff.Days >= intRevisit)
                    {
                        lblVisitType.Text = "Old";
                        lblPTVisitType.Text = "Old";
                    }

                }
            }

        }

        void GetPatientVisitHistory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HPV_STATUS <>'D'  ";
            Criteria += " AND HPV_PT_ID='" + hidActualFileNo.Value + "'";



            DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" HPV_SEQNO,HPV_DR_ID,HPV_EMR_ID,HPV_Date,HPV_PT_ID,convert(varchar,HPV_Date,103) as HPV_DateDesc,HPV_TIME   "+
                ",HPV_DR_NAME,HPV_DEP_NAME,HPV_DR_TOKEN,(SELECT CASE HPV_PT_TYPE WHEN 'CR' THEN 'Credit' when 'CA' THEN 'Cash'  ELSE HPV_PT_TYPE END ) AS HPV_PT_TYPEDesc "+
                ",HPV_COMP_ID,HPV_COMP_NAME", "HMS_PATIENT_VISIT", Criteria, "HPV_Date Desc");
            //DS = dbo.PatientVisitGetForReg(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvVisitHistory.DataSource = DS;
                gvVisitHistory.DataBind();
            }

        }

        void BindDepartment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            ds = dbo.DepartmentGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HD_INITIALNO_ID";
                drpDepartment.DataTextField = "HD_DESC";
                drpDepartment.DataBind();



            }

            ds.Clear();
            ds.Dispose();
        }


        void BindCity()
        {
            DataSet ds = new DataSet();
            ds = dbo.city();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCity.DataSource = ds;
                drpCity.DataValueField = "HCIM_NAME";
                drpCity.DataTextField = "HCIM_NAME";
                drpCity.DataBind();
            }
            drpCity.Items.Insert(0, "--- Select ---");
            drpCity.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindCountry()
        {
            DataSet ds = new DataSet();
            ds = dbo.country();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCountry.DataSource = ds;
                drpCountry.DataValueField = "hcm_name";
                drpCountry.DataTextField = "hcm_name";
                drpCountry.DataBind();

            }
            drpCountry.Items.Insert(0, "--- Select ---");
            drpCountry.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindArea()
        {
            DataSet ds = new DataSet();
            ds = dbo.area();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpArea.DataSource = ds;
                drpArea.DataValueField = "ham_area_name";
                drpArea.DataTextField = "ham_area_name";
                drpArea.DataBind();
            }

            drpArea.Items.Insert(0, "--- Select ---");
            drpArea.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindNationality()
        {
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "HNM_NAME";
                drpNationality.DataTextField = "HNM_NAME";
                drpNationality.DataBind();

                drpSubsNationality.DataSource = ds;
                drpSubsNationality.DataValueField = "HNM_NAME";
                drpSubsNationality.DataTextField = "HNM_NAME";
                drpSubsNationality.DataBind();


            }
            drpNationality.Items.Insert(0, "--- Select ---");
            drpNationality.Items[0].Value = "0";

            drpSubsNationality.Items.Insert(0, "--- Select ---");
            drpSubsNationality.Items[0].Value = "";

            ds.Clear();
            ds.Dispose();

        }

        void BindIDCaptionMaster()
        {
            DataSet ds = new DataSet();
            ds = dbo.IDCaptionMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpIDCaption.DataSource = ds;
                drpIDCaption.DataValueField = "HIC_ID_CAPTION";
                drpIDCaption.DataTextField = "HIC_Name_CAPTION";
                drpIDCaption.DataBind();
            }
            ds.Clear();
            ds.Dispose();

        }


        void BindPayer()
        {
            string Criteria = " 1=1  and HPC_STATUS='A' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.PatientCompanyGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpPayer.DataSource = ds;
                drpPayer.DataValueField = "HPC_PATIENT_COMPANY";
                drpPayer.DataTextField = "HPC_PATIENT_COMPANY";
                drpPayer.DataBind();
            }

            drpPayer.Items.Insert(0, "--- Select ---");
            drpPayer.Items[0].Value = "0";
        }



        void BindNetworkName()
        {
            //string Criteria = " 1=1 ";
            //Criteria += " AND HICM_STATUS = 'A' AND HICM_PACKAGE='Y'";
            //Criteria += " AND hicm_ins_Cat_id IN ( select hcb_cat_type from HMS_COMP_BENEFITS where HCB_COMP_ID='" + drpDefaultCompany.SelectedValue + "')";

            //drpNetworkName.Items.Clear();
            //DataSet ds = new DataSet();
            //ds = dbo.InsCatMasterGet(Criteria);
            //if (ds.Tables[0].Rows.Count > 0)
            //{

            //    drpNetworkName.DataSource = ds;
            //    drpNetworkName.DataValueField = "HICM_INS_CAT_ID";
            //    drpNetworkName.DataTextField = "HICM_INS_CAT_NAME";
            //    drpNetworkName.DataBind();
            //}
            //drpNetworkName.Items.Insert(0, "--- Select ---");
            //drpNetworkName.Items[0].Value = "0";
            //ds.Clear();
            //ds.Dispose();

            DataSet Ds1 = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
            Ds1 = dbo.retun_inccmp_details(Criteria);

            drpNetworkName.Items.Clear();

            if (drpNetworkName.Items.Count == 0)
            {
                drpNetworkName.Items.Insert(0, "--- Select ---");
                drpNetworkName.Items[0].Value = "0";
            }

            if (Ds1.Tables[0].Rows.Count > 0)
            {
                string strPackage = Convert.ToString(Ds1.Tables[0].Rows[0]["HCM_PACKAGE_DETAILS"]);
                string[] arrPackage = strPackage.Split('|');




                for (int j = 0; j <= arrPackage.Length - 1; j++)
                {

                    drpNetworkName.Items.Insert(j + 1, arrPackage[j]);
                    drpNetworkName.Items[j + 1].Value = arrPackage[j];

                }
            }


        }

        void CompanyParentGet()
        {

            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND HCM_COMP_ID =  HCM_BILL_CODE  ";

            if (drpPatientType.SelectedValue == "CR")
            {
                Criteria += " AND HCM_COMP_TYPE ='I' ";
            }
            else if (drpPatientType.SelectedValue == "CU")
            {
                Criteria += " AND HCM_COMP_TYPE ='N' ";
            }
            else
            {
                Criteria += " AND HCM_COMP_TYPE ='P' ";
            }
            //Criteria += " AND HCM_COMP_ID in (select HCM_BILL_CODE from HMS_COMPANY_MASTER where  HCM_COMP_ID !=  HCM_BILL_CODE ) ";
            drpParentCompany.Items.Clear();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpParentCompany.DataSource = ds;
                drpParentCompany.DataValueField = "HCM_COMP_ID";
                drpParentCompany.DataTextField = "HCM_NAME";
                drpParentCompany.DataBind();



            }
            drpParentCompany.Items.Insert(0, "All");
            drpParentCompany.Items[0].Value = "0";





            drpParentCompany.SelectedIndex = 0;


            ds.Clear();
            ds.Dispose();

        }


        void BindDefaultCompany()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (drpParentCompany.SelectedIndex != 0)
            {
                Criteria += " AND  (  HCM_COMP_ID !=  HCM_BILL_CODE  AND    HCM_BILL_CODE ='" + drpParentCompany.SelectedValue + "'    OR  HCM_COMP_ID ='" + drpParentCompany.SelectedValue + "' )";
            }
            if (drpPatientType.SelectedValue == "CR")
            {
                Criteria += " AND HCM_COMP_TYPE ='I' ";
            }
            else if (drpPatientType.SelectedValue == "CU")
            {
                Criteria += " AND HCM_COMP_TYPE ='N' ";
            }

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            drpDefaultCompany.Items.Clear();
            drpNetworkName.Items.Clear();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDefaultCompany.DataSource = ds;
                drpDefaultCompany.DataValueField = "HCM_COMP_ID";
                drpDefaultCompany.DataTextField = "HCM_NAME";
                drpDefaultCompany.DataBind();
            }


            drpDefaultCompany.Items.Insert(0, "--- Select ---");
            drpDefaultCompany.Items[0].Value = "0";
        }

        void BindCompanyDetails(out string strRemainderRecept)
        {
            strRemainderRecept = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
            DataSet ds = new DataSet();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HCM_REM_RCPTN") == false)
                    strRemainderRecept = Convert.ToString(ds.Tables[0].Rows[0]["HCM_REM_RCPTN"]);




            }


        }

        void BinScanCardMasterGet()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HSM_Card_Type = '" + drpCardType.SelectedValue + "'";

            DataSet ds = new DataSet();
            ds = dbo.ScanCardMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCardName.DataSource = ds;
                drpCardName.DataValueField = "HSM_Card_Name";
                drpCardName.DataTextField = "HSM_Card_Name";
                drpCardName.DataBind();
            }


            ds.Clear();
            ds.Dispose();

        }

        string ScanCardTypeGet(string CardName)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HSM_Card_Name = '" + CardName + "'";

            DataSet ds = new DataSet();
            ds = dbo.ScanCardMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0]["HSM_Card_Type"]);
            }

            return "";

        }


        void BindPTVisitParentGet()
        {

            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND HCM_COMP_ID =  HCM_BILL_CODE  ";

            if (drpPTVisitType.SelectedValue == "CR")
            {
                Criteria += " AND HCM_COMP_TYPE ='I' ";
            }
            else if (drpPTVisitType.SelectedValue == "CU")
            {
                Criteria += " AND HCM_COMP_TYPE ='N' ";
            }
            else
            {
                Criteria += " AND HCM_COMP_TYPE ='P' ";
            }
            //Criteria += " AND HCM_COMP_ID in (select HCM_BILL_CODE from HMS_COMPANY_MASTER where  HCM_COMP_ID !=  HCM_BILL_CODE ) ";
            drpPTVisitParentCompany.Items.Clear();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {




                drpPTVisitParentCompany.DataSource = ds;
                drpPTVisitParentCompany.DataValueField = "HCM_COMP_ID";
                drpPTVisitParentCompany.DataTextField = "HCM_NAME";
                drpPTVisitParentCompany.DataBind();

            }


            drpPTVisitParentCompany.Items.Insert(0, "All");
            drpPTVisitParentCompany.Items[0].Value = "0";




            drpPTVisitParentCompany.SelectedIndex = 0;

            ds.Clear();
            ds.Dispose();

        }

        void BindPTVisitCompany()
        {

            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            if (drpPTVisitParentCompany.SelectedIndex != 0)
            {
                Criteria += " AND  (  HCM_COMP_ID !=  HCM_BILL_CODE  AND    HCM_BILL_CODE ='" + drpPTVisitParentCompany.SelectedValue + "'    OR  HCM_COMP_ID ='" + drpPTVisitParentCompany.SelectedValue + "' )";
            }

            if (drpPTVisitType.SelectedValue == "CR")
            {
                Criteria += " AND HCM_COMP_TYPE ='I' ";
            }
            else if (drpPatientType.SelectedValue == "CU")
            {
                Criteria += " AND HCM_COMP_TYPE ='N' ";
            }




            drpPTVisitCompany.Items.Clear();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpPTVisitCompany.DataSource = ds;
                drpPTVisitCompany.DataValueField = "HCM_COMP_ID";
                drpPTVisitCompany.DataTextField = "HCM_NAME";
                drpPTVisitCompany.DataBind();
            }
            drpPTVisitCompany.Items.Insert(0, "--- Select ---");
            drpPTVisitCompany.Items[0].Value = "";


            ds.Clear();
            ds.Dispose();

        }

        void BindPTVisitCompanyPlan()
        {
            drpPTVisitCompanyPlan.Items.Clear();

            if (drpPTVisitCompany.SelectedIndex != 0)
            {
                DataSet DS = new DataSet();
                string Criteria = " 1=1 ";
                Criteria += " AND HCB_COMP_ID = '" + drpPTVisitCompany.SelectedValue + "'";
                DS = dbo.CompBenefitsGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    drpPTVisitCompanyPlan.DataSource = DS;
                    drpPTVisitCompanyPlan.DataTextField = "HCB_CAT_TYPE";
                    drpPTVisitCompanyPlan.DataValueField = "HCB_CAT_TYPE";
                    drpPTVisitCompanyPlan.DataBind();
                }


            }

            drpPTVisitCompanyPlan.Items.Insert(0, "--- Select ---");
            drpPTVisitCompanyPlan.Items[0].Value = "";

        }


        decimal fnCalculatePTAmount(string AmountType)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HPB_PT_ID='" + txtFileNo.Text + "' ";
            switch (AmountType)
            {
                case "INVOICE":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "RETURN":
                    Criteria += " AND HPB_TRANS_TYPE='INV_RTN' ";
                    break;
                case "PAIDAMT":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "RETURNPAIDAMT":
                    Criteria += " AND HPB_TRANS_TYPE='INV_RTN' ";
                    break;
                case "REJECTED":
                    Criteria += " AND HPB_TRANS_TYPE='INVOICE' ";
                    break;
                case "ADVANCE":
                    Criteria += " AND HPB_TRANS_TYPE='ADVANCE' ";
                    break;
                case "NPPAID":

                    break;
                default:
                    break;
            }
            DataSet DS = new DataSet();
            DS = dbo.GetPatientBalHisTotal(Criteria);

            decimal Amt = 0;
            if (DS.Tables[0].Rows.Count > 0)
            {
                switch (AmountType)
                {
                    case "INVOICE":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalInvoiceAmout"]);
                        break;
                    case "RETURN":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalInvoiceAmout"]);
                        break;
                    case "PAIDAMT":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "RETURNPAIDAMT":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "REJECTED":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalRejectAmount"]);
                        break;
                    case "ADVANCE":
                        Amt = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"]);
                        break;
                    case "NPPAID":

                        break;
                    default:
                        break;
                }
            }

            return Amt;
        }


        void BindToken()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();

            Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text.Trim() + "'";
            DS = dbo.retun_doctor_detailss(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["TokenNo"]).Trim();
            }

        }



        void BindPriority()
        {
            string Criteria = " 1=1 ";
            DataSet ds = new DataSet();
            ds = dbo.PriorityGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                drpPriority.DataSource = ds;
                drpPriority.DataValueField = "HVP_SLNO";
                drpPriority.DataTextField = "HVP_DESC";
                drpPriority.DataBind();

            }
            drpPriority.Items.Insert(0, "--- Select ---");
            drpPriority.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindKnowFrom()
        {
            string Criteria = " 1=1 ";
            DataSet ds = new DataSet();
            ds = dbo.KnowFromGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                drpKnowFrom.DataSource = ds;
                drpKnowFrom.DataValueField = "Code";
                drpKnowFrom.DataTextField = "Description";
                drpKnowFrom.DataBind();

            }
            drpKnowFrom.Items.Insert(0, "--- Select ---");
            drpKnowFrom.Items[0].Value = "";

            ds.Clear();
            ds.Dispose();

        }


        void BindLastCunsultDtls()
        {


            DataSet ds = new DataSet();
            ds = dbo.GetLastCunsultDtls(hidActualFileNo.Value);
            divCunsult.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                divCunsult.Visible = true;
                lblLastCunsultDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HIM_DATE"]);
                lblLastCunsultAmount.Text = Convert.ToString(ds.Tables[0].Rows[0]["HIT_AMOUNT"]);

            }

            ds.Clear();
            ds.Dispose();

        }

        string BindStaffList()
        {
            string strDeptName = "";

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvStaffList.DataSource = ds;
                gvStaffList.DataBind();
            }

            return strDeptName;
        }
        string GetDrDeptName(string DrID)
        {
            string strDeptName = "";

            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_STAFF_ID='" + DrID + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strDeptName = Convert.ToString(ds.Tables[0].Rows[0]["HSFM_DEPT_ID"]);
            }

            return strDeptName;
        }

        Boolean GetAppointment()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HAM_FILENUMBER='" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND  convert(varchar(10),HAM_STARTTIME,103)  =   convert(varchar(10),getdate(),103)  ";
            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                strMessage += " This patient have an appointment in ";

                for (int i = 0; i < DSAppt.Tables[0].Rows.Count; i++)
                {
                    string strDeptName = "";

                    strDeptName = GetDrDeptName(Convert.ToString(DSAppt.Tables[0].Rows[i]["HAM_DR_CODE"]));

                    strMessage += strDeptName + "  at " + Convert.ToString(DSAppt.Tables[0].Rows[i]["AppintmentSTTime"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[i]["AppintmentFITime"]) + ",";

                }
                return true;
            }

            return false;
        }

        void UpdateAppointmentStatus()
        {
            //SAVE APPOINTMENT STATUS AS WAITING
            string Criteria = " 1=1 ";
            Criteria += " AND HAM_FILENUMBER ='" + hidActualFileNo.Value + "'";
            Criteria += " AND HAM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";

            Criteria += " AND  convert(varchar(10),HAM_STARTTIME,103)  =   convert(varchar(10),getdate(),103)  ";

            CommonBAL objCom = new CommonBAL();

            objCom.fnUpdateTableData("HAM_STATUS=3", "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria);

        }

        void BindModifyData()
        {

            //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 1" + strNewLineChar);
            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 1" + strNewLineChar);

            Boolean isEFilling;
            if (GlobalValues.FileDescription.ToUpper() != "ALNOOR" && GlobalValues.FileDescription.ToUpper() != "ALTAIF")
            {
                isEFilling = dbo.CheckEFilling(Convert.ToString(Session["ScanSave"]), txtFileNo.Text);

                if (isEFilling == true)
                {
                    lblEFilling.Text = "E-Filling- Found";
                }
                else
                {
                    lblEFilling.Text = "E-Filling- Not Found";
                }
            }

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 2" + strNewLineChar);


            //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindModifyData() Start");

            if (ds.Tables[0].Rows.Count > 0)
            {
                BindModifyData(ds);


                if (txtDOB.Text.Trim() != "")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                    AgeCalculation();
                }

                if (Convert.ToString(ViewState["APPOINTMENT_ALERT"]) == "1")
                {
                    if (GetAppointment() == true)
                    {
                        //  ModalPopupExtender1.Show();
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' APPOINTMENT ',' " + strMessage + "','Brown')", true);

                    }
                }
            }
            else
            {
                Clear();
            }
        }


        void BindModifyData(DataSet ds)
        {

            string strPolicyType = "";
            if (ds.Tables[0].Rows.Count > 0)
            {

                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 3" + strNewLineChar);



                ViewState["Patient_ID_VALID"] = "1";
                hidActualFileNo.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]);
                hidTodayDBDate.Value = Convert.ToString(ds.Tables[0].Rows[0]["TodayDBDate"]);
                hidLatestVisit.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LATESTVISIT"]);
                hidLatestVisitHours.Value = Convert.ToString(ds.Tables[0].Rows[0]["LastVisitedHours"]);
                hidLastVisitDrId.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);




                txtPTVisitFileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]);
                txtPTVisitFullName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    txtPTVisitMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]).Trim();
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "0")
                {
                    txtPTVisitGender.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                }






                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_STATUS"]) == "A")
                {
                    chkStatus.Checked = true;
                }
                else
                {
                    chkStatus.Checked = false;
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_TITLE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]) != "0")
                {
                    for (int intCount = 0; intCount < drpTitle.Items.Count; intCount++)
                    {
                        if (drpTitle.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]))
                        {
                            drpTitle.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TITLE"]);
                            goto ForTitle;
                        }

                    }
                }
            ForTitle: ;

                if (Convert.ToString(ViewState["LoadFromEmiratesID"]) != "1")
                {

                    txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]).Trim();
                    txtMName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_MNAME"]).Trim();
                    txtLName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_LNAME"]).Trim();


                }

                if (txtPTCompany.Text.Trim() == "")
                {
                    txtPTCompany.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_BLOOD_GROUP") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]) != "0")
                {
                    for (int intCount = 0; intCount < drpBlood.Items.Count; intCount++)
                    {
                        if (drpBlood.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]))
                        {
                            drpBlood.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_BLOOD_GROUP"]);
                            goto ForBoold;
                        }

                    }
                }
            ForBoold: ;

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPatientType.Items.Count; intCount++)
                    {
                        if (drpPatientType.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]))
                        {
                            drpPatientType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]);
                            drpPTVisitType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]);

                            goto ForPTType;
                        }

                    }
                }

            ForPTType: ;
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 4" + strNewLineChar);

                drpPatientType_SelectedIndexChanged(drpPatientType, new EventArgs());

                drpPTVisitType_SelectedIndexChanged(drpPTVisitType, new EventArgs());

                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 5" + strNewLineChar);



                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPayer.Items.Count; intCount++)
                    {
                        if (drpPayer.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]))
                        {
                            drpPayer.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                            goto ForPayer;
                        }

                    }
                }

            ForPayer: ;
                string strInsCompId = "", strNetworkName = "";
                if (ds.Tables[0].Rows[0].IsNull("HPM_BILL_CODE") == false)
                {


                    if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_BILL_CODE"]) != Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]) && strNetwork == "true")
                    {
                        strInsCompId = Convert.ToString(ds.Tables[0].Rows[0]["HPM_BILL_CODE"]);
                        stParentName1 = Convert.ToString(ds.Tables[0].Rows[0]["HPM_BILL_CODE"]);
                        strNetworkName = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                    }
                    else
                    {
                        strInsCompId = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                        stParentName1 = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                    }


                }


                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 6" + strNewLineChar);



                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtPolicyId.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAMILY_ID"]);
                strPolicyType = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);

                txtPTVisitPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtPTVisitCardNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_NO"]);


                if (ds.Tables[0].Rows[0].IsNull("HPM_PACKAGE_NAME") == false)
                {
                    txtNetworkClass.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PACKAGE_NAME"]);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_CONT_EXPDATEDesc") == false)
                {

                    txtExpiryDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]);
                    txtPTVisitCardExpDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]);


                }
                else
                {
                    txtExpiryDate.Text = "";

                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_STARTDesc") == false)
                {

                    txtPolicyStart.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_STARTDesc"]);



                }
                else
                {
                    txtPolicyStart.Text = "";

                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_AREA") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]) != "0")
                {
                    for (int intAreaCount = 0; intAreaCount < drpArea.Items.Count; intAreaCount++)
                    {
                        if (drpArea.Items[intAreaCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]))
                        {
                            drpArea.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AREA"]);
                            goto ForArea;
                        }

                    }
                }
            ForArea: ;

                if (Convert.ToString(ViewState["LoadFromEmiratesID"]) != "1")
                {
                    txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ADDR"]);

                    txtRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REMARKS"]);
                    txtPoBox.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POBOX"]);

                    if (ds.Tables[0].Rows[0].IsNull("HPM_CITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]) != "0")
                    {
                        for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                        {
                            if (drpCity.Items[intCityCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]))
                            {
                                drpCity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CITY"]);
                                goto ForCity;
                            }

                        }
                    }

                ForCity: ;

                    if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                    {
                        for (int intNatiCount = 0; intNatiCount < drpNationality.Items.Count; intNatiCount++)
                        {
                            if (drpNationality.Items[intNatiCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]))
                            {
                                drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                                goto ForNation;
                            }

                        }
                    }
                ForNation: ;

                    if (ds.Tables[0].Rows[0].IsNull("HPM_EMIRATES_CODE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMIRATES_CODE"]) != "")
                    {

                        drpEmirates.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMIRATES_CODE"]);
                    }


                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_COUNTRY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]) != "0")
                {
                    for (int intCountryCount = 0; intCountryCount < drpCountry.Items.Count; intCountryCount++)
                    {
                        if (drpCountry.Items[intCountryCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]))
                        {
                            drpCountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_COUNTRY"]);
                            goto ForCountry;
                        }

                    }
                }
            ForCountry: ;


                if (ds.Tables[0].Rows[0].IsNull("HPM_VISA_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]) != "0")
                {
                    for (int intVisaType = 0; intVisaType < drpVisaType.Items.Count; intVisaType++)
                    {
                        if (drpVisaType.Items[intVisaType].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]))
                        {
                            drpVisaType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]);
                            goto ForVisaType;
                        }

                    }
                }
            ForVisaType: ;


                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 7" + strNewLineChar);



                if (ds.Tables[0].Rows[0].IsNull("HPM_FAX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]) != "")
                {
                    txtFax.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]).Trim();
                }

                if (Convert.ToString(ViewState["LoadFromEmiratesID"]) != "1")
                {
                    if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE1") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]) != "")
                    {
                        txtPhone1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]).Trim();
                    }

                    if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                    {
                        txtMobile1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]).Trim();
                    }

                    if (ds.Tables[0].Rows[0].IsNull("HPM_EMAIL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]) != "")
                    {
                        txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]).Trim();
                    }

                    if (ds.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                    {
                        txtEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]).Trim();
                    }

                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE2") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE2"]) != "")
                {
                    txtMobile2.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE2"]).Trim();
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE3") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE3"]) != "")
                {
                    txtPhone3.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE3"]).Trim();
                }






                if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false)
                {
                    drpIDCaption.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]);
                }

                txtIDNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_NO"]);

                if (Convert.ToString(ViewState["LoadFromEmiratesID"]) != "1")
                {
                    if (ds.Tables[0].Rows[0].IsNull("HPM_DOBDesc") == false)
                    {
                        txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                    }
                    else
                    {
                        txtDOB.Text = "";


                    }

                    txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                    if (txtDOB.Text != "")
                    {
                        string strDOBDay = "", strDOBMonth = "", strCurrentDay = "", strCurrentMonth = "";

                        string strDOB = txtDOB.Text;
                        string[] arrDOB = strDOB.Split('/');

                        if (arrDOB.Length > 1)
                        {
                            strDOBDay = arrDOB[0];

                            if (arrDOB.Length > 2)
                            {
                                strDOBMonth = arrDOB[1];
                            }
                        }

                        string strCurrentDate = hidTodayDate.Value;
                        string[] arrCurrentDate = strCurrentDate.Split('/');

                        strCurrentDay = arrCurrentDate[0];
                        strCurrentMonth = arrCurrentDate[1];


                        if (strDOBDay == strCurrentDay && strDOBMonth == strCurrentMonth)
                        {
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' BIRTHDAY  MESSAGE ','  This Patient having Birthday Today ','Green')", true);
                        }



                    }

                    if (ds.Tables[0].Rows[0].IsNull("HPM_AGE_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]) != "")
                    {

                        drpAgeType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                    }

                    txtMonth.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                    if (ds.Tables[0].Rows[0].IsNull("HPM_AGE_TYPE1") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]) != "")
                    {

                        drpAgeType1.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);
                    }




                    if (ds.Tables[0].Rows[0].IsNull("HPM_MARITAL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]) != "0")
                    {
                        for (int intCount = 0; intCount < drpMStatus.Items.Count; intCount++)
                        {
                            if (drpMStatus.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]))
                            {
                                drpMStatus.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MARITAL"]);
                                goto ForMStatus;
                            }

                        }
                    }
                ForMStatus: ;


                    if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "0")
                    {
                        for (int intCount = 0; intCount < drpSex.Items.Count; intCount++)
                        {
                            if (drpSex.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]))
                            {
                                drpSex.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                                goto ForSex;
                            }

                        }
                    }
                ForSex: ;

                }
                ////if (Convert.ToString(Session["DrNameDisplay"]).ToUpper() == "Y")
                ////{
                ////    txtDoctorID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]).Trim();
                ////    txtDoctorName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_NAME"]).Trim();
                ////    txtDepName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]).Trim();
                ////}
                //  txtTokenNo.Text = ds.Tables[0].Rows[0]["HPM_TOKEN_NO"].ToString();
                lblVisitedBranch.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LASTVISIT_BR"]);
                if (ds.Tables[0].Rows[0].IsNull("HPM_FIRSTVISITDesc") == false)
                {
                    lblFirstVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FIRSTVISITDesc"]);//.Remove(10,12);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_PREV_VISITDesc") == false)
                {
                    lblPrevVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PREV_VISITDesc"]);//.Remove(10,12);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_LATESTVISITDesc") == false)
                {
                    lblLastVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LATESTVISITDesc"]);//.Remove(10,12);
                }
                lblPrevDrId.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PRE_VISIT_DR_ID"]);
                lblPrevDrName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PRE_VISIT_DR_NAME"]);

                lblCreadedUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_CREATED_USER"]);
                lblModifiedUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MODIFIED_USER"]);



                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 8" + strNewLineChar);



                if (Convert.ToString(ViewState["HPVSeqNo"]) != null && Convert.ToString(ViewState["HPVSeqNo"]) != "")
                {
                    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false)
                    {
                        txtRefDoctorID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                        //.Remove(10, 12)
                    }
                    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_NAME") == false)
                    {
                        txtRefDoctorName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                        //.Remove(10, 12)
                    }
                }

                //    if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "0")
                //    {
                //        for (int intCount = 0; intCount < drpRefDoctor.Items.Count; intCount++)
                //        {
                //            if (drpRefDoctor.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]))
                //            {
                //                drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                //                goto ForRefDoc;
                //            }

                //        }
                //    }
                //ForRefDoc: ;


                txtKinName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_NAME"]);





                if (ds.Tables[0].Rows[0].IsNull("HPM_KIN_RELATION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_RELATION"]) != "")
                {
                    for (int intCount = 0; intCount < drpKinRelation.Items.Count; intCount++)
                    {
                        if (drpKinRelation.Items[intCount].Text == Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_RELATION"]))
                        {
                            drpKinRelation.SelectedIndex = intCount;
                        }

                    }
                }




                txtKinPhone.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_PHONE"]);
                txtKinMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_MOBILE"]);
                txtKinMobile1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KIN_MOBILE1"]);

                txtNoOfChildren.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NO_CHILDREN"]);

                txtOccupation.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_OCCUPATION"]);

                if (ds.Tables[0].Rows[0].IsNull("HPM_PATIENT_STATUS") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPTStatus.Items.Count; intCount++)
                    {
                        if (drpPTStatus.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]))
                        {
                            drpPTStatus.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PATIENT_STATUS"]);
                        }

                    }
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_REGISTERED_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_REGISTERED_TYPE"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_REGISTERED_TYPE"]) != "0")
                {
                    for (int intCount = 0; intCount < drpPtRegisteredType.Items.Count; intCount++)
                    {
                        if (drpPtRegisteredType.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_REGISTERED_TYPE"]))
                        {
                            drpPtRegisteredType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_REGISTERED_TYPE"]);
                        }

                    }
                }

                ////    if (ds.Tables[0].Rows[0].IsNull("HPM_PT_CATEGORY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_CATEGORY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]) != "0")
                ////    {
                ////        for (int intVisaType = 0; intVisaType < drpPTCategory.Items.Count; intVisaType++)
                ////        {
                ////            if (drpPTCategory.Items[intVisaType].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_CATEGORY"]))
                ////            {
                ////                drpPTCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_CATEGORY"]);
                ////                goto PTCategory;
                ////            }

                ////        }
                ////    }
                ////PTCategory: ;


                if (ds.Tables[0].Rows[0].IsNull("HPM_RELIGION_CODE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_RELIGION_CODE"]) != "")
                {

                    drpReligion.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_RELIGION_CODE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_KNOW_FROM") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_KNOW_FROM"]) != "")
                {

                    drpKnowFrom.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_KNOW_FROM"]);
                }


                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() 9" + strNewLineChar);

                txtHoldMsg.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_STATUS_MSG"]);

                if (drpPTStatus.SelectedValue == "Hold")
                {
                    //  strHoldMessage = "Patient Hold Message</br>" + Convert.ToString(ds.Tables[0].Rows[0]["HPM_STATUS_MSG"]);
                    //  ModalPopupExtender2.Show();

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' PATIENT HOLD ','" + strHoldMessage + ".','Brown')", true);

                }

                if (drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU")
                {
                    //txtPolicyNo.Enabled = true;
                    txtIDNo.Enabled = true;
                    //txtExpiryDate.Enabled = true;
                    // txtPolicyStart.Enabled = true;
                    txtPolicyId.Enabled = true;
                    // drpPlanType.Enabled = true;
                    drpNetworkName.Enabled = true;
                    drpDefaultCompany.Enabled = true;
                }
                //string strPath = Server.MapPath("~/img/" + txtFileNo.Text + "/InsCardFront/");
                //DirectoryInfo di = new DirectoryInfo(strPath);
                //if (di.Exists)
                //{
                //    FileInfo[] files = di.GetFiles();
                //    string filepath = files[0].Name;
                //    imgFront.ImageUrl = @"~/img/" + txtFileNo.Text + "/InsCardFront/" + filepath;

                //}
                //string strPath1 = Server.MapPath("~/img/" + txtFileNo.Text + "/InsCardBack/");
                //DirectoryInfo di1 = new DirectoryInfo(strPath1);
                //if (di1.Exists)
                //{
                //    FileInfo[] files1 = di1.GetFiles();
                //    string filepath1 = files1[0].Name;
                //    imgBack.ImageUrl = @"~/img/" + txtFileNo.Text + "/InsCardBack/" + filepath1;
                //}

                string strDedType = "", strCoInsType = "";
                if (ds.Tables[0].Rows[0].IsNull("HPM_DED_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DED_TYPE"]) != "")
                {
                    strDedType = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DED_TYPE"]);
                }



                decimal DedAmt = 0;
                if (ds.Tables[0].Rows[0].IsNull("HPM_DED_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DED_AMT"]) != "")
                {
                    DedAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_DED_AMT"]);


                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_COINS_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COINS_TYPE"]) != "")
                {
                    strCoInsType = Convert.ToString(ds.Tables[0].Rows[0]["HPM_COINS_TYPE"]);

                }
                decimal CoInsAmt = 0;
                if (ds.Tables[0].Rows[0].IsNull("HPM_COINS_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_COINS_AMT"]) != "")
                {
                    CoInsAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_COINS_AMT"]);

                }



                string strCoInsTypeDent = "", strCoInsTypePhy = "", strCoInsTypeLab = "", strCoInsTypeRad = "";
                if (ds.Tables[0].Rows[0].IsNull("HPM_DENT_COINS_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DENT_COINS_TYPE"]) != "")
                {
                    strCoInsTypeDent = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DENT_COINS_TYPE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHY_COINS_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHY_COINS_TYPE"]) != "")
                {
                    strCoInsTypePhy = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHY_COINS_TYPE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_LAB_COINS_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_LAB_COINS_TYPE"]) != "")
                {
                    strCoInsTypeLab = Convert.ToString(ds.Tables[0].Rows[0]["HPM_LAB_COINS_TYPE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_RAD_COINS_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_RAD_COINS_TYPE"]) != "")
                {
                    strCoInsTypeRad = Convert.ToString(ds.Tables[0].Rows[0]["HPM_RAD_COINS_TYPE"]);
                }

                decimal decCoInsAmtDent = 0, decCoInsAmtPhy = 0, decCoInsAmtLab = 0, decCoInsAmtRad = 0;
                if (ds.Tables[0].Rows[0].IsNull("HPM_DENT_COINS_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_DENT_COINS_AMT"]) != "")
                {
                    decCoInsAmtDent = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_DENT_COINS_AMT"]);

                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_PHY_COINS_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHY_COINS_AMT"]) != "")
                {
                    decCoInsAmtPhy = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_PHY_COINS_AMT"]);

                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_LAB_COINS_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_LAB_COINS_AMT"]) != "")
                {
                    decCoInsAmtLab = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_LAB_COINS_AMT"]);

                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_RAD_COINS_AMT") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_RAD_COINS_AMT"]) != "")
                {
                    decCoInsAmtRad = Convert.ToDecimal(ds.Tables[0].Rows[0]["HPM_RAD_COINS_AMT"]);

                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_HAAD_REGISTER") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_HAAD_REGISTER"]) != "")
                {
                    Boolean bolHaadRegister = false;

                    bolHaadRegister = Convert.ToBoolean(ds.Tables[0].Rows[0]["HPM_HAAD_REGISTER"]);

                    if (bolHaadRegister == true)
                    {
                        lblHAADRegister.Text = "Yes";
                    }
                    else
                    {
                        lblHAADRegister.Text = "No";
                    }

                }



                ds.Clear();
                ds.Dispose();

                imgFrontDefault.ImageUrl = "../DisplayCardImage.aspx?PT_ID=" + hidActualFileNo.Value;
                imgBackDefault.ImageUrl = "../DisplayCardImageBack.aspx?PT_ID=" + hidActualFileNo.Value;


             ////   imgPatientPhoto.ImageUrl = "../DisplayPatientPhoto.aspx?PT_ID=" + hidActualFileNo.Value;

                // PatientPhoto();

                //BELOW FUNCTION FOR STORE THE DB IMAGE IN TO PHYSICAL PATH
                //if (drpPatientType.SelectedValue == "CR")
                // {
                //ProcessTimLog(System.DateTime.Now.ToString() + "      ---ScanCardImageGenerate() Start");
                /// //  ScanCardImageGenerate();

                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindScanCardImagePath() Start" + strNewLineChar);

                BindScanCardImagePath();
                //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  BindScanCardImageGet() Start" + strNewLineChar);
                //   UploadDBImage();
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindScanCardImageGet() Start" + strNewLineChar);
                BindScanCardImageGet();
                //ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindScanCardImageGet() End");
                // }
                Boolean bolCompSelected = false;
                if ((drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU") && drpDefaultCompany.Items.Count > 0)
                {

                    if (strInsCompId != "")
                    {
                        for (int intCount = 0; intCount < drpDefaultCompany.Items.Count; intCount++)
                        {
                            if (drpDefaultCompany.Items[intCount].Value == strInsCompId)
                            {
                                drpDefaultCompany.SelectedValue = strInsCompId;
                                bolCompSelected = true;
                            }
                        }

                        for (int intCount = 0; intCount < drpPTVisitCompany.Items.Count; intCount++)
                        {
                            if (drpPTVisitCompany.Items[intCount].Value == strInsCompId)
                            {
                                drpPTVisitCompany.SelectedValue = strInsCompId;

                            }
                        }


                    }

                    if (bolCompSelected == true)
                    {
                        string strRemainderRecept = "";
                        strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindCompanyDetails() Start" + strNewLineChar);
                        BindCompanyDetails(out strRemainderRecept);
                        strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindCompanyDetails() End" + strNewLineChar);

                        strReminderMessage = "Message from Company Master - " + strRemainderRecept;


                        if (strRemainderRecept != "")
                        {
                            // ModalPopupExtender5.Show();
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' COMPANY  MESSAGE ','" + strReminderMessage + ".','Brown')", true);

                        }
                    }
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindSubCompany() Start" + strNewLineChar);
                    BindSubCompany();
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindSubCompany() End" + strNewLineChar);
                    if (drpNetworkName.Items.Count > 0)
                    {
                        if (strNetworkName != "")
                        {
                            for (int intCount = 0; intCount < drpNetworkName.Items.Count; intCount++)
                            {
                                if (drpNetworkName.Items[intCount].Value == strNetworkName)
                                {
                                    drpNetworkName.SelectedValue = strNetworkName;
                                }
                            }



                        }
                    }

                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindPlanType() Start" + strNewLineChar);

                    BindPlanType();
                    BindPTVisitCompanyPlan();

                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindPlanType() End" + strNewLineChar);

                    if (strPolicyType != "")
                    {

                        for (int intCount = 0; intCount < drpPlanType.Items.Count; intCount++)
                        {
                            if (drpPlanType.Items[intCount].Value == strPolicyType)
                            {
                                drpPlanType.SelectedValue = strPolicyType;
                            }
                        }

                        for (int intCount = 0; intCount < drpPTVisitCompanyPlan.Items.Count; intCount++)
                        {
                            if (drpPTVisitCompanyPlan.Items[intCount].Value == strPolicyType)
                            {
                                drpPTVisitCompanyPlan.SelectedValue = strPolicyType;
                            }
                        }

                    }



                }





                if (strDedType != "")
                {

                    for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                    {
                        if (drpDedType.Items[intCount].Value == strDedType)
                        {
                            drpDedType.SelectedValue = strDedType;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                    {
                        if (drpDedType.Items[intCount].Value == "")
                        {
                            drpDedType.SelectedValue = "";
                        }
                    }
                }


                if (strCoInsType != "")
                {

                    for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                    {
                        if (drpCoInsType.Items[intCount].Value == strCoInsType)
                        {
                            drpCoInsType.SelectedValue = strCoInsType;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                    {
                        if (drpCoInsType.Items[intCount].Value == "")
                        {
                            drpCoInsType.SelectedValue = "";
                        }
                    }
                }

                if (DedAmt != 0)
                {
                    txtDeductible.Text = DedAmt.ToString("N2");

                }


                if (CoInsAmt != 0)
                {
                    txtCoIns.Text = CoInsAmt.ToString("N2");

                }



                if (strCoInsTypeDent != "")
                {

                    for (int intCount = 0; intCount < drpCoInsDental.Items.Count; intCount++)
                    {
                        if (drpCoInsDental.Items[intCount].Value == strCoInsTypeDent)
                        {
                            drpCoInsDental.SelectedValue = strCoInsTypeDent;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpCoInsDental.Items.Count; intCount++)
                    {
                        if (drpCoInsDental.Items[intCount].Value == "")
                        {
                            drpCoInsDental.SelectedValue = "";
                        }
                    }
                }

                if (decCoInsAmtDent != 0)
                {
                    txtCoInsDental.Text = decCoInsAmtDent.ToString("N2");

                }


                if (strCoInsTypePhy != "")
                {

                    for (int intCount = 0; intCount < drpCoInsPharmacy.Items.Count; intCount++)
                    {
                        if (drpCoInsPharmacy.Items[intCount].Value == strCoInsTypePhy)
                        {
                            drpCoInsPharmacy.SelectedValue = strCoInsTypePhy;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpCoInsPharmacy.Items.Count; intCount++)
                    {
                        if (drpCoInsPharmacy.Items[intCount].Value == "")
                        {
                            drpCoInsPharmacy.SelectedValue = "";
                        }
                    }
                }


                if (decCoInsAmtPhy != 0)
                {
                    txtCoInsPharmacy.Text = decCoInsAmtPhy.ToString("N2");

                }

                if (strCoInsTypeLab != "")
                {

                    for (int intCount = 0; intCount < drpCoInsLab.Items.Count; intCount++)
                    {
                        if (drpCoInsLab.Items[intCount].Value == strCoInsTypeLab)
                        {
                            drpCoInsLab.SelectedValue = strCoInsTypeLab;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpCoInsLab.Items.Count; intCount++)
                    {
                        if (drpCoInsLab.Items[intCount].Value == "")
                        {
                            drpCoInsLab.SelectedValue = "";
                        }
                    }
                }


                if (decCoInsAmtLab != 0)
                {
                    txtCoInsLab.Text = decCoInsAmtLab.ToString("N2");

                }
                if (strCoInsTypeRad != "")
                {

                    for (int intCount = 0; intCount < drpCoInsRad.Items.Count; intCount++)
                    {
                        if (drpCoInsRad.Items[intCount].Value == strCoInsTypeRad)
                        {
                            drpCoInsRad.SelectedValue = strCoInsTypeRad;
                        }
                    }

                }
                else
                {

                    for (int intCount = 0; intCount < drpCoInsRad.Items.Count; intCount++)
                    {
                        if (drpCoInsRad.Items[intCount].Value == "")
                        {
                            drpCoInsRad.SelectedValue = "";
                        }
                    }
                }


                if (decCoInsAmtRad != 0)
                {
                    txtCoInsRad.Text = decCoInsAmtRad.ToString("N2");

                }
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindToken() Start" + strNewLineChar);
                BindToken();
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindGCGrid() Start" + strNewLineChar);
                BindGCGrid();
                // BindLastCunsultDtls(); TODO: This has taking more time so we commented.
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " GetPatientVisit() Start" + strNewLineChar);
                GetPatientVisit();
                GetPatientVisitHistory();
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindPatientVisitLabel() Start" + strNewLineChar);
                BindPatientVisitLabel();
                ////ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindPatientVisitLabel() Complete");

                if ((drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU") && drpDefaultCompany.Items.Count > 0)
                {
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " CheckCompanyActive() Start" + strNewLineChar);
                    if (CheckCompanyActive(strInsCompId) == false)
                    {
                        // ModalPopupExtender1.Show();
                        // strMessage = "This company is not active.";
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' COMPANY NOT ACTIVE ',' Selected company is not active.','Brown')", true);

                    }
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " CheckCompanyActive() End" + strNewLineChar);
                }





            }

            // //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(Convert.ToString(//strProcessTimLogData)); }
        }

        void BindScanCardImageGet()
        {
            DataSet Ds1 = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";

            Ds1 = dbo.ScanCardImageGet(Criteria);
            ViewState["ScanCard"] = Ds1.Tables[0];

            if (Ds1.Tables[0].Rows.Count > 0)
            {
                gvScanCard.Visible = true;
                gvScanCard.DataSource = Ds1.Tables[0];
                gvScanCard.DataBind();


            }

            /*
            // FOR DEFAULT CARD DATA LODING COMMENTED AS PER CLIENT REQUEST -SMCH

            if (GlobalValues.FileDescription.ToUpper() != "SMCH")
            {
                for (int i = 0; i <= Ds1.Tables[0].Rows.Count - 1; i++)
                {
                    if (Ds1.Tables[0].Rows[i]["HSC_ISDEFAULT"].ToString() == "True" && Ds1.Tables[0].Rows[i]["HSC_CARD_NAME"].ToString() == "Insurance")
                    {

                        btnScanCardAdd.Visible = false;
                        btnScanCardUpdate.Visible = true;


                        ViewState["ScanCardSelectIndex"] = i;


                        drpCardName.SelectedValue = Ds1.Tables[0].Rows[i]["HSC_CARD_NAME"].ToString();
                        txtCompany.Text = Ds1.Tables[0].Rows[i]["HSC_INS_COMP_ID"].ToString();
                        txtCompanyName.Text = Ds1.Tables[0].Rows[i]["HSC_INS_COMP_NAME"].ToString();
                        txtCardNo.Text = Ds1.Tables[0].Rows[i]["HSC_CARD_NO"].ToString();
                        txtPolicyCardNo.Text = Ds1.Tables[0].Rows[i]["HSC_POLICY_NO"].ToString();
                        txtCardExpDate.Text = Convert.ToString(Ds1.Tables[0].Rows[i]["HSC_EXPIRY_DATE"].ToString());
                        chkIsDefault.Checked = Convert.ToBoolean(Ds1.Tables[0].Rows[i]["HSC_ISDEFAULT"].ToString());
                        chkCardActive.Checked = Convert.ToBoolean(Ds1.Tables[0].Rows[i]["HSC_STATUS"].ToString());
                        imgFront.ImageUrl = @"../Uploads/" + Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_FRONT"].ToString();
                        imgBack.ImageUrl = @"../Uploads/" + Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_BACK"].ToString();

                        if (Ds1.Tables[0].Rows[i].IsNull("HSC_IMAGE_PATH_FRONT") == false)
                        {
                            Session["InsCardFrontPath"] = Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_FRONT"].ToString();
                        }
                        if (Ds1.Tables[0].Rows[i].IsNull("HSC_IMAGE_PATH_BACK") == false)
                        {
                            Session["InsCardBackPath"] = Ds1.Tables[0].Rows[i]["HSC_IMAGE_PATH_BACK"].ToString();
                        }


                    }

                }
            
            }
 */
        }


        void BindScanCardImage()
        {
            DataSet Ds1 = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";

            Ds1 = dbo.ScanCardImageGet(Criteria);
            ViewState["ScanCard"] = Ds1.Tables[0];

            if (Ds1.Tables[0].Rows.Count > 0)
            {
                gvScanCard.Visible = true;
                gvScanCard.DataSource = Ds1.Tables[0];
                gvScanCard.DataBind();


            }


        }


        void Clear()
        {

            if (Convert.ToString(ViewState["EID_TYPE"]) == "FEITIAN")
            {
                txtEmirateIDData.Focus();
            }
            // imgLoaderEidScan.Visible = false;
            btnSave.Enabled = true;
            btnSaveVisit.Enabled = true;

            TabContainer1.ActiveTab = TabPanelGeneral;
            string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);
            switch (AutoSlNo)
            {
                case "A":
                    chkManual.Checked = false;
                    chkManual.Visible = false;
                    // txtFileNo.ReadOnly = true;
                    btnNew.Visible = true;
                    break;
                case "M":
                    chkManual.Checked = true;
                    chkManual.Visible = false;
                    txtFileNo.ReadOnly = false;
                    btnNew.Visible = false;
                    break;
                case "B":
                    // chkManual.Checked = true;
                    chkManual.Visible = true;
                    btnNew.Visible = true;
                    break;
                default:
                    break;

            }
            //if (hidPateName.Value != "HomeCare")
            //{
            //    chkUpdateWaitList.Checked = true;
            //}
            chkDisplayInDrWaitingList.Checked = true;

            hidLatestVisitHours.Value = "";
            hidLastVisitDrId.Value = "";
            hidWaitingList.Value = "";
            stParentName = "0";
            stParentName1 = "0";

            lblEFilling.Text = "";
            lblStatus.Text = "";
            // imgPhoto.Visible = false;
            lblVisitType.Text = "New";
            lblPTVisitType.Text = "New";

            //if (drpDepartment.Items.Count > 0)
            //    drpDepartment.SelectedIndex = 0;
            chkStatus.Checked = true;

            if (drpTitle.Items.Count > 0)
                drpTitle.SelectedIndex = 0;

            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";

            if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
            {
                drpPatientType.SelectedIndex = 1;
            }
            else
            {
                drpPatientType.SelectedIndex = 0;
            }

            txtDOB.Text = "";

            txtAge.Text = "";
            txtMonth.Text = "";
            drpSex.SelectedIndex = 0;



            if (chkFamilyMember.Checked == false)
            {
                txtPoBox.Text = "";
                txtAddress.Text = "";
                txtRemarks.Text = "";
                if (drpCity.Items.Count > 0)
                    drpCity.SelectedIndex = 0;
                if (drpArea.Items.Count > 0)
                    drpArea.SelectedIndex = 0;
                if (drpCountry.Items.Count > 0)
                    drpCountry.SelectedIndex = 0;
                if (drpNationality.Items.Count > 0)
                    drpNationality.SelectedIndex = 0;
                if (drpPtRegisteredType.Items.Count > 0)
                    drpPtRegisteredType.SelectedIndex = 0;

                txtPhone1.Text = "";
                txtMobile1.Text = "";
                txtMobile2.Text = "";
                txtPhone3.Text = "";
                txtFax.Text = "";
                txtEmail.Text = "";
            }

            if (drpBlood.Items.Count > 0)
                drpBlood.SelectedIndex = 0;
            if (drpMStatus.Items.Count > 0)
                drpMStatus.SelectedIndex = 0;
            if (drpNetworkName.Items.Count > 0)
                drpNetworkName.SelectedIndex = 0;





            if (drpPriority.Items.Count > 0)
                drpPriority.SelectedIndex = 0;


            txtEmiratesID.Text = "";
            if (drpIDCaption.Items.Count > 0)
            {
                drpIDCaption.SelectedIndex = 0;
            }
            txtDoctorID.Text = "";
            txtDoctorName.Text = "";
            txtDepName.Text = "";
            txtTokenNo.Text = "";
            txtCouponNo.Text = "";


            //if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
            //{
            //    chkUpdateWaitList.Visible = false;
            //    //chkUpdateWaitList.Checked = false;
            //}
            //else
            //{
            //    chkUpdateWaitList.Visible = true;
            //    // chkUpdateWaitList.Checked = true;
            //}
            chkManualToken.Checked = false;




            txtRefDoctorID.Text = "";
            txtRefDoctorName.Text = "";

            txtKinName.Text = "";

            txtKinPhone.Text = "";
            txtKinMobile.Text = "";
            txtKinMobile1.Text = "";
            txtNoOfChildren.Text = "";
            if (drpDefaultCompany.Items.Count > 0)
            {
                drpDefaultCompany.SelectedIndex = 0;
            }

            if (drpKinRelation.Items.Count > 0)
            {
                drpKinRelation.SelectedIndex = 0;
            }

            if (drpPTCategory.Items.Count > 0)
            {
                drpPTCategory.SelectedIndex = 0;
            }

            if (drpPatientClass.Items.Count > 0)
            {
                drpPatientClass.SelectedIndex = 0;
            }



            if (drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU")
            {
                //txtPolicyNo.Enabled = true;
                txtPolicyId.Enabled = true;
                txtIDNo.Enabled = true;
                //  txtExpiryDate.Enabled = true;
                // txtPolicyStart.Enabled = true;
                // drpPlanType.Enabled = true;
                drpDefaultCompany.Enabled = true;
                drpNetworkName.Enabled = true;
            }
            else
            {
                // txtPolicyNo.Enabled = false;
                txtPolicyId.Enabled = false;
                txtIDNo.Enabled = false;
                //  txtExpiryDate.Enabled = false;
                //  txtPolicyStart.Enabled = false;
                drpDefaultCompany.Enabled = false;
                // drpPlanType.Enabled = false;
                drpNetworkName.Enabled = false;
            }



            txtPolicyNo.Text = "";
            txtPolicyId.Text = "";
            drpPlanType.Items.Clear();
            txtIDNo.Text = "";
            txtExpiryDate.Text = "";
            txtPolicyStart.Text = "";
            chkCompCashPT.Checked = false;

            txtPTCompany.Text = "";

            if (drpPayer.Items.Count > 0)
                drpPayer.SelectedIndex = 0;

            if (drpDedType.Items.Count > 0)
                drpDedType.SelectedIndex = 0;

            if (drpCoInsType.Items.Count > 0)
                drpCoInsType.SelectedIndex = 1;


            if (drpCoInsDental.Items.Count > 0)
                drpCoInsDental.SelectedIndex = 0;
            if (drpCoInsPharmacy.Items.Count > 0)
                drpCoInsPharmacy.SelectedIndex = 0;
            if (drpCoInsLab.Items.Count > 0)
                drpCoInsLab.SelectedIndex = 0;
            if (drpCoInsRad.Items.Count > 0)
                drpCoInsRad.SelectedIndex = 0;

            txtDeductible.Text = "";
            txtCoIns.Text = "";

            txtCoInsDental.Text = "";
            txtCoInsPharmacy.Text = "";
            txtCoInsLab.Text = "";
            txtCoInsRad.Text = "";
            txtEligibilityID.Text = "";
            txtSessionInvoiceNo.Text = "";
            hidPTVisitSQNo.Value = "";
            lblVisitedBranch.Text = "";
            lblFirstVisit.Text = "";
            lblPrevVisit.Text = "";
            lblLastVisit.Text = "";
            lblPrevDrId.Text = "";
            lblPrevDrName.Text = "";

            lblCreadedUser.Text = "";
            lblModifiedUser.Text = "";

            txtOccupation.Text = "";
            if (drpPTStatus.Items.Count > 0)
                drpPTStatus.SelectedIndex = 0;
            if (drpPtRegisteredType.Items.Count > 0)
                drpPtRegisteredType.SelectedIndex = 0;
            txtHoldMsg.Text = "";


            txtNetworkClass.Text = "";



            chkLabelPrint.Checked = false;

            if (drpCardType.Items.Count > 0)
            {
                drpCardType.SelectedIndex = 0;
                drpCardType_SelectedIndexChanged(drpCardType, new EventArgs());
            }

            //if (drpCardName.Items.Count > 0)
            //    drpCardName.SelectedIndex = 0;

            if (drpParentCompany.Items.Count > 0)
            {
                drpParentCompany.SelectedIndex = 0;
            }
            txtCompany.Text = "";
            txtCompanyName.Text = "";
            txtCardNo.Text = "";
            txtPolicyCardNo.Text = "";
            txtCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();

            //if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
            //{
            txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    txtEffectiveDate.Text = hidLocalDate.Value;
            //}


            chkIsDefault.Checked = true;
            chkCardActive.Checked = true;

            txtTokenNo.Enabled = false;

            DataTable dt = new DataTable();
            BuildScanCard();
            dt = (DataTable)ViewState["ScanCard"];
            gvScanCard.DataSource = dt;
            gvScanCard.DataBind();
            gvScanCard.Visible = false;
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";


            imgFrontDefault.ImageUrl = "";
            imgBackDefault.ImageUrl = "";
            imgPatientPhoto.ImageUrl = "~/Images/NoImage.png";

            btnScanCardAdd.Visible = true;
            btnScanCardUpdate.Visible = false;


            if (drpVisaType.Items.Count > 0)
                drpVisaType.SelectedIndex = 0;


            if (drpReligion.Items.Count > 0)
                drpReligion.SelectedIndex = 0;

            if (drpEmirates.Items.Count > 0)
                drpEmirates.SelectedIndex = 0;

            txtSubstitute.Text = "";
            txtSubsAge.Text = "";

            if (drpSubsNationality.Items.Count > 0)
                drpSubsNationality.SelectedIndex = 0;

            txtSubsIdNo.Text = "";
            txtSubsType.Text = "";
            txtSubsCapacity.Text = "";
            txtTranslator.Text = "";

            imgSig1.ImageUrl = "";
            imgSig2.ImageUrl = "";
            imgSig3.ImageUrl = "";

            lblHAADRegister.Text = "No";



            chkRequiredTriage.Checked = true;
            trTriageReason.Visible = false;
            drpTriageReason.SelectedIndex = 0;

            gvGCGridView.Visible = false;


            divCunsult.Visible = false;

            if (drpNurse.Items.Count > 0)
            {
                drpNurse.SelectedIndex = 0;
            }


            txtPTVisitFileNo.Text = "";
            txtPTVisitFullName.Text = "";
            txtPTVisitMobile.Text = "";
            txtPTVisitGender.Text = "";


            txtPTVisitPolicyNo.Text = "";
            txtPTVisitCardNo.Text = "";

            if (drpPTVisitCompany.Items.Count > 0)
            {
                drpPTVisitCompany.SelectedIndex = 0;
            }


            if (drpPTVisitCompanyPlan.Items.Count > 0)
            {
                drpPTVisitCompanyPlan.SelectedIndex = 0;
            }

            txtPTVisitCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();

            ViewState["ScanCardSelectIndex"] = "";


            ViewState["Secondary_Ins"] = "";
            Session["InsCardFrontPath"] = "";
            Session["InsCardBackPath"] = "";
            ViewState["Patient_ID_VALID"] = "0";
            ViewState["PatientInfoId"] = "";

            ViewState["Signature1"] = "";
            ViewState["Signature2"] = "";
            ViewState["Signature3"] = "";



            ViewState["HPVSeqNo"] = "";
            ViewState["HGC_ID"] = "";

            ViewState["EmirateIdRef"] = "";
            Session["Photo"] = "";

            ViewState["ConfirmResult"] = "";
            ViewState["EIDConfirmResult"] = "";
            ViewState["ConfirmMsgFor"] = "";
            ViewState["EmiratesIDPT_ID"] = "";
            Session["PatientID"] = "";
            lblConfirmMessage.Text = "";



            Session["InsCardFrontDefaultPath"] = "";
            Session["InsCardBackDefaultPath"] = "";

            ViewState["HGC_ID"] = "";
            ViewState["ReferralEMR_ID"] = "";

        }

        void BindSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE = '" + drpDefaultCompany.SelectedValue + "'";

            if (drpPatientType.SelectedValue == "CR")
            {
                Criteria += " AND HCM_COMP_TYPE ='I' ";
            }
            else if (drpPatientType.SelectedValue == "CU")
            {
                Criteria += " AND HCM_COMP_TYPE ='N' ";
            }

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            drpNetworkName.Items.Clear();
            drpPlanType.Items.Clear();
            txtDeductible.Text = "";
            //  txtCoIns.Text = "";

            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNetworkName.DataSource = ds;
                drpNetworkName.DataValueField = "HCM_COMP_ID";
                drpNetworkName.DataTextField = "HCM_NAME";
                drpNetworkName.DataBind();

            }

            drpNetworkName.Items.Insert(0, "--- Select ---");
            drpNetworkName.Items[0].Value = "0";
            drpNetworkName.SelectedIndex = 0;
        }

        void BindPlanType()
        {
            drpPlanType.Items.Clear();
            txtDeductible.Text = "";
            //  txtCoIns.Text = "";
            if (drpDefaultCompany.Items.Count > 0)
            {
                if (drpDefaultCompany.SelectedItem.Text != "")
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";
                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
                    }
                    DS = dbo.CompBenefitsGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        drpPlanType.DataSource = DS;
                        if (GlobalValues.FileDescription.ToUpper() == "AMAL")
                        {
                            drpPlanType.DataTextField = "HICM_INS_CAT_NAME";
                        }
                        else
                        {
                            drpPlanType.DataTextField = "HCB_CAT_TYPE";
                        }
                        drpPlanType.DataValueField = "HCB_CAT_TYPE";
                        drpPlanType.DataBind();
                    }

                    drpPlanType.Items.Insert(0, "--- Select ---");
                    drpPlanType.Items[0].Value = "0";
                }
            }

        }

        void BindDeductCoInsAmt()
        {

            if (drpDefaultCompany.Items.Count > 0)
            {
                if (drpDefaultCompany.SelectedItem.Text != "")
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";

                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "' and HCB_CAT_TYPE='" + drpPlanType.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpDefaultCompany.SelectedValue + "' and HCB_CAT_TYPE='" + drpPlanType.SelectedValue + "'";
                    }
                    DS = dbo.CompBenefitsGet(Criteria);
                    txtDeductible.Text = "";
                    txtCoIns.Text = "";
                    if (DS.Tables[0].Rows.Count > 0)
                    {


                        string strDedType = "", strCoInsType = "";

                        strDedType = Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_TYPE"]);
                        if (strDedType != "")
                        {

                            for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                            {
                                if (drpDedType.Items[intCount].Value == strDedType)
                                {
                                    drpDedType.SelectedValue = strDedType;
                                }
                            }

                        }
                        else
                        {

                            for (int intCount = 0; intCount < drpDedType.Items.Count; intCount++)
                            {
                                if (drpDedType.Items[intCount].Value == "")
                                {
                                    drpDedType.SelectedValue = "";
                                }
                            }
                        }

                        strCoInsType = Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_TYPE"]);
                        if (strCoInsType != "")
                        {

                            for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                            {
                                if (drpCoInsType.Items[intCount].Value == strCoInsType)
                                {
                                    drpCoInsType.SelectedValue = strCoInsType;
                                    drpCoInsDental.SelectedValue = strCoInsType;
                                    drpCoInsPharmacy.SelectedValue = strCoInsType;
                                    drpCoInsLab.SelectedValue = strCoInsType;
                                    drpCoInsRad.SelectedValue = strCoInsType;

                                }
                            }

                        }
                        else
                        {

                            for (int intCount = 0; intCount < drpCoInsType.Items.Count; intCount++)
                            {
                                if (drpCoInsType.Items[intCount].Value == "")
                                {
                                    drpCoInsType.SelectedValue = "";
                                    drpCoInsDental.SelectedValue = "";
                                    drpCoInsPharmacy.SelectedValue = "";
                                    drpCoInsLab.SelectedValue = "";
                                    drpCoInsRad.SelectedValue = "";
                                }
                            }
                        }



                        if (DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]) != "")
                        {
                            decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]);
                            txtDeductible.Text = DedAmt.ToString("N2");


                        }

                        if (DS.Tables[0].Rows[0].IsNull("HCB_COINS_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]) != "")
                        {
                            decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]);
                            txtCoIns.Text = CoInsAmt.ToString("N2");
                            txtCoInsDental.Text = CoInsAmt.ToString("N2");
                            txtCoInsPharmacy.Text = CoInsAmt.ToString("N2");
                            txtCoInsLab.Text = CoInsAmt.ToString("N2");
                            txtCoInsRad.Text = CoInsAmt.ToString("N2");
                        }





                    }

                }
            }

        }


        void BindDeductCoInsGrid()
        {

            if (drpDefaultCompany.Items.Count > 0)
            {
                if (drpDefaultCompany.SelectedItem.Text != "" && drpPlanType.SelectedIndex != 0)
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";

                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCBD_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCBD_COMP_ID = '" + drpDefaultCompany.SelectedValue + "'";
                    }

                    if (drpPlanType.SelectedIndex != 0)
                    {
                        Criteria += " AND HCBD_CAT_TYPE = '" + drpPlanType.SelectedValue + "'";
                    }

                    DS = dbo.CompBenefitsDtlsGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        gvHCB.Visible = true;
                        gvHCB.DataSource = DS;
                        gvHCB.DataBind();
                    }



                }
            }

        }

        void BuildScanCard()
        {
            DataTable dt = new DataTable();

            DataColumn HSC_CARD_NAME = new DataColumn();
            HSC_CARD_NAME.ColumnName = "HSC_CARD_NAME";

            DataColumn HSC_CARD_TYPE = new DataColumn();
            HSC_CARD_TYPE.ColumnName = "HSC_CARD_TYPE";

            DataColumn HSC_CARD_NO = new DataColumn();
            HSC_CARD_NO.ColumnName = "HSC_CARD_NO";

            DataColumn HSC_POLICY_NO = new DataColumn();
            HSC_POLICY_NO.ColumnName = "HSC_POLICY_NO";

            DataColumn HSC_INS_COMP_ID = new DataColumn();
            HSC_INS_COMP_ID.ColumnName = "HSC_INS_COMP_ID";

            DataColumn HSC_INS_COMP_NAME = new DataColumn();
            HSC_INS_COMP_NAME.ColumnName = "HSC_INS_COMP_NAME";


            DataColumn HSC_EXPIRY_DATE = new DataColumn();
            HSC_EXPIRY_DATE.ColumnName = "HSC_EXPIRY_DATE";

            DataColumn HSC_ISDEFAULT = new DataColumn();
            HSC_ISDEFAULT.ColumnName = "HSC_ISDEFAULT";

            DataColumn HSC_ISDEFAULTDesc = new DataColumn();
            HSC_ISDEFAULTDesc.ColumnName = "HSC_ISDEFAULTDesc";

            DataColumn HSC_STATUS = new DataColumn();
            HSC_STATUS.ColumnName = "HSC_STATUS";

            DataColumn HSC_STATUSDesc = new DataColumn();
            HSC_STATUSDesc.ColumnName = "HSC_STATUSDesc";

            DataColumn HSC_IMAGE_PATH_FRONT = new DataColumn();
            HSC_IMAGE_PATH_FRONT.ColumnName = "HSC_IMAGE_PATH_FRONT";

            DataColumn HSC_IMAGE_PATH_BACK = new DataColumn();
            HSC_IMAGE_PATH_BACK.ColumnName = "HSC_IMAGE_PATH_BACK";

            DataColumn HSC_ISNEW = new DataColumn();
            HSC_ISNEW.ColumnName = "HSC_ISNEW";

            DataColumn HSC_POLICY_START = new DataColumn();
            HSC_POLICY_START.ColumnName = "HSC_POLICY_START";


            DataColumn HSC_PLAN_TYPE = new DataColumn();
            HSC_PLAN_TYPE.ColumnName = "HSC_PLAN_TYPE";

            DataColumn HSC_PHOTO_ID = new DataColumn();
            HSC_PHOTO_ID.ColumnName = "HSC_PHOTO_ID";


            dt.Columns.Add(HSC_CARD_NAME);
            dt.Columns.Add(HSC_CARD_TYPE);
            dt.Columns.Add(HSC_CARD_NO);
            dt.Columns.Add(HSC_POLICY_NO);
            dt.Columns.Add(HSC_INS_COMP_ID);
            dt.Columns.Add(HSC_INS_COMP_NAME);
            dt.Columns.Add(HSC_EXPIRY_DATE);
            dt.Columns.Add(HSC_ISDEFAULT);
            dt.Columns.Add(HSC_ISDEFAULTDesc);
            dt.Columns.Add(HSC_STATUS);
            dt.Columns.Add(HSC_STATUSDesc);
            dt.Columns.Add(HSC_IMAGE_PATH_FRONT);
            dt.Columns.Add(HSC_IMAGE_PATH_BACK);
            dt.Columns.Add(HSC_ISNEW);
            dt.Columns.Add(HSC_POLICY_START);
            dt.Columns.Add(HSC_PLAN_TYPE);
            dt.Columns.Add(HSC_PHOTO_ID);

            ViewState["ScanCard"] = dt;
        }

        void ClearScan()
        {
            btnScanCardAdd.Visible = true;
            btnScanCardUpdate.Visible = false;

            if (drpCardType.Items.Count > 0)
            {
                drpCardType.SelectedIndex = 0;
                drpCardType_SelectedIndexChanged(drpCardType, new EventArgs());
            }

            //if (drpCardName.Items.Count > 0)
            //    drpCardName.SelectedIndex = 0;
            if (drpParentCompany.Items.Count > 0)
            {
                drpParentCompany.SelectedIndex = 0;
            }
            stParentName = "0";
            txtCompany.Text = "";
            txtCompanyName.Text = "";
            txtCardNo.Text = "";
            txtPolicyCardNo.Text = "";
            txtCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();
            //if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
            //{
            txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    txtEffectiveDate.Text = hidLocalDate.Value;
            //}
            chkIsDefault.Checked = true;
            chkCardActive.Checked = true;
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";

            txtPlanType.Text = "";

        }



        void BindPhoto()
        {
            try
            {

                //imgPhoto.ImageUrl = "";
                //imgPhoto.Visible = true;
                //imgPhoto.ImageUrl = "../DisplayImage.aspx";





            }
            catch (Exception e)
            {

                //  imgPhoto.ImageUrl = "../Images/Loginlogo.jpeg";
            }

        }

        //void CheckCity(String City)
        //{
        //    if (drpCity.SelectedIndex != 0)
        //    {
        //        dbo.CityMasterAdd(Convert.ToString(Session["Branch_ID"]), City, Convert.ToString(Session["User_ID"]));

        //    }
        //}



        string fnGetClaimFormName(String mCompId, String Dept)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = dbo.retun_inccmp_details(Criteria);

            string RptName;
            RptName = "HMSInsCardPrint.rpt";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "HMSInsCardPrint.rpt";
                }
                else
                {
                    RptName = (Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) + ".rpt");
                }

                if (Dept.ToUpper() == "DENTAL" && chkCompletionRpt.Checked == true)
                {
                    if (File.Exists(strReportPath + "Completion" + Dept + RptName) == true)
                    {
                        return "Completion" + Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }
                else if (Dept.ToUpper() == "DENTAL" && chkCompletionRpt.Checked == false)
                {
                    if (File.Exists(strReportPath + Dept + RptName) == true)
                    {
                        return Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }

                if (chkCompletionRpt.Checked == true)
                {
                    if (File.Exists(strReportPath + "Completion" + RptName) == true)
                    {
                        return "Completion" + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }

                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        string fnGetWebClaimFormName(String mCompId, String Dept)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = dbo.retun_inccmp_details(Criteria);

            string RptName = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {


                }
                else
                {
                    RptName = ("EMR_" + Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) + "AuthorizationRequest.aspx");
                }

                if (Dept.ToUpper() == "DENTAL")
                {
                    if (File.Exists(Server.MapPath("Report\\" + "EMR_" + Dept + RptName)))
                    {
                        return Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }
                return RptName;
            }
            else
            {
                return RptName;
            }



        }



        Boolean CheckMobileNo()
        {
            Boolean bolMobileNo = false;
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            //Criteria += " AND HPM_PT_ID Like '" + txtFileNo.Text + "'";
            if (ViewState["Patient_ID_VALID"] == "0")
            {
                Criteria += " AND HPM_MOBILE Like '" + txtMobile1.Text + "'";
            }
            else
            {
                Criteria += " AND HPM_PT_ID != '" + txtFileNo.Text + "' AND HPM_MOBILE Like '" + txtMobile1.Text + "'";
            }
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strMessage = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) + " : " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]) + " having this mobile No.";
                return true;

            }
            else
            {
                strMessage = "";
            }
            return bolMobileNo;
        }

        Boolean CheckPatientName()
        {
            Boolean bolMobileNo = false;
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND HPM_PT_ID Like '" + txtFileNo.Text + "'";
            if (ViewState["Patient_ID_VALID"] == "0")
            {
                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_FNAME Like '" + txtFName.Text.Trim() + "'";
                }

                if (txtMName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_MNAME Like '" + txtMName.Text.Trim() + "'";
                }

                if (txtLName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_LNAME Like '" + txtLName.Text.Trim() + "'";
                }

            }
            else
            {
                Criteria += " AND HPM_PT_ID != '" + txtFileNo.Text.Trim() + "'";

                if (txtFName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_FNAME Like '" + txtFName.Text.Trim() + "'";
                }

                if (txtMName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_MNAME Like '" + txtMName.Text.Trim() + "'";
                }

                if (txtLName.Text.Trim() != "")
                {
                    Criteria += " AND HPM_PT_LNAME Like '" + txtLName.Text.Trim() + "'";
                }

            }

            // Criteria += " AND hpm_pt_Fname + ' ' +isnull(hpm_pt_Mname,'') + ' '  + isnull(hpm_pt_Lname,'')   like '%" + txtFName.Text.Trim() + ' '  +txtMName.Text.Trim() + ' '   +txtLName.Text.Trim() + "%' ";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strMessage = "File No: " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) + " also having this Name '" + Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) + "'";
                return true;

            }
            else
            {
                strMessage = "";
            }
            return bolMobileNo;
        }

        Boolean CheckCompanyActive(string strCompanyID)
        {


            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND HCM_COMP_ID ='" + strCompanyID + "'";
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                return true;

            }
            return false;




        }

        void CheckFamilyAppoinment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            string strPTName = txtFileNo.Text.Trim();
            string[] arrPTName = strPTName.Split('/');

            //Criteria += " AND  HPM_PT_ID != '" + strPTName + "'";

            if (txtMobile1.Text.Trim() != "")
            {
                Criteria += " AND ( HPM_PT_ID Like '" + txtFileNo.Text.Trim() + "' OR HPM_MOBILE='" + txtMobile1.Text.Trim() + "')";

            }
            else
            {
                Criteria += " AND  HPM_PT_ID Like '" + txtFileNo.Text.Trim() + "'";
            }
            ds = dbo.retun_patient_details(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["TodayDBDateDesc"]) == Convert.ToString(ds.Tables[0].Rows[i]["HPM_LATESTVISITDesc"]))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This Patient having Appoinment Today');", true);
                        i = ds.Tables[0].Rows.Count;

                    }
                }

            }


        }

        void GetAppoinmentDtls()
        {
            ViewState["AppointCreatedDateTime"] = "";
            ViewState["AppointStartDateTime"] = "";
            ViewState["AppointStartTime"] = "";
            ViewState["ReferralAppoint"] = "false";
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += "   AND  CONVERT(DATE,HAM_FINISHTIME,101) = CONVERT(DATE, GETDATE(),101) ";
           // Criteria += " AND  HAM_FILENUMBER = '" + txtFileNo.Text.Trim() + "'";


            Criteria += " AND  HAM_DR_CODE = '" + txtDoctorID.Text.Trim() + "'";

            if (txtMobile1.Text.Trim() != "")
            {
                Criteria += " AND ( HAM_FILENUMBER Like '" + txtFileNo.Text.Trim() + "' OR HAM_MOBILENO='" + txtMobile1.Text.Trim() + "')";

            }
            else
            {
                Criteria += " AND  HAM_FILENUMBER = '" + txtFileNo.Text.Trim() + "'";
            }
            // ds = dbo.AppointmentOutlookGet(Criteria);
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue("TOP 1  " +
        "  ( CONVERT(VARCHAR,HAM_CREATED_DATE,103)+' '+ CONVERT(VARCHAR,HAM_CREATED_DATE,108)) AS AppointCreatedDateTime " +
        " ,( CONVERT(VARCHAR,HAM_STARTTIME,103)+' '+ CONVERT(VARCHAR,HAM_STARTTIME,108)) AS AppointStartDateTime" +
        " ,(CONVERT(varchar(5), HAM_STARTTIME,108)) AS AppointStartTime" +
        " ,CASE ISNULL(HPM_REF_DR_CODE,0) WHEN  0 THEN 'false' ELSE 'true' END as ReferralAppoint", "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria, "");


            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["AppointCreatedDateTime"] = Convert.ToString(ds.Tables[0].Rows[0]["AppointCreatedDateTime"]);
                ViewState["AppointStartDateTime"] = Convert.ToString(ds.Tables[0].Rows[0]["AppointStartDateTime"]);
                ViewState["AppointStartTime"] = Convert.ToString(ds.Tables[0].Rows[0]["AppointStartTime"]);
                ViewState["ReferralAppoint"] = Convert.ToString(ds.Tables[0].Rows[0]["ReferralAppoint"]);
            }


        }


        //STORE THE OLD PATIENT IMAGE FROM DATA BASE TO PHYSICAL FILE
        string byteArrayToImage(byte[] byteArrayIn)
        {
            System.Drawing.Image newImage;

            string strName = "DBF_" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strFileName = @Convert.ToString(ViewState["InsCardPath"]) + strName + ".jpeg";

            using (MemoryStream stream = new MemoryStream(byteArrayIn))
            {
                newImage = System.Drawing.Image.FromStream(stream);
                newImage.Save(@strFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

                //Bitmap bm = new Bitmap(stream);
                //bm.Save(@strFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            return strName + ".jpeg";
        }

        string byteArrayToImage1(byte[] byteArrayIn)
        {
            System.Drawing.Image newImage;

            string strName1 = "DBB_" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strFileName = Convert.ToString(ViewState["InsCardPath"]) + strName1 + ".jpeg";

            using (MemoryStream stream = new MemoryStream(byteArrayIn))
            {
                newImage = System.Drawing.Image.FromStream(stream);
                newImage.Save(@strFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

            }
            return strName1 + ".jpeg";
        }

        void UploadDBImage()
        {

            DataSet DS;
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'  AND HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True'   AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = new DataSet();
            DS = dbo.ScanCardImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //for (int i = 0; i <= DS.Tables[0].Rows.Count; i++)
                //{
                if (DS.Tables[0].Rows[0].IsNull("HSC_IMAGE_PATH_FRONT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSC_IMAGE_PATH_FRONT"]) != "")
                {
                    goto FunEnd;
                }

                // }

            }


            byte[] InsCardFront, InsCardBack;


            dboperations objOperations = new dboperations();
            Criteria = " 1=1 ";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HPP_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = new DataSet();
            DS = objOperations.PatientPhotoGet(Criteria);
            string strInsCardFront = "", strInsCardBack = "";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                {
                    InsCardFront = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
                    strInsCardFront = byteArrayToImage(InsCardFront);
                }
                else
                {
                    strInsCardFront = "";

                }

                if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD1") == false)
                {
                    InsCardBack = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD1"];
                    strInsCardBack = byteArrayToImage1(InsCardBack);
                }
                else
                {
                    strInsCardBack = "";
                }

                //UPDATE THE CARD FRIEND FRIEND AND BACK FILE NAME IN DB
                objOperations.ScanCardImageModify(txtFileNo.Text.Trim(), Convert.ToString(Session["Branch_ID"]), strInsCardFront, strInsCardBack);

            }

        FunEnd: ;

        }

        //public byte[] imageToByteArray(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        //    return ms.ToArray();
        //}

        //public byte[] imageToByteArrayPNG(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        //    return ms.ToArray();
        //}

        //public byte[] imageToByteArrayJpeg(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //    return ms.ToArray();
        //}

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        void ScanCardImageGenerate()
        {

            DataSet DS;
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'  AND HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True' AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + hidActualFileNo.Value + "'";//txtFileNo.Text.Trim() 
            Criteria += " AND HSC_POLICY_START= Convert(datetime,'" + txtPolicyStart.Text + "',103) AND  HSC_EXPIRY_DATE= Convert(datetime,'" + txtExpiryDate.Text + "',103)";
            // Criteria += " AND HSC_IMAGE_PATH_FRONT= '" + Convert.ToString(Session["InsCardFrontDefaultPath"]) + "' AND  HSC_IMAGE_PATH_BACK='" + Convert.ToString( Session["InsCardBackDefaultPath"]) +"'" ;

            DS = new DataSet();
            DS = dbo.ScanCardImageGet(Criteria);
            if (DS.Tables[0].Rows.Count == 0)
            {
                dbo.ScanCardImageUpdateOldCard(Convert.ToString(Session["Branch_ID"]), hidActualFileNo.Value);//txtFileNo.Text.Trim()
                dbo.ScanCardImageGenerate(Convert.ToString(Session["Branch_ID"]), hidActualFileNo.Value);//txtFileNo.Text.Trim()
            }
            else if (Convert.ToString(Session["InsCardFrontDefaultPath"]) == "" || Session["InsCardFrontDefaultPath"] == null)
            {
                Session["InsCardFrontDefaultPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSC_IMAGE_PATH_FRONT"]);
                Session["InsCardBackDefaultPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSC_IMAGE_PATH_BACK"]);

            }


        }


        void BindScanCardImagePath()
        {
            DataSet DS;
            string Criteria = " 1=1 ";
            Criteria += " AND HSC_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'  AND HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True' AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + hidActualFileNo.Value + "'";//txtFileNo.Text.Trim() 

            DS = new DataSet();
            DS = dbo.ScanCardImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                Session["InsCardFrontDefaultPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSC_IMAGE_PATH_FRONT"]);
                Session["InsCardBackDefaultPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSC_IMAGE_PATH_BACK"]);

            }


        }

        Boolean CheckPatientPhoto()
        {
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";
            dbo = new dboperations();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            ds = dbo.PatientPhotoGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {

                return true;

            }
            return false;
        }

        void BindPatientInfo()
        {
            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND HPI_ID = '" + Convert.ToString(ViewState["PatientInfoId"]) + "'";

            ds = dbo.PatientInfoGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_PT_FNAME"]);

                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_DOBDesc"]);


                if (txtDOB.Text != "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPI_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]) != "0")
                {
                    for (int intCount = 0; intCount < drpSex.Items.Count; intCount++)
                    {
                        if (drpSex.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPI_SEX"]);
                            goto Sex;
                        }

                    }
                }

            Sex: ;
                txtOccupation.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_OCCUPATION"]);

                txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_ADDR"]);
                txtRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_REMARKS"]);

                txtPoBox.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_POBOX"]);//need to add

                if (ds.Tables[0].Rows[0].IsNull("HPI_CITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]) != "0")
                {
                    for (int intCount = 0; intCount < drpCity.Items.Count; intCount++)
                    {
                        if (drpCity.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]))
                        {
                            drpCity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPI_CITY"]);
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;
                if (ds.Tables[0].Rows[0].IsNull("HPI_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]) != "0")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]))
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPI_NATIONALITY"]);
                            goto ForNatio;
                        }

                    }
                }
            ForNatio: ;
                txtPhone1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_PHONE1"]);
                txtMobile1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_MOBILE1"]);
                txtMobile2.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_MOBILE2"]);
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_EMAIL"]).Trim();
                txtKinName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_KIN_NAME"]);
                txtKinPhone.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPI_KIN_PHONE"]);

            }


        }


        Boolean CheckDoctorAvailable()
        {
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'  AND  HSFM_SF_STATUS='Present'";
            Criteria += " AND HSFM_STAFF_ID = '" + txtDoctorID.Text.Trim() + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(ds.Tables[0].Rows[0]["FullName"]).Trim() == txtDoctorName.Text.Trim() || Convert.ToString(ds.Tables[0].Rows[0]["FullName1"]).Trim() == txtDoctorName.Text.Trim())
                {
                    return true;
                }
            }
            return false;
        }

        Boolean CheckDoctorDetailsForMalaffi()
        {
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'  AND  HSFM_SF_STATUS='Present'";
            Criteria += " AND HSFM_STAFF_ID = '" + txtDoctorID.Text.Trim() + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HSFM_FNAME") == true || Convert.ToString(ds.Tables[0].Rows[0]["HSFM_FNAME"]) == "")
                {
                    lblStatusVisit.Text = "Doctor First Name is empty";
                    return false;
                }

                if (ds.Tables[0].Rows[0].IsNull("HSFM_LNAME") == true || Convert.ToString(ds.Tables[0].Rows[0]["HSFM_LNAME"]) == "")
                {
                    lblStatusVisit.Text = "Doctor Last Name is empty";
                    return false;
                }

                if (ds.Tables[0].Rows[0].IsNull("HSFM_MOH_NO") == true || Convert.ToString(ds.Tables[0].Rows[0]["HSFM_MOH_NO"]) == "")
                {
                    lblStatusVisit.Text = "Doctor License Number is empty";
                    return false;

                }


                if (ds.Tables[0].Rows[0].IsNull("HSFM_SPECIALITY") == true || Convert.ToString(ds.Tables[0].Rows[0]["HSFM_SPECIALITY"]) == "")
                {
                    lblStatusVisit.Text = "Doctor Speciality is empty";
                    return false;
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' DOCTOR'S DETAILS ',' Provide Existing Doctors Details','Brown')", true);
                return false;
            }

            return true;

        }

        Boolean CheckPatientNameChanged()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "' AND HPM_PT_FNAME <>'" + txtFName.Text.Trim().Replace("'", "") + "'  AND DATEDIFF(DD, CONVERT(DATETIME, HPM_CREATED_DATE,101),CONVERT(DATETIME,GETDATE(),101)) < 1 ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        Boolean CheckPatientIdAvailable()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        Boolean CheckTempImage(Int64 GCId)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTI_IMAGE_TYPE='GeneralConsent' AND HTI_PT_ID = '" + GCId + "'";
            DS = dbo.TempImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;

        }

        void BindGCGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HGC_PT_ID = '" + Convert.ToString(txtFileNo.Text) + "'";

            clsGeneralConsent objGC = new clsGeneralConsent();
            DataSet DS = new DataSet();
            DS = objGC.GeneralConsentGet(Criteria);
            ViewState["PTLastGCId"] = 0;
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["PTLastGCId"] = Convert.ToString(DS.Tables[0].Rows[0]["HGC_ID"]);
                gvGCGridView.Visible = true;
                gvGCGridView.DataSource = DS;
                gvGCGridView.DataBind();

            }
        }

        void AddSignatureTempImage(Int64 GCId)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            clsGeneralConsent objGC = new clsGeneralConsent();
            string Criteria = " 1=1 ";

            Criteria += " AND HGC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HGC_PT_ID='" + hidActualFileNo.Value + "'";

            if (GCId != 0)
            {
                Criteria += " AND HGC_ID=" + GCId;
            }

            DS = objGC.GeneralConsentGet(Criteria);

            Byte[] bytImage1 = null;
            Byte[] bytImage2 = null;
            Byte[] bytImage3 = null;
            Byte[] bytImage4 = null;

            string strCreatedUser = "";
            ViewState["HGC_ID"] = "";
            if (DS.Tables[0].Rows.Count > 0)
            {


                //BELOW CODING WILL INSERT THE SCAN CARD IMAGE IN TEMP FILE TABLE FOR SHOWING THE CARD IMAGE IN REPORT

                string strPath = Server.MapPath("../Uploads/Signature/");

                if (DS.Tables[0].Rows[0].IsNull("HGC_PT_SIGNATURE") == false)
                {

                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(DS.Tables[0].Rows[0]["HGC_PT_SIGNATURE"]));
                    bytImage1 = ImageToByte(image);

                    //Session["SignatureImg1"] = bytImage1;
                    //imgSig1.ImageUrl = "~/DisplaySignature1.aspx";
                }

                if (DS.Tables[0].Rows[0].IsNull("HGC_SUBS_SIGNATURE") == false)
                {

                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(DS.Tables[0].Rows[0]["HGC_SUBS_SIGNATURE"]));
                    bytImage2 = ImageToByte(image);
                }

                if (DS.Tables[0].Rows[0].IsNull("HGC_TRAN_SIGNATURE") == false)
                {

                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(DS.Tables[0].Rows[0]["HGC_TRAN_SIGNATURE"]));
                    bytImage3 = ImageToByte(image);
                }

                if (DS.Tables[0].Rows[0].IsNull("HGC_CREATED_USER") == false)
                {
                    strCreatedUser = Convert.ToString(DS.Tables[0].Rows[0]["HGC_CREATED_USER"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("HGC_ID") == false)
                {
                    ViewState["HGC_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HGC_ID"]);
                }



                DataSet DS1 = new DataSet();
                Criteria = " 1=1 ";
                Criteria += " AND HSP_STAFF_ID = '" + Convert.ToString(strCreatedUser) + "'";

                DS1 = dbo.StaffPhotoGet(Criteria);
                if (DS1.Tables[0].Rows.Count > 0)
                {
                    if (DS1.Tables[0].Rows[0].IsNull("HSP_SIGNATURE") == false)
                    {
                        bytImage4 = (byte[])DS1.Tables[0].Rows[0]["HSP_SIGNATURE"];

                    }

                }


                //hidActualFileNo.Value.Trim()
                if (CheckTempImage(GCId) == false)
                {
                    dbo.TempImageAdd1(Convert.ToString(GCId), Convert.ToString(Session["Branch_ID"]), bytImage1, bytImage2, bytImage3, bytImage4, "GeneralConsent");
                }



            }

           // BindTempImage();


        FunEnd: ;
        }



        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.CommonMastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ////if (strType == "PT_CATEGORY")
                ////{
                ////    drpPTCategory.DataSource = DS;
                ////    drpPTCategory.DataTextField = "HCM_DESC";
                ////    drpPTCategory.DataValueField = "HCM_CODE";
                ////    drpPTCategory.DataBind();
                ////}


                if (strType == "TRIAGE_REASON")
                {
                    drpTriageReason.DataSource = DS;
                    drpTriageReason.DataTextField = "HCM_DESC";
                    drpTriageReason.DataValueField = "HCM_CODE";
                    drpTriageReason.DataBind();
                }


                if (strType == "AddressState")
                {
                    drpEmirates.DataSource = DS;
                    drpEmirates.DataTextField = "HCM_DESC";
                    drpEmirates.DataValueField = "HCM_CODE";
                    drpEmirates.DataBind();
                }


                if (strType == "Religion")
                {
                    drpReligion.DataSource = DS;
                    drpReligion.DataTextField = "HCM_DESC";
                    drpReligion.DataValueField = "HCM_CODE";
                    drpReligion.DataBind();
                }


                if (strType == "Relationship")
                {
                    drpKinRelation.DataSource = DS;
                    drpKinRelation.DataTextField = "HCM_DESC";
                    drpKinRelation.DataValueField = "HCM_CODE";
                    drpKinRelation.DataBind();
                }

                if (strType == "PTREGTYPE")
                {
                    drpPtRegisteredType.DataSource = DS;
                    drpPtRegisteredType.DataTextField = "HCM_DESC";
                    drpPtRegisteredType.DataValueField = "HCM_CODE";
                    drpPtRegisteredType.DataBind();
                }



            }
            ////if (strType == "PT_CATEGORY")
            ////{
            ////    drpPTCategory.Items.Insert(0, "--- Select ---");
            ////    drpPTCategory.Items[0].Value = "";
            ////}
            if (strType == "TRIAGE_REASON")
            {
                drpTriageReason.Items.Insert(0, "--- Select ---");
                drpTriageReason.Items[0].Value = "";
            }

            if (strType == "AddressState")
            {
                drpEmirates.Items.Insert(0, "--- Select ---");
                drpEmirates.Items[0].Value = "";
            }


            if (strType == "Religion")
            {
                drpReligion.Items.Insert(0, "--- Select ---");
                drpReligion.Items[0].Value = "";
            }

            if (strType == "Relationship")
            {
                drpKinRelation.Items.Insert(0, "--- Select ---");
                drpKinRelation.Items[0].Value = "";
            }
            if (strType == "PTREGTYPE")
            {
                drpPtRegisteredType.Items.Insert(0, "--- Select ---");
                drpPtRegisteredType.Items[0].Value = "0";
            }

        }





        void BindPlanPopup()
        {

            string Criteria = " 1=1 ";
            CommonBAL objCom = new CommonBAL();

            if (drpParentCompany.SelectedIndex != 0)
            {
                Criteria += " and HCM_COMP_ID = '" + drpParentCompany.Text.Trim() + "'";
            }

            if (txtSrcPlanName.Text != "")
            {
                Criteria += " and HCB_CAT_TYPE like '" + txtSrcPlanName.Text.Trim() + "%'";
            }



            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue("HCM_COMP_ID, HCM_NAME ,HCB_CAT_TYPE,HCM_PACKAGE_DETAILS ", "HMS_COMPANY_MASTER AS  HCM LEFT JOIN HMS_COMP_BENEFITS AS HCB ON HCM.HCM_COMP_ID = HCB.HCB_COMP_ID", Criteria, "HCM_NAME");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPlanPopup.DataSource = DS;
                gvPlanPopup.DataBind();
            }
            else
            {
                gvPlanPopup.DataBind();
            }
        }

        void BindProviderDtls()
        {
            DataSet DS = new DataSet();
            CommonBAL objphyCom = new CommonBAL();

            string Criteria = " 1=1 AND HAC_STATUS='A'  AND  HAC_PROVIDER_ID='" + GlobalValues.FacilityID + "'";
            DS = objphyCom.AuthorizationCredentialGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {



                if (DS.Tables[0].Rows[0].IsNull("HAC_PROVIDER_ID") == false)
                {
                    ViewState["PROVIDER_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HAC_PROVIDER_ID"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HAC_UPLOAD_FILEPATH") == false)
                {
                    ViewState["UPLOAD_FILEPATH"] = Convert.ToString(DS.Tables[0].Rows[0]["HAC_UPLOAD_FILEPATH"]);

                    //if (!Directory.Exists(Convert.ToString(ViewState["UPLOAD_FILEPATH"])))
                    //{
                    //    if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                    //    {
                    //        Directory.CreateDirectory(Convert.ToString(ViewState["UPLOAD_FILEPATH"]));

                    //    }
                    //}
                }

                if (DS.Tables[0].Rows[0].IsNull("HAC_ERROR_FILEPATH") == false)
                {
                    ViewState["HAC_ERROR_FILEPATH"] = Convert.ToString(DS.Tables[0].Rows[0]["HAC_ERROR_FILEPATH"]);
                    //if (!Directory.Exists(Convert.ToString(ViewState["HAC_ERROR_FILEPATH"])))
                    //{
                    //    if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                    //    {
                    //        Directory.CreateDirectory(Convert.ToString(ViewState["HAC_ERROR_FILEPATH"]));

                    //    }
                    //}
                }




                if (DS.Tables[0].Rows[0].IsNull("HAC_LOGIN_ID") == false)
                {
                    ViewState["LOGIN_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HAC_LOGIN_ID"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HAC_LOGIN_PASSWORD") == false)
                {
                    ViewState["LOGIN_PASSWORD"] = Convert.ToString(DS.Tables[0].Rows[0]["HAC_LOGIN_PASSWORD"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("HAC_DISPOSITION_FLAG") == false)
                {
                    ViewState["HAC_DISPOSITION_FLAG"] = Convert.ToString(DS.Tables[0].Rows[0]["HAC_DISPOSITION_FLAG"]);
                }


            }
        }

        void BindNurses()
        {
            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Nurse'";


            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_doctor_detailss(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpNurse.DataSource = DS;
                drpNurse.DataTextField = "FullName";
                drpNurse.DataValueField = "HSFM_STAFF_ID";
                drpNurse.DataBind();
            }
            drpNurse.Items.Insert(0, "--- Select ---");
            drpNurse.Items[0].Value = "";
        }

        void BindEnquiryMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HCEM_STATUS='Active' ";

            string strStartDate = txtEnqFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }



            if (txtEnqFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HCEM_DATE,101),101) >= '" + strForStartDate + "'";
            }




            string strTotDate = txtEnqToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtEnqToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HCEM_DATE,101),101) <= '" + strForToDate + "'";
            }


            if (txtEnquirSearch.Text.Trim() != "")
            {
                Criteria += " AND (HCEM_FNAME LIKE '%" + txtEnquirSearch.Text.Trim() + "%' OR  HCEM_PHONE LIKE '" + txtEnquirSearch.Text.Trim() + "%' OR   HCEM_MOBILE LIKE '" + txtEnquirSearch.Text.Trim() + "%')";
            }

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*,CONVERT(VARCHAR,HCEM_DATE,103)AS HCEM_DATEDesc ", "HMS_CUSTOMER_ENQUIRY_MASTER ", Criteria, "HCEM_DATE desc");

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvEnquiryMaster.DataSource = DS;

                gvEnquiryMaster.DataBind();
            }
            else
            {
                gvEnquiryMaster.DataBind();
            }


        }

        void BindReferral()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = objCom.fnGetFieldValue("  convert(varchar(10),EPR_CREATED_dATE,103) as EPR_CREATED_DATEDesc, * ", "EMR_PT_MASTER INNER JOIN EMR_PT_REFERNCE ON EPM_ID= EPR_ID", Criteria, "EPR_CREATED_DATE Desc");
            if (DS.Tables[0].Rows.Count > 0)
            {

                gvReferral.DataSource = DS;
                gvReferral.DataBind();

            }

        FunEnd: ;
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = txtFileNo.Text;
            objCom.ID = "";
            objCom.ScreenID = "PATIENTREG";
            objCom.ScreenName = "Patient Registration";
            objCom.ScreenType = "TRANSACTION";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }


        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='PATIENTREG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                // chkUpdateWaitList.Enabled = false;
                btnNew.Enabled = false;
                bntFamilyApt.Enabled = false;
                btnScan.Enabled = false;
                lnkEmIdShow.Enabled = false;
                btnSave.Enabled = false;
                btnClear.Enabled = false;
                btnBalance.Visible = false;

                btnScanCardAdd.Enabled = false;
                btnScanCardUpdate.Enabled = false;

                AsyncFileUpload1.Enabled = false;
                AsyncFileUpload2.Enabled = false;

                btnSaveVisit.Enabled = false;


            }


            if (strPermission == "7")
            {
                // chkUpdateWaitList.Enabled = false;
                btnNew.Enabled = false;
                bntFamilyApt.Enabled = false;
                btnScan.Enabled = false;
                lnkEmIdShow.Enabled = false;
                btnSave.Enabled = false;
                btnClear.Enabled = false;
                btnBalance.Visible = false;

                btnScanCardAdd.Enabled = false;
                btnScanCardUpdate.Enabled = false;

                AsyncFileUpload1.Enabled = false;
                AsyncFileUpload2.Enabled = false;

                btnSaveVisit.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?PageName=Registration");
            }
        }
        # endregion

        #region AutoCompleteExtender

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            strCompName = "";
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            if (stParentName != "0" && stParentName != "")
            {
                Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE like '%" + stParentName + "%' and HCM_COMP_ID Like '%" + prefixText + "%'";
            }
            else if (strTPA == "false")
            {
                Criteria += " AND  HCM_COMP_ID =  HCM_BILL_CODE AND HCM_COMP_ID Like '%" + prefixText + "%'";
            }
            else
            {
                Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";
            }

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_NAME"]);


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            if (stParentName != "0" && stParentName != "")
            {
                // Criteria += " AND HCM_NAME Like '%" + prefixText + "%' AND HCM_COMP_ID in ( select HCM_COMP_ID from HMS_COMPANY_MASTER where HCM_COMP_ID !=  HCM_BILL_CODE AND  HCM_BILL_CODE like '" + stParentName + "%' ) ";
                Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE like '%" + stParentName + "%' and HCM_NAME Like '%" + prefixText + "%'";
            }
            else if (strTPA == "false")
            {
                Criteria += " AND  HCM_COMP_ID =  HCM_BILL_CODE  AND HCM_NAME Like '%" + prefixText + "%'";
            }
            else
            {
                Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";
            }


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_NAME"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            strDrName = "";
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorName(string prefixText)
        {
            string[] Data = null;

            string criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";
            criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds = new DataSet();
            StaffMasterBAL objStaff = new StaffMasterBAL();
            ds = objStaff.GetStaffMaster(criteria);
            int StaffCount = ds.Tables[0].Rows.Count;


            criteria = " 1=1 ";
            criteria += " AND HRDM_FNAME + ' ' +isnull(HRDM_MNAME,'') + ' '  + isnull(HRDM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds1 = new DataSet();
            dboperations dbo = new dboperations();
            ds1 = dbo.RefDoctorMasterGet(criteria);
            int RefStaffCount = ds1.Tables[0].Rows.Count;

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[StaffCount + RefStaffCount];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]).Trim();
                }

            }


            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                {
                    Data[StaffCount + j] = Convert.ToString(ds1.Tables[0].Rows[j]["HRDM_REF_ID"]).Trim() + "~" + Convert.ToString(ds1.Tables[0].Rows[j]["FullName"]).Trim();
                }

                return Data;
            }
            else
            {
                return Data;
            }

            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetPatientCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HPC_PATIENT_COMPANY Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.PatientCompanyGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HPC_PATIENT_COMPANY"]);
                }

                return Data;
            }
            string[] Data1 = { "" };
            return Data1;

        }


        [System.Web.Services.WebMethod]
        public static string[] GetGeneralClass(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            if (stParentName1 != "0" && stParentName1 != "")
            {
                Criteria += " AND HCM_COMP_ID = '" + stParentName1 + "'";
            }

            Criteria += " AND HCM_PACKAGE_DETAILS Like '%" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPackage = Convert.ToString(ds.Tables[0].Rows[0]["HCM_PACKAGE_DETAILS"]);
                string[] arrPackage = strPackage.Split('|');

                Data = new string[arrPackage.Length];
                for (int i = 0; i < arrPackage.Length; i++)
                {
                    Data[i] = arrPackage[i];
                }

                return Data;
            }
            string[] Data1 = { "" };
            return Data1;

        }

        [System.Web.Services.WebMethod]
        public static string[] GetPlanType(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1  AND  HCB_COMP_ID ='" + strCompID + "'";
            Criteria += " AND HCB_CAT_TYPE Like '%" + prefixText + "%'";
            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.CompBenefitsGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HCB_CAT_TYPE"]);


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            lblStatusTab2.Text = "";
            lblStatusVisit.Text = "";

            if (hidLocalDate.Value != "")
            {
                Session["LocalDate"] = hidLocalDate.Value;
            }



            if (!IsPostBack)
            {


                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                AuditLogAdd("OPEN", "Open Patient Registration Page");

                //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(""); }

                // string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
                // ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {
                    lnkMalaffi.Visible = true;
                }


                try
                {
                    ViewState["InsCardPath"] = (string)System.Configuration.ConfigurationSettings.AppSettings["InsCardPath"];
                    hidInsCardPath.Value = (string)System.Configuration.ConfigurationSettings.AppSettings["InsCardPath"];

                    ViewState["Secondary_Ins"] = "";
                    Session["InsCardFrontPath"] = "";
                    Session["InsCardBackPath"] = "";
                    ViewState["Patient_ID_VALID"] = "0";
                    ViewState["PatientInfoId"] = "";

                    ViewState["Signature1"] = "";
                    ViewState["Signature2"] = "";
                    ViewState["Signature3"] = "";



                    ViewState["EmirateIdRef"] = "";
                    Session["Photo"] = "";
                    ViewState["LoadFromEmiratesID"] = "0";

                    ViewState["FileDescription"] = GlobalValues.FileDescription.ToUpper();

                    //  if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad() 1"); }

                    BindScreenCustomization();
                    BindProviderDtls();
                    // if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad() 2"); }


                    if (Convert.ToString(ViewState["EID_TYPE"]) == "FEITIAN")
                    {
                        txtEmirateIDData.Visible = true;
                        txtEmirateIDData.Focus();
                    }

                    if (Convert.ToString(ViewState["EID_TYPE"]) == "WEB")
                    {

                        btnReadEID.Visible = true;
                    }

                    if (Convert.ToString(ViewState["EID_TYPE"]) == "EXE")
                    {

                        btnScan.Visible = true;
                        lnkEmIdShow.Visible = true;

                    }


                    hidHospitalName.Value = GlobalValues.HospitalName;

                    Form.DefaultButton = btnNew.UniqueID;
                    stParentName = "0";

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    txtLablePrintCount.Text = Convert.ToString(Session["LabelCopies"]);

                    txtEnqFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtEnqToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");


                    if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
                    {
                        drpPatientType.SelectedIndex = 1;
                    }
                    else
                    {
                        drpPatientType.SelectedIndex = 0;
                    }


                    if (Convert.ToString(Session["IDPrint"]).ToUpper() == "Y")
                    {
                        btnIDPrint.Visible = true;
                    }
                    else
                    {
                        btnIDPrint.Visible = false;
                    }



                    if (Convert.ToString(Session["ShowToken"]).ToUpper() == "Y")
                    {
                        divToken.Visible = true;
                    }
                    else
                    {
                        divToken.Visible = false;
                    }


                    if (strTPA.ToLower() == "true")
                    {

                        trTPA.Visible = true;
                    }
                    else
                    {

                        trTPA.Visible = false;
                    }


                    if (strNetwork.ToLower() == "true")
                    {
                        tdCaptionNetwork.Visible = true;
                        tdNetwork.Visible = true;
                    }
                    else
                    {
                        tdCaptionNetwork.Visible = false;
                        tdNetwork.Visible = false;
                    }

                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        btnHaadRegister.Visible = true;
                        lblHAADRegisterCaption.Visible = true;
                        lblHAADRegister.Visible = true;

                        TabPanelSignature.Visible = true;
                        imgPlanPopup.Visible = false;
                    }

                    if (PTRegChangePolicyNoCaption.ToLower() == "true")
                    {
                        if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                        {
                            lblPolicyNo.Text = "Member ID";
                            lblPolicyNo1.Text = "Member ID";
                            lblCardNo.Text = "Policy No.";
                            lblCardNo1.Text = "Policy No.";
                        }

                        if (GlobalValues.FileDescription.ToUpper() == "ALAMAL")
                        {
                            lblPolicyNo.Text = "Card No.";
                            lblPolicyNo1.Text = "Card No.";
                            tdCardNo.Visible = false;
                        }

                    }


                    //  if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad() 4"); }
                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {

                        bntFamilyApt.Visible = false;
                        btnScan.Visible = false;
                        lnkEmIdShow.Visible = false;
                        chkStatus.Visible = false;
                        chkCompCashPT.Visible = false;
                        chkFamilyMember.Visible = false;
                        //lblOfficePhNo.Visible = false;
                        txtPhone3.Enabled = false;

                        //  lblOccupation.Visible = false;
                        txtOccupation.Enabled = false;

                        //  lblFax.Visible = false;
                        txtFax.Enabled = false;
                        //  lblRefDr.Visible = false;
                        txtRefDoctorID.Enabled = false;
                        txtRefDoctorName.Enabled = false;


                        //  lblID.Visible = false;
                        txtPolicyId.Enabled = false;

                        btnMRRPrint.Visible = false;

                        txtLablePrintCount.Visible = false;
                        chkLabelPrint.Visible = false;
                        btnLabelPrint.Visible = false;

                        // lblPOBox.Visible = false;
                        txtPoBox.Enabled = false;
                        ///  lblBlood.Visible = false;
                        drpBlood.Enabled = false;


                        txtPhone3.Enabled = false;
                        txtEmail.Enabled = false;
                        txtFax.Enabled = false;

                        txtOccupation.Enabled = false;
                        drpKnowFrom.Enabled = false;

                        txtRefDoctorID.Enabled = false;
                        txtRefDoctorName.Enabled = false;



                        // trOfficePh.Visible = false;
                        // trOccupation.Visible = false;
                        // trRefDr.Visible = false;
                    }


                    if (Convert.ToString(Session["PackageManditory"]).ToUpper() == "Y")
                    {

                        lblNetworkClassMand.Visible = true;


                    }


                    if (Convert.ToString(Session["ConInsPTReg"]).ToUpper() == "Y")
                    {
                        divDeductCoIns.Visible = true;
                    }
                    else
                    {
                        divDeductCoIns.Visible = false;
                    }



                    if (Convert.ToString(Session["PriorityDisplay"]).ToUpper() == "Y")
                    {
                        divPriority.Visible = true;
                    }
                    else
                    {
                        divPriority.Visible = false;
                    }

                    ////if (Convert.ToString(Session["KnowFromDisplay"]).ToUpper() == "Y")
                    ////{
                    ////    drpKnowFrom.Visible = true;
                    ////    lblKnowFrom.Visible = true;

                    ////}
                    ////else
                    ////{
                    ////    drpKnowFrom.Visible = false;
                    ////    lblKnowFrom.Visible = false;
                    ////}

                    if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                    {
                        divDept.Visible = true;
                    }
                    else
                    {
                        divDept.Visible = false;
                    }


                    if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1")
                    {
                        chkRequiredTriage.Visible = true;
                    }



                    if (Convert.ToString(Session["PTNameMultiple"]) != "" && Convert.ToString(Session["PTNameMultiple"]) == "N")
                    {
                        txtMName.Visible = false;
                        txtLName.Visible = false;
                        txtFName.Width = 250;

                    }

                    //TabContainer1.ActiveTab = TabPanelGeneral;
                    //  txtFileNo.Focus();
                    // txtEmirateIDData.Focus();
                    hidTodayDate.Value = System.DateTime.Now.ToString("dd/MM/yyyy");
                    hidTodayTime.Value = System.DateTime.Now.ToShortTimeString();
                    hidMultipleVisitADay.Value = Convert.ToString(Session["MultipleVisitADay"]);

                    txtExpiryDate.Text = "31/12/" + DateTime.Now.Year.ToString();
                    txtCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();
                    txtPTVisitCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();

                    //  if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    // {
                    txtPolicyStart.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");


                    // }
                    //else
                    //{
                    //    txtEffectiveDate.Text = hidLocalDate.Value;
                    //}


                    Session["InsCardFrontPath"] = "";
                    Session["InsCardBackPath"] = "";

                    Session["status"] = "A";
                    ViewState["Patient_ID_VALID"] = "0";

                    Session["IDPath"] = null;
                    Session["SignaturePath"] = null;
                    Session["CompcashPt"] = "Y";
                    ViewState["PatientInfoId"] = "";

                    Year.Value = DateTime.Now.Year.ToString();

                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad() 5"); }

                    BindDepartment();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindCity() Start"); } 
                    BindCity();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindCountry() Start"); }

                    BindCountry();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindArea() Start"); } 
                    BindArea();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindNationality() Start"); } 
                    BindNationality();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ CompanyParentGet() Start"); } 
                    CompanyParentGet();
                    BindPTVisitParentGet();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindDefaultCompany() Start"); } 
                    BindDefaultCompany();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BinScanCardMasterGet() Start"); }
                    BinScanCardMasterGet();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindIDCaptionMaster() Start"); }
                    BindIDCaptionMaster();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindPriority() Start"); }
                    BindPriority();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindKnowFrom() Start"); }
                    BindKnowFrom();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindDeductibleType() Start"); }
                    BindDeductibleType();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindCoInsuranceType() Start"); }
                    BindCoInsuranceType();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindPatientTitle() Start"); }
                    BindPatientTitle();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindPayer() Start"); }
                    BindPayer();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindConsentForm() Start"); }
                    BindConsentForm();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindPlanPopup() Start"); }
                    ////  BindPlanPopup();
                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindCommonMaster() Start"); }

                    //// BindCommonMaster("PT_CATEGORY");
                    BindCommonMaster("TRIAGE_REASON");
                    BindCommonMaster("AddressState");
                    BindCommonMaster("Religion");
                    BindCommonMaster("Relationship");
                    BindCommonMaster("PTREGTYPE");

                    BindPackageServices();

                    BindNurses();

                    BindPTVisitCompany();
                    BindPTVisitCompanyPlan();

                    BindStaffList();
                    if (Convert.ToString(Session["PackageManditory"]).ToUpper() == "Y")
                    {

                        //txtNetworkClass.BorderColor = System.Drawing.Color.Red;
                        //txtNetworkClass.BorderWidth = 1;
                    }

                    if (Convert.ToString(Session["PriorityDisplay"]).ToUpper() == "Y")
                    {

                        // drpPriority.BorderColor = System.Drawing.Color.Red;
                        // drpPriority.BorderWidth = 1;
                    }

                    //if (Convert.ToString(ViewState["EmiratesIdMandatory"]) == "1")
                    //{
                    //    txtEmiratesID.BorderColor = System.Drawing.Color.Red;
                    //    txtEmiratesID.BorderWidth = 1;
                    //}

                    ////if (Convert.ToString(ViewState["NationalityMandatory"]) == "1")
                    ////{
                    ////    drpNationality.BorderColor = System.Drawing.Color.Red;
                    ////    drpNationality.BorderWidth = 1;
                    ////}

                    ////if (Convert.ToString(ViewState["CityMandatory"]) == "1")
                    ////{
                    ////    drpCity.BorderColor = System.Drawing.Color.Red;
                    ////    drpCity.BorderWidth = 1;
                    ////}

                    //if (Convert.ToString(ViewState["VisaTypeMandatory"]) == "1")
                    //{
                    //    drpVisaType.BorderColor = System.Drawing.Color.Red;
                    //    drpVisaType.BorderWidth = 1;
                    //}



                    if (Convert.ToString(ViewState["AgeMandatory"]) == "1")
                    {
                        lblDBOMand.Visible = true;
                        //txtAge.BorderColor = System.Drawing.Color.Red;
                        //txtAge.BorderWidth = 1;
                    }

                    if (Convert.ToString(ViewState["DeductibleMandatory"]) == "1")
                    {
                        //txtDeductible.BorderColor = System.Drawing.Color.Red;
                        txtDeductible.BorderWidth = 1;
                    }

                    ////if (Convert.ToString(ViewState["OTHER_INFO_DISPLAY"]) == "1")
                    ////{
                    ////    divOtherInfo.Visible = true;
                    ////}

                    if (Convert.ToString(ViewState["PT_COMP_ADD"]) == "1")
                    {
                        lblPTCompany.Text = "PT Company";
                        txtPTCompany.Visible = true;
                        drpPayer.Visible = false;
                    }

                    if (Convert.ToString(ViewState["PT_TITLE_NAME"]) == "1")
                    {
                        drpTitle.Visible = true;
                    }

                    if (Convert.ToString(ViewState["PLAN_TYPE_STORE_SCAN_CARD"]) == "1")
                    {
                        //drpPlanType.Enabled = false;
                        lblPlanType.Visible = true;
                        txtPlanType.Visible = true;
                    }


                    if (Convert.ToString(ViewState["CityMandatory"]) == "1")
                    {
                        lblCityMand.Visible = true;
                    }

                    if (Convert.ToString(ViewState["CountryMandatory"]) == "1")
                    {
                        lblCountryMand.Visible = true;
                    }
                    if (Convert.ToString(ViewState["ECLAIM"]) == "AUH")
                    {
                        lblPtRegType.Visible = true;
                    }

                    if (Convert.ToString(ViewState["EMIRATES_ID"]) == "1")
                    {
                        lblEmiratesIDMand.Visible = true;
                    }


                    if (Convert.ToString(ViewState["VISATYPE"]) == "1")
                    {
                        lblVisaTypeMand.Visible = true;
                    }

                    if (Convert.ToString(ViewState["REQ_POLICY_NO"]) == "1")
                    {
                        lblPolicyNoMand.Visible = true;
                    }
                    else
                    {
                        lblPolicyNoMand.Visible = false;
                    }

                    if (Convert.ToString(ViewState["REQ_CARD_NO"]) == "1")
                    {
                        lblCardNo1Mand.Visible = true;
                    }
                    else
                    {
                        lblCardNo1Mand.Visible = false;
                    }

                    if (Convert.ToString(ViewState["REQ_PT_COMPANY"]) == "1")
                    {
                        lblPTCompanyMand.Visible = true;
                    }
                    else
                    {
                        lblPTCompanyMand.Visible = false;
                    }


                    if (Convert.ToString(ViewState["REQ_ELIGIBILITY_ID"]) == "1")
                    {
                        lblEligibilityIDMand.Visible = true;
                    }
                    else
                    {
                        lblEligibilityIDMand.Visible = false;
                    }



                    if (Convert.ToString(ViewState["REQ_EMAIL_ID"]) == "1")
                    {
                        lblEmailMand.Visible = true;
                    }
                    else
                    {
                        lblEmailMand.Visible = false;
                    }

                    if (Convert.ToString(ViewState["REQ_MARITAL_STATUS"]) == "1")
                    {
                        lblMStatusMand.Visible = true;
                    }
                    else
                    {
                        lblMStatusMand.Visible = false;
                    }




                    if (Convert.ToString(ViewState["REQ_RELIGION"]) == "1")
                    {
                        lblReligionMand.Visible = true;
                    }
                    else
                    {
                        lblReligionMand.Visible = false;
                    }


                    if (Convert.ToString(ViewState["REQ_ADDRESS"]) == "1")
                    {
                        lblAddressMand.Visible = true;
                    }
                    else
                    {
                        lblAddressMand.Visible = false;
                    }


                    if (Convert.ToString(ViewState["REQ_EMIRATES"]) == "1")
                    {
                        lblEmiratesMand.Visible = true;
                    }
                    else
                    {
                        lblEmiratesMand.Visible = false;
                    }





                    string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);


                    switch (AutoSlNo)
                    {
                        case "A":
                            chkManual.Checked = false;
                            chkManual.Visible = false;
                            btnNew.Visible = true;
                            break;
                        case "M":
                            chkManual.Checked = true;
                            chkManual.Visible = false;
                            txtFileNo.ReadOnly = false;
                            btnNew.Visible = false;
                            break;
                        case "B":
                            //chkManual.Checked = true;
                            chkManual.Visible = true;
                            btnNew.Visible = true;
                            break;
                        default:
                            break;

                    }

                    if (Convert.ToString(Session["AutoSlNo"]) == "A")
                    {

                        chkManual.Enabled = false;
                    }


                    //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BuildScanCard() Start"); }
                    BuildScanCard();

                    Session["Incoming"] = "";
                    // }
                    string PatientId = "", PatientInfoId = "", HPVSeqNo = "";

                    ViewState["HPVSeqNo"] = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    PatientInfoId = Convert.ToString(Request.QueryString["PatientInfoId"]);
                    HPVSeqNo = Convert.ToString(Request.QueryString["HPVSeqNo"]);


                    if (PatientId == "" || PatientId == null)
                    {
                        if (Convert.ToString(Session["PatientID"]) != "")
                        {

                            PatientId = Convert.ToString(Session["PatientID"]);
                        }
                    }


                    hidPateName.Value = Convert.ToString(Request.QueryString["PageName"]);

                    if (hidPateName.Value == "HomeCare")
                    {
                        // chkUpdateWaitList.Checked = false;
                        //txtDoctorID.BorderColor = System.Drawing.ColorTranslator.FromHtml("#cccccc");
                        lblHomeCarePT.Visible = true;
                    }
                    else
                    {
                        // chkUpdateWaitList.Checked = true;
                        // txtDoctorID.BorderColor = System.Drawing.Color.Red;
                        lblHomeCarePT.Visible = false;
                    }

                    ViewState["PatientId"] = PatientId;
                    ViewState["PatientInfoId"] = PatientInfoId;
                    ViewState["HPVSeqNo"] = HPVSeqNo;

                    if (PatientId != " " && PatientId != null)
                    {

                        txtFileNo.Text = Convert.ToString(PatientId);
                        //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindModifyData() Start"); }
                        BindModifyData();
                        //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindModifyData() End"); }

                    }

                    if (PatientInfoId != " " && PatientInfoId != null)
                    {
                        //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindPatientInfo() Start"); }
                        BindPatientInfo();
                        //if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1") { PTRegProcessTimLog(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "PageLoad_ BindPatientInfo() End"); }

                    }

                    if (Convert.ToString(ViewState["CUST_ENQUIRY_DISPLAY"]) == "1")
                    {
                        lnkEnquiryMaster.Visible = true;
                        BindEnquiryMaster();
                    }


                }
                catch (Exception ex)
                {

                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                btnSave.Enabled = true;
                btnSaveVisit.Enabled = true;

                hidActualFileNo.Value = "";
                ViewState["Patient_ID_VALID"] = "0";
                ViewState["LoadFromEmiratesID"] = "0";
                // FOR ADD THE FILE NO PREFIX YEAR AND MONTH LIKSE "1306"
                string strMonth = "", strYear = "", strPrefix = "";
                if (Convert.ToString(Session["FileNoMonthYear"]).ToUpper() == "Y")
                {
                    strMonth = System.DateTime.Now.Month.ToString();
                    if (strMonth.Length == 1)
                    {
                        strMonth = "0" + strMonth;
                    }
                    strYear = System.DateTime.Now.Year.ToString();
                    strYear = strYear.Substring(2, 2);
                    strPrefix = strYear + strMonth;
                }

                if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                {
                    txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), drpDepartment.SelectedValue);
                }
                else
                {
                    txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PATIENTREG");
                }
                if (Convert.ToString(ViewState["PatientInfoId"]) == "")
                {
                    Clear();
                }
                // txtFileNo.Focus();

                if (Convert.ToString(ViewState["EID_TYPE"]) == "FEITIAN")
                {
                    txtEmirateIDData.Text = "";
                    txtEmirateIDData.Focus();
                }



                chkManual.Checked = false;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void chkManual_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkManual.Checked == true)
                {
                    if (chkFamilyMember.Checked == false)
                    {
                        txtFileNo.Text = "";
                    }
                    txtFileNo.ReadOnly = false;

                }
                else
                {
                    txtFileNo.ReadOnly = true;
                    // FOR ADD THE FILE NO PREFIX YEAR AND MONTH LIKSE "1306"
                    string strMonth = "", strYear = "", strPrefix = "";
                    if (Convert.ToString(Session["FileNoMonthYear"]).ToUpper() == "Y")
                    {
                        strMonth = System.DateTime.Now.Month.ToString();
                        if (strMonth.Length == 1)
                        {
                            strMonth = "0" + strMonth;
                        }
                        strYear = System.DateTime.Now.Year.ToString();
                        strYear = strYear.Substring(2, 2);
                        strPrefix = strYear + strMonth;
                    }

                    if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                    {
                        txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), drpDepartment.SelectedValue);
                    }
                    else
                    {
                        txtFileNo.Text = strPrefix.Trim() + dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PATIENTREG");
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.chkManual_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        bool ReturnValue()
        {
            return false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + txtFileNo.Text.Trim() + " Patient Registration btnSave_Click Start" + strNewLineChar);
            btnSave.Enabled = false;
            try
            {


                if (drpPatientType.SelectedValue == "CR")
                {

                    if (CheckCompanyActive(drpDefaultCompany.SelectedValue) == false)
                    {



                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' COMPANY NOT ACTIVE ',' Selected company is not active.','Brown')", true);

                    }
                }



                // TextFileWriting(System.DateTime.Now.ToString() + " PTRegistration 2");


                if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                {
                    // TextFileWriting(System.DateTime.Now.ToString() + " PTRegistration 2.1");
                    if (Convert.ToString(ViewState["ConfirmResult"]) != "Yes")
                    {
                        // TextFileWriting(System.DateTime.Now.ToString() + " PTRegistration 2.2");
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click CheckPatientNameChanged() Start"+ strNewLineChar);
                        if (CheckPatientNameChanged() == true)
                        {
                            // TextFileWriting(System.DateTime.Now.ToString() + " PTRegistration 2.3");
                            // divConfirmMessageClose.Style.Add("display", "block");
                            divConfirmMessage.Style.Add("display", "block");
                            lblConfirmMessage.Text = "Patient Name is differed from Old name, Do you want to Continue ?";

                            ViewState["ConfirmMsgFor"] = "DuplicateNameSave";

                            ViewState["ConfirmResult"] = "";

                            goto SaveEnd;

                        }

                    }

                    ViewState["ConfirmResult"] = "";
                }
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + txtFileNo.Text.Trim() + " Patient Registration CheckPatientNameChanged() End" + strNewLineChar);

                if (Convert.ToString(ViewState["REQ_EID_DUPLICATE_CHECK"]) == "1")
                {
                    DataSet DSIqamaNo = new DataSet();
                    string CriteriaIqamaNo = " HPM_IQAMA_NO  = '" + txtEmiratesID.Text.Trim() + "' AND HPM_PT_ID !='" + txtFileNo.Text.Trim() + "'";

                    CommonBAL objCom = new CommonBAL();

                    DSIqamaNo = objCom.fnGetFieldValue("TOP 1 HPM_IQAMA_NO", "HMS_PATIENT_MASTER", CriteriaIqamaNo, "");
                    if (DSIqamaNo.Tables[0].Rows.Count > 0)
                    {
                        btnSave.Enabled = true;
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' DUPLICATE EMIRATES ID OR PASSPORT NUMBER',' Duplicate Emirates ID or Passport Number','red')", true);
                        goto SaveEnd;
                    }

                }

                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + txtFileNo.Text.Trim() + " Patient Registration  EID_DUPLICATE_CHECK End" + strNewLineChar);


                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {

                    string strCountryCode = GetCountryCode(drpCountry.SelectedValue);
                    if (strCountryCode == "")
                    {
                        btnSave.Enabled = true;
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Country Code is Empty.','Red')", true);
                        goto SaveEnd;
                    }


                    string NationalityCode = GetNationalityCode(drpNationality.SelectedValue);
                    if (NationalityCode == "")
                    {
                        btnSave.Enabled = true;
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Nationality Code is Empty.','Red')", true);
                        goto SaveEnd;

                    }
                }

                ClientScriptManager CSM = Page.ClientScript;
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + txtFileNo.Text.Trim() + " Patient Registration  Validation End" + strNewLineChar);
                if (Page.IsValid)
                {
                    TextFileWriting(System.DateTime.Now.ToString() + "  Save Started ");
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + txtFileNo.Text.Trim() + " Patient Registration  Save Started " + strNewLineChar);

                    //*********************  PATIENT MASTER INSERT START  **************************
                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter adpt = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_PTMasterDemographicAdd";

                    cmd.Parameters.Add(new SqlParameter("@HPM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_ID", SqlDbType.VarChar)).Value = txtFileNo.Text.ToString().ToUpper();
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_TITLE", SqlDbType.VarChar)).Value = drpTitle.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_FNAME", SqlDbType.NVarChar)).Value = txtFName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_MNAME", SqlDbType.NVarChar)).Value = txtMName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_LNAME", SqlDbType.NVarChar)).Value = txtLName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_FNAME_An", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_MNAME_An", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_LNAME_An", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_BLOOD_GROUP", SqlDbType.VarChar)).Value = drpBlood.SelectedValue.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_TYPE", SqlDbType.VarChar)).Value = drpPatientType.SelectedValue.ToString();


                    if (Convert.ToString(ViewState["PT_COMP_ADD"]) == "1")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_COMP_NAME", SqlDbType.VarChar)).Value = txtPTCompany.Text.ToString();
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_COMP_NAME", SqlDbType.VarChar)).Value = drpPayer.SelectedValue.ToString();
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPM_COMP_NAME", SqlDbType.VarChar)).Value = "";


                    cmd.Parameters.Add(new SqlParameter("@HPM_COMP_CASHPT", SqlDbType.VarChar)).Value = "";//checkbox
                    cmd.Parameters.Add(new SqlParameter("@HPM_INS_TYPE_TRMT", SqlDbType.VarChar)).Value = "";//from insurance

                    if (drpPatientType.SelectedValue == "CR")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_TYPE", SqlDbType.VarChar)).Value = "Insurance";

                        if (drpNetworkName.SelectedIndex == 0)
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedItem.Text;
                            cmd.Parameters.Add(new SqlParameter("@HPM_BILL_CODE", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpNetworkName.SelectedValue;
                            cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpNetworkName.SelectedItem.Text;
                            cmd.Parameters.Add(new SqlParameter("@HPM_BILL_CODE", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                        }


                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.ToString();
                        cmd.Parameters.Add(new SqlParameter("@HPM_FAMILY_ID", SqlDbType.VarChar)).Value = txtPolicyId.Text.ToString();
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_TYPE", SqlDbType.VarChar)).Value = drpPlanType.SelectedValue;


                        string strExpDate = txtExpiryDate.Text;

                        if (txtExpiryDate.Text != "")
                        {
                            string[] arrDate = strExpDate.Split('/');
                            string strForExpDate = "";
                            if (arrDate.Length > 1)
                            {
                                strForExpDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                            }
                            //DateTime TSDate;
                            //  TSDate = Convert.ToDateTime(txtExpiryDate.Text);
                            cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = strForExpDate;// TSDate.ToString("MM/dd/yyyy");


                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = "";
                        }



                        string strPolicyStart = txtPolicyStart.Text;

                        if (txtPolicyStart.Text != "")
                        {
                            string[] arrPlicyStartDate = strPolicyStart.Split('/');
                            string strForPolicyStart = "";
                            if (arrPlicyStartDate.Length > 1)
                            {
                                strForPolicyStart = arrPlicyStartDate[1] + "/" + arrPlicyStartDate[0] + "/" + arrPlicyStartDate[2];
                            }
                            //DateTime TSDate;
                            //  TSDate = Convert.ToDateTime(txtExpiryDate.Text);
                            cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_START", SqlDbType.VarChar)).Value = strForPolicyStart;// TSDate.ToString("MM/dd/yyyy");


                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_START", SqlDbType.VarChar)).Value = "";
                        }


                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_BILL_CODE", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_TYPE", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_NO", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_FAMILY_ID", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_TYPE", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = "";
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_DISCTYPE", SqlDbType.VarChar)).Value = "";//from insurance ;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DISCAMOUNT", SqlDbType.Decimal)).Value = DBNull.Value;// from insurance;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DEDUCTIBLE", SqlDbType.Decimal)).Value = DBNull.Value;// from insurance;
                    cmd.Parameters.Add(new SqlParameter("@HPM_ADDR", SqlDbType.VarChar)).Value = txtAddress.Text.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HPM_REMARKS", SqlDbType.VarChar)).Value = txtRemarks.Text.ToString();
                    cmd.Parameters.Add(new SqlParameter("@HPM_POBOX", SqlDbType.VarChar)).Value = txtPoBox.Text.ToString();
                    if (drpCity.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_CITY", SqlDbType.VarChar)).Value = drpCity.SelectedValue.ToString();
                    }
                    if (drpArea.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_AREA", SqlDbType.VarChar)).Value = drpArea.SelectedValue.ToString();
                    }
                    if (drpCountry.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_COUNTRY", SqlDbType.VarChar)).Value = drpCountry.SelectedValue.ToString();
                    }
                    if (drpNationality.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_NATIONALITY", SqlDbType.VarChar)).Value = drpNationality.SelectedValue.ToString();
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_PHONE1", SqlDbType.VarChar)).Value = txtPhone1.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_PHONE2", SqlDbType.VarChar)).Value = txtMobile2.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_FAX", SqlDbType.VarChar)).Value = txtFax.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_MOBILE", SqlDbType.VarChar)).Value = txtMobile1.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_TYPE", SqlDbType.VarChar)).Value = drpPlanType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_NO", SqlDbType.VarChar)).Value = txtIDNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_ISSUEDBY", SqlDbType.VarChar)).Value = "";





                    string strDOBDate = txtDOB.Text;

                    if (txtDOB.Text != "")
                    {
                        //DateTime DOBDate;
                        // DOBDate = Convert.ToDateTime(txtDOB.Text);

                        ////string[] arrDOBDate = strDOBDate.Split('/');

                        ////string strForDOBDate = "";
                        ////if (arrDOBDate.Length > 1)
                        ////{
                        ////    strForDOBDate = arrDOBDate[1] + "/" + arrDOBDate[0] + "/" + arrDOBDate[2];
                        ////}

                        cmd.Parameters.Add(new SqlParameter("@HPM_DOB", SqlDbType.VarChar)).Value = txtDOB.Text.Trim();// strForDOBDate;// DOBDate.ToString("MM/dd/yyyy");

                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE", SqlDbType.Char)).Value = drpAgeType.SelectedValue; // "Y";
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE", SqlDbType.Int)).Value = txtAge.Text;



                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_DOB", SqlDbType.VarChar)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE", SqlDbType.Char)).Value = "";
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE", SqlDbType.Int)).Value = "";
                    }


                    if (txtMonth.Text != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE1", SqlDbType.Int)).Value = txtMonth.Text;
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE1", SqlDbType.VarChar)).Value = drpAgeType1.SelectedValue; ;// "M";


                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE1", SqlDbType.Int)).Value = 0;
                        cmd.Parameters.Add(new SqlParameter("@HPM_AGE_TYPE1", SqlDbType.VarChar)).Value = "";
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_MARITAL", SqlDbType.VarChar)).Value = drpMStatus.SelectedValue;

                    if (drpSex.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_SEX", SqlDbType.VarChar)).Value = drpSex.SelectedValue;
                    }

                    ////cmd.Parameters.Add(new SqlParameter("@HPM_DR_ID", SqlDbType.VarChar)).Value = txtDoctorID.Text.Trim();
                    ////cmd.Parameters.Add(new SqlParameter("@HPM_DR_NAME", SqlDbType.VarChar)).Value = txtDoctorName.Text.Trim();
                    ////cmd.Parameters.Add(new SqlParameter("@HPM_DEP_NAME", SqlDbType.VarChar)).Value = txtDepName.Text.Trim();



                    //cmd.Parameters.Add(new SqlParameter("@HPM_TOKEN_NO", SqlDbType.VarChar)).Value = txtTokenNo.Text;

                    //if (txtRefDoctorID.Text.Trim() != "")
                    //{
                    //    cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_ID", SqlDbType.VarChar)).Value = txtRefDoctorID.Text.Trim();
                    //    cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_NAME", SqlDbType.VarChar)).Value = txtRefDoctorName.Text.Trim();
                    //}


                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_NAME", SqlDbType.VarChar)).Value = txtKinName.Text;
                    if (drpKinRelation.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_KIN_RELATION", SqlDbType.VarChar)).Value = drpKinRelation.SelectedItem.Text;
                    }
                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_PHONE", SqlDbType.VarChar)).Value = txtKinPhone.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_MOBILE", SqlDbType.VarChar)).Value = txtKinMobile.Text;

                    if (chkStatus.Checked == true)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_STATUS", SqlDbType.Char)).Value = 'A';
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_STATUS", SqlDbType.Char)).Value = 'I';
                    }

                    // cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.Char)).Value = drpPatientStatus.SelectedValue;
                    //IN THE SYSTEM OPTIONS IF FILEING REQUEST IS SELECTED VISIT STATUS IS 'F' ELSE 'W'
                    if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.VarChar)).Value = "F";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.VarChar)).Value = "W";
                    }



                    cmd.Parameters.Add(new SqlParameter("@HPM_COMMIT_FLAG", SqlDbType.Char)).Value = 'N';

                    cmd.Parameters.Add(new SqlParameter("@HPM_MODIFIED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);

                    cmd.Parameters.Add(new SqlParameter("@HPM_IQAMA_NO", SqlDbType.VarChar)).Value = txtEmiratesID.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_NO_CHILDREN", SqlDbType.TinyInt)).Value = txtNoOfChildren.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_OCCUPATION", SqlDbType.VarChar)).Value = txtOccupation.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DED_TYPE", SqlDbType.Char)).Value = drpDedType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_DED_AMT", SqlDbType.Decimal)).Value = txtDeductible.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_COINS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_COINS_AMT", SqlDbType.Decimal)).Value = txtCoIns.Text.Trim();

                    if (drpKnowFrom.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_KNOW_FROM", SqlDbType.VarChar)).Value = drpKnowFrom.SelectedValue;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_KNOW_FROM", SqlDbType.VarChar)).Value = "";
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPM_PACKAGE_NAME", SqlDbType.VarChar)).Value = txtNetworkClass.Text;

                    cmd.Parameters.Add(new SqlParameter("@HPM_PATIENT_STATUS", SqlDbType.VarChar)).Value = drpPTStatus.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_PTREGTYPE", SqlDbType.VarChar)).Value = drpPtRegisteredType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_STATUS_MSG", SqlDbType.VarChar)).Value = txtHoldMsg.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_IQAMA_PATH", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_PATH", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@BUCODE", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPM_EMPLOYEE_STATUS", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@PT_DEP_ID", SqlDbType.VarChar)).Value = "";

                    if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "0")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                        cmd.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char)).Value = "A";

                        if (Convert.ToString(ViewState["PatientInfoId"]) != "" && Convert.ToString(ViewState["PatientInfoId"]) != null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@PatientInfoId", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["PatientInfoId"]);
                        }



                    }
                    else if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                    {

                        cmd.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char)).Value = "M";
                    }
                    cmd.Parameters.Add(new SqlParameter("@IsManual", SqlDbType.Bit)).Value = chkManual.Checked;

                    cmd.Parameters.Add(new SqlParameter("@HPM_ID_CAPTION", SqlDbType.VarChar)).Value = drpIDCaption.SelectedValue;




                    if (Convert.ToString(Session["DeptWisePTId"]).ToUpper() == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = drpDepartment.SelectedValue;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = "PATIENTREG";
                    }




                    cmd.Parameters.Add(new SqlParameter("@HPM_KIN_MOBILE1", SqlDbType.Decimal)).Value = txtKinMobile1.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPM_PHONE3", SqlDbType.Decimal)).Value = txtPhone3.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HPM_VISA_TYPE", SqlDbType.VarChar)).Value = drpVisaType.SelectedValue;


                    cmd.Parameters.Add(new SqlParameter("HPM_DENT_COINS_TYPE", SqlDbType.VarChar)).Value = drpCoInsDental.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("HPM_DENT_COINS_AMT", SqlDbType.Decimal)).Value = txtCoInsDental.Text;
                    cmd.Parameters.Add(new SqlParameter("HPM_PHY_COINS_TYPE", SqlDbType.VarChar)).Value = drpCoInsPharmacy.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("HPM_PHY_COINS_AMT", SqlDbType.Decimal)).Value = txtCoInsPharmacy.Text;
                    cmd.Parameters.Add(new SqlParameter("HPM_LAB_COINS_TYPE", SqlDbType.VarChar)).Value = drpCoInsLab.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("HPM_LAB_COINS_AMT", SqlDbType.Decimal)).Value = txtCoInsLab.Text;
                    cmd.Parameters.Add(new SqlParameter("HPM_RAD_COINS_TYPE", SqlDbType.VarChar)).Value = drpCoInsRad.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("HPM_RAD_COINS_AMT", SqlDbType.Decimal)).Value = txtCoInsRad.Text;

                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_CATEGORY", SqlDbType.VarChar)).Value = "";// drpPTCategory.SelectedValue;


                    cmd.Parameters.Add(new SqlParameter("@HPM_RELIGION_CODE", SqlDbType.VarChar)).Value = drpReligion.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_RELIGION", SqlDbType.VarChar)).Value = drpReligion.SelectedItem.Text;

                    cmd.Parameters.Add(new SqlParameter("@HPM_EMIRATES_CODE", SqlDbType.VarChar)).Value = drpEmirates.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPM_EMIRATES", SqlDbType.VarChar)).Value = drpEmirates.SelectedItem.Text;



                    int k = cmd.Parameters.Count;
                    for (int i = 0; i < k; i++)
                    {
                        if (cmd.Parameters[i].Value == "")
                        {
                            cmd.Parameters[i].Value = DBNull.Value;
                        }
                    }

                    SqlParameter returnValue = new SqlParameter("@ReturnPTId", SqlDbType.VarChar, 15);
                    returnValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnValue);


                    cmd.ExecuteNonQuery();
                    string strReturnFileNo;
                    strReturnFileNo = Convert.ToString(returnValue.Value);
                    hidActualFileNo.Value = strReturnFileNo;

                    txtPTVisitFileNo.Text = strReturnFileNo;
                    drpPTVisitType.SelectedValue = drpPatientType.SelectedValue;

                    drpPTVisitType_SelectedIndexChanged(drpPTVisitType, new EventArgs());


                    txtPTVisitFullName.Text = txtFName.Text.Trim() + " " + txtMName.Text.Trim() + " " + txtLName.Text.Trim();
                    txtPTVisitMobile.Text = txtMobile1.Text.Trim();
                    txtPTVisitGender.Text = drpSex.SelectedItem.Text;

                    if (drpDefaultCompany.SelectedIndex != 0)
                    {
                        BindPTVisitCompany();

                        drpPTVisitCompany.SelectedValue = drpDefaultCompany.SelectedValue;


                        if (drpPlanType.SelectedIndex != 0)
                        {
                            BindPTVisitCompanyPlan();

                            drpPTVisitCompanyPlan.SelectedValue = drpPlanType.SelectedValue;
                        }

                        txtPTVisitPolicyNo.Text = txtPolicyNo.Text;
                        txtPTVisitCardNo.Text = txtIDNo.Text;
                        txtPTVisitCardExpDate.Text = txtCardExpDate.Text;

                    }

                    con.Close();

                    TextFileWriting(System.DateTime.Now.ToString() + "Patent Master Insert End ");
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Registration  Insert End " + strNewLineChar);


                    //*********************  PATIENT MASTER INSERT END  **************************

                    //if (drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU")
                    //{
                    string strPhotoID = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;
                    ViewState["Photo_ID"] = strPhotoID;
                    string strPath = Convert.ToString(ViewState["InsCardPath"]);
                    Byte[] bytImage = null, bytImage1 = null;

                    //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click CheckPatientPhoto() Start "+ strNewLineChar);
                    if (CheckPatientPhoto() == false)
                    {
                        if (Convert.ToString(Session["InsCardFrontDefaultPath"]) != "")
                        {
                            System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontDefaultPath"]));
                            bytImage = ImageToByte(image);
                        }



                        if (Convert.ToString(Session["InsCardBackDefaultPath"]) != "")
                        {
                            System.Drawing.Image image1 = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardBackDefaultPath"]));
                            bytImage1 = ImageToByte(image1);
                        }

                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click PatientPhotoAdd  Start "+ strNewLineChar);
                        dbo.PatientPhotoAdd(txtFileNo.Text, Convert.ToString(Session["Branch_ID"]), bytImage, bytImage1, strPhotoID, "Insurance");
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click PatientPhotoAdd  End "+ strNewLineChar);


                        ////dbo.BranchID = Convert.ToString(Session["Branch_ID"]);
                        ////dbo.PT_ID = txtFileNo.Text;
                        ////dbo.VISIT_NO = hidPTVisitSQNo.Value;
                        ////dbo.INS_CARD_FRONT = bytImage;
                        ////dbo.INS_CARD_BACK = bytImage1;
                        ////dbo.UserID = Convert.ToString(Session["User_ID"]);
                        ////dbo.PatientInsuranceHistory();



                        ////if (strPhotoID != "" && strPhotoID != null && hidPTVisitSQNo.Value != "" && hidPTVisitSQNo.Value != null)
                        ////{
                        ////    string FieldWithValue = " HPV_PHOTO_ID='" + strPhotoID + "'";
                        ////    string UpdateCondition = " HPV_PT_ID='" + hidActualFileNo.Value + "' AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_SEQNO=" + hidPTVisitSQNo.Value;


                        ////    dbo.fnUpdatetoDb("HMS_PATIENT_VISIT", FieldWithValue, UpdateCondition);
                        ////    //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click Photo ID save in Visit Table "+ strNewLineChar);

                        ////}


                    }
                    else
                    {

                        strPhotoID = Convert.ToString(ViewState["Photo_ID"]);

                        if (Convert.ToString(Session["InsCardFrontDefaultPath"]) != "")
                        {
                            System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontDefaultPath"]));
                            bytImage = ImageToByte(image);
                        }


                        if (Convert.ToString(Session["InsCardBackDefaultPath"]) != "")
                        {
                            System.Drawing.Image image1 = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardBackDefaultPath"]));
                            bytImage1 = ImageToByte(image1);
                        }
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click PatientPhotoInsUpdate  Start "+ strNewLineChar);

                        dbo.PatientPhotoInsUpdate(txtFileNo.Text, Convert.ToString(Session["Branch_ID"]), bytImage, bytImage1, strPhotoID, "Insurance");
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click PatientPhotoInsUpdate  End "+ strNewLineChar);

                        ////dbo.BranchID = Convert.ToString(Session["Branch_ID"]);
                        ////dbo.PT_ID = txtFileNo.Text;
                        ////dbo.VISIT_NO = hidPTVisitSQNo.Value;
                        ////dbo.INS_CARD_FRONT = bytImage;
                        ////dbo.INS_CARD_BACK = bytImage1;
                        ////dbo.UserID = Convert.ToString(Session["User_ID"]);
                        ////dbo.PatientInsuranceHistory();


                        ////if (strPhotoID != "" && strPhotoID != null && hidPTVisitSQNo.Value != "" && hidPTVisitSQNo.Value != null)
                        ////{
                        ////    string FieldWithValue = " HPV_PHOTO_ID='" + strPhotoID + "'";
                        ////    string UpdateCondition = " HPV_PT_ID='" + hidActualFileNo.Value + "' AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_SEQNO=" + hidPTVisitSQNo.Value;


                        ////    dbo.fnUpdatetoDb("HMS_PATIENT_VISIT", FieldWithValue, UpdateCondition);
                        ////    //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click Photo ID save in Visit Table "+ strNewLineChar);

                        ////}

                    }
                    Byte[] bytPatientPhotoImage = null;
                    string strPhotoPath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PatientPhoto_FilePath"]);
                    string strFileName = txtEmiratesID.Text.Replace("-", "") + ".jpg";


                    strPhotoPath = strPhotoPath + strFileName;

                    if (System.IO.File.Exists(strPhotoPath) == true)
                    {
                        if (strPhotoPath != null)
                        {
                            System.Drawing.Image PTPhoto = System.Drawing.Image.FromFile(@strPhotoPath);
                            bytPatientPhotoImage = ImageToByte(PTPhoto);
                        }

                        dbo.PatientProfilePhotoAdd(txtFileNo.Text, Convert.ToString(Session["Branch_ID"]), bytPatientPhotoImage);
                    }

                    // }

                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Photo Update End " + strNewLineChar);
                    //SAVE APPOINTMENT STATUS AS WAITING
                    UpdateAppointmentStatus();
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Registration  UpdateAppointmentStatus() End " + strNewLineChar);

                    //    ModalPopupExtender1.Show();
                    //    strMessage = "Details Saved !";

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS SAVED.',' Patient Details Saved.','Green')", true);


                    if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Patient Registration, Patient ID is " + hidActualFileNo.Value);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Patient Registration, Patient ID is " + hidActualFileNo.Value);
                    }




                    // if (Convert.ToString(Session["InsCardFrontDefaultPath"]) != "" && Session["InsCardFrontDefaultPath"] != null)
                    if (drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU")
                    {
                        ScanCardImageGenerate();

                        dboperations objOperations = new dboperations();
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click ScanCardImageModifye Start "+ strNewLineChar);
                        objOperations.ScanCardImageModify(hidActualFileNo.Value, Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["InsCardFrontDefaultPath"]), Convert.ToString(Session["InsCardBackDefaultPath"]));
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click ScanCardImageModifye End "+ strNewLineChar);

                        //    CommonBAL objCom = new CommonBAL();
                        //  string FieldNameWithValues1 = " HSC_IMAGE_PATH_FRONT='" + Convert.ToString(Session["InsCardFrontDefaultPath"]) + "', HSC_IMAGE_PATH_BACK='" + Convert.ToString(Session["InsCardBackDefaultPath"]) + "'";
                        //  FieldNameWithValues1 += " ,HSC_POLICY_START= Convert(datetime,'" + txtPolicyStart.Text + "',103) , HSC_EXPIRY_DATE= Convert(datetime,'" + txtExpiryDate.Text + "',103)";
                        //   FieldNameWithValues1 += " ,HSC_PLAN_TYPE='" + drpPlanType.SelectedValue + "'";
                        //   FieldNameWithValues1 += " ,HSC_INS_COMP_ID='" + drpDefaultCompany.SelectedValue + "', HSC_INS_COMP_NAME='" + drpDefaultCompany.SelectedItem.Text + "'";
                        //   FieldNameWithValues1 += " ,HSC_CARD_NO='" + txtIDNo.Text + "', HSC_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'";
                        //,HSC_PHOTO_ID

                        //  string Criteria1 = " 1=1 ";
                        //   Criteria1 += "  AND HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True' AND HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HSC_PT_ID='" + hidActualFileNo.Value + "'";//txtFileNo.Text.Trim() 
                        //   Criteria1 += " ,HSC_POLICY_START= Convert(datetime,'" + txtPolicyStart.Text + "',103) , HSC_EXPIRY_DATE= Convert(datetime,'" + txtExpiryDate.Text + "',103)";

                        //   objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_SCANCARD_IMAGES", Criteria1);

                    }


                    if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                    {
                        strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Registration  Malaffi update Start " + strNewLineChar);

                        if (hidActualFileNo.Value != "")
                        {
                            CommonBAL objCom = new CommonBAL();
                            DataSet DSMalaffi = new DataSet();
                            string Criteria1 = " HMHM_UPLOAD_STATUS ='PENDING' ";
                            Criteria1 += " AND HMHM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HMHM_PT_ID='" + hidActualFileNo.Value + "'";

                            if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                            {
                                Criteria1 += " AND HMHM_MESSAGE_TYPE='ADT' and HMHM_MESSAGE_CODE='ADT-A31'";
                            }
                            else
                            {
                                Criteria1 += " AND HMHM_MESSAGE_TYPE='ADT' and HMHM_MESSAGE_CODE='ADT-A28'";
                            }
                            DSMalaffi = objCom.fnGetFieldValue(" TOP 1 *", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1, "");

                            if (DSMalaffi.Tables[0].Rows.Count > 0)
                            {
                                TextFileWriting("Malaffi table already this data available PT ID =" + hidActualFileNo.Value);
                                goto MalafffiSaveEnd;
                            }

                            HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();

                            objHL7Mess.PT_ID = hidActualFileNo.Value;
                            objHL7Mess.VISIT_ID = "";
                            objHL7Mess.EMR_ID = "";
                            objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                            objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                            objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);

                            objHL7Mess.MESSAGE_TYPE = "ADT";
                            objHL7Mess.UPLOAD_STATUS = "PENDING";
                            objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);

                            if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                            {
                                objHL7Mess.MessageCode = "ADT-A31";
                                objHL7Mess.MessageDesc = "Update Patient Information";
                                objHL7Mess.MalaffiHL7MessageMasterAdd();


                            }
                            else
                            {
                                objHL7Mess.MessageCode = "ADT-A28";
                                objHL7Mess.MessageDesc = "Add Patient Information";
                                objHL7Mess.MalaffiHL7MessageMasterAdd();

                            }
                            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Registration  Malaffi update  End " + strNewLineChar);
                        MalafffiSaveEnd: ;

                        }
                    }


                    ////if (Convert.ToString(Session["ClearAfterSave"]).ToUpper() == "Y")
                    ////{
                    ////    txtFileNo.Text = "";
                    ////    chkFamilyMember.Checked = false;
                    ////    Clear();
                    ////}
                    ////else
                    ////{
                    ViewState["Patient_ID_VALID"] = "1";
                    /// }

                }

                //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click End "+ strNewLineChar);
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Registration  btnSave_Click End " + strNewLineChar);


                if (Convert.ToString(ViewState["EnquiryID"]) != "")
                {
                    CommonBAL objCom = new CommonBAL();
                    string strEnqCriteria = "HCEM_ID='" + Convert.ToString(ViewState["EnquiryID"]) + "'";
                    objCom.fnUpdateTableData("HCEM_PT_ID='" + hidActualFileNo.Value + "',HCEM_STATUS='Closed'", "HMS_CUSTOMER_ENQUIRY_MASTER", strEnqCriteria);

                    ViewState["EnquiryID"] = "";
                }





            SaveEnd: ;
                if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1" || Convert.ToString(Session["ENABLE_USER_LOG"]) == "1")
                {
                    ProcessTimeLog(strPageTimeLog.ToString());
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1" || Convert.ToString(Session["ENABLE_USER_LOG"]) == "1")
                {
                    ProcessTimeLog(strPageTimeLog.ToString());
                }
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

            if (hidPateName.Value == "HomeCare")
            {
                Response.Redirect("../HomeCare/HomeCareRegistration.aspx?PatientId=" + hidActualFileNo.Value);
            }

        }

        protected void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCompCashPT.Checked == true)
            {
                Session["CompcashPt"] = "Y";
            }
            else
            {
                Session["CompcashPt"] = "N";
            }
        }

        protected void chkStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStatus.Checked == true)
            {
                Session["status"] = "A";

            }

            else
            {
                Session["status"] = "I";
            }

        }

        protected void chkManualToken_CheckedChanged(object sender, EventArgs e)
        {
            if (chkManualToken.Checked == true)
            {
                txtTokenNo.Enabled = true;
                txtTokenNo.Focus();
            }

            else
            {
                txtTokenNo.Enabled = false;
            }

        }

        protected void drpPatientType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CompanyParentGet();

            BindDefaultCompany();

            drpNetworkName.Items.Clear();
            if (drpDefaultCompany.Items.Count > 0)
            {
                drpDefaultCompany.SelectedIndex = 0;
            }
            txtPolicyNo.Text = "";
            txtPolicyId.Text = "";
            txtIDNo.Text = "";
            txtNetworkClass.Text = "";

            txtExpiryDate.Text = "";
            txtPolicyStart.Text = "";
            // drpDefaultCompany.Items.Clear();
            drpPlanType.Items.Clear();

            //if (drpPatientType.SelectedValue == "CU")
            //{
            //    txtPolicyNo.Text = GlobalValues.HospitalLicenseNo + "#" + txtFileNo.Text.Trim();
            //}


            if (drpPatientType.SelectedValue == "CR" || drpPatientType.SelectedValue == "CU")
            {
                drpParentCompany.Enabled = true;

                txtPolicyNo.Enabled = true;
                txtPolicyId.Enabled = true;
                txtIDNo.Enabled = true;
                txtNetworkClass.Enabled = true;
                txtExpiryDate.Enabled = true;
                txtPolicyStart.Enabled = true;
                drpPlanType.Enabled = true;
                drpDefaultCompany.Enabled = true;
                drpNetworkName.Enabled = true;
            }
            else
            {
                drpParentCompany.Enabled = false;
                txtPolicyNo.Enabled = false;
                txtPolicyId.Enabled = false;
                txtIDNo.Enabled = false;
                txtNetworkClass.Enabled = false;
                txtExpiryDate.Enabled = false;
                txtPolicyStart.Enabled = false;
                drpDefaultCompany.Enabled = false;
                drpPlanType.Enabled = false;
                drpNetworkName.Enabled = false;
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            txtFileNo.Text = "";
            hidActualFileNo.Value = "";
            txtFileNo.Focus();
            chkFamilyMember.Checked = false;
            Clear();
            if (Convert.ToString(ViewState["EID_TYPE"]) == "FEITIAN")
            {
                txtEmirateIDData.Text = "";
            }
            chkStatus.Checked = true;
            //  chkManual_CheckedChanged(chkManual, new EventArgs());
            ViewState["LoadFromEmiratesID"] = "0";
        }

        protected void chkFamilyMember_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFamilyMember.Checked == true)
            {
                //lblVisitType.Text = "New";
                //txtFName.Text = "";
                //txtMName.Text = "";
                //txtLName.Text = "";
                //txtDOB.Text = "";
                //txtEmiratesID.Text = "";
                //txtFileNo.Focus();
                //txtAge.Text = "";
                //drpSex.SelectedIndex = 0;
                //  ViewState["Patient_ID_VALID"] = "0";
                Clear();
            }

            else
            {

            }

        }

        #region Crystal Report

        protected void btnMRRPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }

            // System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            //// Pro.StartInfo.FileName = "C:\\P1.bat";
            //Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            //Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + Server.MapPath("Reports\\HmsMRRPrint.rpt") + "${HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'$SCREEN$$$1";
            // Pro.StartInfo.RedirectStandardInput = false;
            // Pro.StartInfo.RedirectStandardOutput = true;
            // Pro.StartInfo.CreateNoWindow = true;
            // Pro.StartInfo.UseShellExecute = false;
            // Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            // Pro.Start();

            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            // string strReport = "HmsMRRPrint.rpt";
            //String DBString = "", strFormula = "";
            //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            //strFormula = hidActualFileNo.Value;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowBalance('" + strReport + "','" + DBString + "','" + hidActualFileNo.Value + "');", true);
            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMRRPrintPDF('" + hidActualFileNo.Value + "');", true);


        FunEnd: ;
        }



        protected void btnTokenPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }
            DataSet ds = new DataSet();
            ds = dbo.GetMaxPatientVisit(hidActualFileNo.Value);
            Int32 intSQNO = 1;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HPVSEQNO") == false)
                {
                    intSQNO = Convert.ToInt32(Convert.ToString(ds.Tables[0].Rows[0]["HPVSEQNO"]));
                }
            }

            //System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            //Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            //Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + Server.MapPath("Reports\\HMSToken.rpt") + "${hms_patient_visit.HPV_SEQNO}='" + intSQNO + "'$PRINT$$$1";
            //Pro.StartInfo.RedirectStandardInput = false;
            //Pro.StartInfo.RedirectStandardOutput = true;
            //Pro.StartInfo.CreateNoWindow = true;
            //Pro.StartInfo.UseShellExecute = false;
            //Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //Pro.Start();


            // string strFormula = " {hms_patient_visit.HPV_SEQNO}='" + intSQNO + "'";
            //Session["ReportFormula"] = strFormula;
            //   string strPrintCount="1";
            //if (txtLablePrintCount.Text.Trim() != "")
            //{
            //    strPrintCount = txtLablePrintCount.Text.Trim();
            //}

            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ReportPrintPopup('HMSToken.rpt','PRINT','" + strPrintCount + "');", true);


            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text.Trim());
            string strReport = "HMSToken.rpt";

            String DBString = "";


            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowOnLoad('" + strReport + "','" + DBString + "','" + intSQNO + "','" + intCount + "');", true);
        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            //strLabel = "";
        //strToken = "";
        //BIndToken();
        //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), " window.print();", true);


FunEnd: ;
        }


        protected void btnIDPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }
            DataSet ds = new DataSet();
            ds = dbo.GetMaxPatientVisit(hidActualFileNo.Value);
            Int32 intSQNO = 1;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HPVSEQNO") == false)
                {
                    intSQNO = Convert.ToInt32(Convert.ToString(ds.Tables[0].Rows[0]["HPVSEQNO"]));
                }
            }

            //System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            ////Pro.StartInfo.FileName = "c:\\printRpt.exe";
            //Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            //Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + Server.MapPath("Reports\\HmsPtIDCard.rpt") + "${HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'$SCREEN$$" + strIdPrinterName + "$" + txtLablePrintCount.Text;
            //Pro.StartInfo.RedirectStandardInput = false;
            //Pro.StartInfo.RedirectStandardOutput = true;
            //Pro.StartInfo.CreateNoWindow = true;
            //Pro.StartInfo.UseShellExecute = false;
            //Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //Pro.Start();

            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text.Trim());

            string strReport = "HmsPtIDCard.rpt";
            String DBString = "", strFormula = "";
            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualFileNo.Value;
            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowIDPrint('" + strReport + "','" + DBString + "','" + hidActualFileNo.Value + "');", true);
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowOnLoad('" + strReport + "','" + DBString + "','" + intSQNO + "','" + intCount + "');", true);

        // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END




    FunEnd: ;


        }

        protected void btnLabelPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }

            /*System.Diagnostics.Process Pro = new System.Diagnostics.Process();
            Pro.StartInfo.FileName = Server.MapPath("bin\\printRpt.exe");
            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text);

            string strReport = Server.MapPath("Reports\\HmsLabel.rpt");
            if (chkLabelPrint.Checked == true)
            {
                strReport = Server.MapPath("Reports\\HmsLabel1.rpt");
            }

            Pro.StartInfo.Arguments = "1$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD + "$" + strReport + "${HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'$PRINT$$" + strLabelPrinterName + "$" + txtLablePrintCount.Text;
            Pro.StartInfo.RedirectStandardInput = false;
            Pro.StartInfo.RedirectStandardOutput = true;
            Pro.StartInfo.CreateNoWindow = true;
            Pro.StartInfo.UseShellExecute = false;
            Pro.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            Pro.Start();
            */
            // string rptcall = "CReports/ReportsView.aspx?ReportName=HmsLabel&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=" + txtFileNo.Text.Trim()  ;
            //  string rptcall = "CReports/ReportsView.aspx?ReportName=HmsLabel.rpt&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() +"'" ;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);

            // ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('CReports/ReportsView.aspx?ReportName=HmsLabel.rpt&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=" + txtFileNo.Text.Trim() + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);

            ////string strReport = "HmsLabel.rpt";
            ////if (chkLabelPrint.Checked == true)
            ////{
            ////    strReport = "HmsLabel1.rpt";
            ////}
            ////string strFormula = " {HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'";
            ////Session["ReportFormula"] = strFormula;
            ////string strPrintCount = "1";
            ////if (txtLablePrintCount.Text.Trim() != "")
            ////{
            ////    strPrintCount = txtLablePrintCount.Text.Trim();
            ////}

            //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ReportPrintPopup('" + strReport + "','PRINT','" + strPrintCount + "');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "ReportPopup(+" strReport ,"{HMS_PATIENT_MASTER.HPM_PT_ID}=" + txtFileNo.Text.Trim() + ");


            ////   Response.Redirect("CReports\\ReportsView.aspx?ReportName="+ strReport +"&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'");



            // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START

            Int32 intCount = Convert.ToInt32(txtLablePrintCount.Text);
            string strReport = "HmsLabel.rpt";
            if (chkLabelPrint.Checked == true)
            {
                strReport = "HmsLabel.rpt";
            }

            String DBString = "", strFormula = "", PTVisitSQNo = "";

            DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            strFormula = hidActualFileNo.Value;
            // PTVisitSQNo = hidPTVisitSQNo.Value;

            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowOnLoad('" + strReport + "','" + DBString + "','" + strFormula + "','" + intCount + "', '" + PTVisitSQNo + "');", true);
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowOnLoad('" + strReport + "','" + DBString + "','" + strFormula + "','" + intCount + "');", true);

           // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            //strLabel = "";
        //strToken = "";
        //BindLabel();
        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), " window.print();", true);
        FunEnd: ;
        }

        //protected void btnBalance_Click(object sender, EventArgs e)
        //{
        //    if (hidActualFileNo.Value == "")
        //    {
        //        goto FunEnd;
        //    }
        //    // CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
        //    string strReport = "HmsPtBalHist.rpt";
        //    String DBString = "", strFormula = "";
        //    DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
        //    strFormula = hidActualFileNo.Value;
        //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowBalance('" + strReport + "','" + DBString + "','" + hidActualFileNo.Value + "');", true);
        //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END



        //FunEnd: ;

        //}

        protected void btnClaimPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (hidActualFileNo.Value == "")
                {
                    goto FunEnd;
                }
                if (drpDefaultCompany.Items.Count == 0)
                {
                    goto FunEnd;
                }

                //BELOW CODING WILL INSERT THE SCAN CARD IMAGE IN TEMP FILE TABLE FOR SHOWING THE CARD IMAGE IN REPORT
                if (Convert.ToString(Session["InsCardFrontPath"]) != "")
                {
                    string strPath = Convert.ToString(ViewState["InsCardPath"]);
                    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontPath"]));


                    Byte[] bytImage;
                    bytImage = ImageToByte(image);

                    intImageSize = bytImage.Length;

                    dbo.TempImageAdd(txtFileNo.Text.Trim(), Convert.ToString(Session["Branch_ID"]), bytImage);

                }


                string strRptName = "";
                strRptName = fnGetClaimFormName(drpDefaultCompany.SelectedValue, txtDepName.Text);

                if (File.Exists(strReportPath + strRptName) == false)
                {
                    lblStatus.Text = "File Not Found";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
                //String DBString = "";
                //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowClaimPrint('" + strRptName + "','" + DBString + "','" + hidActualFileNo.Value + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);

                //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowClaimPrintPDF('" + strRptName + "','" + hidActualFileNo.Value + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);



                //BELOW CODING WILL DELETE THE SCAN CARD IMAGE FROM TEMP FILE TABLE AFTER SHOWING THE CARD IMAGE IN REPORT
                //  dbo.TempImageDelete(txtFileNo.Text.Trim());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnClaimPrint_Click");
                TextFileWriting(ex.Message.ToString());

            }
        FunEnd: ;

        }


        protected void btnGeneConTreatment_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")//|| Convert.ToString(ViewState["PTLastGCId"]) == "")
            {
                goto FunEnd;
            }



            //  Int64 GCId = Convert.ToInt64(ViewState["PTLastGCId"]);
            //  AddSignatureTempImage(GCId);

            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            ////  string strReport = "GeneralTreatmentConsent.rpt";
            //String DBString = "", strFormula = "";
            //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            //strFormula = hidActualFileNo.Value;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReport('" + DBString + "'," + Convert.ToString(ViewState["HGC_ID"]) + ");", true);

            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReportPDF('" + hidActualFileNo.Value + "');", true);


            //dbo.TempImageDelete(hidActualFileNo.Value);

            //WEB REPORT COMMENTED AS PER CLIENT REQUEST
        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "GeneralConsentPopup('" + hidActualFileNo.Value + "','0');", true);


        FunEnd: ;


        }

        protected void btnDamanGC_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")// || Convert.ToString(ViewState["PTLastGCId"]) == "")
            {
                goto FunEnd;
            }

            // Int64 GCId = Convert.ToInt64(ViewState["PTLastGCId"]);
            // AddSignatureTempImage(GCId);

            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
            //// string strReport = "DamanConscent.rpt";
            //String DBString = "", strFormula = "";
            //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
            //strFormula = hidActualFileNo.Value;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowDamanGCReport('" + DBString + "'," + ViewState["HGC_ID"] + ");", true);
            //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowDamanGCReportPDF('" + hidActualFileNo.Value + "');", true);

          //  string rptcall = "CReports/ReportsView.aspx?ReportName=DamanConscent.rpt&SelectionFormula={GeneralConsentView.HGC_ID}=" + ViewState["HGC_ID"];
        //  ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);

          // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReport1('" + ViewState["HGC_ID"] + "');", true);


        FunEnd: ;

        }



        protected void btnConsentFormPrint_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")// || Convert.ToString(ViewState["PTLastGCId"]) == "")
            {
                goto FunEnd;
            }

            // Int64 GCId = Convert.ToInt64(ViewState["PTLastGCId"]);
            //  AddSignatureTempImage(GCId);

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowConsentFormReportPDF('" + hidActualFileNo.Value + "');", true);



        FunEnd: ;

        }


        #endregion

        #region Web Report


        protected void btnWebRepot_Click(object sender, EventArgs e)
        {
            //BELOW CODING WILL INSERT THE SCAN CARD IMAGE IN TEMP FILE TABLE FOR SHOWING THE CARD IMAGE IN REPORT
            //if (Convert.ToString(Session["InsCardFrontPath"]) != "")
            //{
            //    string strPath = Server.MapPath("../Uploads/");
            //    System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Session["InsCardFrontPath"].ToString());


            //    Byte[] bytImage;
            //    bytImage = imageToByteArray(image);

            //    intImageSize = bytImage.Length;

            //    dbo.TempImageAdd(txtFileNo.Text.Trim(), Convert.ToString(Session["Branch_ID"]), bytImage);

            //}
            //string strRptName = "";
            //strRptName = fnGetWebClaimFormName(drpDefaultCompany.SelectedValue, txtDepName.Text);

            //string rptcall = "WebReport/" + strRptName + "?PatientId=" + txtFileNo.Text + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=1&DRId=" + txtDoctorID.Text + "&DrDepName=" + txtDepName.Text + "&Date=0";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);
            ////  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "WebReportPopup(" + txtFileNo.Text + ");", true);

            ////BELOW CODING WILL DELETE THE SCAN CARD IMAGE FROM TEMP FILE TABLE AFTER SHOWING THE CARD IMAGE IN REPORT
            //dbo.TempImageDelete(txtFileNo.Text.Trim());

            //CompanyId

            string rptcall = "../WebReport/WebReportLoader.aspx?PatientId=" + txtFileNo.Text + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=6&DRId=" + txtDoctorID.Text + "&DrDepName=" + txtDepName.Text + "&Date=0&CompanyId=" + drpDefaultCompany.SelectedValue;// txtCompany.Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);



        }

        protected void btnDentalWebRepot_Click(object sender, EventArgs e)
        {
            string rptcall = "../WebReport/EMR_Barbara_DentalConsentForm.aspx?PatientId=" + txtFileNo.Text + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=6&DRId=" + txtDoctorID.Text + "&DrDepName=" + txtDepName.Text + "&Date=0";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

        }




        protected void btnCardPrint_Click(object sender, EventArgs e)
        {
            //string rptcall = "WebReport/ScandCardPrint.aspx";
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeSheet Report", "window.open('" + rptcall + "','_new','menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1');", true);
            if (Session["InsCardFrontPath"] != "")
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ScandCardPopup('" + txtFileNo.Text + "');", true);
            }


        }
        #endregion

        protected void btnBalance_Click(object sender, EventArgs e)
        {
            if (hidActualFileNo.Value == "")
            {
                goto FunEnd;
            }




        FunEnd: ;

        }

        protected void AsyncFileUpload1_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                string strPath = @Convert.ToString(ViewState["InsCardPath"]);

                if (!Directory.Exists(@strPath))
                {
                    Directory.CreateDirectory(@strPath);
                }

                if (!Directory.Exists(@strPath + @"Temp\"))
                {
                    Directory.CreateDirectory(@strPath + @"Temp\");
                }

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));

                    AsyncFileUpload1.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardFrontPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardFrontPathT"] = strFileNmae;
                    string strPath1 = Server.MapPath("../Uploads/Temp/");// @strPath + @"Temp\";
                    AsyncFileUpload1.SaveAs(strPath1 + strFileNmae);


                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardFrontPath"] = null;
                }

                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload1_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AsyncFileUpload2_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                string strPath = @Convert.ToString(ViewState["InsCardPath"]);

                if (!Directory.Exists(@strPath))
                {
                    Directory.CreateDirectory(@strPath);
                }

                if (!Directory.Exists(@strPath + @"Temp\"))
                {
                    Directory.CreateDirectory(@strPath + @"Temp\");
                }

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));

                    AsyncFileUpload2.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardBackPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardBackPathT"] = strFileNmae;
                    string strPath2 = Server.MapPath("../Uploads/Temp/");// @strPath + @"Temp\";
                    AsyncFileUpload2.SaveAs(strPath2 + strFileNmae);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardBackPath"] = null;
                }
                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload2_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void btnScanCardAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strPhotoID = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                DataTable dt = new DataTable();

                dt = (DataTable)ViewState["ScanCard"];



                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (Convert.ToString(dt.Rows[i]["HSC_CARD_NAME"]) == drpCardName.SelectedValue && chkIsDefault.Checked == true && Convert.ToString(dt.Rows[i]["HSC_ISDEFAULT"]) == "True")
                    {
                        lblStatusTab2.Text = "Only one Default card can add for Selected Card Name";
                        goto EvEnd;
                    }

                    if (Convert.ToString(dt.Rows[i]["HSC_INS_COMP_ID"]) == txtCompany.Text && Convert.ToString(dt.Rows[i]["HSC_EXPIRY_DATE"]) == txtCardExpDate.Text)
                    {
                        lblStatusTab2.Text = "This card already exits for same Expiry date";
                        goto EvEnd;
                    }


                }



                dboperations objOP = new dboperations();
                objOP.PatienId = hidActualFileNo.Value;
                objOP.BranchId = Convert.ToString(Session["Branch_ID"]);
                objOP.CardName = drpCardName.SelectedValue;
                objOP.CompId = txtCompany.Text.Trim();
                objOP.CompName = txtCompanyName.Text.Trim();
                objOP.CardNo = txtCardNo.Text.Trim();
                objOP.PolicyCardNo = txtPolicyCardNo.Text.Trim();
                objOP.ExpDate = txtCardExpDate.Text;
                objOP.PolicyStart = txtEffectiveDate.Text;


                if (chkIsDefault.Checked == true)
                {
                    objOP.isDeafult = "True";
                }
                else
                {
                    objOP.isDeafult = "False";
                }


                if (chkCardActive.Checked == true)
                {
                    objOP.Status = "True";
                }
                else
                {
                    objOP.Status = "False";
                }


                objOP.ImagePathFront = Convert.ToString(Session["InsCardFrontPath"]);
                objOP.ImagePathBack = Convert.ToString(Session["InsCardBackPath"]);
                objOP.HSC_PLAN_TYPE = "";


                objOP.HSC_PHOTO_ID = strPhotoID;


                objOP.ScanCardImageAdd(objOP);



                ClearScan();

                BindScanCardImage();
            EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnScanCardAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnScanCardUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string strPhotoID = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["ScanCard"];
                Int32 R = Convert.ToInt32(ViewState["ScanCardSelectIndex"]);

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (R != i && Convert.ToString(dt.Rows[i]["HSC_CARD_NAME"]) == drpCardName.SelectedValue && chkIsDefault.Checked == true && Convert.ToString(dt.Rows[i]["HSC_ISDEFAULT"]) == "True")
                    {
                        lblStatusTab2.Text = "Only one Default card can add for Selected Card Name";
                        goto EvEnd;
                    }



                    if (R != i && Convert.ToString(dt.Rows[i]["HSC_INS_COMP_ID"]) == txtCompany.Text && Convert.ToString(dt.Rows[i]["HSC_EXPIRY_DATE"]) == txtCardExpDate.Text)
                    {
                        lblStatusTab2.Text = "This card already exits for same Expiry date";
                        goto EvEnd;
                    }

                }

                string strisDefault = "", strStatus = "";
                if (chkIsDefault.Checked == true)
                {
                    strisDefault = "True";
                }
                else
                {
                    strisDefault = "False";
                }


                if (chkCardActive.Checked == true)
                {
                    strStatus = "True";
                }
                else
                {
                    strStatus = "False";
                }


                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "";
                string Criteria = "1=2";

                if (drpCardType.SelectedValue == "Insurance")
                {
                    FieldNameWithValues = " HSC_IMAGE_PATH_FRONT='" + Convert.ToString(Session["InsCardFrontPath"]) + "', HSC_IMAGE_PATH_BACK='" + Convert.ToString(Session["InsCardBackPath"]) + "'";
                    FieldNameWithValues += ", HSC_ISDEFAULT='" + strisDefault + "',HSC_STATUS='" + strStatus + "'";

                    Criteria = " HSC_PT_ID='" + hidActualFileNo.Value + "' AND  HSC_INS_COMP_ID='" + txtCompany.Text.Trim() + "' AND HSC_POLICY_NO='" + txtPolicyCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + txtCardExpDate.Text + "',103)";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_SCANCARD_IMAGES", Criteria);

                }
                else
                {
                    FieldNameWithValues = " HSC_IMAGE_PATH_FRONT='" + Convert.ToString(Session["InsCardFrontPath"]) + "', HSC_IMAGE_PATH_BACK='" + Convert.ToString(Session["InsCardBackPath"]) + "'";
                    FieldNameWithValues += ", HSC_ISDEFAULT='" + strisDefault + "',HSC_STATUS='" + strStatus + "'";

                    Criteria = " HSC_PT_ID='" + hidActualFileNo.Value + "' AND  HSC_CARD_NAME='" + drpCardName.SelectedValue + "' AND HSC_CARD_NO='" + txtCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + txtCardExpDate.Text + "',103)";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_SCANCARD_IMAGES", Criteria);
                }



                ClearScan();

                BindScanCardImage();
            EvEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnScanCardUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void EditScanCard_Click(object sender, EventArgs e)
        {
            try
            {
                btnScanCardAdd.Visible = false;
                btnScanCardUpdate.Visible = true;

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ScanCardSelectIndex"] = gvScanCard.RowIndex;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblCardType, lblCardName, lblCompId, lblCompName, lblgvCardNo, lblPolicyCardNo, lblExpDate, lblIsDefault, lblStatus, lblFrontPath, lblBackPath, lblEffectiveDate, lblPlanType;
                lblCardType = (Label)gvScanCard.Cells[0].FindControl("lblCardType");
                lblCardName = (Label)gvScanCard.Cells[0].FindControl("lblCardName");


                lblCompId = (Label)gvScanCard.Cells[0].FindControl("lblCompId");
                lblCompName = (Label)gvScanCard.Cells[0].FindControl("lblCompName");
                lblgvCardNo = (Label)gvScanCard.Cells[0].FindControl("lblgvCardNo");
                lblPolicyCardNo = (Label)gvScanCard.Cells[0].FindControl("lblPolicyCardNo");

                lblExpDate = (Label)gvScanCard.Cells[0].FindControl("lblExpDate");
                lblEffectiveDate = (Label)gvScanCard.Cells[0].FindControl("lblEffectiveDate");
                lblIsDefault = (Label)gvScanCard.Cells[0].FindControl("lblIsDefault");
                lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");
                lblFrontPath = (Label)gvScanCard.Cells[0].FindControl("lblFrontPath");
                lblBackPath = (Label)gvScanCard.Cells[0].FindControl("lblBackPath");
                lblPlanType = (Label)gvScanCard.Cells[0].FindControl("lblPlanType");

                string strCardType;

                strCardType = ScanCardTypeGet(lblCardName.Text);
                if (strCardType != "")
                {
                    for (int intCount = 0; intCount < drpCardType.Items.Count; intCount++)
                    {
                        if (drpCardType.Items[intCount].Value == strCardType)
                        {
                            drpCardType.SelectedValue = strCardType;
                        }
                    }
                }


                drpCardType_SelectedIndexChanged(drpCardType, new EventArgs());
                drpCardName.SelectedValue = lblCardName.Text;
                txtCardExpDate.Text = lblExpDate.Text;
                txtEffectiveDate.Text = lblEffectiveDate.Text;
                txtPolicyCardNo.Text = lblPolicyCardNo.Text;
                txtCompany.Text = lblCompId.Text;
                txtCompanyName.Text = lblCompName.Text;
                txtCardNo.Text = lblgvCardNo.Text;

                txtPlanType.Text = lblPlanType.Text;


                chkIsDefault.Checked = Convert.ToBoolean(lblIsDefault.Text);
                chkCardActive.Checked = Convert.ToBoolean(lblStatus.Text);
                //  imgFront.ImageUrl ="~/" + @Convert.ToString(ViewState["InsCardPath"]) + lblFrontPath.Text;  // @"../Uploads/" + lblFrontPath.Text;
                imgBack.ImageUrl = "~/" + @Convert.ToString(ViewState["InsCardPath"]) + lblBackPath.Text;// @"../Uploads/" + lblBackPath.Text;

                imgFront.ImageUrl = "../DisplayOtherCardImage.aspx?PT_ID=" + hidActualFileNo.Value + "&FileName=" + lblFrontPath.Text;
                imgBack.ImageUrl = "../DisplayOtherCardImage1.aspx?PT_ID=" + hidActualFileNo.Value + "&FileName=" + lblBackPath.Text;



                Session["InsCardFrontPath"] = lblFrontPath.Text;
                Session["InsCardBackPath"] = lblBackPath.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.EditScanCard_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteScanCard_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                string Criteria = "1=2";
                Label lblCardName = (Label)gvScanCard.Cells[0].FindControl("lblCardName");
                Label lblCompId = (Label)gvScanCard.Cells[0].FindControl("lblCompId");
                Label lblPolicyCardNo = (Label)gvScanCard.Cells[0].FindControl("lblPolicyCardNo");
                Label lblExpDate = (Label)gvScanCard.Cells[0].FindControl("lblExpDate");

                if (lblCardName.Text == "Insurance" || lblCardName.Text == "Topup")
                {
                    Criteria = " HSC_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HSC_INS_COMP_ID='" + lblCompId.Text + "' AND HSC_POLICY_NO='" + lblPolicyCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + lblExpDate.Text + "',103)";
                }
                else
                {
                    Criteria = " HSC_PT_ID='" + txtFileNo.Text.Trim() + "'  AND HSC_POLICY_NO='" + lblPolicyCardNo.Text + "' AND HSC_EXPIRY_DATE= Convert(datetime,'" + lblExpDate.Text + "',103)";

                }

                CommonBAL objCom = new CommonBAL();

                objCom.fnDeleteTableData("HMS_SCANCARD_IMAGES", Criteria);

                BindScanCardImage();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.DeleteScanCard_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void bntClear_Click(object sender, EventArgs e)
        {
            //btnScanCardAdd.Visible = true;
            //btnScanCardUpdate.Visible = false;
            //ClearScan();

            Response.Redirect("PTRegistration.aspx");
        }


        protected void gvScanCard_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (PTRegChangePolicyNoCaption.ToLower() == "true")
                {
                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        e.Row.Cells[3].Text = "Member ID";

                        e.Row.Cells[5].Text = "Policy No.";
                    }


                    if (GlobalValues.FileDescription.ToUpper() == "ALAMAL")
                    {
                        e.Row.Cells[3].Text = "Card No.";
                        gvScanCard.Columns[5].Visible = false;

                    }

                }
            }
        }

        void GetNationalityCode(string NationalityCode, out string NationalityName)
        {

            NationalityName = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HNM_EID_NAME ='" + NationalityCode + "'";
            DS = objCom.fnGetFieldValue(" top 1 HNM_ID,HNM_NAME", "HMS_NATIONALITY_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                NationalityName = Convert.ToString(DS.Tables[0].Rows[0]["HNM_NAME"]);
            }
        }

        void BindEmirateIdDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTE_ID Like '" + Convert.ToString(ViewState["EmirateIdRef"]) + "'";
            DS = dbo.TempEmiratesIdGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HTE_Photo") == false)
                    Session["Photo"] = (Byte[])DS.Tables[0].Rows[0]["HTE_PHOTO"];

                //chkManual.Checked = false;
                //chkManual_CheckedChanged("chkManual", new EventArgs());
                string strFullName = Convert.ToString(DS.Tables[0].Rows[0]["HTE_FullName"]).Trim().ToUpper();
                string[] ArrFullName = strFullName.Split(',');

                txtLName.Text = "";
                if (ArrFullName.Length > 0)
                {

                    for (int j = 0; j <= ArrFullName.Length - 1; j++)
                    {
                        if (j == 0)
                        {
                            txtFName.Text = ArrFullName[j];
                        }
                        else if (j == 1)
                        {
                            txtMName.Text = ArrFullName[j];
                        }
                        else if (j >= 2)
                        {
                            txtLName.Text += ArrFullName[j].Replace(",", "") + " ";
                        }
                    }

                }
                string EmiratesID = "";
                EmiratesID = Convert.ToString(DS.Tables[0].Rows[0]["HTE_IDNumber"]).Trim();

                string FormatedEmiratesID = "";
                if (EmiratesID.Length > 0)
                {

                    for (int j = 0; j <= EmiratesID.Length; j++)
                    {

                        if (j == 2)
                        {
                            FormatedEmiratesID = EmiratesID.Substring(0, 3).Trim();
                        }
                        if (j == 6)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(3, 4).Trim();
                        }

                        if (j == 14)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(7, 7).Trim();
                        }

                        if (j == 15)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(14, 1).Trim();
                        }
                    }
                }

                txtEmiratesID.Text = FormatedEmiratesID.Trim();



                if (EmiratesID.Length == 3)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }
                if (EmiratesID.Length == 8)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }
                if (EmiratesID.Length == 16)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }


                txtCardNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CardNo"]).Trim();

                string strDOB = Convert.ToString(DS.Tables[0].Rows[0]["HTE_DOBDesc"]).Trim();
                string[] arrDOB = strDOB.Split(' ');

                if (arrDOB.Length > 0)
                {
                    txtDOB.Text = arrDOB[0];
                }

                if (DS.Tables[0].Rows[0].IsNull("HTE_Sex") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]) != "0")
                {
                    for (int intSex = 0; intSex < drpSex.Items.Count; intSex++)
                    {
                        if (drpSex.Items[intSex].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Sex"]).Trim();
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;

                if (DS.Tables[0].Rows[0].IsNull("HTE_Nationality") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_Nationality"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_Nationality"]) != "0")
                {

                    string NationalityCode = "", NationalityName = "";

                    NationalityCode = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Nationality"]);

                    GetNationalityCode(NationalityCode, out  NationalityName);


                    for (int intMstatus = 0; intMstatus < drpNationality.Items.Count; intMstatus++)
                    {
                        if (drpNationality.Items[intMstatus].Value == NationalityName)
                        {
                            drpNationality.SelectedValue = NationalityName;
                            goto ForNationality;
                        }

                    }
                }
            ForNationality: ;


                if (DS.Tables[0].Rows[0].IsNull("HTE_MaritalStatus") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]) != "0")
                {
                    for (int intMstatus = 0; intMstatus < drpMStatus.Items.Count; intMstatus++)
                    {
                        if (drpMStatus.Items[intMstatus].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]))
                        {
                            drpMStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_MaritalStatus"]).Trim();
                            goto ForMStatus;
                        }

                    }
                }
            ForMStatus: ;


                if (DS.Tables[0].Rows[0].IsNull("HTE_CityDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]) != "0")
                {
                    for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                    {
                        if (drpCity.Items[intCityCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]))
                        {
                            drpCity.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CityDesc"]);
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;


                if (DS.Tables[0].Rows[0].IsNull("HTE_AreaDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]) != "0")
                {
                    for (int intAreaCount = 0; intAreaCount < drpArea.Items.Count; intAreaCount++)
                    {
                        if (drpArea.Items[intAreaCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]))
                        {
                            drpArea.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HTE_AreaDesc"]);
                            goto ForArea;
                        }

                    }
                }
            ForArea: ;

                txtPoBox.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_POBox"]).Trim();
                txtAddress.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Address"]).Trim();
                txtPhone1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Phone"]).Trim();
                if (lblVisitType.Text.Trim() == "New")
                    txtMobile1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_Mobile"]).Trim();

                if (DS.Tables[0].Rows[0].IsNull("HTE_CompanyName") == false && Convert.ToString(DS.Tables[0].Rows[0]["HTE_CompanyName"]).Trim() != "")
                {
                    txtPTCompany.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CompanyName"]).Trim();
                }


                if (txtDOB.Text.Trim() != "")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                    AgeCalculation();
                }

                BindPhoto();


            }
        }

        void BindEmiretsId()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTE_ID Like '" + Convert.ToString(ViewState["EmirateIdRef"]) + "'";
            DS = dbo.TempEmiratesIdGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HTE_Photo") == false)
                    Session["Photo"] = (Byte[])DS.Tables[0].Rows[0]["HTE_PHOTO"];


                string strEmiratesID = (string)DS.Tables[0].Rows[0]["HTE_IDNUMBER"];
                string strFormatedEmiratesID = "";

                string strEIDPart1 = "", strEIDPart2 = "", strEIDPart3 = "", strEIDPart4 = "";

                strEIDPart1 = strEmiratesID.Substring(0, 3);
                strEIDPart2 = strEmiratesID.Substring(3, 4);
                strEIDPart3 = strEmiratesID.Substring(7, 7);
                strEIDPart4 = strEmiratesID.Substring(14, 1);


                strFormatedEmiratesID = strEIDPart1 + "-" + strEIDPart2 + "-" + strEIDPart3 + "-" + strEIDPart4;

                DataSet ds1 = new DataSet();
                Criteria = " ( HPM_IQAMA_NO  = '" + Convert.ToString(DS.Tables[0].Rows[0]["HTE_IDNUMBER"]).Trim() + "'  OR  HPM_IQAMA_NO  = '" + strFormatedEmiratesID + "'  )";
                ds1 = dbo.retun_patient_details(Criteria);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(ViewState["EIDConfirmResult"]) != "Yes")
                    {

                        // divConfirmMessageClose.Style.Add("display", "block");
                        divConfirmMessage.Style.Add("display", "block");
                        lblConfirmMessage.Text = "Would you like to replace the patient information from Emirates ID Card?";// "Do you want to update Informationg as per Emirates ID?";
                        ViewState["ConfirmMsgFor"] = "EmiratesIDLoad";
                        ViewState["EmiratesIDPT_ID"] = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();
                        ViewState["EIDConfirmResult"] = "";
                        goto FunEnd;



                    }

                    DS.Dispose();


                }

                BindEmirateIdDtls();

            }

        FunEnd: ;


        }

        protected void btnScan_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                chkFamilyMember.Checked = false;
                // Clear();


                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                ViewState["EmirateIdRef"] = strName;
                String DBString = "";
                DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "GetEmiratesIdData('" + @DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnScan_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpDefaultCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (drpDefaultCompany.SelectedIndex != 0)
                {
                    stParentName1 = drpDefaultCompany.SelectedValue.Trim();
                }


                string strRemainderRecept = "";
                BindCompanyDetails(out strRemainderRecept);

                strReminderMessage = "Message from Company Master - " + strRemainderRecept;



                if (strRemainderRecept != "")
                {

                    //ModalPopupExtender5.Show();
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' COMPANY MESSAGE ',' " + strReminderMessage + "','Brown')", true);

                }
                BindSubCompany();
                BindPlanType();


                // BindScanCardImageSelected();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpDefaultCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void drpCardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BinScanCardMasterGet();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpCardType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void bntFamilyApt_Click(object sender, EventArgs e)
        {
            try
            {
                CheckFamilyAppoinment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.bntFamilyApt_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpParentCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDefaultCompany();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpParentCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " txtFileNo_TextChanged Start" + strNewLineChar);

                hidActualFileNo.Value = "";
                txtFileNo.Text = txtFileNo.Text.ToUpper();
                if (chkFamilyMember.Checked != true)
                {
                    Clear();
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " Clear() completed" + strNewLineChar);
                    BindModifyData();
                    BindReferral();
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " BindModifyData() completed" + strNewLineChar);
                }
                // drpPatientType.Focus();

                if (Convert.ToString(ViewState["REQ_PT_ZERO_BALANCE_DUE"]) == "1")
                {
                    Decimal CreditBalAmt = (fnCalculatePTAmount("INVOICE") - fnCalculatePTAmount("RETURN")) - (fnCalculatePTAmount("PAIDAMT") + fnCalculatePTAmount("RETURNPAIDAMT") + fnCalculatePTAmount("REJECTED")) - fnCalculatePTAmount("NPPAID");
                    if (CreditBalAmt > 0)
                    {
                        strMessage = "Balance Amount Due : " + CreditBalAmt.ToString("#0.00");  //HMS_CURRENCY_FORMAT)
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' PATIENT BALABCE ',' " + strMessage + ".','Brown')", true);

                    }
                }

                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " txtFileNo_TextChanged End" + strNewLineChar);


                if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1" || Convert.ToString(Session["ENABLE_USER_LOG"]) == "1")
                {
                    ProcessTimeLog(strPageTimeLog.ToString());
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1" || Convert.ToString(Session["ENABLE_USER_LOG"]) == "1")
                {
                    ProcessTimeLog(strPageTimeLog.ToString());
                }

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void txtMobile1_TextChanged(object sender, EventArgs e)
        {
            GetAppoinmentDtls();
            if (CheckMobileNo() == true)
            {
                // ModalPopupExtender1.Show();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' MOBILE NO ',' " + strMessage + ".','Brown')", true);

            }

            txtMobile2.Focus();
        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {


            txtTokenNo.Text = "";
            txtDepName.Text = "";
            string strName = txtDoctorID.Text.Trim();

            string[] arrName = strName.Split('~');
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'  AND  HSFM_SF_STATUS='Present'";

            DataSet DS = new DataSet();


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0].Trim();
                txtDoctorName.Text = arrName[1].Trim();
                Criteria += " and HSFM_STAFF_ID='" + arrName[0].Trim() + "' ";
                DS = dbo.retun_doctor_detailss(Criteria);
            }
            else
            {
                Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text.Trim() + "'";
                DS = dbo.retun_doctor_detailss(Criteria);
            }


            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]).Trim();
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["TokenNo"]).Trim();
                txtDepName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]).Trim();
            }
            // if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            // {
            GetPatientVisit();
            // }






        }

        protected void txtDoctorName_TextChanged(object sender, EventArgs e)
        {

            txtTokenNo.Text = "";
            txtDepName.Text = "";
            string strName = txtDoctorName.Text.Trim();

            string[] arrName = strName.Split('~');
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'  AND  HSFM_SF_STATUS='Present'";

            DataSet DS = new DataSet();


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0].Trim();
                txtDoctorName.Text = arrName[1].Trim();
                Criteria += " and HSFM_STAFF_ID='" + arrName[0].Trim() + "' ";
                DS = dbo.retun_doctor_detailss(Criteria);
            }
            else
            {
                Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text + "'";
                DS = dbo.retun_doctor_detailss(Criteria);
            }


            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]).Trim();
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["TokenNo"]).Trim();
                txtDepName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]).Trim();
            }

            // if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            // {
            GetPatientVisit();
            // }




        }




        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {

            string strName = txtCompany.Text;
            string[] arrName = strName.Split('~');

            strCompID = txtCompany.Text.Trim();
            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];
                strCompID = txtCompany.Text.Trim();
            }

        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {

            string strName = txtCompanyName.Text;
            string[] arrName = strName.Split('~');
            strCompID = txtCompany.Text.Trim();

            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];
                strCompID = txtCompany.Text.Trim();
            }


        }


        protected void drpNetworkName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPlanType();
        }

        protected void drpPlanType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GlobalValues.FileDescription.ToUpper() != "ALAMAL")
            {
                BindDeductCoInsAmt();
            }
        }







        protected void btnHCBShow_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (drpDefaultCompany.Items.Count > 0)
                {
                    if (drpDefaultCompany.SelectedItem.Text != "")
                    {
                        BindDeductCoInsGrid();
                        ModalPopupExtender7.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnHCBShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void lnkEmIdShow_Click(object sender, EventArgs e)
        {
            try
            {

                BindEmiretsId();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.lnkEmIdShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtNetworkClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = "1=1 and  HICM_NOT_COVERED='Y' ";
                Criteria += " AND HICM_INS_CAT_NAME='" + txtNetworkClass.Text.Trim() + "'";

                DataSet DS = new DataSet();

                dbo = new dboperations();

                DS = dbo.InsCatMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    //   strMessage = "Selected Network is not covered.";
                    //  ModalPopupExtender1.Show();

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  NETWORK NOT COVERED. ',' Selected Network is not covered.','Brown')", true);

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.txtNetworkClass_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void ImgbtnSig1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                ViewState["Signature1"] = strName;
                String DBString = "";
                DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignature('" + DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.ImgbtnSig1_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void lnkViewSig1_Click(object sender, EventArgs e)
        {
            try
            {
                //  Session["PatientID"] = hidActualFileNo.Value;
                imgPatientSig1.ImageUrl = "../DisplaySignaturePatient.aspx?PatientID=" + hidActualFileNo.Value;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignaturePopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.ImgbtnSig1_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }



        protected void AsyncFileUpload3_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                string strPath = @Convert.ToString(ViewState["InsCardPath"]);

                if (!Directory.Exists(@strPath))
                {
                    Directory.CreateDirectory(@strPath);
                }

                if (!Directory.Exists(@strPath + @"Temp\"))
                {
                    Directory.CreateDirectory(@strPath + @"Temp\");
                }
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;


                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));

                    AsyncFileUpload3.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardFrontDefaultPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardFrontDefaultPathT"] = strFileNmae;
                    string strPath1 = Server.MapPath("../Uploads/Temp/");//@strPath + @"Temp\"; //
                    AsyncFileUpload3.SaveAs(strPath1 + strFileNmae);


                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardFrontDefaultPath"] = null;
                }

                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload1_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AsyncFileUpload4_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                string strPath = @Convert.ToString(ViewState["InsCardPath"]);

                if (!Directory.Exists(@strPath))
                {
                    Directory.CreateDirectory(@strPath);
                }

                if (!Directory.Exists(@strPath + @"Temp\"))
                {
                    Directory.CreateDirectory(@strPath + @"Temp\");
                }

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + System.DateTime.Now.TimeOfDay.Milliseconds;

                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));

                    AsyncFileUpload4.SaveAs(strPath + strName + Path.GetExtension(strFileNmae));
                    Session["InsCardBackDefaultPath"] = strName + Path.GetExtension(strFileNmae);// e.FileName;
                    Session["InsCardBackDefaultPathT"] = strFileNmae;
                    string strPath2 = Server.MapPath("../Uploads/Temp/"); // strPath + @"Temp\";
                    AsyncFileUpload4.SaveAs(strPath2 + strFileNmae);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["InsCardBackDefaultPath"] = null;
                }
                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.AsyncFileUpload2_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnConfirmYes_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["EIDConfirmResult"] = "Yes";
                ViewState["ConfirmResult"] = "Yes";
                ViewState["LoadFromEmiratesID"] = "1";

                // divConfirmMessageClose.Style.Add("display", "none");
                divConfirmMessage.Style.Add("display", "none");
                if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "EmiratesIDLoad")
                {


                    txtFileNo.Text = Convert.ToString(ViewState["EmiratesIDPT_ID"]);
                    BindModifyData();
                    BindEmirateIdDtls();
                }
                else if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "NewEmiratesIDLoad")
                {
                    txtFileNo.Text = Convert.ToString(ViewState["EmiratesIDPT_ID"]);
                    BindModifyData();
                    BindPatientPhoto();
                }
                else if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "DuplicateNameSave")
                {
                    btnSave_Click(btnSave, new EventArgs());

                }


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnConfirmNo_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["EIDConfirmResult"] = "No";
                ViewState["ConfirmResult"] = "No";
                ViewState["LoadFromEmiratesID"] = "0";
                // divConfirmMessageClose.Style.Add("display", "none");
                divConfirmMessage.Style.Add("display", "none");

                if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "EmiratesIDLoad")
                {
                    txtFileNo.Text = Convert.ToString(ViewState["EmiratesIDPT_ID"]);
                    BindModifyData();

                }
                if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "NewEmiratesIDLoad")
                {
                    txtFileNo.Text = Convert.ToString(ViewState["EmiratesIDPT_ID"]);
                    BindModifyData();
                    BindPatientPhoto();
                }
                else if (Convert.ToString(ViewState["ConfirmMsgFor"]) == "DuplicateNameSave")
                {


                }



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void imgPlanRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindPlanType();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.imgPlanRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        #endregion



        #region TabGeneralConsent

        void BindTempImage1()
        {
            Byte[] bytImage = null;

            string strPath = Server.MapPath("../Uploads/Signature/" + Convert.ToString(ViewState["Signature1"]) + ".jpg");
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTI_PT_ID Like '" + Convert.ToString(ViewState["Signature1"]) + "'";
            DS = dbo.TempImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HTI_IMAGE1") == false)
                {
                    bytImage = (byte[])DS.Tables[0].Rows[0]["HTI_IMAGE1"];
                    Session["SignatureImg1"] = (byte[])DS.Tables[0].Rows[0]["HTI_IMAGE1"];
                    imgSig1.ImageUrl = "../DisplaySignature1.aspx";

                    // Response.Redirect("../DisplayCardImage.aspx");
                }

                System.IO.FileStream MonFileStream = new System.IO.FileStream(strPath, System.IO.FileMode.Create);
                MonFileStream.Write(bytImage, 0, bytImage.Length - 1);
                MonFileStream.Close();

                // imgSig1.ImageUrl = strPath;

                dbo.TempImageDelete(Convert.ToString(ViewState["Signature1"]));
            }


        }

        void BindTempImage2()
        {
            Byte[] bytImage = null;

            string strPath = Server.MapPath("../Uploads/Signature/" + Convert.ToString(ViewState["Signature2"]) + ".jpg");
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTI_PT_ID Like '" + Convert.ToString(ViewState["Signature2"]) + "'";
            DS = dbo.TempImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HTI_IMAGE1") == false)
                {
                    bytImage = (byte[])DS.Tables[0].Rows[0]["HTI_IMAGE1"];
                    Session["SignatureImg2"] = (byte[])DS.Tables[0].Rows[0]["HTI_IMAGE1"];
                    imgSig2.ImageUrl = "../DisplaySignature2.aspx";

                    // Response.Redirect("../DisplayCardImage.aspx");
                }

                System.IO.FileStream MonFileStream = new System.IO.FileStream(strPath, System.IO.FileMode.Create);
                MonFileStream.Write(bytImage, 0, bytImage.Length - 1);
                MonFileStream.Close();

                // imgSig1.ImageUrl = strPath;

                dbo.TempImageDelete(Convert.ToString(ViewState["Signature2"]));
            }


        }

        void BindTempImage3()
        {
            Byte[] bytImage = null;

            string strPath = Server.MapPath("../Uploads/Signature/" + Convert.ToString(ViewState["Signature3"]) + ".jpg");
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HTI_PT_ID Like '" + Convert.ToString(ViewState["Signature3"]) + "'";
            DS = dbo.TempImageGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HTI_IMAGE1") == false)
                {
                    bytImage = (byte[])DS.Tables[0].Rows[0]["HTI_IMAGE1"];
                    Session["SignatureImg3"] = (byte[])DS.Tables[0].Rows[0]["HTI_IMAGE1"];
                    imgSig3.ImageUrl = "../DisplaySignature3.aspx";

                    // Response.Redirect("../DisplayCardImage.aspx");
                }

                System.IO.FileStream MonFileStream = new System.IO.FileStream(strPath, System.IO.FileMode.Create);
                MonFileStream.Write(bytImage, 0, bytImage.Length - 1);
                MonFileStream.Close();

                // imgSig1.ImageUrl = strPath;

                dbo.TempImageDelete(Convert.ToString(ViewState["Signature3"]));
            }


        }


        protected void btnSignature1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                //ViewState["Signature1"] = strName;
                //String DBString = "";
                //DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignature('" + DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);

                if (Convert.ToString(ViewState["HGC_ID"]) != "" && Convert.ToString(ViewState["HGC_ID"]) != null)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGeneralConsentWacomSign('" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(ViewState["HGC_ID"]) + "','PT_SIGN');", true);
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSignature1_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void btnSignature2_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                //ViewState["Signature2"] = strName;
                //String DBString = "";
                //DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignature('" + DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
                if (Convert.ToString(ViewState["HGC_ID"]) != "" && Convert.ToString(ViewState["HGC_ID"]) != null)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGeneralConsentWacomSign('" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(ViewState["HGC_ID"]) + "','SUBS_SIGN');", true);
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSignature2_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void btnSignature3_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                //ViewState["Signature3"] = strName;
                //String DBString = "";
                //DBString = strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSignature('" + DBString + "','" + strName + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
                if (Convert.ToString(ViewState["HGC_ID"]) != "" && Convert.ToString(ViewState["HGC_ID"]) != null)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGeneralConsentWacomSign('" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(ViewState["HGC_ID"]) + "','TRAN_SIGN');", true);
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSignature3_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnSigShow1_Click(object sender, EventArgs e)
        {
            try
            {
                // BindTempImage1();

                imgSig1.ImageUrl = "../DisplaySignature1.aspx?PT_ID=" + hidActualFileNo.Value + "&HGC_ID=" + Convert.ToString(ViewState["HGC_ID"]);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSigShow1_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnSigShow2_Click(object sender, EventArgs e)
        {
            try
            {
                // BindTempImage2();
                imgSig2.ImageUrl = "../DisplaySignature2.aspx?PT_ID=" + hidActualFileNo.Value + "&HGC_ID=" + Convert.ToString(ViewState["HGC_ID"]);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSigShow2_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnSigShow3_Click(object sender, EventArgs e)
        {
            try
            {
                // BindTempImage3();
                imgSig3.ImageUrl = "../DisplaySignature3.aspx?PT_ID=" + hidActualFileNo.Value + "&HGC_ID=" + Convert.ToString(ViewState["HGC_ID"]);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSigShow3_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void gvGCGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGCGridView.PageIndex = e.NewPageIndex;
            BindGCGrid();

        }

        protected void gvGCGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGCGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.gvGCGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void gvGCGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                    lblSerial.Text = ((gvGCGridView.PageIndex * gvGCGridView.PageSize) + e.Row.RowIndex + 1).ToString();
                }
            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.gvGCGridView_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void GCReport_Click(object sender, EventArgs e)
        {
            try
            {
                btnScanCardAdd.Visible = false;
                btnScanCardUpdate.Visible = true;

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvGCGridView;
                gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
                //  ViewState["ScanCardSelectIndex"] = gvGCGridView.RowIndex;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblSeqNo;
                lblSeqNo = (Label)gvGCGridView.Cells[0].FindControl("lblSeqNo");

                Int64 intGCId = 0;
                intGCId = Convert.ToInt64(lblSeqNo.Text.Trim());

                //AddSignatureTempImage(intGCId);

                //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - START
                // string strReport = "GeneralTreatmentConsent.rpt";
                //String DBString = "", strFormula = "";
                //DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;
                //strFormula = hidActualFileNo.Value;
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGCReport('" + DBString + "'," + ViewState["HGC_ID"] + ");", true);
                //// CODING FOR CALL THE CRYSTAL REPORT USING VB.EXE - END
                // dbo.TempImageDelete(txtFileNo.Text.Trim());

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowGridGCReportPDF('" + lblSeqNo.Text + "');", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.GCReport_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void GCSelect_Click(object sender, EventArgs e)
        {
            try
            {
                btnScanCardAdd.Visible = false;
                btnScanCardUpdate.Visible = true;

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvGCGridView;
                gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
                //  ViewState["ScanCardSelectIndex"] = gvGCGridView.RowIndex;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;



                Label lblSeqNo = (Label)gvGCGridView.Cells[0].FindControl("lblSeqNo");
                Label lblSubstituteName = (Label)gvGCGridView.Cells[0].FindControl("lblSubstituteName");
                Label lblSubstituteAge = (Label)gvGCGridView.Cells[0].FindControl("lblSubstituteAge");
                Label lblSubstituteNational = (Label)gvGCGridView.Cells[0].FindControl("lblSubstituteNational");
                Label lblSubstitutePolicy = (Label)gvGCGridView.Cells[0].FindControl("lblSubstitutePolicy");
                Label lblSubstituteType = (Label)gvGCGridView.Cells[0].FindControl("lblSubstituteType");
                Label lblSubstituteCapacity = (Label)gvGCGridView.Cells[0].FindControl("lblSubstituteCapacity");
                Label lblTransalator = (Label)gvGCGridView.Cells[0].FindControl("lblTransalator");

                ViewState["HGC_ID"] = lblSeqNo.Text;

                txtSubstitute.Text = lblSubstituteName.Text;
                txtSubsAge.Text = lblSubstituteAge.Text;

                for (int intCount = 0; intCount < drpSubsNationality.Items.Count; intCount++)
                {
                    if (drpSubsNationality.Items[intCount].Value == lblSubstituteNational.Text)
                    {
                        drpSubsNationality.SelectedValue = lblSubstituteNational.Text;
                        goto ForPTType;
                    }

                }

            ForPTType: ;


                txtSubsIdNo.Text = lblSubstitutePolicy.Text;
                txtSubsType.Text = lblSubstituteType.Text;
                txtSubsCapacity.Text = lblSubstituteCapacity.Text;
                txtTranslator.Text = lblTransalator.Text;

                imgSig1.ImageUrl = "../DisplaySignature1.aspx?PT_ID=" + hidActualFileNo.Value + "&HGC_ID=" + Convert.ToString(ViewState["HGC_ID"]);
                imgSig2.ImageUrl = "../DisplaySignature2.aspx?PT_ID=" + hidActualFileNo.Value + "&HGC_ID=" + Convert.ToString(ViewState["HGC_ID"]);
                imgSig3.ImageUrl = "../DisplaySignature3.aspx?PT_ID=" + hidActualFileNo.Value + "&HGC_ID=" + Convert.ToString(ViewState["HGC_ID"]);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.GCSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteGC_Click(object sender, EventArgs e)
        {
            try
            {
                btnScanCardAdd.Visible = false;
                btnScanCardUpdate.Visible = true;

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvGCGridView;
                gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;


                Label lblSeqNo = (Label)gvGCGridView.Cells[0].FindControl("lblSeqNo");


                ViewState["HGC_ID"] = lblSeqNo.Text;
                CommonBAL objCom = new CommonBAL();

                string Criteria = " HGC_ID=" + lblSeqNo.Text;
                objCom.fnDeleteTableData("HMS_GENERAL_CONSENT", Criteria);

                BindGCGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.DeleteGC_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSaveConsentForm_Click(object sender, EventArgs e)
        {
            try
            {
                // GENERALCONSENT SAVE - START
                clsGeneralConsent objGC = new clsGeneralConsent();
                objGC.BranchId = Convert.ToString(Session["Branch_ID"]);
                objGC.PatienId = hidActualFileNo.Value;
                objGC.PatienSignature = "";
                objGC.SubsName = txtSubstitute.Text.Trim();
                objGC.SubsSignature = "";
                objGC.SubsAge = txtSubsAge.Text.Trim();
                if (drpSubsNationality.SelectedIndex != 0)
                {
                    objGC.SubsNationality = drpSubsNationality.SelectedValue;
                }
                else
                {
                    objGC.SubsNationality = "";
                }

                objGC.SubsIdNo = txtSubsIdNo.Text.Trim();
                objGC.SubsType = txtSubsType.Text.Trim();
                objGC.SubsCapacity = txtSubsCapacity.Text.Trim();

                objGC.TranName = txtTranslator.Text.Trim();
                objGC.TranSignature = "";
                objGC.DrId = txtDoctorID.Text.Trim();
                objGC.DrName = txtDoctorName.Text.Trim();
                objGC.UserId = Convert.ToString(Session["User_ID"]);
                objGC.GeneralConsentAdd();



                // GENERALCONSENT SAVE -   END

                BindGCGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.btnSaveConsentForm_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        #endregion

        protected void imgPlanPopup_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPlanPopup()", true);

            BindPlanPopup();
        }

        protected void txtSrcPlanName_TextChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPlanPopup()", true);
            BindPlanPopup();
        }

        protected void imgPlanSelect_Click(object sender, EventArgs e)
        {

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePlanPopup()", true);

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblCompID = (Label)gvScanCard.Cells[0].FindControl("lblCompID");
            Label lblCompName = (Label)gvScanCard.Cells[0].FindControl("lblCompName");
            Label lblCatType = (Label)gvScanCard.Cells[0].FindControl("lblCatType");



            drpDefaultCompany.SelectedValue = lblCompID.Text;

            drpDefaultCompany_SelectedIndexChanged(drpDefaultCompany, new EventArgs());
            drpPlanType.SelectedValue = lblCatType.Text;

        }


        string GetHAADNationalityCode()
        {
            string strHaadCode = "";

            CommonBAL objCom = new CommonBAL();
            string Criteria = "HNM_NAME='" + drpNationality.SelectedValue + "'";
            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue("HNM_HAAD_CODE", " HMS_NATIONALITY_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strHaadCode = Convert.ToString(DS.Tables[0].Rows[0]["HNM_HAAD_CODE"]);

            }


            return strHaadCode;
        }

        void GeneratePersonRegistertXML(out   string ReturanMessage, out string FileName)
        {
            ReturanMessage = "";
            FileName = "";

            string strDate = "", strTime = "";

            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");


            StringBuilder strData = new StringBuilder();

            // date.SetAttribute("modified", DateTime.Now.ToString());
            XmlDocument XD = new XmlDocument();
            XD.CreateXmlDeclaration("1.0", "utf-8", "yes");
            XD.CreateProcessingInstruction("xml", "version='1.0' encoding='utf-8'");



            strData.Append("<?xml version='1.0' encoding='utf-8' ?>");

            strData.Append(@"<Person.Register xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='https://www.haad.ae/DataDictionary/CommonTypes/PriorRequest.xsd'> ");



            strData.Append("<Header>");

            strData.Append("<SenderID>" + Convert.ToString(ViewState["PROVIDER_ID"]) + "</SenderID>");
            strData.Append("<ReceiverID>HAAD</ReceiverID>");//ReceiverID
            strData.Append("<TransactionDate>" + strDate + "</TransactionDate>");
            strData.Append("<RecordCount>1</RecordCount>");
            strData.Append("<DispositionFlag>" + Convert.ToString(ViewState["HAC_DISPOSITION_FLAG"]) + "</DispositionFlag>");
            strData.Append("</Header>");

            FileName = Convert.ToString(ViewState["PROVIDER_ID"]) + "-" + strTimeStamp + hidActualFileNo.Value;

            string MemberID = Convert.ToString(ViewState["PROVIDER_ID"]) + "#" + hidActualFileNo.Value;

            strData.Append("<Person>");
            //strData.Append("<UnifiedNumber>" + txtEmiratesID.Text.Trim() + "</UnifiedNumber>");// this will allow only number
            strData.Append("<FirstName>" + txtFName.Text.Trim() + "</FirstName>");
            strData.Append("<FirstNameEn>" + txtFName.Text.Trim() + "</FirstNameEn>");
            strData.Append("<MiddleNameEn>" + txtMName.Text.Trim() + "</MiddleNameEn>");
            strData.Append("<LastNameEn>" + txtLName.Text.Trim() + "</LastNameEn>");
            strData.Append("<ContactNumber>" + txtMobile1.Text.Trim() + "</ContactNumber>");
            strData.Append("<BirthDate>" + txtDOB.Text.Trim() + "</BirthDate>");


            Int32 Gender = 9;
            if (drpSex.SelectedValue == "Male")
            {
                Gender = 1;
            }
            else if (drpSex.SelectedValue == "Female")
            {
                Gender = 0;
            }

            strData.Append("<Gender>" + Gender + "</Gender>");

            string Nationality = GetHAADNationalityCode();

            strData.Append("<Nationality>" + Nationality + "</Nationality>");  // this will allow only HAAD list countries and it should match
            //strData.Append("<NationalityCode>" + drpNationality.SelectedValue + "</NationalityCode>");

            strData.Append("<City>" + drpCity.SelectedValue + "</City>");
            //strData.Append("<CityCode>" + drpCity.SelectedValue + "</CityCode>");

            strData.Append("<CountryOfResidence>" + drpCountry.SelectedValue + "</CountryOfResidence>");



            if (drpCountry.SelectedValue == "United Arab Emirates")
            {
                strData.Append("<EmirateOfResidence>" + drpCity.SelectedValue.Replace("AL AIN", "Abu Dhabi") + "</EmirateOfResidence>");
            }


            if (drpIDCaption.SelectedValue != "EmiratesId")
            {
                strData.Append("<PassportNumber>" + txtEmiratesID.Text.Trim() + "</PassportNumber>");

            }

            strData.Append("<EmiratesIDNumber>" + txtEmiratesID.Text.Trim() + "</EmiratesIDNumber>");



            strData.Append("<Member>");
            strData.Append("<ID>" + MemberID + "</ID>");
            //strData.Append("<Relation>" + txtEmiratesID.Text.Trim() + "</Relation>");
            // strData.Append("<RelationTo>" + txtEmiratesID.Text.Trim() + "</RelationTo>");

            strData.Append("</Member>");



            strData.Append("</Person>");
            strData.Append("</Person.Register>");

            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " GeneratePersonRegistertXML() 1" + strNewLineChar);

            string DamanPrescriptionRequest = Convert.ToString(strData.Replace("&", " "));
            string strPath = Convert.ToString(ViewState["UPLOAD_FILEPATH"]);
            string strFileId = Convert.ToString(FileName);
            string strFileName = "", strFileName1 = "";


            strFileName = strPath + "PERREG_" + strFileId.Replace("/", "_") + ".XML";
            strFileName1 = "PERREG_" + strFileId.Replace("/", "_");

            StreamWriter oWrite;
            oWrite = File.CreateText(strFileName);
            oWrite.WriteLine(DamanPrescriptionRequest);
            oWrite.Close();
            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " GeneratePersonRegistertXML() 2" + strNewLineChar);
            //   goto LoopEnd;

            WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");
            string errorMessage;

            byte[] fileContent, errorReport;
            fileContent = System.Text.Encoding.UTF8.GetBytes(DamanPrescriptionRequest);
            soapclient.UploadTransaction(Convert.ToString(ViewState["LOGIN_ID"]), Convert.ToString(ViewState["LOGIN_PASSWORD"]), fileContent, strFileName1 + ".XML", out errorMessage, out errorReport);


            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " GeneratePersonRegistertXML() 3" + strNewLineChar);
            //using (FileStream fs = new FileStream(strFileName, FileMode.Create))
            //{
            //    fs.Write(fileContent, 0, fileContent.Length);
            //}

            //   Console.WriteLine(errorMessage);
            ReturanMessage = errorMessage;
            if (!errorMessage.Equals("") && errorReport != null && errorReport.Length > 0)
            {


                string strErrorPath = Convert.ToString(ViewState["HAC_ERROR_FILEPATH"]);
                string strErrorFileName = strErrorPath + "Error_" + strFileName1 + ".zip";
                ViewState["ErrorFileFullPath"] = strErrorFileName;

                using (FileStream fs = new FileStream(strErrorFileName, FileMode.Create))
                {
                    fs.Write(errorReport, 0, errorReport.Length);
                }

            }
            if (errorMessage.Contains("successfully") == true || errorMessage.Contains("SUCCESSFULLY") == true)
            {
                CommonBAL objCom = new CommonBAL();
                string Criteria = " HPM_PT_ID='" + hidActualFileNo.Value + "'";
                string FieldNameWithValues = " HPM_HAAD_REGISTER=1 ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_MASTER", Criteria);

                lblHAADRegister.Text = "Yes";
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' DOH REGISTER',' Patient Register at DOH ','Green')", true);

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' DOH REGISTER','Patient Not Register, Please check the error file','Brown')", true);

            }


        LoopEnd: ;

        }

        protected void btnHaadRegister_Click(object sender, EventArgs e)
        {
            try
            {
                string ReturanMessage;
                ReturanMessage = "";
                string FileName = "";

                if (lblHAADRegister.Text == "Yes")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' DOH REGISTER',' Patient already Register at DOH','Brown')", true);

                    goto FunEnd;
                }

                GeneratePersonRegistertXML(out ReturanMessage, out FileName);

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnHaadRegister_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void chkRequiredTriage_CheckedChanged(object sender, EventArgs e)
        {

            if (chkRequiredTriage.Checked == true)
            {
                trTriageReason.Visible = false;
                drpTriageReason.SelectedIndex = 0;
            }
            else
            {
                trTriageReason.Visible = true;
            }

        }



        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvEnquiryMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvEnquiryMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblEnquiryID = (Label)gvScanCard.Cells[0].FindControl("lblEnquiryID");
                Label lblEnquiryDate = (Label)gvScanCard.Cells[0].FindControl("lblEnquiryDate");
                Label lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");
                Label lblPhone = (Label)gvScanCard.Cells[0].FindControl("lblPhone");
                Label lblgvEnqMobile = (Label)gvScanCard.Cells[0].FindControl("lblgvEnqMobile");
                Label lblEMail = (Label)gvScanCard.Cells[0].FindControl("lblEMail");




                txtFName.Text = lblName.Text;
                txtPhone1.Text = lblPhone.Text;
                txtMobile1.Text = lblgvEnqMobile.Text;
                txtEmail.Text = lblEMail.Text;


                ViewState["EnquiryID"] = lblEnquiryID.Text;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideEnquiryMaster()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void imgEnqRefresh_Click(object sender, ImageClickEventArgs e)
        {
            BindEnquiryMaster();
        }

        protected void drpPTVisitCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

            txtPTVisitPolicyNo.Text = "";
            txtPTVisitCardNo.Text = "";
            txtPTVisitCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();

            if (drpPTVisitType.SelectedValue == "CU")
            {
                txtPTVisitPolicyNo.Text = GlobalValues.HospitalLicenseNo + "#" + txtFileNo.Text.Trim();
            }

            BindPTVisitCompanyPlan();
        }


        bool CheckVisitExist()
        {
            Int32 intMultipleVisitADay = 7;

            if (Convert.ToString(Session["MultipleVisitADay"]) != "")
            {
                intMultipleVisitADay = Convert.ToInt32(Session["MultipleVisitADay"]);
            }

            intMultipleVisitADay = intMultipleVisitADay * 60;

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HPV_PT_ID='" + txtFileNo.Text.Trim() + "' and  HPV_DR_ID='" + txtDoctorID.Text.Trim() + "' AND  DATEDIFF(DD,HPV_DATE,getdate()) =0 AND  DATEDIFF(mi,HPV_DATE,getdate()) <= " + intMultipleVisitADay;
            Criteria += " AND HPV_STATUS <> 'D' ";
            DS = objCom.fnGetFieldValue("top 1 *", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");


            if (DS.Tables[0].Rows.Count > 0)
            {

                return true;
            }


            return false;
        }

        string GetNationalityCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HNM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HNM_MALAFFI_CODE", "HMS_NATIONALITY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HNM_MALAFFI_CODE"]);
            }

            return strCode;
        }

        string GetCountryCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_MALAFFI_CODE", "HMS_COUNTRY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_MALAFFI_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]) != "")
                {
                    strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]).Trim();
                }
            }

            return strCode;
        }

        Boolean PatientMalaffiDataValidate()
        {

            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = "HPM_PT_ID='" + hidActualFileNo.Value + "'";

            DS = objCom.fnGetFieldValue("TOP 1 *", "HMS_PATIENT_MASTER", Criteria, "");



            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (DR.IsNull("HPM_PT_FNAME") == true || Convert.ToString(DR["HPM_PT_FNAME"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient Given Name is Empty.','Red')", true);

                    return false;

                }

                if (DR.IsNull("HPM_PT_LNAME") == true || Convert.ToString(DR["HPM_PT_LNAME"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient Family  Name is Empty.','Red')", true);
                    return false;

                }


                if (DR.IsNull("HPM_SEX") == true || Convert.ToString(DR["HPM_SEX"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient Gender is Empty.','Red')", true);
                    return false;

                }

                if (DR.IsNull("HPM_DOB") == true || Convert.ToString(DR["HPM_DOB"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient DOB is Empty.','Red')", true);
                    return false;

                }


                if (DR.IsNull("HPM_ADDR") == true || Convert.ToString(DR["HPM_ADDR"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient Address is Empty.','Red')", true);
                    return false;

                }

                if (DR.IsNull("HPM_CITY") == true || Convert.ToString(DR["HPM_CITY"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient City is Empty.','Red')", true);
                    return false;

                }



                //  COMP_ID = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  InsuranceCompanyName = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);


                if (DR.IsNull("HPM_COUNTRY") == true || Convert.ToString(DR["HPM_COUNTRY"]) == "")
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Country Of Residence is Empty.','Red')", true);
                    return false;
                }

                string strCountryCode = GetCountryCode(Convert.ToString(DR["HPM_COUNTRY"]));
                if (strCountryCode == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Country Code is Empty.','Red')", true);
                    return false;
                }


                if (DR.IsNull("HPM_NATIONALITY") == true || Convert.ToString(DR["HPM_NATIONALITY"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Nationality is Empty.','Red')", true);
                    return false;

                }
                string NationalityCode = GetNationalityCode(Convert.ToString(DR["HPM_NATIONALITY"]).Trim());
                if (NationalityCode == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Nationality Code is Empty.','Red')", true);
                    return false;

                }

                if (DR.IsNull("HPM_EMIRATES_CODE") == true || Convert.ToString(DR["HPM_EMIRATES_CODE"]) == "")
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient Emirates  is Empty.','Red')", true);
                    return false;
                }




                if (DR.IsNull("HPM_MARITAL") == true || Convert.ToString(DR["HPM_MARITAL"]) == "")
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Marital Status is Empty.','Red')", true);
                    return false;
                }

                if (DR.IsNull("HPM_IQAMA_NO") == true || Convert.ToString(DR["HPM_IQAMA_NO"]) == "")
                {

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Patient EmiratesID / Passport is Empty.','Red')", true);
                    return false;
                }



                if (DR.IsNull("HPM_KNOW_FROM") == true || Convert.ToString(DR["HPM_KNOW_FROM"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Know From is Empty.','Red')", true);
                    return false;
                }




                if (DR.IsNull("HPM_MOBILE") == true || Convert.ToString(DR["HPM_MOBILE"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Mobile No is Empty.','Red')", true);
                    return false;
                }

                if (DR.IsNull("HPM_RELIGION_CODE") == true || Convert.ToString(DR["HPM_RELIGION_CODE"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Religion is Empty.','Red')", true);
                    return false;
                }



                if (DR.IsNull("HPM_KIN_NAME") == false && Convert.ToString(DR["HPM_KIN_NAME"]) != "")
                {

                    if (DR.IsNull("HPM_KIN_RELATION") == true || Convert.ToString(DR["HPM_KIN_RELATION"]) == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Kin Relation is Empty.','Red')", true);
                        return false;
                    }


                    if (DR.IsNull("HPM_KIN_MOBILE") == true || Convert.ToString(DR["HPM_KIN_MOBILE"]) == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Kin Mobile No is Empty.','Red')", true);
                        return false;
                    }


                }

                string PT_TYPE = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]);


                if (PT_TYPE == "CR")
                {
                    if (DR.IsNull("HPM_POLICY_NO") == true || Convert.ToString(DR["HPM_POLICY_NO"]) == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  PLEASE CHECK BELOW ERRORS',' Kin Mobile No is Empty.','Red')", true);
                        return false;
                    }

                    //if (DR.IsNull("HPM_ID_NO") == false || Convert.ToString(DR["HPM_ID_NO"]) != "")
                    //{
                    //    InsCardNumber = Convert.ToString(DR["HPM_ID_NO"]);
                    //}

                }
            }

            return true;
        }

        protected void btnSaveVisit_Click(object sender, EventArgs e)
        {
            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " btnSaveVisit_Click Start" + strNewLineChar);
            GetAppoinmentDtls();
            strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " GetAppoinmentDtls() End" + strNewLineChar);
            btnSaveVisit.Enabled = false;
            try
            {

                if (hidPateName.Value != "HomeCare")// IF PT REGISTRATION COME FROM HOME CARE DOCTOR ID IS NOT MANDATORY
                {
                    if (drpPTCategory.SelectedIndex == 0)
                    {
                        //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click CheckDoctorAvailable() Start"+ strNewLineChar);

                        if (CheckDoctorAvailable() == false)
                        {
                            btnSaveVisit.Enabled = true;
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' DOCTOR'S DETAILS ',' Provide Existing Doctors Details','Brown')", true);
                            goto SaveEnd;
                        }
                    }
                }


                if (CheckVisitExist() == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS NOT SAVED.',' Patient Visit Details already exist','Red')", true);
                    btnSaveVisit.Enabled = true;
                    goto SaveEnd;

                }

                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " CheckVisitExist() End" + strNewLineChar);


                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {

                    if (CheckDoctorDetailsForMalaffi() == false)
                    {
                        btnSaveVisit.Enabled = true;
                        goto SaveEnd;
                    }

                    if (PatientMalaffiDataValidate() == false)
                    {
                        btnSave.Enabled = true;
                        btnSaveVisit.Enabled = true;
                        goto SaveEnd;
                    }

                }
                // TextFileWriting(System.DateTime.Now.ToString() + " PTRegistration 2");


                if (Convert.ToString(ViewState["REQ_PT_ZERO_BALANCE_DUE"]) == "1")
                {
                    Decimal CreditBalAmt = (fnCalculatePTAmount("INVOICE") - fnCalculatePTAmount("RETURN")) - (fnCalculatePTAmount("PAIDAMT") + fnCalculatePTAmount("RETURNPAIDAMT") + fnCalculatePTAmount("REJECTED")) - fnCalculatePTAmount("NPPAID");
                    if (CreditBalAmt > 0)
                    {
                        strMessage = "Balance Amount Due : " + CreditBalAmt.ToString("#0.00");  //HMS_CURRENCY_FORMAT)
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' PATIENT BALABCE ',' " + strMessage + ".','Brown')", true);

                        goto SaveEnd;
                    }
                }

                ClientScriptManager CSM = Page.ClientScript;
                // TextFileWriting(System.DateTime.Now.ToString() + " PTRegistration 3");

                if (Page.IsValid)
                {
                    TextFileWriting(System.DateTime.Now.ToString() + " Patient Visit Save Started ");
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Visit Save Started " + strNewLineChar);

                    //*********************  PATIENT MASTER INSERT START  **************************
                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter adpt = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 864000;
                    cmd.CommandText = "HMS_SP_PTMasterVisitAdd";

                    cmd.Parameters.Add(new SqlParameter("@HPM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_ID", SqlDbType.VarChar)).Value = hidActualFileNo.Value;//  txtFileNo.Text.ToString().ToUpper();

                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_FNAME", SqlDbType.NVarChar)).Value = txtFName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_MNAME", SqlDbType.NVarChar)).Value = txtMName.Text.ToString().ToUpper().Replace("'", "");
                    cmd.Parameters.Add(new SqlParameter("@HPM_PT_LNAME", SqlDbType.NVarChar)).Value = txtLName.Text.ToString().ToUpper().Replace("'", "");


                    cmd.Parameters.Add(new SqlParameter("@HPV_PT_TYPE", SqlDbType.VarChar)).Value = drpPTVisitType.SelectedValue.ToString();




                    if (drpPTVisitType.SelectedValue == "CR" || drpPTVisitType.SelectedValue == "CU")
                    {


                        //  if (drpNetworkName.SelectedIndex == 0 ||  drpPTVisitType.SelectedValue == "CU")
                        // {
                        //cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedValue;
                        //cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpDefaultCompany.SelectedItem.Text;

                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpPTVisitCompany.SelectedValue;
                        cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpPTVisitCompany.SelectedItem.Text;

                        //}
                        //else
                        //{
                        //    cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_ID", SqlDbType.VarChar)).Value = drpNetworkName.SelectedValue;
                        //    cmd.Parameters.Add(new SqlParameter("@HPM_INS_COMP_NAME", SqlDbType.VarChar)).Value = drpNetworkName.SelectedItem.Text;

                        //}


                        //cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.ToString();
                        //cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_TYPE", SqlDbType.VarChar)).Value = drpPlanType.SelectedValue;

                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_NO", SqlDbType.VarChar)).Value = txtPTVisitPolicyNo.Text.ToString();
                        cmd.Parameters.Add(new SqlParameter("@HPM_POLICY_TYPE", SqlDbType.VarChar)).Value = drpPTVisitCompanyPlan.SelectedValue;

                        if (txtPTVisitCardExpDate.Text != "") //txtExpiryDate
                        {

                            // cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = txtExpiryDate.Text;// TSDate.ToString("MM/dd/yyyy");
                            cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = txtPTVisitCardExpDate.Text;


                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPM_CONT_EXPDATE", SqlDbType.VarChar)).Value = "";
                        }

                        cmd.Parameters.Add(new SqlParameter("@HPM_ID_NO", SqlDbType.VarChar)).Value = txtPTVisitCardNo.Text.Trim();
                    }



                    if (drpNationality.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_NATIONALITY", SqlDbType.VarChar)).Value = drpNationality.SelectedValue.ToString();
                    }


                    cmd.Parameters.Add(new SqlParameter("@HPM_DR_ID", SqlDbType.VarChar)).Value = txtDoctorID.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_DR_NAME", SqlDbType.VarChar)).Value = txtDoctorName.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPM_DEP_NAME", SqlDbType.VarChar)).Value = txtDepName.Text.Trim();



                    cmd.Parameters.Add(new SqlParameter("@HPM_TOKEN_NO", SqlDbType.VarChar)).Value = txtTokenNo.Text;

                    ////if (txtRefDoctorID.Text.Trim() != "")
                    ////{
                    ////    cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_ID", SqlDbType.VarChar)).Value = txtRefDoctorID.Text.Trim();
                    ////    cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_NAME", SqlDbType.VarChar)).Value = txtRefDoctorName.Text.Trim();
                    ////}




                    // cmd.Parameters.Add(new SqlParameter("@HPM_PT_STATUS", SqlDbType.Char)).Value = drpPatientStatus.SelectedValue;
                    //IN THE SYSTEM OPTIONS IF FILEING REQUEST IS SELECTED VISIT STATUS IS 'F' ELSE 'W'




                    cmd.Parameters.Add(new SqlParameter("@HPM_MODIFIED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);




                    if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "0")
                    {
                        //cmd.Parameters.Add(new SqlParameter("@HPM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                        cmd.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char)).Value = "A";

                        //if (Convert.ToString(ViewState["PatientInfoId"]) != "" && Convert.ToString(ViewState["PatientInfoId"]) != null)
                        //{
                        //    cmd.Parameters.Add(new SqlParameter("@PatientInfoId", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["PatientInfoId"]);
                        //}

                    }
                    else if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                    {

                        cmd.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char)).Value = "M";
                    }
                    cmd.Parameters.Add(new SqlParameter("@IsManual", SqlDbType.Bit)).Value = chkManual.Checked;


                    if (Convert.ToString(ViewState["HPVSeqNo"]) == null || Convert.ToString(ViewState["HPVSeqNo"]) == "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPVSeqNo", SqlDbType.VarChar)).Value = "0";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPVSeqNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["HPVSeqNo"]);
                    }


                    if (drpPriority.SelectedIndex != 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_PRIORITY_SLNO", SqlDbType.TinyInt)).Value = drpPriority.SelectedValue;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_PRIORITY_SLNO", SqlDbType.TinyInt)).Value = "";
                    }
                    cmd.Parameters.Add(new SqlParameter("@HPV_ENTRY_FROM", SqlDbType.VarChar)).Value = "";


                    if (txtAge.Text.Trim() != "" && txtMonth.Text.Trim() != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_AGE", SqlDbType.VarChar)).Value = txtAge.Text.Trim() + " " + drpAgeType.SelectedValue + " " + Convert.ToString(txtMonth.Text.Trim()) + " " + drpAgeType1.SelectedValue;
                    }
                    else if (txtAge.Text.Trim() != "" && txtMonth.Text.Trim() == "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_AGE", SqlDbType.VarChar)).Value = txtAge.Text.Trim() + " " + drpAgeType.SelectedValue;
                    }
                    else if (txtAge.Text.Trim() == "" && txtMonth.Text.Trim() != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_AGE", SqlDbType.VarChar)).Value = Convert.ToString(txtMonth.Text.Trim()) + " " + drpAgeType1.SelectedValue; // + " M";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_AGE", SqlDbType.VarChar)).Value = txtAge.Text.Trim() + " " + drpAgeType.SelectedValue;//+ " Y ";
                    }



                    if (txtRefDoctorID.Text.Trim() != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL", SqlDbType.Char)).Value = "Y";
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL", SqlDbType.Char)).Value = "N";
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL_TO", SqlDbType.VarChar)).Value = txtRefDoctorID.Text.Trim();// drpRefDoctor.SelectedValue.ToString();

                    cmd.Parameters.Add(new SqlParameter("@HPV_APPOINT_TIME", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["AppointStartTime"]);

                    cmd.Parameters.Add(new SqlParameter("@HPV_APPOINT_CREATED_DATE", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["AppointCreatedDateTime"]);
                    cmd.Parameters.Add(new SqlParameter("@HPV_APPOINT_STARTTIME", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["AppointStartDateTime"]);
                    cmd.Parameters.Add(new SqlParameter("@HPV_REFERRAL_APPOINT", SqlDbType.Bit)).Value = Convert.ToBoolean(ViewState["ReferralAppoint"]);



                    cmd.Parameters.Add(new SqlParameter("@HPV_TYPE", SqlDbType.Char)).Value = "OP";

                    cmd.Parameters.Add(new SqlParameter("@HPV_MOBILE", SqlDbType.VarChar)).Value = txtMobile1.Text.ToString();

                    cmd.Parameters.Add(new SqlParameter("@HPV_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_VISITTYPE", SqlDbType.VarChar)).Value = " ";
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_NURSE_STATUS", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_ID", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@HPV_COUPON_NO", SqlDbType.VarChar)).Value = txtCouponNo.Text.Trim();


                    string strDepartment = "|" + txtDepName.Text + "|";
                    string strTraigeHideDepts = Convert.ToString(Session["TRIAGE_HIDE_DEPTS"]);
                    bool bolHideTriage = false;

                    if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1")
                    {
                        if (strTraigeHideDepts.Contains(strDepartment) == true)
                        {
                            bolHideTriage = true;
                        }
                    }

                    //IN THE SYSTEM OPTIONS IF FILEING REQUEST IS SELECTED VISIT STATUS IS 'F' ELSE 'W'
                    if (Convert.ToString(Session["FilingRequest"]).ToUpper() == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_STATUS", SqlDbType.VarChar)).Value = "F";

                        //if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1" )
                        //{
                        //    //THIS 'T' STATUS FOR TRIAGE, THIS WILL NOT GO TO DOCTOR EMR WATING LIST.
                        //    cmd.Parameters.Add(new SqlParameter("@HPV_EMR_STATUS", SqlDbType.VarChar)).Value = "T";

                        //}
                    }
                    else
                    {
                        if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1" && chkRequiredTriage.Checked == true && bolHideTriage == false)
                        {
                            //THIS 'T' STATUS FOR TRIAGE, THIS WILL NOT GO TO DOCTOR EMR WATING LIST.
                            cmd.Parameters.Add(new SqlParameter("@HPV_EMR_STATUS", SqlDbType.VarChar)).Value = "T";
                            cmd.Parameters.Add(new SqlParameter("@HPV_STATUS", SqlDbType.VarChar)).Value = "T";
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@HPV_EMR_STATUS", SqlDbType.VarChar)).Value = "W";
                            cmd.Parameters.Add(new SqlParameter("@HPV_STATUS", SqlDbType.VarChar)).Value = "W";

                        }
                    }
                    cmd.Parameters.Add(new SqlParameter("@HPV_TRIAGE_REASON", SqlDbType.VarChar)).Value = drpTriageReason.Text;
                    cmd.Parameters.Add(new SqlParameter("@HPV_VISIT_TYPE", SqlDbType.VarChar)).Value = lblVisitType.Text;


                    Boolean UpdateWaitingList = true;  //false
                    string strUpdateWaitingList = hidWaitingList.Value;


                    ////if (chkUpdateWaitList.Checked == true)
                    ////{
                    ////    UpdateWaitingList = true;

                    ////    if (hidPateName.Value != "HomeCare")
                    ////    {
                    ////        if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                    ////        {
                    ////            if (strUpdateWaitingList == "true")
                    ////            {
                    ////                UpdateWaitingList = true;
                    ////            }
                    ////            else
                    ////            {
                    ////                UpdateWaitingList = false;
                    ////            }
                    ////        }
                    ////        else
                    ////        {
                    ////            UpdateWaitingList = true;
                    ////        }

                    ////    }

                    ////}
                    ////else
                    ////{
                    ////    UpdateWaitingList = false;



                    ////}



                    // cmd.Parameters.Add(new SqlParameter("@IsAddVisit", SqlDbType.Bit)).Value = UpdateWaitingList;
                    cmd.Parameters.Add(new SqlParameter("@IsUpdateVisit", SqlDbType.Bit)).Value = UpdateWaitingList;

                    cmd.Parameters.Add(new SqlParameter("@IsManualToken", SqlDbType.Bit)).Value = chkManualToken.Checked;



                    cmd.Parameters.Add(new SqlParameter("@HPV_ELIGIBILITY_ID", SqlDbType.VarChar)).Value = txtEligibilityID.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HPV_NURSE_ID", SqlDbType.VarChar)).Value = drpNurse.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HPV_SESSION_INVOICE_ID", SqlDbType.VarChar)).Value = txtSessionInvoiceNo.Text.Trim();


                    cmd.Parameters.Add(new SqlParameter("@HPV_PACKAGE_SERV_ID", SqlDbType.VarChar)).Value = drpPTCategory.SelectedValue;




                    if (chkDisplayInDrWaitingList.Checked == false || txtDepName.Text.Trim().ToUpper() == "RADIOLOGY" || txtDepName.Text.Trim().ToUpper() == "LABORATORY")
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_EMR_DR_WAITING", SqlDbType.Bit)).Value = false;
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HPV_EMR_DR_WAITING", SqlDbType.Bit)).Value = true;
                    }

                    cmd.Parameters.Add(new SqlParameter("@HPV_PATIENT_CLASS", SqlDbType.VarChar)).Value = drpPatientClass.SelectedValue;

                    cmd.Parameters.Add(new SqlParameter("@HPV_REF_EMR_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ReferralEMR_ID"]);

                    int k = cmd.Parameters.Count;
                    for (int i = 0; i < k; i++)
                    {
                        if (cmd.Parameters[i].Value == "")
                        {
                            cmd.Parameters[i].Value = DBNull.Value;
                        }
                    }

                    SqlParameter PTVisitID = new SqlParameter("@PTVisitID", SqlDbType.VarChar, 15);
                    PTVisitID.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(PTVisitID);

                    cmd.ExecuteNonQuery();


                    string strPTVisitID = "";
                    strPTVisitID = Convert.ToString(PTVisitID.Value);

                    if (strPTVisitID != "")
                    {
                        hidPTVisitSQNo.Value = strPTVisitID;
                    }

                    con.Close();

                    TextFileWriting(System.DateTime.Now.ToString() + "Patient Visit Insert End ");
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient Visit Insert End " + strNewLineChar);
                    //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click Patent Master Insert End  () "+ strNewLineChar);


                    if (strPTVisitID == "")
                    {

                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS NOT SAVED.',' Patient Visit Details already exist','Red')", true);
                        goto SaveEnd;
                    }

                    //*********************  PATIENT MASTER INSERT END  **************************

                    if (drpPTVisitType.SelectedValue == "CR" || drpPTVisitType.SelectedValue == "CU")
                    {
                        string strPhotoID = Convert.ToString(ViewState["Photo_ID"]);
                        string strPath = Convert.ToString(ViewState["InsCardPath"]);
                        Byte[] bytImage = null, bytImage1 = null;


                        if (Convert.ToString(Session["InsCardFrontDefaultPath"]) != "")
                        {
                            System.Drawing.Image image = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardFrontDefaultPath"]));
                            bytImage = ImageToByte(image);
                        }


                        if (Convert.ToString(Session["InsCardBackDefaultPath"]) != "")
                        {
                            System.Drawing.Image image1 = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["InsCardBackDefaultPath"]));
                            bytImage1 = ImageToByte(image1);
                        }

                        dbo.BranchID = Convert.ToString(Session["Branch_ID"]);
                        dbo.PT_ID = txtFileNo.Text;
                        dbo.VISIT_NO = hidPTVisitSQNo.Value;
                        dbo.INS_CARD_FRONT = bytImage;
                        dbo.INS_CARD_BACK = bytImage1;
                        dbo.UserID = Convert.ToString(Session["User_ID"]);
                        dbo.PatientInsuranceHistory();
                        strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient PatientInsuranceHistory() End " + strNewLineChar);

                        if (strPhotoID != "" && strPhotoID != null && hidPTVisitSQNo.Value != "" && hidPTVisitSQNo.Value != null)
                        {
                            string FieldWithValue = " HPV_PHOTO_ID='" + strPhotoID + "'";
                            string UpdateCondition = " HPV_PT_ID='" + hidActualFileNo.Value + "' AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_SEQNO=" + hidPTVisitSQNo.Value;


                            dbo.fnUpdatetoDb("HMS_PATIENT_VISIT", FieldWithValue, UpdateCondition);
                            //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click Photo ID save in Visit Table "+ strNewLineChar);

                        }



                    }

                    //SAVE APPOINTMENT STATUS AS WAITING
                    UpdateAppointmentStatus();
                    strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + " Patient UpdateAppointmentStatus() End " + strNewLineChar);

                    //    ModalPopupExtender1.Show();
                    //    strMessage = "Details Saved !";

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS SAVED.',' Patient Visit Details Saved.','Green')", true);


                    //if (Convert.ToString(ViewState["Patient_ID_VALID"]) == "1")
                    //{
                    //    AuditLogAdd("MODIFY", "Modify Existing entry in the Patient Registration, Patient ID is " + hidActualFileNo.Value);
                    //}
                    //else
                    //{
                    //    AuditLogAdd("ADD", "Add new entry in the Patient Registration, Patient ID is " + hidActualFileNo.Value);
                    //}


                    if (hidLabelPrint.Value == "true")
                    {
                        btnLabelPrint_Click(btnLabelPrint, new EventArgs());


                    }


                    if (hidTokenPrint.Value == "true")
                    {
                        btnTokenPrint_Click(btnTokenPrint, new EventArgs());
                        if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                        {
                            btnIDPrint_Click(btnIDPrint, new EventArgs());
                        }

                    }

                    if (Convert.ToString(ViewState["ReferralEMR_ID"]) != "")
                    {
                        CommonBAL objCom = new CommonBAL();
                        string strReferralCriteria = "EPR_ID=" + Convert.ToString(ViewState["ReferralEMR_ID"]);
                        objCom.fnUpdateTableData("EPR_STATUS='Completed'", "EMR_PT_REFERNCE", strReferralCriteria);

                        ViewState["ReferralEMR_ID"] = "";
                    }



                    if (Convert.ToString(Session["ClearAfterSave"]).ToUpper() == "Y")
                    {
                        txtFileNo.Text = "";
                        chkFamilyMember.Checked = false;
                        Clear();
                    }
                    else
                    {
                        ViewState["Patient_ID_VALID"] = "1";
                    }

                }

                //strProcessTimLogData.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + " btnSave_Click End "+ strNewLineChar);


                if (Convert.ToString(ViewState["EnquiryID"]) != "")
                {
                    CommonBAL objCom = new CommonBAL();
                    string strEnqCriteria = "HCEM_ID='" + Convert.ToString(ViewState["EnquiryID"]) + "'";
                    objCom.fnUpdateTableData("HCEM_PT_ID='" + hidActualFileNo.Value + "',HCEM_STATUS='Closed'", "HMS_CUSTOMER_ENQUIRY_MASTER", strEnqCriteria);

                    ViewState["EnquiryID"] = "";
                }




                strPageTimeLog.Append(System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt") + "  PT_ID =" + hidActualFileNo.Value + "btnSaveVisit_Click End " + strNewLineChar);



            SaveEnd: ;

                if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1" || Convert.ToString(Session["ENABLE_USER_LOG"]) == "1")
                {
                    ProcessTimeLog(strPageTimeLog.ToString());
                }

            }
            catch (Exception ex)
            {
                if (Convert.ToString(Session["HMS_ENABLE_LOGFILE"]) == "1" || Convert.ToString(Session["ENABLE_USER_LOG"]) == "1")
                {
                    ProcessTimeLog(strPageTimeLog.ToString());
                }

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnSaveVisit_Click");
                TextFileWriting(ex.Message.ToString());

                lblStatusVisit.Text = "Data Not Saved";
                lblStatusVisit.ForeColor = System.Drawing.Color.Red;
            }

            if (hidPateName.Value == "HomeCare")
            {
                Response.Redirect("../HomeCare/HomeCareRegistration.aspx?PatientId=" + hidActualFileNo.Value);
            }
        }

        protected void drpPTVisitType_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                BindPTVisitParentGet();
                BindPTVisitCompany();


                if (drpPTVisitCompany.Items.Count > 0)
                {
                    drpPTVisitCompany.SelectedIndex = 0;
                }

                txtPTVisitPolicyNo.Text = "";
                txtPTVisitCardNo.Text = "";
                txtPTVisitCardExpDate.Text = "31/12/" + DateTime.Now.Year.ToString();

                drpPTVisitCompanyPlan.Items.Clear();

                drpPTVisitParentCompany.Enabled = false;
                drpPTVisitCompany.Enabled = false;
                drpPTVisitCompanyPlan.Enabled = false;
                txtPTVisitPolicyNo.Enabled = false;
                txtPTVisitCardNo.Enabled = false;


                txtPTVisitCardExpDate.Enabled = false;
                txtPTVisitPolicyNo.Text = "";

                if (drpPTVisitType.SelectedValue == "CR" || drpPTVisitType.SelectedValue == "CU")
                {

                    if (drpPTVisitType.SelectedValue == "CU")
                    {
                        txtPTVisitPolicyNo.Text = GlobalValues.HospitalLicenseNo + "#" + txtFileNo.Text.Trim();
                    }

                    drpPTVisitParentCompany.Enabled = true;
                    drpPTVisitCompany.Enabled = true;
                    drpPTVisitCompanyPlan.Enabled = true;
                    txtPTVisitPolicyNo.Enabled = true;
                    txtPTVisitCardNo.Enabled = true;
                    txtPTVisitCardExpDate.Enabled = true;

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpPTVisitType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void drpPTVisitParentCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                BindPTVisitCompany();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.drpPTVisitParentCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

                lblStatusVisit.Text = "Data Not Saved";
                lblStatusVisit.ForeColor = System.Drawing.Color.Red;
            }
        }


        protected void ReferralSelect_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvEnquiryMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;




                Label lblgvReferralEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvReferralEMR_ID");
                Label lblgvReferralDR_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvReferralDR_ID");

                Label lblgvReferralEMR_DR_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvReferralEMR_DR_ID");
                Label lblgvReferralEMR_DR_Name = (Label)gvScanCard.Cells[0].FindControl("lblgvReferralEMR_DR_Name");


                txtDoctorID.Text = lblgvReferralDR_ID.Text.Trim();
                txtDoctorID_TextChanged(txtDoctorID, new EventArgs());


                txtRefDoctorID.Text = lblgvReferralEMR_DR_ID.Text.Trim();
                txtRefDoctorName.Text = lblgvReferralEMR_DR_Name.Text.Trim();

                ViewState["ReferralEMR_ID"] = lblgvReferralEMR_ID.Text;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideReferralList()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "ReferralSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        void BindEmirateIdDtlsNew()
        {
            if (txtEmirateIDData.Text != "")
            {
                string strEmirateIDData = txtEmirateIDData.Text.Trim();
                string[] arrEmirateIDData = strEmirateIDData.Split('|');


                //chkManual.Checked = false;
                //chkManual_CheckedChanged("chkManual", new EventArgs());
                string strFullName = arrEmirateIDData[1].ToUpper();
                string[] ArrFullName = strFullName.Split(' ');

                txtLName.Text = "";
                if (ArrFullName.Length > 0)
                {

                    for (int j = 0; j <= ArrFullName.Length - 1; j++)
                    {
                        if (j == 0)
                        {
                            txtFName.Text = ArrFullName[j];
                        }
                        else if (j == 1)
                        {
                            txtMName.Text = ArrFullName[j];
                        }
                        else if (j >= 2)
                        {
                            txtLName.Text += ArrFullName[j].Replace(",", "") + " ";
                        }
                    }

                }
                string EmiratesID = "";
                EmiratesID = arrEmirateIDData[0].Trim();

                string FormatedEmiratesID = "";
                if (EmiratesID.Length > 0)
                {

                    for (int j = 0; j <= EmiratesID.Length; j++)
                    {

                        if (j == 2)
                        {
                            FormatedEmiratesID = EmiratesID.Substring(0, 3).Trim();
                        }
                        if (j == 6)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(3, 4).Trim();
                        }

                        if (j == 14)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(7, 7).Trim();
                        }

                        if (j == 15)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(14, 1).Trim();
                        }
                    }
                }

                txtEmiratesID.Text = FormatedEmiratesID.Trim();



                if (EmiratesID.Length == 3)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }
                if (EmiratesID.Length == 8)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }
                if (EmiratesID.Length == 16)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }


                // txtCardNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CardNo"]).Trim();

                string strDOB = arrEmirateIDData[2].Trim();
                string[] arrDOB = strDOB.Split(' ');

                if (arrDOB.Length > 0)
                {
                    txtDOB.Text = arrDOB[0];
                }

                if (arrEmirateIDData[3].Trim() != "")
                {
                    for (int intSex = 0; intSex < drpSex.Items.Count; intSex++)
                    {
                        if (drpSex.Items[intSex].Value == arrEmirateIDData[3].Trim())
                        {
                            drpSex.SelectedValue = arrEmirateIDData[3].Trim();
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;

                if (arrEmirateIDData[4].Trim() != "")
                {
                    for (int intMstatus = 0; intMstatus < drpMStatus.Items.Count; intMstatus++)
                    {
                        if (drpMStatus.Items[intMstatus].Value == arrEmirateIDData[4].Trim())
                        {
                            drpMStatus.SelectedValue = arrEmirateIDData[4].Trim();
                            goto ForMStatus;
                        }

                    }
                }
            ForMStatus: ;

                txtPoBox.Text = arrEmirateIDData[5].Trim();
                txtAddress.Text = arrEmirateIDData[6].Trim();

                if (arrEmirateIDData[7].Trim() != "")
                {
                    for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                    {
                        if (drpCity.Items[intCityCount].Value.ToUpper() == arrEmirateIDData[7].Trim().ToUpper())
                        {
                            drpCity.SelectedIndex = intCityCount;
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;

                if (arrEmirateIDData[7].Trim() != "")
                {
                    for (int intCityCount = 0; intCityCount < drpEmirates.Items.Count; intCityCount++)
                    {
                        if (drpEmirates.Items[intCityCount].Text.ToUpper() == arrEmirateIDData[7].Trim().ToUpper())
                        {
                            drpEmirates.SelectedIndex = intCityCount;
                            goto ForEmirates;
                        }

                    }
                }
            ForEmirates: ;

                //    if (arrEmirateIDData[8].Trim() != "")
                //    {
                //        for (int intAreaCount = 0; intAreaCount < drpArea.Items.Count; intAreaCount++)
                //        {
                //            if (drpArea.Items[intAreaCount].Value == arrEmirateIDData[8].Trim()) ;
                //            {
                //                drpArea.SelectedValue = arrEmirateIDData[8].Trim();
                //                goto ForArea;
                //            }

                //        }
                //    }
                //ForArea: ;

                if (arrEmirateIDData[9].Trim() != "")
                {
                    string NationalityCode = "", NationalityName = "";

                    NationalityCode = arrEmirateIDData[9].Trim();

                    GetNationalityCode(NationalityCode, out  NationalityName);


                    for (int intMstatus = 0; intMstatus < drpNationality.Items.Count; intMstatus++)
                    {
                        if (drpNationality.Items[intMstatus].Value == NationalityName)
                        {
                            drpNationality.SelectedValue = NationalityName;
                            goto ForNationality;
                        }

                    }
                }
            ForNationality: ;




                if (arrEmirateIDData.Length > 10)
                {
                    txtPhone1.Text = arrEmirateIDData[10].Trim();
                }
                if (arrEmirateIDData.Length > 11)
                {
                    if (lblVisitType.Text.Trim() == "New")
                    {

                        txtMobile1.Text = arrEmirateIDData[11].Trim();
                    }
                }

                if (arrEmirateIDData.Length > 12)
                {
                    txtEmail.Text = arrEmirateIDData[12].Trim();
                }

                if (arrEmirateIDData.Length > 13)
                {
                    txtPTCompany.Text = arrEmirateIDData[13].Trim();
                }



                if (txtDOB.Text.Trim() != "")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                    AgeCalculation();
                }

                // BindPhoto();


            }
        }

        void BindPatientPhoto()
        {
            string strFileName = txtEmiratesID.Text.Replace("-", "") + ".jpg";
            imgPatientPhoto.ImageUrl = "../DisplayPatientPhoto.aspx?PT_ID=" + hidActualFileNo.Value + "&FileName=" + strFileName;
        }

        protected void txtEmirateIDData_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtEmirateIDData.Text != "")
                {
                    Clear();

                    BindEmirateIdDtlsNew();
                    // imgLoaderEidScan.Visible = false;

                    if (txtEmiratesID.Text.Trim() != "")
                    {
                        DataSet ds1 = new DataSet();
                        string Criteria = "    HPM_IQAMA_NO  = '" + txtEmiratesID.Text.Trim() + "'  ";
                        ds1 = dbo.retun_patient_details(Criteria);
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            //txtFileNo.Text = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();
                            //BindModifyData();

                            //BindPatientPhoto();

                            if (Convert.ToString(ViewState["EIDConfirmResult"]) != "Yes")
                            {
                                //lblConfirmMessage.Text = "Would you like to replace the patient information from Emirates ID Card?";// "Do you want to update Informationg as per Emirates ID?";

                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowConfirmMessage('Would you like to replace the patient information from Emirates ID Card?')", true);

                                // divConfirmMessageClose.Style.Add("display", "block");
                                //  divConfirmMessage.Style.Add("display", "block");
                                ViewState["ConfirmMsgFor"] = "NewEmiratesIDLoad";
                                ViewState["EmiratesIDPT_ID"] = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();
                                ViewState["EIDConfirmResult"] = "";




                            }


                        }
                    }
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.txtEmirateIDData_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void SelectPrint_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
            Label lblPatientId, lblSeqNo, lblDrId, lblDateDesc;
            lblPatientId = (Label)gvGCGridView.Cells[0].FindControl("lblPatientId");
            lblSeqNo = (Label)gvGCGridView.Cells[0].FindControl("lblSeqNo");
            lblDrId = (Label)gvGCGridView.Cells[0].FindControl("lblDrId");
            lblDateDesc = (Label)gvGCGridView.Cells[0].FindControl("lblDateDesc");
            Label lblCompId = (Label)gvGCGridView.Cells[0].FindControl("lblCompId");
            Label lblDepName = (Label)gvGCGridView.Cells[0].FindControl("lblDepName");

            if (drpVisitConsentForm.SelectedIndex != 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowVisitConsentFormReportPDF('" + lblPatientId.Text + "','" + lblSeqNo.Text + "','" + lblDrId.Text + "','" + lblDateDesc.Text + "');", true);
            }
            if (chkClaimFormPrint.Checked == true)
            {
                string strRptName = "";
                strRptName = fnGetClaimFormName(lblCompId.Text, lblDepName.Text);

                if (File.Exists(strReportPath + strRptName) == false)
                {
                    lblStatus.Text = "File Not Found";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowVisitClaimPrintPDF('" + strRptName + "','" + hidActualFileNo.Value + "'," + lblSeqNo.Text + ");", true);

            }


        FunEnd: ;

        }

        protected void SelectStaff_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblStaffID = (Label)gvScanCard.Cells[0].FindControl("lblStaffID");
                Label lblStaffName = (Label)gvScanCard.Cells[0].FindControl("lblStaffName");
                Label lblDepartment = (Label)gvScanCard.Cells[0].FindControl("lblDepartment");


                txtDoctorID.Text = lblStaffID.Text;
                txtDoctorName.Text = lblStaffName.Text;
                txtDepName.Text = lblDepartment.Text;


                BindToken();


                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideStaffList()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "SelectStaff_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        void BindEmirateIdData()
        {
            if (hidEmirateIDData.Value != "")
            {
                string EmirateIDData = hidEmirateIDData.Value;

                string[] arrEmirateIDData = EmirateIDData.Split('|');


                //chkManual.Checked = false;
                //chkManual_CheckedChanged("chkManual", new EventArgs());
                string strFullName = arrEmirateIDData[1].ToUpper();
                string[] ArrFullName = strFullName.Split(',');

                txtLName.Text = "";
                if (ArrFullName.Length > 0)
                {

                    for (int j = 0; j <= ArrFullName.Length - 1; j++)
                    {
                        if (j == 0)
                        {
                            txtFName.Text = ArrFullName[j];
                        }
                        else if (j == 1)
                        {
                            txtMName.Text = ArrFullName[j];
                        }
                        else if (j >= 2)
                        {
                            txtLName.Text += ArrFullName[j].Replace(",", "") + " ";
                        }
                    }

                }
                string EmiratesID = "";
                EmiratesID = arrEmirateIDData[0].Trim();

                string FormatedEmiratesID = "";
                if (EmiratesID.Length > 0)
                {

                    for (int j = 0; j <= EmiratesID.Length; j++)
                    {

                        if (j == 2)
                        {
                            FormatedEmiratesID = EmiratesID.Substring(0, 3).Trim();
                        }
                        if (j == 6)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(3, 4).Trim();
                        }

                        if (j == 14)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(7, 7).Trim();
                        }

                        if (j == 15)
                        {
                            FormatedEmiratesID = FormatedEmiratesID + "-" + EmiratesID.Substring(14, 1).Trim();
                        }
                    }
                }

                txtEmiratesID.Text = FormatedEmiratesID.Trim();



                if (EmiratesID.Length == 3)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }
                if (EmiratesID.Length == 8)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }
                if (EmiratesID.Length == 16)
                {

                    txtEmiratesID.Text = EmiratesID + '-';
                }


                // txtCardNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HTE_CardNo"]).Trim();

                string strDOB = arrEmirateIDData[2].Trim();
                string[] arrDOB = strDOB.Split(' ');

                if (arrDOB.Length > 0)
                {
                    txtDOB.Text = arrDOB[0];
                }

                if (arrEmirateIDData[3].Trim() != "")
                {
                    for (int intSex = 0; intSex < drpSex.Items.Count; intSex++)
                    {
                        if (drpSex.Items[intSex].Value == arrEmirateIDData[3].Trim())
                        {
                            drpSex.SelectedValue = arrEmirateIDData[3].Trim();
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;

                if (arrEmirateIDData[4].Trim() != "")
                {
                    for (int intMstatus = 0; intMstatus < drpMStatus.Items.Count; intMstatus++)
                    {
                        if (drpMStatus.Items[intMstatus].Value == arrEmirateIDData[4].Trim())
                        {
                            drpMStatus.SelectedValue = arrEmirateIDData[4].Trim();
                            goto ForMStatus;
                        }

                    }
                }
            ForMStatus: ;

                txtPoBox.Text = arrEmirateIDData[5].Trim();
                txtAddress.Text = arrEmirateIDData[6].Trim();

                if (arrEmirateIDData[7].Trim() != "")
                {
                    for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                    {
                        if (drpCity.Items[intCityCount].Value.ToUpper() == arrEmirateIDData[7].Trim().ToUpper())
                        {
                            drpCity.SelectedIndex = intCityCount;
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;

                if (arrEmirateIDData[7].Trim() != "")
                {
                    for (int intCityCount = 0; intCityCount < drpEmirates.Items.Count; intCityCount++)
                    {
                        if (drpEmirates.Items[intCityCount].Text.ToUpper() == arrEmirateIDData[7].Trim().ToUpper())
                        {
                            drpEmirates.SelectedIndex = intCityCount;
                            goto ForEmirates;
                        }

                    }
                }
            ForEmirates: ;


                if (arrEmirateIDData[9].Trim() != "")
                {
                    string NationalityCode = "", NationalityName = "";

                    NationalityCode = arrEmirateIDData[9].Trim();

                    GetNationalityCode(NationalityCode, out  NationalityName);


                    for (int intMstatus = 0; intMstatus < drpNationality.Items.Count; intMstatus++)
                    {
                        if (drpNationality.Items[intMstatus].Value == NationalityName)
                        {
                            drpNationality.SelectedValue = NationalityName;
                            goto ForNationality;
                        }

                    }
                }
            ForNationality: ;




                if (arrEmirateIDData.Length > 10)
                {
                    txtPhone1.Text = arrEmirateIDData[10].Trim();
                }
                if (arrEmirateIDData.Length > 11)
                {
                    if (lblVisitType.Text.Trim() == "New")
                    {

                        txtMobile1.Text = arrEmirateIDData[11].Trim();
                    }
                }

                if (arrEmirateIDData.Length > 12)
                {
                    txtEmail.Text = arrEmirateIDData[12].Trim();
                }

                if (arrEmirateIDData.Length > 13)
                {
                    txtPTCompany.Text = arrEmirateIDData[13].Trim();
                }



                if (txtDOB.Text.Trim() != "")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "AgeCalculation();", true);
                    AgeCalculation();
                }

                // BindPhoto();


            }
        }


        protected void btnReadEID_Click(object sender, EventArgs e)
        {


            try
            {
                if (hidEmirateIDData.Value != "")
                {
                    Clear();

                    BindEmirateIdData();
                    // imgLoaderEidScan.Visible = false;
                    Session["PatientPhoto"] = hidPatientEIDPhoto.Value;

                    //  BindPatientPhoto();
                    if (hidEmirateIDData.Value != "")
                    {
                        DataSet ds1 = new DataSet();
                        string Criteria = "    HPM_IQAMA_NO  = '" + txtEmiratesID.Text.Trim() + "'  ";
                        ds1 = dbo.retun_patient_details(Criteria);
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            //txtFileNo.Text = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();
                            //BindModifyData();

                            //BindPatientPhoto();

                            if (Convert.ToString(ViewState["EIDConfirmResult"]) != "Yes")
                            {
                                //lblConfirmMessage.Text = "Would you like to replace the patient information from Emirates ID Card?";// "Do you want to update Informationg as per Emirates ID?";

                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowConfirmMessage('Would you like to replace the patient information from Emirates ID Card?')", true);

                                // divConfirmMessageClose.Style.Add("display", "block");
                                //  divConfirmMessage.Style.Add("display", "block");
                                ViewState["ConfirmMsgFor"] = "NewEmiratesIDLoad";
                                ViewState["EmiratesIDPT_ID"] = Convert.ToString(ds1.Tables[0].Rows[0]["HPM_PT_ID"]).Trim();
                                ViewState["EIDConfirmResult"] = "";




                            }


                        }
                    }
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PTRegistration.btnReadEID_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }
    }
}