﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;
using System.Threading.Tasks;

namespace Mediplus.HMS.Registration
{
    public partial class AppointmentPopup : System.Web.UI.Page
    {
        public static string APPOINTMENTMSG = (string)System.Configuration.ConfigurationSettings.AppSettings["APPOINTMENTMSG"];
        public static string REMINDERMSG = (string)System.Configuration.ConfigurationSettings.AppSettings["REMINDERMSG"];
        public static string AUTHORIZATIONMSG = (string)System.Configuration.ConfigurationSettings.AppSettings["AUTHORIZATIONMSG"];

        public static string APPOINTMENTMSG_A = (string)System.Configuration.ConfigurationSettings.AppSettings["APPOINTMENTMSG_A"];
        public static string REMINDERMSG_A = (string)System.Configuration.ConfigurationSettings.AppSettings["REMINDERMSG_A"];
        public static string AUTHORIZATIONMSG_A = (string)System.Configuration.ConfigurationSettings.AppSettings["AUTHORIZATIONMSG_A"];

        public string strMailPreview;

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "24";
                }


            }


        }

        void GetAppointment()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HAM_APPOINTMENTID='" + Convert.ToString(ViewState["AppId"]) + "'";


            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            string[] arrAppDtls = { " " };

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_FILENUMBER") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_PT_NAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) != "")
                {
                    txtFName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_LASTNAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]) != "")
                {
                    txtLName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MOBILENO") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILENO"]) != "")
                {
                    txtMobile.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILENO"]);
                }
                else
                {
                    txtMobile.Text = "";
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_EMAIL") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_EMAIL"]) != "")
                {
                    txtEmail.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_EMAIL"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentDate") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentDate"]) != "")
                {
                    txtFromDate.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentDate"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentSTTime") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]) != "")
                {

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadStartTime12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]));
                    }
                    else
                    {
                        LoadStartTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]));
                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentFITime") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]) != "")
                {
                    LoadFinishTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadFinishTime12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));
                    }
                    else
                    {
                        LoadFinishTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));
                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_REMARKS") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]) != "")
                {
                    txtRemarks.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_DR_CODE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_DR_CODE"]) != "")
                {
                    drpDoctor.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_DR_CODE"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HPM_REF_DR_CODE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HPM_REF_DR_CODE"]) != "")
                {
                    drpRefDoctor.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HPM_REF_DR_CODE"]);
                }




                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SERVICE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) != "")
                {
                    if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SERVICE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) != "" && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) != "0")
                    {
                        for (int intCount = 0; intCount < drpService.Items.Count; intCount++)
                        {
                            if (drpService.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]))
                            {
                                drpService.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]);
                                goto ForMStatus;
                            }

                        }
                    }
                ForMStatus: ;


                    lblServices.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_STATUS") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]) != "")
                {
                    drpStatus.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_CREATEDUSER") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATEDUSER"]) != "")
                {
                    lblCreatedUser.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATEDUSER"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_CREATED_DATE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATED_DATE"]) != "")
                {
                    DateTime dtCreatedDate;
                    dtCreatedDate = Convert.ToDateTime(DSAppt.Tables[0].Rows[0]["HAM_CREATED_DATE"]);

                    lblCreatedDate.Text = dtCreatedDate.ToString("dd/MM/yyyy HH:mm");
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MODIFIED_DATE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_DATE"]) != "")
                {
                    DateTime dtModifiedDate;
                    dtModifiedDate = Convert.ToDateTime(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_DATE"]);
                    lblModifiedDate.Text = dtModifiedDate.ToString("dd/MM/yyyy HH:mm");
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_OTAPPOINTMENT") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_OTAPPOINTMENT"]) != "")
                {
                    chkOTAppointment.Checked = Convert.ToBoolean(DSAppt.Tables[0].Rows[0]["HAM_OTAPPOINTMENT"]);

                    if (chkOTAppointment.Checked == true)
                    {
                        divOTAppointment.Visible = true;
                    }
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HSAM_PROCEDURE_CODE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HSAM_PROCEDURE_CODE"]) != "")
                {
                    txtServCode.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HSAM_PROCEDURE_CODE"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_HSAM_PROCEDURE_DESC") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_HSAM_PROCEDURE_DESC"]) != "")
                {
                    txtServName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_HSAM_PROCEDURE_DESC"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SURGERYTYPE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SURGERYTYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpSurgeryType.Items.Count; intCount++)
                    {
                        if (drpSurgeryType.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SURGERYTYPE"]))
                        {
                            drpSurgeryType.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SURGERYTYPE"]);
                        }

                    }


                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_ANESTHESIA_TYPE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHESIA_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpAnesthesia.Items.Count; intCount++)
                    {
                        if (drpAnesthesia.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHESIA_TYPE"]))
                        {
                            drpAnesthesia.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHESIA_TYPE"]);
                        }

                    }
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_ANESTHETIST_NAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHETIST_NAME"]) != "")
                {
                    for (int intCount = 0; intCount < drpAnesthetist.Items.Count; intCount++)
                    {
                        if (drpAnesthetist.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHETIST_NAME"]))
                        {
                            drpAnesthetist.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHETIST_NAME"]);
                        }

                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SPECIAL_REQUEST") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SPECIAL_REQUEST"]) != "")
                {
                    txtSpecialRequest.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SPECIAL_REQUEST"]);
                }




                if (DSAppt.Tables[0].Rows[0].IsNull("TimeIn") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeIn"]) != "")
                {

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadTimeIn12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeIn"]));
                    }
                    else
                    {
                        LoadTimeIn(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeIn"]));
                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("TimeOut") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeOut"]) != "")
                {
                    // LoadTimeOut(Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_TIME_OUT"]));

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadTimeOut12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeOut"]));
                    }
                    else
                    {
                        LoadTimeOut(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeOut"]));
                    }

                }



                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_PT_CATEGORY") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_CATEGORY"]) != "")
                {
                    for (int intCount = 0; intCount < drpPTCategory.Items.Count; intCount++)
                    {
                        if (drpPTCategory.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_CATEGORY"]))
                        {
                            drpPTCategory.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_CATEGORY"]);
                        }

                    }


                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_APP_PT_TYPE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APP_PT_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpAppPatientType.Items.Count; intCount++)
                    {
                        if (drpAppPatientType.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APP_PT_TYPE"]))
                        {
                            drpAppPatientType.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APP_PT_TYPE"]);
                        }

                    }


                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SEX") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SEX"]) != "" && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SEX"]) != "0")
                {
                    for (int intCount = 0; intCount < drpSex.Items.Count; intCount++)
                    {
                        if (drpSex.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SEX"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SEX"]);
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_ROOM") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ROOM"]) != "" && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ROOM"]) != "0")
                {
                    for (int intCount = 0; intCount < drpRoom.Items.Count; intCount++)
                    {
                        if (drpRoom.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ROOM"]))
                        {
                            drpRoom.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ROOM"]);
                            goto ForRoom;
                        }

                    }
                }
            ForRoom: ;


            }

        }

        Boolean CheckAppointment(string AppointmentDate)
        {

            string Criteria = " 1=1 ";

            string strSelectedDate = AppointmentDate;// txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";



            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";

            if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
            {
                //Criteria += " AND HAM_FILENUMBER !='" + txtFileNo.Text.Trim() + "'";

                Criteria += " AND HAM_APPOINTMENTID !=" + Convert.ToString(ViewState["AppId"]);
            }

            if (chkOTAppointment.Checked == false)
            {
                Criteria += " AND HAM_DR_CODE='" + drpDoctor.SelectedValue + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";
            }

            string strSTHour, strFIHours;
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
            {
                strSTHour = drpSTHour.SelectedValue;
                strFIHours = drpFIHour.SelectedValue;
                if (drpSTAM.SelectedValue == "PM")
                {
                    Int32 intStTime = 0;

                    if (drpSTHour.SelectedValue != "12")
                    {
                        intStTime = Convert.ToInt32(drpSTHour.SelectedValue) + 12;
                        strSTHour = Convert.ToString(intStTime);
                    }
                    else
                    {
                        strSTHour = drpSTHour.SelectedValue;
                    }

                }
                else
                {
                    strSTHour = drpSTHour.SelectedValue;
                }


                if (drpFIAM.SelectedValue == "PM")
                {
                    Int32 intFITime = 0;

                    if (drpFIHour.SelectedValue != "12")
                    {
                        intFITime = Convert.ToInt32(drpFIHour.SelectedValue) + 12;
                        strFIHours = Convert.ToString(intFITime);
                    }
                    else
                    {
                        strFIHours = drpFIHour.SelectedValue;
                    }


                }
                else
                {
                    strFIHours = drpFIHour.SelectedValue;
                }



            }
            else
            {
                strSTHour = drpSTHour.SelectedValue;
                strFIHours = drpFIHour.SelectedValue;

            }




            //string strSTTime = strForStartDate + " " + strSTHour + ":" + drpSTMin.SelectedValue + ":00";
            //string strFITime = strForStartDate + " " + strFIHours + ":" + drpFIMin.SelectedValue + ":00";

            //  Criteria += "  AND( HAM_STARTTIME BETWEEN  '" + strSTTime + "' AND  '" + strFITime + "'";
            //  Criteria += "  AND  HAM_FINISHTIME BETWEEN  '" + strSTTime + "'  AND '" + strFITime + "')";


            string strRawStDate = "";

            if (arrDate.Length > 1)
            {
                strRawStDate = arrDate[2] + arrDate[1] + arrDate[0];
            }


            Criteria += "  AND convert(varchar,HAM_FINISHTIME,112)+''+ replace(convert(VARCHAR(5),HAM_FINISHTIME,108),':','')  >  " + strRawStDate.Trim() + strSTHour + drpSTMin.SelectedValue;
            Criteria += " AND convert(varchar,HAM_STARTTIME,112)+''+ replace(convert(VARCHAR(5),HAM_STARTTIME,108),':','')  <  " + strRawStDate.Trim() + strFIHours + drpFIMin.SelectedValue;


            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;
        }

        Boolean CheckAppointment1()
        {

            string Criteria = " 1=1 ";

            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";

            Criteria += " AND HAM_FILENUMBER !='" + txtFileNo.Text.Trim() + "'";

            Criteria += " AND HAM_DR_CODE='" + drpDoctor.SelectedValue + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            //  Criteria += "  AND HAM_STARTTIME = '" + strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00'";

            string strSTTime = strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
            string strFITime = strForStartDate + " " + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + ":00";
            Criteria += "  AND( HAM_STARTTIME BETWEEN  DATEADD(minute, 1,'" + strSTTime + "') AND   DATEADD(minute, -1,'" + strFITime + "')";
            Criteria += "  OR  HAM_FINISHTIME BETWEEN  DATEADD(minute, 1,'" + strSTTime + "') AND DATEADD(minute, -1,'" + strFITime + "'))";



            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;
        }

        void GetAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1 ";


            Criteria += " AND HAS_STATUS='Waiting' OR  HAS_STATUS='Cancelled'";

            DS = dbo.AppointmentStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }

        void BindPatientDtls(string SearchOption)
        {
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (txtFileNo.Text.Trim() != "" && SearchOption == "PTFileNo")
            {
                Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text.Trim() + "'";
            }


            if (txtMobile.Text.Trim() != "" && SearchOption == "Mobile1")
            {
                Criteria += " AND HPM_MOBILE like '" + txtMobile.Text.Trim() + "%'";
            }


            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count == 1)
            {
                txtFName.Text = "";
                txtLName.Text = "";
                txtMobile.Text = "";
                txtEmail.Text = "";
                lblFullName.Text = "";

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_FNAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]) != "")
                {
                    txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_LNAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_LNAME"]) != "")
                {
                    txtLName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_LNAME"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_EMAIL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]) != "")
                {
                    txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) != "")
                {
                    lblFullName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_CATEGORY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_CATEGORY"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_VISA_TYPE"]) != "0")
                {
                    for (int intVisaType = 0; intVisaType < drpPTCategory.Items.Count; intVisaType++)
                    {
                        if (drpPTCategory.Items[intVisaType].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_CATEGORY"]))
                        {
                            drpPTCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_CATEGORY"]);
                            goto PTCategory;
                        }

                    }
                }

            PTCategory: ;

                if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "" && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "0")
                {
                    for (int intCount = 0; intCount < drpSex.Items.Count; intCount++)
                    {
                        if (drpSex.Items[intCount].Value == Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;


            }


            if (ds.Tables[0].Rows.Count > 1)
            {
                if (txtMobile.Text.Trim() != "" && SearchOption == "Mobile1")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "PatientPopup1('" + SearchOption + "','" + txtMobile.Text.Trim() + "');", true);
                }
            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }




        }
        void BindRefDoctor()
        {

            string Criteria = " 1=1 ";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.RefDoctorMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpRefDoctor.DataSource = ds;
                drpRefDoctor.DataValueField = "HRDM_REF_ID";
                drpRefDoctor.DataTextField = "FullName";
                drpRefDoctor.DataBind();
            }
            drpRefDoctor.Items.Insert(0, "--- Select ---");
            drpRefDoctor.Items[0].Value = "0";



        }


        void BindAnesthetistDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_DEPT_ID='ANAETHESIOLOGY' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpAnesthetist.DataSource = ds;
                drpAnesthetist.DataValueField = "HSFM_STAFF_ID";
                drpAnesthetist.DataTextField = "FullName";
                drpAnesthetist.DataBind();
            }

            drpAnesthetist.Items.Insert(0, "--- Select ---");
            drpAnesthetist.Items[0].Value = "0";


        }

        void BindAppointmentServices()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentServicesGet();

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpService.DataSource = DS;
                drpService.DataTextField = "HAS_SERVICENAME";
                drpService.DataValueField = "HAS_SERVICENAME";
                drpService.DataBind();
            }

        }


        void GetDuration()
        {

            lblDuration.Text = "";
            lblDurationType.Text = "";
            string strDuration;
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string Criteria = " HAS_SERVICENAME ='" + drpService.SelectedValue + "'";



            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*", "HMS_APPOINTMENTSERVICES", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HAS_SERVICE_DURATION") == false && Convert.ToString(DS.Tables[0].Rows[0]["HAS_SERVICE_DURATION"]) != "")
                {
                    strDuration = Convert.ToString(DS.Tables[0].Rows[0]["HAS_SERVICE_DURATION"]);

                    lblDuration.Text = strDuration;
                    lblDurationType.Text = " Minutes";
                }

            }
        }


        void BindServiceGrid()
        {

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";



            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*", "HMS_APPOINTMENTSERVICES", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvService.DataSource = DS;
                gvService.DataBind();

            }
        }
        void BindAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentStatusGet("1=1");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpStatus.DataSource = DS;
                drpStatus.DataTextField = "HAS_STATUS";
                drpStatus.DataValueField = "HAS_ID";
                drpStatus.DataBind();

                //drpStatus.SelectedValue = "4";
                for (int intCount = 0; intCount < drpStatus.Items.Count; intCount++)
                {
                    if (drpStatus.Items[intCount].Text == "Booked")
                    {
                        drpStatus.SelectedValue = DS.Tables[0].Rows[intCount]["HAS_ID"].ToString();
                    }

                }

            }

        }

        void BindTime()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");

                drpFIHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpFIHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");


                drpTimeInHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpTimeInHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");

                drpTimeOutHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpTimeOutHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            drpTimeInMin.Items.Insert(0, Convert.ToString("00"));
            drpTimeInMin.Items[0].Value = Convert.ToString("00");

            drpTimeOutMin.Items.Insert(0, Convert.ToString("00"));
            drpTimeOutMin.Items[0].Value = Convert.ToString("00");



            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                drpTimeInMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpTimeInMin.Items[index].Value = Convert.ToString(j - intCount);

                drpTimeOutMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpTimeOutMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindTime12Hrs()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);
            int index = 0;

            for (int i = 1; i <= 12; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

                drpFIHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
                drpFIHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void LoadStartTime(string strSTTime)
        {

            string[] arrSTTime = strSTTime.Split(':');

            if (arrSTTime.Length > 1)
            {
                for (int intCount = 0; intCount < drpSTHour.Items.Count; intCount++)
                {
                    if (drpSTHour.Items[intCount].Value == arrSTTime[0])
                    {
                        drpSTHour.SelectedValue = arrSTTime[0];
                        intCount = drpSTHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpSTMin.Items.Count; intCount++)
                {
                    if (drpSTMin.Items[intCount].Value == arrSTTime[1])
                    {
                        drpSTMin.SelectedValue = arrSTTime[1];
                        intCount = drpSTMin.Items.Count;
                    }

                }


            }
        }

        void LoadFinishTime(string strFITime)
        {
            string[] arrFITime = strFITime.Split(':');

            if (arrFITime.Length > 1)
            {
                for (int intCount = 0; intCount < drpFIHour.Items.Count; intCount++)
                {
                    if (drpFIHour.Items[intCount].Value == arrFITime[0])
                    {
                        drpFIHour.SelectedValue = arrFITime[0];
                        intCount = drpFIHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpFIMin.Items.Count; intCount++)
                {
                    if (drpFIMin.Items[intCount].Value == arrFITime[1])
                    {
                        drpFIMin.SelectedValue = arrFITime[1];
                        intCount = drpFIMin.Items.Count;
                    }

                }

            }
        }

        void LoadStartTime12Hrs(string strSTTime)
        {

            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strSTTime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                string strHour = arrTime[0];
                if (arrTime[0].Length == 1)
                {
                    strHour = "0" + arrTime[0];

                }

                for (int intCount = 0; intCount < drpSTHour.Items.Count; intCount++)
                {


                    if (drpSTHour.Items[intCount].Value == strHour)
                    {
                        drpSTHour.SelectedValue = strHour;
                        intCount = drpSTHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpSTMin.Items.Count; intCount++)
                {
                    if (drpSTMin.Items[intCount].Value == arrTime[1])
                    {
                        drpSTMin.SelectedValue = arrTime[1];
                        intCount = drpSTMin.Items.Count;
                    }

                }


                string[] arrSTTime1 = strFormatTie.Split(' ');


                if (arrSTTime.Length > 1)
                {
                    drpSTAM.SelectedValue = arrSTTime1[2];

                }


            }
        }

        void LoadFinishTime12Hrs(string strFITime)
        {
            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strFITime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                string strHour = arrTime[0];
                if (arrTime[0].Length == 1)
                {
                    strHour = "0" + arrTime[0];

                }

                for (int intCount = 0; intCount < drpFIHour.Items.Count; intCount++)
                {
                    if (drpFIHour.Items[intCount].Value == strHour)
                    {
                        drpFIHour.SelectedValue = strHour;
                        intCount = drpFIHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpFIMin.Items.Count; intCount++)
                {
                    if (drpFIMin.Items[intCount].Value == arrTime[1])
                    {
                        drpFIMin.SelectedValue = arrTime[1];
                        intCount = drpFIMin.Items.Count;
                    }

                }
                string[] arrSTTime1 = strFormatTie.Split(' ');

                if (arrSTTime.Length > 1)
                {
                    drpFIAM.SelectedValue = arrSTTime1[2];

                    //  drpFIAM.SelectedValue = drpSTAM.SelectedValue;

                }


            }
        }



        void LoadTimeIn(string strSTTime)
        {

            string[] arrSTTime = strSTTime.Split(':');

            if (arrSTTime.Length > 1)
            {
                for (int intCount = 0; intCount < drpTimeInHour.Items.Count; intCount++)
                {
                    if (drpTimeInHour.Items[intCount].Value == arrSTTime[0])
                    {
                        drpTimeInHour.SelectedValue = arrSTTime[0];
                        intCount = drpTimeInHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeInMin.Items.Count; intCount++)
                {
                    if (drpTimeInMin.Items[intCount].Value == arrSTTime[1])
                    {
                        drpTimeInMin.SelectedValue = arrSTTime[1];
                        intCount = drpTimeInMin.Items.Count;
                    }

                }


            }
        }

        void LoadTimeOut(string strFITime)
        {
            string[] arrFITime = strFITime.Split(':');

            if (arrFITime.Length > 1)
            {
                for (int intCount = 0; intCount < drpTimeOutHour.Items.Count; intCount++)
                {
                    if (drpTimeOutHour.Items[intCount].Value == arrFITime[0])
                    {
                        drpTimeOutHour.SelectedValue = arrFITime[0];
                        intCount = drpTimeOutHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeOutMin.Items.Count; intCount++)
                {
                    if (drpTimeOutMin.Items[intCount].Value == arrFITime[1])
                    {
                        drpTimeOutMin.SelectedValue = arrFITime[1];
                        intCount = drpTimeOutMin.Items.Count;
                    }

                }

            }
        }

        void LoadTimeIn12Hrs(string strSTTime)
        {

            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strSTTime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                for (int intCount = 0; intCount < drpTimeInHour.Items.Count; intCount++)
                {
                    if (drpTimeInHour.Items[intCount].Value == arrTime[0])
                    {
                        drpTimeInHour.SelectedValue = arrTime[0];
                        intCount = drpTimeInHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeInMin.Items.Count; intCount++)
                {
                    if (drpTimeInMin.Items[intCount].Value == arrTime[1])
                    {
                        drpTimeInMin.SelectedValue = arrTime[1];
                        intCount = drpTimeInMin.Items.Count;
                    }

                }


                string[] arrSTTime1 = strFormatTie.Split(' ');


                if (arrSTTime.Length > 1)
                {
                    drpTimeInAM.SelectedValue = arrSTTime1[2];

                }


            }
        }

        void LoadTimeOut12Hrs(string strFITime)
        {
            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strFITime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                for (int intCount = 0; intCount < drpTimeOutHour.Items.Count; intCount++)
                {
                    if (drpTimeOutHour.Items[intCount].Value == arrTime[0])
                    {
                        drpTimeOutHour.SelectedValue = arrTime[0];
                        intCount = drpTimeOutHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeOutMin.Items.Count; intCount++)
                {
                    if (drpTimeOutMin.Items[intCount].Value == arrTime[1])
                    {
                        drpTimeOutMin.SelectedValue = arrTime[1];
                        intCount = drpTimeOutMin.Items.Count;
                    }

                }
                string[] arrSTTime1 = strFormatTie.Split(' ');

                if (arrSTTime.Length > 1)
                {
                    drpTimeOutAM.SelectedValue = arrSTTime1[2];

                }


            }
        }


        void GetServiceMasterName(string ServID, out string ServName)
        {

            ServName = "";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_HAAD_CODE  ='" + ServID + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            txtServName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServName = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
            }

        }

        void GetDepartmentID()
        {

            string strDeptID = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + drpDoctor.SelectedValue + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strDeptID = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]);
            }

            ViewState["Department"] = strDeptID;
        }


        void GetDepartmentName()
        {
            string strDeptID = "", Department = "";

            // strDeptID = GetDepartmentID();

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' and HDM_DEP_ID='" + strDeptID + "'";
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Department = Convert.ToString(ds.Tables[0].Rows[0]["HDM_DEP_NAME"]);
            }

            ViewState["Department"] = Department;
        }

        void BindScreenCustomization()
        {
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS'";

            DataSet DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "SMS_TYPE")
                    {
                        ViewState["SMS_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "SMS_SenderID")
                    {
                        ViewState["SMS_SenderID"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                    }


                }
            }


        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.CommonMastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (strType == "PT_CATEGORY")
                {
                    drpPTCategory.DataSource = DS;
                    drpPTCategory.DataTextField = "HCM_DESC";
                    drpPTCategory.DataValueField = "HCM_CODE";
                    drpPTCategory.DataBind();
                }

                if (strType == "APP_PT_TYPE")
                {
                    drpAppPatientType.DataSource = DS;
                    drpAppPatientType.DataTextField = "HCM_DESC";
                    drpAppPatientType.DataValueField = "HCM_CODE";
                    drpAppPatientType.DataBind();
                }

                if (strType == "APP_ROOM")
                {
                    drpRoom.DataSource = DS;
                    drpRoom.DataTextField = "HCM_DESC";
                    drpRoom.DataValueField = "HCM_CODE";
                    drpRoom.DataBind();
                }
            }

            if (strType == "PT_CATEGORY")
            {
                drpPTCategory.Items.Insert(0, "--- Select ---");
                drpPTCategory.Items[0].Value = "";

            }
            if (strType == "APP_PT_TYPE")
            {
                drpAppPatientType.Items.Insert(0, "--- Select ---");
                drpAppPatientType.Items[0].Value = "";
            }
            if (strType == "APP_ROOM")
            {
                drpRoom.Items.Insert(0, "--- Select ---");
                drpRoom.Items[0].Value = "";
            }


        }

        void Clear()
        {
            txtFileNo.Text = "";
            txtFName.Text = "";
            txtLName.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
            drpSex.SelectedIndex = 0;
        }
        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = txtFileNo.Text;
            objCom.ID = "";
            objCom.ScreenID = "APPOINT";
            objCom.ScreenName = "Patient Appointment";
            objCom.ScreenType = "TRANSACTION";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {


                btnSave.Enabled = false;
                btnCancel.Enabled = false;
                btnDelete.Enabled = false;

                btnSendSMS.Enabled = false;
                btnSendMail.Enabled = false;
                btnClose.Enabled = false;





            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;
                btnCancel.Enabled = false;

                btnSendSMS.Enabled = false;
                btnSendMail.Enabled = false;
                btnClose.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");

            }
        }

        #endregion

        #region AutoExtenderFunctions


        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID  like '" + prefixText + "%' ";
            dboperations dbo = new dboperations();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_HAAD_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            dboperations dbo = new dboperations();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_HAAD_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion


        #region EVents
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Patient Appointment Creation Page");

                BindSystemOption();
                BindScreenCustomization();
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");

                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                {

                    BindTime12Hrs();
                    drpSTAM.Visible = true;
                    drpFIAM.Visible = true;
                    drpTimeInAM.Visible = true;
                    drpTimeOutAM.Visible = true;



                }
                else
                {
                    BindTime();
                    drpTimeInAM.Visible = false;
                    drpTimeOutAM.Visible = false;
                }

                ViewState["PageName"] = Convert.ToString(Request.QueryString["PageName"]);
                ViewState["AppId"] = Convert.ToString(Request.QueryString["AppId"]);
                ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                ViewState["Time"] = Convert.ToString(Request.QueryString["Time"]);
                ViewState["DrID"] = Convert.ToString(Request.QueryString["DrID"]);
                ViewState["PatientID"] = Convert.ToString(Request.QueryString["PatientID"]);
                ViewState["DeptID"] = Convert.ToString(Request.QueryString["DeptID"]);

                if (Convert.ToString(ViewState["PageName"]) == "ApptEmrDr")
                {
                    drpDoctor.Enabled = false;
                }

                if (Convert.ToString(ViewState["PatientID"]) != "" && Convert.ToString(ViewState["PatientID"]) != null)
                {
                    txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);
                    BindPatientDtls("PTFileNo");
                }

                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    chkOTAppointment.Visible = true;
                }

                if (GlobalValues.FileDescription.ToUpper() == "KHALID_NUAIMI")
                {
                    btnSendSMSComplement.Visible = true;
                }

                if (GlobalValues.FileDescription.ToUpper() == "CHANDRAN")
                {
                    btnLabelTreatAppointPrint.Visible = true;
                }
                BindCommonMaster("PT_CATEGORY");
                BindCommonMaster("APP_PT_TYPE");
                BindCommonMaster("APP_ROOM");

                BindDoctor();
                BindRefDoctor();
                BindAnesthetistDoctor();
                BindAppointmentServices();
                GetDuration();
                BindAppointmentStatus();
                GetAppointment();
                GetAppointmentStatus();
                if (Convert.ToString(ViewState["Date"]) != null && Convert.ToString(ViewState["Date"]) != "0")
                {
                    txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                }
                if (Convert.ToString(ViewState["Time"]) != null && Convert.ToString(ViewState["Time"]) != "0")
                {
                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadStartTime12Hrs(Convert.ToString(ViewState["Time"]));
                    }
                    else
                    {
                        LoadStartTime(Convert.ToString(ViewState["Time"]));
                    }


                    int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
                    string strlTime;
                    strlTime = Convert.ToString(ViewState["Time"]);// drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;

                    string strNewTime1 = "";
                    if (strNewTime1 == "")
                    {
                        strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                    }

                    string[] arrlTime = strlTime.Split(':');
                    if (arrlTime.Length > 1)
                    {
                        strlTime = arrlTime[0];
                    }

                    if (Convert.ToInt32(strNewTime1) >= 60)
                    {
                        strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                    }
                    else
                    {
                        strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
                    }





                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {

                        if (lblDuration.Text.Trim() != "")
                        {
                            string stFrmTime = txtFromDate.Text + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";

                            DateTime dtStartDate = DateTime.ParseExact(txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

                            // Add 30 minutes
                            dtStartDate = dtStartDate.AddMinutes(Convert.ToInt64(lblDuration.Text));

                            strlTime = dtStartDate.ToString("HH:mm");

                            LoadFinishTime12Hrs(strlTime);
                        }
                        else
                        {
                            LoadFinishTime12Hrs(strlTime);
                        }

                    }
                    else
                    {
                        LoadFinishTime(strlTime);
                    }


                }

                if (Convert.ToString(ViewState["DrID"]) != null && Convert.ToString(ViewState["DrID"]) != "0")
                {
                    drpDoctor.SelectedValue = Convert.ToString(ViewState["DrID"]);

                }

                GetDepartmentID();


            }
        }

        void fnSave(string AppointmentDate)
        {
            string strSTHour, strFIHours, stsTimeInHour, stsTimeOutHour;


            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
            {
                //-------------------------------------------------------
                strSTHour = drpSTHour.SelectedValue;
                strFIHours = drpFIHour.SelectedValue;
                if (drpSTAM.SelectedValue == "PM")
                {
                    Int32 intStTime = 0;

                    if (drpSTHour.SelectedValue != "12")
                    {
                        intStTime = Convert.ToInt32(drpSTHour.SelectedValue) + 12;
                        strSTHour = Convert.ToString(intStTime);
                    }
                    else
                    {
                        strSTHour = drpSTHour.SelectedValue;
                    }

                }
                else
                {


                    if (drpSTHour.SelectedValue == "12")
                    {

                        strSTHour = "00";
                    }
                    else
                    {
                        strSTHour = drpSTHour.SelectedValue;
                    }
                }


                if (drpFIAM.SelectedValue == "PM")
                {
                    Int32 intFITime = 0;

                    if (drpFIHour.SelectedValue != "12")
                    {
                        intFITime = Convert.ToInt32(drpFIHour.SelectedValue) + 12;
                        strFIHours = Convert.ToString(intFITime);
                    }
                    else
                    {
                        strFIHours = drpFIHour.SelectedValue;
                    }


                }
                else
                {


                    if (drpFIHour.SelectedValue == "12")
                    {
                        strFIHours = "00";
                    }
                    else
                    {
                        strFIHours = drpFIHour.SelectedValue;
                    }
                }

                //-----------------------------------------------------------------

                //-------------------------------------------------------
                stsTimeInHour = drpTimeInHour.SelectedValue;
                stsTimeOutHour = drpTimeOutHour.SelectedValue;

                if (drpTimeInHour.SelectedValue != "")
                {
                    if (drpSTAM.SelectedValue == "PM")
                    {
                        Int32 intStTime = 0;

                        if (drpTimeInHour.SelectedValue != "12")
                        {
                            intStTime = Convert.ToInt32(drpTimeInHour.SelectedValue) + 12;
                            stsTimeInHour = Convert.ToString(intStTime);
                        }
                        else
                        {
                            stsTimeInHour = drpTimeInHour.SelectedValue;
                        }

                    }
                    else
                    {
                        stsTimeInHour = drpTimeInHour.SelectedValue;
                    }
                }

                if (drpTimeOutHour.SelectedValue != "")
                {
                    if (drpFIAM.SelectedValue == "PM")
                    {
                        Int32 intFITime = 0;

                        if (drpTimeOutHour.SelectedValue != "12")
                        {
                            intFITime = Convert.ToInt32(drpTimeOutHour.SelectedValue) + 12;
                            stsTimeOutHour = Convert.ToString(intFITime);
                        }
                        else
                        {
                            stsTimeOutHour = drpTimeOutHour.SelectedValue;
                        }


                    }
                    else
                    {
                        stsTimeOutHour = drpTimeOutHour.SelectedValue;
                    }

                }
                //-----------------------------------------------------------------

            }
            else
            {
                strSTHour = drpSTHour.SelectedValue;
                strFIHours = drpFIHour.SelectedValue;

                stsTimeInHour = drpTimeInHour.SelectedValue;
                stsTimeOutHour = drpTimeOutHour.SelectedValue;

            }

            //  goto SaveEnd;
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_AppointmentOutlookAdd";
            cmd.Parameters.Add(new SqlParameter("@HAM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
            cmd.Parameters.Add(new SqlParameter("@HAM_DR_CODE", SqlDbType.VarChar)).Value = drpDoctor.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_DR_NAME", SqlDbType.VarChar)).Value = drpDoctor.SelectedItem.Text;
            cmd.Parameters.Add(new SqlParameter("@HAM_PT_CODE", SqlDbType.VarChar)).Value = "Reg:No " + txtFileNo.Text.Trim() + " Mobile: " + txtMobile.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@HAM_PT_NAME", SqlDbType.VarChar)).Value = txtFName.Text.Trim();


            //  DateTime dtStartDate = DateTime.ParseExact(txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

            //   DateTime dtFinishDate = DateTime.ParseExact(txtFromDate.Text.Trim() + " " + strFIHours.SelectedValue + ":" + drpFIMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);



            cmd.Parameters.Add(new SqlParameter("@HAM_STARTTIME", SqlDbType.VarChar)).Value = AppointmentDate + " " + strSTHour + ":" + drpSTMin.SelectedValue + ":00";
            cmd.Parameters.Add(new SqlParameter("@HAM_FINISHTIME", SqlDbType.VarChar)).Value = AppointmentDate + " " + strFIHours + ":" + drpFIMin.SelectedValue + ":00";

            cmd.Parameters.Add(new SqlParameter("@HAM_REMARKS", SqlDbType.VarChar)).Value = txtRemarks.Text;
            cmd.Parameters.Add(new SqlParameter("@HAM_STATUS", SqlDbType.VarChar)).Value = drpStatus.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_LASTNAME", SqlDbType.VarChar)).Value = txtLName.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@HAM_FILENUMBER", SqlDbType.VarChar)).Value = txtFileNo.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@HAM_MOBILENO", SqlDbType.VarChar)).Value = txtMobile.Text.Trim();

            if (chkStatus.Checked == true)
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_SERVICE", SqlDbType.VarChar)).Value = lblServices.Text;
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_SERVICE", SqlDbType.VarChar)).Value = drpService.SelectedValue;
            }
            cmd.Parameters.Add(new SqlParameter("@HAM_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@HAM_CREATEDUSER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);


            cmd.Parameters.Add(new SqlParameter("@HAM_OTAPPOINTMENT", SqlDbType.Bit)).Value = chkOTAppointment.Checked;
            cmd.Parameters.Add(new SqlParameter("@HSAM_PROCEDURE_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@HAM_HSAM_PROCEDURE_DESC", SqlDbType.VarChar)).Value = txtServName.Text.Trim();
            cmd.Parameters.Add(new SqlParameter("@HAM_SURGERYTYPE", SqlDbType.VarChar)).Value = drpSurgeryType.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_ANESTHESIA_TYPE", SqlDbType.VarChar)).Value = drpAnesthesia.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_ANESTHETIST_NAME", SqlDbType.VarChar)).Value = drpAnesthetist.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_SPECIAL_REQUEST", SqlDbType.VarChar)).Value = txtSpecialRequest.Text.Trim();

            if (stsTimeInHour != "")
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_TIME_IN", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + stsTimeInHour + ":" + drpTimeInMin.SelectedValue + ":00";
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_TIME_IN", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + "00:00:00";

            }

            if (stsTimeOutHour != "")
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_TIME_OUT", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + stsTimeOutHour + ":" + drpTimeOutMin.SelectedValue + ":00";
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_TIME_OUT", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + "00:00:00";
            }

            if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_APPOINTMENTID", SqlDbType.Int)).Value = Convert.ToString(ViewState["AppId"]);
                cmd.Parameters.Add(new SqlParameter("@MODE", SqlDbType.Char)).Value = "M";

            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@HAM_APPOINTMENTID", SqlDbType.Int)).Value = 0;
                cmd.Parameters.Add(new SqlParameter("@MODE", SqlDbType.Char)).Value = "A";
            }

            cmd.Parameters.Add(new SqlParameter("@HAM_PT_CATEGORY", SqlDbType.VarChar)).Value = drpPTCategory.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_APP_PT_TYPE", SqlDbType.VarChar)).Value = drpAppPatientType.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_SEX", SqlDbType.VarChar)).Value = drpSex.SelectedValue;
            cmd.Parameters.Add(new SqlParameter("@HAM_ROOM", SqlDbType.VarChar)).Value = drpRoom.SelectedValue;

            if (drpRefDoctor.SelectedIndex != 0)
            {
                cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_CODE", SqlDbType.VarChar)).Value = drpRefDoctor.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HPM_REF_DR_NAME", SqlDbType.VarChar)).Value = drpRefDoctor.SelectedItem.Text;
            }


            cmd.ExecuteNonQuery();
            con.Close();

            if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
            {
                AuditLogAdd("MODIFY", "Modify Existing entry in the Patient Appointment");
            }
            else
            {
                AuditLogAdd("ADD", "Add new entry in the Patient Appointment");
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "','" + Convert.ToString(ViewState["DeptID"]) + "');", true);

        SaveEnd: ;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strDuplicatAppDates = "";

                if (CheckAppointment(txtFromDate.Text.Trim()) == true)
                {
                    //lblStatus.Text = "Appointment already exist for this Time";
                    // lblStatus.ForeColor = System.Drawing.Color.Red;

                    strDuplicatAppDates = txtFromDate.Text.Trim();
                    goto SaveEnd;
                }

                fnSave(txtFromDate.Text.Trim());
            SaveEnd: ;

                if (txtUpcomingDays.Text != "")
                {

                    Int32 intDays = Convert.ToInt32(txtUpcomingDays.Text.Trim());

                    for (int i = 1; i <= intDays; i++)
                    {
                        DateTime dtCurrentDate = DateTime.ParseExact(txtFromDate.Text.Trim(), "dd/MM/yyyy", null);
                        DateTime dtUpComingApptDate = dtCurrentDate.AddDays(i);

                        if (CheckAppointment(dtUpComingApptDate.ToString("dd/MM/yyyy")) == false)
                        {
                            fnSave(dtUpComingApptDate.ToString("dd/MM/yyyy"));

                        }
                        else
                        {
                            if (strDuplicatAppDates != "")
                            {
                                strDuplicatAppDates += ", " + dtUpComingApptDate.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                strDuplicatAppDates = dtUpComingApptDate.ToString("dd/MM/yyyy");
                            }

                        }


                    }



                }

                if (strDuplicatAppDates != "")
                {

                    lblStatus.Text = "Appointment already exist for For Following Date(s) " + strDuplicatAppDates;
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Details Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                dboperations dbo = new dboperations();
                string strStatusId = "0";

                for (int i = 0; i <= drpStatus.Items.Count - 1; i++)
                {
                    if (drpStatus.Items[i].Text == "Cancelled")
                    {
                        strStatusId = drpStatus.Items[i].Value;
                        goto ForEnd;
                    }
                }
            ForEnd: ;
                if (Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    dbo.AppointmentOutlookUpdate(Convert.ToString(ViewState["AppId"]), strStatusId);

                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "','" + Convert.ToString(ViewState["DeptID"]) + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnCancel_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindPatientDtls("PTFileNo");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.aspx_txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtMobile_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindPatientDtls("Mobile1");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.aspx_txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnSendSMS_Click(object sender, EventArgs e)
        {

            if (txtMobile.Text.Trim() == "")
            {
                lblStatus.Text = "Please enter Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            string MobileNumber = txtMobile.Text.Trim();

            if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
            {
                lblStatus.Text = "Please check the Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            clsSMS objSMS = new clsSMS();
            string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";

            string strContent = "";
            strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + strNewLineChar + txtFName.Text.Trim() + " " + txtLName.Text.Trim()
               + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;

            //if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            //{
            strContent = "";
            if (drpSMSLanguage.SelectedValue == "3" || drpSMSLanguage.SelectedValue == "8")
            {
                strContent = APPOINTMENTMSG_A.Replace("|PatientName|", txtFName.Text.Trim());
            }
            else
            {
                strContent = APPOINTMENTMSG.Replace("|PatientName|", txtFName.Text.Trim());
            }

            strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());
            // strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);

            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
            {
                strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + " " + drpSTAM.SelectedValue);//+ "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            }
            else
            {
                strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue);//+ "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            }

            strContent = strContent.Replace("|department|", Convert.ToString(ViewState["Department"]));
            strContent = strContent.Replace("|DoctorName|", drpDoctor.SelectedItem.Text);

            strContent = strContent + " " + txtRemarks.Text.Trim();
            // }


            objSMS.MobileNumber = txtMobile.Text.Trim(); // "0502213045";
            objSMS.template = strContent;


            Boolean boolResult = false;

            if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "AXON")
            {

                // boolResult = objSMS.SendSMS_smsmarketing();
                //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                boolResult = objSMS.SendSMS_Axome(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "BRANDMASTER")
            {

                // boolResult = objSMS.SendSMS_smsmarketing();
                //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                boolResult = objSMS.SendSMS_BrandMaster(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSMARKETING")
            {
                boolResult = objSMS.SendSMS_smsmarketing();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "MSHASTRA")
            {
                boolResult = objSMS.SendSMS_Mshastra();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "API_MPP_KWT")
            {
                //LANGUAGE : 1  FOR ENGLISH  , 3 FOR ARABIC
                boolResult = objSMS.SendSMS_API_MPP_KWT(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSCOUNTRY")
            {
                boolResult = objSMS.SendSMS_SMScountry();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMARTCALL")
            {
                //LANGUAGE : 0  FOR ENGLISH  , 1 FOR ARABIC
                boolResult = objSMS.SendSMS_Smartcall(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else
            {
                boolResult = objSMS.SendSMS();
            }


            if (boolResult == true)
            {
                lblStatus.Text = "SMS Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblStatus.Text = "SMS Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        FunEnd: ;

        }


        protected void btnSendSMSReminder_Click(object sender, EventArgs e)
        {

            if (txtMobile.Text.Trim() == "")
            {
                lblStatus.Text = "Please enter Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            string MobileNumber = txtMobile.Text.Trim();

            if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
            {
                lblStatus.Text = "Please check the Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            clsSMS objSMS = new clsSMS();
            string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";

            string strContent = "";
            strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + strNewLineChar + txtFName.Text.Trim() + " " + txtLName.Text.Trim()
               + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;

            //if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            //{
            strContent = "";

            if (drpSMSLanguage.SelectedValue == "3" || drpSMSLanguage.SelectedValue == "8")
            {
                strContent = REMINDERMSG_A.Replace("|PatientName|", txtFName.Text.Trim());
            }
            else
            {
                strContent = REMINDERMSG.Replace("|PatientName|", txtFName.Text.Trim());
            }

            strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());
            //strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
            {
                strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + " " + drpSTAM.SelectedValue);//+ "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            }
            else
            {
                strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue);//+ "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            }
            strContent = strContent.Replace("|department|", Convert.ToString(ViewState["Department"]));
            strContent = strContent.Replace("|DoctorName|", drpDoctor.SelectedItem.Text);

            strContent = strContent + " " + txtRemarks.Text.Trim();
            //  }


            objSMS.MobileNumber = txtMobile.Text.Trim(); // "0502213045";
            objSMS.template = strContent;


            Boolean boolResult = false;

            if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "AXON")
            {

                // boolResult = objSMS.SendSMS_smsmarketing();
                //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                boolResult = objSMS.SendSMS_Axome(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSMARKETING")
            {
                boolResult = objSMS.SendSMS_smsmarketing();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "MSHASTRA")
            {
                boolResult = objSMS.SendSMS_Mshastra();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "API_MPP_KWT")
            {
                //LANGUAGE : 1  FOR ENGLISH  , 3 FOR ARABIC
                boolResult = objSMS.SendSMS_API_MPP_KWT(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSCOUNTRY")
            {
                boolResult = objSMS.SendSMS_SMScountry();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMARTCALL")
            {
                //LANGUAGE : 0  FOR ENGLISH  , 1 FOR ARABIC
                boolResult = objSMS.SendSMS_Smartcall(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else
            {
                boolResult = objSMS.SendSMS();
            }


            if (boolResult == true)
            {
                lblStatus.Text = "SMS Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblStatus.Text = "SMS Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }


        FunEnd: ;

        }



        protected void btnSendSMSAuth_Click(object sender, EventArgs e)
        {

            if (txtMobile.Text.Trim() == "")
            {
                lblStatus.Text = "Please enter Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            string MobileNumber = txtMobile.Text.Trim();

            if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
            {
                lblStatus.Text = "Please check the Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            clsSMS objSMS = new clsSMS();
            string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";

            string strContent = "";
            strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + strNewLineChar + txtFName.Text.Trim() + " " + txtLName.Text.Trim()
               + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;

            //if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            //{
            strContent = "";

            if (drpSMSLanguage.SelectedValue == "8")
            {
                strContent = AUTHORIZATIONMSG_A.Replace("|PatientName|", txtFName.Text.Trim());
            }
            else
            {
                strContent = AUTHORIZATIONMSG.Replace("|PatientName|", txtFName.Text.Trim());
            }

            strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());
            //strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
            {
                strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + " " + drpSTAM.SelectedValue);//+ "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            }
            else
            {
                strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue);//+ "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            }
            strContent = strContent.Replace("|department|", Convert.ToString(ViewState["Department"]));
            strContent = strContent.Replace("|DoctorName|", drpDoctor.SelectedItem.Text);
            strContent = strContent + " " + txtRemarks.Text.Trim();
            //}


            objSMS.MobileNumber = txtMobile.Text.Trim(); // "0502213045";
            objSMS.template = strContent;


            Boolean boolResult = false;

            if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "AXON")
            {

                // boolResult = objSMS.SendSMS_smsmarketing();

                //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC
                boolResult = objSMS.SendSMS_Axome(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSMARKETING")
            {
                boolResult = objSMS.SendSMS_smsmarketing();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "MSHASTRA")
            {
                boolResult = objSMS.SendSMS_Mshastra();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "API_MPP_KWT")
            {
                //LANGUAGE : 1  FOR ENGLISH  , 3 FOR ARABIC
                boolResult = objSMS.SendSMS_API_MPP_KWT(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSCOUNTRY")
            {
                boolResult = objSMS.SendSMS_SMScountry();
            }
            else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMARTCALL")
            {
                //LANGUAGE : 0  FOR ENGLISH  , 1 FOR ARABIC
                boolResult = objSMS.SendSMS_Smartcall(Convert.ToInt32(drpSMSLanguage.SelectedValue));
            }
            else
            {
                boolResult = objSMS.SendSMS();
            }


            if (boolResult == true)
            {
                lblStatus.Text = "SMS Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblStatus.Text = "SMS Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        FunEnd: ;

        }


        string BindMailContnt()
        {

            string Details_String = "";

            Details_String = Details_String + "<TABLE cellpadding='2' cellspacing='2'>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>Dear  <b>" + txtFName.Text + ",</b></TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='Left'>Appointment Details</TD></TR>";
            Details_String = Details_String + "<TR><TD valign='top' align='left'><B>Date & Time : </B></TD> <TD>" + txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + "</TD></TR>";
            Details_String = Details_String + "<TR><TD valign='top' align='left'><B>Doctor Name : </B></TD> <TD>" + "DR:" + drpDoctor.SelectedItem.Text + "</TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>Regards</TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>" + GlobalValues.HospitalName + "</TD></TR>";

            Details_String = Details_String + "</TABLE>";

            return Details_String;
        }
        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Email ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                CommonBAL objCom = new CommonBAL();



                string Details_String = BindMailContnt();

                int intStatuls;

                intStatuls = objCom.HTMLMailSend(txtEmail.Text.Trim(), "Reg Appointment", Details_String);
                lblStatus.Text = "Mail Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnSendMail_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Mail Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }

        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            string Details_String = "";

            string strFromEmail = GlobalValues.Hospital_FromMailID;
            string strToEmail = txtEmail.Text.Trim();

            Details_String = Details_String + "<TABLE cellpadding='2' cellspacing='2'>";
            Details_String = Details_String + "<TR><TD valign='top' align='left'><B>From: </B></TD> <TD>" + strFromEmail + "</TD></TR>";
            Details_String = Details_String + "<TR><TD valign='top' align='left'><B>To : </B></TD> <TD> <B>" + strToEmail + "  </B> </TD></TR>";
            Details_String = Details_String + "<TR><TD valign='top' align='left'><B>Subject : </B></TD> <TD>Reg Appointment</TD></TR>";
            Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";


            Details_String = Details_String + "</TABLE>";

            Details_String = Details_String + BindMailContnt();

            strMailPreview = Details_String;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMailPreview()", true);
            divMailPreview.Visible = true;

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                dboperations dbo = new dboperations();
                if (Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    dbo.AppointmentOutlookDelete(Convert.ToString(ViewState["AppId"]));
                    AuditLogAdd("DELETE", "Delete entry in the Patient Appointment");

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "','" + Convert.ToString(ViewState["DeptID"]) + "');", true);


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void drpService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkStatus.Checked == true)
                {
                    lblServices.Text = lblServices.Text + "," + drpService.SelectedValue;
                }

                GetDuration();


                string strlTime = "";

                if (lblDuration.Text.Trim() != "")
                {
                    string stFrmTime = txtFromDate.Text + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";

                    DateTime dtStartDate = DateTime.ParseExact(txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

                    // Add 30 minutes
                    dtStartDate = dtStartDate.AddMinutes(Convert.ToInt64(lblDuration.Text));

                    strlTime = dtStartDate.ToString("HH:mm");

                    LoadFinishTime12Hrs(strlTime);

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.drpService_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void drpDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetDepartmentID();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.drpDoctor_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void chkOTAppointment_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOTAppointment.Checked == true)
            {
                divOTAppointment.Visible = true;
            }
            else
            {
                divOTAppointment.Visible = false;
            }

        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            string ServName = "";

            GetServiceMasterName(txtServCode.Text.Trim(), out  ServName);

            txtServName.Text = ServName;


        }

        #endregion

        protected void imgServZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindServiceGrid();
            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMasterPopup()", true);
            divServicePopup.Visible = true;
        }

        protected void ServiceSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");

                drpService.SelectedValue = lblName.Text;
                drpService_SelectedIndexChanged(drpService, new EventArgs());

                divServicePopup.Visible = false;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PaymentSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnMsgClose_Click(object sender, ImageClickEventArgs e)
        {
            divServicePopup.Visible = false;
        }

        protected void btnLabelTreatAppointPrint_Click(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "";
            string AppointmentID = "0";
            if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
            {
                AppointmentID = Convert.ToString(ViewState["AppId"]);
            }
            else
            {
                objCom = new CommonBAL();
                DS = new DataSet();
                Criteria = " HAM_FILENUMBER='" + txtFileNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue(" TOP 1 HAM_APPOINTMENTID", "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria, "HAM_CREATED_DATE DESC");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    AppointmentID = Convert.ToString(DS.Tables[0].Rows[0]["HAM_APPOINTMENTID"]);
                }
            }

            objCom = new CommonBAL();
            DS = new DataSet();
            Criteria = " HIM_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = objCom.fnGetFieldValue(" TOP 1 HIM_INVOICE_ID", "HMS_INVOICE_MASTER", Criteria, "HIM_DATE DESC");
            if (DS.Tables[0].Rows.Count > 0)
            {
                string strInvoiceID = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_ID"]);
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowtLabelTreatAppointPrint('" + AppointmentID + "','" + strInvoiceID + "');", true);
                // ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowtLabelTreatAppointPrint('" + strInvoiceID + "');", true);
            }

        }



        protected void ImageButton1_Click(object sender, EventArgs e)
        {
            divMailPreview.Visible = false;
        }

        protected void imgPTMobileZoom_Click(object sender, ImageClickEventArgs e)
        {
            if (txtMobile.Text.Trim() != "")
            {
                BindPatientDtls("Mobile1");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "PatientPopup1('','');", true);
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }


    }
}