﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Text;
using Mediplus_BAL;
using System.Runtime.InteropServices;
namespace Mediplus.HMS.Billing
{
    public partial class GenerateReSubmission : System.Web.UI.Page
    {
        # region Variable Declaration
        static string stParentName = "0";
        static string strSessionBranchId;

        string strAttachData;

        DataSet DS = new DataSet();
        DataSet DS1 = new DataSet();
        dboperations dbo = new dboperations();



        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ReSubSaveLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../ReSubSaveLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSrcDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcDoctor.DataSource = ds;
                drpSrcDoctor.DataValueField = "HSFM_STAFF_ID";
                drpSrcDoctor.DataTextField = "FullName";
                drpSrcDoctor.DataBind();
            }
            drpSrcDoctor.Items.Insert(0, "--- All ---");
            drpSrcDoctor.Items[0].Value = "0";



        }


        void BindInvoiceGrid()
        {
            lblTotalAmount.Text = "0.00";
            lblTotalPaid.Text = "0.00";
            lblTotalRejected.Text = "0.00";

            lblTotalBalance.Text = "0.00";


            string Criteria = " 1=1 ";
            Criteria += " AND HIM_INVOICE_TYPE = 'Credit'";

            if (chkShowRejectInv.Checked == true)
            {
                Criteria += " AND ( HIM_CLAIM_REJECTED IS NOT NULL AND HIM_CLAIM_REJECTED >0 ) ";//AND HIM_CLAIM_REJECTED >1
            }

            if (chkShowSubmit.Checked == true)
            {
                Criteria += " AND HIM_RESUB_LATEST=1 ";
            }

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }

            if (txtFromTime.Text != "")
            {
                Criteria += " AND HIM_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND    CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101)  <= '" + strForToDate + "'";
            }

            if (txtToTime.Text != "")
            {



                Criteria += " AND HIM_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";
            }




            if (txtCompany.Text != "")
            {
                Criteria += " AND HIM_BILLTOCODE = '" + txtCompany.Text.Trim() + "'";
            }


            if (drpType.SelectedIndex != 0)
            {
                Criteria += " AND HIM_TRTNT_TYPE = '" + drpType.SelectedValue + "'";

            }


            if (drpSrcDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HIM_DR_CODE = '" + drpSrcDoctor.SelectedValue + "'";

            }



            if (txtInvoiceNo.Text != "")
            {
                string strIsuNos = "";
                strIsuNos = txtInvoiceNo.Text.Trim();

                string[] arrIsuNos = strIsuNos.Split(',');

                string strIsuNo = "";
                for (int i = 0; i < arrIsuNos.Length; i++)
                {
                    strIsuNo += "'" + arrIsuNos[i] + "',";
                }
                if (strIsuNo.Length > 1)
                    strIsuNo = strIsuNo.Substring(0, strIsuNo.Length - 1);

                Criteria += " AND HIM_INVOICE_ID IN (" + strIsuNo + ")";
            }
            if (GlobalValues.FileDescription.ToUpper() == "AMAL")
            {
                if (chkShowRejectInv.Checked == true || chkShowSubmit.Checked == true)
                {
                    Criteria += " AND HIM_INVOICE_ID NOT IN ( SELECT HIM_INVOICE_ID FROM HMS_RESUBMISSION_MASTER WHERE ReType='Accepted' ) ";
                }

            }

            // ViewState["Criteria"] = Criteria;
            dbo = new dboperations();
            DS = new DataSet();


            decimal decTotalClaimAmount = 0;
            decimal decTotalBalanceAmount = 0;

            decimal decTotalPaidAmount = 0;
            decimal decTotalRejectedAmount = 0;

            if (chkShowSubmit.Checked == false)
            {
                DS = dbo.InvoiceMasterGet(Criteria);
            }
            else
            {
                DS = dbo.ResubmissionGet(Criteria);
            }

            ViewState["ExportData"] = DS;

            lblTotalClaims.Text = "0";
            gvInvoice.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvoice.Visible = true;
                gvInvoice.DataSource = DS;
                gvInvoice.DataBind();





                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (DS.Tables[0].Rows[i].IsNull("HIM_CLAIM_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIM_CLAIM_AMOUNT"]) != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(DS.Tables[0].Rows[i]["HIM_CLAIM_AMOUNT"]);
                    }
                }



                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (DS.Tables[0].Rows[i].IsNull("HIM_CLAIM_AMOUNT_PAID") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIM_CLAIM_AMOUNT_PAID"]) != "")
                    {
                        decTotalPaidAmount += Convert.ToDecimal(DS.Tables[0].Rows[i]["HIM_CLAIM_AMOUNT_PAID"]);
                    }
                }


                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (DS.Tables[0].Rows[i].IsNull("HIM_CLAIM_REJECTED") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIM_CLAIM_REJECTED"]) != "")
                    {
                        decTotalRejectedAmount += Convert.ToDecimal(DS.Tables[0].Rows[i]["HIM_CLAIM_REJECTED"]);
                    }
                }


                //for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                //{
                //    if (DS.Tables[0].Rows[i].IsNull("BlanceAmt") == false && Convert.ToString(DS.Tables[0].Rows[i]["BlanceAmt"]) != "")
                //    {
                //        decTotalBalanceAmount += Convert.ToDecimal(DS.Tables[0].Rows[i]["BlanceAmt"]);
                //    }
                //}

                lblTotalClaims.Text = Convert.ToString(DS.Tables[0].Rows.Count);

                lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");
                lblTotalPaid.Text = decTotalPaidAmount.ToString("N2");
                lblTotalRejected.Text = decTotalRejectedAmount.ToString("N2");


                decTotalBalanceAmount = decTotalClaimAmount - decTotalPaidAmount;

                lblTotalBalance.Text = decTotalBalanceAmount.ToString("N2");



                hidTotalClaims.Value = Convert.ToString(DS.Tables[0].Rows.Count);
                hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");





            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }





        }

        void ResubmissionGet(string strInvoiceID)
        {
            string Criteria = " 1=1  AND HIM_RESUB_LATEST=1 ";
            Criteria += " AND  HIM_INVOICE_ID='" + strInvoiceID + "'";

            DataSet DS = new DataSet();
            DS = dbo.ResubmissionGet(Criteria);

            ViewState["ReSubNo"] = "";

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["ReSubNo"] = Convert.ToString(DS.Tables[0].Rows[0]["ReSubNo"]);
                txtResubNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["ReSubNo"]);
                // txtIDPayer.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REMARKS"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_AUTHID"]);

                //txtReComment.Text = Convert.ToString(DS.Tables[0].Rows[0]["ReComment"]);
                if (DS.Tables[0].Rows[0].IsNull("ReType") == false && Convert.ToString(DS.Tables[0].Rows[0]["ReType"]) != "")
                {
                    drpReType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["ReType"]);
                }
                lblFileName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_FILE_NAME"]);
                lblFileNameActual.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_FILE_NAME_ACTUAL"]);
                ViewState["AttachmentData"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ATTACHMENT"]);

                if (lblFileNameActual.Text != "")
                {
                    DeleteAttach.Visible = true;
                }
                else
                {
                    DeleteAttach.Visible = false;
                }
            }


        }

        void BindInvoiceTran()
        {
            string Criteria = " 1=1 ";
            //  Criteria += " AND  ReSubNo='" + Convert.ToString(ViewState["ReSubNo"]) + "'";

            DataSet DS = new DataSet();
            //  DS = dbo.ResubmissionTransGet(Criteria);

            //if (DS.Tables[0].Rows.Count <= 0)
            //{

            // Criteria = " 1=1 ";
            //Criteria += " AND  HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";
            //DS = new DataSet();
            //DS = dbo.InvoiceTransGet(Criteria);

            DataColumn TransReComment = new DataColumn();
            //TransReComment.ColumnName = "TransReComment";

            //DataColumn TransReType = new DataColumn();
            //TransReType.ColumnName = "TransReType";

            //DataColumn HIT_RESUBMITTED = new DataColumn();
            //HIT_RESUBMITTED.ColumnName = "HIT_RESUBMITTED";

            //DS.Tables[0].Columns.Add(TransReComment);
            //DS.Tables[0].Columns.Add(TransReType);
            //DS.Tables[0].Columns.Add(HIT_RESUBMITTED);
            //}

            string ReSubNo = "0";
            if (Convert.ToString(ViewState["ReSubNo"]) != "" && Convert.ToString(ViewState["ReSubNo"]) != null)
            {
                ReSubNo = Convert.ToString(ViewState["ReSubNo"]);
            }

            clsInvoice objInv = new clsInvoice();
            Criteria = " 1=1 ";
            Criteria += " AND  HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";
            DS = objInv.InvoiceTransResubGet(Criteria, ReSubNo);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvTran.Visible = true;
                gvInvTran.DataSource = DS;
                gvInvTran.DataBind();

            }
            else
            {
                gvInvTran.DataBind();
            }

        }


        void BindResubTranHistory()
        {
            string Criteria = " 1=1 ";
            //  Criteria += " AND  ReSubNo='" + Convert.ToString(ViewState["ReSubNo"]) + "'";

            DataSet DS = new DataSet();
            //  DS = dbo.ResubmissionTransGet(Criteria);

            //if (DS.Tables[0].Rows.Count <= 0)
            //{

            // Criteria = " 1=1 ";
            //Criteria += " AND  HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";
            //DS = new DataSet();
            //DS = dbo.InvoiceTransGet(Criteria);

            DataColumn TransReComment = new DataColumn();
            //TransReComment.ColumnName = "TransReComment";

            //DataColumn TransReType = new DataColumn();
            //TransReType.ColumnName = "TransReType";

            //DataColumn HIT_RESUBMITTED = new DataColumn();
            //HIT_RESUBMITTED.ColumnName = "HIT_RESUBMITTED";

            //DS.Tables[0].Columns.Add(TransReComment);
            //DS.Tables[0].Columns.Add(TransReType);
            //DS.Tables[0].Columns.Add(HIT_RESUBMITTED);
            //}



            clsInvoice objInv = new clsInvoice();
            Criteria = " 1=1 ";
            Criteria += " AND  HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";
            DS = dbo.ResubmissionTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvResubTransHistory.Visible = true;
                gvResubTransHistory.DataSource = DS;
                gvResubTransHistory.DataBind();

            }
            else
            {
                gvResubTransHistory.DataBind();
            }

        }

        void BindInvoiceClaimDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  HIC_RESUBNO='" + Convert.ToString(ViewState["ReSubNo"]) + "'";


            DataSet DS = new DataSet();
            DS = dbo.ResubmissionClaimDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count <= 0)
            {
                Criteria = " 1=1 ";
                Criteria += " AND  HIC_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";
                DS = new DataSet();
                DS = dbo.InvoiceClaimDtlsGet(Criteria);
            }



            txtPrincipalCode.Text = "";
            txtAdmitCode.Text = "";
            txtS1Code.Text = "";
            txtS2Code.Text = "";
            txtS3Code.Text = "";
            txtS4Code.Text = "";
            txtS5Code.Text = "";
            txtS6Code.Text = "";
            txtS7Code.Text = "";
            txtS8Code.Text = "";
            txtS11Code.Text = "";
            txtS12Code.Text = "";
            txtS13Code.Text = "";
            txtS14Code.Text = "";
            txtS15Code.Text = "";

            txtS1Name.Text = "";
            txtS2Name.Text = "";
            txtS3Name.Text = "";
            txtS4Name.Text = "";
            txtS5Name.Text = "";
            txtS6Name.Text = "";
            txtS7Name.Text = "";
            txtS8Name.Text = "";
            txtS11Name.Text = "";
            txtS12Name.Text = "";
            txtS13Name.Text = "";
            txtS14Name.Text = "";
            txtS15Name.Text = "";

            txtS16Name.Text = "";
            txtS17Name.Text = "";
            txtS18Name.Text = "";
            txtS19Name.Text = "";
            txtS20Name.Text = "";
            txtS21Name.Text = "";
            txtS22Name.Text = "";
            txtS23Name.Text = "";
            txtS24Name.Text = "";
            txtS25Name.Text = "";
            txtS26Name.Text = "";
            txtS27Name.Text = "";
            txtS28Name.Text = "";


            drpClmType.SelectedIndex = 0;
            txtClmStartDate.Text = "";
            txtClmFromTime.Text = "00:00";
            txtClmEndDate.Text = "";
            txtClmToTime.Text = "00:00";
            drpClmStartType.SelectedIndex = 0;
            drpClmEndType.SelectedIndex = 0;

            if (DS.Tables[0].Rows.Count > 0)
            {


                if (DS.Tables[0].Rows[0].IsNull("HIC_TYPE") == false)
                {
                    drpClmType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_TYPE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_STARTDATEDesc") == false)
                {
                    txtClmStartDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTDATEDesc"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HIC_STARTDATE") == false)
                {
                    txtClmFromTime.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTTimeDesc"]);
                    //string strStartDate = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTDATE"]);
                    //string[] arrStartDate = strStartDate.Split(' ');
                    //if (arrStartDate.Length > 1)
                    //{
                    //    string strClmStartTime = arrStartDate[1].ToString();
                    //    string[] arrTime = strClmStartTime.Split(':');
                    //    if (arrTime.Length > 0)
                    //    {
                    //        strClmStartTime = arrTime[0] + ":" + arrTime[1];
                    //        txtClmFromTime.Text = strClmStartTime;
                    //    }
                    //}
                }


                if (DS.Tables[0].Rows[0].IsNull("HIC_ENDDATEDesc") == false)
                {
                    txtClmEndDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATEDesc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ENDDATE") == false)
                {
                    txtClmToTime.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATETimeDesc"]);
                    //    string strClmEndDate = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]);
                    //    string[] arrClmEndDate = strClmEndDate.Split(' ');
                    //    if (arrClmEndDate.Length > 1)
                    //    {
                    //        string strClmEndTime = arrClmEndDate[1].ToString();
                    //        string[] arrETime = strClmEndTime.Split(':');
                    //        if (arrETime.Length > 0)
                    //        {
                    //            strClmEndTime = arrETime[0] + ":" + arrETime[1];
                    //            txtClmToTime.Text = strClmEndTime;
                    //        }
                    //    }
                }




                if (DS.Tables[0].Rows[0].IsNull("HIC_STARTTYPE") == false)
                {
                    drpClmStartType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTTYPE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ENDTYPE") == false)
                {
                    drpClmEndType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDTYPE"]);
                }



                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_P") == false)
                {
                    txtPrincipalCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_P"]);
                    txtPrincipalName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_PDesc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_A") == false)
                {
                    txtAdmitCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_A"]);
                    txtAdmitName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_ADesc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S1") == false)
                {
                    txtS1Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S1"]);
                    txtS1Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S1Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S2") == false)
                {
                    txtS2Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S2"]);
                    txtS2Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S2Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S3") == false)
                {
                    txtS3Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S3"]);
                    txtS3Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S3Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S4") == false)
                {
                    txtS4Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S4"]);
                    txtS4Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S4Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S5") == false)
                {
                    txtS5Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S5"]);
                    txtS5Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S5Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S6") == false)
                {
                    txtS6Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S6"]);
                    txtS6Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S6Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S7") == false)
                {
                    txtS7Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S7"]);
                    txtS7Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S7Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S8") == false)
                {
                    txtS8Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S8"]);
                    txtS8Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S8Desc"]);
                }



                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S11") == false)
                {
                    txtS11Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S11"]);
                    txtS11Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S11Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S12") == false)
                {
                    txtS12Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S12"]);
                    txtS12Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S12Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S13") == false)
                {
                    txtS13Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S13"]);
                    txtS13Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S13Desc"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S14") == false)
                {
                    txtS14Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S14"]);
                    txtS14Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S14Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S15") == false)
                {
                    txtS15Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S15"]);
                    txtS15Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S15Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S16") == false)
                {
                    txtS16Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S16"]);
                    txtS16Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S16Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S17") == false)
                {
                    txtS17Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S17"]);
                    txtS17Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S17Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S18") == false)
                {
                    txtS18Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S18"]);
                    txtS18Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S18Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S19") == false)
                {
                    txtS19Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S19"]);
                    txtS19Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S19Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S20") == false)
                {
                    txtS20Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S20"]);
                    txtS20Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S20Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S21") == false)
                {
                    txtS21Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S21"]);
                    txtS21Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S21Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S22") == false)
                {
                    txtS22Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S22"]);
                    txtS22Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S22Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S23") == false)
                {
                    txtS23Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S23"]);
                    txtS23Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S23Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S24") == false)
                {
                    txtS24Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S24"]);
                    txtS24Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S24Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S25") == false)
                {
                    txtS25Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S25"]);
                    txtS25Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S25Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S26") == false)
                {
                    txtS26Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S26"]);
                    txtS26Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S26Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S27") == false)
                {
                    txtS27Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S27"]);
                    txtS27Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S27Desc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIC_ICD_CODE_S28") == false)
                {
                    txtS28Code.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S28"]);
                    txtS28Name.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ICD_CODE_S28Desc"]);
                }



            }


        }


        void BuildeResubClaimsHistory()
        {

            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn HIC_RESUBNO = new DataColumn();
            HIC_RESUBNO.ColumnName = "HIC_RESUBNO";


            DataColumn HIC_ICD_CODE = new DataColumn();
            HIC_ICD_CODE.ColumnName = "HIC_ICD_CODE";

            DataColumn HIC_ICD_DESC = new DataColumn();
            HIC_ICD_DESC.ColumnName = "HIC_ICD_DESC";


            DataColumn HIC_ICD_TYPE = new DataColumn();
            HIC_ICD_TYPE.ColumnName = "HIC_ICD_TYPE";

            dt.Columns.Add(HIC_RESUBNO);
            dt.Columns.Add(HIC_ICD_CODE);
            dt.Columns.Add(HIC_ICD_DESC);
            dt.Columns.Add(HIC_ICD_TYPE);

            ViewState["ResubClaims"] = dt;
        }

        void BindResubClaimHistory()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND  HIC_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";

            dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ResubmissionClaimDtlsGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["ResubClaims"];

                    DataRow objrow;
                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);
                        objrow["HIC_RESUBNO"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_RESUBNO"]);
                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);

                        objrow["HIC_ICD_TYPE"] = "Principal";


                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HIC_RESUBNO"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_RESUBNO"]);
                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HIC_ICD_TYPE"] = "Admitting";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();
                                //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);
                                objrow["HIC_RESUBNO"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_RESUBNO"]);
                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }




                }

                ViewState["ResubClaims"] = DT;

            }
        }

        void BindResubClaimHistoryGrid()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["ResubClaims"];
            if (DT.Rows.Count > 0)
            {
                gvResubClaimsHistory.DataSource = DT;
                gvResubClaimsHistory.DataBind();

            }
            else
            {
                gvResubClaimsHistory.DataBind();
            }

        }

        void GetReceiptTran()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND HRT_IDPAYER IS NOT NULL AND  HRT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";

            DataSet DS = new DataSet();
            DS = dbo.ReceiptTranGet(Criteria);
            txtIDPayer.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtIDPayer.Text = Convert.ToString(DS.Tables[0].Rows[0]["HRT_IDPAYER"]);
            }


        }

        void BindScreenCustomization()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='ECLAIM' AND SEGMENT='ECLAIM_TYPE'";

            DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "AUH";
                }
            }


        }

        void XMLLogFileWriting(string strContent)
        {
            try
            {
                // string strName = "EClaimErrorLog" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".txt";
                string strName = GlobalValues.Shafafiya_ReSubEClaim_Error_FilePath + Convert.ToString(ViewState["LogFileName"]) + ".txt"; ;

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }
        }



        void BindServiceDtls(string ServId)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HSM_SERV_ID='" + ServId + "'";
            dbo = new dboperations();
            DS = new DataSet();

            DS = dbo.ServiceMasterGet(Criteria);

            int i = Convert.ToInt32(ViewState["TranSelectIndex"]);

            Label lblHIOType, lblDiscType;
            TextBox txtServCode, txtServeDesc, txtFee, txtDiscAmt, txtHitAmount, txtHaadCode, txtHIOCode, txtHIOValue, txtHIOValueType;
            DropDownList drpDiscType;
            txtServCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServCode");
            txtServeDesc = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServeDesc");

            txtHaadCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHaadCode");
            lblHIOType = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblHIOType");
            txtHIOCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOCode");
            txtHIOValue = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValue");
            txtHIOValueType = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValueType");

            txtHitAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHitAmount");
            txtFee = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtFee");
            txtDiscAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDiscAmt");


            // txtCoInsAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtCoInsAmt");

            drpDiscType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpDiscType");
            // drpCoInsType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpCoInsType");

            lblDiscType = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblDiscType");
            //lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");

            lblHIOType.Text = "";
            txtHIOCode.Text = "";
            txtHIOValue.Text = "";
            txtHIOValueType.Text = "";

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);
                txtServeDesc.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_FEE"]);
                txtHitAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_FEE"]);
                txtHaadCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_HAAD_CODE"]);

                txtDiscAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_AMT"]);
                drpDiscType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_TYPE"]);
                lblDiscType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_TYPE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]) == "Y")
                {
                    lblHIOType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_CODE_TYPEDesc"]);
                    txtHIOCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                    txtHIOValue.Text = "";
                    txtHIOValueType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);
                }

            }
        }

        void BindResubType()
        {


            drpReType.Items.Insert(0, "--- Select ---");
            drpReType.Items[0].Value = "";

            drpReType.Items.Insert(1, "Correction");
            drpReType.Items[1].Value = "correction";

            drpReType.Items.Insert(2, "Internal Complaint");
            drpReType.Items[2].Value = "internal complaint";


            if (GlobalValues.FileDescription.ToUpper() == "AMAL")
            {
                drpReType.Items.Insert(3, "Accepted");
                drpReType.Items[3].Value = "Accepted";
            }

        }

        void Clear()
        {
            txtResubNo.Text = "";
            txtIDPayer.Text = "";
            txtPolicyNo.Text = "";
            txtPriorAuthorizationID.Text = "";
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtFromTime.Text = "";
            txtToTime.Text = "";
            txtCompany.Text = "";
            txtCompanyName.Text = "";
            drpType.SelectedIndex = 0;
            txtInvoiceNo.Text = "";
            chkShowSubmit.Checked = false;

            DS = new DataSet();
            Session["Resubmission"] = DS;


            lblTotalClaims.Text = "0";
            lblTotalAmount.Text = "0";

            gvInvoice.Visible = false;
            gvInvoice.DataBind();

            gvInvTran.Visible = false;
            gvInvTran.DataBind();

            gvResubTransHistory.Visible = false;
            gvResubTransHistory.DataBind();

            gvResubClaimsHistory.Visible = false;
            gvResubClaimsHistory.DataBind();

            drpClmType.SelectedIndex = 0;
            txtClmStartDate.Text = "";
            txtClmFromTime.Text = "00:00";
            txtClmEndDate.Text = "";
            txtClmToTime.Text = "00:00";

            ViewState["FileName"] = "";
            ViewState["Criteria"] = "";
            ViewState["SelectIndex"] = 0;

            chkResubVerified.Checked = false;


        }

        void ClearSaveData()
        {

            gvInvTran.Visible = false;
            gvInvTran.DataBind();

            ViewState["SelectIndex"] = 0;
            txtResubNo.Text = "";
            txtIDPayer.Text = "";
            txtPolicyNo.Text = "";
            // txtReComment.Text = "";
            drpReType.SelectedIndex = 0;
            txtPriorAuthorizationID.Text = "";


            txtPrincipalCode.Text = "";
            txtAdmitCode.Text = "";
            txtS1Code.Text = "";
            txtS2Code.Text = "";
            txtS3Code.Text = "";
            txtS4Code.Text = "";
            txtS5Code.Text = "";
            txtS6Code.Text = "";
            txtS7Code.Text = "";
            txtS8Code.Text = "";
            txtS11Code.Text = "";
            txtS12Code.Text = "";
            txtS13Code.Text = "";
            txtS14Code.Text = "";
            txtS15Code.Text = "";

            txtS1Name.Text = "";
            txtS2Name.Text = "";
            txtS3Name.Text = "";
            txtS4Name.Text = "";
            txtS5Name.Text = "";
            txtS6Name.Text = "";
            txtS7Name.Text = "";
            txtS8Name.Text = "";
            txtS11Name.Text = "";
            txtS12Name.Text = "";
            txtS13Name.Text = "";
            txtS14Name.Text = "";
            txtS15Name.Text = "";

            txtS16Name.Text = "";
            txtS17Name.Text = "";
            txtS18Name.Text = "";
            txtS19Name.Text = "";
            txtS20Name.Text = "";
            txtS21Name.Text = "";
            txtS22Name.Text = "";
            txtS23Name.Text = "";
            txtS24Name.Text = "";
            txtS25Name.Text = "";
            txtS26Name.Text = "";
            txtS27Name.Text = "";
            txtS28Name.Text = "";

            ViewState["ReSubNo"] = "";
            ViewState["InvoiceId"] = "";
            drpClmType.SelectedIndex = 0;
            txtClmStartDate.Text = "";
            txtClmFromTime.Text = "00:00";
            txtClmEndDate.Text = "";
            txtClmToTime.Text = "00:00";

            lblFileName.Text = "";
            lblFileNameActual.Text = "";
            ViewState["AttachmentData"] = "";
            DeleteAttach.Visible = false;
            chkResubVerified.Checked = false;
        }

        void GetDataFromXMLFile()
        {
            //if (fileLogo.FileName == "")
            //{
            //    lblStatus.Text = "Please select the file!";
            //    lblStatus.ForeColor = System.Drawing.Color.Red;
            //    goto BindEnd;
            //}

            Boolean IsHeader = false, IsClaim = false, IsEncounter = false, IsDiagnosis = false, IsActivity = false, IsObservation = false, IsContract = false, IsResubmission = false;
            string strName = "";

            //if (Path.GetExtension(fileLogo.FileName.ToLower()) == ".xml")
            //{
            //    strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".XML";
            //    string strPath = Server.MapPath("../Uploads/Resubmission/");
            //    fileLogo.SaveAs(strPath + strName);

            //    DS.ReadXml(Server.MapPath("../Uploads/Resubmission/" + strName));

            //    ViewState["FileName"] = fileLogo.FileName;

            //}
            //else
            //{
            //    //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
            //    lblStatus.Text = "Incorrect File Format!";
            //    lblStatus.ForeColor = System.Drawing.Color.Red;
            //    goto BindEnd;

            //}

            foreach (DataTable table in DS.Tables)
            {
                if (table.TableName == "Header")
                {
                    IsHeader = true;
                }
                if (table.TableName == "Claim")
                {
                    IsClaim = true;
                }
                if (table.TableName == "Encounter")
                {
                    IsEncounter = true;
                }
                if (table.TableName == "Diagnosis")
                {
                    IsDiagnosis = true;
                }
                if (table.TableName == "Activity")
                {
                    IsActivity = true;
                }
                if (table.TableName == "Observation")
                {
                    IsObservation = true;
                }

                if (table.TableName == "Contract")
                {
                    IsContract = true;
                }

                if (table.TableName == "Resubmission")
                {
                    IsResubmission = true;
                }



            }


            if (IsHeader == false || IsClaim == false || IsEncounter == false || IsDiagnosis == false || IsActivity == false)
            {
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }



            string Criteria = " 1=2 ";
            DataSet DSView = new DataSet();
            DSView = dbo.ResubmissionViewGet(Criteria);

            for (int i = 0; i < DS.Tables["Claim"].Rows.Count; i++)
            {
                DataTable DTActivity = new DataTable();
                DataRow[] DRActivity;
                DRActivity = DS.Tables["Activity"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                DataTable DTEncounter = new DataTable();
                DataRow[] DREncounter;
                DREncounter = DS.Tables["Encounter"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                DataTable DTDiagnosis = new DataTable();
                DataRow[] DRDiagnosis;
                DRDiagnosis = DS.Tables["Diagnosis"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());


                DataTable DTContract = new DataTable();
                DataRow[] DRContract = null;
                if (IsContract == true)
                {
                    DRContract = DS.Tables["Contract"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());
                }


                DataTable DTResubmission = new DataTable();
                DataRow[] DRResubmission = null;
                if (IsResubmission == true)
                {
                    DRResubmission = DS.Tables["Resubmission"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());
                }


                for (int j = 0; j < DRActivity.Length; j++)
                {
                    DataTable DTObservation = new DataTable();
                    DataRow[] DRObservation;



                    DataRow objrow;
                    objrow = DSView.Tables[0].NewRow();

                    objrow["CompId"] = DS.Tables["Header"].Rows[0]["ReceiverID"].ToString();
                    objrow["ID"] = DS.Tables["Claim"].Rows[i]["ID"].ToString();
                    objrow["IDPayer"] = DS.Tables["Claim"].Rows[i]["IDPayer"].ToString();
                    objrow["MemberID"] = DS.Tables["Claim"].Rows[i]["MemberID"].ToString();
                    objrow["PayerID"] = DS.Tables["Claim"].Rows[i]["PayerID"].ToString();
                    objrow["ProviderID"] = DS.Tables["Claim"].Rows[i]["ProviderID"].ToString();

                    string EmiratesID = "";
                    if (DS.Tables["Claim"].Rows[i].IsNull("EmiratesIDNumber") == true || Convert.ToString(DS.Tables["Claim"].Rows[i]["EmiratesIDNumber"]) == "" || Convert.ToString(DS.Tables["Claim"].Rows[i]["EmiratesIDNumber"]).ToUpper() == "TEMP")
                    {
                        EmiratesID = "000-0000-0000000-0";
                    }
                    else
                    {
                        EmiratesID = Convert.ToString(DS.Tables["Claim"].Rows[i]["EmiratesIDNumber"]);
                    }

                    objrow["EmiratesIDNumber"] = EmiratesID;
                    objrow["Gross"] = DS.Tables["Claim"].Rows[i]["Gross"].ToString();
                    objrow["PatientShare"] = DS.Tables["Claim"].Rows[i]["PatientShare"].ToString();
                    objrow["Net"] = DS.Tables["Claim"].Rows[i]["Net"].ToString();




                    objrow["Type"] = DS.Tables["Encounter"].Rows[0]["Type"].ToString();
                    objrow["PatientID"] = DS.Tables["Encounter"].Rows[0]["PatientID"].ToString();
                    objrow["Start"] = DS.Tables["Encounter"].Rows[0]["Start"].ToString();
                    objrow["End"] = DS.Tables["Encounter"].Rows[0]["End"].ToString();
                    objrow["StartType"] = DS.Tables["Encounter"].Rows[0]["StartType"].ToString();
                    objrow["EndType"] = DS.Tables["Encounter"].Rows[0]["EndType"].ToString();


                    for (int k = 0; k < DRDiagnosis.Length; k++)
                    {
                        if (DS.Tables["Diagnosis"].Rows[k]["Type"].ToString() == "Principal")
                        {
                            objrow["Type1"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                            objrow["Code"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();
                        }

                        else if (DS.Tables["Diagnosis"].Rows[k]["Type"].ToString() == "Admitting")
                        {
                            objrow["ICDA"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                            objrow["ICDCodeA"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();
                        }
                        else
                        {

                            switch (k)
                            {
                                case 1:

                                    objrow["Type3"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["Code3"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();

                                    break;
                                case 2:

                                    objrow["ICDS2"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS2"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 3:

                                    objrow["ICDS3"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS3"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 4:

                                    objrow["ICDS4"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS4"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 5:

                                    objrow["ICDS5"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS5"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 6:

                                    objrow["ICDS6"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS6"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 7:

                                    objrow["ICDS7"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS7"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 8:

                                    objrow["ICDS8"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS8"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 9:

                                    objrow["ICDS9"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS9"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                case 10:

                                    objrow["ICDS10"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS10"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();

                                    break;
                                case 11:

                                    objrow["ICDS11"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                                    objrow["ICDCodeS11"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();


                                    break;
                                default:
                                    break;

                            }

                        }




                    }



                    objrow["ID"] = DRActivity[j]["ID"].ToString();
                    objrow["Start1"] = Convert.ToDateTime(Convert.ToString(DS.Tables["Encounter"].Rows[0]["Start"])).ToString("dd/MM/yyyy HH:mm");
                    objrow["Type2"] = Convert.ToString(DRActivity[j]["Type"]);
                    objrow["Code2"] = Convert.ToString(DRActivity[j]["Code"]);
                    objrow["Quantity"] = Convert.ToString(DRActivity[j]["Quantity"]);
                    objrow["Net1"] = Convert.ToString(DRActivity[j]["Net"]);
                    objrow["OrderingClinician"] = Convert.ToString(DRActivity[j]["OrderingClinician"]);
                    objrow["Clinician"] = Convert.ToString(DRActivity[j]["Clinician"]);

                    objrow["InvoiceID"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]);
                    // objrow["InvDate"] = DRActivity[j][0].ToString();
                    objrow["InvType"] = "Credit";
                    objrow["HIT_SERV_CODE"] = DRActivity[j][2].ToString();

                    if (IsObservation == true)
                    {
                        DRObservation = DS.Tables["Observation"].Select("Activity_Id=" + DS.Tables["Claim"].Rows[i]["ID"].ToString());
                        if (DRObservation.Length > 0)
                        {
                            if (DRActivity.Length > 7)
                            {
                                if (DRActivity[j][6].ToString() != "")
                                {
                                    objrow["PriorAuthorizationId"] = DRActivity[j][6].ToString();
                                }

                                objrow["ObservType"] = DRObservation[0][0].ToString();
                                objrow["ObservCode"] = DRObservation[0][1].ToString();
                                objrow["ObservValue"] = DRObservation[0][2].ToString();
                                objrow["ObservValueType"] = DRObservation[0][3].ToString();

                            }


                        }

                    }

                    if (IsContract == true)
                    {
                        if (DRContract.Length > 0)
                        {
                            objrow["PackageName"] = DRContract[0][0].ToString();
                        }
                    }

                    if (IsResubmission == true)
                    {
                        if (DRResubmission.Length > 0)
                        {
                            objrow["ReType"] = DRResubmission[0][0].ToString();
                            objrow["ReComment"] = DRResubmission[0][1].ToString();
                            objrow["ReSubNo"] = DRResubmission[0][2].ToString();
                        }
                    }




                    DSView.Tables[0].Rows.Add(objrow);
                }

            }


            File.Delete(Server.MapPath("../Uploads/Resubmission/" + strName));
            Session["Resubmission"] = DSView;

            lblTotalClaims.Text = "0";



        BindEnd: ;

        }


        void XMLFileWrite(string DispositionFlag)
        {

            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strPath = GlobalValues.Shafafiya_ReSubEClaim_FilePath;

            string Criteria = " 1=1 ";
            Criteria += " AND HMS_RESUBMISSION_MASTER.ReType <> 'Accepted'";
            string strInvoiceID = "";

            for (int intCount = 0; intCount < gvInvoice.Rows.Count; intCount++)
            {

                CheckBox chkInvResub;
                chkInvResub = (CheckBox)gvInvoice.Rows[intCount].FindControl("chkInvResub");

                Label lblInvoiceId;
                lblInvoiceId = (Label)gvInvoice.Rows[intCount].FindControl("lblInvoiceId");

                if (chkInvResub.Checked == true)
                {
                    strInvoiceID += "'" + lblInvoiceId.Text + "',";
                }


            }

            if (strInvoiceID.Length > 0)
                strInvoiceID = strInvoiceID.Substring(0, strInvoiceID.Length - 1);

            if (strInvoiceID != "")
            {
                Criteria += " AND HIM_INVOICE_ID in( " + strInvoiceID + ")";
            }
            else
            {
                lblStatus.Text = "Please select the invoice ";
                lblStatus.ForeColor = System.Drawing.Color.Red;

                goto ClaimEnd;
            }

            // Criteria = ViewState["Criteria"].ToString();
            dbo = new dboperations();
            DS1 = new DataSet();
            DS1 = dbo.GetResubmissionViewGroupBy(Criteria);


            if (DS1.Tables[0].Rows.Count <= 0)
            {
                lblStatus.Text = "No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto ClaimEnd;
            }
            Boolean IsError = false;
            ViewState["LogFileName"] = "ReSubEClaimErrorLog" + strName + ".txt";


            StringBuilder strData = new StringBuilder();

            XmlDocument XD = new XmlDocument();
            XD.CreateXmlDeclaration("1.0", "utf-8", "yes");
            XD.CreateProcessingInstruction("xml", "version='1.0' encoding='utf-8'");

            string strNewLineChar = "\n";

            Int32 ActCount = 0;

            strData.Append("<?xml version='1.0' encoding='US-ASCII'?>" + strNewLineChar);


            if (ViewState["CUST_VALUE"].ToString() == "DXB")
            {
                strData.Append("<Claim.Submission xmlns:tns='http://www.eclaimlink.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='http://www.eclaimlink.ae/DataDictionary/CommonTypes/DataDictionary.xsd'>" + strNewLineChar);
            }
            else
            {
                //strData.Append("<Claim.Submission xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/DataDictionary.xsd'>");
                strData.Append("<Claim.Submission xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/ClaimSubmission.xsd' xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" + strNewLineChar);


            }


            // if (ViewState["CUST_VALUE"].ToString() == "DXB")
            //{

            //    strData.Append("<Remittance.Advice xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/DataDictionary.xsd' xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' >");
            //}
            //else
            //{
            //    strData.Append("<Remittance.Advice xsi:noNamespaceSchemaLocation='http://www.haad.ae/DataDictionary/CommonTypes/DataDictionary.xsd' xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' >");
            //}

            strData.Append("<Header>" + strNewLineChar);

            strData.Append("<SenderID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim()  + "</SenderID>" + strNewLineChar);//GlobalValues.FacilityID
            strData.Append("<ReceiverID>" + Convert.ToString(DS1.Tables[0].Rows[0]["ReceiverID"]) + "</ReceiverID>" + strNewLineChar);
            strData.Append("<TransactionDate>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "</TransactionDate>" + strNewLineChar);
            strData.Append("<RecordCount>" + DS1.Tables[0].Rows.Count.ToString() + "</RecordCount>" + strNewLineChar);
            strData.Append("<DispositionFlag>" + DispositionFlag + "</DispositionFlag>" + strNewLineChar);
            strData.Append("</Header>" + strNewLineChar);


            for (Int32 i = 0; i < DS1.Tables[0].Rows.Count; i++)
            {

                strData.Append("<Claim>" + strNewLineChar);
                //if (DS.Tables[0].Rows[0].IsNull("MemberID") == true)
                //{
                //    IsError = true;
                //    TextFileWriting("Invalid or blank MemberID");
                //    // goto ClaimEnd;
                //}

                strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + "</ID>" + strNewLineChar);
                strData.Append("<IDPayer>" + Convert.ToString(DS1.Tables[0].Rows[i]["IDPayer"]) + "</IDPayer>" + strNewLineChar);
                strData.Append("<MemberID>" + Convert.ToString(DS1.Tables[0].Rows[i]["MemberID"]) + "</MemberID>" + strNewLineChar);
                strData.Append("<PayerID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PayerID"]) + "</PayerID>" + strNewLineChar);
                strData.Append("<ProviderID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim()  + "</ProviderID>" + strNewLineChar) ; //GlobalValues.FacilityID
                if (DS1.Tables[0].Rows[i].IsNull("EmiratesIDNumber") == false && Convert.ToString(DS1.Tables[0].Rows[i]["EmiratesIDNumber"]) != "")
                {
                    strData.Append("<EmiratesIDNumber>" + Convert.ToString(DS1.Tables[0].Rows[i]["EmiratesIDNumber"]) + "</EmiratesIDNumber>" + strNewLineChar);
                }
                else
                {
                    strData.Append("<EmiratesIDNumber>000-0000-0000000-0</EmiratesIDNumber>" + strNewLineChar);
                }
                strData.Append("<Gross>" + Convert.ToString(DS1.Tables[0].Rows[i]["Gross"]) + "</Gross>" + strNewLineChar);
                strData.Append("<PatientShare>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientShare"]) + "</PatientShare>" + strNewLineChar);
                strData.Append("<Net>" + Convert.ToString(DS1.Tables[0].Rows[i]["Net"]) + "</Net>" + strNewLineChar);

                strData.Append("<Encounter>");
                strData.Append("<FacilityID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</FacilityID>" + strNewLineChar); //GlobalValues.FacilityID
                strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type"]) + "</Type>" + strNewLineChar);
                strData.Append("<PatientID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientID"]) + "</PatientID>" + strNewLineChar);
                if (DS1.Tables[0].Rows[i].IsNull("Start") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Start"]) != "")
                {
                    strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["Start"])).ToString("dd/MM/yyyy HH:mm") + "</Start>" + strNewLineChar);
                }
                else
                {
                    strData.Append("<Start></Start>" + strNewLineChar);
                }
                if (DS1.Tables[0].Rows[i].IsNull("End") == false && Convert.ToString(DS1.Tables[0].Rows[i]["End"]) != "")
                {
                    strData.Append("<End>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["End"])).ToString("dd/MM/yyyy HH:mm") + "</End>" + strNewLineChar);
                }
                else
                {
                    strData.Append("<End></End>" + strNewLineChar);
                }
                strData.Append("<StartType>" + Convert.ToString(DS1.Tables[0].Rows[i]["StartType"]) + "</StartType>" + strNewLineChar);
                strData.Append("<EndType>" + Convert.ToString(DS1.Tables[0].Rows[i]["EndType"]) + "</EndType>" + strNewLineChar);
                strData.Append("</Encounter>" + strNewLineChar);



                Boolean isDiagnosis = false;

                if (DS1.Tables[0].Rows[i].IsNull("Type1") == false && DS1.Tables[0].Rows[i].IsNull("Code") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Code"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type1"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code"]) + "</Code>" + strNewLineChar);
                   
                    if (ViewState["CUST_VALUE"].ToString() != "DXB")
                    {
                        if (DS1.Tables[0].Rows[i].IsNull("HIC_YEAR_OF_ONSET_P") == false && DS1.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"].ToString() != "")
                        {
                            strData.Append("<DxInfo>");
                            strData.Append("<Type>Year of onset</Type>");
                            strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"]) + "</Code>");
                            strData.Append("</DxInfo>");
                        }
                    }


                    strData.Append("</Diagnosis>" + strNewLineChar);

                }



                if (DS1.Tables[0].Rows[i].IsNull("Type3") == false && DS1.Tables[0].Rows[i].IsNull("Code3") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Code3"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type3"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code3"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS2") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS2") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS2"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS2"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS2"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS3") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS3") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS3"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS3"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS3"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS4") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS4") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS4"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS4"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS4"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS5") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS5") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS5"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS5"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS5"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS6") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS6") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS6"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS6"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS6"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS7") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS7") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS7"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS7"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS7"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS8") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS8") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS8"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS8"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS8"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS9") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS9") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS9"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS9"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS9"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS10") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS10") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS10"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS10"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS10"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS11") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS11") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS11"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS11"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS11"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS12") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS12") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS12"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS12"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS12"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS13") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS13") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS13"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS13"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS13"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS14") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS14") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS14"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS14"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS14"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS15") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS15") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS15"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS15"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS15"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS16") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS16") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS16"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS16"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS16"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS17") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS17") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS17"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS17"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS17"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS18") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS18") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS18"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS18"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS18"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS19") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS19") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS19"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS19"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS19"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS20") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS20") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS20"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS20"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS20"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS21") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS21") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS21"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS21"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS21"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS22") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS22") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS22"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS22"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS22"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS23") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS23") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS23"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS23"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS23"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS24") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS24") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS24"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS24"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS24"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS25") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS25") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS25"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS25"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS25"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS26") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS26") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS26"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS26"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS26"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDA") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeA") == false && Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeA"]) != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDA"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeA"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>");

                }

                if (isDiagnosis == false)
                {
                    IsError = true;
                    XMLLogFileWriting("Invalid or blank Diagnosis  (InvoiceNo=" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + ")");
                    // goto ClaimEnd;
                }


                //   ResubmissionGet(Convert.ToString( DS1.Tables[0].Rows[i]["ID"])) ;
                //Criteria = ViewState["Criteria"].ToString();

                Criteria = " 1=1 ";
                Criteria += " AND HMS_RESUBMISSION_MASTER.ReType <> 'Accepted'";
                // Criteria += " AND HIM_INVOICE_ID=" + DS1.Tables[0].Rows[i]["ID"].ToString();
                // Criteria += " AND HMS_RESUBMISSION_MASTER.ReSubNo='" + Convert.ToString(ViewState["ReSubNo"]) + "'";
                Criteria += " AND HMS_RESUBMISSION_MASTER.ReSubNo='" + Convert.ToString(DS1.Tables[0].Rows[i]["ReSubNo"]) + "'";

                dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.ResubmissionViewGet(Criteria);


                //DataTable DTActivity = new DataTable();
                //DataRow[] DRActivity;
                //DRActivity = DS1.Tables[0].Select("ID=" + DS.Tables[0].Rows[i]["ID"].ToString());

                string strResubComment = "";

                for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_RESUBMITTED"]) == "True")
                    {
                        ActCount = ActCount + 1;

                        if (DS.Tables[0].Rows[j].IsNull("Clinician") == true)
                        {
                            IsError = true;
                            XMLLogFileWriting("Invalid or blank Clinician  (InvoiceNo=" + Convert.ToString(DS.Tables[0].Rows[j]["ID"]) + ")");
                            // goto ClaimEnd;
                        }

                        if (DS.Tables[0].Rows[j].IsNull("Code2") == true)
                        {
                            IsError = true;
                            XMLLogFileWriting("Invalid or blank  Activity Code (InvoiceNo=" + Convert.ToString(DS.Tables[0].Rows[j]["ID"]) + ")");
                            // goto ClaimEnd;
                        }

                        strData.Append("<Activity>" + strNewLineChar);

                        if (Convert.ToString(DS1.Tables[0].Rows[i]["ReType"]) == "internal complaint")
                        {
                            strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + Convert.ToString(DS.Tables[0].Rows[j]["HIT_SLNO"]) + "</ID>" + strNewLineChar);
                        }
                        else if (Convert.ToString(DS1.Tables[0].Rows[i]["ReType"]) == "correction")
                        {
                            strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ReSubNo"]) + Convert.ToString(DS.Tables[0].Rows[j]["HIT_SLNO"]) + "</ID>" + strNewLineChar);
                        }


                        //strData.Append("<ID>" + ActCount + "</ID>" + strNewLineChar);

                        if (DS1.Tables[0].Rows[i].IsNull("Start") == false && Convert.ToString(DS1.Tables[0].Rows[i]["Start"]) != "")
                        {
                            // strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS.Tables[0].Rows[j]["Start1"])).ToString("dd/MM/yyyy HH:mm") + "</Start>");
                            strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["Start"])).ToString("dd/MM/yyyy HH:mm") + "</Start>" + strNewLineChar);
                        }
                        else
                        {
                            strData.Append("<Start></Start>" + strNewLineChar);
                        }
                        strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["Type2"]) + "</Type>" + strNewLineChar);
                        strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["Code2"]) + "</Code>" + strNewLineChar);
                        strData.Append("<Quantity>" + Convert.ToString(DS.Tables[0].Rows[j]["Quantity"]) + "</Quantity>" + strNewLineChar);
                        strData.Append("<Net>" + Convert.ToString(DS.Tables[0].Rows[j]["Net1"]) + "</Net>" + strNewLineChar);
                        //strData.Append("<PatientShare>" + Convert.ToString(DS.Tables[0].Rows[j]["PatientShare"]) + "</PatientShare>" + strNewLineChar);
                        if (ViewState["CUST_VALUE"].ToString() != "DXB")
                        {
                            if (GlobalValues.FileDescription == "SMCH")
                            {
                                strData.Append("<OrderingClinician>" + DS1.Tables[0].Rows[i]["OrderingClinician"].ToString() + "</OrderingClinician>" + strNewLineChar);
                            }
                            else
                            {
                                strData.Append("<OrderingClinician>" + DS.Tables[0].Rows[j]["OrderingClinician"].ToString() + "</OrderingClinician>" + strNewLineChar);
                            }

                        }
                        strData.Append("<Clinician>" + Convert.ToString(DS.Tables[0].Rows[j]["Clinician"]) + "</Clinician>" + strNewLineChar);





                        if (DS.Tables[0].Rows[j].IsNull("PriorAuthorizationId") == false && Convert.ToString(DS.Tables[0].Rows[j]["PriorAuthorizationId"]) != "")
                        {
                            strData.Append("<PriorAuthorizationID>" + Convert.ToString(DS.Tables[0].Rows[j]["PriorAuthorizationId"]) + "</PriorAuthorizationID>" + strNewLineChar);
                        }
                        else
                        {
                            strData.Append("<PriorAuthorizationID></PriorAuthorizationID>" + strNewLineChar);
                        }

                        if (DS.Tables[0].Rows[j].IsNull("ObservType") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) != "")
                        {
                            bool bolObservDone = false;
                            if (ViewState["CUST_VALUE"].ToString() == "DXB")
                            {
                                string strObsValue = Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]);
                                string[] arrObsValue = strObsValue.Split('/');
                                if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80061" && arrObsValue.Length > 2)
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>CHOL</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);

                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>HDL</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);

                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>LDL</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);

                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>TRG</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);

                                }


                                string[] arrBPObsValue = strObsValue.Split('/');
                                if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_SERV_TYPE"]).Trim() == "C" && arrBPObsValue.Length > 1)
                                {
                                    bolObservDone = true;
                                    strData.Append("<Observation>");

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>BS</Code>");
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrBPObsValue[0] + "</Value>");
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>");
                                    }


                                    strData.Append("</Observation>");

                                    strData.Append("<Observation>");

                                    strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>BD</Code>");
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + arrBPObsValue[1] + "</Value>");
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>");
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>");
                                    }


                                    strData.Append("</Observation>");

                                }


                            }
                            else
                            {

                            }

                            string strObservCode = Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]);
                            string[] arrObservCode = strObservCode.Split(',');

                            if (arrObservCode.Length > 1)
                            {

                                for (Int32 intCode = 0; intCode < arrObservCode.Length; intCode++)
                                {

                                    strData.Append("<Observation>" + strNewLineChar);

                                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                    if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                    {
                                        strData.Append("<Code>" + arrObservCode[intCode] + "</Code>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Code></Code>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                    {
                                        strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<Value></Value>" + strNewLineChar);
                                    }
                                    if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                    {
                                        strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                    }
                                    else
                                    {
                                        strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                    }

                                    strData.Append("</Observation>" + strNewLineChar);
                                }
                            }
                            else if (bolObservDone == false)
                            {
                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]) + "</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }

                                strData.Append("</Observation>" + strNewLineChar);
                            }

                        }
                        if (Convert.ToString(DS.Tables[0].Rows[j]["TransReComment"]) != "")
                        {
                            strResubComment += Convert.ToString(DS.Tables[0].Rows[j]["Code2"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["TransReComment"]) + strNewLineChar;
                        }
                        strData.Append("</Activity>" + strNewLineChar);

                    }


                }

                strData.Append("<Resubmission>" + strNewLineChar);
                strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ReType"]) + "</Type>" + strNewLineChar);
                strData.Append("<Comment>" + strResubComment.Replace("&", "") + "</Comment>" + strNewLineChar);

                DataSet DSAttach = new DataSet();
                string CriteriaAttach = " 1=1 ";
                CriteriaAttach += " AND HMS_RESUBMISSION_MASTER.ReSubNo='" + Convert.ToString(DS1.Tables[0].Rows[i]["ReSubNo"]) + "'";
                DSAttach = dbo.ResubmissionGet(CriteriaAttach);
                if (DSAttach.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(DSAttach.Tables[0].Rows[0]["HIM_ATTACHMENT"]) != "")
                    {
                        strData.Append("<Attachment>" + Convert.ToString(DSAttach.Tables[0].Rows[0]["HIM_ATTACHMENT"]) + "</Attachment>" + strNewLineChar);
                    }

                }


                strData.Append("</Resubmission>" + strNewLineChar);


                //strData.Append("<Contract>" + strNewLineChar);
                //strData.Append("<PackageName>" + Convert.ToString(DS1.Tables[0].Rows[i]["PackageName"]) + "</PackageName>" + strNewLineChar);
                //strData.Append("</Contract>" + strNewLineChar);



                strData.Append("</Claim>" + strNewLineChar);
            }
            strData.Append("</Claim.Submission>" + strNewLineChar);

            if (IsError == false)
            {
                string strFileName = strPath + "ReSubEClaim" + strName + ".XML";

                StreamWriter oWrite;
                oWrite = File.CreateText(strFileName);
                oWrite.WriteLine(strData);

                oWrite.Close();
                lblStatus.Text = "ReSubmission E-Claim Created - File Name is ReSubEClaim" + strName + ".XML";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('E-Claim Not Created, Check the Log File !');", true);
                lblStatus.Text = "ReSubmission E-Claim Not Created, Check the Log File  - File Name is  ReSubEClaimErrorLog" + strName + ".txt";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        ClaimEnd: ;

        }

        DataSet GetServiceMaster(string ServCode)
        {

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            return DS;

        }


        Boolean TransferResubValidation()
        {
            string Criteria = "";

            dboperations dbo = new dboperations();
            DataSet DSInv = new DataSet();
            Criteria = " HIM_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "' AND  ResubNo='" + txtResubNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DSInv = dbo.ResubmissionGet(Criteria);

            if (DSInv.Tables[0].Rows.Count <= 0)
            {
                lblStatus.Text = "Invoice Dtls not available ";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return false;
            }


            if (DSInv.Tables[0].Rows.Count > 0)
            {

                ////if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]).ToUpper() == "CASH" || Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]).ToUpper() == "CA")
                ////{
                ////    lblStatus.Text = " Cash Invoice can not Transfer";
                ////    lblStatus.ForeColor = System.Drawing.Color.Red;
                ////    return false;
                ////}


                if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_RESUB_VERIFY_STATUS"]) == "Completed")
                {
                    lblStatus.Text = " Already Transfered";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    return false;
                }

                ////Decimal decCompAmount = 0;

                ////if (DSInv.Tables[0].Rows[0].IsNull("HIM_CLAIM_AMOUNT") == false && Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]) != "")
                ////{

                ////    decCompAmount = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                ////}

                ////if (decCompAmount <= 0)
                ////{
                ////    lblStatus.Text = " Claim Amount should grater than zero ";
                ////    lblStatus.ForeColor = System.Drawing.Color.Red;
                ////    return false;
                ////}
            }





            if (txtClmStartDate.Text.Trim() == "" || txtClmFromTime.Text.Trim() == "")
            {
                lblStatus.Text = "Encounter Start Date & Time is empty ";

                return false;
            }

            if (txtClmEndDate.Text.Trim() == "" || txtClmToTime.Text.Trim() == "")
            {
                lblStatus.Text = "Encounter End Date & Time is empty";

                return false;
            }

            if (gvInvTran.Rows.Count <= 0)
            {
                lblStatus.Text = "Invoice Services is empty";

                return false;
            }


            if (txtPrincipalCode.Text.Trim() == "")
            {
                lblStatus.Text = "Principal Diagnosis is empty ";

                return false;
            }

            /*
            string strDiagCodes = "";
            if (gvResubClaims.Rows.Count > 0)
            {
                for (int intCurRow = 0; intCurRow < gvResubClaims.Rows.Count; intCurRow++)
                {
                    Label lblResubICDCode = (Label)gvResubClaims.Rows[intCurRow].FindControl("lblResubICDCode");

                    if (lblResubICDCode.Text == "")
                    {
                        lblStatus.Text = "Please Enter Diagnosis Code";

                        return false;
                    }

                    if (strDiagCodes.Contains(lblResubICDCode.Text.Trim() + "|") == true)
                    {
                        lblStatus.Text = "Duplicate Diagnosis Code -" + lblResubICDCode.Text.Trim();

                        return false;
                    }

                    if (strDiagCodes != "")
                    {
                        strDiagCodes = strDiagCodes + lblResubICDCode.Text.Trim() + "|";
                    }
                    else
                    {
                        strDiagCodes = lblResubICDCode.Text.Trim() + "|";
                    }



                }
            }

            */


            for (int intCurRow = 0; intCurRow < gvInvTran.Rows.Count; intCurRow++)
            {
                Label lblServCode = (Label)gvInvTran.Rows[intCurRow].FindControl("lblServCode");
                TextBox txtServeDesc = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtServeDesc");

                TextBox txtStartDt = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtStartDt");

                Label lblHIOType = (Label)gvInvTran.Rows[intCurRow].FindControl("lblHIOType");
                TextBox txtHIOCode = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtHIOCode");

                TextBox txtHIOValue = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtHIOValue");
                TextBox txtHIOValueType = (TextBox)gvInvTran.Rows[intCurRow].FindControl("txtHIOValueType");

                DataSet DSServ = new DataSet();

                DSServ = GetServiceMaster(lblServCode.Text);

                String ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "", CodeType = "";

                decimal ObsConversion;
                if (DSServ.Tables[0].Rows.Count > 0)
                {



                    if (lblServCode.Text == "")
                    {
                        lblStatus.Text = "Please Enter Service Code";

                        return false;
                    }

                    if (lblServCode.Text == "")
                    {
                        lblStatus.Text = "Please Enter Service Code";

                        return false;
                    }


                    CodeType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);


                    ObsNeeded = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
                    if (ObsNeeded == "Y")
                    {
                        ObsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);//txtObsCode.Text =
                        ObsValueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                        ObsConversion = DSServ.Tables[0].Rows[0].IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]);

                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "DXB" && CodeType == "3")// && ServTrnt == "L")
                        {
                            ObsType = "Result";
                        }
                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "AUH" && CodeType == "3")// && ServTrnt == "L")
                        {

                            ObsType = "LOINC";

                        }
                        else if (CodeType == "6")
                        {
                            ObsType = "Universal Dental";
                        }


                        lblHIOType.Text = ObsType;
                        txtHIOCode.Text = ObsCode;


                    }
                }


                if (ObsNeeded.ToUpper() == "Y")    /// if (txtObsType.Text.Trim() != "")
                {
                    if (txtHIOValue.Text.Trim() == "")
                    {
                        lblStatus.Text = "Please Enter Observation Dtls for " + lblServCode.Text;

                        return false;

                    }

                }

                if (txtStartDt.Text == "")
                {
                    lblStatus.Text = "Please Enter Service Start Date";

                    return false;
                }



                if (txtClmStartDate.Text.Trim() != "" && txtClmFromTime.Text.Trim() != "")
                {

                    DateTime dtEncStartDate = DateTime.ParseExact(txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);//:ss

                    DateTime dtEncEndDate = DateTime.ParseExact(txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);//:ss

                    DateTime dtServStartDate = DateTime.ParseExact(txtStartDt.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);//



                    TimeSpan Diff = dtServStartDate - dtEncStartDate;

                    if (Diff.TotalMinutes < 0)
                    {
                        lblStatus.Text = "Service Date Between Encounter Start Date and End Date";

                        return false;
                    }

                    TimeSpan Diff1 = dtServStartDate - dtEncEndDate;

                    if (Diff1.TotalMinutes > 0)
                    {
                        lblStatus.Text = "Service Date Between Encounter Start Date and End Date";

                        return false;
                    }


                }
            }


            return true;
        }
        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='ECLARESUB' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnClear.Enabled = false;
                btnShow.Enabled = false;
                filAttachment.Enabled = false;
                btnTestEclaim.Enabled = false;
                btnProductionEclaim.Enabled = false;
                btnUpdate.Enabled = false;
                btnSaveToDB.Enabled = false;


            }


            if (strPermission == "7")
            {

                btnClear.Enabled = false;
                btnShow.Enabled = false;
                filAttachment.Enabled = false;
                btnTestEclaim.Enabled = false;
                btnProductionEclaim.Enabled = false;
                btnUpdate.Enabled = false;
                btnSaveToDB.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");

            }
        }


        #endregion



        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_COMP_ID Like '" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetICDCode(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";


            Criteria += " AND DIM_ICD_ID Like '" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.DrICDMasterGet(Criteria, "20");
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["DIM_ICD_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetICDName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";


            Criteria += " AND DIM_ICD_DESCRIPTION Like '" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.DrICDMasterGet(Criteria, "20");
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["DIM_ICD_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetSerCode(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";


            Criteria += " AND HSM_SERV_ID Like '" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.ServiceMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSM_SERV_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HSM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetSerName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";


            Criteria += " AND HSM_NAME Like '" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.ServiceMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSM_NAME"].ToString() + "~" + ds.Tables[0].Rows[i]["HSM_SERV_ID"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        void CountClaimAmount()
        {
            decimal decTotalClaimAmount = 0;
            Int32 intTotalCount = 0;

            for (int i = 0; i < gvInvoice.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvInvoice.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblgvClaimAmount = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvClaimAmount");

                if (chkInvResub.Checked == true)
                {
                    intTotalCount += 1;

                    if (lblgvClaimAmount.Text != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(lblgvClaimAmount.Text);

                    }
                }

            }


            lblTotalClaims.Text = Convert.ToString(intTotalCount); ;
            lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

            hidTotalClaims.Value = Convert.ToString(intTotalCount);
            hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");
        }
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {

                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();
                try
                {


                    //txtFromDate.Text = "01/08/2013";
                    //txtToDate.Text = "31/08/2013";
                    //txtCompany.Text = "I00006"; ;
                    //txtCompanyName.Text = "ADNIC";

                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {
                        btnExport.Visible = true;
                    }
                    if (GlobalValues.FileDescription == "SMCH")
                    {
                        btnDispense.Visible = true;
                    }

                    DateTime strTodayDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //txtClmStartDate.Text = strTodayDate.ToString("dd/MM/yyyy");
                    //txtClmEndDate.Text = strTodayDate.ToString("dd/MM/yyyy");

                    ViewState["SelectIndex"] = 0;
                    Session["Resubmission"] = "";
                    ViewState["FileName"] = "";
                    ViewState["Criteria"] = "";
                    stParentName = "0";
                    //  btnUpdate.Visible = false;
                    BindSrcDoctor();
                    BindResubType();

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    BindScreenCustomization();

                    // BindServiceGrid();
                    //gvService.Visible = false;

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_btnbtnShow_Click");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                ClearSaveData();
                gvInvoice.Visible = false;
                gvInvoice.DataBind();

                gvInvTran.Visible = false;
                gvInvTran.DataBind();

                BindInvoiceGrid();
            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_btnbtnShow_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnTestEclaim_Click(object sender, EventArgs e)
        {
            Boolean boolIsChecked = false;
            decimal decTotalClaimAmount = 0;
            Int32 intTotalCount = 0;

            for (int i = 0; i < gvInvoice.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvInvoice.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblgvClaimAmount = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvClaimAmount");

                if (chkInvResub.Checked == true)
                {
                    intTotalCount += 1;
                    boolIsChecked = true;
                    if (lblgvClaimAmount.Text != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(lblgvClaimAmount.Text);

                    }
                }

            }


            lblTotalClaims.Text = Convert.ToString(intTotalCount);
            lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

            hidTotalClaims.Value = Convert.ToString(intTotalCount);
            hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");

            if (boolIsChecked == false)
            {
                lblStatus.Text = "Please select Any of one Bill Details";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;

            }



            string strClaimGenerate = hidClaimGenerate.Value;

            //if (strClaimGenerate == "true")
            //{
            XMLFileWrite("TEST");

          //  }


        FunEnd: ;

            hidClaimGenerate.Value = "";
        }

        protected void btnProductionEclaim_Click(object sender, EventArgs e)
        {

            Boolean boolIsChecked = false;
            decimal decTotalClaimAmount = 0;
            Int32 intTotalCount = 0;

            for (int i = 0; i < gvInvoice.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvInvoice.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblgvClaimAmount = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvClaimAmount");

                if (chkInvResub.Checked == true)
                {
                    intTotalCount += 1;
                    boolIsChecked = true;
                    if (lblgvClaimAmount.Text != "")
                    {
                        decTotalClaimAmount += Convert.ToDecimal(lblgvClaimAmount.Text);

                    }
                }

            }


            lblTotalClaims.Text = Convert.ToString(intTotalCount);
            lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

            hidTotalClaims.Value = Convert.ToString(intTotalCount);
            hidTotalAmount.Value = decTotalClaimAmount.ToString("N2");

            if (boolIsChecked == false)
            {
                lblStatus.Text = "Please select Any of one Bill Details";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;

            }



            string strClaimGenerate = hidClaimGenerate.Value;

            //if (strClaimGenerate == "true")
            //{
            XMLFileWrite("PRODUCTION");

          //  }


        FunEnd: ;

            hidClaimGenerate.Value = "";

        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            ClearSaveData();
        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                txtResubNo.Text = "";
                txtIDPayer.Text = "";
                txtPolicyNo.Text = "";
                txtPriorAuthorizationID.Text = "";
                // txtReComment.Text = "";
                drpReType.SelectedIndex = 0;

                gvInvoice.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;


                gvInvoice.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");
                Label lblgvFileNo = (Label)gvScanCard.Cells[0].FindControl("lblgvFileNo");
                Label lblPolicyNo = (Label)gvScanCard.Cells[0].FindControl("lblPolicyNo");
                Label lblResubVerify = (Label)gvScanCard.Cells[0].FindControl("lblResubVerify");
                Label lblAuthorizationID = (Label)gvScanCard.Cells[0].FindControl("lblAuthorizationID");


                ViewState["InvoiceId"] = lblInvoiceId.Text;
                ViewState["FileNo"] = lblgvFileNo.Text;

                txtPolicyNo.Text = lblPolicyNo.Text;
                txtPriorAuthorizationID.Text = lblAuthorizationID.Text;

                ResubmissionGet(lblInvoiceId.Text);
                //if ( Convert.ToString(ViewState["ReSubNo"]) != "" && Convert.ToString(ViewState["ReSubNo"]) !=null)
                //{
                //    btnUpdate.Visible=true ;
                //}
                BindInvoiceTran();
                BindInvoiceClaimDtls();
                //if (Convert.ToString(ViewState["ReSubNo"]) == "" || Convert.ToString(ViewState["ReSubNo"]) == null)
                //{
                GetReceiptTran();
                // }

                BindResubTranHistory();
                BuildeResubClaimsHistory();
                BindResubClaimHistory();
                BindResubClaimHistoryGrid();

                if (lblResubVerify.Text == "Completed")
                {
                    chkResubVerified.Checked = true;

                }
                else
                {
                    chkResubVerified.Checked = false;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void InvoiceSelect_Click(object sender, EventArgs e)
        {
            try
            {
                CountClaimAmount();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void ServSelect_Click(object sender, EventArgs e)
        {
            try
            {


                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblServId;
                lblServId = (Label)gvScanCard.Cells[0].FindControl("lblServId");

                BindServiceDtls(lblServId.Text.Trim());



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_ServSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void gvInvTran_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drpDiscType, drpCoInsType;
                CheckBox chkSubmit;
                Label lblDiscType, lblCoInsType, lblResubType, lblSubmitted;


                drpDiscType = (DropDownList)e.Row.Cells[0].FindControl("drpDiscType");
                drpCoInsType = (DropDownList)e.Row.Cells[0].FindControl("drpCoInsType");

                chkSubmit = (CheckBox)e.Row.Cells[0].FindControl("chkSubmit");


                lblDiscType = (Label)e.Row.Cells[0].FindControl("lblDiscType");
                lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");
                lblResubType = (Label)e.Row.Cells[0].FindControl("lblResubType");
                lblSubmitted = (Label)e.Row.Cells[0].FindControl("lblSubmitted");

                drpDiscType.SelectedValue = lblDiscType.Text;
                drpCoInsType.SelectedValue = lblCoInsType.Text;
                if (lblSubmitted.Text == "True")
                {
                    chkSubmit.Checked = true;
                }
                else
                {
                    chkSubmit.Checked = false;
                }


            }
        }

        protected void btnSaveToDB_Click(object sender, EventArgs e)
        {

            try
            {
                Boolean boolIsChecked = false; ;
                for (int i = 0; i < gvInvTran.Rows.Count; i++)
                {
                    CheckBox chkSubmit;
                    chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");

                    if (chkSubmit.Checked == true)
                    {
                        boolIsChecked = true;
                    }

                }

                if (drpReType.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please select Resubmission Type";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;
                }

                if (boolIsChecked == false)
                {
                    lblStatus.Text = "Please select Any of one Bill Details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;

                }

                if (GlobalValues.FileDescription == "SMCH")
                {
                    string Criteria = " 1=1  AND   HIM_RESUB_VERIFY_STATUS IS NULL ";
                    Criteria += " AND  HIM_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";

                    DataSet DS = new DataSet();
                    DS = dbo.ResubmissionGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblStatus.Text = "This invoice already Resubmitted";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto SaveEnd;
                    }
                }

                if (gvInvoice.Rows.Count > 0 && ViewState["InvoiceId"] != "")
                {


                    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionAdd";
                    cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                    cmd.Parameters.Add(new SqlParameter("@HIM_REMARKS", SqlDbType.VarChar)).Value = txtIDPayer.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@ReComment", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@ReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue;

                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME", SqlDbType.VarChar)).Value = lblFileName.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME_ACTUAL", SqlDbType.VarChar)).Value = lblFileNameActual.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_ATTACHMENT", SqlDbType.Text)).Value = ViewState["AttachmentData"];
                    cmd.Parameters.Add(new SqlParameter("@HIM_AUTHID", SqlDbType.VarChar)).Value = txtPriorAuthorizationID.Text.Trim();



                    SqlParameter returnValue = new SqlParameter("@ReturnReSubNo", SqlDbType.VarChar, 20);
                    returnValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnValue);

                    cmd.ExecuteNonQuery();
                    string strReturnReSubNo;
                    strReturnReSubNo = Convert.ToString(returnValue.Value);
                    con.Close();


                    for (int i = 0; i < gvInvTran.Rows.Count; i++)
                    {
                        CheckBox chkSubmit;
                        TextBox txtResubComment;
                        DropDownList drpDiscType, drpCoInsType;
                        Label lblHIOType, lblServCode, lblSlno;
                        TextBox txtServCode, txtServeDesc, txtFee, txtDiscAmt, txtQTY, txtDeduct, txtCoInsAmt, txtHitAmount, txtPTAmount, txtHaadCode, txtHIOCode, txtHIOValue, txtHIOValueType;



                        chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");
                        txtResubComment = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtResubComment");

                        TextBox txtgvInvoiceID = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtgvInvoiceID");
                        lblServCode = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblServCode");
                        lblSlno = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblSlno");
                        txtServCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServCode");
                        txtServeDesc = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServeDesc");

                        txtFee = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtFee");
                        txtDiscAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDiscAmt");
                        txtQTY = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtQTY");
                        txtDeduct = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDeduct");
                        txtCoInsAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtCoInsAmt");

                        txtHitAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHitAmount");
                        txtPTAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtPTAmount");
                        txtHaadCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHaadCode");
                        lblHIOType = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblHIOType");
                        txtHIOCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOCode");
                        txtHIOValue = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValue");
                        txtHIOValueType = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValueType");



                        drpDiscType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpDiscType");
                        drpCoInsType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpCoInsType");


                        // if (chkSubmit.Checked == true)
                        // {
                        SqlConnection con1 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con1.Open();
                        cmd = new SqlCommand();

                        cmd.Connection = con1;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionTranAdd";
                        cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = txtgvInvoiceID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIT_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;

                        cmd.Parameters.Add(new SqlParameter("@HIT_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIT_DESCRIPTION", SqlDbType.VarChar)).Value = txtServeDesc.Text.Trim();
                        if (txtFee.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = txtFee.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_DISC_TYPE", SqlDbType.Char)).Value = drpDiscType.SelectedValue;

                        if (txtDiscAmt.Text.Trim() != "")

                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = txtDiscAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtQTY.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = txtQTY.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = "1";

                        if (txtDeduct.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = txtDeduct.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;

                        if (txtCoInsAmt.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = txtCoInsAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtHitAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = txtHitAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        if (txtPTAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = txtPTAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_HAAD_CODE", SqlDbType.VarChar)).Value = txtHaadCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@TransReComment", SqlDbType.VarChar)).Value = txtResubComment.Text.Trim();
                        if (chkSubmit.Checked == true)
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue.Trim();
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = "";
                        }
                        cmd.Parameters.Add(new SqlParameter("@HIT_RESUBMITTED", SqlDbType.VarChar)).Value = chkSubmit.Checked;
                        cmd.Parameters.Add(new SqlParameter("@HIT_SLNO", SqlDbType.Int)).Value = lblSlno.Text;
                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                        cmd.ExecuteNonQuery();
                        con1.Close();

                        SqlConnection con2 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con2.Open();
                        cmd = new SqlCommand();

                        cmd.Connection = con2;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionObservAdd";
                        cmd.Parameters.Add(new SqlParameter("@HIO_INVOICE_ID", SqlDbType.VarChar)).Value = txtgvInvoiceID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIO_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;
                        cmd.Parameters.Add(new SqlParameter("@HIO_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_TYPE", SqlDbType.VarChar)).Value = lblHIOType.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_CODE", SqlDbType.VarChar)).Value = txtHIOCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUE", SqlDbType.VarChar)).Value = txtHIOValue.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUETYPE", SqlDbType.VarChar)).Value = txtHIOValueType.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_SERV_SLNO", SqlDbType.Int)).Value = lblSlno.Text;

                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                        cmd.ExecuteNonQuery();
                        con2.Close();


                        // }

                    }

                    SqlConnection con3 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con3.Open();
                    cmd = new SqlCommand();

                    cmd.Connection = con3;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionClaimDtlsAdd";
                    cmd.Parameters.Add(new SqlParameter("@HIC_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                    cmd.Parameters.Add(new SqlParameter("@HIC_TYPE", SqlDbType.TinyInt)).Value = drpClmType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTDATE", SqlDbType.VarChar)).Value = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDDATE", SqlDbType.VarChar)).Value = txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTTYPE", SqlDbType.TinyInt)).Value = drpClmStartType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDTYPE", SqlDbType.TinyInt)).Value = drpClmEndType.SelectedValue;

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_P", SqlDbType.VarChar)).Value = txtPrincipalCode.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_A", SqlDbType.VarChar)).Value = txtAdmitCode.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S1", SqlDbType.VarChar)).Value = txtS1Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S2", SqlDbType.VarChar)).Value = txtS2Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S3", SqlDbType.VarChar)).Value = txtS3Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S4", SqlDbType.VarChar)).Value = txtS4Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S5", SqlDbType.VarChar)).Value = txtS5Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S6", SqlDbType.VarChar)).Value = txtS6Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S7", SqlDbType.VarChar)).Value = txtS7Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S8", SqlDbType.VarChar)).Value = txtS8Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S11", SqlDbType.VarChar)).Value = txtS11Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S12", SqlDbType.VarChar)).Value = txtS12Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S13", SqlDbType.VarChar)).Value = txtS13Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S14", SqlDbType.VarChar)).Value = txtS14Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S15", SqlDbType.VarChar)).Value = txtS15Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S16", SqlDbType.VarChar)).Value = txtS16Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S17", SqlDbType.VarChar)).Value = txtS17Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S18", SqlDbType.VarChar)).Value = txtS18Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S19", SqlDbType.VarChar)).Value = txtS19Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S20", SqlDbType.VarChar)).Value = txtS20Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S21", SqlDbType.VarChar)).Value = txtS21Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S22", SqlDbType.VarChar)).Value = txtS22Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S23", SqlDbType.VarChar)).Value = txtS23Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S24", SqlDbType.VarChar)).Value = txtS24Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S25", SqlDbType.VarChar)).Value = txtS25Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S26", SqlDbType.VarChar)).Value = txtS26Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S27", SqlDbType.VarChar)).Value = txtS27Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S28", SqlDbType.VarChar)).Value = txtS28Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                    cmd.ExecuteNonQuery();
                    con3.Close();



                    //c.conopen();
                    //cmd = new SqlCommand();

                    //cmd.Connection = c.con;
                    //cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.CommandText = "HMS_SP_ResubmissionUpdate";
                    //cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                    //cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = strReturnReSubNo;

                    //cmd.ExecuteNonQuery();
                    //c.conclose();

                }

                ClearSaveData();
                lblStatus.Text = "Details Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.aspx_btnSaveToDB_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["ReSubNo"]) == "" || Convert.ToString(ViewState["ReSubNo"]) == null)
                {
                    lblStatus.Text = "Resubmission is not available";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;
                }


                SqlCommand cmd = new SqlCommand();

                if (gvInvoice.Rows.Count > 0 && ViewState["InvoiceId"] != "")
                {




                    /*  
                    c.conopen();
                   cmd.Connection = c.con;
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.CommandText = "HMS_SP_ResubmissionAdd";
                   cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                   cmd.Parameters.Add(new SqlParameter("@HIM_REMARKS", SqlDbType.VarChar)).Value = txtIDPayer.Text.Trim();

                   SqlParameter returnValue = new SqlParameter("@ReturnReSubNo", SqlDbType.VarChar, 20);
                   returnValue.Direction = ParameterDirection.Output;
                   cmd.Parameters.Add(returnValue);

                   cmd.ExecuteNonQuery();
                   string strReturnReSubNo;
                   strReturnReSubNo = Convert.ToString(returnValue.Value);
                   c.conclose();
                   */

                    for (int i = 0; i < gvInvTran.Rows.Count; i++)
                    {
                        CheckBox chkSubmit;
                        TextBox txtResubComment;
                        DropDownList drpDiscType, drpCoInsType;
                        Label lblHIOType, lblServCode;
                        TextBox txtServCode, txtServeDesc, txtFee, txtDiscAmt, txtQTY, txtDeduct, txtCoInsAmt, txtHitAmount, txtPTAmount, txtHaadCode, txtHIOCode, txtHIOValue, txtHIOValueType;



                        chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");
                        txtResubComment = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtResubComment");


                        Label lblSlno = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblSlno");

                        TextBox txtgvInvoiceID = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtgvInvoiceID");
                        lblServCode = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblServCode");

                        txtServCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServCode");
                        txtServeDesc = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtServeDesc");

                        txtFee = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtFee");
                        txtDiscAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDiscAmt");
                        txtQTY = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtQTY");
                        txtDeduct = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtDeduct");
                        txtCoInsAmt = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtCoInsAmt");

                        txtHitAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHitAmount");
                        txtPTAmount = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtPTAmount");
                        txtHaadCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHaadCode");
                        lblHIOType = (Label)gvInvTran.Rows[i].Cells[0].FindControl("lblHIOType");
                        txtHIOCode = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOCode");
                        txtHIOValue = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValue");
                        txtHIOValueType = (TextBox)gvInvTran.Rows[i].Cells[0].FindControl("txtHIOValueType");


                        drpDiscType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpDiscType");
                        drpCoInsType = (DropDownList)gvInvTran.Rows[i].Cells[0].FindControl("drpCoInsType");

                        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con.Open();
                        cmd = new SqlCommand();
                        //   ReSubSaveLog("  ResubmissionTranUpdate ");

                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionTranUpdate";
                        cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = txtgvInvoiceID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIT_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;

                        cmd.Parameters.Add(new SqlParameter("@HIT_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIT_DESCRIPTION", SqlDbType.VarChar)).Value = txtServeDesc.Text.Trim();

                        if (txtFee.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = txtFee.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_FEE", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_DISC_TYPE", SqlDbType.Char)).Value = drpDiscType.SelectedValue;

                        if (txtDiscAmt.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = txtDiscAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DISC_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtQTY.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = txtQTY.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_QTY", SqlDbType.Decimal)).Value = "1";

                        if (txtDeduct.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = txtDeduct.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_DEDUCT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;

                        if (txtCoInsAmt.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = txtCoInsAmt.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_CO_INS_AMT", SqlDbType.Decimal)).Value = "0";

                        if (txtHitAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = txtHitAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        if (txtPTAmount.Text.Trim() != "")
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = txtPTAmount.Text.Trim();
                        else
                            cmd.Parameters.Add(new SqlParameter("@HIM_PT_AMOUNT", SqlDbType.Decimal)).Value = "0";

                        cmd.Parameters.Add(new SqlParameter("@HIT_HAAD_CODE", SqlDbType.VarChar)).Value = txtHaadCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@TransReComment", SqlDbType.VarChar)).Value = txtResubComment.Text.Trim();
                        if (chkSubmit.Checked == true)
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue.Trim();
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@TransReType", SqlDbType.VarChar)).Value = "";
                        }
                        cmd.Parameters.Add(new SqlParameter("@HIT_RESUBMITTED", SqlDbType.VarChar)).Value = chkSubmit.Checked;
                        cmd.Parameters.Add(new SqlParameter("@HIT_SLNO", SqlDbType.Int)).Value = lblSlno.Text;
                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ReSubNo"]);


                        // ReSubSaveLog(" ResubmissionTranUpdate Started ");

                        cmd.ExecuteNonQuery();
                        con.Close();
                        //  ReSubSaveLog(" ResubmissionTranUpdate End ");

                        SqlConnection con1 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con1.Open();
                        cmd = new SqlCommand();

                        cmd.Connection = con1;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_ResubmissionObservUpdate";
                        cmd.Parameters.Add(new SqlParameter("@HIO_INVOICE_ID", SqlDbType.VarChar)).Value = txtgvInvoiceID.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@Old_HIO_SERV_CODE", SqlDbType.VarChar)).Value = lblServCode.Text;
                        cmd.Parameters.Add(new SqlParameter("@HIO_SERV_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_TYPE", SqlDbType.VarChar)).Value = lblHIOType.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_CODE", SqlDbType.VarChar)).Value = txtHIOCode.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUE", SqlDbType.VarChar)).Value = txtHIOValue.Text.Trim();
                        cmd.Parameters.Add(new SqlParameter("@HIO_VALUETYPE", SqlDbType.VarChar)).Value = txtHIOValueType.Text.Trim();


                        cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ReSubNo"]);

                        // ReSubSaveLog(" ResubmissionObservUpdate Start ");
                        cmd.ExecuteNonQuery();
                        con1.Close();
                        //  ReSubSaveLog(" ResubmissionObservUpdate Start ");




                    }

                    SqlConnection con2 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con2.Open();
                    cmd = new SqlCommand();

                    cmd.Connection = con2;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionClaimDtlsUpdate";
                    cmd.Parameters.Add(new SqlParameter("@HIC_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                    cmd.Parameters.Add(new SqlParameter("@HIC_TYPE", SqlDbType.TinyInt)).Value = drpClmType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTDATE", SqlDbType.VarChar)).Value = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDDATE", SqlDbType.VarChar)).Value = txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim() + ":000";//:00
                    cmd.Parameters.Add(new SqlParameter("@HIC_STARTTYPE", SqlDbType.TinyInt)).Value = drpClmStartType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HIC_ENDTYPE", SqlDbType.TinyInt)).Value = drpClmEndType.SelectedValue;

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_P", SqlDbType.VarChar)).Value = txtPrincipalCode.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_A", SqlDbType.VarChar)).Value = txtAdmitCode.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S1", SqlDbType.VarChar)).Value = txtS1Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S2", SqlDbType.VarChar)).Value = txtS2Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S3", SqlDbType.VarChar)).Value = txtS3Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S4", SqlDbType.VarChar)).Value = txtS4Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S5", SqlDbType.VarChar)).Value = txtS5Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S6", SqlDbType.VarChar)).Value = txtS6Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S7", SqlDbType.VarChar)).Value = txtS7Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S8", SqlDbType.VarChar)).Value = txtS8Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S11", SqlDbType.VarChar)).Value = txtS11Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S12", SqlDbType.VarChar)).Value = txtS12Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S13", SqlDbType.VarChar)).Value = txtS13Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S14", SqlDbType.VarChar)).Value = txtS14Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S15", SqlDbType.VarChar)).Value = txtS15Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S16", SqlDbType.VarChar)).Value = txtS16Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S17", SqlDbType.VarChar)).Value = txtS17Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S18", SqlDbType.VarChar)).Value = txtS18Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S19", SqlDbType.VarChar)).Value = txtS19Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S20", SqlDbType.VarChar)).Value = txtS20Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S21", SqlDbType.VarChar)).Value = txtS21Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S22", SqlDbType.VarChar)).Value = txtS22Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S23", SqlDbType.VarChar)).Value = txtS23Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S24", SqlDbType.VarChar)).Value = txtS24Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S25", SqlDbType.VarChar)).Value = txtS25Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S26", SqlDbType.VarChar)).Value = txtS26Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S27", SqlDbType.VarChar)).Value = txtS27Code.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIC_ICD_CODE_S28", SqlDbType.VarChar)).Value = txtS28Code.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ReSubNo"]);
                    // ReSubSaveLog(" ResubmissionClaimDtlsUpdate Start ");
                    cmd.ExecuteNonQuery();
                    con2.Close();
                    //  ReSubSaveLog(" ResubmissionClaimDtlsUpdate End ");

                    SqlConnection con3 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con3.Open();
                    cmd = new SqlCommand();

                    cmd.Connection = con3;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_ResubmissionUpdate";
                    cmd.Parameters.Add(new SqlParameter("@HIT_INVOICE_ID", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                    cmd.Parameters.Add(new SqlParameter("@HIM_REMARKS", SqlDbType.VarChar)).Value = txtIDPayer.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HIM_POLICY_NO", SqlDbType.VarChar)).Value = txtPolicyNo.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@ReComment", SqlDbType.VarChar)).Value = "";
                    cmd.Parameters.Add(new SqlParameter("@ReType", SqlDbType.VarChar)).Value = drpReType.SelectedValue;


                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME", SqlDbType.VarChar)).Value = lblFileName.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_FILE_NAME_ACTUAL", SqlDbType.VarChar)).Value = lblFileNameActual.Text;
                    cmd.Parameters.Add(new SqlParameter("@HIM_ATTACHMENT", SqlDbType.Text)).Value = ViewState["AttachmentData"];
                    cmd.Parameters.Add(new SqlParameter("@HIM_AUTHID", SqlDbType.VarChar)).Value = txtPriorAuthorizationID.Text.Trim();

                    cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ReSubNo"]);

                    // ReSubSaveLog(" ResubmissionUpdate Start ");
                    cmd.ExecuteNonQuery();
                    con3.Close();

                    // ReSubSaveLog(" ResubmissionUpdate End ");
                }
                ClearSaveData();
                lblStatus.Text = "Details Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvInvoice.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvInvoice.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkInvResub");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }
                }

                CountClaimAmount();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpReType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < gvInvTran.Rows.Count; i++)
                {
                    CheckBox chkSubmit;

                    chkSubmit = (CheckBox)gvInvTran.Rows[i].Cells[0].FindControl("chkSubmit");

                    if (drpReType.SelectedValue == "correction")
                    {
                        chkSubmit.Checked = true;
                    }
                    else
                    {
                        chkSubmit.Checked = false;
                    }


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.drpReType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 ";
                DS = new DataSet();

                string strName = txtCompany.Text.Trim();
                string[] arrName = strName.Split('~');

                if (arrName.Length > 1)
                {
                    txtCompany.Text = arrName[0].Trim();
                    txtCompanyName.Text = arrName[1].Trim();
                    Criteria += " and HCM_COMP_ID='" + arrName[0].Trim() + "' ";
                }
                else
                {
                    Criteria += " and HCM_COMP_ID='" + txtCompany.Text.Trim() + "'";
                }

                DS = dbo.retun_inccmp_details(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_NAME"]).Trim();
                }
                else
                {
                    txtCompanyName.Text = "";
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.txtCompany_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAttaAdd_Click(object sender, EventArgs e)
        {

            try
            {

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                string strPath = GlobalValues.AuthorizationAttachmentPath;

                Int32 intLength;
                intLength = filAttachment.PostedFile.ContentLength;
                String ext = System.IO.Path.GetExtension(filAttachment.FileName);
                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                lblFileName.Text = strName + ext;
                lblFileNameActual.Text = filAttachment.FileName;
                ViewState["AttachmentData"] = Convert.ToBase64String(filAttachment.FileBytes);
                DeleteAttach.Visible = true;


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteAttach_Click(object sender, EventArgs e)
        {

            try
            {
                lblFileName.Text = "";
                lblFileNameActual.Text = "";
                ViewState["AttachmentData"] = "";
                DeleteAttach.Visible = false;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.DeleteAttach_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();

                SqlCommand cmd = new SqlCommand();

                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_ResubmissionDelete";
                cmd.Parameters.Add(new SqlParameter("@BranchID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@InvoiceNo", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["InvoiceId"]);
                cmd.Parameters.Add(new SqlParameter("@ReSubNo", SqlDbType.VarChar)).Value = txtResubNo.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                cmd.ExecuteNonQuery();
                con.Close();

                lblStatus.Text = "Following Resubmission Deleted - " + txtResubNo.Text.Trim();
                lblStatus.ForeColor = System.Drawing.Color.Green;
                ClearSaveData();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

                createDataInExcel();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnExport_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        private void createDataInExcel()
        {
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strPath = GlobalValues.Shafafiya_ResubEClaimExport_FilePath;

            if (Directory.Exists(strPath) == false)
            {
                Directory.CreateDirectory(strPath);
            }


            oXL = new Microsoft.Office.Interop.Excel.Application();
            oXL.Visible = false;
            //Get a new workbook.
            oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(System.Reflection.Missing.Value));
            oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
            //System.Data.DataTable dtGridData=ds.Tables[0];
            //int iRow = 2;
            //if (ds.Tables[0].Rows.Count > 0)
            //{

            //    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
            //    {
            //        oSheet.Cells[1, j + 1] = ds.Tables[0].Columns[j].ColumnName;
            //    }



            //    // For each row, print the values of each column.
            //    for (int rowNo = 0; rowNo < ds.Tables[0].Rows.Count; rowNo++)
            //    {
            //        for (int colNo = 0; colNo < ds.Tables[0].Columns.Count; colNo++)
            //        {
            //            oSheet.Cells[iRow, colNo + 1] = ds.Tables[0].Rows[rowNo][colNo].ToString();
            //        }
            //        iRow++;
            //    }

            //}


            oSheet.Cells[1, 1] = "Inv Code";
            oSheet.Cells[1, 2] = "Date";
            oSheet.Cells[1, 3] = "Claim Amt";


            oSheet.Cells[1, 4] = "Paid Amt.";
            oSheet.Cells[1, 5] = "Blance Amt.";

            oSheet.Cells[1, 6] = "Patient Share";
            oSheet.Cells[1, 7] = "Reject";

            oSheet.Cells[1, 8] = "File No";
            oSheet.Cells[1, 9] = "Patient Name";
            oSheet.Cells[1, 10] = "Dr Name";

            oSheet.Cells[1, 11] = "Policy No";
            oSheet.Cells[1, 12] = "Claim No";

            int iRow = 2;
            for (int i = 0; i < gvInvoice.Rows.Count; i++)
            {

                CheckBox chkInvResub = (CheckBox)gvInvoice.Rows[i].Cells[0].FindControl("chkInvResub");

                Label lblInvoiceId = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblInvoiceId");

                Label lblgvDate = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvDate");

                Label lblgvClaimAmount = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvClaimAmount");

                Label lblgvPaidAmt = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvPaidAmt");
                Label lblgvBlanceAmt = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvBlanceAmt");


                Label lblgvPatientShare = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvPatientShare");

                Label lblgvRejectedAmt = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvRejectedAmt");



                Label lblgvFileNo = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvFileNo");

                Label lblgvPTName = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvPTName");

                Label lblgvDrName = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvDrName");


                Label lblgvPolicyNo = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvPolicyNo");

                Label lblgvVoucherNo = (Label)gvInvoice.Rows[i].Cells[0].FindControl("lblgvVoucherNo");



                if (chkInvResub.Checked == true)
                {

                    oSheet.Cells[iRow, 1] = lblInvoiceId.Text;
                    oSheet.Cells[iRow, 2] = lblgvDate.Text;
                    oSheet.Cells[iRow, 3] = lblgvClaimAmount.Text;
                    oSheet.Cells[iRow, 4] = lblgvPaidAmt.Text;
                    oSheet.Cells[iRow, 5] = lblgvBlanceAmt.Text;

                    oSheet.Cells[iRow, 6] = lblgvPatientShare.Text;
                    oSheet.Cells[iRow, 7] = lblgvRejectedAmt.Text;

                    oSheet.Cells[iRow, 8] = lblgvFileNo.Text;
                    oSheet.Cells[iRow, 9] = lblgvPTName.Text;
                    oSheet.Cells[iRow, 10] = lblgvDrName.Text;

                    oSheet.Cells[iRow, 11] = lblgvPolicyNo.Text;
                    oSheet.Cells[iRow, 12] = lblgvVoucherNo.Text;

                    iRow++;
                }


            }



            oRng = oSheet.get_Range("A1", "IV1");
            oRng.EntireColumn.AutoFit();
            oXL.Visible = false;
            oXL.UserControl = false;
            string strFile = "ReSubExcel" + strName + ".xls"; //"report" + DateTime.Now.Ticks.ToString() + ".xls";//+
            oWB.SaveAs(strPath +
            strFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, null, null, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, false, false, null, null);
            // Need all following code to clean up and remove all references!!!
            oWB.Close(null, null, null);
            oXL.Workbooks.Close();
            oXL.Quit();
            //Marshal.ReleaseComObject(oRng);
            //Marshal.ReleaseComObject(oXL);
            //Marshal.ReleaseComObject(oSheet);
            //Marshal.ReleaseComObject(oWB);
            //string strMachineName = Request.ServerVariables["SERVER_NAME"];
            //Response.Redirect("http://" + strMachineName + "/" + "ViewNorthWindSample/reports/" + strFile);

            //lblStatus.Text = "ReSubmission Excel Exported - File Name is ReSubExcel" + strName + ".xls";
            //lblStatus.ForeColor = System.Drawing.Color.Green;
            //lblStatus.Visible = true;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowExportMsg('" + strName + "')", true);

        }


        protected void chkResubVerified_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "HIM_RESUB_VERIFY_STATUS='Pending'";

                if (chkResubVerified.Checked == true)
                {
                    FieldNameWithValues = " HIM_RESUB_VERIFY_STATUS='Completed' ";

                }


                string Criteria = " HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HIM_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);

                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_RESUBMISSION_MASTER", Criteria);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      GenerateReSubmission.chkResubVerified_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDispense_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();
                DataSet DS = new DataSet();
                string Criteria = " ClaimID='" + Convert.ToString(ViewState["InvoiceId"]) + "'  AND ReSubNo='" + txtResubNo.Text.Trim() + "'";
                DS = objCom.fnGetFieldValue("*", "HMS_ECLAIM_XML_DATA_MASTER", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = " Already Transfered";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Criteria = " 1=1 ";
                Criteria += " AND  HIT_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "' AND ReSubNo='" + txtResubNo.Text.Trim() + "'";

                dboperations dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.ResubmissionTransGet(Criteria);


                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = " Services not available for this Resubmission";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Criteria = " 1=1 ";
                Criteria += " AND  HIC_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "' AND HIC_RESUBNO='" + txtResubNo.Text.Trim() + "'";

                  dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.ResubmissionClaimDtlsGet(Criteria);


                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = " Diagnosis not available for this Resubmission";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (TransferResubValidation() == false)
                {
                    goto FunEnd;
                }



                // TextFileWriting("");
                // TextFileWriting(Convert.ToString(ViewState["BRANCH_ID"]) + "," + txtInvoiceID.Text.Trim() + "," + txtFileNo.Text + "," + Convert.ToString(ViewState["VisitType"]) + "," + Convert.ToString(Session["User_ID"]));

                clsInvoice objInv = new clsInvoice();

                objInv.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objInv.HIM_INVOICE_ID = Convert.ToString(ViewState["InvoiceId"]);
                objInv.RESUB_NO = txtResubNo.Text.Trim();
                objInv.HIM_PT_ID = Convert.ToString(ViewState["FileNo"]);
                objInv.UserID = Convert.ToString(Session["User_ID"]);



                objInv.EclaimXMLDataMasterAddFromResub();



                TextFileWriting(" EclaimXMLDataMasterAdd completed");

                BindInvoiceGrid();


                TextFileWriting(" BindVisit completed");

                lblStatus.Text = " Transfer Completed";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ReSubmission.btnDispense_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        #endregion

    }
}