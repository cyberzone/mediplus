﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Text;
using System.Xml;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace Mediplus.HMS.Billing
{
    public partial class MRDAudit : System.Web.UI.Page
    {
        #region Variables
        dboperations dbo = new dboperations();

        CommonBAL objCom = new CommonBAL();
        eClaimCoderBAL objeClaimCoder;



        DataSet DS;


        static string strSessionBranchId;
        #endregion

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            //  Criteria += " AND SCREENNAME='EMR_SCANNING' ";

            DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);
            ViewState["EMR_SCAN_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "ECLAIM_TYPE")
                    {
                        if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                        }
                        else
                        {
                            ViewState["CUST_VALUE"] = "AUH";
                        }
                    }
                }
            }


        }

        void BindVisit()
        {
            string Criteria = " 1=1 ";
            //Criteria += " AND HPV_PT_TYPE IN ('CR','Credit')";

            if (txtSrcFileNo.Text.Trim() != "")
                Criteria += " AND HPV_PT_ID='" + txtSrcFileNo.Text + "'";

            string strInvNo = "";
            for (int i = 0; i < lstInvoiceNo.Items.Count; i++)
            {
                strInvNo += "'" + lstInvoiceNo.Items[i].Text + "',";
            }

            if (strInvNo.Length > 1) strInvNo = strInvNo.Substring(0, strInvNo.Length - 1);

            if (strInvNo != "")
            {
                Criteria += " AND HPV_INVOICE_ID IN (" + strInvNo + ")";
            }


            if (txtInvoiceNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_INVOICE_ID IN ('" + txtInvoiceNo.Text.Trim() + "')";
            }


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }

            if (txtFromTime.Text != "")
            {
                Criteria += " AND HPV_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND    CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101)  <= '" + strForToDate + "'";
            }

            if (txtToTime.Text != "")
            {



                Criteria += " AND  HPV_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";
            }




            if (txtCompanyID.Text.Trim() != "")
            {
                //  Criteria += " AND HPV_COMP_ID='" + txtCompanyID.Text + "'";
                Criteria += " AND   HPV_COMP_ID IN (SELECT HCM_COMP_ID   FROM HMS_COMPANY_MASTER WHERE HCM_BILL_CODE='" + txtCompanyID.Text + "' )";
            }

            //if (txtCompanyName.Text.Trim() != "")
            //    Criteria += " AND HPV_COMP_NAME='" + txtCompanyName.Text + "'";


            if (txtDoctorID.Text.Trim() != "")
                Criteria += " AND HPV_DR_ID='" + txtDoctorID.Text + "'";

            if (drpPatientType.Items.Count > 0)
            {
                if (drpPatientType.SelectedIndex != 0)
                {

                    if (drpPatientType.SelectedIndex == 1)
                    {
                        Criteria += " AND ( HPV_PT_TYPE ='CA' OR  HPV_PT_TYPE ='CASH') ";
                    }
                    else if (drpPatientType.SelectedIndex == 2)
                    {
                        Criteria += " AND ( HPV_PT_TYPE ='CR' OR  HPV_PT_TYPE ='CREDIT') ";

                        Criteria += " AND  HPV_COMP_ID IS NOT NULL AND HPV_COMP_ID <>'' AND  HPV_COMP_NAME IS NOT NULL AND HPV_COMP_NAME <>'' ";

                    }
                }

            }


            /*

             if (drpCoderType.SelectedIndex != 0)
             {
                 if (drpCoderType.SelectedValue == "Completed")
                 {
                     Criteria += " AND (HEIM_CODER_ID is not null and HEIM_CODER_ID !='' )";
                 }
                 else if (drpCoderType.SelectedValue == "Pending")
                 {
                     Criteria += " AND (HEIM_CODER_ID is  null OR HEIM_CODER_ID = '' )";
                 }

             }
             */




            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.Patient_Master_VisitGet(Criteria);

            gvVisit.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvVisit.Visible = true;
                gvVisit.DataSource = DS;
                gvVisit.DataBind();

            }


        }

        void BindAdmission()
        {
            string Criteria = " 1=1 ";
            //Criteria += " AND HPV_PT_TYPE IN ('CR','Credit')";

            if (txtSrcFileNo.Text.Trim() != "")
                Criteria += " AND IAS_PT_ID='" + txtSrcFileNo.Text + "'";

            string strInvNo = "";
            for (int i = 0; i < lstInvoiceNo.Items.Count; i++)
            {
                strInvNo += "'" + lstInvoiceNo.Items[i].Text + "',";
            }

            if (strInvNo.Length > 1) strInvNo = strInvNo.Substring(0, strInvNo.Length - 1);

            if (strInvNo != "")
            {
                Criteria += " AND IAS_INVOICE_ID IN (" + strInvNo + ")";
            }


            if (txtInvoiceNo.Text.Trim() != "")
            {
                Criteria += " AND IAS_INVOICE_ID IN ('" + txtInvoiceNo.Text.Trim() + "')";
            }


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) >= '" + strForStartDate + "'";
            }

            if (txtFromTime.Text != "")
            {
                Criteria += " AND IAS_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND    CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101)  <= '" + strForToDate + "'";
            }

            if (txtToTime.Text != "")
            {



                Criteria += " AND  IAS_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";
            }




            if (txtCompanyID.Text.Trim() != "")
            {
                //Criteria += " AND IAS_INS_COMP_ID='" + txtCompanyID.Text + "'";
                Criteria += " AND   IAS_INS_COMP_ID IN (SELECT HCM_COMP_ID   FROM HMS_COMPANY_MASTER WHERE HCM_BILL_CODE='" + txtCompanyID.Text + "' )";
            }

            //if (txtCompanyName.Text.Trim() != "")
            //    Criteria += " AND IAS_INS_COMP_NAME='" + txtCompanyName.Text + "'";


            if (txtDoctorID.Text.Trim() != "")
                Criteria += " AND IAS_DR_ID='" + txtDoctorID.Text + "'";

            if (drpPatientType.Items.Count > 0)
            {
                if (drpPatientType.SelectedIndex != 0)
                {

                    if (drpPatientType.SelectedIndex == 1)
                    {
                        Criteria += " AND ( IAS_PT_TYPE ='CA' OR  IAS_PT_TYPE ='CASH') ";
                    }
                    else if (drpPatientType.SelectedIndex == 2)
                    {
                        Criteria += " AND ( IAS_PT_TYPE ='CR' OR  IAS_PT_TYPE ='CREDIT') ";
                    }
                }

            }


            /*

             if (drpCoderType.SelectedIndex != 0)
             {
                 if (drpCoderType.SelectedValue == "Completed")
                 {
                     Criteria += " AND (HEIM_CODER_ID is not null and HEIM_CODER_ID !='' )";
                 }
                 else if (drpCoderType.SelectedValue == "Pending")
                 {
                     Criteria += " AND (HEIM_CODER_ID is  null OR HEIM_CODER_ID = '' )";
                 }

             }
             */




            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();
            DS = dbo.GetIPAdmissionSummary(Criteria);

            gvIPAdmission.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIPAdmission.Visible = true;
                gvIPAdmission.DataSource = DS;
                gvIPAdmission.DataBind();

            }


        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["HPM_PT_FNAME"].ToString();
                txtMName.Text = ds.Tables[0].Rows[0]["HPM_PT_MNAME"].ToString();
                txtLName.Text = ds.Tables[0].Rows[0]["HPM_PT_LNAME"].ToString();

                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);
                txtMonth.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);
                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();
                txtNationality.Text = ds.Tables[0].Rows[0]["HPM_NATIONALITY"].ToString();

                txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                    lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //   hidDepID.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                //  txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                // txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //  txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //  txtDrName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_NAME"]);

                // GetPatientVisit();



            }

        }

        void PatientPhoto()
        {
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";
            dbo = new dboperations();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND  CONVERT(varchar,HPP_DATE,103)='" + Convert.ToString(ViewState["EMR_Date"]) + "'";
            if (Convert.ToString(ViewState["Photo_ID"]) != "" && Convert.ToString(ViewState["Photo_ID"]) != null)
            {
                Criteria += " AND HPP_PHOTO_ID='" + Convert.ToString(ViewState["Photo_ID"]) + "'";
            }

            ds = dbo.PatientPhotoGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {

                if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                {

                    Session["SignatureImg1"] = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD"];


                    imgFront.ImageUrl = "../DisplayCardImage.aspx";
                }

                if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD1") == false)
                {
                    Session["SignatureImg2"] = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD1"];

                    imgBack.ImageUrl = "../DisplayCardImageBack.aspx";
                }

            }
        }

        void BindInvoice()
        {
            string Criteria = " 1=1 ";

            if (txtSrcFileNo.Text.Trim() != "")
                Criteria += " AND HIM_PT_ID='" + txtSrcFileNo.Text + "'";

            string strInvNo = "";
            for (int i = 0; i < lstInvoiceNo.Items.Count; i++)
            {
                strInvNo += "'" + lstInvoiceNo.Items[i].Text + "',";
            }

            if (strInvNo.Length > 1) strInvNo = strInvNo.Substring(0, strInvNo.Length - 1);

            if (strInvNo != "")
            {
                Criteria += " AND HIM_INVOICE_ID IN (" + strInvNo + ")";
            }


            if (txtInvoiceNo.Text.Trim() != "")
            {
                Criteria += " AND HIM_INVOICE_ID IN ('" + txtInvoiceNo.Text.Trim() + "')";
            }


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }

            if (txtFromTime.Text != "")
            {
                Criteria += " AND HIM_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND    CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101)  <= '" + strForToDate + "'";
            }

            if (txtToTime.Text != "")
            {



                Criteria += " AND  HIM_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";
            }




            if (txtCompanyID.Text.Trim() != "")
                Criteria += " AND HIM_BILLTOCODE='" + txtCompanyID.Text + "'";


            if (txtCompanyName.Text.Trim() != "")
                Criteria += " AND HIM_INS_NAME='" + txtCompanyName.Text + "'";


            if (txtDoctorID.Text.Trim() != "")
                Criteria += " AND HIM_DR_CODE='" + txtDoctorID.Text + "'";

            //if (txtDoctorName.Text.Trim() != "")
            //    Criteria += " AND HIM_DR_NAME='" + txtDoctorName.Text + "'";


            if (drpCoderType.SelectedIndex != 0)
            {
                if (drpCoderType.SelectedValue == "Completed")
                {
                    Criteria += " AND (HEIM_CODER_ID is not null and HEIM_CODER_ID !='' )";
                }
                else if (drpCoderType.SelectedValue == "Pending")
                {
                    Criteria += " AND (HEIM_CODER_ID is  null OR HEIM_CODER_ID = '' )";
                }

            }


            //Criteria += "  AND CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= CONVERT(datetime,convert(varchar(10),getdate(),101),101)";

            // Criteria += " AND HPV_STATUS='F' ";

            //if (drpDoctor.SelectedIndex != 0)
            //{
            //    Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            //}

            //if (drpDepartment.SelectedIndex != 0)
            //{
            //    Criteria += " AND HPV_DEP_NAME LIKE '" + drpDepartment.SelectedItem.Text + "'";
            //}


            objeClaimCoder = new eClaimCoderBAL();
            DataSet DS = new DataSet();
            DS = objeClaimCoder.Invoice_EclaimCoderInvoiceGet(Criteria);

            gvInvoice.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvoice.Visible = true;
                gvInvoice.DataSource = DS;
                gvInvoice.DataBind();
            }
            else
            {


            }


        }

        void PatientDtlsClear()
        {

            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";

            txtDOB.Text = "";
            txtAge.Text = "";
            txtMonth.Text = "";

            txtSex.Text = "";

            txtAddress.Text = "";
            txtPoBox.Text = "";

            txtArea.Text = "";
            txtCity.Text = "";
            txtNationality.Text = "";

            txtPhone1.Text = "";
            txtMobile1.Text = "";
            txtMobile2.Text = "";

            lblIDCaption.Text = "EmiratesId";
            txtEmiratesID.Text = "";
            //hidAgeType.Value = "";
            //hidInsCode.Value = "";
            //hidInsName.Value = "";
            //hidDepID.Value = "";




        }

        #region Visit Details Tab


        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            CommonBAL objComm = new CommonBAL();
            DS = objComm.DiagnosisGet(Criteria);
            gvDiagnosis.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();
                gvDiagnosis.Visible = true;

            }
            else
            {
                gvDiagnosis.DataBind();
            }
        FunEnd: ;
        }

        void BindIPDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPD_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            CommonBAL objComm = new CommonBAL();
            DS = objComm.IPDiagnosisGet(Criteria);
            gvIPDiagnosis.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIPDiagnosis.DataSource = DS;
                gvIPDiagnosis.DataBind();
                gvIPDiagnosis.Visible = true;

            }
            else
            {
                gvIPDiagnosis.DataBind();
            }
        FunEnd: ;
        }

        void ClearDiagnosis()
        {
            gvDiagnosis.DataBind();
        }

        //---------------------



        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.ProceduresGet(Criteria);
            gvProcedure.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProcedure.DataSource = DS;
                gvProcedure.DataBind();
                gvProcedure.Visible = true;
            }
            else
            {
                gvProcedure.DataBind();
            }


        FunEnd: ;

        }

        void BindIPProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.IPProceduresGet(Criteria);
            gvIPProcedure.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIPProcedure.DataSource = DS;
                gvIPProcedure.DataBind();
                gvIPProcedure.Visible = true;
            }
            else
            {
                gvIPProcedure.DataBind();
            }


        FunEnd: ;

        }

        //-----------------------------
        //-----------------------------

        void BindRadiology()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPR_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.RadiologyGet(Criteria);
            gvRadiology.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadiology.DataSource = DS;
                gvRadiology.DataBind();
                gvRadiology.Visible = true;
            }
            else
            {
                gvRadiology.DataBind();
            }
        FunEnd: ;
        }

        void BindIPRadiology()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPR_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.IPRadiologyGet(Criteria);
            gvIPRadiology.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIPRadiology.DataSource = DS;
                gvIPRadiology.DataBind();
                gvIPRadiology.Visible = true;
            }
            else
            {
                gvIPRadiology.DataBind();
            }
        FunEnd: ;
        }
        //-----------------------------

        //-----------------------------


        void BindLaboratory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPL_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.LaboratoryGet(Criteria);
            gvLaboratory.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLaboratory.DataSource = DS;
                gvLaboratory.DataBind();

                gvLaboratory.Visible = true;

            }
            else
            {
                gvLaboratory.DataBind();
            }

        FunEnd: ;

        }

        void BindIPLaboratory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPL_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.IPLaboratoryGet(Criteria);
            gvIPLaboratory.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIPLaboratory.DataSource = DS;
                gvIPLaboratory.DataBind();

                gvIPLaboratory.Visible = true;

            }
            else
            {
                gvIPLaboratory.DataBind();
            }

        FunEnd: ;

        }



        //-----------------------------


        //-----------------------------

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.PharmacyGet(Criteria);
            gvPharmacy.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();
                gvPharmacy.Visible = true;
            }
            else
            {
                gvPharmacy.DataBind();
            }
        FunEnd: ;

        }

        void BindIPPharmacy()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.IPPharmacyGet(Criteria);
            gvIPPharmacy.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIPPharmacy.DataSource = DS;
                gvIPPharmacy.DataBind();
                gvIPPharmacy.Visible = true;

            }
            else
            {
                gvIPPharmacy.DataBind();
            }
        FunEnd: ;

        }
        //-----------------------------

        #endregion

        void BindEMRPTMaster()
        {
            ViewState["EMR_Date"] = "";

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.EMRPTMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                    lblTreatmentPlan.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]);
                else
                    lblTreatmentPlan.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                    lblFollowupNotes.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]);
                else
                    lblFollowupNotes.Text = "";

                ViewState["EMR_Date"] = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]);
            }


        }




        void BindNursingtInstruction()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPC_ID = '" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }

            DS = objCom.PatientInstructionGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursingInsttruction.DataSource = DS;
                gvNursingInsttruction.DataBind();
            }
            else
            {
                gvNursingInsttruction.DataBind();
            }

        FunEnd: ;
        }


        void BindEMRPTMasterGrid()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (txtEMR_PTMaster_FromDate.Text != "")
            {
                Criteria += " AND   EPM_DATE >= CONVERT(datetime,'" + txtEMR_PTMaster_FromDate.Text + "',103)";
            }

            if (txtEMR_PTMaster_ToDate.Text != "")
            {
                Criteria += " AND   EPM_DATE <= CONVERT(datetime,'" + txtEMR_PTMaster_ToDate.Text + "',103)";
            }


            Criteria += " AND EPM_INS_CODE='" + Convert.ToString(ViewState["SubInsCode"]) + "'";
            Criteria += " AND EPM_DR_CODE='" + Convert.ToString(ViewState["InvDrCode"]) + "'";
            Criteria += " AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "'";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.EMRPTMasterGet(Criteria);
            gvEMR_PTMaster.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvEMR_PTMaster.DataSource = DS;
                gvEMR_PTMaster.DataBind();
                gvEMR_PTMaster.Visible = true;
            }
            else
            {
                gvEMR_PTMaster.DataBind();
            }
            string strPatientEMRIDs = "";

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (strPatientEMRIDs != "") strPatientEMRIDs += ",";
                strPatientEMRIDs += "'" + DR["EPM_ID"] + "'";
            }

            ViewState["PatientEMRIDs"] = strPatientEMRIDs;

        }

        void BindIP_PTMasterGrid()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";

            if (txtEMR_PTMaster_FromDate.Text != "")
            {
                Criteria += " AND   IPM_DATE >= CONVERT(datetime,'" + txtEMR_PTMaster_FromDate.Text + "',103)";
            }

            if (txtEMR_PTMaster_ToDate.Text != "")
            {
                Criteria += " AND   IPM_DATE <= CONVERT(datetime,'" + txtEMR_PTMaster_ToDate.Text + "',103)";
            }

            if (Convert.ToString(ViewState["SubInsCode"]) != "" && Convert.ToString(ViewState["SubInsCode"]) != null)
            {
                Criteria += " AND IPM_INS_CODE='" + Convert.ToString(ViewState["SubInsCode"]) + "'";
            }

            Criteria += " AND IPM_DR_CODE='" + Convert.ToString(ViewState["InvDrCode"]) + "'";

            Criteria += " AND IPM_PT_ID='" + txtFileNo.Text.Trim() + "'";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.IPPTMasterGet(Criteria);
            gvIP_PTMaster.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIP_PTMaster.DataSource = DS;
                gvIP_PTMaster.DataBind();
                gvIP_PTMaster.Visible = true;
            }
            else
            {
                gvIP_PTMaster.DataBind();
            }
            string strPatientEMRIDs = "";

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (strPatientEMRIDs != "") strPatientEMRIDs += ",";
                strPatientEMRIDs += "'" + DR["IPM_ID"] + "'";
            }

            ViewState["PatientEMRIDs"] = strPatientEMRIDs;

        }

        void BindLaboratoryResult()
        {


            string Criteria = " 1=1  AND LTRM_STATUS not in ('Cancelled', 'Completed') ";

            DataSet DS = new DataSet();

            LaboratoryBAL objLab = new LaboratoryBAL();
            Criteria += " AND LTRM_PATIENT_ID = '" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DS = objLab.TestReportMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLabResult.DataSource = DS;
                gvLabResult.DataBind();
            }
            else
            {
                gvLabResult.DataBind();
            }
        }

        void BindRadiologyResult()
        {

            DataSet DS = new DataSet();
            //objCom = new CommonBAL();
            //DS = objCom.RadiologyResultGet(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim());

            RadiologyBAL objRad = new RadiologyBAL();

            string Criteria = " 1=1 ";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND RTR_PATIENT_ID='" + txtFileNo.Text.Trim() + "'";


            DS = objRad.TestReportGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadResult.DataSource = DS;
                gvRadResult.DataBind();
            }
            else
            {
                gvRadResult.DataBind();
            }
        }



        void BindWEMR_spS_GetEMRS()
        {
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.EMRID = Convert.ToString(ViewState["INV_EMR_ID"]);

            DS = objCom.WEMR_spS_GetEMRS();
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("FinalDiagnosis") == false && Convert.ToString(DS.Tables[0].Rows[0]["FinalDiagnosis"]) != "")
                    lblFinalDiag.Text = DS.Tables[0].Rows[0]["FinalDiagnosis"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("Plan") == false && Convert.ToString(DS.Tables[0].Rows[0]["Plan"]) != "")
                    lblPlan.Text = DS.Tables[0].Rows[0]["Plan"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("ExaminationType") == false && Convert.ToString(DS.Tables[0].Rows[0]["ExaminationType"]) != "")
                    ViewState["ExaminationType"] = DS.Tables[0].Rows[0]["ExaminationType"].ToString();

            }
            else
            {
                lblFinalDiag.Text = "";
                lblPlan.Text = "";


            }

        FunEnd: ;

        }

        void BindEMRS_PT_DETAILS()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = "1=1";
            Criteria += " and EMRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' and EMRS_SEQNO='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            DS = objCom.EMRS_PT_DETAILSGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EMRS_CPT") == false && Convert.ToString(DS.Tables[0].Rows[0]["EMRS_CPT"]) != "" && txtENMCode.Text == "")
                    txtENMCode.Text = DS.Tables[0].Rows[0]["EMRS_CPT"].ToString();
            }

        }


        void BindWEMR_spS_GetEMCPTInvoice()
        {
            if (txtENMCode.Text.Trim() == "")
            {
                goto FunEnd;
            }
            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.WEMR_spS_GetEMCPTInvoice(Convert.ToString(Session["Branch_ID"]), Convert.ToString(ViewState["INV_EMR_ID"]), txtENMCode.Text.Trim());
            if (DS.Tables.Count > 0)
            {
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvCPT.DataSource = DS;
                    gvCPT.DataBind();
                }
                else
                {
                    gvCPT.DataBind();
                }
            }

        FunEnd: ;
        }


        void BindWEMR_spS_GetEMRSHPI()
        {
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }

            string HPIResult = "", ROSResult = "N/A", PFSHResult = "N/A", FinalHistoryResult = "", PEResult = "N/A", PSResult = "Minimal", DPResult = "Minimal";
            double PSTotal = 0, DPTotal = 0, RiskTotal = 0;
            int HPICount = 0, ROSCount = 0, PFSHCount = 0, PECount = 0, PEOrganCount = 0, TimeDifference = 0;

            string RiskResult = "Minimal", MDMResult = "", CPTCode = "", RiskPPResult = "", RiskDGPResult = "", RiskMOSResult = "", PatientType = "New";

            string Criteria = " 1=1 ";
            DS = new DataSet();
            objCom = new CommonBAL();
            Criteria = " 1=1 ";
            Criteria += " AND ESVD_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            // DS = objCom.WEMR_spS_GetSegmentVisits(Convert.ToString(Session["Branch_ID"]), Convert.ToString(ViewState["INV_EMR_ID"]));
            DS = objCom.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string SubType = "";
                string strValue = "", strType = "", strSubType = "";
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (DS.Tables[0].Rows[i].IsNull("ESVD_VALUE_YES") == false && Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE_YES"]) != "")
                        strValue = Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE_YES"]);

                    if (DS.Tables[0].Rows[i].IsNull("ESVD_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["ESVD_TYPE"]) != "")
                        strType = Convert.ToString(DS.Tables[0].Rows[i]["ESVD_TYPE"]);

                    if (DS.Tables[0].Rows[i].IsNull("ESVD_SUBTYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) != "")
                        strSubType = Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]);


                    if (strValue.Equals("Y") || strValue.Equals("N"))  //if (strValue.Equals("Y"))
                    {
                        if (strType.Equals("HPI"))
                        {
                            HPICount++;
                        }
                        else if (strType.Equals("ROS"))
                        {
                            ROSCount++;
                        }
                        else if (strType.Equals("HIST_PAST") || strType.Equals("HIST_FAMILY") || strType.Equals("HIST_SOCIAL"))
                        {
                            PFSHCount++;
                        }
                        else if (strType.Equals("PE"))
                        {
                            PECount++;
                        }
                        else if (strType.Equals("PE") && (!SubType.Equals(strSubType)))
                        {
                            PEOrganCount++;
                        }
                    }
                    SubType = strSubType;
                }

            }

            DS = new DataSet();
            objCom = new CommonBAL();
            Criteria = " 1=1 ";
            Criteria += " AND ESVD_ID='" + txtFileNo.Text.Trim() + "'";
            // DS = objCom.WEMR_spS_GetSegmentVisits(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim());
            DS = objCom.SegmentVisitDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                string strValue = "", strType = "", strSubType = "";
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (DS.Tables[0].Rows[i].IsNull("ESVD_VALUE_YES") == false && Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE_YES"]) != "")
                        strValue = Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE_YES"]);

                    if (DS.Tables[0].Rows[i].IsNull("ESVD_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["ESVD_TYPE"]) != "")
                        strType = Convert.ToString(DS.Tables[0].Rows[i]["ESVD_TYPE"]);

                    if (DS.Tables[0].Rows[i].IsNull("ESVD_SUBTYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) != "")
                        strSubType = Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]);


                    if (strValue.Equals("Y") || strValue.Equals("N"))  //if (strValue.Equals("Y"))
                    {
                        if (strType.Equals("HIST_PAST") || strType.Equals("HIST_FAMILY") || strType.Equals("HIST_SOCIAL"))
                        {
                            PFSHCount++;
                        }

                    }

                }

            }


            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.WEMR_spS_GetEMRSDatas(Convert.ToString(Session["Branch_ID"]), Convert.ToString(ViewState["INV_EMR_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {

                string strType = "", strSubType = "", strPoints = ""; ;
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (DS.Tables[0].Rows[i].IsNull("Type") == false && Convert.ToString(DS.Tables[0].Rows[i]["Type"]) != "")
                        strType = Convert.ToString(DS.Tables[0].Rows[i]["Type"]);

                    if (DS.Tables[0].Rows[i].IsNull("SubType") == false && Convert.ToString(DS.Tables[0].Rows[i]["SubType"]) != "")
                        strSubType = Convert.ToString(DS.Tables[0].Rows[i]["SubType"]);

                    if (DS.Tables[0].Rows[i].IsNull("Points") == false && Convert.ToString(DS.Tables[0].Rows[i]["Points"]) != "")
                        strPoints = Convert.ToString(DS.Tables[0].Rows[i]["Points"]);


                    if (strType.Equals("EMRS_PS"))
                    {
                        PSTotal += Double.Parse(strPoints);
                    }
                    else if (strType.Equals("EMRS_DP"))
                    {
                        DPTotal += Double.Parse(strPoints == null ? "0" : strPoints);
                    }
                    else if (strType.Equals("RISK_PPS"))
                    {
                        RiskPPResult = strSubType;
                    }
                    else if (strType.Equals("RISK_DGP"))
                    {
                        RiskDGPResult = strSubType;
                    }
                    else if (strType.Equals("RISK_MOS"))
                    {
                        RiskMOSResult = strSubType;
                    }
                }


            }

            string ChronicStatus = "", LimitedHistory = "";


            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.WEMR_spS_GetEMRSHPI(Convert.ToString(Session["Branch_ID"]), Convert.ToString(ViewState["INV_EMR_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("ChronicStatus") == false && Convert.ToString(DS.Tables[0].Rows[0]["ChronicStatus"]) != "")
                    ChronicStatus = DS.Tables[0].Rows[0]["ChronicStatus"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("LimitedHistory") == false && Convert.ToString(DS.Tables[0].Rows[0]["LimitedHistory"]) != "")
                    LimitedHistory = DS.Tables[0].Rows[0]["LimitedHistory"].ToString();




                if (HPICount >= 4 || (ChronicStatus != null && ChronicStatus.Equals("1")) || (LimitedHistory != null && LimitedHistory.Equals("1")))
                {
                    HPIResult = "Extended";
                }
                else if (HPICount >= 1 && HPICount <= 3) // please verify it in en coding 
                {
                    HPIResult = "Brief";
                }

                lblHPIResult.Text = HPIResult;
            }
            else
            {
                lblHPIResult.Text = "";


            }



            // result for ROS
            if (ROSCount == 1)
            {
                ROSResult = "Pertinent to Problem";
            }
            else if (ROSCount >= 2 && ROSCount <= 9)
            {
                ROSResult = "Extended";
            }
            else if (ROSCount >= 10)
            {
                ROSResult = "Complete";
            }

            // result for PFSH
            if (PFSHCount == 1)
            {
                PFSHResult = "Pertinent to Problem";
            }
            else if (PFSHCount > 1)
            {
                PFSHResult = "Complete";
            }

            // result for Level of History Documented
            if (HPIResult.Equals("Brief") && ROSResult.Equals("N/A") && PFSHResult.Equals("N/A"))
            {
                FinalHistoryResult = "Problem Focused";
            }
            else if (HPIResult.Equals("Brief") && !ROSResult.Equals("N/A"))
            {
                FinalHistoryResult = "Expandable Problem Focused";
            }
            else if (HPIResult.Equals("Brief") && ROSResult.Equals("N/A") && PFSHResult.Equals("N/A"))
            {
                FinalHistoryResult = "Problem Focused";
            }
            else if (HPIResult.Equals("Extended") && ROSResult.Equals("Extended") && PFSHResult.Equals("Pertinent"))
            {
                FinalHistoryResult = "Detailed";
            }
            else if (HPIResult.Equals("Extended") && ROSResult.Equals("Complete") && PFSHResult.Equals("Complete"))
            {
                FinalHistoryResult = "Comprehensive";
            }


            // --------------------------------------------
            string ExaminationType = Convert.ToString(ViewState["ExaminationType"]);
            // Examination Level result
            if (ExaminationType == null)
            {
                ExaminationType = "1997";
            }
            if (ExaminationType.Equals("1995"))
            {
                if (PECount == 1)
                {
                    PEResult = "Problem Focused";
                }
                else if (PECount >= 2 && PECount <= 7)
                {
                    PEResult = "Detailed";
                }
                else if (PECount >= 8)
                {
                    PEResult = "Comprehensive";
                }
            }
            else if (ExaminationType.Equals("1997"))
            {
                if (PECount >= 1 && PECount <= 5)
                {
                    PEResult = "Problem Focused";
                }
                else if (PECount >= 6 && PECount <= 11)
                {
                    PEResult = "Expanded Problem Focused";
                }
                else if (PECount >= 12 && PECount <= 17)
                {
                    PEResult = "Detailed";
                }
                else if (PECount >= 18)
                {
                    PEResult = "Comprehensive";
                }

                if (PEOrganCount >= 9 && (PECount == 18))
                {
                    PEResult = "Comprehensive";
                }
                else if (PEOrganCount >= 2 && (PECount == 12 && PECount <= 17))
                {
                    PEResult = "Detailed";
                }
            }





            lblROSResult.Text = ROSResult;
            lblPFSHResult.Text = PFSHResult;
            lblFinalHistoryResult.Text = FinalHistoryResult;
            lblPEResult.Text = PEResult;

            //--------------------------------------------------------------------------------------------------------



            // Problem Focused
            if (PSTotal == 1)
            {
                PSResult = "Minimal";
            }
            else if (PSTotal == 2)
            {
                PSResult = "Limited";
            }
            else if (PSTotal == 3)
            {
                PSResult = "Multiple";
            }
            else if (PSTotal >= 4)
            {
                PSResult = "Extensive";
            }


            // Data Reviewed
            if (DPTotal <= 1)
            {
                DPResult = "Minimal";
            }
            else if (DPTotal == 2)
            {
                DPResult = "Limited";
            }
            else if (DPTotal == 3)
            {
                DPResult = "Multiple";
            }
            else if (DPTotal >= 4)
            {
                DPResult = "Extensive";
            }



            // Risk Assessment result
            if (RiskPPResult.Equals("HIGH") || RiskDGPResult.Equals("HIGH") || RiskMOSResult.Equals("HIGH"))
            {
                RiskResult = "High";
            }
            else if (RiskPPResult.Equals("MOD") || RiskDGPResult.Equals("MOD") || RiskMOSResult.Equals("MOD"))
            {
                RiskResult = "Moderate";
            }
            else if (RiskPPResult.Equals("LOW") || RiskDGPResult.Equals("LOW") || RiskMOSResult.Equals("LOW"))
            {
                RiskResult = "Low";
            }
            else if (RiskPPResult.Equals("MIN") || RiskDGPResult.Equals("MIN") || RiskMOSResult.Equals("MIN"))
            {
                RiskResult = "Minmal";
            }
            /*else
            {
                RiskResult = "Minimal";
            }*/

            // calculating MDM
            if (PSResult.Equals("Minimal") || DPResult.Equals("Minimal") || RiskResult.Equals("Minimal"))
            {
                MDMResult = "Straight Forward";
            }
            else if (PSResult.Equals("Limited") || DPResult.Equals("Limited") || RiskResult.Equals("Low"))
            {
                MDMResult = "Low Complexity";
            }
            else if (PSResult.Equals("Multiple") || DPResult.Equals("Multiple") || RiskResult.Equals("Moderate"))
            {
                MDMResult = "Moderate Complexity";
            }
            else if (PSResult.Equals("Extensive") || DPResult.Equals("Extensive") || RiskResult.Equals("High"))
            {
                MDMResult = "High Complexity";
            }

            lblPSResult.Text = PSResult;
            lblDPResult.Text = DPResult;
            lblRiskResult.Text = RiskResult;
            lblMDMResult.Text = MDMResult;
        FunEnd: ;
        }


        void BuildeInvoiceClaims()
        {
            string Criteria = "1=2";
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            objeClaimCoder = new eClaimCoderBAL();
            ds = objeClaimCoder.EclaimCoderInvoiceClaimsGet(Criteria);

            ViewState["InvoiceClaims"] = ds.Tables[0];
        }

        void BindInvoiceClaimDtls()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND  HIC_INVOICE_ID='" + hidInvoice.Value + "'";

            dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.InvoiceClaimDtlsGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["InvoiceClaims"];

                    DataRow objrow;
                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();
                        objrow["HEIC_HEIM_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        objrow["HEIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        objrow["HEIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        objrow["HEIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        objrow["HEIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HEIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HEIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);
                        objrow["HEIC_PRIMARY"] = 1;
                        objrow["HEIC_PRIMARYDesc"] = "Primary";


                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();
                        objrow["HEIC_HEIM_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        objrow["HEIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        objrow["HEIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        objrow["HEIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        objrow["HEIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HEIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HEIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HEIC_PRIMARY"] = 0;
                        objrow["HEIC_PRIMARYDesc"] = "Secondary";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();
                                objrow["HEIC_HEIM_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                objrow["HEIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                objrow["HEIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                objrow["HEIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                objrow["HEIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                                objrow["HEIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HEIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HEIC_PRIMARY"] = 0;
                                objrow["HEIC_PRIMARYDesc"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }




                }

                ViewState["InvoiceClaims"] = DT;

            }
        }

        void BindTempInvoiceClaims()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceClaims"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceClaims.DataSource = DT;
                gvInvoiceClaims.DataBind();

            }
            else
            {
                gvInvoiceClaims.DataBind();
            }

        }

        void BuildeInvoiceTrans()
        {
            string Criteria = "1=2";
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            objeClaimCoder = new eClaimCoderBAL();
            ds = objeClaimCoder.EclaimCoderInvoiceTransGet(Criteria);

            ViewState["InvoiceTrans"] = ds.Tables[0];
        }

        void BindInvoiceTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HIT_INVOICE_ID='" + hidInvoice.Value + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.InvoiceTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["InvoiceTrans"];

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["HEIT_SERV_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_SERV_CODE"]);
                    objrow["HEIT_SERV_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_DESCRIPTION"]);
                    objrow["HEIT_CAT_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_CAT_ID"]);

                    objrow["HEIT_FEE"] = Convert.ToString(DS.Tables[0].Rows[i].IsNull("HIT_FEE") == false ? Convert.ToString(DS.Tables[0].Rows[i]["HIT_FEE"]) : "0");
                    objrow["HEIT_AMOUNT"] = Convert.ToString(DS.Tables[0].Rows[i].IsNull("HIT_AMOUNT") == false ? Convert.ToString(DS.Tables[0].Rows[i]["HIT_AMOUNT"]) : "0");
                    objrow["HEIT_NET_AMOUNT"] = Convert.ToString(DS.Tables[0].Rows[i].IsNull("HIT_COMP_TOTAL") == false ? Convert.ToString(DS.Tables[0].Rows[i]["HIT_COMP_TOTAL"]) : "0");
                    objrow["HEIT_QTY"] = Convert.ToString(DS.Tables[0].Rows[i].IsNull("HIT_QTY") == false ? Convert.ToString(DS.Tables[0].Rows[i]["HIT_QTY"]) : "0");
                    objrow["HEIT_PT_AMOUNT"] = Convert.ToString(DS.Tables[0].Rows[i].IsNull("HIT_PT_AMOUNT") == false ? Convert.ToString(DS.Tables[0].Rows[i]["HIT_PT_AMOUNT"]) : "0");

                    objrow["HEIT_DR_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_DR_CODE"]);
                    objrow["HEIT_DR_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_DR_NAME"]);
                    objrow["HEIT_HAAD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_HAAD_CODE"]);

                    objrow["HEIT_AUTHORIZATIONID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIT_AUTHORIZATIONID"]);
                    objrow["HEIT_OBS_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIO_TYPE"]);
                    objrow["HEIT_OBS_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIO_CODE"]);
                    objrow["HEIT_OBS_VALUE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIO_VALUE"]);
                    objrow["HEIT_OBS_VALUE_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIO_VALUETYPE"]);



                    DT.Rows.Add(objrow);
                }

                ViewState["InvoiceTrans"] = DT;

            }

        }


        void BindTempInvoiceTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceTrans"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceTrans.DataSource = DT;
                gvInvoiceTrans.DataBind();

            }
            else
            {
                gvInvoiceTrans.DataBind();
            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }
            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "0";



        }

        void BindAttachments()
        {
            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " and ESF_PT_ID='" + txtFileNo.Text.Trim() + "'";
            DS = objCom.EMRScanFileGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAttachments.DataSource = DS;
                gvAttachments.DataBind();
            }
            else
            {
                gvAttachments.DataBind();
            }

        }



        void GetServiceMasterName(string ServCode, out string ServID, out string ServName)
        {
            ServID = "";
            ServName = "";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            txtServName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
                ServName = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                hidCatID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HSM_CAT_ID"]);
                hidCatType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
            }

        }

        void GetHaadServDtls(string ServCode, out string ServType)
        {
            ServType = "3";


            DataSet DS = new DataSet();
            dboperations objDBO = new dboperations();

            string Criteria = " 1=1 ";
            Criteria += " AND HHS_CODE='" + ServCode + "'";
            DS = objDBO.HaadServiceGet(Criteria, "", "100");
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServType = Convert.ToString(DS.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
            }



        }

        void GetCompanyAgrDtls(string ServCode, string CompanyID, out decimal ServPrice)
        {

            ServPrice = 0;

            DataSet DS = new DataSet();
            dboperations objDBO = new dboperations();

            string Criteria = " 1=1 ";
            Criteria += " AND HAS_SERV_ID='" + ServCode + "' AND HAS_COMP_ID='" + CompanyID + "'";
            DS = objDBO.AgrementServiceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
                {
                    ServPrice = Convert.ToDecimal(DS.Tables[0].Rows[0]["HAS_NETAMOUNT"]);
                }
            }



        }

        void ClearPTDtls()
        {
            txtFileNo.Text = "";
            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";

            txtDOB.Text = "";
            txtAge.Text = "";
            txtMonth.Text = "";

            txtSex.Text = "";

            txtAddress.Text = "";
            txtPoBox.Text = "";

            txtArea.Text = "";
            txtCity.Text = "";
            txtNationality.Text = "";

            txtPhone1.Text = "";
            txtMobile1.Text = "";
            txtMobile2.Text = "";


            lblIDCaption.Text = "EmiratesId";
            txtEmiratesID.Text = "";



            txtProviderName.Text = "";
            txtPolicyNo.Text = "";
            txtPolicyType.Text = "";
            txtDrName.Text = "";
            txtOrderingClinician.Text = "";

            Session["Photo"] = "";
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";
        }



        void ClearResults()
        {
            gvLabResult.DataBind();
            gvRadResult.DataBind();
        }

        void ClearEMReviewsheet()
        {
            lblFinalDiag.Text = "";
            lblPlan.Text = "";
            lblHPIResult.Text = "";
            lblROSResult.Text = "";
            lblPFSHResult.Text = "";
            lblFinalHistoryResult.Text = "";
            lblPEResult.Text = "";
            lblPSResult.Text = "";
            lblDPResult.Text = "";
            lblRiskResult.Text = "";
            lblMDMResult.Text = "";

        }


        void Clear()
        {

            hidInvoice.Value = "";

            txtProviderName.Text = "";
            txtPolicyNo.Text = "";
            txtPolicyType.Text = "";
            txtDrName.Text = "";
            txtOrderingClinician.Text = "";

            txtBillAmt.Text = "";
            txtClaimAmt.Text = "";
            txtHospDisc.Text = "";
            txtDedAmt.Text = "";
            txtCoInsAmt.Text = "";
            txtSplDisc.Text = "";

            ViewState["BRANCH_ID"] = "";
            ViewState["INV_EMR_ID"] = "";
            ViewState["BilToCode"] = "";
            ViewState["SubInsCode"] = "";
            ViewState["InvDrCode"] = "";
            ViewState["InvDate"] = "";

            ViewState["IAS_ADMISSION_NO"] = "";


            gvInvoiceTrans.DataBind();
            gvInvoiceClaims.DataBind();

            ClearPTDtls();

            ClearResults();
            ClearEMReviewsheet();
            ClearService();
            ClearClaims();



            gvVisit.DataBind();
            gvVisit.Visible = false;

            gvDiagnosis.DataBind();
            gvDiagnosis.Visible = false;

            gvProcedure.DataBind();
            gvProcedure.Visible = false;


            gvRadiology.DataBind();
            gvRadiology.Visible = false;

            gvLaboratory.DataBind();
            gvLaboratory.Visible = false;


            gvPharmacy.DataBind();
            gvPharmacy.Visible = false;

            gvEMR_PTMaster.DataBind();
            gvEMR_PTMaster.Visible = false;


            // ----------------------------------------------

            gvIPAdmission.DataBind();
            gvIPAdmission.Visible = false;

            gvIPDiagnosis.DataBind();
            gvIPDiagnosis.Visible = false;


            gvIPProcedure.DataBind();
            gvIPProcedure.Visible = false;

            gvIPRadiology.DataBind();
            gvIPRadiology.Visible = false;

            gvIPLaboratory.DataBind();
            gvIPLaboratory.Visible = false;

            gvIPPharmacy.DataBind();
            gvIPPharmacy.Visible = false;

            gvIP_PTMaster.DataBind();
            gvIP_PTMaster.Visible = false;


            txtWardNo.Text = "";
            txtRoomNo.Text = "";
            txtBedNo.Text = "";


            txtDrgCode.Text = "";
            txtDRGAmount.Text = "";
            txtPriorAuthoID.Text = "";


            txtLenthOfStay.Text = "";



        }

        void ClearClaims()
        {
            txtDiagCode.Text = "";
            txtDiagName.Text = "";
        }

        void ClearService()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            txtQty.Text = "1";
            drpDoctor.SelectedIndex = 0;
            hidCatID.Value = "";
            hidCatType.Value = "";
            hidHaadID.Value = "";

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='CODVERIFI' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnCoderVerify.Enabled = false;
                btnClear.Enabled = false;
                btnInvDiagAdd.Enabled = false;
                btnServiceAdd.Enabled = false;

            }


            if (strPermission == "7")
            {

                btnCoderVerify.Enabled = false;
                btnClear.Enabled = false;
                btnInvDiagAdd.Enabled = false;
                btnServiceAdd.Enabled = false;




            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }

        void BindReferral()
        {
            if (Convert.ToString(ViewState["INV_EMR_ID"]) == "")
            {
                goto FunEnd;
            }

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " and EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID=" + Convert.ToString(ViewState["INV_EMR_ID"]);
            DS = objCom.EMRPTReferenceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                gvReferral.DataSource = DS;
                gvReferral.DataBind();

            }

        FunEnd: ;
        }



        Boolean CheckSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_COMP_ID Like '" + Convert.ToString(ViewState["SubInsCode"]) + "' and  HCM_COMP_ID !=  HCM_BILL_CODE";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                return true;
            }

            return false;


            ds.Dispose();

        }

        string XMLFileWrite(string DispositionFlag)
        {

            // string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strPath = GlobalValues.Shafafiya_EClaim_FilePath;
            string strFileName = "", strFileId;
            string Criteria;

            string strDate = "", strTime = "";

            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");

            Criteria = " HIM_INVOICE_ID='" + hidInvoice.Value + "'";   //ViewState["Criteria"].ToString();
            dbo = new dboperations();

            Boolean bolIsSubCompany = false;
            bolIsSubCompany = CheckSubCompany();



            DataSet DS1 = new DataSet();
            if (Convert.ToString(bolIsSubCompany).ToLower() == "false")
            {
                DS1 = dbo.GetEClaimViewGroupBy(Criteria);
            }
            else
            {
                DS1 = dbo.GetEClaimSubViewGroupBy(Criteria);
            }



            Boolean IsError = false;
            ViewState["LogFileName"] = "EClaimErrorLog" + strTimeStamp + ".txt";


            //DS = new DataSet();

            //string    ClaimCriteria = " ID='" + hidInvoice.Value + "'";  
            //if (Convert.ToString(bolIsSubCompany).ToLower() == "false")
            //{
            //    DS = dbo.GetEClaimView(ClaimCriteria);
            //}
            //else
            //{
            //    DS = dbo.GetEClaimSubView(ClaimCriteria);
            //}


            if (DS1.Tables[0].Rows.Count <= 0)
            {
                goto ClaimEnd;

            }


            if (DS1.Tables[0].Rows[0].IsNull("ProviderId") == true)
            {
                IsError = true;
                TextFileWriting("Invalid or blank SenderID");
                goto ClaimEnd;
            }


            if (DS1.Tables[0].Rows[0].IsNull("CompId") == true)
            {
                IsError = true;
                TextFileWriting("Invalid or blank ReceiverID");
                goto ClaimEnd;
            }
            StringBuilder strData = new StringBuilder();

            // date.SetAttribute("modified", DateTime.Now.ToString());
            XmlDocument XD = new XmlDocument();
            XD.CreateXmlDeclaration("1.0", "utf-8", "yes");
            XD.CreateProcessingInstruction("xml", "version='1.0' encoding='utf-8'");



            string strPayerID = "", InvoiceID = "";
            Int32 ActCount = 0;


            strPayerID = Convert.ToString(DS1.Tables[0].Rows[0]["PayerID"]);
            InvoiceID = Convert.ToString(DS1.Tables[0].Rows[0]["ID"]);
            strData.Append("<?xml version='1.0' encoding='utf-8' ?>");


            if (ViewState["CUST_VALUE"].ToString() == "DXB")
            {
                strData.Append("<Claim.Submission xmlns:tns='http://www.eclaimlink.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='http://www.eclaimlink.ae/DataDictionary/CommonTypes/ClaimSubmission.xsd'>");
            }
            else
            {
                strData.Append(@"<Claim.Submission xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='https://www.haad.ae/DataDictionary/CommonTypes/PriorRequest.xsd'> ");

                //strData.Append("<Claim.Submission xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='https://www.haad.ae/DataDictionary/CommonTypes/PriorRequest.xsd'>");

            }

            strData.Append("<Header>");

            strData.Append("<SenderID>" + GlobalValues.FacilityID + "</SenderID>");
            strData.Append("<ReceiverID>" + Convert.ToString(DS1.Tables[0].Rows[0]["ReceiverID"]) + "</ReceiverID>");
            strData.Append("<TransactionDate>" + strDate + "</TransactionDate>");
            strData.Append("<RecordCount>" + Convert.ToString(DS1.Tables[0].Rows.Count) + "</RecordCount>");
            strData.Append("<DispositionFlag>" + DispositionFlag + "</DispositionFlag>");
            strData.Append("</Header>");

            strFileId = GlobalValues.FacilityID + " " + strPayerID + " " + strTimeStamp + InvoiceID;


            for (Int32 i = 0; i < DS1.Tables[0].Rows.Count; i++)
            {

                strData.Append("<Claim>");
                if (DS1.Tables[0].Rows[0].IsNull("MemberID") == true)
                {
                    IsError = true;
                    TextFileWriting("Invalid or blank MemberID");
                    // goto ClaimEnd;
                }





                strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + "</ID>");
                strData.Append("<IDPayer>" + Convert.ToString(DS1.Tables[0].Rows[i]["IDPayer"]) + "</IDPayer>");
                strData.Append("<MemberID>" + Convert.ToString(DS1.Tables[0].Rows[i]["MemberID"]) + "</MemberID>");
                strData.Append("<PayerID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PayerID"]) + "</PayerID>");
                strData.Append("<ProviderID>" + GlobalValues.FacilityID + "</ProviderID>");
                strData.Append("<EmiratesIDNumber>" + Convert.ToString(DS1.Tables[0].Rows[i]["EmiratesIDNumber"]) + "</EmiratesIDNumber>");
                strData.Append("<Gross>" + Convert.ToString(DS1.Tables[0].Rows[i]["Gross"]) + "</Gross>");
                strData.Append("<PatientShare>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientShare"]) + "</PatientShare>");
                strData.Append("<Net>" + Convert.ToString(DS1.Tables[0].Rows[i]["Net"]) + "</Net>");

                strData.Append("<Encounter>");
                strData.Append("<FacilityID>" + GlobalValues.FacilityID + "</FacilityID>");
                strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type"]) + "</Type>");
                strData.Append("<PatientID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientID"]) + "</PatientID>");
                strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["Start"])).ToString("dd/MM/yyyy HH:mm") + "</Start>");
                strData.Append("<End>" + Convert.ToDateTime(DS1.Tables[0].Rows[i]["End"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</End>");
                strData.Append("<StartType>" + Convert.ToString(DS1.Tables[0].Rows[i]["StartType"]) + "</StartType>");
                strData.Append("<EndType>" + Convert.ToString(DS1.Tables[0].Rows[i]["EndType"]) + "</EndType>");
                strData.Append("</Encounter>");



                Boolean isDiagnosis = false;

                if (DS1.Tables[0].Rows[i].IsNull("Type1") == false && DS1.Tables[0].Rows[i].IsNull("Code") == false && DS1.Tables[0].Rows[i]["Code"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type1"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code"]) + "</Code>");
                    strData.Append("</Diagnosis>");

                }



                if (DS1.Tables[0].Rows[i].IsNull("Type3") == false && DS1.Tables[0].Rows[i].IsNull("Code3") == false && DS1.Tables[0].Rows[i]["Code3"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type3"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code3"]) + "</Code>");
                    strData.Append("</Diagnosis>");

                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS2") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS2") == false && DS1.Tables[0].Rows[i]["ICDCodeS2"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS2"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS2"]) + "</Code>");
                    strData.Append("</Diagnosis>");

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS3") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS3") == false && DS1.Tables[0].Rows[i]["ICDCodeS3"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS3"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS3"]) + "</Code>");
                    strData.Append("</Diagnosis>");

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS4") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS4") == false && DS1.Tables[0].Rows[i]["ICDCodeS4".ToString()] != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS4"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS4"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS5") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS5") == false && DS1.Tables[0].Rows[i]["ICDCodeS5"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS5"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS5"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS6") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS6") == false && DS1.Tables[0].Rows[i]["ICDCodeS6"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS6"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS6"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS7") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS7") == false && DS1.Tables[0].Rows[i]["ICDCodeS7"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS7"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS7"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS8") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS8") == false && DS1.Tables[0].Rows[i]["ICDCodeS8"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS8"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS8"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS9") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS9") == false && DS1.Tables[0].Rows[i]["ICDCodeS9"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS9"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS9"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS10") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS10") == false && DS1.Tables[0].Rows[i]["ICDCodeS10"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS10"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS10"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS11") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS11") == false && DS1.Tables[0].Rows[i]["ICDCodeS11"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS11"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS11"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS12") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS12") == false && DS1.Tables[0].Rows[i]["ICDCodeS12"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS12"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS12"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS13") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS13") == false && DS1.Tables[0].Rows[i]["ICDCodeS13"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS13"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS13"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS14") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS14") == false && DS1.Tables[0].Rows[i]["ICDCodeS14"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS14"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS14"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS15") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS15") == false && DS1.Tables[0].Rows[i]["ICDCodeS15"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS15"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS15"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS16") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS16") == false && DS1.Tables[0].Rows[i]["ICDCodeS16"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS16"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS16"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS17") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS17") == false && DS1.Tables[0].Rows[i]["ICDCodeS17"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS17"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS17"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS18") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS18") == false && DS1.Tables[0].Rows[i]["ICDCodeS18"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS18"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS18"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS19") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS19") == false && DS1.Tables[0].Rows[i]["ICDCodeS19"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS19"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS19"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS20") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS20") == false && DS1.Tables[0].Rows[i]["ICDCodeS20"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS20"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS20"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS21") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS21") == false && DS1.Tables[0].Rows[i]["ICDCodeS21"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS21"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS21"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS22") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS22") == false && DS1.Tables[0].Rows[i]["ICDCodeS22"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS22"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS22"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS23") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS23") == false && DS1.Tables[0].Rows[i]["ICDCodeS23"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS23"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS23"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS24") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS24") == false && DS1.Tables[0].Rows[i]["ICDCodeS24"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS24"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS24"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS25") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS25") == false && DS1.Tables[0].Rows[i]["ICDCodeS25"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS25"].ToString() + "</Type>");
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS25"].ToString() + "</Code>");
                    strData.Append("</Diagnosis>");
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS26") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS26") == false && DS1.Tables[0].Rows[i]["ICDCodeS26"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS26"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS26"]) + "</Code>");
                    strData.Append("</Diagnosis>");
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDA") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeA") == false && DS1.Tables[0].Rows[i]["ICDCodeA"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>");
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDA"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeA"]) + "</Code>");
                    strData.Append("</Diagnosis>");

                }


                if (isDiagnosis == false)
                {
                    IsError = true;
                    TextFileWriting("Invalid or blank Diagnosis  (InvoiceNo=" + DS1.Tables[0].Rows[i]["ID"].ToString() + ")");
                    // goto ClaimEnd;
                }




                //  Criteria = " HIM_INVOICE_ID=" + DS1.Tables[0].Rows[i]["ID"].ToString();
                Criteria = " HMS_INVOICE_MASTER.HIM_INVOICE_ID='" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + "'";
                dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.GetEClaimView(Criteria);


                //DataTable DTActivity = new DataTable();
                //DataRow[] DRActivity;
                //DRActivity = DS1.Tables[0].Select("ID=" + DS.Tables[0].Rows[i]["ID"].ToString());



                for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                {
                    ActCount = ActCount + 1;

                    if (DS.Tables[0].Rows[j].IsNull("Clinician") == true)
                    {
                        IsError = true;
                        TextFileWriting("Invalid or blank Clinician  (InvoiceNo=" + DS.Tables[0].Rows[j]["ID"].ToString() + ")");
                        // goto ClaimEnd;
                    }

                    if (DS.Tables[0].Rows[j].IsNull("Code2") == true)
                    {
                        IsError = true;
                        TextFileWriting("Invalid or blank  Activity Code (InvoiceNo=" + DS.Tables[0].Rows[j]["ID"].ToString() + ")");
                        // goto ClaimEnd;
                    }

                    strData.Append("<Activity>");
                    strData.Append("<ID>" + ActCount + "</ID>");
                    strData.Append("<Start>" + Convert.ToDateTime(DS.Tables[0].Rows[j]["Start1"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</Start>");
                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["Type2"]) + "</Type>");
                    strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["Code2"]) + "</Code>");
                    strData.Append("<Quantity>" + Convert.ToString(DS.Tables[0].Rows[j]["Quantity"]) + "</Quantity>");
                    strData.Append("<Net>" + Convert.ToString(DS.Tables[0].Rows[j]["Net1"]) + "</Net>");
                    if (ViewState["CUST_VALUE"].ToString() != "DXB")
                    {
                        strData.Append("<OrderingClinician>" + Convert.ToString(DS.Tables[0].Rows[j]["OrderingClinician"]) + "</OrderingClinician>");
                    }
                    strData.Append("<Clinician>" + Convert.ToString(DS.Tables[0].Rows[j]["Clinician"]) + "</Clinician>");




                    if (DS.Tables[0].Rows[j].IsNull("PriorAuthorizationId") == false && DS.Tables[0].Rows[j]["PriorAuthorizationId"].ToString() != "")
                    {
                        strData.Append("<PriorAuthorizationId>" + Convert.ToString(DS.Tables[0].Rows[j]["PriorAuthorizationId"]) + "</PriorAuthorizationId>");
                    }
                    //else
                    //{
                    //    strData.Append("<PriorAuthorizationId></PriorAuthorizationId>");
                    //}




                    if (DS.Tables[0].Rows[j].IsNull("ObservType") == false && DS.Tables[0].Rows[j]["ObservType"].ToString() != "")
                    {
                        if (ViewState["CUST_VALUE"].ToString() == "DXB")
                        {
                            string strObsValue = Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]);

                            string[] arrObsValue = strObsValue.Split('/');
                            if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80061" && arrObsValue.Length > 2)
                            {

                                strData.Append("<Observation>");

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>CHOL</Code>");
                                }
                                else
                                {
                                    strData.Append("<Code></Code>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[0] + "</Value>");
                                }
                                else
                                {
                                    strData.Append("<Value></Value>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>");
                                }


                                strData.Append("</Observation>");

                                strData.Append("<Observation>");

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>HDL</Code>");
                                }
                                else
                                {
                                    strData.Append("<Code></Code>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[1] + "</Value>");
                                }
                                else
                                {
                                    strData.Append("<Value></Value>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>");
                                }


                                strData.Append("</Observation>");

                                strData.Append("<Observation>");

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>LDL</Code>");
                                }
                                else
                                {
                                    strData.Append("<Code></Code>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[2] + "</Value>");
                                }
                                else
                                {
                                    strData.Append("<Value></Value>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>");
                                }


                                strData.Append("</Observation>");

                                strData.Append("<Observation>");

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>TRG</Code>");
                                }
                                else
                                {
                                    strData.Append("<Code></Code>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[3] + "</Value>");
                                }
                                else
                                {
                                    strData.Append("<Value></Value>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>");
                                }


                                strData.Append("</Observation>");


                            }

                            string[] arrBPObsValue = strObsValue.Split('/');
                            if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_SERV_TYPE"]).Trim() == "C" && arrBPObsValue.Length > 1)
                            {
                                strData.Append("<Observation>");

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>BS</Code>");
                                }
                                else
                                {
                                    strData.Append("<Code></Code>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrBPObsValue[0] + "</Value>");
                                }
                                else
                                {
                                    strData.Append("<Value></Value>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>");
                                }


                                strData.Append("</Observation>");

                                strData.Append("<Observation>");

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>BD</Code>");
                                }
                                else
                                {
                                    strData.Append("<Code></Code>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrBPObsValue[1] + "</Value>");
                                }
                                else
                                {
                                    strData.Append("<Value></Value>");
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>");
                                }


                                strData.Append("</Observation>");

                            }


                        }
                        else
                        {
                            strData.Append("<Observation>");

                            strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>");
                            if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                            {
                                strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]) + "</Code>");
                            }
                            else
                            {
                                strData.Append("<Code></Code>");
                            }
                            if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                            {
                                strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>");
                            }
                            else
                            {
                                strData.Append("<Value></Value>");
                            }
                            if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                            {
                                strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>");
                            }
                            else
                            {
                                strData.Append("<ValueType></ValueType>");
                            }


                            strData.Append("</Observation>");
                        }
                    }

                    strData.Append("</Activity>");



                    // }



                }
                if (DS1.Tables[0].Rows[i].IsNull("PackageName") == false && DS1.Tables[0].Rows[i]["PackageName"].ToString() != "")
                {
                    strData.Append("<Contract>");
                    strData.Append("<PackageName>" + DS1.Tables[0].Rows[i]["PackageName"].ToString() + "</PackageName>");
                    strData.Append("</Contract>");
                }
                strData.Append("</Claim>");
            }
            strData.Append("</Claim.Submission>");

            if (IsError == false)
            {
                strFileName = strPath + strFileId + ".XML";

                StreamWriter oWrite;
                oWrite = File.CreateText(strFileName);
                oWrite.WriteLine(strData);

                oWrite.Close();
                lblStatus.Text = "E-Claim Created - File Name is  " + strFileId + ".XML";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                /* WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");
                  string errorMessage;

                  byte[] fileContent, errorReport;
                  fileContent = System.Text.Encoding.UTF8.GetBytes(Convert.ToString(strData));
                //////  soapclient.UploadTransaction(GlobalValues.Shafafiya_LoginID, GlobalValues.Shafafiya_LoginPassword, fileContent, strFileId + ".XML", out errorMessage, out errorReport);
              

                  Console.WriteLine(errorMessage);
                  if (!errorMessage.Equals("") && errorReport != null && errorReport.Length > 0)
                  {
                      string strErrorPath = GlobalValues.Shafafiya_ErrorFilePath;
                      string strErrorFileName = strErrorPath + strFileId + ".zip";

                      using (FileStream fs = new FileStream(strErrorFileName, FileMode.Create))
                      {
                          fs.Write(errorReport, 0, errorReport.Length);
                      }

                  }
                  else
                  {
                    
                  }
                  */

            }
            else
            {
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('E-Claim Not Created, Check the Log File !');", true);
                lblStatus.Text = "E-Claim Not Created, Check the Log File  - File Name is " + strFileId + ".txt";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        ClaimEnd: ;

            return strFileName;

        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID =  HCM_BILL_CODE ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID =  HCM_BILL_CODE ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID  like '" + prefixText + "%' ";
            DS = dbo.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            DS = dbo.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }




        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisID(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_ID  like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_DESCRIPTION   like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion


        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            //if (gvEMR_PTMaster.Rows.Count > 0)
            //{
            //    if (Convert.ToString(ViewState["EMRSelectIndex"]) != "")
            //    {
            //        gvEMR_PTMaster.Rows[Convert.ToInt16(ViewState["EMRSelectIndex"])].BackColor = System.Drawing.Color.FromName("#c5e26d");
            //    }
            //}

            if (!IsPostBack)
            {
                TextFileWriting("test data");

                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {

                    ViewState["INV_EMR_ID"] = "";
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");


                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    string PatientId = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);

                    if (PatientId != " " && PatientId != null)
                    {
                        txtSrcFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();
                    }
                    BindDoctor();
                    BindScreenCustomization();
                    // BindVisit();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.Page_Load");
                    TextFileWriting(ex.Message.ToString());

                }
            }
        }

        protected void btnInvFind_Click(object sender, EventArgs e)
        {
            try
            {

                Clear();

                if (drpTreatmentType.SelectedValue == "OP")
                {
                    gvIPAdmission.Visible = false;
                    BindVisit();
                }
                else if (drpTreatmentType.SelectedValue == "IP")
                {
                    gvVisit.Visible = false;
                    BindAdmission();
                }


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.eClaimCoderVerification");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void VisitSelect_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvVisit.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblgvVisit_PTID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_PTID");
                Label lblgvVisit_BRANCH_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_BRANCH_ID");
                Label lblgvVisit_EMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_EMR_ID");
                Label lblgvVisit_Invoice_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_Invoice_ID");
                Label lblgvVisit_Date = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_Date");

                Label lblgvVisit_CompID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_CompID");
                Label lblgvVisit_DR_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_DR_ID");
                Label lblgvVisit_PhotoID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_PhotoID");
                Label lblgvVisitType = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitType");

                txtEMR_PTMaster_FromDate.Text = Convert.ToString(lblgvVisit_Date.Text);
                txtEMR_PTMaster_ToDate.Text = Convert.ToString(lblgvVisit_Date.Text);

                txtFileNo.Text = lblgvVisit_PTID.Text;


                ViewState["BRANCH_ID"] = lblgvVisit_BRANCH_ID.Text;
                ViewState["INV_EMR_ID"] = lblgvVisit_EMR_ID.Text;
                hidInvoice.Value = lblgvVisit_Invoice_ID.Text;
                //   ViewState["BilToCode"] = lblBilToCode.Text;
                ViewState["SubInsCode"] = lblgvVisit_CompID.Text;
                ViewState["InvDrCode"] = lblgvVisit_DR_ID.Text;
                ViewState["InvDate"] = lblgvVisit_Date.Text;
                //ViewState["InvoiceType"] = lblInvoiceType.Text;

                ViewState["Photo_ID"] = lblgvVisit_PhotoID.Text;

                ViewState["VisitType"] = lblgvVisitType.Text;

                PatientDtlsClear();
                PatientDataBind();




                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" PatientDataBind() completed");
                ClearDiagnosis();
                BindDiagnosis();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindDiagnosis() completed");
                BindProcedure();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindProcedure() completed");
                BindRadiology();

                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindRadiology() completed");
                BindLaboratory();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindLaboratory() completed");

                BindPharmacy();
                BindEMRPTMaster();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindEMRPTMaster() completed");
                BindNursingtInstruction();
                BindEMRPTMasterGrid();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindEMRPTMasterGrid() completed");
                PatientPhoto();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" PatientPhoto() completed");
                BindLaboratoryResult();

                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindLaboratoryResult() completed");
                BindRadiologyResult();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindRadiologyResult() completed");
                BindWEMR_spS_GetEMRS();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindWEMR_spS_GetEMRS() completed");
                BindEMRS_PT_DETAILS();
                BindWEMR_spS_GetEMCPTInvoice();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindWEMR_spS_GetEMCPTInvoice() completed");
                BindWEMR_spS_GetEMRSHPI();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindWEMR_spS_GetEMRSHPI() completed");
                BuildeInvoiceTrans();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BuildeInvoiceTrans() completed");
                BindInvoiceTrans();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindInvoiceTrans() completed");
                BindTempInvoiceTrans();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindTempInvoiceTrans() completed");

                BuildeInvoiceClaims();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BuildeInvoiceClaims() completed");
                BindInvoiceClaimDtls();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindInvoiceClaimDtls() completed");
                BindTempInvoiceClaims();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindTempInvoiceClaims() completed");
                BindAttachments();
                BindReferral();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.VisitSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void InvSelect_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["InvSelectIndex"] = gvScanCard.RowIndex;

                gvInvoice.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblGrossTotal, lblClaimAmount, lblHospDiscAmt, lblDeductible, lblCoInsAmt, lblSplDisc;

                Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
                Label lblBranchID = (Label)gvScanCard.Cells[0].FindControl("lblBranchID");
                Label lblEMRID = (Label)gvScanCard.Cells[0].FindControl("lblEMRID");
                Label lblInvoiceID = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceID");
                Label lblBilToCode = (Label)gvScanCard.Cells[0].FindControl("lblBilToCode");
                Label lblgvInvSubInsCode = (Label)gvScanCard.Cells[0].FindControl("lblgvInvSubInsCode");
                Label lblgvInvInsName = (Label)gvScanCard.Cells[0].FindControl("lblgvInvInsName");
                Label lblgvInvPolicyNo = (Label)gvScanCard.Cells[0].FindControl("lblgvInvPolicyNo");
                Label lblgvInvPolicyType = (Label)gvScanCard.Cells[0].FindControl("lblgvInvPolicyType");
                Label lblgvInvDrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvInvDrCode");
                Label lblgvInvDrName = (Label)gvScanCard.Cells[0].FindControl("lblgvInvDrName");
                Label lblgvInvDt = (Label)gvScanCard.Cells[0].FindControl("lblgvInvDt");
                Label lblInvoiceType = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceType");
                Label lblEMCPTCOde = (Label)gvScanCard.Cells[0].FindControl("lblEMCPTCOde");
                Label lblgvOrderingDR = (Label)gvScanCard.Cells[0].FindControl("lblgvOrderingDR");

                lblGrossTotal = (Label)gvScanCard.Cells[0].FindControl("lblGrossTotal");
                lblClaimAmount = (Label)gvScanCard.Cells[0].FindControl("lblClaimAmount");
                lblHospDiscAmt = (Label)gvScanCard.Cells[0].FindControl("lblHospDiscAmt");
                lblDeductible = (Label)gvScanCard.Cells[0].FindControl("lblDeductible");
                lblCoInsAmt = (Label)gvScanCard.Cells[0].FindControl("lblCoInsAmt");
                lblSplDisc = (Label)gvScanCard.Cells[0].FindControl("lblSplDisc");




                txtBillAmt.Text = lblGrossTotal.Text;
                txtClaimAmt.Text = lblClaimAmount.Text;
                txtHospDisc.Text = lblHospDiscAmt.Text;
                txtDedAmt.Text = lblDeductible.Text;
                txtCoInsAmt.Text = lblCoInsAmt.Text;
                txtSplDisc.Text = lblSplDisc.Text;



                txtFileNo.Text = lblPatientId.Text;
                ViewState["BRANCH_ID"] = lblBranchID.Text;
                ViewState["INV_EMR_ID"] = lblEMRID.Text;
                hidInvoice.Value = lblInvoiceID.Text;
                ViewState["BilToCode"] = lblBilToCode.Text;
                ViewState["SubInsCode"] = lblgvInvSubInsCode.Text;
                ViewState["InvDrCode"] = lblgvInvDrCode.Text;
                ViewState["InvDate"] = lblgvInvDt.Text;



                txtProviderName.Text = lblgvInvInsName.Text;
                txtPolicyNo.Text = lblgvInvPolicyNo.Text;
                txtPolicyType.Text = lblgvInvPolicyType.Text;
                txtDrName.Text = lblgvInvDrName.Text;
                txtOrderingClinician.Text = lblgvOrderingDR.Text;

                if (lblEMCPTCOde.Text != "" && string.IsNullOrEmpty(lblEMCPTCOde.Text) == false)
                {
                    txtENMCode.Text = lblEMCPTCOde.Text;
                }


                txtEMR_PTMaster_FromDate.Text = Convert.ToString(ViewState["InvDate"]);
                txtEMR_PTMaster_ToDate.Text = Convert.ToString(ViewState["InvDate"]);

                PatientDtlsClear();
                PatientDataBind();


                ClearDiagnosis();
                BindDiagnosis();

                BindProcedure();

                BindRadiology();


                BindLaboratory();


                BindPharmacy();
                BindEMRPTMaster();
                BindNursingtInstruction();
                BindEMRPTMasterGrid();
                PatientPhoto();

                BindLaboratoryResult();


                BindRadiologyResult();


                BindWEMR_spS_GetEMRS();
                BindEMRS_PT_DETAILS();
                BindWEMR_spS_GetEMCPTInvoice();
                BindWEMR_spS_GetEMRSHPI();




                BuildeInvoiceTrans();
                BindInvoiceTrans();
                BindTempInvoiceTrans();

                BuildeInvoiceClaims();
                BindInvoiceClaimDtls();
                BindTempInvoiceClaims();

                BindAttachments();

                BindReferral();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.InvSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void gvInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvInvoice.PageIndex * gvInvoice.PageSize) + e.Row.RowIndex + 1).ToString();
            }


        }

        protected void txtCompanyID_TextChanged(object sender, EventArgs e)
        {
            string Criteria = " 1=1 ";
            DS = new DataSet();

            string strName = txtCompanyID.Text.Trim();
            string[] arrName = strName.Split('~');

            if (arrName.Length > 1)
            {
                txtCompanyID.Text = arrName[0].Trim();
                txtCompanyName.Text = arrName[1].Trim();
                Criteria += " and HCM_COMP_ID='" + arrName[0].Trim() + "' ";
            }
            else
            {
                Criteria += " and HCM_COMP_ID='" + txtCompanyID.Text.Trim() + "'";
            }

            DS = dbo.retun_inccmp_details(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_NAME"]).Trim();
            }
            else
            {
                txtCompanyName.Text = "";
            }

        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {
            string Criteria = " 1=1 ";
            DS = new DataSet();

            string strName = txtDoctorID.Text.Trim();

            string[] arrName = strName.Split('~');

            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0].Trim();
                txtDoctorName.Text = arrName[1].Trim();
                Criteria += " and HSFM_STAFF_ID='" + arrName[0].Trim() + "' ";

            }
            else
            {
                Criteria += " and HSFM_STAFF_ID='" + txtDoctorID.Text.Trim() + "'";
            }
            DS = dbo.retun_doctor_detailss(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]).Trim();
            }
            else
            {
                txtDoctorName.Text = "";
            }

        }

        protected void btnAddInvoice_Click(object sender, EventArgs e)
        {
            if (txtInvoiceNo.Text != "")
            {
                lstInvoiceNo.Items.Add(txtInvoiceNo.Text);
                txtInvoiceNo.Text = "";
            }
            txtInvoiceNo.Focus();

        }

        //  ------------------





        // -----------------------------



        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string ServID = "", ServName = "";
                string ServCode = txtServCode.Text.Trim();
                GetServiceMasterName(ServCode, out ServID, out  ServName);
                hidHaadID.Value = ServID;
                txtServName.Text = ServName;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnServiceAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string strHaadCode = "", strServType = "", strServID = "";
                strHaadCode = hidHaadID.Value.Trim();
                strServID = txtServCode.Text.Trim();

                Decimal decServPrice = 0;
                // GetHaadServDtls(strHaadCode, out  strServType);
                GetCompanyAgrDtls(strServID, Convert.ToString(ViewState["BilToCode"]), out  decServPrice);

                decimal decQty = 1, decPrice = 0;
                if (txtQty.Text.Trim() != "")
                {
                    decQty = Convert.ToDecimal(txtQty.Text.Trim());
                }
                decPrice = decQty * decServPrice;

                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceTrans"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HEIT_SERV_CODE"] = txtServCode.Text.Trim();
                objrow["HEIT_SERV_DESC"] = txtServName.Text.Trim();
                objrow["HEIT_CAT_ID"] = hidCatID.Value;
                objrow["HEIT_FEE"] = Convert.ToString(decServPrice);
                objrow["HEIT_AMOUNT"] = Convert.ToString(decPrice);
                objrow["HEIT_NET_AMOUNT"] = Convert.ToString(decPrice);
                objrow["HEIT_QTY"] = txtQty.Text.Trim();
                objrow["HEIT_PT_AMOUNT"] = 0;

                objrow["HEIT_DR_CODE"] = drpDoctor.SelectedValue;
                objrow["HEIT_DR_NAME"] = drpDoctor.SelectedItem.Text;
                objrow["HEIT_HAAD_CODE"] = strHaadCode;


                DT.Rows.Add(objrow);

                ViewState["InvoiceTrans"] = DT;
                BindTempInvoiceTrans();

                txtServCode.Text = "";
                txtServName.Text = "";
                txtQty.Text = "1";
                drpDoctor.SelectedIndex = 0;
                hidCatID.Value = "";
                hidCatType.Value = "";
                hidHaadID.Value = "";

                ClearService();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnServiceAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }


        protected void btnInvDiagAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceClaims"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HEIC_ICD_CODE"] = txtDiagCode.Text.Trim();
                objrow["HEIC_ICD_DESC"] = txtDiagName.Text.Trim();
                objrow["HEIC_PRIMARY"] = 0;
                objrow["HEIC_PRIMARYDesc"] = "Secondary";

                DT.Rows.Add(objrow);

                ViewState["InvoiceClaims"] = DT;
                BindTempInvoiceClaims();

                txtDiagCode.Text = "";
                txtDiagName.Text = "";
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnInvDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnCoderVerify_Click(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 ";

                Criteria += " AND HEIM_INVOICE_ID='" + hidInvoice.Value + " '";
                objeClaimCoder = new eClaimCoderBAL();
                DataSet DS = new DataSet();
                DS = objeClaimCoder.EclaimCoderInvoiceGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This invoice already verified by coder";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                string strHEIM_ID = "";
                objeClaimCoder = new eClaimCoderBAL();
                objeClaimCoder.BRANCH_ID = Convert.ToString(ViewState["BRANCH_ID"]);
                objeClaimCoder.INVOICE_ID = hidInvoice.Value;
                objeClaimCoder.HEIM_CODER_ID = Convert.ToString(Session["User_ID"]); //Convert.ToString(Session["User_Code"]);
                objeClaimCoder.HEIM_CODER_NAME = Convert.ToString(Session["User_Name"]);
                objeClaimCoder.HEIM_EMCPT_CODE = txtENMCode.Text.Trim(); ;
                objeClaimCoder.UserID = Convert.ToString(Session["User_ID"]);
                objeClaimCoder.EclaimCoderInvoiceAdd(out  strHEIM_ID);

                for (int j = 0; j < gvInvoiceClaims.Rows.Count; j++)
                {
                    Label lblType, lblStDate, lblEndDate, lblStType, lblEndType, lblICDCode, lblICDDesc, lblPrimary;
                    lblType = (Label)gvInvoiceClaims.Rows[j].FindControl("lblType");
                    lblStDate = (Label)gvInvoiceClaims.Rows[j].FindControl("lblStDate");
                    lblEndDate = (Label)gvInvoiceClaims.Rows[j].FindControl("lblEndDate");
                    lblStType = (Label)gvInvoiceClaims.Rows[j].FindControl("lblStType");
                    lblEndType = (Label)gvInvoiceClaims.Rows[j].FindControl("lblEndType");


                    lblICDCode = (Label)gvInvoiceClaims.Rows[j].FindControl("lblICDCode");
                    lblICDDesc = (Label)gvInvoiceClaims.Rows[j].FindControl("lblICDDesc");
                    lblPrimary = (Label)gvInvoiceClaims.Rows[j].FindControl("lblPrimary");


                    objeClaimCoder = new eClaimCoderBAL();
                    objeClaimCoder.HEIT_HEIM_ID = strHEIM_ID;
                    objeClaimCoder.HEIC_TYPE = lblType.Text;
                    objeClaimCoder.HEIC_STARTDATE = lblStDate.Text;
                    objeClaimCoder.HEIC_ENDDATE = lblEndDate.Text;
                    objeClaimCoder.HEIC_STARTTYPE = lblStType.Text;
                    objeClaimCoder.HEIC_ENDTYPE = lblEndType.Text;

                    objeClaimCoder.HEIC_ICD_CODE = lblICDCode.Text;
                    objeClaimCoder.HEIC_ICD_DESC = lblICDDesc.Text;
                    objeClaimCoder.HEIC_PRIMARY = lblPrimary.Text;


                    objeClaimCoder.EclaimCoderInvoiceClaimsAdd();

                }


                for (int i = 0; i < gvInvoiceTrans.Rows.Count; i++)
                {
                    Label lblServCode, lblServDesc, lblCatID, lblDRCode, lblDRName, lblHaadCode;
                    lblServCode = (Label)gvInvoiceTrans.Rows[i].FindControl("lblServCode");
                    lblServDesc = (Label)gvInvoiceTrans.Rows[i].FindControl("lblServDesc");
                    lblCatID = (Label)gvInvoiceTrans.Rows[i].FindControl("lblCatID");
                    lblDRCode = (Label)gvInvoiceTrans.Rows[i].FindControl("lblDRCode");
                    lblDRName = (Label)gvInvoiceTrans.Rows[i].FindControl("lblDRName");
                    lblHaadCode = (Label)gvInvoiceTrans.Rows[i].FindControl("lblHaadCode");

                    TextBox txtFee, txtgvQty, txtHitAmount, txtNet, txtPTAmount, txtAuthID, txtObsType, txtObsCode, txtObsValue, txtObsValueType;
                    txtFee = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtFee");
                    txtgvQty = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtgvQty");
                    txtHitAmount = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtHitAmount");
                    txtNet = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtNet");
                    txtPTAmount = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtPTAmount");
                    txtAuthID = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtAuthID");
                    txtObsType = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtObsType");
                    txtObsCode = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtObsCode");
                    txtObsValue = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtObsValue");
                    txtObsValueType = (TextBox)gvInvoiceTrans.Rows[i].FindControl("txtObsValueType");

                    objeClaimCoder = new eClaimCoderBAL();
                    objeClaimCoder.HEIT_HEIM_ID = strHEIM_ID;
                    objeClaimCoder.HEIT_SERV_CODE = lblServCode.Text;
                    objeClaimCoder.HEIT_SERV_DESC = lblServDesc.Text;
                    objeClaimCoder.HEIT_CAT_ID = lblCatID.Text;
                    objeClaimCoder.HEIT_FEE = txtFee.Text.Trim();
                    objeClaimCoder.HEIT_QTY = txtgvQty.Text.Trim();
                    objeClaimCoder.HEIT_AMOUNT = txtHitAmount.Text.Trim();
                    objeClaimCoder.HEIT_NET_AMOUNT = txtNet.Text.Trim();
                    objeClaimCoder.HEIT_PT_AMOUNT = txtPTAmount.Text.Trim();
                    objeClaimCoder.HEIT_DR_CODE = lblDRCode.Text.Trim();
                    objeClaimCoder.HEIT_DR_NAME = lblDRName.Text.Trim();
                    objeClaimCoder.HEIT_CLINICIAN_ID = "";
                    objeClaimCoder.HEIT_HAAD_CODE = lblHaadCode.Text.Trim();
                    objeClaimCoder.HEIT_AUTHORIZATIONID = txtAuthID.Text.Trim();
                    objeClaimCoder.HEIT_ACTIVITY_START = "";
                    objeClaimCoder.HEIT_ACTIVITY_END = "";
                    objeClaimCoder.HEIT_START_TYPE = "";
                    objeClaimCoder.HEIT_END_TYPE = "";
                    objeClaimCoder.HEIT_OBS_TYPE = txtObsType.Text.Trim();
                    objeClaimCoder.HEIT_OBS_CODE = txtObsCode.Text.Trim();
                    objeClaimCoder.HEIT_OBS_VALUE = txtObsValue.Text.Trim();
                    objeClaimCoder.HEIT_OBS_VALUE_TYPE = txtObsValueType.Text.Trim();
                    objeClaimCoder.EclaimCoderInvoiceTransAdd();


                }


                objeClaimCoder = new eClaimCoderBAL();
                objeClaimCoder.HEIM_HEIM_ID = strHEIM_ID;
                objeClaimCoder.EclaimCoderInvoiceUpdate();
                /*

                for (int intLabRequest = 0; intLabRequest < gvLabRequest.Rows.Count; intLabRequest++)
                {

                    CheckBox chkLabRequest;
                    chkLabRequest = (CheckBox)gvLabRequest.Rows[intLabRequest].FindControl("chkLabRequest");

                    Label lblLabReqEPRID;
                    lblLabReqEPRID = (Label)gvLabRequest.Rows[intLabRequest].FindControl("lblLabReqEPRID");

                    if (chkLabRequest.Checked == true)
                    {
                        objeClaimCoder = new eClaimCoderBAL();
                        objeClaimCoder.BRANCH_ID = Convert.ToString(ViewState["BRANCH_ID"]);
                        objeClaimCoder.HEIM_CODER_ID = strHEIM_ID;
                        objeClaimCoder.INVOICE_ID = hidInvoice.Value;
                        objeClaimCoder.HCDM_MAP_ID = lblLabReqEPRID.Text;
                        objeClaimCoder.HCDM_TYPE = "LabRequest";
                        objeClaimCoder.CoderDataMapAdd();

                    }


                }

                for (int intLabRsult = 0; intLabRsult < gvLabResult.Rows.Count; intLabRsult++)
                {

                    CheckBox chkLabResult;
                    chkLabResult = (CheckBox)gvLabResult.Rows[intLabRsult].FindControl("chkLabResult");

                    Label lblLabTransNo;
                    lblLabTransNo = (Label)gvLabResult.Rows[intLabRsult].FindControl("lblRadTransNo");

                    if (chkLabResult.Checked == true)
                    {


                        objeClaimCoder = new eClaimCoderBAL();
                        objeClaimCoder.BRANCH_ID = Convert.ToString(ViewState["BRANCH_ID"]);
                        objeClaimCoder.HEIM_CODER_ID = strHEIM_ID;
                        objeClaimCoder.INVOICE_ID = hidInvoice.Value;
                        objeClaimCoder.HCDM_MAP_ID = lblLabTransNo.Text;
                        objeClaimCoder.HCDM_TYPE = "LabResult";
                        objeClaimCoder.CoderDataMapAdd();

                    }


                }






                for (int intRadRequest = 0; intRadRequest < gvRadRequest.Rows.Count; intRadRequest++)
                {

                    CheckBox chkRadRequest;
                    chkRadRequest = (CheckBox)gvRadRequest.Rows[intRadRequest].FindControl("chkRadRequest");

                    Label lblRadReqEPRID;
                    lblRadReqEPRID = (Label)gvRadRequest.Rows[intRadRequest].FindControl("lblRadReqEPRID");

                    if (chkRadRequest.Checked == true)
                    {


                        objeClaimCoder = new eClaimCoderBAL();
                        objeClaimCoder.BRANCH_ID = Convert.ToString(ViewState["BRANCH_ID"]);
                        objeClaimCoder.HEIM_CODER_ID = strHEIM_ID;
                        objeClaimCoder.INVOICE_ID = hidInvoice.Value;
                        objeClaimCoder.HCDM_MAP_ID = lblRadReqEPRID.Text;
                        objeClaimCoder.HCDM_TYPE="RadRequest";
                        objeClaimCoder.CoderDataMapAdd();

                    }


                }



                for (int intRadRsult = 0; intRadRsult < gvRadResult.Rows.Count; intRadRsult++)
                {

                    CheckBox chkRadResult;
                    chkRadResult = (CheckBox)gvRadResult.Rows[intRadRsult].FindControl("chkRadResult");

                    Label lblRadTransNo;
                    lblRadTransNo = (Label)gvRadResult.Rows[intRadRsult].FindControl("lblRadTransNo");

                    if (chkRadResult.Checked == true)
                    {


                        objeClaimCoder = new eClaimCoderBAL();
                        objeClaimCoder.BRANCH_ID = Convert.ToString(ViewState["BRANCH_ID"]);
                        objeClaimCoder.HEIM_CODER_ID = strHEIM_ID;
                        objeClaimCoder.INVOICE_ID = hidInvoice.Value;
                        objeClaimCoder.HCDM_MAP_ID = lblRadTransNo.Text;
                        objeClaimCoder.HCDM_TYPE = "RadResult";
                        objeClaimCoder.CoderDataMapAdd();

                    }


                }

                */



                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                Clear();
                BindInvoice();

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnCoderVerify_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnEMR_PTMasterRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["EMRSelectIndex"] = "";
                ViewState["IPSelectIndex"] = "";

                if (drpTreatmentType.SelectedValue == "OP")
                {
                    BindEMRPTMasterGrid();
                }
                else if (drpTreatmentType.SelectedValue == "IP")
                {
                    BindIP_PTMasterGrid();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnEMR_PTMasterRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvEMR_PTMasterSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["EMRSelectIndex"]) != "")
                {
                    if (gvEMR_PTMaster.Rows.Count > 0)
                        gvEMR_PTMaster.Rows[Convert.ToInt32(ViewState["EMRSelectIndex"])].BackColor = System.Drawing.Color.White;
                }

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["EMRSelectIndex"] = gvScanCard.RowIndex;

                gvEMR_PTMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblgvEMR_PTMaster_Emr_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvEMR_PTMaster_Emr_ID");
                Label lblgvEMR_PTMaster_DrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvEMR_PTMaster_DrCode");

                ViewState["Selected_Emr_ID"] = lblgvEMR_PTMaster_Emr_ID.Text;


                //string strRptPath = "../WebReport/eClaimCoderClinicalSummary.aspx";
                //string rptcall = @strRptPath + "?INV_EMR_ID=" + lblgvEMR_PTMaster_Emr_ID.Text;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                string strRptPath = "../../EMR/WebReports/ClinicalSummary.aspx";
                string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(ViewState["Selected_Emr_ID"]) + "&EMR_PT_ID=" + txtFileNo.Text.Trim() + "&DR_ID=" + lblgvEMR_PTMaster_DrCode.Text.Trim();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.gvEMR_PTMasterSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void gvIP_PTMasterSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["IPSelectIndex"]) != "")
                {
                    if (gvEMR_PTMaster.Rows.Count > 0)
                        gvEMR_PTMaster.Rows[Convert.ToInt32(ViewState["EMRSelectIndex"])].BackColor = System.Drawing.Color.White;
                }

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["IPSelectIndex"] = gvScanCard.RowIndex;

                gvIP_PTMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblgvIP_PTMaster_IP_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvIP_PTMaster_IP_ID");
                Label lblgvIP_PTMaster_DrCode = (Label)gvScanCard.Cells[0].FindControl("lblgvIP_PTMaster_DrCode");

                ViewState["Selected_Emr_ID"] = lblgvIP_PTMaster_IP_ID.Text;

                string EMR_ID = lblgvIP_PTMaster_IP_ID.Text;
                string EMR_PT_ID = txtFileNo.Text.Trim();
                string DR_ID = lblgvIP_PTMaster_DrCode.Text;
                string IAS_ADMISSION_NO = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);



                string strPath1 = "&EMR_ID=" + EMR_ID + "&EMR_PT_ID=" + EMR_PT_ID + "&DR_ID=" + DR_ID + "&IAS_ADMISSION_NO=" + IAS_ADMISSION_NO;

                string strPath = GlobalValues.EMR_IP_PATH + "/CommonPageLoader.aspx?BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&User_DeptID=" + Convert.ToString(Session["User_DeptID"]) + strPath1 + "&PageName=MRDAuditIP&LoginFrom=CommonPage";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Summary", "window.open('" + strPath + "','_new','top=200,left=100,height=800,width=900,toolbar=no,scrollbars=yes,menubar=no');", true);



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.gvIP_PTMasterSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnFullSummaryReport_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(ViewState["Selected_Emr_ID"]) == "")
                {
                    goto FunEnd;
                }



                string strLabTransNo = "", strRadTransNo = "";

                foreach (GridViewRow gvRow in gvLabResult.Rows)
                {
                    Label lblLabTransNo;
                    lblLabTransNo = (Label)gvRow.FindControl("lblLabTransNo");
                    CheckBox chkLabResult;
                    chkLabResult = (CheckBox)gvRow.FindControl("chkLabResult");

                    if (chkLabResult.Checked == true)
                    {
                        if (strLabTransNo != "") strLabTransNo += ",";
                        strLabTransNo += "'" + lblLabTransNo.Text + "'";
                    }


                }



                foreach (GridViewRow gvRow in gvRadResult.Rows)
                {
                    Label lblRadTransNo;
                    lblRadTransNo = (Label)gvRow.FindControl("lblRadTransNo");
                    CheckBox chkRadResult;
                    chkRadResult = (CheckBox)gvRow.FindControl("chkRadResult");

                    if (chkRadResult.Checked == true)
                    {
                        if (strRadTransNo != "") strRadTransNo += ",";
                        strRadTransNo += "'" + lblRadTransNo.Text + "'";
                    }


                }



                string strRptPath = "../WebReport/eClaimCoderClinicalSummaryAll.aspx";
                string rptcall = @strRptPath + "?INV_EMR_ID=" + Convert.ToString(ViewState["Selected_Emr_ID"]);// + "&LabTransNo=" + strLabTransNo + "&RadTransNo=" + strRadTransNo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);


            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnFullSummaryReport_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void SelectLabPrint_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;

            Label lblLabTransNo = (Label)gvGCGridView.Cells[0].FindControl("lblLabTransNo");
            Label lblReportType = (Label)gvGCGridView.Cells[0].FindControl("lblReportType");

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "LabResultReport('" + lblLabTransNo.Text + "','" + lblReportType.Text.Trim() + "');", true);

           // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "LabReport('" + lblPatientId.Text + "','" + lblSeqNo.Text + "','" + lblDrId.Text + "','" + lblDateDesc.Text + "');", true);


        FunEnd: ;

        }


        protected void SelectRadPrint_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
            Label lblRadTransNo;
            lblRadTransNo = (Label)gvGCGridView.Cells[0].FindControl("lblRadTransNo");
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "RadResultReport('" + lblRadTransNo.Text + "');", true);

           // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "LabReport('" + lblPatientId.Text + "','" + lblSeqNo.Text + "','" + lblDrId.Text + "','" + lblDateDesc.Text + "');", true);


        FunEnd: ;

        }


        void BIndReport(string ReportName, string SelectionFormula1, string FileName, string strFolderName)
        {
            string CoderVerification_FilePath = System.Configuration.ConfigurationSettings.AppSettings["CoderVerification_FilePath"].ToString().Trim();

            ReportDocument crystalReport1 = new ReportDocument();
            try
            {
                string ReportName1 = ReportName;
                crystalReport1.Load(GlobalValues.REPORT_PATH + ReportName1);
                crystalReport1.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, GlobalValues.DB_SERVER1, GlobalValues.DB_DATABASE);

                crystalReport1.RecordSelectionFormula = SelectionFormula1;

                string filelocation = @CoderVerification_FilePath + strFolderName + "\\" + FileName;

                crystalReport1.ExportToDisk(ExportFormatType.PortableDocFormat, filelocation);


                //   crystalReport1.Close();
                // crystalReport1.Dispose();

                //Response.ContentType = "image/jpeg";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=SailBig.jpg");
                //Response.TransmitFile(Server.MapPath("~/images/sailbig.jpg"));
                //Response.End();

                //WebClient WC = new WebClient();
                //WC.DownloadFile(

            }
            catch (Exception ex)
            {


                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AccuReportViewer.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
            finally
            {
                crystalReport1.Close();
                crystalReport1.Dispose();
            }

        }

        void SaveFile()
        {
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            String Header = "Attachment; Filename=XMLFile.xml";
            Response.AppendHeader("Content-Disposition", Header);
            //  System.IO.FileInfo Dfile = new System.IO.FileInfo(filelocation);
            // Response.WriteFile(Dfile.FullName);
            //Don't forget to add the following line
            Response.End();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            //try
            //{
            string CoderVerification_FilePath = System.Configuration.ConfigurationSettings.AppSettings["CoderVerification_FilePath"].ToString().Trim();
            string ReportName, strFileName = "", strFolderName = "";

            string strFldName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

            strFolderName = @txtFileNo.Text.Trim() + "_" + strFldName;

            string path = @CoderVerification_FilePath + strFolderName;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


            foreach (GridViewRow gvRow in gvLabResult.Rows)
            {
                Label lblLabTransNo;
                lblLabTransNo = (Label)gvRow.FindControl("lblLabTransNo");
                CheckBox chkLabResult;
                chkLabResult = (CheckBox)gvRow.FindControl("chkLabResult");

                if (chkLabResult.Checked == true)
                {
                    //if (strLabTransNo != "") strLabTransNo += ",";
                    //strLabTransNo += "'" + lblLabTransNo.Text + "'";


                    string SelectionFormula1 = "1=1";
                    SelectionFormula1 += "  AND  {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}='" + lblLabTransNo.Text + "'";


                    string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                    strFileName = "LAB_" + txtFileNo.Text.Trim() + "_" + strName + ".pdf";
                    // BIndReport("RadiologyReport.rpt", SelectionFormula1,strFileName);
                    BIndReport("LabReportUsr.rpt", SelectionFormula1, strFileName, strFolderName);
                    // LabReportUsr
                    //Labreport
                    // LabReportUsrEMR.rpt
                }



            }

            foreach (GridViewRow gvRow in gvRadResult.Rows)
            {
                Label lblRadTransNo;
                lblRadTransNo = (Label)gvRow.FindControl("lblRadTransNo");
                CheckBox chkRadResult;
                chkRadResult = (CheckBox)gvRow.FindControl("chkRadResult");

                if (chkRadResult.Checked == true)
                {


                    string SelectionFormula1 = "1=1";
                    SelectionFormula1 += "  AND  {RAD_TEST_REPORT.RTR_TRANS_NO}='" + lblRadTransNo.Text + "'";


                    string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                    strFileName = "RAD_" + txtFileNo.Text.Trim() + "_" + strName + ".pdf";
                    // BIndReport("RadiologyReport.rpt", SelectionFormula1,strFileName);
                    BIndReport("RadiologyReport.rpt", SelectionFormula1, strFileName, strFolderName);
                }



            }


            //-----------------------------------------------------------------
            string SelectionFormula = "1=1", strInvFIleName = "";
            SelectionFormula += "  AND  {HMS_INVOICE_MASTER.HIM_INVOICE_ID}='" + hidInvoice.Value + "'";

            //if (Convert.ToString(ViewState["InvoiceType"]) == "Cash")
            //{
            //    ReportName = "HmsInvoiceCa.rpt";
            //}
            //else
            //{
            // ReportName = "HmsInvoiceCr.rpt";
            ReportName = "HmsMergeCreditBill.rpt";

            // }


            strInvFIleName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "INV_" + txtFileNo.Text.Trim() + "_" + strInvFIleName + ".pdf";
            // BIndReport("RadiologyReport.rpt", SelectionFormula1,strFileName);
            BIndReport(ReportName, SelectionFormula, strFileName, strFolderName);

            //-----------------------------------------------------------------

            //-----------------------------------------------------------------


            //if (Convert.ToString(ViewState["InvoiceType"]) == "Credit")
            //{
            SelectionFormula = "1=1";
            SelectionFormula += "  AND  {HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text + "'";

            ReportName = "INSCARD1.rpt";

            strInvFIleName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "CARD_" + txtFileNo.Text.Trim() + "_" + strInvFIleName + ".pdf";
            // BIndReport("RadiologyReport.rpt", SelectionFormula1,strFileName);
            BIndReport(ReportName, SelectionFormula, strFileName, strFolderName);

            // }


            //-----------------------------------------------------------------

            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " and ESF_EMR_ID='" + Convert.ToString(ViewState["INV_EMR_ID"]) + "'";
            DS = objCom.EMRScanFileGet(Criteria);

            string ScanPath = "", ScanFileName = "", ScanSourcePath, ScanDistPath;
            foreach (DataRow dr in DS.Tables[0].Rows)
            {
                ScanPath = Convert.ToString(dr["ESF_FULLPATH"]);
                ScanFileName = Convert.ToString(dr["ESF_FILENAME"]);
                ScanSourcePath = @Convert.ToString(ViewState["EMR_SCAN_PATH"]) + ScanPath + ScanFileName;
                ScanDistPath = @Convert.ToString(path + "\\" + ScanFileName);

                if (File.Exists(ScanSourcePath) == true)
                {
                    System.IO.File.Copy(ScanSourcePath, ScanDistPath);
                }
            }

            lblStatus.Text = " Data Export Successfully!";
            lblStatus.ForeColor = System.Drawing.Color.Green;

            //}
            //catch (Exception ex)
            //{

            //    TextFileWriting("-----------------------------------------------");
            //    TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnExport_Click");
            //    TextFileWriting(ex.Message.ToString());
            //}

        }


        protected void txtENMCode_TextChanged(object sender, EventArgs e)
        {
            try
            {

                BindWEMR_spS_GetEMCPTInvoice();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnExport_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        void DownloadFile(string ReportName, string SelectionFormula1, string FileName)
        {
            //string CoderVerification_FilePath = System.Configuration.ConfigurationSettings.AppSettings["CoderVerification_FilePath"].ToString().Trim();

            ReportDocument crystalReport1 = new ReportDocument();
            try
            {
                string ReportName1 = ReportName;
                crystalReport1.Load(GlobalValues.REPORT_PATH + ReportName1);
                crystalReport1.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, GlobalValues.DB_SERVER1, GlobalValues.DB_DATABASE);

                crystalReport1.RecordSelectionFormula = SelectionFormula1;

                //string filelocation = @CoderVerification_FilePath + strFolderName + "\\" + FileName;

                //crystalReport1.ExportToDisk(ExportFormatType.PortableDocFormat, filelocation);

                crystalReport1.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, FileName);

                //  crystalReport1.Close();
                //  crystalReport1.Dispose();

                //Response.ContentType = "image/jpeg";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=SailBig.jpg");
                //Response.TransmitFile(Server.MapPath("~/images/sailbig.jpg"));
                //Response.End();

                //WebClient WC = new WebClient();
                //WC.DownloadFile(
            }
            catch (Exception ex)
            {


                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AccuReportViewer.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
            finally
            {
                crystalReport1.Close();
                crystalReport1.Dispose();
            }

        }


        protected void btnCardPrint_Click(object sender, EventArgs e)
        {
            string ReportName, strFileName = "";
            string SelectionFormula = "1=1", strInvFIleName = "";


            string strStartDate = Convert.ToString(ViewState["EMR_Date"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            //if (Convert.ToString(ViewState["InvoiceType"]) == "Credit")
            //{
            SelectionFormula = "1=1";
            SelectionFormula += " AND {HMS_PATIENT_PHOTO.HPP_PT_ID} = '" + txtFileNo.Text + "'";
            SelectionFormula += " AND {HMS_PATIENT_PHOTO.HPP_BRANCH_ID}='" + Convert.ToString(Session["Branch_ID"]) + "'";
            // SelectionFormula += " AND {HMS_PATIENT_PHOTO.HPP_DATE}=date('" + strForStartDate + "')";
            // SelectionFormula += "  AND  {HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text + "'";
            if (Convert.ToString(ViewState["Photo_ID"]) != "" || Convert.ToString(ViewState["Photo_ID"]) != null)
            {
                SelectionFormula += " AND  {HMS_PATIENT_PHOTO.HPP_PHOTO_ID}='" + Convert.ToString(ViewState["Photo_ID"]) + "'";
            }

            ReportName = "InsuranceCardPrint.rpt";

            strInvFIleName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "CARD_" + txtFileNo.Text.Trim() + "_" + strInvFIleName;// +".pdf";
            //  BIndReport(ReportName, SelectionFormula, strFileName);
            DownloadFile(ReportName, SelectionFormula, strFileName);

            //}
        }


        protected void btnInvoicePrint_Click(object sender, EventArgs e)
        {
            if (hidInvoice.Value == "")
            {
                goto FunEnd;
            }
            string ReportName, strFileName = "";
            string SelectionFormula = "1=1", strInvFIleName = "";

            string strStartDate = Convert.ToString(ViewState["InvDate"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            // SelectionFormula += "  AND  {HMS_INVOICE_MASTER.HIM_INVOICE_ID}='" + hidInvoice.Value + "'";
            SelectionFormula += "  AND  {HMS_INVOICE_MASTER.HIM_PT_ID}='" + txtFileNo.Text + "'";
            SelectionFormula += "  AND  {HMS_INVOICE_MASTER.HIM_BRANCH_ID}='" + Convert.ToString(Session["Branch_ID"]) + "'";
            SelectionFormula += "  AND  {HMS_INVOICE_MASTER.HIM_DR_CODE}='" + Convert.ToString(ViewState["InvDrCode"]) + "'";
            SelectionFormula += "  AND  {HMS_INVOICE_MASTER.HIM_DATE}=date('" + strForStartDate + "')";




            //if (Convert.ToString(ViewState["InvoiceType"]) == "Cash")
            //{
            //    ReportName = "HmsInvoiceCa.rpt";
            //}
            //else
            //{
            // ReportName = "HmsInvoiceCr.rpt";
            ReportName = "HmsMergeCreditBill.rpt";

            //}

            strInvFIleName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "INV_" + txtFileNo.Text.Trim() + "_" + strInvFIleName + ".pdf";
            DownloadFile(ReportName, SelectionFormula, strFileName);

        FunEnd: ;
        }


        protected void SelectLabDownload_Click(object sender, EventArgs e)
        {
            string strFileName = "";
            string SelectionFormula = " 1=1 ";
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;

            Label lblLabTransNo = (Label)gvGCGridView.Cells[0].FindControl("lblLabTransNo");


            SelectionFormula += "  AND  {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}='" + lblLabTransNo.Text + "'";

            string Report = "LabReport.rpt";





            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "LAB_" + txtFileNo.Text.Trim() + "_" + strName + ".pdf";
            // BIndReport("RadiologyReport.rpt", SelectionFormula1,strFileName);
            DownloadFile(Report, SelectionFormula, strFileName);

        FunEnd: ;

        }


        protected void SelectRadDownload_Click(object sender, EventArgs e)
        {
            string strFileName = "";
            string SelectionFormula = " 1=1 ";
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
            Label lblRadTransNo;
            lblRadTransNo = (Label)gvGCGridView.FindControl("lblRadTransNo");


            SelectionFormula += "  AND  {RAD_TEST_REPORT.RTR_TRANS_NO}='" + lblRadTransNo.Text + "'";


            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "RAD_" + txtFileNo.Text.Trim() + "_" + strName + ".pdf";
            // BIndReport("RadiologyReport.rpt", SelectionFormula1,strFileName);
            DownloadFile("RadiologyReport.rpt", SelectionFormula, strFileName);


        FunEnd: ;

        }


        protected void SelectAttachment_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
            Label lblgvAttaEMRID = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaEMRID");
            Label lblgvAttaCategory = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaCategory");
            Label lblgvAttaFileName = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaFileName");
            Label lblgvAttaPTID = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaPTID");


            // string strUrl = "DisplayAttachments.aspx?EMR_ID=" + lblgvAttaEMRID.Text + "&Category=" + lblgvAttaCategory.Text + "&FileName=" + lblgvAttaFileName.Text;

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + lblgvAttaPTID.Text + "','" + lblgvAttaFileName.Text + "');", true);



        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvVisit.PageIndex = e.NewPageIndex;
                BindVisit();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }




        protected void gvGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSlNo = (Label)e.Row.FindControl("lblSlNo");
                lblSlNo.Text = ((gvVisit.PageIndex * gvVisit.PageSize) + e.Row.RowIndex + 1).ToString();
            }


        }



        protected void gvIPAdmission_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvIPAdmission.PageIndex = e.NewPageIndex;
                BindVisit();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvIPAdmission_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label gvIPAdmissionSLNo = (Label)e.Row.FindControl("gvIPAdmissionSLNo");
                gvIPAdmissionSLNo.Text = ((gvIPAdmission.PageIndex * gvIPAdmission.PageSize) + e.Row.RowIndex + 1).ToString();
            }


        }

        protected void IPAdmissionSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;

                gvIPAdmission.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblgvIPAdmissionNo = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmissionNo");

                Label lblgvIPAdmPTID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmPTID");
                Label lblgvIPAdmBranchID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmBranchID");
                Label lblgvIPAdmIPEMRID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmIPEMRID");
                Label lblgvIPAdmInvoiceID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmInvoiceID");
                Label lblgvIPAdmDate = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmDate");

                Label lblgvIPAdmCompID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmCompID");
                Label lblgvIPAdmDRID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmDRID");
                //Label lblgvVisit_PhotoID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_PhotoID");



                Label lblgvIPAdmWard = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmWard");
                Label lblgvIPAdmRoom = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmRoom");
                Label lblgvIPAdmBed = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmBed");


                Label lblgvIPAdmDrgCode = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmDrgCode");
                Label lblgvIPAdmDrgAmt = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmDrgAmt");

                Label lblgvIPAdmAuthID = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmAuthID");
                Label lblgvIPAdmAuthStart = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmAuthStart");

                Label lblgvIPAdmAuthEnd = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmAuthEnd");
                Label lblgvIPAdmStay = (Label)gvScanCard.Cells[0].FindControl("lblgvIPAdmStay");


                txtEMR_PTMaster_FromDate.Text = Convert.ToString(lblgvIPAdmDate.Text);
                txtEMR_PTMaster_ToDate.Text = Convert.ToString(lblgvIPAdmDate.Text);

                txtFileNo.Text = lblgvIPAdmPTID.Text;

                ViewState["IAS_ADMISSION_NO"] = lblgvIPAdmissionNo.Text;
                ViewState["BRANCH_ID"] = lblgvIPAdmBranchID.Text;
                ViewState["INV_EMR_ID"] = lblgvIPAdmIPEMRID.Text;
                hidInvoice.Value = lblgvIPAdmInvoiceID.Text;
                //   ViewState["BilToCode"] = lblBilToCode.Text;
                ViewState["SubInsCode"] = lblgvIPAdmCompID.Text;
                ViewState["InvDrCode"] = lblgvIPAdmDRID.Text;
                ViewState["InvDate"] = lblgvIPAdmDate.Text;
                //ViewState["InvoiceType"] = lblInvoiceType.Text;

                //ViewState["Photo_ID"] = lblgvVisit_PhotoID.Text;


                txtWardNo.Text = lblgvIPAdmWard.Text;
                txtRoomNo.Text = lblgvIPAdmRoom.Text;
                txtBedNo.Text = lblgvIPAdmBed.Text;


                txtDrgCode.Text = lblgvIPAdmDrgCode.Text;
                txtDRGAmount.Text = lblgvIPAdmDrgAmt.Text;
                txtPriorAuthoID.Text = lblgvIPAdmAuthID.Text;

                txtFromDate.Text = lblgvIPAdmAuthStart.Text;
                txtToDate.Text = lblgvIPAdmAuthEnd.Text;
                txtLenthOfStay.Text = lblgvIPAdmStay.Text;





                PatientDtlsClear();
                PatientDataBind();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" PatientDataBind() completed");
                //  ClearDiagnosis();
                BindIPDiagnosis();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindDiagnosis() completed");
                BindIPProcedure();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindProcedure() completed");
                BindIPRadiology();

                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindRadiology() completed");
                BindIPLaboratory();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindLaboratory() completed");

                BindIPPharmacy();

                //BindEMRPTMaster();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindEMRPTMaster() completed");
                //BindNursingtInstruction();
                BindIP_PTMasterGrid();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindEMRPTMasterGrid() completed");
                //PatientPhoto();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" PatientPhoto() completed");
                //BindLaboratoryResult();

                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindLaboratoryResult() completed");
                //BindRadiologyResult();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindRadiologyResult() completed");
                //BindWEMR_spS_GetEMRS();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindWEMR_spS_GetEMRS() completed");
                //BindEMRS_PT_DETAILS();
                //BindWEMR_spS_GetEMCPTInvoice();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindWEMR_spS_GetEMCPTInvoice() completed");
                //BindWEMR_spS_GetEMRSHPI();
                //if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindWEMR_spS_GetEMRSHPI() completed");
                BuildeInvoiceTrans();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BuildeInvoiceTrans() completed");
                BindInvoiceTrans();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindInvoiceTrans() completed");
                BindTempInvoiceTrans();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindTempInvoiceTrans() completed");

                BuildeInvoiceClaims();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BuildeInvoiceClaims() completed");
                BindInvoiceClaimDtls();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindInvoiceClaimDtls() completed");
                BindTempInvoiceClaims();
                if (hidErrorChecking.Value.ToLower() == "true") TextFileWriting(" BindTempInvoiceClaims() completed");
                //BindAttachments();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.VisitSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void SelectRefPrint_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvReferralEMRID = (Label)gvGCGridView.Cells[0].FindControl("lblgvReferralEMRID");
            Label lblgvReferralType = (Label)gvGCGridView.Cells[0].FindControl("lblgvReferralType");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Referral", "ShowReferral('" + lblgvReferralType.Text + "','" + txtFileNo.Text.Trim() + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + lblgvReferralEMRID.Text + "');", true);

        }


        protected void btnXMLGenerate_Click(object sender, EventArgs e)
        {
            string strFileName;
            strFileName = XMLFileWrite("TEST");


        }

        protected void btnAccumedTransfer_Click(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                DataSet DS = new DataSet();
                string Criteria = " ClaimID='" + hidInvoice.Value + "'";
                DS = objCom.fnGetFieldValue("*", "ACCUMED.DBO.ECLAIM_XML_DATA_MASTER", Criteria, "");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = " Already Transfered";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                AccumedBAL objAcc = new AccumedBAL();

                objAcc.BranchID = Convert.ToString(ViewState["BRANCH_ID"]);
                objAcc.ClaimID = hidInvoice.Value;
                objAcc.PatientID = txtFileNo.Text;
                objAcc.VisitType = Convert.ToString(ViewState["VisitType"]);
                objAcc.UserID = Convert.ToString(Session["User_ID"]);
                objAcc.ACCUEclaimXMLDataMasterAdd();


                dboperations dbo = new dboperations();
                DataSet ds = new DataSet();
                Criteria = " 1=1 ";
                Criteria += " AND HPP_PT_ID = '" + txtFileNo.Text + "'";
                Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


                ds = dbo.PatientPhotoGet(Criteria);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                    {

                        Session["SignatureImg1"] = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD"];



                    }

                    if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD1") == false)
                    {
                        Session["SignatureImg2"] = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD1"];


                    }


                }

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnAccumedTransfer_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void btnAccSearch_Click(object sender, EventArgs e)
        {
            try
            {


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnAccSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        void BindXMLMasterBind()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            DS = objCom.fnGetFieldValue("*", "ACCUMED.DBO.ECLAIM_XML_DATA_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                // gvAccumedXMLMaster.DataSource = DS;
                // gvAccumedXMLMaster.DataBind();


            }
        }

        protected void btnAccumedXMLGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                BindXMLMasterBind();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eClaimCoderVerification.btnAccumedXMLGenerate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }




        #endregion

    }
}