﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;
using System.Data.Odbc;
using Excel = Microsoft.Office.Interop.Excel;

namespace Mediplus.HMS.Billing
{
    public partial class Remittance : System.Web.UI.Page
    {
        # region Variable Declaration
        static string stParentName = "0";
        static string strSessionBranchId;
        string strFileName;

        DataSet DS = new DataSet();
        DataSet DS1 = new DataSet();
        dboperations dbo = new dboperations();
        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void LogFIleFileWriting(string strContent)
        {
            try
            {
                string strPath = GlobalValues.Shafafiya_Remittance_FilePath;

                string strName = strPath +  "RemittanceLogFile.txt";


                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void RemittanceLogWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../RemittanceLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BuildRemittance()
        {
            DataTable dt = new DataTable();

            DataColumn SenderID = new DataColumn();
            SenderID.ColumnName = "SenderID";

            DataColumn ProviderId = new DataColumn();
            ProviderId.ColumnName = "ProviderId";

            DataColumn TransactionDate = new DataColumn();
            TransactionDate.ColumnName = "TransactionDate";

            DataColumn RecordCount = new DataColumn();
            RecordCount.ColumnName = "RecordCount";

            DataColumn DispositionFlag = new DataColumn();
            DispositionFlag.ColumnName = "DispositionFlag";

            DataColumn ClaimId = new DataColumn();
            ClaimId.ColumnName = "ClaimId";

            DataColumn IDPayer = new DataColumn();
            IDPayer.ColumnName = "IDPayer";

            DataColumn PaymentReference = new DataColumn();
            PaymentReference.ColumnName = "PaymentReference";

            DataColumn DateSettlement = new DataColumn();
            DateSettlement.ColumnName = "DateSettlement";

            DataColumn ActivityId = new DataColumn();
            ActivityId.ColumnName = "ActivityId";

            DataColumn Start1 = new DataColumn();
            Start1.ColumnName = "Start1";

            DataColumn Type2 = new DataColumn();
            Type2.ColumnName = "Type2";

            DataColumn Code2 = new DataColumn();
            Code2.ColumnName = "Code2";

            DataColumn Quantity = new DataColumn();
            Quantity.ColumnName = "Quantity";

            DataColumn Net1 = new DataColumn();
            Net1.ColumnName = "Net1";

            DataColumn List = new DataColumn();
            List.ColumnName = "List";

            DataColumn Clinician = new DataColumn();
            Clinician.ColumnName = "Clinician";

            DataColumn PriorAuthorizationID = new DataColumn();
            PriorAuthorizationID.ColumnName = "PriorAuthorizationID";

            DataColumn Gross = new DataColumn();
            Gross.ColumnName = "Gross";

            DataColumn PatientShare = new DataColumn();
            PatientShare.ColumnName = "PatientShare";

            DataColumn PaymentAmount = new DataColumn();
            PaymentAmount.ColumnName = "PaymentAmount";

            DataColumn DenialCode = new DataColumn();
            DenialCode.ColumnName = "DenialCode";


            dt.Columns.Add(SenderID);
            dt.Columns.Add(ProviderId);
            dt.Columns.Add(TransactionDate);
            dt.Columns.Add(RecordCount);
            dt.Columns.Add(DispositionFlag);
            dt.Columns.Add(ClaimId);
            dt.Columns.Add(IDPayer);
            dt.Columns.Add(PaymentReference);
            dt.Columns.Add(DateSettlement);
            dt.Columns.Add(ActivityId);
            dt.Columns.Add(Start1);
            dt.Columns.Add(Type2);
            dt.Columns.Add(Code2);
            dt.Columns.Add(Quantity);
            dt.Columns.Add(Net1);
            dt.Columns.Add(List);
            dt.Columns.Add(Clinician);
            dt.Columns.Add(PriorAuthorizationID);
            dt.Columns.Add(Gross);
            dt.Columns.Add(PatientShare);
            dt.Columns.Add(PaymentAmount);
            dt.Columns.Add(DenialCode);


            DS = new DataSet();
            DS.Tables.Add(dt);


            ViewState["TempRemittance"] = DS;


        }

        protected string valid1(OdbcDataReader myreader, int stval)
        {
            //if any columns are found null then they are replaced by zero  
            object val = myreader[stval];
            if (object.ReferenceEquals(val, DBNull.Value))
            {
                return Convert.ToString(0);
            }
            else
            {
                return val.ToString();
            }
        }


        void BindGrid()
        {
            string Criteria = " 1=1 ";
            ViewState["Criteria"] = Criteria;
            dbo = new dboperations();
            DS = new DataSet();


            if (radRemitFileType.SelectedValue == "HAAD_XML")
            {
                GetDataFromXMLFile();
            }
            else if (radRemitFileType.SelectedValue == "LOCAL_EXCEL")
            {
                if (ValidateExcelFile() == true)
                {
                    GetDataFromExcelFile();
                }
            }


        BindEnd: ;

        }



        void GetDataFromXMLFile()
        {
            if (fileLogo.FileName == "")
            {
                lblStatus.Text = "Please select the XML  file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }

            Boolean IsHeader = false, IsClaim = false, IsEncounter = false, IsActivity = false;

            string strPath = GlobalValues.Shafafiya_Remittance_FilePath;
            if (Path.GetExtension(fileLogo.FileName.ToLower()) == ".xml")
            {
                ViewState["ActualFileName"] = fileLogo.FileName.ToLower();
                strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".XML";

                fileLogo.SaveAs(strPath + strFileName);

                ViewState["FileName"] = strFileName;
                DS.ReadXml(strPath + strFileName);
                ViewState["RawXMLData"] = DS;

            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;

            }



            foreach (DataTable table in DS.Tables)
            {
                if (table.TableName == "Header")
                {
                    IsHeader = true;
                }
                if (table.TableName == "Claim")
                {
                    IsClaim = true;
                }
                if (table.TableName == "Encounter")
                {
                    IsEncounter = true;
                }

                if (table.TableName == "Activity")
                {
                    IsActivity = true;
                }



            }


            if (IsHeader == false || IsClaim == false || IsEncounter == false || IsActivity == false)
            {
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }
            //string strPaymentReference;
            //strPaymentReference=  DS.Tables["Claim"].Rows[0]["PaymentReference"].ToString().Trim();

            //if (dbo.CheckPaymentReference(strPaymentReference) == true)
            //{
            //    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowFileExists();", true);
            //}

            //if (hidUpdateFile.Value == "false")
            //{
            //    goto BindEnd;
            //}

            Decimal decPaymentAmountTotal = 0, decNetAmountTotal = 0, decRejectAmountTotal = 0;

            DataSet DSView = new DataSet();
            DSView = (DataSet)ViewState["TempRemittance"];


            for (int i = 0; i < DS.Tables["Claim"].Rows.Count; i++)
            {
                DataTable DTActivity = new DataTable();
                DataRow[] DRActivity;
                DRActivity = DS.Tables["Activity"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                DataTable DTEncounter = new DataTable();
                DataRow[] DREncounter;
                DREncounter = DS.Tables["Encounter"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());




                for (int j = 0; j < DRActivity.Length; j++)
                {
                    //DataTable DTObservation = new DataTable();
                    //DataRow[] DRObservation;



                    DataRow objrow;
                    objrow = DSView.Tables[0].NewRow();

                    if (DS.Tables["Header"].Columns.Contains("SenderID") == true)
                        objrow["SenderID"] = Convert.ToString(DS.Tables["Header"].Rows[0]["SenderID"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("ReceiverID") == true)
                        objrow["ProviderID"] = Convert.ToString(DS.Tables["Header"].Rows[0]["ReceiverID"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("TransactionDate") == true)
                        objrow["TransactionDate"] = Convert.ToString(DS.Tables["Header"].Rows[0]["TransactionDate"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("RecordCount") == true)
                        objrow["RecordCount"] = Convert.ToString(DS.Tables["Header"].Rows[0]["RecordCount"]).Trim();

                    if (DS.Tables["Header"].Columns.Contains("DispositionFlag") == true)
                        objrow["DispositionFlag"] = Convert.ToString(DS.Tables["Header"].Rows[0]["DispositionFlag"]).Trim();

                    if (DS.Tables["Claim"].Columns.Contains("ID") == true)
                        objrow["ClaimId"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]).Trim();


                    if (DS.Tables["Claim"].Columns.Contains("IDPayer") == true)
                        objrow["IDPayer"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["IDPayer"]).Trim();

                    if (DS.Tables["Claim"].Columns.Contains("PaymentReference") == true)
                        objrow["PaymentReference"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["PaymentReference"]).Trim();


                    if (DS.Tables["Claim"].Columns.Contains("DateSettlement") == true)
                        objrow["DateSettlement"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["DateSettlement"]).Trim();



                    if (DRActivity[0].Table.Columns.Contains("ID") == true)
                        objrow["ActivityId"] = Convert.ToString(DRActivity[j]["ID"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Start") == true)
                        objrow["Start1"] = Convert.ToString(DRActivity[j]["Start"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Type") == true)
                        objrow["Type2"] = Convert.ToString(DRActivity[j]["Type"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Code") == true)
                        objrow["Code2"] = Convert.ToString(DRActivity[j]["Code"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Quantity") == true)
                        objrow["Quantity"] = Convert.ToString(DRActivity[j]["Quantity"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Net") == true)
                        objrow["Net1"] = Convert.ToString(DRActivity[j]["Net"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("List") == true)
                        objrow["List"] = Convert.ToString(DRActivity[j]["List"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Clinician") == true)
                        objrow["Clinician"] = Convert.ToString(DRActivity[j]["Clinician"]).Trim();
                    //if(DRActivity.col

                    if (DRActivity[0].Table.Columns.Contains("PriorAuthorizationID") == true)
                        objrow["PriorAuthorizationID"] = Convert.ToString(DRActivity[j]["PriorAuthorizationID"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("Gross") == true)
                        objrow["Gross"] = Convert.ToString(DRActivity[j]["Gross"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("PatientShare") == true)
                        objrow["PatientShare"] = Convert.ToString(DRActivity[j]["PatientShare"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("PaymentAmount") == true)
                        objrow["PaymentAmount"] = Convert.ToString(DRActivity[j]["PaymentAmount"]).Trim();

                    if (DRActivity[0].Table.Columns.Contains("DenialCode") == true)
                        objrow["DenialCode"] = Convert.ToString(DRActivity[j]["DenialCode"]).Trim();

                    DSView.Tables[0].Rows.Add(objrow);
                }

            }

            RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 1. XML Data Saved in DataSet");
            // File.Delete(strPath + strFileName);

            ViewState["TempRemittance"] = DSView;
            ViewState["Remittance"] = DSView;



            TempSaveXMLData();

            RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 2. XML Data Saved in Temp Table ");

            string Criteria = " 1=1 ";
            Criteria += " AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";


            ViewState["Criteria"] = Criteria;
            DataSet DS1 = new DataSet();
            DS1 = dbo.TempRemittanceGroupByGet(Criteria);
            RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() TempRemittanceGroupByGet() Completed ");
            DataSet DS2 = new DataSet();
            DS2 = dbo.TempRemittanceGet(Criteria);
            ViewState["Remittance"] = DS2;
            RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() TempRemittanceGet() Completed ");

            if (DS1.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DS1;
                gvGridView.DataBind();


                RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 3. XML Data Loaded in  grid ");

                for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                {
                    if (DS1.Tables[0].Rows[i]["PaymentAmount"].ToString() != "")
                    {
                        decPaymentAmountTotal = decPaymentAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["PaymentAmount"]); ;
                    }

                    if (DS1.Tables[0].Rows[i]["Net1"].ToString() != "")
                    {
                        decNetAmountTotal = decNetAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["Net1"]); ;
                    }

                    if (DS1.Tables[0].Rows[i]["Discount"].ToString() != "")
                    {
                        decRejectAmountTotal = decRejectAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["Discount"]); ;
                    }



                }

                RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 4. XML Data Total amount counding loop completed");

                txtCurPay.Text = Convert.ToString(decPaymentAmountTotal);
                txtNetAmt.Text = Convert.ToString(decNetAmountTotal);
                txtRejectAmt.Text = Convert.ToString(decRejectAmountTotal);


                lblTotal.Text = DS1.Tables[0].Rows.Count.ToString().Trim();
                txtCurPay.Text = Convert.ToString(decPaymentAmountTotal);

                txtCompany.Text = DS1.Tables[0].Rows[0]["HIM_BILLTOCODE"].ToString();
                txtCompanyName.Text = DS1.Tables[0].Rows[0]["HIM_INS_NAME"].ToString();
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }


            /*
            lblTotal.Text = "0";
            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DSView;
                gvGridView.DataBind();

                lblTotal.Text = DS.Tables["Activity"].Rows.Count.ToString().Trim();
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }
            */

        BindEnd: ;

        }

        Boolean  ValidateExcelFile()
        {

            Boolean IsNoError = true;
            if (fileLogo.FileName == "")
            {
                lblStatus.Text = "Please select XML the file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                return false;
            }

            string strPath = GlobalValues.Shafafiya_Remittance_FilePath;

            strFileName = "T" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".XLS";

            fileLogo.SaveAs(strPath + strFileName);



            string filelocation = strPath + strFileName;
            string conStr = @"Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=" + filelocation + ";ReadOnly=0;";

            OdbcConnection con = new OdbcConnection(conStr);
            con.Open();

            try
            {
                //string query = "SELECT [Type],[Code],[Desc],[Price],[Service_Type],[Service_Category],[Discount],[Covering_Type],[Net_Amount] FROM [Sheet1$]";

                string query = "SELECT [SenderID], [ProviderId] ,[TransactionDate],[RecordCount],[DispositionFlag],[ClaimId],[IDPayer],[PaymentReference],[DateSettlement],[ActivityId],[Start1],[Type2],[Code2],[Quantity],[Net1],[List],[Clinician],[Gross],[PatientShare],[PaymentAmount],[DenialCode]  FROM [Sheet1$] WHERE [ClaimId] IS NOT NULL";

                OdbcCommand ocmd = new OdbcCommand(query, con);
                OdbcDataReader odr = ocmd.ExecuteReader();


                DataSet DSView = new DataSet();
                DSView = (DataSet)ViewState["TempRemittance"];

                string SenderID = "", ProviderID = "", TransactionDate = "", RecordCount = "", DispositionFlag = "", ClaimId = "", IDPayer = "", PaymentReference = "",
                     DateSettlement = "", ActivityId = "", Start1 = "", Type2 = "", Code2 = "", Quantity = "", Net1 = "", List = "", Clinician = "", Gross = "",
                     PatientShare = "", PaymentAmount = "", DenialCode = "";


                Int32 intLineNo = 1;
                while (odr.Read())
                {



                    SenderID = valid1(odr, 0);
                    ProviderID = valid1(odr, 1);
                    TransactionDate = valid1(odr, 2);
                    RecordCount = valid1(odr, 3);
                    DispositionFlag = valid1(odr, 4);
                    ClaimId = valid1(odr, 5);
                    IDPayer = valid1(odr, 6);
                    PaymentReference = valid1(odr, 7);
                    DateSettlement = valid1(odr, 8);
                    ActivityId = valid1(odr, 9);
                    Start1 = valid1(odr, 10);

                    Type2 = valid1(odr, 11);
                    Code2 = valid1(odr, 12);
                    Quantity = valid1(odr, 13);
                    Net1 = valid1(odr, 14);
                    List = valid1(odr, 15);
                    Clinician = valid1(odr, 16);
                    Gross = valid1(odr, 17);
                    PatientShare = valid1(odr, 18);
                    PaymentAmount = valid1(odr, 19);
                    DenialCode = valid1(odr, 20);

                    if (ClaimId == "" || ClaimId == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " ClaimId is wrong");
                        IsNoError = false;
                    }

                    if (Code2 == "" || Code2 == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " Code2 is wrong");
                        IsNoError = false;
                    }


                    if (Quantity == "" || Quantity == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " Quantity is wrong");
                        IsNoError = false;
                    }

                    if (Net1 == "" || Net1 == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " Net1 is wrong");
                        IsNoError = false;
                    }

                    if (List == "" || List == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " List is wrong");
                        IsNoError = false;
                    }

                    if (Clinician == "" || Clinician == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " Clinician is wrong");
                        IsNoError = false;
                    }

                    if (Gross == "" || Gross == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " Gross is wrong");
                        IsNoError = false;
                    }

                    if (PatientShare == "" )
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " PatientShare is wrong");
                        IsNoError = false;
                    }

                    if (PaymentAmount == "" || PaymentAmount == "0")
                    {

                        LogFIleFileWriting("");
                        LogFIleFileWriting("Line No: " + intLineNo + "   Date:" + System.DateTime.Now.ToString());

                        LogFIleFileWriting("ClaimId:" + ClaimId + " Code:" + Code2 + " PaymentAmount is wrong");
                        IsNoError = false;
                    }


                    intLineNo = intLineNo + 1;

                }

                if (IsNoError == false)
                {
                    lblStatus.Text = "Errer in Remittance file. Please Check the log file RemittanceLogFile.txt";
                    return false;

                }

                con.Close();
                  return true;
            }
            catch (DataException ex)
            {
                  return false;
            }
            finally
            {
                con.Close();
            }

       
        }

        void GetDataFromExcelFile()
        {
            if (fileLogo.FileName == "")
            {
                lblStatus.Text = "Please select XML the file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }

            string strPath = GlobalValues.Shafafiya_Remittance_FilePath;

            strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".XLS";

            fileLogo.SaveAs(strPath + strFileName);

            ViewState["FileName"] = strFileName;

            string filelocation = strPath + strFileName;
            string conStr = @"Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=" + filelocation + ";ReadOnly=0;";

            OdbcConnection con = new OdbcConnection(conStr);
            con.Open();

            try
            {
                //string query = "SELECT [Type],[Code],[Desc],[Price],[Service_Type],[Service_Category],[Discount],[Covering_Type],[Net_Amount] FROM [Sheet1$]";

                string query = "SELECT [SenderID], [ProviderId] ,[TransactionDate],[RecordCount],[DispositionFlag],[ClaimId],[IDPayer],[PaymentReference],[DateSettlement],[ActivityId],[Start1],[Type2],[Code2],[Quantity],[Net1],[List],[Clinician],[Gross],[PatientShare],[PaymentAmount],[DenialCode]  FROM [Sheet1$] WHERE [ClaimId] IS NOT NULL";

                OdbcCommand ocmd = new OdbcCommand(query, con);
                OdbcDataReader odr = ocmd.ExecuteReader();

                //string strType = "", HaadCode = "", ServDesc = "", strCodeType = "", strServiceType = "", strServCode = "", strServiceCat = "", strConveringType = "", strDiscType = "";
                //Decimal decDiscAmt = 0, decPrice = 0, decNetAmount = 0;


                Decimal decPaymentAmountTotal = 0, decNetAmountTotal = 0, decRejectAmountTotal = 0;

                DataSet DSView = new DataSet();
                DSView = (DataSet)ViewState["TempRemittance"];

                Int32 intLineNo = 1;
                while (odr.Read())
                {


                    DataRow objrow;
                    objrow = DSView.Tables[0].NewRow();


                    objrow["SenderID"] = valid1(odr, 0);
                    objrow["ProviderID"] = valid1(odr, 1);
                    objrow["TransactionDate"] = valid1(odr, 2);
                    objrow["RecordCount"] = valid1(odr, 3);
                    objrow["DispositionFlag"] = valid1(odr, 4);
                    objrow["ClaimId"] = valid1(odr, 5);
                    objrow["IDPayer"] = valid1(odr, 6);
                    objrow["PaymentReference"] = valid1(odr, 7);
                    objrow["DateSettlement"] = valid1(odr, 8);
                    objrow["ActivityId"] = valid1(odr, 9);
                    objrow["Start1"] = valid1(odr, 10);

                    objrow["Type2"] = valid1(odr, 11);
                    objrow["Code2"] = valid1(odr, 12);
                    objrow["Quantity"] = valid1(odr, 13);
                    objrow["Net1"] = valid1(odr, 14);
                    objrow["List"] = valid1(odr, 15);
                    objrow["Clinician"] = valid1(odr, 16);
                    objrow["Gross"] = valid1(odr, 17);
                    objrow["PatientShare"] = valid1(odr, 18);
                    objrow["PaymentAmount"] = valid1(odr, 19);
                    objrow["DenialCode"] = valid1(odr, 20);

                    DSView.Tables[0].Rows.Add(objrow);


                    intLineNo = intLineNo + 1;

                }


                RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromExcelFile() 1. Excel Data Saved in DataSet");
                // File.Delete(strPath + strFileName);

                ViewState["TempRemittance"] = DSView;
                ViewState["Remittance"] = DSView;



                TempSaveXMLData();

                RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromExcelFile() 2. Excel Data Saved in Temp Table ");

                string Criteria = " 1=1 ";
                Criteria += " AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";


                ViewState["Criteria"] = Criteria;
                DataSet DS1 = new DataSet();
                DS1 = dbo.TempRemittanceGroupByGet(Criteria);
                RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromExcelFile() TempRemittanceGroupByGet() cmpleted ");
                DataSet DS2 = new DataSet();
                DS2 = dbo.TempRemittanceGet(Criteria);
                ViewState["Remittance"] = DS2;
                RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromExcelFile() TempRemittanceGet() cmpleted ");

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    gvGridView.Visible = true;
                    gvGridView.DataSource = DS1;
                    gvGridView.DataBind();


                    RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromExcelFile() 3. XML Data Loaded in  grid ");

                    for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                    {
                        if (DS1.Tables[0].Rows[i]["PaymentAmount"].ToString() != "")
                        {
                            decPaymentAmountTotal = decPaymentAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["PaymentAmount"]); ;
                        }

                        if (DS1.Tables[0].Rows[i]["Net1"].ToString() != "")
                        {
                            decNetAmountTotal = decNetAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["Net1"]); ;
                        }

                        if (DS1.Tables[0].Rows[i]["Discount"].ToString() != "")
                        {
                            decRejectAmountTotal = decRejectAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["Discount"]); ;
                        }



                    }

                    RemittanceLogWriting(System.DateTime.Now.ToString() + "GetDataFromXMLFile() 4. XML Data Total amount counding loop completed");

                    txtCurPay.Text = Convert.ToString(decPaymentAmountTotal);
                    txtNetAmt.Text = Convert.ToString(decNetAmountTotal);
                    txtRejectAmt.Text = Convert.ToString(decRejectAmountTotal);


                    lblTotal.Text = DS1.Tables[0].Rows.Count.ToString().Trim();
                    txtCurPay.Text = Convert.ToString(decPaymentAmountTotal);

                    txtCompany.Text = DS1.Tables[0].Rows[0]["HIM_BILLTOCODE"].ToString();
                    txtCompanyName.Text = DS1.Tables[0].Rows[0]["HIM_INS_NAME"].ToString();

                }

            ServEmpty: ;
                con.Close();
            }
            catch (DataException ex)
            {
            }
            finally
            {
                con.Close();
            }

        BindEnd: ;
        }

        void TempSaveXMLData()
        {

            DataSet DSView = new DataSet();
            DSView = (DataSet)ViewState["TempRemittance"];

            if (DSView.Tables[0].Rows.Count <= 0)
            {

                lblStatus.Text = "Please select the XML File !";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto funEnd;
            }

            for (int i = 0; i < DSView.Tables[0].Rows.Count; i++)
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_TempRemittancexmlDataAdd";
                cmd.Parameters.Add(new SqlParameter("@SenderID", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["SenderID"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@ProviderId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ProviderId"].ToString().Trim();
                //string strTranDate = DSView.Tables[0].Rows[i]["TransactionDate"].ToString().Trim();
                //if (strTranDate != "")
                //{
                //    string[] arrTranDate = strTranDate.Split('/');

                //    string strForTranDate = "";
                //    if (arrTranDate.Length > 1)
                //    {
                //        strForTranDate = arrTranDate[1] + "/" + arrTranDate[0] + "/" + arrTranDate[2];
                //    }

                //    cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = strForTranDate;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = "";
                //}
                cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["TransactionDate"].ToString().Trim();

                cmd.Parameters.Add(new SqlParameter("@RecordCount", SqlDbType.Int)).Value = DSView.Tables[0].Rows[i]["RecordCount"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@DispositionFlag", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DispositionFlag"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@ClaimId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ClaimId"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@IDPayer", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["IDPayer"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PaymentReference", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["PaymentReference"].ToString().Trim();


                //string strSettleDate = DSView.Tables[0].Rows[i]["DateSettlement"].ToString().Trim();
                //if (strSettleDate != "")
                //{
                //    string[] arrSettleDate = strSettleDate.Split('/');

                //    string strForSettleDate = "";
                //    if (arrSettleDate.Length > 1)
                //    {
                //        strForSettleDate = arrSettleDate[1] + "/" + arrSettleDate[0] + "/" + arrSettleDate[2];
                //    }

                //    cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = strForSettleDate;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = "";
                //}

                cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DateSettlement"].ToString().Trim();

                cmd.Parameters.Add(new SqlParameter("@ActivityId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ActivityId"].ToString().Trim();

                //string strStartDate = DSView.Tables[0].Rows[i]["Start1"].ToString().Trim();
                //if (strStartDate != "")
                //{
                //    string[] arrStartDate = strStartDate.Split('/');

                //    string strForStartDate = "";
                //    if (arrStartDate.Length > 1)
                //    {
                //        strForStartDate = arrStartDate[1] + "/" + arrStartDate[0] + "/" + arrStartDate[2];
                //    }

                //    cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = strForStartDate;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = "";
                //}

                cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Start1"].ToString().Trim();

                cmd.Parameters.Add(new SqlParameter("@Type2", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Type2"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Code2", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Code2"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Quantity"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Net1", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Net1"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@List", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["List"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Clinician", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Clinician"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PriorAuthorizationID", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["PriorAuthorizationID"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Gross", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Gross"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PatientShare", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["PatientShare"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PaymentAmount", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["PaymentAmount"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@DenialCode", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DenialCode"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["FileName"]);
                cmd.Parameters.Add(new SqlParameter("ActualFileName", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ActualFileName"]);

                for (int j = 0; j < cmd.Parameters.Count; j++)
                {
                    if (cmd.Parameters[j].Value == "")
                    {
                        cmd.Parameters[j].Value = DBNull.Value;
                    }
                }




                cmd.ExecuteNonQuery();
                con.Close();
            }
        funEnd: ;
        }


        void SaveXMLData()
        {

            DataSet DSView = new DataSet();
            DSView = (DataSet)ViewState["Remittance"];

            if (DSView.Tables[0].Rows.Count <= 0)
            {

                lblStatus.Text = "Please select the XML File !";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto funEnd;
            }

            for (int i = 0; i < DSView.Tables[0].Rows.Count; i++)
            {

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_RemittancexmlDataAdd";
                cmd.Parameters.Add(new SqlParameter("@SenderID", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["SenderID"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@ProviderId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ProviderId"].ToString().Trim();
                //string strTranDate = DSView.Tables[0].Rows[i]["TransactionDate"].ToString().Trim();
                //if (strTranDate != "")
                //{
                //    string[] arrTranDate = strTranDate.Split('/');

                //    string strForTranDate = "";
                //    if (arrTranDate.Length > 1)
                //    {
                //        strForTranDate = arrTranDate[1] + "/" + arrTranDate[0] + "/" + arrTranDate[2];
                //    }

                //    cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = strForTranDate;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = "";
                //}


                cmd.Parameters.Add(new SqlParameter("@TransactionDate", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["TransactionDate"].ToString().Trim();

                cmd.Parameters.Add(new SqlParameter("@RecordCount", SqlDbType.Int)).Value = DSView.Tables[0].Rows[i]["RecordCount"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@DispositionFlag", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DispositionFlag"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@ClaimId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ClaimId"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@IDPayer", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["IDPayer"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PaymentReference", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["PaymentReference"].ToString().Trim();


                //string strSettleDate = DSView.Tables[0].Rows[i]["DateSettlement"].ToString().Trim();
                //if (strSettleDate != "")
                //{
                //    string[] arrSettleDate = strSettleDate.Split('/');

                //    string strForSettleDate = "";
                //    if (arrSettleDate.Length > 1)
                //    {
                //        strForSettleDate = arrSettleDate[1] + "/" + arrSettleDate[0] + "/" + arrSettleDate[2];
                //    }

                //    cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = strForSettleDate;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = "";
                //}
                cmd.Parameters.Add(new SqlParameter("@DateSettlement", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DateSettlement"].ToString().Trim();


                cmd.Parameters.Add(new SqlParameter("@ActivityId", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["ActivityId"].ToString().Trim();

                //string strStartDate = DSView.Tables[0].Rows[i]["Start1"].ToString().Trim();
                //if (strStartDate != "")
                //{
                //    string[] arrStartDate = strStartDate.Split('/');

                //    string strForStartDate = "";
                //    if (arrStartDate.Length > 1)
                //    {
                //        strForStartDate = arrStartDate[1] + "/" + arrStartDate[0] + "/" + arrStartDate[2];
                //    }

                //    cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = strForStartDate;
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = "";
                //}
                cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Start1"].ToString().Trim();


                cmd.Parameters.Add(new SqlParameter("@Type2", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Type2"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Code2", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Code2"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Quantity"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Net1", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Net1"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@List", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["List"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Clinician", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["Clinician"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PriorAuthorizationID", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["PriorAuthorizationID"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@Gross", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["Gross"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PatientShare", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["PatientShare"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@PaymentAmount", SqlDbType.Decimal)).Value = DSView.Tables[0].Rows[i]["PaymentAmount"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("@DenialCode", SqlDbType.VarChar)).Value = DSView.Tables[0].Rows[i]["DenialCode"].ToString().Trim();
                cmd.Parameters.Add(new SqlParameter("XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["FileName"]);
                cmd.Parameters.Add(new SqlParameter("ActualFileName", SqlDbType.VarChar)).Value = Convert.ToString(ViewState["ActualFileName"]);

                for (int j = 0; j < cmd.Parameters.Count; j++)
                {
                    if (cmd.Parameters[j].Value == "")
                    {
                        cmd.Parameters[j].Value = DBNull.Value;
                    }
                }




                cmd.ExecuteNonQuery();
                con.Close();
            }
        funEnd: ;
        }

        void BindCompanyBalance()
        {
            //   string strCompId = CompanyId;
            // strCompId = "3";
            string strPrevBal = dbo.CompanyBalanceGet(txtCompany.Text.Trim());
            string strCurPaid = txtCurPay.Text;

            txtPrevBal.Text = strPrevBal;
            if (strCurPaid != "")
            {
                txtCurBalance.Text = Convert.ToString(Convert.ToDecimal(strPrevBal) - Convert.ToDecimal(strCurPaid));
            }
        }

        void BindService()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";
            Criteria += " AND HIM_INVOICE_ID='" + Convert.ToString(ViewState["InvoiceId"]) + "'";

            DataSet DS1 = new DataSet();
            DS1 = dbo.TempRemittanceGet(Criteria);

            if (DS1.Tables[0].Rows.Count > 0)
            {
                gvService.Visible = true;
                gvService.DataSource = DS1;
                gvService.DataBind();

            }
            else
            {

                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }

        }

        void Clear()
        {
            if (drpType.Items.Count > 0)
                drpType.SelectedIndex = 1;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtDate.Text = strFromDate.ToString("dd/MM/yyyy");
            if (drpStatus.Items.Count > 0)
                drpStatus.SelectedIndex = 0;

            txtCompany.Text = "";
            txtCompanyName.Text = "";
            txtDescription.Text = "";
            txtRefNo.Text = "";
            txtCurPay.Text = "";
            txtCurBalance.Text = "";
            txtPrevBal.Text = "";
            drpPayType.SelectedIndex = 0;


            txtInvNo.Text = "";
            txtInvDate.Text = "";
            txtAmount.Text = "";
            txtFileNo.Text = "";
            txtName.Text = "";
            txtPolicyNo.Text = "";
            txtRemarks.Text = "";

            txtNetAmt.Text = "";
            txtRejectAmt.Text = "";
            lblTotal.Text = "";

            gvGridView.Visible = false;
            gvGridView.DataBind();

            gvService.Visible = false;
            gvService.DataBind();

            ViewState["DuplicateInvId"] = "";

            ViewState["FileName"] = "";
            ViewState["ActualFileName"] = "";
        }

        void CheckInvoiceDuplication()
        {

            DataSet DSView = new DataSet();
            DSView = (DataSet)ViewState["Remittance"];
            string strClaimId, strIDPayer;

            string strInvoiceId = "";
            ViewState["DuplicateInvId"] = "";

            for (int i = 0; i < DSView.Tables[0].Rows.Count; i++)
            {

                // strClaimId = DSView.Tables[0].Rows[i]["ClaimId"].ToString().Trim();
                strIDPayer = DSView.Tables[0].Rows[i]["IDPayer"].ToString().Trim();


                DS = new DataSet();
                string Criteria = " 1=1 ";

                Criteria += " AND IDPayer='" + strIDPayer + "'";

                DS = dbo.RemittanceXMLDataGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strInvoiceId += Convert.ToString(DS.Tables[0].Rows[0]["ClaimId"]) + ",";

                }
            }
            if (strInvoiceId.Length > 1)
                strInvoiceId = strInvoiceId.Substring(0, strInvoiceId.Length - 1);
            ViewState["DuplicateInvId"] = strInvoiceId;

        funEnd: ;
        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='RECEIPT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnFind.Enabled = false;
                btnClear.Enabled = false;
                btnShow.Enabled = false;
                fileLogo.Enabled = false;
                btnSaveToDB.Enabled = false;
                btnCancel.Enabled = false;

            }


            if (strPermission == "7")
            {


                btnFind.Enabled = false;
                btnClear.Enabled = false;
                btnShow.Enabled = false;
                fileLogo.Enabled = false;
                btnSaveToDB.Enabled = false;
                btnCancel.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");

            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();

                try
                {


                    ViewState["DuplicateInvId"] = "";
                    txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                    stParentName = "0";

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]).Trim();
                    BuildRemittance();



                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 ";
                Criteria += " and ActualFileName='" + fileLogo.FileName.ToLower() + "'";

                DataSet DS = new DataSet();

                DS = dbo.RemittanceXMLDataGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This File already Uploaded";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                Clear();
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                txtRefNo.Text = fileLogo.FileName.ToLower();
                BuildRemittance();
                BindGrid();
                BindCompanyBalance();
                dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "Remittance", "TRANSACTION", "Remittance XML Loaded, File Name is   " + Convert.ToString(ViewState["FileName"]), Convert.ToString(Session["User_ID"]).Trim());
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_btnShow_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGridView.PageIndex = e.NewPageIndex;
            DS = new DataSet();
            DS = (DataSet)ViewState["Remittance"];


            gvGridView.Visible = true;
            gvGridView.DataSource = DS;
            gvGridView.DataBind();



        }

        protected void btnSaveToDB_Click(object sender, EventArgs e)
        {
            try
            {
                //CheckInvoiceDuplication();
                //if (Convert.ToString(ViewState["DuplicateInvId"]) != "" && Convert.ToString(ViewState["DuplicateInvId"]) != null)
                //{
                //    lblStatus.Text = "Following Invoice Already updated " + Convert.ToString(ViewState["DuplicateInvId"]);
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto funEnd;
                //}
                TextFileWriting(System.DateTime.Now.ToString() + "1. XML Data Saving Started");

                //  SaveXMLData(); NOTE: created new store proc and commented this

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                SqlCommand cmd1 = new SqlCommand();

                con.Open();
                cmd1 = new SqlCommand();
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.CommandTimeout = 864000;
                cmd1.CommandText = "HMS_SP_RemittancexmlDataUpload";
                cmd1.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = Convert.ToString(Convert.ToString(ViewState["FileName"])).Trim();

                cmd1.ExecuteNonQuery();
                con.Close();



                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "2. XML Data saved successfully in HMS_REMITTANCE_XML_DATA File Name" + Convert.ToString(ViewState["FileName"]));
                dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "Remittance", "TRANSACTION", "XML data saved to HMS_REMITTANCE_XML_DATA table, File Name is " + Convert.ToString(ViewState["FileName"]), Convert.ToString(Session["User_ID"]).Trim());



                // dbo.RemittancexmlDataAdd(Convert.ToString( ViewState["Criteria"]));

                dbo.TempRemittanceDelete(Convert.ToString(ViewState["FileName"]));

                TextFileWriting(System.DateTime.Now.ToString() + "3. Delete Temp Data");


                SqlConnection con1 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con1.Open();
                cmd1 = new SqlCommand();
                cmd1.Connection = con1;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.CommandTimeout = 864000;
                cmd1.CommandText = "HMS_SP_RemittanceUpdate";

                // cmd1.CommandText = "HMS_SP_RemittanceUpdateTest";
                cmd1.Parameters.Add(new SqlParameter("@HRM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                string strDate = txtDate.Text.Trim();
                if (strDate != "")
                {
                    cmd1.Parameters.Add(new SqlParameter("@DATE", SqlDbType.VarChar)).Value = strDate;
                }
                else
                {
                    cmd1.Parameters.Add(new SqlParameter("@DATE", SqlDbType.VarChar)).Value = "";
                }
                cmd1.Parameters.Add(new SqlParameter("@HRM_STATUS", SqlDbType.VarChar)).Value = drpStatus.SelectedValue;
                cmd1.Parameters.Add(new SqlParameter("@HRM_COMP_ID", SqlDbType.VarChar)).Value = txtCompany.Text.Trim();
                cmd1.Parameters.Add(new SqlParameter("@HRM_COMP_NAME", SqlDbType.VarChar)).Value = txtCompanyName.Text.Trim();
                cmd1.Parameters.Add(new SqlParameter("@HRM_COMP_TYPE", SqlDbType.VarChar)).Value = txtType.Text.Trim();
                cmd1.Parameters.Add(new SqlParameter("@HRM_DESCRIPTION", SqlDbType.VarChar)).Value = txtDescription.Text;
                cmd1.Parameters.Add(new SqlParameter("@HRM_REF_NO", SqlDbType.VarChar)).Value = txtRefNo.Text;
                cmd1.Parameters.Add(new SqlParameter("@HRM_PAYMENT_TYPE", SqlDbType.VarChar)).Value = drpPayType.SelectedValue;
                cmd1.Parameters.Add(new SqlParameter("@HRM_AMT_RCVD", SqlDbType.VarChar)).Value = txtCurPay.Text.Trim() != "" ? txtCurPay.Text.Trim() : "0";

                cmd1.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = ViewState["FileName"];
                cmd1.Parameters.Add(new SqlParameter("@CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]).Trim(); ;


                cmd1.ExecuteNonQuery();
                con1.Close();

                dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]).Trim(), "Remittance", "TRANSACTION", "Remittance Updated, File Name is " + Convert.ToString(ViewState["FileName"]), Convert.ToString(Session["User_ID"]).Trim());
                lblStatus.Text = "Details Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                Clear();
                txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
                TextFileWriting(System.DateTime.Now.ToString() + "5. Remittance Updated.");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_btnSaveToDB_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Details Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        funEnd: ;

        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {

                gvService.Visible = false;
                gvService.DataBind();

                string Criteria = " 1=1 ";
                Criteria += " AND XMLFILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";

                if (txtInvNo.Text.Trim() != "")
                {
                    Criteria += " AND HIM_INVOICE_ID='" + txtInvNo.Text.Trim() + "'";
                }

                string strStartDate = txtInvDate.Text;
                string[] arrDate = strStartDate.Split('/');
                string strForInvtDate = "";

                if (arrDate.Length > 1)
                {
                    strForInvtDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }

                if (txtInvDate.Text.Trim() != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) ='" + strForInvtDate + "'";
                }

                if (txtAmount.Text.Trim() != "")
                {
                    //Criteria += " AND   (HIM_CLAIM_AMOUNT - HIM_CLAIM_RETURN_AMT) =" + txtAmount.Text.Trim();

                    Criteria += " AND   PaymentAmount =" + txtAmount.Text.Trim();
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_ID ='" + txtFileNo.Text.Trim() + "'";
                }

                if (txtName.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_NAME like '" + txtName.Text.Trim() + "%'";
                }

                if (txtPolicyNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_POLICY_NO ='" + txtPolicyNo.Text.Trim() + "'";
                }

                if (txtRemarks.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_REMARKS ='" + txtRemarks.Text.Trim() + "'";
                }

                ViewState["Criteria"] = Criteria;
                Decimal decPaymentAmountTotal = 0, decNetAmountTotal = 0, decRejectAmountTotal = 0;
                DataSet DS1 = new DataSet();
                DS1 = dbo.TempRemittanceGroupByGet(Criteria);
                txtCurPay.Text = "";
                txtNetAmt.Text = "";
                txtRejectAmt.Text = "";


                DataSet DS2 = new DataSet();
                DS2 = dbo.TempRemittanceGet(Criteria);
                ViewState["Remittance"] = DS2;

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    gvGridView.Visible = true;
                    gvGridView.DataSource = DS1;
                    gvGridView.DataBind();

                    lblTotal.Text = DS1.Tables[0].Rows.Count.ToString().Trim();

                    for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                    {
                        if (DS1.Tables[0].Rows[i]["PaymentAmount"].ToString() != "")
                        {
                            decPaymentAmountTotal = decPaymentAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["PaymentAmount"]); ;
                        }

                        if (DS1.Tables[0].Rows[i]["Net1"].ToString() != "")
                        {
                            decNetAmountTotal = decNetAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["Net1"]); ;
                        }

                        if (DS1.Tables[0].Rows[i]["Discount"].ToString() != "")
                        {
                            decRejectAmountTotal = decRejectAmountTotal + Convert.ToDecimal(DS1.Tables[0].Rows[i]["Discount"]); ;
                        }



                    }

                    txtCurPay.Text = Convert.ToString(decPaymentAmountTotal);
                    txtNetAmt.Text = Convert.ToString(decNetAmountTotal);
                    txtRejectAmt.Text = Convert.ToString(decRejectAmountTotal);


                }
                else
                {
                    gvGridView.Visible = false;

                    lblStatus.Text = "No Data !";
                    lblStatus.ForeColor = System.Drawing.Color.Red;

                }

                BindCompanyBalance();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_btnFind_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {
            string strName = txtCompany.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];

            }
        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            string strName = txtCompanyName.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            txtReceiptNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]).Trim(), "RECEIPT");
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblInvoiceId;


                lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");

                ViewState["InvoiceId"] = lblInvoiceId.Text;
                BindService();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     Remittance.aspx_Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}