﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="GenerateEClaim.aspx.cs" Inherits="Mediplus.HMS.Billing.GenerateEClaim" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

 
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
      <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

     
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

     <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>
      <script language="javascript" type="text/javascript">

          function OnlyNumeric(evt) {
              var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
              if (chCode >= 48 && chCode <= 57 ||
                   chCode == 46) {
                  return true;
              }
              else

                  return false;
          }

          function BindCompanyDtls(CompanyId, CompanyName) {

              document.getElementById("<%=txtCompany.ClientID%>").value = CompanyId;
             document.getElementById("<%=txtCompanyName.ClientID%>").value = CompanyName;

         }

         function InsurancePopup(PageName, strValue, evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode == 126) {
                 var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                 win.focus();
                 return false;
             }
             return true;
         }

         function CompIdSelected() {
             if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];


                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();


                }
            }

            return true;
        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }



        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if ('<%=radGeneratedType.SelectedValue%>' == "DB") {

                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Date";
                    document.getElementById('<%=txtFromDate.ClientID%>').focus;
                    return false;
                }

                if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtFromDate.ClientID%>').focus()
                    return false;
                }


                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Date";
                    document.getElementById('<%=txtToDate.ClientID%>').focus;
                    return false;
                }

                if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtToDate.ClientID%>').focus()
                    return false;
                }

                if (document.getElementById('<%=txtCompany.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company";
                    document.getElementById('<%=txtCompany.ClientID%>').focus;
                    return false;
                }

                if (document.getElementById('<%=txtCompanyName.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company";
                    document.getElementById('<%=txtCompanyName.ClientID%>').focus;
                    return false;
                }

            }

            return true;
        }



        function ValClaimGenerate() {

            var TotalClaims = '<%=  strTotalClaims %>'
                var TotalAmount = '<%=  strTotalAmount %>'


                var isClaimGenerate = window.confirm('Total No. of claims : ' + TotalClaims + ' Total Amount : ' + TotalAmount + ' Do you want to Continue ? ');
                document.getElementById('<%=hidClaimGenerate.ClientID%>').value = isClaimGenerate;

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
      <input type="hidden" id="hidPermission" runat="server" value="9" />

     <input type="hidden" id="hidClaimGenerate" runat="server"   />
     
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>

    <div style="padding-top: 5px;">
        <asp:HiddenField ID="hidIsEclaimSub" runat="server" />

        <table width="900px" cellpadding="3" cellspacing="3">
            <tr>
                <td style="width: 150px;height:30px;">
                    <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                        Text="From"></asp:Label><span style="color:red;">* </span>
                </td>
                <td style="width: 400px" colspan="3">
                    <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                    <asp:TextBox ID="txtFromTime" runat="server" Width="30px" MaxLength="5" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                    &nbsp;

 
                    <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                        Text="To"></asp:Label>

                    <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle"    onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                    <asp:TextBox ID="txtToTime" runat="server" Width="30px" MaxLength="5" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>


                </td>
            </tr>
            <tr>
                <td style="width: 150px;height:30px;">
                    <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                        Text="Company"></asp:Label><span style="color:red;">* </span>

                </td>
                <td style="width: 400px">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="TextBoxStyle"    Width="70px" MaxLength="10" onKeyUp="DoUpper(this);" onblur="return CompIdSelected()" onkeypress="return InsurancePopup('CompanyId',this.value,event)"></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle"     Width="270px" MaxLength="50" onKeyUp="DoUpper(this);" onblur="return CompNameSelected()" onkeypress="return InsurancePopup('CompanyName',this.value,event)"></asp:TextBox>
                              <div ID="divwidth" style="visibility:hidden;"></div>
                             <div ID="div1" style="visibility:hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany" 
                                 CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth" ></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName" 
                                 CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth" ></asp:AutoCompleteExtender>

                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>

                <td style="width: 100px">
                    

                </td>
                <td style="width: 100px">
                   



                </td>
            </tr>
            
            <tr>
                  <td style="width: 150px;height:30px;">
                    <asp:Label ID="Label38" runat="server" CssClass="lblCaption1"
                        Text="Invoice No."></asp:Label>

                </td>
                <td style="width: 400px" colspan="3">
                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle"    Width="200px" MaxLength="10" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                   

                </td>

                <td colspan="3" style="width: 200px">
                    

                </td>


            </tr>
            <tr id="Tr1"   runat="server" visible="false">
                <td style="width: 120px">
                    <asp:Label ID="Label7" runat="server" CssClass="lblCaption1"
                        Text="Eclaim Generated Type"></asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList ID="radGeneratedType" runat="server" CssClass="label" Width="150px" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="radGeneratedType_SelectedIndexChanged">
                        <asp:ListItem Text="DB" Value="DB" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="File" Value="File>"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="300px">
                                <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnShow" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnShow" runat="server" CssClass="button red small" Width="70px" OnClientClick="return Val();"
                                OnClick="btnShow_Click" Text="Show" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td colspan="3">
                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button gray small" Width="70px" OnClick="btnClear_Click" />
                </td>
            </tr>

        </table>
    </div>
     
    <div style="padding-top: 0px; width: 900px; height: 375px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False"  OnRowDataBound="gvGridView_RowDataBound"
                    EnableModelValidation="True" Width="100%"   OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200">
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="Claim Id">
                            <ItemTemplate>
                                <asp:Label ID="lnlInvId" CssClass="GridRow" runat="server" Text='<%# Bind("InvoiceID") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Provider Id">
                            <ItemTemplate>
                                <asp:Label ID="lblProviderId" CssClass="GridRow" runat="server" Text='<%# Bind("ProviderId") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Company Code">
                            <ItemTemplate>
                                <asp:Label ID="lblCompId" CssClass="GridRow" runat="server" Text='<%# Bind("CompId") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID">
                            <ItemTemplate>
                                <asp:Label ID="lblID" CssClass="GridRow" runat="server" Text='<%# Bind("ID") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IDPayer">
                            <ItemTemplate>
                                <asp:Label ID="lblIDPayer" CssClass="GridRow" runat="server" Text='<%# Bind("IDPayer") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PayerId">
                            <ItemTemplate>
                                <asp:Label ID="lblPayerId" CssClass="GridRow" runat="server" Text='<%# Bind("PayerId") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MemberID">
                            <ItemTemplate>
                                <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("MemberID") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Emirates ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEmiratesIDNumber" CssClass="GridRow" runat="server" Text='<%# Bind("EmiratesIDNumber") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Gross">
                            <ItemTemplate>
                                <asp:Label ID="lblGross" CssClass="GridRow" runat="server" Text='<%# Eval("Gross", "{0:0.00}") %>'  Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share">
                            <ItemTemplate>
                                <asp:Label ID="lblPatientShare" CssClass="GridRow" runat="server" Text='<%# Eval("PatientShare", "{0:0.00}") %>'   Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Net">
                            <ItemTemplate>
                                <asp:Label ID="lblNet" CssClass="GridRow" runat="server"  Text='<%# Eval("Net", "{0:0.00}") %>'    Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType" CssClass="GridRow" runat="server" Text='<%# Bind("Type") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PatientID">
                            <ItemTemplate>
                                <asp:Label ID="lblPatientID" CssClass="GridRow" runat="server" Text='<%# Bind("PatientID") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start">
                            <ItemTemplate>
                                <asp:Label ID="lblStart" CssClass="GridRow" runat="server" Text='<%# Bind("Start") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End">
                            <ItemTemplate>
                                <asp:Label ID="lblEnd" CssClass="GridRow" runat="server" Text='<%# Bind("End") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start Type">
                            <ItemTemplate>
                                <asp:Label ID="lblStartType" CssClass="GridRow" runat="server" Text='<%# Bind("StartType") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Type">
                            <ItemTemplate>
                                <asp:Label ID="lblEndType" CssClass="GridRow" runat="server" Text='<%# Bind("EndType") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType1" CssClass="GridRow" runat="server" Text='<%# Bind("Type1") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblCode" CssClass="GridRow" runat="server" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType3" CssClass="GridRow" runat="server" Text='<%# Bind("Type3") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblCode3" CssClass="GridRow" runat="server" Text='<%# Bind("Code3") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS2" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS2") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS2" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS2") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS3" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS3") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS3" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS3") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS4" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS4") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS4" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS4") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS5" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS5") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS5" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS5") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS6" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS6") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS6" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS6") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS7" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS7") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS7" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS7") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS8" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS8") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS8" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS8") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS9" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS9") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS9" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS9") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDS10" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS10") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS10" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS10") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="lblSICDS11" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS11") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS11" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS11") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS12") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS12" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS12") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label10" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS13") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeS13" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS13") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label12" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS14") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label13" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS14") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label14" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS15") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label15" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS15") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label16" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS16") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label17" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS16") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label18" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS17") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label19" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS17") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label20" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS18") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label21" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS18") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label22" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS19") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label23" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS19") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label24" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS20") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label25" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS20") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label26" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS21") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label27" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS21") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label28" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS22") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label29" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS22") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label30" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS23") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label31" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS23") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label32" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS24") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label33" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS24") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label34" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS25") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label35" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS25") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Diagnosis Type">
                            <ItemTemplate>
                                <asp:Label ID="Label36" CssClass="GridRow" runat="server" Text='<%# Bind("ICDS26") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Diagnosis Code">
                            <ItemTemplate>
                                <asp:Label ID="Label37" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeS26") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>








                        <asp:TemplateField HeaderText="Admitting Type">
                            <ItemTemplate>
                                <asp:Label ID="lblICDA" CssClass="GridRow" runat="server" Text='<%# Bind("ICDA") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Admitting Code">
                            <ItemTemplate>
                                <asp:Label ID="lblICDCodeA" CssClass="GridRow" runat="server" Text='<%# Bind("ICDCodeA") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Admitting Start">
                            <ItemTemplate>
                                <asp:Label ID="lblStart1" CssClass="GridRow" runat="server" Text='<%# Bind("Start1") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Activity Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType2" CssClass="GridRow" runat="server" Text='<%# Bind("Type2") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activity Code">
                            <ItemTemplate>
                                <asp:Label ID="lblCode2" CssClass="GridRow" runat="server" Text='<%# Bind("Code2") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Quantity">
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" CssClass="GridRow" runat="server" Text='<%# Bind("Quantity") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Net">
                            <ItemTemplate>
                                <asp:Label ID="lblNet1" CssClass="GridRow" runat="server" Text='<%# Bind("Net1") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Clinician">
                            <ItemTemplate>
                                <asp:Label ID="lblClinician" CssClass="GridRow" runat="server" Text='<%# Bind("Clinician") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PriorAuth. Id">
                            <ItemTemplate>
                                <asp:Label ID="lblPriorAuthorizationId" CssClass="GridRow" runat="server" Text='<%# Bind("PriorAuthorizationId") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Observ. Type">
                            <ItemTemplate>
                                <asp:Label ID="lblObservType" CssClass="GridRow" runat="server" Text='<%# Bind("ObservType") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ Code">
                            <ItemTemplate>
                                <asp:Label ID="lblObservCode" CssClass="GridRow" runat="server" Text='<%# Bind("ObservCode") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observ. Value">
                            <ItemTemplate>
                                <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("ObservValue") %>' Width="130px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                       
                       
                    </Columns>

                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <table width="70%">
        <tr>
            <td class="style2">
                <asp:Label ID="Label6" runat="server" CssClass="label" Font-Bold="true"
                    Text="Total No. of Claims :"></asp:Label>
                &nbsp;
                <asp:Label ID="lblTotalClaims" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                  <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                    Text="Total Amount :"></asp:Label>
                &nbsp;
                <asp:Label ID="lblTotalAmount" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
            </td>


        </tr>

    </table>
    <br />
    <div>
        <table width="350px" align="center">
            <tr>
                <td>
                    <asp:Button ID="btnTestEclaim" runat="server" CssClass="button red small" Width="150px" OnClientClick="return ValClaimGenerate();"
                        OnClick="btnTestEclaim_Click" Text="Test E-Claim" />
                </td>
                <td>
                    <asp:Button ID="btnProductionEclaim" runat="server" CssClass="button red small" Width="150px"  OnClientClick="return ValClaimGenerate();"
                        OnClick="btnProductionEclaim_Click" Text="Production E-Claim" />
                </td>
                <td>
                    <asp:Button ID="btnSaveToDB" runat="server" CssClass="button orange small" Width="150px"
                        OnClick="btnSaveToDB_Click" Text="Save to DB" visible="false" />

                </td>
            </tr>
        </table>

    </div>
    </div>
                 <br /> <br /> <br />
    
 
       </asp:Content>