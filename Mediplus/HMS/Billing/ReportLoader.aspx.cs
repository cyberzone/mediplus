﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;
using System.Text;
using System.Globalization;
using System.Collections;
using System.Drawing;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Mediplus.HMS.Billing
{
    public partial class ReportLoader : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();
        static string strSessionBranchId;
        public string strPageHeader = "";

        public string strCriteria = "";

        string strDataSource = (string)System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].Trim();
        string strDBName = (string)System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].Trim();
        string strDBUserId = (string)System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].Trim();
        string strDBUserPWD = (string)System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].Trim();


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ReportLoaderLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../AccuReportLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            DataSet ds = new DataSet();
            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpUsers.DataSource = ds;
                drpUsers.DataValueField = "HUM_USER_ID";
                drpUsers.DataTextField = "HUM_USER_NAME";
                drpUsers.DataBind();

            }

            drpUsers.Items.Insert(0, "--- All --");
            drpUsers.Items[0].Value = "";

        }

        void BindDepartment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HDM_DEP_NAME";//HDM_DEP_ID
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataBind();



            }

            drpDepartment.Items.Insert(0, "--- All --");
            drpDepartment.Items[0].Value = "";

            ds.Clear();
            ds.Dispose();
        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                // txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();

            }

        }

        void BindNationality()
        {
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "--- All ---");
            drpNationality.Items[0].Value = "";


            ds.Clear();
            ds.Dispose();

        }

        void BindAgeCondition()
        {
            drpAge.Items.Insert(0, "");
            drpAge.Items[0].Value = "";

            drpAge.Items.Insert(1, "=");
            drpAge.Items[1].Value = "=";

            drpAge.Items.Insert(2, ">=");
            drpAge.Items[2].Value = ">=";

            drpAge.Items.Insert(3, "<=");
            drpAge.Items[3].Value = "<=";

            drpAge.Items.Insert(4, ">");
            drpAge.Items[4].Value = ">";

            drpAge.Items.Insert(5, "<");
            drpAge.Items[5].Value = "<";
        }

        void ReportExportToExcel(string ReportName, string SelectionFormula1, string FileName, string strFolderName)
        {
            ReportDocument crystalReport1 = new ReportDocument();

            try
            {
                string CoderVerification_FilePath = System.Configuration.ConfigurationSettings.AppSettings["CoderVerification_FilePath"].ToString().Trim();


                string ReportName1 = ReportName;
                crystalReport1.Load(GlobalValues.REPORT_PATH + ReportName1);
                crystalReport1.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, GlobalValues.DB_SERVER1, GlobalValues.DB_DATABASE);

                ReportLoaderLog("DataBase Credential done ");
                crystalReport1.RecordSelectionFormula = SelectionFormula1;

                string filelocation = @CoderVerification_FilePath + strFolderName + "\\" + FileName;

                crystalReport1.ExportToDisk(ExportFormatType.Excel, filelocation);

                string strFullPath = @filelocation;
                // File.Copy(strFullPath , @"C:\" + FileName);

                ReportLoaderLog("Excel Exported Completed ");

                Response.ContentType = "APPLICATION/OCTET-STREAM";
                String Header = FileName;
                Response.AppendHeader("Content-Disposition", Header);
                System.IO.FileInfo Dfile = new System.IO.FileInfo(@filelocation);
                Response.WriteFile(Dfile.FullName);
                //Don't forget to add the following line
                Response.End();


                //Response.ContentType = "image/jpeg";
                //Response.AppendHeader("Content-Disposition", "attachment; filename=SailBig.jpg");
                //Response.TransmitFile(Server.MapPath("~/images/sailbig.jpg"));
                //Response.End();

                //WebClient WC = new WebClient();
                //WC.DownloadFile(

                ReportLoaderLog("Export function finished ");

            }
            catch (Exception ex)
            {


                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MRDAudit.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
            finally
            {
                crystalReport1.Close();
                crystalReport1.Dispose();
            }

        }

        void BindReport()
        {
            string Criteria = " 1=1  and type  ='" + hidPageName.Value + "'";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.MediplusReportGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpReport.DataSource = ds;
                drpReport.DataValueField = "Code";
                drpReport.DataTextField = "Name";
                drpReport.DataBind();

            }

            drpReport.Items.Insert(0, "--- Select --");
            drpReport.Items[0].Value = "";

        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.CommonMastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                if (strType == "APP_ROOM")
                {
                    drpRoom.DataSource = DS;
                    drpRoom.DataTextField = "HCM_DESC";
                    drpRoom.DataValueField = "HCM_CODE";
                    drpRoom.DataBind();
                }
            }


            if (strType == "APP_ROOM")
            {
                drpRoom.Items.Insert(0, "--- All ---");
                drpRoom.Items[0].Value = "";
            }


        }

        DataSet GetServiceMasterName(string SearchType, string ServCode)
        {


            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            dboperations dbo = new dboperations();
            DataSet DSServ = new DataSet();
            DSServ = dbo.ServiceMasterGet(Criteria);
            txtServName.Text = "";


            return DSServ;

        }

        void LoadServiceDtls(string ServCode)
        {

            DataSet DSServ = new DataSet();

            DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);

            //ProcessTimLog("fnServiceAdd()_ 1");
            if (DSServ.Tables[0].Rows.Count > 0)
            {
                txtServCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                txtServName.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);

            }


        }


        void BindAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentStatusGet("1=1");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpAppStatus.DataSource = DS;
                drpAppStatus.DataTextField = "HAS_STATUS";
                drpAppStatus.DataValueField = "HAS_ID";
                drpAppStatus.DataBind();


            }

            drpAppStatus.Items.Insert(0, "--- All ---");
            drpAppStatus.Items[0].Value = "";

        }

        void CompanyParentGet()
        {

            DataSet ds = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND HCM_COMP_ID =  HCM_BILL_CODE  ";


            //Criteria += " AND HCM_COMP_ID in (select HCM_BILL_CODE from HMS_COMPANY_MASTER where  HCM_COMP_ID !=  HCM_BILL_CODE ) ";
            drpParentCompany.Items.Clear();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpParentCompany.DataSource = ds;
                drpParentCompany.DataValueField = "HCM_COMP_ID";
                drpParentCompany.DataTextField = "HCM_NAME";
                drpParentCompany.DataBind();



            }
            drpParentCompany.Items.Insert(0, "All");
            drpParentCompany.Items[0].Value = "";





            drpParentCompany.SelectedIndex = 0;


            ds.Clear();
            ds.Dispose();

        }


        void Clear()
        {

            txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

            txtCompany.Text = "";
            txtCompanyName.Text = "";

            txtDoctorID.Text = "";
            txtDoctorName.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            drpInvType.SelectedIndex = 0;
            drpTreatType.SelectedIndex = 0;

            if (drpUsers.Items.Count > 0)
                drpUsers.SelectedIndex = 0;

            txtFileNo.Text = "";
            txtFName.Text = "";
            drpSex.SelectedIndex = 0;
            drpAge.SelectedIndex = 0;
            txtAge.Text = "";

            if (drpDepartment.Items.Count > 0)
                drpDepartment.SelectedIndex = 0;

            if (drpRoom.Items.Count > 0)
                drpRoom.SelectedIndex = 0;

            txtMobile.Text = "";

            if (drpParentCompany.Items.Count > 0)
            {
                drpParentCompany.SelectedIndex = 0;
            }


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=HMSReports");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='" + hidPageName.Value + "' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnShowReport.Visible = false;
                btnReportExport.Visible = false;
            }


            if (strPermission == "7")
            {


                btnShowReport.Visible = false;
                btnReportExport.Visible = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=HMSReports");

            }
        }
        #endregion


        #region AutoExt
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchCompany(string prefixText, int count)
        {
            //string conString = ConfigurationManager.ConnectionStrings["AdventureWorksConnectionString"].ConnectionString;
            string conString = (string)System.Configuration.ConfigurationSettings.AppSettings["ConStr"].Trim();

            using (SqlConnection conn = new SqlConnection(conString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = "SELECT * FROM HMS_COMPANY_MASTER where HCM_COMP_ID like @Searchtext + '%'";
                    cmd.Parameters.AddWithValue("@Searchtext", prefixText);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> names = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                           // names.Add(sdr["HCM_COMP_ID"].ToString());
                            string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(sdr["HCM_NAME"].ToString(), sdr["HCM_NAME"].ToString());
                            names.Add(item);
                        }
                    }
                    conn.Close();
                    return names;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string customerId = Request.Form[hfCustomerId.UniqueID];
            string customerName = Request.Form[txtCompany.UniqueID];
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText, string contextKey)
        {
            string BillToCode = "";
            string[] strContKey = contextKey.Split('|');

            if (strContKey.Length > 0)
            {
                BillToCode = strContKey[0];
            }
            string[] Data;
            string Criteria = " 1=1 ";

            if (BillToCode != "")
            {
                Criteria += " AND HCM_BILL_CODE='" + BillToCode + "'";
            }

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText, string contextKey)
        {
            string BillToCode = "";
            string[] strContKey = contextKey.Split('|');

            if (strContKey.Length > 1)
            {
                BillToCode = strContKey[0];
            }
            string[] Data;
            string Criteria = " 1=1 ";

            if (BillToCode != "")
            {
                Criteria += " AND HCM_BILL_CODE='" + BillToCode + "'";
            }

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            Criteria += " AND  HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }




            string[] Data1 = { "" };

            return Data1;

        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            Criteria += " AND  HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }


            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";
            if (!IsPostBack)
            {
                string DBString = "$" + strDataSource + "$" + strDBName + "$" + strDBUserId + "$" + strDBUserPWD;

                //AuExtCompany.ServiceMethod = "GetCompany";
                //AuExtCompany.UseContextKey = true;
                //AuExtCompany.ContextKey = drpParentCompany.SelectedValue;

                AuExtCompanyName.ServiceMethod = "GetCompanyName";
                AuExtCompanyName.UseContextKey = true;
                AuExtCompanyName.ContextKey = drpParentCompany.SelectedValue;


                hidDBString.Value = DBString;
                strSessionBranchId = Convert.ToString(Session["Branch_ID"]);
                strPageHeader = Convert.ToString(Request.QueryString["PageName"]);

                hidPageName.Value = strPageHeader;

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                try
                {



                    if (strPageHeader == "PatientReport")
                    {
                        hidPageName.Value = "RPTPATIENT";
                    }
                    else if (strPageHeader == "BillingReport")
                    {
                        hidPageName.Value = "RPTBILLING";
                    }
                    else if (strPageHeader == "IncomeReport")
                    {
                        hidPageName.Value = "RPTINCOME";
                    }





                    ViewState["PageName"] = strPageHeader;
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                    BindUsers();
                    BindNationality();
                    BindAgeCondition();
                    BindDepartment();
                    BindReport();
                    BindCommonMaster("APP_ROOM");
                    BindAppointmentStatus();
                    CompanyParentGet();

                    string PatientId = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);

                    if (PatientId != " " && PatientId != null)
                    {
                        txtFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();
                    }

                    switch (strPageHeader)
                    {

                        case "HMS_RPT_BILLING"://BillingReport
                            {
                                lblReportHeader.Text = "Billing Report";
                                trCompany.Visible = true;
                                trFileNo.Visible = true;
                                trInvTYpe.Visible = true;
                                lblType.Text = "Inv. Type";
                                trUsers.Visible = true;
                                trTreatType.Visible = true;
                                break;
                            }
                        case "IncomeReport":
                            {
                                lblReportHeader.Text = "Income Report";
                                trCompany.Visible = true;
                                trFileNo.Visible = true;
                                trInvTYpe.Visible = true;
                                lblType.Text = "Inv. Type";
                                trUsers.Visible = true;
                                trTreatType.Visible = true;
                                trDepartment.Visible = true;
                                break;
                            }
                        case "PatientReport":
                            {
                                lblReportHeader.Text = "Patient Report";
                                trCompany.Visible = true;
                                trFileNo.Visible = true;
                                trInvTYpe.Visible = true;
                                lblType.Text = "Patient Type";
                                trUsers.Visible = true;

                                trNationality.Visible = true;
                                trSexAge.Visible = true;
                                break;
                            }
                        case "CompReceiptDtls":
                            {
                                lblReportHeader.Text = "Company Receipts Dtls Report";
                                trCompany.Visible = true;
                                trFileNo.Visible = false;
                                trInvTYpe.Visible = false;
                                lblType.Text = "Patient Type";
                                trUsers.Visible = false;

                                trNationality.Visible = false;
                                trSexAge.Visible = false;
                                break;
                            }
                        case "InsuranceStatment":
                            {
                                lblReportHeader.Text = "Insurance Statment Report";
                                trCompany.Visible = true;
                                trFileNo.Visible = false;
                                trInvTYpe.Visible = false;
                                lblType.Text = "Patient Type";
                                trUsers.Visible = false;

                                trNationality.Visible = false;
                                trSexAge.Visible = false;
                                break;
                            }
                        case "ClaimTransfer":
                            {
                                lblReportHeader.Text = "Transfer Report";
                                //   btnShowReport.Style.Add("display","none");
                                //  btnShowClaimTransferDtls.Visible = true;
                                //  btnShowClaimTransferSummary.Visible = true;

                                break;
                            }

                        case "HMS_APPOINTMENT":
                            {
                                lblReportHeader.Text = "Appointment Report";
                                trCompany.Visible = false;
                                trService.Visible = false;
                                trRoom.Visible = true;
                                trFileNo.Visible = true;
                                trMobile.Visible = true;
                                trDepartment.Visible = true;
                                trAppStatus.Visible = true;

                                //   btnShowReport.Style.Add("display","none");
                                //  btnShowClaimTransferDtls.Visible = true;
                                //  btnShowClaimTransferSummary.Visible = true;

                                break;
                            }

                        default:
                            lblReportHeader.Text = "Report";
                            break;

                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString());
                    TextFileWriting("ReportLoader.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }


        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {
            string strName = txtCompany.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];

            }
        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            string strName = txtCompanyName.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];

            }
        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {

            string strDoctorID = txtDoctorID.Text;
            if (strDoctorID.Length > 1)
            {
                if (strDoctorID.Substring(strDoctorID.Length - 1, 1) == "~")
                {
                    strDoctorID = strDoctorID.Substring(0, strDoctorID.Length - 1);
                }
            }
            txtDoctorID.Text = strDoctorID;

            string strName = txtDoctorID.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0];
                txtDoctorName.Text = arrName[1];

            }



        }

        protected void txtDoctorName_TextChanged(object sender, EventArgs e)
        {
            string strName = txtDoctorName.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0];
                txtDoctorName.Text = arrName[1];

            }


        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {

                string strName = txtServCode.Text.Trim();

                string[] arrName = strName.Split('~');
                if (arrName.Length > 1)
                {
                    txtServCode.Text = arrName[0].Trim();
                    txtServName.Text = arrName[1].Trim();

                }

                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void txtServName_TextChanged(object sender, EventArgs e)
        {
            try
            {

                string strName = txtServName.Text.Trim();

                string[] arrName = strName.Split('~');
                if (arrName.Length > 1)
                {
                    txtServCode.Text = arrName[0].Trim();
                    txtServName.Text = arrName[1].Trim();

                }

                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PatientDataBind();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btnExcelExport_Click(object sender, EventArgs e)
        {

            string CoderVerification_FilePath = System.Configuration.ConfigurationSettings.AppSettings["CoderVerification_FilePath"].ToString().Trim();
            string ReportName, strFileName = "", strFolderName = "";

            string strFldName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

            strFolderName = @strFldName;// @txtFileNo.Text.Trim() + "_" + strFldName;

            string path = @CoderVerification_FilePath + strFolderName;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string SelectionFormula1 = hidRptFormula.Value;
            // SelectionFormula1 += "  AND  {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}='" + lblLabTransNo.Text + "'";


            string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "Exp_" + strName + ".xls";

            ReportExportToExcel(drpReport.SelectedValue + ".rpt", SelectionFormula1, strFileName, strFolderName);

        }

        protected void drpParentCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            //AuExtCompany.ServiceMethod = "GetCompany";
            //AuExtCompany.UseContextKey = true;
            //AuExtCompany.ContextKey = drpParentCompany.SelectedValue;

            AuExtCompanyName.ServiceMethod = "GetCompanyName";
            AuExtCompanyName.UseContextKey = true;
            AuExtCompanyName.ContextKey = drpParentCompany.SelectedValue;
        }

        protected void btnShowDrpReport_Click(object sender, EventArgs e)
        {
            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowReportFromDrp('true')", true);

            if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                {
                    CommonBAL objCom = new CommonBAL();
                    DataSet DS = new DataSet();
                    string Create = "Code ='" + drpReport.SelectedValue + "'";
                    DS = objCom.fnGetFieldValue("*", "HMS_VIEW_MediplusReportGet", Create, "");

                    string strScreenID = "";
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        strScreenID = Convert.ToString(DS.Tables[0].Rows[0]["ScreenID"]);

                    }

                    string strPermission = "0";

                    string Criteria = " 1=1 AND HRT_SCREEN_ID='" + strScreenID + "' ";
                    Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
                    dboperations dbo = new dboperations();
                    DataSet DSRollTrans = new DataSet();
                    DSRollTrans = dbo.RollTransGet(Criteria);


                    if (DSRollTrans.Tables[0].Rows.Count > 0)
                    {
                        strPermission = Convert.ToString(DSRollTrans.Tables[0].Rows[0]["HRT_PERMISSION"]);

                    }

                    if (strPermission == "1" || strPermission == "7" || strPermission == "9")
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PTRegistration", "ShowReportFromDrp('true')", true);
                    }
                    else
                    {
                        lblStatus.Text = "Permission Denied";
                        goto FunEnd;
                    }
                }


            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PTRegistration", "ShowReportFromDrp('true')", true);


        FunEnd: ;

        }



    }
}