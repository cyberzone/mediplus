﻿<%@ Page Title=""  MasterPageFile="~/Site2.Master"   Language="C#"  AutoEventWireup="true" CodeBehind="Remittance.aspx.cs" Inherits="Mediplus.HMS.Billing.Remittance" %>
 <%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

     
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

   <script language="javascript" type="text/javascript">
       function OnlyNumeric(evt) {
           var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
           if (chCode >= 48 && chCode <= 57 ||
                chCode == 46) {
               return true;
           }
           else

               return false;
       }

       function CompIdSelected() {
           if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                  var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                  var Data1 = Data.split('~');

                  if (Data1.length > 1) {
                      document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');


                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];


                }
            }

            return true;
        }


        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            return true;
        }

        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }

        function ShowFileExists() {

            var isUpdateFile = window.confirm('This file is already uploaded, Do you want to continue ? ');
            document.getElementById('<%=hidUpdateFile.ClientID%>').value = isUpdateFile;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:label id="lblStatus" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
            </td>
        </tr>

    </table>

    <div style="padding-top: 5px;">
          <input type="hidden" id="hidPermission" runat="server" value="9" />
                 <asp:HiddenField ID="hidUpdateFile" runat="server" value="false" />
            
        <table width="80%" cellpadding="3" cellspacing="3">
            <tr>
                <td style="width: 70px; height: 30px;">
                    <asp:label id="Label2" runat="server" cssclass="lblCaption1"
                        text="Type"></asp:label>
                </td>
                <td colspan="2">
                    <asp:dropdownlist id="drpType" runat="server"  CssClass="TextBoxStyle" width="100px">
                        <asp:ListItem Text="Patient" Value="Patient"></asp:ListItem>
                        <asp:ListItem Text="Company" Value="Company" Selected="True"></asp:ListItem>
                    </asp:dropdownlist>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:label id="Label3" runat="server" cssclass="lblCaption1"
                         text="Receipt No"></asp:label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox id="txtReceiptNo" runat="server"  CssClass="TextBoxStyle" width="135px"  readonly="true"></asp:textbox>
                </td>

                <td style="width: 70px; height: 30px;">
                    <asp:label id="Label4" runat="server" cssclass="lblCaption1"
                        text="Date"></asp:label><span style="color:red;">* </span>
                </td>
                <td>
                    <asp:textbox id="txtDate" runat="server" width="75px"  CssClass="TextBoxStyle" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                    <asp:calendarextender id="CalendarExtender1" runat="server"
                        enabled="True" targetcontrolid="txtDate" format="dd/MM/yyyy">
                    </asp:calendarextender>
                    <asp:maskededitextender id="MaskedEditExtender2" runat="server" enabled="true" targetcontrolid="txtDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                </td>
                <td>
                    <asp:label id="Label5" runat="server" cssclass="lblCaption1"
                        text="Status"></asp:label>
                </td>
                <td>
                    <asp:dropdownlist id="drpStatus" runat="server"  CssClass="TextBoxStyle" width="100px">
                        <asp:ListItem Text="Open" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Hold" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Void" Value="3"></asp:ListItem>
                    </asp:dropdownlist>

                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;">
                    <asp:label id="Label8" runat="server" cssclass="lblCaption1"
                        text="Company"></asp:label><span style="color:red;">* </span>

                </td>
                <td colspan="2">
                    <asp:updatepanel id="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCompany" runat="server"  CssClass="TextBoxStyle" BackColor="#e3f7ef" Width="70px" MaxLength="10" onKeyUp="DoUpper(this);" onblur="return CompIdSelected()" OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server"  CssClass="TextBoxStyle" BackColor="#e3f7ef" Width="270px" MaxLength="50" onKeyUp="DoUpper(this);" onblur="return CompNameSelected()" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"></asp:AutoCompleteExtender>
                        </ContentTemplate>

                    </asp:updatepanel>
                </td>

                <td>
                    <asp:label id="Label7" runat="server" cssclass="lblCaption1"
                        text="Type"></asp:label>
                </td>
                <td>
                    <asp:textbox id="txtType" runat="server"  CssClass="TextBoxStyle" width="110px" text="Insurance"  readonly="true"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;">
                    <asp:label id="lbldesc" runat="server" cssclass="lblCaption1"
                        text="Description"></asp:label>

                </td>
                <td colspan="2">
                    <asp:textbox id="txtDescription" runat="server"  CssClass="TextBoxStyle" width="345px" ></asp:textbox>
                </td>
                <td>
                    <asp:label id="lblRef" runat="server" cssclass="lblCaption1"
                        text="Ref.No"></asp:label>

                </td>
                <td colspan="3">
                    <asp:textbox id="txtRefNo" runat="server"  CssClass="TextBoxStyle" width="92%" ></asp:textbox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;">
                    <asp:label id="lblCurPay" runat="server" cssclass="lblCaption1"
                        text="Cur.Pay."></asp:label>

                </td>
                <td colspan="2">
                    <asp:textbox id="txtCurPay" runat="server"  CssClass="TextBoxStyle" width="100px"  readonly="true" style="text-align: right;"></asp:textbox>
                    &nbsp;&nbsp;&nbsp;
                    <asp:label id="lblCurBal" runat="server" cssclass="lblCaption1"
                        text="Cur.Balance"></asp:label>
                    &nbsp;&nbsp;&nbsp;
                    <asp:textbox id="txtCurBalance" runat="server"  CssClass="TextBoxStyle" width="135px"  readonly="true" style="text-align: right;"></asp:textbox>
                </td>

                <td>
                    <asp:label id="lblPayType" runat="server" cssclass="lblCaption1"
                        text="Pay Type"></asp:label>

                </td>
                <td>
                    <asp:dropdownlist id="drpPayType" runat="server"  CssClass="TextBoxStyle" width="110px">
                        <asp:ListItem Text="Cash" Value="Cash" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Credit Card" Value="Credit Card"></asp:ListItem>
                        <asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>
                        <asp:ListItem Text="PDC" Value="PDC"></asp:ListItem>
                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                        <asp:ListItem Text="Debit Card" Value="Debit Card"></asp:ListItem>
                    </asp:dropdownlist>
                </td>
                <td>
                    <asp:label id="Label9" runat="server" cssclass="lblCaption1"
                        text="Prev. Balance"></asp:label>
                </td>
                <td>
                    <asp:textbox id="txtPrevBal" runat="server"  CssClass="TextBoxStyle" width="100px"  readonly="true" style="text-align: right;"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;">
                    <asp:label id="Label10" runat="server" cssclass="lblCaption1"
                        text="Net Amount"></asp:label>

                </td>
                <td colspan="2">
                    <asp:textbox id="txtNetAmt" runat="server"  CssClass="TextBoxStyle" width="100px"  readonly="true" style="text-align: right;"></asp:textbox>
                    &nbsp;&nbsp;&nbsp;
                    <asp:label id="Label11" runat="server" cssclass="lblCaption1"
                        text="Reject"></asp:label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:textbox id="txtRejectAmt" runat="server"  CssClass="TextBoxStyle" width="135px"  readonly="true" style="text-align: right;"></asp:textbox>
                </td>

                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
        </table>
      
      <fieldset style="width:80%;"> <legend class="lblCaption1">Search</legend>
        <table width=100%">
            <tr>
                <td style="width: 100px; height: 30px;" class="lblCaption1">Inv.No</td>
                <td>
                    <asp:textbox id="txtInvNo" runat="server"  CssClass="TextBoxStyle" width="100px" ></asp:textbox>
                </td>
                <td style="width: 70px;" class="lblCaption1">Date</td>
                <td>
                    <asp:textbox id="txtInvDate" runat="server" width="75px"  CssClass="TextBoxStyle" maxlength="10" onkeypress="return OnlyNumeric(event);" ></asp:textbox>
                    <asp:calendarextender id="CalendarExtender2" runat="server"
                        enabled="True" targetcontrolid="txtInvDate" format="dd/MM/yyyy">
                    </asp:calendarextender>
                    <asp:maskededitextender id="MaskedEditExtender4" runat="server" enabled="true" targetcontrolid="txtInvDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                </td>
                <td style="width: 70px;" class="lblCaption1">Amount</td>
                <td>
                    <asp:textbox id="txtAmount" runat="server"  CssClass="TextBoxStyle" width="100px" ></asp:textbox>
                </td>

            </tr>
            <tr>
                <td style="width: 100px; height: 30px;" class="lblCaption1">File No</td>
                <td>
                    <asp:textbox id="txtFileNo" runat="server"  CssClass="TextBoxStyle" width="100px" ></asp:textbox>
                </td>
                <td style="width: 70px;" class="lblCaption1">Name</td>
                <td>
                    <asp:textbox id="txtName" runat="server"  CssClass="TextBoxStyle" width="200px" ></asp:textbox>
                </td>
                <td style="width: 70px;" class="lblCaption1">Policy No</td>
                <td>
                    <asp:textbox id="txtPolicyNo" runat="server"  CssClass="TextBoxStyle" width="100px" ></asp:textbox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 30px;" class="lblCaption1">Remarks</td>
                <td >
                    <asp:textbox id="txtRemarks" runat="server"  CssClass="TextBoxStyle" width="300px" ></asp:textbox>
                </td>

           
                <td style="width: 100px; height: 30px;" class="label"></td>
                <td>
                    <asp:button id="btnFind" runat="server" text="Find" width="100px" cssclass="button red small" onclick="btnFind_Click" />

                </td>
                <td >
                    <asp:button id="btnClear" runat="server" text="Clear" width="100px" cssclass="button gray small" onclick="btnClear_Click" />
                </td>

            </tr>
        </table>

          </fieldset>
    </div>
    <br />
    <table>
        <tr>
            <td colspan="3">
                 <asp:RadioButtonList  ID="radRemitFileType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="250px"   >
                 <asp:ListItem Text="HAAD XML File" Value="HAAD_XML" Selected="True"  > </asp:ListItem>
                 <asp:ListItem Text="Local Excel File" Value="LOCAL_EXCEL"> </asp:ListItem>
                 
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px;">
                <asp:label id="Label1" runat="server" cssclass="lblCaption1"
                    text="Brows XML or Excel File"></asp:label>
            </td>

            <td>
                <asp:updatepanel id="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="300px">
                            <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnShow" />
                    </Triggers>
                </asp:updatepanel>
            </td>
            <td >
                <asp:updatepanel id="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnShow" runat="server" CssClass="button red small" Width="100px" OnClientClick="return Val();"
                            OnClick="btnShow_Click" Text="Load Details" />
                    </ContentTemplate>
                </asp:updatepanel>
            </td>
        </tr>
        


    </table>
    <div style="padding-top: 10px; width: 80%; height: 300px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
        <asp:updatepanel id="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200">
                    <HeaderStyle CssClass="GridHeader_Gray" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <PagerStyle  CssClass="TextBoxStyle" Font-Bold="true" />
                    <Columns>

                        <asp:TemplateField HeaderText="Inv Code">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCompName" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblInvoiceId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_DATE") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Net Amt.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAmount" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblAmount" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("Net1")%>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paid Amt.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPaid" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblPaid" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("HIM_CLAIM_AMOUNT_PAID") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cur.Pay">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPaymentAmt" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblPaymentAmt" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("PaymentAmount") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTShare" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblPTShare" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("PatientShare") %>' Width="100px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Reject">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDiscount" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblDiscount" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("Discount") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Balance">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBalance" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblBalance" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("Balance") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTId" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblPTId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PT_ID") %>' Width="100px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PT_NAME") %>' Width="200px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dr Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_DR_NAME") %>' Width="200px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Policy No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPolicyNo" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblPolicyNo" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_POLICY_NO") %>' Width="100px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Claim No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCoucherNo" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblCoucherNo" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_VOUCHER_NO") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkType" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_TYPE") %>' Width="70px"></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>



            </ContentTemplate>
        </asp:updatepanel>

    </div>
    <table width="70%">
        <tr>
            <td class="style2">
                <asp:label id="Label6" runat="server" cssclass="label" font-bold="true"
                    text="Selected Records :"></asp:label>
                &nbsp;
                                    <asp:label id="lblTotal" runat="server" cssclass="label" font-bold="true"></asp:label>
            </td>


        </tr>

    </table>


    <div style="padding-top: 10px; width: 80%; height: 200px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
        <asp:updatepanel id="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvService" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" PageSize="200">
                    <HeaderStyle CssClass="GridHeader_Gray" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <PagerStyle CssClass="label" Font-Bold="true" />
                    <Columns>

                        <asp:TemplateField HeaderText="Inv Code">
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_DATE") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ServiceCode">
                            <ItemTemplate>
                                <asp:Label ID="lblServiceCode" CssClass="GridRow" runat="server" Text='<%# Bind("ServiceCode") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Net Amt.">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("Net1") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paid Amt.">
                            <ItemTemplate>
                                <asp:Label ID="lblPaidAmt" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("HIT_CLAIM_AMOUNT_PAID") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cur.Pay">
                            <ItemTemplate>
                                <asp:Label ID="lblStart" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("PaymentAmount") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Share">
                            <ItemTemplate>
                                <asp:Label ID="lblPTShare" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("PatientShare") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reject">
                            <ItemTemplate>
                                <asp:Label ID="lblDiscount" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("Discount") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Balance">
                            <ItemTemplate>
                                <asp:Label ID="lblDiscount" CssClass="GridRow" style="text-align:right;padding-right:5px;" runat="server" Text='<%# Bind("Balance") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No">
                            <ItemTemplate>
                                <asp:Label ID="lblPTId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PT_ID") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name">
                            <ItemTemplate>
                                <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PT_NAME") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dr Name">
                            <ItemTemplate>
                                <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_DR_NAME") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Policy No">
                            <ItemTemplate>
                                <asp:Label ID="llPolicyNo" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_POLICY_NO") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Claim No">
                            <ItemTemplate>
                                <asp:Label ID="lblVoucherNo" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_VOUCHER_NO") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_TYPE") %>' Width="70px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Denial Code">
                            <ItemTemplate>
                                <asp:Label ID="lblDenial" CssClass="GridRow" runat="server" Text='<%# Bind("DenialCode") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Denial Desc">
                            <ItemTemplate>
                                <asp:Label ID="lblDenialDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HDM_DESCRIPTION") %>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>

                </asp:GridView>

            </ContentTemplate>
        </asp:updatepanel>

    </div>

    <div>
        <table width="350px" align="center">
            <tr>
                <td>
                    <asp:button id="btnSaveToDB" runat="server" cssclass="button red small" width="100px" onclientclick="return Val();"
                        onclick="btnSaveToDB_Click" text="Save to DB" />
                    <asp:button id="btnCancel" runat="server" cssclass="button gray small" width="100px"
                        onclick="btnCancel_Click" text="Cancel" />
                </td>
            </tr>
        </table>
     
    </div>

    <br />
        <br />
     
   </asp:Content>