﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Text;
using System.Data.SqlClient;
using System.Xml;

namespace Mediplus.HMS.Billing
{
    public partial class GenerateEClaim : System.Web.UI.Page
    {
        # region Variable Declaration
        static string stParentName = "0";
        static string strSessionBranchId;

        DataSet DS = new DataSet();
        DataSet DS1 = new DataSet();
        dboperations dbo = new dboperations();


        public string strTotalClaims = "";
        public string strTotalAmount = "";

        #endregion

        #region Methods

        void BindGrid()
        {
            string Criteria = " 1=1 ";
            string Criteria1 = " 1=1 ";// AND( HMS_INVOICE_MASTER.HIM_INVOICE_TYPE = 'Credit') AND ( HMS_INVOICE_MASTER.HIM_CLAIM_AMOUNT > 0) ";
            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria1 += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),Start,101),101) >= '" + strForStartDate + "'";
            }

            if (txtFromTime.Text != "")
            {
                Criteria1 += " AND HIM_DATE >= '" + strForStartDate + " " + txtFromTime.Text + "'";
                Criteria += " AND Start >= '" + strForStartDate + " " + txtFromTime.Text + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria1 += " AND    CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101)  <= '" + strForToDate + "'";
                Criteria += " AND    CONVERT(datetime,convert(varchar(10),Start,101),101)  <= '" + strForToDate + "'";
            }

            if (txtToTime.Text != "")
            {
                Criteria1 += " AND  HIM_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";
                Criteria += " AND  Start <= '" + strForToDate + " " + txtToTime.Text + "'";
            }




            if (txtCompany.Text != "")
            {
                Criteria1 += " AND HIM_BILLTOCODE = '" + txtCompany.Text + "'";
                //Criteria += " AND PayerID =(SELECT TOP 1 HCM_PAYERID FROM hMS_COMPANY_MASTER WHERE   HCM_PAYERID !='' AND HCM_BILL_CODE = '" + txtCompany.Text + "')";
                Criteria += " AND CompID ='" + txtCompany.Text + "'";

            }

            //if (drpType.SelectedIndex != 0)
            //{
            //    Criteria1 += " and  HIC_TYPE=" + drpType.SelectedValue;
            //    Criteria += " and  invType=" + drpType.SelectedValue;
            //}
            /*
            if (drpType.SelectedIndex != 0)
            {

                if (drpType.SelectedValue == "IP")
                {
                    Criteria += " and (HIC_TYPE=3 or HIC_TYPE=4 ) ";
                }
                else if (drpType.SelectedValue == "OP")
                {
                    Criteria += "  and (HIC_TYPE=1 or HIC_TYPE=2 ) ";

                }
                else if (drpType.SelectedValue == "DAYCARE")
                {
                    Criteria += "and (HIC_TYPE=5 or HIC_TYPE=6 ) ";
                }
                else if (drpType.SelectedValue == "IP/DAYCARE")
                {
                    Criteria += " and (HIC_TYPE=3 or HIC_TYPE=4 or HIC_TYPE=5 or HIC_TYPE=6 ) ";
                }


            }
            */

            //if (drpInv.SelectedIndex != 0)
            //{
            //    Criteria1 += " AND HIM_INVOICE_TYPE = '" + drpInv.SelectedValue + "'";
            //    Criteria += " AND invType = '" + drpInv.SelectedValue + "'";
            //}

            if (txtInvoiceNo.Text.Trim() != "")
            {
                Criteria1 += " AND HIM_INVOICE_ID = '" + txtInvoiceNo.Text.Trim() + "'";
                Criteria += " AND ID = '" + txtInvoiceNo.Text.Trim() + "'";
            }

            ViewState["Criteria"] = Criteria;//Criteria1
            dbo = new dboperations();
            DS = new DataSet();



            if (radGeneratedType.SelectedIndex == 0)
            {
                if (hidIsEclaimSub.Value.ToLower() == "false")
                {
                    DS = dbo.GetEClaimView(Criteria);//Criteria1
                }
                else
                {
                    DS = dbo.GetEClaimSubView(Criteria);//Criteria1
                }
                Session["EClaim"] = DS;



                lblTotalClaims.Text = "0";
                lblTotalAmount.Text = "0";

                gvGridView.Visible = false;

                decimal decTotalClaimAmount = 0;



                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvGridView.Visible = true;
                    gvGridView.DataSource = DS;
                    gvGridView.DataBind();

                    //lblTotalClaims.Text = Convert.ToString(DS.Tables[0].Rows.Count);
                    //strTotalClaims = lblTotalClaims.Text;


                    //for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    //{
                    //    if (DS.Tables[0].Rows[i].IsNull("Net1") == false && Convert.ToString(DS.Tables[0].Rows[i]["Net1"]) != "")
                    //    {
                    //        decTotalClaimAmount += Convert.ToDecimal(DS.Tables[0].Rows[i]["Net1"]);
                    //    }
                    //}

                    //lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");

                    //strTotalAmount = lblTotalAmount.Text;


                    CommonBAL objCom = new CommonBAL();
                    DataSet DSInv = new DataSet();
                    if (hidIsEclaimSub.Value.ToLower() == "false")
                    {
                        //DSInv = objCom.fnGetFieldValue(" count(*)TotalCount,sum(ISNULL(HIM_CLAIM_AMOUNT,0))TotalClaimAmount", "HMS_INVOICE_MASTER", Criteria1, "");
                        DSInv = objCom.fnGetFieldValue("  ISNULL( SUM(ISNULL(Net,0)),0) AS TotalClaimAmount", "EClaim", Criteria, "");
                    }
                    else
                    {
                        DSInv = objCom.fnGetFieldValue("  ISNULL(SUM(ISNULL(Net,0)),0) AS TotalClaimAmount", "EClaimsub", Criteria, "");
                    }


                    if (DSInv.Tables[0].Rows.Count > 0)
                    {

                        decTotalClaimAmount = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["TotalClaimAmount"]);
                        lblTotalAmount.Text = decTotalClaimAmount.ToString("N2");
                        strTotalAmount = lblTotalAmount.Text;

                    }


                    DataSet DSInv1 = new DataSet();
                    if (hidIsEclaimSub.Value.ToLower() == "false")
                    {
                        //DSInv = objCom.fnGetFieldValue(" count(*)TotalCount,sum(ISNULL(HIM_CLAIM_AMOUNT,0))TotalClaimAmount", "HMS_INVOICE_MASTER", Criteria1, "");
                        DSInv1 = objCom.fnGetFieldValue(" ID ", "EClaim", Criteria + " GROUP BY ID ", "");
                    }
                    else
                    {
                        DSInv1 = objCom.fnGetFieldValue(" ID ", "EClaimsub", Criteria + " GROUP BY ID ", "");
                    }



                    if (DSInv1.Tables[0].Rows.Count > 0)
                    {
                        lblTotalClaims.Text = Convert.ToString(DSInv1.Tables[0].Rows.Count);
                        strTotalClaims = lblTotalClaims.Text;



                    }


                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
                    gvGridView.DataBind();
                    gvGridView.Visible = false;
                    lblStatus.Text = "No Data !";
                    lblStatus.ForeColor = System.Drawing.Color.Red;

                }






            }
            else
            {
                GetDataFromXMLFile();
            }


        BindEnd: ;

        }

        void BindScreenCustomization()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='ECLAIM' AND SEGMENT='ECLAIM_TYPE'";

            DS = new DataSet();
            DS = dbo.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "AUH";
                }
            }


        }

        void TextFileWriting(string strContent)
        {
            try
            {
                // string strName = "EClaimErrorLog" + System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".txt";
                string strName = GlobalValues.Shafafiya_EClaim_Error_FilePath + Convert.ToString(ViewState["LogFileName"]) + ".txt"; ;

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        Boolean CheckSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_COMP_ID Like '" + txtCompany.Text + "' and  HCM_COMP_ID !=  HCM_BILL_CODE";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                return true;
            }

            return false;

            ds.Clear();
            ds.Dispose();

        }

        void Clear()
        {
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtFromTime.Text = "";
            txtToTime.Text = "";
            txtCompany.Text = "";
            txtCompanyName.Text = "";

            txtInvoiceNo.Text = "";

            // drpType.SelectedIndex = 0;
            // drpInv.SelectedIndex = 0;

            DS = new DataSet();
            Session["EClaim"] = DS;


            lblTotalClaims.Text = "0";
            lblTotalAmount.Text = "0";

            gvGridView.Visible = false;
            gvGridView.DataBind();

            ViewState["FileName"] = "";
            ViewState["Criteria"] = "";

            hidClaimGenerate.Value = "";
        }

        void GetDataFromXMLFile()
        {
            if (fileLogo.FileName == "")
            {
                lblStatus.Text = "Please select the file!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }

            Boolean IsHeader = false, IsClaim = false, IsEncounter = false, IsDiagnosis = false, IsActivity = false, IsObservation = false, IsContract = false;
            string strName = "";

            if (Path.GetExtension(fileLogo.FileName.ToLower()) == ".xml")
            {
                strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".XML";
                string strPath = Server.MapPath("../Uploads/EClaims/");
                fileLogo.SaveAs(strPath + strName);

                DS.ReadXml(Server.MapPath("../Uploads/EClaims/" + strName));
                ViewState["FileName"] = fileLogo.FileName;

            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;

            }

            foreach (DataTable table in DS.Tables)
            {
                if (table.TableName == "Header")
                {
                    IsHeader = true;
                }
                if (table.TableName == "Claim")
                {
                    IsClaim = true;
                }
                if (table.TableName == "Encounter")
                {
                    IsEncounter = true;
                }
                if (table.TableName == "Diagnosis")
                {
                    IsDiagnosis = true;
                }
                if (table.TableName == "Activity")
                {
                    IsActivity = true;
                }
                if (table.TableName == "Observation")
                {
                    IsObservation = true;
                }

                if (table.TableName == "Contract")
                {
                    IsContract = true;
                }


            }


            if (IsHeader == false || IsClaim == false || IsEncounter == false || IsDiagnosis == false || IsActivity == false)
            {
                lblStatus.Text = "Incorrect File Format!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto BindEnd;
            }



            string Criteria = " 1=2 ";
            DataSet DSView = new DataSet();
            DSView = dbo.GetEClaimView(Criteria);

            for (int i = 0; i < DS.Tables["Claim"].Rows.Count; i++)
            {
                DataTable DTActivity = new DataTable();
                DataRow[] DRActivity;
                DRActivity = DS.Tables["Activity"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                DataTable DTEncounter = new DataTable();
                DataRow[] DREncounter;
                DREncounter = DS.Tables["Encounter"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());

                DataTable DTDiagnosis = new DataTable();
                DataRow[] DRDiagnosis;
                DRDiagnosis = DS.Tables["Diagnosis"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());


                DataTable DTContract = new DataTable();
                DataRow[] DRContract = null;
                if (IsContract == true)
                {
                    DRContract = DS.Tables["Contract"].Select("Claim_Id=" + DS.Tables["Claim"].Rows[i]["Claim_Id"].ToString());
                }



                for (int j = 0; j < DRActivity.Length; j++)
                {
                    DataTable DTObservation = new DataTable();
                    DataRow[] DRObservation;



                    DataRow objrow;
                    objrow = DSView.Tables[0].NewRow();

                    objrow["CompId"] = DS.Tables["Header"].Rows[0]["PayerID"].ToString();//ReceiverID
                    objrow["ID"] = DS.Tables["Claim"].Rows[i]["ID"].ToString();
                    objrow["IDPayer"] = DS.Tables["Claim"].Rows[i]["IDPayer"].ToString();
                    objrow["MemberID"] = DS.Tables["Claim"].Rows[i]["MemberID"].ToString();
                    objrow["PayerID"] = DS.Tables["Claim"].Rows[i]["PayerID"].ToString();
                    objrow["ProviderID"] = DS.Tables["Claim"].Rows[i]["ProviderID"].ToString();
                    objrow["EmiratesIDNumber"] = DS.Tables["Claim"].Rows[i]["EmiratesIDNumber"].ToString();

                    decimal decGross, decPatientShare, decNet;
                    decGross = Convert.ToDecimal(DS.Tables["Claim"].Rows[i]["Gross"].ToString());
                    decPatientShare = Convert.ToDecimal(DS.Tables["Claim"].Rows[i]["PatientShare"].ToString());
                    decNet = Convert.ToDecimal(DS.Tables["Claim"].Rows[i]["Net"].ToString());

                    objrow["Gross"] = decGross.ToString("N1");
                    objrow["PatientShare"] = decPatientShare.ToString("N1");
                    objrow["Net"] = decNet.ToString("N1");




                    objrow["Type"] = DS.Tables["Encounter"].Rows[0]["Type"].ToString();
                    objrow["PatientID"] = DS.Tables["Encounter"].Rows[0]["PatientID"].ToString();

                    objrow["Start"] = Convert.ToDateTime(DS.Tables["Encounter"].Rows[0]["Start"].ToString()).ToString("dd/MM/yyyy HH:mm");
                    objrow["End"] = Convert.ToDateTime(DS.Tables["Encounter"].Rows[0]["End"].ToString()).ToString("dd/MM/yyyy HH:mm");
                    objrow["StartType"] = DS.Tables["Encounter"].Rows[0]["StartType"].ToString();
                    objrow["EndType"] = DS.Tables["Encounter"].Rows[0]["EndType"].ToString();


                    for (int k = 0; k < DRDiagnosis.Length; k++)
                    {
                        if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Principal")
                        {
                            //objrow["Type1"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                            //objrow["Code"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();

                            objrow["Type1"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["Code"] = Convert.ToString(DRDiagnosis[k]["Code"]);

                        }

                        if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Admitting")
                        {
                            //objrow["ICDA"] = DS.Tables["Diagnosis"].Rows[k]["Type"].ToString();
                            //objrow["ICDCodeA"] = DS.Tables["Diagnosis"].Rows[k]["Code"].ToString();

                            objrow["ICDA"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDCodeA"] = Convert.ToString(DRDiagnosis[k]["Code"]);

                        }
                        if (Convert.ToString(DRDiagnosis[k]["Type"]) == "Secondary")
                        {
                            objrow["Type3"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS2"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS3"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS4"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS5"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS6"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS7"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS8"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS9"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS10"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS11"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS12"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS13"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS14"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS15"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS16"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS17"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS18"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS19"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS20"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS21"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS22"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS23"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS24"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS25"] = Convert.ToString(DRDiagnosis[k]["Type"]);
                            objrow["ICDS26"] = Convert.ToString(DRDiagnosis[k]["Type"]);

                            switch (k)
                            {
                                case 1:
                                    objrow["Code3"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 2:
                                    objrow["ICDCodeS2"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 3:
                                    objrow["ICDCodeS3"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 4:
                                    objrow["ICDCodeS4"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 5:
                                    objrow["ICDCodeS5"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 6:
                                    objrow["ICDCodeS6"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 7:
                                    objrow["ICDCodeS7"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 8:
                                    objrow["ICDCodeS8"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 9:
                                    objrow["ICDCodeS9"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 10:
                                    objrow["ICDCodeS10"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 11:
                                    objrow["ICDCodeS11"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 12:
                                    objrow["ICDCodeS12"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 13:
                                    objrow["ICDCodeS13"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 14:
                                    objrow["ICDCodeS14"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 15:
                                    objrow["ICDCodeS15"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 16:
                                    objrow["ICDCodeS16"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 17:
                                    objrow["ICDCodeS17"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 18:
                                    objrow["ICDCodeS18"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 19:
                                    objrow["ICDCodeS19"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 20:
                                    objrow["ICDCodeS20"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 21:
                                    objrow["ICDCodeS21"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 22:
                                    objrow["ICDCodeS22"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 23:
                                    objrow["ICDCodeS23"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 24:
                                    objrow["ICDCodeS24"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 25:
                                    objrow["ICDCodeS25"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;
                                case 26:
                                    objrow["ICDCodeS26"] = Convert.ToString(DRDiagnosis[k]["Code"]);
                                    break;

                                default:
                                    break;

                            }

                        }




                    }



                    objrow["ID"] = DRActivity[j]["ID"].ToString();
                    objrow["Start1"] = Convert.ToDateTime(Convert.ToString(DS.Tables["Encounter"].Rows[0]["Start"])).ToString("dd/MM/yyyy HH:mm");
                    objrow["Type2"] = Convert.ToString(DRActivity[j]["Type"]);
                    objrow["Code2"] = Convert.ToString(DRActivity[j]["Code"]);
                    objrow["Quantity"] = Convert.ToString(DRActivity[j]["Quantity"]);
                    objrow["Net1"] = Convert.ToString(DRActivity[j]["Net"]);
                    objrow["OrderingClinician"] = Convert.ToString(DRActivity[j]["OrderingClinician"]);
                    objrow["Clinician"] = Convert.ToString(DRActivity[j]["Clinician"]);

                    objrow["InvoiceID"] = Convert.ToString(DS.Tables["Claim"].Rows[i]["ID"]);
                    // objrow["InvDate"] = DRActivity[j][0].ToString();
                    objrow["InvType"] = "Credit";
                    objrow["HIT_SERV_CODE"] = Convert.ToString(DRActivity[j]["Code"]);

                    if (IsObservation == true)
                    {
                        DRObservation = DS.Tables["Observation"].Select("Activity_Id=" + (DRActivity[j]["Activity_Id"]));
                        if (DRObservation.Length > 0)
                        {

                            if (DRActivity[j]["Clinician"].ToString() != "")
                            {
                                objrow["PriorAuthorizationId"] = DRActivity[j]["Clinician"].ToString();
                            }

                            objrow["ObservType"] = DRObservation[0]["Type"].ToString();
                            objrow["ObservCode"] = DRObservation[0]["Code"].ToString();
                            objrow["ObservValue"] = DRObservation[0]["Value"].ToString();
                            objrow["ObservValueType"] = DRObservation[0]["ValueType"].ToString();




                        }

                    }

                    if (IsContract == true)
                    {
                        if (DRContract.Length > 0)
                        {
                            objrow["PackageName"] = DRContract[0][0].ToString();
                        }
                    }



                    DSView.Tables[0].Rows.Add(objrow);
                }

            }


            File.Delete(Server.MapPath("../Uploads/EClaims/" + strName));
            Session["EClaim"] = DSView;

            lblTotalClaims.Text = "0";
            gvGridView.Visible = false;

            DataView DV = new DataView();
            DV.Table = DSView.Tables[0];
            DV.Sort = Convert.ToString("InvoiceID Asc");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

                lblTotalClaims.Text = Convert.ToString(DS.Tables["Activity"].Rows.Count);
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);
                lblStatus.Text = "No Data !";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }


        BindEnd: ;

        }

        string XMLFileWrite(string DispositionFlag)
        {

            // string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            string strPath = GlobalValues.Shafafiya_EClaim_FilePath;
            string strFileName = "", strFileId;
            string Criteria;

            string strDate = "", strTime = "";

            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");

            Criteria = ViewState["Criteria"].ToString();
            dbo = new dboperations();
            DS1 = new DataSet();
            if (hidIsEclaimSub.Value.ToLower() == "false")
            {
                DS1 = dbo.GetEClaimViewGroupBy(Criteria);
            }
            else
            {
                DS1 = dbo.GetEClaimSubViewGroupBy(Criteria);
            }



            Boolean IsError = false;
            ViewState["LogFileName"] = "EClaimErrorLog" + strTimeStamp + ".txt";


            DS = new DataSet();
            DS = (DataSet)Session["EClaim"];


            if (DS.Tables[0].Rows[0].IsNull("ProviderId") == true)
            {
                IsError = true;
                TextFileWriting("Invalid or blank SenderID");
                goto ClaimEnd;
            }


            if (DS.Tables[0].Rows[0].IsNull("CompId") == true)
            {
                IsError = true;
                TextFileWriting("Invalid or blank ReceiverID");
                goto ClaimEnd;
            }
            StringBuilder strData = new StringBuilder();

            string strNewLineChar = "\n";

            // date.SetAttribute("modified", DateTime.Now.ToString());
            XmlDocument XD = new XmlDocument();
            XD.CreateXmlDeclaration("1.0", "utf-8", "yes");
            XD.CreateProcessingInstruction("xml", "version='1.0' encoding='utf-8'");



            string strPayerID = "", strReceiverID = "", InvoiceID = "";
            Int32 ActCount = 0;


            strPayerID = Convert.ToString(DS.Tables[0].Rows[0]["PayerID"]);
            strReceiverID = Convert.ToString(DS.Tables[0].Rows[0]["ReceiverID"]);
            InvoiceID = Convert.ToString(DS.Tables[0].Rows[0]["InvoiceID"]);
            strData.Append("<?xml version='1.0' encoding='utf-8' ?>" + strNewLineChar);


            if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
            {
                strData.Append("<Claim.Submission xmlns:tns='http://www.eclaimlink.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='http://www.eclaimlink.ae/DataDictionary/CommonTypes/ClaimSubmission.xsd'>" + strNewLineChar);
            }
            else
            {
                strData.Append(@"<Claim.Submission xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='https://www.haad.ae/DataDictionary/CommonTypes/PriorRequest.xsd'> " + strNewLineChar);

                //strData.Append("<Claim.Submission xmlns:tns='http://www.haad.ae/DataDictionary/CommonTypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='https://www.haad.ae/DataDictionary/CommonTypes/PriorRequest.xsd'>"+ strNewLineChar);

            }

            strData.Append("<Header>" + strNewLineChar);

            strData.Append("<SenderID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</SenderID>" + strNewLineChar); // GlobalValues.FacilityID
            strData.Append("<ReceiverID>" + strReceiverID + "</ReceiverID>" + strNewLineChar);//ReceiverID
            strData.Append("<TransactionDate>" + strDate + "</TransactionDate>" + strNewLineChar);
            strData.Append("<RecordCount>" + Convert.ToString(DS1.Tables[0].Rows.Count) + "</RecordCount>" + strNewLineChar);
            strData.Append("<DispositionFlag>" + DispositionFlag + "</DispositionFlag>" + strNewLineChar);
            strData.Append("</Header>" + strNewLineChar);

            strFileId = "CLSUB_" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "-" + strPayerID + "-" + strReceiverID + "-" + strTimeStamp;


            for (Int32 i = 0; i < DS1.Tables[0].Rows.Count; i++)
            {

                strData.Append("<Claim>" + strNewLineChar);
                if (DS1.Tables[0].Rows[0].IsNull("MemberID") == true)
                {
                    IsError = true;
                    TextFileWriting("Invalid or blank MemberID");
                    // goto ClaimEnd;
                }





                strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + "</ID>" + strNewLineChar);
                strData.Append("<IDPayer>" + Convert.ToString(DS1.Tables[0].Rows[i]["IDPayer"]) + "</IDPayer>" + strNewLineChar);
                strData.Append("<MemberID>" + Convert.ToString(DS1.Tables[0].Rows[i]["MemberID"]) + "</MemberID>" + strNewLineChar);
                strData.Append("<PayerID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PayerID"]) + "</PayerID>" + strNewLineChar);
                strData.Append("<ProviderID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</ProviderID>" + strNewLineChar);//GlobalValues.FacilityID
                strData.Append("<EmiratesIDNumber>" + Convert.ToString(DS1.Tables[0].Rows[i]["EmiratesIDNumber"]) + "</EmiratesIDNumber>" + strNewLineChar);
                strData.Append("<Gross>" + Convert.ToString(DS1.Tables[0].Rows[i]["Gross"]) + "</Gross>" + strNewLineChar);
                strData.Append("<PatientShare>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientShare"]) + "</PatientShare>" + strNewLineChar);
                strData.Append("<Net>" + Convert.ToString(DS1.Tables[0].Rows[i]["Net"]) + "</Net>" + strNewLineChar);

                strData.Append("<Encounter>" + strNewLineChar);
                strData.Append("<FacilityID>" + Convert.ToString(Session["Branch_ProviderID"]).Trim() + "</FacilityID>" + strNewLineChar);//GlobalValues.FacilityID 
                strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type"]) + "</Type>" + strNewLineChar);
                strData.Append("<PatientID>" + Convert.ToString(DS1.Tables[0].Rows[i]["PatientID"]) + "</PatientID>" + strNewLineChar);
                strData.Append("<Start>" + Convert.ToDateTime(Convert.ToString(DS1.Tables[0].Rows[i]["Start"])).ToString("dd/MM/yyyy HH:mm") + "</Start>" + strNewLineChar);
                strData.Append("<End>" + Convert.ToDateTime(DS1.Tables[0].Rows[i]["End"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</End>" + strNewLineChar);
                strData.Append("<StartType>" + Convert.ToString(DS1.Tables[0].Rows[i]["StartType"]) + "</StartType>" + strNewLineChar);
                strData.Append("<EndType>" + Convert.ToString(DS1.Tables[0].Rows[i]["EndType"]) + "</EndType>" + strNewLineChar);
                strData.Append("</Encounter>" + strNewLineChar);



                Boolean isDiagnosis = false;

                if (DS1.Tables[0].Rows[i].IsNull("Type1") == false && DS1.Tables[0].Rows[i].IsNull("Code") == false && DS1.Tables[0].Rows[i]["Code"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type1"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code"]) + "</Code>" + strNewLineChar);

                    if (Convert.ToString(Session["ECLAIM_TYPE"]) != "DXB")
                    {
                        if (DS1.Tables[0].Rows[i].IsNull("HIC_YEAR_OF_ONSET_P") == false && DS1.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"].ToString() != "")
                        {
                            strData.Append("<DxInfo>" + strNewLineChar);
                            strData.Append("<Type>Year of onset</Type>" + strNewLineChar);
                            strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"]) + "</Code>" + strNewLineChar);
                            strData.Append("</DxInfo>" + strNewLineChar);
                        }
                    }

                    strData.Append("</Diagnosis>" + strNewLineChar);

                }



                if (DS1.Tables[0].Rows[i].IsNull("Type3") == false && DS1.Tables[0].Rows[i].IsNull("Code3") == false && DS1.Tables[0].Rows[i]["Code3"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["Type3"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["Code3"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS2") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS2") == false && DS1.Tables[0].Rows[i]["ICDCodeS2"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS2"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS2"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS3") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS3") == false && DS1.Tables[0].Rows[i]["ICDCodeS3"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS3"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS3"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS4") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS4") == false && DS1.Tables[0].Rows[i]["ICDCodeS4"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS4"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS4"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS5") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS5") == false && DS1.Tables[0].Rows[i]["ICDCodeS5"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS5"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS5"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS6") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS6") == false && DS1.Tables[0].Rows[i]["ICDCodeS6"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS6"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS6"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS7") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS7") == false && DS1.Tables[0].Rows[i]["ICDCodeS7"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS7"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS7"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS8") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS8") == false && DS1.Tables[0].Rows[i]["ICDCodeS8"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS8"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS8"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS9") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS9") == false && DS1.Tables[0].Rows[i]["ICDCodeS9"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS9"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS9"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS10") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS10") == false && DS1.Tables[0].Rows[i]["ICDCodeS10"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS10"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS10"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS11") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS11") == false && DS1.Tables[0].Rows[i]["ICDCodeS11"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS11"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS11"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }

                if (DS1.Tables[0].Rows[i].IsNull("ICDS12") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS12") == false && DS1.Tables[0].Rows[i]["ICDCodeS12"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS12"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS12"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS13") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS13") == false && DS1.Tables[0].Rows[i]["ICDCodeS13"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS13"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS13"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS14") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS14") == false && DS1.Tables[0].Rows[i]["ICDCodeS14"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS14"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS14"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS15") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS15") == false && DS1.Tables[0].Rows[i]["ICDCodeS15"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS15"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS15"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS16") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS16") == false && DS1.Tables[0].Rows[i]["ICDCodeS16"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS16"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS16"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }




                if (DS1.Tables[0].Rows[i].IsNull("ICDS17") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS17") == false && DS1.Tables[0].Rows[i]["ICDCodeS17"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS17"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS17"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS18") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS18") == false && DS1.Tables[0].Rows[i]["ICDCodeS18"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS18"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS18"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS19") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS19") == false && DS1.Tables[0].Rows[i]["ICDCodeS19"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS19"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS19"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS20") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS20") == false && DS1.Tables[0].Rows[i]["ICDCodeS20"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS20"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS20"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS21") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS21") == false && DS1.Tables[0].Rows[i]["ICDCodeS21"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS21"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS21"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS22") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS22") == false && DS1.Tables[0].Rows[i]["ICDCodeS22"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS22"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS22"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS23") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS23") == false && DS1.Tables[0].Rows[i]["ICDCodeS23"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS23"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS23"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDS24") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS24") == false && DS1.Tables[0].Rows[i]["ICDCodeS24"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS24"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS24"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS25") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS25") == false && DS1.Tables[0].Rows[i]["ICDCodeS25"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + DS1.Tables[0].Rows[i]["ICDS25"].ToString() + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + DS1.Tables[0].Rows[i]["ICDCodeS25"].ToString() + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }



                if (DS1.Tables[0].Rows[i].IsNull("ICDS26") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeS26") == false && DS1.Tables[0].Rows[i]["ICDCodeS26"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDS26"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeS26"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);
                }


                if (DS1.Tables[0].Rows[i].IsNull("ICDA") == false && DS1.Tables[0].Rows[i].IsNull("ICDCodeA") == false && DS1.Tables[0].Rows[i]["ICDCodeA"].ToString() != "")
                {
                    isDiagnosis = true;
                    strData.Append("<Diagnosis>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDA"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS1.Tables[0].Rows[i]["ICDCodeA"]) + "</Code>" + strNewLineChar);
                    strData.Append("</Diagnosis>" + strNewLineChar);

                }


                if (isDiagnosis == false)
                {
                    IsError = true;
                    TextFileWriting("Invalid or blank Diagnosis  (InvoiceNo=" + DS1.Tables[0].Rows[i]["ID"].ToString() + ")" + strNewLineChar);
                    // goto ClaimEnd;
                }




                //    Criteria = " HIM_INVOICE_ID=" + DS1.Tables[0].Rows[i]["ID"].ToString();
                Criteria = " ID='" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + "'";
                dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.GetEClaimView(Criteria);


                //DataTable DTActivity = new DataTable();
                //DataRow[] DRActivity;
                //DRActivity = DS1.Tables[0].Select("ID=" + DS.Tables[0].Rows[i]["ID"].ToString());



                for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                {
                    ActCount = ActCount + 1;

                    if (DS.Tables[0].Rows[j].IsNull("Clinician") == true)
                    {
                        IsError = true;
                        TextFileWriting("Invalid or blank Clinician  (InvoiceNo=" + DS.Tables[0].Rows[j]["ID"].ToString() + ")");
                        // goto ClaimEnd;
                    }

                    if (DS.Tables[0].Rows[j].IsNull("Code2") == true)
                    {
                        IsError = true;
                        TextFileWriting("Invalid or blank  Activity Code (InvoiceNo=" + DS.Tables[0].Rows[j]["ID"].ToString() + ")");
                        // goto ClaimEnd;
                    }

                    strData.Append("<Activity>" + strNewLineChar);
                    strData.Append("<ID>" + Convert.ToString(DS1.Tables[0].Rows[i]["ID"]) + ActCount + "</ID>" + strNewLineChar);
                    strData.Append("<Start>" + Convert.ToDateTime(DS.Tables[0].Rows[j]["Start1"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</Start>" + strNewLineChar);
                    strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["Type2"]) + "</Type>" + strNewLineChar);
                    strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["Code2"]) + "</Code>" + strNewLineChar);
                    strData.Append("<Quantity>" + Convert.ToString(DS.Tables[0].Rows[j]["Quantity"]) + "</Quantity>" + strNewLineChar);
                    strData.Append("<Net>" + Convert.ToString(DS.Tables[0].Rows[j]["Net1"]) + "</Net>" + strNewLineChar);
                    if (Convert.ToString(Session["ECLAIM_TYPE"]) != "DXB")
                    {
                        strData.Append("<OrderingClinician>" + Convert.ToString(DS.Tables[0].Rows[j]["OrderingClinician"]) + "</OrderingClinician>" + strNewLineChar);
                    }
                    strData.Append("<Clinician>" + Convert.ToString(DS.Tables[0].Rows[j]["Clinician"]) + "</Clinician>" + strNewLineChar);




                    if (DS.Tables[0].Rows[j].IsNull("PriorAuthorizationId") == false && DS.Tables[0].Rows[j]["PriorAuthorizationId"].ToString() != "")
                    {
                        strData.Append("<PriorAuthorizationId>" + Convert.ToString(DS.Tables[0].Rows[j]["PriorAuthorizationId"]) + "</PriorAuthorizationId>" + strNewLineChar);
                    }
                    //else
                    //{
                    //    strData.Append("<PriorAuthorizationId></PriorAuthorizationId>"+ strNewLineChar);
                    //}




                    if (DS.Tables[0].Rows[j].IsNull("ObservType") == false && DS.Tables[0].Rows[j]["ObservType"].ToString() != "")
                    {
                        if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
                        {
                            string strObsValue = Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"] + strNewLineChar);

                            string[] arrObsValue = strObsValue.Split('/');

                            string[] arrBPObsValue = strObsValue.Split('/');


                            if (Convert.ToString(DS.Tables[0].Rows[j]["Code2"]).Trim() == "80061" && arrObsValue.Length > 2)
                            {

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>CHOL</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>HDL</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>LDL</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[2] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>TRG</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[3] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);


                            }

                            else if (Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]).ToUpper() == "GRBS" && arrObsValue.Length > 1)
                            {

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservType"]) + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>FBS</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[0] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>RBS</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrObsValue[1] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);

                            }

                            else if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_SERV_TYPE"]).Trim() == "C" && arrBPObsValue.Length > 1)
                            {

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    // strData.Append("<Code>BS</Code>" + strNewLineChar);
                                    strData.Append("<Code>BPS</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrBPObsValue[0] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);

                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    // strData.Append("<Code>BD</Code>" + strNewLineChar);
                                    strData.Append("<Code>BPD</Code>" + strNewLineChar);

                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false)
                                {
                                    strData.Append("<Value>" + arrBPObsValue[1] + "</Value>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Value></Value>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false)
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                }


                                strData.Append("</Observation>" + strNewLineChar);




                            }
                            else
                            {
                                strData.Append("<Observation>" + strNewLineChar);

                                strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                                if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                                {
                                    strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]) + "</Code>" + strNewLineChar);
                                }
                                else
                                {
                                    strData.Append("<Code></Code>" + strNewLineChar);
                                }
                                if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"])  !="")
                                {
                                    strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                                }
                                //else
                                //{
                                //    strData.Append("<Value></Value>" + strNewLineChar);
                                //}
                                if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) != "")
                                {
                                    strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                                }
                                //else
                                //{
                                //    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                                //}


                                strData.Append("</Observation>" + strNewLineChar);
                            }


                        }
                        else
                        {
                            strData.Append("<Observation>" + strNewLineChar);

                            strData.Append("<Type>" + DS.Tables[0].Rows[j]["ObservType"].ToString() + "</Type>" + strNewLineChar);
                            if (DS.Tables[0].Rows[j].IsNull("ObservCode") == false)
                            {
                                strData.Append("<Code>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservCode"]) + "</Code>" + strNewLineChar);
                            }
                            else
                            {
                                strData.Append("<Code></Code>" + strNewLineChar);
                            }
                            if (DS.Tables[0].Rows[j].IsNull("ObservValue") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) != "")
                            {
                                strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) + "</Value>" + strNewLineChar);
                            }
                            //else
                            //{
                            //    strData.Append("<Value></Value>" + strNewLineChar);
                            //}
                            if (DS.Tables[0].Rows[j].IsNull("ObservValueType") == false && Convert.ToString(DS.Tables[0].Rows[j]["ObservValue"]) != "")
                            {
                                strData.Append("<ValueType>" + Convert.ToString(DS.Tables[0].Rows[j]["ObservValueType"]) + "</ValueType>" + strNewLineChar);
                            }
                            //else
                            //{
                            //    strData.Append("<ValueType></ValueType>" + strNewLineChar);
                            //}


                            strData.Append("</Observation>" + strNewLineChar);
                        }
                    }

                    if (Convert.ToString(Session["ECLAIM_TYPE"]) == "DXB")
                    {

                        if (Convert.ToString(DS.Tables[0].Rows[j]["HIM_CC"]) != "" && ActCount == 1)
                        {

                            strData.Append("<Observation>" + strNewLineChar);
                            strData.Append("<Type>Text</Type>" + strNewLineChar);
                            strData.Append("<Code>Presenting-Complaint</Code>" + strNewLineChar);
                            strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["HIM_CC"]) + "</Value>" + strNewLineChar);
                            strData.Append("<ValueType>Presenting-Complaint</ValueType>" + strNewLineChar);
                            strData.Append("</Observation>" + strNewLineChar);
                        }




                        if (Convert.ToString(DS.Tables[0].Rows[j]["HIT_ATTACHMENT"]) != "" && Convert.ToString(DS.Tables[0].Rows[j]["HIT_ATTACHMENT"]) != "0")
                        {
                            strData.Append("<Observation>" + strNewLineChar);
                            strData.Append("<Type>File</Type>" + strNewLineChar);
                            strData.Append("<Code>File</Code>" + strNewLineChar);
                            strData.Append("<Value>" + Convert.ToString(DS.Tables[0].Rows[j]["HIT_ATTACHMENT"]) + "</Value>" + strNewLineChar);
                            strData.Append("<ValueType>File</ValueType>" + strNewLineChar);
                            strData.Append("</Observation>" + strNewLineChar);
                        }




                    }
                    strData.Append("</Activity>" + strNewLineChar);



                    // }



                }
                if (DS1.Tables[0].Rows[i].IsNull("PackageName") == false && DS1.Tables[0].Rows[i]["PackageName"].ToString() != "")
                {
                    strData.Append("<Contract>" + strNewLineChar);
                    strData.Append("<PackageName>" + DS1.Tables[0].Rows[i]["PackageName"].ToString() + "</PackageName>" + strNewLineChar);
                    strData.Append("</Contract>" + strNewLineChar);
                }
                strData.Append("</Claim>" + strNewLineChar);
            }
            strData.Append("</Claim.Submission>" + strNewLineChar);

            if (IsError == false)
            {
                strFileName = strPath + strFileId + ".XML";

                StreamWriter oWrite;
                oWrite = File.CreateText(strFileName);
                oWrite.WriteLine(strData);

                oWrite.Close();
                lblStatus.Text = "E-Claim Created - File Name is  " + strFileId + ".XML";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                /* WebservicesSoapClient soapclient = new WebservicesSoapClient("WebservicesSoap");
                  string errorMessage;

                  byte[] fileContent, errorReport;
                  fileContent = System.Text.Encoding.UTF8.GetBytes(Convert.ToString(strData));
                //////  soapclient.UploadTransaction(GlobalValues.Shafafiya_LoginID, GlobalValues.Shafafiya_LoginPassword, fileContent, strFileId + ".XML", out errorMessage, out errorReport);
              

                  Console.WriteLine(errorMessage);
                  if (!errorMessage.Equals("") && errorReport != null && errorReport.Length > 0)
                  {
                      string strErrorPath = GlobalValues.Shafafiya_ErrorFilePath;
                      string strErrorFileName = strErrorPath + strFileId + ".zip";

                      using (FileStream fs = new FileStream(strErrorFileName, FileMode.Create))
                      {
                          fs.Write(errorReport, 0, errorReport.Length);
                      }

                  }
                  else
                  {
                    
                  }
                  */

            }
            else
            {
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('E-Claim Not Created, Check the Log File !');", true);
                lblStatus.Text = "E-Claim Not Created, Check the Log File  - File Name is " + strFileId + ".txt";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        ClaimEnd: ;

            return strFileName;

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='ECLAIM' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnClear.Enabled = false;
                btnShow.Enabled = false;
                fileLogo.Enabled = false;
                btnTestEclaim.Enabled = false;
                btnProductionEclaim.Enabled = false;
                btnSaveToDB.Enabled = false;


            }


            if (strPermission == "7")
            {

                btnClear.Enabled = false;
                btnShow.Enabled = false;
                fileLogo.Enabled = false;
                btnTestEclaim.Enabled = false;
                btnProductionEclaim.Enabled = false;
                btnSaveToDB.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }

        #endregion

        #region AutoCompleteExtender

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //        SetPermission();

                try
                {


                    Session["EClaim"] = "";
                    ViewState["FileName"] = "";
                    stParentName = "0";

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);
                    hidIsEclaimSub.Value = "false";

                    BindScreenCustomization();

                }
                catch (Exception ex)
                {
                    Session["ErrorMsg"] = ex.Message;
                    Response.Redirect("ErrorPage.aspx");
                }
            }

        }


        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGridView.PageIndex = e.NewPageIndex;
            DS = new DataSet();
            DS = (DataSet)Session["EClaim"];


            gvGridView.Visible = true;
            gvGridView.DataSource = DS;
            gvGridView.DataBind();



        }

        protected void gvGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblType = (Label)e.Row.FindControl("lblType");
                Label lblStartType = (Label)e.Row.FindControl("lblStartType");
                Label lblEndType = (Label)e.Row.FindControl("lblEndType");

                string strType = "";
                switch (lblType.Text)
                {
                    case "1":
                        strType = "No Bed + No Emergency Room (OP)";
                        break;
                    case "2":
                        strType = "No Bed + Emergency Room (OP)";
                        break;
                    case "3":
                        strType = "InPatient Bed + No Emergency Room (IP)";
                        break;
                    case "4":
                        strType = "InPatient Bed + Emergency Room (IP)";
                        break;
                    case "5":
                        strType = "Daycase Bed + No Emergency Room (Day Care)";
                        break;
                    case "6":
                        strType = "Daycase Bed + Emergency Room (Day Care)";
                        break;
                    case "7":
                        strType = "National Screening";
                        break;
                    default:
                        break;
                }
                lblType.Text = strType;


                string strStartType = "";
                switch (lblStartType.Text)
                {
                    case "1":
                        strStartType = "Elective";
                        break;
                    case "2":
                        strStartType = "Emergency";
                        break;
                    case "3":
                        strStartType = "Transfer";
                        break;
                    case "4":
                        strStartType = "Live Birth";
                        break;
                    case "5":
                        strStartType = "Still Birth";
                        break;
                    case "6":
                        strStartType = "Dead on Arrival";
                        break;
                    default:
                        break;
                }
                lblStartType.Text = strStartType;


                string strEndType = "";
                switch (lblEndType.Text)
                {
                    case "1":
                        strEndType = "Discharged with approval";
                        break;
                    case "2":
                        strEndType = "Discharged against advice";
                        break;
                    case "3":
                        strEndType = "Discharged absent without leave";
                        break;
                    case "4":
                        strEndType = "Transfer to another facility";
                        break;
                    case "5":
                        strEndType = "Deceased";
                        break;
                    default:
                        break;
                }
                lblEndType.Text = strEndType;




            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            if (radGeneratedType.SelectedIndex == 0)
            {
                Boolean bolIsSubCompany = false;
                bolIsSubCompany = CheckSubCompany();
                hidIsEclaimSub.Value = Convert.ToString(bolIsSubCompany).ToLower();
            }
            else
            {

                hidIsEclaimSub.Value = "false";
            }

            BindGrid();
        }

        protected void btnTestEclaim_Click(object sender, EventArgs e)
        {

            string strClaimGenerate = hidClaimGenerate.Value;

            if (strClaimGenerate == "true")
            {
                string strFileName;
                strFileName = XMLFileWrite("TEST");

            }
            hidClaimGenerate.Value = "";

        }

        protected void btnProductionEclaim_Click(object sender, EventArgs e)
        {
            string strClaimGenerate = hidClaimGenerate.Value;

            if (strClaimGenerate == "true")
            {
                XMLFileWrite("PRODUCTION");
            }
            hidClaimGenerate.Value = "";
        }

        protected void btnSaveToDB_Click(object sender, EventArgs e)
        {
            try
            {

                DS = new DataSet();
                DS = (DataSet)Session["EClaim"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                        con.Open();
                        SqlCommand cmd = new SqlCommand();
                        SqlDataAdapter adpt = new SqlDataAdapter();
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "HMS_SP_EclaimxmlDataAdd";
                        cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@IDPayer", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["IDPayer"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@MemberID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["MemberID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@PayerID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["PayerID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ProviderID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ProviderID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@EmiratesIDNumber", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["EmiratesIDNumber"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Gross", SqlDbType.Decimal)).Value = DS.Tables[0].Rows[i]["Gross"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@PatientShare", SqlDbType.Decimal)).Value = DS.Tables[0].Rows[i]["PatientShare"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Net", SqlDbType.Decimal)).Value = DS.Tables[0].Rows[i]["Net"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Type"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@PatientID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["PatientID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Start", SqlDbType.DateTime)).Value = DS.Tables[0].Rows[i]["Start"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@End", SqlDbType.DateTime)).Value = DS.Tables[0].Rows[i]["End"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Type1", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Type1"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Code", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Code"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Type3", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Type3"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Code3", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Code3"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Start1", SqlDbType.DateTime)).Value = DS.Tables[0].Rows[i]["Start1"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Type2", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Type2"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Code2", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Code2"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Quantity"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Net1", SqlDbType.Decimal)).Value = DS.Tables[0].Rows[i]["Net1"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@OrderingClinician", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["OrderingClinician"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Clinician", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["Clinician"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@PriorAuthorizationID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["PriorAuthorizationID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@CompID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["CompID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@InvoiceID", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["InvoiceID"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@InvType", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["InvType"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@HIT_SERV_CODE", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["HIT_SERV_CODE"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@StartType", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["StartType"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@EndType", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["EndType"].ToString();

                        cmd.Parameters.Add(new SqlParameter("@PackageName", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["PackageName"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS2", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS2"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS2", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS2"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS3", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS3"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS3", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS3"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS4", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS4"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS4", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS4"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS5", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS5"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS5", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS5"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS6", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS6"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS6", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS6"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS7", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS7"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS7", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS7"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS8", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS8"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS8", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS8"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS9", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS9"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS9", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS9"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS10", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS10"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS10", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS10"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDS11", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDS11"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeS11", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeS11"].ToString();

                        cmd.Parameters.Add(new SqlParameter("@ICDA", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDA"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ICDCodeA", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ICDCodeA"].ToString();

                        cmd.Parameters.Add(new SqlParameter("@ObservType", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ObservType"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ObservCode", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ObservCode"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ObservValue", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ObservValue"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@ObservValueType", SqlDbType.VarChar)).Value = DS.Tables[0].Rows[i]["ObservValueType"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = ViewState["FileName"].ToString();

                        // cmd.Parameters.Add(new SqlParameter("@InvDate", SqlDbType.DateTime)).Value = DS.Tables[0].Rows[i]["Start"].ToString();

                        int k = cmd.Parameters.Count;
                        for (int j = 0; j < k; j++)
                        {
                            if (cmd.Parameters[j].Value == "")
                            {
                                cmd.Parameters[j].Value = DBNull.Value;
                            }
                        }

                        cmd.ExecuteNonQuery();
                        con.Close();

                    }


                }
                lblStatus.Text = "Details Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            catch (Exception ex)
            {
                Session["ErrorMsg"] = ex.Message;
                Response.Redirect("ErrorPage.aspx");
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void radGeneratedType_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTestEclaim.Visible = true;
            btnProductionEclaim.Visible = true;
            DS = new DataSet();
            Session["EClaim"] = DS;


            lblTotalClaims.Text = "0";
            gvGridView.Visible = false;
            gvGridView.DataBind();

            if (radGeneratedType.SelectedIndex == 1)
            {
                btnTestEclaim.Visible = false;
                btnProductionEclaim.Visible = false;
            }
        }

        #endregion

    }
}