﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
 
namespace Mediplus.HMS.Billing
{
    public partial class CashRegisterOut : System.Web.UI.Page
    {
        # region Variable Declaration

        CashRegisterBAL objCash = new CashRegisterBAL();
        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='RECEIPT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnFind.Enabled = false;
                //btnClear.Enabled = false;
                //btnShow.Enabled = false;
                //fileLogo.Enabled = false;
                //btnSaveToDB.Enabled = false;
                //btnCancel.Enabled = false;

            }


            if (strPermission == "7")
            {


                //btnFind.Enabled = false;
                //btnClear.Enabled = false;
                //btnShow.Enabled = false;
                //fileLogo.Enabled = false;
                //btnSaveToDB.Enabled = false;
                //btnCancel.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }


        void BindData()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND BRANCHID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND UserId= '" + txtUserCode.Value.Trim() + "' and convert(varchar(10),Date,103)='" + txtDate.Value.Trim() + "'";

            DataSet DS = new DataSet();
            objCash = new CashRegisterBAL();
            DS = objCash.CashRegisterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblShiftStatus.InnerText = Convert.ToString(DS.Tables[0].Rows[0]["Status"]);

                if (lblShiftStatus.InnerText.ToUpper() == "CLOSE")
                {
                    btnShiftClosing.Enabled = false;
                }
                if (DS.Tables[0].Rows[0].IsNull("CountOut1000") == false)
                {
                    txt1000Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut1000"]) * 1000) + ".00";
                    txt1000.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut1000"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut500") == false)
                {
                    txt500Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut500"]) * 500) + ".00";
                    txt500.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut500"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut200") == false)
                {
                    txt200Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut200"]) * 200) + ".00";
                    txt200.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut200"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut100") == false)
                {
                    txt100Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut100"]) * 100) + ".00";
                    txt100.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut100"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut50") == false)
                {
                    txt50Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut50"]) * 50) + ".00";
                    txt50.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut50"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut20") == false)
                {
                    txt20Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut20"]) * 20) + ".00";
                    txt20.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut20"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut10") == false)
                {
                    txt10Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut10"]) * 10) + ".00";
                    txt10.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut10"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut5") == false)
                {
                    txt5Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut5"]) * 5) + ".00";
                    txt5.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut5"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut1") == false)
                {
                    txt1Amt.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut1"]) + ".00";
                    txt1.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut1"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut0_50") == false)
                {
                    txt0_50Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut50"]) * .50);
                    txt0_50.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut0_50"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CountOut0_25") == false)
                {
                    txt0_25Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["CountOut0_25"]) * .25);
                    txt0_25.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOut0_25"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("CountOutOther") == false)
                {
                    txtOtherAmt.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOutOther"]) + ".00";
                    txtOther.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOutOther"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("TotalAmount") == false)
                {
                    txtOpeningCash.Value = Convert.ToString(DS.Tables[0].Rows[0]["TotalAmountDesc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("InvoiceCash") == false)
                {
                    txtInvoiceCash.Value = Convert.ToString(DS.Tables[0].Rows[0]["InvoiceCash"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("InvoiceCC") == false)
                {
                    txtInvoiceCC.Value = Convert.ToString(DS.Tables[0].Rows[0]["InvoiceCC"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("InvoiceChq") == false)
                {
                    txtInvoiceChq.Value = Convert.ToString(DS.Tables[0].Rows[0]["InvoiceChq"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Expense") == false)
                {
                    txtExpense.Value = Convert.ToString(DS.Tables[0].Rows[0]["Expense"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("MagazinePaid") == false)
                {
                    txtMagazinePaid.Value = Convert.ToString(DS.Tables[0].Rows[0]["MagazinePaid"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("CounterWithDrawel") == false)
                {
                    txtCounterWithDrawel.Value = Convert.ToString(DS.Tables[0].Rows[0]["CounterWithDrawel"]);
                }






            }

        }

        void BindInvoice()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_CREATED_USER= '" + txtUserCode.Value.Trim() + "' and convert(varchar(10),HIM_DATE,103)='" + txtDate.Value.Trim() + "'";
            Criteria += " AND  HIM_PAYMENT_TYPE='Credit Card' ";

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" CAST(SUM(isnull(HIM_CC_AMT,0)) AS  DECIMAL(10,2)) as TotalCCAmt", "HMS_INVOICE_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("TotalCCAmt") == false)
                {
                    txtInvoiceCC.Value = Convert.ToString(DS.Tables[0].Rows[0]["TotalCCAmt"]);
                }
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();

                try
                {


                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtDate.Value = strFromDate.ToString("dd/MM/yyyy");

                    txtUserCode.Value = Convert.ToString(Session["User_ID"]);
                    txtUserName.Value = Convert.ToString(Session["User_Name"]);

                    BindData();
                    BindInvoice();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "     CashRegisterOut.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }



        protected void btnShiftClosing_Click(object sender, EventArgs e)
        {
            try
            {
                objCash = new CashRegisterBAL();
                objCash.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCash.UserID = Convert.ToString(Session["User_ID"]);
                objCash.Date = txtDate.Value.Trim();
                objCash.CounterNo = "";
                objCash.UserName = txtUserName.Value.Trim();
                objCash.Time = "";
                objCash.CountOut1000 = txt1000.Value.Trim();
                objCash.CountOut500 = txt500.Value.Trim();
                objCash.CountOut200 = txt200.Value.Trim();
                objCash.CountOut100 = txt100.Value.Trim();
                objCash.CountOut50 = txt50.Value.Trim();
                objCash.CountOut20 = txt20.Value.Trim();
                objCash.CountOut10 = txt10.Value.Trim();
                objCash.CountOut5 = txt5.Value.Trim();
                objCash.CountOut1 = txt1.Value.Trim();
                objCash.CountOut0_50 = txt0_50.Value.Trim();
                objCash.CountOut0_25 = txt0_25.Value.Trim();
                objCash.CountOutOther = txtOther.Value.Trim();
                objCash.InvoiceCash = txtInvoiceCash.Value.Trim();
                objCash.InvoiceCC = txtInvoiceCC.Value.Trim();
                objCash.InvoiceChq = txtInvoiceChq.Value.Trim();
                objCash.Expense = txtExpense.Value.Trim();
                objCash.MagazinePaid = txtMagazinePaid.Value.Trim();
                objCash.CounterWithDrawel = txtCounterWithDrawel.Value.Trim();




                objCash.CashRegisterOutUpdate();



                lblStatus.Text = "Data Saved ";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                btnShiftClosing.Enabled = false;
                BindData();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     CashRegisterOut.btnShiftClosing_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        #endregion
    }
}