﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
 

namespace Mediplus.HMS.Billing
{
    public partial class CashRegisterIn : System.Web.UI.Page
    {
        # region Variable Declaration

        CashRegisterBAL objCash = new CashRegisterBAL();
        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='RECEIPT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                //btnFind.Enabled = false;
                //btnClear.Enabled = false;
                //btnShow.Enabled = false;
                //fileLogo.Enabled = false;
                //btnSaveToDB.Enabled = false;
                //btnCancel.Enabled = false;

            }


            if (strPermission == "7")
            {


                //btnFind.Enabled = false;
                //btnClear.Enabled = false;
                //btnShow.Enabled = false;
                //fileLogo.Enabled = false;
                //btnSaveToDB.Enabled = false;
                //btnCancel.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }


        void BindData()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND BRANCHID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND UserId= '" + txtUserCode.Value.Trim() + "' and convert(varchar(10),Date,103)='" + txtDate.Value.Trim() + "'";
            Criteria += " AND  [Status] !='Close' ";

            DataSet DS = new DataSet();
            objCash = new CashRegisterBAL();
            DS = objCash.CashRegisterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("Count1000") == false)
                {
                    txt1000Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count1000"]) * 1000) + ".00";
                    txt1000.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count1000"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count500") == false)
                {
                    txt500Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count500"]) * 500) + ".00";
                    txt500.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count500"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count200") == false)
                {
                    txt200Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count200"]) * 200) + ".00";
                    txt200.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count200"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count100") == false)
                {
                    txt100Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count100"]) * 100) + ".00";
                    txt100.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count100"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count50") == false)
                {
                    txt50Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count50"]) * 50) + ".00";
                    txt50.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count50"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count20") == false)
                {
                    txt20Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count20"]) * 20) + ".00";
                    txt20.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count20"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count10") == false)
                {
                    txt10Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count10"]) * 10) + ".00";
                    txt10.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count10"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count5") == false)
                {
                    txt5Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count5"]) * 5) + ".00";
                    txt5.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count5"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count1") == false)
                {
                    txt1Amt.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count1"]) + ".00";
                    txt1.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count1"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count0_50") == false)
                {
                    txt0_50Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count50"]) * .50);
                    txt0_50.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count0_50"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("Count0_25") == false)
                {
                    txt0_25Amt.Value = Convert.ToString(Convert.ToInt32(DS.Tables[0].Rows[0]["Count0_25"]) * .25);
                    txt0_25.Value = Convert.ToString(DS.Tables[0].Rows[0]["Count0_25"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("CountOther") == false)
                {
                    txtOtherAmt.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOther"]) + ".00";
                    txtOther.Value = Convert.ToString(DS.Tables[0].Rows[0]["CountOther"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("TotalAmount") == false)
                {
                    txtTotalAmt.Value = Convert.ToString(DS.Tables[0].Rows[0]["TotalAmountDesc"]);
                }
            }

        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();

                try
                {


                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtDate.Value = strFromDate.ToString("dd/MM/yyyy");

                    txtUserCode.Value = Convert.ToString(Session["User_ID"]);
                    txtUserName.Value = Convert.ToString(Session["User_Name"]);

                    BindData();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "     CashRegisterIn.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }



        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 ";
                Criteria += " AND BRANCHID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND UserId= '" + txtUserCode.Value.Trim() + "' and convert(varchar(10),Date,103)='" + txtDate.Value.Trim() + "'";
                Criteria += " AND  [Status] ='Close' ";

                DataSet DS = new DataSet();
                objCash = new CashRegisterBAL();
                DS = objCash.CashRegisterGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    goto FunEnd;
                }
                objCash = new CashRegisterBAL();
                objCash.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCash.UserID = Convert.ToString(Session["User_ID"]);
                objCash.Date = txtDate.Value.Trim();
                objCash.CounterNo = "";
                objCash.UserName = txtUserName.Value.Trim();
                objCash.Time = "";
                objCash.Count1000 = txt1000.Value.Trim();
                objCash.Count500 = txt500.Value.Trim();
                objCash.Count200 = txt200.Value.Trim();
                objCash.Count100 = txt100.Value.Trim();
                objCash.Count50 = txt50.Value.Trim();
                objCash.Count20 = txt20.Value.Trim();
                objCash.Count10 = txt10.Value.Trim();
                objCash.Count5 = txt5.Value.Trim();
                objCash.Count1 = txt1.Value.Trim();
                objCash.Count0_50 = txt0_50.Value.Trim();
                objCash.Count0_25 = txt0_25.Value.Trim();
                objCash.CountOther = txtOther.Value.Trim();
                objCash.TotalAmount = txtTotalAmt.Value.Trim();


                objCash.CashRegisterAdd();



              //  lblStatus.Text = "Data Saved ";
            //lblStatus.ForeColor = System.Drawing.Color.Green;


            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     CashRegisterIn.btnLogin_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("Invoice.aspx?MenuName=Billing");

        }

        #endregion
    }
}