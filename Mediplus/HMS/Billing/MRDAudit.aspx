﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master"   AutoEventWireup="true" CodeBehind="MRDAudit.aspx.cs" Inherits="Mediplus.HMS.Billing.MRDAudit" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


 
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />

     
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>

   <script language="javascript" type="text/javascript">
       function LabResultReport(TransNo, RptType) {
           var ReportName = "LabReport.rpt";


           if (RptType.toUpperCase() == 'U') {
               Report = "LabReportUsr.rpt";
           }

           var Criteria = " 1=1 ";

           if (TransNo != "") {
               Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';
               var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'RadReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

               win.focus();
           }

       }

       function RadResultReport(TransNo) {

           var Report = "RadiologyReport.rpt";
           var Criteria = " 1=1 ";

           if (TransNo != "") {

               Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

               var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
               win.focus();


           }

       }


       function CoderAllSummaryReport(TransNo) {
           var Report = "LabReportUsrEMR.rpt";
           var Criteria = " 1=1 ";

           if (TransNo != "") {

               Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';

               var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

               win.focus();

           }

       }


       function ShowAttachment(PTID, FileName) {
           var win = window.open('DisplayAttachments.aspx?PT_ID=' + PTID + '&FileName=' + FileName, '_new', 'menubar=no,left=100,top=80,height=400,width=500,scrollbars=1')
           win.focus();
           return true;

       }

       function ShowReferral(ReferalType, FileNo, BranchId, EMRID) {
           var Report;
           if (ReferalType == "IntRef") {
               Report = "IntReferral.rpt";

           }
           else {
               Report = "ExtReferral.rpt";
           }

           var Criteria = " 1=1 ";
           Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
           Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
           Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

           //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
           var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

           win.focus();

       }




    </script>
   <script language="javascript" type="text/javascript">

       function OnlyNumeric(evt) {
           var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
           if (chCode >= 48 && chCode <= 57 ||
                chCode == 46) {
               return true;
           }
           else

               return false;
       }


       function CoderVerifyVal() {
           var label;
           label = document.getElementById('<%=lblStatus.ClientID%>');
                  label.style.color = 'red';
                  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



            if (document.getElementById('<%=hidInvoice.ClientID%>').value == "") {
                      document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Invoice";
                return false;

            }


        }

        function ServiceAddVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



            if (document.getElementById('<%=txtServCode.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the service code";
                return false;

            }

            if (document.getElementById('<%=txtServName.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the service Name";
                return false;

            }


            if (document.getElementById('<%=drpDoctor.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Doctor";
                 return false;

             }
         }


         function PatientPopup(CtrlName, strValue) {
             var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=CoderVerification&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
             win.focus();

             return true;

         }

         function InsurancePopup(PageName, strValue, evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode == 126) {
                 var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                 win.focus();
                 return false;
             }
             return true;
         }


         function CompIdSelected() {
             if (document.getElementById('<%=txtCompanyID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyID.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }
            }

            return true;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function ServIdSelected() {
            if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function DiagIdSelected() {
            if (document.getElementById('<%=txtDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DiagNameSelected() {
            if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }




    </script>

   
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     
    <div>
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidErrorChecking" runat="server" value="false" />
 
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>

    </table>
    <div style="padding-top: 0px; width: 1000PX; height: 150px;   border: thin; border-color: #000; border-style: groove;">
        <input type="hidden" id="hidInvoice" runat="server" />
             <table width="100%" border="0">
                 <tr>
                         <td class="lblCaption1" >
                              File No
                          </td>
                          <td>
                               <asp:TextBox ID="txtSrcFileNo" runat="server" Width="150px" height="20px" MaxLength="10" CssClass="TextBoxStyle" BackColor="#e3f7ef"  ondblclick="return PatientPopup('FileNo',this.value);" ></asp:TextBox>
                              &nbsp;
                              <asp:label id="Label32" runat="server" cssclass="lblCaption1"
                                text="Treatment Type"></asp:label>  &nbsp;
                            
                         <asp:DropDownList ID="drpTreatmentType" runat="server" CssClass="TextBoxStyle" Width="100px"   >
                                 
                                <asp:ListItem Value="OP">OP</asp:ListItem>
                                <asp:ListItem Value="IP">IP</asp:ListItem>
                             </asp:DropDownList>
                          </td>
                          <td class="lblCaption1">
                              Invoice No.
                          </td>
                          <td>
                             <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextBoxStyle" height="20px"    Width="200px" MaxLength="10" ></asp:TextBox>
                             <asp:Button ID="btnAddInvoice" runat="server" Text="Add" Width="60px" CssClass="button gray small"  OnClick="btnAddInvoice_Click"  />

                                <asp:Button ID="btnInvFind" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button red small"
                            OnClick="btnInvFind_Click" Text="Refresh"   />
                          </td>
                  </tr>
                   <tr>
                        <td style="width: 150px">
                            <asp:Label ID="Label7" runat="server" CssClass="lblCaption1"
                                Text="From"></asp:Label>
                        </td>
                        <td style="width: 400px" >
                            <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            <asp:TextBox ID="txtFromTime" runat="server" Width="30px" MaxLength="5" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFromTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                            &nbsp;

 
                            <asp:Label ID="Label8" runat="server" CssClass="lblCaption1"
                                Text="To"></asp:Label>

                            <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10"  CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            <asp:TextBox ID="txtToTime" runat="server" Width="30px" MaxLength="5" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtToTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>


                        </td>
                       <td></td>
                       <td rowspan="4">
                             <asp:ListBox ID="lstInvoiceNo" runat="server" CssClass="TextBoxStyle" Height="90px"  Width="200px" SelectionMode="Multiple" ></asp:ListBox>
                       </td>
                 </tr>
                <tr>
                   <td class="lblCaption1">
                    Company 

                    </td>
                    <td style="width: 400px" colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtCompanyID" runat="server" CssClass="TextBoxStyle"    Width="70px" AutoPostBack="true" OnTextChanged="txtCompanyID_TextChanged"  MaxLength="10"   onblur="return CompIdSelected()"  ></asp:TextBox>
                                <asp:TextBox ID="txtCompanyName" runat="server" CssClass="TextBoxStyle"    Width="275px" MaxLength="50"  onblur="return CompNameSelected()"  ></asp:TextBox>

                                <div ID="divComp" style="visibility:hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompanyID" MinimumPrefixLength="1" ServiceMethod="GetCompany" 
                                CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divComp"    ></asp:autocompleteextender>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                 CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divComp"    ></asp:autocompleteextender>
                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
               
                    </tr>
              <tr>
                        <td class="lblCaption1">
                                       Doctor 
                       </td>
                       <td class="auto-style28" colspan="3">
                                      
                            <div ID="divDr" style="visibility:hidden;"></div>           
                           <asp:TextBox ID="txtDoctorID" runat="server" CssClass="TextBoxStyle"    Width="70px" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()"  ></asp:TextBox>
                           <asp:TextBox ID="txtDoctorName" runat="server" CssClass="TextBoxStyle"   Width="275px" MaxLength="50" onblur="return DRNameSelected()"  ></asp:TextBox>
                           <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"
                                CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divDr"  ></asp:AutoCompleteExtender>
                           <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName" 
                                CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divDr"  ></asp:AutoCompleteExtender>
                                                                                      
                       </td>
              </tr>
                 <tr>
                     <td>

                     </td>
                     <td>
                            <asp:dropdownlist id="drpCoderType" runat="server" cssclass="TextBoxStyle" width="155px"  >
                                 <asp:ListItem Value="">All</asp:ListItem>
                                            <asp:ListItem Value="Pending">Pending</asp:ListItem>
                                            <asp:ListItem Value="Completed">Completed</asp:ListItem>
                       </asp:dropdownlist>
                    &nbsp;
                            <asp:label id="Label31" runat="server" cssclass="lblCaption1"
                                text="Patient Type"></asp:label>

                        
                             <asp:DropDownList ID="drpPatientType" runat="server" CssClass="TextBoxStyle" Width="100px"   >
                                 <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                <asp:ListItem Value="CR">Credit</asp:ListItem>
                             </asp:DropDownList>
                       
                        </td>
                 </tr>
                </table>

        </div>
      <div style="padding-top: 0px; width: 1000PX; height: 200px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
           <asp:GridView ID="gvVisit" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False"   OnRowDataBound="gvGridView_RowDataBound"
                    EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200" Width="100%"  >
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                   
                    <Columns>
                         <asp:TemplateField HeaderText="Action"  HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" >
                                        <ItemTemplate>
                                               <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="VisitSelect_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="No"   HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblSlNo" CssClass="GridRow" Width="30px" Style="text-align: center; padding-right: 2px;" runat="server" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No"  >
                            <ItemTemplate>
                                 
                                    <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>' ></asp:Label>
                                     <asp:Label ID="lblgvVisit_BRANCH_ID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_BRANCH_ID") %>'  visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisit_EMR_ID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_EMR_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisit_DR_ID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_DR_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisit_CompID" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_COMP_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisit_PhotoID" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_PHOTO_ID") %>' visible="false"></asp:Label>
                                 <asp:Label ID="lblgvVisitType" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_VISIT_TYPE") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvVisit_PTID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>
                                 
                            </ItemTemplate>

                        </asp:TemplateField>
                      <asp:TemplateField HeaderText="Invoice#."  >
                            <ItemTemplate>
                                 
                                    <asp:Label ID="lblgvVisit_Invoice_ID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HPV_INVOICE_ID") %>'></asp:Label>
                                 
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Doctor"  >
                            <ItemTemplate>
                                 
                                    <asp:Label ID="lblgvVisit_DrName" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Date"  >
                            <ItemTemplate>
                                
                                <asp:Label ID="lblgvVisit_Date" CssClass="GridRow" Width="100px"  runat="server" Text='<%# Bind("HPV_DATEDesc") %>'></asp:Label>
                                 <%# Eval("HPV_TIME") %> 
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                          
                          <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                            <ItemTemplate>
                                    <asp:Label ID="lblgvVisit_CompName" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>

           <asp:GridView ID="gvIPAdmission" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False"   OnRowDataBound="gvIPAdmission_RowDataBound"
                    EnableModelValidation="True" OnPageIndexChanging="gvIPAdmission_PageIndexChanging" PageSize="200" Width="100%" gridlines="None">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" />
                   
                    <Columns>

                        <asp:TemplateField HeaderText="No"   HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton22" runat="server" OnClick="VisitSelect_Click" ToolTip="Update Waiting List">
                                    <asp:Label ID="gvIPAdmissionSLNo" CssClass="GridRow" Width="30px" Style="text-align: center; padding-right: 2px;" runat="server" Text='<%# Bind("IAS_ADMISSION_NO") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File No"  >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton23" runat="server" OnClick="IPAdmissionSelect_Click"    >
                                  
                                     <asp:Label ID="lblgvIPAdmBranchID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_BRANCHID") %>'  visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmIPEMRID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_IP_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmDRID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_DR_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmCompID" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("IAS_INS_COMP_ID") %>' visible="false"></asp:Label>
                                    <%--<asp:Label ID="Label39" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("HPV_PHOTO_ID") %>' visible="false"></asp:Label>--%>

                                    <asp:Label ID="lblgvIPAdmPTID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_PT_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                      <asp:TemplateField HeaderText="Invoice No."  >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton24" runat="server" OnClick="IPAdmissionSelect_Click"   >
                                    <asp:Label ID="lblgvIPAdmInvoiceID" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("IAS_INVOICE_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Admission No."  >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton32" runat="server" OnClick="IPAdmissionSelect_Click"   >
                                      <asp:Label ID="lblgvIPAdmWard" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_WARD_NO") %>' visible="false"></asp:Label>
                                     <asp:Label ID="lblgvIPAdmRoom" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_ROOM_NO") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmBed" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_BED_NO") %>' visible="false"></asp:Label>
                                    

                                     <asp:Label ID="lblgvIPAdmDrgCode" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_DRG_CODE") %>' visible="false"></asp:Label>
                                     <asp:Label ID="lblgvIPAdmDrgAmt" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_DRG_AMOUNT") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmAuthID" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_AUTHORIZATION_ID") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmAuthStart" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_AUTHORIZATION_STARTDesc") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmAuthEnd" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_AUTHORIZATION_ENDDesc") %>' visible="false"></asp:Label>
                                    <asp:Label ID="lblgvIPAdmStay" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("IAS_LENTH_OF_STAY") %>' visible="false"></asp:Label>



                                    <asp:Label ID="lblgvIPAdmissionNo" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("IAS_ADMISSION_NO") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Doctor"  >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton25" runat="server" OnClick="IPAdmissionSelect_Click"   >
                                    <asp:Label ID="lblgvIPAdmDRName" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("IAS_DR_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Date"  >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton26" runat="server" OnClick="IPAdmissionSelect_Click"   >
                                    <asp:Label ID="lblgvIPAdmDate" CssClass="GridRow" Width="70px"  runat="server" Text='<%# Bind("IAS_DATEDesc") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                          
                          <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton28" runat="server" OnClick="IPAdmissionSelect_Click"   >
                                    <asp:Label ID="lblgvIPAdmCompName" CssClass="GridRow" Width="250px"  runat="server" Text='<%# Bind("IAS_INS_COMP_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status" FooterText="Status" SortExpression="IAS_STATUSDesc"   >
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton33" runat="server" OnClick="IPAdmissionSelect_Click"   >
                                    <asp:Label ID="lblIASStatus" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_STATUSDesc") %>'></asp:Label>
                                </asp:LinkButton> 
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
      </div>
      <div style="padding-top: 0px; width: 1000PX; height: 200px; overflow: auto; border: thin; border-color: #000; border-style: groove;display:none;">
              <asp:GridView ID="gvInvoice" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%" OnRowDataBound="gvInvoice_RowDataBound" gridlines="none">
                <HeaderStyle CssClass="GridHeader_Blue" />
                <RowStyle CssClass="GridRow" />
                <Columns>
                     <asp:TemplateField HeaderText="Action"  HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" >
                                        <ItemTemplate>
                                               <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                OnClick="InvSelect_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="No"  ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="50px">
                            <ItemTemplate>
                                 
                                     <asp:Label ID="lblGrossTotal" CssClass="label" runat="server" Text='<%# Bind("HIM_GROSS_TOTAL") %>'  VISIBLE="FALSE"  ></asp:Label>
                                     <asp:Label ID="lblClaimAmount" CssClass="label" runat="server" Text='<%# Bind("HIM_CLAIM_AMOUNT") %>'  VISIBLE="FALSE"  ></asp:Label>
                                     <asp:Label ID="lblHospDiscAmt" CssClass="label" runat="server" Text='<%# Bind("HIM_HOSP_DISC_AMT") %>'  VISIBLE="FALSE"  ></asp:Label>
                                     <asp:Label ID="lblDeductible" CssClass="label" runat="server" Text='<%# Bind("HIM_DEDUCTIBLE") %>'  VISIBLE="FALSE"  ></asp:Label>
                                     <asp:Label ID="lblCoInsAmt" CssClass="label" runat="server" Text='<%# Bind("HIM_CO_INS_AMOUNT") %>'  VISIBLE="FALSE"  ></asp:Label>
                                     <asp:Label ID="lblSplDisc" CssClass="label" runat="server" Text='<%# Bind("HIM_SPL_DISC") %>'  VISIBLE="FALSE"  ></asp:Label>
                                    <asp:Label ID="lblInvoiceType" CssClass="label" runat="server" Text='<%# Bind("HIM_INVOICE_TYPE") %>'  VISIBLE="FALSE"  ></asp:Label>
                                      <asp:Label ID="lblBranchID" CssClass="label" runat="server" Text='<%# Bind("HIM_BRANCH_ID") %>'   VISIBLE="FALSE" ></asp:Label>
                                     <asp:Label ID="lblEMRID" CssClass="label" runat="server" Text='<%# Bind("HIM_EMR_ID") %>'  VISIBLE="FALSE"  ></asp:Label>
                                     <asp:Label ID="lblEMCPTCOde" CssClass="label" runat="server" Text='<%# Bind("HEIM_EMCPT_CODE") %>'  VISIBLE="FALSE"  ></asp:Label>
                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" style="text-align:center;padding-right:2px;" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>' ></asp:Label>

                                 
                            </ItemTemplate>
                     </asp:TemplateField>
                    <asp:TemplateField HeaderText="Inv. No" HeaderStyle-Width="110px">
                        <ItemTemplate>
                             
                                <asp:Label ID="lblInvoiceID" CssClass="label" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>'  ></asp:Label>
                           
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Inv. Date" HeaderStyle-Width="110px">
                        <ItemTemplate>
                            
                                <asp:Label ID="lblgvInvDt" CssClass="label" runat="server" Text='<%# Bind("HIM_DATEDESC") %>'  ></asp:Label>
                           
                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Patient ID">
                        <ItemTemplate>
                             
                                <asp:Label ID="lblPatientId" CssClass="label" runat="server" Text='<%# Bind("HIM_PT_ID") %>'></asp:Label>
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Doctor">
                        <ItemTemplate>
                            
                                 <asp:Label ID="lblgvInvDrCode" CssClass="label" runat="server" Text='<%# Bind("HIM_DR_CODE") %>' visible="false"></asp:Label>
                                <asp:Label ID="lblgvOrderingDR" CssClass="label" runat="server" Text='<%# Bind("HIM_ORDERING_DR_NAME") %>' visible="false"></asp:Label>
                                <asp:Label ID="lblgvInvDrName" CssClass="label" runat="server" Text='<%# Bind("HIM_DR_NAME") %>'></asp:Label>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                      
                    <asp:TemplateField HeaderText="Company">
                        <ItemTemplate>
                            
                                <asp:Label ID="lblBilToCode" CssClass="label" runat="server" Text='<%# Bind("HIM_BILLTOCODE") %>' visible="false" ></asp:Label>
                                <asp:Label ID="lblgvInvSubInsCode" CssClass="label" runat="server" Text='<%# Bind("HIM_SUB_INS_CODE") %>' visible="false" ></asp:Label>
                                <asp:Label ID="lblgvInvPolicyNo" CssClass="label" runat="server" Text='<%# Bind("HIM_POLICY_NO") %>' visible="false" ></asp:Label>
                                <asp:Label ID="lblgvInvPolicyType" CssClass="label" runat="server" Text='<%# Bind("HIM_POLICY_TYPE") %>' visible="false" ></asp:Label>
                                <asp:Label ID="lblgvInvInsName" CssClass="label" runat="server" Text='<%# Bind("HIM_INS_NAME") %>'></asp:Label>
                             
                        </ItemTemplate>
                    </asp:TemplateField>
                  
                     <asp:TemplateField HeaderText="Coder">
                        <ItemTemplate>
                             
                                <asp:Label ID="Label24" CssClass="label" runat="server" Text='<%# Bind("HEIM_CODER_NAME") %>'></asp:Label>
                             
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
    <br />
    
      <table width="100%" border="0">
                     <tr>
                         <td>
                        <asp:Button ID="btnCoderVerify" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small" OnClick="btnCoderVerify_Click" Text="Coder Verify"  OnClientClick="return CoderVerifyVal();"/>
                        <asp:Button ID="btnXMLGenerate"  Visible="false"  runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnXMLGenerate_Click" Text="XML Generate"  />

                        <asp:Button ID="btnAccumedTransfer"   runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button gray small" OnClick="btnAccumedTransfer_Click" Text="Accumed Transfer"  />

                        <asp:Button ID="btnAccumedXMLGenerate" Visible="false"   runat="server" Style="padding-left: 5px; padding-right: 5px; width: 150px;" CssClass="button gray small" OnClick="btnAccumedXMLGenerate_Click" Text="ACCumed XML Generate"  />

                       <asp:Button ID="btnClear" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnClear_Click" Text="Clear" />
                       <asp:Button ID="btnFullSummaryReport" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnFullSummaryReport_Click" Text="Summary Report" visible="false"  />
                       <asp:Button ID="btnExport" visible="false" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnExport_Click" Text="Export"  />


                         </td>

                         </tr>
          </table>
      <div style="padding: 5px; width: 1000px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
           
    <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="AjaxTabStyle" width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Patient Details" Width="100%">
            <contenttemplate>
                 <table width="100%" border="0">
                     <tr>
                        <td class="lblCaption1" style="height:25px;">
                         File No
                        </td>
                           <td>
                            <asp:TextBox ID="txtFileNo" runat="server" Width="150px" CssClass="TextBoxStyle"      ></asp:TextBox>

                           </td>
                     </tr>

                       <tr>
                        <td class="lblCaption1" style="height:25px;">
                          First  Name
                        </td>
                           <td>
                            <asp:TextBox ID="txtFName" runat="server" Width="150px" CssClass="TextBoxStyle"    ></asp:TextBox>

                           </td>
                            <td class="lblCaption1" style="height:25px;">
                              Middle Name
                        </td>
                         <td  > 
                               <asp:TextBox ID="txtMName" runat="server" Width="150px" CssClass="TextBoxStyle"   ></asp:TextBox>

                            </td>
                             <td class="lblCaption1" >
                           Last Name
                              </td>
                         <td  >
                           <asp:TextBox ID="txtLName" runat="server" Width="150px" CssClass="TextBoxStyle"      ></asp:TextBox>

                        </td>
                   </tr>     
                      <tr>
                          
                            <td class="lblCaption1" style="height:25px;">
                        DOB
                        </td>
                           <td>
                             <asp:TextBox ID="txtDOB" runat="server" Width="75px" CssClass="TextBoxStyle" MaxLength="10"  ReadOnly="True"      ></asp:TextBox>
                             <asp:Label ID="Label120" runat="server" CssClass="label" Text="Age"></asp:Label>
                             <asp:TextBox ID="txtAge" runat="server" Width="20px" MaxLength="3" CssClass="TextBoxStyle"  ReadOnly="True"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                             <asp:TextBox ID="txtMonth" runat="server" Width="20px" MaxLength="2" CssClass="TextBoxStyle"  ReadOnly="True"   ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                      Sex
                        </td>
                           <td>
                             <asp:TextBox ID="txtSex" runat="server" Width="150px" CssClass="TextBoxStyle" MaxLength="10"  ReadOnly="True"      ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:25px;">
                                 <asp:Label ID="lblIDCaption" runat="server" CssClass="label" Text="EmiratesId"></asp:Label>
                        </td>
                           <td>
                             <asp:TextBox ID="txtEmiratesID" runat="server" Width="150px" CssClass="TextBoxStyle"    ReadOnly="True"    ></asp:TextBox>

                           </td>

                      </tr>
                       <tr>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                       PO Box
                        </td>
                           <td>
                             <asp:TextBox ID="txtPoBox" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"  ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                    Address
                        </td>
                           <td colspan="3">
                             <asp:TextBox ID="txtAddress" runat="server" Width="87%" CssClass="TextBoxStyle"    TextMode="MultiLine"    BorderColor="#CCCCCC" ReadOnly="True"   ></asp:TextBox>

                           </td>
                            
                      </tr>
                       <tr>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                        City
                        </td>
                           <td>
                             <asp:TextBox ID="txtCity" runat="server" Width="150px" CssClass="TextBoxStyle"  ReadOnly="True"      ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                     Area
                        </td>
                           <td>
                             <asp:TextBox ID="txtArea" runat="server" Width="150px" CssClass="TextBoxStyle"  ReadOnly="True"      ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:25px;">
                       Nationality
                        </td>
                           <td>
                             <asp:TextBox ID="txtNationality" runat="server" Width="150px" CssClass="TextBoxStyle"    ReadOnly="True"      ></asp:TextBox>

                           </td>

                      </tr>
                        <tr>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                      Home PH.No.
                        </td>
                           <td>
                             <asp:TextBox ID="txtPhone1" runat="server" Width="150px" CssClass="TextBoxStyle"  ReadOnly="True"   ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:25px;">
                    Mob. No 1
                        </td>
                           <td>
                             <asp:TextBox ID="txtMobile1" runat="server" Width="150px" CssClass="TextBoxStyle"   ReadOnly="True"     ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:25px;">
                     Mob. No 2
                        </td>
                           <td>
                             <asp:TextBox ID="txtMobile2" runat="server" Width="150px"  CssClass="TextBoxStyle"       ReadOnly="True"      ></asp:TextBox>

                           </td>

                      </tr>
                     <tr>
                            <td  class="lblCaption1"  style="height:25px;">
                           Ins. Co
                         </td>
                           <td>
                               
                             <asp:TextBox ID="txtProviderName" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"      ></asp:TextBox>

                           </td>
                           <td  class="lblCaption1"  style="height:25px;">
                           Policy Type
                        </td>
                           <td>
                             <asp:TextBox ID="txtPolicyType" runat="server" Width="150px" CssClass="TextBoxStyle"    ReadOnly="True"     ></asp:TextBox>

                           </td>
                         <td  class="lblCaption1"  style="height:25px;">
                           Policy No.
                        </td>
                           <td>
                             <asp:TextBox ID="txtPolicyNo" runat="server" Width="150px" CssClass="TextBoxStyle"   ReadOnly="True"    ></asp:TextBox>

                           </td>
                         
                     </tr>
                     <tr>
                          <td  class="lblCaption1"  style="height:25px;" valign="top">
                             Doctor Name
                         </td>
                          <td valign="top">
                             <asp:TextBox ID="txtDrName" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"    ></asp:TextBox>
                            
                          </td>
                     <td  class="lblCaption1"  style="height:25px;" valign="top">
                             Ordering Clinician  
                         </td>
                          <td valign="top">
                             <asp:TextBox ID="txtOrderingClinician" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"    ></asp:TextBox>
                            
                          </td>
                     </tr>
                     <tr>
                           <td  class="lblCaption1"  style="height:25px;" valign="top">Ward No
                                    </td>
                                    <td>
                                         <asp:TextBox ID="txtWardNo" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"    ></asp:TextBox>
                                           
                                    </td>
                                    <td class="lblCaption1">Room No
                                    </td>
                                    <td>
                                         <asp:TextBox ID="txtRoomNo" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"    ></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Bed No
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBedNo" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"    ></asp:TextBox>
                                    </td>
                     </tr>
                      <tr>
                                     <td  class="lblCaption1"  style="height:25px;" valign="top">DRG Code
                                    </td>
                                    <td>
                                         <asp:TextBox ID="txtDrgCode" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"  ></asp:TextBox>
                                           
                                    </td>
                                    <td class="lblCaption1">DRG Amount
                                    </td>
                                    <td>
                                       <asp:TextBox ID="txtDRGAmount" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"  ></asp:TextBox>
                                           
                                    </td>
                                    <td class="lblCaption1">Prior Authorization ID
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPriorAuthoID" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"  ></asp:TextBox>
                                            
                                    </td>
                     </tr>
                     
                                 <tr>
                                     <td  class="lblCaption1"  style="height:25px;" valign="top">Authorization Start
                                    </td>
                                    <td>
                                      
                                                 <asp:TextBox ID="TextBox4" runat="server" Width="150px" MaxLength="10" CssClass="TextBoxStyle"    ReadOnly="True"  ></asp:TextBox>
                                                     
                                    </td>
                                    <td class="lblCaption1">Authorization End
                                    </td>
                                    <td>
                                        
                                                 <asp:TextBox ID="TextBox5" runat="server" Width="150px" MaxLength="10" CssClass="TextBoxStyle"    ReadOnly="True"  ></asp:TextBox>
                                                    
                                    </td>
                                    <td class="lblCaption1">Lenth of Stay
                                    </td>
                                    <td>
                                         <asp:TextBox ID="txtLenthOfStay" runat="server" Width="150px" CssClass="TextBoxStyle"     ReadOnly="True"   onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                             
                                    </td>
                                </tr>

                         <tr>

                             <td colspan="2" >
                             <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgFront" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                         </td>
                         <td colspan="2" >

                             <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgBack" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                         </td>
                             <td colspan="2" valign="top">
                                 <asp:Button ID="btnCardPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 120px;" CssClass="button gray small" OnClick="btnCardPrint_Click" Text="Ins. Card Download"  />

                             </td>
                       </tr>
                     

                         
                     

                  </table>
            </contenttemplate>
        </asp:TabPanel>
       <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Medical Records " Width="100%">
      <contenttemplate>

            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td >
                        <span class="lblCaption1" >Visit Details</span>
                    </td>
                </tr>
            </table>  
          <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1" width="100px">
                        From Date: 
                    </td>
                    <td style="width: 400px" >
                            <asp:TextBox ID="txtEMR_PTMaster_FromDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                Enabled="True" TargetControlID="txtEMR_PTMaster_FromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtEMR_PTMaster_FromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            &nbsp;

 
                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                                Text="To"></asp:Label>

                            <asp:TextBox ID="txtEMR_PTMaster_ToDate" runat="server" Width="75px" MaxLength="10" CssClass="TextBoxStyle"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                Enabled="True" TargetControlID="txtEMR_PTMaster_ToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                             <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtEMR_PTMaster_ToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            &nbsp;
                         <asp:Button ID="btnEMR_PTMasterRefresh" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button red small"
                            OnClick="btnEMR_PTMasterRefresh_Click" Text="Refresh"   />
                        </td>
                    
               </tr>
            </table>
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvEMR_PTMaster" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                             <HeaderStyle CssClass="GridHeader_Blue" Height="20px" Font-Bold="true" />
                             <RowStyle CssClass="GridRow" Height="20px" />
                                <Columns>
                                   
                                <asp:TemplateField HeaderText="Emr ID">
                                    <ItemTemplate>
                                          <asp:LinkButton ID="LinkButton4" runat="server" OnClick="gvEMR_PTMasterSelect_Click" ToolTip="Clinical Summary"   >
                                                <asp:Label ID="lblGVEmrDeptName" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_DEP_NAME") %>' visible="false"  ></asp:Label>
                                            <asp:Label ID="lblgvEMR_PTMaster_Emr_ID" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_ID") %>'  ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                          <asp:LinkButton ID="LinkButton5" runat="server" OnClick="gvEMR_PTMasterSelect_Click"  ToolTip="Clinical Summary" >
                                            <asp:Label ID="lblgvEMR_PTMaster_Date" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_DATEDesc") %>'  ></asp:Label>
                                              </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Doctor">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton6" runat="server" OnClick="gvEMR_PTMasterSelect_Click" ToolTip="Clinical Summary"  >
                                            <asp:Label ID="lblgvEMR_PTMaster_DrCode" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_DR_CODE") %>'  visible="false" ></asp:Label>
                                            <asp:Label ID="lblBMI" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_DR_NAME") %>'  ></asp:Label>
                                        </asp:LinkButton> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton7" runat="server" OnClick="gvEMR_PTMasterSelect_Click"  ToolTip="Clinical Summary" >
                                          <asp:Label ID="lblgvEMR_PTMaster_InsCode" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_INS_CODE") %>'  visible="false"></asp:Label>
                                            <asp:Label ID="Label1" CssClass="GridRow"   runat="server" Text='<%# Bind("EPM_STATUES") %>'  ></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                              

    </asp:gridview>

                        <asp:gridview id="gvIP_PTMaster" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                             <HeaderStyle CssClass="GridHeader_Blue" Height="20px" Font-Bold="true" />
                             <RowStyle CssClass="GridRow" Height="20px" />
                                <Columns>
                                   
                                <asp:TemplateField HeaderText="Emr ID">
                                    <ItemTemplate>
                                          <asp:LinkButton ID="LinkButton27" runat="server" OnClick="gvIP_PTMasterSelect_Click" ToolTip="Clinical Summary"   >
                                                <asp:Label ID="Label48" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_DEP_NAME") %>' visible="false"  ></asp:Label>
                                            <asp:Label ID="lblgvIP_PTMaster_IP_ID" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_ID") %>'  ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                          <asp:LinkButton ID="LinkButton29" runat="server" OnClick="gvIP_PTMasterSelect_Click"  ToolTip="Clinical Summary" >
                                            <asp:Label ID="Label54" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_DATEDesc") %>'  ></asp:Label>
                                              </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Doctor">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton30" runat="server" OnClick="gvIP_PTMasterSelect_Click" ToolTip="Clinical Summary"  >
                                            <asp:Label ID="lblgvIP_PTMaster_DrCode" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_DR_CODE") %>'  visible="false" ></asp:Label>
                                            <asp:Label ID="Label56" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_DR_NAME") %>'  ></asp:Label>
                                        </asp:LinkButton> 
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="LinkButton31" runat="server" OnClick="gvIP_PTMasterSelect_Click"  ToolTip="Clinical Summary" >
                                          <asp:Label ID="Label57" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_INS_CODE") %>'  visible="false"></asp:Label>
                                            <asp:Label ID="Label58" CssClass="GridRow"   runat="server" Text='<%# Bind("IPM_STATUS") %>'  ></asp:Label>
                                         </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                              

    </asp:gridview>
                    </td>
                </tr>
            </table>
       
           
        </contenttemplate>
 </asp:TabPanel>
 
 <asp:TabPanel runat="server" ID="TabPanel9" HeaderText=" Results " Width="100%">
      <contenttemplate>
            
           <table width="100%"    >
               <tr>
                                      <td class="lblCaption"  style="width:50%;" >
                                     
                                           <asp:Label ID="Label22" CssClass="label" runat="server" Text=" Laboratory Result"  ></asp:Label>
                                      </td>
                                      <td class="lblCaption" style="width:50%;" >

                                     <asp:Label ID="Label3" CssClass="label" runat="server" Text=" Radiology Result"  ></asp:Label>
                                      </td>
                                  </tr>
                                 
                 <tr>
                                       <td style="width:50%;" >
                                      <div style="padding-top: 0px; width: 100%; height: 200px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                                         <asp:GridView ID="gvLabResult" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                           <HeaderStyle CssClass="GridHeader_Blue" Height="20px" />
                                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                                            <Columns>
                                                 <asp:TemplateField HeaderText="Select" visible="false" HeaderStyle-Width="50" ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                            <asp:CheckBox ID="chkLabResult" runat="server" CssClass="label"    />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Download" HeaderStyle-Width="70px">
                                                    <ItemTemplate>
                                                          <asp:LinkButton ID="LinkButton14" runat="server" OnClick="SelectLabDownload_Click">
                                                            <asp:Label ID="Label25" CssClass="GridRow" runat="server" Text="Download"  ></asp:Label>
                                                          </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TransNo" HeaderStyle-Width="110px"   visible="false" >
                                                    <ItemTemplate>
                                                          <asp:LinkButton ID="lnkReport" runat="server" OnClick="SelectLabPrint_Click">
                                                            <asp:Label ID="lblReportType" CssClass="GridRow" runat="server" Text='<%# Bind("LTRM_REPORT_TYPE") %>' visible="false"  ></asp:Label>
                                                            <asp:Label ID="lblLabTransNo" CssClass="GridRow" runat="server" Text='<%# Bind("LTRM_TEST_REPORT_ID") %>'  ></asp:Label>
                                                          </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Caption" HeaderStyle-Width="110px">
                                                    <ItemTemplate>
                                                          <asp:LinkButton ID="lnkCaption" runat="server" OnClick="SelectLabPrint_Click">
                                                            <asp:Label ID="lblCaption" CssClass="GridRow" runat="server" Text='<%# Bind("LTRM_CAPTION") %>'  ></asp:Label>
                                                          </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                         <asp:LinkButton ID="LinkButton9" runat="server" OnClick="SelectLabPrint_Click">
                                                            <asp:Label ID="Label17" CssClass="GridRow" runat="server" Text='<%# Bind("LTRM_TEST_DATEDesc") %>'></asp:Label>
                                                             </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               
                                                  <asp:TemplateField HeaderText="Doctor Name">
                                                    <ItemTemplate>
                                                         <asp:LinkButton ID="LinkButton10" runat="server" OnClick="SelectLabPrint_Click">
                                                            <asp:Label ID="Label18" CssClass="GridRow" runat="server" Text='<%# Bind("LTRM_DR_NAME") %>'></asp:Label>
                                                             </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                                          </div>
                                   </td>
                                       <td style="width:50%;" >
                                             <div style="padding-top: 0px; width: 100%; height: 200px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                                              <asp:GridView ID="gvRadResult" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="100%">
                                                            <HeaderStyle CssClass="GridHeader" Height="20px" />
                                                             <RowStyle CssClass="GridRow" Height="20px" />
                            
                                                            <Columns>
                                                                  <asp:TemplateField visible="false" HeaderText="Select" HeaderStyle-Width="50" ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkRadResult" runat="server" CssClass="label"    />
                                                                                    </ItemTemplate>
                                                                   </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Download"  HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                          <asp:LinkButton ID="LinkButton15" runat="server" OnClick="SelectRadDownload_Click">
                                                                            <asp:Label ID="Label30" CssClass="GridRow" runat="server" Text="Download"  ></asp:Label>
                                                                          </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TransNo" HeaderStyle-Width="110px" visible="false">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="LinkButton8" runat="server" OnClick="SelectRadPrint_Click">
                                                                            <asp:Label ID="lblRadTransNo" CssClass="GridRow" runat="server" Text='<%# Bind("RTR_TRANS_NO") %>'  ></asp:Label>
                                                                         </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                                                          <asp:LinkButton ID="lnkgvRadHistDesc" CssClass="lblCaption1"  runat="server"  OnClick="SelectRadPrint_Click"   >
                                                                            <asp:Label ID="lblgvRadResultDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_RAD_DESCRIPTION") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                              
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton11" runat="server" OnClick="SelectRadPrint_Click">
                                                                            <asp:Label ID="Label20" CssClass="GridRow" runat="server" Text='<%# Bind("RTR_DATEDesc") %>'></asp:Label>
                                                                         </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                               
                                                                  <asp:TemplateField HeaderText="Doctor Name">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="LinkButton12" runat="server" OnClick="SelectRadPrint_Click">
                                                                            <asp:Label ID="Label21" CssClass="GridRow" runat="server" Text='<%# Bind("RTR_DR_NAME") %>'></asp:Label>
                                                                             </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                               
                                                    </Columns>

                                                </asp:GridView>
                                                 </div>
                                              </td>

                               </tr>
            </table>
              
         </contenttemplate>
     </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel15" HeaderText=" Referral " Width="100%">
      <contenttemplate>
           <asp:GridView ID="gvReferral" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="100%">
                                                            <HeaderStyle CssClass="GridHeader" Height="20px" />
                                                             <RowStyle CssClass="GridRow" Height="20px" />
                            
                                                            <Columns>
                                                                 
                                                                
                                                                <asp:TemplateField HeaderText="Type" HeaderStyle-Width="110px" visible="false">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="LinkButton35" runat="server" OnClick="SelectRefPrint_Click">
                                                                              <asp:Label ID="lblgvReferralEMRID" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_ID") %>' visible="false"  ></asp:Label>
                                                                            <asp:Label ID="lblgvReferralType" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_REFTYPE") %>'  ></asp:Label>
                                                                         </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="In Dr.">
                                                                    <ItemTemplate>
                                                                          <asp:LinkButton ID="LinkButton36" CssClass="lblCaption1"  runat="server"  OnClick="SelectRefPrint_Click"   >
                                                                            <asp:Label ID="Label59" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPR_DR_CODE") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                              
                                                                <asp:TemplateField HeaderText="Out Dr.">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton37" runat="server" OnClick="SelectRefPrint_Click">
                                                                            <asp:Label ID="Label60" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_OUT_DRNAME") %>'></asp:Label>
                                                                         </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                               
                                                                  <asp:TemplateField HeaderText="Hospital Name">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="LinkButton38" runat="server" OnClick="SelectRefPrint_Click">
                                                                            <asp:Label ID="Label61" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_OUT_HOSPITAL") %>'></asp:Label>
                                                                             </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Reason for Referral">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="LinkButton34" runat="server" OnClick="SelectRefPrint_Click">
                                                                            <asp:Label ID="Label53" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_REMARKS") %>'></asp:Label>
                                                                             </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Provisional Diagnosis">
                                                                    <ItemTemplate>
                                                                         <asp:LinkButton ID="LinkButton39" runat="server" OnClick="SelectRefPrint_Click">
                                                                            <asp:Label ID="Label62" CssClass="GridRow" runat="server" Text='<%# Bind("EPR_PRO_DIAG") %>'></asp:Label>
                                                                             </asp:LinkButton>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                    </Columns>

                                                </asp:GridView>
    </contenttemplate>
     </asp:TabPanel>


     <asp:TabPanel runat="server" ID="TabPanel10" HeaderText="EMR Review Sheet" Width="100%">
          <contenttemplate>
           <table width="100%">
                                    <tr>
                                        <td valign="top" style="width:45%">
                                         <table width="100%" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                         <tr>
                                              <td class="lblCaption" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                               Final Diagnosis
                                              </td>
                                          </tr>
                                           <tr>
                                            <td style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                                <asp:label id="lblFinalDiag" cssclass="label" runat="server"></asp:label>
                                            </td>
                                        </tr>
                                         <tr>
                                              <td class="lblCaption" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                              Plan
                                              </td>
                                          </tr>
                                        <tr>
                                            <td style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                                <asp:label id="lblPlan" cssclass="label" runat="server"></asp:label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <asp:GridView ID="gvCPT" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                           <HeaderStyle CssClass="GridHeader" Height="10px" />
                                           <RowStyle CssClass="GridRow" Height="10px" />
                            
                                            <Columns>
                                              
                                                <asp:TemplateField HeaderText="EM" HeaderStyle-Width="110px" >
                                                    <ItemTemplate>
                                                            <asp:Label ID="lblLabReqEPRID" CssClass="label" runat="server" Text='<%# Bind("CPTCode") %>'  ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="200px">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label27" CssClass="label" runat="server" Text='<%# Bind("Description") %>'  ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fee">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label28" CssClass="label" runat="server" Text='<%# Bind("Fee") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Claim Fee" >
                                                    <ItemTemplate>
                                        
                                                            <asp:Label ID="Label29" CssClass="label" runat="server" Text='<%# Bind("ClaimFee") %>'  ></asp:Label>
                                        
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                 
                                    </Columns>
                                </asp:GridView>
                                            </td>
                                        </tr>
                                        </table>
                                      
                                        </td>
                                        <td style="width:5%;">

                                        </td>
                                        <td valign="top" style="width:50%">
                                            <table width="100%">
                                            <tr>
                                                <td class="lblCaption"  colspan="2" >
                                                    EM Result Summary
                                                </td>
                                            </tr>
                                             <tr>
                                                <td  colspan="2"  style="height:30px;">
                                                        <span class="lblCaption BoldStyle" >(1) History</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                  <td class="lblCaption1" style="height:20px;width:300px;" >
                                                      Level of HPI
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblHPIResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                              </tr>
                                             <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                                     No. of ROS
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblROSResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                                   No. of PFSH
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblPFSHResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                                 Level of History Documented
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblFinalHistoryResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                              </tr>

                                              <tr>
                                                 <td  colspan="2"  style="height:30px; ">
                                                        <span class="lblCaption BoldStyle">(2) Examination Type</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                                   Examination
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblPEResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                             </tr>
                                             <tr>
                                                 <td  colspan="2"  style="height:30px;">
                                                        <span class="lblCaption BoldStyle" >(3) Medical Decision Making</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                                  Problem Point Scored
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblPSResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                             </tr>
                                            <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                                  Data Point Scored
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblDPResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                             </tr>
                                              <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                               Risk Management
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblRiskResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td class="lblCaption1" style="height:20px;" >
                                              Final MDM
                                                  </td>
                                                  <td>
                                                       <asp:label id="lblMDMResult" cssclass="lblCaption1" runat="server"></asp:label>
                                                  </td>
                                             </tr>

                                            <tr>
                                                 <td  colspan="2"  style="height:30px; ">
                                                        <span class="lblCaption BoldStyle" >(4) Selected CPT Code</span>
                                                </td>
                                            </tr>
                                            
                                                <tr>
                                                    <td class="lblCaption1" style="height:20px;" >
                                                        E & M Code
                                                    </td>
                                                    <td style="height:50px;" >
                                                         <asp:textbox id="txtENMCode" runat="server" cssclass="TextBoxStyle"  width="100px" height="22PX" maxlength="10"  ></asp:textbox>

                                                    </td>
                                                </tr>
                                             </table>
                                        </td>
                                    </tr>
               <tr>
                   
               </tr>
           </table>
                      
          </contenttemplate>
     </asp:TabPanel> 
  <asp:TabPanel runat="server" ID="TabPanel11" HeaderText=" Invoice" Width="100%">
      <contenttemplate>
            <asp:tabcontainer id="TabContainer3" runat="server" activetabindex="0" cssclass="AjaxTabStyle" width="100%">
                 <asp:TabPanel runat="server" ID="TabPanel13" HeaderText="Diagnosis" Width="100%">
                <contenttemplate>
                     <table>
                          <tr>
                              <td  class="lblCaption">
                                  Diagnosis <span style="color:red;">* </span>
                              </td>
                                <td>
                                <asp:Button ID="btnInvoicePrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 120px;" CssClass="button gray small" OnClick="btnInvoicePrint_Click" Text="Invoice Download"  />

                              </td>
                              
                  
                 
                          </tr>
                          <tr>
                              <td>
                                 
                                  <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" CssClass="TextBoxStyle"      onblur="return DiagIdSelected()"></asp:TextBox>
                              </td>
                               <td>
                                   <asp:TextBox ID="txtDiagName" runat="server" Width="400px" CssClass="TextBoxStyle"       onblur="return DiagNameSelected()"></asp:TextBox>
                     
                                    <div ID="divDiagExt" style="visibility:hidden;"></div>
                                    <asp:autocompleteextender id="AutoCompleteExtender7" runat="Server" targetcontrolid="txtDiagCode" minimumprefixlength="1" servicemethod="GetDiagnosisID"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divDiagExt"    ></asp:autocompleteextender>
                                    <asp:autocompleteextender id="AutoCompleteExtender8" runat="Server" targetcontrolid="txtDiagName" minimumprefixlength="1" servicemethod="GetDiagnosisName"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divDiagExt"    ></asp:autocompleteextender>
                      

                                 </td>
                               
                              <td>
                                   <asp:Button ID="btnInvDiagAdd" runat="server" Style="padding-left: 2px; padding-right: 2px;  width: 50px;" CssClass="button gray small"
                                                                     OnClick="btnInvDiagAdd_Click" Text="Add"  OnClientClick="return DiagAddVal();"/>

                              </td>
                          </tr>
                           
                      </table>
                    <table width="100%">
                                            <tr>
                                                <td>
                                                     <asp:GridView ID="gvInvoiceClaims" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="99%" gridline="none">
                                                             <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                                                             <RowStyle CssClass="GridRow" Height="20px" />
                                                
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblICDCode" width="100px" CssClass="GridRow" runat="server" Text='<%# Bind("HEIC_ICD_CODE") %>'  ></asp:Label>
                                        
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblICDDesc" width="400px" CssClass="GridRow" runat="server" Text='<%# Bind("HEIC_ICD_DESC") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Primary">
                                                                    <ItemTemplate>
                                                                            <asp:Label ID="lblType" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIC_TYPE") %>' visible="false"></asp:Label>
                                                                            <asp:Label ID="lblStDate" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIC_STARTDATE") %>' visible="false"></asp:Label>
                                                                            <asp:Label ID="lblEndDate" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIC_ENDDATE") %>' visible="false"></asp:Label>
                                                                            <asp:Label ID="lblStType" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIC_STARTTYPE") %>' visible="false"></asp:Label>
                                                                            <asp:Label ID="lblEndType" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIC_ENDTYPE") %>' visible="false"></asp:Label>
                                                                            
                                                                            <asp:Label ID="lblPrimary" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIC_PRIMARY") %>' visible="false"></asp:Label>
                                                                            <asp:Label ID="Label26" width="70px" CssClass="GridRow" runat="server" Text='<%# Bind("HEIC_PRIMARYDesc") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                
                                                                  
                                                                


                                                    </Columns>
                                                </asp:GridView>
                                                </td>
                                            </tr>
                                           </table>
                </contenttemplate>
                 </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel12" HeaderText="Services" Width="100%">
                <contenttemplate>
                      <table>
                          <tr>
                              <td  class="lblCaption">
                                  Service <span style="color:red;">* </span>
                              </td>
                                <td>

                              </td>
                              <td class="lblCaption1">
                                                      Qty: <span style="color:red;">* </span>
                              </td>
                  
                 
                          </tr>
                          <tr>
                              <td>
                                  <input type="hidden" id="hidHaadID" runat="server" />
                                  <input type="hidden" id="hidCatID" runat="server" />
                                   <input type="hidden" id="hidCatType" runat="server" />
                                  <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label"  AutoPostBack="true"  OnTextChanged="txtServCode_TextChanged"    onblur="return ServIdSelected()"></asp:TextBox>
                              </td>
                               <td>
                                   <asp:TextBox ID="txtServName" runat="server" Width="400px" CssClass="label"       onblur="return ServNameSelected()"></asp:TextBox>
                     
                                    <div ID="divwidth" style="visibility:hidden;"></div>
                                    <asp:autocompleteextender id="AutoCompleteExtender5" runat="Server" targetcontrolid="txtServCode" minimumprefixlength="1" servicemethod="GetServiceID"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth"    ></asp:autocompleteextender>
                                    <asp:autocompleteextender id="AutoCompleteExtender6" runat="Server" targetcontrolid="txtServName" minimumprefixlength="1" servicemethod="GetServiceName"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth"    ></asp:autocompleteextender>
                      

                                 </td>
                               <td>
                                                <asp:TextBox  ID="txtQty" runat="server" Width="30px" height="19px"  CssClass="label" MaxLength="4" Text="1"     onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                       
                               </td>
                              <td>
                                   <asp:Button ID="btnServiceAdd" runat="server" Style="padding-left: 2px; padding-right: 2px;  width: 50px;" CssClass="button gray small"
                                                                     OnClick="btnServiceAdd_Click" Text="Add"  OnClientClick="return ServiceAddVal();"/>

                              </td>
                          </tr>
                           <tr>
                              <td class="lblCaption1" >
                                         Doctor 
                                  
                               </td>
                               <td colspan="2">
                                   <asp:dropdownlist id="drpDoctor" cssclass="TextBoxStyle"  runat="server" width="250px">
                                                                        </asp:dropdownlist>
                               </td>
                          </tr>
                      </table>
                      <div style="padding-top: 0px; width: 950px; height: 200px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%">
                                            <tr>
                                                <td>
                                                     <asp:GridView ID="gvInvoiceTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="99%" gridline="none">
                                                            <HeaderStyle CssClass="GridHeader_Gray" />
                                                            <RowStyle CssClass="GridRow" />
                                                
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblServCode" width="100px" CssClass="label" runat="server" Text='<%# Bind("HEIT_SERV_CODE") %>'  ></asp:Label>
                                        
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblServDesc" width="300px" CssClass="label" runat="server" Text='<%# Bind("HEIT_SERV_DESC") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Category">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblCatID" width="70px" CssClass="label" runat="server" Text='<%# Bind("HEIT_CAT_ID") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Fee">
                                                                    <ItemTemplate>
                                        
                                                                             <asp:TextBox ID="txtFee" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_FEE") %>' ></asp:TextBox>

                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:TextBox ID="txtgvQty" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="30px"  Height="30px"  Text='<%# Bind("HEIT_QTY") %>' ></asp:TextBox>

                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                          <asp:TextBox ID="txtHitAmount" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_AMOUNT") %>' ></asp:TextBox>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                 <asp:TemplateField HeaderText="Claim" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                        
                                                                        <asp:TextBox ID="txtNet" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_NET_AMOUNT") %>' ></asp:TextBox>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                   
                                                                 <asp:TemplateField HeaderText="PT. Amount" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtPTAmount" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_PT_AMOUNT") %>' ></asp:TextBox>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Dr.Name">
                                                                    <ItemTemplate>
                                                                            <asp:Label ID="lblDRCode" CssClass="label" runat="server" Text='<%# Bind("HEIT_DR_CODE") %>' VISIBLE="FALSE"></asp:Label>
                                                                            <asp:Label ID="lblDRName" width="250px" CssClass="label" runat="server" Text='<%# Bind("HEIT_DR_NAME") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Haad Code">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblHaadCode" width="100px" CssClass="label" runat="server" Text='<%# Bind("HEIT_HAAD_CODE") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Authorization ID" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtAuthID" runat="server" style="border:none;" Width="100px"  Height="30px"  Text='<%# Bind("HEIT_AUTHORIZATIONID") %>' ></asp:TextBox>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Observ.Type" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtObsType" runat="server" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_OBS_TYPE") %>' ></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Observ.Code" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtObsCode" runat="server" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_OBS_CODE") %>' ></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Observ.Value" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtObsValue" runat="server" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_OBS_VALUE") %>' ></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Observ.ValueType" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtObsValueType" runat="server" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HEIT_OBS_VALUE_TYPE") %>' ></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                                </td>
                                            </tr>
                                           </table>
                            </div>
                    <br />
                    <table Width="100%">
                        <tr>
                            <td class="lblCaption1" style="height:30px;">
                                Bill Amount:
                               <asp:TextBox ID="txtBillAmt" runat="server" Width="100px" CssClass="label"  ></asp:TextBox>
                                &nbsp;&nbsp;
                                Claim  Amount:
                                <asp:TextBox ID="txtClaimAmt" runat="server" Width="100px" CssClass="label"  ></asp:TextBox>
                               &nbsp;&nbsp; Hosp.Disc
                               <asp:TextBox ID="txtHospDisc" runat="server" Width="100px" CssClass="label"  ></asp:TextBox>

                             </td>
                        </tr>
                        <tr>
                             <td class="lblCaption1" style="height:30px;">
                                 Deductible :
                                  <asp:TextBox ID="txtDedAmt" runat="server" Width="100px" CssClass="label"  ></asp:TextBox>
                               &nbsp;&nbsp; Co-Ins.Total:
                                  <asp:TextBox ID="txtCoInsAmt" runat="server" Width="100px" CssClass="label"  ></asp:TextBox>
                               &nbsp;&nbsp; Spl.Disc
                                  <asp:TextBox ID="txtSplDisc" runat="server" Width="100px" CssClass="label"  ></asp:TextBox>
                            
                            </td>
                        </tr>
                        <tr>
                            <td style="height:50PX;"></td>
                        </tr>
                       
                    </table>
                </contenttemplate>
                 </asp:TabPanel>
            </asp:tabcontainer>

      </contenttemplate>
 </asp:TabPanel> 
<asp:TabPanel runat="server" ID="TabPanel14" HeaderText="Attachments" Width="50%">
         <contenttemplate>
              
             <table style="width:100%">
                 <tr>
                     <td style="width:90%">
                          <div style="padding-top: 0px; width: 450px; height: 200px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <asp:GridView ID="gvAttachments" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    EnableModelValidation="True" Width="100%">
                                                                   <HeaderStyle CssClass="GridHeader_Gray" Height="10px" />
                                                                   <RowStyle CssClass="GridRow" Height="10px" />
                                          <Columns>
                                              
                                             <asp:TemplateField HeaderText="Date" HeaderStyle-Width="110px" >
                                                   <ItemTemplate>
                                                       <asp:LinkButton ID="LinkButton17" runat="server" OnClick="SelectAttachment_Click">
                                                           <asp:Label ID="lblgvAttaEMRID" CssClass="label" runat="server" Text='<%# Bind("ESF_EMR_ID") %>' visible="false"  ></asp:Label>
                                                             <asp:Label ID="lblgvAttaPTID" CssClass="label" runat="server" Text='<%# Bind("ESF_PT_ID") %>' visible="false"  ></asp:Label>
                                                       <asp:Label ID="lblgvAttaDate" CssClass="label" runat="server" Text='<%# Bind("ESF_CREATED_DATEDesc") %>'  ></asp:Label>
                                                      </asp:LinkButton>
                                                    </ItemTemplate>
                                             </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Category" HeaderStyle-Width="200px">
                                                   <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton18" runat="server" OnClick="SelectAttachment_Click">
                                                          <asp:Label ID="lblgvAttaCategory" CssClass="label" runat="server" Text='<%# Bind("ESF_CAT_ID") %>'  ></asp:Label>

                                                        </asp:LinkButton>
                                                   </ItemTemplate>
                                             </asp:TemplateField>
                                              <asp:TemplateField HeaderText="File Name">
                                                    <ItemTemplate>
                                                     <asp:LinkButton ID="LinkButton19" runat="server" OnClick="SelectAttachment_Click">
                                                          <asp:Label ID="lblgvAttaFileName" CssClass="label" runat="server" Text='<%# Bind("ESF_FILENAME") %>'></asp:Label>
                                                     </asp:LinkButton>
                                                   </ItemTemplate>
                                              </asp:TemplateField>
                    
                                                 
                                            </Columns>
                                        </asp:GridView>
                               </div>
                     </td>
                     
                       <td style="width:90%">
                            
                       </td>
                 </tr>
             </table>
             
         </contenttemplate>
 </asp:TabPanel>

 <asp:TabPanel runat="server" ID="TabPanel2" HeaderText=" Visit Details " Width="100%">
      <contenttemplate>
          <br />
            <asp:tabcontainer id="TabContainer2" runat="server" activetabindex="0" cssclass="AjaxTabStyle" width="100%">
                <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Diagnosis" Width="100%">
                <contenttemplate>
                <table width="100%">
                     
                      <tr>
                         
                        <td valign="top">
                            <input type="hidden" id="hidServCode" runat="server" />
                            <input type="hidden" id="hidServDesc" runat="server" />
                        <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblDiagCode" CssClass="label" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblDiagName" CssClass="label" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                    </Columns>

                </asp:GridView>

                             <asp:GridView ID="gvIPDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label33" CssClass="label" runat="server" Text='<%# Bind("IPD_DIAG_CODE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label34" CssClass="label" runat="server" Text='<%# Bind("IPD_DIAG_NAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>

                </table>
                </contenttemplate>
                 </asp:TabPanel>
                  <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="Procedures" Width="100%">
                <contenttemplate>
                 <table width="100%">
                   
                      <tr>
                       
                        <td valign="top">
                          
                        <asp:GridView ID="gvProcedure" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True"  Width="99%">
                             <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                           
                                            <asp:Label ID="lblDiagCode" CssClass="label" runat="server" Text='<%# Bind("EPP_DIAG_CODE") %>'  ></asp:Label>
                                          
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblDiagName" CssClass="label" runat="server" Text='<%# Bind("EPP_DIAG_NAME") %>'></asp:Label>
                                             
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblQTY" CssClass="label" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>
                                            
                                    </ItemTemplate>

                                </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Price">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblPrice" CssClass="label" runat="server" Text='<%# Bind("EPP_PRICE") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 
                                
                    </Columns>

                </asp:GridView>

                            <asp:GridView ID="gvIPProcedure" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True"  Width="99%">
                             <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                           
                                            <asp:Label ID="Label35" CssClass="label" runat="server" Text='<%# Bind("IPP_PRO_CODE") %>'  ></asp:Label>
                                          
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="Label36" CssClass="label" runat="server" Text='<%# Bind("IPP_PRO_NAME") %>'></asp:Label>
                                             
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="Label37" CssClass="label" runat="server" Text='<%# Bind("IPP_QTY") %>'></asp:Label>
                                            
                                    </ItemTemplate>

                                </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Price">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="Label38" CssClass="label" runat="server" Text='<%# Bind("IPP_COST") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 
                                
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>

                </table>
                  </contenttemplate>
                 </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel5" HeaderText="Radiology" Width="100%">
                         <contenttemplate>
                   <table width="100%">
                       
                      <tr>
                        
                        <td valign="top">
                            <input type="hidden" id="hidRadCode" runat="server" />
                            <input type="hidden" id="hidRadDesc" runat="server" />
                        <asp:GridView ID="gvRadiology" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True"  Width="99%">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblDiagCode" CssClass="label" runat="server" Text='<%# Bind("EPR_RAD_CODE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblDiagName" CssClass="label" runat="server" Text='<%# Bind("EPR_RAD_NAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                            <asp:Label ID="lblQTY" CssClass="label" runat="server" Text='<%# Bind("EPR_QTY") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                    </Columns>

                </asp:GridView>

                             <asp:GridView ID="gvIPRadiology" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True"  Width="99%">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label39" CssClass="label" runat="server" Text='<%# Bind("IPR_RAD_CODE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label40" CssClass="label" runat="server" Text='<%# Bind("IPR_RAD_NAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                            <asp:Label ID="Label41" CssClass="label" runat="server" Text='<%# Bind("IPR_QTY") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>

                  </table>
                         </contenttemplate>
                 </asp:TabPanel>
                   <asp:TabPanel runat="server" ID="TabPanel6" HeaderText="Laboratory" Width="100%">
                         <contenttemplate>
                  <table width="100%">
                       
                  <tr>
                        
                        <td valign="top">
                            <input type="hidden" id="hidLabCode" runat="server" />
                            <input type="hidden" id="hidLabDesc" runat="server" />
                        <asp:GridView ID="gvLaboratory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblDiagCode" CssClass="label" runat="server" Text='<%# Bind("EPL_LAB_CODE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblDiagName" CssClass="label" runat="server" Text='<%# Bind("EPL_LAB_NAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                            <asp:Label ID="lblQTY" CssClass="label" runat="server" Text='<%# Bind("EPL_QTY") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 
                    </Columns>

                </asp:GridView>

                            <asp:GridView ID="gvIPLaboratory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label42" CssClass="label" runat="server" Text='<%# Bind("IPL_LAB_CODE") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label43" CssClass="label" runat="server" Text='<%# Bind("IPL_LAB_NAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                            <asp:Label ID="Label44" CssClass="label" runat="server" Text='<%# Bind("IPL_QTY") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>
                  </table>
                         </contenttemplate>
                 </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel7" HeaderText="Pharmacy" Width="100%">
                         <contenttemplate>
                               <table width="100%">
                                <tr>
                                    <td>
                                         <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                EnableModelValidation="True" Width="99%" gridline="none">
                                                <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                                                <RowStyle CssClass="GridRow" Height="20px" />
                                                
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("EPP_PHY_CODE") %>'  ></asp:Label>
                                        
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label9" CssClass="label" runat="server" Text='<%# Bind("EPP_PHY_NAME") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Unit">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label10" CssClass="label" runat="server" Text='<%# Bind("EPP_UNIT") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Freq.">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label11" CssClass="label" runat="server" Text='<%# Bind("EPP_FREQUENCY") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Route">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label12" CssClass="label" runat="server" Text='<%# Bind("EPP_ROUTE") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Duration">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label13" CssClass="label" runat="server" Text='<%# Bind("EPP_DURATION") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Quantity">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label14" CssClass="label" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Refil">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label15" CssClass="label" runat="server" Text='<%# Bind("EPP_REFILL") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                          <asp:GridView ID="gvIPPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                EnableModelValidation="True" Width="99%" gridline="none">
                                                <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                                                <RowStyle CssClass="GridRow" Height="20px" />
                                                
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label45" CssClass="label" runat="server" Text='<%# Bind("IPP_PHY_CODE") %>'  ></asp:Label>
                                        
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label46" CssClass="label" runat="server" Text='<%# Bind("IPP_PHY_NAME") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Dosage">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label47" CssClass="label" runat="server" Text='<%# Bind("IPP_DOSAGE1") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                   
                                                     <asp:TemplateField HeaderText="Route">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label49" CssClass="label" runat="server" Text='<%# Bind("IPP_ROUTE") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Duration">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label50" CssClass="label" runat="server" Text='<%# Bind("IPP_DURATION") %>'></asp:Label>
                                                 <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_DURATION_TYPEDesc") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Quantity">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label51" CssClass="label" runat="server" Text='<%# Bind("IPP_QTY") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Refil">
                                                        <ItemTemplate>
                                        
                                                                <asp:Label ID="Label52" CssClass="label" runat="server" Text='<%# Bind("IPP_REFILL") %>'></asp:Label>
                                      
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </td>
                                </tr>
                               </table>
                        </contenttemplate>
                 </asp:TabPanel>
                 <asp:TabPanel runat="server" ID="TabPanel8" HeaderText="Others" Width="100%">
                         <contenttemplate>
                               <table width="100%" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                 <tr>
                                      <td class="lblCaption" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                        Treatment Plan
                                      </td>
                                  </tr>
                                <tr>
                                    <td style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                        <asp:label id="lblTreatmentPlan" cssclass="label" runat="server"></asp:label>
                                    </td>
                                </tr>
                                 <tr>
                                      <td class="lblCaption" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                       FollowupNotes
                                      </td>
                                  </tr>
                                <tr>
                                    <td style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                        <asp:label id="lblFollowupNotes" cssclass="label" runat="server"></asp:label>
                                    </td>
                                </tr>
                                    <tr>
                                      <td class="lblCaption" style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                      Nurseing Order
                                      </td>
                                  </tr>
                                <tr>
                                    <td style=" border-collapse: collapse; border: 1px solid #dcdcdc;height: 25px;">
                                         <table width="100%">
                       
                  <tr>
                        
                        <td valign="top">
                            
                        <asp:GridView ID="gvNursingInsttruction" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                             <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="EMR ID" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label4" CssClass="label" runat="server" Text='<%# Bind("EPC_ID") %>'  ></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label5" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>
                                      
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                            <asp:Label ID="Label16" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Username">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="Label19" CssClass="label" runat="server" Text='<%# Bind("EPC_USERNAME") %>'></asp:Label>
                                      
                                    </ItemTemplate>

                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Time">
                                    <ItemTemplate>
                                            <asp:Label ID="Label23" CssClass="label" runat="server" Text='<%# Bind("EPC_TIME") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>
                  </table>
                                    </td>
                                </tr>
                                </table
                         
                         </contenttemplate>
                 </asp:TabPanel>

            </asp:tabcontainer>
             


      </contenttemplate>
 </asp:TabPanel>      
 


</asp:tabcontainer>

          <br />
</div>
   
    </div>

 

 </asp:Content>