﻿ 


function ShowBillingRpt(Report, Date1, Date2, CompanyCode, DrId) {
    Report = Report + ".rpt";


    var Criteria = " 1=1 ";

    if (CompanyCode != "") {

        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BILLTOCODE}=\'' + CompanyCode + '\'';
    }

    if (DrId != "") {

        Criteria += ' AND {HMS_INVOICE_MASTER.HIM_DR_CODE}=\'' + DrId + '\'';
    }


    Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'' + Date2 + '\')'


    var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
 
}

function ShoweClaim(Report, Date1, Date2, CompanyCode, DrId) {
    Report = Report + ".rpt";

    var Criteria = " 1=1 ";

    if (CompanyCode != "") {

        Criteria += ' AND {HMS_ECLAIM_XML_DATA_MASTER.BillToCode}=\'' + CompanyCode + '\'';
    }

    if (DrId != "") {

        Criteria += ' AND {HMS_ECLAIM_XML_DATA_MASTER.DoctorCode}=\'' + DrId + '\'';
    }


    Criteria += ' AND  {HMS_ECLAIM_XML_DATA_MASTER.AuthRequestDate}>=date(\'' + Date1 + '\') AND  {HMS_ECLAIM_XML_DATA_MASTER.AuthRequestDate}<=date(\'' + Date2 + '\')'


    var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



}

function ShowClaimTransfer(Report, Date1, Date2, CompanyCode, DrId) {
    Report = Report + ".rpt";
    
    var Criteria = " 1=1 ";

    if (CompanyCode != "") {

        Criteria += ' AND {ECLAIM_XML_DATA_MASTER.BillToCode}=\'' + CompanyCode + '\'';
    }

    if (DrId != "") {

        Criteria += ' AND {ECLAIM_XML_DATA_MASTER.DoctorCode}=\'' + DrId + '\'';
    }


    Criteria += ' AND  {ECLAIM_XML_DATA_MASTER.AuthRequestDate}>=date(\'' + Date1 + '\') AND  {ECLAIM_XML_DATA_MASTER.AuthRequestDate}<=date(\'' + Date2 + '\')'


    var win = window.open('../../CReports/AccuReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



}