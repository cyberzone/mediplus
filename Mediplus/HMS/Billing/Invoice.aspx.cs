﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

using System.Globalization;

namespace Mediplus.HMS.Billing
{
    public partial class Invoice : System.Web.UI.Page
    {
        public static string strSessionBranchId, strSubCompanyID, strCompAgrmtType;

        public string strTest, MailContent = "";
        dboperations dbo = new dboperations();
        DataSet DS;
        public string strMessage = "";
        string JourNo = "";

        string HospDiscType = "", Co_Ins_Type = "", Deductible_Type, HSOM_ADV_PAYMENT_SERV_CODE, mDef_Serv_Disc_Type, mDef_Hosp_Disc_Type;
        decimal HospDiscAmt, Deductible, Co_Ins_Amt;
        Boolean Co_Incld_Hosp, Ded_Max_Amt, Ded_CoIns, SplCode, CoInsGrossAmt, CoInsGrossAmtClaimNetAmt, HMS_DEDUCTIBLE_BENEFIT_TYPE, HMS_COINS_PTREG;
        Boolean blnMultipleReg, Phy_duplicatefound, HMS_ENABLE_ADVANCE, HMS_SEP_INV_NO, HMS_INV_PRINTED, mMinistryService;
        Int32 invoicePrintPreview;
        string ReportName, strPrintType;

        #region  Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ProcessTimLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../ProcessTimLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void InvoiceSaveLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../InvoiceSaveLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            // Criteria += " AND SCREENNAME='ECLAIM' AND SEGMENT='ECLAIM_TYPE'";


            Criteria += " AND ( SCREENNAME='ECLAIM' OR   SCREENNAME='HMS' ) ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            ViewState["ECLAIM_TYPE"] = "AUH";
            ViewState["SALESTAX"] = "NO";

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "ECLAIM_TYPE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["ECLAIM_TYPE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "SALESTAXNEEDED")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SALESTAX"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "SMS_TYPE")
                    {
                        ViewState["SMS_TYPE"] = Convert.ToString(DR["CUST_VALUE"]);
                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "SMS_SenderID")
                    {
                        ViewState["SMS_SenderID"] = Convert.ToString(DR["CUST_VALUE"]);
                    }
                }

            }


        }

        void BindYear()
        {
            Int32 index = 0;
            for (int i = 1950; i <= 2030; i++)
            {

                drpYearOfOnset.Items.Insert(index, Convert.ToString(i));
                drpYearOfOnset.Items[index].Value = Convert.ToString(i);

                index = index + 1;
            }

            drpYearOfOnset.Items.Insert(0, Convert.ToString("--- Select ---"));
            drpYearOfOnset.Items[0].Value = Convert.ToString("");
        }

        void BindNurses()
        {
            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Nurse'";


            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_doctor_detailss(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpNurse.DataSource = DS;
                drpNurse.DataTextField = "FullName";
                drpNurse.DataValueField = "HSFM_STAFF_ID";
                drpNurse.DataBind();
            }
            drpNurse.Items.Insert(0, "--- Select ---");
            drpNurse.Items[0].Value = "";
        }

        void BindSMSTemplate()
        {
            string Criteria = " 1=1  AND HCM_STATUS='A' AND HCM_TYPE='SMS_TEMPLATES' ";


            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "HCM_ORDER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSMSTemplate.DataSource = DS;
                drpSMSTemplate.DataTextField = "HCM_DESC";
                drpSMSTemplate.DataValueField = "HCM_CODE";
                drpSMSTemplate.DataBind();
            }
            drpSMSTemplate.Items.Insert(0, "SMS Template");
            drpSMSTemplate.Items[0].Value = "";

            if (drpSMSTemplate.Items.Count == 2)
            {
                drpSMSTemplate.SelectedIndex = 1;
            }
        }
        void GetHaadName1(string HaadCode, out string strHaadName)
        {
            strHaadName = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HHS_CODE='" + HaadCode + "'";
            dboperations dbo = new dboperations();
            DataSet DSHaad = new DataSet();
            DSHaad = dbo.HaadServiceGet(Criteria, "", "1");

            if (DSHaad.Tables[0].Rows.Count > 0)
            {
                strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
            }

        }

        DataSet GetHaadName(string HaadCode)
        {

            string Criteria = " 1=1 ";
            Criteria += " AND HHS_CODE='" + HaadCode + "'";
            dboperations dbo = new dboperations();
            DataSet DSHaad = new DataSet();
            DSHaad = dbo.HaadServiceGet(Criteria, "", "1");


            return DSHaad;
        }


        DataSet GetServiceMasterName(string SearchType, string ServCode)
        {
            //ServID = "";
            //ServName = "";
            //HSM_TYPE = "";
            //CatID = "";
            //CatTyp = "";
            //Fee = 0;

            //obsCode="";
            //obsDesc="";
            //obsvalueType="";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            string CodeType = "";
            if (txtServType.Text.Trim() != "")
            {
                string strCodeType = txtServType.Text.Trim();

                switch (strCodeType)
                {
                    case "CPT":
                        CodeType = "3";
                        break;
                    case "DENTAL":
                        CodeType = "6";
                        break;
                    case "DRUG":
                        CodeType = "5";
                        break;
                    case "HCPCS":
                        CodeType = "4";
                        break;

                    case "SERVICE":
                        CodeType = "8";
                        break;

                }
            }

            if (CodeType != "")
            {
                Criteria += " AND  HSM_CODE_TYPE   ='" + CodeType + "'";
            }


            dboperations dbo = new dboperations();
            DataSet DSServ = new DataSet();
            //DSServ = dbo.ServiceMasterGet(Criteria);
            DSServ = dbo.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            txtServName.Text = "";
            if (DSServ.Tables[0].Rows.Count > 0)
            {
                //ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                //ServName = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                //HSM_TYPE = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);
                //CatID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                //CatTyp = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
                //if (DSServ.Tables[0].Rows[0].IsNull("HSM_CODE_TYPE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]) != "")
                //    Fee = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

                //  obsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                // obsDesc = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                // obsvalueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);
            }

            return DSServ;

        }

        string GetServiceMasterID(string SearchType, string ServCode)
        {
            string ServID = "";
            //ServName = "";
            //HSM_TYPE = "";
            //CatID = "";
            //CatTyp = "";
            //Fee = 0;

            //obsCode="";
            //obsDesc="";
            //obsvalueType="";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            dboperations dbo = new dboperations();
            DataSet DSServ = new DataSet();
            DSServ = dbo.ServiceMasterGet(Criteria);
            // txtServName.Text = "";
            if (DSServ.Tables[0].Rows.Count > 0)
            {
                if (SearchType == "HSM_SERV_ID")
                {
                    ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
                }
                else
                {
                    ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                }

            }

            return ServID;

        }

        void GetStaffName(string DrCode, out string DrName, out string DeptName)
        {
            DrName = "";
            DeptName = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_STAFF_ID='" + DrCode + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DataSet DSHaad = new DataSet();
            DSHaad = objStaff.GetStaffMaster(Criteria);

            if (DSHaad.Tables[0].Rows.Count > 0)
            {
                DrName = Convert.ToString(DSHaad.Tables[0].Rows[0]["FullName"]);
                DeptName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HSFM_DEPT_ID"]);

            }

        }

        void GetRefDrName(string DrCode, out string DrName)
        {

            string criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";
            criteria += " AND HSFM_STAFF_ID = '" + DrCode + "'";
            DataSet DS = new DataSet();
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DrName = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
                goto FunEnd;
            }

            DrName = "";
            criteria = " HRDM_REF_ID = '" + DrCode + "'";
            DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.RefDoctorMasterGet(criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DrName = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
            }

        FunEnd: ;
        }

        DataSet GetCompanyAgrDtls(string ServCode, string CompanyID)
        {



            DataSet DS = new DataSet();
            dboperations objDBO = new dboperations();

            string Criteria = " 1=1 ";
            Criteria += " AND HAS_SERV_ID='" + ServCode + "' AND HAS_COMP_ID='" + CompanyID + "'";
            DS = objDBO.AgrementServiceGet(Criteria);
            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    if (DS.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
            //    {
            //        ServPrice = Convert.ToDecimal(DS.Tables[0].Rows[0]["HAS_NETAMOUNT"]);
            //    }
            //}

            return DS;



        }

        void fnSetdefaults()
        {
            if (GlobalValues.FileDescription == "SMCH")
            {
                txtHospDisc.Enabled = false;
                drpHospDiscType.Enabled = false;
                chkNoMergePrint.Visible = true;
            }

            Phy_duplicatefound = false;
            HMS_ENABLE_ADVANCE = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "Y" ? true : false;
            invoicePrintPreview = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "Y" ? 1 : 0;

            if (HMS_ENABLE_ADVANCE == true)
            {
                //  HMS_ADV_SERV_CODE = Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]);
                if (Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Please set Advance Payment Service Code in System Options.');", true);

                }
                // CmdAdvanceDtls.Visible = True
            }

            drpPayType.Items.Insert(0, "Cash");
            drpPayType.Items[0].Value = "Cash";

            drpPayType.Items.Insert(1, "Credit Card");
            drpPayType.Items[1].Value = "Credit Card";

            drpPayType.Items.Insert(2, "Cheque");
            drpPayType.Items[2].Value = "Cheque";

            drpPayType.Items.Insert(3, "Debit Card");
            drpPayType.Items[3].Value = "Debit Card";
            
            drpPayType.Items.Insert(4, "Voucher");
            drpPayType.Items[4].Value = "Voucher";

            drpPayType.Items.Insert(5, "Online");
            drpPayType.Items[5].Value = "Online";

            ViewState["NewFlag"] = true;

            if (HMS_ENABLE_ADVANCE == true && Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]) != "")
            {
                drpPayType.Items.Insert(5, "Advance");
                drpPayType.Items[5].Value = "Advance";
            }

            if (GlobalValues.BLNALLOWADVANCEPAYMENT_INVOICE == true)
            {
                drpPayType.Items.Insert(6, "Utilise Advance");
                drpPayType.Items[6].Value = "Utilise Advance";
            }

            

            HMS_SEP_INV_NO = Convert.ToString(ViewState["HSOM_SEP_INV_NO"]) == "Y" ? true : false;

            HMS_INV_PRINTED = Convert.ToString(ViewState["HSOM_INV_PRINTED"]) == "Y" ? true : false;

            mDef_Serv_Disc_Type = "";
            mDef_Hosp_Disc_Type = "";


            mDef_Serv_Disc_Type = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "" ? "%" : Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]);// IIf(IsNull(Temprs.Fields(0).Value), "", IIf(Trim(Temprs.Fields(0).Value) = "", "%", Temprs.Fields(0).Value))
            mDef_Hosp_Disc_Type = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "" ? "%" : Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]); //IIf(IsNull(Temprs.Fields(1).Value), "", IIf(Trim(Temprs.Fields(1).Value) = "", "%", Temprs.Fields(1).Value))
            //lblhospdisctype.Caption = mDef_Hosp_Disc_Type

            dboperations objDbo = new dboperations();
            string Criteria = " 1=1 ";
            Criteria += " AND HSM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HSM_STATUS='A'";

            DataSet DSServ = new DataSet();
            DSServ = objDbo.ServiceMasterGet(Criteria);

            if (DSServ.Tables[0].Rows.Count <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Please set Advance Payment Service Code in System Options.');", true);

            }

            if (GlobalValues.FileDescription == "AEMC" || GlobalValues.FileDescription == "ALDAR" || GlobalValues.FileDescription == "KBR") mMinistryService = true; else mMinistryService = false;


            BIndCCType();
            TreatmentType();
            if (GlobalValues.FileDescription == "SMCH" || GlobalValues.FileDescription == "ALAMAL")
            {
                btnPrntDeductible.Visible = true;
                btnPrntDeductible.Text = "Merge Print";

                if (GlobalValues.FileDescription == "ALAMAL")
                {
                    drpRemarks.Visible = true;
                    BindRemarks();
                }
            }
            else if (GlobalValues.FileDescription == "FINECARE")
            {
                btnPrntDeductible.Visible = true;
                btnPrntDeductible.Text = "Expense";
            }
            else if (GlobalValues.FileDescription == "KQMC")
            {
                btnPrntDeductible.Visible = false;
            }
            else if (GlobalValues.FileDescription == "ALDAR")
            {
                drpRemarks.Visible = true;
            }
            else if (GlobalValues.FileDescription == "GRACE")
            {
                //Label20.Caption = "Trtmt Date"
            }
            else if (GlobalValues.FileDescription == "ALREEM")
            {
                drpCommissionType.Visible = true;

                lblInsCard.Text = "Comm.Type.";
                lblInsCard.Visible = true;
                // CmbCommissionType.Top = Me.CmbStatus.Top

            }

            if (GlobalValues.FileDescription == "BMC" || GlobalValues.FileDescription == "GMC")
            {
                //ChkPrintCreditBill.Visible = False
                //  ChkOnlySave.Visible = False
            }




        }

        void fnSetServices(string CompCategory, string CompID, string PolicyType)
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCB_COMP_ID='" + CompID + "' AND HCB_CAT_TYPE='" + PolicyType + "'";
            dboperations dbo = new dboperations();
            DS = dbo.CompBenefitsGet(Criteria);


            if (DS.Tables[0].Rows.Count <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Company Benefits are not Found for this company.');", true);
                HospDiscType = "";
                HospDiscAmt = 0;
                Deductible = 0;
                Co_Ins_Type = "%";
                Co_Ins_Amt = 100;
                Co_Incld_Hosp = false;
                Ded_Max_Amt = false;
                Ded_CoIns = false;
                CoInsGrossAmt = false;
                CoInsGrossAmtClaimNetAmt = false;
                Deductible_Type = "$";
                SplCode = false;
                HMS_DEDUCTIBLE_BENEFIT_TYPE = false;
            }
            else
            {
                HospDiscType = DS.Tables[0].Rows[0].IsNull("HCB_HOSP_DISC_TYPE") == true ? "" : Convert.ToString(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_TYPE"]);//IIf(IsNull(RSCompBfts.Fields("HCB_HOSP_DISC_TYPE").Value), "", RSCompBfts.Fields("HCB_HOSP_DISC_TYPE").Value)
                HospDiscAmt = DS.Tables[0].Rows[0].IsNull("HCB_HOSP_DISC_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_AMT"]);//IIf(IsNull(RSCompBfts.Fields("HCB_HOSP_DISC_AMT").Value), 0, RSCompBfts.Fields("HCB_HOSP_DISC_AMT").Value)
                Deductible_Type = DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE_TYPE") == true ? "$" : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_TYPE"]);// IIf(IsNull(RSCompBfts.Fields("HCB_DEDUCTIBLE_TYPE").Value), "$", RSCompBfts.Fields("HCB_DEDUCTIBLE_TYPE").Value)
                Deductible = DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]);//IIf(IsNull(RSCompBfts.Fields("HCB_DEDUCTIBLE").Value), 0, RSCompBfts.Fields("HCB_DEDUCTIBLE").Value)
                SplCode = DS.Tables[0].Rows[0].IsNull("HCB_SPL_CODE") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_SPL_CODE"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_SPL_CODE").Value), False, IIf(RSCompBfts.Fields("HCB_SPL_CODE").Value = "Y", True, False))

                Co_Incld_Hosp = DS.Tables[0].Rows[0].IsNull("HCB_CONSLT_INCL_HOSP_DISC") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_CONSLT_INCL_HOSP_DISC"]) == "Y" ? true : false;//  IIf(IsNull(RSCompBfts.Fields("HCB_CONSLT_INCL_HOSP_DISC").Value), False, IIf(RSCompBfts.Fields("HCB_CONSLT_INCL_HOSP_DISC").Value = "Y", True, False))
                Ded_Max_Amt = DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE_MAX_CONSLT_AMT") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_MAX_CONSLT_AMT"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_DEDUCTIBLE_MAX_CONSLT_AMT").Value), False, IIf(RSCompBfts.Fields("HCB_DEDUCTIBLE_MAX_CONSLT_AMT").Value = "Y", True, False))
                Ded_CoIns = DS.Tables[0].Rows[0].IsNull("HCB_DED_COINS_APPLICABLE") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_COINS_APPLICABLE"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_DED_COINS_APPLICABLE").Value), False, IIf(RSCompBfts.Fields("HCB_DED_COINS_APPLICABLE").Value = "Y", True, False))
                CoInsGrossAmt = DS.Tables[0].Rows[0].IsNull("HCB_COINS_FROM_GROSSAMT") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_FROM_GROSSAMT"]) == "Y" ? true : false; ;// IIf(IsNull(RSCompBfts.Fields("HCB_COINS_FROM_GROSSAMT").Value), False, IIf(RSCompBfts.Fields("HCB_COINS_FROM_GROSSAMT").Value = "Y", True, False))
                CoInsGrossAmtClaimNetAmt = DS.Tables[0].Rows[0].IsNull("HCB_COINSFROMGROSS_CLAIMFROMNET") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_COINSFROMGROSS_CLAIMFROMNET").Value), False, IIf(RSCompBfts.Fields("HCB_COINSFROMGROSS_CLAIMFROMNET").Value = "Y", True, False))
                hidInsTrtnt.Value = DS.Tables[0].Rows[0].IsNull("HCB_INS_TYPE_TREATMENT") == true ? "" : Convert.ToString(DS.Tables[0].Rows[0]["HCB_INS_TYPE_TREATMENT"]);// IIf(IsNull(RSCompBfts.Fields("HCB_INS_TYPE_TREATMENT").Value), "", RSCompBfts.Fields("HCB_INS_TYPE_TREATMENT").Value)
                HMS_DEDUCTIBLE_BENEFIT_TYPE = DS.Tables[0].Rows[0].IsNull("HCB_DED_INSURANCE_TYPE") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_INSURANCE_TYPE"]) == "Y" ? true : false;//  IIf(IsNull(RSCompBfts.Fields("HCB_DED_INSURANCE_TYPE").Value), False, IIf(RSCompBfts.Fields("HCB_DED_INSURANCE_TYPE").Value = "Y", True, False))

            }


            //lblhospdisctype.Caption = IIf(HospDiscType = "", "", IIf(HospDiscType = "%", "%", "$"))
            //TxtHospDisc.Text = Format(HospDiscAmt, HMS_CURRENCY_FORMAT)
            //'lblCoInsType.Caption = IIf(Co_Ins_Type = "", "", IIf(Co_Ins_Type = "P", "%", "$"))
            //'txtCon_insAmt.Text = Format(Co_Ins_Amt, HMS_CURRENCY_FORMAT)
            //If txtInsTrtnt <> "Service Type" Then
            //    If RsCompBftsDetails.State = adStateOpen Then RsCompBftsDetails.Close
            //    RsCompBftsDetails.CursorLocation = adUseClient
            //    RsCompBftsDetails.Open "select * from HMS_COMP_BENEFITS_DETAILS WHERE HCBD_BRANCH_ID ='" & HMS_BRANCH & "'  AND HCBD_COMP_ID='" & Trim(CompID) & "'  AND HCBD_CAT_TYPE='" & Trim(PolicyType) & "' and HCBD_CATEGORY_ID='" & InsTrntCol(CmbTrtntType.ListIndex + 1) & "' AND HCBD_CATEGORY_TYPE='C'", CnInvoice, adOpenStatic, adLockOptimistic
            //    If RsCompBftsDetails.BOF And RsCompBftsDetails.EOF Then
            //        If App.Title = "0" Then
            //            BCoAmt = 0
            //            BCoType = "%"
            //            BCovered = True
            //            Blimit = 0
            //        Else
            //            BCoAmt = 100
            //            BCoType = "%"
            //            BCovered = False
            //            Blimit = 0
            //            If Not HMS_COINS_PTREG Then MsgBox "Company Benefits Informations are not found. So this invoice cannot be claimed!", vbInformation, HMS_ErrorCaption
            //        End If
            //    Else
            //        BCoAmt = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_CO_INS_AMT").Value), 0, RsCompBftsDetails.Fields("HCBD_CO_INS_AMT").Value)
            //        BCoType = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_CO_INS_TYPE").Value), "%", RsCompBftsDetails.Fields("HCBD_CO_INS_TYPE").Value)
            //        BCovered = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_CATEGORY_TYPE").Value), "", IIf(RsCompBftsDetails.Fields("HCBD_CATEGORY_TYPE").Value = "C" Or RsCompBftsDetails.Fields("HCBD_CATEGORY_TYPE").Value = "P", True, False))
            //        Blimit = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_LIMIT").Value), 0, RsCompBftsDetails.Fields("HCBD_LIMIT").Value)
            //    End If
            //Else
            //        BCoAmt = 100
            //        BCoType = "%"
            //        BCovered = False
            //        Blimit = 0
            //End If
            //Co_Ins_Type = BCoType
            //Co_Ins_Amt = BCoAmt
            hidCoInsType.Value = Co_Ins_Type != "" ? Co_Ins_Type : "%";
            //txtCon_insAmt.Text = Format(BCoAmt, HMS_CURRENCY_FORMAT)
        }

        void BIndCCType()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            clsInvoice objInv = new clsInvoice();
            DS = objInv.CCTypeGet(Criteria);
            drpccType.Items.Clear();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpccType.DataSource = DS;
                drpccType.DataTextField = "HCT_CC_TYPE";
                drpccType.DataValueField = "HCT_CC_TYPE";
                drpccType.DataBind();
            }



        }

        void TreatmentType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HITT_STATUS=1 ";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HITT_NAME,HITT_ID", "HMS_INS_TRTMT_TYPE", Criteria, "HITT_NAME desc");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTreatmentType.DataSource = DS;
                drpTreatmentType.DataTextField = "HITT_NAME";
                drpTreatmentType.DataValueField = "HITT_ID";
                drpTreatmentType.DataBind();
            }
        }

        void GetJobnumber()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HPV_STATUS <>'D' ";
            Criteria += " AND HPV_PT_ID='" + txtFileNo.Text.Trim() + "' AND (HPV_DATE BETWEEN CONVERT(DATETIME, '" + txtInvDate.Text.Trim() + " 00:00:00', 103) AND CONVERT(DATETIME, '" + txtInvDate.Text.Trim() + " 23:59:59', 103))  And HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("isnull(HPV_JOBNUMBER,'')+ '|' + isnull(HPV_BUSINESSUNITCODE,'') HPV_JOBNUMBER ", "HMS_PATIENT_VISIT ", Criteria, "HPV_DATE DESC");

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strJobNo = Convert.ToString(DS.Tables[0].Rows[0]["HPV_JOBNUMBER"]);
                string[] arrJobNo = strJobNo.Split('|');
                if (arrJobNo.Length > 1)
                {
                    txtJobnumber.Text = arrJobNo[0];
                    txtJobnumber.ToolTip = arrJobNo[1];
                }

            }
        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HBM_BANKNAME", "HMS_BANK_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "HBM_BANKNAME";
                drpBank.DataValueField = "HBM_BANKNAME";
                drpBank.DataBind();
            }
        }


        void BindCurrency()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HCRM_ID,HCRM_NAME", "HMS_CURRENCY_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCurType.DataSource = DS;
                drpCurType.DataTextField = "HCRM_NAME";
                drpCurType.DataValueField = "HCRM_ID";
                drpCurType.DataBind();
            }
        }


        void fnListAdvanceDtls()
        {
            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            DS = objInv.AdvanceDtlsGet(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim());

            Int32 intCount = 0;

            if (DS.Tables[0].Rows.Count > 0)
            {
                intCount = DS.Tables[0].Rows.Count;
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string strData = "";

                    strData = Convert.ToString(DS.Tables[0].Rows[i]["HIO_REMARKS"]) + " - " + Convert.ToString(Convert.ToDecimal(DS.Tables[0].Rows[i]["Used"]) + Convert.ToDecimal(DS.Tables[0].Rows[i]["AdvAmt"]));
                    lstAdvDtls.Items.Insert(i, strData);
                    lstAdvDtls.Items[i].Value = strData;


                }

            }

            DataSet DS1 = new DataSet();

            DS1 = objInv.InvoiceAdvanceDtlsGet(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim(), txtInvoiceNo.Text.Trim());


            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                {
                    string strData = "";

                    strData = Convert.ToString(DS.Tables[0].Rows[j]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["HIM_DATE"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["AdvAmt"]);
                    lstAdvDtls.Items.Insert(intCount, strData);
                    lstAdvDtls.Items[intCount].Value = strData;
                    intCount = intCount + 1;
                }

            }



        }

        void BindRemarks()
        {

            drpRemarks.Items.Insert(0, "OP");
            drpRemarks.Items[0].Value = "OP";

            drpRemarks.Items.Insert(1, "NV");
            drpRemarks.Items[1].Value = "NV";

            drpRemarks.Items.Insert(2, "WI");
            drpRemarks.Items[2].Value = "WI";

            drpRemarks.Items.Insert(3, "FU");
            drpRemarks.Items[3].Value = "FU";

            drpRemarks.Items.Insert(4, "RV");
            drpRemarks.Items[4].Value = "RV";

        }

        void BindPlanType()
        {
            //drpPlanType.Items.Clear();

            //DataSet DS = new DataSet();
            //string Criteria = " 1=1 ";
            ////if (drpSrcCompany.Items.Count > 0)
            ////{
            ////    if (drpSrcCompany.SelectedItem.Text != "")
            ////    {

            ////        if (drpNetworkName.SelectedIndex != 0)
            ////        {
            ////            Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
            ////        }
            ////        else
            ////        {
            //Criteria += " AND HCB_COMP_ID = '" + txtSubCompanyID.Text.Trim() + "'";
            //// }
            //DS = dbo.CompBenefitsGet(Criteria);

            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    drpPlanType.DataSource = DS;
            //    drpPlanType.DataTextField = "HCB_CAT_TYPE";
            //    drpPlanType.DataValueField = "HCB_CAT_TYPE";
            //    drpPlanType.DataBind();
            //}

            //drpPlanType.Items.Insert(0, "--- All ---");
            //drpPlanType.Items[0].Value = "0";
            ////}
            //// }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);
            ViewState["HSOM_DR_MANUALSEELCT_INVOICE"] = "0";
            ViewState["HSOM_INV_PTWAITING_ONLY"] = "N";
            ViewState["HSOM_ENABLE_ADV_PAYMENT"] = "N";
            ViewState["HSOM_INVOICE_PREVIEW"] = "0";
            // ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]
            ViewState["HSOM_SEP_INV_NO"] = "N";
            ViewState["HSOM_INV_PRINTED"] = "N";
            ViewState["HSOM_DEFAULT_SERV_DISC_TYPE"] = "$";
            ViewState["HSOM_DEFAULT_HOSP_DISC_TYPE"] = "$";
            ViewState["HSOM_COSTPRICE_BILLING"] = "N";
            ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"] = "N";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_INV_PTWAITING_ONLY") == false)
                {
                    ViewState["HSOM_INV_PTWAITING_ONLY"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INV_PTWAITING_ONLY"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DR_MANUALSEELCT_INVOICE") == false)
                {
                    ViewState["HSOM_DR_MANUALSEELCT_INVOICE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DR_MANUALSEELCT_INVOICE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ENABLE_ADV_PAYMENT") == false)
                {
                    ViewState["HSOM_ENABLE_ADV_PAYMENT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ENABLE_ADV_PAYMENT"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_INVOICE_PREVIEW") == false)
                {
                    ViewState["HSOM_INVOICE_PREVIEW"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INVOICE_PREVIEW"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_ADV_PAYMENT_SERV_CODE") == false)
                {
                    ViewState["HSOM_ADV_PAYMENT_SERV_CODE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ADV_PAYMENT_SERV_CODE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_SEP_INV_NO") == false)
                {
                    ViewState["HSOM_SEP_INV_NO"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SEP_INV_NO"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_INV_PRINTED") == false)
                {
                    ViewState["HSOM_INV_PRINTED"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INV_PRINTED"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_SERV_DISC_TYPE") == false)
                {
                    ViewState["HSOM_DEFAULT_SERV_DISC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_SERV_DISC_TYPE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_HOSP_DISC_TYPE") == false)
                {
                    ViewState["HSOM_DEFAULT_HOSP_DISC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_HOSP_DISC_TYPE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_COSTPRICE_BILLING") == false)
                {
                    ViewState["HSOM_COSTPRICE_BILLING"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_COSTPRICE_BILLING"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_INVOICE_PAIDAMT_CHANGE") == false)
                {
                    ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INVOICE_PAIDAMT_CHANGE"]);

                }

            }

        }



        Boolean TopUpCardChecking(string FileNo)
        {
            DataSet DSInv = new DataSet();
            clsInvoice objInv = new clsInvoice();
            string Criteria = "1=1 ";
            Criteria += " AND HSC_PT_ID='" + FileNo + "' AND  HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' And HSC_CARD_NAME ='Topup' and (HSC_ISDEFAULT=1)";
            DSInv = dbo.ScanCardImageGet(Criteria);
            if (DSInv.Tables[0].Rows.Count > 0)
            {

                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_ID") == false)
                    ViewState["mTopupBillToCode"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_ID"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_ID") == false)
                    ViewState["mTopupInsCode"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_ID"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_NAME") == false)
                    ViewState["mTopupBillToCompName"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_NAME"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_NAME") == false)
                    ViewState["mTopupInsName"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_NAME"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_EXPIRY_DATE") == false)
                    ViewState["mTopupExpiry"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_EXPIRY_DATE"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_POLICY_NO") == false)
                    ViewState["mTopupPolicyNo"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_POLICY_NO"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_CARD_NO") == false)
                    ViewState["mTopupIDNo"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_CARD_NO"]);

                return true;

            }
            return false;
        }

        void IsPatientBilledToday()
        {
            DataSet DSInv = new DataSet();
            clsInvoice objInv = new clsInvoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  convert(varchar(10),HIM_DATE,103)  = '" + txtInvDate.Text.Trim() + "'";

            CommonBAL objCom = new CommonBAL();
            DSInv = objCom.fnGetFieldValue("TOP 5 HIM_INVOICE_ID,HIM_DR_NAME ", "HMS_INVOICE_MASTER", Criteria, "");

            //  DSInv = objInv.InvoiceMasterGet(Criteria);


            if (DSInv.Tables[0].Rows.Count > 0)
            {
                string strInvNo = "";

                for (int i = 0; i < DSInv.Tables[0].Rows.Count; i++)
                {
                    strInvNo += Convert.ToString(DSInv.Tables[0].Rows[i]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DSInv.Tables[0].Rows[i]["HIM_DR_NAME"]) + ", ";
                }
                if (strInvNo.Length > 1)
                {
                    strInvNo = strInvNo.Substring(0, strInvNo.Length - 1);
                }

                MPExtMessage.Show();
                strMessage = "This Patient is already billed today. Invoice Number(s) : " + strInvNo;

            }
        }

        Boolean IsPatientBilledSameDaySameDr(Boolean ShowMsg)
        {
            DataSet DSInv = new DataSet();
            clsInvoice objInv = new clsInvoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  convert(varchar(10),HIM_DATE,103)  = '" + txtInvDate.Text.Trim() + "'";
            Criteria += " AND  HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_ID !='" + txtInvoiceNo.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_ID !='T" + txtInvoiceNo.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_TYPE ='" + drpInvType.SelectedValue + "'";

            CommonBAL objCom = new CommonBAL();
            DSInv = objCom.fnGetFieldValue("TOP 1 HIM_INVOICE_ID,HIM_DR_NAME ", "HMS_INVOICE_MASTER", Criteria, "");

            // DSInv = objInv.InvoiceMasterGet(Criteria);
            //  System.DateTime.Now.Date.ToString("yyyy-MM-dd")

            if (DSInv.Tables[0].Rows.Count > 0)
            {
                string strInvNo = "";
                strInvNo += Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_DR_NAME"]) + ", ";
                if (ShowMsg == true)
                {
                    MPExtMessage.Show();
                    strMessage = "This Patient is already billed today for same Doctor . Invoice Number : " + strInvNo;
                }
                return true;


            }
            return false;
        }

        Boolean CheckDuplicateInvoice()
        {
            DataSet DSInv = new DataSet();
            clsInvoice objInv = new clsInvoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  convert(varchar(10),HIM_DATE,103)  = '" + txtInvDate.Text.Trim() + "'";
            Criteria += " AND  HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_TYPE ='" + drpInvType.SelectedValue + "'";

            CommonBAL objCom = new CommonBAL();
            DSInv = objCom.fnGetFieldValue("TOP 1 HIM_INVOICE_ID ", "HMS_INVOICE_MASTER", Criteria, "");

            // DSInv = objInv.InvoiceMasterGet(Criteria);

            if (DSInv.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindCommissionType()
        {

            string strName = txtRefDrName.Text;
            string RefDrID = "", RefDrName = "";
            string[] arrName = strName.Split('~');
            if (arrName.Length > 1)
            {
                RefDrID = arrName[0];
                RefDrName = arrName[1];

            }

            string Criteria = " 1=1 ";

            Criteria += " AND HRDM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HRDM_REF_ID='" + RefDrID + "'";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HRDM_COMMISSION_1,HRDM_COMMISSION_2,HRDM_COMMISSION_3,HRDM_COMMISSION_4", "HMS_REF_DOCTOR_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_1") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_1"] != "")
                {
                    drpCommissionType.Items.Insert(0, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_1"]));
                    drpCommissionType.Items[0].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_1"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_2") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_2"] != "")
                {
                    drpCommissionType.Items.Insert(1, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_2"]));
                    drpCommissionType.Items[1].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_2"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_3") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_3"] != "")
                {
                    drpCommissionType.Items.Insert(2, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_3"]));
                    drpCommissionType.Items[2].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_3"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_4") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_4"] != "")
                {
                    drpCommissionType.Items.Insert(3, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_4"]));
                    drpCommissionType.Items[3].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_4"]);
                }

            }

        }

        //void DisplaySecondaryInsuranceCompany()
        //{
        //    CommonBAL objCom = new CommonBAL();
        //    string Criteria=" 1=1 ";
        //    DataSet DS = new DataSet();

        //    Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPM_PT_ID='" + txtFileNo.Text.Trim() + "'";
        //  DS =  objCom.fnGetFieldValue(" count(*) ", " HMS_PATIENT_SECONDARY_COMPANY", Criteria, "");

        //  if (DS.Tables[0].Rows.Count > 1)
        //  {
        //      DataSet DSComp = new DataSet();
        //      clsInvoice objInv = new clsInvoice();
        //      Criteria += " AND HPM_INS_COMP_ID
        //      objInv.Company_SecondayCompanyMasterGet(Criteria);



        //  }
        //}

        decimal fnCalculatePTAmount(string AmountType)
        {

            decimal decPTBalance = 0;

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            if (AmountType == "NPPAID")
            {
                Criteria += " AND HIT_SERV_CODE='2' AND HIM_PT_ID = '" + txtFileNo.Text + "'";
            }
            else
            {
                Criteria += " AND HPB_PT_ID = '" + txtFileNo.Text + "'";
            }

            clsInvoice objInv = new clsInvoice();
            DS = objInv.CalculatePTAmount(AmountType, Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("PTAmount") == false)
                {
                    decPTBalance = Convert.ToDecimal(DS.Tables[0].Rows[0]["PTAmount"]);
                }
            }

            return decPTBalance;
        }


        void SaveClaimDetail()
        {

            CommonBAL objCom = new CommonBAL();
            string Criteria = "1=1";

            Criteria = "HIC_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            objCom.fnDeleteTableData("HMS_INVOICE_CLAIMSDTLS", Criteria);

            clsInvoiceClaims objInvClms = new clsInvoiceClaims();
            objInvClms.HIC_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInvClms.HIC_INVOICE_ID = txtInvoiceNo.Text.Trim();
            objInvClms.HIC_TYPE = drpClmType.SelectedValue;
            objInvClms.HIC_STARTDATE = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim();
            objInvClms.HIC_ENDDATE = txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim();
            objInvClms.HIC_STARTTYPE = drpClmStartType.SelectedValue;
            objInvClms.HIC_ENDTYPE = drpClmEndType.SelectedValue;

            DataTable DT = (DataTable)ViewState["InvoiceClaims"];


            //objrow["HIC_ICD_CODE"] = txtDiagCode.Text.Trim();
            //objrow["HIC_ICD_DESC"] = txtDiagName.Text.Trim();
            //objrow["HIC_ICD_TYPE"] = drpDiag

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string strICDType = "", strICDCode = "";

                strICDCode = Convert.ToString(DT.Rows[i]["HIC_ICD_CODE"]);
                strICDType = Convert.ToString(DT.Rows[i]["HIC_ICD_TYPE"]);

                if (strICDType == "Principal")
                {
                    objInvClms.HIC_ICD_CODE_P = strICDCode;
                    objInvClms.HIC_YEAR_OF_ONSET_P = Convert.ToString(DT.Rows[i]["HIC_YEAR_OF_ONSET_P"]);
                    goto EndFor;
                }

                if (strICDType == "Admitting")
                {
                    objInvClms.HIC_ICD_CODE_A = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S1 == null)
                {
                    objInvClms.HIC_ICD_CODE_S1 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S2 == null)
                {
                    objInvClms.HIC_ICD_CODE_S2 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S3 == null)
                {
                    objInvClms.HIC_ICD_CODE_S3 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S4 == null)
                {
                    objInvClms.HIC_ICD_CODE_S4 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S5 == null)
                {
                    objInvClms.HIC_ICD_CODE_S5 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S6 == null)
                {
                    objInvClms.HIC_ICD_CODE_S6 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S7 == null)
                {
                    objInvClms.HIC_ICD_CODE_S7 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S8 == null)
                {
                    objInvClms.HIC_ICD_CODE_S8 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S11 == null)
                {
                    objInvClms.HIC_ICD_CODE_S11 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S12 == null)
                {
                    objInvClms.HIC_ICD_CODE_S12 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S13 == null)
                {
                    objInvClms.HIC_ICD_CODE_S13 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S14 == null)
                {
                    objInvClms.HIC_ICD_CODE_S14 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S15 == null)
                {
                    objInvClms.HIC_ICD_CODE_S15 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S16 == null)
                {
                    objInvClms.HIC_ICD_CODE_S16 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S17 == null)
                {
                    objInvClms.HIC_ICD_CODE_S17 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S18 == null)
                {
                    objInvClms.HIC_ICD_CODE_S18 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S19 == null)
                {
                    objInvClms.HIC_ICD_CODE_S19 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S20 == null)
                {
                    objInvClms.HIC_ICD_CODE_S20 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S21 == null)
                {
                    objInvClms.HIC_ICD_CODE_S21 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S22 == null)
                {
                    objInvClms.HIC_ICD_CODE_S22 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S23 == null)
                {
                    objInvClms.HIC_ICD_CODE_S23 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S24 == null)
                {
                    objInvClms.HIC_ICD_CODE_S24 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S25 == null)
                {
                    objInvClms.HIC_ICD_CODE_S25 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S26 == null)
                {
                    objInvClms.HIC_ICD_CODE_S26 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S27 == null)
                {
                    objInvClms.HIC_ICD_CODE_S27 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S28 == null)
                {
                    objInvClms.HIC_ICD_CODE_S28 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "ReasonForVisit" && objInvClms.HIC_ICD_CODE_R1 == null)
                {
                    objInvClms.HIC_ICD_CODE_R1 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "ReasonForVisit" && objInvClms.HIC_ICD_CODE_R2 == null)
                {
                    objInvClms.HIC_ICD_CODE_R2 = strICDCode;
                    goto EndFor;
                }


                if (strICDType == "ReasonForVisit" && objInvClms.HIC_ICD_CODE_R3 == null)
                {
                    objInvClms.HIC_ICD_CODE_R3 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "ReasonForVisit" && objInvClms.HIC_ICD_CODE_R4 == null)
                {
                    objInvClms.HIC_ICD_CODE_R4 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "ReasonForVisit" && objInvClms.HIC_ICD_CODE_R5 == null)
                {
                    objInvClms.HIC_ICD_CODE_R5 = strICDCode;
                    goto EndFor;
                }

            EndFor: ;
            }

            if (DT.Rows.Count > 0)
            {
                objInvClms.InvoiceClaimDtlsAdd();
            }

            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {

                if (DT.Rows.Count > 0)
                {
                    Criteria = "HIC_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIC_INVOICE_ID='T" + txtInvoiceNo.Text.Trim() + "'";
                    objCom.fnDeleteTableData("HMS_INVOICE_CLAIMSDTLS", Criteria);

                    objInvClms.HIC_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                    objInvClms.InvoiceClaimDtlsAdd();
                }
            }

        }

        void fnSaveReceipt(decimal PrevPTCrPaid)
        {

            if (hidReceipt.Value == "" || hidReceipt.Value == null)
            {
                goto SaveReceiptEnd;
            }

            clsReceipt objRec = new clsReceipt();
            objRec.HRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objRec.HRM_RECEIPTNO = hidReceipt.Value;
            objRec.HRM_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objRec.HRM_PT_ID = txtFileNo.Text.Trim();
            objRec.HRM_PT_NAME = txtName.Text.Trim();
            objRec.HRM_PT_TYPE = drpInvType.Text.Trim();
            objRec.HRM_COMP_ID = txtSubCompanyID.Text.Trim();
            objRec.HRM_COMP_NAME = txtCompanyName.Text.Trim();
            objRec.HRM_COMP_TYPE = hidInsType.Value;
            objRec.HRM_DR_ID = txtDoctorID.Text.Trim();
            objRec.HRM_DR_NAME = txtDoctorName.Text.Trim();

            string strName = txtRefDrName.Text;
            string RefDrID = "", RefDrName = "";
            string[] arrName = strName.Split('~');
            if (arrName.Length > 1)
            {
                RefDrID = arrName[0];
                RefDrName = arrName[1];

            }

            objRec.HRM_REF_ID = RefDrID; // txtRefDrId.Text.Trim();
            objRec.HRM_REF_NAME = RefDrName; // txtRefDrName.Text.Trim();


            objRec.HRM_AMT_RCVD = Convert.ToString(Convert.ToDecimal(txtPaidAmount.Text) - PrevPTCrPaid);
            objRec.HRM_PAYMENT_TYPE = drpPayType.SelectedValue;
            objRec.HRM_CCNO = txtCCNo.Text.Trim();
            objRec.HRM_CCNAME = drpccType.SelectedValue;
            objRec.HRM_HOLDERNAME = txtCCHolder.Text.Trim();
            objRec.HRM_CCREFNO = txtCCRefNo.Text.Trim();
            objRec.HRM_CHEQUENO = txtCCNo.Text.Trim();
            objRec.HRM_BANKNAME = drpBank.SelectedValue;
            objRec.HRM_CHEQUEDATE = txtDcheqdate.Text.Trim();
            objRec.HRM_CASH_AMT = txtCashAmt.Text.Trim();
            objRec.HRM_CC_AMT = txtCCAmt.Text.Trim();
            objRec.HRM_CHEQUE_AMT = txtChqAmt.Text.Trim();
            objRec.HRM_CUR_TYP = drpCurType.SelectedValue;
            objRec.HRM_CUR_RATE = txtConvRate.Text.Trim();
            objRec.HRM_CUR_VALUE = txtcurValue.Text.Trim();
            objRec.HRM_CUR_RCVD_AMT = txtRcvdAmount.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objRec.NewFlag = "A";
            }
            else
            {
                objRec.NewFlag = "M";
            }


            objRec.UserID = Convert.ToString(Session["User_ID"]);

            string srtReceptNo = "";
            srtReceptNo = objRec.ReceiptMasterAdd();
            hidReceipt.Value = srtReceptNo;

            objRec.HRT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objRec.HRT_RECEIPTNO = hidReceipt.Value;
            objRec.HRT_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objRec.HRT_INVOICE_ID = txtInvoiceNo.Text.Trim();
            objRec.HRT_NET_AMOUNT = txtBillAmt4.Text.Trim();
            objRec.HRT_PAID_AMOUNT = txtPaidAmount.Text.Trim();
            objRec.HRT_DISCOUNT = txtSplDisc.Text.Trim();

            objRec.ReceiptTransAdd();

            if (ChkTopupCard.Checked == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                string srtReceptNo1 = "";
                objRec.HRM_RECEIPTNO = hidReceipt.Value;
                srtReceptNo1 = objRec.ReceiptMasterAdd();

                objRec.HRT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objRec.HRT_RECEIPTNO = srtReceptNo1;
                objRec.HRT_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                objRec.HRT_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                objRec.HRT_NET_AMOUNT = txtBillAmt4.Text.Trim();
                objRec.HRT_PAID_AMOUNT = txtPaidAmount.Text.Trim();
                objRec.HRT_DISCOUNT = txtSplDisc.Text.Trim();

                objRec.ReceiptTransAdd();
            }

        SaveReceiptEnd: ;

        }

        void PatientDeductibleBind()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("TOP 1 HPM_PT_ID, HPM_DED_TYPE,HPM_DED_AMT,HPM_COINS_TYPE,HPM_COINS_AMT,HPM_DENT_COINS_TYPE,HPM_DENT_COINS_AMT " +
            ",HPM_PHY_COINS_TYPE,HPM_PHY_COINS_AMT,HPM_LAB_COINS_TYPE,HPM_LAB_COINS_AMT,HPM_RAD_COINS_TYPE,HPM_RAD_COINS_AMT"
            , "HMS_PATIENT_MASTER", Criteria, "");

            //   DS = dbo.retun_patient_details(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //if (HMS_COINS_PTREG == false)
                // {
                if (lblVisitType.Text != "Revisit")  //GlobalValues.FileDescription.ToUpper() != "JABALSINA" &&
                {
                    ViewState["Deductible_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_DED_TYPE"]);// IIf(IsNull(.Fields("HPM_DED_TYPE").Value), "%", .Fields("HPM_DED_TYPE").Value)
                    ViewState["Deductible"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_DED_AMT"]);//IIf(IsNull(.Fields("HPM_DED_AMT").Value), 0, .Fields("HPM_DED_AMT").Value)
                    ViewState["Co_Ins_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_COINS_TYPE"]);//IIf(IsNull(.Fields("HPM_COINS_TYPE").Value), "%", .Fields("HPM_COINS_TYPE").Value)
                    ViewState["Co_Ins_Amt"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_COINS_AMT"]);//IIf(IsNull(.Fields("HPM_COINS_AMT").Value), 0, .Fields("HPM_COINS_AMT").Value)




                    ViewState["Co_Ins_TypeDental"] = DS.Tables[0].Rows[0].IsNull("HPM_DENT_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_DENT_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtDental"] = DS.Tables[0].Rows[0].IsNull("HPM_DENT_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_DENT_COINS_AMT"]);

                    ViewState["Co_Ins_TypePharmacy"] = DS.Tables[0].Rows[0].IsNull("HPM_PHY_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_PHY_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtPharmacy"] = DS.Tables[0].Rows[0].IsNull("HPM_PHY_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_PHY_COINS_AMT"]);

                    ViewState["Co_Ins_TypeLab"] = DS.Tables[0].Rows[0].IsNull("HPM_LAB_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_LAB_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtLab"] = DS.Tables[0].Rows[0].IsNull("HPM_LAB_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_LAB_COINS_AMT"]);

                    ViewState["Co_Ins_TypeRad"] = DS.Tables[0].Rows[0].IsNull("HPM_RAD_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_RAD_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtRad"] = DS.Tables[0].Rows[0].IsNull("HPM_RAD_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_RAD_COINS_AMT"]);

                }

                // }

            }

        EndSub: ;
        }

        void BindPatientMailID()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("TOP 1 HPM_EMAIL ", "HMS_PATIENT_MASTER", Criteria, "");

            // DS = dbo.retun_patient_details(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblEmailID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_EMAIL"]);

            }
        }

        void GetPatientVisitVisitType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HPV_STATUS <>'D'  ";

            // Criteria += " AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPV_PT_ID='" + txtFileNo.Text.Trim() + "'";



            //if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            //{
            //    if (txtDepName.Text != "")
            //    {
            //        Criteria += " AND HPV_DEP_NAME='" + txtDepName.Text.Trim() + "'";
            //    }
            //}
            //else
            //{
            if (txtDoctorID.Text.Trim() != "")
            {
                Criteria += " AND HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";
            }
            // }
            //VISIT DATE GRATER THAN 3 YEARS TREAT AS NEW PATIENT
            Criteria += " AND  DATEADD(YY,-3, CONVERT(DATETIME,GETDATE(),101)) <= CONVERT(DATETIME, HPV_DATE,101) ";
            DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" TOP 1  HPV_DATE ", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE desc");
            //   DS = dbo.PatientVisitGetForReg(Criteria);

            lblVisitType.Text = "New";
            if (DS.Tables[0].Rows.Count > 0)
            {
                int intCount = DS.Tables[0].Rows.Count;
                string strLastVisit = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATE"]);



                int intRevisit = Convert.ToInt32(Session["RevisitDays"]);
                if (strLastVisit != "")
                {

                    DateTime dtLastVisit = Convert.ToDateTime(strLastVisit);
                    DateTime dtTransDate = DateTime.ParseExact(txtInvDate.Text.Trim(), "dd/MM/yyyy", null);

                    // Today =     Convert.ToDateTime(hidTodayDBDate.Value);


                    TimeSpan Diff = dtTransDate.Date - dtLastVisit.Date;

                    if (Diff.Days <= intRevisit)
                    {
                        lblVisitType.Text = "Revisit";
                    }
                    else if (Diff.Days >= intRevisit)
                    {
                        lblVisitType.Text = "Old";
                    }

                }
            }

        }

        void PatientDataBind()
        {
            Int32 VisitCount = 0;
            strSubCompanyID = "";
            DateTime Today = System.DateTime.Now;

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("TOP 1 HPM_PT_ID, HPM_DED_TYPE,HPM_DED_AMT,HPM_COINS_TYPE,HPM_COINS_AMT,HPM_DENT_COINS_TYPE,HPM_DENT_COINS_AMT " +
            ",HPM_PHY_COINS_TYPE,HPM_PHY_COINS_AMT,HPM_LAB_COINS_TYPE,HPM_LAB_COINS_AMT,HPM_RAD_COINS_TYPE,HPM_RAD_COINS_AMT" +
            ",(ISNULL(HPM_PT_TITLE,'') +' '+ISNULL(HPM_PT_FNAME,'') +' '+ ISNULL(HPM_PT_MNAME,' ') +' '+ ISNULL(HPM_PT_LNAME,' ')) as FullName" +
            ",HPM_PT_COMP_NAME,HPM_EMAIL,HPM_MOBILE,HPM_PT_TYPE,HPM_DR_ID,HPM_DR_NAME,HPM_DEP_NAME,HPM_REF_DR_ID,HPM_REF_DR_NAME,HPM_PREV_VISIT,HPM_INS_COMP_ID,HPM_INS_COMP_NAME,HPM_CONT_EXPDATE" +
            ",HPM_INS_COMP_TYPE,HPM_BILL_CODE,HPM_POLICY_TYPE,HPM_ID_NO,HPM_POLICY_NO,CONVERT(VARCHAR,HPM_CONT_EXPDATE,103)as HPM_CONT_EXPDATEDesc"

            , "HMS_PATIENT_MASTER", Criteria, "");

            // DS = dbo.retun_patient_details(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                // if (HMS_COINS_PTREG == false)
                // {
                if (lblVisitType.Text != "Revisit")  //GlobalValues.FileDescription.ToUpper() != "JABALSINA" && 
                {
                    ViewState["Deductible_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_DED_TYPE"]);// IIf(IsNull(.Fields("HPM_DED_TYPE").Value), "%", .Fields("HPM_DED_TYPE").Value)
                    ViewState["Deductible"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_DED_AMT"]);//IIf(IsNull(.Fields("HPM_DED_AMT").Value), 0, .Fields("HPM_DED_AMT").Value)
                    ViewState["Co_Ins_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_COINS_TYPE"]);//IIf(IsNull(.Fields("HPM_COINS_TYPE").Value), "%", .Fields("HPM_COINS_TYPE").Value)
                    ViewState["Co_Ins_Amt"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_COINS_AMT"]);//IIf(IsNull(.Fields("HPM_COINS_AMT").Value), 0, .Fields("HPM_COINS_AMT").Value)


                    ViewState["Co_Ins_TypeDental"] = DS.Tables[0].Rows[0].IsNull("HPM_DENT_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_DENT_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtDental"] = DS.Tables[0].Rows[0].IsNull("HPM_DENT_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_DENT_COINS_AMT"]);

                    ViewState["Co_Ins_TypePharmacy"] = DS.Tables[0].Rows[0].IsNull("HPM_PHY_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_PHY_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtPharmacy"] = DS.Tables[0].Rows[0].IsNull("HPM_PHY_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_PHY_COINS_AMT"]);

                    ViewState["Co_Ins_TypeLab"] = DS.Tables[0].Rows[0].IsNull("HPM_LAB_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_LAB_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtLab"] = DS.Tables[0].Rows[0].IsNull("HPM_LAB_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_LAB_COINS_AMT"]);

                    ViewState["Co_Ins_TypeRad"] = DS.Tables[0].Rows[0].IsNull("HPM_RAD_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_RAD_COINS_TYPE"]);
                    ViewState["Co_Ins_AmtRad"] = DS.Tables[0].Rows[0].IsNull("HPM_RAD_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_RAD_COINS_AMT"]);
                }
                // }

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    VisitCount = PatientVisitBind();

                }


                if (GlobalValues.FileDescription != "SMCH") fnLastConsltDate();

                // drpInvType.SelectedValue = DS.Tables[0].Rows[0]["HPM_PT_TYPE"].ToString();

                /*
                string msg1, msg2;
                msg1 = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";
                msg2 = drpInvType.SelectedValue == "Cash" ? "Cash" : "Credit";

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    if (drpInvType.SelectedValue != msg1)
                    {


                        // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This patient is registered as " + msg1 + " Do you want Invoice this Patient as" + msg2 + "');", true);
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "CheckInvType('" + msg1 + "','" + msg2 + "');", true);

                    }
                }
                8*/

                // txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_TOKEN_NO"]);
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
                txtPTComp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);

                lblEmailID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_EMAIL"]);

                lblMobileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]);



                if (VisitCount == 0)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA")
                    {
                        hidInvoiceType.Value = "Cash";
                    }
                    else if (Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CR")
                    {
                        hidInvoiceType.Value = "Credit";
                    }
                    else if (Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CU")
                    {
                        hidInvoiceType.Value = "Customer";
                    }

                    if (Convert.ToString(ViewState["HSOM_DR_MANUALSEELCT_INVOICE"]) == "0")
                    {
                        txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_ID"]);
                        txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_NAME"]);
                        hidDrDept.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DEP_NAME"]);

                        txtServDrCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_ID"]);
                        txtServDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_NAME"]);


                        if (Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "" && DS.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false)
                        {
                            txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                            txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);

                            txtServOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                            txtServOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                        }
                        else
                        {
                            txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_ID"]);
                            txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_NAME"]);

                            txtServOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_ID"]);
                            txtServOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_NAME"]);
                        }

                        if (Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "" && DS.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false)
                        {
                            txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                            //  txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                        }

                    }




                    string vInvType = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";
                    string vdrpInvType = drpInvType.SelectedValue;


                    divInvTypeMessage.Style.Add("display", "none");

                    if (vInvType != vdrpInvType)
                    {

                        divInvTypeMessage.Style.Add("display", "block");
                        lblInvTypeMessage.Text = "This patient is registered as " + vInvType + ". Do you want Invoice this Patient as " + vdrpInvType + " ?";

                        strSubCompanyID = "";
                    }

                    GetPatientVisitVisitType();

                }


                if (drpTreatmentType.Items.Count > 0) drpTreatmentType.SelectedValue = "OP";

                ////if (DS.Tables[0].Rows[0].IsNull("HPM_PREV_VISIT") == false)
                ////{
                ////    if (hidPTVisitType.Value == "")
                ////    {
                ////        DateTime dtLastVisit;
                ////        dtLastVisit = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPM_PREV_VISIT"]);


                ////        TimeSpan Diff = Today.Date - dtLastVisit;

                ////        if (Diff.Days <= Convert.ToInt32(Session["RevisitDays"]))
                ////        {
                ////            hidPTVisitType.Value = "Revisit";
                ////        }
                ////        else if (Diff.Days >= Convert.ToInt32(Session["RevisitDays"]))
                ////        {
                ////            hidPTVisitType.Value = "Old";
                ////        }
                ////    }
                ////}
                ////else
                ////{
                ////    hidPTVisitType.Value = "New";
                ////}

                Decimal CreditBalAmt = 0;

                CreditBalAmt = (fnCalculatePTAmount("INVOICE") - fnCalculatePTAmount("RETURN")) - (fnCalculatePTAmount("PAIDAMT") + fnCalculatePTAmount("RETURNPAIDAMT") + fnCalculatePTAmount("REJECTED")) - fnCalculatePTAmount("NPPAID");
                if (CreditBalAmt > 0)
                {
                    lblBalanceMsg.Text = "Balance Amount Due : " + Convert.ToString(CreditBalAmt);  //HMS_CURRENCY_FORMAT)
                }
                else
                {
                    lblBalanceMsg.Text = "";
                }




                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "Confirm();", true);
                //string confirmValue = Request.Form["confirm_value"];
                //if (confirmValue == "Yes")
                //{
                //   // this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked YES!')", true);
                //    goto EndSub;
                //}
                //else
                //{
                //    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked NO!')", true);
                //}



                //strMessage = "Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice.";

                //if (strMessage != "")
                //{
                //    MPExtMessage.Show();
                //}
                if (hidInvoiceType.Value == "Credit" || hidInvoiceType.Value == "Customer") //if (drpInvType.SelectedValue == "Credit")
                {


                    if (VisitCount == 0)
                    {
                        if (DS.Tables[0].Rows[0].IsNull("HPM_INS_COMP_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]) != "")
                        {

                            if (hidInvoiceType.Value == "Credit")
                            {
                                if (DS.Tables[0].Rows[0].IsNull("HPM_CONT_EXPDATE") == false)
                                {
                                    DateTime dtPolicyExp, dtInvDate;
                                    dtPolicyExp = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATE"]);
                                    dtInvDate = DateTime.ParseExact(txtInvDate.Text.Trim(), "dd/MM/yyyy", null);

                                    TimeSpan Diff1 = dtPolicyExp.Date - dtInvDate.Date;// Today.Date;

                                    if (Diff1.Days < 0)
                                    {
                                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?')", true);

                                        goto EndSub;
                                    }

                                }
                            }

                            hidInsType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_TYPE"]);
                            txtSubCompanyID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                            strSubCompanyID = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                            txtSubCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                            hidCompanyID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPM_BILL_CODE"]);
                            hidCmpRefCode.Value = txtSubCompanyID.Text.Trim();
                            txtPlanType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);



                            txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_ID_NO"]);
                            txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                            txtPolicyExp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]);
                            //  
                            //  TxtVoucherNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_VOUCHER_NO"]);



                            //  

                            // GetPatientVisit();

                            CompanyMasterBAL objComp = new CompanyMasterBAL();

                            if (hidCompanyID.Value.Trim() == txtSubCompanyID.Text.Trim())
                            {
                                txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                                txtSubCompanyName.Text = "";
                            }
                            else
                            {


                                DataSet DSComp = new DataSet();
                                Criteria = " 1=1 ";
                                Criteria += "AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_BILL_CODE='" + hidCompanyID.Value + "' AND HCM_COMP_ID='" + hidCompanyID.Value + "'";
                                DSComp = objComp.GetCompanyMaster(Criteria);
                                if (DSComp.Tables[0].Rows.Count > 0)
                                {
                                    txtCompanyName.Text = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_NAME"]);
                                    hidCompAgrmtType.Value = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                                    strCompAgrmtType = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                                }

                            }

                            DataSet DSComp1 = new DataSet();
                            Criteria = " 1=1 ";
                            Criteria += " AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_COMP_ID='" + txtSubCompanyID.Text + "'";
                            DSComp1 = objComp.GetCompanyMaster(Criteria);
                            if (DSComp1.Tables[0].Rows.Count > 0)
                            {
                                hidUpdateCodeType.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]);
                                hidCompAgrmtType.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                                strCompAgrmtType = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                                if (hidInvoiceType.Value == "Credit")
                                {
                                    fnSetServices(Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]), txtSubCompanyID.Text.Trim(), txtPlanType.Text.Trim());
                                }

                                if (Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_REM_RCPTN"]) != "" && DSComp1.Tables[0].Rows[0].IsNull("HCM_REM_RCPTN") == false)// App.FileDescription = "RASHIDIYA" Then
                                {
                                    MPExtMessage.Show();
                                    strMessage = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_REM_RCPTN"]);//  temprec1.Fields("HCM_REM_RCPTN").Value;
                                }
                            }


                        }
                    }
                }



                //      If CmbInvoiceType.Text = "Cash" Then
                //    fnFillCmpServInListBox
                //Else
                //    If Check_CmpService(TxtInsCode.Text) And mMinistryService Then
                //        fnFillCmpServInListBox "MINISTRY", mRefCode
                //    Else
                //        fnFillCmpServInListBox
                //    End If
                //End If

                if (txtRefDrName.Text.Trim() != "" && GlobalValues.FileDescription == "ALREEM")
                {
                    BindCommissionType();
                }

                if (GlobalValues.FileDescription == "KHALID_NUAIMI")
                {
                    ShowAppointmentDtls();
                }

                BindeAuthorizationDtls();
            }
            else
            {
                //txtName.Text = "";
                //txtInsType.Text = "";
                //TxtInsCode.Text = "";
                //TXTSubInsCode.Text = "";
                //txtinsName.Text = "";
                //txtSubins.Text = "";
                //txtptComp.Text = "";
                //TxtIDNo.Text = "";
                //TxtPolicyNo.Text = "";
                //TxtIDNo.Text = "";
                //txtpolicyExp.Text = "";
                //txtInsTrtnt.Text = "";
                //txtDrCode.Text = "";
                //TxtDRName.Text = "";
                //TxtRefDr.Text = "";
                //TxtRefID.Text = "";
                //CmbTrtntType.ListIndex = -1;
            }
        EndSub: ;
        }

        Boolean IsPatientVisitAvailable()
        {
            string Criteria = " 1=1 AND HPV_STATUS <>'D' ";

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID   = '" + txtFileNo.Text.Trim() + "' ";
            }

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.Patient_Master_VisitGet(Criteria);

            if (DS.Tables[0].Rows.Count > 1)
            {
                if (hidInvoiceType.Value == "Credit")
                {
                    TxtVoucherNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_CLAIM_NO"]);
                }

                return true;
            }

            return false;


        }

        Int32 PatientVisitBind()
        {
            divMessage.Style.Add("display", "none");
            Int32 VisitCount = 0;

            DateTime Today = System.DateTime.Now;

            string Criteria = " 1=1 AND HPV_STATUS <>'D' AND (HPV_INVOICE_ID IS NULL OR HPV_INVOICE_ID ='') ";

            if (Convert.ToString(ViewState["SelectedVisitID"]) != "" && Convert.ToString(ViewState["SelectedVisitID"]) != null)
            {
                Criteria += " AND HPV_SEQNO='" + Convert.ToString(ViewState["SelectedVisitID"]) + "'";
            }
            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) = '" + strForStartDate + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID   = '" + txtFileNo.Text.Trim() + "' ";
            }
            hidVisitID.Value = "";
            txtEMRID.Text = "";
            ModPopExtVisits.Hide();

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            // DS = dbo.Patient_Master_VisitGet(Criteria);
            CommonBAL objCom = new CommonBAL();
            // DS = objCom.fnGetFieldValue(" CONVERT(VARCHAR,HPV_POLICY_EXP,103) as HPV_POLICY_EXPDesc,*", "HMS_PATIENT_VISIT", Criteria, "");

            DS = objCom.fnGetFieldValue(" TOP 100 HPV_SEQNO, HPV_PT_TYPE, HPV_VISIT_TYPE, HPV_DR_ID,HPV_DR_NAME,HPV_DEP_NAME,HPV_CLAIM_NO,HPV_REFERRAL_TO" +
            ",HPV_ELIGIBILITY_ID,HPV_EMR_ID,HPV_PATIENT_CLASS,HPV_NURSE_ID,HPV_DR_COMMENT,HPV_POLICY_EXP,HPV_COMP_ID,HPV_COMP_NAME,HPV_POLICY_TYPE,HPV_ID_NO" +
            ",HPV_POLICY_NO,CONVERT(VARCHAR,HPV_POLICY_EXP,103) as HPV_POLICY_EXPDesc,HPV_PACKAGE_SERV_ID", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");

            // DS = objCom.PatientVisitTopGet(Criteria, "500");
            VisitCount = DS.Tables[0].Rows.Count;

            if (VisitCount > 1)
            {
                //  gvVisit.DataSource = DS;
                //  gvVisit.DataBind();
                //  ModPopExtVisits.Show();


                txtSrcVisitFileNo.Text = txtFileNo.Text.Trim();
                BindVisitDtlsAll();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowVisitDetailsPopup()", true);

                return VisitCount;
            }




            if (DS.Tables[0].Rows.Count == 1)
            {

                hidInvoiceType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PT_TYPE"]);

                string vInvType = hidInvoiceType.Value;
                string vdrpInvType = drpInvType.SelectedValue;


                divInvTypeMessage.Style.Add("display", "none");

                if (vInvType != vdrpInvType)
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowInvTypeMessage()", true);

                    divInvTypeMessage.Style.Add("display", "block");
                    lblInvTypeMessage.Text = "This patient is registered as " + vInvType + ". Do you want Invoice this Patient as " + vdrpInvType + " ?";

                    strSubCompanyID = "";
                }



                lblVisitType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPE"]);
                hidVisitID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);
                hidDrDept.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DEP_NAME"]);

                txtServDrCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                txtServDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);

                if( DS.Tables[0].Rows[0].IsNull("HPV_REFERRAL_TO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]) !="")
                {
                txtServOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]);

                string strRefDrName = "", strRefDrDept = "";
                GetStaffName(Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]),out strRefDrName,out strRefDrDept);

                txtServOrdClinName.Text = strRefDrName;

                }
                else{
                     txtServOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                txtServOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);
                }
                if (hidInvoiceType.Value == "Credit")
                {
                    TxtVoucherNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_CLAIM_NO"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_REFERRAL_TO") == false)
                {

                    txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]);

                    string strDrCode = "", strDrName = "";
                    strDrCode = Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]);
                    GetRefDrName(strDrCode, out strDrName);

                    txtRefDrName.Text = strDrCode + "~" + strDrName;

                    txtOrdClinName.Text = strDrName;

                }
                else
                {
                    // txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                    // txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);

                    txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                    txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);
                }

                txtEligibilityID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_ELIGIBILITY_ID"]);
                txtEMRID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_EMR_ID"]);

                ViewState["PATIENT_CLASS"] = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PATIENT_CLASS"]);

                if (Convert.ToString(ViewState["PATIENT_CLASS"]) == "UC")
                {
                    drpClmType.SelectedValue = "2";
                }

                if (txtEMRID.Text.Trim() != "")
                {
                    BindEMRDtls();

                }

                //if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_REFERRAL_TO") == false)
                //{
                //    txtRefDrId_TextChanged(txtRefDrId, new EventArgs());

                //}
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_NURSE_ID"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_NURSE_ID") == false)
                {
                    drpNurse.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HPV_NURSE_ID"]);
                }
                // txtOrdClinCode_TextChanged(txtOrdClinCode, new EventArgs());

                if (GlobalValues.FileDescription.ToUpper() == "JABALSINA")
                {

                    string strComment = "";

                    if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_COMMENT"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_DR_COMMENT") == false)
                    {
                        strComment = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_COMMENT"]);
                    }

                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Message','"+ strComment +"','Green')", true);
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('Message','" + strComment + "','Green')", true);

                    //  this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('"+ strComment + "')", true);
                    if (strComment != "")
                    {
                        divMessage.Style.Add("display", "block");
                        lblMessage1.Text = "Doctor Message";
                        lblMessage2.Text = strComment;
                    }
                }

                if (hidInvoiceType.Value == "Credit" || hidInvoiceType.Value == "Customer")
                {
                    if (hidInvoiceType.Value == "Credit")
                    {
                        if (DS.Tables[0].Rows[0].IsNull("HPV_POLICY_EXP") == false)
                        {
                            DateTime dtPolicyExp, dtInvDate;
                            dtPolicyExp = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPV_POLICY_EXP"]);
                            dtInvDate = DateTime.ParseExact(txtInvDate.Text.Trim(), "dd/MM/yyyy", null);


                            TimeSpan Diff1 = dtPolicyExp.Date - dtInvDate.Date;// Today.Date;


                            if (Diff1.Days < 0)
                            {
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?')", true);

                                goto EndSub;
                            }

                        }
                    }

                    if (DS.Tables[0].Rows[0].IsNull("HPV_COMP_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_ID"]) != "")
                    {

                        if (hidInvoiceType.Value == "Credit")
                        {
                            hidInsType.Value = "Insurance";  //Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_TYPE"]);
                        }
                        txtSubCompanyID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_ID"]);
                        strSubCompanyID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_ID"]);
                        txtSubCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                      //  hidCompanyID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_ID"]); //HPM_BILL_CODE
                        hidCmpRefCode.Value = txtSubCompanyID.Text.Trim();
                        txtPlanType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_POLICY_TYPE"]);



                        if (DS.Tables[0].Rows[0].IsNull("HPV_ID_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_ID_NO"]) != "")
                        {
                            txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_ID_NO"]);
                        }
                        if (DS.Tables[0].Rows[0].IsNull("HPV_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_POLICY_NO"]) != "")
                        {
                            txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_POLICY_NO"]);
                        }
                        if (DS.Tables[0].Rows[0].IsNull("HPV_POLICY_EXPDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_POLICY_EXPDesc"]) != "")
                        {
                            txtPolicyExp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_POLICY_EXPDesc"]);
                        }




                        CompanyMasterBAL objComp = new CompanyMasterBAL();

                        ////if (hidCompanyID.Value.Trim() == txtSubCompanyID.Text.Trim())
                        ////{
                        ////    txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                        ////    txtSubCompanyName.Text = "";
                        ////}
                        ////else
                        ////{


                        ////    DataSet DSComp = new DataSet();
                        ////    Criteria = " 1=1 ";
                        ////    Criteria += "AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_BILL_CODE='" + hidCompanyID.Value + "' AND HCM_COMP_ID='" + hidCompanyID.Value + "'";
                        ////    DSComp = objComp.GetCompanyMaster(Criteria);
                        ////    if (DSComp.Tables[0].Rows.Count > 0)
                        ////    {
                        ////        txtCompanyName.Text = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_NAME"]);
                        ////        hidCompAgrmtType.Value = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                        ////        strCompAgrmtType = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                        ////    }

                        ////}

                        DataSet DSComp1 = new DataSet();
                        Criteria = " 1=1 ";
                        Criteria += " AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_COMP_ID='" + txtSubCompanyID.Text + "'";
                        DSComp1 = objComp.GetCompanyMaster(Criteria);
                        if (DSComp1.Tables[0].Rows.Count > 0)
                        {
                            hidUpdateCodeType.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]);
                            hidCompAgrmtType.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                            strCompAgrmtType = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);

                            if (hidInvoiceType.Value == "Credit")
                            {
                                fnSetServices(Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]), txtSubCompanyID.Text.Trim(), txtPlanType.Text.Trim());
                            }

                            if (Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_REM_RCPTN"]) != "" && DSComp1.Tables[0].Rows[0].IsNull("HCM_REM_RCPTN") == false)// App.FileDescription = "RASHIDIYA" Then
                            {
                                MPExtMessage.Show();
                                strMessage = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_REM_RCPTN"]);//  temprec1.Fields("HCM_REM_RCPTN").Value;
                            }

                            hidCompanyID.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_BILL_CODE"]);


                            if (hidCompanyID.Value.Trim() == txtSubCompanyID.Text.Trim())
                            {
                                txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                                txtSubCompanyName.Text = "";
                            }
                            else
                            {
                                DataSet DSComp = new DataSet();
                                Criteria = " 1=1 ";
                                Criteria += "AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_BILL_CODE='" + hidCompanyID.Value + "' AND HCM_COMP_ID='" + hidCompanyID.Value + "'";
                                DSComp = objComp.GetCompanyMaster(Criteria);
                                if (DSComp.Tables[0].Rows.Count > 0)
                                {
                                    txtCompanyName.Text = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_NAME"]);
                                    hidCompAgrmtType.Value = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                                    strCompAgrmtType = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                                }

                            }
                        }


                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_PACKAGE_SERV_ID"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_PACKAGE_SERV_ID") == false)
                {

                    //LoadServiceDtls(Convert.ToString(DS.Tables[0].Rows[0]["HPV_PACKAGE_SERV_ID"]));

                    string PackageServID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PACKAGE_SERV_ID"]);
                    string strServConditions = "  (HSM_SERV_ID ='" + PackageServID + "')";
                    string strAgreConditions = "  (HAS_SERV_ID ='" + PackageServID + "')";

                    // fnServiceAddFromSO(Convert.ToString("'" + DS.Tables[0].Rows[0]["HPV_PACKAGE_SERV_ID"]) + "'");

                    fnServiceAddFromSO(strServConditions, strAgreConditions,"");

                }
                else
                {
                    if (IsPatientBilledSameDaySameDr(false) == false)
                    {
                        BuildeInvoiceTrans();
                        BuildeInvoiceClaims();

                        BindSalesOrderServ();
                        BindSalesOrderDiag();
                    }
                }

            }

        EndSub: ;
            return VisitCount;


        }

        void BindVisitDtlsAll()
        {
            string Criteria = " 1=1 AND HPV_STATUS <>'D' ";

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) = '" + strForStartDate + "'";
            }

            if (drpSrcVisitInvoiceStatus.SelectedValue != "")
            {

                if (drpSrcVisitInvoiceStatus.SelectedValue == "Invoiced")
                {
                    Criteria += " AND  HPV_INVOICE_ID IS NOT NULL AND HPV_INVOICE_ID !='' ";
                }
                else if (drpSrcVisitInvoiceStatus.SelectedValue == "NotInvoiced")
                {
                    Criteria += " AND ( HPV_INVOICE_ID IS NULL OR HPV_INVOICE_ID ='' ) ";
                }
            }

            if (txtSrcVisitFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID   = '" + txtSrcVisitFileNo.Text.Trim() + "' ";
            }

            hidVisitID.Value = "";
            txtEMRID.Text = "";
            ModPopExtVisits.Hide();


            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" TOP 50 HPV_SEQNO,  HPV_EMR_ID,HPV_ELIGIBILITY_ID,HPV_POLICY_TYPE,HPV_ID_NO,HPV_POLICY_NO,HPV_POLICY_EXP" +
                ",CONVERT(VARCHAR,HPV_POLICY_EXP,103) as HPV_POLICY_EXPDesc,convert(varchar,HPV_Date,103) as HPV_DateDesc,CONVERT(VARCHAR,HPV_DATE,108) as HPV_DateTimeDesc" +
                ",HPV_PT_ID,HPV_PT_NAME,HPV_REFERRAL_TO,HPV_DR_ID,HPV_DR_NAME,HPV_VISIT_TYPE, HPV_COMP_ID ,HPV_COMP_NAME,HPV_PT_TYPE" +
                ",(SELECT CASE HPV_PT_TYPE WHEN 'CR' THEN 'Credit' when 'CA' THEN 'Cash'  ELSE HPV_PT_TYPE END ) AS HPV_PT_TYPEDesc", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");


            // DS = objCom.PatientVisitTopGet(Criteria, "500");


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvVisitDtlsAll.Visible = true;
                gvVisitDtlsAll.DataSource = DS;
                gvVisitDtlsAll.DataBind();

            }
            else
            {
                gvVisitDtlsAll.Visible = false;
                gvVisitDtlsAll.DataBind();
            }
        }

        void BindInvoice()
        {
            string Criteria = " 1=1  ";

            Criteria += " AND HIM_INVOICE_ID ='" + txtInvoiceNo.Text.Trim() + "'";


            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            DS = objInv.InvoiceMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;

                if (GlobalValues.FileDescription.ToUpper() == "NNMC")
                {
                    if (DS.Tables[0].Rows[0].IsNull("HIM_CLAIM_SUBMITTED") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CLAIM_SUBMITTED"]) != "")
                    {
                        btnSave.Visible = false;
                        btnDelete.Visible = false;
                        btnSaveAndEmail.Visible = false;

                    }
                    else
                    {
                        btnSave.Visible = true;
                        btnDelete.Visible = true;
                        btnSaveAndEmail.Visible = true;
                    }
                }

                drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]);

                txtTokenNo.Text = DS.Tables[0].Rows[0]["HIM_TOKEN_NO"].ToString();
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_STATUS") == false)
                    drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_STATUS"]);

                txtInvDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DATEDesc"]);

                DateTime dtInvoice = Convert.ToDateTime(DS.Tables[0].Rows[0]["HIM_DATE"]);
                txtInvTime.Text = dtInvoice.ToString("HH:mm:ss");



                //////if (txtFileNo.Text.Trim() != "OPDCR" && txtFileNo.Text.Trim() != "OPD")
                //////{
                //////    PatientDataBind();
                //////    if (txtFileNo.Text.Trim() == "ADAB") txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);
                //////}


                hidCompanyID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_BILLTOCODE"]);
                txtJobnumber.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_BUSINESSUNITCODE"]);
                txtJobnumber.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_JOBNUMBER"]);

                txtSubCompanyID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_CODE"]);
                strSubCompanyID = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_CODE"]);
                txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_NAME"]);
                txtSubCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_NAME"]);
                txtPlanType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_TYPE"]);

                txtPTComp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_COMP"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);
                txtPolicyExp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_EXPDesc"]);
                hidInsTrtnt.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_TRTNT_TYPE"]);


                TxtVoucherNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_VOUCHER_NO"]);

                txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_NAME"]);



                txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_CODE"]);
                txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_NAME"]);

                drpTreatmentType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_TYPE"]);

                //  txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_DR_CODE"]);
                if (DS.Tables[0].Rows[0].IsNull("HIM_REF_DR_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_DR_CODE"]) != "")
                {
                    txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_DR_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_DR_NAME"]);
                }

                txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REMARKS"]);
                txtEMRID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_EMR_ID"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_MOBILE_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_MOBILE_NO"]) != "")
                {
                    lblMobileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_MOBILE_NO"]);
                }

                txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_AUTHID"]);
                txtEligibilityID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ELIGIBILITY_ID"]);
                txtTotalSession.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TOTAL_SESSION"]);
                txtAvailableSession.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_AVAILABLE_SESSION"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_NURSE_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_NURSE_ID"]) != "")
                {

                    drpNurse.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_NURSE_ID"]);
                }

                if (drpRemarks.Items.Count > 0)
                {
                    drpRemarks.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REMARKS"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_CLAIM_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]) != "")
                {
                    decimal decClaimAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                    txtClaimAmt.Text = decClaimAmt.ToString("#0.00");
                }


                if (DS.Tables[0].Rows[0].IsNull("HIM_GROSS_TOTAL") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]) != "")
                {
                    decimal decBillAmt1 = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]);
                    txtBillAmt1.Text = decBillAmt1.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_HOSP_DISC") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_HOSP_DISC"]) != "")
                {
                    decimal decHospDisc = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_HOSP_DISC"]);
                    txtHospDisc.Text = decHospDisc.ToString("#0.00");
                }


                if (txtBillAmt1.Text.Trim() != "" && txtHospDisc.Text != "")
                {
                    decimal decBillAmt2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - Convert.ToDecimal(txtHospDisc.Text);
                    txtBillAmt2.Text = decBillAmt2.ToString("#0.00");
                }




                if (DS.Tables[0].Rows[0].IsNull("HIM_NET_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_NET_AMOUNT"]) != "")
                {
                    decimal decBillAmt3 = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_NET_AMOUNT"]);
                    txtBillAmt3.Text = decBillAmt3.ToString("#0.00");
                }




                if (DS.Tables[0].Rows[0].IsNull("HIM_PT_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_AMOUNT"]) != "")
                {
                    decimal decPTAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_PT_AMOUNT"]);
                    txtBillAmt4.Text = decPTAmt.ToString("#0.00");
                }



                drpHospDiscType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_HOSP_DISC_TYPE"]);


                if (DS.Tables[0].Rows[0].IsNull("HIM_HOSP_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_HOSP_DISC_AMT"]) != "")
                {
                    decimal decHospDiscAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_HOSP_DISC_AMT"]);
                    txtHospDiscAmt.Text = decHospDiscAmt.ToString("#0.00");
                }


                if (DS.Tables[0].Rows[0].IsNull("HIM_DEDUCTIBLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_DEDUCTIBLE"]) != "")
                {
                    decimal decDect = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_DEDUCTIBLE"]);
                    txtDeductible.Text = decDect.ToString("#0.00");
                }


                if (DS.Tables[0].Rows[0].IsNull("HIM_CO_INS_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CO_INS_AMOUNT"]) != "")
                {
                    decimal decCoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_CO_INS_AMOUNT"]);
                    txtCoInsTotal.Text = decCoInsAmt.ToString("#0.00");
                }

                hidCoInsType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CO_INS_TYPE"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_SPL_DISC") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_SPL_DISC"]) != "")
                {
                    decimal decSplDisc = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_SPL_DISC"]);
                    txtSplDisc.Text = decSplDisc.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_PT_CREDIT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_CREDIT"]) != "")
                {
                    decimal decPTCredit = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_PT_CREDIT"]);
                    txtPTCredit.Text = decPTCredit.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_PAID_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]) != "")
                {
                    decimal decPaidAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]);
                    txtPaidAmount.Text = decPaidAmt.ToString("#0.00");
                    hidPaidAmtCheck.Value = decPaidAmt.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_PAYMENT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpPayType.Items.Count; intCount++)
                    {
                        if (drpPayType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]))
                        {
                            drpPayType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]);


                            goto fordrpPayType;
                        }

                    }
                }
            fordrpPayType: ;

                if (drpPayType.SelectedValue == "Credit Card")
                {
                    divCC.Visible = true;

                }
                if (drpPayType.SelectedValue == "Cheque")
                {
                    divCheque.Visible = true;

                }
                if (drpPayType.SelectedValue == "Advance")
                {
                    divAdvane.Visible = true;

                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_CUR_TYP") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_TYP"]) != "")
                {
                    for (int intCount = 0; intCount < drpCurType.Items.Count; intCount++)
                    {
                        if (drpCurType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_TYP"]))
                        {
                            drpCurType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_TYP"]);
                            goto forCurType;
                        }

                    }
                }
            forCurType: ;

                txtConvRate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_RATE"]);
                txtcurValue.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_VALUE"]);

                txtRcvdAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_RCVD_AMT"]);
                hidReceipt.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_RECEIPT_NO"]);
                hidType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TYPE"]);


                if (DS.Tables[0].Rows[0].IsNull("HIM_COMMISSION_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMISSION_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpCommissionType.Items.Count; intCount++)
                    {
                        if (drpCommissionType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMISSION_TYPE"]))
                        {
                            drpCommissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMISSION_TYPE"]);
                            goto forCommissionType;
                        }

                    }
                }
            forCommissionType: ;

                txtAdvAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ADV_AMT"]);

                hidccText.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_TRTNT_TYPE_01") == false)
                    txtICDDesc.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_TYPE_01"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_TRTNT_Type_code") == false)
                    txtICdCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_Type_code"]);

                chkReadyforEclaim.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HIM_READYFORECLAIM"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_TOPUP") == false)
                    ChkTopupCard.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HIM_TOPUP"]);
                TxtTopupAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TOPUP_AMT"]);



                if (DS.Tables[0].Rows[0].IsNull("HIM_CC_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpccType.Items.Count; intCount++)
                    {
                        if (drpccType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_TYPE"]))
                        {
                            drpccType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_TYPE"]);
                            goto forccType;
                        }

                    }
                }
            forccType: ;

                txtCCHolder.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_NAME"]);
                txtCCNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_NO"]);
                txtCCRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_NO"]);

                txtDcheqno.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CHEQ_NO"]);
                txtCashAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CASH_AMT"]);
                txtCCAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_AMT"]);
                txtChqAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CHEQUE_AMT"]);


                if (DS.Tables[0].Rows[0].IsNull("HIM_BANK_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_BANK_NAME"]) != "")
                {
                    for (int intCount = 0; intCount < drpBank.Items.Count; intCount++)
                    {
                        if (drpBank.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_BANK_NAME"]))
                        {
                            drpBank.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_BANK_NAME"]);
                            goto forBank;
                        }

                    }
                }
            forBank: ;



                txtDcheqdate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CHEQ_DATE"]);
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_ORDER"]);



                lblCreatedUser.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CREATED_USER"]);
                lblModifiedUser.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_MODIFIED_USER"]);



                ViewState["HIM_INS_VERIFY_STATUS"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_VERIFY_STATUS"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_ATTACHMENT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_ATTACHMENT"]) != "")
                {
                    ViewState["AttachmentData"] = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ATTACHMENT"]);
                    DeleteAttach.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_ATTACHMENT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_ATTACHMENT_NAME"]) != "")
                {
                    lblFileNameActual.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ATTACHMENT_NAME"]);

                }


                PatientDeductibleBind();

            }

        }

        void BuildeInvoiceTrans()
        {
            string Criteria = "1=2";
            DataTable dt = new DataTable();
            DataSet DS = new DataSet();

            DS = dbo.InvoiceTransGet(Criteria);

            ViewState["InvoiceTrans"] = DS.Tables[0];
        }

        void BindInvoiceTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.InvoiceTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["InvoiceTrans"] = DS.Tables[0];

            }

        }


        void BindTempInvoiceTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceTrans"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceTrans.DataSource = DT;
                gvInvoiceTrans.DataBind();



                gvInvoiceTrans.Columns[12].Visible = false; //Co-Total
                gvInvoiceTrans.Columns[13].Visible = false;    //'Comp. Type
                gvInvoiceTrans.Columns[14].Visible = false;    //Comp. Amount
                gvInvoiceTrans.Columns[15].Visible = false; //Comp Total

                gvInvoiceTrans.Columns[18].Visible = false; //Serv. Category
                gvInvoiceTrans.Columns[19].Visible = false;    //'Serv. Treatment Type
                gvInvoiceTrans.Columns[20].Visible = false;    //Covered
                gvInvoiceTrans.Columns[21].Visible = false; //Serv. Type
                gvInvoiceTrans.Columns[22].Visible = false;    //'RefNo.
                gvInvoiceTrans.Columns[23].Visible = false;    //'TeethNo.

                gvInvoiceTrans.Columns[28].Visible = false;// 'CostPrice.

                if (Convert.ToString(ViewState["HSOM_COSTPRICE_BILLING"]).ToUpper() == "Y")
                {
                    gvInvoiceTrans.Columns[28].Visible = true; // 'CostPrice.
                }



            }
            else
            {
                gvInvoiceTrans.DataBind();
            }

        }
        
        void BuildeInvoiceClaims()
        {

            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn HIC_ICD_CODE = new DataColumn();
            HIC_ICD_CODE.ColumnName = "HIC_ICD_CODE";

            DataColumn HIC_ICD_DESC = new DataColumn();
            HIC_ICD_DESC.ColumnName = "HIC_ICD_DESC";


            DataColumn HIC_ICD_TYPE = new DataColumn();
            HIC_ICD_TYPE.ColumnName = "HIC_ICD_TYPE";

            DataColumn HIC_YEAR_OF_ONSET_P = new DataColumn();
            HIC_YEAR_OF_ONSET_P.ColumnName = "HIC_YEAR_OF_ONSET_P";


            dt.Columns.Add(HIC_ICD_CODE);
            dt.Columns.Add(HIC_ICD_DESC);
            dt.Columns.Add(HIC_ICD_TYPE);
            dt.Columns.Add(HIC_YEAR_OF_ONSET_P);

            ViewState["InvoiceClaims"] = dt;
        }
        
        void BindInvoiceClaimDtls()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND  HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.InvoiceClaimDtlsGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {


                drpClmType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_TYPE"]);
                txtClmStartDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTDATEDesc"]);
                txtClmFromTime.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTTimeDesc"]);

                txtClmEndDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATEDesc"]);
                txtClmToTime.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATETimeDesc"]);


                drpClmStartType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTTYPE"]);
                drpClmEndType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDTYPE"]);





                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["InvoiceClaims"];

                    DataRow objrow;
                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);

                        objrow["HIC_ICD_TYPE"] = "Principal";
                        objrow["HIC_YEAR_OF_ONSET_P"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_YEAR_OF_ONSET_P"]);

                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);


                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HIC_ICD_TYPE"] = "Admitting";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();
                                //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }

                    for (int j = 1; j <= 10; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_R" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_R" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_R" + j) == false)
                            {
                                objrow = DT.NewRow();
                                //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_R" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_R" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "ReasonForVisit";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }


                }

                ViewState["InvoiceClaims"] = DT;

            }
        }

        void BindTempInvoiceClaims()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceClaims"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceClaims.DataSource = DT;
                gvInvoiceClaims.DataBind();


                drpDiagType.SelectedIndex = 1;

            }
            else
            {
                gvInvoiceClaims.DataBind();
            }

        }

        void GetPackageServiceIDs(string ServiceID, out string PackageServiceIDs)
        {
            PackageServiceIDs = "";

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HPSM_PACK_SERV_CODE='" + ServiceID + "'";

            DS = objCom.fnGetFieldValue("HPST_SERV_CODE", "HMS_PACKAGE_SERVICE_MASTER INNER JOIN  HMS_PACKAGE_SERVICE_TRANS " +
                   " ON HPSM_PACKAGE_ID = HPST_PACKAGE_ID ", Criteria, "");

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (PackageServiceIDs != "")
                {
                    PackageServiceIDs += "," + "'" + Convert.ToString(DR["HPST_SERV_CODE"]) + "'";
                }
                else
                {
                    PackageServiceIDs = "'" + Convert.ToString(DR["HPST_SERV_CODE"]) + "'";
                }
            }

        }

        void BindSalesOrderServ()
        {
            ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindSalesOrderServ() Start");

            ProcessTimLog(System.DateTime.Now.ToString() + "      DrSalesOrderTransGet() Start");
            string Criteria = " 1=1 ";
            Criteria += " AND DSM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND DSM_DATE=CONVERT(DATETIME,'" + txtInvDate.Text.Trim() + "',103) AND DSM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
            //Criteria += " AND  DSM_DOCTOR_TRANS_ID ='" + Convert.ToString(ViewState["HPV_EMR_ID"]) + "'";

            if (Convert.ToString(Session["SO_ADD_ONLY_APPROVED_SERV"]) == "1")
            {
                if (drpInvType.SelectedValue == "Customer" || drpInvType.SelectedValue == "Cash")
                {
                    Criteria += " AND  ISNULL(DST_INS_APPROVAL_STATUS,'PENDING')  <>  'APPROVED' ";
                }
                else
                {
                    Criteria += " AND  DST_INS_APPROVAL_STATUS  = 'APPROVED' ";
                }
            }

            clsInvoice objInv = new clsInvoice();

            DataSet DS = new DataSet();
            DS = objInv.DrSalesOrderTransGet(Criteria);
            ProcessTimLog(System.DateTime.Now.ToString() + "      DrSalesOrderTransGet() End");


            string strHaadIds = "", strServIds = "", strCompanyWiseServIds = "", strServConditions = "", strAgreConditions = "";
            if (DS.Tables[0].Rows.Count > 0)
            {

                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    string strServCode = Convert.ToString(DS.Tables[0].Rows[i]["DST_SERV_CODE"]);
                    string strObsCode = Convert.ToString(DS.Tables[0].Rows[i]["DST_OBS_CODE"]);

                    string strServCodeType = "";
                    if (DS.Tables[0].Rows[i].IsNull("DST_SERV_CODE_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["DST_SERV_CODE_TYPE"]) != "")
                    {
                        strServCodeType = Convert.ToString(DS.Tables[0].Rows[i]["DST_SERV_CODE_TYPE"]);
                    }

                    //  string ServiseID="", CompWiseServID="";
                    //  ServiseID = GetServiceMasterID("HSM_HAAD_CODE", strServCode);

                    //if (drpInvType.SelectedValue == "Credit")
                    //{
                    //    GetCompanywiseServiceID(strServCode, out CompWiseServID);
                    //}

                    if (strHaadIds != "")
                    {
                        strHaadIds += ",'" + strServCode + "'";
                        //  strServIds += ",'" + ServiseID + "'";
                        strServIds += ",'" + strServCode + "'";

                        string PackageServiceIDs = "";
                        GetPackageServiceIDs(strServCode, out PackageServiceIDs);

                        if (PackageServiceIDs != "")
                        {

                            strServConditions += " OR (HSM_SERV_ID IN  (" + PackageServiceIDs + ") )";
                            strAgreConditions += " OR (HAS_SERV_ID  IN (" + PackageServiceIDs + ") )";

                        }
                        else
                        {

                            //if (strServCodeType != "")
                            //{

                            //    strServConditions += " OR (HSM_SERV_ID ='" + strServCode + "' AND HSM_CODE_TYPE=" + strServCodeType + ")";
                            //    strAgreConditions += " OR (HAS_SERV_ID ='" + strServCode + "' AND HAS_CODE_TYPE=" + strServCodeType + ")";


                            //}
                            //else
                            //{

                                //strServConditions += " OR (HSM_SERV_ID ='" + strServCode + "')";
                                //strAgreConditions += " OR (HAS_SERV_ID ='" + strServCode + "')";

                           // }

                            strServConditions = "  (HSM_SERV_ID ='" + strServCode + "')";
                            strAgreConditions = "  (HAS_SERV_ID ='" + strServCode + "')";
                        }

                        //if (CompWiseServID != "")
                        //{
                        //    strCompanyWiseServIds += ",'" + CompWiseServID + "'";
                        //}
                        //else
                        //{
                        //    strCompanyWiseServIds += ",'" + ServiseID + "'";
                        //}
                    }
                    else
                    {
                        strHaadIds = "'" + strServCode + "'";
                        //   strServIds = "'" + ServiseID + "'";
                        strServIds = "'" + strServCode + "'";
                        string PackageServiceIDs = "";
                        GetPackageServiceIDs(strServCode, out PackageServiceIDs);

                        if (PackageServiceIDs != "")
                        {
                            strServConditions += "  HSM_SERV_ID IN  (" + PackageServiceIDs + ")";
                            strAgreConditions += "  HAS_SERV_ID  IN (" + PackageServiceIDs + ")";
                        }
                        else
                        {

                            //if (strServCodeType != "")
                            //{
                            //    strServConditions += " (HSM_SERV_ID ='" + strServCode + "' AND HSM_CODE_TYPE=" + strServCodeType + ")";
                            //    strAgreConditions += " (HAS_SERV_ID ='" + strServCode + "' AND HAS_CODE_TYPE=" + strServCodeType + ")";
                            //}
                            //else
                            //{
                                //strServConditions += "  (HSM_SERV_ID ='" + strServCode + "')";
                                //strAgreConditions += "  (HAS_SERV_ID ='" + strServCode + "')";
                           // }

                            strServConditions = "  (HSM_SERV_ID ='" + strServCode + "')";
                            strAgreConditions = "  (HAS_SERV_ID ='" + strServCode + "')";
                        }

                        // if (CompWiseServID != "")
                        //{
                        //    strCompanyWiseServIds = "'" + CompWiseServID + "'";
                        //}
                        //else
                        //{
                        //    strCompanyWiseServIds = "'" + ServiseID + "'";
                        //}
                    }

                    ViewState["SO_HAAD_IDS"] = strHaadIds;

                    fnServiceAddFromSO(strServConditions, strAgreConditions, strObsCode);
                }

                // if(drpInvType.SelectedValue == "Cash")
                //{
                // fnServiceAddFromSO(strServIds);

              ////  fnServiceAddFromSO(strServConditions, strAgreConditions);
                //}
                //else
                //{
                //    fnServiceAddFromSO(strCompanyWiseServIds);
                //}


            }
        }

        void BindSalesOrderDiag()
        {
            ProcessTimLog(System.DateTime.Now.ToString() + "      BindSalesOrderDiag() Start");

            if (string.IsNullOrEmpty(txtEMRID.Text.Trim()) == true)
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            EMR_PT_DiagnosisBAL objPTDiag = new EMR_PT_DiagnosisBAL();
            string Criteria = " 1=1 ";
            Criteria += " and EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID ='" + txtEMRID.Text.Trim() + "'";

            DS = objPTDiag.GetEMR_PTDiagnosis(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();

                Int32 i = 0;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DT = (DataTable)ViewState["InvoiceClaims"];
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["HIC_ICD_CODE"] = DR["EPD_DIAG_CODE"];
                    objrow["HIC_ICD_DESC"] = DR["EPD_DIAG_NAME"];
                    objrow["HIC_ICD_TYPE"] = DR["EPD_TYPE"];

                    //if (i == 0)
                    //{
                    //    objrow["HIC_ICD_TYPE"] = "Principal";
                    //}
                    //else
                    //{
                    //    objrow["HIC_ICD_TYPE"] = "Secondary";

                    //}

                    objrow["HIC_YEAR_OF_ONSET_P"] = DR["EPD_YEAR_OF_ONSET"];
                    DT.Rows.Add(objrow);

                    i = i + 1;
                }

                ViewState["InvoiceClaims"] = DT;

                BindTempInvoiceClaims();
                ProcessTimLog(System.DateTime.Now.ToString() + "      BindSalesOrderDiag() End");
            }


        FunEnd: ;

        }

        void Clear()
        {
            btnSave.Visible = true;
            btnDelete.Visible = true;
            btnSaveAndEmail.Visible = true;

            //CommonBAL objCom = new CommonBAL();
            //string strDate = "", strTime = ""; ;
            //strDate = objCom.fnGetDate("dd/MM/yyyy");
            //strTime = objCom.fnGetDate("HH:mm:ss");

            //txtInvDate.Text = strDate;
            //txtInvTime.Text = strTime;

            //txtClmStartDate.Text = strDate;
            //txtClmFromTime.Text = strTime.Substring(0, 5);

            //txtClmEndDate.Text = strDate;
            //txtClmToTime.Text = strTime.Substring(0, 5);


            hidInvoiceType.Value = "";

            ViewState["NewFlag"] = true;
            //if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
            //{
            //    drpInvType.SelectedIndex = 1;
            //}
            //else
            //{
            //    drpInvType.SelectedIndex = 0;
            //}

            txtTokenNo.Text = "";
            //txtFileNo.Text = "";
            txtName.Text = "";

            if (drpStatus.Items.Count > 0)
                drpStatus.SelectedIndex = 0;


            txtJobnumber.Text = "";
            txtJobnumber.Text = "";

            txtSubCompanyID.Text = "";
            txtCompanyName.Text = "";
            txtSubCompanyName.Text = "";
            txtPlanType.Text = "";
            txtPTComp.Text = "";
            txtPolicyNo.Text = "";
            txtIdNo.Text = "";
            txtPolicyExp.Text = "";
            lblEmailID.Text = "";
            lblMobileNo.Text = "";
            txtPayerID.Text = "";
            txtReciverID.Text = "";

            TxtVoucherNo.Text = "";

            txtEMRID.Text = "";
            txtChiefComplaints.Text = "";
            txtDoctorID.Text = "";
            txtDoctorName.Text = "";
            hidDrDept.Value = "";
            txtOrdClinCode.Text = "";
            txtOrdClinName.Text = "";

            if (drpTreatmentType.Items.Count > 0)
                drpTreatmentType.SelectedIndex = 0;

            // txtRefDrId.Text = "";
            txtRefDrName.Text = "";
            txtRemarks.Text = "";

            if (drpRemarks.Items.Count > 0)
                drpRemarks.SelectedIndex = 0;

            txtClaimAmt.Text = "0.00";
            txtBillAmt1.Text = "0.00";
            txtBillAmt2.Text = "0.00";
            txtBillAmt3.Text = "0.00";
            txtBillAmt4.Text = "0.00";
            if (drpHospDiscType.Items.Count > 0)
                drpHospDiscType.SelectedIndex = 0;

            txtHospDisc.Text = "0.00";
            txtHospDiscAmt.Text = "0.00";
            txtDeductible.Text = "0.00";
            txtCoInsTotal.Text = "0.00";

            txtSplDisc.Text = "0.00";
            txtPTCredit.Text = "0.00";
            txtPaidAmount.Text = "0.00";
            TxtRcvdAmt.Text = "0.00";


            txtTotalTaxAmt.Text = "0.00";
            txtTotalClaimTaxAmt.Text = "0.00";
            txtEligibilityID.Text = "";

            txtTotalSession.Text = "";
            txtAvailableSession.Text = "";

            if (drpPayType.Items.Count > 0)
                drpPayType.SelectedIndex = 0;

            if (drpCurType.Items.Count > 0)
                drpCurType.SelectedIndex = 0;

            if (drpNurse.Items.Count > 0)
                drpNurse.SelectedIndex = 0;


            txtConvRate.Text = "";
            txtcurValue.Text = "";

            txtRcvdAmount.Text = "";


            if (drpCommissionType.Items.Count > 0)
                drpCommissionType.SelectedIndex = 0;


            txtAdvAmt.Text = "";

            txtICDDesc.Text = "";
            txtICdCode.Text = "";

            ChkTopupCard.Checked = false;
            TxtTopupAmt.Text = "";

            if (drpccType.Items.Count > 0)
                drpccType.SelectedIndex = 0;


            txtCCHolder.Text = "";
            txtCCNo.Text = "";
            txtCCRefNo.Text = "";

            txtDcheqno.Text = "";
            txtCashAmt.Text = "";
            txtCCAmt.Text = "";
            txtChqAmt.Text = "";

            if (drpBank.Items.Count > 0)
                drpBank.SelectedIndex = 0;

            txtDcheqdate.Text = "";
            txtTokenNo.Text = "";



            BuildeInvoiceTrans();
            gvInvoiceTrans.DataBind();

            BuildeInvoiceClaims();
            gvInvoiceClaims.DataBind();

            ViewState["Deductible_Type"] = "";
            ViewState["Deductible"] = "0";
            ViewState["Co_Ins_Type"] = "";
            ViewState["Co_Ins_Amt"] = "0";


            ViewState["Co_Ins_TypeDental"] = "";
            ViewState["Co_Ins_AmtDental"] = "0";

            ViewState["Co_Ins_TypePharmacy"] = "";
            ViewState["Co_Ins_AmtPharmacy"] = "0";

            ViewState["Co_Ins_TypeLab"] = "";
            ViewState["Co_Ins_AmtLab"] = "0";

            ViewState["Co_Ins_TypeRad"] = "";
            ViewState["Co_Ins_AmtRad"] = "0";


            ViewState["COINSFROMGROSS_CLAIMFROMNET"] = "";

            lblVisitType.Text = "";
            hidVisitID.Value = "";
            hidCompanyID.Value = "";
            hidCmpRefCode.Value = "";
            hidInsType.Value = "";
            hidIDNo.Value = "";
            hidInsTrtnt.Value = "";

            hidPTVisitType.Value = "";
            hidUpdateCodeType.Value = "";
            hidCompAgrmtType.Value = "";
            hidCoInsType.Value = "";
            hidReceipt.Value = "";
            hidType.Value = "";
            hidccText.Value = "";
            hidmasterinvoicenumber.Value = "";

            hidHaadID.Value = "";
            hidCatID.Value = "";
            hidCatType.Value = "";
            hidPaidAmtCheck.Value = "";


            divCC.Visible = false;
            divCheque.Visible = false;
            divAdvane.Visible = false;
            divMergeInvoiceNo.Visible = false;
            divMultiCurrency.Visible = false;

            if (drpClmType.Items.Count > 0)
                drpClmType.SelectedIndex = 0;
            if (drpClmStartType.Items.Count > 0)
                drpClmStartType.SelectedIndex = 0;
            if (drpClmEndType.Items.Count > 0)
                drpClmEndType.SelectedIndex = 0;


            if (gvInvoiceClaims.Rows.Count > 0)
            {
                drpDiagType.SelectedIndex = 1;
            }
            else
            {
                drpDiagType.SelectedIndex = 0;
            }

            TabContainer1.ActiveTab = tabService;

            hidMsgConfirm.Value = "";


            lblLstInvNoDate.Text = "";
            lblFstInvDate.Text = "";
            lblModifiedUser.Text = "";
            lblCreatedUser.Text = "";

            chkOnlySave.Checked = false;
            chkPrintCrBill.Checked = false;
            drpPrintType.SelectedIndex = 0;
            txtPriorAuthorizationID.Text = "";
            lblBalanceMsg.Text = "";

            chkNoMergePrint.Checked = false;

            gvAttachments.Visible = false;
            gvAttachments.DataBind();

            ViewState["HIM_INS_VERIFY_STATUS"] = "";


            //lblFileName.Text = "";
            lblFileNameActual.Text = "";
            ViewState["AttachmentData"] = "";
            DeleteAttach.Visible = false;

            divMessage.Style.Add("display", "none");
            txtSrcVisitFileNo.Text = "";

            ViewState["PATIENT_CLASS"] = "";
            ClearServicePopup();
        }

        void ClearServicePopup()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            txtServType.Text = "";

            txtServPrice.Text = "0.00";
            drpServDiscType.SelectedIndex = 0;
            txtServDiscAmt.Text = "0.00";
            txtServQty.Text = "1";
            txtServAmount.Text = "0.00";
            txtServDedect.Text = "0.00";
            drpServCoInsPres.SelectedIndex = 0;
            txtServCoInsAmount.Text = "0.00";
            txtServTaxAmount.Text = "0.00";
            txtServTaxPresent.Text = "0.00";
            txtServClaimTaxAmount.Text = "0.00";
            txtServHaadCode.Text = "";
            txtServHaadDesc.Text = "";

            txtServCostPrice.Text = "0.00";
            txtServAuthID.Text = "";
            txtServObsType.Text = "";
            txtServObsCode.Text = "";
            txtServObsDesc.Text = "";
            txtServObsValue.Text = "";
            txtServObsValueType.Text = "";

           



            ViewState["ServHAAD_ID"] = "";
            ViewState["ServCAT_ID"] = "";
            ViewState["ServCAT_type"] = "";
            ViewState["Serv_type"] = "";
            ViewState["Serv_Coverd"] = "";


            ViewState["FeeChanged"] = "";

            ViewState["DiscTypeChanged"] = "";
            ViewState["DiscAmtChanged"] = "";
            ViewState["CoInsPresChanged"] = "";
            ViewState["CoInsAmtChanged"] = "";
            ViewState["DectAmtChanged"] = "";

            ViewState["InvTransSelectIndex"] = "";

        }

        void New()
        {
            ViewState["NewFlag"] = true;
            txtFileNo.Text = "";

            CommonBAL objCom = new CommonBAL();
            string strDate = "", strTime = ""; ;
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            strTime = objCom.fnGetDate("HH:mm:ss");
            txtInvDate.Text = strDate;
            txtInvTime.Text = strTime;

            txtClmStartDate.Text = strDate;
            txtClmFromTime.Text = strTime;//.Substring(0, 8);

            txtClmEndDate.Text = strDate;
            txtClmToTime.Text = strTime;//.Substring(0, 8);


            txtServStartDt.Text = strDate;
            txtServStartTime.Text = strTime;




            txtInvoiceNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "INVOICE");

            drpStatus.SelectedValue = "Open";


        }

        void BindAttachments()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " and ESF_PT_ID='" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND ESF_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            DS = objCom.EMRScanFileGet(Criteria);

            gvAttachments.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAttachments.Visible = true;
                gvAttachments.DataSource = DS;
                gvAttachments.DataBind();
            }
            else
            {
                gvAttachments.DataBind();
            }

        }

        Boolean CheckEMR_ID()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "' AND EPM_ID=" + txtEMRID.Text.Trim();
            Criteria += " AND EPM_DR_CODE ='" + txtDoctorID.Text.Trim() + "'";
            DataSet DS = new DataSet();
            EMR_PTMasterBAL obj = new EMR_PTMasterBAL();
            DS = obj.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            MPExtMessage.Show();
            strMessage = "EMR ID is not valied ";
            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('EMR ID is not valied  ',' EMR ID is not valied ')", true);


            return false;
        }

        void BindEMRDtls()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "' AND EPM_ID=" + txtEMRID.Text.Trim();

            DataSet DS = new DataSet();
            EMR_PTMasterBAL obj = new EMR_PTMasterBAL();
            DS = obj.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //txtInvDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]);

                //if (DS.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                //{
                //    txtInvTime.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"].ToString()).ToString("HH:mm");
                //}





                if (DS.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                {
                    txtClmStartDate.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"].ToString()).ToString("dd/MM/yyyy ");
                    txtClmFromTime.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"].ToString()).ToString("HH:mm:ss");

                    txtServStartDt.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"].ToString()).ToString("dd/MM/yyyy ");
                    txtServStartTime.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"].ToString()).ToString("HH:mm:ss");

                }





                if (DS.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                {
                    txtClmEndDate.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_END_DATE"].ToString()).ToString("dd/MM/yyyy ");
                    txtClmToTime.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_END_DATE"].ToString()).ToString("HH:mm:ss");
                }

                txtChiefComplaints.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]);


            }





        }

        void GetChiefComplaints()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "' AND EPM_ID=" + txtEMRID.Text.Trim();

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" TOP 1 EPM_CC", "EMR_PT_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtChiefComplaints.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]);
            }
        }
        void BindCompanyBenefites()
        {

            string CriteriaCompBenDtls = " 1=1 ";

            if (txtSubCompanyID.Text == "")
            {
                CriteriaCompBenDtls += " AND  HCB_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  HCB_COMP_ID='" + hidCompanyID.Value + "' AND    HCB_CAT_TYPE='" + txtPlanType.Text.Trim() + "'";
            }
            else
            {
                CriteriaCompBenDtls += " AND  HCB_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  HCB_COMP_ID='" + txtSubCompanyID.Text.Trim() + "' AND    HCB_CAT_TYPE='" + txtPlanType.Text.Trim() + "'";

            }

            dboperations dbo = new dboperations();
            DataSet DSCompBenDtls = new DataSet();
            DSCompBenDtls = dbo.CompBenefitsGet(CriteriaCompBenDtls);

            if (DSCompBenDtls.Tables[0].Rows.Count > 0)
            {
                ViewState["COINSFROMGROSS_CLAIMFROMNET"] = Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]);
            }


        }

        void GetCompanyDtls(string InsCode, out string strPayerID, out string strReceiverID)
        {
            strPayerID = "";
            strReceiverID = "";

            DataSet DS = new DataSet();
            CompanyMasterBAL objeComp = new CompanyMasterBAL();

            string Criteria = " 1=1 ";
            Criteria += " and HCM_COMP_ID='" + InsCode + "'";

            DS = objeComp.GetCompanyMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_PAYERID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]) != "")
                {
                    strPayerID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD5") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]) != "")
                {
                    strReceiverID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]);
                }

                hidCompAgrmtType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                strCompAgrmtType = Convert.ToString(DS.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
            }

        }

        Boolean CheckYearOfOnSet(string Code)
        {
            string Criteria = " 1=1 AND  DIM_STATUS='A'";

            Criteria += " AND DIM_ICD_ID='" + Code + "'";
            Criteria += " AND DIM_REQ_YEAR_OF_ONSET=1";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            DS = dbo.DrICDMasterGet(Criteria, "1000");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;

        }

        Boolean CheckTaxApplicableYear()
        {
            string[] arrInvDate = txtInvDate.Text.Trim().Split('/');
            Int32 intInvYear = Convert.ToInt32(arrInvDate[2]);

            if (intInvYear >= Convert.ToInt32(hidTaxApplicableYear.Value))
            {
                return true;
            }

            return false;
        }
        #endregion


        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS ='Present'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS ='Present'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorNurseId(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY IN ('Doctors','Nurse') AND  HSFM_SF_STATUS ='Present' ";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorNurseName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY IN ('Doctors','Nurse') AND  HSFM_SF_STATUS ='Present' ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            if (strSubCompanyID != "" && strSubCompanyID != null && strCompAgrmtType == "S")
            {
                Criteria += " AND HAS_SERV_ID   like '" + prefixText + "%' ";

                Criteria += " AND   HAS_COMP_ID='" + strSubCompanyID + "'";
                DS = objCom.AgrementServiceTopGet(Criteria, "30");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    Data = new string[DS.Tables[0].Rows.Count];
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HAS_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HAS_SERV_NAME"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HAS_CODE_TYPEDesc"]);

                    }


                    return Data;
                }
            }
            else
            {
                Criteria += " AND  HSM_STATUS='A' ";

                Criteria += " AND HSM_SERV_ID   like '" + prefixText + "%' ";
                DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    Data = new string[DS.Tables[0].Rows.Count];
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_CODE_TYPEDesc"]);

                    }


                    return Data;
                }

            }


            string[] Data1 = { "" };

            return Data1;

        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            if (strSubCompanyID != "" && strSubCompanyID != null && strCompAgrmtType == "S")
            {
                Criteria += " AND HAS_SERV_NAME   like '" + prefixText + "%' ";

                Criteria += " AND   HAS_COMP_ID='" + strSubCompanyID + "'";
                DS = objCom.AgrementServiceTopGet(Criteria, "30");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    Data = new string[DS.Tables[0].Rows.Count];
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HAS_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HAS_SERV_NAME"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HAS_CODE_TYPEDesc"]);

                    }


                    return Data;
                }
            }
            else
            {
                Criteria += " AND  HSM_STATUS='A' ";

                Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
                DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

                if (DS.Tables[0].Rows.Count > 0)
                {
                    Data = new string[DS.Tables[0].Rows.Count];
                    for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_CODE_TYPEDesc"]);

                    }


                    return Data;
                }

            }


            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisID(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_ID  like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_DESCRIPTION   like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetRefDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HRDM_FNAME   like '" + prefixText + "%' ";
            Criteria += " AND HRDM_BRANCH_ID ='" + strSessionBranchId + "'";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HRDM_REF_ID,HRDM_FNAME", "HMS_REF_DOCTOR_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HRDM_REF_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HRDM_FNAME"]);
                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetOrderingDoctorID(string prefixText)
        {
            string[] Data = null;

            string criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";
            criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            StaffMasterBAL objStaff = new StaffMasterBAL();
            ds = objStaff.GetStaffMaster(criteria);
            int StaffCount = ds.Tables[0].Rows.Count;


            criteria = " 1=1 ";
            criteria += " AND HRDM_REF_ID Like '%" + prefixText + "%'";
            DataSet ds1 = new DataSet();
            dboperations dbo = new dboperations();
            ds1 = dbo.RefDoctorMasterGet(criteria);
            int RefStaffCount = ds1.Tables[0].Rows.Count;

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[StaffCount + RefStaffCount];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

            }


            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                {
                    Data[StaffCount + j] = Convert.ToString(ds1.Tables[0].Rows[j]["HRDM_REF_ID"]) + "~" + Convert.ToString(ds1.Tables[0].Rows[j]["FullName"]);
                }

                return Data;
            }
            else
            {
                return Data;
            }

            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetOrderingDoctorName(string prefixText)
        {
            string[] Data = null;

            string criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";
            criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds = new DataSet();
            StaffMasterBAL objStaff = new StaffMasterBAL();
            ds = objStaff.GetStaffMaster(criteria);
            int StaffCount = ds.Tables[0].Rows.Count;


            criteria = " 1=1 ";
            criteria += " AND HRDM_FNAME + ' ' +isnull(HRDM_MNAME,'') + ' '  + isnull(HRDM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds1 = new DataSet();
            dboperations dbo = new dboperations();
            ds1 = dbo.RefDoctorMasterGet(criteria);
            int RefStaffCount = ds1.Tables[0].Rows.Count;

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[StaffCount + RefStaffCount];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

            }


            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                {
                    Data[StaffCount + j] = Convert.ToString(ds1.Tables[0].Rows[j]["HRDM_REF_ID"]) + "~" + Convert.ToString(ds1.Tables[0].Rows[j]["FullName"]);
                }

                return Data;
            }
            else
            {
                return Data;
            }

            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        Boolean fnReVisitType()
        {

            string Criteria = " 1=1  ";

            Criteria += " AND   HIT_SERV_TYPE='C' and HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            DS = objInv.InvoiceTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strLastVisit = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DATE"]);

                int intRevisit = Convert.ToInt32(Session["RevisitDays"]);
                if (strLastVisit != "")
                {
                    DateTime dtLastVisit = Convert.ToDateTime(strLastVisit);
                    DateTime Today;
                    //if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    //{
                    Today = System.DateTime.Now; //Convert.ToDateTime(hidTodayDBDate.Value);
                    //}
                    //else
                    //{
                    //    Today = Convert.ToDateTime(Convert.ToString(Session["LocalDate"]));
                    //}

                    TimeSpan Diff = Today.Date - dtLastVisit.Date;

                    if (Diff.Days <= intRevisit && Diff.Days >= 2)
                    {
                        return true;
                    }
                }


            }

            return false;
        }

        void fnMergeReport()
        {
            string Criteria = " 1=1 ";

            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "File No. should not be blank.";
                goto FunEnd;
            }

            string InvoiceNos = "";
            DataSet DSInv = new DataSet();
            CommonBAL objCom = new CommonBAL();

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND    HIM_DATE >= '" + strForStartDate + " 00:00:01'";
                Criteria += " AND    HIM_DATE <= '" + strForStartDate + " 23:59:59'";
            }

            Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND  HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "' AND HIM_SUB_INS_CODE='" + txtSubCompanyID.Text.Trim() + "'";

            if (txtFileNo.Text.Trim() == "OPDCR" || txtFileNo.Text.Trim() == "OPD")
            {
                Criteria += " and HIM_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'";
            }


            DSInv = objCom.fnGetFieldValue(" distinct(HIM_INVOICE_ID) HIM_INVOICE_ID  ", "HMS_INVOICE_MASTER", Criteria, "HIM_INVOICE_ID");

            if (DSInv.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < DSInv.Tables[0].Rows.Count; i++)
                {

                    if (InvoiceNos != "") InvoiceNos = InvoiceNos + @"\";

                    InvoiceNos = InvoiceNos + " " + Convert.ToString(DSInv.Tables[0].Rows[i]["HIM_INVOICE_ID"]);
                }

            }

            string InvDate = txtInvDate.Text.Trim();
            string InvoiceNo = txtInvoiceNo.Text.Trim();
            string FileNo = txtFileNo.Text.Trim();
            string DrId = txtDoctorID.Text.Trim();
            string SubCompId = txtSubCompanyID.Text.Trim();
            string PolicyNo = txtPolicyNo.Text.Trim();
            string NoMergePrint = "N";
            if (chkNoMergePrint.Checked == true)
            {
                NoMergePrint = "Y";
            }
            else
            {
                NoMergePrint = "Y";
            }





            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMergeReport('" + Convert.ToString(Session["Branch_ID"]) + "','" + InvoiceNo + "','" + InvDate + "','" + FileNo + "','" + DrId + "','" + SubCompId + "','" + PolicyNo + "','" + NoMergePrint + "');", true);


        FunEnd: ;

        }

        void fnPrint(string strInvoiceNo)
        {
            ReportName = "";
            strPrintType = "";

            if (chkOnlySave.Checked == true && chkPrintCrBill.Checked == false)
            {
                goto FunEnd;
            }

            if (GlobalValues.FileDescription == "RASHIDIYA")
            {
                goto pr1;
            }
            else
            {
                if (drpStatus.SelectedValue == " Hold" && drpStatus.SelectedValue == "Void")
                {
                    goto FunEnd;
                }

            }
        pr1: ;

            if (chkOnlySave.Checked == false)
            {

                if (drpPrintType.SelectedIndex != 0)
                {
                    strPrintType = drpPrintType.SelectedValue;
                    strPrintType = strPrintType.Substring(0, 5);

                }

                if (strPrintType == "Print")
                {
                    ReportName = "HmsInvoice" + drpPrintType.SelectedValue + ".rpt";
                }

                else if (drpInvType.SelectedValue == "Cash")
                {
                    if (GlobalValues.InvoiceType == "PERFORMA")
                    {
                        ReportName = "HmsInvoiceCa_PERFORMA.rpt";
                    }
                    else
                    {
                        ReportName = "HmsInvoiceCa.rpt";
                    }

                }
                else if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    if (GlobalValues.InvoiceType == "PERFORMA")
                    {
                        ReportName = "HmsInvoiceCr_performa.rpt";
                    }
                    else
                    {
                        ReportName = "HmsInvoiceCr.rpt";
                    }
                }

                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + strInvoiceNo + "','" + ReportName + "');", true);

            }

            if (chkPrintCrBill.Checked == true && (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer"))
            {
                if (GlobalValues.FileDescription == "SMCH")// And HMS_INSURANCE Then
                {
                    fnMergeReport();

                }
                else
                {
                    string strRpt = "";

                    if (GlobalValues.InvoiceType != "")
                    {
                        strRpt = "_" + GlobalValues.InvoiceType;
                    }

                    if (strPrintType == "Print")
                    {
                        ReportName = "HmsInvoice" + drpPrintType.SelectedValue + ".rpt";
                    }

                    else if (drpPrintType.SelectedValue == "Net")
                    {
                        ReportName = "HmsCreditBill1" + strRpt + ".rpt";
                    }
                    else if (drpPrintType.SelectedIndex == 3)// 'credit note
                    {
                        ReportName = "HmsCreditBill2" + strRpt + ".rpt";
                    }
                    else if (drpPrintType.SelectedIndex == 1) //gross
                    {
                        ReportName = "HmsCreditBill0" + strRpt + ".rpt";
                    }
                    else
                    {
                        //if( UCase(Trim(txttype.Text)) = "PHY" )
                        //      ReportName ="IVaissdetA5.rpt";
                        //else
                        //{
                        if (GlobalValues.InvoiceType == "PERFORMA")
                            ReportName = "msCreditBill_performa.rpt";
                        else
                        {
                            ReportName = "HmsCreditBill.rpt";

                            if (GlobalValues.FileDescription == "MASFOOT")
                            {
                                String RptName = "";
                                string Criteria = " 1=1 ";
                                Criteria += " AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HCM_COMP_ID ='" + txtSubCompanyID.Text.Trim() + "'";
                                DataSet DS = new DataSet();
                                CompanyMasterBAL objComp = new CompanyMasterBAL();
                                objComp.GetCompanyMaster(Criteria);

                                if (DS.Tables[0].Rows.Count > 0)
                                {
                                    if (DS.Tables[0].Rows[0].IsNull("INVOICERPT") == false)
                                        RptName = Convert.ToString(DS.Tables[0].Rows[0]["INVOICERPT"]);

                                }
                                if (RptName != "")
                                    ReportName = "HmsCreditBill" + RptName + ".rpt";
                                else
                                    ReportName = "HmsCreditBill.rpt";

                            }
                        }
                    }
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + Convert.ToString(Session["Branch_ID"]) + "','" + strInvoiceNo + "','" + ReportName + "');", true);


                }


            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + Convert.ToString(Session["Branch_ID"]) + "','" + strInvoiceNo + "','" + ReportName + "');", true);

            }



        FunEnd: ;
        }

        void PrintCrBill()
        {
            if (chkPrintCrBill.Checked == true && drpInvType.SelectedValue == "Credit")
            {

                if (txtInvoiceNo.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintCrBill('" + txtInvoiceNo.Text.Trim() + "');", true);
            }

        FunEnd: ;

        }

        void PrintInvoice()
        {
            if (chkOnlySave.Checked == false)
            {

                if (txtInvoiceNo.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + txtInvoiceNo.Text.Trim() + "','" + drpInvType.SelectedValue + "');", true);
            }

        FunEnd: ;

        }

        void fnLastConsltDate()
        {
            lblLstInvNoDate.Text = "";
            lblFstInvDate.Text = "";
            clsInvoice objInv = new clsInvoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DataSet DSVisitType = new DataSet();
            DSVisitType = objInv.ReVisitTypeGet(Criteria);
            if (DSVisitType.Tables[0].Rows.Count > 0)
            {
                lblLstInvNoDate.Text = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDesc"]);

                lblFstInvDate.Text = Convert.ToString(DSVisitType.Tables[0].Rows[DSVisitType.Tables[0].Rows.Count - 1]["HIM_DATEDesc"]);
            }
        }

        void GetLabValue(string strServCode, decimal nConversion, out string ObsValue, out string ObsValueType)
        {
            ObsValue = "";
            ObsValueType = "";
            string strLabValue = "";
            decimal decLabValue = 0;
            clsInvoice objInv = new clsInvoice();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND convert(varchar(10),dbo.LAB_TEST_REPORT_MASTER.LTRM_TEST_DATE ,103)='" + txtInvDate.Text.Trim() + "'" +
            Criteria += " AND ( dbo.LAB_TEST_REPORT_MASTER.LTRM_TEST_DATE  >= dateadd(DD,-1, convert(datetime,'" + txtInvDate.Text.Trim() + "',103)) " +
              " AND dbo.LAB_TEST_REPORT_MASTER.LTRM_TEST_DATE  <= dateadd(DD,3, convert(datetime,'" + txtInvDate.Text.Trim() + "',103))  OR  LTRM_EMR_ID='" + txtEMRID.Text.Trim() + "')" +
              "	AND dbo.LAB_TEST_REPORT_MASTER.LTRM_PATIENT_ID = '" + txtFileNo.Text.Trim() + "'" +
              " AND dbo.LAB_TEST_REPORT_MASTER.LTRM_DR_CODE = '" + txtDoctorID.Text.Trim() + "'" +
                //  "   AND dbo.LAB_TEST_PROFILE_MASTER.LTPM_SERV_ID ='" + strServCode + "'";
             "   AND dbo.LAB_TEST_REPORT_DETAIL.LTRD_SERV_CODE ='" + strServCode + "'";


            DS = objInv.GetLabValues(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ObsValueType = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_TEST_DATEDesc"]);

                if (DS.Tables[0].Rows.Count == 1)
                {
                    decLabValue = DS.Tables[0].Rows[0].IsNull(0) == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0][0]);
                    if (nConversion != 0) decLabValue = decLabValue * nConversion;
                    strLabValue = Convert.ToString(decLabValue);
                }
                else if (DS.Tables[0].Rows.Count > 1)
                {
                    for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        if (strLabValue != "") strLabValue += "/";

                        if (nConversion != 0)
                        {
                            decLabValue = DS.Tables[0].Rows[i].IsNull(0) == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[i][0]) * nConversion;
                        }
                        else
                        {
                            decLabValue = DS.Tables[0].Rows[i].IsNull(0) == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[i][0]);
                        }

                        strLabValue += decLabValue;
                    }
                }

                ObsValue = strLabValue;

            }


        }

        void GetBPValue(string strServCode, out string ObsValue, out string ObsValueType)
        {
            ObsValue = "";
            ObsValueType = "";
            string strBPSys = "", strBPDia = "";
            string strEMRID = "0";
            string Criteria = " 1=1 ";
            Criteria += " AND convert(varchar(10),EPM_DATE ,103)='" + txtInvDate.Text.Trim() + "'" +
             "	 AND EPM_PT_ID = '" + txtFileNo.Text.Trim() + "'" +
             "   AND EPM_DR_CODe = '" + txtDoctorID.Text.Trim() + "'";

            EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
            DS = objPTM.GetEMR_PTMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                strEMRID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);

                Criteria = " 1=1 ";
                Criteria += " AND  EPV_ID = '" + strEMRID + "'";

                CommonBAL objBAL = new CommonBAL();

                DataSet DS1 = new DataSet();
                DS1 = objBAL.EMRPTVitalsGet(Criteria);

                if (DS1.Tables[0].Rows.Count > 0)
                {

                    strBPSys = Convert.ToString(DS1.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);//EPV_BP_SYSTOLICDesc

                    strBPDia = Convert.ToString(DS1.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);//EPV_BP_DIASTOLICDesc

                    ObsValue = strBPSys + "/" + strBPDia;

                    ObsValueType = Convert.ToString(DS1.Tables[0].Rows[0]["EPV_CREATED_DATEDesc"]);

                }

            }


        }

        void ShowAppointmentDtls()
        {

            string Criteria = " 1=1 and HAM_REMARKS <> '' ";
            Criteria += " AND HAM_FILENUMBER='" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND convert(varchar,HAM_STARTTIME,103)=convert(varchar,'" + txtInvDate.Text.Trim() + "',103)";


            dboperations dbo = new dboperations();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                string strRemarks = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]);

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Appointment Remarks - " + strRemarks + "');", true);

                //MPExtMessage.Show();
                //strMessage ="Appointment Remarks - "+ Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]);
            }
        }

        Boolean CheckAccumedXMLDataMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " CLAIMID='" + txtInvoiceNo.Text.Trim() + "'";
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*", "ACCUMED.DBO.ECLAIM_XML_DATA_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        // TODO:AmountCalculation
        void AmountCalculation()
        {
            decimal decDiscount = 0, decFees = 0, decHitAmt = 0, decTotalAmount = 0, decTotalDeduct = 0, decCoInsAmt = 0, decTotalCoInsAmt = 0, decTaxAmount = 0, decTotalTaxAmt = 0, decClaimTaxAmount = 0, decTotalClaimTaxAmt = 0;

            decimal decBefTaxHitAmt = 0;
            for (int intCurRow = 0; intCurRow < gvInvoiceTrans.Rows.Count; intCurRow++)
            {
                TextBox txtFee, txtDiscAmt, txtgvQty, txtHitAmount, txtDeduct, txtCoInsAmt, txtObsType;

                DropDownList drpgvDiscType, drpCoInsType;

                TextBox txtgvServCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvServCode");
                Label lblServType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblServType");

                txtFee = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtFee");
                txtgvQty = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvQty");
                txtHitAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHitAmount");
                drpgvDiscType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpgvDiscType");
                txtDiscAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDiscAmt");
                txtDeduct = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDeduct");
                drpCoInsType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpCoInsType");
                txtCoInsAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtCoInsAmt");

                TextBox txtgvCoInsTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCoInsTotal");
                TextBox txtgvCompType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompType");
                TextBox txtgvCompAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompAmount");
                TextBox txtgvCompTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompTotal");

                txtObsType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsType");

                TextBox txtHaadCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadCode");
                TextBox txtHaadDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadDesc");
                TextBox txtTypeValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTypeValue");

                TextBox txtTaxAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTaxAmount");
                TextBox txtClaimTaxAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtClaimTaxAmount");
                //TextBox txtTaxPercent = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTaxPercent");


                if (txtTaxAmount.Text.Trim() == "" || txtTaxAmount.Text.Trim() == "null")
                {
                    txtTaxAmount.Text = "0.00";
                }

                if (txtClaimTaxAmount.Text.Trim() == "" || txtClaimTaxAmount.Text.Trim() == "null")
                {
                    txtClaimTaxAmount.Text = "0.00";
                }


                decimal decSrvTaxAmount = 0;

                //if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES")
                //{
                //    if (txtTaxPercent.Text.Trim() != "")
                //        decSrvTaxAmount = Convert.ToDecimal(txtTaxPercent.Text.Trim());
                //}
                DataSet DSServ = new DataSet();
                DSServ = GetServiceMasterName("HSM_SERV_ID", txtgvServCode.Text.Trim());


                if (DSServ.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES")
                    {
                        if (DSServ.Tables[0].Rows[0].IsNull("HSM_TAXVALUE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TAXVALUE"]) != "")
                        {
                            decSrvTaxAmount = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_TAXVALUE"]);
                        }
                    }
                }

                if (lblServType.Text == "C")
                {
                    drpCoInsType.Enabled = false;
                    txtCoInsAmt.ReadOnly = true;
                }

                if (txtFee.Text.Trim() == "")
                {
                    txtFee.Text = "0.00";
                }

                if (txtDiscAmt.Text.Trim() == "")
                {
                    txtDiscAmt.Text = "0.00";
                }
                if (txtgvQty.Text.Trim() == "")
                {
                    txtgvQty.Text = "1";
                }
                if (txtCoInsAmt.Text.Trim() == "")
                {
                    txtCoInsAmt.Text = "0.00";
                }
                if (txtHitAmount.Text.Trim() == "")
                {
                    txtHitAmount.Text = "0.00";
                }

                if (txtDeduct.Text.Trim() == "")
                {
                    txtDeduct.Text = "0.00";
                }
                if (txtHospDisc.Text.Trim() == "")
                {
                    txtHospDisc.Text = "0.00";
                }

                if (txtSplDisc.Text.Trim() == "")
                {
                    txtSplDisc.Text = "0.00";
                }

                if (txtPTCredit.Text.Trim() == "")
                {
                    txtPTCredit.Text = "0.00";
                }

                if (txtCoInsTotal.Text.Trim() == "")
                {
                    txtCoInsTotal.Text = "0.00";
                }


                if (txtBillAmt1.Text.Trim() == "")
                {
                    txtBillAmt1.Text = "0.00";
                }

                if (txtBillAmt2.Text.Trim() == "")
                {
                    txtBillAmt2.Text = "0.00";
                }

                if (txtBillAmt3.Text.Trim() == "")
                {
                    txtBillAmt3.Text = "0.00";
                }

                if (txtBillAmt4.Text.Trim() == "")
                {
                    txtBillAmt4.Text = "0.00";
                }

                if (TxtTopupAmt.Text.Trim() == "")
                {
                    TxtTopupAmt.Text = "0.00";
                }

                if (txtCCAmt.Text.Trim() == "")
                {
                    txtCCAmt.Text = "0.00";
                }

                if (txtChqAmt.Text.Trim() == "")
                {
                    txtChqAmt.Text = "0.00";
                }

                if (drpgvDiscType.SelectedIndex == 0)
                {
                    decDiscount = Convert.ToDecimal(txtDiscAmt.Text.Trim());

                }
                else
                {

                    decDiscount = ((Convert.ToDecimal(txtFee.Text.Trim()) / 100) * Convert.ToDecimal(txtDiscAmt.Text.Trim()));
                }


                decFees = Convert.ToDecimal(txtFee.Text.Trim()) - Convert.ToDecimal(decDiscount);

                decHitAmt = Convert.ToDecimal(txtgvQty.Text.Trim()) * decFees;
                //  decBefTaxHitAmt = Convert.ToDecimal(txtgvQty.Text.Trim()) * decFees;

                //if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
                //{
                //    decTaxAmount = ((decHitAmt) / 100) * decSrvTaxAmount;
                //    decHitAmt = decHitAmt + decTaxAmount;
                //}

                //txtHitAmount.Text = decHitAmt.ToString("#0.00");
                //txtTaxAmount.Text = decTaxAmount.ToString("#0.00");

                if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
                {
                    decTaxAmount = Convert.ToDecimal(txtTaxAmount.Text.Trim());  // ((decHitAmt) / 100) * decSrvTaxAmount;
                    decTotalTaxAmt = decTotalTaxAmt + decTaxAmount;

                    decClaimTaxAmount = Convert.ToDecimal(txtClaimTaxAmount.Text.Trim());
                    decTotalClaimTaxAmt = decTotalClaimTaxAmt + decClaimTaxAmount;

                }


                if (txtDeduct.Text.Trim() != "" && lblServType.Text == "C")
                {
                    // txtDeduct.Text = Convert.ToString(Convert.ToDecimal(txtDeduct.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));
                    // txtCoInsAmt.Text = Convert.ToString(Convert.ToDecimal(txtDeduct.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));
                    if (drpCoInsType.SelectedIndex == 0)
                    {
                        decCoInsAmt = Convert.ToDecimal(txtCoInsAmt.Text.Trim());

                    }
                    else
                    {

                        decCoInsAmt = ((Convert.ToDecimal(txtHitAmount.Text.Trim()) / 100) * Convert.ToDecimal(txtCoInsAmt.Text.Trim()));
                    }
                }


                decTotalAmount = decTotalAmount + Convert.ToDecimal(txtHitAmount.Text.Trim());



                decTotalDeduct = decTotalDeduct + Convert.ToDecimal(txtDeduct.Text.Trim());



                if (lblServType.Text != "C")//== "S")
                {
                    if (drpCoInsType.SelectedIndex == 0)
                    {
                        decCoInsAmt = Convert.ToDecimal(txtCoInsAmt.Text.Trim());

                    }
                    else
                    {

                        if (Convert.ToString(ViewState["COINSFROMGROSS_CLAIMFROMNET"]) == "Y")
                        {
                            decCoInsAmt = ((Convert.ToDecimal(txtFee.Text.Trim()) / 100) * Convert.ToDecimal(txtCoInsAmt.Text.Trim()));
                        }
                        else
                        {
                            decCoInsAmt = ((Convert.ToDecimal(txtHitAmount.Text.Trim()) / 100) * Convert.ToDecimal(txtCoInsAmt.Text.Trim()));
                        }
                    }

                }
                decTotalCoInsAmt = decTotalCoInsAmt + decCoInsAmt;

                txtgvCoInsTotal.Text = Convert.ToString(decCoInsAmt);

                txtTotalTaxAmt.Text = decTotalTaxAmt.ToString("#0.00");

                txtTotalClaimTaxAmt.Text = decTotalClaimTaxAmt.ToString("#0.00");

                if (txtSubCompanyID.Text != "")
                {
                    txtgvCompType.Text = drpCoInsType.Text;
                    txtgvCompAmount.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);
                    txtgvCompTotal.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);


                    //  decBefTaxHitAmt = decBefTaxHitAmt - decCoInsAmt;



                    //if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
                    //{
                    //    decClaimTaxAmount = (decBefTaxHitAmt / 100) * decSrvTaxAmount;
                    //    decClaimTotalTaxAmt = decClaimTotalTaxAmt + decClaimTaxAmount;
                    //}
                    //txtClaimTaxAmount.Text = decClaimTaxAmount.ToString("#0.00");

                }
                else
                {
                    txtgvCompType.Text = drpCoInsType.Text;
                    txtgvCompAmount.Text = "0.00";
                    txtgvCompTotal.Text = "0.00";
                }



                string strHaadName = "", strHaadType = "";
                if (txtHaadCode.Text.Trim() != "")
                {
                    DataSet DSHaad = new DataSet();
                    DSHaad = GetHaadName(txtHaadCode.Text.Trim());

                    if (DSHaad.Tables[0].Rows.Count > 0)
                    {
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                        {
                            strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                        }
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                        {
                            strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                        }
                    }
                    txtHaadDesc.Text = strHaadName;
                    txtTypeValue.Text = strHaadType;

                }


            }

            txtBillAmt1.Text = decTotalAmount.ToString("#0.00");
            txtDeductible.Text = decTotalDeduct.ToString("#0.00");



            //--------------------------

            decimal decBill2 = 0, decHosDisc = 0, decHospDiscAmt = 0;
            if (txtBillAmt1.Text.Trim() != "" && txtHospDisc.Text.Trim() != "")
            {
                decHosDisc = Convert.ToDecimal(txtHospDisc.Text.Trim());

                txtHospDisc.Text = decHosDisc.ToString("#0.00");

                if (drpHospDiscType.SelectedIndex == 0)
                {
                    decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - Convert.ToDecimal(txtHospDisc.Text.Trim());
                    txtHospDiscAmt.Text = txtHospDisc.Text.Trim();
                }
                else
                {

                    decHospDiscAmt = (Convert.ToDecimal(txtBillAmt1.Text.Trim()) / 100) * Convert.ToDecimal(txtHospDisc.Text.Trim());

                    decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - decHospDiscAmt;

                    txtHospDiscAmt.Text = decHospDiscAmt.ToString("#0.00");

                }

                txtBillAmt2.Text = decBill2.ToString("#0.00");
            }
            else
            {
                txtBillAmt2.Text = txtBillAmt1.Text.Trim();
            }
            //--------------------------

            decimal decBill3 = 0;
            if (txtBillAmt2.Text.Trim() != "" && txtDeductible.Text.Trim() != "")
            {

                decBill3 = Convert.ToDecimal(txtBillAmt2.Text.Trim()) - Convert.ToDecimal(txtDeductible.Text.Trim());

            }
            txtBillAmt3.Text = decBill3.ToString("#0.00");


            if (drpInvType.SelectedValue == "Cash")
            {
                decTotalCoInsAmt = decTotalCoInsAmt - decHosDisc;
                txtCoInsTotal.Text = decTotalCoInsAmt.ToString("#0.00");
            }
            else
            {
                txtCoInsTotal.Text = decTotalCoInsAmt.ToString("#0.00");
            }

            if (txtSubCompanyID.Text != "")
            {
                decimal decClaimAmt = 0;
                if (txtBillAmt2.Text.Trim() != "" && txtCoInsTotal.Text.Trim() != "")
                {
                    decClaimAmt = Convert.ToDecimal(txtBillAmt2.Text.Trim()) - Convert.ToDecimal(txtCoInsTotal.Text.Trim());
                }
                txtClaimAmt.Text = decClaimAmt.ToString("#0.00");

            }


            else
            {
                txtClaimAmt.Text = "0.00";
            }
            decimal decBill4 = 0;
            if (txtSubCompanyID.Text != "")
            {
                if (txtCoInsTotal.Text.Trim() != "" && txtSplDisc.Text.Trim() != "")
                {
                    ////if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    ////{
                    ////    decBill4 = Convert.ToDecimal(txtCoInsTotal.Text.Trim()); ///// - Convert.ToDecimal(txtSplDisc.Text.Trim());
                    ////}
                    ////else
                    ////{
                    decBill4 = Convert.ToDecimal(txtCoInsTotal.Text.Trim()) - Convert.ToDecimal(txtSplDisc.Text.Trim());
                    //// }

                }
            }
            else
            {
                ////if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                ////{
                ////    decBill4 = Convert.ToDecimal(txtBillAmt3.Text.Trim()); ///// - Convert.ToDecimal(txtSplDisc.Text.Trim());
                ////}
                ////else
                ////{
                decBill4 = Convert.ToDecimal(txtBillAmt3.Text.Trim()) - Convert.ToDecimal(txtSplDisc.Text.Trim());
                //// }
            }
            txtBillAmt4.Text = decBill4.ToString("#0.00");



            decimal decPaidAmt = 0, decTopupAmt = 0, decRevAmt; //Convert.ToDecimal(txtCoInsTotal.Text.Trim());



            txtPaidAmount.Text = decPaidAmt.ToString("#0.00");


            if (GlobalValues.FileDescription == "ALDAR" && (Convert.ToBoolean(ViewState["NewFlag"]) == true || Convert.ToDecimal(txtPTCredit.Text.Trim()) > 0))
            {
                txtPTCredit.Text = Convert.ToString(txtBillAmt4.Text.Trim());
                decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text.Trim()) - Convert.ToDecimal(txtPTCredit.Text.Trim());
            }
            else
            {
                txtPTCredit.Text = Convert.ToString(txtPTCredit.Text.Trim() != "" ? txtPTCredit.Text.Trim() : "0.00");

                if (ChkTopupCard.Checked == true)
                {
                    decTopupAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text.Trim());
                    decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text.Trim()) - decTopupAmt;
                }
                else
                {
                    if (txtPTCredit.Text.Trim() != "" && Convert.ToDecimal(txtPTCredit.Text.Trim()) > 0)
                    {
                        decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text.Trim());
                    }
                    else
                    {
                        decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text);
                    }
                    decTopupAmt = 0;
                }


            }

            ////if (txtSplDisc.Text.Trim() != "")
            ////{
            ////    decPaidAmt = decPaidAmt - Convert.ToDecimal(txtSplDisc.Text.Trim());
            ////}



            txtPaidAmount.Text = decPaidAmt.ToString("#0.00");
            TxtTopupAmt.Text = decTopupAmt.ToString("#0.00");


            decRevAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text) - Convert.ToDecimal(TxtTopupAmt.Text);

            ////if (txtSplDisc.Text.Trim() != "")
            ////{
            ////    decRevAmt = decRevAmt - Convert.ToDecimal(txtSplDisc.Text.Trim());
            ////}

            TxtRcvdAmt.Text = decRevAmt.ToString("#0.00");

            decimal decCCAmt = 0, decChqAmt = 0, decCashAmt = 0;

            if (txtCCAmt.Text.Trim() != "")
            {
                decCCAmt = Convert.ToDecimal(txtCCAmt.Text.Trim());
            }

            if (txtChqAmt.Text.Trim() != "")
            {
                decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
            }

            if (decPaidAmt != 0)
            {
                decCashAmt = decPaidAmt - decCCAmt - decChqAmt;
            }
            else
            {
                decCashAmt = decCCAmt - decChqAmt;
            }

            txtCashAmt.Text = decCashAmt.ToString("#0.00");



            Decimal decBalAmt = 0;
            if (txtPaidAmount.Text != "")
            {
                decBalAmt = (Convert.ToDecimal(txtPaidAmount.Text) + Convert.ToDecimal(txtPTCredit.Text)) - Convert.ToDecimal(txtBillAmt4.Text);// -decBill4;

            }
            txtBalAmt.Text = decBalAmt.ToString("#0.00");


        }

        Boolean fnSave()
        {
            //PrintCrBill();
            //PrintInvoice();

            Boolean isError = false;

            string PrevBilltoCode;
            decimal PrevClaim_Amt = 0, PrevClaimPaid = 0, PrevPTReject = 0, PrevCmpReject = 0, PrevPTCrPaid = 0;

            decimal CmpInvoiceNo = 0;

            PrevBilltoCode = hidCompanyID.Value;

            CommonBAL objCom = new CommonBAL();
            clsInvoice objInv = new clsInvoice();
            dbOperation objDB = new dbOperation();
            String Criteria = "1=1", FieldNameWithValues = "";


            InvoiceSaveLog("************ " + System.DateTime.Now.ToString() + "  Invoice Save Started ");
            InvoiceSaveLog("Invoice.btnSave   1  ");

            if (GlobalValues.FileDescription == "ALREEM")
            {
                if (txtRefDrName.Text.Trim() != "")
                {
                    if (drpCommissionType.SelectedIndex == -1)
                    {
                        lblStatus.Text = "Please select commision type from the list";
                        isError = true;
                        goto FunEnd;
                    }
                }
            }
            InvoiceSaveLog("Invoice.btnSave   2  ");

            if (hidMsgConfirm.Value == "false")
            {
                isError = true;
                goto FunEnd;
            }

            // if (drpInvType.SelectedValue == "Credit")
            //  {

            if (Convert.ToString(ViewState["PATIENT_CLASS"]) != "")
            {
                if (Convert.ToString(ViewState["PATIENT_CLASS"]) == "UC" && drpClmType.SelectedValue != "2")
                {
                    lblStatus.Text = "Patient visit is Urgent Care so Please Select Encounter  Type as 'No Bed + Emergency Room (OP)' ";
                    isError = true;
                    goto FunEnd;
                }

            }
            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter FileNumber";
                isError = true;
                goto FunEnd;
            }

            if (txtName.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Patient Name";
                isError = true;
                goto FunEnd;
            }

            if (txtDoctorID.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Doctor Details";
                isError = true;
                goto FunEnd;
            }

            if (txtDoctorName.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Doctor Details";
                isError = true;
                goto FunEnd;
            }

            if (txtClmStartDate.Text.Trim() == "" || txtClmFromTime.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Encounter Start Date & Time ";
                isError = true;
                goto FunEnd;
            }

            if (txtClmEndDate.Text.Trim() == "" || txtClmToTime.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Encounter End Date & Time ";
                isError = true;
                goto FunEnd;
            }

            if (txtBillAmt1.Text.Trim() == "")
            {
                lblStatus.Text = " Bill Amt(1) should have the value ";
                isError = true;
                goto FunEnd;
            }        


            if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            {
                if (Convert.ToDecimal(txtBillAmt1.Text.Trim()) <= 0)
                {
                    lblStatus.Text = " Bill Amt(1) value should have grater than zero ";
                    isError = true;
                    goto FunEnd;
                }
                if (drpInvType.SelectedValue == "Customer" || drpInvType.SelectedValue == "Cash")
                {
                    if (txtBillAmt4.Text.Trim() == "")
                    {
                        lblStatus.Text = " Bill Amt(4) should have the value ";
                        isError = true;
                        goto FunEnd;
                    }

                    if (Convert.ToDecimal(txtBillAmt4.Text.Trim()) <= 0)
                    {
                        lblStatus.Text = " Bill Amt(4) value should have grater than zero ";
                        isError = true;
                        goto FunEnd;
                    }
                }
            }

            //   }
            DataTable DT1 = new DataTable();

            DT1 = (DataTable)ViewState["InvoiceTrans"];
            if (DT1.Rows.Count <= 0)
            {
                lblStatus.Text = "Please Add the Services ";
                isError = true;
                goto FunEnd;
            }
            
            for (int intCurRow = 0; intCurRow < gvInvoiceTrans.Rows.Count; intCurRow++)
            {
                TextBox txtgvServCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvServCode");
                TextBox txtServDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtServDesc");
                TextBox txtgcStartDt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgcStartDt");
                TextBox txtObsType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsType");
                TextBox txtObsCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsCode");
                TextBox txtObsDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsDesc");
                TextBox txtObsValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsValue");
                TextBox txtObsValueType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsValueType");
                
                if (txtObsType.Text.Trim() != "")
                {
                    if (GlobalValues.FileDescription != "SMCH")
                    {
                        if (txtObsCode.Text.Trim() == "")
                        {
                            lblStatus.Text = "Please Enter Observation Dtls";
                            isError = true;
                            goto FunEnd;
                        }
                    }
                }
                // if (drpInvType.SelectedValue == "Credit")
                // {
                if (txtgcStartDt.Text == "")
                {
                    lblStatus.Text = "Please Enter Service Start Date";
                    isError = true;
                    goto FunEnd;
                }

                if (GlobalValues.FileDescription == "SMCH")
                {
                    if (txtClmStartDate.Text.Trim() != "" && txtClmFromTime.Text.Trim() != "")
                    {
                        //string strEncStartDate = txtClmStartDate.Text;
                        //string[] arrEncStartDate = strEncStartDate.Split('/');
                        //string strForEncStartDate = "";

                        //if (arrEncStartDate.Length > 1)
                        //{
                        //    strForEncStartDate = arrEncStartDate[1] + "/" + arrEncStartDate[0] + "/" + arrEncStartDate[2];
                        //}
                        //string strEncEndDate = txtClmEndDate.Text.Trim();
                        //string[] arrrEncEndDate = strEncEndDate.Split('/');
                        //string strForEncEndDate = "";

                        //if (arrrEncEndDate.Length > 1)
                        //{
                        //    strForEncEndDate = arrrEncEndDate[1] + "/" + arrrEncEndDate[0] + "/" + arrrEncEndDate[2];
                        //}

                        //string strServDate = txtgcStartDt.Text.Trim();
                        //string[] arrServDate = strServDate.Split(' ');
                        //string strForServDate = "";

                        //if (arrServDate.Length > 0)
                        //{

                        //    string[] arrServDat1 = arrServDate[0].Split('/');


                        //    if (arrServDat1.Length > 1)
                        //    {
                        //        strForServDate = arrServDat1[1] + "/" + arrServDat1[0] + "/" + arrServDat1[2];
                        //    }

                        //}

                        //   .ToString("dd/MM/yyyy ");
                        //   txtClmFromTime.Text = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"].ToString()).ToString("HH:mm:ss");

                        DateTime dtEncStartDate = DateTime.ParseExact(txtClmStartDate.Text + " " + txtClmFromTime.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);

                        DateTime dtEncEndDate = DateTime.ParseExact(txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);

                        DateTime dtServStartDate = DateTime.ParseExact(txtgcStartDt.Text.Trim(), "dd/MM/yyyy HH:mm:ss", null);
                        
                        TimeSpan Diff = dtServStartDate - dtEncStartDate;

                        if (Diff.TotalMinutes < 0)
                        {
                            lblStatus.Text = "Service Date Between Encounter Start Date and End Date";
                            isError = true;
                            goto FunEnd;
                        }

                        TimeSpan Diff1 = dtServStartDate - dtEncEndDate;

                        if (Diff1.TotalMinutes > 0)
                        {
                            lblStatus.Text = "Service Date Between Encounter Start Date and End Date";
                            isError = true;
                            goto FunEnd;
                        }
                    }
                }
                //// }
            }

            if (GlobalValues.FileDescription.ToUpper() == "SMCH" && drpInvType.SelectedValue != "Customer") //&& drpInvType.SelectedValue == "Credit"
            {
                if (chkOutsidePT.Checked == false && txtEMRID.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter EMR ID";
                    isError = true;
                    goto FunEnd;
                }
                if (txtPayerID.Text.Trim() == "")
                {
                    lblStatus.Text = "Company Not having the Payer ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }
                if (txtReciverID.Text.Trim() == "")
                {
                    lblStatus.Text = "Company Not having the Reciver ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }
            }

            if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            {
                //if (CheckAccumedXMLDataMaster() == true)
                if (Convert.ToString(ViewState["HIM_INS_VERIFY_STATUS"]) == "Completed")
                {
                    lblStatus.Text = "Transfer Completed, So you can not Edit this Invoice";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    isError = true;
                    goto FunEnd;
                }
            }

            if (GlobalValues.FileDescription.ToUpper() == "SMCH" && chkOutsidePT.Checked == true && txtRefDrName.Text.Trim() == "")
            {
                lblStatus.Text = "Please Enter Ref Dr.";
                isError = true;
                goto FunEnd;
            }

            InvoiceSaveLog("Invoice.btnSave   4  ");

            if (GlobalValues.FileDescription == "ANP" || GlobalValues.FileDescription == "ALNOORSATWA" && Convert.ToString(Session["User_ID"]).ToUpper() != "ADMIN" && drpInvType.SelectedValue == "Cash")
            {
                string dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                DateTime Today;
                Today = Convert.ToDateTime(objCom.fnGetDate("dd/MM/yyyy"));

                Int32 intDatediff = 0;
                intDatediff = objCom.fnGetDateDiff(Convert.ToString(Today), dtInvDate);

                if (intDatediff > 0)
                {
                    lblStatus.Text = "You cannot edit Cash Invoice. ";
                    isError = true;
                    goto FunEnd;
                }
            }
            InvoiceSaveLog("Invoice.btnSave   5  ");

            if (GlobalValues.FileDescription == "ANP" || GlobalValues.FileDescription == "ALREEM" || GlobalValues.FileDescription == "ALNOORSATWA")
            {
                if (txtRemarks.Text == "")
                {
                    lblStatus.Text = "Remarks field should be non-empty for editing Invoice. ";
                    isError = true;
                    goto FunEnd;
                }
            }
            Boolean boolPrincipal = false;

            if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
            {
                DataTable DT2 = new DataTable();
                DT2 = (DataTable)ViewState["InvoiceClaims"];
                if (GlobalValues.FileDescription.ToUpper() == "SMCH") //&& drpInvType.SelectedValue == "Credit"
                {
                    if (DT2.Rows.Count <= 0)
                    {
                        lblStatus.Text = "Please Add the Diagnosis ";
                        isError = true;
                        goto FunEnd;
                    }
                }

                if (DT2.Rows.Count > 0)
                {
                    for (int i = 0; i < DT2.Rows.Count; i++)
                    {
                        string strICDType = "";
                        strICDType = Convert.ToString(DT2.Rows[i]["HIC_ICD_TYPE"]);

                        if (strICDType == "Principal")
                        {
                            boolPrincipal = true;
                            goto EndForDiag;
                        }
                    }
                EndForDiag: ;
                    if (boolPrincipal == false)
                    {
                        lblStatus.Text = "Please Add the Principal Diagnosis";
                        isError = true;
                        goto FunEnd;
                    }
                }
            }

            InvoiceSaveLog("Invoice.btnSave   6  ");
            if (GlobalValues.FileDescription == "PHY") // And App.LegalCopyright <> "1" Then
            {
                FieldNameWithValues = " HIM_PT_COMP='" + txtPTComp.Text.Trim() + "' ";
                Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);

                if (ChkTopupCard.Checked == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                {
                    FieldNameWithValues = " HIM_PT_COMP='" + txtPTComp.Text.Trim() + "' ";
                    Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='T" + txtInvoiceNo.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);
                }
                //fnclearfields
                goto FunEnd;
            }
            InvoiceSaveLog("Invoice.btnSave   7  ");
            // objCom.AudittrailAdd(Convert.ToString(Session["Branch_ID"]), "Invoice", "MASTER", "Validated Invoice Master entry at " + objCom.fnGetDate("dd/MM/yyyy HH:mm:ss"), Convert.ToString(Session["User_ID"]));

            InvoiceSaveLog("Invoice.btnSave   8  ");

            if (drpPayType.SelectedItem.Text == "Multi Currency")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                drpccType.SelectedIndex = -1;
                txtCCNo.Text = "";
                txtCCRefNo.Text = "";
                txtCCAmt.Text = "0";
                txtChqAmt.Text = "0";
            }
            else if (drpPayType.SelectedItem.Text == "Credit Card" || drpPayType.SelectedItem.Text == "Debit Card")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";
                drpCurType.SelectedIndex = -1;
                decimal decPaidAmt = 0, decCCAmt = 0;

                if (txtPaidAmount.Text != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text);
                }

                if (txtCCAmt.Text != "")
                {
                    decCCAmt = Convert.ToDecimal(txtCCAmt.Text);
                }

                txtCashAmt.Text = Convert.ToString(decPaidAmt - decCCAmt);

            }
            else if (drpPayType.SelectedItem.Text.ToUpper() == "UTILISE ADVANCE")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";
                drpCurType.SelectedIndex = -1;
                txtCashAmt.Text = "0";

            }
            else if (drpPayType.SelectedItem.Text == "Cheque")
            {
                txtCCNo.Text = "";
                txtCCRefNo.Text = "";
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                drpCurType.SelectedIndex = -1;
                txtRcvdAmount.Text = "0";
            }
            else if (drpPayType.SelectedItem.Text == "Advance")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";
                drpCurType.SelectedIndex = -1;

                decimal decPaidAmt = 0, decCCAmt = 0;

                if (txtPaidAmount.Text != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text);
                }

                if (txtCCAmt.Text != "")
                {
                    decCCAmt = Convert.ToDecimal(txtCCAmt.Text);
                }

                txtCashAmt.Text = Convert.ToString(decPaidAmt - decCCAmt);

            }
            else
            {
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtCCNo.Text = "";
                txtCCRefNo.Text = "";
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                txtCCHolder.Text = "";
                drpCurType.SelectedIndex = -1;
                drpBank.SelectedIndex = -1;
                drpccType.SelectedIndex = -1;
                txtCCAmt.Text = "0";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";

                decimal decPaidAmt = 0;

                if (txtPaidAmount.Text != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text);
                }


                txtCashAmt.Text = Convert.ToString(decPaidAmt);

            }
            InvoiceSaveLog("Invoice.btnSave   9  ");

            hidReceipt.Value = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "RECEIPT");

            if (GlobalValues.FileDescription != "HOLISTIC")
            {
                InvoiceSaveLog("Invoice.btnSave   10  ");

                Criteria = "1=1 AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                if (objCom.fnCheckduplicate("HMS_INVOICE_MASTER", Criteria) == true && Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {

                    clsInvoice objInvMastEdit = new clsInvoice();
                    objInvMastEdit.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim();
                    objInvMastEdit.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objInvMastEdit.InvoiceMasterEditAdd();

                    InvoiceSaveLog("Invoice.btnSave   11  ");


                    if (GlobalValues.FileDescription == "KMC" && (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer"))
                    {
                        DataSet DSInvNo = new DataSet();
                        DSInvNo = objInv.GetLastCompanyInvoiceNo(Convert.ToString(Session["Branch_ID"]), txtSubCompanyID.Text.Trim());
                        if (DSInvNo.Tables[0].Rows.Count > 0)
                        {
                            if (DSInvNo.Tables[0].Rows[0].IsNull(0) == false)
                                CmpInvoiceNo = Convert.ToDecimal(DSInvNo.Tables[0].Rows[0][0]) + 1;
                        }
                    }








                    DataSet DSInv = new DataSet();
                    Criteria = "1=1 AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    DSInv = objInv.InvoiceMasterGet(Criteria);
                    if (DSInv.Tables[0].Rows.Count > 0)
                    {
                        ViewState["NewFlag"] = false;
                        InvoiceSaveLog("Invoice.btnSave   12  Invoice Modification ");
                        clsInvoice objInvTransEdit = new clsInvoice();
                        objInvTransEdit.HIT_INVOICE_ID = txtInvoiceNo.Text.Trim();
                        objInvTransEdit.HIT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);

                        objInvTransEdit.InvoiceTransEditAdd();
                        InvoiceSaveLog("Invoice.btnSave   13  ");


                        InvoiceSaveLog("Invoice.btnSave   14  ");
                        hidReceipt.Value = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_RECEIPT_NO"]);
                        PrevBilltoCode = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_BILLTOCODE"]);
                        PrevClaim_Amt = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                        PrevClaimPaid = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT_PAID"]);

                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_REJECTED"]) != "")
                        {
                            PrevPTReject = DSInv.Tables[0].Rows[0].IsNull("HIM_PT_REJECTED") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PT_REJECTED"]);
                        }

                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_CLAIM_REJECTED"]) != "")
                        {
                            PrevCmpReject = DSInv.Tables[0].Rows[0].IsNull("HIM_CLAIM_REJECTED") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_REJECTED"]);

                        }
                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT_PAID"]) != "")
                        {
                            PrevPTCrPaid = DSInv.Tables[0].Rows[0].IsNull("HIM_PT_CREDIT_PAID") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT_PAID"]);
                        }

                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_JOUR_NO"]) != "")
                        {
                            JourNo = DSInv.Tables[0].Rows[0].IsNull("HIM_JOUR_NO") == false ? "" : Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_JOUR_NO"]);
                        }

                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_CMP_INVOICE_NO"]) != "")
                        {
                            CmpInvoiceNo = DSInv.Tables[0].Rows[0].IsNull("HIM_CMP_INVOICE_NO") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CMP_INVOICE_NO"]);
                        }
                        InvoiceSaveLog("Invoice.btnSave   15  ");

                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]) == "Advance")
                        {

                            DataSet DSInvOther = new DataSet();
                            Criteria = "1=1 AND HIO_TYPE='Invoice' AND HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                            DSInvOther = objInv.InvoiceOtherGet(Criteria);
                            if (DSInv.Tables[0].Rows.Count > 0)
                            {
                                decimal AdvAmt = 0;
                                Criteria = "1=1 AND HIO_TYPE='Advance' AND HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                                AdvAmt = DSInvOther.Tables[0].Rows[0].IsNull("HIO_ADVANCE_AMT") == false ? 0 : Convert.ToDecimal(DSInvOther.Tables[0].Rows[0]["HIO_ADVANCE_AMT"]);
                                objInv.InvoiceOtherUpdateAdvAmt(AdvAmt, Criteria);
                            }


                            Criteria = "1=1 AND HIO_TYPE='Invoice' AND HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                            objCom.fnDeleteTableData("HMS_INVOICE_OTHERS", Criteria);
                        }
                        InvoiceSaveLog("Invoice.btnSave   16  ");

                        InvoiceSaveLog("Invoice.btnSave   17  ");
                        // if( StrInvoiceType <> "PERFORMA" Then
                        Criteria = "1=1 AND HPB_TRANS_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HPB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_PATIENT_BALANCE_HISTORY", Criteria);

                        Criteria = "1=1 AND hrt_receiptno='" + hidReceipt.Value + "'  AND HRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_RECEIPT_TRANSACTION", Criteria);

                        Criteria = "1=1 AND hrm_receiptno='" + hidReceipt.Value + "' AND  HRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_RECEIPT_MASTER", Criteria);

                        clsInvoice objInvUpdate = new clsInvoice();
                        objInvUpdate.HIM_GROSS_TOTAL = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]);
                        objInvUpdate.HIM_HOSP_DISC_AMT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_HOSP_DISC_AMT"]);
                        objInvUpdate.HIM_PT_AMOUNT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_AMOUNT"]);
                        objInvUpdate.HIM_CLAIM_AMOUNT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                        objInvUpdate.HIM_PAID_AMOUNT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]);
                        objInvUpdate.HIM_PT_CREDIT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT"]);
                        Criteria = "1=1 and HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_PT_ID='" + Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_ID"]) + "'  and HPV_DATE>='" + objCom.fnGetDate("MM/dd/yyyy 00:00:00") + "'  and HPV_DATE<='" + objCom.fnGetDate("MM/dd/yyyy 23:59:00") + "' AND  HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";
                        objInvUpdate.PatientVIsitUpdateInvoiceAmt(Criteria);


                        Criteria = "1=1  and HCB_TRANS_TYPE='INVOICE' AND HCB_TRANS_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HCB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                        if (hidCompanyID.Value != PrevBilltoCode && (PrevClaimPaid > 0 || PrevCmpReject > 0))
                        {
                            objInvUpdate.CompanyBalHistUpdate(Criteria);

                        }
                        else
                        {
                            Criteria = "1=1  and HCB_TRANS_TYPE='INVOICE' AND HCB_TRANS_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HCB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                            objCom.fnDeleteTableData("HMS_COMPANY_BALANCE_HISTORY", Criteria);

                        }

                        InvoiceSaveLog("Invoice.btnSave   18  ");
                        if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
                        {

                            FieldNameWithValues = "HET_INVOICED='N'";
                            Criteria = "HET_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HET_CLAIM_ID='" + Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_VOUCHER_NO"]) + "'";
                            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_ECLAIM_TRANSACTION", Criteria);
                        }


                        Criteria = "1=1 AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_INVOICE_TRANSACTION", Criteria);


                        //  Criteria = "1=1 AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        //  objCom.fnDeleteTableData("HMS_INVOICE_MASTER", Criteria);



                    }



                }


            }


            objInv.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInv.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim(); //'T" & Trim(txtinvoiceno.Text) 
            objInv.HIM_INVOICE_TYPE = drpInvType.SelectedValue;
            objInv.HIM_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objInv.HIM_STATUS = drpStatus.SelectedValue;
            objInv.HIM_MASTER_INVOICE_NUMBER = txtInvoiceNo.Text.Trim();//txtinvoiceno.ToolTipText
            objInv.HIM_INVOICE_ORDER = txtTokenNo.Text.Trim();//txttokenNo.ToolTipText
            objInv.HIM_TOKEN_NO = txtTokenNo.Text.Trim();
            objInv.HIM_PT_ID = txtFileNo.Text.Trim();
            objInv.HIM_PT_NAME = txtName.Text.Trim();
            objInv.HIM_BILLTOCODE = hidCompanyID.Value;
            objInv.HPV_BUSINESSUNITCODE = txtJobnumber.Text;
            objInv.HPV_JOBNUMBER = txtJobnumber.ToolTip;

            objInv.HIM_SUB_INS_CODE = txtSubCompanyID.Text.Trim();
            objInv.HIM_INS_NAME = txtCompanyName.Text.Trim();
            objInv.HIM_SUB_INS_NAME = txtSubCompanyName.Text.Trim();
            objInv.HIM_PT_COMP = txtPTComp.Text.Trim();
            objInv.HIM_POLICY_NO = txtPolicyNo.Text.Trim();
            objInv.HIM_ID_NO = txtIdNo.Text;
            objInv.HIM_POLICY_EXP = txtPolicyExp.Text.Trim();
            objInv.HIM_INS_TRTNT_TYPE = hidInsTrtnt.Value; // "Service Type";
            objInv.HIM_DR_CODE = txtDoctorID.Text.Trim();
            objInv.HIM_DR_NAME = txtDoctorName.Text.Trim();

            objInv.HIM_ORDERING_DR_CODE = txtOrdClinCode.Text.Trim();
            objInv.HIM_ORDERING_DR_NAME = txtOrdClinName.Text.Trim();

            objInv.HIM_TRTNT_TYPE = drpTreatmentType.SelectedValue;

            string strName = txtRefDrName.Text;
            string RefDrId = "", RefDrName = "";
            string[] arrName = strName.Split('~');
            if (arrName.Length > 1)
            {
                RefDrId = arrName[0];
                RefDrName = arrName[1];

            }


            objInv.HIM_REF_DR_CODE = RefDrId;// txtRefDrId.Text;
            objInv.HIM_REF_DR_NAME = RefDrName; // txtRefDrName.Text;

            objInv.HIM_GROSS_TOTAL = txtBillAmt1.Text.Trim();
            objInv.HIM_HOSP_DISC_TYPE = drpHospDiscType.SelectedValue;
            objInv.HIM_HOSP_DISC = txtHospDisc.Text.Trim();
            objInv.HIM_HOSP_DISC_AMT = txtHospDiscAmt.Text.Trim();
            objInv.HIM_CO_INS_TYPE = hidCoInsType.Value;
            objInv.HIM_CO_INS_AMOUNT = "0";// txtCoInsTotal.Text.Trim();
            objInv.HIM_DEDUCTIBLE = txtDeductible.Text.Trim();
            objInv.HIM_NET_AMOUNT = txtBillAmt3.Text.Trim();
            objInv.HIM_CLAIM_AMOUNT = txtClaimAmt.Text.Trim();
            objInv.HIM_CLAIM_AMOUNT_PAID = Convert.ToString(PrevClaimPaid);
            objInv.HIM_CLAIM_REJECTED = Convert.ToString(PrevCmpReject);
            objInv.HIM_PT_AMOUNT = txtBillAmt4.Text.Trim();
            objInv.HIM_PT_CREDIT = txtPTCredit.Text.Trim();
            objInv.HIM_PT_CREDIT_PAID = Convert.ToString(PrevPTCrPaid);
            // objInv.HIM_PT_REJECTED =
            objInv.HIM_SPL_DISC = txtSplDisc.Text.Trim();
            objInv.HIM_PAID_AMOUNT = txtPaidAmount.Text.Trim();
            objInv.HIM_PAYMENT_TYPE = drpPayType.SelectedValue;
            if (drpPayType.SelectedItem.Text == "Credit Card" || drpPayType.SelectedItem.Text == "Debit Card")
            {
                objInv.HIM_CC_TYPE = drpccType.SelectedValue;
            }

            objInv.HIM_CC_NAME = txtCCHolder.Text.Trim();
            objInv.HIM_CC_NO = txtCCNo.Text.Trim();
            objInv.HIM_REF_NO = txtCCRefNo.Text.Trim();
            objInv.HIM_CHEQ_NO = txtDcheqno.Text.Trim();
            objInv.HIM_CASH_AMT = txtCashAmt.Text.Trim();
            objInv.HIM_CC_AMT = txtCCAmt.Text.Trim();
            objInv.HIM_CHEQUE_AMT = txtChqAmt.Text.Trim();

            objInv.PHASECODE = "";
            if (drpPayType.SelectedItem.Text == "Cheque")
            {
                objInv.HIM_BANK_NAME = drpBank.SelectedValue;
            }
            else
            {
                objInv.HIM_BANK_NAME = "";
            }
            objInv.HIM_CHEQ_DATE = txtDcheqdate.Text.Trim();
            if (drpPayType.SelectedItem.Text == "Multi Currency")
            {
                objInv.HIM_CUR_TYP = drpCurType.SelectedValue;
            }
            else
            {
                objInv.HIM_CUR_TYP = "";
            }
            objInv.HIM_CUR_RATE = txtConvRate.Text.Trim(); ;
            objInv.HIM_CUR_VALUE = Convert.ToString(Convert.ToDecimal(txtcurValue.Text.Trim() != "" ? txtcurValue.Text.Trim() : "0") - Convert.ToDecimal(LblMBalAmt.Text.Trim() != "" ? LblMBalAmt.Text.Trim() : "0"));
            objInv.HIM_CUR_RCVD_AMT = txtRcvdAmount.Text.Trim();
            objInv.HIM_RECEIPT_NO = hidReceipt.Value;
            objInv.HIM_COMMIT_FLAG = "N";
            objInv.HIM_VOUCHER_NO = TxtVoucherNo.Text.Trim();
            objInv.HIM_POLICY_TYPE = txtPlanType.Text.Trim();
            objInv.HIM_REMARKS = txtRemarks.Text.Trim();
            objInv.HIM_TYPE = hidType.Value != "PHY" ? "INV" : hidType.Value;
            objInv.HIM_COMMISSION_TYPE = drpCommissionType.SelectedValue;
            objInv.HIM_CMP_INVOICE_NO = Convert.ToString(CmpInvoiceNo);
            objInv.HIM_MERGEINVOICES = txtMergeInvoices.Text.Trim();
            objInv.HIM_ADV_AMT = txtAdvAmt.Text.Trim();
            objInv.HIM_TRTNT_TYPE_01 = txtICDDesc.Text.Trim();
            objInv.HIM_TRTNT_Type_code = txtICdCode.Text.Trim();

            if (chkReadyforEclaim.Checked == true)
            {
                objInv.HIM_READYFORECLAIM = "1";
            }
            else
            {
                objInv.HIM_READYFORECLAIM = "0";
            }
            objInv.HIM_EMR_ID = txtEMRID.Text.Trim();
            objInv.HIM_AUTHID = txtPriorAuthorizationID.Text.Trim();
            objInv.HIM_CC = hidccText.Value;
            objInv.HIM_TAXAMOUNT = txtTotalTaxAmt.Text;
            objInv.HIM_CLAIMAMOUNTTAX = txtTotalClaimTaxAmt.Text;
            objInv.HIM_ELIGIBILITY_ID = txtEligibilityID.Text;
            objInv.HIM_TOTAL_SESSION = txtTotalSession.Text;
            objInv.HIM_NURSE_ID = drpNurse.SelectedValue;
            objInv.HIM_MOBILE_NO = lblMobileNo.Text.Trim();
            objInv.HIM_MAIL_ID = lblEmailID.Text.Trim();

            if (ChkTopupCard.Checked == true)
            {
                objInv.HIM_TOPUP = "1";
            }
            else
            {
                objInv.HIM_TOPUP = "0";
            }
            objInv.HIM_TOPUP_AMT = TxtTopupAmt.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objInv.NewFlag = "A";
            }
            else
            {
                objInv.NewFlag = "M";
            }
            objInv.IsTopupInv = "N";
            objInv.UserID = Convert.ToString(Session["User_ID"]);

            string strInvoiceID = "0";

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                strInvoiceID = objInv.InvoiceMasterAdd();
                txtInvoiceNo.Text = strInvoiceID;
            }
            else
            {
                objInv.InvoiceMasterUpdate();
                strInvoiceID = txtInvoiceNo.Text;

            }

            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                objInv.HIM_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                objInv.HIM_BILLTOCODE = Convert.ToString(ViewState["mTopupBillToCode"]);
                objInv.HIM_SUB_INS_CODE = Convert.ToString(ViewState["mTopupInsCode"]);
                objInv.HIM_INS_NAME = Convert.ToString(ViewState["mTopupBillToCompName"]);
                objInv.HIM_SUB_INS_NAME = Convert.ToString(ViewState["mTopupInsName"]);
                objInv.HIM_POLICY_NO = Convert.ToString(ViewState["mTopupPolicyNo"]);
                objInv.HIM_ID_NO = Convert.ToString(ViewState["mTopupIDNo"]);
                objInv.HIM_POLICY_EXP = Convert.ToString(ViewState["mTopupExpiry"]);
                objInv.IsTopupInv = "Y";

                string strTInvoiceID1 = "0";

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    strTInvoiceID1 = objInv.InvoiceMasterAdd();

                }
                else
                {
                    strTInvoiceID1 = objInv.InvoiceMasterUpdate();

                }
            }

            InvoiceSaveLog("Invoice.btnSave   31  Invoice Master Saved, Invoie ID is  " + strInvoiceID);

            //drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";
            // hidInvoiceType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";

            FieldNameWithValues = " HPV_INVOICE_ID='" + strInvoiceID + "',HPV_STATUS='I' ";

            if (txtTotalSession.Text.Trim() != "")
            {
                FieldNameWithValues += ",HPV_SESSION_INVOICE_ID='" + strInvoiceID + "' ";
            }
            Criteria = "1=1 AND  HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_PT_ID='" + txtFileNo.Text.Trim() + "' AND  (HPV_DATE BETWEEN CONVERT(DATETIME, '" + txtInvDate.Text.Trim() + " 00:00:00', 103) AND CONVERT(DATETIME, '" + txtInvDate.Text.Trim() + " 23:59:59', 103))  AND  HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";

            //Criteria += "AND  HPV_PT_TYPE '=" + drpInvType.SelectedValue == "Cash" ? "CA" : "CR" +"'";

            if (drpInvType.SelectedValue == "Cash")
            {
                Criteria += " AND  ( HPV_PT_TYPE ='CA' OR  HPV_PT_TYPE ='CASH') ";
            }
            else if (drpInvType.SelectedValue == "Credit")
            {
                Criteria += " AND   ( HPV_PT_TYPE ='CR' OR  HPV_PT_TYPE ='CREDIT')  ";
            }
            else if (drpInvType.SelectedValue == "Customer")
            {
                Criteria += " AND   ( HPV_PT_TYPE ='CU' OR  HPV_PT_TYPE ='CUSTOMER')  ";
            }

            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria);


            Criteria = "1=1 AND  HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            objCom.fnDeleteTableData("HMS_INVOICE_OBSERVATION", Criteria);
            InvoiceSaveLog("Invoice.btnSave   32  ");


            Boolean mAdvOnly = false;
            for (int intCurRow = 0; intCurRow < gvInvoiceTrans.Rows.Count; intCurRow++)
            {
                Label lblgvCatID = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvCatID");
                TextBox txtgvServCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvServCode");
                TextBox txtServDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtServDesc");
                TextBox txtFee = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtFee");

                Label lblDiscType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblDiscType");

                DropDownList drpgvDiscType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpgvDiscType");

                TextBox txtDiscAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDiscAmt");
                TextBox txtgvQty = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvQty");
                TextBox txtHitAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHitAmount");
                TextBox txtDeduct = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDeduct");
                Label lblCoInsType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblCoInsType");
                TextBox txtCoInsAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtCoInsAmt");
                TextBox txtgvCoInsTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCoInsTotal");

                TextBox txtgvCompType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompType");
                TextBox txtgvCompAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompAmount");
                TextBox txtgvCompTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompTotal");

                TextBox txtgvDrCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvDrCode");
                TextBox txtgvDrName = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvDrName");
                Label lblServType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblServType");

                Label lblCoverd = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblCoverd");
                Label lblgvRefNo = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvRefNo");
                Label lblgvTeethNo = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTeethNo");


                TextBox txtHaadCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadCode");
                TextBox txtHaadDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadDesc");

                TextBox txtTypeValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTypeValue");

                TextBox txtgvCostPrice = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCostPrice");


                TextBox txtgvAuthID = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvAuthID");

                Label lblgvServCost = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvServCost");
                Label lblgvAdjDedAmt = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvAdjDedAmt");

                TextBox txtgcStartDt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgcStartDt");


                TextBox txtObsType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsType");
                TextBox txtObsCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsCode");
                TextBox txtObsDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsDesc");
                TextBox txtObsValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsValue");
                TextBox txtObsValueType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsValueType");

                TextBox txtTaxAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTaxAmount");
                TextBox txtClaimTaxAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtClaimTaxAmount");
                TextBox txtTaxPercent = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTaxPercent");


                Label lblgvTranClaimPaid = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranClaimPaid");
                Label lblgvTranClaimRej = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranClaimRej");
                Label lblgvTranClaimBal = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranClaimBal");
                Label lblgvTranDenialCode = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranDenialCode");
                Label lblgvTranDenialDesc = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranDenialDesc");

                TextBox lblgvTranOrderDrCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranOrderDrCode");
                TextBox lblgvTranOrderDrName = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTranOrderDrName");



                Label lblAttachment = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblAttachment");
                TextBox lblAttachmentFileName = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("lblAttachmentFileName");

                objInv.HIT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objInv.HIT_INVOICE_ID = strInvoiceID;
                objInv.HIT_CAT_ID = lblgvCatID.Text.Trim();
                objInv.HIT_SERV_CODE = txtgvServCode.Text.Trim();
                objInv.HIT_DESCRIPTION = txtServDesc.Text.Trim().Replace("'", "");
                objInv.HIT_FEE = txtFee.Text.Trim() != "" ? txtFee.Text.Trim() : "0"; ;
                objInv.HIT_DISC_TYPE = drpgvDiscType.SelectedValue;//lblDiscType.Text.Trim();
                objInv.HIT_DISC_AMT = txtDiscAmt.Text.Trim();// 
                objInv.HIT_QTY = txtgvQty.Text.Trim() != "" ? txtgvQty.Text.Trim() : "1"; ;
                objInv.HIT_AMOUNT = txtHitAmount.Text.Trim() != "" ? txtHitAmount.Text.Trim() : "0";
                objInv.HIT_DEDUCT = txtDeduct.Text.Trim() != "" ? txtDeduct.Text.Trim() : "0";
                objInv.HIT_CO_INS_TYPE = lblCoInsType.Text.Trim();
                objInv.HIT_CO_INS_AMT = txtCoInsAmt.Text.Trim() != "" ? txtCoInsAmt.Text.Trim() : "0";
                objInv.HIT_CO_TOTAL = txtgvCoInsTotal.Text.Trim() != "" ? txtgvCoInsTotal.Text.Trim() : "0";

                objInv.HIT_COMP_TYPE = txtgvCompType.Text.Trim();
                objInv.HIT_COMP_AMT = txtgvCompAmount.Text.Trim() != "" ? txtgvCompAmount.Text.Trim() : "0";
                objInv.HIT_COMP_TOTAL = txtgvCompTotal.Text.Trim();

                objInv.HIT_DR_CODE = txtgvDrCode.Text.Trim() != "" ? txtgvDrCode.Text.Trim() : txtDoctorID.Text.Trim();
                objInv.HIT_DR_NAME = txtgvDrName.Text.Trim() != "" ? txtgvDrName.Text.Trim() : txtDoctorName.Text.Trim();

               

                objInv.HIT_COVERED = lblCoverd.Text.Trim();
                objInv.HIT_SERV_TYPE = lblServType.Text.Trim();


                //objInv.HIT_PT_AMOUNT=
                //objInv.HIT_DISC_AMOUNT=
                //objInv.HIT_NET_AMOUNT=

                objInv.HIT_SLNO = Convert.ToString(intCurRow + 1);
                objInv.HIT_REFNO = lblgvRefNo.Text;
                objInv.HIT_TEETHNO = lblgvTeethNo.Text;
                objInv.HIT_HAAD_CODE = txtHaadCode.Text.Trim() != "" ? txtHaadCode.Text.Trim() : txtgvServCode.Text.Trim();   //txtHaadCode.Text.Trim();
                objInv.HIT_TYPE_VALUE = txtTypeValue.Text.Trim() != "" ? txtTypeValue.Text.Trim() : "3";  //txtTypeValue.Text.Trim();
                objInv.HIT_COMMISSION_TYPE = drpCommissionType.SelectedValue;
                objInv.HIT_COSTPRICE = txtgvCostPrice.Text;
                objInv.HIT_AUTHORIZATIONID = txtgvAuthID.Text.Trim();
                objInv.HIT_SERVCOST = lblgvServCost.Text;
                objInv.HIT_ADJ_DEDUCT_AMOUNT = lblgvAdjDedAmt.Text;

                if (txtgcStartDt.Text.Trim() == "")
                {
                    objInv.HIT_STARTDATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                }
                else
                {
                    objInv.HIT_STARTDATE = txtgcStartDt.Text.Trim();
                }
                //objInv.HIT_PHARMACY_CODE=
                //objInv.HIT_TREATMENT_ID=
                //objInv.HIT_RAD_NUMBER=
                //objInv.UTIL_EMNUPDATE=
                //objInv.HIT_addSrno=
                if (txtgvCoInsTotal.Text.Trim() != "")
                {
                    if (ChkTopupCard.Checked == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                    {
                        objInv.HIT_TOPUP_AMT = txtgvCoInsTotal.Text.Trim();
                    }
                    else
                    {
                        objInv.HIT_TOPUP_AMT = "0";
                    }
                }

                objInv.HIT_TAXAMOUNT = txtTaxAmount.Text;
                objInv.HIT_CLAIMAMOUNTTAX = txtClaimTaxAmount.Text;
                objInv.HIT_TAXPERCENT = txtTaxPercent.Text;


                objInv.HIT_CLAIM_AMOUNT_PAID = lblgvTranClaimPaid.Text;
                objInv.HIT_CLAIM_REJECTED = lblgvTranClaimRej.Text.Trim();
                objInv.HIT_CLAIM_BALANCE = lblgvTranClaimBal.Text;
                objInv.HIT_DENIAL_CODE = lblgvTranDenialCode.Text;
                objInv.HIT_DENIAL_DESC = lblgvTranDenialDesc.Text;

                objInv.HIT_ATTACHMENT = lblAttachment.Text;
                objInv.HIT_ATTACHMENT_NAME = lblAttachmentFileName.Text;

                objInv.HIT_CLINICIAN_CODE = lblgvTranOrderDrCode.Text.Trim() != "" ? lblgvTranOrderDrCode.Text.Trim() : txtOrdClinCode.Text.Trim();
                objInv.HIT_CLINICIAN_NAME = lblgvTranOrderDrName.Text.Trim() != "" ? lblgvTranOrderDrName.Text.Trim() : txtOrdClinName.Text.Trim();


                //objInv.HIT_CLAIM_BALANCE=
                objInv.InvoiceTransAdd();

                if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                {
                    objInv.HIT_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                    objInv.HIT_CO_TOTAL = txtgvCompTotal.Text.Trim();


                    objInv.HIT_COMP_AMT = txtCoInsAmt.Text.Trim() != "" ? txtCoInsAmt.Text.Trim() : "0";
                    objInv.HIT_COMP_TOTAL = txtgvCoInsTotal.Text.Trim() != "" ? txtgvCoInsTotal.Text.Trim() : "0";
                    objInv.HIT_TOPUP_AMT = Convert.ToString(txtHitAmount.Text.Trim() != "" ? Convert.ToDecimal(txtHitAmount.Text.Trim()) : 0 - Convert.ToDecimal(txtgvCoInsTotal.Text.Trim()));


                    objInv.InvoiceTransAdd();

                }


                InvoiceSaveLog("Invoice.btnSave   33  Invoice Transaction Saved, Invoie ID is  " + strInvoiceID);

                if (txtObsType.Text.Trim() == "LOINC" || txtObsType.Text.Trim() == "Universal Dental" || txtObsType.Text.Trim() == "Result" || txtObsType.Text.Trim() == "Text")
                {
                    if (txtObsCode.Text.Trim() != "")
                    {
                        objInv.HIO_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                        objInv.HIO_INVOICE_ID = txtInvoiceNo.Text.Trim();
                        objInv.HIO_SERV_CODE = txtgvServCode.Text.Trim();
                        objInv.HIO_TYPE = txtObsType.Text.Trim();
                        objInv.HIO_CODE = txtObsCode.Text.Trim();
                        objInv.HIO_SERV_SLNO = Convert.ToString(intCurRow + 1);
                        objInv.HIO_DESCRIPTION = txtObsDesc.Text.Trim();

                        if (txtObsValue.Text.Trim() != "")
                        {
                            objInv.HIO_VALUE = txtObsValue.Text.Trim();
                        }
                        else
                        {
                            objInv.HIO_VALUE = "NA";
                        }
                        if (txtObsValueType.Text.Trim() != "")
                        {
                            objInv.HIO_VALUETYPE = txtObsValueType.Text.Trim();
                        }
                        else
                        {
                            objInv.HIO_VALUETYPE = "NA";
                        }
                        objInv.InvoiceObserAdd();


                        if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                        {
                            objInv.HIO_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                            objInv.InvoiceObserAdd();

                        }


                    }
                }
                if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
                {

                    FieldNameWithValues = " HET_INVOICED='Y' ";
                    Criteria = " HET_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HET_CLAIM_ID='" + TxtVoucherNo.Text.Trim() + "' and HET_CODE='" + txtHaadCode.Text.Trim() + "' AND HET_TYPE='" + txtTypeValue.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_ECLAIM_TRANSACTION", Criteria);

                }



                if (txtgvServCode.Text.Trim() == Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"])) mAdvOnly = true;

            }


            InvoiceSaveLog("Invoice.btnSave   34  ");



            if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
            {
                SaveClaimDetail();
            }

            if (ChkTopupCard.Checked == false)
            {
                fnSaveReceipt(PrevPTCrPaid);
            }


            InvoiceSaveLog("Invoice.btnSave   35  ");



            if (mAdvOnly == true)
            {
                //Criteria = "1=1 AND  HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HIO_TYPE='Advance'";
                // objCom.fnDeleteTableData("hms_invoice_others", Criteria);
                //fnSaveOtherDetails "Advance", txtinvoiceno.Text, txtPaidAmount.Text, 0
            }
            else if (drpPayType.SelectedValue == "Advance")
            {
                // fnSaveOtherDetails "Invoice", txtinvoiceno.Text, TxtAdvAmt.Text, 0  
            }

            InvoiceSaveLog("Invoice.btnSave   36  ");

            decimal decTopUpAmt = 0;
            if (TxtTopupAmt.Text.Trim() != "" && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                decTopUpAmt = Convert.ToDecimal(TxtTopupAmt.Text.Trim());
            }
            objInv.HPB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInv.HPB_TRANS_ID = txtInvoiceNo.Text.Trim();
            objInv.HPB_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objInv.HPB_PT_ID = txtFileNo.Text.Trim();
            objInv.HPB_PATIENT_TYPE = drpInvType.SelectedValue;
            objInv.HPB_INV_AMOUNT = txtBillAmt4.Text.Trim();
            objInv.HPB_PAID_AMT = txtPaidAmount.Text.Trim();
            objInv.HPB_REJECTED_AMT = Convert.ToString(PrevPTReject);
            objInv.HPB_BALANCE_AMT = Convert.ToString((Convert.ToDecimal(txtBillAmt4.Text.Trim()) - Convert.ToDecimal(txtPaidAmount.Text.Trim()) - PrevPTReject));// - decTopUpAmt
            objInv.UserID = Convert.ToString(Session["User_ID"]);
            objInv.PatientBalHisAdd();

            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                objInv.HPB_TRANS_ID = "T" + txtInvoiceNo.Text.Trim();
                objInv.HPB_INV_AMOUNT = TxtTopupAmt.Text.Trim();
                objInv.HPB_PAID_AMT = "0";
                objInv.HPB_REJECTED_AMT = "0";
                objInv.HPB_BALANCE_AMT = TxtTopupAmt.Text.Trim();//"0";//
                objInv.PatientBalHisAdd();
            }

            InvoiceSaveLog("Invoice.btnSave   37  ");

            InvoiceSaveLog("Invoice.btnSave   38  ");
            if ((drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer") && txtClaimAmt.Text != "" && txtClaimAmt.Text != "0")
            {


                objInv.HCB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objInv.HCB_COMP_TYPE = hidInsType.Value;
                objInv.HCB_COMP_ID = hidCompanyID.Value;
                objInv.HCB_TRANS_ID = txtInvoiceNo.Text.Trim();
                objInv.HCB_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                objInv.HCB_AMT = txtClaimAmt.Text.Trim();
                objInv.HCB_PAID_AMT = Convert.ToString(PrevClaimPaid);
                objInv.HCB_REJECT_AMT = Convert.ToString(PrevCmpReject);
                objInv.HCB_BALANCE_AMT = Convert.ToString(Convert.ToDecimal(txtClaimAmt.Text.Trim()) - (PrevClaimPaid + PrevCmpReject));
                objInv.UserID = Convert.ToString(Session["User_ID"]);
                objInv.CompanytBalHisAdd();

                InvoiceSaveLog("Invoice.btnSave   39  ");


                if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                {


                    objInv.HCB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objInv.HCB_COMP_TYPE = hidInsType.Value;
                    objInv.HCB_COMP_ID = hidCompanyID.Value;
                    objInv.HCB_TRANS_ID = "T" + txtInvoiceNo.Text.Trim();
                    objInv.HCB_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                    objInv.HCB_AMT = TxtTopupAmt.Text.Trim();
                    objInv.HCB_PAID_AMT = "0";
                    objInv.HCB_REJECT_AMT = "0";
                    objInv.HCB_BALANCE_AMT = TxtTopupAmt.Text.Trim();
                    objInv.UserID = Convert.ToString(Session["User_ID"]);
                    objInv.CompanytBalHisAdd();

                }

            }
            InvoiceSaveLog("Invoice.btnSave   40  ");

            FieldNameWithValues = "EPM_INVOICENO='" + txtInvoiceNo.Text.Trim() + "'";
            Criteria = "EPM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND CONVERT(VARCHAR(10),EPM_DATE,103)='" + txtInvDate.Text.Trim() + "' AND EPM_DR_CODE = '" + txtDoctorID.Text.Trim() + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);

            InvoiceSaveLog("Invoice.btnSave   41  ");


            FieldNameWithValues = " HIM_SERVCOST_TOTAL =  (SELECT     SUM(HIT_COSTPRICE * HIT_QTY) AS TotalCost From HMS_INVOICE_TRANSACTION    WHERE      (HIT_INVOICE_ID = HMS_INVOICE_MASTER.HIM_INVOICE_ID) AND (HIT_BRANCH_ID = HMS_INVOICE_MASTER.HIM_BRANCH_ID)) ";
            Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);


            InvoiceSaveLog("Invoice.btnSave   42  ");

            InvoiceSaveLog("Invoice.btnSave   43  ");
            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                FieldNameWithValues = " HIM_SERVCOST_TOTAL =  (SELECT     SUM(HIT_COSTPRICE * ISNULL(HIT_QTY,1)) AS TotalCost From HMS_INVOICE_TRANSACTION    WHERE      (HIT_INVOICE_ID = HMS_INVOICE_MASTER.HIM_INVOICE_ID) AND (HIT_BRANCH_ID = HMS_INVOICE_MASTER.HIM_BRANCH_ID)) ";
                Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='T" + txtInvoiceNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);
            }


            FieldNameWithValues = " HIM_ATTACHMENT='" + Convert.ToString(ViewState["AttachmentData"]) + "' ,   HIM_ATTACHMENT_NAME ='" + lblFileNameActual.Text + "'";
            Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);



            InvoiceSaveLog("Invoice.btnSave   44 ");
            InvoiceSaveLog("***************** Invoice Save End *************");
            InvoiceSaveLog("                                                ");

            fnPrint(txtInvoiceNo.Text.Trim());

        FunEnd: ;
            return isError;
        }


        void UpdateAppointmentStatus()
        {
            //SAVE APPOINTMENT STATUS AS PATIENT BILLED
            string Criteria = " 1=1 ";
            Criteria += " AND HAM_FILENUMBER ='" + txtFileNo.Text.Trim() + "'";
            Criteria += " AND HAM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";

            Criteria += " AND  convert(varchar(10),HAM_STARTTIME,103)  ='" + txtInvDate.Text.Trim() + "'";

            CommonBAL objCom = new CommonBAL();

            objCom.fnUpdateTableData("HAM_STATUS=10", "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria);

        }

        void BindeAuthorizationDtls()
        {

            string Criteria = " 1=1 AND  HAR_TYPE='A' ";
            // Criteria += " AND  TREATMENT_TYPE='" + drpTreatmentType.SelectedValue + "'";HAR_STATUSHAR_ID
            Criteria += " AND HAR_PT_ID  = '" + txtFileNo.Text.Trim() + "' ";

            Criteria += " AND HAR_DR_CODE= '" + txtDoctorID.Text.Trim() + "' ";


            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAR_CREATED_DATE,101),101) >= '" + strForStartDate + "'";

                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAR_CREATED_DATE,101),101) <= '" + strForStartDate + "'";
            }


            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HAR_ID_PAYER", "HMS_AUTHORIZATION_REQUESTS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAR_ID_PAYER"]);
            }
        }

        void BindPatientVisitInvoiceIDBased()
        {

            string Criteria = " 1=1 AND HPV_STATUS <>'D' ";
            Criteria += " AND HPV_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("TOP 1 HPV_PATIENT_CLASS", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["PATIENT_CLASS"] = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PATIENT_CLASS"]);

            }
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = txtFileNo.Text;
            objCom.INVOICE_ID = txtInvoiceNo.Text;
            objCom.EMR_ID = txtEMRID.Text;
            objCom.ID = "";
            objCom.ScreenID = "INVOICE";
            objCom.ScreenName = "Patient Invoice";
            objCom.ScreenType = "TRANSACTION";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Billing");
            }


            string Criteria = " 1=1 AND HRT_SCREEN_ID='INVOICE' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSaveAndEmail.Enabled = false;
                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSaveAndEmail.Enabled = false;
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Billing");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                // string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
                // ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);


                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Patient Invoice Page");
                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    btnSaveAndEmail.Visible = true;
                    btnMailPreview.Visible = true;

                }
                try
                {

                    lblVisitType.Font.Bold = true;
                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);
                    BindSystemOption();
                    BindScreenCustomization();
                    BindYear();
                    BindNurses();
                    BindSMSTemplate();
                    fnSetdefaults();

                    if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
                    {
                        drpInvType.SelectedIndex = 1;
                    }
                    else
                    {
                        drpInvType.SelectedIndex = 0;
                    }


                    //BindDoctor();
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("HH:mm:ss");

                    txtInvDate.Text = strDate;
                    txtInvTime.Text = strTime;

                    txtClmStartDate.Text = strDate;
                    txtClmFromTime.Text = strTime;//.Substring(0, 5);

                    txtClmEndDate.Text = strDate;
                    txtClmToTime.Text = strTime; ;//.Substring(0, 5);



                    txtServStartDt.Text = strDate;
                    txtServStartTime.Text = strTime;//


                    if (GlobalValues.FileDescription == "SYNERGY")
                    {
                        // PICTrtntType01.Visible = True
                        txtICdCode.Visible = true;
                        txtICDDesc.Visible = true;
                        drpTreatmentType.Visible = false;

                    }
                    // BindVisitDtlsAll();

                    txtInvoiceNo.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "INVOICE");
                    BindBank();
                    BindCurrency();
                    BindPlanType();
                    BuildeInvoiceClaims();
                    BuildeInvoiceTrans();

                    string PatientId = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    if (PatientId != " " && PatientId != null)
                    {
                        txtFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();

                    }


                    string HPVSeqNo = "";
                    HPVSeqNo = Convert.ToString(Request.QueryString["HPVSeqNo"]);

                    if (HPVSeqNo != " " && HPVSeqNo != null)
                    {


                    }


                    string InvoiceId = "";
                    InvoiceId = Convert.ToString(Request.QueryString["InvoiceId"]);

                    if (InvoiceId != " " && InvoiceId != null)
                    {
                        txtInvoiceNo.Text = Convert.ToString(InvoiceId);
                        txtInvoiceNo_TextChanged(txtInvoiceNo, new EventArgs());
                        goto FunEnd;
                    }


                    if (GlobalValues.FileDescription == "REENABEGUM")
                    {
                        btnServUpdateToEMR.Visible = true;
                    }


                FunEnd: ;
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.Page_Load");
                    TextFileWriting(ex.Message.ToString());

                }
            }
        }

        protected void txtInvoiceNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                ViewState["SelectedVisitID"] = "";
                BindInvoice();
                TextFileWriting(" BindInvoice() completed");
                BindInvoiceTrans();
                TextFileWriting(" BindInvoiceTrans() completed");
                BindTempInvoiceTrans();
                TextFileWriting(" BindTempInvoiceTrans() completed");
                BindInvoiceClaimDtls();
                BindTempInvoiceClaims();

                AmountCalculation();
                TextFileWriting(" AmountCalculation() completed");
                if (txtEMRID.Text.Trim() != "")
                {
                    GetChiefComplaints();
                }

                BindAttachments();
                BindPatientMailID();

                BindPatientVisitInvoiceIDBased();
                string strPayerID = "", strReceiverID = "";
                if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

                    txtPayerID.Text = strPayerID;
                    txtReciverID.Text = strReceiverID;

                }
                else
                {
                    txtPayerID.Text = "Selfpay";
                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                    {
                        txtReciverID.Text = "@DHA";

                    }
                    else
                    {
                        txtReciverID.Text = "@HAAD";
                    }
                }
                TextFileWriting(" payer ID = " + strPayerID + " Received ID" + strReceiverID);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtInvoiceNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void RefereshPatientDetails(object sender, EventArgs e)
        {
            PatientDataBind();
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    Clear();
                }
                if (TopUpCardChecking(txtFileNo.Text.Trim()) == true)
                {
                    ChkTopupCard.Checked = true;

                }
                else
                {
                    ChkTopupCard.Checked = false;

                    TxtTopupAmt.Text = "0";
                }

                PatientDataBind();
                if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    BindCompanyBenefites();

                    string strPayerID = "", strReceiverID = "";
                    GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

                    txtPayerID.Text = strPayerID;
                    txtReciverID.Text = strReceiverID;

                }
                else
                {
                    txtPayerID.Text = "Selfpay";
                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                    {
                        txtReciverID.Text = "@DHA";

                    }
                    else
                    {
                        txtReciverID.Text = "@HAAD";
                    }
                }
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    IsPatientBilledToday();
                }
                Boolean bolVisitAvail = false;

                //bolVisitAvail = PatientVisitBind();

                if (Convert.ToString(ViewState["HSOM_INV_PTWAITING_ONLY"]).ToUpper() == "Y" && Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    bolVisitAvail = IsPatientVisitAvailable();
                    if (bolVisitAvail == false)
                    {
                        Clear();
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This Patient is not found in PT Waiting List.');", true);

                    }

                }
                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowInvTypeMessage1('"+ hidInvoiceType.Value +"','" + drpInvType.SelectedValue +"')", true);


                ////string vInvType = hidInvoiceType.Value;
                ////string vdrpInvType = drpInvType.SelectedValue;


                ////divInvTypeMessage.Style.Add("display", "none");

                ////if (vInvType != vdrpInvType)
                ////{
                ////    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowInvTypeMessage()", true);

                ////    divInvTypeMessage.Style.Add("display", "block");
                ////    lblInvTypeMessage.Text = "This patient is registered as " + vInvType + ". Do you want Invoice this Patient as " + vdrpInvType + " ?";

                ////    strSubCompanyID = "";
                ////}






            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }


        }

        protected void txtInvDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    goto FunEnd;
                }
                if (txtFileNo.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                Clear();

                if (TopUpCardChecking(txtFileNo.Text.Trim()) == true)
                {
                    ChkTopupCard.Checked = true;

                }
                else
                {
                    ChkTopupCard.Checked = false;

                    TxtTopupAmt.Text = "0";
                }

                PatientDataBind();
                if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    BindCompanyBenefites();
                }

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    IsPatientBilledToday();
                }
                Boolean bolVisitAvail = false;

                //bolVisitAvail = PatientVisitBind();

                if (Convert.ToString(ViewState["HSOM_INV_PTWAITING_ONLY"]).ToUpper() == "Y" && Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    bolVisitAvail = IsPatientVisitAvailable();
                    if (bolVisitAvail == false)
                    {
                        Clear();
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This Patient is not found in PT Waiting List.');", true);

                    }

                }
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }


        }


        protected void txtSubCompanyID_TextChanged1(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 ";
                DS = new DataSet();

                string strName = txtSubCompanyID.Text.Trim();
                string[] arrName = strName.Split('~');

                if (arrName.Length > 1)
                {
                    txtSubCompanyID.Text = arrName[0].Trim();
                    txtCompanyName.Text = arrName[1].Trim();
                    Criteria += " and HCM_COMP_ID='" + arrName[0].Trim() + "' ";
                }
                else
                {
                    Criteria += " and HCM_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
                }

                DS = dbo.retun_inccmp_details(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_NAME"]).Trim();
                }
                else
                {
                    txtCompanyName.Text = "";
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtSubCompanyID_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }


        protected void txtOrdClinCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string strDrCode = "", strDrName = "";



                string strName = txtOrdClinCode.Text.Trim();

                string[] arrName = strName.Split('~');
                if (arrName.Length > 1)
                {
                    txtOrdClinCode.Text = arrName[0].Trim();
                    txtOrdClinName.Text = arrName[1].Trim();

                    //txtServDrCode.Text = strDrCode;
                    //txtServDrName.Text = strDrName;


                }
                else
                {
                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        strDrCode = txtOrdClinCode.Text.Trim();
                        GetRefDrName(strDrCode, out strDrName);

                        if (strDrName != "")
                        {
                            txtOrdClinName.Text = strDrName;
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtOrdClinCode_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void txtOrdClinName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string strDrCode = "", strDrName = "";



                string strName = txtOrdClinName.Text.Trim();

                string[] arrName = strName.Split('~');
                if (arrName.Length > 1)
                {
                    txtOrdClinCode.Text = arrName[0].Trim();
                    txtOrdClinName.Text = arrName[1].Trim();




                    //txtServDrCode.Text = strDrCode;
                    //txtServDrName.Text = strDrName;


                }
                else
                {
                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        strDrCode = txtOrdClinCode.Text.Trim();
                        GetRefDrName(strDrCode, out strDrName);

                        if (strDrName != "")
                        {
                            txtOrdClinName.Text = strDrName;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtOrdClinName_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }
        protected void btnInvDiagAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "AUH")
                {
                    if (drpDiagType.SelectedValue == "Principal" && drpYearOfOnset.SelectedIndex == 0)
                    {
                        if (CheckYearOfOnSet(txtDiagCode.Text.Trim()) == true)
                        {
                            lblStatus.Text = " Please select the Year of Onset";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            goto FunEnd;
                        }
                    }

                }

                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceClaims"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HIC_ICD_CODE"] = txtDiagCode.Text.Trim();
                objrow["HIC_ICD_DESC"] = txtDiagName.Text.Trim();
                objrow["HIC_ICD_TYPE"] = drpDiagType.SelectedValue;
                objrow["HIC_YEAR_OF_ONSET_P"] = drpYearOfOnset.SelectedValue;

                DT.Rows.Add(objrow);

                ViewState["InvoiceClaims"] = DT;
                BindTempInvoiceClaims();

                txtDiagCode.Text = "";
                txtDiagName.Text = "";

                if (drpYearOfOnset.Items.Count > 0)
                {
                    drpYearOfOnset.SelectedIndex = 0;
                }
                drpDiagType.SelectedIndex = 1;

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnInvDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtServType.Text = "";
                string strName = txtServCode.Text.Trim();

                string[] arrName = strName.Split('~');
                if (arrName.Length > 1)
                {
                    txtServCode.Text = arrName[0].Trim();
                    txtServName.Text = arrName[1].Trim();
                    if (arrName.Length > 2)
                    {
                        txtServType.Text = arrName[2].Trim();
                    }
                }

                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        //protected void txtServName_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        string strName = txtServName.Text.Trim();

        //        string[] arrName = strName.Split('~');
        //        if (arrName.Length > 1)
        //        {
        //            txtServCode.Text = arrName[0].Trim();
        //            txtServName.Text = arrName[1].Trim();
        //            if (arrName.Length > 2)
        //            {
        //                txtServType.Text = arrName[2].Trim();
        //            }
        //        }

        //        LoadServiceDtls(txtServCode.Text.Trim());
        //    }
        //    catch (Exception ex)
        //    {

        //        TextFileWriting("-----------------------------------------------");
        //        TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServCode_TextChanged");
        //        TextFileWriting(ex.Message.ToString());
        //    }
        //}



        string GetDentalObserValue(string ServID)
        {

            string ObservValue = "";
            string Criteria = " 1=1 ";
            Criteria += " AND DSM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND DSM_DATE=CONVERT(DATETIME,'" + txtInvDate.Text.Trim() + "',103) AND DSM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
            Criteria += " AND DST_SERV_CODE_TYPE=" + 6;
            Criteria += " AND DST_SERV_CODE='" + ServID + "'";

            clsInvoice objInv = new clsInvoice();

            DataSet DS = new DataSet();
            DS = objInv.DrSalesOrderTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ObservValue = Convert.ToString(DS.Tables[0].Rows[0]["DST_OBS_CODE"]);
            }

            return ObservValue;
        }

        void CheckPriceFactor(string CatID, out decimal decPriceFactor)
        {
            decPriceFactor = 1;

            string strFromDate = txtInvDate.Text.Trim();
            string[] arrFromDate = strFromDate.Split('/');

            string strRawInvoicemDate = "";

            if (arrFromDate.Length > 1)
            {
                strRawInvoicemDate = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0] + " 00:00:00";
            }

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCPF_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
            Criteria += " AND HCPFD_CAT_ID='" + CatID + "'";
            Criteria += "  AND '" + strRawInvoicemDate + "' BETWEEN   HCPF_FROM_DATE AND HCPF_TO_DATE ";

            DS = objCom.fnGetFieldValue(" TOP 1 * ", "HMS_COMP_PRICE_FACTOR  INNER JOIN HMS_COMP_PRICE_FACTOR_DETAILS ON " +
                        " HCPF_FACTOR_ID = HCPFD_FACTOR_ID", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                decPriceFactor = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCPFD_PRICE_FACTOR"]);

            }


        }

        void fnServiceAddFromSO(string strServConditions, string strAgreConditions, string strObsCode) //string strServIds
        {

            string strDeptTaxAmount = "0";
            string strDrCode = "", strDrName = "", strDeptName = "";
            strDrCode = txtServDrCode.Text.Trim();

            GetStaffName(strDrCode, out strDrName, out strDeptName);

            if (strDeptName.ToUpper().Trim() == "COSMETOLOGY" || strDeptName.ToUpper().Trim() == "COSMO" || strDeptName.ToUpper().Trim() == "PLASTIC SURGERY" || strDeptName.ToUpper().Trim() == "PLASTIC")
            {
                strDeptTaxAmount = "5";
            }

            DataSet DSServ = new DataSet();
            dboperations objDBO = new dboperations();
            string ServCode, ServName;

            string Criteria1 = " 1=1 AND HSM_STATUS='A' ";

            // Criteria1 += " AND  HSM_SERV_ID  in (" + strServIds + ")";

            Criteria1 += " AND " + strServConditions;

            DSServ = objDBO.ServiceMasterGet(Criteria1);



            DataSet DSCompAgr = new DataSet();

            string CriteriaAgr = " 1=1 ";

            if (txtSubCompanyID.Text.Trim() != "")
            {
                CriteriaAgr += "  AND HAS_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
                // CriteriaAgr += " AAND HAS_SERV_ID in (" + strServIds + ") ";
                CriteriaAgr += " AND (" + strAgreConditions + ")";
            }
            else
            {
                CriteriaAgr = " 1=2";
            }

            DSCompAgr = objDBO.AgrementServiceGet(CriteriaAgr);


            foreach (DataRow DR in DSServ.Tables[0].Rows)
            {
                string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "", strSrvTaxAmount = "0", CodeType = "";
                decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0, decTaxAmount = 0, ServFeeBeforeTax, decClaimTaxAmount = 0, decCoInsAmt = 0, PriceFactor = 0;

                CodeType = Convert.ToString(DR["HSM_CODE_TYPE"]);


                ServCode = Convert.ToString(DR["HSM_SERV_ID"]);
                ServName = Convert.ToString(DR["HSM_NAME"]);


                if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES")
                {
                    if (DSServ.Tables[0].Rows[0].IsNull("HSM_TAXVALUE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TAXVALUE"]) != "")
                    {
                        strSrvTaxAmount = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TAXVALUE"]);
                    }

                    if (strDeptTaxAmount != "0")
                    {
                        strSrvTaxAmount = strDeptTaxAmount;
                    }
                }

                CommonBAL objCom = new CommonBAL();
                clsInvoice objInv = new clsInvoice();
                string Criteria = " 1=1 ";
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceTrans"];
                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HIT_SERV_CODE"] = ServCode;
                objrow["HIT_Description"] = ServName.Replace("'", "");
                objrow["HIT_QTY"] = 1;
                objrow["HIT_DISC_AMT"] = 0;
                objrow["HIT_DEDUCT"] = 0;
                objrow["HIT_CO_INS_AMT"] = 0;
                objrow["HIT_DR_CODE"] = txtDoctorID.Text;
                objrow["HIT_DR_NAME"] = txtDoctorName.Text;

                objrow["HIT_CLINICIAN_CODE"] = txtOrdClinCode.Text;
                objrow["HIT_CLINICIAN_NAME"] = txtOrdClinName.Text;

                Int32 intCurRow = 0;
                intCurRow = gvInvoiceTrans.Rows.Count - 1;

                //DataSet DSServ = new DataSet();

                //DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);


                //if (DSServ.Tables[0].Rows.Count > 0)
                //{
                ServTrnt = Convert.ToString(DR["HSM_TYPE"]);
                ServCat = Convert.ToString(DR["HSM_CAT_ID"]);

                if (GlobalValues.FileDescription.ToUpper() == "NNMC" && ServTrnt == "C" && lblVisitType.Text == "Revisit")
                {
                    ServFee = 0;
                }
                else
                {

                    ServFee = Convert.ToDecimal(DR["HSM_FEE"]);

                    if (hidCompAgrmtType.Value == "F")
                    {
                        ServFee = Convert.ToDecimal(DR["HSM_COMP_FEE"]);
                        decimal decPriceFactor = 1;
                        CheckPriceFactor(ServCat, out  decPriceFactor);
                        ServFee = ServFee * decPriceFactor;

                    }
                }




                ServiceCost = Convert.ToDecimal(Convert.ToString(DR["HSM_COST"]) != "" ? Convert.ToString(DR["HSM_COST"]) : "0");


                HaadCode = Convert.ToString(DR["HSM_HAAD_CODE"]);
                ObsNeeded = Convert.ToString(DR["HSM_OBSERVATION_NEEDED"]);
                if (ObsNeeded == "Y")
                {
                    ObsCode = Convert.ToString(DR["HSM_OBS_CODE"]);//txtObsCode.Text = 
                    ObsValueType = Convert.ToString(DR["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                    ObsConversion = DR.IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DR["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DR["HSM_OBS_CONVERSION"]);

                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB") //&& CodeType == "3"
                    {
                        ObsType = "Result";
                    }
                    //else
                    //{
                    //    ObsType = "Text";
                    //}

                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "AUH" && CodeType == "3")// && ServTrnt == "L")
                    {

                        if (txtServCode.Text == "17999" || txtServCode.Text == "15999" || txtServHaadCode.Text == "17999" || txtServHaadCode.Text == "15999")
                        {
                            ObsType = "Text";
                        }
                        else
                        {
                            ObsType = "LOINC";
                        }

                    }

                    if (CodeType == "6")
                    {
                        ObsType = "Universal Dental";
                       // ObsCode = GetDentalObserValue(ServCode);
                        ObsCode = strObsCode;
                        ObsValue = "";

                    }


                    if (ObsValue == "" && Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")//&& GlobalValues.FileDescription == "AMMC"
                    {
                        GetLabValue(ServCode, ObsConversion, out ObsValue, out  ObsValueType);
                        if (ServTrnt == "C")
                        {
                            GetBPValue(ServCode, out ObsValue, out  ObsValueType);
                        }

                    }


                }



                //}

                if (ServTrnt == "C" && Convert.ToBoolean(ViewState["NewFlag"]) == true && (txtFileNo.Text.Trim() != "OPDCR" || txtFileNo.Text.Trim() != "ADAB"))
                {
                    Criteria = "  HIT_SERV_TYPE='C' and   HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  and HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
                    DataSet DSVisitType = new DataSet();
                    DSVisitType = objInv.ReVisitTypeGet(Criteria);
                    Boolean boolVisitType = false;
                    if (DSVisitType.Tables[0].Rows.Count > 0)
                    {
                        /*
                          DateTime dtLastInvDate, dtInvDate;
                          dtLastInvDate = Convert.ToDateTime(DSVisitType.Tables[0].Rows[0]["HIM_DATE"]);

                          //dtInvDate =  Convert.ToDateTime(Convert.ToDateTime(txtInvDate.Text).ToString("MM/dd/yyyy"));           
                     
                          dtLastInvDate = Convert.ToDateTime(txtInvDate.Text.Trim());

                        
                          string strStartDate = txtInvDate.Text;
                          string[] arrDate = strStartDate.Split('/');
                          string strForStartDate = "";

                          if (arrDate.Length > 1)
                          {
                              strForStartDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                          }
                        

                         dtInvDate = Convert.ToDateTime(strForStartDate);
                       
                          TimeSpan Diff = dtInvDate - dtLastInvDate;

                          if (Diff.Days >= 2 && Diff.Days < Convert.ToInt32(Session["RevisitDays"]))
                          {
                              boolVisitType = true;
                          }
                          */

                        string dtLastInvDate, dtInvDate;

                        dtLastInvDate = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDESC"]);

                        dtInvDate = Convert.ToString(txtInvDate.Text.Trim());

                        // TimeSpan Diff = dtInvDate - dtLastInvDate;
                        Int32 intDatediff;
                        intDatediff = objCom.fnGetDateDiff(dtInvDate, dtLastInvDate);


                        if (intDatediff >= 2 && intDatediff < Convert.ToInt32(Session["RevisitDays"]))
                        {
                            boolVisitType = true;
                        }

                    }

                    if (boolVisitType == true && GlobalValues.FileDescription != "RASHIDIYA" && GlobalValues.FileDescription != "KQMC")
                    {
                        lblStatus.Text = "This patient is no need to charge consultation. Do you want to invoice.";


                    }
                }



                if (drpInvType.SelectedValue == "Cash")
                {
                    Co_Type = "%";
                    Co_Ins = 100;
                    Covered = "U";// lblCoverd.Text = "U";

                }
                else if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    /*
                    if (hidInsTrtnt.Value != "Service Type")
                    {
                        if (hidCompAgrmtType.Value != "S")
                        {

                            string CriteriaCompBenDtls = " 1=1 ";

                            if (txtSubCompanyID.Text == "")
                            {
                                CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + hidCompanyID.Value + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";
                            }
                            else
                            {
                                CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + txtSubCompanyID.Text.Trim() + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";

                            }

                            dboperations dbo = new dboperations();
                            DataSet DSCompBenDtls = new DataSet();
                            DSCompBenDtls = dbo.CompBenefitsDtlsGet(CriteriaCompBenDtls);

                            if( Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "C" || Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "P" )
                            {
                                if(hidCompAgrmtType.Value == "S")
                                {

                                }
                            }


                        }
                    }
                    else
                    {


                    }
                    */
                    Covered = "U";
                    if (txtSubCompanyID.Text != "" && hidCompAgrmtType.Value == "F")
                    {
                        Covered = "C";
                    }
                    if (txtSubCompanyID.Text != "" && hidCompAgrmtType.Value != "F") //|| CodeType == "6")
                    {


                        DataTable DTCompAgr;

                        DTCompAgr = DSCompAgr.Tables[0];
                        DataRow[] TempRow;
                        TempRow = DTCompAgr.Select("HAS_SERV_ID ='" + ServCode + "'");

                        if (TempRow.Length > 0)
                        {
                            if (TempRow[0].IsNull("HAS_NETAMOUNT") == false)
                            {
                                if (TempRow[0].IsNull("HAS_PRICE_FACTOR") == false && Convert.ToString(TempRow[0]["HAS_PRICE_FACTOR"]) != "")
                                {
                                    PriceFactor = Convert.ToDecimal(TempRow[0]["HAS_PRICE_FACTOR"]);
                                }

                                if (GlobalValues.FileDescription.ToUpper() == "NNMC" && ServTrnt == "C" && lblVisitType.Text == "Revisit")
                                {
                                    ServFee = 0;
                                }
                                else
                                {
                                    ServFee = Convert.ToDecimal(TempRow[0]["HAS_COMP_FEE"]) * PriceFactor;
                                }

                                Covered = Convert.ToString(TempRow[0]["HAS_COVERING_TYPE"]);//lblCoverd.Text = 

                                Co_Type = Convert.ToString(TempRow[0]["HAS_DISC_TYPE"]);
                                Co_Ins = Convert.ToDecimal(TempRow[0]["HAS_DISC_AMT"]);

                                ServCat = Convert.ToString(TempRow[0]["HAS_CATEGORY_ID"]);//lblgvCatID.Text = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_CATEGORY_ID"]);
                                ServiceCost = Convert.ToDecimal(Convert.ToString(TempRow[0]["HAS_SERV_COST"]) != "" ? Convert.ToString(TempRow[0]["HAS_SERV_COST"]) : "0");


                                DiscType = Convert.ToString(TempRow[0]["HAS_DISC_TYPE"]);
                                DiscAmount = Convert.ToDecimal(Convert.ToString(TempRow[0]["HAS_DISC_AMT"]) != "" ? Convert.ToString(TempRow[0]["HAS_DISC_AMT"]) : "0");

                                // ServFee = Val(RsCompAgrmt.Fields("HAS_COMP_FEE").Value)

                                //  ServTrnt = Trim(RsCompAgrmt.Fields("HAS_TREATMENT_TYPE").Value)
                                //  ServCat = Trim(RsCompAgrmt.Fields("HAS_CATEGORY_ID").Value)


                                //  Agrmt_Serv_Desc = Trim(RsCompAgrmt.Fields("HAS_SERV_NAME").Value)
                                //  Covered = "C"

                                objrow["HIT_Description"] = Convert.ToString(TempRow[0]["HAS_SERV_NAME"]);

                            }

                        }
                        else
                        {
                            Co_Type = "%";
                            Co_Ins = 100;
                            Covered = "U";
                        }

                    }


                }


                string strHaadName = "", strHaadType = "0";
                if (HaadCode != "")
                {
                    DataSet DSHaad = new DataSet();
                    DSHaad = GetHaadName(HaadCode);

                    if (DSHaad.Tables[0].Rows.Count > 0)
                    {
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                        {
                            strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                        }
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                        {
                            strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                        }
                    }

                }

                objrow["HIT_SERV_TYPE"] = ServTrnt;
                objrow["HIT_CAT_ID"] = ServCat;
                objrow["HIT_COVERED"] = Covered;// lblCoverd.Text.Trim();
                // objrow["HIT_REFNO"] = lblgvRefNo.Text.Trim();

                objrow["HIT_COSTPRICE"] = ServiceCost.ToString("#0.00"); ;// lblgvCostPrice.Text.Trim();
                objrow["HIT_SERVCOST"] = ServiceCost.ToString("#0.00"); ;
                //   objrow["HIT_ADJ_DEDUCT_AMOUNT"] = lblgvAdjDedAmt.Text.Trim();

                objrow["HIT_FEE"] = ServFee.ToString("#0.00"); ;

                objrow["HIT_DISC_TYPE"] = DiscType;
                objrow["HIT_DISC_AMT"] = DiscAmount.ToString("#0.00"); ;

                decimal decDiscount = 0;


                if (DiscType == "%")
                {
                    decDiscount = ((ServFee / 100) * DiscAmount);
                }
                else
                {
                    decDiscount = DiscAmount;

                }

                objrow["HIT_QTY"] = "1";
                //objrow["HIT_AMOUNT"] = Convert.ToString(ServFee * 1);
                objrow["HIT_AMOUNT"] = ServFee - decDiscount;


                ServFee = Convert.ToDecimal(objrow["HIT_AMOUNT"]);
                ServFeeBeforeTax = Convert.ToDecimal(objrow["HIT_AMOUNT"]);

                if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
                {
                    if (strSrvTaxAmount != "" && strSrvTaxAmount != "0")
                    {
                        decTaxAmount = ((ServFee) / 100) * Convert.ToDecimal(strSrvTaxAmount);
                    }
                    ServFee = ServFee + decTaxAmount;
                }

                objrow["HIT_AMOUNT"] = ServFee.ToString("#0.00"); ;

                objrow["HIT_HAAD_CODE"] = HaadCode;
                objrow["HIT_TYPE_VALUE"] = strHaadType;

                objrow["HIT_STARTDATEDesc"] = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim();     // txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();

                objrow["HIO_TYPE"] = ObsType;

                //objrow["HIO_SERV_CODE"] = ObsCode;
                objrow["HIO_CODE"] = ObsCode;

                objrow["HIO_VALUE"] = ObsValue;
                objrow["HIO_VALUETYPE"] = ObsValueType;


                if (drpInvType.SelectedValue == "Cash")
                {
                    objrow["HIT_CO_INS_TYPE"] = "%";
                    objrow["HIT_CO_INS_AMT"] = "100";
                }


                decimal decDedAmt = 0, decConInsAmt = 0, decCoInsAmtBeforeTax = 0;
                if (txtSubCompanyID.Text != "")
                {
                    if (Convert.ToString(ViewState["Deductible"]) != "")
                    {
                        decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
                    }
                }
                if (Convert.ToString(ViewState["Co_Ins_Amt"]) != "")
                {
                    if (ServTrnt == "D")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtDental"]);

                    }
                    else if (ServTrnt == "P")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtPharmacy"]);

                    }
                    else if (ServTrnt == "L")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtLab"]);

                    }
                    else if (ServTrnt == "R")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtRad"]);

                    }
                    else
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
                    }
                }

                //ProcessTimLog("fnServiceAdd()_ 8");
                if (ServTrnt == "C")
                {
                    if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                    {
                        objrow["HIT_DEDUCT"] = decDedAmt.ToString("#0.00"); ;

                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_TYPE"] = "$";
                            objrow["HIT_CO_INS_AMT"] = decDedAmt.ToString("#0.00");

                            decCoInsAmtBeforeTax = decDedAmt;
                        }
                    }
                    else
                    {
                        objrow["HIT_DEDUCT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_TYPE"] = "$";
                            objrow["HIT_CO_INS_AMT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);

                            decCoInsAmtBeforeTax = Convert.ToDecimal((ServFeeBeforeTax * decDedAmt) / 100);// Convert.ToString(ViewState["Co_Ins_Amt"]);
                        }



                    }

                    decCoInsAmt = Convert.ToDecimal(objrow["HIT_CO_INS_AMT"]);
                }
                else
                {
                    objrow["HIT_DEDUCT"] = "0";
                    if (txtSubCompanyID.Text != "")
                    {
                        objrow["HIT_CO_INS_TYPE"] = Convert.ToString(ViewState["Co_Ins_Type"]);
                    }


                    if (Convert.ToString(ViewState["Co_Ins_Type"]) == "$")
                    {
                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00");
                            decCoInsAmtBeforeTax = decConInsAmt;
                        }
                    }
                    else
                    {
                        if (txtSubCompanyID.Text != "")
                        {
                            ////objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                            ////decCoInsAmtBeforeTax = decConInsAmt;

                            objrow["HIT_CO_INS_TYPE"] = "%";
                            objrow["HIT_CO_INS_AMT"] = decConInsAmt;// Convert.ToString(ViewState["Co_Ins_Amt"]);

                            decCoInsAmtBeforeTax = Convert.ToDecimal((ServFeeBeforeTax * decConInsAmt) / 100);// Convert.ToString(ViewState["Co_Ins_Amt"]);
                        }
                    }

                    decCoInsAmt = Convert.ToDecimal(objrow["HIT_CO_INS_AMT"]);
                }


                //ProcessTimLog("fnServiceAdd()_ 9");

                //objrow["HIT_CO_TOTAL"] =
                //objrow["HIT_COMP_TYPE"] =
                //objrow["HIT_COMP_AMT"] =
                //objrow["HIT_COMP_TOTAL"] = 
                //txtgvCoInsTotal.Text = Convert.ToString(decCoInsAmt);
                //txtgvCompType.Text = drpCoInsType.Text;
                //txtgvCompAmount.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);
                //txtgvCompTotal.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);

                // decClaimTaxAmount =  ServFeeBeforeTax -

                objrow["HIT_TAXPERCENT"] = strSrvTaxAmount;
                objrow["HIT_TAXAMOUNT"] = decTaxAmount.ToString("#0.00");

                if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
                {
                    if (strSrvTaxAmount != "" && strSrvTaxAmount != "0")
                    {
                        decClaimTaxAmount = ((ServFeeBeforeTax - decCoInsAmtBeforeTax) / 100) * Convert.ToDecimal(strSrvTaxAmount);
                    }
                }

                objrow["HIT_CLAIMAMOUNTTAX"] = decClaimTaxAmount.ToString("#0.00");

                DT.Rows.Add(objrow);
                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;



            }

            ProcessTimLog(System.DateTime.Now.ToString() + "      BindTempInvoiceTrans() Start");
            BindTempInvoiceTrans();
            ProcessTimLog(System.DateTime.Now.ToString() + "      BindTempInvoiceTrans() End");

            ProcessTimLog(System.DateTime.Now.ToString() + "      AmountCalculation() Start");
            AmountCalculation();
            ProcessTimLog(System.DateTime.Now.ToString() + "      AmountCalculation() End");

        }

        void LoadServiceDtls(string ServCode)
        {

            Boolean IsConsultExist = false;

            for (int i = 0; i < gvInvoiceTrans.Rows.Count; i++)
            {

                Label lblServType1 = (Label)gvInvoiceTrans.Rows[i].FindControl("lblServType");

                if (Convert.ToString(ViewState["InvTransSelectIndex"]) != "")
                {
                    if (Convert.ToInt32(ViewState["InvTransSelectIndex"]) != i)
                    {
                        if (lblServType1.Text == "C")
                        {
                            IsConsultExist = true;
                        }
                    }

                }
                else
                {
                    if (lblServType1.Text == "C")
                    {
                        IsConsultExist = true;
                    }
                }


            }

            string strDeptTaxAmount = "0";
            string strDrCode = "", strDrName = "", strDeptName = "";
            strDrCode = txtServDrCode.Text.Trim();

            GetStaffName(strDrCode, out strDrName, out strDeptName);

            if (strDeptName.ToUpper().Trim() == "COSMETOLOGY" || strDeptName.ToUpper().Trim() == "COSMO" || strDeptName.ToUpper().Trim() == "PLASTIC SURGERY" || strDeptName.ToUpper().Trim() == "PLASTIC")
            {
                strDeptTaxAmount = "5";
            }


            CommonBAL objCom = new CommonBAL();
            clsInvoice objInv = new clsInvoice();

            string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "", CodeType = "", strSrvTaxAmount = "0", strServType = "";
            decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0, decTaxAmount = 0, ServFeeBeforeTax, decClaimTaxAmount = 0, decCoInsAmt = 0, PriceFactor = 1;
            DataSet DSServ = new DataSet();

            DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);


            //ProcessTimLog("fnServiceAdd()_ 1");
            if (DSServ.Tables[0].Rows.Count > 0)
            {
                strServType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);

                CodeType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
                ServCat = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);

                if (GlobalValues.FileDescription.ToUpper() == "NNMC" && strServType == "C" && lblVisitType.Text == "Revisit")
                {
                    ServFee = 0;
                }
                else
                {
                    ServFee = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

                    if (hidCompAgrmtType.Value == "F")
                    {
                        ServFee = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_COMP_FEE"]);
                        decimal decPriceFactor = 1;
                        CheckPriceFactor(ServCat, out  decPriceFactor);
                        ServFee = ServFee * decPriceFactor;

                    }

                }


                ServTrnt = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);



                ViewState["ServHAAD_ID"] = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                ViewState["ServCAT_ID"] = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                ViewState["ServCAT_type"] = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
                ViewState["Serv_type"] = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);







                txtServCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                txtServName.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                txtServType.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPEDesc"]);



                if (IsConsultExist == true && strServType == "C" && (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer"))
                {
                    txtServCode.Text = "";
                    txtServName.Text = "";
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Sorry, You can invoice only one Consultation for Credit Patients.');", true);
                }





                if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES")
                {
                    if (DSServ.Tables[0].Rows[0].IsNull("HSM_TAXVALUE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TAXVALUE"]) != "")
                    {
                        strSrvTaxAmount = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TAXVALUE"]);
                    }


                    if (strDeptTaxAmount != "0")
                    {
                        strSrvTaxAmount = strDeptTaxAmount;
                    }
                }

                txtServCostPrice.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_COST"]) != "" ? Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_COST"]) : "0";


                txtServHaadCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]) != "" ? Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]) : Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]); // Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]);// txtServHaadDesc.Text 
                ObsNeeded = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
                if (ObsNeeded == "Y")
                {
                    ObsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);//txtObsCode.Text = 
                    ObsValueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                    ObsConversion = DSServ.Tables[0].Rows[0].IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]);

                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")//  && CodeType == "3"
                    {
                        ObsType = "Result";
                    }
                    //else
                    //{
                    //    ObsType = "Text";
                    //}

                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "AUH" && CodeType == "3")// && ServTrnt == "L")
                    {

                        if (txtServCode.Text == "17999" || txtServCode.Text == "15999" || txtServHaadCode.Text == "17999" || txtServHaadCode.Text == "15999")
                        {
                            ObsType = "Text";
                        }
                        else
                        {
                            ObsType = "LOINC";
                        }

                    }

                    if (CodeType == "6")
                    {
                        ObsType = "Universal Dental";
                        ObsCode = GetDentalObserValue(ServCode);
                        ObsValue = "";

                    }


                    if (ObsValue == "" && Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")//&& GlobalValues.FileDescription == "AMMC"
                    {
                        GetLabValue(ServCode, ObsConversion, out ObsValue, out  ObsValueType);
                        if (ServTrnt == "C")
                        {
                            GetBPValue(ServCode, out ObsValue, out  ObsValueType);
                        }

                    }


                }


            }


            txtServObsType.Text = ObsType;
            txtServObsCode.Text = ObsCode;
            txtServObsValue.Text = ObsValue;
            txtServObsValueType.Text = ObsValueType;



            //ProcessTimLog("fnServiceAdd()_ 2");
            if (ServTrnt == "C" && Convert.ToBoolean(ViewState["NewFlag"]) == true && txtFileNo.Text.Trim() != "OPDCR" || txtFileNo.Text.Trim() != "ADAB")
            {
                string Criteria = "  HIT_SERV_TYPE='C' and   HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  and HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
                DataSet DSVisitType = new DataSet();
                DSVisitType = objInv.ReVisitTypeGet(Criteria);
                Boolean boolVisitType = false;
                if (DSVisitType.Tables[0].Rows.Count > 0)
                {
                    /*
                      DateTime dtLastInvDate, dtInvDate;
                      dtLastInvDate = Convert.ToDateTime(DSVisitType.Tables[0].Rows[0]["HIM_DATE"]);

                      //dtInvDate =  Convert.ToDateTime(Convert.ToDateTime(txtInvDate.Text).ToString("MM/dd/yyyy"));           
                     
                      dtLastInvDate = Convert.ToDateTime(txtInvDate.Text.Trim());

                        
                      string strStartDate = txtInvDate.Text;
                      string[] arrDate = strStartDate.Split('/');
                      string strForStartDate = "";

                      if (arrDate.Length > 1)
                      {
                          strForStartDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                      }
                        

                     dtInvDate = Convert.ToDateTime(strForStartDate);
                       
                      TimeSpan Diff = dtInvDate - dtLastInvDate;

                      if (Diff.Days >= 2 && Diff.Days < Convert.ToInt32(Session["RevisitDays"]))
                      {
                          boolVisitType = true;
                      }
                      */

                    string dtLastInvDate, dtInvDate;
                    // TextFileWriting("1");
                    dtLastInvDate = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDESC"]);
                    // TextFileWriting("2");
                    dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                    //  TextFileWriting("3");
                    // TimeSpan Diff = dtInvDate - dtLastInvDate;
                    Int32 intDatediff;
                    intDatediff = objCom.fnGetDateDiff(dtInvDate, dtLastInvDate);


                    if (intDatediff >= 2 && intDatediff < Convert.ToInt32(Session["RevisitDays"]))
                    {
                        boolVisitType = true;
                    }

                }

                if (boolVisitType == true && GlobalValues.FileDescription != "RASHIDIYA" && GlobalValues.FileDescription != "KQMC")
                {
                    lblStatus.Text = "This patient is no need to charge consultation. Do you want to invoice.";


                }
            }

            //ProcessTimLog("fnServiceAdd()_ 3");

            if (drpInvType.SelectedValue == "Cash")
            {
                Co_Type = "%";
                Co_Ins = 100;
                Covered = "U";// lblCoverd.Text = "U";

            }
            else if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
            {
                /*
                if (hidInsTrtnt.Value != "Service Type")
                {
                    if (hidCompAgrmtType.Value != "S")
                    {

                        string CriteriaCompBenDtls = " 1=1 ";

                        if (txtSubCompanyID.Text == "")
                        {
                            CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + hidCompanyID.Value + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";
                        }
                        else
                        {
                            CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + txtSubCompanyID.Text.Trim() + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";

                        }

                        dboperations dbo = new dboperations();
                        DataSet DSCompBenDtls = new DataSet();
                        DSCompBenDtls = dbo.CompBenefitsDtlsGet(CriteriaCompBenDtls);

                        if( Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "C" || Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "P" )
                        {
                            if(hidCompAgrmtType.Value == "S")
                            {

                            }
                        }


                    }
                }
                else
                {


                }
                */
                Covered = "U";

                if (txtSubCompanyID.Text != "" && hidCompAgrmtType.Value == "F")
                {
                    Covered = "C";
                }

                if (txtSubCompanyID.Text != "" && hidCompAgrmtType.Value != "F")  //)|| CodeType == "6")
                {
                    DataSet DSCompAgr = new DataSet();

                    DSCompAgr = GetCompanyAgrDtls(ServCode, txtSubCompanyID.Text.Trim());

                    if (DSCompAgr.Tables[0].Rows.Count > 0)
                    {
                        txtServName.Text = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_SERV_NAME"]);


                        if (DSCompAgr.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
                        {
                            if (DSCompAgr.Tables[0].Rows[0].IsNull("HAS_PRICE_FACTOR") == false && Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_PRICE_FACTOR"]) != "")
                            {
                                PriceFactor = Convert.ToDecimal(DSCompAgr.Tables[0].Rows[0]["HAS_PRICE_FACTOR"]);
                            }
                            if (GlobalValues.FileDescription.ToUpper() == "NNMC" && strServType == "C" && lblVisitType.Text == "Revisit")
                            {
                                ServFee = 0;
                            }
                            else
                            {

                                ServFee = Convert.ToDecimal(DSCompAgr.Tables[0].Rows[0]["HAS_COMP_FEE"]) * PriceFactor;
                            }

                            Covered = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_COVERING_TYPE"]);//lblCoverd.Text = 

                            Co_Type = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_DISC_TYPE"]);
                            Co_Ins = Convert.ToDecimal(DSCompAgr.Tables[0].Rows[0]["HAS_DISC_AMT"]);

                            ServCat = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_CATEGORY_ID"]);//lblgvCatID.Text = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_CATEGORY_ID"]);
                            ServiceCost = Convert.ToDecimal(Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_SERV_COST"]) != "" ? Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_SERV_COST"]) : "0");


                            DiscType = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_DISC_TYPE"]);
                            DiscAmount = Convert.ToDecimal(Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_DISC_AMT"]) != "" ? Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_DISC_AMT"]) : "0");

                            // ServFee = Val(RsCompAgrmt.Fields("HAS_COMP_FEE").Value)

                            //  ServTrnt = Trim(RsCompAgrmt.Fields("HAS_TREATMENT_TYPE").Value)
                            //  ServCat = Trim(RsCompAgrmt.Fields("HAS_CATEGORY_ID").Value)


                            //  Agrmt_Serv_Desc = Trim(RsCompAgrmt.Fields("HAS_SERV_NAME").Value)
                            //  Covered = "C"



                        }

                    }
                    else
                    {
                        Co_Type = "%";
                        Co_Ins = 100;
                        Covered = "U";
                    }

                }


            }


            ViewState["Serv_Coverd"] = Covered;
            ////if (DiscAmount != Convert.ToDecimal(txtServDiscAmt.Text))
            ////{

            ////    DiscAmount = Convert.ToDecimal(txtServDiscAmt.Text);
            ////}

            string strHaadName = "", strHaadType = "0";
            if (HaadCode != "")
            {
                DataSet DSHaad = new DataSet();
                DSHaad = GetHaadName(HaadCode);

                if (DSHaad.Tables[0].Rows.Count > 0)
                {
                    if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                    {
                        strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                    }
                    if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                    {
                        strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                    }
                }

            }
            txtServHaadDesc.Text = strHaadName;
            ////objrow["HIT_SERV_TYPE"] = ServTrnt;
            ////objrow["HIT_CAT_ID"] = ServCat;
            ////objrow["HIT_COVERED"] = Covered;// lblCoverd.Text.Trim();


            ////objrow["HIT_COSTPRICE"] = ServiceCost.ToString("#0.00"); ;// lblgvCostPrice.Text.Trim();
            ////objrow["HIT_SERVCOST"] = ServiceCost.ToString("#0.00"); ;

            if (Convert.ToString(ViewState["FeeChanged"]) == "Yes")
            {
                ServFee = Convert.ToDecimal(txtServPrice.Text);
            }

            txtServPrice.Text = ServFee.ToString("#0.00");





            if (Convert.ToString(ViewState["DiscTypeChanged"]) == "Yes") // if (DiscType != drpServDiscType.SelectedValue)
            {
                DiscType = drpServDiscType.SelectedValue;

            }
            else
            {

                for (int intCount = 0; intCount < drpServDiscType.Items.Count; intCount++)
                {
                    if (drpServDiscType.Items[intCount].Value == DiscType)
                    {
                        drpServDiscType.SelectedValue = DiscType;
                    }

                }


            }


            if (Convert.ToString(ViewState["DiscAmtChanged"]) == "Yes")
            {
                DiscAmount = Convert.ToDecimal(txtServDiscAmt.Text);

            }
            else
            {
                txtServDiscAmt.Text = DiscAmount.ToString("#0.00"); ;
            }




            decimal decDiscount = 0;


            if (DiscType == "%")
            {
                decDiscount = ((ServFee / 100) * DiscAmount);
            }
            else
            {
                decDiscount = DiscAmount;

            }



            decimal decTotQtyDisc = 0, decServAmt = 0, decSingleQtyTax = 0;
            // decServAmt = Convert.ToDecimal(txtServAmount.Text);
            decTotQtyDisc = Convert.ToDecimal(txtServQty.Text.Trim()) * decDiscount;

            ServFee = ServFee - decDiscount;// decTotQtyDisc;
            // txtServAmount.Text =  ServFee.ToString("#0.00");

            ServFeeBeforeTax = ServFee;

            if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
            {
                if (strSrvTaxAmount != "" && strSrvTaxAmount != "0")
                {
                    decTaxAmount = ((ServFee * Convert.ToDecimal(txtServQty.Text.Trim())) / 100) * Convert.ToDecimal(strSrvTaxAmount);

                    decSingleQtyTax = ((ServFee) / 100) * Convert.ToDecimal(strSrvTaxAmount);
                }
                ServFee = ServFee + decSingleQtyTax;
            }

            txtServAmount.Text = (ServFee * Convert.ToDecimal(txtServQty.Text.Trim())).ToString("#0.00");




            ////objrow["HIT_HAAD_CODE"] = HaadCode;
            ////objrow["HIT_TYPE_VALUE"] = strHaadType;
            ////objrow["HIO_TYPE"] = ObsType;
            ////objrow["HIO_CODE"] = ObsCode;
            ////objrow["HIO_VALUE"] = ObsValue;
            // objrow["HIO_VALUETYPE"] = ObsValueType;


            if (drpInvType.SelectedValue == "Cash" || Convert.ToString(ViewState["Serv_Coverd"]).ToUpper() == "U")
            {
                if (Convert.ToString(ViewState["CoInsPresChanged"]) != "Yes")
                {
                    drpServCoInsPres.SelectedValue = "%";
                }


                if (Convert.ToString(ViewState["CoInsAmtChanged"]) != "Yes")
                {
                    if (txtServCoInsAmount.Text != "")
                    {
                        if (Convert.ToDecimal(txtServCoInsAmount.Text) <= 0)
                        {
                            txtServCoInsAmount.Text = "100";
                        }
                    }
                }


            }


            decimal decDedAmt = 0, decConInsAmt = 0, decCoInsAmtBeforeTax = 0;

            if (Convert.ToString(ViewState["DectAmtChanged"]) == "Yes")
            {
                decDedAmt = Convert.ToDecimal(txtServDedect.Text.Trim());
            }
            else
            {
                if (txtSubCompanyID.Text != "" && Convert.ToString(ViewState["Serv_Coverd"]).ToUpper() == "C")
                {
                    if (Convert.ToString(ViewState["Deductible"]) != "")
                    {
                        if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                        {
                            decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
                        }
                        else
                        {
                            decDedAmt = Convert.ToDecimal((ServFee * Convert.ToDecimal(ViewState["Deductible"])) / 100);
                        }

                        txtServDedect.Text = decDedAmt.ToString("#0.00");
                    }
                }
            }

            //if (Convert.ToDecimal(ViewState["Deductible"]) == Convert.ToDecimal(txtServDedect.Text))
            //{

            //    decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
            //}
            //else
            //{
            //    decDedAmt = Convert.ToDecimal(txtServDedect.Text);
            //}




            //if (decDedAmt != Convert.ToDecimal(txtServDedect.Text) && Convert.ToDecimal(txtServDedect.Text) > 0)
            //{
            //    decDedAmt = Convert.ToDecimal(txtServDedect.Text.Trim());
            //}








            //ProcessTimLog("fnServiceAdd()_ 8");
            if (ServTrnt == "C")
            {


                if (txtSubCompanyID.Text != "" && Convert.ToString(ViewState["Serv_Coverd"]).ToUpper() == "C")
                {
                    if (Convert.ToString(ViewState["CoInsPresChanged"]) != "Yes")
                    {
                        drpServCoInsPres.SelectedValue = "$";
                    }

                    if (Convert.ToString(ViewState["CoInsAmtChanged"]) != "Yes")
                    {
                        txtServCoInsAmount.Text = decDedAmt.ToString("#0.00");
                    }


                    decCoInsAmtBeforeTax = decDedAmt;
                }
                //}
                //else
                //{
                //    txtServDedect.Text = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                //    if (txtSubCompanyID.Text != "")
                //    {
                //        drpServCoInsPres.SelectedValue = "$";
                //        txtServCoInsAmount.Text = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);

                //        decCoInsAmtBeforeTax = Convert.ToDecimal((ServFeeBeforeTax * decDedAmt) / 100);// Convert.ToString(ViewState["Co_Ins_Amt"]);
                //    }



                //}


                decCoInsAmt = Convert.ToDecimal(txtServCoInsAmount.Text);
            }
            else
            {
                txtServDedect.Text = "0.00";

                if (txtSubCompanyID.Text != "" && Convert.ToString(ViewState["Serv_Coverd"]).ToUpper() == "C")
                {



                    if (ServTrnt == "D" && Convert.ToString(ViewState["Co_Ins_AmtDental"]) != "")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtDental"]);

                    }
                    else if (ServTrnt == "P" && Convert.ToString(ViewState["Co_Ins_AmtPharmacy"]) != "")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtPharmacy"]);

                    }
                    else if (ServTrnt == "L" && Convert.ToString(ViewState["Co_Ins_AmtLab"]) != "")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtLab"]);

                    }
                    else if (ServTrnt == "R" && Convert.ToString(ViewState["Co_Ins_AmtRad"]) != "")
                    {
                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtRad"]);

                    }
                    else
                    {
                        if (Convert.ToString(ViewState["Co_Ins_Amt"]) != "")
                        {

                            decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
                        }
                    }


                    if (Convert.ToString(ViewState["CoInsAmtChanged"]) == "Yes") //if (decConInsAmt != Convert.ToDecimal(txtServCoInsAmount.Text) && Convert.ToDecimal(txtServCoInsAmount.Text) > 0)
                    {
                        decConInsAmt = Convert.ToDecimal(txtServCoInsAmount.Text.Trim());
                    }
                    else
                    {
                        txtServCoInsAmount.Text = Convert.ToString(decConInsAmt);

                    }

                    if (Convert.ToString(ViewState["CoInsPresChanged"]) != "Yes")
                    {
                        drpServCoInsPres.SelectedValue = Convert.ToString(ViewState["Co_Ins_Type"]);
                    }


                }

                if (drpServCoInsPres.SelectedValue == "$")// if (Convert.ToString(ViewState["Co_Ins_Type"]) == "$")
                {
                    if (txtSubCompanyID.Text != "" && Convert.ToString(ViewState["Serv_Coverd"]).ToUpper() == "C")
                    {
                        ///txtServCoInsAmount.Text = decConInsAmt.ToString("#0.00");
                        decCoInsAmtBeforeTax = decConInsAmt;
                    }
                }
                else
                {
                    if (txtSubCompanyID.Text != "" && Convert.ToString(ViewState["Serv_Coverd"]).ToUpper() == "C")
                    {
                        ////objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                        ////decCoInsAmtBeforeTax = decConInsAmt;

                        drpServCoInsPres.SelectedValue = "%";
                        //  txtServCoInsAmount.Text = decConInsAmt.ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);

                        decCoInsAmtBeforeTax = Convert.ToDecimal((((ServFeeBeforeTax * Convert.ToDecimal(txtServQty.Text.Trim()))) * decConInsAmt) / 100);// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }
                }

                decCoInsAmt = Convert.ToDecimal(txtServCoInsAmount.Text);
            }




            txtServTaxPresent.Text = strSrvTaxAmount;
            txtServTaxAmount.Text = decTaxAmount.ToString("#0.00");

            if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
            {
                if (strSrvTaxAmount != "" && strSrvTaxAmount != "0")
                {
                    decClaimTaxAmount = (((ServFeeBeforeTax * Convert.ToDecimal(txtServQty.Text.Trim())) - decCoInsAmtBeforeTax) / 100) * Convert.ToDecimal(strSrvTaxAmount);
                }
            }

            txtServClaimTaxAmount.Text = decClaimTaxAmount.ToString("#0.00");



        }

        void LoadServiceDtlsFromPopup()
        {
            CommonBAL objCom = new CommonBAL();
            clsInvoice objInv = new clsInvoice();
            string Criteria = " 1=1 ";
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceTrans"];

            if (ViewState["InvTransSelectIndex"] == "" || ViewState["InvTransSelectIndex"] == null)
            {
                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HIT_SERV_CODE"] = txtServCode.Text.Trim();
                objrow["HIT_Description"] = txtServName.Text.Trim().Replace("'", "");
                if (txtServQty.Text.Trim() == "")
                {
                    objrow["HIT_QTY"] = "1";
                }
                {
                    objrow["HIT_QTY"] = txtServQty.Text.Trim();
                }
                objrow["HIT_DISC_AMT"] = 0;
                objrow["HIT_DEDUCT"] = 0;
                objrow["HIT_CO_INS_AMT"] = 0;
                objrow["HIT_DR_CODE"] = txtServDrCode.Text;
                objrow["HIT_DR_NAME"] = txtServDrName.Text;

                objrow["HIT_CLINICIAN_CODE"] = txtServOrdClinCode.Text;
                objrow["HIT_CLINICIAN_NAME"] = txtServOrdClinName.Text;

                Int32 intCurRow = 0;
                intCurRow = gvInvoiceTrans.Rows.Count - 1;

                //  string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "", CodeType = "", strSrvTaxAmount = "0";
                //  decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0, decTaxAmount = 0, ServFeeBeforeTax, decClaimTaxAmount = 0, decCoInsAmt = 0;
                DataSet DSServ = new DataSet();





                objrow["HIT_SERV_TYPE"] = Convert.ToString(ViewState["Serv_type"]); // ServTrnt;
                objrow["HIT_CAT_ID"] = Convert.ToString(ViewState["ServCAT_ID"]);// hidCatID.Value;// ServCat;
                objrow["HIT_COVERED"] = Convert.ToString(ViewState["Serv_Coverd"]);// Covered;// lblCoverd.Text.Trim();
                // objrow["HIT_REFNO"] = lblgvRefNo.Text.Trim();


                if (txtServCostPrice.Text.Trim() != "")
                {
                    objrow["HIT_COSTPRICE"] = txtServCostPrice.Text.Trim();
                }
                else
                {
                    objrow["HIT_COSTPRICE"] = "0";
                }

                if (txtServCostPrice.Text.Trim() != "")
                {
                    objrow["HIT_SERVCOST"] = txtServCostPrice.Text.Trim();
                }
                else
                {
                    objrow["HIT_SERVCOST"] = "0";
                }

                //   objrow["HIT_ADJ_DEDUCT_AMOUNT"] = lblgvAdjDedAmt.Text.Trim();



                if (txtServPrice.Text.Trim() != "")
                {
                    objrow["HIT_FEE"] = txtServPrice.Text.Trim();
                }
                else
                {
                    objrow["HIT_FEE"] = "0";
                }

                objrow["HIT_DISC_TYPE"] = drpServDiscType.SelectedValue;



                objrow["HIT_DISC_AMT"] = txtServDiscAmt.Text.Trim();
                objrow["HIT_DEDUCT"] = txtServDedect.Text.Trim();


                objrow["HIT_QTY"] = txtServQty.Text.Trim();

                if (txtServAmount.Text.Trim() != "")
                {
                    objrow["HIT_AMOUNT"] = txtServAmount.Text.Trim();
                }
                else
                {
                    objrow["HIT_AMOUNT"] = "0";
                }



                objrow["HIT_HAAD_CODE"] = txtServHaadCode.Text.Trim();

                if (Convert.ToString(ViewState["ServCAT_type"]) != "")
                {
                    objrow["HIT_TYPE_VALUE"] = Convert.ToString(ViewState["ServCAT_type"]);
                }
                else
                {
                    objrow["HIT_TYPE_VALUE"] = "0";
                }

                objrow["HIT_AUTHORIZATIONID"] = txtServAuthID.Text.Trim();


                objrow["HIT_STARTDATEDesc"] = txtServStartDt.Text.Trim() + " " + txtServStartTime.Text.Trim();     // txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();

                objrow["HIO_TYPE"] = txtServObsType.Text.Trim();

                //objrow["HIO_SERV_CODE"] = ObsCode;
                objrow["HIO_CODE"] = txtServObsCode.Text.Trim();

                objrow["HIO_VALUE"] = txtServObsValue.Text.Trim();
                objrow["HIO_VALUETYPE"] = txtServObsValueType.Text.Trim();



                objrow["HIT_CO_INS_TYPE"] = drpServCoInsPres.SelectedValue;

                if (txtServCoInsAmount.Text.Trim() != "")
                {
                    objrow["HIT_CO_INS_AMT"] = txtServCoInsAmount.Text.Trim();
                }
                else
                {
                    objrow["HIT_CO_INS_AMT"] = "0";
                }





                objrow["HIT_TAXPERCENT"] = txtServTaxPresent.Text.Trim();



                if (txtServTaxAmount.Text.Trim() != "")
                {
                    objrow["HIT_TAXAMOUNT"] = txtServTaxAmount.Text.Trim();
                }
                else
                {
                    objrow["HIT_TAXAMOUNT"] = "0";
                }



                if (txtServClaimTaxAmount.Text.Trim() != "")
                {
                    objrow["HIT_CLAIMAMOUNTTAX"] = txtServClaimTaxAmount.Text.Trim();
                }
                else
                {
                    objrow["HIT_CLAIMAMOUNTTAX"] = "0";
                }


                DT.Rows.Add(objrow);
                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;

            }
            else
            {
                Int32 R = Convert.ToInt32(ViewState["InvTransSelectIndex"]);
                DT.Rows[R]["HIT_SERV_CODE"] = txtServCode.Text.Trim();
                DT.Rows[R]["HIT_Description"] = txtServName.Text.Trim().Replace("'", "");
                if (txtServQty.Text.Trim() == "")
                {
                    DT.Rows[R]["HIT_QTY"] = "1";
                }
                {
                    DT.Rows[R]["HIT_QTY"] = txtServQty.Text.Trim();
                }
                DT.Rows[R]["HIT_DISC_AMT"] = 0;
                DT.Rows[R]["HIT_DEDUCT"] = 0;
                DT.Rows[R]["HIT_CO_INS_AMT"] = 0;
                DT.Rows[R]["HIT_DR_CODE"] = txtServDrCode.Text;
                DT.Rows[R]["HIT_DR_NAME"] = txtServDrName.Text;

                DT.Rows[R]["HIT_CLINICIAN_CODE"] = txtServOrdClinCode.Text;
                DT.Rows[R]["HIT_CLINICIAN_NAME"] = txtServOrdClinName.Text;


                Int32 intCurRow = 0;
                intCurRow = gvInvoiceTrans.Rows.Count - 1;

                //  string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "", CodeType = "", strSrvTaxAmount = "0";
                //  decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0, decTaxAmount = 0, ServFeeBeforeTax, decClaimTaxAmount = 0, decCoInsAmt = 0;
                DataSet DSServ = new DataSet();





                DT.Rows[R]["HIT_SERV_TYPE"] = Convert.ToString(ViewState["Serv_type"]); // ServTrnt;
                DT.Rows[R]["HIT_CAT_ID"] = Convert.ToString(ViewState["ServCAT_ID"]);// hidCatID.Value;// ServCat;
                DT.Rows[R]["HIT_COVERED"] = Convert.ToString(ViewState["Serv_Coverd"]);// Covered;// lblCoverd.Text.Trim();
                //  DT.Rows[R]["HIT_REFNO"] = lblgvRefNo.Text.Trim();

                DT.Rows[R]["HIT_COSTPRICE"] = txtServCostPrice.Text.Trim();
                DT.Rows[R]["HIT_SERVCOST"] = txtServCostPrice.Text.Trim();
                //    DT.Rows[R]["HIT_ADJ_DEDUCT_AMOUNT"] = lblgvAdjDedAmt.Text.Trim();

                DT.Rows[R]["HIT_FEE"] = txtServPrice.Text.Trim();

                DT.Rows[R]["HIT_DISC_TYPE"] = drpServDiscType.SelectedValue;



                DT.Rows[R]["HIT_DISC_AMT"] = txtServDiscAmt.Text.Trim();

                if (txtServDedect.Text.Trim() != "")
                {
                    DT.Rows[R]["HIT_DEDUCT"] = Convert.ToDecimal(txtServDedect.Text.Trim());
                }


                DT.Rows[R]["HIT_QTY"] = txtServQty.Text.Trim();
                DT.Rows[R]["HIT_AMOUNT"] = txtServAmount.Text.Trim();
                DT.Rows[R]["HIT_HAAD_CODE"] = txtServHaadCode.Text.Trim();

                if (Convert.ToString(ViewState["ServCAT_type"]) != "")
                {
                    DT.Rows[R]["HIT_TYPE_VALUE"] = Convert.ToString(ViewState["ServCAT_type"]);
                }
                else
                {
                    DT.Rows[R]["HIT_TYPE_VALUE"] = "0";
                }

                DT.Rows[R]["HIT_AUTHORIZATIONID"] = txtServAuthID.Text.Trim();


                DT.Rows[R]["HIT_STARTDATEDesc"] = txtServStartDt.Text.Trim() + " " + txtServStartTime.Text.Trim();     // txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();

                DT.Rows[R]["HIO_TYPE"] = txtServObsType.Text.Trim();

                // DT.Rows[R]["HIO_SERV_CODE"] = ObsCode;
                DT.Rows[R]["HIO_CODE"] = txtServObsCode.Text.Trim();

                DT.Rows[R]["HIO_VALUE"] = txtServObsValue.Text.Trim();
                DT.Rows[R]["HIO_VALUETYPE"] = txtServObsValueType.Text.Trim();



                DT.Rows[R]["HIT_CO_INS_TYPE"] = drpServCoInsPres.SelectedValue;
                DT.Rows[R]["HIT_CO_INS_AMT"] = txtServCoInsAmount.Text.Trim();


                DT.Rows[R]["HIT_TAXPERCENT"] = txtServTaxPresent.Text.Trim();
                DT.Rows[R]["HIT_TAXAMOUNT"] = txtServTaxAmount.Text.Trim();

                DT.Rows[R]["HIT_CLAIMAMOUNTTAX"] = txtServClaimTaxAmount.Text.Trim();


                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;
            }

            BindTempInvoiceTrans();

            AmountCalculation();



        }


        protected void EditAuthActivity_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowServiceAddPopup()", true);

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["InvTransSelectIndex"] = gvScanCard.RowIndex;

                Label lblgvCatID = (Label)gvScanCard.FindControl("lblgvCatID");
                TextBox txtgvServCode = (TextBox)gvScanCard.FindControl("txtgvServCode");
                TextBox txtServDesc = (TextBox)gvScanCard.FindControl("txtServDesc");
                TextBox txtFee = (TextBox)gvScanCard.FindControl("txtFee");

                Label lblDiscType = (Label)gvScanCard.FindControl("lblDiscType");

                DropDownList drpgvDiscType = (DropDownList)gvScanCard.FindControl("drpgvDiscType");

                TextBox txtDiscAmt = (TextBox)gvScanCard.FindControl("txtDiscAmt");
                TextBox txtgvQty = (TextBox)gvScanCard.FindControl("txtgvQty");
                TextBox txtHitAmount = (TextBox)gvScanCard.FindControl("txtHitAmount");
                TextBox txtDeduct = (TextBox)gvScanCard.FindControl("txtDeduct");
                Label lblCoInsType = (Label)gvScanCard.FindControl("lblCoInsType");
                TextBox txtCoInsAmt = (TextBox)gvScanCard.FindControl("txtCoInsAmt");
                TextBox txtgvCoInsTotal = (TextBox)gvScanCard.FindControl("txtgvCoInsTotal");

                TextBox txtgvCompType = (TextBox)gvScanCard.FindControl("txtgvCompType");
                TextBox txtgvCompAmount = (TextBox)gvScanCard.FindControl("txtgvCompAmount");
                TextBox txtgvCompTotal = (TextBox)gvScanCard.FindControl("txtgvCompTotal");

                TextBox txtgvDrCode = (TextBox)gvScanCard.FindControl("txtgvDrCode");
                TextBox txtgvDrName = (TextBox)gvScanCard.FindControl("txtgvDrName");
                Label lblServType = (Label)gvScanCard.FindControl("lblServType");

                Label lblCoverd = (Label)gvScanCard.FindControl("lblCoverd");
                Label lblgvRefNo = (Label)gvScanCard.FindControl("lblgvRefNo");
                Label lblgvTeethNo = (Label)gvScanCard.FindControl("lblgvTeethNo");


                TextBox txtHaadCode = (TextBox)gvScanCard.FindControl("txtHaadCode");
                TextBox txtHaadDesc = (TextBox)gvScanCard.FindControl("txtHaadDesc");

                TextBox txtTypeValue = (TextBox)gvScanCard.FindControl("txtTypeValue");

                TextBox txtgvCostPrice = (TextBox)gvScanCard.FindControl("txtgvCostPrice");


                TextBox txtgvAuthID = (TextBox)gvScanCard.FindControl("txtgvAuthID");

                Label lblgvServCost = (Label)gvScanCard.FindControl("lblgvServCost");
                Label lblgvAdjDedAmt = (Label)gvScanCard.FindControl("lblgvAdjDedAmt");

                TextBox txtgcStartDt = (TextBox)gvScanCard.FindControl("txtgcStartDt");


                TextBox txtObsType = (TextBox)gvScanCard.FindControl("txtObsType");
                TextBox txtObsCode = (TextBox)gvScanCard.FindControl("txtObsCode");
                TextBox txtObsDesc = (TextBox)gvScanCard.FindControl("txtObsDesc");
                TextBox txtObsValue = (TextBox)gvScanCard.FindControl("txtObsValue");
                TextBox txtObsValueType = (TextBox)gvScanCard.FindControl("txtObsValueType");

                TextBox txtTaxAmount = (TextBox)gvScanCard.FindControl("txtTaxAmount");
                TextBox txtClaimTaxAmount = (TextBox)gvScanCard.FindControl("txtClaimTaxAmount");
                TextBox txtTaxPercent = (TextBox)gvScanCard.FindControl("txtTaxPercent");


                Label lblgvTranClaimPaid = (Label)gvScanCard.FindControl("lblgvTranClaimPaid");
                Label lblgvTranClaimRej = (Label)gvScanCard.FindControl("lblgvTranClaimRej");
                Label lblgvTranClaimBal = (Label)gvScanCard.FindControl("lblgvTranClaimBal");
                Label lblgvTranDenialCode = (Label)gvScanCard.FindControl("lblgvTranDenialCode");
                Label lblgvTranDenialDesc = (Label)gvScanCard.FindControl("lblgvTranDenialDesc");


                TextBox txtgvCatID = (TextBox)gvScanCard.FindControl("txtgvCatID");
                TextBox lblgvTranOrderDrCode = (TextBox)gvScanCard.FindControl("lblgvTranOrderDrCode");
                TextBox lblgvTranOrderDrName = (TextBox)gvScanCard.FindControl("lblgvTranOrderDrName");


                txtServCode.Text = txtgvServCode.Text;
                txtServName.Text = txtServDesc.Text;
                txtServPrice.Text = txtFee.Text;
                drpServDiscType.SelectedValue = drpgvDiscType.SelectedValue;
                txtServDiscAmt.Text = txtDiscAmt.Text;
                txtServQty.Text = txtgvQty.Text;
                txtServAmount.Text = txtHitAmount.Text;
                txtServDedect.Text = txtDeduct.Text;
                drpServCoInsPres.Text = lblCoInsType.Text;
                txtServCoInsAmount.Text = txtCoInsAmt.Text;

                txtServTaxAmount.Text = txtTaxAmount.Text;
                txtServTaxPresent.Text = txtTaxPercent.Text;
                txtServClaimTaxAmount.Text = txtClaimTaxAmount.Text;
                txtServHaadCode.Text = txtHaadCode.Text;
                txtServHaadDesc.Text = txtHaadDesc.Text;
                txtServCostPrice.Text = lblgvServCost.Text;
                txtServAuthID.Text = txtgvAuthID.Text;

                txtServDrCode.Text = txtgvDrCode.Text;
                txtServDrName.Text = txtgvDrName.Text;

                string strStartDate = txtgcStartDt.Text;
                string[] arrStartDate = strStartDate.Split(' ');

                if (arrStartDate.Length > 0)
                {
                    txtServStartDt.Text = arrStartDate[0];
                    txtServStartTime.Text = arrStartDate[1];
                }

                txtServObsType.Text = txtObsType.Text;
                txtServObsCode.Text = txtObsCode.Text;
                txtServObsDesc.Text = txtObsDesc.Text;
                txtServObsValue.Text = txtObsValue.Text;
                txtServObsValueType.Text = txtObsValueType.Text;

                txtServOrdClinCode.Text = lblgvTranOrderDrCode.Text;
                txtServOrdClinName.Text = lblgvTranOrderDrName.Text;

                ViewState["Serv_type"] = lblServType.Text;
                ViewState["ServCAT_ID"] = txtgvCatID.Text;
                ViewState["Serv_Coverd"] = lblCoverd.Text;
                ViewState["ServCAT_type"] = txtTypeValue.Text;
                ViewState["ServHAAD_ID"] = txtHaadCode.Text;


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnServiceAdd_Click(object sender, EventArgs e)
        {
            try
            {

                LoadServiceDtlsFromPopup();

                ClearServicePopup();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnServiceAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnServiceAddClose_Click(object sender, EventArgs e)
        {
            try
            {
                LoadServiceDtlsFromPopup();

                ClearServicePopup();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideServiceAddPopup()", true);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnServiceAddClose_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvInvTran_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList drpgvDiscType, drpCoInsType;
                    CheckBox chkSubmit;
                    Label lblDiscType, lblCoInsType;


                    drpgvDiscType = (DropDownList)e.Row.Cells[0].FindControl("drpgvDiscType");
                    drpCoInsType = (DropDownList)e.Row.Cells[0].FindControl("drpCoInsType");


                    chkSubmit = (CheckBox)e.Row.Cells[0].FindControl("chkSubmit");


                    lblDiscType = (Label)e.Row.Cells[0].FindControl("lblDiscType");
                    lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");


                    drpgvDiscType.SelectedValue = lblDiscType.Text;
                    drpCoInsType.SelectedValue = lblCoInsType.Text;

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.gvInvTran_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }


        }

        protected void DeletegvInvTrans_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (Convert.ToString(ViewState["InvoiceTrans"]) != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["InvoiceTrans"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["InvoiceTrans"] = DT;

                    }


                    BindTempInvoiceTrans();

                    AmountCalculation();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void txtgvServCode_TextChanged(object sender, EventArgs e)
        {
            //GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
            //TextBox txtgvServCode = (TextBox)currentRow.FindControl("txtgvServCode");
            //TextBox txtServDesc = (TextBox)currentRow.FindControl("txtServDesc");
            //TextBox txtFee = (TextBox)currentRow.FindControl("txtFee");
            //TextBox txtgvQty = (TextBox)currentRow.FindControl("txtgvQty");
            //TextBox txtHitAmount = (TextBox)currentRow.FindControl("txtHitAmount");

            //Label lblCoverd = (Label)currentRow.FindControl("lblCoverd");
            //Label lblgvCatID = (Label)currentRow.FindControl("lblgvCatID");
            //Label lblServType = (Label)currentRow.FindControl("lblServType");

            //TextBox txtHaadCode = (TextBox)currentRow.FindControl("txtHaadCode");
            //TextBox txtHaadDesc = (TextBox)currentRow.FindControl("txtHaadDesc");


            //TextBox txtTypeValue = (TextBox)currentRow.FindControl("txtTypeValue");

            //TextBox txtObsType = (TextBox)currentRow.FindControl("txtObsType");
            //TextBox txtObsCode = (TextBox)currentRow.FindControl("txtObsCode");
            //TextBox txtObsValue = (TextBox)currentRow.FindControl("txtObsValue");



            //DataSet DSServ = new DataSet();

            //DSServ = GetServiceMasterName("HSM_SERV_ID", txtgvServCode.Text.Trim());

            //string ObsNeeded = "N";
            //if (DSServ.Tables[0].Rows.Count > 0)
            //{
            //    lblgvCatID.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
            //    txtServDesc.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
            //    lblServType.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);

            //    txtFee.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

            //    txtHaadCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
            //    //  txtTypeValue.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);

            //    ObsNeeded = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
            //    if (ObsNeeded == "Y")
            //    {
            //        txtObsCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);
            //        txtObsValue.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);

            //        if (lblServType.Text == "L")
            //        {
            //            txtObsType.Text = "LOINC";
            //        }
            //        else
            //        {
            //            txtObsType.Text = "Universal Dental";
            //        }

            //    }


            //}

            //decimal ServPrice = 0;
            //lblCoverd.Text = "U";

            //if (txtSubCompanyID.Text != "")
            //{
            //    DataSet DSCompAgr = new DataSet();

            //    DSCompAgr = GetCompanyAgrDtls(txtgvServCode.Text.Trim(), txtSubCompanyID.Text.Trim());

            //    if (DS.Tables[0].Rows.Count > 0)
            //    {
            //        if (DS.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
            //        {
            //            ServPrice = Convert.ToDecimal(DS.Tables[0].Rows[0]["HAS_NETAMOUNT"]);
            //            lblCoverd.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAS_COVERING_TYPE"]);


            //            txtFee.Text = Convert.ToString(ServPrice);

            //        }

            //    }

            //}


            //string strHaadName = "", strHaadType = "";
            //if (txtHaadCode.Text.Trim() != "")
            //{
            //    DataSet DSHaad = new DataSet();
            //    DSHaad = GetHaadName(txtHaadCode.Text.Trim());

            //    if (DSHaad.Tables[0].Rows.Count > 0)
            //    {
            //        strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
            //        strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
            //    }
            //    txtHaadDesc.Text = strHaadName;
            //    txtTypeValue.Text = strHaadType;

            //}

            //txtgvQty.Text = "1";

            //txtHitAmount.Text = Convert.ToString(Convert.ToDecimal(txtFee.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));

            //AmountCalculation();

        }


        void GridRowRefresh(Int32 intCurRow)
        {


            Label lblServType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblServType");


            DropDownList drpgvDiscType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpgvDiscType");

            TextBox txtDiscAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDiscAmt");

            TextBox txtFee = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtFee");
            TextBox txtgvQty = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvQty");
            TextBox txtHitAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHitAmount");
            TextBox txtDeduct = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDeduct");

            DropDownList drpCoInsType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpCoInsType");
            TextBox txtCoInsAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtCoInsAmt");
            TextBox txtgcStartDt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgcStartDt");


            TextBox txtgvCompType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompType");
            TextBox txtgvCompAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompAmount");
            TextBox txtgvCompTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompTotal");
            TextBox txtTaxPercent = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTaxPercent");


            txtHitAmount.Text = Convert.ToString(Convert.ToDecimal(txtFee.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["InvoiceTrans"];

            string ServTrnt = "", strDeductType = "$";
            ServTrnt = lblServType.Text.Trim();

            decimal decDiscount, decDedAmt = 0, decConInsAmt = 0, ServFee = 0, decTaxAmount = 0, ServFeeBeforeTax = 0, decCoInsAmtBeforeTax = 0, decClaimTaxAmount = 0;

            DT.Rows[intCurRow]["HIT_STARTDATEDesc"] = txtgcStartDt.Text;

            if (drpgvDiscType.SelectedIndex == 0)
            {
                decDiscount = Convert.ToDecimal(txtDiscAmt.Text.Trim());

            }
            else
            {

                decDiscount = ((Convert.ToDecimal(txtFee.Text.Trim()) / 100) * Convert.ToDecimal(txtDiscAmt.Text.Trim()));
            }
            DT.Rows[intCurRow]["HIT_FEE"] = txtFee.Text.Trim();
            DT.Rows[intCurRow]["HIT_DISC_TYPE"] = drpgvDiscType.SelectedValue;
            DT.Rows[intCurRow]["HIT_DISC_AMT"] = txtDiscAmt.Text.Trim();

            DT.Rows[intCurRow]["HIT_QTY"] = txtgvQty.Text.Trim();
            DT.Rows[intCurRow]["HIT_AMOUNT"] = Convert.ToDecimal(txtHitAmount.Text) - (Convert.ToDecimal(txtgvQty.Text.Trim()) * decDiscount);




            ServFee = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_AMOUNT"]);
            ServFeeBeforeTax = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_AMOUNT"]);

            if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
            {
                decTaxAmount = ((ServFee) / 100) * Convert.ToDecimal(txtTaxPercent.Text.Trim());
                ServFee = ServFee + decTaxAmount;
                DT.Rows[intCurRow]["HIT_AMOUNT"] = ServFee.ToString("#0.00"); ;
            }





            if (drpInvType.SelectedValue == "Cash")
            {
                DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "%";
                DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = "100";
            }
            DT.Rows[intCurRow]["HIT_COMP_TYPE"] = txtgvCompType.Text.Trim();
            DT.Rows[intCurRow]["HIT_COMP_AMT"] = txtgvCompAmount.Text.Trim();
            DT.Rows[intCurRow]["HIT_COMP_TOTAL"] = txtgvCompTotal.Text.Trim();

            //decDedAmt = Convert.ToDecimal(txtDeduct.Text.Trim());
            //decConInsAmt = Convert.ToDecimal(txtCoInsAmt.Text.Trim());
            if (Convert.ToDecimal(DT.Rows[intCurRow]["HIT_DEDUCT"]) == Convert.ToDecimal(txtDeduct.Text))
            {
                strDeductType = Convert.ToString(ViewState["Deductible_Type"]);
                if (txtSubCompanyID.Text != "")
                {
                    decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
                }
            }
            else
            {
                decDedAmt = Convert.ToDecimal(txtDeduct.Text);
            }

            if (Convert.ToDecimal(DT.Rows[intCurRow]["HIT_CO_INS_AMT"]) == Convert.ToDecimal(txtCoInsAmt.Text))
            {
                if (ServTrnt == "D" && Convert.ToString(ViewState["Co_Ins_AmtDental"]) != "")
                {
                    decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtDental"]);

                }
                else if (ServTrnt == "P" && Convert.ToString(ViewState["Co_Ins_AmtPharmacy"]) != "")
                {
                    decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtPharmacy"]);

                }
                else if (ServTrnt == "L" && Convert.ToString(ViewState["Co_Ins_AmtLab"]) != "")
                {
                    decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtLab"]);

                }
                else if (ServTrnt == "R" && Convert.ToString(ViewState["Co_Ins_AmtRad"]) != "")
                {
                    decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_AmtRad"]);

                }
                else
                {
                    if (Convert.ToString(ViewState["Co_Ins_Amt"]) != "")
                    {

                        decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
                    }
                }
            }
            else
            {
                decConInsAmt = Convert.ToDecimal(txtCoInsAmt.Text);
            }

            ServFee = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_AMOUNT"]);// Convert.ToDecimal(txtFee.Text.Trim());


            if (ServTrnt == "C")
            {
                if (strDeductType == "$")
                {
                    DT.Rows[intCurRow]["HIT_DEDUCT"] = decDedAmt.ToString("#0.00"); ;

                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "$";
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decDedAmt.ToString("#0.00");

                        decCoInsAmtBeforeTax = decDedAmt;
                    }
                }
                else
                {
                    DT.Rows[intCurRow]["HIT_DEDUCT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "%";
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                        decCoInsAmtBeforeTax = Convert.ToDecimal((ServFeeBeforeTax * decDedAmt) / 100);// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }



                }


            }
            else
            {
                txtDeduct.ReadOnly = true;
                DT.Rows[intCurRow]["HIT_DEDUCT"] = "0.00"; //Convert.ToDecimal(txtDeduct.Text.Trim());// 
                if (txtSubCompanyID.Text != "")
                {
                    DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = drpCoInsType.SelectedValue;// Convert.ToString(ViewState["Co_Ins_Type"]);
                }


                if (drpCoInsType.SelectedValue == "$")
                {
                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00");

                        decCoInsAmtBeforeTax = decConInsAmt;
                    }
                }
                else
                {
                    if (txtSubCompanyID.Text != "")
                    {
                        ////DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                        ////decCoInsAmtBeforeTax = decConInsAmt;

                        DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "$";
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decConInsAmt;// Convert.ToString(ViewState["Co_Ins_Amt"]);

                        decCoInsAmtBeforeTax = Convert.ToDecimal((ServFeeBeforeTax * decConInsAmt) / 100);// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }
                }
            }

            //DT.Rows[intCurRow]["HIT_DEDUCT"] = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_DEDUCT"]) * Convert.ToDecimal(DT.Rows[intCurRow]["HIT_QTY"]);

            //if (Convert.ToString(DT.Rows[intCurRow]["HIT_CO_INS_TYPE"]) == "$")
            //{
            //    DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_CO_INS_AMT"]) * Convert.ToDecimal(DT.Rows[intCurRow]["HIT_QTY"]);

            //}
            DT.Rows[intCurRow]["HIT_TAXAMOUNT"] = decTaxAmount.ToString("#0.00");

            if (Convert.ToString(ViewState["SALESTAX"]).ToUpper() == "YES" && CheckTaxApplicableYear() == true)
            {
                decClaimTaxAmount = ((ServFeeBeforeTax - decCoInsAmtBeforeTax) / 100) * Convert.ToDecimal(txtTaxPercent.Text); // decSrvTaxAmount;
            }

            DT.Rows[intCurRow]["HIT_CLAIMAMOUNTTAX"] = decClaimTaxAmount.ToString("#0.00");

            DT.AcceptChanges();
            ViewState["InvoiceTrans"] = DT;
            BindTempInvoiceTrans();
            AmountCalculation();

        }

        protected void gvInvoiceTrans_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.gvInvoiceTrans_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpgvDiscType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.drpgvDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void SelectVisit_Click(object sender, EventArgs e)
        {
            try
            {
                txtEMRID.Text = "";

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;


                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;



                Label lblVisitID = (Label)gvScanCard.Cells[0].FindControl("lblVisitID");
                Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
                Label lblDRName = (Label)gvScanCard.Cells[0].FindControl("lblDRName");
                Label lblgvhpvRefDrId = (Label)gvScanCard.Cells[0].FindControl("lblgvhpvRefDrId");
                Label lblVisitEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblVisitEMR_ID");
                Label lblEligibilityID = (Label)gvScanCard.Cells[0].FindControl("lblEligibilityID");
                Label lblgvVisitVisitType = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitVisitType");

                Label lblgvVisitPTType = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitPTType");

                txtDoctorID.Text = lblDrID.Text;
                txtDoctorName.Text = lblDRName.Text;
                txtEMRID.Text = lblVisitEMR_ID.Text;
                txtEligibilityID.Text = lblEligibilityID.Text;
                lblVisitType.Text = lblgvVisitVisitType.Text;


                hidInvoiceType.Value = lblgvVisitPTType.Text;

                string vInvType = hidInvoiceType.Value;
                string vdrpInvType = drpInvType.SelectedValue;


                divInvTypeMessage.Style.Add("display", "none");

                if (vInvType != vdrpInvType)
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowInvTypeMessage()", true);

                    divInvTypeMessage.Style.Add("display", "block");
                    lblInvTypeMessage.Text = "This patient is registered as " + vInvType + ". Do you want Invoice this Patient as " + vdrpInvType + " ?";

                    strSubCompanyID = "";
                }




                if (lblgvhpvRefDrId.Text != "")
                {

                    //  txtRefDrId.Text = lblgvhpvRefDrId.Text;
                    txtOrdClinCode.Text = lblgvhpvRefDrId.Text;
                    //txtRefDrId_TextChanged(txtRefDrId, new EventArgs());

                    string strDrCode = "", strDrName = "";
                    strDrCode = lblgvhpvRefDrId.Text;
                    GetRefDrName(strDrCode, out strDrName);
                    txtRefDrName.Text = lblgvhpvRefDrId.Text.Trim() + "~" + strDrName;



                    txtOrdClinCode_TextChanged(txtOrdClinCode, new EventArgs());
                }
                else
                {

                    txtOrdClinCode.Text = lblDrID.Text;
                    txtOrdClinName.Text = lblDRName.Text;


                }



                hidVisitID.Value = lblVisitID.Text;

                ModPopExtVisits.Hide();

                if (txtEMRID.Text.Trim() != "")
                {
                    BindEMRDtls();

                }


                if (IsPatientBilledSameDaySameDr(false) == false)
                {
                    BuildeInvoiceTrans();
                    BuildeInvoiceClaims();

                    BindSalesOrderServ();
                    BindSalesOrderDiag();
                }


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.SelectVisit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpCoInsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpCoInsType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtgvDrCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 intCurRow = currentRow.RowIndex;

                TextBox txtgvDrCode = (TextBox)currentRow.FindControl("txtgvDrCode");
                TextBox txtgvDrName = (TextBox)currentRow.FindControl("txtgvDrName");
                string strDrCode = "", strDrName = "", strDeptName = "";
                strDrCode = txtgvDrCode.Text.Trim();

                GetStaffName(strDrCode, out strDrName, out strDeptName);
                txtgvDrName.Text = strDrName;

                GetJobnumber();

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["InvoiceTrans"];
                DT.Rows[intCurRow]["HIT_DR_CODE"] = txtgvDrCode.Text;
                DT.Rows[intCurRow]["HIT_DR_NAME"] = txtgvDrName.Text;

                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;
                BindTempInvoiceTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtgvDrCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtObsCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 intCurRow = currentRow.RowIndex;

                TextBox txtgvCompType = (TextBox)currentRow.FindControl("txtgvCompType");
                TextBox txtgvCompAmount = (TextBox)currentRow.FindControl("txtgvCompAmount");
                TextBox txtgvCompTotal = (TextBox)currentRow.FindControl("txtgvCompTotal");

                TextBox txtObsCode = (TextBox)currentRow.FindControl("txtObsCode");
                TextBox txtObsDesc = (TextBox)currentRow.FindControl("txtObsDesc");
                TextBox txtObsValue = (TextBox)currentRow.FindControl("txtObsValue");
                TextBox txtObsValueType = (TextBox)currentRow.FindControl("txtObsValueType");


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["InvoiceTrans"];
                DT.Rows[intCurRow]["HIO_CODE"] = txtObsCode.Text;
                DT.Rows[intCurRow]["HIO_DESCRIPTION"] = txtObsDesc.Text;
                DT.Rows[intCurRow]["HIO_VALUE"] = txtObsValue.Text;
                DT.Rows[intCurRow]["HIO_VALUETYPE"] = txtObsValueType.Text;

                DT.Rows[intCurRow]["HIT_COMP_TYPE"] = txtgvCompType.Text.Trim();
                DT.Rows[intCurRow]["HIT_COMP_AMT"] = txtgvCompAmount.Text.Trim();
                DT.Rows[intCurRow]["HIT_COMP_TOTAL"] = txtgvCompTotal.Text.Trim();



                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;
                BindTempInvoiceTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtObsCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void txtHospDisc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //decimal decBill2 = 0;



                //if (txtBillAmt1.Text.Trim() != "" && txtHospDisc.Text.Trim() != "")
                //{
                //    decimal decHosDisc = Convert.ToDecimal(txtHospDisc.Text.Trim());

                //    txtHospDisc.Text = decHosDisc.ToString("#0.00");

                //    if (drpHospDiscType.SelectedIndex == 0)
                //    {
                //        decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - Convert.ToDecimal(txtHospDisc.Text.Trim());

                //    }
                //    else
                //    {

                //        decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - ((Convert.ToDecimal(txtBillAmt1.Text.Trim()) / 100) * Convert.ToDecimal(txtHospDisc.Text.Trim()));
                //    }

                //    txtBillAmt2.Text = decBill2.ToString("#0.00");

                //    decimal decBill3 = 0;
                //    if (txtBillAmt2.Text.Trim() != "" && txtDeductible.Text.Trim() != "")
                //    {
                //        decBill3 = Convert.ToDecimal(txtBillAmt2.Text.Trim()) - Convert.ToDecimal(txtDeductible.Text.Trim());
                //    }
                //    txtBillAmt3.Text = decBill3.ToString("#0.00");

                //}

                AmountCalculation();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtHospDisc_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtSplDisc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //decimal decPaidAmt = Convert.ToDecimal(txtCoInsTotal.Text.Trim());

                //if (txtSplDisc.Text.Trim() != "")
                //{
                //    decPaidAmt = decPaidAmt - Convert.ToDecimal(txtSplDisc.Text.Trim());
                //}

                //if (txtPTCredit.Text.Trim() != "")
                //{
                //    decPaidAmt = decPaidAmt - Convert.ToDecimal(txtPTCredit.Text.Trim());
                //}

                //txtPaidAmount.Text = decPaidAmt.ToString("#0.00");

                AmountCalculation();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtSplDisc_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        ////protected void TxtRcvdAmt_TextChanged(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        decimal decPTCredit = 0, decRcvdAmt = 0;

        ////        if (txtPTCredit.Text.Trim() != "")
        ////            decPTCredit = Convert.ToDecimal(txtPTCredit.Text.Trim());

        ////        if (TxtRcvdAmt.Text.Trim() != "")
        ////            decRcvdAmt = Convert.ToDecimal(TxtRcvdAmt.Text.Trim());

        ////        decPTCredit = decPTCredit - decRcvdAmt;

        ////        txtPTCredit.Text = decPTCredit.ToString("#0.00");


        ////        //  AmountCalculation();
        ////    }
        ////    catch (Exception ex)
        ////    {

        ////        TextFileWriting("-----------------------------------------------");
        ////        TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.TxtRcvdAmt_TextChanged");
        ////        TextFileWriting(ex.Message.ToString());
        ////    }

        ////}

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                Clear();

                New();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(5000);

                Boolean isError = false;
                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    if (IsPatientBilledSameDaySameDr(true) == true)
                    {
                        goto FunEnd;
                    }
                }

                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                    {
                        if (CheckDuplicateInvoice() == true)
                        {
                            goto FunEnd1;
                        }
                    }

                }


                if (txtEMRID.Text.Trim() != "" && txtEMRID.Text.Trim() != "0")
                {
                    if (CheckEMR_ID() == false)
                    {

                        goto FunEnd;
                    }
                }



                isError = fnSave();

            FunEnd1: ;

                if (isError == false)
                {

                    if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the Patient Invoice, Invoice ID is " + txtInvoiceNo.Text);
                    }
                    else
                    {
                        AuditLogAdd("ADD", "Add new entry in the Patient Invoice, Invoice ID is " + txtInvoiceNo.Text);
                    }

                    //SAVE APPOINTMENT STATUS AS PATIENT BILLED
                    UpdateAppointmentStatus();

                    if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                    {
                        if (drpSMSTemplate.SelectedIndex != 0)
                        {
                            SendSMS();

                            if (drpSMSTemplate.SelectedValue == "General Template")
                            {
                                SendSMS_Wishes();
                            }
                        }
                    }

                    Clear();
                    New();

                    MPExtMessage.Show();
                    strMessage = "Details Saved !";

                }
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnSave");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                fnPrint(txtInvoiceNo.Text.Trim());


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnSave");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    goto FunEnd;
                }
                CommonBAL objCom = new CommonBAL();
                if ((GlobalValues.FileDescription == "ANP" || GlobalValues.FileDescription == "ALNOORSATWA") && Convert.ToString(Session["User_ID"]).ToUpper() != "ADMIN" && drpInvType.SelectedValue == "Cash")
                {
                    string dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                    DateTime Today;

                    Today = Convert.ToDateTime(objCom.fnGetDate("dd/MM/yyyy"));


                    Int32 intDatediff = 0;
                    intDatediff = objCom.fnGetDateDiff(Convert.ToString(Today), dtInvDate);


                    if (intDatediff > 0)
                    {
                        lblStatus.Text = "You cannot delete Cash Invoice";
                        goto FunEnd;

                    }
                }

                if (GlobalValues.FileDescription == "ALREEM" && Convert.ToBoolean("NewFlag") == false)
                {
                    if (txtRemarks.Text.Trim() == "")
                    {
                        lblStatus.Text = "Remarks field should be non-empty for deleting Invoice. ";
                        goto FunEnd;
                    }

                }
                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {

                    if (CheckAccumedXMLDataMaster() == true)
                    {
                        lblStatus.Text = "Accumed Transfer Done, So you can not Edit it";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;

                    }
                }
                clsInvoice objInv = new clsInvoice();

                DataSet DS = new DataSet();

                string Criteria = "1=1";
                Criteria += " AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DS = objCom.fnGetFieldValue("TOP 1 HIM_INVOICE_ID,HIM_COMMIT_FLAG  ", "HMS_INVOICE_MASTER", Criteria, "");
                //DS = objInv.InvoiceMasterGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMIT_FLAG"]).ToUpper() == "Y" && Convert.ToString(Session["User_ID"]).ToUpper() != "ADMIN")
                    {
                        lblStatus.Text = "This Invoice is Commited. So you cannot delete this invoice.";
                        goto FunEnd;
                    }

                    objInv.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim();
                    objInv.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objInv.HIM_RECEIPT_NO = hidReceipt.Value;
                    objInv.InvoiceMasterDelete();

                }
                Clear();
                New();
                lblStatus.Text = "Invoice Deleted.";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            Clear();

            New();
        }

        protected void btnPrntDeductible_Click(object sender, EventArgs e)
        {
            try
            {
                fnPrint(txtInvoiceNo.Text.Trim());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnPrntDeductible_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }




        protected void drpPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divCC.Visible = false;
                divCheque.Visible = false;
                divAdvane.Visible = false;
                if (drpPayType.SelectedItem.Text == "Multi Currency")
                {

                }
                else if (drpPayType.SelectedItem.Text == "Credit Card")
                {
                    txtCashAmt.Text = "0.00";
                    divCC.Visible = true;
                    BIndCCType();
                    if (txtCCAmt.Text.Trim() == "0" || txtCCAmt.Text.Trim() == "0.00" || txtCCAmt.Text.Trim() == "0.000")
                    {

                        txtCCAmt.Text = txtPaidAmount.Text;

                    }
                }
                else if (drpPayType.SelectedItem.Text == "Cheque" || drpPayType.SelectedItem.Text == "PDC")
                {
                    txtCashAmt.Text = "0.00";
                    divCheque.Visible = true;
                    if (txtChqAmt.Text.Trim() == "0" || txtChqAmt.Text.Trim() == "0.00" || txtChqAmt.Text.Trim() == "0.000")
                    {

                        txtChqAmt.Text = txtPaidAmount.Text;

                    }

                }
                else if (drpPayType.SelectedItem.Text == "Debit Card")
                {
                    txtCashAmt.Text = "0.00";
                    drpccType.Items.Clear();
                    divCC.Visible = true;


                    drpccType.Items.Insert(0, "Salary Card");
                    drpccType.Items[0].Value = "Salary Card";

                    drpccType.Items.Insert(1, "Knet");
                    drpccType.Items[1].Value = "Knet";

                    if (txtCCAmt.Text.Trim() == "0" || txtCCAmt.Text.Trim() == "0.00" || txtCCAmt.Text.Trim() == "0.000")
                    {
                        txtCCAmt.Text = txtPaidAmount.Text;

                    }
                }
                else if (drpPayType.SelectedItem.Text == "Advance")
                {
                    divAdvane.Visible = true;
                    fnListAdvanceDtls();
                }
                else if (drpPayType.SelectedItem.Text.ToUpper() == "UTILISE ADVANCE")
                {

                }
                else
                {
                    txtCCAmt.Text = "0.00";
                    txtChqAmt.Text = "0.00";
                    decimal decPaidAmt = 0, decccAmt = 0, decChqAmt = 0, decCashAmt = 0;

                    if (txtPaidAmount.Text.Trim() != "")
                    {
                        decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text.Trim());
                    }

                    //if (txtCCAmt.Text.Trim() != "")
                    //{
                    //    decccAmt = Convert.ToDecimal(txtCCAmt.Text.Trim());
                    //}

                    //if (txtChqAmt.Text.Trim() != "")
                    //{
                    //    decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
                    //}

                    decCashAmt = decPaidAmt - decccAmt - decChqAmt;

                    txtCashAmt.Text = decCashAmt.ToString("#0.00");
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpPayType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtConvRate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (drpPayType.SelectedValue == "Multi Currency")
                {
                    txtcurValue.Text = Convert.ToString(Convert.ToDecimal(txtRcvdAmount.Text) * Convert.ToDecimal(txtConvRate.Text));

                    if (Convert.ToDecimal(txtRcvdAmount.Text) > 0)
                    {
                        LblMBalAmt.Text = Convert.ToString(Convert.ToDecimal(txtcurValue.Text) - Convert.ToDecimal(txtPaidAmount.Text));
                        txtCashAmt.Text = Convert.ToString(Convert.ToDecimal(txtPaidAmount.Text) - Convert.ToDecimal(txtcurValue.Text) + Convert.ToDecimal(LblMBalAmt.Text));
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtConvRate_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtCCAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal decPaidAmt = 0, decCCAmt = 0, decChqAmt = 0, decCashAmt = 0;

                if (txtPaidAmount.Text.Trim() != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text.Trim());
                }



                if (txtCCAmt.Text.Trim() != "")
                {
                    decCCAmt = Convert.ToDecimal(txtCCAmt.Text.Trim());
                }



                if (txtChqAmt.Text.Trim() != "")
                {
                    decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
                }

                decCashAmt = decPaidAmt - decCCAmt - decChqAmt;

                txtCashAmt.Text = decCashAmt.ToString("#0.00");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtCCAmt_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void txtChqAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal decPaidAmt = 0, decChqAmt = 0, decCashAmt = 0;

                if (txtPaidAmount.Text.Trim() != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text.Trim());
                }

                if (txtChqAmt.Text.Trim() != "")
                {
                    decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
                }

                decCashAmt = decPaidAmt - decChqAmt;

                txtCashAmt.Text = decCashAmt.ToString("#0.00");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtChqAmt_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }



        protected void DeletegvDiag_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (Convert.ToString(ViewState["InvoiceClaims"]) != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["InvoiceClaims"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["InvoiceClaims"] = DT;

                    }


                    BindTempInvoiceClaims();


                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.DeletegvDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnImportSO_Click(object sender, EventArgs e)
        {
            try
            {
                BuildeInvoiceTrans();
                BuildeInvoiceClaims();

                BindSalesOrderServ();
                BindSalesOrderDiag();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnImportSO_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void SelectAttachment_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvGCGridView;
            gvGCGridView = (GridViewRow)btnEdit.Parent.Parent;
            Label lblgvAttaEMRID = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaEMRID");
            Label lblgvAttaCategory = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaCategory");
            Label lblgvAttaFileName = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaFileName");
            Label lblgvAttaPTID = (Label)gvGCGridView.Cells[0].FindControl("lblgvAttaPTID");


            //   ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + txtFileNo.Text.Trim() + "','" + lblScnFileName.Text + "','" + txtInvoiceNo.Text.Trim() + "');", true);

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + lblgvAttaPTID.Text + "','" + lblgvAttaFileName.Text + "','" + txtInvoiceNo.Text.Trim() + "');", true);



        }

        protected void btnInvTypeYes_Click(object sender, EventArgs e)
        {
            try
            {
                //if ( drpInvType.SelectedValue  == "Cash")
                //{
                //    drpInvType.SelectedValue = "Credit";
                //}

                //if (drpInvType.SelectedValue == "Credit")
                //{
                //    drpInvType.SelectedValue = "Cash";
                //}


                if (drpInvType.SelectedValue == "Cash")
                {
                    ChkTopupCard.Checked = false;
                    TxtTopupAmt.Text = "";
                    strSubCompanyID = "";
                    lblInsCard.Text = "";
                    txtPTComp.Text = "";
                    txtSubCompanyID.Text = "";
                    txtSubCompanyName.Text = "";
                    hidCompanyID.Value = "";
                    txtCompanyName.Text = "";
                    txtPlanType.Text = "";
                    txtPolicyNo.Text = "";
                    txtPolicyExp.Text = "";
                    TxtVoucherNo.Text = "";

                    txtPayerID.Text = "Selfpay";
                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                    {
                        txtReciverID.Text = "@DHA";

                    }
                    else
                    {
                        txtReciverID.Text = "@HAAD";
                    }
                }
                else
                {

                    PatientDataBind();

                    string strPayerID = "", strReceiverID = "";
                    GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

                    txtPayerID.Text = strPayerID;
                    txtReciverID.Text = strReceiverID;


                }



                divInvTypeMessage.Style.Add("display", "none");

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnInvTypeNo_Click(object sender, EventArgs e)
        {
            try
            {


                //if (drpInvType.SelectedValue == "Cash")
                //{
                //    drpInvType.SelectedValue = "Credit";
                //}
                //else
                //{
                //    drpInvType.SelectedValue = "Cash";
                //}
                drpInvType.SelectedValue = hidInvoiceType.Value;

                if (drpInvType.SelectedValue == "Cash")
                {
                    ChkTopupCard.Checked = false;
                    TxtTopupAmt.Text = "";
                    strSubCompanyID = "";
                    lblInsCard.Text = "";
                    txtPTComp.Text = "";
                    txtSubCompanyID.Text = "";
                    txtSubCompanyName.Text = "";
                    hidCompanyID.Value = "";
                    txtCompanyName.Text = "";
                    txtPlanType.Text = "";
                    txtPolicyNo.Text = "";
                    txtPolicyExp.Text = "";
                    TxtVoucherNo.Text = "";

                    txtPayerID.Text = "Selfpay";
                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                    {
                        txtReciverID.Text = "@DHA";

                    }
                    else
                    {
                        txtReciverID.Text = "@HAAD";
                    }
                }
                else
                {
                    PatientDataBind();

                    string strPayerID = "", strReceiverID = "";
                    GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

                    txtPayerID.Text = strPayerID;
                    txtReciverID.Text = strReceiverID;
                }



                divInvTypeMessage.Style.Add("display", "none");

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpInvType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    string strPayerID = "", strReceiverID = "";
                    GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

                    txtPayerID.Text = strPayerID;
                    txtReciverID.Text = strReceiverID;
                }
                else
                {
                    strSubCompanyID = "";

                    txtPayerID.Text = "Selfpay";
                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                    {
                        txtReciverID.Text = "@DHA";

                    }
                    else
                    {
                        txtReciverID.Text = "@HAAD";
                    }
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpInvType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnServiceClear_Click(object sender, EventArgs e)
        {
            ClearServicePopup();
        }

        protected void drpServDiscType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["DiscTypeChanged"] = "Yes";
                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpServDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void txtServPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["FeeChanged"] = "Yes";
                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServPrice_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtServQty_TextChanged(object sender, EventArgs e)
        {

            LoadServiceDtls(txtServCode.Text.Trim());
        }
        protected void txtServDiscAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["DiscAmtChanged"] = "Yes";
                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServDiscAmt_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpServCoInsPres_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["CoInsPresChanged"] = "Yes";
                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpServDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtServCoInsAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["CoInsAmtChanged"] = "Yes";
                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpServDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtServDedect_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["DectAmtChanged"] = "Yes";
                LoadServiceDtls(txtServCode.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpServDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }




        protected void btnServUpdateToEMR_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtInvoiceNo.Text.Trim() == "")
                {
                    lblStatus.Text = "invoice ID is Empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtEMRID.Text.Trim() == "")
                {
                    lblStatus.Text = "EMR ID is Empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtChiefComplaints.Text.Trim() == "")
                {
                    lblStatus.Text = "Chief Complaint is Empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtEMRID.Text.Trim() != "" && txtInvoiceNo.Text.Trim() != "")
                {
                    CommonBAL objCom = new CommonBAL();
                    objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objCom.EMRID = txtEMRID.Text.Trim();
                    objCom.INVOICE_ID = txtInvoiceNo.Text.Trim();

                    objCom.DiagnosisAddFromInvoice();
                    objCom.VisitDetailsAddFromInvoice();


                    string FieldNameWithValues = " EPM_CC='" + txtChiefComplaints.Text.Trim() + "' ";
                    string Criteria = "  EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);




                    lblStatus.Text = "Invoice Details Updated into EMR";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpServDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void btnAttachAdd_Click(object sender, EventArgs e)
        {

            try
            {

                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
                string strPath = GlobalValues.AuthorizationAttachmentPath;

                Int32 intLength;
                intLength = filAttachment.PostedFile.ContentLength;
                String ext = System.IO.Path.GetExtension(filAttachment.FileName);
                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                // lblFileName.Text = strName + ext;
                lblFileNameActual.Text = filAttachment.FileName;
                ViewState["AttachmentData"] = Convert.ToBase64String(filAttachment.FileBytes);
                DeleteAttach.Visible = true;

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = " HIM_ATTACHMENT='" + Convert.ToString(ViewState["AttachmentData"]) + "' ,   HIM_ATTACHMENT_NAME ='" + lblFileNameActual.Text + "'";
                string Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void DeleteAttach_Click(object sender, EventArgs e)
        {

            try
            {
                // lblFileName.Text = "";
                lblFileNameActual.Text = "";
                ViewState["AttachmentData"] = "";
                DeleteAttach.Visible = false;

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = " HIM_ATTACHMENT='' ,   HIM_ATTACHMENT_NAME ='' ";
                string Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.DeleteAttach_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AttachgvInvTrans_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTransAttchmentPopup()", true);

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblgvCatID = (Label)gvScanCard.FindControl("lblgvCatID");
                TextBox txtgvServCode = (TextBox)gvScanCard.FindControl("txtgvServCode");
                TextBox lblAttachmentFileName = (TextBox)gvScanCard.FindControl("lblAttachmentFileName");

                ViewState["InvTransAttachSelectIndex"] = gvScanCard.RowIndex;

                txtTransAttachServCode.Text = txtgvServCode.Text;
                lblTransFileNameActual.Text = lblAttachmentFileName.Text;
                if (lblAttachmentFileName.Text != "")
                {
                    DeleteTransAttach.Visible = true;
                }






            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }



        protected void btnTransAttachAdd_Click(object sender, EventArgs e)
        {

            try
            {
                Int32 intLength;
                intLength = TransFilAttachment.PostedFile.ContentLength;
                String ext = System.IO.Path.GetExtension(TransFilAttachment.FileName);
                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                // lblFileName.Text = strName + ext;
                lblTransFileNameActual.Text = TransFilAttachment.FileName;
                string AttachmentData = "";
                AttachmentData = Convert.ToBase64String(TransFilAttachment.FileBytes);
                DeleteTransAttach.Visible = true;

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = " HIT_ATTACHMENT='" + AttachmentData + "' ,   HIT_ATTACHMENT_NAME ='" + lblTransFileNameActual.Text + "'";
                string Criteria = " HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIT_SERV_CODE='" + txtTransAttachServCode.Text + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria);



                ////Int32 R = Convert.ToInt32(ViewState["InvTransAttachSelectIndex"]);
                ////DataTable DT = new DataTable();

                ////DT = (DataTable)ViewState["InvoiceTrans"];

                ////if (AttachmentData != "")
                ////{
                ////    DT.Rows[R]["HIT_ATTACHMENT"] = AttachmentData;
                ////}
                ////else
                ////{
                ////    DT.Rows[R]["HIT_ATTACHMENT"] = "";
                ////}


                ////if (lblTransFileNameActual.Text != "")
                ////{
                ////    DT.Rows[R]["HIT_ATTACHMENT_NAME"] = lblTransFileNameActual.Text;
                ////}
                ////else
                ////{
                ////    DT.Rows[R]["HIT_ATTACHMENT_NAME"] = "";
                ////}


                ////DT.AcceptChanges();
                ////ViewState["InvoiceTrans"] = DT;


                txtTransAttachServCode.Text = "";
                ///  ViewState["InvTransAttachSelectIndex"] = "";


                BindInvoiceTrans();
                BindTempInvoiceTrans();
            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnTransAttachAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void DeleteTransAttach_Click(object sender, EventArgs e)
        {

            try
            {
                // lblFileName.Text = "";
                btnTransAttachAdd.Text = "";
                // ViewState["AttachmentData"] = "";
                DeleteTransAttach.Visible = false;

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = " HIT_ATTACHMENT='' ,   HIT_ATTACHMENT_NAME =''";
                string Criteria = " HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'  AND HIT_SERV_CODE='" + txtTransAttachServCode.Text + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria);



                ////Int32 R = Convert.ToInt32(ViewState["InvTransAttachSelectIndex"]);
                ////DataTable DT = new DataTable();

                ////DT = (DataTable)ViewState["InvoiceTrans"];
                ////DT.Rows[R]["HIT_ATTACHMENT"] = "";
                ////DT.Rows[R]["HIT_ATTACHMENT_NAME"] = "";

                ////DT.AcceptChanges();
                ////ViewState["InvoiceTrans"] = DT;

                BindInvoiceTrans();
                BindTempInvoiceTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.DeleteTransAttach_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        Boolean SendEmail()
        {
            try
            {
                if (lblEmailID.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Email ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                CommonBAL objCom = new CommonBAL();

                MailContent = "";

                MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' style='width:100%;'>";
                MailContent = MailContent + "<TR><TD  align='center'>&nbsp;</TD></TR>";
                MailContent = MailContent + "<TR><TD  align='left'>Dear  <b>" + txtName.Text + ",</b></TD></TR>";
                MailContent = MailContent + "<TR><TD  align='center'>&nbsp;</TD></TR>";
                MailContent = MailContent + "<TR><TD  align='Left'>Invoice Details</TD></TR>";
                MailContent = MailContent + "<TR><TD valign='top' align='left'><B>Bill No. : </B></TD> <TD>" + txtInvoiceNo.Text.Trim() + "</TD><TD valign='top' align='left'><B>Bill Date : </B></TD> <TD>" + txtInvDate.Text.Trim() + "</TD></TR>";
                MailContent = MailContent + "<TR><TD valign='top' align='left'><B>Name. : </B></TD> <TD>" + txtName.Text.Trim() + "</TD><TD valign='top' align='left'><B>File No. : </B></TD> <TD>" + txtFileNo.Text.Trim() + "</TD></TR>";
                MailContent = MailContent + "<TR><TD valign='top' align='left'><B>Doctor Name : </B></TD> <TD>" + "DR:" + txtDoctorName.Text.Trim() + "</TD></TR>";
                MailContent = MailContent + "<TR><TD  align='center'>&nbsp;</TD></TR>";
                MailContent = MailContent + "</TABLE>";

                string Criteria = " 1=1 ";
                Criteria += " AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

                DataSet DS = new DataSet();
                dboperations dbo = new dboperations();
                DS = dbo.InvoiceTransGet(Criteria);

                MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' style='width:100%;border: thin; border-color: #CCCCCC; border-style: groove;>'";
                MailContent = MailContent + "<TR><TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'>No</TD>";
                MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Code</TD>";
                MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Description</TD>";
                MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Date</TD>";
                MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Qty</TD>";
                MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Rate</TD>";
                MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Amount</TD>";

                MailContent = MailContent + "</TR>";
                if (DS.Tables[0].Rows.Count > 0)
                {
                    int intCount = 1;
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        MailContent = MailContent + "<TR><TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'>" + intCount + "</TD>";
                        MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'>" + Convert.ToString(DR["HIT_SERV_CODE"]) + "</TD>";
                        MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_DESCRIPTION"]) + "</TD>";
                        MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + txtInvDate.Text.Trim() + "</TD>";  // Convert.ToString(DR["HIT_STARTDATEDesc"])
                        MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_QTY"]) + "</TD>";
                        MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_FEE"]) + "</TD>";
                        MailContent = MailContent + "<TD  align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_AMOUNT"]) + "</TD>";

                        MailContent = MailContent + "</TR>";

                        intCount = intCount + 1;
                    }


                }

                MailContent = MailContent + "</TABLE>";


                MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2'  style='width:100%;'>";
                MailContent = MailContent + "<TR><TD  align='center'>&nbsp;</TD></TR>";
                MailContent = MailContent + "<TR><TD  align='left'>Regards</TD></TR>";
                MailContent = MailContent + "<TR><TD  align='left'>" + GlobalValues.HospitalName + "</TD></TR>";
                MailContent = MailContent + "</TABLE>";


                Decimal decGrossAmt = 0, decPatientShare = 0, decClaimAmt = 0;

                decGrossAmt = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text)) + (txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text) + txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

                decPatientShare = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text)) + (txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

                decClaimAmt = txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text);

                MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' style='width:100%;>'";
                MailContent = MailContent + "<TR><TD  align='right'><B> Gross Amount : </B>" + decGrossAmt.ToString("#0.00") + "</TD>";
                MailContent = MailContent + "<TR><TD  align='right'><B> PT. Share : </B>" + decPatientShare.ToString("#0.00") + "</TD>";
                MailContent = MailContent + "<TR><TD  align='right'><B> Claim  Amount : </B>" + decClaimAmt.ToString("#0.00") + "</TD>";

                MailContent = MailContent + "</TABLE>";


                int intStatuls;

                intStatuls = objCom.HTMLMailSend(lblEmailID.Text.Trim(), "Reg Invoice", MailContent);

                if (intStatuls == 0)
                {
                    string FieldNameWithValues = " HIM_MAIL_SEND=1,HIM_MAIL_ID='" + lblEmailID.Text.Trim() + "',HIM_MAIL_DATE=getdate()";
                    string Criteria1 = " HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria1);

                    return true;
                }

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.SendEmail()");
                TextFileWriting(ex.Message.ToString());
                return false;

            }

            return false;

        }

        Boolean SendSMS()
        {
            try
            {
                if (lblMobileNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Mobile Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string MobileNumber = lblMobileNo.Text.Trim();

                if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
                {
                    lblStatus.Text = "Please check the Mobile Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                clsSMS objSMS = new clsSMS();
                string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";


                string Details_String = "";


                if (drpSMSTemplate.SelectedValue == "General Template")
                {
                    Details_String = Details_String + " Dear " + txtName.Text + "," + strNewLineChar;
                    Details_String = Details_String + " Your Invoice Details as follows" + strNewLineChar;
                    Details_String = Details_String + "Bill No. :" + txtInvoiceNo.Text.Trim() + ",Bill Date :" + txtInvDate.Text.Trim();
                    Details_String = Details_String + " Doctor Name : " + txtDoctorName.Text.Trim() + strNewLineChar;


                    string Criteria = " 1=1 ";
                    Criteria += " AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                    Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

                    DataSet DS = new DataSet();
                    dboperations dbo = new dboperations();
                    DS = dbo.InvoiceTransGet(Criteria);


                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        int intCount = 1;
                        foreach (DataRow DR in DS.Tables[0].Rows)
                        {
                            Details_String = Details_String + "Serv. Code : " + Convert.ToString(DR["HIT_SERV_CODE"]);
                            Details_String = Details_String + ",Qty: " + Convert.ToString(DR["HIT_QTY"]);
                            Details_String = Details_String + ",Fees : " + Convert.ToString(DR["HIT_FEE"]);
                            Details_String = Details_String + ",Amount " + Convert.ToString(DR["HIT_AMOUNT"]) + strNewLineChar;

                            intCount = intCount + 1;
                        }


                    }

                    Decimal decGrossAmt = 0, decPatientShare = 0, decClaimAmt = 0;

                    decGrossAmt = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text)) + (txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text));// +(txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

                    decPatientShare = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text));// +(txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

                    decClaimAmt = (txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text));

                    Details_String = Details_String + "Gross Amount :" + decGrossAmt.ToString("#0.00");
                    Details_String = Details_String + ", PT. Share :" + decPatientShare.ToString("#0.00");
                    Details_String = Details_String + ", Claim  Amount :" + decClaimAmt.ToString("#0.00") + strNewLineChar;



                    Details_String = Details_String + "Regards " + strNewLineChar;
                    Details_String = Details_String + GlobalValues.HospitalName;
                }
                else
                {

                    Decimal decGrossAmt = 0, decPatientShare = 0, decClaimAmt = 0;

                    decGrossAmt = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text)) + (txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text));// +(txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

                    decPatientShare = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text));// +(txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

                    decClaimAmt = (txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text));

                    string BranchName = "";
                    if (GlobalValues.FileDescription == "ALFAISAL")
                    {
                        BranchName = "FMC";
                    }
                    else
                    {
                        BranchName = Convert.ToString(Session["Branch_Name"]);
                    }

                    Details_String = Details_String + "Thank you for visiting " + BranchName + strNewLineChar;
                    Details_String = Details_String + "Your invoice details as follows" + strNewLineChar;
                    Details_String = Details_String + "Invoice No. " + txtInvoiceNo.Text.Trim() + ", Date: " + txtInvDate.Text.Trim() + strNewLineChar; ;
                    Details_String = Details_String + drpSMSTemplate.SelectedItem.Text + ": " + decGrossAmt.ToString("#0.00") + " AED" + strNewLineChar;
                    Details_String = Details_String + "Patient Share: " + decPatientShare.ToString("#0.00") + " AED" + strNewLineChar; ;
                    Details_String = Details_String + "Total  Amt: " + decClaimAmt.ToString("#0.00") + " AED" + strNewLineChar;

                }

                objSMS.MobileNumber = lblMobileNo.Text; // "0502213045";
                objSMS.template = Details_String;

                Boolean boolResult = false;

                if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "AXON")
                {

                    // boolResult = objSMS.SendSMS_smsmarketing();
                    //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                    boolResult = objSMS.SendSMS_Axome(0);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "BRANDMASTER")
                {

                    // boolResult = objSMS.SendSMS_smsmarketing();
                    //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                    boolResult = objSMS.SendSMS_BrandMaster(0);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSMARKETING")
                {
                    boolResult = objSMS.SendSMS_smsmarketing();
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "MSHASTRA")
                {
                    boolResult = objSMS.SendSMS_Mshastra();
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "API_MPP_KWT")
                {
                    //LANGUAGE : 1  FOR ENGLISH  , 3 FOR ARABIC
                    boolResult = objSMS.SendSMS_API_MPP_KWT(1);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSCOUNTRY")
                {
                    boolResult = objSMS.SendSMS_SMScountry();
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMARTCALL")
                {
                    //LANGUAGE : 0  FOR ENGLISH  , 1 FOR ARABIC
                    boolResult = objSMS.SendSMS_Smartcall(0);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else
                {
                    boolResult = objSMS.SendSMS();
                }


                if (boolResult == true)
                {
                    CommonBAL objCom = new CommonBAL();
                    string FieldNameWithValues = " HIM_SMS_SEND=1,HIM_MOBILE_NO='" + lblMobileNo.Text.Trim() + "'";
                    string Criteria1 = " HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria1);

                    return true;
                }

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.SendEmail()");
                TextFileWriting(ex.Message.ToString());
                return false;

            }

            return false;

        }

        Boolean SendSMS_Wishes()
        {
            try
            {
                if (lblMobileNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Mobile Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string MobileNumber = lblMobileNo.Text.Trim();

                if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
                {
                    lblStatus.Text = "Please check the Mobile Number";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                clsSMS objSMS = new clsSMS();
                string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";


                string Details_String = "";


                if (drpSMSTemplate.SelectedValue == "General Template")
                {
                    Details_String = Details_String + " We wish you a speedy recovery" + strNewLineChar;
                    Details_String = Details_String + " نتمنى لكم الشفاء العاجل" + strNewLineChar;

                    Details_String = Details_String + "Regards " + strNewLineChar;
                    Details_String = Details_String + GlobalValues.HospitalName;
                }


                objSMS.MobileNumber = lblMobileNo.Text; // "0502213045";
                objSMS.template = Details_String;

                Boolean boolResult = false;

                if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "AXON")
                {

                    // boolResult = objSMS.SendSMS_smsmarketing();
                    //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                    boolResult = objSMS.SendSMS_Axome(0);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "BRANDMASTER")
                {

                    // boolResult = objSMS.SendSMS_smsmarketing();
                    //LANGUAGE : 0  FOR ENGLISH  , 8 FOR ARABIC

                    boolResult = objSMS.SendSMS_BrandMaster(0);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSMARKETING")
                {
                    boolResult = objSMS.SendSMS_smsmarketing();
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "MSHASTRA")
                {
                    boolResult = objSMS.SendSMS_Mshastra();
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "API_MPP_KWT")
                {
                    //LANGUAGE : 1  FOR ENGLISH  , 3 FOR ARABIC
                    boolResult = objSMS.SendSMS_API_MPP_KWT(1);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMSCOUNTRY")
                {
                    boolResult = objSMS.SendSMS_SMScountry();
                }
                else if (Convert.ToString(ViewState["SMS_TYPE"]).ToUpper() == "SMARTCALL")
                {
                    //LANGUAGE : 0  FOR ENGLISH  , 1 FOR ARABIC
                    boolResult = objSMS.SendSMS_Smartcall(0);//Convert.ToInt32(drpSMSLanguage.SelectedValue)
                }
                else
                {
                    boolResult = objSMS.SendSMS();
                }


            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.SendEmail()");
                TextFileWriting(ex.Message.ToString());
                return false;

            }

            return false;

        }

        protected void btnSaveAndEmail_Click(object sender, EventArgs e)
        {

            try
            {
                System.Threading.Thread.Sleep(5000);

                Boolean isError = false, isMailSend = false;
                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    if (IsPatientBilledSameDaySameDr(true) == true)
                    {
                        goto FunEnd;
                    }
                }

                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                    {
                        if (CheckDuplicateInvoice() == true)
                        {
                            goto FunEnd1;
                        }
                    }

                }


                if (txtEMRID.Text.Trim() != "" && txtEMRID.Text.Trim() != "0")
                {
                    if (CheckEMR_ID() == false)
                    {

                        goto FunEnd;
                    }

                }


                isError = fnSave();



            FunEnd1: ;

                if (isError == false)
                {
                    isMailSend = SendEmail();

                    if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                    {
                        if (drpSMSTemplate.SelectedIndex != 0)
                        {
                            SendSMS();

                            if (drpSMSTemplate.SelectedValue == "General Template")
                            {
                                SendSMS_Wishes();
                            }
                        }
                    }

                    Clear();
                    New();

                    if (isMailSend == true)
                    {
                        MPExtMessage.Show();
                        strMessage = "Details Saved and Mail Send Successfully";
                    }
                    else
                    {
                        MPExtMessage.Show();
                        strMessage = "Details Saved , Mail Not Send !";
                    }


                }
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnSaveAndEmail_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void imgGetServDtls_Click(object sender, ImageClickEventArgs e)
        {
            txtServCode_TextChanged(txtServCode, new EventArgs());
        }

        protected void btnAuthorIDGet_Click(object sender, EventArgs e)
        {
            BindeAuthorizationDtls();
        }

        protected void btnImportObservation_Click(object sender, EventArgs e)
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceTrans"];

            string strServIds = "";
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    string strServCode = Convert.ToString(DT.Rows[i]["HIT_SERV_CODE"]);

                    if (strServIds != "")
                    {
                        strServIds += ",'" + strServCode + "'";
                    }
                    else
                    {
                        strServIds = "'" + strServCode + "'";
                    }

                }

            }

            DataSet DSServ = new DataSet();
            dboperations objDBO = new dboperations();
            string ServCode, ServName;

            string Criteria1 = " 1=1 AND HSM_STATUS='A' ";

            Criteria1 += " AND  HSM_SERV_ID  in (" + strServIds + ")";
            DSServ = objDBO.ServiceMasterGet(Criteria1);


            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    ServCode = Convert.ToString(DT.Rows[i]["HIT_SERV_CODE"]);


                    DataRow[] DR;
                    DR = DSServ.Tables[0].Select("HSM_SERV_ID ='" + ServCode + "'");

                    string ServTrnt = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "";
                    decimal ObsConversion = 0;


                    ServTrnt = Convert.ToString(DR[0]["HSM_TYPE"]);

                    ObsNeeded = Convert.ToString(DR[0]["HSM_OBSERVATION_NEEDED"]);

                    if (ObsNeeded == "Y")
                    {
                        ObsCode = Convert.ToString(DR[0]["HSM_OBS_CODE"]);//txtObsCode.Text = 
                        ObsValueType = Convert.ToString(DR[0]["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                        ObsConversion = DR[0].IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DR[0]["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DR[0]["HSM_OBS_CONVERSION"]);

                        if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                        {
                            ObsType = "Result";
                        }
                        else
                        {
                            if (ServTrnt == "L")
                            {
                                ObsType = "LOINC";
                            }
                            else
                            {
                                ObsType = "Universal Dental";
                            }
                        }

                        if (ObsValue == "" && Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")//&& GlobalValues.FileDescription == "AMMC"
                        {
                            GetLabValue(ServCode, ObsConversion, out ObsValue, out  ObsValueType);
                            if (ServTrnt == "C")
                            {
                                GetBPValue(ServCode, out ObsValue, out  ObsValueType);
                            }

                        }


                    }



                    DT.Rows[i]["HIO_TYPE"] = ObsType;
                    DT.Rows[i]["HIO_CODE"] = ObsCode;
                    DT.Rows[i]["HIO_VALUE"] = ObsValue;
                    DT.Rows[i]["HIO_VALUETYPE"] = ObsValueType;
                }

            }
            DT.AcceptChanges();
            ViewState["InvoiceTrans"] = DT;

            BindTempInvoiceTrans();


        }

        protected void btnMailPreview_Click(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();

            MailContent = "";

            MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' style='width:100%;'>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='left'>Dear  <b>" + txtName.Text + ",</b></TD></TR>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='Left'>Invoice Details</TD></TR>";
            MailContent = MailContent + "<TR><TD valign='top' align='left'><B>Bill No. : </B></TD> <TD>" + txtInvoiceNo.Text.Trim() + "</TD><TD valign='top' align='left'><B>Bill Date : </B></TD> <TD>" + txtInvDate.Text.Trim() + "</TD></TR>";
            MailContent = MailContent + "<TR><TD valign='top' align='left'><B>Name. : </B></TD> <TD>" + txtName.Text.Trim() + "</TD><TD valign='top' align='left'><B>File No. : </B></TD> <TD>" + txtFileNo.Text.Trim() + "</TD></TR>";
            MailContent = MailContent + "<TR><TD valign='top' align='left'><B>Doctor Name : </B></TD> <TD>" + "DR:" + txtDoctorName.Text.Trim() + "</TD></TR>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            MailContent = MailContent + "</TABLE>";

            string Criteria = " 1=1 ";
            Criteria += " AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.InvoiceTransGet(Criteria);

            MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' style='width:100%;border: thin; border-color: #CCCCCC; border-style: groove;>'";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'>No</TD>";
            MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Code</TD>";
            MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Description</TD>";
            MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Date</TD>";
            MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Qty</TD>";
            MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Rate</TD>";
            MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> Amount</TD>";

            MailContent = MailContent + "</TR>";
            if (DS.Tables[0].Rows.Count > 0)
            {
                int intCount = 1;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    MailContent = MailContent + "<TR><TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'>" + intCount + "</TD>";
                    MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'>" + Convert.ToString(DR["HIT_SERV_CODE"]) + "</TD>";
                    MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_DESCRIPTION"]) + "</TD>";
                    MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + txtInvDate.Text.Trim() + "</TD>";  // Convert.ToString(DR["HIT_STARTDATEDesc"])
                    MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_QTY"]) + "</TD>";
                    MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_FEE"]) + "</TD>";
                    MailContent = MailContent + "<TD COLSPAN=2 align='center' style='border: 1px solid #dcdcdc; height: 25px;'> " + Convert.ToString(DR["HIT_AMOUNT"]) + "</TD>";

                    MailContent = MailContent + "</TR>";

                    intCount = intCount + 1;
                }


            }

            MailContent = MailContent + "</TABLE>";


            MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' >";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='left'>Regards</TD></TR>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='left'>" + GlobalValues.HospitalName + "</TD></TR>";
            MailContent = MailContent + "</TABLE>";


            Decimal decGrossAmt = 0, decPatientShare = 0, decClaimAmt = 0;

            decGrossAmt = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text)) + (txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text) + txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

            decPatientShare = (txtBillAmt4.Text == "" ? 0 : Convert.ToDecimal(txtBillAmt4.Text)) + (txtSplDisc.Text == "" ? 0 : Convert.ToDecimal(txtSplDisc.Text));

            decClaimAmt = txtClaimAmt.Text == "" ? 0 : Convert.ToDecimal(txtClaimAmt.Text);

            MailContent = MailContent + "<TABLE cellpadding='2' cellspacing='2' style='width:100%;>'";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='right'><B> Gross Amount : </B>" + decGrossAmt.ToString("#0.00") + "</TD>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='right'><B> PT. Share : </B>" + decPatientShare.ToString("#0.00") + "</TD>";
            MailContent = MailContent + "<TR><TD COLSPAN=2 align='right'><B> Claim  Amount : </B>" + decClaimAmt.ToString("#0.00") + "</TD>";

            MailContent = MailContent + "</TABLE>";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMailPreview()", true);
        }

        protected void btnVisitDtlsSrc_Click(object sender, EventArgs e)
        {
            BindVisitDtlsAll();
        }

        protected void SelectVisitDtlsAll_Click(object sender, EventArgs e)
        {
            try
            {
                txtEMRID.Text = "";
                txtSrcVisitFileNo.Text = "";

                ViewState["SelectedVisitID"] = "";

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;


                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent.Parent.NamingContainer;



                Label lblgvVisitDtlsAllID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllID");
                ////Label lblgvVisitDtlsAllEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllEMR_ID");
                Label lblgvVisitDtlsAllPTId = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllPTId");

                ////Label lblgvVisitDtlsAllDrID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllDrID");
                ////Label lblgvVisitDtlsAllDRName = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllDRName");

                ////Label lblgvVisitDtlsAllRefDrId = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllRefDrId");

                Label lblgvVisitDtlsAllEligibilityID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllEligibilityID");

                Label lblgvVisitDtlsAllCompID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllCompID");
                Label lblgvVisitDtlsAllCompName = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllCompName");
                Label lblgvVisitDtlsAllPolicyType = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllPolicyType");
                Label lblgvVisitDtlsAllIDNo = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllIDNo");
                Label lblgvVisitDtlsAllPolicyNo = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllPolicyNo");
                Label lblgvVisitDtlsAllPolicyExp = (Label)gvScanCard.Cells[0].FindControl("lblgvVisitDtlsAllPolicyExp");

                ViewState["SelectedVisitID"] = lblgvVisitDtlsAllID.Text;

                txtFileNo.Text = lblgvVisitDtlsAllPTId.Text;
                ///txtFileNo_TextChanged(txtFileNo, new EventArgs());
                ///

                if (TopUpCardChecking(txtFileNo.Text.Trim()) == true)
                {
                    ChkTopupCard.Checked = true;

                }
                else
                {
                    ChkTopupCard.Checked = false;

                    TxtTopupAmt.Text = "0";
                }

                PatientDataBind();
                if (drpInvType.SelectedValue == "Credit" || drpInvType.SelectedValue == "Customer")
                {
                    BindCompanyBenefites();

                    string strPayerID = "", strReceiverID = "";
                    GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

                    txtPayerID.Text = strPayerID;
                    txtReciverID.Text = strReceiverID;

                }
                else
                {
                    txtPayerID.Text = "Selfpay";
                    if (Convert.ToString(ViewState["ECLAIM_TYPE"]) == "DXB")
                    {
                        txtReciverID.Text = "@DHA";

                    }
                    else
                    {
                        txtReciverID.Text = "@HAAD";
                    }
                }
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    IsPatientBilledToday();
                }



                // PatientVisitBind();
                ViewState["SelectedVisitID"] = "";
            EndSub: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.SelectVisitDtlsAll_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnVisitDtlsShow_Click(object sender, EventArgs e)
        {
            BindVisitDtlsAll();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowVisitDetailsPopup()", true);

        }

        protected void txtSubCompanyID_TextChanged(object sender, EventArgs e)
        {
            //string strName = txtCompany.Text;
            //string[] arrName = strName.Split('~');


            //if (arrName.Length > 1)
            //{
            //    txtCompany.Text = arrName[0];
            //    txtCompanyName.Text = arrName[1];

            //}

            string strPayerID = "", strReceiverID = "";

            GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

            txtPayerID.Text = strPayerID;
            txtReciverID.Text = strReceiverID;


        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            //string strName = txtCompanyName.Text;
            //string[] arrName = strName.Split('~');


            //if (arrName.Length > 1)
            //{
            //    txtCompany.Text = arrName[0];
            //    txtCompanyName.Text = arrName[1];

            //}

            string strPayerID = "", strReceiverID = "";

            GetCompanyDtls(txtSubCompanyID.Text.Trim(), out  strPayerID, out  strReceiverID);

            txtPayerID.Text = strPayerID;
            txtReciverID.Text = strReceiverID;
        }
    }


}