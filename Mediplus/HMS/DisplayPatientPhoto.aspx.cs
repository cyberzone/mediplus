﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Mediplus_BAL;

namespace Mediplus.HMS
{
    public partial class DisplayPatientPhoto : System.Web.UI.Page
    {
        void BindScanFileFromPhysical()
        {
            string ScanFileName = Convert.ToString(ViewState["FileName"]);
            string FileExtension = ScanFileName.Substring(ScanFileName.LastIndexOf('.'), ScanFileName.Length - ScanFileName.LastIndexOf('.'));


            string ScanSourcePath = @"F:\Projects\Mediplus\Mediplus\HMS\Images\PTEmiratesID\" + ScanFileName;


            if (System.IO.File.Exists(ScanSourcePath) == true)
            {
                if (FileExtension.ToLower() == ".pdf")
                {
                    Response.ContentType = "Application/pdf";
                }
                else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
                {
                    Response.ContentType = "application/msword";// "application/ms-word";
                }
                else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
                {
                    Response.AddHeader("content-disposition", "attachment; filename=" + ScanFileName);
                    Response.AddHeader("Content-Type", "application/Excel");
                    Response.ContentType = "application/vnd.xls";
                }
                else if (FileExtension.ToLower() == ".jpg")
                {
                    Response.ContentType = "image/JPEG";
                }
                else if (FileExtension.ToLower() == ".png")
                {
                    Response.ContentType = "image/png";
                }
                else if (FileExtension.ToLower() == ".bmp")
                {
                    Response.ContentType = "image/bmp";
                }
                else if (FileExtension.ToLower() == ".gif")
                {
                    Response.ContentType = "image/gif";
                }



                Response.WriteFile(ScanSourcePath);
                Response.End();


            }


        }

        void PatientPhoto()
        {

            Byte[] bytImage;
            dboperations dbo = new dboperations();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_PT_ID = '" + Convert.ToString(ViewState["PT_ID"]) + "'";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            ds = dbo.PatientPhotoGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {


                if (ds.Tables[0].Rows[0].IsNull("HPP_PHOTO") == false)
                {

                    bytImage = (Byte[])ds.Tables[0].Rows[0]["HPP_PHOTO"];


                    Response.BinaryWrite(bytImage);
                }



            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["PT_ID"] = Request.QueryString["PT_ID"];
            ViewState["FileName"] = Request.QueryString["FileName"];

            ViewState["InsCardPath"] = (string)System.Configuration.ConfigurationSettings.AppSettings["InsCardPath"];

            if (Convert.ToString(ViewState["PT_ID"]) != "")
            {
                PatientPhoto();
            }
            else
            {
                  BindScanFileFromPhysical();

              //  byte[] array;
               // array = Convert.FromBase64String(Convert.ToString(Session["PatientPhoto"]));


                // Byte[] newBytes = (Byte[]) (Session["PatientPhoto"]);

              //  Response.ContentType = "image/JPEG";
               // Response.BinaryWrite(newBytes);
            }
        }

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

    }
}