﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus.HMS
{
    public partial class DisplaySignaturePatient : System.Web.UI.Page
    {
        void BindPhoto()
        {
            Byte[] bytImage;

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            string Criteria = " 1=1  AND HPP_SIGNATURE IS NOT NULL ";
            Criteria += " AND HPP_PT_ID='" + Convert.ToString(ViewState["PatientID"]) + "'";

            DS = dbo.PatientPhotoGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPP_SIGNATURE") == false)
                {
                    bytImage = (Byte[])DS.Tables[0].Rows[0]["HPP_SIGNATURE"];
                    Response.BinaryWrite(bytImage);
                }

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["PatientID"] = Request.QueryString["PatientID"];
            BindPhoto();
        }
    }
}