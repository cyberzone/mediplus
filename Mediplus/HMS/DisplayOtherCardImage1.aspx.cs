﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.HMS
{
    public partial class DisplayOtherCardImage1 : System.Web.UI.Page
    {
        void BindScanFileFromPhysical()
        {
            string ScanFileName = Convert.ToString(ViewState["FileName"]);
            string FileExtension = ScanFileName.Substring(ScanFileName.LastIndexOf('.'), ScanFileName.Length - ScanFileName.LastIndexOf('.'));


            string ScanSourcePath = @Convert.ToString(ViewState["InsCardPath"]) + ScanFileName;


            if (System.IO.File.Exists(ScanSourcePath) == true)
            {
                if (FileExtension.ToLower() == ".pdf")
                {
                    Response.ContentType = "Application/pdf";
                }
                else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
                {
                    Response.ContentType = "application/msword";// "application/ms-word";
                }
                else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
                {
                    Response.AddHeader("content-disposition", "attachment; filename=" + ScanFileName);
                    Response.AddHeader("Content-Type", "application/Excel");
                    Response.ContentType = "application/vnd.xls";
                }
                else if (FileExtension.ToLower() == ".jpg")
                {
                    Response.ContentType = "image/JPEG";
                }
                else if (FileExtension.ToLower() == ".png")
                {
                    Response.ContentType = "image/png";
                }
                else if (FileExtension.ToLower() == ".bmp")
                {
                    Response.ContentType = "image/bmp";
                }
                else if (FileExtension.ToLower() == ".gif")
                {
                    Response.ContentType = "image/gif";
                }



                Response.WriteFile(ScanSourcePath);
                Response.End();


            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["PT_ID"] = Request.QueryString["PT_ID"];
            ViewState["FileName"] = Request.QueryString["FileName"];

            ViewState["InsCardPath"] = (string)System.Configuration.ConfigurationSettings.AppSettings["InsCardPath"];

            BindScanFileFromPhysical();

        }
    }
}