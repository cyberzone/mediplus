﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Mediplus_BAL;


namespace Mediplus.HMS
{
    public partial class DisplayCardImageBack : System.Web.UI.Page
    {
        void PatientPhoto()
        {

            Byte[] bytImage;
            dboperations dbo = new dboperations();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_PT_ID = '" + Convert.ToString(ViewState["PT_ID"]) + "'";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            ds = dbo.PatientPhotoGet(Criteria);

            if (ds.Tables[0].Rows.Count > 0)
            {
                

                if (ds.Tables[0].Rows[0].IsNull("HPP_INS_CARD1") == false)
                {

                    bytImage = (Byte[])ds.Tables[0].Rows[0]["HPP_INS_CARD1"];


                    Response.BinaryWrite(bytImage);
                }



            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Byte[] bytImage;

            //if (Convert.ToString(Session["SignatureImg1"]) != "")
            //{
            //    bytImage = (Byte[])Session["SignatureImg1"];
            //    //Session["SignatureImg1"] = "";

            //    Response.BinaryWrite(bytImage);

            //}

            ViewState["PT_ID"] = Request.QueryString["PT_ID"];
            PatientPhoto();
        }
    }
}