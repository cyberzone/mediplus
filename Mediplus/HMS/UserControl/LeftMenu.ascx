﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftMenu.ascx.cs" Inherits="Mediplus.HMS.UserControl.LeftMenu" %>


<style type="text/css">
    .MenuStyle
    {
        background-color: #fff;
        color: #5c5c5c;
        font-size: 12px;
        font-family: Helvetica, Arial, sans-serif;
        border: none;
        height: 22px;
        width:100%;
        padding: 1px 0px;
        font-weight:bold;
    }
     
        

        .MenuStyle:hover
        {
            /*background-color:#D64535;*/
            color: #D64535;
            font-size: 14px;
            width: 100%;
            vertical-align: central;
            text-decoration:none;
        }
       .MenuStyleSeleted 
        {
               font-size: 14px;
            color: #D64535;
           
        }

      
</style>
 
<div style="width:100%;background-color:#fff;">
 
                <asp:UpdatePanel ID="pnlMenu" runat="server">
                    <ContentTemplate>

                       <%-- <asp:Menu ID="Menu1" runat="server" Orientation="Vertical" Width="100%" Style="width: 100%; position: relative; float: left;padding:0px;"
                            StaticMenuItemStyle-CssClass="MenuStyle" DynamicSelectedStyle-CssClass="MenuStyleSeleted">
                        </asp:Menu>--%>

                          <asp:TreeView ID="MyTree" Width="100px" OnSelectedNodeChanged="MyTree_SelectedNodeChanged"
                                        PathSeparator="|"
                                        ExpandDepth="0"
                                        runat="server" ImageSet="Arrows"
                                        AutoGenerateDataBindings="False">
                                        <SelectedNodeStyle   HorizontalPadding="0px" VerticalPadding="0px"   CssClass="MenuStyleSeleted"></SelectedNodeStyle>
                                        <NodeStyle VerticalPadding="0px"  CssClass="MenuStyle"
                                            HorizontalPadding="5px"  NodeSpacing="0px"></NodeStyle>
                                        <ParentNodeStyle Font-Bold="False" />
                                        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD"></HoverNodeStyle>
                                    </asp:TreeView>
                      
                    </ContentTemplate>
                </asp:UpdatePanel>
 
</div>

