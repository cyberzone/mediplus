﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Mediplus_BAL;
 

namespace Mediplus.HMS
{
    public partial class DisplaySignature3 : System.Web.UI.Page
    {
        void PatientSign()
        {

            Byte[] bytImage;
            string Criteria = " 1=1 ";
            Criteria += " AND HGC_PT_ID = '" + Convert.ToString(ViewState["PT_ID"]) + "'";
            Criteria += " AND HGC_ID = " + Convert.ToString(ViewState["HGC_ID"]);

            clsGeneralConsent objGC = new clsGeneralConsent();
            DataSet DS = new DataSet();
            DS = objGC.GeneralConsentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                if (DS.Tables[0].Rows[0].IsNull("HGC_TRAN_SIGNATURE_IMAGE") == false)
                {

                    bytImage = (Byte[])DS.Tables[0].Rows[0]["HGC_TRAN_SIGNATURE_IMAGE"];


                    Response.BinaryWrite(bytImage);
                }



            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Byte[] bytImage;

            //if (Convert.ToString(Session["SignatureImg1"]) != "")
            //{
            //    bytImage = (Byte[])Session["SignatureImg1"];
            //    //Session["SignatureImg1"] = "";

            //    Response.BinaryWrite(bytImage);

            //}

            ViewState["PT_ID"] = Request.QueryString["PT_ID"];
            ViewState["HGC_ID"] = Request.QueryString["HGC_ID"];
            PatientSign();
        }
    }
}