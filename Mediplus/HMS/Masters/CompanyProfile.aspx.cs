﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using Mediplus_BAL;




namespace Mediplus.HMS.Masters
{
    public partial class CompanyProfile : System.Web.UI.Page
    {


        DataSet DS;
        dboperations dbo = new dboperations();
        CommonBAL objCommBal = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindInsCategory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HICM_STATUS='A'  AND  HICM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.InsCatMastereGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                lstInsCategory.DataSource = DS;
                lstInsCategory.DataTextField = "HICM_INS_CAT_ID";
                lstInsCategory.DataValueField = "HICM_INS_CAT_ID";
                lstInsCategory.DataBind();
            }


        }

        void BindInsCategoryPackage()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HICM_STATUS='A' AND HICM_PACKAGE='Y'  AND  HICM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.InsCatMastereGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                lstPackage.DataSource = DS;
                lstPackage.DataTextField = "HICM_INS_CAT_NAME";
                lstPackage.DataValueField = "HICM_INS_CAT_NAME";
                lstPackage.DataBind();
            }


        }

        void BindSelectedCategory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HCB_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HCB_COMP_ID = '" + txtCompCode.Text.Trim() + "'";

            DS = dbo.CompBenefitsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                lstSelectedCat.DataSource = DS;
                lstSelectedCat.DataTextField = "HCB_CAT_TYPE";
                lstSelectedCat.DataValueField = "HCB_CAT_TYPE";
                lstSelectedCat.DataBind();
            }





        }

        void BindCity()
        {
            DataSet ds = new DataSet();
            ds = dbo.city();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCity.DataSource = ds;
                drpCity.DataValueField = "HCIM_NAME";
                drpCity.DataTextField = "HCIM_NAME";
                drpCity.DataBind();
            }
            drpCity.Items.Insert(0, "--- Select ---");
            drpCity.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindCountry()
        {
            DataSet ds = new DataSet();
            ds = dbo.country();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCountry.DataSource = ds;
                drpCountry.DataValueField = "hcm_name";
                drpCountry.DataTextField = "hcm_name";
                drpCountry.DataBind();

            }
            drpCountry.Items.Insert(0, "--- Select ---");
            drpCountry.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindCompanyStatementMasterGet()
        {
            string Criteria = " 1=1 ";
            DataSet ds = new DataSet();
            ds = dbo.CompanyStatementMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpReportName.DataSource = ds;
                drpReportName.DataValueField = "HCSM_ID";
                drpReportName.DataTextField = "HCSM_DESCRIPTION";
                drpReportName.DataBind();

            }
            drpReportName.Items.Insert(0, "--- Select ---");
            drpReportName.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindCompanyStatementTxtbox()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HCSM_ID='" + drpReportName.SelectedValue + "'";
            DataSet ds = new DataSet();
            ds = dbo.CompanyStatementMasterGet(Criteria);
            txtReportName.Text = "";
            txtReportSummary.Text = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtReportName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HCSM_DETAIL_RPT_NAME"]);
                txtReportSummary.Text = Convert.ToString(ds.Tables[0].Rows[0]["HCSM_SUMMARY_RPT_NAME"]);
            }

            ds.Clear();
            ds.Dispose();

        }

        void ModifyCompany()
        {
            string Criteria = " 1=1 AND  HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HCM_COMP_ID = '" + txtCompCode.Text.Trim() + "' ";


            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_inccmp_details(Criteria);
            if (DS.Tables[0].Rows.Count <= 0)
            {
                txtBillToCode.Text = txtCompCode.Text.Trim();
                goto FunEnd;
            }

            ViewState["NewFlag"] = false;
            hidActualCompanyId.Value = Convert.ToString(DS.Tables[0].Rows[0]["HCM_COMP_ID"]);
            txtCompName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_NAME"]);
            txtBillToCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_BILL_CODE"]);
            txtAbbrev.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_ABRVTN"]);

            drpCompanyType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HCM_COMP_TYPE"]);

            txtAddress.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_ADDR"]);
            txtPOBox.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_POBOX"]);
            drpPayType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYMENT_TYPE"]);

            if (DS.Tables[0].Rows[0].IsNull("HCM_CITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_CITY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HCM_CITY"]) != "0")
            {
                for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                {
                    if (drpCity.Items[intCityCount].Value == DS.Tables[0].Rows[0]["HCM_CITY"].ToString())
                    {
                        drpCity.SelectedValue = DS.Tables[0].Rows[0]["HCM_CITY"].ToString();
                    }

                }
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_COUNTRY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_COUNTRY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HCM_COUNTRY"]) != "0")
            {
                for (int intCountryCount = 0; intCountryCount < drpCountry.Items.Count; intCountryCount++)
                {
                    if (drpCountry.Items[intCountryCount].Value == DS.Tables[0].Rows[0]["HCM_COUNTRY"].ToString())
                    {
                        drpCountry.SelectedValue = DS.Tables[0].Rows[0]["HCM_COUNTRY"].ToString();
                    }

                }
            }
            txtPhone1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PHONE1"]);
            txtPhone2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PHONE2"]);
            txtFax.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FAX"]);
            txtEmail.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_EMAIL"]);
            txtContactPer.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTACT_PERSON"]);
            txtContactNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTACT_NO"]);
            txtWeb.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_WEB"]);
            txtReminder.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_REM_RCPTN"]);



            if (DS.Tables[0].Rows[0].IsNull("HCM_REPORT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_REPORT_NAME"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HCM_REPORT_NAME"]) != "0")
            {
                for (int intCountryCount = 0; intCountryCount < drpReportName.Items.Count; intCountryCount++)
                {
                    if (drpReportName.Items[intCountryCount].Value == DS.Tables[0].Rows[0]["HCM_REPORT_NAME"].ToString())
                    {
                        drpReportName.SelectedValue = DS.Tables[0].Rows[0]["HCM_REPORT_NAME"].ToString();
                    }

                }
            }

            hidAccDiscount.Value = Convert.ToString(DS.Tables[0].Rows[0]["HCM_DISCOUNT"]);

            txtAccountId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_ID"]);
            // txtOpBal.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_OPENBAL"]);
            txtOpBalDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_OPENBAL_DT"]);
            if (DS.Tables[0].Rows[0].IsNull("HCM_REVISIT_DAYS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_REVISIT_DAYS"]) != "")
            {
                decimal CrLimitAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCM_CREDITLIMIT_AMT"]);
                txtCreditLimit.Text = CrLimitAmt.ToString("N2");
            }

            //txtInvoiceAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_INVOICE_AMT"]);
            //txtReceivedAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_RECIEVED_AMT"]);
            //txtRejectAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_REJECTED_AMT"]);


            if (DS.Tables[0].Rows[0].IsNull("HCM_PRE_APRVL_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_PRE_APRVL_AMT"]) != "")
            {
                decimal PreAppAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCM_PRE_APRVL_AMT"]);
                txtPreAppAmt.Text = PreAppAmt.ToString("N2");
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_REVISIT_DAYS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_REVISIT_DAYS"]) != "")
            {
                txtRevisitDays.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_REVISIT_DAYS"]);
            }


            if (DS.Tables[0].Rows[0].IsNull("HCM_STATUS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_STATUS"]) != "")
            {
                if (Convert.ToString(DS.Tables[0].Rows[0]["HCM_STATUS"]) == "A")
                {
                    chkStatus.Checked = true;
                }
                else
                {
                    chkStatus.Checked = false;
                }
            }


            if (DS.Tables[0].Rows[0].IsNull("HCM_AGREEMENT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]).Trim() != "")
            {
                for (int intCountryCount = 0; intCountryCount < drpContractType.Items.Count; intCountryCount++)
                {
                    if (drpContractType.Items[intCountryCount].Value == DS.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"].ToString())
                    {
                        drpContractType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]).Trim();
                    }
                }
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_CONTRACT_REFNO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_REFNO"]) != "")
            {
                txtContractRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_REFNO"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_CONTRACT_SIGDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_SIGDATE"]) != "")
            {
                txtSignedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_SIGDATE"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_CONTRACT_FROM") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_FROM"]) != "")
            {
                txtContStDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_FROM"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_CONTRACT_TO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_TO"]) != "")
            {
                txtContEnDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CONTRACT_TO"]);
            }




            string strPackage = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PACKAGE_DETAILS"]);
            string[] arrPackage = strPackage.Split('|');
            if (arrPackage.Length > 0)
            {
                for (int i = 0; i <= arrPackage.Length - 1; i++)
                {
                    for (int j = 0; j <= lstPackage.Items.Count - 1; j++)
                    {
                        if (arrPackage[i] == lstPackage.Items[j].Value)
                        {
                            lstPackage.Items[j].Selected = true;
                            goto ForEnd;
                        }
                    }
                ForEnd: ;

                }
            }



            if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD1") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD1"]) != "")
            {
                txtPackage.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD1"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_VAT_LIC_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_VAT_LIC_NO"]) != "")
            {
                txtTrnNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_VAT_LIC_NO"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) != "")
            {
                txtClaimFrmName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD3") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD3"]) != "")
            {
                txtRefCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD3"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD5") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]) != "")
            {
                txtHaadCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_PHY_REFCODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_PHY_REFCODE"]) != "")
            {
                txtMOH.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PHY_REFCODE"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_PAYERID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]) != "")
            {
                txtPayerId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_UPDATECODETYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]) != "")
            {
                if (Convert.ToString(DS.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]) == "Y")
                {
                    chkUpdateCodeType.Checked = true;
                }
                else
                {
                    chkUpdateCodeType.Checked = false;
                }
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_VALIDATE_ECLAIM") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_VALIDATE_ECLAIM"]) != "")
            {
                if (Convert.ToString(DS.Tables[0].Rows[0]["HCM_VALIDATE_ECLAIM"]) == "Y")
                {
                    chkValidateEClaim.Checked = true;
                }
                else
                {
                    chkValidateEClaim.Checked = false;
                }
            }

            if (DS.Tables[0].Rows[0].IsNull("HCM_ACCOUNT_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_CODE"]) != "")
            {
                txtAccountName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[0]["HCM_ACCOUNT_NAME"]);
            }

            BindPlanType();
            BindSelectedCategory();
            BindCompanyStatementTxtbox();
            BindCompanyPendingAmount();




        FunEnd: ;

        }

        string GetSelectedPackage()
        {
            string strTreatmentType = "";
            for (int i = 0; i <= lstPackage.Items.Count - 1; i++)
            {
                if (lstPackage.Items[i].Selected == true)
                {
                    strTreatmentType += lstPackage.Items[i].Value + "|";
                }

            }

            return strTreatmentType;
        }

        void BindPlanType()
        {
            drpCatType.Items.Clear();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCB_COMP_ID = '" + txtCompCode.Text.Trim() + "'";

            DS = dbo.CompBenefitsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCatType.DataSource = DS;
                drpCatType.DataTextField = "HCB_CAT_TYPE";
                drpCatType.DataValueField = "HCB_CAT_TYPE";
                drpCatType.DataBind();
            }

            drpCatType.Items.Insert(0, "--- Select ---");
            drpCatType.Items[0].Value = "0";



        }

        void BindCompBenefits()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            Criteria += " AND HCB_COMP_ID = '" + txtCompCode.Text.Trim() + "'";

            Criteria += " AND HCB_CAT_TYPE = '" + drpCatType.SelectedValue + "'";



            DS = dbo.CompBenefitsGet(Criteria);
            txtDeductible.Text = "";
            txtCoIns.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpTreatmentType.SelectedValue = DS.Tables[0].Rows[0]["HCB_TREATMENT_TYPE"].ToString();
                drpBenefitsType.SelectedValue = DS.Tables[0].Rows[0]["HCB_BENEFITS_TYPE"].ToString();


                drpHospDiscType.SelectedValue = DS.Tables[0].Rows[0]["HCB_HOSP_DISC_TYPE"].ToString();
                drpDedType.SelectedValue = DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_TYPE"].ToString();
                drpCoInsType.SelectedValue = DS.Tables[0].Rows[0]["HCB_COINS_TYPE"].ToString();
                drpPharType.SelectedValue = DS.Tables[0].Rows[0]["HCB_PHY_DISC_TYPE"].ToString();
                drpPharCoInsType.SelectedValue = DS.Tables[0].Rows[0]["HCB_PHY_COINS_TYPE"].ToString();




                if (DS.Tables[0].Rows[0].IsNull("HCB_HOSP_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_AMT"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_AMT"]);
                    txtHospDiscAmt.Text = DedAmt.ToString("N2");
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]);
                    txtDeductible.Text = DedAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_MAX_DED") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_MAX_DED"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_MAX_DED"]);
                    TxtMaxDed.Text = DedAmt.ToString("N2");
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_COINS_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]);
                    txtCoIns.Text = CoInsAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_MAX_COINS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_MAX_COINS"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_MAX_COINS"]);
                    TxtMaxCoIns.Text = DedAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_PHY_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PHY_DISC_AMT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PHY_DISC_AMT"]);
                    txtPharDisc.Text = CoInsAmt.ToString("N2");
                }



                if (DS.Tables[0].Rows[0].IsNull("HCB_PHY_COINS_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PHY_COINS_AMT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PHY_COINS_AMT"]);
                    txtPharCoInsDisc.Text = CoInsAmt.ToString("N2");
                }
                if (DS.Tables[0].Rows[0].IsNull("HCB_MAX_PHY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_MAX_PHY"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_MAX_PHY"]);
                    TxtMaxPhy.Text = CoInsAmt.ToString("N2");
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_CONSLT_INCL_HOSP_DISC") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_CONSLT_INCL_HOSP_DISC"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_CONSLT_INCL_HOSP_DISC"]) == "Y")
                    {
                        ChkConsultation.Checked = true;
                    }
                    else
                    {
                        ChkConsultation.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE_MAX_CONSLT_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_MAX_CONSLT_AMT"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_MAX_CONSLT_AMT"]) == "Y")
                    {
                        ChkDedMaxConsAmt.Checked = true;
                    }
                    else
                    {
                        ChkDedMaxConsAmt.Checked = false;
                    }
                }
                txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCB_REMARKS"]);


                if (DS.Tables[0].Rows[0].IsNull("HCB_DED_COINS_APPLICABLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_COINS_APPLICABLE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_COINS_APPLICABLE"]) == "Y")
                    {
                        ChkDedCoIns.Checked = true;
                    }
                    else
                    {
                        ChkDedCoIns.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_COINS_FROM_GROSSAMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_FROM_GROSSAMT"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_FROM_GROSSAMT"]) == "Y")
                    {
                        ChkCoInsFromGrossAmt.Checked = true;
                    }
                    else
                    {
                        ChkCoInsFromGrossAmt.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_SPL_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_SPL_CODE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_SPL_CODE"]) == "Y")
                    {
                        ChkSplCodeNeeded.Checked = true;
                    }
                    else
                    {
                        ChkSplCodeNeeded.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_COINSFROMGROSS_CLAIMFROMNET") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]) == "Y")
                    {
                        ChkCoInsFromGrossandClaimfromNetAmt.Checked = true;
                    }
                    else
                    {
                        ChkCoInsFromGrossandClaimfromNetAmt.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_DED_INSURANCE_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_INSURANCE_TYPE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_INSURANCE_TYPE"]) == "Y")
                    {
                        ChkDedBenefitsType.Checked = true;
                    }
                    else
                    {
                        ChkDedBenefitsType.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_PRICE_FACTOR") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PRICE_FACTOR"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PRICE_FACTOR"]);
                    txtPriceFactor.Text = CoInsAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_PERDAYLIMIT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PERDAYLIMIT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PERDAYLIMIT"]);
                    txtPerDayPTLimit.Text = CoInsAmt.ToString("N2");
                }

            }


        }

        void BindCompBenefitsGrid()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND HCBD_COMP_ID = '" + txtCompCode.Text.Trim() + "'";

            Criteria += " AND HCBD_CAT_TYPE = '" + drpCatType.SelectedValue + "'";


            DS = dbo.CompBenefitsDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHCB.Visible = true;
                gvHCB.DataSource = DS;
                gvHCB.DataBind();
            }
            else
            {
                BindBenefitsGridBasedFrom_InsTrmtType();

            }





        }

        void BindBenefitsGridBasedFrom_InsTrmtType()
        {

            DS = new DataSet();

            DataTable DT = new DataTable();


            DataColumn HCBD_CATEGORY_ID = new DataColumn();
            HCBD_CATEGORY_ID.ColumnName = "HCBD_CATEGORY_ID";

            DataColumn HCBD_CATEGORY = new DataColumn();
            HCBD_CATEGORY.ColumnName = "HCBD_CATEGORY";

            DataColumn HCBD_CATEGORY_TYPE = new DataColumn();
            HCBD_CATEGORY_TYPE.ColumnName = "HCBD_CATEGORY_TYPE";
            DataColumn HCBD_LIMIT = new DataColumn();
            HCBD_LIMIT.ColumnName = "HCBD_LIMIT";



            DataColumn HCBD_CO_INS_TYPE = new DataColumn();
            HCBD_CO_INS_TYPE.ColumnName = "HCBD_CO_INS_TYPE";

            DataColumn HCBD_CO_INS_AMT = new DataColumn();
            HCBD_CO_INS_AMT.ColumnName = "HCBD_CO_INS_AMT";

            DataColumn HCBD_DED_TYPE = new DataColumn();
            HCBD_DED_TYPE.ColumnName = "HCBD_DED_TYPE";

            DataColumn HCBD_DED_AMT = new DataColumn();
            HCBD_DED_AMT.ColumnName = "HCBD_DED_AMT";

            DT.Columns.Add(HCBD_CATEGORY_ID);
            DT.Columns.Add(HCBD_CATEGORY);
            DT.Columns.Add(HCBD_CATEGORY_TYPE);
            DT.Columns.Add(HCBD_LIMIT);
            DT.Columns.Add(HCBD_CO_INS_TYPE);
            DT.Columns.Add(HCBD_CO_INS_AMT);
            DT.Columns.Add(HCBD_DED_TYPE);
            DT.Columns.Add(HCBD_DED_AMT);

            string Criteria = " 1=1 ";
            DataSet DS1 = new DataSet();
            if (drpBenefitsType.SelectedIndex == 0)
            {
                Criteria += " AND HITT_STATUS='1' ";
                DS1 = dbo.TreatmentTypeGet(Criteria);

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                    {
                        DataRow objrow;
                        objrow = DT.NewRow();
                        objrow["HCBD_CATEGORY_ID"] = Convert.ToString(DS1.Tables[0].Rows[i]["HITT_ID"]);
                        objrow["HCBD_CATEGORY"] = Convert.ToString(DS1.Tables[0].Rows[i]["HITT_NAME"]);
                        objrow["HCBD_CATEGORY_TYPE"] = "Covered";
                        objrow["HCBD_LIMIT"] = "0.00";
                        objrow["HCBD_CO_INS_TYPE"] = "%";
                        if (txtCoIns.Text.Trim() != "")
                        {
                            objrow["HCBD_CO_INS_AMT"] = txtCoIns.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_CO_INS_AMT"] = "0.00";
                        }

                        objrow["HCBD_DED_TYPE"] = "%";
                        if (txtDeductible.Text.Trim() != "")
                        {
                            objrow["HCBD_DED_AMT"] = txtDeductible.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_DED_AMT"] = "0.00";
                        }
                        DT.Rows.Add(objrow);
                    }
                }
            }
            else
            {
                Criteria += " AND HSCM_STATUS='A'  AND HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                DS1 = dbo.ServiceCategoryGet(Criteria);

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                    {
                        DataRow objrow;

                        objrow = DT.NewRow();
                        objrow["HCBD_CATEGORY_ID"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_CAT_ID"]);
                        objrow["HCBD_CATEGORY"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_NAME"]);
                        objrow["HCBD_CATEGORY_TYPE"] = "Covered";
                        objrow["HCBD_LIMIT"] = "0.00";
                        objrow["HCBD_CO_INS_TYPE"] = "%";
                        if (txtCoIns.Text.Trim() != "")
                        {
                            objrow["HCBD_CO_INS_AMT"] = txtCoIns.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_CO_INS_AMT"] = "0.00";
                        }

                        objrow["HCBD_DED_TYPE"] = "%";
                        if (txtDeductible.Text.Trim() != "")
                        {
                            objrow["HCBD_DED_AMT"] = txtDeductible.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_DED_AMT"] = "0.00";
                        }

                        DT.Rows.Add(objrow);
                    }
                }
            }




            DS.Tables.Add(DT);
            gvHCB.Visible = true;
            gvHCB.DataSource = DS;
            gvHCB.DataBind();
        }

        void BindPriceFactor()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCPF_COMP_ID='" + txtCompCode.Text.Trim() + "'";
            DS = objCom.fnGetFieldValue("*,CONVERT(VARCHAR, HCPF_FROM_DATE,103) AS HCPF_FROM_DATEDesc,CONVERT(VARCHAR, HCPF_TO_DATE,103) AS HCPF_TO_DATEDesc ", "HMS_COMP_PRICE_FACTOR", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPriceFactorMaster.DataSource = DS;
                gvPriceFactorMaster.DataBind();

            }
        }


        void BindPriceFactorDtls()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSCM_STATUS='A' AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.ServiceCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPriceFactorDtls.DataSource = DS;
                gvPriceFactorDtls.DataBind();
            }


        }

        void BindCompanyPendingAmount()
        {
            Double dbmCmpOPBal, dblCmpInvAmt, dblCmpRejamt, dblCmpRecAmt, dbmCmpOPBalDue;
            dbmCmpOPBal = fnCalculateCompAmount("OPENBAL");
            dblCmpInvAmt = fnCalculateCompAmount("INVOICE") + fnCalculateCompAmount("RETURN");
            dblCmpRejamt = fnCalculateCompAmount("REJECTED");
            dblCmpRecAmt = fnCalculateCompAmount("PAIDAMT") + fnCalculateCompAmount("RETURNPAIDAMT");
            dbmCmpOPBalDue = (dblCmpInvAmt + dbmCmpOPBal) - (dblCmpRecAmt + dblCmpRejamt);

            txtOpBal.Text = dbmCmpOPBal.ToString("N2");
            txtInvoiceAmt.Text = dblCmpInvAmt.ToString("N2");
            txtReceivedAmt.Text = dblCmpRecAmt.ToString("N2");
            txtRejectAmt.Text = dblCmpRejamt.ToString("N2");
            txtBalance.Text = dbmCmpOPBalDue.ToString("N2");

        }

        Double fnCalculateCompAmount(string AmountType)
        {
            Double decCompanyAmount;

            string Criteria = " 1=1 ";

            Criteria += " AND HCB_COMP_ID = '" + txtCompCode.Text.Trim() + "'";
            Criteria += " AND HCB_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet DS = new DataSet();
            DS = objCommBal.CalculateCompAmount(AmountType, Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                decCompanyAmount = Convert.ToDouble(DS.Tables[0].Rows[0]["CompAmount"]);
                DS.Clear();
                DS.Dispose();
                return decCompanyAmount;

            }


            DS.Clear();
            DS.Dispose();

            return 0;

        }

        void Clear()
        {
            TabContainer1.ActiveTab = TabPanelGeneral;

            txtCompName.Text = "";
            txtBillToCode.Text = "";
            txtAbbrev.Text = "";
            txtAddress.Text = "";
            txtPOBox.Text = "";
            drpPayType.SelectedValue = "";

            if (drpCity.Items.Count > 0)
            {
                drpCity.SelectedIndex = 0;
            }

            if (drpCountry.Items.Count > 0)
            {
                drpCountry.SelectedIndex = 0;
            }
            txtPhone1.Text = "";
            txtPhone2.Text = "";
            txtFax.Text = "";
            txtEmail.Text = "";
            txtContactPer.Text = "";
            txtContactNo.Text = "";
            txtWeb.Text = "";
            txtReminder.Text = "";

            txtAccountId.Text = "";
            txtOpBal.Text = "";
            txtOpBalDate.Text = "";
            txtCreditLimit.Text = "";
            txtInvoiceAmt.Text = "";
            txtReceivedAmt.Text = "";
            txtRejectAmt.Text = "";
            txtBalance.Text = "";


            txtPreAppAmt.Text = "";
            txtRevisitDays.Text = "";
            chkStatus.Checked = true;
            if (drpContractType.Items.Count > 0)
            {
                drpContractType.SelectedIndex = 0;
            }
            txtSignedDate.Text = "";
            txtContStDate.Text = "";
            txtContEnDate.Text = "";

            txtAccountName.Text = "";
            //lstPackage.Items.se

            txtPackage.Text = "";
            txtTrnNo.Text = "";
            txtClaimFrmName.Text = "";
            txtRefCode.Text = "";
            txtHaadCode.Text = "";
            txtMOH.Text = "";
            txtPayerId.Text = "";
            chkUpdateCodeType.Checked = false;
            chkValidateEClaim.Checked = false;

            if (drpTreatmentType.Items.Count > 0)
            {
                drpTreatmentType.SelectedIndex = 0;
            }

            if (drpBenefitsType.Items.Count > 0)
            {
                drpBenefitsType.SelectedIndex = 0;
            }

            if (drpHospDiscType.Items.Count > 0)
            {
                drpHospDiscType.SelectedIndex = 0;
            }

            if (drpDedType.Items.Count > 0)
            {
                drpDedType.SelectedIndex = 0;
            }

            if (drpCoInsType.Items.Count > 0)
            {
                drpCoInsType.SelectedIndex = 0;
            }

            if (drpPharType.Items.Count > 0)
            {
                drpPharType.SelectedIndex = 0;
            }

            if (drpPharCoInsType.Items.Count > 0)
            {
                drpPharCoInsType.SelectedIndex = 0;
            }


            lstSelectedCat.Items.Clear();
            if (lstPackage.Items.Count > 0)
            {
                lstPackage.SelectedIndex = -1;
            }

            if (drpReportName.Items.Count > 0)
            {
                drpReportName.SelectedIndex = 0;
            }

            txtReportName.Text = "";
            txtReportSummary.Text = "";


            ViewState["FACTOR_ID"] = "0";
            txtFactorFromDate.Text = "";
            txtFactorToDate.Text = "";

            gvPriceFactorMaster.DataBind();
            BindPriceFactorDtls();

            hidCopyCompanyId.Value = "";
            hidCopyCompanyName.Value = "";
            hidNextCompanyId.Value = "";
            ViewState["NewFlag"] = true;
            ClearBenefit();



        }

        void ClearBenefit()
        {


            txtHospDiscAmt.Text = "";
            txtDeductible.Text = "";
            txtCoIns.Text = "";
            TxtMaxDed.Text = "";
            TxtMaxCoIns.Text = "";
            txtPharDisc.Text = "";
            txtPharCoInsDisc.Text = "";
            TxtMaxPhy.Text = "";
            ChkConsultation.Checked = false;
            ChkDedMaxConsAmt.Checked = false;
            ChkDedCoIns.Checked = false;
            ChkCoInsFromGrossAmt.Checked = false;
            ChkSplCodeNeeded.Checked = false;
            ChkCoInsFromGrossandClaimfromNetAmt.Checked = false;
            ChkDedBenefitsType.Checked = false;
            txtPriceFactor.Text = "";
            txtPerDayPTLimit.Text = "";
            txtRemarks.Text = "";
            gvHCB.DataBind();
        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "COMPANY";
            objCom.ScreenName = "Company Profile Master";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='COMPANY' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                btnClear.Enabled = false;
                btnCopy.Enabled = false;
                btnBalanceHistory.Enabled = false;

                btnContCategory.Enabled = false;
                btnContService.Enabled = false;
                btnCopyAgreement.Enabled = false;

                btnSave1.Enabled = false;
                btnDelete1.Enabled = false;
                btnClear1.Enabled = false;

                btnBenefitSave.Enabled = false;
                btnBenefitDelete.Enabled = false;
                btnBenefitClear.Enabled = false;
                btnUpdate.Enabled = false;


            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;

                btnClear.Enabled = false;
                btnCopy.Enabled = false;
                btnBalanceHistory.Enabled = false;

                btnContCategory.Enabled = false;
                btnContService.Enabled = false;
                btnCopyAgreement.Enabled = false;
                btnSave1.Enabled = false;
                btnDelete1.Enabled = false;
                btnClear1.Enabled = false;

                btnBenefitSave.Enabled = false;
                btnBenefitDelete.Enabled = false;
                btnBenefitClear.Enabled = false;
                btnUpdate.Enabled = false;
            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetAccountList(string prefixText)
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1  AND AAM_STATUS='A' ";
            Criteria += " AND  AAM_ACCOUNT_NAME like '%" + prefixText + "%'";

            // Criteria += " AND  AAM_CATEGORY = '" + strCategory + "'";


            string[] Data;
            MastersBAL objMast = new MastersBAL();
            ds = objMast.AccountMasterGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["AAM_CODE"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["AAM_ACCOUNT_NAME"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";


            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Company Profile Master Page");
                try
                {
                    ViewState["NewFlag"] = true;
                    ViewState["FACTOR_ID"] = "0";
                    string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);
                    switch (AutoSlNo)
                    {
                        case "A":
                            chkManual.Checked = false;
                            chkManual.Visible = false;
                            txtCompCode.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "COMPANY");
                            txtBillToCode.Text = txtCompCode.Text.Trim();
                            hidNextCompanyId.Value = txtCompCode.Text.Trim();

                            break;
                        case "M":
                            chkManual.Checked = true;
                            chkManual.Visible = false;
                            // txtCompCode.ReadOnly = false;
                            break;
                        case "B":
                            chkManual.Visible = true;
                            txtCompCode.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "COMPANY");
                            txtBillToCode.Text = txtCompCode.Text.Trim();
                            hidNextCompanyId.Value = txtCompCode.Text.Trim();
                            break;
                        default:
                            break;

                    }

                    BindInsCategory();
                    BindInsCategoryPackage();
                    BindCity();
                    BindCountry();
                    BindCompanyStatementMasterGet();
                    BindPlanType();
                    BindPriceFactorDtls();

                    if (GlobalValues.FileDescription == "SMCH")
                    {
                        lblMOH.Visible = true;
                        txtMOH.Visible = true;
                    }

                    string CompanyId = "";

                    ViewState["HPVSeqNo"] = "";
                    CompanyId = Convert.ToString(Request.QueryString["CompanyId"]);
                    if (CompanyId != " " && CompanyId != null)
                    {

                        txtCompCode.Text = Convert.ToString(CompanyId);
                        ModifyCompany();
                        BindPriceFactor();
                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void chkManual_CheckedChanged(object sender, EventArgs e)
        {
            if (chkManual.Checked == true)
            {
                txtCompCode.Text = "";
                txtBillToCode.Text = "";
                //txtCompCode.ReadOnly = false;
            }
            else
            {
                // txtCompCode.ReadOnly = true;
                txtCompCode.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "COMPANY");
                txtBillToCode.Text = txtCompCode.Text.Trim();
                //txtCompCode.Text = String.Format("{0:D4}", 20);// string.Format("20", "00000");
            }

        }

        protected void txtCompCode_TextChanged(object sender, EventArgs e)
        {
            try
            {

                Clear();
                ModifyCompany();
                BindPriceFactor();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.txtCompCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void txtOpBal_TextChanged(object sender, EventArgs e)
        {

            txtBalance.Text = txtOpBal.Text.Trim();
        }

        protected void drpCatType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearBenefit();
            BindCompBenefits();
            BindCompBenefitsGrid();

        }

        protected void txtDeductible_TextChanged(object sender, EventArgs e)
        {

            BindCompBenefitsGrid();

        }

        protected void txtCoIns_TextChanged(object sender, EventArgs e)
        {

            BindCompBenefitsGrid();

        }



        protected void btnCopy_Click(object sender, EventArgs e)
        {
            try
            {
                if (hidCopyCompanyId.Value == "")
                {
                    lblStatus.Text = "Please enter Company Code";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (hidCopyCompanyName.Value == "")
                {
                    lblStatus.Text = "Please enter Company Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                string Criteria = " 1=1 AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND HCM_COMP_ID = '" + hidCopyCompanyId.Value + "'";
                DataSet DS = new DataSet();
                dboperations dbo = new dboperations();
                DS = dbo.retun_inccmp_details(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Company code should unique";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Criteria = " 1=1 AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND HCM_COMP_ID = '" + hidActualCompanyId.Value + "'";
                DS = new DataSet();
                DS = dbo.retun_inccmp_details(Criteria);
                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = "Data not available.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }




                Boolean isManual = false;
                if (hidCopyCompanyId.Value != hidNextCompanyId.Value)
                {
                    isManual = true;
                }
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;


                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompanyMasterCopy";
                cmd.Parameters.Add(new SqlParameter("@HCM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_ID", SqlDbType.VarChar)).Value = hidCopyCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@HCM_NAME", SqlDbType.VarChar)).Value = hidCopyCompanyName.Value;
                cmd.Parameters.Add(new SqlParameter("@HCM_BILL_CODE", SqlDbType.VarChar)).Value = hidCopyCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@Old_HCM_COMP_ID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;

                cmd.Parameters.Add(new SqlParameter("@IsManual", SqlDbType.VarChar)).Value = isManual;
                cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = "COMPANY";
                cmd.Parameters.Add(new SqlParameter("@HCM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                cmd.ExecuteNonQuery();
                con.Close();

                txtCompCode.Text = "";
                hidActualCompanyId.Value = "";
                drpCatType.Items.Clear();
                drpCatType.Items.Insert(0, "--- Select ---");
                drpCatType.Items[0].Value = "0";
                Clear();
                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnCopy_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnBalanceHistory_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowBalanceHistoryPDF('" + txtCompCode.Text.Trim() + "');", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnBalanceHistory_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCompCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Company Code";

                    goto FunEnd;
                }

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;


                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompanyMasterAdd";
                cmd.Parameters.Add(new SqlParameter("@HCM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_ID", SqlDbType.VarChar)).Value = txtCompCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_NAME", SqlDbType.VarChar)).Value = txtCompName.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_BILL_CODE", SqlDbType.VarChar)).Value = txtBillToCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_ABRVTN", SqlDbType.VarChar)).Value = txtAbbrev.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_TYPE", SqlDbType.Char)).Value = drpCompanyType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCM_PAYMENT_TYPE", SqlDbType.Char)).Value = drpPayType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCM_ADDR", SqlDbType.VarChar)).Value = txtAddress.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_POBOX", SqlDbType.VarChar)).Value = txtPOBox.Text.Trim();

                if (drpCity.SelectedIndex != 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_CITY", SqlDbType.VarChar)).Value = drpCity.SelectedValue;
                }

                if (drpCountry.SelectedIndex != 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_COUNTRY", SqlDbType.VarChar)).Value = drpCountry.SelectedValue;
                }

                cmd.Parameters.Add(new SqlParameter("@HCM_CONTACT_PERSON", SqlDbType.VarChar)).Value = txtContactPer.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_CONTACT_NO", SqlDbType.VarChar)).Value = txtContactPer.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_PHONE1", SqlDbType.VarChar)).Value = txtPhone1.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_PHONE2", SqlDbType.VarChar)).Value = txtPhone2.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_FAX", SqlDbType.VarChar)).Value = txtFax.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_WEB", SqlDbType.VarChar)).Value = txtWeb.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_CONTRACT_REFNO", SqlDbType.VarChar)).Value = txtContractRefNo.Text.Trim();

                if (txtSignedDate.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_CONTRACT_SIGDATE", SqlDbType.VarChar)).Value = txtSignedDate.Text.Trim();
                }


                if (txtContStDate.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_CONTRACT_FROM", SqlDbType.VarChar)).Value = txtContStDate.Text.Trim();
                }


                if (txtContEnDate.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_CONTRACT_TO", SqlDbType.VarChar)).Value = txtContEnDate.Text.Trim();
                }


                //if (drpContractType.SelectedIndex != 0)
                //{
                //    cmd.Parameters.Add(new SqlParameter("@HCM_AGREEMENT_TYPE", SqlDbType.Char)).Value = drpContractType.SelectedValue;
                //}

                if (txtOpBal.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_OPENBAL", SqlDbType.Decimal)).Value = txtOpBal.Text.Trim();
                }


                if (txtOpBalDate.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_OPENBAL_DT", SqlDbType.VarChar)).Value = txtOpBalDate.Text.Trim();
                }



                if (hidCmpOPBalPaid.Value != "")
                {
                    Decimal decOldpaidamt = 0;
                    decOldpaidamt = decOldpaidamt + Convert.ToDecimal(hidCmpOPBalPaid.Value);
                    cmd.Parameters.Add(new SqlParameter("@HCM_OPENBAL_PAID", SqlDbType.Decimal)).Value = decOldpaidamt;
                }

                if (txtBalance.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_BALANCE_DUE", SqlDbType.Decimal)).Value = txtBalance.Text.Trim();
                }

                if (txtCreditLimit.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_CREDITLIMIT_AMT", SqlDbType.Decimal)).Value = txtCreditLimit.Text.Trim();
                }

                if (txtPackage.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_FIELD1", SqlDbType.VarChar)).Value = txtPackage.Text.Trim();
                }
                
                if (txtClaimFrmName.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_FIELD2", SqlDbType.VarChar)).Value = txtClaimFrmName.Text.Trim();
                }
                if (txtRefCode.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_FIELD3", SqlDbType.VarChar)).Value = txtRefCode.Text.Trim();
                }

                if (txtHaadCode.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_FIELD5", SqlDbType.VarChar)).Value = txtHaadCode.Text.Trim();
                }


                if (txtAccountId.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_ACCOUNT_ID", SqlDbType.VarChar)).Value = txtAccountId.Text.Trim();
                }

                if (txtInvoiceAmt.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_INVOICE_AMT", SqlDbType.Decimal)).Value = txtInvoiceAmt.Text.Trim();
                }

                if (txtReceivedAmt.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_RECIEVED_AMT", SqlDbType.Decimal)).Value = txtReceivedAmt.Text.Trim();
                }

                if (txtRejectAmt.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_REJECTED_AMT", SqlDbType.Decimal)).Value = txtRejectAmt.Text.Trim();
                }


                if (hidAccDiscount.Value != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_DISCOUNT", SqlDbType.Decimal)).Value = hidAccDiscount.Value;
                }


                if (chkStatus.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_STATUS", SqlDbType.Char)).Value = "A";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_STATUS", SqlDbType.Char)).Value = "I";
                }



                if (txtPreAppAmt.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_PRE_APRVL_AMT", SqlDbType.Decimal)).Value = txtPreAppAmt.Text.Trim();
                }

                if (txtRevisitDays.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_REVISIT_DAYS", SqlDbType.SmallInt)).Value = txtRevisitDays.Text.Trim();
                }



                if (txtReminder.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_REM_RCPTN", SqlDbType.VarChar)).Value = txtReminder.Text.Trim();
                }


                if (txtReportName.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_REPORT_NAME", SqlDbType.VarChar)).Value = drpReportName.SelectedValue;
                }



                if (chkMinistryService.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_MINISTRY_SERVICES", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_MINISTRY_SERVICES", SqlDbType.Char)).Value = "N";
                }


                cmd.Parameters.Add(new SqlParameter("@HCM_PACKAGE_DETAILS", SqlDbType.VarChar)).Value = GetSelectedPackage();


                if (txtMOH.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_PHY_REFCODE", SqlDbType.VarChar)).Value = txtMOH.Text.Trim();
                }

                if (txtPayerId.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_PAYERID", SqlDbType.VarChar)).Value = txtPayerId.Text.Trim();
                }




                if (chkUpdateCodeType.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_UPDATECODETYPE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_UPDATECODETYPE", SqlDbType.Char)).Value = "N";
                }

                if (chkValidateEClaim.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_VALIDATE_ECLAIM", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_VALIDATE_ECLAIM", SqlDbType.Char)).Value = "N";
                }
                string strTransId, strTransType = "";
                if (txtOpBal.Enabled == false)
                {
                    strTransId = "OPENPAID";
                    strTransType = "OpenBal Paid ";
                }
                else
                {
                    strTransId = "OPENBAL";
                    strTransType = "Opening Balance";

                }


                cmd.Parameters.Add(new SqlParameter("@HCB_TRANS_ID", SqlDbType.VarChar)).Value = strTransId;
                cmd.Parameters.Add(new SqlParameter("@HCB_TRANS_TYPE", SqlDbType.VarChar)).Value = strTransType;
                cmd.Parameters.Add(new SqlParameter("@IsManual", SqlDbType.VarChar)).Value = chkManual.Checked;
                cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = "COMPANY";
                cmd.Parameters.Add(new SqlParameter("@HCM_AGREEMENT_TYPE", SqlDbType.Char)).Value = drpContractType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);

                string strAccName = txtAccountName.Text.Trim();
                string AccCode = "", AccName = "";

                string[] arrAccName = strAccName.Split('~');
                if (arrAccName.Length > 0)
                {
                    AccCode = arrAccName[0];
                    if (arrAccName.Length > 1)
                    {
                        AccName = arrAccName[1];
                    }
                }

                cmd.Parameters.Add(new SqlParameter("@HCM_ACCOUNT_CODE", SqlDbType.VarChar)).Value = AccCode;
                cmd.Parameters.Add(new SqlParameter("@HCM_ACCOUNT_NAME", SqlDbType.VarChar)).Value = AccName;


                if (txtTrnNo.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCM_VAT_LIC_NO", SqlDbType.VarChar)).Value = txtTrnNo.Text.Trim();
                }

                SqlParameter returnValue = new SqlParameter("@ReturnCompanyId", SqlDbType.VarChar, 10);
                returnValue.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnValue);

                cmd.ExecuteNonQuery();
                string strReturnCompanyId;
                strReturnCompanyId = Convert.ToString(returnValue.Value);
                hidActualCompanyId.Value = strReturnCompanyId;
                con.Close();

                for (int i = 0; i <= lstSelectedCat.Items.Count - 1; i++)
                {
                    con.Open();
                    cmd = new SqlCommand();
                    cmd.Connection = con;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_CompBenefitsAdd";
                    cmd.Parameters.Add(new SqlParameter("@HCB_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HCB_COMP_ID", SqlDbType.VarChar)).Value = strReturnCompanyId;
                    cmd.Parameters.Add(new SqlParameter("@HCB_CAT_TYPE", SqlDbType.VarChar)).Value = lstSelectedCat.Items[i].Text;

                    cmd.ExecuteNonQuery();
                    con.Close();
                }


                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    AuditLogAdd("MODIFY", "Modify Existing entry in the Company Profile Master, Company ID is " + strReturnCompanyId);
                }
                else
                {
                    AuditLogAdd("ADD", "Add new entry in the Company Profile Master, Company ID is " + strReturnCompanyId);
                }


                CompanyMasterBAL objComp = new CompanyMasterBAL();
                //objComp.BranchID = Convert.ToString(Session["Branch_ID"]);
                //objComp.CompanyID = strReturnCompanyId;
                //objComp.FACTOR_ID = Convert.ToString(ViewState["FACTOR_ID"]);
                //objComp.HCPF_FROM_DATE = txtFactorFromDate.Text.Trim();
                //objComp.HCPF_TO_DATE = txtFactorToDate.Text.Trim();
                //objComp.UserID = Convert.ToString(Session["User_ID"]);
                //objComp.CompPriceFactorAdd();

                if (txtFactorFromDate.Text.Trim() != "" && txtFactorToDate.Text.Trim() != "")
                {
                    if (CheckPriceFactor() == true)
                    {
                        lblStatus.Text = "Price Factor Available";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                    con.Open();
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_CompPriceFactorAdd";
                    cmd.Parameters.Add(new SqlParameter("@BranchID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@CompanyID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;
                    cmd.Parameters.Add(new SqlParameter("@FACTOR_ID", SqlDbType.BigInt)).Value = Convert.ToString(ViewState["FACTOR_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HCPF_FROM_DATE", SqlDbType.VarChar)).Value = txtFactorFromDate.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HCPF_TO_DATE", SqlDbType.VarChar)).Value = txtFactorToDate.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                    SqlParameter returnFactorValue = new SqlParameter("@ReturnFactorId", SqlDbType.BigInt, 10);
                    returnFactorValue.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnFactorValue);

                    cmd.ExecuteNonQuery();
                    string strReturnFactorId;
                    strReturnFactorId = Convert.ToString(returnFactorValue.Value);

                    con.Close();

                    foreach (GridViewRow Row in gvPriceFactorDtls.Rows)
                    {
                        Label lblCatID = (Label)Row.FindControl("lblCatID");
                        TextBox txtFactor = (TextBox)Row.FindControl("txtFactor");

                        objComp.CompanyID = strReturnCompanyId;
                        objComp.FACTOR_ID = strReturnFactorId;// Convert.ToString(ViewState["FACTOR_ID"]);
                        objComp.HCPFD_CAT_ID = lblCatID.Text.Trim();
                        objComp.HCPFD_PRICE_FACTOR = txtFactor.Text.Trim();
                        objComp.CompPriceFactorDtlsAdd();
                    }

                }

                txtCompCode.Text = "";
                hidActualCompanyId.Value = "";
                drpCatType.Items.Clear();
                drpCatType.Items.Insert(0, "--- Select ---");
                drpCatType.Items[0].Value = "0";

                Clear();
                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS SAVED.',' Company Details Saved.','Green')", true);


            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        Boolean CheckPriceFactor()
        {


            string strFromDate = txtFactorFromDate.Text.Trim(); ;
            string[] arrFromDate = strFromDate.Split('/');

            string strRawFromDate = "";

            if (arrFromDate.Length > 1)
            {
                strRawFromDate = arrFromDate[2] + arrFromDate[1] + arrFromDate[0];
            }

            string strToDate = txtFactorToDate.Text.Trim(); ;
            string[] arrToDate = strToDate.Split('/');
            string strRawToDate = "";

            if (arrToDate.Length > 1)
            {
                strRawToDate = arrToDate[2] + arrToDate[1] + arrToDate[0];
            }

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCPF_COMP_ID='" + txtCompCode.Text.Trim() + "'";

            if (Convert.ToString(ViewState["FACTOR_ID"]) != "0")
            {
                Criteria += " AND HCPF_FACTOR_ID  != " + Convert.ToString(ViewState["FACTOR_ID"]);
            }

            Criteria += "  AND convert(varchar,HCPF_FROM_DATE,112)  >=  " + strRawFromDate.Trim();
            Criteria += " AND convert(varchar,HCPF_TO_DATE,112) <=  " + strRawToDate.Trim();


            DS = objCom.fnGetFieldValue("*", "HMS_COMP_PRICE_FACTOR", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;

            }



            return false;
        }
     
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtCompCode.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "COMPANY");
            hidActualCompanyId.Value = "";
            drpCatType.Items.Clear();
            drpCatType.Items.Insert(0, "--- Select ---");
            drpCatType.Items[0].Value = "0";

            Clear();
        }

        protected void btnCopyAgreement_Click(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND HCM_COMP_ID = '" + hidCopyCompanyId.Value + "'";
                DataSet DS = new DataSet();
                dboperations dbo = new dboperations();
                DS = dbo.retun_inccmp_details(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Company code should unique";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Criteria = " 1=1 AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND HCM_COMP_ID = '" + hidActualCompanyId.Value + "'";
                DS = new DataSet();
                DS = dbo.retun_inccmp_details(Criteria);
                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = "Data not available.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }




                Boolean isManual = false;
                if (hidCopyCompanyId.Value != hidNextCompanyId.Value)
                {
                    isManual = true;
                }
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompanyMasterCopy";
                cmd.Parameters.Add(new SqlParameter("@HCM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_ID", SqlDbType.VarChar)).Value = hidCopyCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@HCM_NAME", SqlDbType.VarChar)).Value = hidCopyCompanyName.Value;
                cmd.Parameters.Add(new SqlParameter("@HCM_BILL_CODE", SqlDbType.VarChar)).Value = hidCopyCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@Old_HCM_COMP_ID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;

                cmd.Parameters.Add(new SqlParameter("@IsManual", SqlDbType.VarChar)).Value = isManual;
                cmd.Parameters.Add(new SqlParameter("@HIN_SCRN_ID", SqlDbType.VarChar)).Value = "COMPANY";
                cmd.Parameters.Add(new SqlParameter("@HCM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                cmd.ExecuteNonQuery();
                con.Close();

                txtCompCode.Text = "";
                hidActualCompanyId.Value = "";
                drpCatType.Items.Clear();
                drpCatType.Items.Insert(0, "--- Select ---");
                drpCatType.Items[0].Value = "0";
                Clear();
                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnCopyAgreement_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void drpReportName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCompanyStatementTxtbox();
        }

        protected void btnDetailrpt_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowCompDetailsPDF('" + txtReportName.Text.Trim() + "','" + txtCompCode.Text.Trim() + "');", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnDetailrpt_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnSummary_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowCompDetailsPDF('" + txtReportSummary.Text.Trim() + "','" + txtCompCode.Text.Trim() + "');", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnSummary_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void gvHCB_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drpCategoryType, drpGVCoInsType, drpGVDedType;
                Label lblCatType, lblCoInsType, lblDedType;

                drpCategoryType = (DropDownList)e.Row.Cells[0].FindControl("drpCategoryType");
                drpGVCoInsType = (DropDownList)e.Row.Cells[0].FindControl("drpGVCoInsType");
                drpGVDedType = (DropDownList)e.Row.Cells[0].FindControl("drpGVDedType");

                lblCatType = (Label)e.Row.Cells[0].FindControl("lblCatType");
                lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");
                lblDedType = (Label)e.Row.Cells[0].FindControl("lblDedType");


                drpCategoryType.SelectedValue = lblCatType.Text;
                drpGVCoInsType.SelectedValue = lblCoInsType.Text;
                drpGVDedType.SelectedValue = lblDedType.Text;


            }
        }

        protected void btnBenefitSave_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompBenefitsModify";
                cmd.Parameters.Add(new SqlParameter("@HCB_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HCB_COMP_ID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@HCB_CAT_TYPE", SqlDbType.VarChar)).Value = drpCatType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCB_HOSP_DISC_TYPE", SqlDbType.Char)).Value = drpHospDiscType.SelectedValue;
                if (txtHospDiscAmt.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_HOSP_DISC_AMT", SqlDbType.Decimal)).Value = txtHospDiscAmt.Text.Trim();
                }
                if (txtDeductible.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE", SqlDbType.Decimal)).Value = txtDeductible.Text.Trim();
                }
                cmd.Parameters.Add(new SqlParameter("@HCB_COINS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;
                if (txtCoIns.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINS_AMT", SqlDbType.Decimal)).Value = txtCoIns.Text.Trim();
                }
                cmd.Parameters.Add(new SqlParameter("@HCB_TREATMENT_TYPE", SqlDbType.VarChar)).Value = drpTreatmentType.SelectedValue;

                if (ChkConsultation.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_CONSLT_INCL_HOSP_DISC", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_CONSLT_INCL_HOSP_DISC", SqlDbType.Char)).Value = "N";
                }

                if (ChkDedMaxConsAmt.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE_MAX_CONSLT_AMT", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE_MAX_CONSLT_AMT", SqlDbType.Char)).Value = "N";
                }

                cmd.Parameters.Add(new SqlParameter("@HCB_REMARKS", SqlDbType.VarChar)).Value = txtRemarks.Text.Trim();


                cmd.Parameters.Add(new SqlParameter("@HCB_BENEFITS_TYPE", SqlDbType.Char)).Value = drpBenefitsType.SelectedValue;

                if (ChkDedCoIns.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_COINS_APPLICABLE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_COINS_APPLICABLE", SqlDbType.Char)).Value = "N";
                }


                if (ChkCoInsFromGrossAmt.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINS_FROM_GROSSAMT", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINS_FROM_GROSSAMT", SqlDbType.Char)).Value = "N";
                }

                cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE_TYPE", SqlDbType.Char)).Value = drpDedType.SelectedValue;

                if (ChkSplCodeNeeded.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_SPL_CODE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_SPL_CODE", SqlDbType.Char)).Value = "N";
                }

                cmd.Parameters.Add(new SqlParameter("@HCB_PHY_DISC_TYPE", SqlDbType.Char)).Value = drpPharType.SelectedValue;
                if (txtPharDisc.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PHY_DISC_AMT", SqlDbType.Decimal)).Value = txtPharDisc.Text.Trim();
                }
                cmd.Parameters.Add(new SqlParameter("HCB_PHY_COINS_TYPE", SqlDbType.Char)).Value = drpPharCoInsType.SelectedValue;
                if (txtPharCoInsDisc.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PHY_COINS_AMT", SqlDbType.Decimal)).Value = txtPharCoInsDisc.Text.Trim();
                }
                if (TxtMaxDed.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_MAX_DED", SqlDbType.Decimal)).Value = TxtMaxDed.Text.Trim();
                }
                if (TxtMaxCoIns.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_MAX_COINS", SqlDbType.Decimal)).Value = TxtMaxCoIns.Text.Trim();
                }
                if (TxtMaxPhy.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_MAX_PHY", SqlDbType.Decimal)).Value = TxtMaxPhy.Text.Trim();
                }

                if (ChkCoInsFromGrossandClaimfromNetAmt.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINSFROMGROSS_CLAIMFROMNET", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINSFROMGROSS_CLAIMFROMNET", SqlDbType.Char)).Value = "N";
                }

                if (ChkDedBenefitsType.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_INSURANCE_TYPE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_INSURANCE_TYPE", SqlDbType.Char)).Value = "N";
                }

                if (txtPriceFactor.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PRICE_FACTOR", SqlDbType.Decimal)).Value = txtPriceFactor.Text.Trim();
                }
                if (txtPerDayPTLimit.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PERDAYLIMIT", SqlDbType.Decimal)).Value = txtPerDayPTLimit.Text.Trim();
                }

                int k = cmd.Parameters.Count;
                for (int i = 0; i < k; i++)
                {
                    if (cmd.Parameters[i].Value == "")
                    {
                        cmd.Parameters[i].Value = DBNull.Value;
                    }
                }


                cmd.ExecuteNonQuery();
                con.Close();


                for (int index = 0; index <= gvHCB.Rows.Count - 1; index++)
                {
                    Label lblCatId, lblCat;
                    lblCatId = (Label)gvHCB.Rows[index].Cells[0].FindControl("lblCatId");
                    lblCat = (Label)gvHCB.Rows[index].Cells[0].FindControl("lblCat");

                    DropDownList drpCategoryType, drpGVCoInsType, drpGVDedType;
                    drpCategoryType = (DropDownList)gvHCB.Rows[index].Cells[0].FindControl("drpCategoryType");
                    drpGVCoInsType = (DropDownList)gvHCB.Rows[index].Cells[0].FindControl("drpGVCoInsType");
                    drpGVDedType = (DropDownList)gvHCB.Rows[index].Cells[0].FindControl("drpGVDedType");


                    TextBox txtLimit, txtCoInsAmt, txtDedAmt;
                    txtLimit = (TextBox)gvHCB.Rows[index].Cells[0].FindControl("txtLimit");
                    txtCoInsAmt = (TextBox)gvHCB.Rows[index].Cells[0].FindControl("txtCoInsAmt");
                    txtDedAmt = (TextBox)gvHCB.Rows[index].Cells[0].FindControl("txtDedAmt");

                    SqlConnection con1 = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                    con1.Open();
                    cmd = new SqlCommand();
                    cmd.Connection = con1;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_CompBenefitsDtlsAdd";
                    cmd.Parameters.Add(new SqlParameter("@HCBD_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HCBD_COMP_ID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CAT_TYPE", SqlDbType.VarChar)).Value = drpCatType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CATEGORY_ID", SqlDbType.VarChar)).Value = lblCatId.Text;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CATEGORY", SqlDbType.VarChar)).Value = lblCat.Text;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CATEGORY_TYPE", SqlDbType.NVarChar)).Value = drpCategoryType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_LIMIT", SqlDbType.Decimal)).Value = txtLimit.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CO_INS_TYPE", SqlDbType.Char)).Value = drpGVCoInsType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CO_INS_AMT", SqlDbType.Decimal)).Value = txtCoInsAmt.Text;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_DED_TYPE", SqlDbType.Char)).Value = drpGVDedType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_DED_AMT", SqlDbType.Decimal)).Value = txtDedAmt.Text.Trim();


                    cmd.ExecuteNonQuery();
                    con1.Close();


                }
                if (drpCatType.Items.Count > 0)
                {
                    drpCatType.SelectedIndex = 0;
                }
                ClearBenefit();

                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnBenefitSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnBenefitClear_Click(object sender, EventArgs e)
        {

            ClearBenefit();
        }

        protected void btnBenefitDelete_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompBenefitsDelete";
                cmd.Parameters.Add(new SqlParameter("@HCB_COMP_ID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@HCB_CAT_TYPE", SqlDbType.VarChar)).Value = drpCatType.SelectedValue;

                cmd.ExecuteNonQuery();
                con.Close();

                ClearBenefit();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnBenefitDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnCatAdd_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i <= lstSelectedCat.Items.Count - 1; i++)
                {
                    if (lstSelectedCat.Items[i].Value == lstInsCategory.SelectedItem.Text)
                    {
                        goto FunEnd;
                    }

                }

                if (lstSelectedCat.Items.Count <= 0)
                {
                    lstSelectedCat.Items.Insert(0, lstInsCategory.SelectedItem.Text);
                    lstSelectedCat.Items[0].Value = lstInsCategory.SelectedItem.Text;
                }
                else
                {
                    Int32 index = lstSelectedCat.Items.Count;
                    lstSelectedCat.Items.Insert(index, lstInsCategory.SelectedItem.Text);
                    lstSelectedCat.Items[index].Value = lstInsCategory.SelectedItem.Text;
                }
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnCatAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnCatRemove_Click(object sender, EventArgs e)
        {
            try
            {

                string Criteria = "1=1";

                Criteria += " AND HCBD_COMP_ID = '" + txtCompCode.Text.Trim() + "'";
                Criteria += " AND HCBD_CAT_TYPE = '" + drpCatType.SelectedValue + "'";
                DS = dbo.CompBenefitsDtlsGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    goto FunEnd;
                }
                else
                {
                    Criteria = "1=1";
                    Criteria += " AND HCB_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria += " AND HCB_COMP_ID = '" + txtCompCode.Text.Trim() + "'";
                    Criteria += " AND HCB_CAT_TYPE = '" + drpCatType.SelectedValue + "'";
                    dbo.CompBenefitsDelete(Criteria);
                    lstSelectedCat.Items.Remove(lstSelectedCat.SelectedValue);



                }


            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnCatRemove_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (hidActualCompanyId.Value == "")
                {
                    goto FunEnd;
                }
                string Criteria = " 1=1 ";

                Criteria += " AND HIM_SUB_INS_CODE='" + hidActualCompanyId.Value + "'";

                DS = new DataSet();

                DS = dbo.InvoiceMasterGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This record is committed to other data. So you cannot delete this record";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;


                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompanyMasterDelete";
                cmd.Parameters.Add(new SqlParameter("@HCM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_ID", SqlDbType.VarChar)).Value = hidActualCompanyId.Value;
                cmd.Parameters.Add(new SqlParameter("@HCM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);



                cmd.ExecuteNonQuery();
                con.Close();


                AuditLogAdd("DELETE", "Delete the Company Profile , Company ID is " + hidActualCompanyId.Value);

                txtCompCode.Text = "";
                hidActualCompanyId.Value = "";
                Clear();
                lblStatus.Text = "Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void drpBenefitsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindBenefitsGridBasedFrom_InsTrmtType();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPerDayPTLimit.Text.Trim() == "")
                {
                    txtPerDayPTLimit.Text = "0";
                }

                dbo.UpdatePerDayLimit(Convert.ToDecimal(txtPerDayPTLimit.Text.Trim()), drpCatType.SelectedValue, hidActualCompanyId.Value);
                lblStatus.Text = "Data Updated";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Updated";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }


        protected void SelectFactor_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["VisitSelectIndex"] = gvScanCard.RowIndex;



                Label lblFactorID = (Label)gvScanCard.Cells[0].FindControl("lblFactorID");
                Label lblFromDate = (Label)gvScanCard.Cells[0].FindControl("lblFromDate");
                Label lblToDate = (Label)gvScanCard.Cells[0].FindControl("lblToDate");

                txtFactorFromDate.Text = lblFromDate.Text.Trim();
                txtFactorToDate.Text = lblToDate.Text.Trim();
                ViewState["FACTOR_ID"] = lblFactorID.Text;

                foreach (GridViewRow Row in gvPriceFactorDtls.Rows)
                {
                    Label lblCatID = (Label)Row.FindControl("lblCatID");
                    TextBox txtFactor = (TextBox)Row.FindControl("txtFactor");



                    CommonBAL objCom = new CommonBAL();
                    DataSet DS = new DataSet();
                    string Criteria = "HCPFD_FACTOR_ID=" + lblFactorID.Text + " AND HCPFD_CAT_ID='" + lblCatID.Text + "'";
                    DS = objCom.fnGetFieldValue("*", "HMS_COMP_PRICE_FACTOR_DETAILS", Criteria, "");

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        txtFactor.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCPFD_PRICE_FACTOR"]);
                    }
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.SelectFactor_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void CopyFactor_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblFactorID = (Label)gvScanCard.Cells[0].FindControl("lblFactorID");
                Label lblFromDate = (Label)gvScanCard.Cells[0].FindControl("lblFromDate");
                Label lblToDate = (Label)gvScanCard.Cells[0].FindControl("lblToDate");



                foreach (GridViewRow Row in gvPriceFactorDtls.Rows)
                {
                    Label lblCatID = (Label)Row.FindControl("lblCatID");
                    TextBox txtFactor = (TextBox)Row.FindControl("txtFactor");

                    CommonBAL objCom = new CommonBAL();
                    DataSet DS = new DataSet();
                    string Criteria = "HCPFD_FACTOR_ID=" + lblFactorID.Text + " AND HCPFD_CAT_ID='" + lblCatID.Text + "'";
                    DS = objCom.fnGetFieldValue("*", "HMS_COMP_PRICE_FACTOR_DETAILS", Criteria, "");

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        txtFactor.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCPFD_PRICE_FACTOR"]);
                    }
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.SelectFactor_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        #endregion
    }
}