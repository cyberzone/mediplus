﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using Mediplus_BAL;
namespace Mediplus.HMS.Masters
{
    public partial class AgreementService : System.Web.UI.Page
    {
        # region Variable Declaration
        static string strSessionBranchId;

        DataSet DS = new DataSet();
        dboperations dbo = new dboperations();


        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindServices()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HAS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND  HAS_COMP_ID='" + txtCompanyId.Text.Trim() + "'";

            if (drpCategory.SelectedIndex != 0)
            {

                Criteria += " AND  HAS_CATEGORY_ID='" + drpCategory.SelectedValue + "'";
            }


            if (txtServName.Text.Trim() != "")
            {
                if (chkAnySearch.Checked == false)
                {
                    Criteria += " AND HAS_SERV_NAME LIKE '" + txtServName.Text.Trim() + "%' ";
                }
                else
                {
                    Criteria += " AND HAS_SERV_NAME LIKE '%" + txtServName.Text.Trim() + "%'";
                }

            }


            DS = new DataSet();
            DS = dbo.AgrementServiceGet(Criteria);

            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["ServiceSortOrder"]);
            //SORTING CODING - END

            lblTotal.Text = "0";
            gvServices.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvServices.Visible = true;
                gvServices.DataSource = DV;
                gvServices.DataBind();
                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {
                gvServices.DataBind();
            }


        }

        void BindCategory()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSCM_STATUS='A' AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.ServiceCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "HSCM_NAME";
                drpCategory.DataValueField = "HSCM_CAT_ID";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- All ---");
            drpCategory.Items[0].Value = "0";

        }

        void CheckAgreementType()
        {
            string Criteria = " 1=1 AND  HCM_AGREEMENT_TYPE = 'C' AND  HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HCM_COMP_ID = '" + txtCompanyId.Text.Trim() + "' ";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_inccmp_details(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                hidAgreementCategory.Value = "true";

            }

        }

        void UpdateAgreementType()
        {
            dbo.AgrementCategoryDelete(txtCompanyId.Text.Trim());
            dbo.UpdateCompAgrmType(txtCompanyId.Text.Trim(), Convert.ToString(Session["Branch_ID"]), "S");

        }
        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_NAME"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                try
                {
                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    ViewState["CompanyId"] = Request.QueryString["CompanyId"].ToString();
                    ViewState["CompanyName"] = Request.QueryString["CompanyName"].ToString();

                    txtCompanyId.Text = ViewState["CompanyId"].ToString();
                    txtCompanyName.Text = ViewState["CompanyName"].ToString();

                    BindCategory();

                    CheckAgreementType();



                    ViewState["ServiceSortOrder"] = "HAS_CATEGORY_ID,HAS_SERV_NAME Asc";
                    if (hidAgreementCategory.Value != "true")
                    {
                        BindServices();
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      AgreementService.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindServices();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementService.drpCategory_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvServices.PageIndex = e.NewPageIndex;
            BindServices();
        }

        protected void gvServices_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drpDisctype, drpCoveringType;
                Label lblDiscType, lblCoveringType;
                drpDisctype = (DropDownList)e.Row.Cells[0].FindControl("drpDisctype");
                drpCoveringType = (DropDownList)e.Row.Cells[0].FindControl("drpCoveringType");

                lblDiscType = (Label)e.Row.Cells[0].FindControl("lblDiscType");
                lblCoveringType = (Label)e.Row.Cells[0].FindControl("lblCoveringType");

                drpDisctype.SelectedValue = lblDiscType.Text;
                drpCoveringType.SelectedValue = lblCoveringType.Text;
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            BindServices();
        }

        protected void drpSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpSort.SelectedIndex == 1)
            {

                ViewState["ServiceSortOrder"] = "HAS_SERV_NAME Asc";
            }
            else if (drpSort.SelectedIndex == 2)
            {
                ViewState["ServiceSortOrder"] = "HAS_COVERING_TYPE Asc";
            }
            else
            {
                ViewState["ServiceSortOrder"] = "HAS_CATEGORY_ID,HAS_SERV_NAME Asc";
            }
            BindServices();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                dbo.UpdateCoveringType(txtCompanyId.Text.Trim());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementService.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        //BELOW EVENT USED FOR INSERT THE AGREEMENT SERVICE FROM THE SERVICE MASTER
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {

                if (hidAgreementCategory.Value == "true")
                {
                    UpdateAgreementType();
                }

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

                
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_AgreementServiceAdd";
                cmd.Parameters.Add(new SqlParameter("@HAS_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HAS_COMP_ID", SqlDbType.VarChar)).Value = txtCompanyId.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAS_COMP_NAME", SqlDbType.VarChar)).Value = txtCompanyName.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@isRefServiceOnly", SqlDbType.VarChar)).Value = chkRefServOnly.Checked;


                cmd.ExecuteNonQuery();
               con.Close();

                BindServices();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementService.btnAddNew_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }



        protected void gvSave_Click(object sender, EventArgs e)
        {
            try
            {
          


                Button btnSave = new Button();
                btnSave = (Button)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnSave.Parent.Parent;

                Label lblServiceId;
                lblServiceId = (Label)gvScanCard.Cells[0].FindControl("lblServiceId");

                TextBox txtServiceName, txtCompFee, txtDiscount, txtRefNo, txtNetAmt;
                txtServiceName = (TextBox)gvScanCard.Cells[0].FindControl("txtServiceName");
                txtCompFee = (TextBox)gvScanCard.Cells[0].FindControl("txtCompFee");
                txtDiscount = (TextBox)gvScanCard.Cells[0].FindControl("txtDiscount");
                txtRefNo = (TextBox)gvScanCard.Cells[0].FindControl("txtRefNo");
                txtNetAmt = (TextBox)gvScanCard.Cells[0].FindControl("txtNetAmt");

                DropDownList drpDisctype, drpCoveringType;
                drpDisctype = (DropDownList)gvScanCard.Cells[0].FindControl("drpDisctype");
                drpCoveringType = (DropDownList)gvScanCard.Cells[0].FindControl("drpCoveringType");

                if (txtServiceName.Text.Trim() == "")
                {
                    lblStatus.Text = "Blank Service Description is not allowed.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtCompFee.Text.Trim() == "")
                {
                    lblStatus.Text = "Blank Company Fee is not allowed.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                Double decCompFee = 0.00, decDiscount = 0.00, decNetAmt = 0.00;

                if (txtCompFee.Text.Trim() != "")
                {
                    decCompFee = Convert.ToDouble(txtCompFee.Text.Trim());
                }
                if (txtDiscount.Text.Trim() != "")
                {
                    decDiscount = Convert.ToDouble(txtDiscount.Text.Trim());
                }
                if (drpDisctype.SelectedIndex == 0)
                {
                    decNetAmt = decCompFee - (decCompFee * decDiscount / 100);
                }
                else
                {
                    decNetAmt = decCompFee - decDiscount;
                }

                txtNetAmt.Text = decNetAmt.ToString("N2");
                txtDiscount.Text = decDiscount.ToString("N2");
                txtCompFee.Text = decCompFee.ToString("N2");


                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

              
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_AgreementServiceModify";
                cmd.Parameters.Add(new SqlParameter("@HAS_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HAS_COMP_ID", SqlDbType.VarChar)).Value = txtCompanyId.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAS_SERV_ID", SqlDbType.VarChar)).Value = lblServiceId.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAS_SERV_NAME", SqlDbType.VarChar)).Value = txtServiceName.Text.Trim();

                cmd.Parameters.Add(new SqlParameter("@HAS_COMP_FEE", SqlDbType.Decimal)).Value = decCompFee;
                cmd.Parameters.Add(new SqlParameter("@HAS_DISC_TYPE", SqlDbType.VarChar)).Value = drpDisctype.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAS_DISC_AMT", SqlDbType.Decimal)).Value = decDiscount;
                cmd.Parameters.Add(new SqlParameter("@HAS_NETAMOUNT", SqlDbType.Decimal)).Value = decNetAmt;
                cmd.Parameters.Add(new SqlParameter("@HAS_COVERING_TYPE", SqlDbType.VarChar)).Value = drpCoveringType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAS_REFNO", SqlDbType.VarChar)).Value = txtRefNo.Text.Trim();

                cmd.ExecuteNonQuery();
                 con.Close();

                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                // BindServices();

                FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementService.gvSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
        #endregion

    }
}