﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class AgreementCategory : System.Web.UI.Page
    {
        # region Variable Declaration
        static string strSessionBranchId;

        DataSet DS = new DataSet();
        dboperations dbo = new dboperations();


        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindAgrementCategoryGrid(bool DefaultValues)
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND HAC_COMP_ID = '" + txtCompanyId.Text.Trim() + "'";

            //Criteria += " AND HCBD_CAT_TYPE = '" + drpCatType.SelectedValue + "'";


            DS = dbo.AgrementCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0 && DefaultValues == false)
            {
                gvAgreementCat.Visible = true;
                gvAgreementCat.DataSource = DS;
                gvAgreementCat.DataBind();
            }
            else
            {
                DS = BindAgrementCategoryGridBasedFrom_ServiceCategoryMst();
                gvAgreementCat.Visible = true;
                gvAgreementCat.DataSource = DS;
                gvAgreementCat.DataBind();
            }





        }

        DataSet BindAgrementCategoryGridBasedFrom_ServiceCategoryMst()
        {

            DS = new DataSet();

            DataTable DT = new DataTable();


            DataColumn HAC_SERVICE_CAT_ID = new DataColumn();
            HAC_SERVICE_CAT_ID.ColumnName = "HAC_SERVICE_CAT_ID";

            DataColumn HAC_SERVICE_CAT_NAME = new DataColumn();
            HAC_SERVICE_CAT_NAME.ColumnName = "HAC_SERVICE_CAT_NAME";

            DataColumn HAC_COVERING_TYPE = new DataColumn();
            HAC_COVERING_TYPE.ColumnName = "HAC_COVERING_TYPE";

            DataColumn HAC_DISC_AMT = new DataColumn();
            HAC_DISC_AMT.ColumnName = "HAC_DISC_AMT";



            DataColumn HAC_DISC_TYPE = new DataColumn();
            HAC_DISC_TYPE.ColumnName = "HAC_DISC_TYPE";



            DT.Columns.Add(HAC_SERVICE_CAT_ID);
            DT.Columns.Add(HAC_SERVICE_CAT_NAME);
            DT.Columns.Add(HAC_COVERING_TYPE);
            DT.Columns.Add(HAC_DISC_AMT);
            DT.Columns.Add(HAC_DISC_TYPE);


            string Criteria = " 1=1 ";
            Criteria += " AND HSCM_STATUS='A' AND HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DataSet DS1 = new DataSet();
            DS1 = dbo.ServiceCategoryGet(Criteria);

            if (DS1.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                {
                    DataRow objrow;

                    objrow = DT.NewRow();
                    objrow["HAC_SERVICE_CAT_ID"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_CAT_ID"]);
                    objrow["HAC_SERVICE_CAT_NAME"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_NAME"]);
                    objrow["HAC_COVERING_TYPE"] = "Covered";
                    objrow["HAC_DISC_AMT"] = "0.00";
                    objrow["HAC_DISC_TYPE"] = "%";


                    DT.Rows.Add(objrow);
                }
            }

            DS.Tables.Add(DT);
            return DS;
        }


        void Clear()
        {

            gvAgreementCat.DataBind();
        }
        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_NAME"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                try
                {

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);


                    ViewState["CompanyId"] = Request.QueryString["CompanyId"].ToString();
                    ViewState["CompanyName"] = Request.QueryString["CompanyName"].ToString();


                    txtCompanyId.Text = ViewState["CompanyId"].ToString();

                    txtCompanyName.Text = ViewState["CompanyName"].ToString();


                    BindAgrementCategoryGrid(false);
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dbo.AgrementCategoryDelete(txtCompanyId.Text.Trim());

                for (int index = 0; index <= gvAgreementCat.Rows.Count - 1; index++)
                {

                    DropDownList drpDisctype, drpCoveringType;
                    Label lblCatId, lblCatName;
                    TextBox txtDiscount;


                    drpDisctype = (DropDownList)gvAgreementCat.Rows[index].Cells[0].FindControl("drpDisctype");
                    drpCoveringType = (DropDownList)gvAgreementCat.Rows[index].Cells[0].FindControl("drpCoveringType");

                    lblCatId = (Label)gvAgreementCat.Rows[index].Cells[0].FindControl("lblCatId");
                    lblCatName = (Label)gvAgreementCat.Rows[index].Cells[0].FindControl("lblCatName");

                    txtDiscount = (TextBox)gvAgreementCat.Rows[index].Cells[0].FindControl("txtDiscount");

                     
                    SqlCommand cmd;

                     SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                     con.Open();
                    cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_AgreementCategoryAdd";
                    cmd.Parameters.Add(new SqlParameter("@HAC_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HAC_COMP_ID", SqlDbType.VarChar)).Value = txtCompanyId.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HAC_COMP_NAME", SqlDbType.VarChar)).Value = txtCompanyName.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HAC_SERVICE_CAT_ID", SqlDbType.VarChar)).Value = lblCatId.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HAC_SERVICE_CAT_NAME", SqlDbType.VarChar)).Value = lblCatName.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HAC_DISC_TYPE", SqlDbType.Char)).Value = drpDisctype.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HAC_DISC_AMT", SqlDbType.Decimal)).Value = txtDiscount.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HAC_COVERING_TYPE", SqlDbType.Char)).Value = drpCoveringType.SelectedValue;

                    cmd.ExecuteNonQuery();
                   con.Close();


                }

                dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]), "AgreCat", "MASTER", "Inserting values in the HMS_AGRMT_CATEGORY Table", Convert.ToString(Session["User_ID"]));


                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementCategory.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 AND HAC_COMMIT_FLAG='Y' ";

                Criteria += " AND HAC_COMP_ID = '" + txtCompanyId.Text.Trim() + "'";
                DS = dbo.AgrementCategoryGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This record is committed to other data. So you cannot delete it.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                dbo.AgrementCategoryDelete(txtCompanyId.Text.Trim());
                dbo.AudittrailAdd(Convert.ToString(Session["Branch_ID"]), "AgreCat", "MASTER", "Deleting values from the HMS_AGRMT_CATEGORY Table", Convert.ToString(Session["User_ID"]));

                BindAgrementCategoryGrid(false);
                lblStatus.Text = "Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementCategory.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void gvAgreementCat_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList drpDisctype, drpCoveringType;
                    Label lblDiscType, lblCoveringType;
                    drpDisctype = (DropDownList)e.Row.Cells[0].FindControl("drpDisctype");
                    drpCoveringType = (DropDownList)e.Row.Cells[0].FindControl("drpCoveringType");

                    lblDiscType = (Label)e.Row.Cells[0].FindControl("lblDiscType");
                    lblCoveringType = (Label)e.Row.Cells[0].FindControl("lblCoveringType");

                    drpDisctype.SelectedValue = lblDiscType.Text;
                    drpCoveringType.SelectedValue = lblCoveringType.Text;

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementCategory.gvAgreementCat_RowDataBound");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void chkNewCategory_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkNewCategory.Checked == true)
                {
                    chkNewCategory.Enabled = false;

                    string Criteria = " 1=1 ";
                    Criteria += " AND HAC_COMP_ID = '" + txtCompanyId.Text.Trim() + "'";
                    DS = dbo.AgrementCategoryGet(Criteria);
                    if (DS.Tables[0].Rows.Count <= 0)
                    {
                        goto FunEnd;
                    }

                    DataTable DT = new DataTable();
                    DT = DS.Tables[0];

                    Criteria = " 1=1 ";
                    Criteria += " AND HSCM_STATUS='A'  AND HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria += " AND HSCM_CAT_ID NOT  IN (SELECT HAC_SERVICE_CAT_ID FROM HMS_AGRMT_CATEGORY WHERE HAC_COMP_ID = '" + txtCompanyId.Text.Trim() + "')";
                    DataSet DS1 = new DataSet();
                    DS1 = dbo.ServiceCategoryGet(Criteria);


                    if (DS1.Tables[0].Rows.Count > 0)
                    {

                        for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                        {
                            DataRow objrow;
                            objrow = DT.NewRow();
                            objrow["HAC_SERVICE_CAT_ID"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_CAT_ID"]);
                            objrow["HAC_SERVICE_CAT_NAME"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_NAME"]);
                            objrow["HAC_COVERING_TYPE"] = "Covered";
                            objrow["HAC_DISC_AMT"] = "0.00";
                            objrow["HAC_DISC_TYPE"] = "%";
                            DT.Rows.Add(objrow);
                        }


                        gvAgreementCat.Visible = true;
                        gvAgreementCat.DataSource = DT;
                        gvAgreementCat.DataBind();

                    }



                }

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AgreementCategory.gvAgreementCat_RowDataBound");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void chkDeftDiscount_CheckedChanged(object sender, EventArgs e)
        {

            BindAgrementCategoryGrid(chkDeftDiscount.Checked);
        }

        #endregion

    }
}