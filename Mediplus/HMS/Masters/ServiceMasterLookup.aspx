﻿<%@ page language="C#" autoeventwireup="true" codebehind="ServiceMasterLookup.aspx.cs" inherits="Mediplus.HMS.Masters.ServiceMasterLookup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script language="javascript" type="text/javascript">

        function passvalue(ServCode, ServName, HaadCode, Fees, CodeType) {
            var CtrlName = document.getElementById('hidCtrlName').value;
            window.opener.BindServiceDtls(ServCode, ServName, HaadCode, CtrlName, Fees, CodeType);
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
          <input type="hidden" id="hidCtrlName" runat="server" />
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
        <div>
            <div style="padding-top: 0px; padding-left: 0px; width: 99%; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table width="100%">
                    <tr>
                         <td class="label" style="width: 100px;">Search
                        </td>
                        <td>
                           <asp:TextBox ID="txtSearch" runat="server"  CssClass="label" Width="400px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>

                        </td>
                       
                        <td class="label">Category
                        </td>
                        <td>
                                    <asp:DropDownList ID="drpCategory" CssClass="label" runat="server" Width="300px" BorderWidth="1px" BorderColor="#cccccc"  AutoPostBack="true" OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                                    </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <table width="99%">
                <tr>
                    <td class="style2" align="right">
                        <asp:Label ID="Label9" runat="server" CssClass="label"
                            Text="Selected Records:"></asp:Label>
                        &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gvServices" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                EnableModelValidation="True" Width="99%" PageSize="200" OnPageIndexChanging="gvServices_PageIndexChanging">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HSM_SERV_ID")  %>','<%# Eval("HSM_NAME")%>','<%# Eval("HSM_HAAD_CODE")%>','<%# Eval("HSM_FEEDesc")%>' ,'<%# Eval("CodeTypeDesc")%>'  )">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HSM_SERV_ID") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Service Name">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HSM_SERV_ID")  %>','<%# Eval("HSM_NAME")%>','<%# Eval("HSM_HAAD_CODE")%>','<%# Eval("HSM_FEEDesc")%>','<%# Eval("CodeTypeDesc")%>' )">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HSM_NAME") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Haad Code">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HSM_SERV_ID")  %>','<%# Eval("HSM_NAME")%>','<%# Eval("HSM_HAAD_CODE")%>','<%# Eval("HSM_FEEDesc")%>','<%# Eval("CodeTypeDesc")%>' )">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("HSM_HAAD_CODE") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category Id">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HSM_SERV_ID")  %>','<%# Eval("HSM_NAME")%>','<%# Eval("HSM_HAAD_CODE")%>','<%# Eval("HSM_FEEDesc")%>','<%# Eval("CodeTypeDesc")%>' )">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HSM_CAT_ID") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fee">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HSM_SERV_ID")  %>','<%# Eval("HSM_NAME")%>','<%# Eval("HSM_HAAD_CODE")%>','<%# Eval("HSM_FEEDesc")%>','<%# Eval("CodeTypeDesc")%>' )">
                                <asp:Label ID="Label3" style="text-align:right;padding-right:5px;"   CssClass="label" runat="server" Text='<%# Bind("HSM_FEEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />
            </asp:GridView>

             <asp:GridView ID="gvAgrServices" runat="server" AutoGenerateColumns="False" AllowPaging="true" visible="false"
                EnableModelValidation="True" Width="99%" PageSize="200" OnPageIndexChanging="gvAgrServices_PageIndexChanging">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HAS_SERV_ID")  %>','<%# Eval("HAS_SERV_NAME")%>','<%# Eval("HAS_SERV_ID")%>','<%# Eval("HAS_COMP_FEE")%>')">
                                <asp:Label ID="Label4" CssClass="label" runat="server" Text='<%# Bind("HAS_SERV_ID") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Service Name">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HAS_SERV_ID")  %>','<%# Eval("HAS_SERV_NAME")%>','<%# Eval("HAS_SERV_ID")%>','<%# Eval("HAS_COMP_FEE")%>')">
                                <asp:Label ID="Label5" CssClass="label" runat="server" Text='<%# Bind("HAS_SERV_NAME") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Category Id">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HAS_SERV_ID")  %>','<%# Eval("HAS_SERV_NAME")%>','<%# Eval("HAS_SERV_ID")%>','<%# Eval("HAS_COMP_FEE")%>')">
                                <asp:Label ID="Label7" CssClass="label" runat="server" Text='<%# Bind("HAS_CATEGORY_ID") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fee">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HAS_SERV_ID")  %>','<%# Eval("HAS_SERV_NAME")%>','<%# Eval("HAS_SERV_ID")%>','<%# Eval("HAS_COMP_FEE")%>')">
                                <asp:Label ID="Label8" style="text-align:right;padding-right:5px;"   CssClass="label" runat="server" Text='<%# Bind("HAS_COMP_FEE") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>