﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Mediplus_BAL;
using System.IO;
namespace Mediplus.HMS.Masters
{
    public partial class Insurance_Zoom : System.Web.UI.Page
    {
        
        dboperations dbo = new dboperations();
        DataSet ds = new DataSet();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS='A' AND HCM_BRANCH_ID='" + Session["Branch_ID"] + "'";

            if (txtCompanyId.Text != "")
            {
                Criteria += " AND HCM_COMP_ID Like '%" + txtCompanyId.Text.Trim() + "%'";
            }

            if (txtCompanyName.Text != "")
            {
                Criteria += " AND HCM_NAME Like '%" + txtCompanyName.Text.Trim() + "%'";
            }

            if (drpType.SelectedIndex != 0)
            {
                Criteria += " AND HCM_COMP_TYPE = '" + drpType.SelectedValue + "'";
            }


            if (ViewState["Value"] != "")
            {
                if (Convert.ToString(ViewState["BillToCode"]) != "")
                {
                    Criteria += " AND HCM_BILL_CODE = '" + Convert.ToString(ViewState["BillToCode"]) + "'";
                }
            }

            ds = dbo.retun_inccmp_details(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            gvGridView.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }


        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["BillToCode"] = "";
                ViewState["PageName"] = Request.QueryString["PageName"].ToString();
                ViewState["Value"] = Request.QueryString["Value"].ToString();

              
                    if (ViewState["PageName"].ToString() == "CompanyId")
                    {
                        txtCompanyId.Text = ViewState["Value"].ToString();
                    }
                    else if (ViewState["PageName"].ToString() == "CompanyName")
                    {
                        txtCompanyName.Text = ViewState["Value"].ToString();

                    }

                    else if (ViewState["PageName"].ToString() == "BillToCode")
                    {
                        ViewState["BillToCode"] = ViewState["Value"].ToString();

                    }

                    ViewState["SortOrder"] = "HCM_COMP_ID ASC";
                    BindGrid();
               
            }


        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            ViewState["PageName"] = "";
            BindGrid();

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                Session["ErrorMsg"] = ex.Message;
                Response.Redirect("ErrorPage.aspx");
            }


        }
    }
}