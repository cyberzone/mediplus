﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Masters
{
    public partial class UserMaster : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindUsersGrid()
        {
            string Criteria = " 1=1  ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID <> 'SUPER_ADMIN'";

            DataSet ds = new DataSet();

            ds = objCom.fnGetFieldValue("*,CASE HUM_STATUS WHEN  'A' THEN 'Active' ELSE 'InActive' END AS HUM_STATUSDesc", "HMS_USER_MASTER", Criteria, "HUM_USER_NAME");
            if (ds.Tables[0].Rows.Count > 0)
            {

                gvUserList.DataSource = ds;
                gvUserList.DataBind();

            }



        }

        private bool CheckPasswordFormat(string passwordText)
        {

            int minimumLength = Convert.ToInt32(Session["PWD_MIN_LENGTH"]), maximumLength = Convert.ToInt32(Session["PWD_MAX_LENGTH"]),
                minimumNumbers = Convert.ToInt32(Session["PWD_NUM_MIN_LENGTH"]), minimumLetters = Convert.ToInt32(Session["PWD_LETTER_MIN_LENGTH"]),
                minimumSpecialCharacters = 1, minUpperCharacters = 1;

            //Assumes that special characters are anything except upper and lower case letters and digits
            //Assumes that ASCII is being used (not suitable for many languages)

            int letters = 0;
            int digits = 0;
            int specialCharacters = 0;
            int UpperCharacters = 0;

            //Make sure there are enough total characters
            if (passwordText.Length < minimumLength)
            {
                ShowError("You must have at least " + minimumLength + " characters in your password.");
                return false;
            }
            //Make sure there are enough total characters
            if (passwordText.Length > maximumLength)
            {
                ShowError("You must have no more than " + maximumLength + " characters in your password.");
                return false;
            }

            foreach (var ch in passwordText)
            {
                if (char.IsLetter(ch)) letters++; //increment letters
                if (char.IsDigit(ch)) digits++; //increment digits

                //Test for only letters and numbers...
                if (!((ch > 47 && ch < 58) || (ch > 64 && ch < 91) || (ch > 96 && ch < 123)))
                {
                    specialCharacters++;
                }

                if (Char.IsUpper(ch) == true)
                {
                    UpperCharacters++;
                }
            }
            //Make sure there are enough digits
            if (letters < minimumLetters)
            {
                ShowError("You must have at least " + minimumLetters + " letters in your password.");
                return false;
            }


            //Make sure there are enough digits
            if (digits < minimumNumbers)
            {
                ShowError("You must have at least " + minimumNumbers + " numbers in your password.");
                return false;
            }

            //Make sure there are enough special characters -- !(a-zA-Z0-9)
            if (specialCharacters < minimumSpecialCharacters)
            {
                ShowError("You must have at least " + minimumSpecialCharacters + " special characters (like @,$,%,#) in your password.");
                return false;
            }


            if (UpperCharacters < minUpperCharacters)
            {
                ShowError("You must have at least one Upper characters  in your password.");
                return false;
            }
            return true;
        }

        private void ShowError(string errorMessage)
        {
            //lblChangePwdStatus.Text = errorMessage;

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' User Master ',' " + errorMessage + "')", true);
        }

        void Clear()
        {
            txtUserID.Text = "";
            txtPassword.Text = "";
            txtConPwd.Text = "";
            txtUserName.Text = "";
            txtStaffID.Text = "";
            chkActive.Checked = true;

        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "USER";
            objCom.ScreenName = "User master";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }


            string Criteria = " 1=1 AND HRT_SCREEN_ID='USER' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Enabled = false;

                btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {


            }

            if (strPermission == "7")
            {

                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                AuditLogAdd("OPEN", "Open User Master Page");
                ViewState["NewFlag"] = true;

                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                BindUsersGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                CommonBAL objCom = new CommonBAL();
                string EncryPassword = "";

                string Criteria = " 1=1 ";
                string OldPwd = "";
                
                    Criteria += " AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "'";
                    ds = objCom.UserMasterGet(Criteria);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ViewState["NewFlag"] = false;
                        OldPwd = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_PASS"]);
                    }

                


                if (Convert.ToString(Session["PWD_PREV_NOTREPEAT"]) == "1")
                {
                    Criteria = " 1=1 ";
                    Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "'";

                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")//&& OldPwd.Length > 20
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtPassword.Text.Trim()));
                        Criteria += " AND (HUM_USER_PASS='" + EncryPassword + "' OR HUM_PREV_PASS1='" + EncryPassword + "' OR HUM_PREV_PASS2='" + EncryPassword + "')";
                    }
                    else
                    {
                        Criteria += " AND (HUM_USER_PASS='" + txtPassword.Text.Trim() + "' OR HUM_PREV_PASS1='" + txtPassword.Text.Trim() + "' OR HUM_PREV_PASS2='" + txtPassword.Text.Trim() + "')";
                    }


                    DataSet DS = new DataSet();
                    DS = objCom.UserMasterGet(Criteria);
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('User Master','Password should not match with previous two passwords')", true);

                        //lblChangePwdStatus.Text = "Password should not match with previous two passwords";
                        //lblChangePwdStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }

                }
                if (Convert.ToString(Session["PWD_FORMAT_REQUIRED"]) == "1")
                {

                    if (CheckPasswordFormat(txtPassword.Text) == false)
                    {
                        goto FunEnd;
                    }

                }




                EncryPassword = "";




                string FieldNameWithValues = "";

                EncryPassword = "";


                objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.UserID = txtUserID.Text.Trim();
                objCom.UserName = txtUserName.Text.Trim();

                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                {
                    EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtPassword.Text.Trim()));
                    objCom.Password = EncryPassword;
                }
                else
                {
                    objCom.Password = txtPassword.Text.Trim();
                }

                objCom.StaffID = txtStaffID.Text.Trim();

                string strStatus = "I";

                if (chkActive.Checked == true)
                {
                    strStatus = "A";
                }
                objCom.Status = strStatus;

                objCom.CreatedUser = Convert.ToString(Session["User_ID"]);

                objCom.UserMasterAdd();
                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    if (strStatus == "I")
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the User Master as InActive, User ID is " + txtUserID.Text.Trim());
                    }
                    else
                    {
                        AuditLogAdd("MODIFY", "Modify Existing entry in the User Master, User ID is " + txtUserID.Text.Trim());
                    }
                }
                else
                {
                    AuditLogAdd("ADD", "Add new entry in the User Master, User ID is " + txtUserID.Text.Trim());
                }


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('User Master','Password Changed Successfully')", true);

                Clear();
                BindUsersGrid();

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }


        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            BindUsersGrid();
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            Clear();
            TextFileWriting(System.DateTime.Now.ToString() + "Edit_Click Started");

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;


            Label lblUserID = (Label)gvScanCard.Cells[0].FindControl("lblUserID");
            Label lblUserName = (Label)gvScanCard.Cells[0].FindControl("lblUserName");
            Label lblPassword = (Label)gvScanCard.Cells[0].FindControl("lblPassword");
            Label lblStaffID = (Label)gvScanCard.Cells[0].FindControl("lblStaffID");
            Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");


            if (Convert.ToString(Session["User_ID"]).ToUpper() == "SUPER_ADMIN")
            {
                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1" )//&& lblPassword.Text.Length > 15
                {
                    txtPassword.Text = objCom.SimpleDecrypt(txtPassword.Text.Trim());

                }
                else
                {
                    txtPassword.Text = lblPassword.Text.Trim();
                }

            }
            txtUserID.Text = lblUserID.Text.Trim();
            txtUserName.Text = lblUserName.Text.Trim();
            txtStaffID.Text = lblStaffID.Text.Trim();

            if (lblStatus.Text == "A")
            {
                chkActive.Checked = true;
            }
            else
            {
                chkActive.Checked = false;
            }

        }
    }
}