﻿<%@ page language="C#" autoeventwireup="true" codebehind="CompanyCopyAgreement.aspx.cs" inherits="Mediplus.HMS.Masters.CompanyCopyAgreement" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />

    <title></title>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
   
    <script language="javascript" type="text/javascript">
        function CompanyShow(PageName) {
            var Value;

            var win = window.open("Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + Value, "newwin1", "top=200,left=100,height=500,width=950,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
            return false;

        }

        function CompIdSelected() {
            if (document.getElementById('<%=txtCompanyFrom.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyFrom.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyFrom.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyNameFrom.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyNameFrom.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyNameFrom.ClientID%>').value;
                var Data1 = Data.split('~');

                var Name = Data1[0];
                var Code = Data1[1];


                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompanyNameFrom.ClientID%>').value = Name.trim();
                    document.getElementById('<%=txtCompanyFrom.ClientID%>').value = Code.trim();


                }
            }

            return true;
        }


        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var strCompCode = document.getElementById('<%=txtCompanyFrom.ClientID%>').value
              if (/\S+/.test(strCompCode) == false) {
                  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Company Code";
                document.getElementById('<%=txtCompanyFrom.ClientID%>').focus;
                return false;
            }

          

            var strCompName = document.getElementById('<%=txtCompanyNameFrom.ClientID%>').value
              if (/\S+/.test(strCompName) == false) {
                  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter From Company Name";
                document.getElementById('<%=txtCompanyNameFrom.ClientID%>').focus;
                return false;
            }

            var strToCompCode = document.getElementById('<%=txtCompanyTo.ClientID%>').value
            if (/\S+/.test(strToCompCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Company Code";
                  document.getElementById('<%=txtCompanyTo.ClientID%>').focus;
                  return false;
              }

            var strToCompName = document.getElementById('<%=txtCompanyNameTo.ClientID%>').value
            if (/\S+/.test(strToCompName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter To Company Name";
                document.getElementById('<%=txtCompanyNameTo.ClientID%>').focus;
                return false;
            }

         if (strCompCode.trim() == strToCompCode.trim()) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Company From and Company To are same. Please select another company.";
             return false;
        }

          
            return true
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="500px">
                <tr>

                    <td>
                        <asp:updatepanel id="UpdatePanel0" runat="server">
                            <contenttemplate>
                     <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </contenttemplate>
                        </asp:updatepanel>
                    </td>
                </tr>
            </table>
            <fieldset style="width: 500px">
                <legend class="label">Copy From 
                </legend>

                <table style="width: 100%">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="label"
                                Text="Company Id:"></asp:Label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel7" runat="server">
                                <contenttemplate>
                            <asp:TextBox ID="txtCompanyFrom" runat="server" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="70px" MaxLength="10"   onblur="return CompIdSelected()" ></asp:TextBox>
                            <asp:TextBox ID="txtCompanyNameFrom" runat="server" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="270px" MaxLength="50"  onblur="return CompNameSelected()"  ></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompanyFrom" MinimumPrefixLength="1" ServiceMethod="GetCompany"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyNameFrom" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"></asp:AutoCompleteExtender>
                        </contenttemplate>

                            </asp:updatepanel>
                        </td>

                    </tr>
                </table>
            </fieldset>
            <fieldset style="width: 500px">
                <legend class="label">Copy To 
                </legend>

                <table style="width: 100%">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" CssClass="label"
                                Text="Company Id:"></asp:Label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
                            <asp:TextBox ID="txtCompanyTo" runat="server" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="70px" MaxLength="10" Readonly="true"  ></asp:TextBox>
                            <asp:TextBox ID="txtCompanyNameTo" runat="server" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="270px" MaxLength="50"  Readonly="true"   ></asp:TextBox>
                        </contenttemplate>

                            </asp:updatepanel>
                        </td>

                    </tr>
                </table>
            </fieldset>
            <table style="width: 500px">
                <tr>
                    <td>
                        <asp:CheckBox ID="chkCopyAgrement" runat="server" CssClass="label" Text="Copy Agrement" Checked="true" />
                    </td>

                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkCopyBenefits" runat="server" CssClass="label" Text="Copy Benefits and Exclusions" Checked="true" />
                    </td>

                </tr>
                <tr>
                    <td style="height: 20px;"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCopy" runat="server" CssClass="button orange small" Width="100px" OnClick="btnCopy_Click" OnClientClick="return SaveVal();" Text="Copy" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="button orange small" Width="100px" Text="Cancel" OnClientClick="window.close();" />
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
