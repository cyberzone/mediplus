﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="InitialNumber.aspx.cs" Inherits="Mediplus.HMS.Masters.InitialNumber" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strScreenCode = document.getElementById('<%=txtScreenCode.ClientID%>').value
            if (/\S+/.test(strScreenCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Screen Code";
                document.getElementById('<%=txtScreenCode.ClientID%>').focus;
                return false;
            }

            var strScreenName = document.getElementById('<%=txtScreenName.ClientID%>').value
            if (/\S+/.test(strScreenName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Screen Name";
                document.getElementById('<%=txtScreenName.ClientID%>').focus;
                return false;
            }



            return fal;
        }

        function DeleteVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strScreenCode = document.getElementById('<%=txtScreenCode.ClientID%>').value
            if (/\S+/.test(strScreenCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Screen Code";
                document.getElementById('<%=txtScreenCode.ClientID%>').focus;
                return false;
            }

              var isDelCompany = window.confirm('Do you want to delete Screen Information?');

              if (isDelCompany == true) {
                  return true
              }
              else {
                  return false;
              }
              return true
          }
   </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table >
        <tr>
            <td class="PageHeader"> Initial Number Settings
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
      <table width="100%" border="0">
          <tr>

              <td class="label" style="height:30PX;">
                  Screen Code
              </td>
                <td>
                     <asp:TextBox ID="txtScreenCode"  CssClass="label" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="Red"   AutoPostBack="True"   OnTextChanged="txtScreenCode_TextChanged" ></asp:TextBox>
              </td>
          </tr>
           <tr>

              <td class="label style="height:30PX;">
                  Screen Name
              </td>
                <td>
                     <asp:TextBox ID="txtScreenName"  CssClass="label" runat="server" Width="150px" MaxLength="50" BorderWidth="1px" BorderColor="Red"  ></asp:TextBox>
              </td>
          </tr>
            <tr>

              <td class="label" style="height:30PX;">
                  Prefix
              </td>
                <td>
                     <asp:TextBox ID="txtPrefix"  CssClass="label" runat="server" Width="150px" MaxLength="2" BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>
              </td>
          </tr>
            <tr>

              <td class="label" style="height:30PX;">
                 Last No.
              </td>
                <td>
                     <asp:TextBox ID="txtLastNo"  CssClass="label" runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
              </td>
          </tr>
        </table>
    <div runat="server" id="divTran" style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
  
    <asp:GridView ID="gvInitialNo" runat="server" AllowPaging="True" AutoGenerateColumns="False"  OnRowDataBound="gvInitialNo_RowDataBound"
        EnableModelValidation="True" Width="100%" PageSize="200" OnPageIndexChanging="gvInitialNo_PageIndexChanging">
        <HeaderStyle CssClass="GridHeader_Blue"  Font-Bold="true" />
        <RowStyle CssClass="GridRow" />
        <Columns>
               <asp:TemplateField HeaderText="No" SortExpression="HPV_SEQNO">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Select_Click" >
                                     <asp:Label ID="lblScreenId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HIN_SCRN_ID") %>' visible="false" ></asp:Label>
                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" style="text-align: right;" runat="server" Text='<%# Bind("HIN_SCRN_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

            <asp:TemplateField HeaderText="Code">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HIN_SCRN_ID") %>'>

                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click" > 
                    <asp:Label ID="lblScreenName" CssClass="GridRow"  runat="server"  Text='<%# Bind("HIN_SCRN_NAME") %>'></asp:Label>

                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="Prefix">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkFee" runat="server" OnClick="Select_Click" > 
                     <asp:Label ID="lblPrefix" CssClass="GridRow" runat="server"  Text='<%# Bind("HIN_PREFIX") %>'></asp:Label>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Last No." HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkCompFee" runat="server" OnClick="Select_Click" >
                         <asp:Label ID="lblLastNo" CssClass="GridRow" Width="100px" runat="server" style="text-align: right;padding-right:3px;" Text='<%# Bind("HIN_LASTNO") %>'></asp:Label>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
           
        </Columns>
          <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

    </asp:GridView>

    </div>
    <br />
        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
    <asp:Button ID="btnDelete" runat="server" CssClass="button orange small" Width="100px" OnClick="btnDelete_Click" OnClientClick="return DeleteVal();" Text="Delete" visible="true" />
    <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="100px" OnClick="btnClear_Click" Text="Clear" />


</asp:Content>
