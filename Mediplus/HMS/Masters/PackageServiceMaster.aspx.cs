﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class PackageServiceMaster : System.Web.UI.Page
    {


        dboperations dbo = new dboperations();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='PATIENTREG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?PageName=Registration");
            }
        }

        void BindPackageMaster()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPSm_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPSM_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*,Convert(varchar,HPSM_EFFECTIVE_DATE,103) as HPSM_EFFECTIVE_DATEDesc, Convert(varchar,HPSM_EXPIRY_DATE,103) as HPSM_EXPIRY_DATEDesc ", "HMS_PACKAGE_SERVICE_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtPackageName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPSM_PACKAGE_NAME"]);
                txtEffectiveDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPSM_EFFECTIVE_DATEDesc"]);
                txtExpiryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPSM_EXPIRY_DATEDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HPSM_PACK_SERV_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPSM_PACK_SERV_CODE"]) != "")
                {
                    string strPackSerCode = Convert.ToString(DS.Tables[0].Rows[0]["HPSM_PACK_SERV_CODE"]);
                    string strPackSerName = Convert.ToString(DS.Tables[0].Rows[0]["HPSM_PACK_SERV_DESC"]);
                    txtPackageServiceName.Text = strPackSerCode + "~" + strPackSerName;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPSM_STATUS"]) == "Active")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }


            }
        }

        void BindPackageMasterGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPSm_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            //Criteria += " AND HPSm_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*,Convert(varchar,HPSM_EFFECTIVE_DATE,103) as HPSM_EFFECTIVE_DATEDesc, Convert(varchar,HPSM_EXPIRY_DATE,103) as HPSM_EXPIRY_DATEDesc ", "HMS_PACKAGE_SERVICE_MASTER", Criteria, "HPSM_PACKAGE_ID Desc");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPackageMaster.DataSource = DS;
                gvPackageMaster.DataBind();
            }
            else
            {
                gvPackageMaster.DataBind();
            }
        }


        void BuildePackageTrans()
        {
            DataSet DS = new DataSet();
            string Criteria = "1=2";
            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*", "HMS_PACKAGE_SERVICE_TRANS", Criteria, "");

            ViewState["PackageServiceTrans"] = DS.Tables[0];
        }

        void BindPackageTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPST_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPST_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";

            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*", "HMS_PACKAGE_SERVICE_TRANS", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["PackageServiceTrans"] = DS.Tables[0];

            }
        }

        void BindTempPackageTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["PackageServiceTrans"];
            if (DT.Rows.Count > 0)
            {
                gvPackageTrans.DataSource = DT;
                gvPackageTrans.DataBind();

            }
            else
            {
                gvPackageTrans.DataBind();
            }

        }

        void Clear()
        {

            txtPackageName.Text = "";
            chkActive.Checked = true;
            txtPackageServiceName.Text = "";

            ViewState["gvServSelectIndex"] = "";

            BuildePackageTrans();
            BindTempPackageTrans();
        }

        #endregion

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            Criteria += " AND  HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }




            string[] Data1 = { "" };

            return Data1;
        }


      [System.Web.Services.WebMethod]
        public static string[] GetPackageServiceName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            Criteria += " AND  HSM_STATUS='A' AND HSM_PACKAGE_SERVICE=1 ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }




            string[] Data1 = { "" };

            return Data1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }



            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                txtPackageID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PAKSERVMAS");
                txtEffectiveDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtExpiryDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                BuildePackageTrans();
                //  BindPackageMasterGrid();

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtPackageID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PAKSERVMAS");
            Clear();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {


                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["PackageServiceTrans"];

                strTestCodeDesc = txtServiceName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Service Details.',' Please enter Service Details')", true);

                    goto FunEnd;
                }


                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                DataRow objrow;
                objrow = DT.NewRow();


                if (Convert.ToString(ViewState["gvServSelectIndex"]) != "")
                {
                    Int32 R = Convert.ToInt32(ViewState["gvServSelectIndex"]);


                    DT.Rows[R]["HPST_SERV_CODE"] = TestProfileID;
                    DT.Rows[R]["HPST_SERV_DESC"] = TestProfileDesc;
                    DT.Rows[R]["HPST_QTY"] = txtSessionCount.Text.Trim();



                    DT.AcceptChanges();
                }
                else
                {
                    objrow["HPST_PACKAGE_ID"] = txtPackageID.Text.Trim();
                    objrow["HPST_SERV_CODE"] = TestProfileID;
                    objrow["HPST_SERV_DESC"] = TestProfileDesc;
                    objrow["HPST_QTY"] = txtSessionCount.Text.Trim();




                    DT.Rows.Add(objrow);
                }



                ViewState["PackageServiceTrans"] = DT;


                BindTempPackageTrans();
                txtServiceName.Text = "";
                txtSessionCount.Text = "";

                ViewState["gvServSelectIndex"] = "";
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void PackageTransEdit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvPakTransServCode = (Label)gvScanCard.Cells[0].FindControl("lblgvPakTransServCode");
            Label lblgvPakTransServDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvPakTransServDesc");
            Label lblgvPakTransSessionCount = (Label)gvScanCard.Cells[0].FindControl("lblgvPakTransSessionCount");

            txtServiceName.Text = lblgvPakTransServCode.Text + "~" + lblgvPakTransServDesc.Text;

            txtSessionCount.Text = lblgvPakTransSessionCount.Text;
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvPackageTrans.Rows.Count == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Service Details.',' Please enter Service Details')", true);
                    goto FunEnd;
                }

                string strTestCodeDesc = "", TestProfileID = "", TestProfileDesc = "";
                strTestCodeDesc = txtPackageServiceName.Text;
                string[] arrTestCodeDesc = strTestCodeDesc.Split('~');

                if (arrTestCodeDesc.Length == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Package Service.',' Please enter Package Service ')", true);

                    goto FunEnd;
                }


                TestProfileID = arrTestCodeDesc[0];
                TestProfileDesc = arrTestCodeDesc[1];

                CustomerEnquiryMasterBAL objEnq = new CustomerEnquiryMasterBAL();
                objEnq.BranchID = Convert.ToString(Session["Branch_ID"]);
                objEnq.PackageID = txtPackageID.Text.Trim();
                objEnq.HPSM_PACKAGE_NAME = txtPackageName.Text.Trim();
                objEnq.HPSM_STATUS = chkActive.Checked == true ? "Active" : "InActive";
                objEnq.HPSM_EFFECTIVE_DATE = txtEffectiveDate.Text.Trim();
                objEnq.HPSM_EXPIRY_DATE = txtExpiryDate.Text.Trim();

                objEnq.HPSM_PACK_SERV_CODE = TestProfileID;
                objEnq.HPSM_PACK_SERV_DESC = TestProfileDesc;

                objEnq.PackageServiceMasterAdd();

                CommonBAL objCom = new CommonBAL();
                string Criteria = " HPST_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPST_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";
                objCom.fnDeleteTableData("HMS_PACKAGE_SERVICE_TRANS", Criteria);


                foreach (GridViewRow GR in gvPackageTrans.Rows)
                {
                    Label lblgvPakTransServCode = (Label)GR.FindControl("lblgvPakTransServCode");
                    Label lblgvPakTransServDesc = (Label)GR.FindControl("lblgvPakTransServDesc");
                    Label lblgvPakTransSessionCount = (Label)GR.FindControl("lblgvPakTransSessionCount");

                    objEnq.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objEnq.PackageID = txtPackageID.Text.Trim();
                    objEnq.HPST_SERV_CODE = lblgvPakTransServCode.Text;
                    objEnq.HPST_SERV_DESC = lblgvPakTransServDesc.Text;
                    objEnq.HPST_QTY = lblgvPakTransSessionCount.Text;
                    objEnq.PackageServiceTransAdd();
                }
                Clear();
                txtPackageID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PAKSERVMAS");
                BindPackageTrans();
                BindTempPackageTrans();
                BindPackageMaster();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  Details Saved.',' Package Details Saved.')", true);

            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PackageServiceMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtPackageID_TextChanged(object sender, EventArgs e)
        {
            Clear();
            BindPackageMaster();
            BindPackageTrans();
            BindTempPackageTrans();
        }


        protected void PackageMasEdit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
            Label lblgvPakMasPackageID = (Label)gvScanCard.Cells[0].FindControl("lblgvPakMasPackageID");

            txtPackageID.Text = lblgvPakMasPackageID.Text;

            Clear();
            BindPackageMaster();
            BindPackageTrans();
            BindTempPackageTrans();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "HidePackageDetailsPopup()", true);

        }

        protected void imgPackageMasterZoom_Click(object sender, ImageClickEventArgs e)
        {
            BindPackageMasterGrid();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PTRegistration", "ShowPackageDetailsPopup()", true);
        }

        protected void DeleteTrans_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (Convert.ToString(ViewState["TestProfileTrans"]) != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["PackageServiceTrans"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["PackageServiceTrans"] = DT;

                }
                BindTempPackageTrans();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       PackageServiceMaster.DeleteTrans_Click");
                TextFileWriting(ex.Message.ToString());
            }




        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " HPSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPSM_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";
            objCom.fnDeleteTableData("HMS_PACKAGE_SERVICE_MASTER", Criteria);


            Criteria = " HPST_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPST_PACKAGE_ID='" + txtPackageID.Text.Trim() + "'";
            objCom.fnDeleteTableData("HMS_PACKAGE_SERVICE_TRANS", Criteria);

            txtPackageID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "PAKSERVMAS");
            Clear();

        }

    }
}