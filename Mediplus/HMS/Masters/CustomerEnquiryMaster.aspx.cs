﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Masters
{
    public partial class CustomerEnquiryMaster : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindEnquiryMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            string strStartDate = txtEnqFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }



            if (txtEnqFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HCEM_DATE,101),101) >= '" + strForStartDate + "'";
            }




            string strTotDate = txtEnqToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtEnqToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HCEM_DATE,101),101) <= '" + strForToDate + "'";
            }



            if (txtEnquirSearch.Text.Trim() != "")
            {
                Criteria += " AND (HCEM_FNAME LIKE '%" + txtEnquirSearch.Text.Trim() + "%' OR  HCEM_PHONE LIKE '" + txtEnquirSearch.Text.Trim() + "%' OR   HCEM_MOBILE LIKE '" + txtEnquirSearch.Text.Trim() + "%')";
            }

            if (drpStatus.SelectedIndex != 0)
            {
                Criteria += " AND HCEM_STATUS='" + drpStatus.SelectedValue + "'";
            }

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*,CONVERT(VARCHAR,HCEM_DATE,103)AS HCEM_DATEDesc ", "HMS_CUSTOMER_ENQUIRY_MASTER LEFT JOIN HMS_KNOW_FROM ON CODE = HCEM_KNOW_FROM", Criteria, "HCEM_DATE desc");

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvEnquiryMaster.DataSource = DS;

                gvEnquiryMaster.DataBind();
            }
            else
            {
                gvEnquiryMaster.DataBind();
            }


        }

        void BindKnowFrom()
        {
            dboperations dbo = new dboperations();

            string Criteria = " 1=1 ";
            DataSet ds = new DataSet();
            ds = dbo.KnowFromGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                drpKnowFrom.DataSource = ds;
                drpKnowFrom.DataValueField = "Code";
                drpKnowFrom.DataTextField = "Description";
                drpKnowFrom.DataBind();

            }
            drpKnowFrom.Items.Insert(0, "--- Select ---");
            drpKnowFrom.Items[0].Value = "";

            ds.Clear();
            ds.Dispose();

        }



        private void ClearAttributes()
        {
            //foreach (System.Web.UI.HtmlControls.HtmlTableRow tr in tblEnqCtrls.Rows)
            //{
            //    if (tr.Cells[1].Controls.Count > 1)
            //    {
            //        TextBox AttrValue = (TextBox)tr.Cells[1].Controls[1];
            //        AttrValue.Text = string.Empty;
            //    }
            //}
            txtEnquiryID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "CUSTENQMAS");
            txtFName.Text = "";
            txtPhone.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
            drpKnowFrom.SelectedIndex = 0;

        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='PATIENTREG' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?PageName=Registration");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                txtEnquiryID.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "CUSTENQMAS");
                txtEnquiryDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtEnqFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtEnqToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                BindKnowFrom();
                BindEnquiryMaster();
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvEnquiryMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvEnquiryMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblEnquiryID = (Label)gvScanCard.Cells[0].FindControl("lblEnquiryID");
                Label lblEnquiryDate = (Label)gvScanCard.Cells[0].FindControl("lblEnquiryDate");
                Label lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");
                Label lblPhone = (Label)gvScanCard.Cells[0].FindControl("lblPhone");
                Label lblMobile = (Label)gvScanCard.Cells[0].FindControl("lblMobile");
                Label lblEMail = (Label)gvScanCard.Cells[0].FindControl("lblEMail");
                Label lblKnowFrom = (Label)gvScanCard.Cells[0].FindControl("lblKnowFrom");



                txtEnquiryID.Text = lblEnquiryID.Text;
                txtEnquiryDate.Text = lblEnquiryDate.Text;
                txtFName.Text = lblName.Text;
                txtPhone.Text = lblPhone.Text;
                txtMobile.Text = lblMobile.Text;
                txtEmail.Text = lblEMail.Text;

                drpKnowFrom.SelectedValue = lblKnowFrom.Text;

                ViewState["EnquiryID"] = lblEnquiryID.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void imgACMasRefres_Click(object sender, ImageClickEventArgs e)
        {
            BindEnquiryMaster();
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearAttributes();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerEnquiryMasterBAL objEnq = new CustomerEnquiryMasterBAL();
                objEnq.HCEM_ID = txtEnquiryID.Text.Trim();
                objEnq.BranchID = Convert.ToString(Session["Branch_ID"]);
                objEnq.HCEM_DATE = txtEnquiryDate.Text;
                objEnq.HCEM_FNAME = txtFName.Text.Trim();
                objEnq.HCEM_PHONE = txtPhone.Text.Trim();
                objEnq.HCEM_MOBILE = txtMobile.Text.Trim();
                objEnq.HCEM_EMAIL = txtEmail.Text.Trim();
                objEnq.HCEM_KNOW_FROM = drpKnowFrom.SelectedValue;
                objEnq.CustomerEnquiryMasterAdd();

                BindEnquiryMaster();
                ClearAttributes();


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  Details Saved.',' Customer Details Saved.')", true);

            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CustomerEnquiryMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblEnquiryID = (Label)gvScanCard.Cells[0].FindControl("lblEnquiryID");



                //DataSet DS = new DataSet();
                //string Criteria = " 1=1 ";
                //Criteria += " AND  AJT_ACC_CODE='" + lnkEnquiryID.Text + "'";

                //JournalEntryBAL objJour = new JournalEntryBAL();
                //DS = objJour.JournalTransactionGet(Criteria);

                //if (DS.Tables[0].Rows.Count > 0)
                //{
                //    lblStatus.Text = "This Account Code is Involved another transaction ";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}




                CommonBAL objCom = new CommonBAL();
                string Criteria1 = " HCEM_ID='" + lblEnquiryID.Text + "'";
                objCom.fnDeleteTableData("HMS_CUSTOMER_ENQUIRY_MASTER ", Criteria1);

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  Data Deleted.',' Customer Details Data Deleted.')", true);


                BindEnquiryMaster();


            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CustomerEnquiryMaster.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

    }
}