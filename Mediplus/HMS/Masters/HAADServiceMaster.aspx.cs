﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class HAADServiceMaster : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCodeType()
        {


            DataSet DS = new DataSet();
            DS = dbo.CodeTypeGet();

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "NAME";
                drpCategory.DataValueField = "CODE";
                drpCategory.DataBind();


                drpSrcCategory.DataSource = DS;
                drpSrcCategory.DataTextField = "NAME";
                drpSrcCategory.DataValueField = "CODE";
                drpSrcCategory.DataBind();

            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "";

            drpSrcCategory.Items.Insert(0, "--- All ---");
            drpSrcCategory.Items[0].Value = "";

        }


        void BindHaadService()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";


            if (txtSrcName.Text.Trim() != "")
            {
                Criteria += " AND ( HHS_CODE LIKE '%" + txtSrcName.Text.Trim() + "%' OR   HHS_DESCRIPTION LIKE '%" + txtSrcName.Text.Trim() + "%'  OR   HHS_SHORT_NAME LIKE '%" + txtSrcName.Text.Trim() + "%' ) ";
            }


            if (drpSrcCategory.SelectedIndex != 0)
            {
                Criteria += " AND HHS_TYPE_VALUE='" + drpSrcCategory.SelectedValue + "'";
            }

            DS = objCom.fnGetFieldValue(" top 1000 *", "HMS_HAAD_SERVICES INNER JOIN HMS_VIEW_CodeTypeGet ON  HHS_TYPE_VALUE = Code", Criteria, "HHS_TYPE,HHS_DESCRIPTION");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHaadService.DataSource = DS;
                gvHaadService.DataBind();
            }
            else
            {
                gvHaadService.DataBind();
            }
        }

        void BindModifyData()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND HHS_CODE='" + txtServCode.Text.Trim() + "'";

            DS = objCom.fnGetFieldValue(" top 1 *", "HMS_HAAD_SERVICES INNER JOIN HMS_VIEW_CodeTypeGet ON  HHS_TYPE_VALUE = Code", Criteria, "HHS_TYPE,HHS_DESCRIPTION");

            if (DS.Tables[0].Rows.Count > 0)
            {

                ViewState["NewFlag"] = false;

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHS_TYPE_VALUE"]) != "")
                {
                    for (int intNatiCount = 0; intNatiCount < drpCategory.Items.Count; intNatiCount++)
                    {
                        if (drpCategory.Items[intNatiCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HHS_TYPE_VALUE"]))
                        {
                            drpCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                            goto ForNation;
                        }

                    }
                }
            ForNation: ;

                txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHS_CODE"]);
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                txtShortName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHS_SHORT_NAME"]);
                txtServFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHS_PRICE"]);


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHS_STATUS"]) == "Active")
                {
                    chkStatus.Checked = true;
                }
                else
                {
                    chkStatus.Checked = false;
                }

            }
        }

        void Clear()
        {
            ViewState["NewFlag"] = true;
            drpCategory.SelectedIndex = 0;

            txtServName.Text = "";
            txtShortName.Text = "";
            txtServFee.Text = "";
            chkStatus.Checked = true;

           


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                ViewState["NewFlag"] = true;
                BindCodeType();

                BindHaadService();
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            Clear();
            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvHaadService.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["NewFlag"] = false;
            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;
            gvHaadService.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.LightGray;


            Label lblCodeType = (Label)gvScanCard.Cells[0].FindControl("lblCodeType");
            Label lblServCode = (Label)gvScanCard.Cells[0].FindControl("lblServCode");
            Label lblServDesc = (Label)gvScanCard.Cells[0].FindControl("lblServDesc");
            Label lblShortName = (Label)gvScanCard.Cells[0].FindControl("lblShortName");
            Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");
            Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");




            if (lblCodeType.Text.Trim() != "")
            {
                for (int intNatiCount = 0; intNatiCount < drpCategory.Items.Count; intNatiCount++)
                {
                    if (drpCategory.Items[intNatiCount].Value == lblCodeType.Text.Trim())
                    {
                        drpCategory.SelectedValue = lblCodeType.Text.Trim();
                        goto ForNation;
                    }

                }
            }
        ForNation: ;

            txtServCode.Text = lblServCode.Text.Trim();
            txtServName.Text = lblServDesc.Text.Trim();
            txtShortName.Text = lblShortName.Text.Trim();
            txtServFee.Text = lblPrice.Text.Trim();


            if (lblStatus.Text.Trim() == "Active")
            {
                chkStatus.Checked = true;
            }
            else
            {
                chkStatus.Checked = false;
            }


        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            Clear();

            BindModifyData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strActive = "";

            if (chkStatus.Checked == true)
            {
                strActive = "Active";
            }
            else
            {
                strActive = "InActive";
            }

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                CommonBAL objCom = new CommonBAL();
                string FieldName = "HHS_CODE,HHS_DESCRIPTION,HHS_SHORT_NAME,HHS_PRICE,HHS_STATUS,HHS_TYPE_VALUE,HHS_TYPE";
                string Values = "'" + txtServCode.Text.Trim() + "','" + txtServName.Text + "','" + txtShortName.Text + "'," + txtServFee.Text.Trim()   +
                     ",'" + strActive + "'," + drpCategory.SelectedValue+ ",'"+  drpCategory.SelectedItem.Text +"'" ;
                objCom.fnInsertTableData(FieldName, Values, "HMS_HAAD_SERVICES");

            }
            else
            {
                CommonBAL objCom = new CommonBAL();


                string FieldNameWithValues = "HHS_DESCRIPTION='" + txtServName.Text + "',HHS_SHORT_NAME='" + txtShortName.Text + "'";
                FieldNameWithValues += ",HHS_PRICE=" + txtServFee.Text.Trim() + ",HHS_STATUS='" + strActive + "'";

                string Criteria = "HHS_CODE='" + txtServCode.Text.Trim() + "' AND HHS_TYPE_VALUE=" + drpCategory.SelectedValue ;
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_HAAD_SERVICES", Criteria);

            }

            txtServCode.Text = "";
            Clear();
            BindHaadService();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage('  DETAILS SAVED.',' HAAD Service Details Saved.','Green')", true);

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {


        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtServCode.Text = "";
            Clear();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindHaadService();
        }
    }
}