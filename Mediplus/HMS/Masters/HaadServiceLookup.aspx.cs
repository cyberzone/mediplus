﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class HaadServiceLookup : System.Web.UI.Page
    {
      
        DataSet DS;
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindHaadServiceGrid()
        {
            string Criteria = " 1=1 ";

           // if (Convert.ToString(ViewState["Code"]) != "")
           // {
           //     Criteria += " AND  HHS_CODE like '%" + Convert.ToString(ViewState["Code"]) + "%'";
           //}

           // if (Convert.ToString(ViewState["Name"]) != "")
           // {
           //     Criteria += " AND  HHS_DESCRIPTION like'%" + Convert.ToString(ViewState["Name"]) + "%'";
           // }
         

            if (Convert.ToString(ViewState["CodeType"]) != "")
            {
                Criteria += " AND  HHS_TYPE_VALUE='" + Convert.ToString(ViewState["CodeType"]) + "'";
            }

            if (txtSearch.Text.Trim() != "")
            {
                Criteria += " AND HHS_CODE + ' ' +isnull(HHS_DESCRIPTION,'')   like '%" + txtSearch.Text.Trim() + "%' ";
            }

            


        dboperations    dbo = new dboperations();
            DS = new DataSet();
            DS = dbo.HaadServiceGet(Criteria, Convert.ToString(ViewState["CategoryType"]), "30");

            gvHaadService.Visible = false;
         
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHaadService.Visible = true;
                gvHaadService.DataSource = DS;
                gvHaadService.DataBind();
            }
            else
            {
                gvHaadService.DataBind();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    ViewState["Value"] = Convert.ToString(Request.QueryString["Value"]);
                    ViewState["CodeType"] = Convert.ToString(Request.QueryString["CodeType"]);
                    ViewState["Code"] = Convert.ToString(Request.QueryString["Code"]);
                    ViewState["Name"] = Convert.ToString(Request.QueryString["Name"]);
                    ViewState["CategoryType"] = Convert.ToString(Request.QueryString["CategoryType"]);
                    ViewState["TypeValue"] = (string)Request.QueryString["TypeValue"];

                    txtSearch.Text = Convert.ToString(ViewState["Value"]);

                    BindHaadServiceGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      HaadServiceLookup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void HaadSelect_Click(object sender, EventArgs e)
        {
            try
            {


                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblHaadServId, lblHaadServDesc;
                lblHaadServId = (Label)gvScanCard.Cells[0].FindControl("lblHaadServId");
                lblHaadServDesc = (Label)gvScanCard.Cells[0].FindControl("lblHaadServDesc");
                //txtHaadCode.Text = lblHaadServId.Text.Trim();
                //txtHaadName.Text = lblHaadServDesc.Text.Trim();
                //ModalPopupExtender1.Hide();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HaadServiceLookup.HaadSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindHaadServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HaadServiceLookup.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
 


    }
}