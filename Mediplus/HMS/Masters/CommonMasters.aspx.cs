﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus.HMS.Masters
{
    public partial class CommonMasters : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {

            string Criteria = " 1=1 ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.GetCompanyMaster (Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvCommMaster.DataSource = DS;
                gvCommMaster.DataBind();

                gvDistricts.DataSource = DS;
                gvDistricts.DataBind();

                GridView1.DataSource = DS;
                GridView1.DataBind();

            }


        }

        void Clear()
        {
            txtCode.Text = "";
            txtDesc.Text = "";
            if (drpType.Items.Count > 0)
                drpType.SelectedIndex = 0;

            chkStatus.Checked = true;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    hidHCM_ID.Value = "0";
                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CommonMasters.Page_Load");
                    TextFileWriting(ex.Message.ToString());


                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCommBal = new CommonBAL();
                objCommBal.HCM_ID = hidHCM_ID.Value;
                objCommBal.HCM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objCommBal.HCM_CODE = txtCode.Text.Trim();
                objCommBal.HCM_DESC = txtDesc.Text.Trim();
                objCommBal.HCM_TYPE = drpType.SelectedValue;
                if (chkStatus.Checked == true)
                    objCommBal.HCM_STATUS = "A";
                else
                    objCommBal.HCM_STATUS = "I";
                objCommBal.UserID = Convert.ToString(Session["User_ID"]);
                objCommBal.CommonMastersAdd();

                lblStatus.Text = "Data Saved!";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                Clear();

                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CommonMasters.btnSave_Click");
                TextFileWriting(ex.Message.ToString());


            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }



        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvCommMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["SelectIndex"] = gvScanCard.RowIndex;
                gvCommMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblHcmID = (Label)gvScanCard.Cells[0].FindControl("lblHcmID");
                Label lblHcmCode = (Label)gvScanCard.Cells[0].FindControl("lblHcmCode");
                Label lblHcmDesc = (Label)gvScanCard.Cells[0].FindControl("lblHcmDesc");
                Label lblHcmType = (Label)gvScanCard.Cells[0].FindControl("lblHcmType");
                Label lblActive = (Label)gvScanCard.Cells[0].FindControl("lblActive");
                hidHCM_ID.Value = lblHcmID.Text;
                txtCode.Text = lblHcmCode.Text;
                txtDesc.Text = lblHcmDesc.Text;

                if (lblActive.Text == "A")
                    chkStatus.Checked = true;
                else
                    chkStatus.Checked = false;

                if (lblHcmType.Text != "")
                {

                    for (int intCount = 0; intCount < drpType.Items.Count; intCount++)
                    {
                        if (drpType.Items[intCount].Value == lblHcmType.Text)
                        {
                            drpType.SelectedValue = lblHcmType.Text;
                            goto ForType;

                        }
                    }
                ForType: ;
                }


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnShow_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowEXEApp()", true);


        }

        protected void gvCommMaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 0)
                    e.Row.Style.Add("height", "50px");
            }
        }

        protected void gvDistricts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 0)
                    e.Row.Style.Add("height", "55px");
            }
        }
    }
}