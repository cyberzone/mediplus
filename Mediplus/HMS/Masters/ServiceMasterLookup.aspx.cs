﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class ServiceMasterLookup : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();
        DataSet DS;
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BuildServiceList()
        {
            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn ServCode = new DataColumn();
            ServCode.ColumnName = "ServCode";

            DataColumn Description = new DataColumn();
            Description.ColumnName = "Description";


            DataColumn HaadCode = new DataColumn();
            HaadCode.ColumnName = "HaadCode";

            DataColumn Category = new DataColumn();
            Category.ColumnName = "Category";

            DataColumn Fee = new DataColumn();
            Fee.ColumnName = "Fee";


            dt.Columns.Add(ServCode);
            dt.Columns.Add(Description);
            dt.Columns.Add(HaadCode);
            dt.Columns.Add(Category);
            dt.Columns.Add(Fee);
            ViewState["ServiceList"] = dt;
        }

        void BindServiceGrid()
        {
            string Criteria = " 1=1  ";

            if (Convert.ToString(ViewState["CompanyID"]) != "" && Convert.ToString(ViewState["CompanyID"]) != null)
            {
                Criteria += " AND   HAS_COMP_ID='" + Convert.ToString(ViewState["CompanyID"]) + "'";

                if (hidCtrlName.Value == "txtServCode")
                {
                    Criteria += " AND HAS_SERV_ID   like '" + txtSearch.Text.Trim() + "%' ";
                }
                else if (hidCtrlName.Value == "txtServName")
                {
                    Criteria += " AND HAS_SERV_NAME   like '" + txtSearch.Text.Trim() + "%' ";
                }
                else
                {
                    Criteria += " AND HAS_SERV_ID + ' ' +isnull(HAS_SERV_NAME,'')   like '%" + txtSearch.Text.Trim() + "%' ";
                }


                if (drpCategory.SelectedIndex != 0)
                {

                    Criteria += " AND  HAS_CATEGORY_ID='" + drpCategory.SelectedValue + "'";
                }

                if (Convert.ToString(ViewState["Type"]) != "")
                {
                    Criteria += " AND HAS_CATEGORY_ID='" + Convert.ToString(ViewState["Type"]) + "'";
                }

                CommonBAL objCom = new CommonBAL();

              
                DS = new DataSet();
                DS = objCom.AgrementServiceTopGet(Criteria, "100");

                gvAgrServices.Visible = false;
                lblTotal.Text = Convert.ToString(DS.Tables[0].Rows.Count);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvAgrServices.Visible = true;
                    gvAgrServices.DataSource = DS;
                    gvAgrServices.DataBind();
                }
                else
                {
                    gvAgrServices.DataBind();
                }

            }
            else
            {
                Criteria += " AND HSM_STATUS='A' ";

                Criteria += " AND HSM_SERV_ID + ' ' +isnull(HSM_NAME,'')   like '%" + txtSearch.Text.Trim() + "%' ";



                if (drpCategory.SelectedIndex != 0)
                {

                    Criteria += " AND  HSM_CAT_ID='" + drpCategory.SelectedValue + "'";
                }


                if (Convert.ToString(ViewState["Type"]) != "")
                {
                    Criteria += " AND HSM_TYPE='" + Convert.ToString(ViewState["Type"]) + "'";
                }

                dbo = new dboperations();
                DS = new DataSet();
                DS = dbo.ServiceMasterGet(Criteria);

                gvServices.Visible = false;
                lblTotal.Text = Convert.ToString(DS.Tables[0].Rows.Count);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvServices.Visible = true;
                    gvServices.DataSource = DS;
                    gvServices.DataBind();
                }
                else
                {
                    gvServices.DataBind();
                }



            }


        }

        void BindCategory()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSCM_STATUS='A' "; // AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            dbo = new dboperations();
            DS = new DataSet();
            DS = dbo.ServiceCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "HSCM_NAME";
                drpCategory.DataValueField = "HSCM_CAT_ID";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- All ---");
            drpCategory.Items[0].Value = "0";

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                try
                {
                    ViewState["Value"] = "";
                    ViewState["Type"] = "";

                    BindCategory();
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    ViewState["Value"] = Convert.ToString(Request.QueryString["Value"]);
                    ViewState["CompanyID"] = Convert.ToString(Request.QueryString["CompanyID"]);


                    ViewState["Type"] = (string)Request.QueryString["Type"];

                    txtSearch.Text = Convert.ToString(ViewState["Value"]);
                    BuildServiceList();

                    BindServiceGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void gvServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvServices.PageIndex = e.NewPageIndex;
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.gvServices_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvAgrServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvAgrServices.PageIndex = e.NewPageIndex;
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.gvAgrServices_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.drpCategory_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
 

    }
}