﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Mediplus_BAL;
namespace Mediplus.HMS.Masters
{
    public partial class BenefitsExeclusions : System.Web.UI.Page
    {
        DataSet DS;
        dboperations dbo;
        static string strSessionBranchId;
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCompany()
        {

            string Criteria = " 1=1 AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCompany.DataSource = ds;
                drpCompany.DataValueField = "HCM_COMP_ID";
                drpCompany.DataTextField = "HCM_NAME";
                drpCompany.DataBind();

            }
            drpCompany.Items.Insert(0, "--- Select---");
            drpCompany.Items[0].Value = "0";


        }
        void BindPlanType()
        {
            drpCatType.Items.Clear();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCB_COMP_ID = '" + drpCompany.SelectedValue + "'";
            dbo = new dboperations();
            DS = dbo.CompBenefitsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCatType.DataSource = DS;
                drpCatType.DataTextField = "HCB_CAT_TYPE";
                drpCatType.DataValueField = "HCB_CAT_TYPE";
                drpCatType.DataBind();
            }

            drpCatType.Items.Insert(0, "--- Select ---");
            drpCatType.Items[0].Value = "0";



        }

        void BindBenefitsGridBasedFrom_InsTrmtType()
        {

            DS = new DataSet();

            DataTable DT = new DataTable();


            DataColumn HCBD_CATEGORY_ID = new DataColumn();
            HCBD_CATEGORY_ID.ColumnName = "HCBD_CATEGORY_ID";

            DataColumn HCBD_CATEGORY = new DataColumn();
            HCBD_CATEGORY.ColumnName = "HCBD_CATEGORY";

            DataColumn HCBD_CATEGORY_TYPE = new DataColumn();
            HCBD_CATEGORY_TYPE.ColumnName = "HCBD_CATEGORY_TYPE";
            DataColumn HCBD_LIMIT = new DataColumn();
            HCBD_LIMIT.ColumnName = "HCBD_LIMIT";



            DataColumn HCBD_CO_INS_TYPE = new DataColumn();
            HCBD_CO_INS_TYPE.ColumnName = "HCBD_CO_INS_TYPE";

            DataColumn HCBD_CO_INS_AMT = new DataColumn();
            HCBD_CO_INS_AMT.ColumnName = "HCBD_CO_INS_AMT";

            DataColumn HCBD_DED_TYPE = new DataColumn();
            HCBD_DED_TYPE.ColumnName = "HCBD_DED_TYPE";

            DataColumn HCBD_DED_AMT = new DataColumn();
            HCBD_DED_AMT.ColumnName = "HCBD_DED_AMT";

            DT.Columns.Add(HCBD_CATEGORY_ID);
            DT.Columns.Add(HCBD_CATEGORY);
            DT.Columns.Add(HCBD_CATEGORY_TYPE);
            DT.Columns.Add(HCBD_LIMIT);
            DT.Columns.Add(HCBD_CO_INS_TYPE);
            DT.Columns.Add(HCBD_CO_INS_AMT);
            DT.Columns.Add(HCBD_DED_TYPE);
            DT.Columns.Add(HCBD_DED_AMT);

            string Criteria = " 1=1 ";
            DataSet DS1 = new DataSet();
            if (drpBenefitsType.SelectedIndex == 0)
            {
                Criteria += " AND HITT_STATUS='1' ";
                dbo = new dboperations();
                DS1 = dbo.TreatmentTypeGet(Criteria);

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                    {
                        DataRow objrow;
                        objrow = DT.NewRow();
                        objrow["HCBD_CATEGORY_ID"] = Convert.ToString(DS1.Tables[0].Rows[i]["HITT_ID"]);
                        objrow["HCBD_CATEGORY"] = Convert.ToString(DS1.Tables[0].Rows[i]["HITT_NAME"]);
                        objrow["HCBD_CATEGORY_TYPE"] = "Covered";
                        objrow["HCBD_LIMIT"] = "0.00";
                        objrow["HCBD_CO_INS_TYPE"] = "%";
                        if (txtCoIns.Text.Trim() != "")
                        {
                            objrow["HCBD_CO_INS_AMT"] = txtCoIns.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_CO_INS_AMT"] = "0.00";
                        }

                        objrow["HCBD_DED_TYPE"] = "%";
                        if (txtDeductible.Text.Trim() != "")
                        {
                            objrow["HCBD_DED_AMT"] = txtDeductible.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_DED_AMT"] = "0.00";
                        }
                        DT.Rows.Add(objrow);
                    }
                }
            }
            else
            {
                Criteria += " AND HSCM_STATUS='A'  AND HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                dbo = new dboperations();
                DS1 = dbo.ServiceCategoryGet(Criteria);

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= DS1.Tables[0].Rows.Count - 1; i++)
                    {
                        DataRow objrow;

                        objrow = DT.NewRow();
                        objrow["HCBD_CATEGORY_ID"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_CAT_ID"]);
                        objrow["HCBD_CATEGORY"] = Convert.ToString(DS1.Tables[0].Rows[i]["HSCM_NAME"]);
                        objrow["HCBD_CATEGORY_TYPE"] = "Covered";
                        objrow["HCBD_LIMIT"] = "0.00";
                        objrow["HCBD_CO_INS_TYPE"] = "%";
                        if (txtCoIns.Text.Trim() != "")
                        {
                            objrow["HCBD_CO_INS_AMT"] = txtCoIns.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_CO_INS_AMT"] = "0.00";
                        }

                        objrow["HCBD_DED_TYPE"] = "%";
                        if (txtDeductible.Text.Trim() != "")
                        {
                            objrow["HCBD_DED_AMT"] = txtDeductible.Text.Trim();
                        }
                        else
                        {
                            objrow["HCBD_DED_AMT"] = "0.00";
                        }

                        DT.Rows.Add(objrow);
                    }
                }
            }




            DS.Tables.Add(DT);
            gvHCB.Visible = true;
            gvHCB.DataSource = DS;
            gvHCB.DataBind();
        }

        void BindCompBenefits()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            Criteria += " AND HCB_COMP_ID = '" + drpCompany.SelectedValue + "'";

            Criteria += " AND HCB_CAT_TYPE = '" + drpCatType.SelectedValue + "'";


            dbo = new dboperations();
            DS = dbo.CompBenefitsGet(Criteria);
            txtDeductible.Text = "";
            txtCoIns.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpTreatmentType.SelectedValue = DS.Tables[0].Rows[0]["HCB_TREATMENT_TYPE"].ToString();
                drpBenefitsType.SelectedValue = DS.Tables[0].Rows[0]["HCB_BENEFITS_TYPE"].ToString();


                drpHospDiscType.SelectedValue = DS.Tables[0].Rows[0]["HCB_HOSP_DISC_TYPE"].ToString();
                drpDedType.SelectedValue = DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_TYPE"].ToString();
                drpCoInsType.SelectedValue = DS.Tables[0].Rows[0]["HCB_COINS_TYPE"].ToString();
                drpPharType.SelectedValue = DS.Tables[0].Rows[0]["HCB_PHY_DISC_TYPE"].ToString();
                drpPharCoInsType.SelectedValue = DS.Tables[0].Rows[0]["HCB_PHY_COINS_TYPE"].ToString();




                if (DS.Tables[0].Rows[0].IsNull("HCB_HOSP_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_AMT"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_AMT"]);
                    txtHospDiscAmt.Text = DedAmt.ToString("N2");
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]);
                    txtDeductible.Text = DedAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_MAX_DED") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_MAX_DED"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_MAX_DED"]);
                    TxtMaxDed.Text = DedAmt.ToString("N2");
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_COINS_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_COINS_AMT"]);
                    txtCoIns.Text = CoInsAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_MAX_COINS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_MAX_COINS"]) != "")
                {
                    decimal DedAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_MAX_COINS"]);
                    TxtMaxCoIns.Text = DedAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_PHY_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PHY_DISC_AMT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PHY_DISC_AMT"]);
                    txtPharDisc.Text = CoInsAmt.ToString("N2");
                }



                if (DS.Tables[0].Rows[0].IsNull("HCB_PHY_COINS_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PHY_COINS_AMT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PHY_COINS_AMT"]);
                    txtPharCoInsDisc.Text = CoInsAmt.ToString("N2");
                }
                if (DS.Tables[0].Rows[0].IsNull("HCB_MAX_PHY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_MAX_PHY"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_MAX_PHY"]);
                    TxtMaxPhy.Text = CoInsAmt.ToString("N2");
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_CONSLT_INCL_HOSP_DISC") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_CONSLT_INCL_HOSP_DISC"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_CONSLT_INCL_HOSP_DISC"]) == "Y")
                    {
                        ChkConsultation.Checked = true;
                    }
                    else
                    {
                        ChkConsultation.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE_MAX_CONSLT_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_MAX_CONSLT_AMT"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_MAX_CONSLT_AMT"]) == "Y")
                    {
                        ChkDedMaxConsAmt.Checked = true;
                    }
                    else
                    {
                        ChkDedMaxConsAmt.Checked = false;
                    }
                }
                txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCB_REMARKS"]);


                if (DS.Tables[0].Rows[0].IsNull("HCB_DED_COINS_APPLICABLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_COINS_APPLICABLE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_COINS_APPLICABLE"]) == "Y")
                    {
                        ChkDedCoIns.Checked = true;
                    }
                    else
                    {
                        ChkDedCoIns.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_COINS_FROM_GROSSAMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_FROM_GROSSAMT"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_FROM_GROSSAMT"]) == "Y")
                    {
                        ChkCoInsFromGrossAmt.Checked = true;
                    }
                    else
                    {
                        ChkCoInsFromGrossAmt.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_SPL_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_SPL_CODE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_SPL_CODE"]) == "Y")
                    {
                        ChkSplCodeNeeded.Checked = true;
                    }
                    else
                    {
                        ChkSplCodeNeeded.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HCB_COINSFROMGROSS_CLAIMFROMNET") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]) == "Y")
                    {
                        ChkCoInsFromGrossandClaimfromNetAmt.Checked = true;
                    }
                    else
                    {
                        ChkCoInsFromGrossandClaimfromNetAmt.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_DED_INSURANCE_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_INSURANCE_TYPE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_INSURANCE_TYPE"]) == "Y")
                    {
                        ChkDedBenefitsType.Checked = true;
                    }
                    else
                    {
                        ChkDedBenefitsType.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_PRICE_FACTOR") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PRICE_FACTOR"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PRICE_FACTOR"]);
                    txtPriceFactor.Text = CoInsAmt.ToString("N2");
                }

                if (DS.Tables[0].Rows[0].IsNull("HCB_PERDAYLIMIT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCB_PERDAYLIMIT"]) != "")
                {
                    decimal CoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_PERDAYLIMIT"]);
                    txtPerDayPTLimit.Text = CoInsAmt.ToString("N2");
                }

            }


        }

        void BindCompBenefitsGrid()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND HCBD_COMP_ID = '" + drpCompany.SelectedValue + "'";

            Criteria += " AND HCBD_CAT_TYPE = '" + drpCatType.SelectedValue + "'";

            dbo = new dboperations();
            DS = dbo.CompBenefitsDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHCB.Visible = true;
                gvHCB.DataSource = DS;
                gvHCB.DataBind();
            }
            else
            {
                BindBenefitsGridBasedFrom_InsTrmtType();

            }





        }

        void ClearBenefit()
        {


            txtHospDiscAmt.Text = "";
            txtDeductible.Text = "";
            txtCoIns.Text = "";
            TxtMaxDed.Text = "";
            TxtMaxCoIns.Text = "";
            txtPharDisc.Text = "";
            txtPharCoInsDisc.Text = "";
            TxtMaxPhy.Text = "";
            ChkConsultation.Checked = false;
            ChkDedMaxConsAmt.Checked = false;
            ChkDedCoIns.Checked = false;
            ChkCoInsFromGrossAmt.Checked = false;
            ChkSplCodeNeeded.Checked = false;
            ChkCoInsFromGrossandClaimfromNetAmt.Checked = false;
            ChkDedBenefitsType.Checked = false;
            txtPriceFactor.Text = "";
            txtPerDayPTLimit.Text = "";
            txtRemarks.Text = "";
            gvHCB.DataBind();
        }
        #endregion

        #region AutoCompleteExtender

        [System.Web.Services.WebMethod]
        public static string[] GetCategory(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HICM_STATUS='A'  AND  HICM_BRANCH_ID='" + strSessionBranchId + "'";

            Criteria += " AND HICM_INS_CAT_ID Like '%" + prefixText + "%'";
            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.InsCatMastereGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HICM_INS_CAT_ID"]);


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }



        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                BindCompany();
                strSessionBranchId = Convert.ToString(Session["Branch_ID"]);
                // ViewState["CompanyID"] = Convert.ToString(Request.QueryString["CompanyID"]);

                if (Convert.ToString(Request.QueryString["CompanyID"]) != "")
                {
                    for (int intCount = 0; intCount < drpCompany.Items.Count; intCount++)
                    {
                        if (drpCompany.Items[intCount].Value == Convert.ToString(Request.QueryString["CompanyID"]))
                        {
                            drpCompany.SelectedValue = Convert.ToString(Request.QueryString["CompanyID"]);

                            goto ForComp;
                        }

                    }
                }
            ForComp: ;


                BindPlanType();



                if (Convert.ToString(Request.QueryString["PlanType"]) != "")
                {
                    for (int intCount = 0; intCount < drpCatType.Items.Count; intCount++)
                    {
                        if (drpCatType.Items[intCount].Value == Convert.ToString(Request.QueryString["PlanType"]))
                        {
                            drpCatType.SelectedValue = Convert.ToString(Request.QueryString["PlanType"]);

                            drpCatType_SelectedIndexChanged("drpCatType", new EventArgs());
                            goto ForPlan;
                        }

                    }
                }
            ForPlan: ;

            }
        }


        protected void txtCategory_TextChanged(object sender, EventArgs e)
        {

            if (txtCategory.Text.Trim() != "")
            {
                drpCatType.SelectedIndex = 0;
                for (int intCount = 0; intCount < drpCatType.Items.Count; intCount++)
                {
                    if (drpCatType.Items[intCount].Value == txtCategory.Text.Trim())
                    {
                        drpCatType.SelectedValue = txtCategory.Text.Trim();




                        goto ForTitle;
                    }

                }
            }
        ForTitle: ;
            ClearBenefit();
            BindCompBenefits();
            BindCompBenefitsGrid();


        }

        protected void drpCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPlanType();
        }

        protected void drpCatType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearBenefit();
            BindCompBenefits();
            BindCompBenefitsGrid();

        }

        protected void txtDeductible_TextChanged(object sender, EventArgs e)
        {

            BindCompBenefitsGrid();

        }

        protected void txtCoIns_TextChanged(object sender, EventArgs e)
        {

            BindCompBenefitsGrid();

        }

        protected void gvHCB_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList drpCategoryType, drpGVCoInsType, drpGVDedType;
                Label lblCatType, lblCoInsType, lblDedType;

                drpCategoryType = (DropDownList)e.Row.Cells[0].FindControl("drpCategoryType");
                drpGVCoInsType = (DropDownList)e.Row.Cells[0].FindControl("drpGVCoInsType");
                drpGVDedType = (DropDownList)e.Row.Cells[0].FindControl("drpGVDedType");

                lblCatType = (Label)e.Row.Cells[0].FindControl("lblCatType");
                lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");
                lblDedType = (Label)e.Row.Cells[0].FindControl("lblDedType");


                drpCategoryType.SelectedValue = lblCatType.Text;
                drpGVCoInsType.SelectedValue = lblCoInsType.Text;
                drpGVDedType.SelectedValue = lblDedType.Text;


            }
        }

        protected void btnBenefitSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strSelCategory = "";
                if (drpCatType.SelectedIndex != 0)
                {
                    strSelCategory = drpCatType.SelectedValue;
                }
                else
                {
                    strSelCategory = txtCategory.Text.Trim();
                }


                dboperations objDB;

                objDB = new dboperations();

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                SqlCommand cmd;

                if (txtCategory.Text.Trim() != "")
                {
                   
                    cmd = new SqlCommand();
                   con.Open();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_InsCatMasterAdd";
                    cmd.Parameters.Add(new SqlParameter("@HICM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HICM_INS_CAT_ID", SqlDbType.VarChar)).Value = txtCategory.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@UesrID", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);
                    cmd.ExecuteNonQuery();
                    con.Close();

                }

                if (txtCategory.Text.Trim() != "")
                {

                   
                    cmd = new SqlCommand();
                    con.Open();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_CompBenefitsAdd";
                    cmd.Parameters.Add(new SqlParameter("@HCB_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HCB_COMP_ID", SqlDbType.VarChar)).Value = drpCompany.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCB_CAT_TYPE", SqlDbType.VarChar)).Value = txtCategory.Text.Trim();
                    cmd.ExecuteNonQuery();
                   con.Close();

                }


               
                cmd = new SqlCommand();
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompBenefitsModify";
                cmd.Parameters.Add(new SqlParameter("@HCB_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HCB_COMP_ID", SqlDbType.VarChar)).Value = drpCompany.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCB_CAT_TYPE", SqlDbType.VarChar)).Value = strSelCategory;// txtCategory.Text.Trim(); //drpCatType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCB_HOSP_DISC_TYPE", SqlDbType.Char)).Value = drpHospDiscType.SelectedValue;
                if (txtHospDiscAmt.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_HOSP_DISC_AMT", SqlDbType.Decimal)).Value = txtHospDiscAmt.Text.Trim();
                }
                if (txtDeductible.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE", SqlDbType.Decimal)).Value = txtDeductible.Text.Trim();
                }
                cmd.Parameters.Add(new SqlParameter("@HCB_COINS_TYPE", SqlDbType.Char)).Value = drpCoInsType.SelectedValue;
                if (txtCoIns.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINS_AMT", SqlDbType.Decimal)).Value = txtCoIns.Text.Trim();
                }
                cmd.Parameters.Add(new SqlParameter("@HCB_TREATMENT_TYPE", SqlDbType.VarChar)).Value = drpTreatmentType.SelectedValue;

                if (ChkConsultation.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_CONSLT_INCL_HOSP_DISC", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_CONSLT_INCL_HOSP_DISC", SqlDbType.Char)).Value = "N";
                }

                if (ChkDedMaxConsAmt.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE_MAX_CONSLT_AMT", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE_MAX_CONSLT_AMT", SqlDbType.Char)).Value = "N";
                }

                cmd.Parameters.Add(new SqlParameter("@HCB_REMARKS", SqlDbType.VarChar)).Value = txtRemarks.Text.Trim();


                cmd.Parameters.Add(new SqlParameter("@HCB_BENEFITS_TYPE", SqlDbType.Char)).Value = drpBenefitsType.SelectedValue;

                if (ChkDedCoIns.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_COINS_APPLICABLE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_COINS_APPLICABLE", SqlDbType.Char)).Value = "N";
                }


                if (ChkCoInsFromGrossAmt.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINS_FROM_GROSSAMT", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINS_FROM_GROSSAMT", SqlDbType.Char)).Value = "N";
                }

                cmd.Parameters.Add(new SqlParameter("@HCB_DEDUCTIBLE_TYPE", SqlDbType.Char)).Value = drpDedType.SelectedValue;

                if (ChkSplCodeNeeded.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_SPL_CODE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_SPL_CODE", SqlDbType.Char)).Value = "N";
                }

                cmd.Parameters.Add(new SqlParameter("@HCB_PHY_DISC_TYPE", SqlDbType.Char)).Value = drpPharType.SelectedValue;
                if (txtPharDisc.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PHY_DISC_AMT", SqlDbType.Decimal)).Value = txtPharDisc.Text.Trim();
                }
                cmd.Parameters.Add(new SqlParameter("HCB_PHY_COINS_TYPE", SqlDbType.Char)).Value = drpPharCoInsType.SelectedValue;
                if (txtPharCoInsDisc.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PHY_COINS_AMT", SqlDbType.Decimal)).Value = txtPharCoInsDisc.Text.Trim();
                }
                if (TxtMaxDed.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_MAX_DED", SqlDbType.Decimal)).Value = TxtMaxDed.Text.Trim();
                }
                if (TxtMaxCoIns.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_MAX_COINS", SqlDbType.Decimal)).Value = TxtMaxCoIns.Text.Trim();
                }
                if (TxtMaxPhy.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_MAX_PHY", SqlDbType.Decimal)).Value = TxtMaxPhy.Text.Trim();
                }

                if (ChkCoInsFromGrossandClaimfromNetAmt.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINSFROMGROSS_CLAIMFROMNET", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_COINSFROMGROSS_CLAIMFROMNET", SqlDbType.Char)).Value = "N";
                }

                if (ChkDedBenefitsType.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_INSURANCE_TYPE", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_DED_INSURANCE_TYPE", SqlDbType.Char)).Value = "N";
                }

                if (txtPriceFactor.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PRICE_FACTOR", SqlDbType.Decimal)).Value = txtPriceFactor.Text.Trim();
                }
                if (txtPerDayPTLimit.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HCB_PERDAYLIMIT", SqlDbType.Decimal)).Value = txtPerDayPTLimit.Text.Trim();
                }

                int k = cmd.Parameters.Count;
                for (int i = 0; i < k; i++)
                {
                    if (cmd.Parameters[i].Value == "")
                    {
                        cmd.Parameters[i].Value = DBNull.Value;
                    }
                }


                cmd.ExecuteNonQuery();
               con.Close();


                for (int index = 0; index <= gvHCB.Rows.Count - 1; index++)
                {
                    Label lblCatId, lblCat;
                    lblCatId = (Label)gvHCB.Rows[index].Cells[0].FindControl("lblCatId");
                    lblCat = (Label)gvHCB.Rows[index].Cells[0].FindControl("lblCat");

                    DropDownList drpCategoryType, drpGVCoInsType, drpGVDedType;
                    drpCategoryType = (DropDownList)gvHCB.Rows[index].Cells[0].FindControl("drpCategoryType");
                    drpGVCoInsType = (DropDownList)gvHCB.Rows[index].Cells[0].FindControl("drpGVCoInsType");
                    drpGVDedType = (DropDownList)gvHCB.Rows[index].Cells[0].FindControl("drpGVDedType");


                    TextBox txtLimit, txtCoInsAmt, txtDedAmt;
                    txtLimit = (TextBox)gvHCB.Rows[index].Cells[0].FindControl("txtLimit");
                    txtCoInsAmt = (TextBox)gvHCB.Rows[index].Cells[0].FindControl("txtCoInsAmt");
                    txtDedAmt = (TextBox)gvHCB.Rows[index].Cells[0].FindControl("txtDedAmt");

                   
                    cmd = new SqlCommand();
                    con.Open();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "HMS_SP_CompBenefitsDtlsAdd";
                    cmd.Parameters.Add(new SqlParameter("@HCBD_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                    cmd.Parameters.Add(new SqlParameter("@HCBD_COMP_ID", SqlDbType.VarChar)).Value = drpCompany.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CAT_TYPE", SqlDbType.VarChar)).Value = strSelCategory;// txtCategory.Text.Trim();// drpCatType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CATEGORY_ID", SqlDbType.VarChar)).Value = lblCatId.Text;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CATEGORY", SqlDbType.VarChar)).Value = lblCat.Text;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CATEGORY_TYPE", SqlDbType.NVarChar)).Value = drpCategoryType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_LIMIT", SqlDbType.Decimal)).Value = txtLimit.Text.Trim();
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CO_INS_TYPE", SqlDbType.Char)).Value = drpGVCoInsType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_CO_INS_AMT", SqlDbType.Decimal)).Value = txtCoInsAmt.Text;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_DED_TYPE", SqlDbType.Char)).Value = drpGVDedType.SelectedValue;
                    cmd.Parameters.Add(new SqlParameter("@HCBD_DED_AMT", SqlDbType.Decimal)).Value = txtDedAmt.Text.Trim();


                    cmd.ExecuteNonQuery();
                   con.Close();


                }
                //if (drpCatType.Items.Count > 0)
                //{
                //    drpCatType.SelectedIndex = 0;
                //}
                // ClearBenefit();

                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + strSelCategory + "');", true);

            EndFun: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnBenefitSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnBenefitClear_Click(object sender, EventArgs e)
        {

            ClearBenefit();
        }

        protected void btnBenefitDelete_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;
              
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompBenefitsDelete";
                cmd.Parameters.Add(new SqlParameter("@HCB_COMP_ID", SqlDbType.VarChar)).Value = drpCompany.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HCB_CAT_TYPE", SqlDbType.VarChar)).Value = drpCatType.SelectedValue;

                cmd.ExecuteNonQuery();
                con.Close();

                ClearBenefit();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnBenefitDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }



        protected void drpBenefitsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindBenefitsGridBasedFrom_InsTrmtType();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPerDayPTLimit.Text.Trim() == "")
                {
                    txtPerDayPTLimit.Text = "0";
                }
                dbo = new dboperations();
                dbo.UpdatePerDayLimit(Convert.ToDecimal(txtPerDayPTLimit.Text.Trim()), drpCatType.SelectedValue, drpCompany.SelectedValue);
                lblStatus.Text = "Data Updated";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnUpdate_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Updated";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {

            string strSelCategory = "";
            if (drpCatType.SelectedIndex != 0)
            {
                strSelCategory = drpCatType.SelectedValue;
            }
            else
            {
                strSelCategory = txtCategory.Text.Trim();
            }


            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + strSelCategory + "');", true);

        }

        #endregion
    }
}