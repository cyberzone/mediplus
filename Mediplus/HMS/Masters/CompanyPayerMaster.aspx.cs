﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus.HMS.Masters
{
    public partial class CompanyPayerMaster : System.Web.UI.Page
    {
        CompanyPayerMasterBAL objCPM = new CompanyPayerMasterBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindPayerID()
        {

            string Criteria = " 1=1  and HPC_STATUS='A' ";


            DataSet ds = new DataSet();
            objCPM = new CompanyPayerMasterBAL();
            ds = objCPM.CompanyPayerMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                gvPayerID.DataSource = ds;
                gvPayerID.DataBind();
            }
            else
            {
                gvPayerID.DataBind();
            }



        }


        void Clear()
        {
            txtPayerName.Text = "";


            hidPayerID.Value = "0";

            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvPayerID.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["SelectIndex"] = "";
        }



        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='PTCOMP' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "0" || strPermission == "1")
            {

                btnSave.Enabled = false;
                btnClear.Enabled = false;



            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }

        #endregion


        #region Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {


                    hidPayerID.Value = "0";

                    BindPayerID();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CompanyPayerMaster.Page_Load");
                    TextFileWriting(ex.Message.ToString());

                }

            }
        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvPayerID.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;


                gvPayerID.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblPayerName;

                lblPayerName = (Label)gvScanCard.Cells[0].FindControl("lblPayerName");

                txtPayerName.Text = lblPayerName.Text;


                //  ViewState["HNCP_ID"] = lblHNCPId.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NurseCarePlan.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblPayerName;

                    lblPayerName = (Label)gvScanCard.Cells[0].FindControl("lblPayerName");

                    objCPM = new CompanyPayerMasterBAL();
                    objCPM.HPC_PATIENT_COMPANY = lblPayerName.Text;
                    objCPM.BranchID = Convert.ToString(Session["Branch_ID"]);
                    objCPM.UserId = Convert.ToString(Session["User_ID"]);
                    objCPM.DeleteCompanyPayerMaster();

                    ViewState["SelectIndex"] = "";
                    Clear();
                    BindPayerID();
                    lblStatus.Text = "Data Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NurseCarePlan.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                DataSet ds = new DataSet();
                objCPM = new CompanyPayerMasterBAL();


                string Criteria = " 1=1  ";
                Criteria += " AND HPC_PATIENT_COMPANY='" + txtPayerName.Text.Trim() + "'";


                ds = objCPM.CompanyPayerMasterGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Payer Already Available";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                objCPM = new CompanyPayerMasterBAL();

                objCPM.HPC_PATIENT_COMPANY = txtPayerName.Text.Trim();
                objCPM.AddCompanyPayerMaster();

                Clear();
                BindPayerID();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyPayerMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyPayerMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }



        #endregion
    }
}