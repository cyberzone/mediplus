﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Site2.Master"     AutoEventWireup="true" CodeBehind="StaffProfile.aspx.cs" Inherits="Mediplus.HMS.Masters.StaffProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

      <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script src="../Validation.js" type="text/javascript"></script>

      
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

<script language="javascript" type="text/javascript">

    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }


    function StaffPopup(CtrlName, strValue) {
        var win = window.open("Doctor_Name_Zoom.aspx?PageName=StaffProfile&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=570,width=1100,toolbar=no,scrollbars==yes,menubar=no");
        win.focus();

        return true;

    }

    function SaveVal() {
        var label;
        label = document.getElementById('<%=lblStatus.ClientID%>');
        label.style.color = 'red';
        document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strFileNo = document.getElementById('<%=txtStaffId.ClientID%>').value
        if (/\S+/.test(strFileNo) == false) {
                ShowErrorMessage('Staff ID Empty', 'Please enter Staff ID')
                document.getElementById('<%=txtStaffId.ClientID%>').focus;
                return false;
            }


            if (document.getElementById('<%=drpDepartment.ClientID%>').value == "0") {
                 ShowErrorMessage('Department Empty', 'Please Select Department')
                 document.getElementById('<%=drpDepartment.ClientID%>').focus();
                 return false;
             }

             if (document.getElementById('<%=drpDesignation.ClientID%>').value == "0") {
                 ShowErrorMessage('Designation Empty', 'Please Select Designation')
                document.getElementById('<%=drpDesignation.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtFName.ClientID%>').value == "") {
                ShowErrorMessage('First Name Empty', 'Please enter the FName')
                document.getElementById('<%=txtFName.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtLName.ClientID%>').value == "") {
                ShowErrorMessage('Last Name Empty', 'Please enter the Last')
                document.getElementById('<%=txtLName.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtDOB.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtDOB.ClientID%>').value) == false) {
                    document.getElementById('<%=txtDOB.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtPassIssueOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtPassIssueOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtPassIssueOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtPassExpOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtPassIssueOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtPassExpOn.ClientID%>').focus()
                    return false;
                }
            }



            if (document.getElementById('<%=txtIDIssueOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtIDIssueOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtIDIssueOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtIDExpOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtIDExpOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtIDExpOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtDrivLicIssueOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtDrivLicIssueOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtDrivLicIssueOn.ClientID%>').focus()
                    return false;
                }
            }


            if (document.getElementById('<%=txtDrivLicExpOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtDrivLicExpOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtDrivLicExpOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtHAADIssueOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtHAADIssueOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtHAADIssueOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtHAADExpOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtHAADExpOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtHAADExpOn.ClientID%>').focus()
                    return false;
                }
            }




            if (document.getElementById('<%=txtDocOtherIssueOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtDocOtherIssueOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtDocOtherIssueOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtDocOtherExpOn.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtDocOtherExpOn.ClientID%>').value) == false) {
                     document.getElementById('<%=txtDocOtherExpOn.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtJoinDate.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtJoinDate.ClientID%>').value) == false) {
                     document.getElementById('<%=txtJoinDate.ClientID%>').focus()
                    return false;
                }
            }



            if (document.getElementById('<%=txtExitDate.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtExitDate.ClientID%>').value) == false) {
                     document.getElementById('<%=txtExitDate.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtFrom1.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtFrom1.ClientID%>').value) == false) {
                     document.getElementById('<%=txtFrom1.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtFrom2.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtFrom2.ClientID%>').value) == false) {
                     document.getElementById('<%=txtFrom2.ClientID%>').focus()
                    return false;
                }
            }
            if (document.getElementById('<%=txtFrom3.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtFrom3.ClientID%>').value) == false) {
                     document.getElementById('<%=txtFrom3.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtTo1.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtTo1.ClientID%>').value) == false) {
                     document.getElementById('<%=txtTo1.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtTo2.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtTo2.ClientID%>').value) == false) {
                     document.getElementById('<%=txtTo2.ClientID%>').focus()
                    return false;
                }
            }
            if (document.getElementById('<%=txtTo3.ClientID%>').value != "") {
            if (isDate(document.getElementById('<%=txtTo3.ClientID%>').value) == false) {
                     document.getElementById('<%=txtTo3.ClientID%>').focus()
                    return false;
                }
            }


        }

        function AgeCalculation() {
            document.getElementById("<%=txtAge.ClientID%>").value = 0;

             if (document.getElementById("<%=txtDOB.ClientID%>").value != "__/__/____") {
                 var dob = document.getElementById("<%=txtDOB.ClientID%>").value;
                document.getElementById("<%=txtAge.ClientID%>").value = 0;
                var arrDOB = dob.split('/');
                var currentyear = document.getElementById("<%=hidYear.ClientID%>").value

                var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
                var arrTodayDate1 = TodayDate.split('/');


                if (arrDOB.length == 3 && dob.length == 10) {

                    if (currentyear != arrDOB[2]) {
                        var age = currentyear - arrDOB[2];

                        document.getElementById("<%=txtAge.ClientID%>").value = age;

                    }
                }

            }

        }

        function AsyncFileUpload1_uploadComplete(sender, args) {

            var imageDisplay = document.getElementById("<%= imgStaffPhoto.ClientID %>");
            var img = new Image();
            img.onload = function () {
                imageDisplay.src = img.src;
            };

            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();

        }

        function AsyncFileUpload2_uploadComplete(sender, args) {

            var imageDisplay = document.getElementById("<%= imgStaffSig.ClientID %>");
            var img = new Image();
            img.onload = function () {
                imageDisplay.src = img.src;
            };

            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();

        }

        function DeleteStaffVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strCompCode = document.getElementById('<%=txtStaffId.ClientID%>').value
            if (/\S+/.test(strCompCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Staff Code";
                document.getElementById('<%=txtStaffId.ClientID%>').focus;
                return false;
            }

            var isDelCompany = window.confirm('Do you want to delete Staff Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }

    
    function ShowErrorMessage(vMessage1, vMessage2) {

        document.getElementById("divMessage").style.display = 'block';

        document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
             document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
         }

    function HideErrorMessage() {

        document.getElementById("divMessage").style.display = 'none';

        document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
        document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';

    }
</script>
     </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <input type="hidden" id="hidPermission" runat="server" value="9" />

    
     <div style="padding-left: 60%; width: 100%;">

               
                <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999;  border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">
               
                      <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                              <span style="float:right;"> 
                      
                                  <input type="button" id="Button1" class="ButtonStyle" style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideErrorMessage()" />

                              </span>
                            <br />
                           <div style="width:100%;height:20px;background:#E3E3E3;">
                                 &nbsp; <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold;color:#02a0e0;font-weight:bold;"></asp:Label>
                           </div>
                                  &nbsp; <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold;color:#666;"></asp:Label>
                            
                    </ContentTemplate>
                                </asp:UpdatePanel>

                    <br />

                </div>
            </div>
 


         <table style="width: 80%;">
            <tr>
                <td align="right">
                   
              
                    <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="100px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return DeleteStaffVal();" />
                    <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="100px" OnClick="btnClear_Click" Text="Clear" />

               
                </td>
            </tr>
        </table>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="TextBoxStyle"    ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div style="padding: 5px; width: 80%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
                <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Staff Profile - 1 " Width="100%">
                    <ContentTemplate>
                        <asp:HiddenField ID="hidYear" runat="server" />
                        <asp:HiddenField ID="hidTodayDate" runat="server" />
                        <asp:HiddenField ID="hidImgFront" runat="server" />
                        <fieldset>
                            <legend class="label">General Details
                            </legend>
                            <table width="100%" border="0">
                                <tr>
                                    <td class="label" style="height: 30px; width: 15%"></td>
                                    <td style="width: 20%">
                                        <asp:CheckBox ID="chkManual" runat="server" CssClass="TextBoxStyle"   Text="Manual" AutoPostBack="True" OnCheckedChanged="chkManual_CheckedChanged" />
                                    </td>
                                    <td style="width: 15%"></td>
                                    <td style="width: 20%"></td>
                                    <td style="width: 15%"></td>
                                    <td valign="middle" class="auto-style5" rowspan="4" style="width: 15%">
                                        <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                            <ContentTemplate>
                                                <asp:Image ID="imgStaffPhoto" runat="server" Height="120px" Width="150px" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:AsyncFileUpload ID="AsyncFileUpload1" runat="server"
                                            OnUploadedComplete="AsyncFileUpload1_UploadedComplete"
                                            OnClientUploadComplete="AsyncFileUpload1_uploadComplete"
                                            Width="225px" FailedValidation="False" />


                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">Staff ID  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStaffId" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10" ondblclick="return StaffPopup('StaffId',this.value);" AutoPostBack="True" OnTextChanged="txtStaffId_TextChanged"></asp:TextBox>

                                    </td>
                                    <td class="label">Category  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpCategory" runat="server" CssClass="TextBoxStyle"   Width="155px"  ></asp:DropDownList>

                                    </td>

                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">Serv.Category  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpServCategory" CssClass="TextBoxStyle"   runat="server" Width="155px" ></asp:DropDownList>

                                    </td>
                                    <td class="label">Department  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpDepartment" runat="server" CssClass="TextBoxStyle"   Width="155px" ></asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>

                                    <td class="label" style="height: 30px;">Status  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpStatus" CssClass="TextBoxStyle"   runat="server" Width="155px" >
                                            <asp:ListItem Selected="True" Text="--- Select ---" />
                                            <asp:ListItem Value="Present">Present</asp:ListItem>
                                            <asp:ListItem Value="Vacation">Vacation</asp:ListItem>
                                            <asp:ListItem Value="Meeting">Meeting</asp:ListItem>
                                            <asp:ListItem Value="On Exit">On Exit</asp:ListItem>
                                            <asp:ListItem Value="Other">Other</asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                    <td class="label">Designation  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpDesignation" CssClass="TextBoxStyle"   runat="server" Width="155px"  ></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">First Name  <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFName" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"   ondblclick="return StaffPopup('Name',this.value);"></asp:TextBox>
                                    </td>
                                    <td class="label" style="height: 30px;">Middle Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMName" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"   ondblclick="return StaffPopup('Name',this.value);"></asp:TextBox>
                                    </td>
                                    <td class="label" style="height: 30px;">Last Name <span style="color: red; font-size: 11px;">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLName" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"   ondblclick="return StaffPopup('Name',this.value);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">DOB
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDOB" runat="server" Width="150px" CssClass="TextBoxStyle"    MaxLength="10" onblur="AgeCalculation()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtDOB" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="True" TargetControlID="txtDOB" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>

                                    </td>
                                    <td class="label" style="height: 30px;">Age
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAge" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </td>
                                    <td class="label" style="height: 30px;">Sex
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpSex" runat="server" CssClass="TextBoxStyle"   Width="155px"  >
                                            <asp:ListItem Text="--- Select --- " Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                            <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                        </asp:DropDownList>







                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">Martiial Status
                                    </td>
                                    <td class="auto-style36">
                                        <asp:DropDownList ID="drpMStatus" CssClass="TextBoxStyle"   Width="155px"   runat="server">
                                            <asp:ListItem Selected="True" Text="--- Select ---" />
                                            <asp:ListItem>Married</asp:ListItem>
                                            <asp:ListItem>Unmarried</asp:ListItem>
                                            <asp:ListItem>Divorced</asp:ListItem>
                                            <asp:ListItem>Widowed</asp:ListItem>
                                        </asp:DropDownList>







                                    </td>

                                    <td class="label" style="height: 30px;">No. of Children
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtChildren" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>








                                    </td>
                                    <td class="label" style="height: 30px;">Religion
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpReligion" runat="server" CssClass="TextBoxStyle"   Width="155px"  ></asp:DropDownList>







                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">Blood Group
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpBlood" CssClass="TextBoxStyle"   Width="155px"   runat="server"></asp:DropDownList>







                                    </td>
                                    <td class="label" style="height: 30px;">Edu. Quali
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEduQuali" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>








                                    </td>
                                    <td class="label" style="height: 30px;">PO Box
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPOBox" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>








                                    </td>

                                </tr>
                                <tr>
                                    <td class="label" valign="top" style="height: 30px;" rowspan="3">Address
                                    </td>
                                    <td rowspan="3">
                                        <asp:TextBox ID="txtAddress" CssClass="TextBoxStyle"   runat="server" TextMode="MultiLine" Width="155px" Height="120px" MaxLength="50"  ></asp:TextBox>







                                    </td>
                                    <td class="label" style="height: 30px;">Country
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpCountry" runat="server" CssClass="TextBoxStyle"   Width="155px"  ></asp:DropDownList>







                                    </td>
                                    <td class="label" style="height: 30px;">State
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpState" runat="server" CssClass="TextBoxStyle"   Width="155px"  ></asp:DropDownList>







                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">City
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpCity" runat="server" CssClass="TextBoxStyle"   Width="155px"  ></asp:DropDownList>


                                    </td>
                                    <td class="label" style="height: 30px;">Nationality
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle"   Width="155px"  ></asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">Phone (R)
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhone1" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                    </td>
                                    <td class="label" style="height: 30px;">Phone-2
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPhone2" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>


                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" style="height: 30px;">Fax
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFax" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                    </td>
                                    <td class="label" style="height: 30px;">Mobile
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMobile" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                    </td>
                                    <td class="label" style="height: 30px;">Email
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                    </td>

                                </tr>
                            </table>
                        </fieldset>

                    </ContentTemplate>







                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Staff Profile - 2 " Width="100%">
                    <ContentTemplate>
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Documents Details
                                        </legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="width: 10%;"></td>
                                                <td class="label" style="width: 25%;">No</td>
                                                <td class="label" style="width: 10%;"></td>
                                                <td class="label" style="width: 25%;">Issueed On</td>
                                                <td class="label" style="width: 10%;"></td>
                                                <td class="label" style="width: 20%;">Expired On</td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%">Passport
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:TextBox ID="txtPassport" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>
                                                <td style="height: 30px; width: 10%;"></td>
                                                <td class="label" style="height: 30px; width: 25%;">
                                                    <asp:TextBox ID="txtPassIssueOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender9" runat="server" Enabled="True" TargetControlID="txtPassIssueOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender16" runat="server" Enabled="true" TargetControlID="txtPassIssueOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td style="height: 30px; width: 10%;"></td>
                                                <td style="width: 20%">
                                                    <asp:TextBox ID="txtPassExpOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender10" runat="server" Enabled="True" TargetControlID="txtPassExpOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender17" runat="server" Enabled="true" TargetControlID="txtPassExpOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%">ID
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtID" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td class="label" style="height: 30px;">
                                                    <asp:TextBox ID="txtIDIssueOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender11" runat="server" Enabled="True" TargetControlID="txtIDIssueOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender18" runat="server" Enabled="true" TargetControlID="txtIDIssueOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td>
                                                    <asp:TextBox ID="txtIDExpOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender12" runat="server" Enabled="True" TargetControlID="txtIDExpOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender19" runat="server" Enabled="true" TargetControlID="txtIDExpOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">Driv. Licence
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDrivLic" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td class="label" style="height: 30px;">
                                                    <asp:TextBox ID="txtDrivLicIssueOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender13" runat="server" Enabled="True" TargetControlID="txtDrivLicIssueOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender20" runat="server" Enabled="true" TargetControlID="txtDrivLicIssueOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td>
                                                    <asp:TextBox ID="txtDrivLicExpOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender14" runat="server" Enabled="True" TargetControlID="txtDrivLicExpOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender21" runat="server" Enabled="true" TargetControlID="txtDrivLicExpOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">HAAD
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtHAAD" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td class="label" style="height: 30px;">
                                                    <asp:TextBox ID="txtHAADIssueOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender15" runat="server" Enabled="True" TargetControlID="txtHAADIssueOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender22" runat="server" Enabled="true" TargetControlID="txtHAADIssueOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td>
                                                    <asp:TextBox ID="txtHAADExpOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender16" runat="server" Enabled="True" TargetControlID="txtHAADExpOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender23" runat="server" Enabled="true" TargetControlID="txtHAADExpOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">Clinician Login ID
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtClinicianLoginID" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>
                                                <td class="label" style="height: 30px;">Password
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtClinicianLoginPwd" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">Other
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDocOther" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="50"  ></asp:TextBox>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td class="label" style="height: 30px;">
                                                    <asp:TextBox ID="txtDocOtherIssueOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender17" runat="server" Enabled="True" TargetControlID="txtDocOtherIssueOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender24" runat="server" Enabled="true" TargetControlID="txtDocOtherIssueOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td style="height: 30px; width: 100px;"></td>
                                                <td>
                                                    <asp:TextBox ID="txtDocOtherExpOn" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"  ></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender18" runat="server" Enabled="True" TargetControlID="txtDocOtherExpOn" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender25" runat="server" Enabled="true" TargetControlID="txtDocOtherExpOn" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Salary Details</legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%">Basic
                                                </td>
                                                <td class="label" style="height: 30px; width: 25%">
                                                    <asp:TextBox ID="txtBasic" CssClass="TextBoxStyle"   runat="server" Style="text-align: right;" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%">HRA
                                                </td>
                                                <td class="label" style="height: 30px; width: 25%">
                                                    <asp:TextBox ID="txtHRA" CssClass="TextBoxStyle"   runat="server" Style="text-align: right;" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%">DA
                                                </td>
                                                <td class="label" style="height: 30px; width: 20%">
                                                    <asp:TextBox ID="txtDA" CssClass="TextBoxStyle"   runat="server" Style="text-align: right;" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="label" style="height: 30px;">Other Allowance
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOtherAllow" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px;">Vacation Pay
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVacationPay" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px;">Assured Commission
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAssuredComm" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">Working Hours
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtWorkingHours" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px;">Friday Compensi.
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFriComp" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px;">Fixed OT
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFixedOT" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">OT Per Hour
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOTPerHour" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px;">Absent Per hour
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAbsePerHour" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px;">Advance Salary
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAdvSalary" CssClass="TextBoxStyle"   Style="text-align: right;" runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px;">Duty Roster
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpDutyRoaster" runat="server" CssClass="TextBoxStyle"   Width="155px"  >
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="label" style="height: 30px;">Commission
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="drpCommission" runat="server" CssClass="TextBoxStyle"   Width="155px"  >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Loan Details</legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%">Loan
                                                </td>
                                                <td class="label" style="height: 30px; width: 25%">
                                                    <asp:TextBox ID="txtLoan" CssClass="TextBoxStyle"   runat="server" Style="text-align: right;" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%">No. Of Installment
                                                </td>
                                                <td class="label" style="height: 30px; width: 25%">
                                                    <asp:TextBox ID="txtNoInstallment" CssClass="TextBoxStyle"   runat="server" Style="text-align: right;" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%">Loan Deduct
                                                </td>
                                                <td class="label" style="height: 30px; width: 20%">
                                                    <asp:TextBox ID="txtloanDeduct" CssClass="TextBoxStyle"   runat="server" Style="text-align: right;" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                            </tr>

                                        </table>

                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Join and Exit details</legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%">Join Date
                                                </td>
                                                <td class="label" style="height: 30px; width: 25%">
                                                    <asp:TextBox ID="txtJoinDate" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender7" runat="server" Enabled="True" TargetControlID="txtJoinDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender14" runat="server" Enabled="true" TargetControlID="txtJoinDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td class="label" style="height: 30px; width: 10%">Date of Exit
                                                </td>
                                                <td class="label" style="height: 30px; width: 25%">
                                                    <asp:TextBox ID="txtExitDate" CssClass="TextBoxStyle"   runat="server" Width="150px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender8" runat="server" Enabled="True" TargetControlID="txtExitDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender15" runat="server" Enabled="true" TargetControlID="txtExitDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                </td>
                                                <td class="label" style="height: 30px; width: 10%"></td>
                                                <td class="label" style="height: 30px; width: 20%"></td>


                                            </tr>

                                        </table>
                                    </fieldset>

                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>







                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel2" HeaderText=" Staff Profile - 3 " Width="100%">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Appointment Details
                                        </legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 100px;">Interval
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtInterval" runat="server" CssClass="TextBoxStyle"   Style="text-align: right;" Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Blocked Dates</legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%;">From1
                                                </td>
                                                <td style="height: 30px; width: 25%;">
                                                    <asp:TextBox ID="txtFrom1" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                                        Enabled="True" TargetControlID="txtFrom1" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtFrom1" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                    <asp:TextBox ID="txtFrom1Time" runat="server" CssClass="TextBoxStyle"   Width="30px" MaxLength="5"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtFrom1Time" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%;">From 2
                                                </td>
                                                <td style="height: 30px; width: 25%;">

                                                    <asp:TextBox ID="txtFrom2" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                                        Enabled="True" TargetControlID="txtFrom2" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtFrom2" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                    <asp:TextBox ID="txtFrom2Time" runat="server" CssClass="TextBoxStyle"   Width="30px" MaxLength="5"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtFrom2Time" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%;">From 3
                                                </td>
                                                <td style="height: 30px; width: 20%;">

                                                    <asp:TextBox ID="txtFrom3" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                                        Enabled="True" TargetControlID="txtFrom3" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtFrom3" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                    <asp:TextBox ID="txtFrom3Time" runat="server" CssClass="TextBoxStyle"   Width="30px" MaxLength="5"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="true" TargetControlID="txtFrom3Time" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px; width: 100px;">To 1
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTo1" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                                                        Enabled="True" TargetControlID="txtTo1" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender8" runat="server" Enabled="true" TargetControlID="txtTo1" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                    <asp:TextBox ID="txtTo1Time" runat="server" CssClass="TextBoxStyle"   Width="30px" MaxLength="5"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender9" runat="server" Enabled="true" TargetControlID="txtTo1Time" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">To 2
                                                </td>
                                                <td>

                                                    <asp:TextBox ID="txtTo2" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender5" runat="server"
                                                        Enabled="True" TargetControlID="txtTo2" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender10" runat="server" Enabled="true" TargetControlID="txtTo2" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                    <asp:TextBox ID="txtTo2Time" runat="server" CssClass="TextBoxStyle"   Width="30px" MaxLength="5"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender11" runat="server" Enabled="true" TargetControlID="txtTo2Time" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">To 3
                                                </td>
                                                <td>

                                                    <asp:TextBox ID="txtTo3" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender6" runat="server"
                                                        Enabled="True" TargetControlID="txtTo3" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender12" runat="server" Enabled="true" TargetControlID="txtTo3" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                    <asp:TextBox ID="txtTo3Time" runat="server" CssClass="TextBoxStyle"   Width="30px" MaxLength="5"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender13" runat="server" Enabled="true" TargetControlID="txtTo3Time" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>

                                                </td>
                                            </tr>

                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Token Details</legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%;">Token Prefix
                                                </td>
                                                <td style="height: 30px; width: 25%;">
                                                    <asp:TextBox ID="txtTokenPrefix" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"  ></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%;">Token Start
                                                </td>
                                                <td style="height: 30px; width: 25%;">
                                                    <asp:TextBox ID="txtTokenStart" Style="text-align: right;" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%;">Token End
                                                </td>
                                                <td style="height: 30px; width: 20%;">
                                                    <asp:TextBox ID="txtTokenEnd" Style="text-align: right;" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>


                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px; width: 100px;">Token Current
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTokenCurrent" Style="text-align: right;" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"  ></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">Token Reserve
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTokenReserve" Style="text-align: right;" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">Room
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRoom"  runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   ></asp:TextBox>
                                                </td>


                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="label">Auto Invoice Generation</legend>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%;">Service Code
                                                </td>
                                                <td style="height: 30px; width: 25%;">
                                                    <asp:TextBox ID="txtServiceCode" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"  ></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%;">Fee
                                                </td>
                                                <td style="height: 30px; width: 25%x;">
                                                    <asp:TextBox ID="txtFee" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 10%;">Ist Follow Up
                                                </td>
                                                <td style="height: 30px; width: 20%;">
                                                    <asp:TextBox ID="txtFollowUp1" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>


                                            </tr>
                                            <tr>
                                                <td class="label" style="height: 30px; width: 100px;">IInd Followup
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">
                                                    <asp:TextBox ID="txtFollowUp2" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"  ></asp:TextBox>
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">Other -1
                                                </td>
                                                <td class="label" style="height: 30px; width: 100px;">
                                                    <asp:TextBox ID="txtAutoInvOther" runat="server" CssClass="TextBoxStyle"   Width="75px" MaxLength="10"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                </td>
                                                <td class="label" colspan="2">Enter Follow up as FromDay - toDay- Fee (Eg. 1-7-75)
                                                </td>


                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset style="height: 100px;">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td class="label" style="height: 30px; width: 10%;">Speciality <span style="color: red; font-size: 11px;">*</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpSpeciality" CssClass="TextBoxStyle"     runat="server" Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnCommHist" runat="server" CssClass="button gray small" Width="150px" Text="Commission History" />
                                                    <asp:Button ID="btnServHist" runat="server" CssClass="button gray small" Width="150px" Text="Service History" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="label" style="height: 50px; width: 100px;">EMR Department
                                                </td>
                                                <td>
                                                    <asp:ListBox ID="lstEMRDept" runat="server" CssClass="TextBoxStyle"   Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                                </td>
                                                <td class="label" style="font-weight: bold;">EMR View Category        
                                                    <br />
                                                    <asp:CheckBox ID="chkEMRViewDate" runat="server" CssClass="TextBoxStyle"   Width="100px" Text="Date" Checked="false" />
                                                    <asp:CheckBox ID="chkEMRViewDept" runat="server" CssClass="TextBoxStyle"   Width="100px" Text="Department" Checked="false" />
                                                    <asp:CheckBox ID="chkEMRViewCat" runat="server" CssClass="TextBoxStyle"   Width="100px" Text="Category" Checked="false" />

                                                </td>

                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>


                    </ContentTemplate>


                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel3" HeaderText=" Miscellaneous " Width="100%">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="label" style="text-align: center">Signature
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" class="auto-style5">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgStaffSig" runat="server" Height="120px" Width="250px" />


                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <asp:AsyncFileUpload ID="AsyncFileUpload2" runat="server"
                                        OnUploadedComplete="AsyncFileUpload2_UploadedComplete"
                                        OnClientUploadComplete="AsyncFileUpload2_uploadComplete"
                                        Width="225px" FailedValidation="False" />

                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>








                </asp:TabPanel>

            </asp:TabContainer>
        </div>
        <br />
         
 </asp:Content>
