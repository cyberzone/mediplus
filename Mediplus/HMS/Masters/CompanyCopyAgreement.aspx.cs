﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class CompanyCopyAgreement : System.Web.UI.Page
    {
        # region Variable Declaration
        static string strSessionBranchId;

        DataSet DS = new DataSet();
        DataSet DS1 = new DataSet();
        dboperations dbo = new dboperations();


        #endregion

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_NAME"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            // lblStatus.Text = "";
            if (!IsPostBack)
            {
                try
                {
                    // this.Page.Form.Enctype = "multipart/form-data";

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);


                    ViewState["CompanyId"] = Request.QueryString["CompanyId"].ToString();
                    ViewState["CompanyName"] = Request.QueryString["CompanyName"].ToString();


                    txtCompanyTo.Text = ViewState["CompanyId"].ToString();

                    txtCompanyNameTo.Text = ViewState["CompanyName"].ToString();




                }
                catch (Exception ex)
                {
                    Session["ErrorMsg"] = ex.Message;
                    Response.Redirect("ErrorPage.aspx");
                }
            }
        }

        protected void btnCopy_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCompanyFrom.Text.Trim() == txtCompanyTo.Text.Trim())
                {
                    lblStatus.Text = "Company From and Company To are same. Please select another company.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;

                    goto FunEnd;
                }

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

               
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_CompanyCopyAgreement";
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_ID_FROM", SqlDbType.VarChar)).Value = txtCompanyFrom.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HCM_COMP_ID_TO", SqlDbType.VarChar)).Value =txtCompanyTo.Text.Trim();
                 cmd.Parameters.Add(new SqlParameter("@HCM_COMP_NAME_TO", SqlDbType.VarChar)).Value =txtCompanyNameTo.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@IsCopyAggrement", SqlDbType.VarChar)).Value = chkCopyAgrement.Checked;
                cmd.Parameters.Add(new SqlParameter("@IsCopyBenefits", SqlDbType.VarChar)).Value = chkCopyBenefits.Checked;
                
                cmd.ExecuteNonQuery();
                con.Close();

                lblStatus.Text = "Agreement Copied Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CompanyProfile.btnCopyAgreement_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }


        #endregion
    }
}