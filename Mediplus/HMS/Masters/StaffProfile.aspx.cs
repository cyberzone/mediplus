﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.Data;
using System.IO;


namespace Mediplus.HMS.Masters
{
    public partial class StaffProfile : System.Web.UI.Page
    {
        DataSet DS;
        dboperations dbo = new dboperations();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindCategory()
        {

            DS = new DataSet();
            string Criteria = " 1=1 ";
            CommonBAL objComBAL = new CommonBAL();
            DS = objComBAL.GetStaffCategory(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "Name";
                drpCategory.DataValueField = "Code";
                drpCategory.DataBind();

            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "0";
        }

        void BindReligion()
        {

            DS = new DataSet();
            string Criteria = " 1=1 ";
            CommonBAL objComBAL = new CommonBAL();
            DS = objComBAL.GetReligion(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpReligion.DataSource = DS;
                drpReligion.DataTextField = "Name";
                drpReligion.DataValueField = "Code";
                drpReligion.DataBind();
            }
            drpReligion.Items.Insert(0, "--- Select ---");
            drpReligion.Items[0].Value = "0";

        }

        void BindBloodGroup()
        {

            DS = new DataSet();
            string Criteria = " 1=1 ";
            CommonBAL objComBAL = new CommonBAL();
            DS = objComBAL.GetBloodGroup(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpBlood.DataSource = DS;
                drpBlood.DataTextField = "Name";
                drpBlood.DataValueField = "Code";
                drpBlood.DataBind();

            }

            drpBlood.Items.Insert(0, "--- Select ---");
            drpBlood.Items[0].Value = "0";
        }

        void BindServCategory()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSCM_STATUS='A' AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.ServiceCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpServCategory.DataSource = DS;
                drpServCategory.DataTextField = "HSCM_NAME";
                drpServCategory.DataValueField = "HSCM_CAT_ID";
                drpServCategory.DataBind();
            }
            drpServCategory.Items.Insert(0, "--- Select ---");
            drpServCategory.Items[0].Value = "0";

        }

        void BindRosterMaster()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HROS_STATUS='A'  AND  HROS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.RosterMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDutyRoaster.DataSource = DS;
                drpDutyRoaster.DataTextField = "HROS_NAME";
                drpDutyRoaster.DataValueField = "HROS_ROSTER_ID";
                drpDutyRoaster.DataBind();
            }
            drpDutyRoaster.Items.Insert(0, "--- Select ---");
            drpDutyRoaster.Items[0].Value = "0";

        }

        void BindDepartment()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' ";
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataTextField = "HDM_DEP_ID";
                drpDepartment.DataBind();


                lstEMRDept.DataSource = ds;
                lstEMRDept.DataTextField = "HDM_DEP_NAME";
                lstEMRDept.DataValueField = "HDM_DEP_ID";
                lstEMRDept.DataBind();

            }
            drpDepartment.Items.Insert(0, "--- Select ---");
            drpDepartment.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();
        }

        void BindDesignation()
        {

            DS = new DataSet();
            string Criteria = " 1=1 ";
            CommonBAL objComBAL = new CommonBAL();
            DS = objComBAL.GetStaffDesignation(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDesignation.DataSource = DS;
                drpDesignation.DataTextField = "HSDM_NAME";
                drpDesignation.DataValueField = "HSDM_NAME";
                drpDesignation.DataBind();

            }
            drpDesignation.Items.Insert(0, "--- Select ---");
            drpDesignation.Items[0].Value = "0";

        }

        void BindState()
        {

            DS = new DataSet();
            string Criteria = " 1=1 ";
            CommonBAL objComBAL = new CommonBAL();
            DS = objComBAL.GetStateMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpState.DataSource = DS;
                drpState.DataTextField = "HMSS_STATE_NAME";
                drpState.DataValueField = "HMSS_STATE_ID";
                drpState.DataBind();

            }
            drpState.Items.Insert(0, "--- Select ---");
            drpState.Items[0].Value = "0";

        }

        void BindCity()
        {
            DataSet ds = new DataSet();
            ds = dbo.city();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCity.DataSource = ds;
                drpCity.DataValueField = "HCIM_NAME";
                drpCity.DataTextField = "HCIM_NAME";
                drpCity.DataBind();
            }
            drpCity.Items.Insert(0, "--- Select ---");
            drpCity.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();

        }

        void BindCountry()
        {
            DataSet ds = new DataSet();
            ds = dbo.country();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCountry.DataSource = ds;
                drpCountry.DataValueField = "HCM_NAME";
                drpCountry.DataTextField = "HCM_NAME";
                drpCountry.DataBind();

            }
            drpCountry.Items.Insert(0, "--- Select ---");
            drpCountry.Items[0].Value = "0";
            ds.Clear();
            ds.Dispose();

        }

        void BindNationality()
        {
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "HNM_NAME";
                drpNationality.DataTextField = "HNM_NAME";
                drpNationality.DataBind();


            }
            drpNationality.Items.Insert(0, "--- Select ---");
            drpNationality.Items[0].Value = "0";



            ds.Clear();
            ds.Dispose();

        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.CommonMastersGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (strType == "Specialty")
                {
                    drpSpeciality.DataSource = DS;
                    drpSpeciality.DataTextField = "HCM_DESC";
                    drpSpeciality.DataValueField = "HCM_CODE";
                    drpSpeciality.DataBind();
                }




            }
            if (strType == "Specialty")
            {
                drpSpeciality.Items.Insert(0, "--- Select ---");
                drpSpeciality.Items[0].Value = "";
            }

        }

        ////void BindSpeciality()
        ////{
        ////    DS = new DataSet();
        ////    string Criteria = " 1=1 ";
        ////    CommonBAL objComBAL = new CommonBAL();
        ////    DS = objComBAL.GetSpeciality(Criteria);

        ////    if (DS.Tables[0].Rows.Count > 0)
        ////    {
        ////        drpSpeciality.DataSource = DS;
        ////        drpSpeciality.DataTextField = "HDS_DESCRIPTION";
        ////        drpSpeciality.DataValueField = "HDS_CODE";
        ////        drpSpeciality.DataBind();

        ////    }
        ////    drpSpeciality.Items.Insert(0, "--- Select ---");
        ////    drpSpeciality.Items[0].Value = "0";

        ////}

        void BindCommission()
        {

            DS = new DataSet();
            string Criteria = " 1=1 AND  HCO_STATUS='A'";
            CommonBAL objComBAL = new CommonBAL();
            DS = objComBAL.GetCommission(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCommission.DataSource = DS;
                drpCommission.DataTextField = "HCO_ID";
                drpCommission.DataValueField = "HCO_ID";
                drpCommission.DataBind();

            }
            drpCommission.Items.Insert(0, "--- Select ---");
            drpCommission.Items[0].Value = "0";

        }

        void Clear()
        {
            chkManual.Checked = false;
            if (drpCategory.Items.Count > 0)
                drpCategory.SelectedIndex = 0;
            if (drpServCategory.Items.Count > 0)
                drpServCategory.SelectedIndex = 0;
            if (drpDepartment.Items.Count > 0)
                drpDepartment.SelectedIndex = 0;
            if (drpStatus.Items.Count > 0)
                drpStatus.SelectedIndex = 0;
            if (drpDesignation.Items.Count > 0)
                drpDesignation.SelectedIndex = 0;

            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";
            txtDOB.Text = "";
            txtAge.Text = "";
            if (drpSex.Items.Count > 0)
                drpSex.SelectedIndex = 0;
            if (drpMStatus.Items.Count > 0)
                drpMStatus.SelectedIndex = 0;

            txtChildren.Text = "";

            if (drpReligion.Items.Count > 0)
                drpReligion.SelectedIndex = 0;
            if (drpBlood.Items.Count > 0)
                drpBlood.SelectedIndex = 0;

            txtEduQuali.Text = "";
            txtPOBox.Text = "";
            txtAddress.Text = "";
            if (drpCountry.Items.Count > 0)
                drpCountry.SelectedIndex = 0;
            if (drpState.Items.Count > 0)
                drpState.SelectedIndex = 0;
            if (drpCity.Items.Count > 0)
                drpCity.SelectedIndex = 0;
            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;


            txtPhone1.Text = "";
            txtPhone2.Text = "";
            txtFax.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
            txtPassport.Text = "";
            txtPassIssueOn.Text = "";
            txtPassExpOn.Text = "";
            txtID.Text = "";
            txtIDIssueOn.Text = "";
            txtIDExpOn.Text = "";
            txtDrivLic.Text = "";
            txtDrivLicIssueOn.Text = "";
            txtDrivLicExpOn.Text = "";
            txtHAAD.Text = "";
            txtHAADIssueOn.Text = "";
            txtHAADExpOn.Text = "";
            txtDocOther.Text = "";
            txtDocOtherIssueOn.Text = "";
            txtDocOtherExpOn.Text = "";
            txtBasic.Text = "";
            txtHRA.Text = "";
            txtDA.Text = "";
            txtOtherAllow.Text = "";
            txtVacationPay.Text = "";
            txtAssuredComm.Text = "";
            txtWorkingHours.Text = "";
            txtFriComp.Text = "";
            txtFixedOT.Text = "";
            txtOTPerHour.Text = "";
            txtAbsePerHour.Text = "";
            txtAdvSalary.Text = "";

            if (drpDutyRoaster.Items.Count > 0)
                drpDutyRoaster.SelectedIndex = 0;
            if (drpCommission.Items.Count > 0)
                drpCommission.SelectedIndex = 0;

            txtLoan.Text = "";
            txtNoInstallment.Text = "";
            txtloanDeduct.Text = "";
            txtJoinDate.Text = "";
            txtExitDate.Text = "";
            txtInterval.Text = "";
            txtFrom1.Text = "";
            txtFrom1Time.Text = "";
            txtFrom2.Text = "";
            txtFrom2Time.Text = "";
            txtFrom3.Text = "";
            txtFrom3Time.Text = "";

            txtTo1.Text = "";
            txtTo1Time.Text = "";
            txtTo2.Text = "";
            txtTo2Time.Text = "";
            txtTo3.Text = "";
            txtTo3Time.Text = "";

            txtTokenPrefix.Text = "";
            txtTokenStart.Text = "";
            txtTokenEnd.Text = "";
            txtTokenCurrent.Text = "";
            txtTokenReserve.Text = "";
            txtRoom.Text = "";

            txtServiceCode.Text = "";
            txtFee.Text = "";
            txtFollowUp1.Text = "";

            txtFollowUp2.Text = "";
            txtAutoInvOther.Text = "";

            if (drpSpeciality.Items.Count > 0)
                drpSpeciality.SelectedIndex = 0;

            if (lstEMRDept.Items.Count > 0)
                lstEMRDept.SelectedIndex = -1;
            chkEMRViewDate.Checked = false;
            chkEMRViewDept.Checked = false;
            chkEMRViewCat.Checked = false;
            imgStaffPhoto.ImageUrl = "";
            imgStaffSig.ImageUrl = "";

            txtClinicianLoginID.Text = "";
            txtClinicianLoginPwd.Text = "";

            Session["StaffPhoto"] = "";
            Session["StaffSig"] = "";



        }

        void ModifyStaff()
        {
            DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + txtStaffId.Text.Trim() + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;
                if (DS.Tables[0].Rows[0].IsNull("HSFM_CATEGORY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CATEGORY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CATEGORY"]) != "0")
                {
                    for (int i = 0; i < drpCategory.Items.Count; i++)
                    {
                        if (drpCategory.Items[i].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CATEGORY"]))
                        {
                            drpCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CATEGORY"]);
                            goto ForCategory;
                        }

                    }
                }
            ForCategory: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SRV_CATEGORY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SRV_CATEGORY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SRV_CATEGORY"]) != "0")
                {
                    for (int i = 0; i < drpServCategory.Items.Count; i++)
                    {
                        if (drpServCategory.Items[i].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SRV_CATEGORY"]))
                        {
                            drpServCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SRV_CATEGORY"]);
                            goto ForServCategory;
                        }

                    }
                }
            ForServCategory: ;



                if (DS.Tables[0].Rows[0].IsNull("HSFM_SPECIALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]) != "0")
                {
                    for (int intCount = 0; intCount < drpSpeciality.Items.Count; intCount++)
                    {
                        if (drpSpeciality.Items[intCount].Text == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]))
                        {
                            drpSpeciality.SelectedIndex = intCount;
                            goto ForSpecialityy;
                        }

                    }
                }
            ForSpecialityy: ;



                if (DS.Tables[0].Rows[0].IsNull("HSFM_DEPT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]) != "0")
                {
                    for (int i = 0; i < drpDepartment.Items.Count; i++)
                    {
                        if (drpDepartment.Items[i].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]))
                        {
                            drpDepartment.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]);
                            goto ForDepartment;
                        }

                    }
                }
            ForDepartment: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SF_STATUS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SF_STATUS"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SF_STATUS"]) != "0")
                {
                    for (int i = 0; i < drpStatus.Items.Count; i++)
                    {
                        if (drpStatus.Items[i].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SF_STATUS"]))
                        {
                            drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SF_STATUS"]);
                            goto ForStatus;
                        }

                    }
                }
            ForStatus: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DESIG") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DESIG"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DESIG"]) != "0")
                {
                    for (int i = 0; i < drpDesignation.Items.Count; i++)
                    {
                        if (drpDesignation.Items[i].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DESIG"]))
                        {
                            drpDesignation.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DESIG"]);
                            goto ForDesignation;
                        }

                    }
                }
            ForDesignation: ;




                if (DS.Tables[0].Rows[0].IsNull("HSFM_FNAME") == false)
                    txtFName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MNAME") == false)
                    txtMName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_LNAME") == false)
                    txtLName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DOB") == false)
                    txtDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DOB"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_AGE") == false)
                    txtAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_AGE"]);



                if (DS.Tables[0].Rows[0].IsNull("HSFM_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SEX"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SEX"]) != "0")
                {
                    for (int intCommission = 0; intCommission < drpSex.Items.Count; intCommission++)
                    {
                        if (drpSex.Items[intCommission].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SEX"]))
                        {
                            drpSex.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SEX"]);
                            goto ForSex;
                        }

                    }
                }
            ForSex: ;


                if (DS.Tables[0].Rows[0].IsNull("HSFM_MARITAL") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MARITAL"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MARITAL"]) != "0")
                {
                    for (int intCommission = 0; intCommission < drpMStatus.Items.Count; intCommission++)
                    {
                        if (drpMStatus.Items[intCommission].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MARITAL"]))
                        {
                            drpMStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MARITAL"]);
                            goto ForMStatus;
                        }

                    }
                }
            ForMStatus: ;



                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_CHILD") == false)
                    txtChildren.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_CHILD"]);



                if (DS.Tables[0].Rows[0].IsNull("HSFM_RELIGION") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_RELIGION"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_RELIGION"]) != "0")
                {
                    for (int intCommission = 0; intCommission < drpReligion.Items.Count; intCommission++)
                    {
                        if (drpReligion.Items[intCommission].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_RELIGION"]))
                        {
                            drpReligion.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_RELIGION"]);
                            goto ForReligion;
                        }

                    }
                }
            ForReligion: ;


                if (DS.Tables[0].Rows[0].IsNull("HSFM_BLOODGROUP") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BLOODGROUP"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BLOODGROUP"]) != "0")
                {
                    for (int intCommission = 0; intCommission < drpBlood.Items.Count; intCommission++)
                    {
                        if (drpBlood.Items[intCommission].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BLOODGROUP"]))
                        {
                            drpBlood.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BLOODGROUP"]);
                            goto ForBlood;
                        }

                    }
                }
            ForBlood: ;


                if (DS.Tables[0].Rows[0].IsNull("HSFM_EDU_QLFY") == false)
                    txtEduQuali.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EDU_QLFY"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_POBOX") == false)
                    txtPOBox.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_POBOX"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_ADDR") == false)
                    txtAddress.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ADDR"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_COUNTRY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COUNTRY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COUNTRY"]) != "0")
                {
                    for (int intCountryCount = 0; intCountryCount < drpCountry.Items.Count; intCountryCount++)
                    {
                        if (drpCountry.Items[intCountryCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COUNTRY"]))
                        {
                            drpCountry.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COUNTRY"]);
                            goto ForCountry;
                        }

                    }
                }
            ForCountry: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_STATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STATE"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STATE"]) != "0")
                {
                    for (int intCityCount = 0; intCityCount < drpState.Items.Count; intCityCount++)
                    {
                        if (drpState.Items[intCityCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STATE"]))
                        {
                            drpState.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STATE"]);
                            goto ForState;
                        }

                    }
                }
            ForState: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_CITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CITY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CITY"]) != "0")
                {
                    for (int intCityCount = 0; intCityCount < drpCity.Items.Count; intCityCount++)
                    {
                        if (drpCity.Items[intCityCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CITY"]))
                        {
                            drpCity.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CITY"]);
                            goto ForCity;
                        }

                    }
                }
            ForCity: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NLTY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NLTY"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NLTY"]) != "0")
                {
                    for (int intNatiCount = 0; intNatiCount < drpNationality.Items.Count; intNatiCount++)
                    {
                        if (drpNationality.Items[intNatiCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NLTY"]))
                        {
                            drpNationality.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NLTY"]);
                            goto ForNation;
                        }

                    }
                }
            ForNation: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_PHONE1") == false)
                    txtPhone1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PHONE1"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_PHONE2") == false)
                    txtPhone2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PHONE2"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_FAX") == false)
                    txtFax.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FAX"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOBILE") == false)
                    txtMobile.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOBILE"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_EMAIL") == false)
                    txtEmail.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMAIL"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_PASSPORT") == false)
                    txtPassport.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PASSPORT"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_PASS_ISSUE") == false)
                    txtPassIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PASS_ISSUEDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_PASS_EXPDesc") == false)
                    txtPassExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PASS_EXPDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SS_ID") == false)
                    txtID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SS_ID"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SS_ISSUE") == false)
                    txtIDIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SS_ISSUEDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SS_EXP") == false)
                    txtIDExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SS_EXPDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DRV_LIC") == false)
                    txtDrivLic.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DRV_LIC"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DRV_LIC_ISSUE") == false)
                    txtDrivLicIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DRV_LIC_ISSUEDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DRV_LIC_EXPDesc") == false)
                    txtDrivLicExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DRV_LIC_EXPDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_NO") == false)
                    txtHAAD.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_ISSUE") == false)
                    txtHAADIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_ISSUEDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_EXPDesc") == false)
                    txtHAADExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_EXPDesc"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHER_NO") == false)
                    txtDocOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHER_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHER_ISSUE") == false)
                    txtDocOtherIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHER_ISSUEDesc"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHER_EXP") == false)
                    txtDocOtherExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHER_EXPDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_BASIC") == false)
                    txtBasic.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BASIC"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_HRA") == false)
                    txtHRA.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_HRA"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DA") == false)
                    txtDA.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DA"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHR_ALLOW") == false)
                    txtOtherAllow.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHR_ALLOW"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_VACATIONPAY") == false)
                    txtVacationPay.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_VACATIONPAY"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_HOMETRAVEL") == false)
                    txtAssuredComm.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_HOMETRAVEL"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_WORKINGHRS") == false)
                    txtWorkingHours.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_WORKINGHRS"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_FRI_COMPENSATION") == false)
                    txtFriComp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FRI_COMPENSATION"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_FIXEDOVERTIME") == false)
                    txtFixedOT.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FIXEDOVERTIME"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_OVERTIMEPHR") == false)
                    txtOTPerHour.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OVERTIMEPHR"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_ABSENTPHR") == false)
                    txtAbsePerHour.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ABSENTPHR"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_ADV_SALARY") == false)
                    txtAdvSalary.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ADV_SALARY"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_ROSTER_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]) != "0")
                {
                    for (int intRoaster = 0; intRoaster < drpDutyRoaster.Items.Count; intRoaster++)
                    {
                        if (drpDutyRoaster.Items[intRoaster].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]))
                        {
                            drpDutyRoaster.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]);
                            goto ForRoaster;
                        }

                    }
                }
            ForRoaster: ;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_COMM_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]) != "0")
                {
                    for (int intCommission = 0; intCommission < drpCommission.Items.Count; intCommission++)
                    {
                        if (drpCommission.Items[intCommission].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]))
                        {
                            drpCommission.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]);
                            goto ForCommission;
                        }

                    }
                }
            ForCommission: ;


                if (DS.Tables[0].Rows[0].IsNull("HSFM_LOAN") == false)
                    txtLoan.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LOAN"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NOOFPAYMENT") == false)
                    txtNoInstallment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NOOFPAYMENT"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_LOANDEDUCT") == false)
                    txtloanDeduct.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LOANDEDUCT"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DOJ") == false)
                    txtJoinDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DOJDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DOEXIT") == false)
                    txtExitDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DOEXITDesc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_APNT_TIME_INT") == false)
                    txtInterval.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_APNT_TIME_INT"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM1") == false)
                    txtFrom1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM1Desc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM1TIME") == false)
                    txtFrom1Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM1TIME"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM2") == false)
                    txtFrom2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM2Desc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM2TIME") == false)
                    txtFrom2Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM2TIME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM3") == false)
                    txtFrom3.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM3Desc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM3TIME") == false)
                    txtFrom3Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM3TIME"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO1") == false)
                    txtTo1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO1Desc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO1TIME") == false)
                    txtTo1Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO1TIME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO2") == false)
                    txtTo2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO2Desc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO2TIME") == false)
                    txtTo2Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO2TIME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO3") == false)
                    txtTo3.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO3Desc"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO3TIME") == false)
                    txtTo3Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO3TIME"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_PRE_DRTOKEN") == false)
                    txtTokenPrefix.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PRE_DRTOKEN"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_START") == false)
                    txtTokenStart.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_START"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_END") == false)
                    txtTokenEnd.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_END"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_CURRENT") == false)
                    txtTokenCurrent.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_CURRENT"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_RESERVE") == false)
                    txtTokenReserve.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_RESERVE"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_ROOM") == false)
                    txtRoom.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROOM"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS1") == false)
                    txtServiceCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS1"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS2") == false)
                    txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS2"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS3") == false)
                    txtFollowUp1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS3"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS4") == false)
                    txtFollowUp2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS4"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS5") == false)
                    txtAutoInvOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS5"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_EMR_DATE") == false)
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMR_DATE"]) == "1")
                        chkEMRViewDate.Checked = true;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_EMR_DEP") == false)
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMR_DEP"]) == "1")
                        chkEMRViewDept.Checked = true;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_EMR_CAT") == false)
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMR_CAT"]) == "1")
                        chkEMRViewCat.Checked = true;

                if (DS.Tables[0].Rows[0].IsNull("HSFM_CLINICIAN_LOGIN") == false)
                    txtClinicianLoginID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CLINICIAN_LOGIN"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_CLINICIAN_PWD") == false)
                    txtClinicianLoginPwd.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CLINICIAN_PWD"]);


                BindEMRDocDept();
                BindPhoto();
            }

        }

        void BindEMRDocDept()
        {

            CommonBAL objComm = new CommonBAL();

            DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HEDD_DR_CODE='" + txtStaffId.Text.Trim() + "'";

            DS = objComm.GetEMRDoctorDep(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int intCount = 0; intCount <= DS.Tables[0].Rows.Count - 1; intCount++)
                {
                    for (int i = 0; i < lstEMRDept.Items.Count; i++)
                    {
                        if (lstEMRDept.Items[i].Value == Convert.ToString(DS.Tables[0].Rows[intCount]["HEDD_DEP_CODE"]))
                        {
                            lstEMRDept.Items[i].Selected = true;
                            goto lstEnd;
                        }

                    }
                lstEnd: ;

                }
            }


        }


        void BindPhoto()
        {
            dboperations dbo = new dboperations();
            string Criteria = " 1=1  ";
            Criteria += " AND HSP_STAFF_ID='" + txtStaffId.Text.Trim() + "'";

            DS = dbo.StaffPhotoGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSP_PHOTO") == false)
                {
                    Session["SignatureImg1"] = (Byte[])DS.Tables[0].Rows[0]["HSP_PHOTO"];
                    imgStaffPhoto.Visible = true;
                    imgStaffPhoto.ImageUrl = "../DisplaySignature1.aspx";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSP_Signature") == false)
                {
                    Session["SignatureImg2"] = (Byte[])DS.Tables[0].Rows[0]["HSP_Signature"];
                    imgStaffSig.Visible = true;
                    imgStaffSig.ImageUrl = "../DisplaySignature2.aspx";
                }



            }

        }

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "STAFF";
            objCom.ScreenName = "Staff Profile";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }


        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='STAFF' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                btnClear.Enabled = false;

                AsyncFileUpload1.Enabled = false;

            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Staff Profile Page");
                try
                {
                    Session["StaffPhoto"] = "";
                    Session["StaffSign"] = "";
                    ViewState["NewFlag"] = true;
                    hidYear.Value = DateTime.Now.Year.ToString();
                    hidTodayDate.Value = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtStaffId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "STAFF");

                    BindCategory();
                    BindServCategory();
                    BindDepartment();
                    BindDesignation();
                    BindReligion();
                    BindBloodGroup();
                    BindRosterMaster();
                    BindState();
                    BindCountry();
                    BindCity();
                    BindNationality();
                    //BindSpeciality();

                    BindCommonMaster("Specialty");


                    BindCommission();

                    string StaffId = "";

                    StaffId = Convert.ToString(Request.QueryString["StaffId"]);

                    if (StaffId != " " && StaffId != null)
                    {
                        txtStaffId.Text = Convert.ToString(StaffId);
                        ModifyStaff();

                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      StaffProfile.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void chkManual_CheckedChanged(object sender, EventArgs e)
        {
            if (chkManual.Checked == true)
            {
                txtStaffId.Text = "";
            }
            else
            {
                txtStaffId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "STAFF");
            }

        }

        protected void txtStaffId_TextChanged(object sender, EventArgs e)
        {
            try
            {

                ModifyStaff();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      StaffProfile.txtStaffId_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFName.Text.Trim()  == "")
                {
                    lblStatus.Text = "Enter First Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }

                if (txtLName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Last Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }
                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {
                    if (drpSpeciality.SelectedIndex == 0)
                    {
                        lblStatus.Text = "Select Speciality";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;

                    }
                }

                StaffMasterBAL objStaff = new StaffMasterBAL();
                objStaff.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objStaff.STAFF_ID = txtStaffId.Text.Trim();
                objStaff.FNAME = txtFName.Text.Trim();
                objStaff.MNAME = txtMName.Text.Trim();
                objStaff.LNAME = txtLName.Text.Trim();
                objStaff.CATEGORY = drpCategory.SelectedValue;
                objStaff.DEPT_ID = drpDepartment.SelectedValue;
                objStaff.SF_STATUS = drpStatus.SelectedValue;
                objStaff.POBOX = txtPOBox.Text.Trim();
                objStaff.ADDR = txtAddress.Text.Trim();
                objStaff.CITY = drpCity.SelectedValue;
                objStaff.COUNTRY = drpCountry.SelectedValue;
                objStaff.STATE = drpState.SelectedValue;
                objStaff.EDU_QLFY = txtEduQuali.Text.Trim();
                objStaff.DESIG = drpDesignation.SelectedValue;
                objStaff.DOB = txtDOB.Text.Trim();
                objStaff.AGE = txtAge.Text.Trim();
                objStaff.RELIGION = drpReligion.SelectedValue;
                objStaff.SEX = drpSex.SelectedValue;
                objStaff.MARITAL = drpMStatus.SelectedValue;
                objStaff.NO_CHILD = txtChildren.Text.Trim();
                objStaff.BLOODGROUP = drpBlood.SelectedValue;
                objStaff.NLTY = drpNationality.SelectedValue;
                objStaff.PHONE1 = txtPhone1.Text.Trim();
                objStaff.PHONE2 = txtPhone2.Text.Trim();
                objStaff.FAX = txtFax.Text.Trim();
                objStaff.MOBILE = txtMobile.Text.Trim();
                objStaff.EMAIL = txtEmail.Text.Trim();
                objStaff.PASSPORT = txtPassport.Text.Trim();
                objStaff.PASS_ISSUE = txtPassIssueOn.Text.Trim();
                objStaff.PASS_EXP = txtPassExpOn.Text.Trim();
                objStaff.SS_ID = txtID.Text.Trim();
                objStaff.SS_ISSUE = txtIDIssueOn.Text.Trim();
                objStaff.SS_EXP = txtIDExpOn.Text.Trim();
                objStaff.DRV_LIC = txtDrivLic.Text.Trim();
                objStaff.DRV_LIC_ISSUE = txtDrivLicIssueOn.Text.Trim();
                objStaff.DRV_LIC_EXP = txtDrivLicExpOn.Text.Trim();
                objStaff.MOH_NO = txtHAAD.Text.Trim();
                objStaff.MOH_ISSUE = txtHAADIssueOn.Text.Trim();
                objStaff.MOH_EXP = txtHAADExpOn.Text.Trim();
                objStaff.OTHER_NO = txtDocOther.Text.Trim();
                objStaff.OTHER_ISSUE = txtDocOtherIssueOn.Text.Trim();
                objStaff.OTHER_EXP = txtDocOtherExpOn.Text.Trim();

                objStaff.BASIC = txtBasic.Text.Trim();
                objStaff.HRA = txtHRA.Text.Trim();
                objStaff.DA = txtDA.Text.Trim();
                objStaff.OTHR_ALLOW = txtOtherAllow.Text.Trim();
                objStaff.ADV_SALARY = txtAdvSalary.Text.Trim();
                objStaff.LOAN = txtLoan.Text.Trim();
                objStaff.VACATIONPAY = txtVacationPay.Text.Trim();
                objStaff.HOMETRAVEL = txtAssuredComm.Text.Trim();
                objStaff.WORKINGHRS = txtWorkingHours.Text.Trim();
                objStaff.FRI_COMPENSATION = txtFriComp.Text.Trim();
                objStaff.ROSTER_ID = drpDutyRoaster.SelectedValue;
                objStaff.DOJ = txtJoinDate.Text.Trim();
                objStaff.DOEXIT = txtExitDate.Text.Trim();
                objStaff.NOOFPAYMENT = txtNoInstallment.Text.Trim();
                objStaff.LOANDEDUCT = txtloanDeduct.Text.Trim();

                objStaff.FIXEDOVERTIME = txtFixedOT.Text.Trim();
                objStaff.OVERTIMEPHR = txtOTPerHour.Text.Trim();
                objStaff.ABSENTPHR = txtAbsePerHour.Text.Trim();
                objStaff.COMM_ID = drpCommission.SelectedValue;
                objStaff.PRE_DRTOKEN = txtTokenPrefix.Text.Trim();
                objStaff.TOKEN_START = txtTokenStart.Text.Trim();
                objStaff.TOKEN_END = txtTokenEnd.Text.Trim();
                objStaff.TOKEN_CURRENT = txtTokenCurrent.Text.Trim();
                objStaff.APNT_TIME_INT = txtInterval.Text.Trim();
                objStaff.NO_APNT_DATEFROM1 = txtFrom1.Text.Trim();
                objStaff.NO_APNT_FROM1TIME = txtFrom1Time.Text.Trim();
                objStaff.NO_APNT_DATEFROM2 = txtFrom2.Text.Trim();
                objStaff.NO_APNT_FROM2TIME = txtFrom2Time.Text.Trim();
                objStaff.NO_APNT_DATEFROM3 = txtFrom3.Text.Trim();
                objStaff.NO_APNT_FROM3TIME = txtFrom3Time.Text.Trim();
                objStaff.NO_APNT_DATETO1 = txtTo1.Text.Trim();
                objStaff.NO_APNT_TO1TIME = txtTo1Time.Text.Trim();
                objStaff.NO_APNT_DATETO2 = txtTo2.Text.Trim();
                objStaff.NO_APNT_TO2TIME = txtTo2Time.Text.Trim();
                objStaff.NO_APNT_DATETO3 = txtTo3.Text.Trim();
                objStaff.NO_APNT_TO3TIME = txtTo3Time.Text.Trim();
                objStaff.OTHERS1 = txtServiceCode.Text.Trim();
                objStaff.OTHERS2 = txtFee.Text.Trim();
                objStaff.OTHERS3 = txtFollowUp1.Text.Trim();
                objStaff.OTHERS4 = txtFollowUp2.Text.Trim();
                objStaff.OTHERS5 = txtAutoInvOther.Text.Trim();
                objStaff.SRV_CATEGORY = drpServCategory.SelectedValue;
                objStaff.SPECIALITY = drpSpeciality.SelectedItem.Text;
                objStaff.TOKEN_RESERVE = txtTokenReserve.Text.Trim();


                if (chkEMRViewDate.Checked == true)
                {
                    objStaff.EMR_DATE = "1";
                }
                else
                {
                    objStaff.EMR_DATE = "0";
                }
                if (chkEMRViewDept.Checked == true)
                {
                    objStaff.EMR_DEP = "1";
                }
                else
                {
                    objStaff.EMR_DEP = "0";
                }
                if (chkEMRViewCat.Checked == true)
                {
                    objStaff.EMR_CAT = "1";
                }
                else
                {
                    objStaff.EMR_CAT = "0";
                }

                objStaff.ROOM = txtRoom.Text.Trim();

                objStaff.HSFM_CLINICIAN_LOGIN = txtClinicianLoginID.Text.Trim();
                objStaff.HSFM_CLINICIAN_PWD = txtClinicianLoginPwd.Text.Trim();

                objStaff.IsManual = chkManual.Checked;
                objStaff.UserId = Convert.ToString(Session["User_ID"]);

                string strEMR_DR_DEPList = "";

                for (int i = 0; i < lstEMRDept.Items.Count; i++)
                {
                    if (lstEMRDept.Items[i].Selected == true)
                    {
                        strEMR_DR_DEPList += lstEMRDept.Items[i].Value + "|";

                    }
                }
                objStaff.EMR_DR_DEP = strEMR_DR_DEPList;



                string strPath = Server.MapPath("../Uploads/Temp/");
                Byte[] bytImage = null, bytImage1 = null;

                if (Convert.ToString(Session["StaffPhoto"]) != "")
                {
                    System.Drawing.Image image = System.Drawing.Image.FromFile(@strPath + Convert.ToString(Session["StaffPhoto"]));
                    bytImage = imageToByteArray(image);


                    objStaff.HSP_PHOTO = bytImage;
                }

                if (Convert.ToString(Session["StaffSig"]) != "")
                {

                    System.Drawing.Image image1 = System.Drawing.Image.FromFile(strPath + Convert.ToString(Session["StaffSig"]));
                    bytImage1 = imageToByteArray(image1);

                    objStaff.HSP_Signature = bytImage1;
                }

                string strReturnId;
                strReturnId = objStaff.AddStaffMaster(objStaff);

                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    AuditLogAdd("MODIFY", "Modify Existing entry in the Staff Profile Master, Staff Profile ID is " + strReturnId);
                }
                else
                {
                    AuditLogAdd("ADD", "Add new entry in the Staff Profile, Staff Profile ID is " + strReturnId);
                }

                txtStaffId.Text = "";
                Clear();
                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      StaffProfile.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }


        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStaffId.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                DS = new DataSet();
                string Criteria = " 1=1 ";

                Criteria += " AND HIM_DR_CODE='" + txtStaffId.Text.Trim() + "'";

                DS = dbo.InvoiceMasterGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This record is committed to other data. So you cannot delete this record";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                StaffMasterBAL objStaff = new StaffMasterBAL();
                objStaff.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objStaff.STAFF_ID = txtStaffId.Text.Trim();
                objStaff.UserId = Convert.ToString(Session["User_ID"]);
                objStaff.DeleteStaffMaster(objStaff);

                txtStaffId.Text = "";
                Clear();
                lblStatus.Text = "Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      StaffProfile.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }


        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtStaffId.Text = "";
                Clear();
                // txtStaffId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "STAFF");


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      StaffProfile.btnClear_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void AsyncFileUpload1_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;


                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {
                    string strFileNmae = e.FileName;
                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));
                    Session["StaffPhoto"] = strFileNmae;
                    string strPath1 = Server.MapPath("../Uploads/Temp/");

                    if (File.Exists(strPath1 + strFileNmae) == false)
                        AsyncFileUpload1.SaveAs(strPath1 + strFileNmae);

                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["StaffPhoto"] = "";
                }

                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.AsyncFileUpload1_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AsyncFileUpload2_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;


                if (Path.GetExtension(e.FileName.ToLower()) == ".jpg" || Path.GetExtension(e.FileName.ToLower()) == ".png" || Path.GetExtension(e.FileName.ToLower()) == ".jpeg" || Path.GetExtension(e.FileName.ToLower()) == ".bmp")
                {

                    string strFileNmae = e.FileName;

                    strFileNmae = strFileNmae.Substring(strFileNmae.LastIndexOf("\\") + 1, strFileNmae.Length - (strFileNmae.LastIndexOf("\\") + 1));
                    Session["StaffSig"] = strFileNmae;
                    string strPath1 = Server.MapPath("../Uploads/Temp/");

                    if (File.Exists(strPath1 + strFileNmae) == false)
                        AsyncFileUpload2.SaveAs(strPath1 + strFileNmae);



                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Incorrect File Format');", true);
                    Session["StaffSig"] = "";
                }

                //EvEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientRegistration.AsyncFileUpload1_UploadedComplete");
                TextFileWriting(ex.Message.ToString());
            }
        }

        #endregion

    }
}