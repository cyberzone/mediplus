﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class ServiceMaster : System.Web.UI.Page
    {
        DataSet DS;
        DataTable DT;
        dboperations dbo = new dboperations();
       

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindServices()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (drpCategory.SelectedIndex != 0)
            {

                Criteria += " AND  HSM_CAT_ID='" + drpCategory.SelectedValue + "'";
            }

            if (txtServCode.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HSM_SERV_ID like '" + txtServCode.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HSM_SERV_ID like '%" + txtServCode.Text.Trim() + "%' ";
                }

                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HSM_SERV_ID = '" + txtServCode.Text.Trim() + "' ";
                }

            }


            if (txtServName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HSM_NAME like '" + txtServName.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HSM_NAME like '%" + txtServName.Text.Trim() + "%' ";
                }

                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HSM_NAME = '" + txtServName.Text.Trim() + "' ";
                }


            }


            DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            gvServices.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvServices.Visible = true;
                gvServices.DataSource = DS;
                gvServices.DataBind();
            }
            else
            {
                gvServices.DataBind();
            }


        }

        void BindCategory()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSCM_STATUS='A' AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = new DataSet();
            DS = dbo.ServiceCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "HSCM_NAME";
                drpCategory.DataValueField = "HSCM_CAT_ID";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- Select ---");
            drpCategory.Items[0].Value = "0";

        }

        void BindServiceGroup()
        {
            string Criteria = " 1=1 ";



            DS = new DataSet();
            DS = dbo.ServiceGroupGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpServiceGroup.DataSource = DS;
                drpServiceGroup.DataTextField = "HSGM_ID";
                drpServiceGroup.DataValueField = "HSGM_ID";
                drpServiceGroup.DataBind();
            }
            drpServiceGroup.Items.Insert(0, "--- Select ---");
            drpServiceGroup.Items[0].Value = "0";

        }

        void BindServiceType()
        {
            DS = new DataSet();
            DS = dbo.ServiceTypeGet();

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpServType.DataSource = DS;
                drpServType.DataTextField = "NAME";
                drpServType.DataValueField = "CODE";
                drpServType.DataBind();
            }
            drpServType.Items.Insert(0, "--- Select ---");
            drpServType.Items[0].Value = "0";

        }

        void BindCodeType()
        {


            DS = new DataSet();
            DS = dbo.CodeTypeGet();

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCodeType.DataSource = DS;
                drpCodeType.DataTextField = "NAME";
                drpCodeType.DataValueField = "CODE";
                drpCodeType.DataBind();
            }
            drpCodeType.Items.Insert(0, "--- Select ---");
            drpCodeType.Items[0].Value = "0";

        }

        void BindTreatmentType()
        {
            string Criteria = " 1=1 ";


            DS = new DataSet();
            DS = dbo.TreatmentTypeGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                lstTreatmentType.DataSource = DS;
                lstTreatmentType.DataTextField = "HITT_NAME";
                lstTreatmentType.DataValueField = "HITT_ID";
                lstTreatmentType.DataBind();

            }


        }

        void ModifyServices()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND  HSM_SERV_ID='" + Convert.ToString(txtServCode.Text.Trim()) + "'";



            DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                 ViewState["NewFlag"]  = false;

                if (DS.Tables[0].Rows[0].IsNull("HSM_CAT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_CAT_ID"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSM_CAT_ID"]) != "0")
                {
                    for (int intCount = 0; intCount < drpCategory.Items.Count; intCount++)
                    {
                        if (drpCategory.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSM_CAT_ID"]))
                        {
                            drpCategory.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSM_CAT_ID"]);
                        }

                    }
                }

                txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                txtServFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_Fee"]);
                txtCompFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_Comp_Fee"]);

                if (DS.Tables[0].Rows[0].IsNull("HSM_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_TYPE"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSM_TYPE"]) != "0")
                {
                    for (int intCount = 0; intCount < drpServType.Items.Count; intCount++)
                    {
                        if (drpServType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSM_TYPE"]))
                        {
                            drpServType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSM_TYPE"]);
                        }

                    }
                }


                txtCost.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_CoST"]);

                if (DS.Tables[0].Rows[0].IsNull("HSM_GROUP") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_GROUP"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSM_GROUP"]) != "0")
                {
                    for (int intCount = 0; intCount < drpServiceGroup.Items.Count; intCount++)
                    {
                        if (drpServiceGroup.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSM_GROUP"]))
                        {
                            drpServiceGroup.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSM_GROUP"]);
                        }

                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_DISC_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_TYPE"]) != "")
                {
                    drpDiscType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_TYPE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_AMT"]) != "")
                {
                    txtDiscAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_DISC_AMT"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_CODE_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_CODE_TYPE"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSM_CODE_TYPE"]) != "0")
                {
                    drpCodeType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSM_CODE_TYPE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_REF_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_REF_CODE"]) != "")
                {
                    txtRefCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_REF_CODE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_HAAD_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_HAAD_CODE"]) != "")
                {
                    txtHaadCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
                }






                if (DS.Tables[0].Rows[0].IsNull("HSM_FAVORITE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_FAVORITE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSM_FAVORITE"]) == "1")
                    {
                        chkFavorite.Checked = true;
                    }
                    else
                    {
                        chkFavorite.Checked = false;
                    }
                }


                if (DS.Tables[0].Rows[0].IsNull("HSM_STATUS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_STATUS"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSM_STATUS"]) == "A")
                    {
                        chkStatus.Checked = true;
                    }
                    else
                    {
                        chkStatus.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_SEND_TO_MALAFFI") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_SEND_TO_MALAFFI"]) != "")
                {
                    chkSendToMalaffi.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HSM_SEND_TO_MALAFFI"]);
                }



                if (DS.Tables[0].Rows[0].IsNull("HSM_ALLOWFACTOR") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_ALLOWFACTOR"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSM_ALLOWFACTOR"]) == "1")
                    {
                        chkAllowFactor.Checked = true;
                    }
                    else
                    {
                        chkAllowFactor.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_OBSERVATION_NEEDED") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]) == "Y")
                    {
                        chkObseNeeded.Checked = true;
                    }
                    else
                    {
                        chkObseNeeded.Checked = false;
                    }
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_OBS_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_CODE"]) != "")
                {
                    txtObsCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_OBS_VALUETYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]) != "")
                {
                    txtObsValueType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSM_OBS_CONVERSION") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]) != "")
                {
                    txtObsConversion.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("HSM_TAXTYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_TAXTYPE"]) != "")
                {
                    drpTaxType.SelectedValue  = Convert.ToString(DS.Tables[0].Rows[0]["HSM_TAXTYPE"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("HSM_TAXVALUE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_TAXVALUE"]) != "")
                {
                    txtTaxValue.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_TAXVALUE"]);
                }

              

                if (DS.Tables[0].Rows[0].IsNull("HSM_PACKAGE_SERVICE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_PACKAGE_SERVICE"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HSM_PACKAGE_SERVICE"]) == "True")
                    {
                        chkPackageService.Checked = true;
                    }
                    else
                    {
                        chkPackageService.Checked = false;
                    }
                }
                else
                {
                    chkPackageService.Checked = false;
                }
                if (DS.Tables[0].Rows[0].IsNull("HSM_TREATMENT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSM_TREATMENT_TYPE"]) != "")
                {

                    string[] strTreatmentType = Convert.ToString(DS.Tables[0].Rows[0]["HSM_TREATMENT_TYPE"]).Split('/');

                    for (int i = 0; i < lstTreatmentType.Items.Count; i++)
                    {
                        for (int j = 0; j < strTreatmentType.Length - 1; j++)
                        {
                            if (lstTreatmentType.Items[i].Value == strTreatmentType[j])
                            {
                                lstTreatmentType.Items[i].Selected = true;
                                goto EndLoop;
                            }
                        }
                    EndLoop: ;

                    }

                }

                GetHaadService();

            }



        }

        string GetTreatmentType()
        {
            string strTreatmentType = "";
            for (int i = 0; i <= lstTreatmentType.Items.Count - 1; i++)
            {
                if (lstTreatmentType.Items[i].Selected == true)
                {
                    strTreatmentType += lstTreatmentType.Items[i].Value + "/";
                }

            }

            return strTreatmentType;
        }

        void GetNextServiceId()
        {
            if (drpCategory.SelectedIndex != 0)
            {
                string Criteria = " 1=1 ";
                Criteria += " AND HSCM_STATUS='A' AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND  HSCM_CAT_ID='" + drpCategory.SelectedValue + "'";

                DS = new DataSet();
                DS = dbo.ServiceCategoryGet(Criteria);

                string strServstart = "", strServEnd = "";
                txtServCode.Text = "";
                Int32 intServID = 0;
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (DS.Tables[0].Rows[0].IsNull("HSCM_SERV_START") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSCM_SERV_START"]) != "")
                    {
                        strServstart = Convert.ToString(DS.Tables[0].Rows[0]["HSCM_SERV_START"]);
                        strServEnd = Convert.ToString(DS.Tables[0].Rows[0]["HSCM_SERV_END"]);
                    }
                }


                Criteria = " 1=1 ";
                Criteria += " AND  HSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND  HSM_CAT_ID='" + drpCategory.SelectedValue + "'";

                intServID = dbo.MaxServiceIdGet(Criteria);

                if (intServID == 0)
                {
                    txtServCode.Text = strServstart;
                }
                else
                {
                    txtServCode.Text = Convert.ToString(intServID + 1);
                }





            }

        }

        void Clear()
        {
            lblStatus.Text = "";
            chkManual.Checked = false;
            chkStatus.Checked = true;
            chkSendToMalaffi.Checked = true;
            drpCategory.SelectedIndex = 0;
            txtServCode.Text = "";
            txtServName.Text = "";
            txtServFee.Text = "";
            txtCompFee.Text = "";
            txtCost.Text = "";
            drpServType.SelectedIndex = 0;
            chkObseNeeded.Checked = false;
            drpDiscType.SelectedIndex = 0;
            drpServiceGroup.SelectedIndex = 0;
            chkSavePrice.Checked = false;
            txtDiscAmount.Text = "";
            drpCodeType.SelectedIndex = 0;
            chkFavorite.Checked = false;
            txtRefCode.Text = "";
            txtHaadCode.Text = "";
            txtHaadName.Text = "";
            chkAllowFactor.Checked = false;
            txtObsCode.Text = "";
            txtObsValueType.Text = "";
            txtObsConversion.Text = "";
            txtTaxValue.Text = "";
            chkAllTreatment.Checked = false;
            chkPackageService.Checked = false;

            lstTreatmentType.ClearSelection();

            if (gvServices.Rows.Count > 0)
            {
                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvServices.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
            }

            ViewState["ServiceId"] = "0";
            ViewState["SelectIndex"] = "";
            ViewState["NewFlag"] = true;

        }

        void BindHaadServiceGrid(string Name)
        {
            string Criteria = " 1=1 ";

            if (Name == "CODE")
            {
                Criteria += " AND  HHS_CODE like '%" + txtHaadCode.Text.Trim() + "%'";
            }

            if (Name == "DESC")
            {
                Criteria += " AND  HHS_DESCRIPTION like'%" + txtHaadName.Text.Trim() + "%'";
            }

            Criteria += " AND  HHS_TYPE_VALUE='" + drpCodeType.SelectedValue + "'";
            dbo = new dboperations();
            DS = new DataSet();
            DS = dbo.HaadServiceGet(Criteria, "", "1000");

            gvHaadService.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHaadService.Visible = true;
                gvHaadService.DataSource = DS;
                gvHaadService.DataBind();
            }
            else
            {
                gvHaadService.DataBind();
            }

        }


        void GetHaadService()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  HHS_CODE  ='" + txtHaadCode.Text.Trim() + "'";
            Criteria += " AND  HHS_TYPE_VALUE ='" + drpCodeType.SelectedValue + "'";
            dbo = new dboperations();
            DS = new DataSet();
            DS = dbo.HaadServiceGet(Criteria, "", "1");
            txtHaadName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtHaadName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
            }

        }

     

        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "SERV";
            objCom.ScreenName = "Service Master";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }


        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }

            string Criteria = " 1=1 AND HRT_SCREEN_ID='SERV' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

           
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "0" || strPermission == "1")
            {

                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                btnClear.Enabled = false;



            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                AuditLogAdd("OPEN", "Open Service Master Page");

                try
                {

                    ViewState["NewFlag"] = true;
                    ViewState["ServiceId"] = "0";

                    string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);
                    switch (AutoSlNo)
                    {
                        case "A":
                            chkManual.Checked = false;
                            chkManual.Visible = false;
                            break;
                        case "M":
                            chkManual.Checked = true;
                            chkManual.Visible = false;
                            txtServCode.ReadOnly = false;
                            break;
                        case "B":
                            chkManual.Visible = true;
                            break;
                        default:
                            break;

                    }

                    BindCategory();
                    BindServiceGroup();
                    BindServiceType();
                    BindCodeType();
                    BindTreatmentType();
                    BindServices();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void chkManual_CheckedChanged(object sender, EventArgs e)
        {
            if (chkManual.Checked == true)
            {
                txtServCode.Text = "";
                txtServCode.ReadOnly = false;
            }
            else
            {
                txtServCode.ReadOnly = true;
            }

        }

        protected void chkAllTreatment_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAllTreatment.Checked == true)
            {
                for (int j = 0; j < lstTreatmentType.Items.Count; j++)
                {

                    lstTreatmentType.Items[j].Selected = true;


                }
            }
            else
            {
                lstTreatmentType.ClearSelection();
            }

        }



        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvServices.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;


                gvServices.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblServiceId;


                lblServiceId = (Label)gvScanCard.Cells[0].FindControl("lblServiceId");

                txtServCode.Text = lblServiceId.Text;
                ModifyServices();
                //BindInvoiceTran();
                //BindInvoiceClaimDtls();
                //GetReceiptTran();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtServCode.Text = "";
            BindServices();
            GetNextServiceId();
        }

        protected void gvServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Clear();

            gvServices.PageIndex = e.NewPageIndex;
            BindServices();
        }

        protected void gvServices_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvServices.PageIndex * gvServices.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindServices();
                ModifyServices();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtHaadCode_TextChanged(object sender, EventArgs e)
        {
            //if (drpCodeType.SelectedIndex != 0)
            //{
            //    ModalPopupExtender1.Show();
            //    BindHaadServiceGrid("CODE");
            //}

            if (drpCodeType.SelectedIndex != 0)
            {
                GetHaadService();
            }
        }



        protected void HaadSelect_Click(object sender, EventArgs e)
        {
            try
            {


                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblHaadServId, lblHaadServDesc;
                lblHaadServId = (Label)gvScanCard.Cells[0].FindControl("lblHaadServId");
                lblHaadServDesc = (Label)gvScanCard.Cells[0].FindControl("lblHaadServDesc");
                txtHaadCode.Text = lblHaadServId.Text.Trim();
                txtHaadName.Text = lblHaadServDesc.Text.Trim();
                ModalPopupExtender1.Hide();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.HaadSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtServName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindServices();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_ServiceMasterAdd";
                cmd.Parameters.Add(new SqlParameter("@HSM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HSM_SERV_ID", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HSM_NAME", SqlDbType.VarChar)).Value = txtServName.Text.Trim();


                if (txtServFee.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_FEE", SqlDbType.Decimal)).Value = txtServFee.Text.Trim();
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_FEE", SqlDbType.Decimal)).Value = 0;
                }
                if (txtCompFee.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_COMP_FEE", SqlDbType.Decimal)).Value = txtCompFee.Text.Trim();
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_COMP_FEE", SqlDbType.Decimal)).Value = 0;
                }
                if (txtCost.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_COST", SqlDbType.Decimal)).Value = txtCost.Text.Trim();
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_COST", SqlDbType.Decimal)).Value = 0;
                }
                cmd.Parameters.Add(new SqlParameter("@HSM_CAT_ID", SqlDbType.VarChar)).Value = drpCategory.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HSM_DISC_TYPE", SqlDbType.Char)).Value = drpDiscType.SelectedValue;

                if (txtDiscAmount.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_DISC_AMT", SqlDbType.Decimal)).Value = txtDiscAmount.Text.Trim();
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_DISC_AMT", SqlDbType.Decimal)).Value = 0;
                }

                cmd.Parameters.Add(new SqlParameter("@HSM_TYPE", SqlDbType.Char)).Value = drpServType.SelectedValue;

                if (chkStatus.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_STATUS", SqlDbType.Char)).Value = "A";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_STATUS", SqlDbType.Char)).Value = "I";
                }

            
                

                cmd.Parameters.Add(new SqlParameter("@HSM_TREATMENT_TYPE", SqlDbType.VarChar)).Value = GetTreatmentType();


                if (chkFavorite.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_FAVORITE", SqlDbType.TinyInt)).Value = 1;
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_FAVORITE", SqlDbType.TinyInt)).Value = 0;
                }

                cmd.Parameters.Add(new SqlParameter("@HSM_REF_CODE", SqlDbType.VarChar)).Value = txtRefCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HSM_HAAD_CODE", SqlDbType.VarChar)).Value = txtHaadCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HSM_CODE_TYPE", SqlDbType.TinyInt)).Value = drpCodeType.SelectedValue;

                cmd.Parameters.Add(new SqlParameter("@HSM_GROUP", SqlDbType.VarChar)).Value = drpServiceGroup.SelectedValue;

                if (chkObseNeeded.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_OBSERVATION_NEEDED", SqlDbType.Char)).Value = "Y";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_OBSERVATION_NEEDED", SqlDbType.Char)).Value = "N";
                }

                //cmd.Parameters.Add(new SqlParameter("@HSM_CHARTOFACCOUNT", SqlDbType.VarChar)).Value = "";
                //cmd.Parameters.Add(new SqlParameter("@HSM_PHASECODE", SqlDbType.VarChar)).Value = "";


                if (chkAllowFactor.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_ALLOWFACTOR", SqlDbType.Int)).Value = 1;
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_ALLOWFACTOR", SqlDbType.Int)).Value = 0;
                }

                cmd.Parameters.Add(new SqlParameter("@HSM_OBS_CODE", SqlDbType.VarChar)).Value = txtObsCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HSM_OBS_CONVERSION", SqlDbType.VarChar)).Value = txtObsConversion.Text.Trim();

                cmd.Parameters.Add(new SqlParameter("@HSM_OBS_VALUETYPE", SqlDbType.VarChar)).Value = txtObsValueType.Text.Trim();

                if (chkSavePrice.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@SavetoCmpAgrmt", SqlDbType.Bit)).Value = true;
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@SavetoCmpAgrmt", SqlDbType.Bit)).Value = false;
                }


                cmd.Parameters.Add(new SqlParameter("@HSM_TAXTYPE", SqlDbType.Char )).Value =drpTaxType.SelectedValue ;

                if (txtTaxValue.Text.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_TAXVALUE", SqlDbType.VarChar)).Value = txtTaxValue.Text.Trim();
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_TAXVALUE", SqlDbType.VarChar)).Value = "0";
                }

                if (chkPackageService.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_PACKAGE_SERVICE", SqlDbType.Bit)).Value = true;
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HSM_PACKAGE_SERVICE", SqlDbType.Bit)).Value = false;
                }

                cmd.Parameters.Add(new SqlParameter("@HSM_SEND_TO_MALAFFI", SqlDbType.Bit)).Value = chkSendToMalaffi.Checked;

                cmd.Parameters.Add(new SqlParameter("@HSM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);



                cmd.ExecuteNonQuery();
                con.Close();

                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    AuditLogAdd("MODIFY", "Modify Existing entry in the Lab Service Master, Service ID is " + txtServCode.Text.Trim());
                }
                else
                {
                    AuditLogAdd("ADD", "Add new entry in the Lab Service Master, Service ID is " + txtServCode.Text.Trim());
                }

                Clear();
                BindServices();
                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtServCode.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                DS = new DataSet();
                string Criteria = " 1=1 ";

                Criteria += " AND HIT_SERV_CODE='" + txtServCode.Text.Trim() + "'";

                DS = dbo.InvoiceTransGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This record is committed to other data. So you cannot delete this record";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd;

 
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_ServiceMasterDelete";
                cmd.Parameters.Add(new SqlParameter("@HSM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HSM_SERV_ID", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);



                cmd.ExecuteNonQuery();
                con.Close();

                Clear();
                BindServices();
                lblStatus.Text = "Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            BindServices();
        }
    }
}