﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordChange.aspx.cs" Inherits="Mediplus.HMS.Masters.PasswordChange" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />
    <title></title>
    <script type="text/javascript" language="Javascript">

        function validatechangepawd()
        {
            if (document.getElementById('<%=txtOldPwd.ClientID%>').value == "")
            {
                alert("Please Enter the Old Password");
                document.getElementById('<%=txtOldPwd.ClientID%>').focus();
		                            return false;
                                }	
                    	
                                if (document.getElementById('<%=txtNewPwd.ClientID%>').value == "")
            {
                alert("Please Enter the New Password");
                document.getElementById('<%=txtNewPwd.ClientID%>').focus();
		                            return false;
                                }	
                    	
                                if (document.getElementById('<%=txtConPwd.ClientID%>').value == "")
            {
                alert("Please Enter the Confirm Password");
                document.getElementById('<%=txtConPwd.ClientID%>').focus();
		                            return false;
                                }	
                    	
                                if ( document.getElementById('<%=txtNewPwd.ClientID%>').value != document.getElementById('<%=txtConPwd.ClientID%>').value )
            {
                alert("Please Enter the Same Confirm Password");
                document.getElementById('<%=txtConPwd.ClientID%>').value = "";
		                            document.getElementById('<%=txtConPwd.ClientID%>').focus();
	                        return false;
	                    }	
	                            
                        if (txtPassword(document.getElementById('<%=txtNewPwd.ClientID%>').value) == false)
            {
                alert('Password ! Single Code Characters are not Allowed');
                document.getElementById('<%=txtNewPwd.ClientID%>').value = "";
	                                document.getElementById('<%=txtConPwd.ClientID%>').value = "";
	                         document.getElementById('<%=txtNewPwd.ClientID%>').focus();
	                         return false;
	                     }
	             
                         if (txtPassword(document.getElementById('<%=txtOldPwd.ClientID%>').value) == false)
            {
                alert('The Old Password is incorrect');
                document.getElementById('<%=txtOldPwd.ClientID%>').value = "";
	                                document.getElementById('<%=txtOldPwd.ClientID%>').focus();
	                       return false;
	                   }
            //return true;
                       return ConfirmMsg();
                    	
        }

        </script>
    <style type = "text/css">
        body{resize:none}
        </style>
 
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <table>
                <tr>
                    <td class="PageHeader"> Change Password
               
                    </td>
                </tr>
            </table>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
                <tr>

                    <td>
                        
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                           
                        
                    </td>
                </tr>
            </table>
    <table>
        <tr>
          <td class="lblCaption1" style="height: 30px;">Old Password : <span style="color:red;">* </span>
          </td>
          <td>
             <asp:TextBox ID="txtOldPwd" runat="server" textmode="password" cssclass="label" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10"></asp:TextBox>
          </td>
        </tr>
        <tr>
          <td class="lblCaption1" style="height: 30px;">New Password : <span style="color:red;">* </span>
          </td>
          <td>
             <asp:TextBox ID="txtNewPwd" runat="server" textmode="password" cssclass="label" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10"></asp:TextBox>
          </td>
        </tr>
         <tr>
          <td class="lblCaption1" style="height: 30px;">Confirm Password : <span style="color:red;">* </span>
          </td>
          <td>
             <asp:TextBox ID="txtConPwd" runat="server" textmode="password" cssclass="label" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10"></asp:TextBox>
          </td>
        </tr>
        <tr>
            <td></td>
            <td>
              <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click"  OnClientClick="return validatechangepawd();"  Text="Save" />

            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
