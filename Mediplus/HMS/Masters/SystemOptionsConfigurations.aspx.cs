﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class SystemOptionsConfigurations : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOptions()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet Ds = new DataSet();
            Ds = objCom.fnGetFieldValue("TOP 1 *", "HMS_SYSTEM_OPTIONS", " 1=1 ", "");

            foreach (DataRow DR in Ds.Tables[0].Rows)
            {

                txtAppIntStart.Text = Convert.ToString(DR["HSOM_APPNMNT_START"]);
                txtAppIntEnd.Text = Convert.ToString(DR["HSOM_APPNMNT_END"]);
                txtAppIntInterval.Text = Convert.ToString(DR["HSOM_APPOINTMENT_INTERVAL"]);
                drpAppintFormat.SelectedValue = Convert.ToString(DR["HSOM_APPOINTMENT_DSPLY_FORMAT"]);


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSystemOptions();
            }
        }

        protected void txtAppintSave_TextChanged(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();
            string FieldNameWithValues = "HSOM_APPNMNT_START='" + txtAppIntStart.Text + "'";
            FieldNameWithValues += ",HSOM_APPNMNT_END='" + txtAppIntEnd.Text + "'";
            FieldNameWithValues += ",HSOM_APPOINTMENT_INTERVAL='" + txtAppIntInterval.Text + "'";
            FieldNameWithValues += ",HSOM_APPOINTMENT_DSPLY_FORMAT='" + drpAppintFormat.SelectedValue + "'";

            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_SYSTEM_OPTIONS", "1=1");

            BindSystemOptions();
        }
    }
}