﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.Data;
using System.IO;

namespace Mediplus.HMS.Masters
{
    public partial class RollMaster : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindRollMaster()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND HRM_ROLL_ID='" + txtRollID.Text.Trim() + "'";

            DS = objCom.fnGetFieldValue("*", "HMS_ROLL_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtRollID.Text = Convert.ToString(DR["HRM_ROLL_ID"]);
                    txtRollName.Text = Convert.ToString(DR["HRM_ROLL_NAME"]);

                    if (Convert.ToString(DR["HRM_STATUS"]) == "A")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }

        }

        void BindRollMasterGrid()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            DS = objCom.fnGetFieldValue("*", "HMS_ROLL_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRollMaster.DataSource = DS;
                gvRollMaster.DataBind();
            }
            else
            {
                gvRollMaster.DataBind();
            }

        }


        void BindScreenMaster()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSCR_STATUS='A' ";

            DS = objCom.fnGetFieldValue("DISTINCT   HSCR_SCREEN_ID, *, (SELECT TOP 1 HRT_PERMISSION FROM  HMS_ROLL_TRANSACTION WHERE HRT_SCREEN_ID = HSCR_SCREEN_ID AND HRT_ROLL_ID='" + txtRollID.Text.Trim() + "' ) AS HRT_PERMISSION ", "HMS_SCREEN_MASTER", Criteria, " HSCR_MODULE_NAME,HSCR_MAIN_MENUNAME,HSCR_SCREEN_NAME ");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRollTrans.DataSource = DS;
                gvRollTrans.DataBind();

            }

        }


        void BindRollTrans(string ScreenID)
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND HRT_SCREEN_ID='" + ScreenID + "' and HRT_ROLL_ID='" + txtRollID.Text.Trim() + "'";

            DS = objCom.fnGetFieldValue("*", "HMS_ROLL_TRANSACTION", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {


            }

        }


        void Clear()
        {
            txtRollID.Text = "";
            txtRollName.Text = "";
            chkActive.Checked = true;

        }

             void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }


            string Criteria = " 1=1 AND HRT_SCREEN_ID='ROLL' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                
                btnSave.Enabled = false;
              
                btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                 

            }

            if (strPermission == "7")
            {
                
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx?MenuName=Masters");
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }

                BindRollMasterGrid();

                BindRollMaster();
                BindScreenMaster();
            }
        }

        protected void txtRollID_TextChanged(object sender, EventArgs e)
        {
            BindRollMaster();
            BindScreenMaster();
        }

        protected void gvRollTrans_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblScreenID = (Label)e.Row.FindControl("lblScreenID");
                Label lblPermission = (Label)e.Row.FindControl("lblPermission");
                CheckBox chkNoPermission = (CheckBox)e.Row.FindControl("chkNoPermission");
                CheckBox chkReadOnly = (CheckBox)e.Row.FindControl("chkReadOnly");

                CheckBox chkCreate = (CheckBox)e.Row.FindControl("chkCreate");
                CheckBox chkModify = (CheckBox)e.Row.FindControl("chkModify");
                CheckBox chkDelete = (CheckBox)e.Row.FindControl("chkDelete");
                CheckBox chkFullPermission = (CheckBox)e.Row.FindControl("chkFullPermission");

                if (lblPermission.Text == "" || lblPermission.Text == "0")
                {
                    chkNoPermission.Checked = true;
                }


                if (lblPermission.Text == "1")
                {
                    chkReadOnly.Checked = true;
                }

                if (lblPermission.Text == "3")
                {
                    chkCreate.Checked = true;
                }
                if (lblPermission.Text == "5")
                {
                    chkModify.Checked = true;
                }
                if (lblPermission.Text == "7")
                {
                    chkDelete.Checked = true;
                }
                if (lblPermission.Text == "9")
                {
                    chkFullPermission.Checked = true;
                }


            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.ROLL_ID = txtRollID.Text.Trim();
                objCom.ROLL_NAME = txtRollName.Text.Trim();


                string strStatus = "I";

                if (chkActive.Checked == true)
                {
                    strStatus = "A";
                }
                objCom.Status = strStatus;
                objCom.UserID = Convert.ToString(Session["User_ID"]);
                objCom.RollMasterAdd();



                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);


                for (Int32 i = 0; i < gvRollTrans.Rows.Count; i++)
                {

                    Label lblScreenID = (Label)gvRollTrans.Rows[i].FindControl("lblScreenID");
                    Label lblScreenName = (Label)gvRollTrans.Rows[i].FindControl("lblScreenName");
                    Label lblPermission = (Label)gvRollTrans.Rows[i].FindControl("lblPermission");
                    CheckBox chkNoPermission = (CheckBox)gvRollTrans.Rows[i].FindControl("chkNoPermission");
                    CheckBox chkReadOnly = (CheckBox)gvRollTrans.Rows[i].FindControl("chkReadOnly");

                    CheckBox chkCreate = (CheckBox)gvRollTrans.Rows[i].FindControl("chkCreate");
                    CheckBox chkModify = (CheckBox)gvRollTrans.Rows[i].FindControl("chkModify");
                    CheckBox chkDelete = (CheckBox)gvRollTrans.Rows[i].FindControl("chkDelete");
                    CheckBox chkFullPermission = (CheckBox)gvRollTrans.Rows[i].FindControl("chkFullPermission");

                    string strPermission = "";


                    if (chkNoPermission.Checked == true)
                    {
                        strPermission = "0";
                    }

                    if (chkReadOnly.Checked == true)
                    {
                        strPermission = "1";
                    }

                    if (chkCreate.Checked == true)
                    {
                        strPermission = "3";
                    }
                    if (chkModify.Checked == true)
                    {

                        strPermission = "5";
                    }
                    if (chkDelete.Checked == true)
                    {

                        strPermission = "7";
                    }
                    if (chkFullPermission.Checked == true)
                    {

                        strPermission = "9";
                    }

                    objCom.ROLL_ID = txtRollID.Text.Trim();
                    objCom.SCREEN_ID = lblScreenID.Text;
                    objCom.SCREEN_NAME = lblScreenName.Text;
                    objCom.PERMISSION = strPermission;
                    objCom.UserID = Convert.ToString(Session["User_ID"]);
                    objCom.RollTransrAdd();



                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Roll Master','Data Saved')", true);

                Clear();
                BindScreenMaster();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RollMaster.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            BindScreenMaster();
        }

        protected void imgbtnRollView_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowRollMaster()", true);

            BindRollMasterGrid();
        }


        protected void imgRollMasterSelect_Click(object sender, EventArgs e)
        {

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideRollMaster()", true);

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblID = (Label)gvScanCard.Cells[0].FindControl("lblID");
            Label lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");
            Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");


            txtRollID.Text = lblID.Text;
            txtRollName.Text = lblName.Text;

            if (lblStatus.Text.Trim() == "A")
            {
                chkActive.Checked = true;
            }
            else
            {
                chkActive.Checked = false;
            }

            BindScreenMaster();

        }
    }
}