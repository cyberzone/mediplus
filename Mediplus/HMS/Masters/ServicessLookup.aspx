﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServicessLookup.aspx.cs" Inherits="Mediplus.HMS.HomeCare.ServicessLookup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title></title>
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
 
    <script language="javascript" type="text/javascript">

        function passvalue(Code, Desc) {
        
            var CtrlName = document.getElementById('hidCtrlName').value;

            window.opener.BindServicess(Code, Desc, CtrlName);
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
         <input type="hidden" id="hidCtrlName" runat="server" />
    <div>
         <div style="padding-top: 0px; padding-left: 0px; width: 700px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table width="100%">
                    <tr>
                         <td class="label" style="width: 100px;">Search
                        </td>
                        <td>
                           <asp:TextBox ID="txtSearch" runat="server"  CssClass="label" Width="300px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                        </td>
                       
                    </tr>
                </table>
            </div>
      <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="95%">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("Code")  %>','<%# Eval("Description")%>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Service Name">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("Code")  %>','<%# Eval("Description")%>')">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Price" HeaderStyle-Width="80px" visible="false">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("Code")  %>','<%# Eval("Description")%>')">
                                <asp:Label ID="Label3" CssClass="label" runat="server"  Width="70px" runat="server" style="text-align: right;padding-right:5px;"  Text=' <%#  decimal.Parse(Eval("Price").ToString()).ToString("N2")%>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
    </div>
    </form>
</body>
</html>
