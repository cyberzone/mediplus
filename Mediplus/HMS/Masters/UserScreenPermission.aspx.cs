﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class UserScreenPermission : System.Web.UI.Page
    {

        void BindScreenMaster()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSCR_STATUS='A' ";

            string UserID="";
            if (txtRefDoctorName.Text.Trim() != "")
            {
                string strDiagNameDtls = txtRefDoctorName.Text.Trim();
                string[] arrDiagName = strDiagNameDtls.Split('~');

                if (arrDiagName.Length > 0)
                {
                    UserID = arrDiagName[0];

                }

            }



            DS = objCom.fnGetFieldValue("DISTINCT   HSCR_SCREEN_ID, HSCR_MODULE_NAME,HSCR_MAIN_MENUNAME,HSCR_SCREEN_NAME" +
                ",(SELECT TOP 1 HRT_PERMISSION FROM  HMS_ROLL_TRANSACTION   inner join HMS_GROUP_PERMISSIONS on   HRT_ROLL_ID =  HGP_ROLL_ID " +
                " WHERE HRT_SCREEN_ID = HSCR_SCREEN_ID  and  HGP_STATUS ='A' "+
                " AND ( HGP_USER_ID  = '"+  UserID +"' OR HGP_GROUP_ID in	(SELECT   HGU_GROUP_ID FROM HMS_GROUP_TRANSACTION WHERE HGU_USER_ID  = '"+ UserID +"')) ) AS HRT_PERMISSION ", "HMS_SCREEN_MASTER", Criteria, " HSCR_MODULE_NAME,HSCR_MAIN_MENUNAME,HSCR_SCREEN_NAME ");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRollTrans.DataSource = DS;
                gvRollTrans.DataBind();

            }

        }

        #region AutoCompleteExtender


        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorName(string prefixText)
        {
            string[] Data = null;

            string criteria = " 1=1 AND  HUM_STATUS='A'";
            criteria += " AND HUM_USER_ID + ' ' +isnull(HUM_USER_NAME,'')    like '%" + prefixText + "%' ";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue("HUM_USER_ID,HUM_USER_NAME", "HMS_USER_MASTER", criteria, "HUM_USER_NAME");
            int StaffCount = ds.Tables[0].Rows.Count;




            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[StaffCount];

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                  
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HUM_USER_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HUM_USER_NAME"]);
                }
                return Data;
            }
            

            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            BindScreenMaster();
        }

        protected void gvRollTrans_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblScreenID = (Label)e.Row.FindControl("lblScreenID");
                Label lblPermission = (Label)e.Row.FindControl("lblPermission");
                CheckBox chkNoPermission = (CheckBox)e.Row.FindControl("chkNoPermission");
                CheckBox chkReadOnly = (CheckBox)e.Row.FindControl("chkReadOnly");

                CheckBox chkCreate = (CheckBox)e.Row.FindControl("chkCreate");
                CheckBox chkModify = (CheckBox)e.Row.FindControl("chkModify");
                CheckBox chkDelete = (CheckBox)e.Row.FindControl("chkDelete");
                CheckBox chkFullPermission = (CheckBox)e.Row.FindControl("chkFullPermission");

                if (lblPermission.Text == "" || lblPermission.Text == "0")
                {
                    chkNoPermission.Checked = true;
                }


                if (lblPermission.Text == "1")
                {
                    chkReadOnly.Checked = true;
                }

                if (lblPermission.Text == "3")
                {
                    chkCreate.Checked = true;
                }
                if (lblPermission.Text == "5")
                {
                    chkModify.Checked = true;
                }
                if (lblPermission.Text == "7")
                {
                    chkDelete.Checked = true;
                }
                if (lblPermission.Text == "9")
                {
                    chkFullPermission.Checked = true;
                }


            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtRefDoctorName.Text = "";
            gvRollTrans.DataBind();
        }
    }
}