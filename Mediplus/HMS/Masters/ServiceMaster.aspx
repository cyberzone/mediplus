﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="ServiceMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.ServiceMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
             label.style.color = 'red';
             document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

        if (document.getElementById('<%=drpCategory.ClientID%>').value == "0") {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                   document.getElementById('<%=drpCategory.ClientID%>').focus;
                   return false;
               }

               var strSerCode = document.getElementById('<%=txtServCode.ClientID%>').value
             if (/\S+/.test(strSerCode) == false) {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Code";
            document.getElementById('<%=txtServCode.ClientID%>').focus;
            return false;
        }

        var strServName = document.getElementById('<%=txtServName.ClientID%>').value
             if (/\S+/.test(strServName) == false) {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Name";
            document.getElementById('<%=txtServName.ClientID%>').focus;
            return false;
        }

        if (document.getElementById('<%=drpServType.ClientID%>').value == "0") {
                 document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Service Type";
                   document.getElementById('<%=drpServType.ClientID%>').focus;
                   return false;
               }



               return true;
           }

           function BindCompFee() {

               if (document.getElementById('<%=txtCompFee.ClientID%>').value == "") {
                document.getElementById('<%=txtCompFee.ClientID%>').value = document.getElementById('<%=txtServFee.ClientID%>').value;

            }

        }

        function HaadShow() {
            var CodeType = document.getElementById('<%=drpCodeType.ClientID%>').value
            var ServCode = document.getElementById('<%=txtHaadCode.ClientID%>').value

            var win = window.open("HaadServiceLookup.aspx?CtrlName=Code&CodeType=" + CodeType + "&Code=" + ServCode + "&CategoryType=All", "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
            return false;

        }

        function HaadShow1() {
            var CodeType = document.getElementById('<%=drpCodeType.ClientID%>').value
            var ServName = document.getElementById('<%=txtHaadName.ClientID%>').value

            var win = window.open("HaadServiceLookup.aspx?CtrlName=Name&CodeType=" + CodeType + "&Name=" + ServName + "&CategoryType=All", "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
            return false;

        }

        function DeleteVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strSerCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strSerCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }
            var isDelCompany = window.confirm('Do you want to delete Service Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }

        function BindHaadDtls(HaadCode, HaadName, CtrlName) {

            document.getElementById("<%=txtHaadCode.ClientID%>").value = HaadCode;
            document.getElementById("<%=txtHaadName.ClientID%>").value = HaadName;

        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />


    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table width="80%" cellpadding="5" cellspacing="5">
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="Label1" runat="server" CssClass="label"
                            Text="Category"></asp:Label>
                        <span style="color: red; font-size: 11px;">*</span>
                        <asp:CheckBox ID="chkManual" runat="server" CssClass="label" Text="Manual" Checked="false" AutoPostBack="True" OnCheckedChanged="chkManual_CheckedChanged" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpCategory" CssClass="TextBoxStyle" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label2" runat="server" CssClass="label"
                    Text="Serv.Fee"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtServFee" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10" Style="text-align: right;" onblur="return BindCompFee()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 150px;display:none;">
                <asp:Label ID="Label13" runat="server" CssClass="label"
                    Text="Ins. Type of Treatment"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox runat="server" ID="chkAllTreatment" CssClass="label" Text="All" AutoPostBack="True" OnCheckedChanged="chkAllTreatment_CheckedChanged"></asp:CheckBox>

            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="Label3" runat="server" CssClass="label" Text="Code"></asp:Label>
                        <span style="color: red; font-size: 11px;">*</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chkStatus" runat="server" CssClass="label" Text="Active" Checked="true" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtServCode" CssClass="TextBoxStyle" runat="server" Width="150px" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged"></asp:TextBox>




                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label4" runat="server" CssClass="label"
                    Text="Comp.Fee"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtCompFee" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="100" Style="text-align: right;" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td rowspan="6" valign="top" style="display:none;">
                <asp:UpdatePanel ID="UpdatePanel19" runat="server" >
                    <ContentTemplate>
                        <div runat="server" id="div1" style="padding-top: 0px; width: 170px; height: 150px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
                            <asp:CheckBoxList runat="server" CssClass="label" ID="lstTreatmentType" Width="150px" RepeatLayout="Flow"></asp:CheckBoxList>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label5" runat="server" CssClass="label"
                    Text="Name"></asp:Label>
                <span style="color: red; font-size: 11px;">*</span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtServName" CssClass="TextBoxStyle" runat="server" Width="90%" MaxLength="100" AutoPostBack="true" OnTextChanged="txtServName_TextChanged"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td style="width: 150px">
                <asp:Label ID="Label6" runat="server" CssClass="label"
                    Text="Cost"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtCost" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10" Style="text-align: right;" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label7" runat="server" CssClass="label"
                    Text="Serv.Type"></asp:Label>
                <span style="color: red; font-size: 11px;">*</span>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpServType" CssClass="TextBoxStyle" runat="server" Width="150px">
                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td style="width: 150px">
                <asp:Label ID="Label8" runat="server" CssClass="label"
                    Text="Disc.Type"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpDiscType" CssClass="TextBoxStyle" runat="server" Width="75px">
                            <asp:ListItem Text="$" Value="$"></asp:ListItem>
                            <asp:ListItem Text="%" Value="%"></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label9" runat="server" CssClass="label"
                    Text="Group"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpServiceGroup" CssClass="TextBoxStyle" runat="server" Width="150px">
                        </asp:DropDownList>
                        <asp:CheckBox ID="chkSavePrice" runat="server" CssClass="label" Text="Save Price to all Company" Checked="false" Visible="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label10" runat="server" CssClass="label"
                    Text="Disc. Amount"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtDiscAmount" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10" Style="text-align: right;" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td></td>
        </tr>

        <tr>

            <td style="width: 150px;">
                <asp:Label ID="Label14" runat="server" CssClass="label"
                    Text="Code.Type"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpCodeType" CssClass="TextBoxStyle" runat="server" Width="150px">
                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 150px;">
                <asp:Label ID="Label11" runat="server" CssClass="label"
                    Text="Ref.Code"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtRefCode" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <table width="80%">
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label18" runat="server" CssClass="label"
                    Text="HAAD's Code"></asp:Label>
            </td>
            <td colspan="4">
                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtHaadCode" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" AutoPostBack="true" ondblclick="HaadShow();" OnTextChanged="txtHaadCode_TextChanged"></asp:TextBox>
                        <asp:TextBox ID="txtHaadName" CssClass="TextBoxStyle" runat="server" Width="55%" MaxLength="100" BorderWidth="1px" ondblclick="HaadShow1();"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                    <ContentTemplate>
                        <asp:CheckBox ID="chkAllowFactor" runat="server" CssClass="label" Text="Apply facotr price for this service" Checked="false" Visible="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label22" runat="server" CssClass="label"
                    Text="Sales Tax"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanelTaxType" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpTaxType" CssClass="TextBoxStyle" runat="server" Width="50px" Enabled="false">
                            <asp:ListItem Value="$">$</asp:ListItem>
                            <asp:ListItem Value="%" Selected="True">%</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtTaxValue" CssClass="TextBoxStyle" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label15" runat="server" CssClass="label"
                    Text="Obs: Code"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtObsCode" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td style="width: 150px">
                <asp:Label ID="Label16" runat="server" CssClass="label"
                    Text="Value Type"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtObsValueType" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:Label ID="Label17" runat="server" CssClass="label"
                    Text="Conversion"></asp:Label>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtObsConversion" CssClass="TextBoxStyle" runat="server" Width="75px" MaxLength="10"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="7">
                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                    <ContentTemplate>
                        <asp:CheckBox ID="chkSendToMalaffi" runat="server" CssClass="label" Text="Send To Malaffi" Checked="true" />
                        &nbsp; &nbsp; &nbsp; &nbsp;
                                <asp:CheckBox ID="chkObseNeeded" runat="server" CssClass="label" Text="Observation Needed" Checked="false" />&nbsp; &nbsp; &nbsp; &nbsp;
                                <asp:CheckBox ID="chkFavorite" runat="server" CssClass="label" Text="Favorite" Checked="false" />&nbsp; &nbsp; &nbsp; &nbsp;
                                <asp:CheckBox ID="chkPackageService" runat="server" CssClass="label" Text="Package service" Checked="false" />&nbsp; &nbsp; &nbsp; &nbsp;
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
    <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="100px" OnClick="btnDelete_Click" OnClientClick="return DeleteVal();" Text="Delete" Visible="true" />
    <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="100px" OnClick="btnClear_Click" Text="Clear" />
    <br />
    <br />
    <div runat="server" id="divTran" style="padding-top: 0px; width: 80%; height: 400px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvServices" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnRowDataBound="gvServices_RowDataBound"
                    EnableModelValidation="True" Width="100%" PageSize="200" OnPageIndexChanging="gvServices_PageIndexChanging">
                    <HeaderStyle CssClass="GridHeader_Gray" Font-Bold="true" />
                    <RowStyle CssClass="GridRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="No" SortExpression="HPV_SEQNO" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="lblServiceId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" Style="text-align: right;" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Code">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_SERV_ID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Services">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_NAME") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fee">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkFee" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label19" CssClass="GridRow" Width="70px" runat="server" Style="text-align: right; padding-right: 3px;" Text='<%# Eval("HSM_FEE","{0:0.00}") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comp.Fee">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCompFee" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label12" CssClass="GridRow" Width="100px" runat="server" Style="text-align: right; padding-right: 5px;" Text='<%# Eval("HSM_COMP_FEE","{0:0.00}") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click" Text='<%# Bind("HSCM_NAME") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label20" CssClass="GridRow" Width="100%" runat="server" Text='<%# Bind("HSM_STATUS") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="HAAD Code">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click" Style="text-align: right;" Text='<%# Bind("HSM_HAAD_CODE") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Group">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_GROUP") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Obs. Needed">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClick="Select_Click">
                                    <asp:Label ID="Label21" CssClass="GridRow" Width="100%" runat="server" Text='<%# Bind("HSM_OBSERVATION_NEEDED") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Obs. Code">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_OBS_CODE") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Value Type">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_OBS_VALUETYPE") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Conversion">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_OBS_CONVERSION") %>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <asp:LinkButton ID="lnkService" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
        ForeColor="Blue"></asp:LinkButton>
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkService"
        PopupControlID="pnlService" BackgroundCssClass="modalBackground" CancelControlID="btnServiceClose"
        PopupDragHandleControlID="pnlService">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlService" runat="server" Height="500px" Width="700px" CssClass="modalPopup">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="btnServiceClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                </td>
            </tr>
            <tr>
                <td align="left" style="height: 230px;">

                    <div style="padding-top: 0px; width: 680px; height: 400px; overflow: auto; border-color: #e3f7ef; border-style: groove;">
                        <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvHaadService" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="650">
                                    <HeaderStyle CssClass="GridHeader_Gray" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                    <Columns>


                                        <asp:TemplateField HeaderText="Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHaadServId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHS_CODE") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblHaadServDesc" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHS_DESCRIPTION") %>' Visible="false"></asp:Label>
                                                <asp:LinkButton ID="lnkServId" runat="server" OnClick="HaadSelect_Click" Text='<%# Bind("HHS_CODE") %>'></asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Service Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkServName" runat="server" OnClick="HaadSelect_Click" Text='<%# Bind("HHS_DESCRIPTION") %>'></asp:LinkButton>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkType" runat="server" OnClick="HaadSelect_Click" Text='<%# Bind("HHS_TYPE") %>'></asp:LinkButton>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                    </Columns>

                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>


                </td>
            </tr>

        </table>
    </asp:Panel>
    <br />


</asp:Content>

