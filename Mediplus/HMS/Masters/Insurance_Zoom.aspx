﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Insurance_Zoom.aspx.cs" Inherits="Mediplus.HMS.Masters.Insurance_Zoom" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
        <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
        <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title></title>
    <script language="javascript" type="text/javascript">

        function passvalue(CompanyId, CompanyName) {
           // window.opener.BindCompanyDtls(CompanyId, CompanyName);
            opener.location.href = "CompanyProfile.aspx?MenuName=Masters&CompanyId=" + CompanyId;
            opener.focus();
            window.close();

        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }
    </script>
    

</head>
<body>
    <form id="form1" runat="server">
        <div style="background-color: #FFFFFF; height: 485px; width: 900px;">


            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <div style="padding-top: 0px; vertical-align:top;" >
                <table align="center">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                                Text="Company Id:"></asp:Label>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCompanyId" runat="server" Width="70px" onKeyUp="DoUpper(this);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <span>
                                <asp:Label ID="Label4" runat="server" CssClass="lblCaption1"
                                    Text=" Name:"></asp:Label>

                            </span>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCompanyName" runat="server" Width="150px" onKeyUp="DoUpper(this);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td>
                            <span>
                                <asp:Label ID="Label5" runat="server" CssClass="lblCaption1"
                                    Text="Category:"></asp:Label>

                            </span>
                        </td>
                        <td class="style5">
                            <asp:DropDownList ID="drpType" runat="server" Width="163px" CssClass="lblCaption1">
                                <asp:ListItem>--- All ---</asp:ListItem>
                                <asp:ListItem Value="I">Insurance</asp:ListItem>
                                <asp:ListItem>Non Insurance</asp:ListItem>
                                <asp:ListItem>Personal</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnFind" runat="server" CssClass="button gray small" Height="25px"
                                        OnClick="btnFind_Click" Text="Find" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="padding-top: 10px; width: 900px; height: 400px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvGridView" runat="server"
                            AllowSorting="True" AutoGenerateColumns="False" CssClass="Grid"
                            EnableModelValidation="True" OnSorting="gvGridView_Sorting"
                            Width="870px">
                            <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="Company Code" SortExpression="HCM_COMP_ID">
                                    <ItemTemplate>
                                        <a href="javascript:passvalue('<%# Eval("HCM_COMP_ID")  %>','<%# Eval("HCM_NAME")%>')">
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HCM_COMP_ID") %>'></asp:Label>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HCM_NAME">
                                    <ItemTemplate>
                                        <a href="javascript:passvalue('<%# Eval("HCM_COMP_ID")  %>','<%# Eval("HCM_NAME")%>')">
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HCM_NAME") %>'></asp:Label>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bill To Code" SortExpression="HCM_BILL_CODE">
                                    <ItemTemplate>
                                        <a href="javascript:passvalue('<%# Eval("HCM_COMP_ID")  %>','<%# Eval("HCM_NAME")%>')">
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HCM_BILL_CODE") %>'></asp:Label>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments" SortExpression="HCM_REM_RCPTN">
                                    <ItemTemplate>
                                        <a href="javascript:passvalue('<%# Eval("HCM_COMP_ID")  %>','<%# Eval("HCM_NAME")%>')">
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HCM_REM_RCPTN") %>'></asp:Label>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </form>
</body>
</html>
