﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1Arabic.Master" AutoEventWireup="true" CodeBehind="ServiceMasterA.aspx.cs" Inherits="Mediplus.HMS.Masters.ServiceMasterA" %>



<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=drpCategory.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                document.getElementById('<%=drpCategory.ClientID%>').focus;
                return false;
            }

            var strSerCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strSerCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }

            var strServName = document.getElementById('<%=txtServName.ClientID%>').value
            if (/\S+/.test(strServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Name";
                document.getElementById('<%=txtServName.ClientID%>').focus;
                return false;
            }

            if (document.getElementById('<%=drpServType.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Service Type";
                document.getElementById('<%=drpServType.ClientID%>').focus;
                return false;
            }



            return true;
        }

        function BindCompFee() {

            if (document.getElementById('<%=txtCompFee.ClientID%>').value == "") {
                document.getElementById('<%=txtCompFee.ClientID%>').value = document.getElementById('<%=txtServFee.ClientID%>').value;

            }

        }

        function HaadShow() {
            var CodeType = document.getElementById('<%=drpCodeType.ClientID%>').value
            var ServCode = document.getElementById('<%=txtHaadCode.ClientID%>').value

            var win = window.open("HaadServiceLookup.aspx?CtrlName=Code&CodeType=" + CodeType + "&Code=" + ServCode + "&CategoryType=All", "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
            return false;

        }

        function HaadShow1() {
            var CodeType = document.getElementById('<%=drpCodeType.ClientID%>').value
            var ServName = document.getElementById('<%=txtHaadName.ClientID%>').value

            var win = window.open("HaadServiceLookup.aspx?CtrlName=Name&CodeType=" + CodeType + "&Name=" + ServName + "&CategoryType=All", "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
            return false;

        }

        function DeleteVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strSerCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strSerCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }
            var isDelCompany = window.confirm('Do you want to delete Service Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }

        function BindHaadDtls(HaadCode, HaadName, CtrlName) {

            document.getElementById("<%=txtHaadCode.ClientID%>").value = HaadCode;
            document.getElementById("<%=txtHaadName.ClientID%>").value = HaadName;

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <table width="70px">
        <tr>
            <td class="PageHeader">خدمة ماستر
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:updatepanel id="UpdatePanel0" runat="server">
                    <contenttemplate>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

                    </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="5" cellspacing="5">
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label1" runat="server" CssClass="label"
                    Text="فئة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
                <asp:DropDownList ID="drpCategory" CssClass="label" runat="server" Width="150px"  BorderWidth="1px" BorderColor="Red"  AutoPostBack="true" OnSelectedIndexChanged="drpCategory_SelectedIndexChanged">
                </asp:DropDownList>
                 
                <asp:CheckBox ID="chkManual" runat="server" CssClass="label" Text="كتيب" Checked="false"  AutoPostBack="True"  OnCheckedChanged="chkManual_CheckedChanged" />
                    </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label2" runat="server" CssClass="label"
                    Text="رسوم الخدمة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtServFee" CssClass="label"  runat="server" Width="75px" MaxLength="10" BorderWidth="1px" Style="text-align: right;" BorderColor="#cccccc" onblur="return BindCompFee()"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label13" runat="server" CssClass="label"
                    Text="تأمين نوع من العلاج"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;<br />
                <asp:checkbox runat="server" id="chkAllTreatment" CssClass="label"   Text="جميع"  AutoPostBack="True"  OnCheckedChanged="chkAllTreatment_CheckedChanged"  ></asp:checkbox>
                
            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label3" runat="server" CssClass="label"
                    Text="قانون"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel4" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtServCode" CssClass="label"  runat="server" Width="150px" MaxLength="10" BorderWidth="1px" BorderColor="Red" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged"></asp:TextBox>

                <asp:CheckBox ID="chkStatus" runat="server" CssClass="label" Text="نشط" Checked="true" />
                    </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label4" runat="server" CssClass="label"
                    Text="رسوم شركة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel5" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtCompFee" CssClass="label"  runat="server" Width="75px" MaxLength="100" BorderWidth="1px"  Style="text-align: right;" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>
            <td rowspan="6" valign="top">
                <asp:updatepanel id="UpdatePanel19" runat="server">
                    <contenttemplate>
                         <div runat="server" id="div1" style="padding-top: 0px; width: 170px; height: 150px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
                <asp:CheckBoxList runat="server" CssClass="label"  id="lstTreatmentType" Width="150px"   RepeatLayout="Flow"  ></asp:CheckBoxList>

                             </div>
                   </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label5" runat="server" CssClass="label"
                    Text="اسم"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel6" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtServName" CssClass="label"  runat="server" Width="90%" MaxLength="100" BorderWidth="1px" BorderColor="Red" AutoPostBack="true" OnTextChanged="txtServName_TextChanged"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>

            </td>
            <td style="width: 150px">
                <asp:Label ID="Label6" runat="server" CssClass="label"
                    Text="كلفة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel7" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtCost" CssClass="label"  runat="server" Width="75px" MaxLength="10" BorderWidth="1px"  Style="text-align: right;" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                     </contenttemplate>
                </asp:updatepanel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label7" runat="server" CssClass="label"
                    Text="نوع الخدمة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel8" runat="server">
                    <contenttemplate>
                    <asp:DropDownList ID="drpServType" CssClass="label" runat="server" Width="150px" BorderWidth="1px" BorderColor="Red">
                    </asp:DropDownList>
                    <asp:CheckBox ID="chkObseNeeded" runat="server" CssClass="label" Text="الملاحظة مطلوب" Checked="false" />
                    </contenttemplate>
                </asp:updatepanel>

            </td>
            <td style="width: 150px">
                <asp:Label ID="Label8" runat="server" CssClass="label"
                    Text="نوع الخصم"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel9" runat="server">
                    <contenttemplate>
                    <asp:DropDownList ID="drpDiscType" CssClass="label" runat="server" Width="75px" BorderWidth="1px"  BorderColor="#CCCCCC">
                        <asp:ListItem Text="$" Value="$"></asp:ListItem>
                        <asp:ListItem Text="%" Value="%"></asp:ListItem>
                    </asp:DropDownList>
                     </contenttemplate>
                </asp:updatepanel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label9" runat="server" CssClass="label"
                    Text="مجموعة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel10" runat="server">
                    <contenttemplate>
                <asp:DropDownList ID="drpServiceGroup" CssClass="label" runat="server" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC">
                </asp:DropDownList>
                <asp:CheckBox ID="chkSavePrice" runat="server" CssClass="label" Text="حفظ الأسعار لجميع الشركات" Checked="false" ToolTip="Save Price to All Company" />
                  </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label10" runat="server" CssClass="label"
                    Text="مقدار الخصم"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel11" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtDiscAmount" CssClass="label"  runat="server" Width="75px" MaxLength="10" BorderWidth="1px"  Style="text-align: right;" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                     </contenttemplate>
                </asp:updatepanel>
            </td>
            <td></td>
        </tr>

        <tr>

            <td style="width: 150px;">
                <asp:Label ID="Label14" runat="server" CssClass="label"
                    Text="نوع كود"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel12" runat="server">
                    <contenttemplate>
                <asp:DropDownList ID="drpCodeType" CssClass="label" runat="server" Width="150px" BorderWidth="1px"  BorderColor="#CCCCCC">
                </asp:DropDownList>
                <asp:CheckBox ID="chkFavorite" runat="server" CssClass="label" Text="مفضل" Checked="false" ToolTip="Favorite" />
                    </contenttemplate>
                </asp:updatepanel>
            </td>
            <td style="width: 150px;">
                <asp:Label ID="Label11" runat="server" CssClass="label"
                    Text="رمز مرجع"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel13" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtRefCode" CssClass="label"  runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>

    <table width="100%">
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label18" runat="server" CssClass="label"
                    Text="كود قانون"></asp:Label>
            </td>
            <td colspan="4">
                <asp:updatepanel id="UpdatePanel14" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtHaadCode" CssClass="label" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" AutoPostBack="true" ondblclick="HaadShow();" OnTextChanged="txtHaadCode_TextChanged" BorderColor="#cccccc" ToolTip="Haad Code" ></asp:TextBox>
                <asp:TextBox ID="txtHaadName" CssClass="label" runat="server" Width="55%" MaxLength="100" BorderWidth="1px"   ondblclick="HaadShow1();"   BorderColor="#cccccc"></asp:TextBox>
                    </contenttemplate>
                </asp:updatepanel>

            </td>
            <td>
                <asp:updatepanel id="UpdatePanel15" runat="server">
                    <contenttemplate>
                <asp:CheckBox ID="chkAllowFactor" runat="server" CssClass="label" Text="Apply facotr price for this service" Checked="false" />
                         </contenttemplate>
                </asp:updatepanel>
            </td>
              <td>
                <asp:updatepanel id="UpdatePanel21" runat="server">
                    <contenttemplate>
                <asp:CheckBox ID="chkPackageService" runat="server" CssClass="label" Text="Package service" Checked="false" />
                         </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
        <tr>
            <td style="width: 150px; height: 30px">
                <asp:Label ID="Label15" runat="server" CssClass="label"
                    Text="كود الملاحظة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel16" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtObsCode" CssClass="label" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ToolTip="Observation Code"></asp:TextBox>
                 </contenttemplate>
                </asp:updatepanel>

            </td>
            <td style="width: 150px">
                <asp:Label ID="Label16" runat="server" CssClass="label"
                    Text="نوع القيمة"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel17" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtObsValueType" CssClass="label" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ToolTip="Value Type"></asp:TextBox>
                           </contenttemplate>
                </asp:updatepanel>
            </td>
            <td>
                <asp:Label ID="Label17" runat="server" CssClass="label"
                    Text="تحويل"></asp:Label>
            </td>
            <td>
                <asp:updatepanel id="UpdatePanel18" runat="server">
                    <contenttemplate>
                <asp:TextBox ID="txtObsConversion" CssClass="label" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ToolTip="Conversion"></asp:TextBox>
                           </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>



    <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="حفظ" ToolTip="Save" />
    <asp:Button ID="btnDelete" runat="server" CssClass="button orange small" Width="100px" OnClick="btnDelete_Click" OnClientClick="return DeleteVal();" Text="حذف" visible="true" ToolTip="Delete" />
    <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="100px" OnClick="btnClear_Click" Text="واضح" ToolTip="Clear" />
    <br />   <br />
    <div runat="server" id="divTran" style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
        <asp:updatepanel id="UpdatePanel2" runat="server">
            <contenttemplate>
    <asp:GridView ID="gvServices" runat="server" AllowPaging="True" AutoGenerateColumns="False"  OnRowDataBound="gvServices_RowDataBound"
        EnableModelValidation="True" Width="100%" PageSize="200" OnPageIndexChanging="gvServices_PageIndexChanging">
        <HeaderStyle CssClass="GridHeader"  Font-Bold="true" />
        <RowStyle CssClass="GridRow" />
        <Columns>
               <asp:TemplateField HeaderText="عدد" SortExpression="HPV_SEQNO">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Select_Click" >
                                     <asp:Label ID="lblServiceId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>' visible="false" ></asp:Label>
                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" style="text-align: right;" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

            <asp:TemplateField HeaderText="قانون" >
                <ItemTemplate>
                    <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_SERV_ID") %>' ToolTip="Code"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="الخدمات">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkServices" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_NAME") %>' ToolTip="Services"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="رسوم">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkFee" runat="server" OnClick="Select_Click" > 
                     <asp:Label ID="Label19" CssClass="GridRow" Width="70px" runat="server" style="text-align: right;padding-right:3px;" Text='<%# Eval("HSM_FEE","{0:0.00}") %>'    ToolTip="Fee"></asp:Label>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="رسوم شركة">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkCompFee" runat="server" OnClick="Select_Click" >
                         <asp:Label ID="Label12" CssClass="GridRow" Width="100px" runat="server" style="text-align: right;padding-right:5px;" Text='<%# Eval("HSM_COMP_FEE","{0:0.00}") %>'  ToolTip="Company Fee" ></asp:Label>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="فئة" >
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click" Text='<%# Bind("HSCM_NAME") %>' ToolTip="Category"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="وضع">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Select_Click" >
                    <asp:Label ID="Label20" CssClass="GridRow" Width="100%" runat="server"   Text='<%# Bind("HSM_STATUS") %>' ToolTip="Status"></asp:Label>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="كود قانون">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click" style="text-align:right;" Text='<%# Bind("HSM_HAAD_CODE") %>' ToolTip="HAAD Code"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="مجموعة">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_GROUP") %>' ToolTip="Group"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="الملاحظة المطلوبة">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="Select_Click" >
                      <asp:Label ID="Label21" CssClass="GridRow" Width="100%" runat="server"   Text='<%# Bind("HSM_OBSERVATION_NEEDED") %>' ToolTip="Obs. Needed"></asp:Label>

                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="كود الملاحظة">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton6" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_OBS_CODE") %>' ToolTip="Obs. Code"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="نوع القيمة">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton7" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_OBS_VALUETYPE") %>' ToolTip="Value Type"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="تحويل">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton8" runat="server" OnClick="Select_Click" Text='<%# Bind("HSM_OBS_CONVERSION") %>' ToolTip="Conversion"> </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
          <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

    </asp:GridView>
      </contenttemplate>
        </asp:updatepanel>
    </div>

    <asp:LinkButton ID="lnkService" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
        ForeColor="Blue"></asp:LinkButton>
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkService"
        PopupControlID="pnlService" BackgroundCssClass="modalBackground" CancelControlID="btnServiceClose"
        PopupDragHandleControlID="pnlService">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlService" runat="server" Height="500px" Width="700px" CssClass="modalPopup">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="btnServiceClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                </td>
            </tr>
            <tr>
                <td align="left" style="height: 230px;">

                    <div style="padding-top: 0px; width: 680px; height: 400px; overflow: auto; border-color: #e3f7ef; border-style: groove;">
                        <asp:updatepanel id="UpdatePanel20" runat="server">
                            <contenttemplate>
                        <asp:GridView ID="gvHaadService" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="650">
                            <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>


                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                         <asp:Label ID="lblHaadServId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHS_CODE") %>' visible="false" ></asp:Label>
                                        <asp:Label ID="lblHaadServDesc" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHS_DESCRIPTION") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkServId" runat="server" OnClick="HaadSelect_Click" Text='<%# Bind("HHS_CODE") %>'></asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Service Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkServName" runat="server" OnClick="HaadSelect_Click" Text='<%# Bind("HHS_DESCRIPTION") %>'></asp:LinkButton>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkType" runat="server" OnClick="HaadSelect_Click" Text='<%# Bind("HHS_TYPE") %>'></asp:LinkButton>

                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                           </contenttemplate>
                        </asp:updatepanel>

                    </div>


                </td>
            </tr>

        </table>
    </asp:Panel>
    <br />
    <style type="text/css">
        .style1 {
            width: 20px;
            height: 32px;
        }

        .modalBackground {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>
</asp:Content>
