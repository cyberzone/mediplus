﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Mediplus_BAL;
using System.Data;


namespace Mediplus.HMS.Masters
{
    public partial class DepartmentMaster : System.Web.UI.Page
    {
        MastersBAL objMast = new MastersBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDepttMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


             CommonBAL objCom = new  CommonBAL ();
             DS = objCom.DepMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvDepttMaster.DataSource = DS;

                gvDepttMaster.DataBind();
            }
            else
            {
                gvDepttMaster.DataBind();
            }


        }

         
        void Clear()
        {
            txtDeptCode.Text = "";
            txtDeptName.Text = "";
            chkStatus.Checked = true;
            txtDeptCode.Enabled = true;
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            lblStatus.Text = "";
            if (!IsPostBack)
            {
                BindDepttMaster();
            }
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvDepttMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvDepttMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblDeptCode = (Label)gvScanCard.Cells[0].FindControl("lblDeptCode");
                Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");

                Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");


                txtDeptCode.Text = lblDeptCode.Text;
                txtDeptCode.Enabled = false;
                txtDeptName.Text = lblDeptName.Text;

                if (lblStatus.Text == "A")
                {
                    chkStatus.Checked = true;
                }
                else
                {
                    chkStatus.Checked = false;
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDeptCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Dept Code";
                    goto FunEnd;
                }

                if (txtDeptName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Dept Name";
                    goto FunEnd;
                }

              
                DataSet DS = new DataSet();
                string Criteria = " 1=1 AND HDM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  ";
                Criteria += " AND  (  HDM_DEP_ID = '" + txtDeptCode.Text.Trim() + "' OR  HDM_DEP_NAME='" + txtDeptName.Text.Trim() + "'  ) ";


                if (txtDeptCode.Text.Trim() != "" && txtDeptCode.Text.Trim()  != null)
                {
                    Criteria += " AND  HDM_DEP_ID != '" + txtDeptCode.Text.Trim() + "'";
                }

                CommonBAL objCom = new CommonBAL();
                DS = objCom.DepMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "Same Dept Dtls Already Exists";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }




                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.DEP_ID = txtDeptCode.Text.Trim();
                objCom.DEP_NAME = txtDeptName.Text.Trim();

                if (chkStatus.Checked == true)
                {
                    objCom.Status = "A";
                }
                else
                {
                    objCom.Status = "I";
                }
               
                objCom.UserID = Convert.ToString(Session["User_ID"]);
                objCom.DepMasterAdd();

                Clear();
                BindDepttMaster();
                lblStatus.Text = "Saved Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;




                Label lblDeptCode = (Label)gvScanCard.Cells[0].FindControl("lblDeptCode");
                Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");

                eAuthorizationBAL eAuth = new eAuthorizationBAL();


                CommonBAL obCom = new CommonBAL();
                DataSet DSPay = new DataSet();
                string Criteria1 = " 1=1 ";
                Criteria1 += " AND  HSFM_DEPT_ID='" + lblDeptName.Text + "' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                DSPay = obCom.GetStaffMaster(Criteria1);

                if (DSPay.Tables[0].Rows.Count > 0)
                {
                    lblStatus.Text = "This Department Code is Involved another transaction ";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                //ReceiptEntryBAL objRec = new ReceiptEntryBAL();
                //DataSet DSRec = new DataSet();

                //string Criteria2 = " 1=1 ";
                //Criteria2 += " AND  ARE_ACC_CODE='" + lblDeptCode.Text.Trim() + "' AND ARE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                //DSRec = objRec.ReceiptEntryGet(Criteria2);

                //if (DSRec.Tables[0].Rows.Count > 0)
                //{

                //    lblStatus.Text = "This Account Code is Involved another transaction ";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;

                //}


                CommonBAL objCom = new CommonBAL();
                string Criteria = " HDM_DEP_ID='" + lblDeptCode.Text + "'";
               objCom.fnDeleteTableData("HMS_DEP_MASTER", Criteria);



                lblStatus.Text = " Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindDepttMaster();


            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
     
        #endregion

    }
}