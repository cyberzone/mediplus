﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="RollMaster.aspx.cs" Inherits="Mediplus.HMS.Masters.RollMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

      <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script src="../Validation.js" type="text/javascript"></script>

      
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }

         function fnSelectPermission(chk) {

             var row = chk.parentNode.parentNode;
             var rowIndex = row.rowIndex - 1;

             var chkValue = chk.checked;


             document.getElementById("ContentPlaceHolder1_gvRollTrans_chkNoPermission_" + rowIndex).checked = false;
             document.getElementById("ContentPlaceHolder1_gvRollTrans_chkReadOnly_" + rowIndex).checked = false;
             document.getElementById("ContentPlaceHolder1_gvRollTrans_chkCreate_" + rowIndex).checked = false;
             document.getElementById("ContentPlaceHolder1_gvRollTrans_chkModify_" + rowIndex).checked = false;
             document.getElementById("ContentPlaceHolder1_gvRollTrans_chkDelete_" + rowIndex).checked = false;
             document.getElementById("ContentPlaceHolder1_gvRollTrans_chkFullPermission_" + rowIndex).checked = false;


             chk.checked = chkValue;


         }


         function ShowRollMaster() {

             document.getElementById("divRollMaster").style.display = 'block';


         }

         function HideRollMaster() {

             document.getElementById("divRollMaster").style.display = 'none';

         }

         function ShowErrorMessage(vMessage1, vMessage2) {

             document.getElementById("divMessage").style.display = 'block';

             document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
          }

          function HideErrorMessage() {

              document.getElementById("divMessage").style.display = 'none';

              document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

      <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="width: 100%; height: 20px; background: #E3E3E3;">
                        &nbsp;
                        <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                    </div>
                    &nbsp;
                    <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
      </div>
        <div style="width: 80%;">
            <table width="100%">
                <tr>
                    <td class="lblCaption1">ID
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtRollID" runat="server" CssClass="TextBoxStyle" Width="100px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtRollID_TextChanged"></asp:TextBox>
                                <asp:ImageButton  ID="imgbtnRollView"    runat="server" ToolTip="View selected Diagnosis" ImageUrl="~//Images/zoom.png" 
                                                       OnClick="imgbtnRollView_Click"  style="height:15px;width:25px;"  />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td class="lblCaption1">Name
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtRollName" runat="server" CssClass="TextBoxStyle" Width="200px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1">Active
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="chkActive" runat="server" Checked="true"  />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                  <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Text="Clear" OnClick="btnClear_Click" />
                              
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px;padding-left:5px; height:600px; width: 100%;   overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvRollTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="5" CellSpacing="0"
                            EnableModelValidation="True" Width="100%" GridLines="None" OnRowDataBound="gvRollTrans_RowDataBound">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="MAIN MENU NAME" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                         <asp:Label ID="lblMainMenuName" CssClass="GridRow" runat="server" Text='<%# Bind("HSCR_MAIN_MENUNAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SCREEN NAME" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScreenID" CssClass="GridRow" runat="server" Text='<%# Bind("HSCR_SCREEN_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblPermission" CssClass="GridRow" runat="server" Text='<%# Bind("HRT_PERMISSION") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblScreenName" CssClass="GridRow" runat="server" Text='<%# Bind("HSCR_SCREEN_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField  HeaderText="NO PERMISSION" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                        <asp:CheckBox ID="chkNoPermission"  runat="server" onClick="fnSelectPermission(this)" />
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="READ ONLY" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkReadOnly" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CREATE" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCreate" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MODIFY" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkModify" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DELETE" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkDelete" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FULL PERMISSION" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkFullPermission" runat="server" onClick="fnSelectPermission(this)"/>
                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>


              <div id="divRollMaster" style=" display: none;border: groove; height: 350px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #005c7b; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:200px;top:100px;">
                     <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">
                                     <asp:UpdatePanel runat="server" ID="updatePanel12">
                                    <ContentTemplate>
                                    <input type="button" id="btnHistoryClose" class="ButtonStyle" style="color:  #005c7b;background-color:#f6f6f6; border: none;border-radius: 0px;font-weight:bold;cursor:pointer;" value=" X " onclick="HideRollMaster()" />
                                        </ContentTemplate>
                                         </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                         <asp:UpdatePanel runat="server" ID="updatePanel14">
                                    <ContentTemplate>
                                   <div style="padding-top: 0px; width: 580px;overflow:auto;height:300px; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">

                                        <asp:GridView ID="gvRollMaster" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   GridLines="None"   Width="100%"  >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />

                                             <Columns>
                                                 <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgRollMasterSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                            OnClick="imgRollMasterSelect_Click" />&nbsp;&nbsp;
                                                    </ItemTemplate>
                        </asp:TemplateField>
                                      <asp:TemplateField HeaderText="ID" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                           
                                            <asp:Label ID="lblID" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("HRM_ROLL_ID") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                           
                                            <asp:Label ID="lblName" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("HRM_ROLL_NAME") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="600px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" CssClass="label" Font-Size="11px" Width="100%"  runat="server" Text='<%# Bind("HRM_STATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    
                                </Columns>
                                  </asp:GridView>

                                       </div>
                                    </ContentTemplate>
                                        
                                        </asp:UpdatePanel>
                                         

                                  
               </div>
        </div>

    <br />
    <br />
</asp:Content>
