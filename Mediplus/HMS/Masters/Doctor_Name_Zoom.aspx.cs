﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Mediplus_BAL;
namespace Mediplus.HMS.Masters
{
    public partial class Doctor_Name_Zoom : System.Web.UI.Page
    {
        
        dboperations dbo = new dboperations();
        DataSet ds = new DataSet();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDept()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' ";
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDepartment.DataSource = ds;
                drpDepartment.DataValueField = "HDM_DEP_NAME";
                drpDepartment.DataTextField = "HDM_DEP_NAME";
                drpDepartment.DataBind();
                drpDepartment.Items.Insert(0, "--- All ---");
                drpDepartment.Items[0].Value = "0";

            }

        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_BRANCH_ID='" + Session["Branch_ID"] + "'";
          
            if (txtDoctorId.Text != "")
            {

                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HSFM_STAFF_ID like '" + txtDoctorId.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HSFM_STAFF_ID  like '%" + txtDoctorId.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HSFM_STAFF_ID   = '" + txtDoctorId.Text + "' ";
                }



            }

            if (txtDoctorName.Text != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HSFM_FNAME like '" + txtDoctorName.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + txtDoctorName.Text + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   = '" + txtDoctorName.Text + "' ";
                }

            }


            if (drpDepartment.SelectedIndex != 0)
            {
                Criteria += " AND HSFM_DEPT_ID = '" + drpDepartment.SelectedValue + "'";
            }



            ds = dbo.retun_doctor_detailss(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            gvGridView.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    BindDept();

                    hidPageName.Value = Request.QueryString["PageName"].ToString();
                    ViewState["CtrlName"] = Request.QueryString["CtrlName"].ToString();
                    ViewState["Value"] = Request.QueryString["Value"].ToString();

                    if (ViewState["CtrlName"] != "")
                    {
                        if (ViewState["CtrlName"].ToString() == "StaffId")
                        {
                            txtDoctorId.Text = ViewState["Value"].ToString();
                        }
                        else if (ViewState["CtrlName"].ToString() == "Name")
                        {
                            txtDoctorName.Text = ViewState["Value"].ToString();

                        }


                        BindGrid();
                    }
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Doctor_Name_Zoom.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["PageName"] = "";
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Doctor_Name_Zoom.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                Session["ErrorMsg"] = ex.Message;
                Response.Redirect("ErrorPage.aspx");
            }


        }
        
        protected void drpDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}