﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;

namespace Mediplus.HMS.Masters
{
    public partial class InitialNumber : System.Web.UI.Page
    {
        DataSet DS = new DataSet();


        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindInitialNo()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HIN_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DS = new DataSet();

            InitialNumberBAL objINM = new InitialNumberBAL();
            DS = objINM.GetInitialNumber(Criteria);
            gvInitialNo.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInitialNo.Visible = true;
                gvInitialNo.DataSource = DS;
                gvInitialNo.DataBind();
            }
            else
            {
                gvInitialNo.DataBind();
            }
        }


        void ModifyInitialNo()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  HIN_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND  HIN_SCRN_ID='" + Convert.ToString(txtScreenCode.Text.Trim()) + "'";

            DS = new DataSet();

            InitialNumberBAL objINM = new InitialNumberBAL();
            DS = objINM.GetInitialNumber(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtScreenName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIN_SCRN_NAME"]);
                txtPrefix.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIN_PREFIX"]);
                txtLastNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIN_LASTNO"]);
            }
        }
        void Clear()
        {
            txtScreenCode.Text = "";
            txtScreenName.Text = "";
            txtPrefix.Text = "";
            txtLastNo.Text = "";
            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvInitialNo.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["ServiceId"] = "0";
            ViewState["SelectIndex"] = "";

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='SERV' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "0" || strPermission == "1")
            {

                btnSave.Enabled = false;
                btnDelete.Enabled = false;
                btnClear.Enabled = false;



            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {

                    BindInitialNo();
                }

                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      InitialNumber.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                    lblStatus.Text = "Data Not Saved";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void txtScreenCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ModifyInitialNo();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.txtScreenCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                InitialNumberBAL objINM = new InitialNumberBAL();
                objINM.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objINM.ScreenCode = txtScreenCode.Text.Trim();
                objINM.ScreenName = txtScreenName.Text.Trim();
                objINM.Prefix = txtPrefix.Text.Trim();
                objINM.LastNo = txtLastNo.Text.Trim();
                objINM.UserId = Convert.ToString(Session["User_ID"]);
                objINM.AddInitialNumber();

                Clear();
                BindInitialNo();
                lblStatus.Text = "Data Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            }

            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      InitialNumber.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtScreenCode.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                InitialNumberBAL objINM = new InitialNumberBAL();
                objINM.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objINM.ScreenCode = txtScreenCode.Text.Trim();
                objINM.UserId = Convert.ToString(Session["User_ID"]);
                objINM.DeleteInitialNumber(objINM);

                Clear();
                BindInitialNo();
                lblStatus.Text = "Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      InitialNumber.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvInitialNo.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;


                gvInitialNo.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;

                //Label lblTLeadId;

                //lblTLeadId = (Label)gvScanCard.Cells[0].FindControl("lblLeadId");

                //ViewState["LeadId"] = lblTLeadId.Text;


                Label lblScreenId, lblScreenName, lblPrefix, lblLastNo;


                lblScreenId = (Label)gvScanCard.Cells[0].FindControl("lblScreenId");
                lblScreenName = (Label)gvScanCard.Cells[0].FindControl("lblScreenName");
                lblPrefix = (Label)gvScanCard.Cells[0].FindControl("lblPrefix");
                lblLastNo = (Label)gvScanCard.Cells[0].FindControl("lblLastNo");



                txtScreenCode.Text = lblScreenId.Text;
                txtScreenName.Text = lblScreenName.Text;
                txtPrefix.Text = lblPrefix.Text;
                txtLastNo.Text = lblLastNo.Text;


                // ModifyInitialNo();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvInitialNo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Clear();

            gvInitialNo.PageIndex = e.NewPageIndex;
            BindInitialNo();
        }

        protected void gvInitialNo_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvInitialNo.PageIndex * gvInitialNo.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }
        #endregion
    }
}