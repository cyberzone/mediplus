﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Mediplus_BAL;
using System.IO;

namespace Mediplus.HMS.Masters
{
    public partial class StaffProfileView : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ModifyStaff()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtStaffId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STAFF_ID"]);

                txtCategory.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CATEGORY"]);
                txtServCategory.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SRV_CATEGORY"]);

                txtDepartment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]);


                txtStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SF_STATUS"]);


                txtDesignation.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DESIG"]);





                if (DS.Tables[0].Rows[0].IsNull("HSFM_FNAME") == false)
                    txtFName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MNAME") == false)
                    txtMName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_LNAME") == false)
                    txtLName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_DOB") == false)
                    txtDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DOB"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_AGE") == false)
                    txtAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_AGE"]);



                if (DS.Tables[0].Rows[0].IsNull("HSFM_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SEX"]) != "0")

                txtSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SEX"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MARITAL") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MARITAL"]) != "0")
                txtMStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MARITAL"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_CHILD") == false)
                    txtChildren.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_CHILD"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_RELIGION") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_RELIGION"]) != "0")
                txtReligion.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_RELIGION"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_BLOODGROUP") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BLOODGROUP"]) != "0")
                txtBlood.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BLOODGROUP"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_EDU_QLFY") == false)
                    txtEduQuali.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EDU_QLFY"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_POBOX") == false)
                    txtPOBox.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_POBOX"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_ADDR") == false)
                    txtAddress.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ADDR"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_COUNTRY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COUNTRY"]) != "0")
                txtCountry.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COUNTRY"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_STATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STATE"]) != "0")
                txtState.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STATE"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_CITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CITY"]) != "0")
                txtCity.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_CITY"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_NLTY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NLTY"]) != "0")
                txtNationality.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NLTY"]);


                if (DS.Tables[0].Rows[0].IsNull("HSFM_PHONE1") == false)
                    txtPhone1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PHONE1"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_PHONE2") == false)
                    txtPhone2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PHONE2"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_FAX") == false)
                    txtFax.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FAX"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOBILE") == false)
                    txtMobile.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOBILE"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_EMAIL") == false)
                    txtEmail.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMAIL"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_PASSPORT") == false)
                //        txtPassport.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PASSPORT"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_PASS_ISSUE") == false)
                //        txtPassIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PASS_ISSUEDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_PASS_EXPDesc") == false)
                //        txtPassExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PASS_EXPDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_SS_ID") == false)
                //        txtID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SS_ID"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_SS_ISSUE") == false)
                //        txtIDIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SS_ISSUEDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_SS_EXP") == false)
                //        txtIDExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SS_EXPDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_DRV_LIC") == false)
                //        txtDrivLic.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DRV_LIC"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_DRV_LIC_ISSUE") == false)
                //        txtDrivLicIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DRV_LIC_ISSUEDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_DRV_LIC_EXPDesc") == false)
                //        txtDrivLicExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DRV_LIC_EXPDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_NO") == false)
                //        txtHAAD.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_ISSUE") == false)
                //        txtHAADIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_ISSUEDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_EXPDesc") == false)
                //        txtHAADExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_EXPDesc"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHER_NO") == false)
                //        txtDocOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHER_NO"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHER_ISSUE") == false)
                //        txtDocOtherIssueOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHER_ISSUEDesc"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHER_EXP") == false)
                //        txtDocOtherExpOn.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHER_EXPDesc"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_BASIC") == false)
                //        txtBasic.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_BASIC"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_HRA") == false)
                //        txtHRA.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_HRA"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_DA") == false)
                //        txtDA.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DA"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHR_ALLOW") == false)
                //        txtOtherAllow.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHR_ALLOW"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_VACATIONPAY") == false)
                //        txtVacationPay.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_VACATIONPAY"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_HOMETRAVEL") == false)
                //        txtAssuredComm.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_HOMETRAVEL"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_WORKINGHRS") == false)
                //        txtWorkingHours.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_WORKINGHRS"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_FRI_COMPENSATION") == false)
                //        txtFriComp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FRI_COMPENSATION"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_FIXEDOVERTIME") == false)
                //        txtFixedOT.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FIXEDOVERTIME"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_OVERTIMEPHR") == false)
                //        txtOTPerHour.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OVERTIMEPHR"]);

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_ABSENTPHR") == false)
                //        txtAbsePerHour.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ABSENTPHR"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_ADV_SALARY") == false)
                //        txtAdvSalary.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ADV_SALARY"]);


                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_ROSTER_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]) != "0")
                //    {
                //        for (int intRoaster = 0; intRoaster < drpDutyRoaster.Items.Count; intRoaster++)
                //        {
                //            if (drpDutyRoaster.Items[intRoaster].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]))
                //            {
                //                drpDutyRoaster.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROSTER_ID"]);
                //                goto ForRoaster;
                //            }

                //        }
                //    }
                //ForRoaster: ;

                //    if (DS.Tables[0].Rows[0].IsNull("HSFM_COMM_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]) != "0")
                //    {
                //        for (int intCommission = 0; intCommission < drpCommission.Items.Count; intCommission++)
                //        {
                //            if (drpCommission.Items[intCommission].Value == Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]))
                //            {
                //                drpCommission.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_COMM_ID"]);
                //                goto ForCommission;
                //            }

                //        }
                //    }
                //ForCommission: ;


                //if (DS.Tables[0].Rows[0].IsNull("HSFM_LOAN") == false)
                //    txtLoan.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LOAN"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NOOFPAYMENT") == false)
                //    txtNoInstallment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NOOFPAYMENT"]);


                //if (DS.Tables[0].Rows[0].IsNull("HSFM_LOANDEDUCT") == false)
                //    txtloanDeduct.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LOANDEDUCT"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_DOJ") == false)
                //    txtJoinDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DOJDesc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_DOEXIT") == false)
                //    txtExitDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DOEXITDesc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_APNT_TIME_INT") == false)
                //    txtInterval.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_APNT_TIME_INT"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM1") == false)
                //    txtFrom1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM1Desc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM1TIME") == false)
                //    txtFrom1Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM1TIME"]);


                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM2") == false)
                //    txtFrom2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM2Desc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM2TIME") == false)
                //    txtFrom2Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM2TIME"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATEFROM3") == false)
                //    txtFrom3.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATEFROM3Desc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_FROM3TIME") == false)
                //    txtFrom3Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_FROM3TIME"]);


                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO1") == false)
                //    txtTo1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO1Desc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO1TIME") == false)
                //    txtTo1Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO1TIME"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO2") == false)
                //    txtTo2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO2Desc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO2TIME") == false)
                //    txtTo2Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO2TIME"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_DATETO3") == false)
                //    txtTo3.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_DATETO3Desc"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_NO_APNT_TO3TIME") == false)
                //    txtTo3Time.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_NO_APNT_TO3TIME"]);


                //if (DS.Tables[0].Rows[0].IsNull("HSFM_PRE_DRTOKEN") == false)
                //    txtTokenPrefix.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_PRE_DRTOKEN"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_START") == false)
                //    txtTokenStart.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_START"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_END") == false)
                //    txtTokenEnd.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_END"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_CURRENT") == false)
                //    txtTokenCurrent.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_CURRENT"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_TOKEN_RESERVE") == false)
                //    txtTokenReserve.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_TOKEN_RESERVE"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_ROOM") == false)
                //    txtRoom.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_ROOM"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS1") == false)
                //    txtServiceCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS1"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS2") == false)
                //    txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS2"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS3") == false)
                //    txtFollowUp1.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS3"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS4") == false)
                //    txtFollowUp2.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS4"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_OTHERS5") == false)
                //    txtAutoInvOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_OTHERS5"]);

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_EMR_DATE") == false)
                //    if (Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMR_DATE"]) == "1")
                //        chkEMRViewDate.Checked = true;

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_EMR_DEP") == false)
                //    if (Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMR_DEP"]) == "1")
                //        chkEMRViewDept.Checked = true;

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_EMR_CAT") == false)
                //    if (Convert.ToString(DS.Tables[0].Rows[0]["HSFM_EMR_CAT"]) == "1")
                //        chkEMRViewCat.Checked = true;



                BindPhoto();
            }

        }

        void BindPhoto()
        {
            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            string Criteria = " 1=1  ";
            Criteria += " AND HSP_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

            DS = dbo.StaffPhotoGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSP_PHOTO") == false)
                {
                    Session["SignatureImg1"] = (Byte[])DS.Tables[0].Rows[0]["HSP_PHOTO"];
                    imgStaffPhoto.Visible = true;
                    imgStaffPhoto.ImageUrl = "../DisplaySignature1.aspx";
                }





            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ModifyStaff();
            }
        }
    }
}