﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Mediplus_BAL;

using System.IO;

namespace Mediplus.HMS.Masters
{
    public partial class PasswordChange : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["PWD_FORMAT_REQUIRED"] = "0";

            ViewState["PWD_MIN_LENGTH"] = "0";
            ViewState["PWD_MAX_LENGTH"] = "0";
            ViewState["PWD_NUM_MIN_LENGTH"] = "0";
            ViewState["PWD_LETTER_MIN_LENGTH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_FORMAT_REQUIRED")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_FORMAT_REQUIRED"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MAX_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_MAX_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_NUM_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_NUM_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_LETTER_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PWD_LETTER_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }






                }
            }



        }

        private bool CheckPasswordFormat(string passwordText)
        {

            int minimumLength = Convert.ToInt32(ViewState["PWD_MIN_LENGTH"]), maximumLength = Convert.ToInt32(ViewState["PWD_MAX_LENGTH"]), minimumNumbers = Convert.ToInt32(ViewState["PWD_NUM_MIN_LENGTH"]), minimumLetters = Convert.ToInt32(ViewState["PWD_LETTER_MIN_LENGTH"]);//,   minimumSpecialCharacters = 1

            //Assumes that special characters are anything except upper and lower case letters and digits
            //Assumes that ASCII is being used (not suitable for many languages)

            int letters = 0;
            int digits = 0;
            int specialCharacters = 0;

            //Make sure there are enough total characters
            if (passwordText.Length < minimumLength)
            {
                ShowError("You must have at least " + minimumLength + " characters in your password.");
                return false;
            }
            //Make sure there are enough total characters
            if (passwordText.Length > maximumLength)
            {
                ShowError("You must have no more than " + maximumLength + " characters in your password.");
                return false;
            }

            foreach (var ch in passwordText)
            {
                if (char.IsLetter(ch)) letters++; //increment letters
                if (char.IsDigit(ch)) digits++; //increment digits
                /*
                                //Test for only letters and numbers...
                                if (!((ch > 47 && ch < 58) || (ch > 64 && ch < 91) || (ch > 96 && ch < 123)))
                                {
                                    specialCharacters++;
                                }
                 * */
            }
            //Make sure there are enough digits
            if (letters < minimumLetters)
            {
                ShowError("You must have at least " + minimumLetters + " letters in your password.");
                return false;
            }


            //Make sure there are enough digits
            if (digits < minimumNumbers)
            {
                ShowError("You must have at least " + minimumNumbers + " numbers in your password.");
                return false;
            }
            /*
            //Make sure there are enough special characters -- !(a-zA-Z0-9)
            if (specialCharacters < minimumSpecialCharacters)
            {
                ShowError("You must have at least " + minimumSpecialCharacters + " special characters (like @,$,%,#) in your password.");
                return false;
            }

             * */
            return true;
        }

        private void ShowError(string errorMessage)
        {
            lblStatus.Text = errorMessage;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

             if (!IsPostBack)
             {
                 BindScreenCustomization();
             }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["PWD_FORMAT_REQUIRED"]) != "0")
                {

                    if (CheckPasswordFormat(txtNewPwd.Text) == false)
                    {
                        goto FunEnd;
                    }

                }

                string Criteria = " 1=1 ";
                Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "' AND   HUM_USER_PASS= '" + txtOldPwd.Text.Trim() + "'";

                DataSet ds = new DataSet();
                dboperations dbo = new dboperations();
                ds = dbo.UserMasterGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dbOperation objDB = new dbOperation();
                    objDB.ExecuteReader("UPDATE HMS_USER_MASTER SET HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "' WHERE   HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "'");

                    lblStatus.Text = "Password Changed Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblStatus.Text = "Your Old Password Is Incorrect";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                
            }

        FunEnd: ;
        }
    }


}