﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" AutoEventWireup="true" CodeBehind="CompanyProfile.aspx.cs" Inherits="Mediplus.HMS.Masters.CompanyProfile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
               document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

               if (vColor != '') {

                   document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


                 }

                 document.getElementById("divMessage").style.display = 'block';



             }

             function HideErrorMessage() {

                 document.getElementById("divMessage").style.display = 'none';

                 document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

        function CopyVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
               label.style.color = 'red';
               document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

             var NextCompId = document.getElementById('<%=hidNextCompanyId.ClientID%>').value

               var CopyCompId = prompt("Enter New Company Code", NextCompId);
               document.getElementById('<%=hidCopyCompanyId.ClientID%>').value = CopyCompId;

             var CopyCompName = prompt("Enter New Company Name", CopyCompName, "Company Name");
             document.getElementById('<%=hidCopyCompanyName.ClientID%>').value = CopyCompName;

            return SaveVal();

        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br>";

            document.getElementById("<%=txtCompCode.ClientID%>").style.border = '1px solid  #999';
            document.getElementById("<%=txtBillToCode.ClientID%>").style.border = '1px solid  #999';
            document.getElementById("<%=txtCompName.ClientID%>").style.border = '1px solid #999';
            document.getElementById("<%=txtSignedDate.ClientID%>").style.border = '1px solid  #999';
            document.getElementById("<%=txtContStDate.ClientID%>").style.border = '1px solid  #999';

            var strCompanyCode = document.getElementById('<%=txtCompCode.ClientID%>').value
            if (/\S+/.test(strCompanyCode) == false) {
                document.getElementById("<%=txtCompCode.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtCompCode.ClientID%>').focus();
                }
                ErrorMessage += 'Company Codee is Empty.' + trNewLineChar;
                IsError = true;

            }

            var strBillToCode = document.getElementById('<%=txtBillToCode.ClientID%>').value
            if (/\S+/.test(strBillToCode) == false) {
                document.getElementById("<%=txtBillToCode.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtBillToCode.ClientID%>').focus();
                }
                ErrorMessage += 'Bill To Codee is Empty.' + trNewLineChar;
                IsError = true;

            }


            var strBillToCode = document.getElementById('<%=txtBillToCode.ClientID%>').value
            if (/\S+/.test(strBillToCode) == false) {
                document.getElementById("<%=txtBillToCode.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtBillToCode.ClientID%>').focus();
                }
                ErrorMessage += 'Bill To Codee is Empty.' + trNewLineChar;
                IsError = true;

            }


            var strCompName = document.getElementById('<%=txtCompName.ClientID%>').value
            if (/\S+/.test(strCompName) == false) {
                document.getElementById("<%=txtCompName.ClientID%>").style.border = '1px solid red';
                if (IsError == false) {
                    document.getElementById('<%=txtCompName.ClientID%>').focus();
                }
                ErrorMessage += 'Company Name is Empty.' + trNewLineChar;
                IsError = true;
            }

            var strSignedDate = document.getElementById('<%=txtSignedDate.ClientID%>').value
            if (/\S+/.test(strSignedDate) != false) {
                if (isDate(strSignedDate) == false) {
                    document.getElementById("<%=txtSignedDate.ClientID%>").style.border = '1px solid red';
                    document.getElementById('<%=txtSignedDate.ClientID%>').focus()
                    ErrorMessage += 'Date is Wrong.' + trNewLineChar;
                    IsError = true;
                }
            }

            var strContStDate = document.getElementById('<%=txtContStDate.ClientID%>').value
            if (/\S+/.test(strContStDate) != false) {
                if (isDate(strContStDate) == false) {
                    document.getElementById("<%=txtContStDate.ClientID%>").style.border = '1px solid red';
                    document.getElementById('<%=txtContStDate.ClientID%>').focus()
                    ErrorMessage += 'Cont. Start Date is Wrong.' + trNewLineChar;
                    IsError = true;
                }
            }

            var strContEndDate = document.getElementById('<%=txtContEnDate.ClientID%>').value
            if (/\S+/.test(strContEndDate) != false) {
                if (isDate(strContEndDate) == false) {
                    document.getElementById('<%=txtContEnDate.ClientID%>').focus()
                    ErrorMessage += 'Cont. End Date is Wrong.' + trNewLineChar;
                    IsError = true;
                }
            }

            if (IsError == true) {
                ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }



            return true
        }


        function DeleteCompanyVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strCompCode = document.getElementById('<%=txtCompCode.ClientID%>').value
            if (/\S+/.test(strCompCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company Code";
                document.getElementById('<%=txtCompCode.ClientID%>').focus;
                return false;
            }

            var isDelCompany = window.confirm('Do you want to delete Company Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }


        function SaveBenefitsVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            if (document.getElementById('<%=drpCatType.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                document.getElementById('<%=drpCatType.ClientID%>').focus;
                return false;
            }



            return true
        }

        function DeleteBenefitsVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            if (document.getElementById('<%=drpCatType.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select Category";
                document.getElementById('<%=drpCatType.ClientID%>').focus;
                return false;
            }

            var isDelBenefit = window.confirm('Do you want to delete Category Information?');

            if (isDelBenefit == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }


        function BindCompanyDtls(CompanyId, CompanyName) {
            document.getElementById("<%=txtCompCode.ClientID%>").value = CompanyId;
            document.getElementById("<%=txtCompName.ClientID%>").value = CompanyName;

        }


        function CompanyShow(PageName) {
            var Value;
            if (PageName == 'CompanyId') {
                Value = document.getElementById('<%=txtCompCode.ClientID%>').value
            }
            else if (PageName == 'BillToCode') {
                Value = document.getElementById('<%=txtBillToCode.ClientID%>').value
            }
            else if (PageName == 'CompanyName') {
                Value = document.getElementById('<%=txtCompName.ClientID%>').value
            }

    var win = window.open("Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + Value, "newwin1", "top=200,left=100,height=500,width=950,toolbar=no,scrollbars=yes,menubar=no");
    win.focus();
    return true;

}

function CopyAgreementShow() {
    var CompanyId = document.getElementById('<%=txtCompCode.ClientID%>').value;
    var CompanyName = document.getElementById('<%=txtCompName.ClientID%>').value;

    var win = window.open("CompanyCopyAgreement.aspx?CompanyId=" + CompanyId + "&CompanyName=" + CompanyName, "newwin1", "top=200,left=200,height=350,width=600,toolbar=no,scrollbars=yes,menubar=no");
    win.focus();
    return true;
}

function ContCategoryShow() {
    var CompanyId = document.getElementById('<%=txtCompCode.ClientID%>').value;
    var CompanyName = document.getElementById('<%=txtCompName.ClientID%>').value;

    var win = window.open("AgreementCategory.aspx?CompanyId=" + CompanyId + "&CompanyName=" + CompanyName, "newwin1", "top=200,left=200,height=600,width=900,toolbar=no,scrollbars=yes,menubar=no");
    win.focus();
    return true;
}


function ContServiceShow() {
    var CompanyId = document.getElementById('<%=txtCompCode.ClientID%>').value;
    var CompanyName = document.getElementById('<%=txtCompName.ClientID%>').value;

    var win = window.open("AgreementService.aspx?CompanyId=" + CompanyId + "&CompanyName=" + CompanyName, "newwin1", "top=200,left=200,height=600,width=1000,toolbar=no,scrollbars=yes,menubar=no");
    win.focus();
    return true;
}

function ShowBalanceHistoryPDF(CompanyId) {

    var Report = "HmsCompBalHist.rpt";

    var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={HMS_COMPANY_BALANCE_HISTORY.HCB_COMP_ID}=\'' + CompanyId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
    win.focus();

}

function ShowCompDetailsPDF(Report, CompanyId) {

    var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={HMS_COMPANY_MASTER.HCM_COMP_ID}=\'' + CompanyId + '\' AND {VWInvoiceSummary.HIM_DATE}>=Date(01/01/1900) AND {VWInvoiceSummary.HIM_DATE}<=Date(01/01/1900)', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
    win.focus();
    return true;
}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="padding-left: 60%; width: 100%;">


        <div id="divMessage" style="display: none; border: groove; height: auto; width: 600px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">

            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div style="padding-left: 5px;">
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">

                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>
                        <br />
                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>
                    </div>
                    <span style="float: right; margin-right: 20px">

                        <input type="button" id="Button5" class="ButtonStyle gray" style="font-weight: bold; cursor: pointer; width: 50px;" value=" OK " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>


    <table style="padding-left: 0px; width: 100%;" cellspacing="0" cellpadding="0">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div style="padding: 5px; width: 80%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">

        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
            <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Company Details-1 " Width="100%">
                <ContentTemplate>
                    <input type="hidden" id="hidPermission" runat="server" value="9" />
                    <asp:HiddenField ID="hidAccDiscount" runat="server" />
                    <asp:HiddenField ID="hidCmpOPBalPaid" runat="server" />
                    <asp:HiddenField ID="hidActualCompanyId" runat="server" />
                    <asp:HiddenField ID="hidNextCompanyId" runat="server" />
                    <asp:HiddenField ID="hidCopyCompanyId" runat="server" />
                    <asp:HiddenField ID="hidCopyCompanyName" runat="server" />

                    <table width="100%" cellpadding="5" cellspacing="5">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="100px" OnClick="btnClear_Click" Text="Clear" />

                                <asp:Button ID="btnDelete" runat="server" CssClass="button gray small" Width="100px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return DeleteCompanyVal();" />

                                <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />

                            </td>
                        </tr>
                    </table>

                    <table width="100%" border="0">
                        <tr>
                            <td width="60px" style="padding-left: 5px; vertical-align: top;">
                                <fieldset>
                                    <legend class="label">General Details
                                    </legend>
                                    <table width="100%">
                                        <tr>
                                            <td class="label" style="height: 30px;"></td>
                                            <td>
                                                <asp:CheckBox ID="chkManual" runat="server" CssClass="label" Text="Manual" Checked="false" AutoPostBack="True" OnCheckedChanged="chkManual_CheckedChanged" />
                                            </td>
                                            <td class="label"></td>
                                            <td>
                                                <asp:CheckBox ID="chkStatus" runat="server" CssClass="label" Text="Active" Checked="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Code <span style="color: red; font-size: 11px;">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCompCode" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="10" ondblclick="CompanyShow('CompanyId');" AutoPostBack="true" OnTextChanged="txtCompCode_TextChanged"></asp:TextBox>
                                            </td>
                                            <td class="label">Comp.Type   <span style="color: red; font-size: 11px;">*</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpCompanyType" runat="server" CssClass="TextBoxStyle" Width="157px">
                                                    <asp:ListItem Value="I">Insurance</asp:ListItem>
                                                    <asp:ListItem Value="N">Non Insurance</asp:ListItem>
                                                    <asp:ListItem Value="P">Personal</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Bill To Code   <span style="color: red; font-size: 11px;">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBillToCode" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="10" ondblclick="CompanyShow('BillToCode');"></asp:TextBox>

                                            </td>
                                            <td class="label">Name  <span style="color: red; font-size: 11px;">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCompName" runat="server" CssClass="TextBoxStyle" Width="150px" MaxLength="50" ondblclick="CompanyShow('CompanyName');"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Pay. Type  <span style="color: red; font-size: 11px;">*</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpPayType" runat="server" CssClass="TextBoxStyle" Width="157px">
                                                    <asp:ListItem Value="C">Cash</asp:ListItem>
                                                    <asp:ListItem Value="R">Credit</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="label">Abbreviation  <span style="color: red; font-size: 11px;">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAbbrev" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="10"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 60px;">Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress" CssClass="TextBoxStyle" runat="server" Width="157px" Height="50px" MaxLength="50" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td class="label" style="vertical-align: top">PO Box
                                            </td>
                                            <td style="vertical-align: top">
                                                <asp:TextBox ID="txtPOBox" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="10"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">City
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpCity" runat="server" CssClass="TextBoxStyle" Width="157px">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="label">Country
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpCountry" runat="server" CssClass="TextBoxStyle" Width="157px">
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Phone-1
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone1" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="label">Phone-2
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone2" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="20"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Fax
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFax" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                            </td>
                                            <td class="label">Email
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="50"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Contact Per.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtContactPer" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td class="label">Contact No.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtContactNo" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Web
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtWeb" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="30"></asp:TextBox>
                                            </td>
                                            <td class="label" style="height: 30px;">Claim Form Name
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtClaimFrmName" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="25"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Received ID
                                            </td>
                                            <td class="label">
                                                <asp:TextBox ID="txtHaadCode" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
                                                <asp:Label ID="lblMOH" runat="server" CssClass="label" Text="MOH" Visible="false"></asp:Label>
                                                <asp:TextBox ID="txtMOH" CssClass="TextBoxStyle" runat="server" Width="55px" MaxLength="10" Visible="false"></asp:TextBox>
                                            </td>

                                            <td class="label" style="height: 30px;">Payer Id  
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPayerId" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="100"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px; width: 150px">Package  
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPackage" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="25"></asp:TextBox>

                                            </td>
                                            <td class="label" style="height: 30px;">Ref. Code 
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRefCode" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="25"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px; width: 150px">TRN No.  
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTrnNo" CssClass="TextBoxStyle" runat="server" Width="150px" MaxLength="25"></asp:TextBox>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="label" style="height: 30px; vertical-align: top;">Reminder for Receptionist
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtReminder" CssClass="TextBoxStyle" runat="server" Width="100%" Height="30px" Style="resize: none;" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="lblCaption1" style="width: 100px;">Company A/C
                                            </td>
                                            <td colspan="3">

                                                <asp:TextBox ID="txtAccountName" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20px"></asp:TextBox>
                                                <div id="divwidth" style="visibility: hidden;"></div>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtAccountName" MinimumPrefixLength="1" ServiceMethod="GetAccountList"
                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                                </asp:AutoCompleteExtender>

                                            </td>

                                        </tr>
                                    </table>
                                </fieldset>


                            </td>
                            <td width="40px" align="left" valign="top" style="padding-left: 5px;">
                                <fieldset>
                                    <legend class="label">Company Balance Details
                                    </legend>
                                    <table width="100%">
                                        <tr>
                                            <td class="label" style="height: 30px;">Account Id
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAccountId" CssClass="TextBoxStyle" runat="server" Width="100px" MaxLength="10"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Op.Bal.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOpBal" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtOpBal_TextChanged"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Op.Bal. Date
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOpBalDate" CssClass="TextBoxStyle" runat="server" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtOpBalDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtOpBalDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Credit Limit
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCreditLimit" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Invoice Amt
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInvoiceAmt" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" ReadOnly="true" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Received Amt
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtReceivedAmt" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" ReadOnly="true" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Reject Amt
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRejectAmt" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" ReadOnly="true" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label">Balance
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBalance" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" ReadOnly="true" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="height: 27px;"></td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <fieldset style="height: 70px;">
                                    <table width="100%" style="vertical-align: top;">
                                        <tr>
                                            <td class="label">Pre. App. Amt
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPreAppAmt" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" style="height: 30px;">Revisit Days
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRevisitDays" CssClass="TextBoxStyle" runat="server" Width="100px" Style="text-align: right;" MaxLength="3" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" colspan="2" style="height: 30px;">
                                                <asp:CheckBox ID="chkMinistryService" runat="server" CssClass="TextBoxStyle" Text="Service From Ministry of Health" Checked="false" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 50px;"></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnCopy" runat="server" CssClass="button gray small" Width="100px" OnClick="btnCopy_Click" OnClientClick="return CopyVal();" Text="Copy" />
                                <asp:Button ID="btnBalanceHistory" runat="server" CssClass="button gray small" Width="150px" OnClick="btnBalanceHistory_Click" Text="Balance History" />
                                <asp:Button ID="btnContCategory" runat="server" CssClass="button gray small" Width="170px" OnClientClick="return ContCategoryShow();" Text="Contract - Category wise" />
                                <asp:Button ID="btnContService" runat="server" CssClass="button gray small" Width="170px" OnClientClick="return ContServiceShow();" Text="Contract - Service wise" />
                                <asp:Button ID="btnCopyAgreement" runat="server" CssClass="button gray small" Width="150px" OnClientClick="return CopyAgreementShow();" Text="Copy Agreement" />

                            </td>
                        </tr>

                    </table>
                    <br />


                </ContentTemplate>
            </asp:TabPanel>
            <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Company Details-2 " Width="96%">
                <ContentTemplate>

                    <fieldset style="width: 99%; display: none;">
                        <legend class="label">Contract Details
                        </legend>
                        <table width="99%" style="display: none;">

                            <tr>
                                <td class="label" style="height: 30px;">Ref.No
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContractRefNo" CssClass="TextBoxStyle" runat="server" Width="100px" MaxLength="20"></asp:TextBox>

                                </td>

                                <td class="label">Signed Date
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSignedDate" CssClass="TextBoxStyle" runat="server" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                        Enabled="True" TargetControlID="txtSignedDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtSignedDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                                </td>
                            </tr>
                            <tr>
                                <td class="label" style="height: 30px;">Start Date
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContStDate" runat="server" Width="100px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                        Enabled="True" TargetControlID="txtContStDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtContStDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                                </td>
                                <td class="label" style="height: 30px;">End Date
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtContEnDate" runat="server" Width="140px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                        Enabled="True" TargetControlID="txtContEnDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtContEnDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                                </td>
                            </tr>
                        </table>
                    </fieldset>

                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" CssClass="label" Text="Available Category"></asp:Label>
                            </td>
                            <td style="width: 200px;"></td>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="label" Text="Selected Category"></asp:Label>
                            </td>
                            <td style="width: 200px;"></td>
                            <td>
                                <asp:Label ID="Label2" runat="server" CssClass="label" Text="Package Name"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lstInsCategory" runat="server" CssClass="TextBoxStyle" Height="170px" Width="250px"></asp:ListBox>
                            </td>
                            <td style="text-align: center;">
                                <asp:Button ID="btnCatAdd" runat="server" CssClass="button gray small" Width="20px" Font-Bold="true" OnClick="btnCatAdd_Click" Text=">>" /><br />
                                <br />
                                <asp:Button ID="btnCatRemove" runat="server" CssClass="button gray small" Width="20px" Font-Bold="true" OnClick="btnCatRemove_Click" Text="<<" />
                            </td>
                            <td>
                                <asp:ListBox ID="lstSelectedCat" runat="server" CssClass="TextBoxStyle" Height="170px" Width="250px"></asp:ListBox>
                            </td>
                            <td style="width: 200px;"></td>
                            <td>
                                <asp:ListBox ID="lstPackage" runat="server" CssClass="TextBoxStyle" Height="170px" Width="250px" SelectionMode="Multiple"></asp:ListBox>
                            </td>
                        </tr>


                    </table>

                    <table width="100%" border="0" style="display: none;">
                        <tr>

                            <td valign="top">
                                <fieldset style="width: 350px;">
                                    <legend class="label">Reports
                                    </legend>
                                    <table width="100%">
                                        <tr>
                                            <td style="height: 30px;" class="label">Report Name
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpReportName" runat="server" CssClass="TextBoxStyle" Width="207px" AutoPostBack="true" OnSelectedIndexChanged="drpReportName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;" class="label">Details
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtReportName" CssClass="TextBoxStyle" runat="server" Width="200px" ReadOnly="true"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;" class="label">Summary
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtReportSummary" CssClass="TextBoxStyle" runat="server" Width="200px" ReadOnly="true"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="label">Preview
                                  _____________________________________
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 48px;"></td>
                                            <td>
                                                <asp:Button ID="btnDetailrpt" runat="server" Text="Detail" CssClass="button gray small" Width="100px" OnClick="btnDetailrpt_Click" />
                                                <asp:Button ID="btnSummary" runat="server" Text="Summary" CssClass="button gray small" Width="100px" OnClick="btnSummary_Click" />
                                            </td>
                                        </tr>
                                    </table>

                                </fieldset>
                            </td>
                        </tr>
                    </table>


                    <table width="100%">
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkUpdateCodeType" runat="server" CssClass="label" Text="Update Code Type to Service Master when saving Invoice" Checked="false" />
                                &nbsp;&nbsp;&nbsp;
                          <asp:CheckBox ID="chkValidateEClaim" runat="server" CssClass="label" Text="Validate for E-Claim" Checked="false" />

                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="label">Type
                            </td>
                            <td>
                                <asp:DropDownList ID="drpContractType" runat="server" CssClass="TextBoxStyle" Width="150px" Enabled="true">
                                    <asp:ListItem Value="" Text="---Select ---" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="S">Service Wise</asp:ListItem>
                                    <asp:ListItem Value="C">Category Wise</asp:ListItem>
                                    <asp:ListItem Value="F">Factor Wise</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td class="lblCaption1">From Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFactorFromDate" runat="server" Width="100px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                                    Enabled="True" TargetControlID="txtFactorFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtFactorFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                            </td>

                            <td class="lblCaption1">To Date:
                            </td>
                            <td>


                                     <asp:TextBox ID="txtFactorToDate" runat="server" Width="100px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender5" runat="server"
                                    Enabled="True" TargetControlID="txtFactorToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtFactorToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                            </td>
                        </tr>
                    </table>
                    <table width="100%" style="width: 100%;">
                        <tr>
                            <td class="lblCaption1" style="width: 50%;">
                                <div style="padding-top: 0px; width: 95%; height: 250px; overflow: auto; border: 1px solid #005c7b; padding: 0px; border-radius: 10px;">
                                    
                                   <asp:GridView ID="gvPriceFactorMaster" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="10" GridLines="Horizontal"  >
                                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                                <RowStyle CssClass="GridRow" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Show Service Dtls" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                                                OnClick="SelectFactor_Click" />
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ID" HeaderStyle-Width="30px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFactorID" CssClass="GridRow" Style="text-align: center;" runat="server" Text='<%# Bind("HCPF_FACTOR_ID") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="FROM DATE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFromDate" CssClass="GridRow" runat="server" Text='<%# Bind("HCPF_FROM_DATEDesc") %>'  ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="TO DATE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblToDate" CssClass="GridRow" runat="server" Text='<%# Bind("HCPF_TO_DATEDesc") %>'   ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkFactorCopy" runat="server" CssClass="lblCaption1" OnClick="CopyFactor_Click" Text="Copy"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                     

                                </div>

                            </td>

                            <td class="lblCaption1" style="width: 50%;">
                                <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto; border: 1px solid #005c7b; padding: 0px; border-radius: 10px;">
                                   
                                            <asp:GridView ID="gvPriceFactorDtls" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="10" GridLines="Horizontal" >
                                                <HeaderStyle CssClass="GridHeader_Blue"   />
                                                <RowStyle CssClass="GridRow" />
                                                <Columns>

                                                    <asp:TemplateField HeaderText="CATEGORY">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCatID" CssClass="GridRow" runat="server" Text='<%# Bind("HSCM_CAT_ID") %>'  ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="FACTOR" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtFactor" CssClass="label" runat="server" Text="1"   Width="100px" Style="text-align: center;" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
 

                                </div>
                            </td>
                        </tr>
                    </table>

                    <br />
                    <table width="900px" cellpadding="5" cellspacing="5">
                        <tr>
                            <td>
                                <asp:Button ID="btnSave1" runat="server" CssClass="button red small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
                                <asp:Button ID="btnDelete1" runat="server" CssClass="button gray small" Width="100px" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return DeleteCompanyVal();" />
                                <asp:Button ID="btnClear1" runat="server" CssClass="button gray small" Width="100px" OnClick="btnClear_Click" Text="Clear" />

                            </td>
                        </tr>
                    </table>


                </ContentTemplate>


            </asp:TabPanel>
            <asp:TabPanel runat="server" ID="TabPanel2" HeaderText=" Benefits & Execlusions " Width="100%">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td class="label" style="height: 30px;">Type Of Category
                            </td>
                            <td>
                                <asp:DropDownList ID="drpCatType" runat="server" CssClass="TextBoxStyle" Width="157px" BorderWidth="1px" AutoPostBack="true" OnSelectedIndexChanged="drpCatType_SelectedIndexChanged" BorderColor="#CCCCCC">
                                </asp:DropDownList>
                            </td>
                            <td class="label">Treatment Type
                            </td>
                            <td>
                                <asp:DropDownList ID="drpTreatmentType" runat="server" CssClass="TextBoxStyle" Width="157px">
                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                                </asp:DropDownList>
                            </td>

                            <td class="label">Benefits Type
                            </td>
                            <td>
                                <asp:DropDownList ID="drpBenefitsType" runat="server" CssClass="TextBoxStyle" Width="157px" AutoPostBack="true" OnSelectedIndexChanged="drpBenefitsType_SelectedIndexChanged">
                                    <asp:ListItem Value="I">Insurance Type</asp:ListItem>
                                    <asp:ListItem Value="S">Service Type</asp:ListItem>
                                </asp:DropDownList>
                            </td>


                        </tr>

                        <tr>
                            <td class="label" style="height: 30px;">Hospital Discount
                            </td>
                            <td class="label">
                                <asp:DropDownList ID="drpHospDiscType" CssClass="TextBoxStyle" runat="server" Width="75px">
                                    <asp:ListItem Text="$" Value="$"></asp:ListItem>
                                    <asp:ListItem Text="%" Value="%"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtHospDiscAmt" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                max.Claim
                            </td>
                            <td colspan="4">
                                <asp:CheckBox ID="ChkConsultation" runat="server" CssClass="label" Text="Consultation Included in Hospital Discount" Checked="false" />
                                <asp:CheckBox ID="ChkDedMaxConsAmt" runat="server" CssClass="label" Text="Deductible to the max of Consultation Amt" Checked="false" />

                            </td>
                        </tr>

                        <tr>
                            <td class="label" style="height: 30px;">Deductible
                            </td>
                            <td>
                                <asp:DropDownList ID="drpDedType" runat="server" CssClass="TextBoxStyle" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                                    <asp:ListItem Text="$" Value="$"></asp:ListItem>
                                    <asp:ListItem Text="%" Value="%"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtDeductible" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtDeductible_TextChanged"></asp:TextBox>
                                <asp:TextBox ID="TxtMaxDed" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            </td>
                            <td colspan="4">
                                <asp:CheckBox ID="ChkDedCoIns" runat="server" CssClass="label" Text="Deductible & Co Insurance applicable for Consultation" Checked="false" />

                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 30px;">Co Insurance
                            </td>
                            <td>
                                <asp:DropDownList ID="drpCoInsType" runat="server" CssClass="TextBoxStyle" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                                    <asp:ListItem Text="$" Value="$"></asp:ListItem>
                                    <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtCoIns" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtCoIns_TextChanged"></asp:TextBox>
                                <asp:TextBox ID="TxtMaxCoIns" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </td>
                            <td colspan="4">
                                <asp:CheckBox ID="ChkCoInsFromGrossAmt" runat="server" CssClass="label" Text="Co-Insurance From Gross Amount" Checked="false" />
                                <asp:CheckBox ID="ChkSplCodeNeeded" runat="server" CssClass="label" Text="Special Code Needed" Checked="false" />

                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 30px;">Pharmacy Disc.
                            </td>
                            <td>
                                <asp:DropDownList ID="drpPharType" runat="server" CssClass="TextBoxStyle" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                                    <asp:ListItem Text="$" Value="$"></asp:ListItem>
                                    <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtPharDisc" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </td>
                            <td colspan="4">
                                <asp:CheckBox ID="ChkCoInsFromGrossandClaimfromNetAmt" runat="server" CssClass="label" Text="Co-Insurance From Gross Amt and  Claim Amt from  Net Amt" Checked="false" />

                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 30px;">Pharmacy Co.Ins
                            </td>
                            <td>
                                <asp:DropDownList ID="drpPharCoInsType" runat="server" CssClass="TextBoxStyle" Width="75px" BorderWidth="1" BorderColor="#cccccc">
                                    <asp:ListItem Text="$" Value="$"></asp:ListItem>
                                    <asp:ListItem Text="%" Value="%" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtPharCoInsDisc" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:TextBox ID="TxtMaxPhy" runat="server" Style="text-align: right;" Width="50px" CssClass="TextBoxStyle" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </td>
                            <td colspan="4">
                                <asp:CheckBox ID="ChkDedBenefitsType" runat="server" CssClass="label" Text="Calculate Deductible as Benefits Type" Checked="false" />

                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 30px;">Price Factor
                            </td>
                            <td>
                                <asp:TextBox ID="txtPriceFactor" CssClass="TextBoxStyle" runat="server" Width="130px" Style="text-align: right;" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </td>
                            <td class="label">Perday Limit of the Patient
                            </td>
                            <td>
                                <asp:TextBox ID="txtPerDayPTLimit" CssClass="TextBoxStyle" runat="server" Width="115px" Style="text-align: right;" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:Button ID="btnUpdate" runat="server" CssClass="button red small" Width="70px" OnClick="btnUpdate_Click" Text="Update" ToolTip="Update Perday Limit of the Patient" />

                            </td>
                        </tr>
                        <tr>
                            <td class="label">Remarks
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="txtRemarks" CssClass="TextBoxStyle" runat="server" Width="480px" MaxLength="50"></asp:TextBox>

                            </td>
                        </tr>

                    </table>



                    <asp:Button ID="btnBenefitSave" runat="server" CssClass="button red small" Width="100px" OnClick="btnBenefitSave_Click" OnClientClick="return SaveBenefitsVal();" Text="Save" />
                    <asp:Button ID="btnBenefitDelete" runat="server" CssClass="button gray small" Width="100px" OnClick="btnBenefitDelete_Click" OnClientClick="return DeleteBenefitsVal();" Text="Delete" />
                    <asp:Button ID="btnBenefitClear" runat="server" CssClass="button gray small" Width="100px" OnClick="btnBenefitClear_Click" Text="Clear" />
                    <br />

                    <div style="padding-top: 0px; padding-left: 0px; width: 900px; height: 350px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                        <asp:GridView ID="gvHCB" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowDataBound="gvHCB_RowDataBound"
                            EnableModelValidation="True" Width="99%">
                            <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>


                                <asp:TemplateField HeaderText="Category" HeaderStyle-Width="250px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCatId" runat="server" Text='<%# Bind("HCBD_CATEGORY_ID") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCBD_CATEGORY") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Category Type" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCatType" runat="server" Text='<%# Bind("HCBD_CATEGORY_TYPE") %>' Visible="false"></asp:Label>
                                        <asp:DropDownList ID="drpCategoryType" runat="server" CssClass="TextBoxStyle" Style="border: none;" Width="100%" BorderWidth="1px">
                                            <asp:ListItem Text="Covered" Value="C" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Un Covered" Value="U"></asp:ListItem>
                                            <asp:ListItem Text="PreApproval" Value="P"></asp:ListItem>

                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Limit" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtLimit" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HCBD_LIMIT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCoInsType" CssClass="GridRow" runat="server" Text='<%# Bind("HCBD_CO_INS_TYPE") %>' Visible="false"></asp:Label>
                                        <asp:DropDownList ID="drpGVCoInsType" CssClass="LabelStyle" runat="server" Width="100%" Height="30px">
                                            <asp:ListItem Value="$">$</asp:ListItem>
                                            <asp:ListItem Value="%">%</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCoInsAmt" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HCBD_CO_INS_AMT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Ded Type" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDedType" CssClass="GridRow" runat="server" Text='<%# Bind("HCBD_DED_TYPE") %>' Visible="false"></asp:Label>
                                        <asp:DropDownList ID="drpGVDedType" CssClass="LabelStyle" runat="server" Width="100%" Height="30px">
                                            <asp:ListItem Value="$">$</asp:ListItem>
                                            <asp:ListItem Value="%">%</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Deductible" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDedAmt" runat="server" MaxLength="10" Style="text-align: right; padding-right: 5px; border: none;" Width="98%" Height="30px" Text='<%#  decimal.Parse(  Convert.ToString(Eval("HCBD_DED_AMT"))).ToString("N2")%>' onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>


                    </div>

                </ContentTemplate>
            </asp:TabPanel>

        </asp:TabContainer>
    </div>


</asp:Content>
