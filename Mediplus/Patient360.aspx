﻿<%@ Page Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Patient360.aspx.cs" Inherits="Mediplus.Patient360" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/EMR/VisitDetails/LabResult.ascx" TagPrefix="UC1" TagName="LabResult" %>
<%@ Register Src="~/EMR/VisitDetails/RadiologyResult.ascx" TagPrefix="UC1" TagName="RadiologyResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage1, vMessage2) {

            document.getElementById("divMessage").style.display = 'block';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1;
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div style="padding-left: 60%; width: 100%;">


            <div id="divMessage" style="display: none; border: groove; height: 120px; width: 500px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 300px; top: 300px;">

                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                    <ContentTemplate>
                        <span style="float: right;">

                            <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;" value=" X " onclick="HideErrorMessage()" />

                        </span>
                        <br />
                        <div style="width: 100%; height: 20px; background: #E3E3E3;">
                            &nbsp;
                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: #02a0e0; font-weight: bold;"></asp:Label>
                        </div>
                        &nbsp;
                        <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <br />

            </div>
        </div>
        <table>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Date
                </td>
                <td>
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                </td>
                <td class="lblCaption1">To Date

                </td>
                <td>
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="TextBoxStyle" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                </td>
                <td class="lblCaption1">File No

                </td>
                <td>
                    <asp:TextBox ID="txtFileNo" runat="server" CssClass="TextBoxStyle" Width="70px"></asp:TextBox>

                </td>
                <td>
                    <asp:Button ID="btnRefresh" runat="server" CssClass="button red small" Width="80px"
                        Text="Refresh" OnClick="btnRefresh_Click" />
                    <asp:Button ID="btnTodayDate" runat="server" CssClass="button gray small" Width="80px"
                        Text="Today" OnClick="btnTodayDate_Click" />
                    <asp:Button ID="btnClear" runat="server" CssClass="button gray small" Width="80px"
                        Text="Clear" OnClick="btnClear_Click" />
                </td>
            </tr>

        </table>
        <table width="99%">
            <tr>
                <td style="width: 210px;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Visit Dtls." disabled="disabled" />
                </td>
                <td class="lblCaption1">Max. Row Display
                             <asp:TextBox ID="txtVisitMaxRow" runat="server" CssClass="TextBoxStyle" Width="50px" Text="500" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                </td>
                <td style="float: right;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="Label9" runat="server" CssClass="label"
                                Text="Total :"></asp:Label>
                            &nbsp;
                                            <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label7" runat="server" CssClass="label"
                                        Text="New :"></asp:Label>
                            &nbsp;
                                            <asp:Label ID="lblNew" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label8" runat="server" CssClass="label"
                                        Text="Followup(Revisit) :"></asp:Label>
                            &nbsp;
                                            <asp:Label ID="lblRevisit" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label10" runat="server" CssClass="label"
                                        Text="Established(Old) :"></asp:Label>
                            &nbsp;<asp:Label ID="lblOld" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp;&nbsp;&nbsp;
                           <asp:ImageButton ID="btnPTVisitRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnPTVisitRef_Click" />




                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
        <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvPTVisit" runat="server"
                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvPTVisit_Sorting"   BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0">
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                        
                        <Columns>
                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" ToolTip="Select" ImageUrl="~/Images/Select.png" Height="15" Width="15"
                                        OnClick="Edit_Click" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id" Visible="false">
                                <ItemTemplate>

                                    <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_EMR_ID") %>'></asp:Label>
                                    <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HPV_SEQNO") %>'></asp:Label>
                                    <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_Id") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Name" SortExpression="HPV_PT_NAME" Visible="false">
                                <ItemTemplate>

                                    <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="HPV_DATE">
                                <ItemTemplate>

                                    <asp:Label ID="lblVisitDate" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateDesc") %>'></asp:Label>
                                    <asp:Label ID="lblVisitDateTime" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DateTimeDesc") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Doctor" SortExpression="HPV_DR_NAME">
                                <ItemTemplate>

                                    <asp:Label ID="Label4" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PT Type" SortExpression="HPV_PT_TYPEDesc">
                                <ItemTemplate>

                                    <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_PT_TYPEDesc") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company" SortExpression="HPV_COMP_NAME">
                                <ItemTemplate>

                                    <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("HPV_COMP_NAME") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <table border="0" style="width: 99.5%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 210px;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Invoice Dtls." disabled="disabled" />
                </td>
                <td class="lblCaption1">Max. Row Display
                             <asp:TextBox ID="txtInvoiceMaxRow" runat="server" CssClass="TextBoxStyle" Width="50px" Text="500" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                </td>
                <td style="float: right;">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>

                            <asp:Label ID="Label3" runat="server" CssClass="label"
                                Text="Gross :"></asp:Label>

                            <asp:Label ID="lblInvGross" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp;
                                    <asp:Label ID="Label6" runat="server" CssClass="label"
                                        Text="Net :"></asp:Label>

                            <asp:Label ID="lblInvNet" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                            &nbsp; &nbsp;
                                    <asp:Label ID="Label12" runat="server" CssClass="label"
                                        Text="PT.Share :"></asp:Label>

                            <asp:Label ID="lblInvPTShare" runat="server" CssClass="label" Font-Bold="true"></asp:Label>

                            &nbsp; &nbsp;
                                     <asp:ImageButton ID="btnInvoiceRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnInvoiceRef_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
        </table>
         <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">

            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvInvoice" runat="server" AutoGenerateColumns="False"  BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0">
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                       
                        <Columns>
                            <asp:TemplateField HeaderText="Inv. No" HeaderStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:Label ID="lblInvoiceId" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_ID") %>' Width="70px" Visible="false"></asp:Label>
                                    <asp:Label ID="lblInvType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_TYPE") %>' Width="70px" Visible="false"></asp:Label>

                                    <asp:Label ID="lnkInvoiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_INVOICE_ID") %>' Enabled="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>
                                    <asp:Label ID="lnkDate" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_DATEDESC") %>' Enabled="false"> </asp:Label>
                                    <asp:Label ID="LinkButton2" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_DATETimeDesc") %>' Enabled="false"> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="File No">
                                <ItemTemplate>
                                    <asp:Label ID="LinkButton5" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>

                                    <asp:Label ID="LinkButton4" runat="server" OnClick="Select_Click" Text='<%# Bind("HIM_PT_NAME") %>' Enabled="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="Type" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblgvInvType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_INVOICE_TYPE") %>' Width="70px"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Gorss Amt" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkgvInvGrossAmt" runat="server" OnClick="Select_Click" Enabled="false">
                                        <asp:Label ID="lblgvInvGrossAmt" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_GROSS_TOTAL") %>' Width="70px"></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Net Amt" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblgvInvNetAmt" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_CLAIM_AMOUNT") %>' Width="70px"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PT. Share" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblgvInvPTShare" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PT_AMOUNT") %>' Width="70px"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Pay Type" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                                <ItemTemplate>

                                    <asp:Label ID="lblgvPaymentType" CssClass="GridRow" runat="server" Text='<%# Bind("HIM_PAYMENT_TYPE") %>' Width="70px"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>


        </div>
        <table border="0" style="width: 99.5%;" cellpadding="0" cellspacing="0">

            <tr>
                <td style="width: 10%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Appointment Dtls." disabled="disabled" />
                </td>
                <td style="width: 90%; float: right;">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:ImageButton ID="btnAppointmentRef" runat="server" ImageUrl="~/Images/Refresh.png" Style="height: 18px; width: 18px;" ToolTip="Refresh" OnClick="btnAppointmentRef_Click" />

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

        <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvAppointment" runat="server" AutoGenerateColumns="False"  BorderStyle="None" GridLines="Horizontal"
                        EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0">
                        <HeaderStyle CssClass="GridHeader_Gray" />
                        <RowStyle CssClass="GridRow" />
                       
                        <Columns>

                            <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton5" runat="server" Text='<%# Bind("HAM_FILENUMBER") %>' Visible="false"></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" Text='<%# Bind("HAM_PT_NAME") %>' Enabled="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDate" runat="server" Text='<%# Bind("AppintmentDate") %>' Enabled="false"> </asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="LinkButton8" runat="server" Text='<%# Bind("AppintmentSTTime") %>' Enabled="false"></asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Bind("AppintmentFITime") %>' Enabled="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Doctor" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDate" runat="server" Text='<%# Bind("HAM_DR_NAME") %>' Enabled="false"> </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <table border="0" style="width: 99.5%;" cellpadding="0" cellspacing="0">

            <tr>
                <td style="width: 50%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="EMR Dtls." disabled="disabled" /><br />
                    <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
                        <asp:GridView ID="gvEMRDtls" runat="server" AutoGenerateColumns="False"  BorderStyle="None" GridLines="Horizontal"
                            EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                           
                            <Columns>

                                <asp:TemplateField HeaderText="Name" Visible="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton5" runat="server" OnClick="Select_Click" Text='<%# Bind("EPM_PT_ID") %>' Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click" Text='<%# Bind("EPM_PT_NAME") %>' Enabled="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDate" runat="server" OnClick="Select_Click" Text='<%# Bind("EPM_DATEDesc") %>' Enabled="false"> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Doctor">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" OnClick="Select_Click" Text='<%# Bind("EPM_DR_NAME") %>' Enabled="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblEpmStatus" runat="server" OnClick="Select_Click" Text='<%# Bind("EPM_STATUSDesc") %>' Enabled="false"> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ins. Company">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblEpmInsName" runat="server" OnClick="Select_Click" Text='<%# Bind("EPM_INS_NAME") %>' Enabled="false"> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>



                            </Columns>

                        </asp:GridView>
                    </div>
                </td>
                <td style="width: 50%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Nursing Order Dtls." disabled="disabled" /><br />
                   <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
                        <asp:GridView ID="gvNursingOrder" runat="server" AllowSorting="True" AutoGenerateColumns="False"  BorderStyle="None" GridLines="Horizontal"
                            EnableModelValidation="True" Width="99%" CellPadding="2" CellSpacing="0">
                            <HeaderStyle CssClass="GridHeader" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />

                            <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="100px">
                                    <ItemTemplate>

                                        <asp:Label ID="lblgvHistDate" CssClass="label" runat="server" Text='<%# Bind("EPC_DATEDesc") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>


                                        <asp:Label ID="lblgvHistInsttruction" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>

                                        <asp:Label ID="lblgvHistComment" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name" HeaderStyle-Width="100px">
                                    <ItemTemplate>

                                        <asp:Label ID="lblgvHistUser" CssClass="label" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time" HeaderStyle-Width="150px">
                                    <ItemTemplate>

                                        <asp:Label ID="lblgvHistTime" CssClass="label" runat="server" Text='<%# Bind("EPC_TIME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>




                            </Columns>

                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Diagnosis" disabled="disabled" /><br />
                     <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
                        <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False"  BorderStyle="None" GridLines="Horizontal"
                            EnableModelValidation="True" Width="100%" CellPadding="2" CellSpacing="0">
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />

                            <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="110px">
                                    <ItemTemplate>

                                        <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("EPM_DATEDesc") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Doctor" >
                                    <ItemTemplate>

                                        <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("EPM_DR_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDiagCode" CssClass="label" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDiagName" CssClass="label" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>

                                        <asp:Label ID="lblDiagType" CssClass="label" runat="server" Text='<%# Bind("EPD_TYPE") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Pharmacy" disabled="disabled" /><br />
                  <div style="padding-top: 0px; width: 98%; height: 200px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
                        <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"  BorderStyle="None" GridLines="Horizontal"
                            EnableModelValidation="True" Width="100%"  >
                            <HeaderStyle CssClass="GridHeader_Gray" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />

                            <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="110px">
                                    <ItemTemplate>

                                        <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("EPM_DATEDesc") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Doctor" >
                                    <ItemTemplate>

                                        <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("EPM_DR_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                    <ItemTemplate>

                                        <asp:Label ID="Label6" CssClass="label" runat="server" Text='<%# Bind("EPP_PHY_CODE") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>

                                        <asp:Label ID="Label9" CssClass="label" runat="server" Text='<%# Bind("EPP_PHY_NAME") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit">
                                    <ItemTemplate>

                                        <asp:Label ID="Label10" CssClass="label" runat="server" Text='<%# Bind("EPP_UNIT") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Freq.">
                                    <ItemTemplate>

                                        <asp:Label ID="Label11" CssClass="label" runat="server" Text='<%# Bind("EPP_FREQUENCY") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Route">
                                    <ItemTemplate>

                                        <asp:Label ID="Label12" CssClass="label" runat="server" Text='<%# Bind("EPP_ROUTE") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration">
                                    <ItemTemplate>

                                        <asp:Label ID="Label13" CssClass="label" runat="server" Text='<%# Bind("EPP_DURATION") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Quantity">
                                    <ItemTemplate>

                                        <asp:Label ID="Label14" CssClass="label" runat="server" Text='<%# Bind("EPP_QTY") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refil">
                                    <ItemTemplate>

                                        <asp:Label ID="Label15" CssClass="label" runat="server" Text='<%# Bind("EPP_REFILL") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

            </tr>
            <tr id="trRadLab" runat="server">

                <td style="width: 50%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Laboratory Result" disabled="disabled" /><br />
                    <div style="padding: 0px; width: 99%; height: 150px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <UC1:LabResult ID="LabResult" runat="server" />
                    </div>
                </td>

                <td style="width: 50%;">
                    <input type="button" class="PageSubHeaderRed" style="width: 150px;" value="Radiology Result" disabled="disabled" /><br />
                    <div style="padding: 0px; width: 99%; height: 150px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <UC1:RadiologyResult ID="RadiologyResult" runat="server" />
                    </div>
                </td>
            </tr>
        </table>

    </div>
    <br />
    <br />
    <br />

</asp:Content>
