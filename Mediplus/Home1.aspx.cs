﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.Data;
using System.IO;
using System.Web.UI.DataVisualization.Charting;

namespace Mediplus
{
    public partial class Home1 : System.Web.UI.Page
    {


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                //string strFileName = Server.MapPath("MediplusLog.txt");

                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }

            drpDoctor.Items.Insert(0, "--- All ---");
            drpDoctor.Items[0].Value = "0";


        }

        void BindNurses()
        {
            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Nurse'";


            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_doctor_detailss(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpNurse.DataSource = DS;
                drpNurse.DataTextField = "FullName";
                drpNurse.DataValueField = "HSFM_STAFF_ID";
                drpNurse.DataBind();
            }
            drpNurse.Items.Insert(0, "--- All ---");
            drpNurse.Items[0].Value = "";
        }

        void BindAppointmentStatus()
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            string Criteria = "1=1";



            DS = dbo.AppointmentStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpAppointStatus.DataSource = DS;
                drpAppointStatus.DataTextField = "HAS_STATUS";
                drpAppointStatus.DataValueField = "HAS_ID";
                drpAppointStatus.DataBind();


                //for (int intCount = 0; intCount < drpAppointStatus.Items.Count; intCount++)
                //{
                //    if (drpAppointStatus.Items[intCount].Text == "Completed")
                //    {
                //        drpAppointStatus.SelectedValue = DS.Tables[0].Rows[intCount]["HAS_ID"].ToString();
                //    }

                //}

            }
            drpAppointStatus.Items.Insert(0, "--- All ---");
            drpAppointStatus.Items[0].Value = "";

        }

        void BindVisitDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND    HPV_DATE  >= '" + strForStartDate + " 00:00:00'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                //  Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
                Criteria += " AND    HPV_DATE  <= '" + strForToDate + " 23:59:59'";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            }

            if (drpNurse.SelectedIndex != 0)
            {
                Criteria += " AND HPV_NURSE_ID='" + drpNurse.SelectedValue + "'";
            }


            //if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            //{
            //    Criteria += " AND HPV_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
            //}



            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= convert(date,getdate(),101)";


            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            //// DS = dbo.PatientVisitGet(Criteria);
            CommonBAL objCom = new CommonBAL();

            string strTopValue = "100";
            if (txtVisitMaxRow.Text.Trim() != "")
            {
                strTopValue = txtVisitMaxRow.Text.Trim();
            }


            DS = objCom.fnGetFieldValue(" TOP   " + strTopValue   +
                       " CONVERT(VARCHAR,HPV_POLICY_EXP,103) as HPV_POLICY_EXPDesc,convert(varchar,HPV_Date,103) as HPV_DateDesc,CONVERT(VARCHAR(5),HPV_DATE,108) as HPV_DateTimeDesc" +
                       ",HPV_EMR_ID,HPV_SEQNO,HPV_PT_ID,HPV_PT_NAME,HPV_DR_ID,HPV_DR_NAME,HPV_DR_TOKEN,'' AS HPM_TOKEN_NO,HPV_VISIT_TYPE  ,(SELECT CASE ISNULL(HPV_INVOICE_ID,'')  WHEN '' then 'Not Invoiced' ELSE  'Invoiced' END) AS InvoiceStatus" +
                       ",(SELECT CASE  isnull(HPV_EMR_STATUS,'W') when  'W' then 'Waiting'   WHEN 'P' THEN 'In Process' WHEN 'Y' THEN 'Completed'   END) AS HPV_EMR_STATUSDesc , HPV_COMP_ID ,HPV_COMP_NAME,HPV_PT_TYPE " +
                       ",(SELECT CASE HPV_PT_TYPE WHEN 'CR' THEN 'Credit' when 'CA' THEN 'Cash'  ELSE HPV_PT_TYPE END ) AS HPV_PT_TYPEDesc", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");


            // DS = objCom.PatientVisitTopGet(Criteria, txtVisitMaxRow.Text.Trim());
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END
            lblNew.Text = "0";
            lblRevisit.Text = "0";
            lblOld.Text = "0";
            lblTotal.Text = "0";
            gvPTVisit.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPTVisit.Visible = true;
                gvPTVisit.DataSource = DV;
                gvPTVisit.DataBind();


                DataRow[] TempRow;
                TempRow = DS.Tables[0].Select(" HPV_VISIT_TYPE='New'");

                lblNew.Text = TempRow.Length.ToString();

                DataRow[] TempRow1;
                TempRow1 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Revisit'");

                lblRevisit.Text = TempRow1.Length.ToString();

                DataRow[] TempRow2;
                TempRow2 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Old'");

                lblOld.Text = TempRow2.Length.ToString();


                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {

                gvPTVisit.DataBind();
            }


        }

        void BindInvoiceDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND    HIM_DATE  >= '" + strForStartDate + " 00:00:00'";

            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
                Criteria += " AND    HIM_DATE  <= '" + strForToDate + " 23:59:59'";
            }


            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HIM_DR_CODE='" + drpDoctor.SelectedValue + "'";
            }

            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= convert(date,getdate(),101)";

            if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    Criteria += " AND HIM_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
                }
            }

            if (drpNurse.SelectedIndex != 0)
            {
                Criteria += "  AND HIM_INVOICE_ID IN (SELECT HPV_INVOICE_ID FROM HMS_PATIENT_VISIT WHERE  HPV_NURSE_ID='" + drpNurse.SelectedValue + "' )";
            }

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            lblInvTotal.Text = "0";
            lblInvGross.Text = "0.00";
            lblInvNet.Text = "0.00";
            lblInvPTShare.Text = "0.00";


            decimal decGrossAmt = 0, decNetAmt = 0, decPTShare = 0, decTGrossAmt = 0, decTNetAmt = 0, decTPTShare = 0;

            //  DS = dbo.InvoiceMasterGet(Criteria);
            CommonBAL objCom = new CommonBAL();

            string strTopValue = "100";
            if (txtInvoiceMaxRow.Text.Trim() != "")
            {
                strTopValue = txtInvoiceMaxRow.Text.Trim();
            }

             DS = objCom.fnGetFieldValue(" TOP   " + strTopValue   +
                 "HIM_INVOICE_TYPE,HIM_INVOICE_ID,CONVERT(VARCHAR,HIM_DATE,103)AS HIM_DATEDESC,CONVERT(VARCHAR(5),HIM_DATE,108) as HIM_DATETimeDesc"+
                 ",HIM_PT_ID,HIM_PT_NAME,HIM_INVOICE_TYPE,CAST(HIM_GROSS_TOTAL AS DECIMAL(9,2)) AS HIM_GROSS_TOTAL,CAST(HIM_CLAIM_AMOUNT AS DECIMAL(9,2)) AS HIM_CLAIM_AMOUNT ,CAST(HIM_PT_AMOUNT AS DECIMAL(9,2)) AS HIM_PT_AMOUNT    ,HIM_PAYMENT_TYPE"
                 , "HMS_INVOICE_MASTER", Criteria, "HIM_INVOICE_ID DESC");
          //  DS = objCom.InvoiceMasterTopGet(Criteria, txtInvoiceMaxRow.Text.Trim());


            gvInvoice.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvoice.Visible = true;
                gvInvoice.DataSource = DS;
                gvInvoice.DataBind();


                for (int i = 0; i < gvInvoice.Rows.Count; i++)
                {

                    Label lblgvInvGrossAmt = (Label)gvInvoice.Rows[i].FindControl("lblgvInvGrossAmt");
                    Label lblgvInvNetAmt = (Label)gvInvoice.Rows[i].FindControl("lblgvInvNetAmt");
                    Label lblgvInvPTShare = (Label)gvInvoice.Rows[i].FindControl("lblgvInvPTShare");


                    if (lblgvInvGrossAmt.Text != "" && lblgvInvGrossAmt.Text != null)
                    {
                        decGrossAmt = Convert.ToDecimal(lblgvInvGrossAmt.Text);
                    }

                    if (lblgvInvNetAmt.Text != "" && lblgvInvNetAmt.Text != null)
                    {
                        decNetAmt = Convert.ToDecimal(lblgvInvNetAmt.Text);
                    }

                    if (lblgvInvPTShare.Text != "" && lblgvInvPTShare.Text != null)
                    {
                        decPTShare = Convert.ToDecimal(lblgvInvPTShare.Text);
                    }


                    decTGrossAmt += decGrossAmt;
                    decTNetAmt += decNetAmt;
                    decTPTShare += decPTShare;


                }



            }
            else
            {
                gvInvoice.DataBind();

            }
            lblInvTotal.Text = Convert.ToString(gvInvoice.Rows.Count);
            lblInvGross.Text = decTGrossAmt.ToString("N2");
            lblInvNet.Text = decTNetAmt.ToString("N2");
            lblInvPTShare.Text = decTPTShare.ToString("N2");


            BindInvoiceTotal();
        }

        void BindInvoiceTotal()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND    HIM_DATE  >= '" + strForStartDate + " 00:00:00'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
                Criteria += " AND    HIM_DATE  <= '" + strForToDate + " 23:59:59'";
            }


            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HIM_DR_CODE='" + drpDoctor.SelectedValue + "'";
            }

            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= convert(date,getdate(),101)";

            if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    Criteria += " AND HIM_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
                }
            }

            if (drpNurse.SelectedIndex != 0)
            {
                Criteria += "  AND HIM_INVOICE_ID IN (SELECT HPV_INVOICE_ID FROM HMS_PATIENT_VISIT WHERE  HPV_NURSE_ID='" + drpNurse.SelectedValue + "' )";
            }

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            lblInvGross.Text = "0.00";
            lblInvNet.Text = "0.00";
            lblInvPTShare.Text = "0.00";


            decimal decTGrossAmt = 0, decTNetAmt = 0, decTPTShare = 0;

            DS = dbo.InvoiceTotalGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {


                decTGrossAmt = 0;
                decTNetAmt = 0;
                decTPTShare = 0;


                if (DS.Tables[0].Rows[0].IsNull("GrossTotal") == false && Convert.ToString(DS.Tables[0].Rows[0]["GrossTotal"]) != "")
                {
                    decTGrossAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["GrossTotal"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("ClaimTotal") == false && Convert.ToString(DS.Tables[0].Rows[0]["ClaimTotal"]) != "")
                {
                    decTNetAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["ClaimTotal"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("PTShareTotal") == false && Convert.ToString(DS.Tables[0].Rows[0]["PTShareTotal"]) != "")
                {
                    decTPTShare = Convert.ToDecimal(DS.Tables[0].Rows[0]["PTShareTotal"]);
                }


            }

            lblInvGross.Text = decTGrossAmt.ToString("N2");
            lblInvNet.Text = decTNetAmt.ToString("N2");
            lblInvPTShare.Text = decTPTShare.ToString("N2");



        }

        void BindAppointmentDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                //  Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND    HAM_STARTTIME  >= '" + strForStartDate + " 00:00:00'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) <= '" + strForToDate + "'";
                Criteria += " AND    HAM_STARTTIME  <= '" + strForToDate + " 23:59:59'";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HAM_DR_CODE='" + drpDoctor.SelectedValue + "'";
            }


            if (drpAppointStatus.SelectedIndex != 0)
            {
                Criteria += " AND HAM_STATUS='" + drpAppointStatus.SelectedValue + "'";
            }


            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) >= convert(date,getdate(),101) ";
            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) <= convert(date,getdate(),101)";

            //if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            //{
            //    Criteria += " AND HAM_CREATEDUSER='" + Convert.ToString(Session["User_ID"]) + "'";
            //}



            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string strTopValue = "100";
            if (txtAppointMaxRow.Text.Trim() != "")
            {
                strTopValue = txtAppointMaxRow.Text.Trim();
            }

            DS = objCom.fnGetFieldValue(" TOP   " + strTopValue +
                " CONVERT(varchar, HAM_STARTTIME,103) AS AppintmentDate,convert(varchar(5), HAM_STARTTIME,108) AS AppintmentSTTime,convert(varchar(5), HAM_FINISHTIME,108) AS AppintmentFITime "+
                " ,HAM_FILENUMBER,HAM_PT_NAME,HAM_DR_NAME,HAS_STATUS,HAM_CREATEDUSER "
                 , "HMS_APPOINTMENT_OUTLOOKSTYLE LEFT JOIN HMS_APPOINTMENTSTATUS ON HAM_STATUS=HAS_ID", Criteria, "HAM_SYSTEM_ID DESC");


          //  DS = dbo.AppointmentOutlookTopGet(Criteria, txtAppointMaxRow.Text.Trim());

            lblAppointTotal.Text = "0";
            lblBooked.Text = "0";
            lblConfirmed.Text = "0";
            lblCompleted.Text = "0";


            gvAppointment.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAppointment.Visible = true;
                gvAppointment.DataSource = DS;
                gvAppointment.DataBind();

                DataRow[] TempRow;
                TempRow = DS.Tables[0].Select(" HAS_STATUS='Booked'");

                lblBooked.Text = TempRow.Length.ToString();

                DataRow[] TempRow1;
                TempRow1 = DS.Tables[0].Select(" HAS_STATUS='Confirmed'");

                lblConfirmed.Text = TempRow1.Length.ToString();

                DataRow[] TempRow2;
                TempRow2 = DS.Tables[0].Select(" HAS_STATUS IN ( 'Completed','Closed' )");

                lblCompleted.Text = TempRow2.Length.ToString();


                lblAppointTotal.Text = DS.Tables[0].Rows.Count.ToString();

            }
            else
            {
                gvAppointment.DataBind();

            }

            if (GlobalValues.FileDescription == "REPC")
            {
                lblBookedCaption.Visible = false;
                lblBooked.Visible = false;

                lblConfirmedCaption.Visible = false;
                lblConfirmed.Visible = false;


                lblCompletedCaption.Visible = false;
                lblCompleted.Visible = false;

            }



        }

        /*
                void BindVisitDtlsChart()
                {
                    string Report = "VisitDtlsPieChart.rpt";

                    string Criteria = " 1=1 ";
                    string strStartDate = txtFromDate.Text;
                    string[] arrDate = strStartDate.Split('/');
                    string strForStartDate = "";

                    if (arrDate.Length > 1)
                    {
                        strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                    }


                    string strTotDate = txtToDate.Text;
                    string[] arrToDate = strTotDate.Split('/');
                    string strForToDate = "";

                    if (arrToDate.Length > 1)
                    {
                        strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
                    }


                    Criteria += " AND  {HMS_PATIENT_VISIT.HPV_DATE}>=date(\'" + strForStartDate + "\') AND  {HMS_PATIENT_VISIT.HPV_DATE}<=date(\'" + strForToDate + "\')";

                    if (drpDoctor.SelectedIndex != 0)
                    {
                        Criteria += " AND {HMS_PATIENT_VISIT.HPV_DR_ID} =\'" + drpDoctor.SelectedValue + "\'";
                    }


                    frmVisitDtlhart.Src = "CReports/ChartViewer.aspx?ReportName=" + Report + "&SelectionFormula=" + Criteria;
                }


                void BindInvoicDtlsChart()
                {
                    string Report = "InvoiceDtlsPieChart.rpt";

                    string Criteria = " 1=1 ";
                    string strStartDate = txtFromDate.Text;
                    string[] arrDate = strStartDate.Split('/');
                    string strForStartDate = "";

                    if (arrDate.Length > 1)
                    {
                        strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                    }


                    string strTotDate = txtToDate.Text;
                    string[] arrToDate = strTotDate.Split('/');
                    string strForToDate = "";

                    if (arrToDate.Length > 1)
                    {
                        strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
                    }


                    Criteria += " AND  {HMS_INVOICE_MASTER.HIM_DATE}>=date(\'" + strForStartDate + "\') AND  {HMS_INVOICE_MASTER.HIM_DATE}<=date(\'" + strForToDate + "\')";

                    if (drpDoctor.SelectedIndex != 0)
                    {
                        Criteria += " AND {HMS_INVOICE_MASTER.HIM_DR_CODE} =\'" + drpDoctor.SelectedValue + "\'";
                    }


                    frmInvoiceDtlhart.Src = "CReports/ChartViewer.aspx?ReportName=" + Report + "&SelectionFormula=" + Criteria;
                }

        */
        void Clear()
        {
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

            if (drpDoctor.Items.Count > 0)
                drpDoctor.SelectedIndex = 0;

            gvPTVisit.Visible = false;
            gvPTVisit.DataBind();

            gvInvoice.Visible = false;
            gvInvoice.DataBind();

            gvAppointment.Visible = false;
            gvAppointment.DataBind();

            //frmVisitDtlhart.Src = "#";
            // frmInvoiceDtlhart.Src = "#";

            lblNew.Text = "0";
            lblRevisit.Text = "0";
            lblOld.Text = "0";
            lblTotal.Text = "0";


            lblInvTotal.Text = "0";
            lblInvGross.Text = "0.00";
            lblInvNet.Text = "0.00";
            lblInvPTShare.Text = "0.00";

            lblAppointTotal.Text = "0";
            lblBooked.Text = "0";
            lblConfirmed.Text = "0";
            lblCompleted.Text = "0";
        }

        /*
        protected void BindChart()
        {
            DataSet ds = new DataSet();
            
           // string cmdstr = "select top 6 Country, COUNT(CompanyName) [Total Suppliers] from [Suppliers] group by Country";

            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            }



            CommonBAL objcom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objcom.fnGetFieldValue("COUNT(HPV_VISIT_TYPE),COUNT(HPV_PT_ID)","HMS_PATIENT_VISIT WHERE ", Criteria,"");

            string[] x = new string[DS.Tables[0].Rows.Count];
            int[] y = new int[DS.Tables[0].Rows.Count];
            for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            {
                x[i] = DS.Tables[0].Rows[i][0].ToString();
                y[i] = Convert.ToInt32(DS.Tables[0].Rows[i][1]);
            }
            Chart1.Series[0].Points.DataBindXY(x, y);
            Chart1.Series[0].ChartType = SeriesChartType.Column;
            Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            Chart1.Legends[0].Enabled = true;
        }

        */

        void GetVisitDtlChartData()
        {

            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
            }



            CommonBAL objcom = new CommonBAL();
            DataSet DS = new DataSet();
            if (txtVisitMaxRow.Text.Trim() != "")
            {
                DS = objcom.fnGetFieldValue(" TOP " + txtVisitMaxRow.Text.Trim() + " HPV_VISIT_TYPE  as VisitType,COUNT(HPV_PT_ID) as PTCount", "HMS_PATIENT_VISIT ", Criteria + " GROUP BY HPV_VISIT_TYPE ", "");
            }
            else
            {
                DS = objcom.fnGetFieldValue(" HPV_VISIT_TYPE  as VisitType,COUNT(HPV_PT_ID) as PTCount", "HMS_PATIENT_VISIT ", Criteria + " GROUP BY HPV_VISIT_TYPE ", "");
            }

            Series series = VisitDtlChart.Series["Series1"];
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                series.Points.AddXY(Convert.ToString(DR["VisitType"]), Convert.ToString(DR["PTCount"]));
            }

        }

        void GetInvoiceDtlChartData()
        {

            string Criteria = " 1=1 ";
            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
            }


            if (drpDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HIM_DR_CODE ='" + drpDoctor.SelectedValue + "'";
            }




            CommonBAL objcom = new CommonBAL();
            DataSet DS = new DataSet();
            if (txtInvoiceMaxRow.Text.Trim() != "")
            {
                DS = objcom.fnGetFieldValue(" TOP " + txtInvoiceMaxRow.Text.Trim() + " HIM_INVOICE_TYPE AS InvType ,COUNT(HIM_INVOICE_ID) as InvCount", "HMS_INVOICE_MASTER ", Criteria + " GROUP BY HIM_INVOICE_TYPE ", "");
            }
            else
            {
                DS = objcom.fnGetFieldValue(" HIM_INVOICE_TYPE AS InvType ,COUNT(HIM_INVOICE_ID) as InvCount", "HMS_INVOICE_MASTER ", Criteria + " GROUP BY HIM_INVOICE_TYPE ", "");

            }

            Series series = InvoiceDtlChart.Series["Series1"];
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                series.Points.AddXY(Convert.ToString(DR["InvType"]), Convert.ToString(DR["InvCount"]));
            }

        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    BindDoctor();
                    BindNurses();
                    BindAppointmentStatus();
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {
                        if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                        {
                            divVisitDtls.Visible = false;
                        }
                    }

                    // //   BindVisitDtls();
                    // //   BindInvoiceDtls();
                    // //   BindAppointmentDtls();

                    ////    GetVisitDtlChartData();
                    // //   GetInvoiceDtlChartData();


                    // frmVisitDtlhart.Src = " ChartViewer.aspx";//?SelectionFormula=" + Criteria;

                    // BindVisitDtlsChart();
                    //  BindInvoicDtlsChart();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Home1.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");

            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");


            if (lblEMR_ID.Text != "" && lblEMR_ID.Text != null)
            {

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('EMR already Done','This visit already Done the EMR, So you can not edit.')", true);

                goto FunEnd;
            }
            else
            {
                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {
                    Response.Redirect("HMS/Registration/PTAndVisitRegistration.aspx?PatientId=" + lblPatientId.Text.Trim());
                }
                else
                {
                    Response.Redirect("HMS/Registration/PTRegistration.aspx?PatientId=" + lblPatientId.Text.Trim() + "&HPVSeqNo=" + lblSeqNo.Text.Trim());
                }
            }



        FunEnd: ;


        }

        protected void gvPTVisit_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindVisitDtls();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void imgInvSelect_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


            // Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");


            Label lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");


            Response.Redirect("HMS/Billing/Invoice.aspx?InvoiceId=" + lblInvoiceId.Text.Trim());




        FunEnd: ;
        }

        protected void gvInvPrint_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            // Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");

            string ReportName = "";
            Label lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");
            Label lblgvInvType = (Label)gvScanCard.Cells[0].FindControl("lblgvInvType");

            if (lblgvInvType.Text == "Cash")
            {
                if (GlobalValues.InvoiceType == "PERFORMA")
                {
                    ReportName = "HmsInvoiceCa_PERFORMA.rpt";
                }
                else
                {
                    ReportName = "HmsInvoiceCa.rpt";
                }

            }
            else if (lblgvInvType.Text == "Credit" || lblgvInvType.Text == "Customer")
            {
                if (GlobalValues.InvoiceType == "PERFORMA")
                {
                    ReportName = "HmsInvoiceCr_performa.rpt";
                }
                else
                {
                    ReportName = "HmsInvoiceCr.rpt";
                }
            }


            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + Convert.ToString(Session["Branch_ID"]) + "','" + lblInvoiceId.Text + "','" + ReportName + "');", true);





        FunEnd: ;
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                gvInvoice.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblInvoiceId, lblInvType;

                lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");
                lblInvType = (Label)gvScanCard.Cells[0].FindControl("lblInvType");




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home1.aspx_Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }




        protected void btnRefresh_Click(object sender, EventArgs e)
        {


            BindVisitDtls();
            BindInvoiceDtls();
            BindAppointmentDtls();
            //   GetVisitDtlChartData();
            //  GetInvoiceDtlChartData();
            // BindVisitDtlsChart();
            // BindInvoicDtlsChart();
        }

        protected void btnTodayDate_Click(object sender, EventArgs e)
        {
            Clear();

            BindVisitDtls();
            BindInvoiceDtls();
            BindAppointmentDtls();


            //   BindVisitDtlsChart();
            //  BindInvoicDtlsChart();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            Clear();

        }

        protected void btnPTVisitRef_Click(object sender, EventArgs e)
        {
            BindVisitDtls();
        }


        protected void btnInvoiceRef_Click(object sender, EventArgs e)
        {
            BindInvoiceDtls();
        }


        protected void btnAppointmentRef_Click(object sender, EventArgs e)
        {
            BindAppointmentDtls();
        }




        protected void lnkgvPTVisitInvStatus_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


            Label lblInvoiceStatus = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceStatus");
            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");

            if (lblInvoiceStatus.Text == "Not Invoiced")
            {
                Response.Redirect("HMS/Billing/Invoice.aspx?PatientId=" + lblPatientId.Text.Trim() + "&HPVSeqNo=" + lblSeqNo.Text.Trim());
            }

        }

        #endregion
    }
}