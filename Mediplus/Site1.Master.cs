﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;


using Mediplus_BAL;


namespace Mediplus
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("~/HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }

            lblLoginUser.Text = "Welcome " + Convert.ToString(Session["User_Name"]).ToLower() + ", " + Convert.ToString(Session["User_DeptID"]).ToLower();

            if (!IsPostBack)
            {
               
                    if (Convert.ToString(Session["User_Code"]) != "")
                    {
                        DataSet DS = new DataSet();
                        CommonBAL dbo = new CommonBAL();
                        string Criteria = " 1=1  ";
                        Criteria += " AND HSP_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

                        DS = dbo.StaffPhotoGet(Criteria);

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            if (DS.Tables[0].Rows[0].IsNull("HSP_PHOTO") == false)
                            {
                                imgStaffPhoto.ImageUrl = "DisplayUserProfileImage.aspx";
                            }

                        }
                    }
               


            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1 ";
                Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "' AND   HUM_USER_PASS= '" + txtOldPwd.Text.Trim() + "'";
                DataSet ds = new DataSet();
                CommonBAL objCom = new CommonBAL();


                ds = objCom.UserMasterGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string FieldNameWithValues = "";
                    FieldNameWithValues = " HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_USER_MASTER", Criteria);
                    // objDB.ExecuteReader("UPDATE HMS_USER_MASTER SET HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "' WHERE   HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "'");

                    lblStatus.Text = "Password Changed Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblStatus.Text = "Your Old Password Is Incorrect";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnHomeLink_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "UnderConstruct.aspx";
        }
        protected void btn360Link_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "UnderConstruct.aspx";
        }

        protected void btnAppointmentLink_Click(object sender, ImageClickEventArgs e)
        {

          //  string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=AppointmentDay&LoginFrom=CommonPage";


           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;
            frmHome.Src = "HMS/Registration/AppointmentDay.aspx";

        }

        protected void btnPTRegistration_Click(object sender, ImageClickEventArgs e)
        {
           // string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PatientId=" + Convert.ToString( Session["PatientID"]) + "&PageName=PTRegistration&LoginFrom=CommonPage";


           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "HMS/Registration/Patient_Registration.aspx";
        }

        protected void btnInvoiceLink_Click(object sender, ImageClickEventArgs e)
        {
          //  string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=Invoice&LoginFrom=CommonPage";

 
           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "HMS/Billing/Invoice.aspx";
        }

        protected void btneAuthApproval_Click(object sender, ImageClickEventArgs e)
        {

            frmHome.Src = "eAuthorization/eAuthApproval.aspx";
        }



        protected void btnEMRLink_Click(object sender, ImageClickEventArgs e)
        {
           // string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&User_DeptID=" + Convert.ToString(Session["User_DeptID"]) + "&PageName=Home&LoginFrom=CommonPage";

           // frmHome.Src = GlobalValues.EMR_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "EMR/Registration/PatientWaitingList.aspx";
        }

        protected void btnRadiologyLink_Click(object sender, ImageClickEventArgs e)
        {
            //string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=Radiology&LoginFrom=CommonPage";


           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "Radiology/Transaction.aspx";
        }

        protected void btnLaboratoryLink_Click(object sender, ImageClickEventArgs e)
        {
            //string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=Laboratory&LoginFrom=CommonPage";


           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;
            frmHome.Src = "Laboratory/LabTestReport.aspx";

        }

        protected void btnIPLink_Click(object sender, ImageClickEventArgs e)
        {
            string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&User_DeptID=" + Convert.ToString(Session["User_DeptID"]) + "&PageName=Home&LoginFrom=CommonPage";

               frmHome.Src =GlobalValues.EMR_IP_Path + "/CommonPageLoader.aspx?" + rptcall;
        }

        protected void btnMasters_Click(object sender, ImageClickEventArgs e)
        {
            string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=Masters&LoginFrom=CommonPage";


            frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;
        }

        protected void btnMasterLink1_Click(object sender, ImageClickEventArgs e)
        {
           // string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=CompanyProfile&LoginFrom=CommonPage";


           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "HMS/Masters/CompanyProfile.aspx";
        }

        protected void btnMasterLink2_Click(object sender, ImageClickEventArgs e)
        {
            //string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=StaffProfile&LoginFrom=CommonPage";


           // frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "HMS/Masters/StaffProfile.aspx";
        }

        protected void btnMasterLink3_Click(object sender, ImageClickEventArgs e)
        {
            //string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&PageName=HospitalServicese&LoginFrom=CommonPage";


            //frmHome.Src = GlobalValues.HMS_Path + "/CommonPageLoader.aspx?" + rptcall;

            frmHome.Src = "HMS/Masters/ServiceMaster.aspx";
        }


        protected void btnPaymentLink_Click(object sender, ImageClickEventArgs e)
        {

            frmHome.Src = "Accounts/PaymentEntry.aspx?PageName=Payment";

        }

        protected void btnReceiptLink_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Accounts/ReceiptEntry.aspx?PageName=Receipt";
        }

        protected void btnAccountMasLink_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Accounts/AccountMaster.aspx";
        }

        protected void btnAccMastLink_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Accounts/AccountMaster.aspx";
        }

        protected void btnCostMastLink_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Accounts/CostCenterMaster.aspx";
        }

        protected void btnPDCPosting_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Accounts/PDCPosting.aspx";
        }

        protected void btnReport_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Accounts/ReportLoader.aspx";
        }

        protected void btnInventory_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Inventory/ProductMaster.aspx";
        }

        protected void btnSupplier_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "";
        }

        protected void btnGRN_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Inventory/GoodsReceivedNote.aspx";
        }

        protected void btnGCV_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Inventory/GoodsConsumptionVoucher.aspx";
        }

        protected void btnGTV_Click(object sender, ImageClickEventArgs e)
        {
            frmHome.Src = "Inventory/GoodsTransferVoucher.aspx";
        }

       

       
    }
}