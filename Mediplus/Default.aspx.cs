﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace Mediplus
{
    public partial class Default : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("~/Log/MediplusLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        private bool CheckPasswordFormat(string passwordText)
        {

            int minimumLength = Convert.ToInt32(Session["PWD_MIN_LENGTH"]), maximumLength = Convert.ToInt32(Session["PWD_MAX_LENGTH"]),
                minimumNumbers = Convert.ToInt32(Session["PWD_NUM_MIN_LENGTH"]), minimumLetters = Convert.ToInt32(Session["PWD_LETTER_MIN_LENGTH"]),
                minimumSpecialCharacters = 1, minUpperCharacters = 1;

            //Assumes that special characters are anything except upper and lower case letters and digits
            //Assumes that ASCII is being used (not suitable for many languages)

            int letters = 0;
            int digits = 0;
            int specialCharacters = 0;
            int UpperCharacters = 0;

            //Make sure there are enough total characters
            if (passwordText.Length < minimumLength)
            {
                ShowError("You must have at least " + minimumLength + " characters in your password.");
                return false;
            }
            //Make sure there are enough total characters
            if (passwordText.Length > maximumLength)
            {
                ShowError("You must have no more than " + maximumLength + " characters in your password.");
                return false;
            }

            foreach (var ch in passwordText)
            {
                if (char.IsLetter(ch)) letters++; //increment letters
                if (char.IsDigit(ch)) digits++; //increment digits

                //Test for only letters and numbers...
                if (!((ch > 47 && ch < 58) || (ch > 64 && ch < 91) || (ch > 96 && ch < 123)))
                {
                    specialCharacters++;
                }

                if (Char.IsUpper(ch) == true)
                {
                    UpperCharacters++;
                }
            }
            //Make sure there are enough digits
            if (letters < minimumLetters)
            {
                ShowError("You must have at least " + minimumLetters + " letters in your password.");
                return false;
            }


            //Make sure there are enough digits
            if (digits < minimumNumbers)
            {
                ShowError("You must have at least " + minimumNumbers + " numbers in your password.");
                return false;
            }

            //Make sure there are enough special characters -- !(a-zA-Z0-9)
            if (specialCharacters < minimumSpecialCharacters)
            {
                ShowError("You must have at least " + minimumSpecialCharacters + " special characters (like @,$,%,#) in your password.");
                return false;
            }


            if (UpperCharacters < minUpperCharacters)
            {
                ShowError("You must have at least one Upper characters  in your password.");
                return false;
            }
            return true;
        }

        private void ShowError(string errorMessage)
        {
            lblChangePwdStatus.Text = errorMessage;
        }

        #region Methods
        void BindBranch()
        {

            DataSet ds = new DataSet();
            ds = objCom.BranchMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpBranch.DataSource = ds;
                drpBranch.DataValueField = "HBM_ID";
                drpBranch.DataTextField = "HBM_ID";
                drpBranch.DataBind();

            }

        }


        void GetBranchDtls()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HBM_STATUS='A' AND HBM_ID='" + drpBranch.SelectedValue + "'";
            DS = objCom.fnGetFieldValue("*", "HMS_BRANCH_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["Branch_ProviderID"] = Convert.ToString(DS.Tables[0].Rows[0]["HBM_COMM_LIC_NO"]);
                Session["Branch_Name"] = Convert.ToString(DS.Tables[0].Rows[0]["HBM_NAME"]);


            }

        }


        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";  // AND HUM_USER_ID <> 'SUPER_ADMIN'
            Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "'";
            // Criteria += " AND ( HUM_REMARK IN (SELECT HSFM_STAFF_ID FROM HMS_STAFF_MASTER WHERE HSFM_CATEGORY IN ('Doctors','Nurse') OR HUM_USER_ID ='ADMIN')) ";


            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpUsers.DataSource = ds;
                drpUsers.DataValueField = "HUM_USER_ID";
                drpUsers.DataTextField = "HUM_USER_NAME";
                drpUsers.DataBind();

            }
            drpUsers.Items.Insert(0, "Select Username");
            drpUsers.Items[0].Value = "0";


        }


        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();
            DS = objCom.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEF_CUR_ID") == false)
                {
                    Session["HSOM_DEF_CUR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEF_CUR_ID"]);

                }
                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_AUTO_SL_NO") == false)
                {
                    Session["AutoSlNo"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_AUTO_SL_NO"]);

                }


                //PATIENT VISIT STATUS IS 'F' IF IT IS Y ELSE IT WILL BE 'W'
                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILING_REQUEST") == false)
                {
                    Session["FilingRequest"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_FILING_REQUEST"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IdPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ID_PRINT"]);

                }
                //CLEAR THE DATA AFTER SAVING
                if (DS.Tables[0].Rows[0].IsNull("HSOM_CLEAR_AFT_SAVE") == false)
                {
                    Session["ClearAfterSave"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_CLEAR_AFT_SAVE"]);

                }

                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_SCAN_SAVE") == false)
                {
                    Session["ScanSave"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SCAN_SAVE"]);

                }
                else
                {
                    Session["ScanSave"] = "0";
                }

                //REVISIT DAYS 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_REVISIT_DAYS") == false)
                {
                    Session["RevisitDays"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_REVISIT_DAYS"]);

                }


                //NETWORK NAME  DROPDWON IS MANDATORY OR NOT
                if (DS.Tables[0].Rows[0].IsNull("HSOM_PACKAGE_MANDITORY_PTREG") == false)
                {
                    Session["PackageManditory"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PACKAGE_MANDITORY_PTREG"]);


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_MULTIPLEVISITADAY") == false)
                {
                    Session["MultipleVisitADay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_MULTIPLEVISITADAY"]);


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PTNAME_MULTIPLE") == false)
                {
                    Session["PTNameMultiple"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PTNAME_MULTIPLE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("hsom_label_copies") == false)
                {
                    Session["LabelCopies"] = Convert.ToString(DS.Tables[0].Rows[0]["hsom_label_copies"]);

                }
                else
                {
                    Session["LabelCopies"] = 1;
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_TYPE") == false)
                {
                    Session["DefaultType"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_TYPE"]);

                }
                else
                {
                    Session["DefaultType"] = "false";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SEARCH_OPT") == false)
                {
                    Session["SearchOption"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SEARCH_OPT"]);

                }
                else
                {
                    Session["SearchOption"] = "AN";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILENO_MONTHYEAR") == false)
                {
                    Session["FileNoMonthYear"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_FILENO_MONTHYEAR"]);

                }
                else
                {
                    Session["FileNoMonthYear"] = "N";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_REPORT_PATH") == false)
                {
                    Session["ReportPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_REPORT_PATH"]);

                }
                else
                {
                    Session["ReportPath"] = "D:\\HMS\\Reports";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"]);

                }
                else
                {
                    Session["TokenPrint"] = "";
                }



                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IDPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ID_PRINT"]);

                }
                else
                {
                    Session["IDPrint"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_COINS_PTREG") == false)
                {
                    Session["ConInsPTReg"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_COINS_PTREG"]);

                }
                else
                {
                    Session["ConInsPTReg"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SHOW_TOKEN") == false)
                {
                    Session["ShowToken"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SHOW_TOKEN"]);

                }
                else
                {
                    Session["ShowToken"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DSPLY_DR_PTREG") == false)
                {
                    Session["DrNameDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DSPLY_DR_PTREG"]);

                }
                else
                {
                    Session["DrNameDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PRIORITY_OPT_FILING") == false)
                {
                    Session["PriorityDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PRIORITY_OPT_FILING"]);

                }
                else
                {
                    Session["PriorityDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_KNOW_FROM") == false)
                {
                    Session["KnowFromDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_KNOW_FROM"]);

                }
                else
                {
                    Session["KnowFromDisplay"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEP_WISE_PTID") == false)
                {
                    Session["DeptWisePTId"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEP_WISE_PTID"]);

                }
                else
                {
                    Session["DeptWisePTId"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    Session["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    Session["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    Session["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    Session["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    Session["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    Session["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    Session["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    Session["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    Session["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ECLAIM") == false)
                {
                    Session["HSOM_ECLAIM"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ECLAIM"]);

                }
                else
                {
                    Session["HSOM_ECLAIM"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_LAB_LIST") == false)
                {
                    Session["HSOM_LAB_LIST"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_LAB_LIST"]);

                }
                else
                {
                    Session["HSOM_LAB_LIST"] = "INVOICE";
                }





            }

        }

        void GetUsersRemarks()
        {
            string Criteria = " 1=1 ";
            if (GlobalValues.FileDescription == "SMCH")// if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            {
                Criteria += " AND HUM_STATUS='A' and  HUM_USER_ID='" + txtUserID.Text.Trim() + "'";

            }
            else
            {
                Criteria += " AND HUM_STATUS='A' and  HUM_USER_ID='" + drpUsers.SelectedValue + "'";
            }
            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["HUM_REMARK"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REMARK"]);
            }
        }


        void BindStaffData()
        {
            DataSet DS = new DataSet();

            DS = (DataSet)ViewState["StaffData"];
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

            DS = (DataSet)ViewState["StaffData"];
            DataRow[] result = DS.Tables[0].Select(Criteria);
            foreach (DataRow DR in result)
            {

                Session["FullName"] = Convert.ToString(DR["FullName"]);
                Session["User_DeptID"] = Convert.ToString(DR["HSFM_DEPT_ID"]);
                Session["User_Category"] = Convert.ToString(DR["HSFM_CATEGORY"]);
                //txtStaffName.Text = Convert.ToString(DR["FullName"]);
                Session["HSFM_ROOM"] = Convert.ToString(DR["HSFM_ROOM"]);
            }

        }

        void BindAllStaffData()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += "  AND HSFM_BRANCH_ID ='" + drpBranch.SelectedValue + "'";

            DS = objCom.GetStaffMaster(Criteria);

            ViewState["StaffData"] = DS;
        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND ( SCREENNAME='EMR' OR  SCREENNAME='HMS' OR SCREENNAME='MEDIPLUS' OR    SCREENNAME='ECLAIM' ) ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            Session["EMR_START_PROC_CONFIRM"] = "0";
            Session["EMR_COMP_PROC_CONFIRM"] = "0";

            Session["EMR_LOGOUT_CONFIRM"] = "0";

            Session["EMR_EDIT_DATA_AFTER_COMPLETE"] = "0";
            Session["EMR_SAVE_PRESC_TO_HAAD"] = "0";
            Session["EMR_DISPLAY_AUDIT"] = "0";

            Session["EMR_SAVE_VITAL_MULTIPLE"] = "0";
            Session["EMR_REQUIRED_AUDIT"] = "0";

            Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"] = "1";


            Session["EMR_ALLERGY_DISPLAY"] = "0";
            Session["EMR_HIST_SOCIAL_DISPLAY"] = "0";
            Session["EMR_HIST_PAST_DISPLAY"] = "0";

            Session["EMR_DISPLAY_TEMPLATE"] = "0";
            Session["EMR_DISPLAY_HISTORY"] = "0";
            Session["EMR_DISPLAY_ICD_CROSSWALK"] = "0";

            Session["EMR_COMMOM_DR_ID"] = "";

            Session["EMR_DIAG_UPDATE_INVOICE"] = "0";

            Session["EMR_HEADER_ALERT_SEGMENTS"] = "";


            Session["EMR_SEGMENTS_SAVE_PTID"] = "";
            Session["EMR_CLSUMMARY_SEGMENTS"] = "";
            Session["EMR_CLSUMMARY_SEGMENTS1"] = "";

            Session["EMR_NARRATIVE_DIAGNOSIS"] = "0";
            Session["EMR_MESSENGER_OPTION"] = "0";
            Session["EMR_CLINIC_STATUS"] = "0";
            Session["EMR_OTHER_USER_EDIT"] = "0";
            Session["EMR_EDIT_TIME_PERIOD"] = "0";
            Session["HOMEPAGE_NAME"] = "Home1.aspx";


            Session["SALESTAX"] = "NO";

            Session["PWD_EXPIRY_DAYS"] = "0";

            Session["HMS_AUTHENTICATION"] = "0";
            Session["HMS_ENABLE_LOGFILE"] = "0";

            Session["MEDITRANS_FILE_STORE_IN_DB"] = "0";

            Session["TRIAGE_OPTION"] = "0";
            Session["TRIAGE_HIDE_DEPTS"] = "";

            Session["REQ_INVOICE_NO_LAB_TESTREPORT"] = "0";

            Session["LAB_RESULT_DATAUPLOAD"] = "0";
            Session["ECLAIM_TYPE"] = "AUH";

            Session["PWD_ENCRYPT"] = "0";
            Session["PWD_MINIMUM_AGE"] = "0";

            Session["PWD_FORMAT_REQUIRED"] = "0";

            Session["PWD_MIN_LENGTH"] = "0";
            Session["PWD_MAX_LENGTH"] = "0";
            Session["PWD_NUM_MIN_LENGTH"] = "0";
            Session["PWD_LETTER_MIN_LENGTH"] = "0";


            Session["PWD_PREV_NOTREPEAT"] = "0";

            Session["HMS_LABEL_DIRECT_PRINT"] = "0";
            Session["HMS_LOCAL_REPORT_EXE_PATH"] = "D:\\HMS\\";
            Session["HMS_LOCAL_SIGNATUREPAD_EXE_PATH"] = "D:\\HMS\\NewWacomSignature\\";
            Session["HMS_LOCAL_EMIRATESID_EXE_PATH"] = "D:\\HMS\\EIDA_CardDisplay\\";
            Session["HMS_LOCAL_BARCODE_EXE_PATH"] = "D:\\HMS\\";

            Session["EMR_ENABLE_MALAFFI"] = "0";

            Session["LAB_TEST_REPORT_WITHOUT_SAMPLE_COLLECTION"] = "0";
            Session["SO_ADD_ONLY_APPROVED_SERV"] = "0";

            Session["LOGIN_ACTIVE_DIRECTORY_USER"] = "0";
            Session["ACTIVE_DIRECTORY_NAME"] = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_START_PROC_CONFIRM")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_START_PROC_CONFIRM"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_COMP_PROC_CONFIRM")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_COMP_PROC_CONFIRM"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_LOGOUT_CONFIRM")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_LOGOUT_CONFIRM"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_EDIT_DATA_AFTER_COMPLETE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_EDIT_DATA_AFTER_COMPLETE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SAVE_PRESC_TO_HAAD")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_SAVE_PRESC_TO_HAAD"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DISPLAY_AUDIT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_DISPLAY_AUDIT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SAVE_VITAL_MULTIPLE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_SAVE_VITAL_MULTIPLE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_REQUIRED_AUDIT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_REQUIRED_AUDIT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_VISIT_DTLS_EDIT_ALL_USERS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_ALLERGY_DISPLAY")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_ALLERGY_DISPLAY"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_HIST_SOCIAL_DISPLAY")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_HIST_SOCIAL_DISPLAY"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_HIST_PAST_DISPLAY")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_HIST_PAST_DISPLAY"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DISPLAY_TEMPLATE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_DISPLAY_TEMPLATE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DISPLAY_HISTORY")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_DISPLAY_HISTORY"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DISPLAY_ICD_CROSSWALK")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_DISPLAY_ICD_CROSSWALK"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_COMMOM_DR_ID")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_COMMOM_DR_ID"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DIAG_UPDATE_INVOICE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_DIAG_UPDATE_INVOICE"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_HEADER_ALERT_SEGMENTS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_HEADER_ALERT_SEGMENTS"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SEGMENTS_SAVE_PTID")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_SEGMENTS_SAVE_PTID"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_CLSUMMARY_SEGMENTS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_CLSUMMARY_SEGMENTS"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }



                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_CLSUMMARY_SEGMENTS1")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_CLSUMMARY_SEGMENTS1"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }



                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_NARRATIVE_DIAGNOSIS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_NARRATIVE_DIAGNOSIS"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_MESSENGER_OPTION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_MESSENGER_OPTION"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_CLINIC_STATUS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_CLINIC_STATUS"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_OTHER_USER_EDIT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_OTHER_USER_EDIT"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_EDIT_TIME_PERIOD")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_EDIT_TIME_PERIOD"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]).ToUpper() == "SALESTAX")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["SALESTAX"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]).ToUpper() == "HOMEPAGE_NAME")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HOMEPAGE_NAME"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_EXPIRY_DAYS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_EXPIRY_DAYS"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_AUTHENTICATION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_AUTHENTICATION"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_ENABLE_LOGFILE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_ENABLE_LOGFILE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "MEDITRANS_FILE_STORE_IN_DB")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["MEDITRANS_FILE_STORE_IN_DB"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "TRIAGE_OPTION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["TRIAGE_OPTION"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "TRIAGE_HIDE_DEPTS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["TRIAGE_HIDE_DEPTS"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "REQ_INVOICE_NO_LAB_TESTREPORT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["REQ_INVOICE_NO_LAB_TESTREPORT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "LAB_RESULT_DATAUPLOAD")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["LAB_RESULT_DATAUPLOAD"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                    if (Convert.ToString(DR["SEGMENT"]) == "ECLAIM_TYPE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["ECLAIM_TYPE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_ENCRYPT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_ENCRYPT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MINIMUM_AGE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MINIMUM_AGE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_FORMAT_REQUIRED")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_FORMAT_REQUIRED"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MAX_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MAX_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_NUM_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_NUM_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_LETTER_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_LETTER_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_PREV_NOTREPEAT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_PREV_NOTREPEAT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_LABEL_DIRECT_PRINT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_LABEL_DIRECT_PRINT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_LOCAL_REPORT_EXE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_LOCAL_REPORT_EXE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_LOCAL_SIGNATUREPAD_EXE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_LOCAL_SIGNATUREPAD_EXE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_LOCAL_EMIRATESID_EXE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_LOCAL_EMIRATESID_EXE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_LOCAL_BARCODE_EXE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_LOCAL_BARCODE_EXE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_ENABLE_MALAFFI")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_ENABLE_MALAFFI"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "LAB_TEST_REPORT_WITHOUT_SAMPLE_COLLECTION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["LAB_TEST_REPORT_WITHOUT_SAMPLE_COLLECTION"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "SO_ADD_ONLY_APPROVED_SERV")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["SO_ADD_ONLY_APPROVED_SERV"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "LOGIN_ACTIVE_DIRECTORY_USER")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["LOGIN_ACTIVE_DIRECTORY_USER"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "ACTIVE_DIRECTORY_NAME")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["ACTIVE_DIRECTORY_NAME"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }


                }
            }



        }

        Boolean CheckPasswordExpiry()
        {

            string Criteria = " 1=1 ";

            if (GlobalValues.FileDescription == "SMCH")  //if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            {
                Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND  HUM_USER_ID='" + txtUserID.Text.Trim() + "'";

            }
            else
            {
                Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "'";
            }




            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue("DATEDIFF(DD,CONVERT(DATETIME,HUM_CREATED_DATE,101),CONVERT(DATETIME,GETDATE(),101)) AS UserCreatedDateDiff", "HMS_USER_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {

                Int64 intUserDateDiff = 0, intExpDays = 0;

                if (DS.Tables[0].Rows[0].IsNull("UserCreatedDateDiff") == false && Convert.ToString(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]) != "")
                {

                    intUserDateDiff = Convert.ToInt64(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]);

                    Session["UserCreatedDateDiff"] = Convert.ToInt64(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("UserCreatedDateDiff") == false && Convert.ToString(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]) != "")
                {
                    intExpDays = Convert.ToInt64(Session["PWD_EXPIRY_DAYS"]);

                }


                Int64 intUserPasswordExpire = intExpDays - intUserDateDiff;



                Session["UserPasswordExpire"] = intUserPasswordExpire;


                if (intUserDateDiff > intExpDays)
                {
                    return true;
                }
            }

            return false;
        }

        private void GetADUsers()
        {
            try
            {
                System.Security.Principal.WindowsIdentity identity = HttpContext.Current.Request.LogonUserIdentity;
                string SystemUser = identity.Name;
                SystemUser = SystemUser.Substring(SystemUser.LastIndexOf("\\") + 1, SystemUser.Length - (SystemUser.LastIndexOf("\\") + 1));


                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2] { 
                new DataColumn("AccountName", typeof(string)), new DataColumn("GivenName", typeof(string)) 
            
            });
                using (var context = new PrincipalContext(ContextType.Domain, Convert.ToString(Session["ACTIVE_DIRECTORY_NAME"])))
                {
                    using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                    {
                        foreach (var result in searcher.FindAll())
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            string ADUser = Convert.ToString(de.Properties["samAccountName"].Value);
                            if (ADUser == SystemUser)
                            {
                                ViewState["AD_USER_VALID"] = true;
                                ViewState["AD_USER_NAME"] = ADUser;
                                txtUserID.Text = ADUser;

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Default.GetADUsers()");
                TextFileWriting(ex.Message.ToString());
            }
        }

        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["LoginFrom"]) == "CommonPage")
            {
                Session["LoginFrom"] = "";
                Response.Redirect("Default1.aspx");
            }

            BindSystemOption();
            BindScreenCustomization();

            if (!IsPostBack)
            {
                Session["Branch_ID"] = null;
                Session["User_ID"] = null;
                Session["User_Code"] = null;
                Session["User_Name"] = null;
                Session["User_Active"] = null;
                Session["Roll_Id"] = null;
                Session["HomeCareId"] = null;
                Session["ROLL_IDS"] = null;

                Session["WaitingLIstFromDate"] = null;
                Session["WaitingLIstToDate"] = null;
                Session["WaitingLIstStatus"] = "0";
                Session["HUM_REFRESH_INTERVAL"] = "0";

                Session["CopyClinicalNotes"] = "";
                Session["UserCreatedDateDiff"] = "0";
                Session["PREV_LOGIN"] = "";

                Session["MalaffiLogFromDate"] = "";
                Session["MalaffiLogToDate"] = "";
                Session["MalaffiLogFileNo"] = "";
                Session["MalaffiLogStatus"] = "";



                string strEMRPath = (string)System.Configuration.ConfigurationSettings.AppSettings["EMR_PATH"];

                Session["EMR_Path"] = strEMRPath;

                if (GlobalValues.FileDescription == "SMCH")// if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                {
                    drpUsers.Visible = false;
                    txtUserID.Visible = true;
                    txtUserID.Focus();
                }
                else
                {
                    drpUsers.Focus();
                }
                BindBranch();
                BindUsers();
                BindAllStaffData();

                if (!Directory.Exists(Server.MapPath("Log")))
                {
                    Directory.CreateDirectory(Server.MapPath("Log"));

                }
                string SystemUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                SystemUser = SystemUser.Substring(SystemUser.LastIndexOf("\\") + 1, SystemUser.Length - (SystemUser.LastIndexOf("\\") + 1));

                //  TextFileWriting(System.DateTime.Now.ToString() + " System User: " + SystemUser);
                // TextFileWriting(System.DateTime.Now.ToString() + " Environment User: " + Environment.UserName);

                System.Security.Principal.WindowsIdentity identity = HttpContext.Current.Request.LogonUserIdentity;

                if (Convert.ToString(Session["LOGIN_ACTIVE_DIRECTORY_USER"]) == "1" && Convert.ToString(Session["ACTIVE_DIRECTORY_NAME"]) != "")
                {
                    TextFileWriting(System.DateTime.Now.ToString() + " identity User: " + identity.Name);

                      GetADUsers();
                    // ViewState["AD_USER_VALID"] = true;
                   //  ViewState["AD_USER_NAME"] = "mirzad";
                   //  txtUserID.Text = "mirzad";
                    btnLogin_Click(btnLogin, new EventArgs());
                }


            }
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click   Start");
            if (GlobalValues.FileDescription == "SMCH")// if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            {
                if (txtUserID.Text.Trim() == "")
                {
                    goto FunEnd;
                }

            }
            else
            {
                if (drpUsers.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

            }
            // TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click   0");


            if (Convert.ToString(Session["PWD_EXPIRY_DAYS"]) != "0")
            {
                if ( Convert.ToBoolean(ViewState["AD_USER_VALID"]) == false &&  txtUserID.Text.ToUpper().Trim() != "SUPER_ADMIN")
                {
                    if (CheckPasswordExpiry() == true)
                    {
                        //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Password Expired');", true);

                        lblChangePwdStatus.Text = "Your Password has been Expired. Please change the Password.";
                        txtChangePwdUserID.Text = txtUserID.Text;
                        txtChangePwdUserID.Text = drpUsers.SelectedValue;
                        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowUserDetailsPopup();", true);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowUserDetailsPopup();", true);


                        goto FunEnd;
                    }
                }

            }
            //  TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  1");

            //q0D4PAvohF3CkmCESH3uKuZLHd5i0ISzPe9M87Nkqks=  (admin@123)
            string Criteria = " 1=1 ";

            string EncryPassword = "";
            if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            {
                EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtPassword.Text.Trim()));

                if (Convert.ToString(Session["LOGIN_ACTIVE_DIRECTORY_USER"]) == "1" && txtUserID.Text.ToUpper().Trim() != "SUPER_ADMIN")
                {
                    if (Convert.ToBoolean(ViewState["AD_USER_VALID"]) == true)
                    {
                        Criteria += "  AND HUM_USER_ID ='" + Convert.ToString(ViewState["AD_USER_NAME"]) + "'";
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Failed');", true);
                        goto FunEnd;
                    }
                }
                else
                {

                    if (GlobalValues.FileDescription == "SMCH")
                    {
                        Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";
                    }
                    else
                    {
                        Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";

                    }
                }

            }
            else
            {
                if (Convert.ToString(Session["LOGIN_ACTIVE_DIRECTORY_USER"]) == "1" && txtUserID.Text.ToUpper().Trim() != "SUPER_ADMIN")
                {
                    if (Convert.ToBoolean(ViewState["AD_USER_VALID"]) == true)
                    {
                        Criteria += "  AND HUM_USER_ID ='" + Convert.ToString(ViewState["AD_USER_NAME"]) + "'";
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Failed');", true);
                        goto FunEnd;
                    }
                }
                else
                {
                    Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + txtPassword.Text.Trim() + "'";
                }
            }

            //  TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  2 " + Criteria);

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                // TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  3");
                Session["Branch_ID"] = drpBranch.SelectedValue;
                Session["User_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_ID"]);
                Session["User_Code"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REMARK"]);
                Session["User_Name"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_NAME"]);
                Session["User_Active"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_STATUS"]);
                Session["Roll_Id"] = Convert.ToString(ds.Tables[0].Rows[0]["HGP_ROLL_ID"]);

                Session["ROLL_IDS"] = Convert.ToString(ds.Tables[0].Rows[0]["ROLL_IDS"]);


                if (ds.Tables[0].Columns.Contains("HUM_ENABLE_LOG") == true)
                {
                    if (ds.Tables[0].Rows[0].IsNull("HUM_ENABLE_LOG") == false && Convert.ToString(ds.Tables[0].Rows[0]["HUM_ENABLE_LOG"]) != "")
                    {
                        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["HUM_ENABLE_LOG"]) == true)
                        {
                            Session["ENABLE_USER_LOG"] = "1";
                        }
                        else
                        {
                            Session["ENABLE_USER_LOG"] = "0";
                        }
                    }
                }

                if (ds.Tables[0].Rows[0].IsNull("HUM_PREV_LOGIN") == false && Convert.ToString(ds.Tables[0].Rows[0]["HUM_PREV_LOGIN"]) != "")
                {
                    DateTime dtPrevLoginDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["HUM_PREV_LOGIN"]);
                    Session["PREV_LOGIN"] = dtPrevLoginDate.ToString("dd/MM/yyyy HH:mm");
                }

                //  TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  5");
                if (ds.Tables[0].Columns.Contains("HUM_REFRESH_INTERVAL") == true)
                {
                    if (ds.Tables[0].Rows[0].IsNull("HUM_REFRESH_INTERVAL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HUM_REFRESH_INTERVAL"]) != "")
                    {
                        Session["HUM_REFRESH_INTERVAL"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REFRESH_INTERVAL"]);
                    }
                    else
                    {
                        Session["HUM_REFRESH_INTERVAL"] = "0";
                    }
                }

                if (ds.Tables[0].Columns.Contains("HUM_ADDNEW_RECORD_FROMEMR") == true)
                {
                    if (ds.Tables[0].Rows[0].IsNull("HUM_ADDNEW_RECORD_FROMEMR") == false && Convert.ToString(ds.Tables[0].Rows[0]["HUM_ADDNEW_RECORD_FROMEMR"]) != "")
                    {
                        Session["HUM_ADDNEW_RECORD_FROMEMR"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_ADDNEW_RECORD_FROMEMR"]);
                    }
                    else
                    {
                        Session["HUM_ADDNEW_RECORD_FROMEMR"] = "False";
                    }
                }
                //  TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  6");
                BindStaffData();
                GetBranchDtls();

                ////if (GlobalValues.FileDescription != "SMCH")
                ////{
                ////    if ((Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" || Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTOR"))
                ////    {
                ////        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Permission Denied');", true);
                ////        ds.Clear();
                ////        ds.Dispose();

                ////        goto FunEnd;
                ////    }
                ////}
                // TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  7");
                if (Session["User_Active"].ToString() == "A")
                {
                    //  TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  8");
                    objCom = new CommonBAL();
                    string FieldNameWithValues = " HUM_PREV_LOGIN=getdate()";
                    if (GlobalValues.FileDescription == "SMCH")// if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                    {
                        Criteria = "  HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "'";
                    }
                    else
                    {
                        Criteria = "   HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "'";
                    }
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_USER_MASTER", Criteria);
                    //  TextFileWriting(System.DateTime.Now.ToString() + "      Default.btnLogin_Click  9");

                    // if (GlobalValues.FileDescription == "SMCH")
                    // {
                    //Response.Redirect("Home1.aspx");

                    Response.Redirect(Convert.ToString(Session["HOMEPAGE_NAME"]));
                    // }
                    // else
                    // {
                    //Response.Redirect("DeptHome.aspx?MenuName=Insurance");
                    //  Response.Redirect("DeptHome.aspx?MenuName=Laboratory");

                    //  }

                    // Response.Redirect("DeptHome.aspx?MenuName=Laboratory");
                    //    Response.Redirect("Insurance/HaadUtility.aspx?MenuName=Insurance");
                    ds.Clear();
                    ds.Dispose();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('User Inactive');", true);
                    ds.Clear();
                    ds.Dispose();
                }

            }
            else
            {
                txtPassword.Focus();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Failed');", true);
                ds.Clear();
                ds.Dispose();
            }

        FunEnd: ;
        }

        protected void btnForgotpassword_Click(object sender, EventArgs e)
        {
            try
            {
                txtChangePwdUserID.Text = drpUsers.SelectedValue;

                string StaffID = "", UserName = "", UserID = "", Password = "";
                CommonBAL objCom = new CommonBAL();
                DataSet DS = new DataSet();
                string Criteria = "HSFM_SF_STATUS='Present' AND HSFM_EMAIL='" + txtMailID.Text.Trim() + "'";
                DS = objCom.GetStaffMaster(Criteria);



                if (DS.Tables[0].Rows.Count <= 0)
                {
                    lblStatus.Text = "Mail ID is not valid";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (DS.Tables[0].Rows.Count > 0)
                {
                    StaffID = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STAFF_ID"]);
                    DataSet DSUser = new DataSet();
                    Criteria = " 1=1 AND HUM_REMARK='" + StaffID + "'";
                    DSUser = objCom.UserMasterGet(Criteria);
                    if (DSUser.Tables[0].Rows.Count > 0)
                    {
                        UserID = Convert.ToString(DSUser.Tables[0].Rows[0]["HUM_USER_ID"]);
                        UserName = Convert.ToString(DSUser.Tables[0].Rows[0]["HUM_USER_NAME"]);

                        string DecryptPassword = "";
                        if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                        {
                            DecryptPassword = objCom.SimpleDecrypt(Convert.ToString(DSUser.Tables[0].Rows[0]["HUM_USER_PASS"]));

                            Password = DecryptPassword;
                        }
                        else
                        {
                            Password = Convert.ToString(DSUser.Tables[0].Rows[0]["HUM_USER_PASS"]);
                        }

                    }

                    string Details_String = "";

                    Details_String = Details_String + "<TABLE cellpadding='2' cellspacing='2'>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>Dear  <b>" + UserName + ",</b></TD></TR>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='Left'>HIS User ID and Password, </TD></TR>";
                    Details_String = Details_String + "<TR><TD valign='top' align='left'><B>User ID : </B></TD> <TD>" + UserID + "</TD></TR>";
                    Details_String = Details_String + "<TR><TD valign='top' align='left'><B>Password : </B></TD> <TD>" + Password + "</TD></TR>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>Regards</TD></TR>";
                    Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>" + GlobalValues.HospitalName + "</TD></TR>";

                    Details_String = Details_String + "</TABLE>";

                    int intStatuls;

                    intStatuls = objCom.HTMLMailSend(txtMailID.Text.Trim(), "Reg HIS User ID and Password ", Details_String);
                    lblStatus.Text = "Your Uesr ID and Password will send to the Below Mail ID";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                }
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnForgotpassword_Click");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                CommonBAL objCom = new CommonBAL();
                string EncryPassword = "";

                string Criteria = " 1=1 ";
                string OldPwd = "";
                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                {
                    Criteria += " AND HUM_USER_ID ='" + txtChangePwdUserID.Text.Trim() + "'";
                    ds = objCom.UserMasterGet(Criteria);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        OldPwd = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_PASS"]);
                    }

                }


                if (Convert.ToString(Session["PWD_PREV_NOTREPEAT"]) == "1")
                {
                    Criteria = " 1=1 ";
                    Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + txtChangePwdUserID.Text.Trim() + "'";

                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")//&& OldPwd.Length > 20
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));
                        Criteria += " AND (HUM_USER_PASS='" + EncryPassword + "' OR HUM_PREV_PASS1='" + EncryPassword + "' OR HUM_PREV_PASS2='" + EncryPassword + "')";
                    }
                    else
                    {
                        Criteria += " AND (HUM_USER_PASS='" + txtNewPwd.Text.Trim() + "' OR HUM_PREV_PASS1='" + txtNewPwd.Text.Trim() + "' OR HUM_PREV_PASS2='" + txtNewPwd.Text.Trim() + "')";
                    }


                    DataSet DS = new DataSet();
                    DS = objCom.UserMasterGet(Criteria);
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblChangePwdStatus.Text = "Password should not match with previous two passwords";
                        lblChangePwdStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }

                }
                if (Convert.ToString(Session["PWD_FORMAT_REQUIRED"]) == "1")
                {

                    if (CheckPasswordFormat(txtNewPwd.Text) == false)
                    {
                        goto FunEnd;
                    }

                }




                if (txtOldPwd.Text.Trim() == "")
                {
                    lblChangePwdStatus.Text = "Please Enter the Old Password";
                    lblChangePwdStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                Criteria = " 1=1 ";

                EncryPassword = "";



                Criteria = " 1=1 ";

                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")///&& OldPwd.Length > 20
                {
                    EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtOldPwd.Text.Trim()));
                    Criteria += "  AND HUM_USER_ID ='" + txtChangePwdUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";

                }
                else
                {
                    Criteria += " AND  HUM_USER_ID ='" + txtChangePwdUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + txtOldPwd.Text.Trim() + "'";

                }


                ds = new DataSet();



                ds = objCom.UserMasterGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string FieldNameWithValues = "";

                    EncryPassword = "";
                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));

                        FieldNameWithValues = " HUM_PREV_PASS1 = HUM_PREV_PASS2 ,HUM_PREV_PASS2 = HUM_USER_PASS ";
                        FieldNameWithValues += ",HUM_USER_PASS ='" + EncryPassword + "',HUM_CREATED_DATE=getdate()";
                    }
                    else
                    {
                        FieldNameWithValues = " HUM_PREV_PASS1 = HUM_PREV_PASS2 ,HUM_PREV_PASS2 = HUM_USER_PASS ";
                        FieldNameWithValues += " ,HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "',HUM_CREATED_DATE=getdate()";
                    }

                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_USER_MASTER", Criteria);


                    lblChangePwdStatus.Text = "Password Changed Successfully";
                    lblChangePwdStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblChangePwdStatus.Text = "Your Old Password Is Incorrect";
                    lblChangePwdStatus.ForeColor = System.Drawing.Color.Red;
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }

        FunEnd: ;
        }


    }
}