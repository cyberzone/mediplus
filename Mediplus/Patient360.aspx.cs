﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mediplus_BAL;
using System.Data;
using System.IO;

namespace Mediplus
{
    public partial class Patient360 : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindVisitDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }
            if (txtFileNo.Text != "")
            {
                Criteria += " AND   HPV_PT_ID='" + txtFileNo.Text + "'";
            }



            //if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            //{
            //    Criteria += " AND HPV_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
            //}



            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= convert(date,getdate(),101)";


            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            //// DS = dbo.PatientVisitGet(Criteria);
            CommonBAL objCom = new CommonBAL();
            string strTopValue = "100";
            if (txtVisitMaxRow.Text.Trim() != "")
            {
                strTopValue = txtVisitMaxRow.Text.Trim();
            }


            DS = objCom.fnGetFieldValue(" TOP   " + strTopValue +
                       " CONVERT(VARCHAR,HPV_POLICY_EXP,103) as HPV_POLICY_EXPDesc,convert(varchar,HPV_Date,103) as HPV_DateDesc,CONVERT(VARCHAR(5),HPV_DATE,108) as HPV_DateTimeDesc" +
                       ",HPV_EMR_ID,HPV_SEQNO,HPV_PT_ID,HPV_PT_NAME,HPV_DR_ID,HPV_DR_NAME,HPV_DR_TOKEN,'' AS HPM_TOKEN_NO,HPV_VISIT_TYPE  ,(SELECT CASE ISNULL(HPV_INVOICE_ID,'')  WHEN '' then 'Not Invoiced' ELSE  'Invoiced' END) AS InvoiceStatus" +
                       ",(SELECT CASE  isnull(HPV_EMR_STATUS,'W') when  'W' then 'Waiting'   WHEN 'P' THEN 'In Process' WHEN 'Y' THEN 'Completed'   END) AS HPV_EMR_STATUSDesc , HPV_COMP_ID ,HPV_COMP_NAME,HPV_PT_TYPE " +
                       ",(SELECT CASE HPV_PT_TYPE WHEN 'CR' THEN 'Credit' when 'CA' THEN 'Cash'  ELSE HPV_PT_TYPE END ) AS HPV_PT_TYPEDesc", "HMS_PATIENT_VISIT", Criteria, "HPV_DATE DESC");



          //  DS = objCom.PatientVisitTopGet(Criteria, txtVisitMaxRow.Text.Trim());
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END
            lblNew.Text = "0";
            lblRevisit.Text = "0";
            lblOld.Text = "0";
            lblTotal.Text = "0";
            gvPTVisit.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPTVisit.Visible = true;
                gvPTVisit.DataSource = DV;
                gvPTVisit.DataBind();


                DataRow[] TempRow;
                TempRow = DS.Tables[0].Select(" HPV_VISIT_TYPE='New'");

                lblNew.Text = TempRow.Length.ToString();

                DataRow[] TempRow1;
                TempRow1 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Revisit'");

                lblRevisit.Text = TempRow1.Length.ToString();

                DataRow[] TempRow2;
                TempRow2 = DS.Tables[0].Select(" HPV_VISIT_TYPE='Old'");

                lblOld.Text = TempRow2.Length.ToString();


                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();
            }
            else
            {

                gvPTVisit.DataBind();
            }



        }

        void BindInvoiceDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
            }


            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= convert(date,getdate(),101)";

            if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS")
            {
                Criteria += " AND HIM_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
            }
            else if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND HIM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }


            if (txtFileNo.Text != "")
            {
                Criteria += " AND   HIM_PT_ID='" + txtFileNo.Text + "'";
            }


            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            lblInvGross.Text = "0.00";
            lblInvNet.Text = "0.00";
            lblInvPTShare.Text = "0.00";


            decimal decGrossAmt = 0, decNetAmt = 0, decPTShare = 0, decTGrossAmt = 0, decTNetAmt = 0, decTPTShare = 0;

            //  DS = dbo.InvoiceMasterGet(Criteria);
            CommonBAL objCom = new CommonBAL();
            string strTopValue = "100";
            if (txtInvoiceMaxRow.Text.Trim() != "")
            {
                strTopValue = txtInvoiceMaxRow.Text.Trim();
            }

            DS = objCom.fnGetFieldValue(" TOP   " + strTopValue +
                "HIM_INVOICE_TYPE,HIM_INVOICE_ID,CONVERT(VARCHAR,HIM_DATE,103)AS HIM_DATEDESC,CONVERT(VARCHAR(5),HIM_DATE,108) as HIM_DATETimeDesc" +
                ",HIM_PT_ID,HIM_PT_NAME,HIM_INVOICE_TYPE,CAST(HIM_GROSS_TOTAL AS DECIMAL(9,2)) AS HIM_GROSS_TOTAL,CAST(HIM_CLAIM_AMOUNT AS DECIMAL(9,2)) AS HIM_CLAIM_AMOUNT ,CAST(HIM_PT_AMOUNT AS DECIMAL(9,2)) AS HIM_PT_AMOUNT    ,HIM_PAYMENT_TYPE"
                , "HMS_INVOICE_MASTER", Criteria, "HIM_INVOICE_ID DESC");

          //  DS = objCom.InvoiceMasterTopGet(Criteria, txtInvoiceMaxRow.Text.Trim());


            gvInvoice.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvInvoice.Visible = true;
                gvInvoice.DataSource = DS;
                gvInvoice.DataBind();


                for (int i = 0; i < gvInvoice.Rows.Count; i++)
                {

                    Label lblgvInvGrossAmt = (Label)gvInvoice.Rows[i].FindControl("lblgvInvGrossAmt");
                    Label lblgvInvNetAmt = (Label)gvInvoice.Rows[i].FindControl("lblgvInvNetAmt");
                    Label lblgvInvPTShare = (Label)gvInvoice.Rows[i].FindControl("lblgvInvPTShare");


                    if (lblgvInvGrossAmt.Text != "" && lblgvInvGrossAmt.Text != null)
                    {
                        decGrossAmt = Convert.ToDecimal(lblgvInvGrossAmt.Text);
                    }

                    if (lblgvInvNetAmt.Text != "" && lblgvInvNetAmt.Text != null)
                    {
                        decNetAmt = Convert.ToDecimal(lblgvInvNetAmt.Text);
                    }

                    if (lblgvInvPTShare.Text != "" && lblgvInvPTShare.Text != null)
                    {
                        decPTShare = Convert.ToDecimal(lblgvInvPTShare.Text);
                    }


                    decTGrossAmt += decGrossAmt;
                    decTNetAmt += decNetAmt;
                    decTPTShare += decPTShare;


                }



            }
            else
            {
                gvInvoice.DataBind();

            }

            lblInvGross.Text = decTGrossAmt.ToString("N2");
            lblInvNet.Text = decTNetAmt.ToString("N2");
            lblInvPTShare.Text = decTPTShare.ToString("N2");


            BindInvoiceTotal();
        }

        void BindInvoiceTotal()
        {
            string Criteria = " 1=1 ";



            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtFileNo.Text != "")
            {
                Criteria += " AND   HIM_PT_ID='" + txtFileNo.Text + "'";
            }



            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= convert(date,getdate(),101)";




            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            lblInvGross.Text = "0.00";
            lblInvNet.Text = "0.00";
            lblInvPTShare.Text = "0.00";


            decimal decTGrossAmt = 0, decTNetAmt = 0, decTPTShare = 0;

            DS = dbo.InvoiceTotalGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {


                decTGrossAmt = 0;
                decTNetAmt = 0;
                decTPTShare = 0;


                if (DS.Tables[0].Rows[0].IsNull("GrossTotal") == false && Convert.ToString(DS.Tables[0].Rows[0]["GrossTotal"]) != "")
                {
                    decTGrossAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["GrossTotal"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("ClaimTotal") == false && Convert.ToString(DS.Tables[0].Rows[0]["ClaimTotal"]) != "")
                {
                    decTNetAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["ClaimTotal"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("PTShareTotal") == false && Convert.ToString(DS.Tables[0].Rows[0]["PTShareTotal"]) != "")
                {
                    decTPTShare = Convert.ToDecimal(DS.Tables[0].Rows[0]["PTShareTotal"]);
                }


            }

            lblInvGross.Text = decTGrossAmt.ToString("N2");
            lblInvNet.Text = decTNetAmt.ToString("N2");
            lblInvPTShare.Text = decTPTShare.ToString("N2");



        }

        void BindAppointmentDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) <= '" + strForToDate + "'";
            }


            if (txtFileNo.Text != "")
            {
                Criteria += " AND   HAM_FILENUMBER='" + txtFileNo.Text + "'";
            }


            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND HAM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }




            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) >= convert(date,getdate(),101) ";
            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAM_STARTTIME,101),101) <= convert(date,getdate(),101)";

            //if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            //{
            //    Criteria += " AND HAM_CREATEDUSER='" + Convert.ToString(Session["User_ID"]) + "'";
            //}



            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string strTopValue = "100";
            //if (txtAppointMaxRow.Text.Trim() != "")
            //{
            //    strTopValue = txtAppointMaxRow.Text.Trim();
            //}

            DS = objCom.fnGetFieldValue(" TOP   " + strTopValue +
                " CONVERT(varchar, HAM_STARTTIME,103) AS AppintmentDate,convert(varchar(5), HAM_STARTTIME,108) AS AppintmentSTTime,convert(varchar(5), HAM_FINISHTIME,108) AS AppintmentFITime " +
                " ,HAM_FILENUMBER,HAM_PT_NAME,HAM_DR_NAME,HAS_STATUS,HAM_CREATEDUSER "
                 , "HMS_APPOINTMENT_OUTLOOKSTYLE LEFT JOIN HMS_APPOINTMENTSTATUS ON HAM_STATUS=HAS_ID", Criteria, "HAM_SYSTEM_ID DESC");

           // DS = dbo.AppointmentOutlookGet(Criteria);

            gvAppointment.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAppointment.Visible = true;
                gvAppointment.DataSource = DS;
                gvAppointment.DataBind();

            }
            else
            {
                gvAppointment.DataBind();

            }





        }

        void BindEMRDtls()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) <= '" + strForToDate + "'";
            }
            if (txtFileNo.Text != "")
            {
                Criteria += " AND   EPM_PT_ID='" + txtFileNo.Text + "'";
            }


            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }

            //if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            //{
            //    Criteria += " AND HPV_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
            //}



            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= convert(date,getdate(),101) ";
            // Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= convert(date,getdate(),101)";


            EMR_BAL.EMR_PTMasterBAL obEMRPT = new EMR_BAL.EMR_PTMasterBAL();
            DataSet DS = new DataSet();
            DS = obEMRPT.GetEMR_PTMaster(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            gvEMRDtls.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvEMRDtls.Visible = true;
                gvEMRDtls.DataSource = DV;
                gvEMRDtls.DataBind();


            }
            else
            {

                gvEMRDtls.DataBind();
            }


        }


        void BindNursingOrder()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND EPC_BRANCH_ID	 = '" + Convert.ToString(Session["Branch_ID"]) + "'";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) <= '" + strForToDate + "'";
            }




            Criteria += " AND   EPM_PT_ID='" + txtFileNo.Text.Trim() + "'";


            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }

            EMR_BAL.EMR_PTMasterBAL obEMRPT = new EMR_BAL.EMR_PTMasterBAL();

            DS = obEMRPT.PTInstructionGet(Criteria);

            gvNursingOrder.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursingOrder.Visible = true;
                gvNursingOrder.DataSource = DS;
                gvNursingOrder.DataBind();


            }
            else
            {
                gvNursingOrder.DataBind();
            }


        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) <= '" + strForToDate + "'";
            }




            Criteria += " AND   EPM_PT_ID='" + txtFileNo.Text.Trim() + "'";


            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }


            DataSet DS = new DataSet();
            EMR_BAL.EMR_PTDiagnosis objDiag = new EMR_BAL.EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            gvDiagnosis.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();
                gvDiagnosis.Visible = true;

            }
            else
            {
                gvDiagnosis.DataBind();
            }
        FunEnd: ;
        }




        void BindPharmacy()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) <= '" + strForToDate + "'";
            }




            Criteria += " AND   EPM_PT_ID='" + txtFileNo.Text.Trim() + "'";


            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
            {
                Criteria += " AND EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }
            DataSet DS = new DataSet();
            EMR_BAL.EMR_PTPharmacy objPhy = new EMR_BAL.EMR_PTPharmacy();
            DS = objPhy.PharmacyGet(Criteria);
            gvPharmacy.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();
                gvPharmacy.Visible = true;
            }
            else
            {
                gvPharmacy.DataBind();
            }
        FunEnd: ;

        }

        void Clear()
        {
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

            txtFileNo.Text = "";
            gvPTVisit.Visible = false;
            gvPTVisit.DataBind();

            gvInvoice.Visible = false;
            gvInvoice.DataBind();

            gvAppointment.Visible = false;
            gvAppointment.DataBind();

            gvEMRDtls.Visible = false;
            gvEMRDtls.DataBind();

            gvNursingOrder.Visible = false;
            gvNursingOrder.DataBind();

            gvDiagnosis.Visible = false;
            gvDiagnosis.DataBind();

            gvPharmacy.Visible = false;
            gvPharmacy.DataBind();

            //frmVisitDtlhart.Src = "#";
            // frmInvoiceDtlhart.Src = "#";

            LabResult.FileNo = txtFileNo.Text.Trim();
            LabResult.BindLaboratoryResult();
            RadiologyResult.FileNo = txtFileNo.Text.Trim();
            RadiologyResult.BindRadiologyResult();

        }


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    if (Mediplus_BAL.GlobalValues.FileDescription == "KHALID_NUAIMI")
                    {
                        trRadLab.Visible = false;
                    }
                    if (txtFileNo.Text.Trim() != "")
                    {
                        BindVisitDtls();
                        BindInvoiceDtls();
                        BindAppointmentDtls();
                        BindEMRDtls();
                        BindNursingOrder();
                        BindDiagnosis();
                        BindPharmacy();

                        LabResult.FileNo = txtFileNo.Text.Trim();
                        LabResult.BindLaboratoryResult();
                        RadiologyResult.FileNo = txtFileNo.Text.Trim();
                        RadiologyResult.BindRadiologyResult();


                    }


                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Patient360.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");

            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");


            if (lblEMR_ID.Text != "" && lblEMR_ID.Text != null)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('EMR already Done','This visit already Done the EMR, So you can not edit.')", true);

                goto FunEnd;
            }
            else
            {
                Response.Redirect("HMS/Registration/Patient_Registration.aspx?PatientId=" + lblPatientId.Text.Trim() + "&HPVSeqNo=" + lblSeqNo.Text.Trim());
            }



        FunEnd: ;


        }

        protected void gvPTVisit_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindVisitDtls();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientVisit.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }



        }



        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                gvInvoice.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblInvoiceId, lblInvType;

                lblInvoiceId = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceId");
                lblInvType = (Label)gvScanCard.Cells[0].FindControl("lblInvType");




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Patient360.aspx_Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            if (txtFileNo.Text.Trim() != "")
            {
                BindVisitDtls();
                BindInvoiceDtls();
                BindAppointmentDtls();
                BindEMRDtls();
                BindNursingOrder();
                BindDiagnosis();
                BindPharmacy();

                LabResult.FileNo = txtFileNo.Text.Trim();
                LabResult.BindLaboratoryResult();
                RadiologyResult.FileNo = txtFileNo.Text.Trim();
                RadiologyResult.BindRadiologyResult();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter File No','Please enter the patient file No.')", true);

            }
        }

        protected void btnTodayDate_Click(object sender, EventArgs e)
        {
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

            if (txtFileNo.Text.Trim() != "")
            {
                BindVisitDtls();
                BindInvoiceDtls();
                BindAppointmentDtls();
                BindEMRDtls();
                BindNursingOrder();
                BindDiagnosis();
                BindPharmacy();

                LabResult.FileNo = txtFileNo.Text.Trim();
                LabResult.BindLaboratoryResult();
                RadiologyResult.FileNo = txtFileNo.Text.Trim();
                RadiologyResult.BindRadiologyResult();
            }


        }


        protected void btnClear_Click(object sender, EventArgs e)
        {

            Clear();

        }

        protected void btnPTVisitRef_Click(object sender, EventArgs e)
        {
            BindVisitDtls();
        }


        protected void btnInvoiceRef_Click(object sender, EventArgs e)
        {
            BindInvoiceDtls();
        }


        protected void btnAppointmentRef_Click(object sender, EventArgs e)
        {
            BindAppointmentDtls();
        }

        #endregion
    }
}