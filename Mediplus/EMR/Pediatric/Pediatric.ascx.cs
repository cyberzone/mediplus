﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.IO;
using System.Data;

namespace Mediplus.EMR.Pediatric
{
    public partial class Pediatric : System.Web.UI.UserControl
    {
        public String Gender = "MALE";
        public string Segment = "";

        public void BindPediatricGrid()
        {
            string Criteria = " 1=1 ";
            //  Criteria += " AND EGD_EIM_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

            Criteria += " AND EGD_EIM_ID  IN ( SELECT  EPM_ID FROM EMR_PT_MASTER WHERE EPM_PT_ID= '" + Convert.ToString(Session["EMR_PT_ID"]) + "')";

            // Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            EMR_Pediatric objPed = new EMR_Pediatric();
            DS = objPed.GrowthChartDataGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPediatric.DataSource = DS;
                gvPediatric.DataBind();
            }
            else
            {
                gvPediatric.DataBind();
            }
        }

        public void BindPediatric()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EGD_EIM_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";


            // Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            EMR_Pediatric objPed = new EMR_Pediatric();
            DS = objPed.GrowthChartDataGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtFromDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EGD_DATEDesc"]);
                age.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_AGE"]);
                weight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_WEIGHT"]);
                length.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_LENGTH"]);
                headcirc.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_HEADCIRC"]);
                stature.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_STATURE"]);
                bmi.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_BMI"]);
                comments.Value = Convert.ToString(DS.Tables[0].Rows[0]["EGD_COMMENTS"]);

            }

        }


        public void SavePediatric()
        {
            EMR_Pediatric objPed = new EMR_Pediatric();
            objPed.segment = Segment;
            objPed.date = txtFromDate.Text;
            objPed.age = age.Value;
            objPed.weight = weight.Value;
            objPed.length = length.Value;
            objPed.headcirc = headcirc.Value;
            objPed.stature = stature.Value;
            objPed.bmi = bmi.Value;
            objPed.comments = comments.Value;
            objPed.patientmasterid = Convert.ToString(Session["EMR_ID"]);
            objPed.WEMR_spI_SavePediatrics();
            Clear();
            BindPediatricGrid();


        }

        void Clear()
        {
            age.Value = "";
            weight.Value = "";
            length.Value = "";
            headcirc.Value = "";
            stature.Value = "";
            bmi.Value = "";
            comments.Value = "";
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Gender = Convert.ToString(Session["HPM_SEX"]).ToUpper() != null ? Convert.ToString(Session["HPM_SEX"]).ToUpper() : "MALE";
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");

                BindPediatric();
                BindPediatricGrid();

                age.Value = Convert.ToString(Session["HPV_AGEDesc"]);
            }
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblSegment = (Label)gvScanCard.Cells[0].FindControl("lblSegment");
            Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
            Label lblAge = (Label)gvScanCard.Cells[0].FindControl("lblAge");
            Label lblWeight = (Label)gvScanCard.Cells[0].FindControl("lblWeight");
            Label lblLength = (Label)gvScanCard.Cells[0].FindControl("lblLength");
            Label lblHeadcircumfrence = (Label)gvScanCard.Cells[0].FindControl("lblHeadcircumfrence");
            Label lblStature = (Label)gvScanCard.Cells[0].FindControl("lblStature");
            Label lblBMI = (Label)gvScanCard.Cells[0].FindControl("lblBMI");
            Label lblComment = (Label)gvScanCard.Cells[0].FindControl("lblComment");

            DropDownList drpSegment = (DropDownList)Parent.Parent.FindControl("drpSegment");


            drpSegment.SelectedValue = lblSegment.Text;
            txtFromDate.Text = lblDate.Text;
            age.Value = lblAge.Text;
            weight.Value = lblWeight.Text;
            length.Value = lblLength.Text;
            headcirc.Value = lblHeadcircumfrence.Text;
            stature.Value = lblStature.Text;
            bmi.Value = lblBMI.Text;
            comments.Value = lblComment.Text;

        }


        protected void Delete_Click(object sender, EventArgs e)
        {
            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblSegment = (Label)gvScanCard.Cells[0].FindControl("lblSegment");
            Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");



            CommonBAL objCom = new CommonBAL();
            string Criteria = " EGD_SEGMENT ='" + lblSegment.Text + "' and EGD_EIM_ID='" + Convert.ToString(Session["EMR_ID"]) + "' AND CONVERT(DATETIME,EGD_DATE,103)= CONVERT(DATETIME,'" + lblDate.Text + "',103)";

            objCom.fnDeleteTableData("EMR_GROWTHCHART_DATA", Criteria);
            Clear();
            BindPediatricGrid();

        }

    }
}