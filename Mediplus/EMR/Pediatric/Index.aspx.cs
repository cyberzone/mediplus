﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.Pediatric
{
    public partial class Index : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void GetMenus()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = "PEDIATRICS_OPTIONS";
            objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);

            DS = objCom.GetMenus();

            drpSegment.DataSource = DS;
            drpSegment.DataTextField = "MenuAction";
            drpSegment.DataValueField = "MenuAction";
            drpSegment.DataBind();


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }

                GetMenus();
                if (drpSegment.Items.Count > 0)
                {
                    pnlPeriodonticChart.SegmentType = drpSegment.SelectedValue;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                pnlPediatric.Segment = drpSegment.SelectedValue;
                pnlPediatric.SavePediatric();
                pnlVaccinationChart.SaveVaccinationChart();



                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Pediatric.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pnlPeriodonticChart.SegmentType = drpSegment.SelectedValue;
                pnlPeriodonticChart.BindPediatricImage();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Pediatric.drpSegment_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}