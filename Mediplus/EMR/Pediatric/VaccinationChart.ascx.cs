﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Pediatric
{
    public partial class VaccinationChart : System.Web.UI.UserControl
    {
        EMR_Pediatric objPed = new EMR_Pediatric();
        CommonBAL objCom = new CommonBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND SEGM_STATUS=1 ";
            Criteria += " and SEGM_TYPE='" + SegmentType + "'";



            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "EMR_VACCINATION_SEGMENT_MASTER", Criteria, "SEGM_ORDER");



            return DS;

        }

        void BindVaccinationGrid()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster("VACCINATION_CHART");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvVaccinationChart.DataSource = DS;
                gvVaccinationChart.DataBind();
            }

        }

        void BindSegmentDataDtls(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_VACCINATION"] != "" && ViewState["EMR_VACCINATION"] != null)
            {

                DS = (DataSet)ViewState["EMR_VACCINATION"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["SEGD_VALUE"]);
                    strValue = Convert.ToString(DR["SEGD_COMMENT"]);
                }
            }


        }

        DataSet GetSegmentDataDtls()
        {
            string Criteria = " 1=1   and SEGD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            objCom = new CommonBAL();

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "EMR_VACCINATION_SEGMENT_DTLS", Criteria, "SEGD_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_VACCINATION"] = DS;
            }

            return DS;

        }

        void SaveSegmentDtls()
        {
            string Criteria = " 1=1   AND SEGD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            objCom.fnDeleteTableData("EMR_VACCINATION_SEGMENT_DTLS", Criteria);



            for (Int32 i = 0; i < gvVaccinationChart.Rows.Count; i++)
            {
                Label lblLevelType = (Label)gvVaccinationChart.Rows[i].FindControl("lblLevelType");
                Label lblFieldID = (Label)gvVaccinationChart.Rows[i].FindControl("lblFieldID");
                Label lblDescription = (Label)gvVaccinationChart.Rows[i].FindControl("lblDescription");

                for (Int32 j = 0; j < gvVaccinationChart.Columns.Count; j++)
                {
                    if (j != 0)
                    {
                        CheckBox chkBox = gvVaccinationChart.Rows[i].Cells[j].Controls[1] as CheckBox;

                        if (chkBox.Checked == true)
                        {
                            objPed = new EMR_Pediatric();
                            objPed.BranchID = Convert.ToString(Session["Branch_ID"]);
                            objPed.EMRID = Convert.ToString(Session["EMR_ID"]);
                            objPed.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                            objPed.SEGD_TYPE = lblLevelType.Text;
                            objPed.SEGD_SUBTYPE = chkBox.ToolTip;
                            objPed.SEGD_FIELD_ID = lblFieldID.Text;
                            objPed.SEGD_FIELD_NAME = lblDescription.Text;
                            objPed.SEGD_VALUE = "Y";
                            objPed.SEGD_COMMENT = "";
                            objPed.SEGD_ENTRY_FROM = "OP";
                            objPed.VaccinationDtlsAdd();
                        }
                    }
                }
            }
        }


        public void SaveVaccinationOtherDtls()
        {


            string Criteria = " 1=1   AND EVOD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            objCom.fnDeleteTableData("EMR_VACCINATION_OTHER_DTLS", Criteria);


            objPed = new EMR_Pediatric();
            objPed.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPed.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPed.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objPed.EVOD_TYPE = "VACCINATION_OTHER";
            objPed.EVOD_FIELD_ID = "1";
            objPed.EVOD_FIELD_NAME = "Typhoid";
            objPed.EVOD_DATE1 = txtTyphoidDate1.Text.Trim();
            objPed.EVOD_DATE2 = txtTyphoidDate2.Text.Trim();
            objPed.EVOD_DATE3 = txtTyphoidDate3.Text.Trim();
            objPed.EVOD_DATE4 = txtTyphoidDate4.Text.Trim();
            objPed.EVOD_DATE5 = txtTyphoidDate5.Text.Trim();
            objPed.EVOD_DATE6 = txtTyphoidDate6.Text.Trim();
            objPed.EVOD_DATE7 = txtTyphoidDate7.Text.Trim();
            objPed.VaccinationOtherDtlsAdd();

            objPed = new EMR_Pediatric();
            objPed.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPed.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPed.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objPed.EVOD_TYPE = "VACCINATION_OTHER";
            objPed.EVOD_FIELD_ID = "2";
            objPed.EVOD_FIELD_NAME = "PPD / Mantoux Test";
            objPed.EVOD_DATE1 = txtPPD_MantouxDate1.Text.Trim();
            objPed.EVOD_DATE2 = txtPPD_MantouxDate2.Text.Trim();
            objPed.EVOD_DATE3 = txtPPD_MantouxDate3.Text.Trim();
            objPed.EVOD_DATE4 = txtPPD_MantouxDate4.Text.Trim();
            objPed.EVOD_DATE5 = txtPPD_MantouxDate5.Text.Trim();
            objPed.EVOD_DATE6 = txtPPD_MantouxDate6.Text.Trim();
            objPed.EVOD_DATE7 = txtPPD_MantouxDate7.Text.Trim();
            objPed.VaccinationOtherDtlsAdd();





            objPed = new EMR_Pediatric();
            objPed.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPed.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPed.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objPed.EVOD_TYPE = "VACCINATION_OTHER";
            objPed.EVOD_FIELD_ID = "3";
            objPed.EVOD_FIELD_NAME = "HPV Series";
            objPed.EVOD_DATE1 = txtHPVSeriesDate1.Text.Trim();
            objPed.EVOD_DATE2 = txtHPVSeriesDate2.Text.Trim();
            objPed.EVOD_DATE3 = txtHPVSeriesDate3.Text.Trim();
            objPed.EVOD_DATE4 = txtHPVSeriesDate4.Text.Trim();
            objPed.EVOD_DATE5 = txtHPVSeriesDate5.Text.Trim();
            objPed.EVOD_DATE6 = txtHPVSeriesDate6.Text.Trim();
            objPed.EVOD_DATE7 = txtHPVSeriesDate7.Text.Trim();
            objPed.VaccinationOtherDtlsAdd();

            objPed = new EMR_Pediatric();
            objPed.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPed.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPed.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objPed.EVOD_TYPE = "VACCINATION_OTHER";
            objPed.EVOD_FIELD_ID = "4";
            objPed.EVOD_FIELD_NAME = "Flu Vaccine";
            objPed.EVOD_DATE1 = txtFluVaccineDate1.Text.Trim();
            objPed.EVOD_DATE2 = txtFluVaccineDate2.Text.Trim();
            objPed.EVOD_DATE3 = txtFluVaccineDate3.Text.Trim();
            objPed.EVOD_DATE4 = txtFluVaccineDate4.Text.Trim();
            objPed.EVOD_DATE5 = txtFluVaccineDate5.Text.Trim();
            objPed.EVOD_DATE6 = txtFluVaccineDate6.Text.Trim();
            objPed.EVOD_DATE7 = txtFluVaccineDate7.Text.Trim();
            objPed.VaccinationOtherDtlsAdd();

            objPed = new EMR_Pediatric();
            objPed.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPed.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPed.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objPed.EVOD_TYPE = "VACCINATION_OTHER";
            objPed.EVOD_FIELD_ID = "5";
            objPed.EVOD_FIELD_NAME = "Yellow Fever Vaccine";
            objPed.EVOD_DATE1 = txtYellowFeverVaccineDate1.Text.Trim();
            objPed.EVOD_DATE2 = txtYellowFeverVaccineDate2.Text.Trim();
            objPed.EVOD_DATE3 = txtYellowFeverVaccineDate3.Text.Trim();
            objPed.EVOD_DATE4 = txtYellowFeverVaccineDate4.Text.Trim();
            objPed.EVOD_DATE5 = txtYellowFeverVaccineDate5.Text.Trim();
            objPed.EVOD_DATE6 = txtYellowFeverVaccineDate6.Text.Trim();
            objPed.EVOD_DATE7 = txtYellowFeverVaccineDate7.Text.Trim();
            objPed.VaccinationOtherDtlsAdd();


        }


        void BindVaccinationOtherDtls()
        {
            objPed = new EMR_Pediatric();
            DataSet DS = new DataSet();
            string Criteria = " 1=1   AND EVOD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";


            DS = objPed.VaccinationOtherDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["EVOD_FIELD_ID"]) == "1")
                    {
                        txtTyphoidDate1.Text = Convert.ToString(DR["EVOD_DATE1Desc"]);
                        txtTyphoidDate2.Text = Convert.ToString(DR["EVOD_DATE2Desc"]);
                        txtTyphoidDate3.Text = Convert.ToString(DR["EVOD_DATE3Desc"]);
                        txtTyphoidDate4.Text = Convert.ToString(DR["EVOD_DATE4Desc"]);
                        txtTyphoidDate5.Text = Convert.ToString(DR["EVOD_DATE5Desc"]);
                        txtTyphoidDate6.Text = Convert.ToString(DR["EVOD_DATE6Desc"]);
                        txtTyphoidDate7.Text = Convert.ToString(DR["EVOD_DATE7Desc"]);

                    }

                    if (Convert.ToString(DR["EVOD_FIELD_ID"]) == "2")
                    {
                        txtPPD_MantouxDate1.Text = Convert.ToString(DR["EVOD_DATE1Desc"]);
                        txtPPD_MantouxDate2.Text = Convert.ToString(DR["EVOD_DATE2Desc"]);
                        txtPPD_MantouxDate3.Text = Convert.ToString(DR["EVOD_DATE3Desc"]);
                        txtPPD_MantouxDate4.Text = Convert.ToString(DR["EVOD_DATE4Desc"]);
                        txtPPD_MantouxDate5.Text = Convert.ToString(DR["EVOD_DATE5Desc"]);
                        txtPPD_MantouxDate6.Text = Convert.ToString(DR["EVOD_DATE6Desc"]);
                        txtPPD_MantouxDate7.Text = Convert.ToString(DR["EVOD_DATE7Desc"]);

                    }

                    if (Convert.ToString(DR["EVOD_FIELD_ID"]) == "3")
                    {
                        txtHPVSeriesDate1.Text = Convert.ToString(DR["EVOD_DATE1Desc"]);
                        txtHPVSeriesDate2.Text = Convert.ToString(DR["EVOD_DATE2Desc"]);
                        txtHPVSeriesDate3.Text = Convert.ToString(DR["EVOD_DATE3Desc"]);
                        txtHPVSeriesDate4.Text = Convert.ToString(DR["EVOD_DATE4Desc"]);
                        txtHPVSeriesDate5.Text = Convert.ToString(DR["EVOD_DATE5Desc"]);
                        txtHPVSeriesDate6.Text = Convert.ToString(DR["EVOD_DATE6Desc"]);
                        txtHPVSeriesDate7.Text = Convert.ToString(DR["EVOD_DATE7Desc"]);

                    }

                    if (Convert.ToString(DR["EVOD_FIELD_ID"]) == "4")
                    {
                        txtFluVaccineDate1.Text = Convert.ToString(DR["EVOD_DATE1Desc"]);
                        txtFluVaccineDate2.Text = Convert.ToString(DR["EVOD_DATE2Desc"]);
                        txtFluVaccineDate3.Text = Convert.ToString(DR["EVOD_DATE3Desc"]);
                        txtFluVaccineDate4.Text = Convert.ToString(DR["EVOD_DATE4Desc"]);
                        txtFluVaccineDate5.Text = Convert.ToString(DR["EVOD_DATE5Desc"]);
                        txtFluVaccineDate6.Text = Convert.ToString(DR["EVOD_DATE6Desc"]);
                        txtFluVaccineDate7.Text = Convert.ToString(DR["EVOD_DATE7Desc"]);

                    }

                    if (Convert.ToString(DR["EVOD_FIELD_ID"]) == "5")
                    {
                        txtYellowFeverVaccineDate1.Text = Convert.ToString(DR["EVOD_DATE1Desc"]);
                        txtYellowFeverVaccineDate2.Text = Convert.ToString(DR["EVOD_DATE2Desc"]);
                        txtYellowFeverVaccineDate3.Text = Convert.ToString(DR["EVOD_DATE3Desc"]);
                        txtYellowFeverVaccineDate4.Text = Convert.ToString(DR["EVOD_DATE4Desc"]);
                        txtYellowFeverVaccineDate5.Text = Convert.ToString(DR["EVOD_DATE5Desc"]);
                        txtYellowFeverVaccineDate6.Text = Convert.ToString(DR["EVOD_DATE6Desc"]);
                        txtYellowFeverVaccineDate7.Text = Convert.ToString(DR["EVOD_DATE7Desc"]);

                    }

                }
            }



        }


        public void SaveVaccinationChart()
        {
            SaveSegmentDtls();
            SaveVaccinationOtherDtls();
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                if (!IsPostBack)
                {
                    GetSegmentDataDtls();
                    BindVaccinationGrid();
                    BindVaccinationOtherDtls();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Ultrasound.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvOPDAssessment_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                Label lblDescription = (Label)e.Row.FindControl("lblDescription");

                for (Int32 j = 0; j < gvVaccinationChart.Columns.Count; j++)
                {
                    if (j != 0)
                    {
                        CheckBox chkBox = e.Row.Cells[j].Controls[1] as CheckBox;


                        string Criteria = " 1=1 ";
                        Criteria += " AND SEGD_TYPE ='" + lblLevelType.Text + "' AND SEGD_SUBTYPE='" + chkBox.ToolTip + "' AND SEGD_FIELD_ID='" + lblFieldID.Text + "'";
                        string strValue, strValueYes;

                        BindSegmentDataDtls(Criteria, out strValue, out strValueYes);
                        // radPTAnswer.SelectedValue = strValueYes;
                        //txtDtls.Text = strValue;

                        if (strValueYes == "Y")
                        {
                            chkBox.Checked = true;

                        }


                    }
                }

            }


        }
    }
}