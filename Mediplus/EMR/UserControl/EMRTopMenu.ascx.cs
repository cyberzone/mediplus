﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;

namespace Mediplus.EMR.UserControl
{
    public partial class EMRTopMenu : System.Web.UI.UserControl
    {
        public Boolean  LoadEMRData { set; get; }

        CommonBAL objCom = new CommonBAL();
        public string Allergy = "", SocialHistory = "", PastHistory = "";

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindMenus()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = "PRINT_MENU";
            objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);

            DS = objCom.GetMenus();
            selReports.DataSource = DS;
            selReports.DataTextField = "MenuName";
            selReports.DataValueField = "MenuAction";
            selReports.DataBind();

        }

        void BindLaboratoryResult()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.LaboratoryResultGet(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_PT_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TransNo = "", RptType = "";

                TransNo = Convert.ToString(DS.Tables[0].Rows[0]["TransNo"]);
                RptType = Convert.ToString(DS.Tables[0].Rows[0]["ReportType"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "LabResultReport('" + TransNo + "','" + RptType + "');", true);

            }
        }

        void BindRadiologyResult()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND RTR_PATIENT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.RadiologyResultGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TransNo = "";
                TransNo = Convert.ToString(DS.Tables[0].Rows[0]["TransNo"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Radio Report", "RadResultReport('" + TransNo + "');", true);

            }

        }

        string fnGetClaimFormName()
        {
            string mCompId = "", Dept = "";
            mCompId = Convert.ToString(Session["HPV_COMP_ID"]);
            Dept = Convert.ToString(Session["HPV_DEP_ID"]);
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = objCom.GetCompanyMaster(Criteria);

            string RptName;
            RptName = "HMSInsCardPrint.rpt";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "HMSInsCardPrint.rpt";
                }
                else
                {
                    RptName = (Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) + ".rpt");
                }

                //if (Dept.ToUpper() == "DENTAL" && chkCompletionRpt.Checked == true)
                //{
                //    if (File.Exists(strReportPath + "Completion" + Dept + RptName) == true)
                //    {
                //        return "Completion" + Dept + RptName;
                //    }
                //    else
                //    {
                //        return RptName;
                //    }
                //}
                if (Dept.ToUpper() == "DENTAL")
                {
                    if (File.Exists(GlobalValues.REPORT_PATH + Dept + RptName) == true)
                    {
                        return Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }

                //if (chkCompletionRpt.Checked == true)
                //{
                //    if (File.Exists(strReportPath + "Completion" + RptName) == true)
                //    {
                //        return "Completion" + RptName;
                //    }
                //    else
                //    {
                //        return RptName;
                //    }
                //}

                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        string fnGetEMRClaimFormName()
        {
            string mCompId = "", Dept = "";
            mCompId = Convert.ToString(Session["HPV_COMP_ID"]);
            Dept = Convert.ToString(Session["HPV_DEP_NAME"]);
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = objCom.GetCompanyMaster(Criteria);

            string RptName = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "EMR_DAMAN.rpt";

                }
                else
                {
                    RptName = ("EMR_" + DS.Tables[0].Rows[0]["HCM_FIELD2"].ToString() + ".rpt");
                }

                if (Dept.ToUpper() == "DENTAL" || Dept.ToUpper() == "DENT")
                {
                    RptName = (DS.Tables[0].Rows[0]["HCM_FIELD2"].ToString() + ".rpt");
                    if (File.Exists(@GlobalValues.REPORT_PATH + "EMR_" + Dept + RptName))
                    {
                        return "EMR_" + Dept + RptName;
                    }
                    else
                    {
                        return "EMR_" + RptName;
                    }
                }
                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        string fnGetEMRAuthorizationFormName()
        {
            string mCompId = "", Dept = "";
            mCompId = Convert.ToString(Session["HPV_COMP_ID"]);
            Dept = Convert.ToString(Session["HPV_DEP_ID"]);
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = objCom.GetCompanyMaster(Criteria);

            string RptName = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "EMR_DAMANAUTHORIZATIONREQUEST.rpt";

                }
                else
                {
                    RptName = ("EMR_" + DS.Tables[0].Rows[0]["HCM_FIELD2"].ToString() + "AUTHORIZATIONREQUEST.rpt");
                }

                if (Dept.ToUpper() == "DENTAL")
                {
                    RptName = (DS.Tables[0].Rows[0]["HCM_FIELD2"].ToString() + "AUTHORIZATIONREQUEST.rpt");
                    if (File.Exists(Server.MapPath("EMR_" + Dept + RptName)))
                    {
                        return "EMR_" + Dept + RptName;
                    }
                    else
                    {
                        return "EMR_" + RptName;
                    }
                }
                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        void CheckReferralType()
        {
            ViewState["ReferalType"] = "IntRef";
            EMR_PTReferral objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " and EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID=" + Convert.ToString(Session["EMR_ID"]);
            DS = objRef.EMRPTReferenceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(DS.Tables[0].Rows[0]["EPR_TYPE"]) != null && Convert.ToString(DS.Tables[0].Rows[0]["EPR_TYPE"]).Equals("2"))
                {
                    ViewState["ReferalType"] = "ExtRef";

                }
                else
                {
                    ViewState["ReferalType"] = "IntRef";

                }
            }


        }

        void ChekcProcessCompleted()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";
            //Criteria += " AND EPM_STATUS = 'Y' ";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EPM_STATUS"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]);
                ViewState["EPM_VIP"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]);
                ViewState["EPM_ROYAL"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]);
                ViewState["EPM_START_DATE"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);

            }


        }


        public void BindEMR()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "' AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                //  lblEMRDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["EPM_DATEDesc"]);




                lblEMR_ID.Text = Convert.ToString(ds.Tables[0].Rows[0]["EPM_ID"]);

                string RefDrID = "";

                if (ds.Tables[0].Rows[0].IsNull("EPM_PT_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PT_ID"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPTID.Text = str;
                    lblPTID.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PT_ID"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_REF_DR_CODE") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_REF_DR_CODE"]) != "")
                {
                    RefDrID = Convert.ToString(ds.Tables[0].Rows[0]["EPM_REF_DR_CODE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_STATUS") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]) != "")
                {


                    if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]).ToUpper() == "Y")
                    {
                        chkProcComp.Checked = true;
                    }
                    else
                    {
                        chkProcComp.Checked = false;
                    }
                }

                //if (ds.Tables[0].Rows[0].IsNull("EPM_VIP") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]) != "")
                //{
                //    if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]).ToUpper() == "1")
                //    {
                //        chkVIP.Checked = true;
                //    }
                //    else
                //    {
                //        chkVIP.Checked = false;
                //    }
                //}

                //if (ds.Tables[0].Rows[0].IsNull("EPM_ROYAL") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]) != "")
                //{
                //    if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]).ToUpper() == "1")
                //    {
                //        chkRoyal.Checked = true;
                //    }
                //    else
                //    {
                //        chkRoyal.Checked = false;
                //    }
                //}


                if (ds.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblBeginDt.Text = str;
                    lblBeginDt.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["EPM_END_DATEDesc"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblEndDt.Text = str;
                    lblEndDt.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["EPM_END_DATEDesc"]);
                }



                BindPTDetails();
                BindPatientPhoto();

                //if (RefDrID != "")
                //{
                //    string RefDrName = "";
                //    RefDrName = BindRefDrData(RefDrID);

                //    if (RefDrName != "")
                //    {
                //        string str = RefDrName;
                //        if (str.Length > 15)
                //        {
                //            str = str.Substring(0, 15);
                //        }
                //        lblRefBy.Text = str;
                //        lblRefBy.ToolTip = RefDrName;
                //    }

                //}
            }
            else
            {
                //Clear();
            }

        }

        void BindPTVisitDetails()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND HPV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPV_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND HPV_EMR_ID = '" + Convert.ToString(Session["EMR_ID"]) + "' ";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.PatientVisitGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPV_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]) != "")
                {

                    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblProviderName.Text = str;
                    lblProviderName.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HPV_VISIT_TYPEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPEDesc"]) != "")
                {

                    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPEDesc"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPatientType.Text = str;
                    lblPatientType.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPEDesc"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("AgeDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["AgeDesc"]) != "")
                {
                    string str = Convert.ToString(DS.Tables[0].Rows[0]["AgeDesc"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblAge.Text = str;
                    lblAge.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["AgeDesc"]);
                }



                //if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                //{
                //    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblPolicyType.Text = str;
                //    lblPolicyType.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //}
            }



        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                if (ds.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPTName.Text = str;
                    lblPTName.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);

                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblNationality.Text = str;
                    lblNationality.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                }


                //if (ds.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                //{

                //    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblProviderName.Text = str;
                //    lblProviderName.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //}

                if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }

                   
                    

                    lblSex.Text = str;
                    lblSex.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblMobile.Text = str;
                    lblMobile.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                }
                /*
                if (ds.Tables[0].Rows[0].IsNull("HPM_AGE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblAge.Text = str;
                    lblAge.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                }

                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                 
                 */
                lblDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);


                if (ds.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblEmiratesID.Text = str;
                    lblEmiratesID.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);

                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                    //if (str.Length > 15)
                    //{
                    //    str = str.Substring(0, 15);
                    //}
                    lblPolicyNo.Text = str;
                    lblPolicyNo.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                    Session["HPM_POLICY_NO"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                }

                //if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                //{
                //    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblPolicyType.Text = str;
                //    lblPolicyType.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //}

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE1") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblLanguage.Text = str;
                    lblLanguage.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_FAX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblPassportNo.Text = str;
                    lblPassportNo.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]);
                }



                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblPTCompany.Text = str;
                    lblPTCompany.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                }




                if (ds.Tables[0].Rows[0].IsNull("HPM_REF_DR_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_NAME"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblRefBy.Text = str;

                    lblRefBy.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                }




            }

        }

        void BindPatientPhoto()
        {
            imgFront.ImageUrl = "EMR/DisplayCardImage.aspx";

            //objCom = new CommonBAL();
            //string Criteria = " 1=1 ";
            //Criteria += " AND HPP_BRANCH_ID='" + Session["Branch_ID"] + "' AND  HPP_PT_ID='" + lblPTID.Text + "'";
            //DataSet DS = new DataSet();
            //DS = objCom.PatientPhotoGet(Criteria);


            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
            //    {
            //        imgFront.Visible = true;
            //        Session["HTI_IMAGE1"] = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
            //        imgFront.ImageUrl = "~/DisplayCardImage.aspx";
            //    }

            //}



        }

        public void BindVital()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                if (Convert.ToString(Session["HPM_SEX"]).ToUpper() == "FEMALE")
                {
                    lblPregnant.Visible = true;

                    // LMPDate = Convert.ToString(DS.Tables[0].Rows[0]["EPV_LMP_DATE"]);
                    if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "1")
                    {
                        lblPregnant.Text = "Pregnant";
                    }
                    else
                    {
                        lblPregnant.Text = "";
                    }

                }

            }


        }

        void Clear()
        {

            lblPTID.Text = "";
            lblEMR_ID.Text = "";
            lblRefBy.Text = "";
            lblBeginDt.Text = "";
            lblEndDt.Text = "";

            lblPTName.Text = "";

            lblNationality.Text = "";

            lblProviderName.Text = "";
            lblSex.Text = "";
            lblMobile.Text = "";
            lblAge.Text = "";

            lblEmiratesID.Text = "";
            lblPolicyNo.Text = "";
           // lblPolicyType.Text = "";
            imgFront.Visible = false;


        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            if (!IsPostBack)
            {
               
                try
                {
                    if (LoadEMRData == false)
                    {
                        goto FunEnd;
                    }

                    BindMenus();

                    if (Convert.ToString(Session["HPV_INS_VERIFY_STATUS"]) == "Completed")
                    {
                        chkInsVerified.Checked = true;
                    }

                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
                    {
                        chkInsVerified.Visible = true;
                    }

                    if (GlobalValues.FileDescription == "MAMPILLY" && Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {
                        selReports.Visible = false;
                        btnPrint.Visible = false;
                    }

                    if (GlobalValues.FileDescription == "ALANAMEL")
                    {
                        spnStatus.Visible = true;
                        drpEMRClinicStatus.Visible = true;
                    }

                    if (Convert.ToString(Session["HPV_EMR_CLINIC_STATUS"]) != "" && Convert.ToString(Session["HPV_EMR_CLINIC_STATUS"]) != null)
                    {
                        drpEMRClinicStatus.SelectedValue = Convert.ToString(Session["HPV_EMR_CLINIC_STATUS"]);
                    }
                    //////if (Convert.ToString(Session["EMR_ALLERGY_DISPLAY"]) == "1")
                    //////{
                    //////    lnlAllergy.Visible = true;

                    //////}


                    divInsImage.Visible = true;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "openTopNavEMR", "", true);
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "openTopNavEMR()", true);

                    BindEMR();
                    BindPTVisitDetails();
                    BindVital();
                    if (Convert.ToString(Session["EMR_ALLERGY_DISPLAY"]) == "1")
                    {
                        trAlergy.Visible = true;
                        lblAllergy.Text = Convert.ToString(Session["PT_ALLERGY"]);

                    }

                    if (Convert.ToString(Session["EMR_HIST_SOCIAL_DISPLAY"]) == "1")
                    {
                        trSocialHist.Visible = true;
                        lblSocialHistory.Text  = Convert.ToString(Session["PT_HIST_SOCIAL"]);

                    }

                    if (Convert.ToString(Session["EMR_HIST_PAST_DISPLAY"]) == "1")
                    {
                        trPastHist.Visible = true;
                        lblPastHistory.Text  = Convert.ToString(Session["PT_HIST_PAST"]);

                    }

                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {
                        // trAlergy.Visible = true;
                        // Allergy = Convert.ToString(Session["PT_ALLERGY"]);
                        lblhdStartDate.Visible = false;
                        lblBeginDt.Visible = false;

                        lblhdEndDate.Visible = false;
                        lblEndDt.Visible = false;


                        lblhdDOB.Visible = false;
                        lblDOB.Visible = false;

                        lblhdEmiratesID.Visible = false;
                        lblEmiratesID.Visible = false;



                        lblhdRefBy.Visible = false;
                        lblRefBy.Visible = false;

                    }
                    if (GlobalValues.FileDescription.ToUpper() == "REENABEEGUM")
                    {
                        trExtraDtls.Visible = true;



                    }

                FunEnd: ;
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                string strControl = selReports.Value;// hidPrintClicked.Value;// gvdrpReports.SelectedValue;

                string strRptPath = "";
                if (strControl.ToUpper() == "CLINICALSUMMARY" || strControl.ToUpper() == "DENTALSUMMARY")
                {
                    strRptPath = "../WebReports/ClinicalSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }
                else if (strControl.ToUpper() == "CLINICALSUMMARYALL")
                {
                    strRptPath = "../WebReports/ClinicalSummaryAll.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }

                else if (strControl.ToUpper() == "PRESCRIPTION" || strControl.ToUpper() == "CRYSTALREPORT-PRESCRIPTION")
                {
                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC" || GlobalValues.FileDescription.ToUpper() == "REENABEEGUM" || GlobalValues.FileDescription.ToUpper() == "AMWAJ")
                    {

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Prescription", "ShowPrescriptionPrintPDF('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);
                    }
                    else
                    {
                        strRptPath = "../WebReports/Prescription.aspx";
                        string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                    }
                }
                else if (strControl.ToUpper() == "CYCLOREFRACTION")
                {
                    strRptPath = "../WebReports/GlassPrescription.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "RADIOLOGY")
                {
                    strRptPath = "../WebReports/Radiology.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Rad", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "LABORATORY")
                {
                    strRptPath = "../WebReports/Laboratory.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Lab", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "DRWISEINCOME")
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "DRWISEINCOME", "ShowDrWiseIncome('" + Convert.ToString(Session["HPV_DR_ID"]) + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "');", true);

                }

                else if (strControl.ToUpper() == "WEBREPORTFORM-LABORATORY")
                {
                    BindLaboratoryResult();
                }
                else if (strControl.ToUpper() == "WEBREPORTFORM-RADIOLOGY")
                {
                    BindRadiologyResult();
                }

                else if (strControl.ToUpper() == "CLAIMFORM" || strControl.ToUpper() == "WEBCLAIMFORM")
                {

                    // if (GlobalValues.FileDescription.ToUpper() == "ALAMAL" || GlobalValues.FileDescription.ToUpper() == "MAMPILLY" || GlobalValues.FileDescription.ToUpper() == "ABUSHARIA")
                    // {

                    if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                    {
                        string strRptName = "";
                        strRptName = fnGetEMRClaimFormName();

                        if (File.Exists(GlobalValues.REPORT_PATH + strRptName) == false)
                        {
                            // lblStatus.Text = "File Not Found";
                            //lblStatus.ForeColor = System.Drawing.Color.Red;
                            TextFileWriting("File Not Found " + GlobalValues.REPORT_PATH + strRptName);
                            goto FunEnd;
                        }

                        // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowClaimPrintPDF('" + strRptName + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClaimForm", "ShowClaimPrintPDF('" + strRptName + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);
                    }
                    else
                    {
                        if (Convert.ToString(Session["HPV_COMP_ID"]) != "" && Convert.ToString(Session["HPV_COMP_ID"]) != null)
                        {
                            strRptPath = GlobalValues.HMS_Path + "/WebReport/WebReportLoader.aspx";
                            string rptcall = @strRptPath + "?PatientId=" + Convert.ToString(Session["EMR_PT_ID"]) + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=" + Convert.ToString(Session["EMR_ID"]) + "&DRId=" + Convert.ToString(Session["HPV_DR_ID"]) + "&DrDepName=" + Convert.ToString(Session["HPV_DEP_NAME"]) + "&Date=" + Convert.ToString(Session["EPM_DATE"]) + "&CompanyId=" + Convert.ToString(Session["HPV_COMP_ID"]);

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Lab", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);
                        }
                    }



                }
                else if (strControl.ToUpper() == "AUTHORIZATIONFORM".ToUpper())
                {


                    string strRptName = "";
                    strRptName = fnGetEMRAuthorizationFormName();

                    if (File.Exists(GlobalValues.REPORT_PATH + strRptName) == false)
                    {

                        TextFileWriting("File Not Found " + GlobalValues.REPORT_PATH + strRptName);
                        goto FunEnd;
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Authorization", "ShowAuthorizationPrintPDF('" + strRptName + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);




                }


                else if (strControl.ToUpper() == "REFERRAL")
                {
                    CheckReferralType();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Referral", "ShowReferral('" + Convert.ToString(ViewState["ReferalType"]) + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);


                }

                else if (strControl.ToUpper() == "CLINICALNOTES")
                {
                    strRptPath = "../WebReports/ClinicalNotes.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


                }

                else if (strControl.ToUpper() == "CLINICALNOTESALL")
                {
                    strRptPath = "../WebReports/ClinicalNotesAll.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


                }
                else if (strControl.ToUpper() == "TIMEOUTPROCEDURE".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/TimeOutProcedure.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "FALLPREVENTIONASSESSMENT".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/FallPreventionAsses.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "DENTALCASEHISTORY".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/DentalCaseHistory.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "EDUCATIONALFORMALL".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/EducationalFormAll.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "PAINASSESSMENT".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/PainAssessment.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "REASSESSMENTSHEET".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/ReassessmentSheet.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "OUTPATIENTSUMMARY".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/OutPatientSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "PATIENTTRANSFERFORM".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/PatientTransferForm.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "INITIALOPASSESSMENT".ToUpper())
                {
                    strRptPath = "../WebReports/SHReports/InitialOPAssessment.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "AdmissionSummary".ToUpper())
                {
                    strRptPath = "../WebReports/InPatient/AdmissionSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]) + "&ADMISSION_NO=" + Convert.ToString(Session["IAS_ADMISSION_NO"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AdmissionSummary", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }

                else if (strControl.ToUpper() == "DENTALASSESSMENT".ToUpper())
                {
                    strRptPath = "../WebReports/DentalAssessment.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "DENTALEXAMINATION".ToUpper())
                {
                    strRptPath = "../WebReports/DentalExaminations.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "DENTALPROCEDURESHEET".ToUpper())
                {
                    strRptPath = "../WebReports/DentalProcedureSheet.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "PATIENTDISCHARGE".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PatientDischarge", "ShowPatientDischarge('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }

                else if (strControl.ToUpper() == "UltrasoundSingle".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UltrasoundSingle", "ShowUltrasound('UltrasoundSingle','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
                else if (strControl.ToUpper() == "UltrasoundTwins".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UltrasoundTwins", "ShowUltrasound('UltrasoundTwins','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
                else if (strControl.ToUpper() == "UltrasoundTriplets".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UltrasoundTriplets", "ShowUltrasound('UltrasoundTriplets','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
                else if (strControl.ToUpper() == "VISITSUMMAYALL" || strControl.ToUpper() == "VISITSUMMAYALL")
                {
                    strRptPath = "../WebReports/VisitDtlsSummaryAll.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "VisitDtlsSummaryAll", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }


                else if (strControl.ToUpper() == "PTBalanceHistory".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PTBalanceHistory", "ShowPTBalHistory('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }

                else if (strControl.ToUpper() == "OperationNotes".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OperationNotes", "ShowOperationNotes('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.btnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void imgHome_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDateddMMyyyy();
                strTime = objCom.fnGetDate("HH:mm:ss");

                ChekcProcessCompleted();

                if (Convert.ToString(ViewState["EPM_STATUS"]).ToUpper() != "Y" && hidCompleteProc.Value == "true")
                {
                    EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                    objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                    objPTMaster.EPM_CRITICAL_NOTES = "";
                    objPTMaster.ProcessCompleted = "Completed";
                    objPTMaster.EPM_VIP = Convert.ToString(ViewState["EPM_VIP"]);// chkVIP.Checked == true ? "1" : "0";
                    objPTMaster.EPM_ROYAL = Convert.ToString(ViewState["EPM_ROYAL"]);//chkRoyal.Checked == true ? "1" : "0";
                    objPTMaster.EPM_START_DATE = Convert.ToString(ViewState["EPM_START_DATE"]);// lblBeginDt.Text;

                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                    // objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                    objPTMaster.EPM_END_DATE = strDate + " " + strTime;


                    objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                    objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                    objPTMaster.AddPatientProcess();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.imgHome_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("~/EMR/Registration/PatientWaitingList.aspx");
        }

        protected void lnkHome_Click(object sender, EventArgs e)
        {
            try
            {
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDateddMMyyyy();
                strTime = objCom.fnGetDate("hh:mm:ss");

                ChekcProcessCompleted();

                if (Convert.ToString(ViewState["EPM_STATUS"]).ToUpper() != "Y" && hidCompleteProc.Value == "true")
                {
                    EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                    objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                    objPTMaster.EPM_CRITICAL_NOTES = "";
                    objPTMaster.ProcessCompleted = "Completed";
                    objPTMaster.EPM_VIP = Convert.ToString(ViewState["EPM_VIP"]);// chkVIP.Checked == true ? "1" : "0";
                    objPTMaster.EPM_ROYAL = Convert.ToString(ViewState["EPM_ROYAL"]);//chkRoyal.Checked == true ? "1" : "0";
                    objPTMaster.EPM_START_DATE = Convert.ToString(ViewState["EPM_START_DATE"]);// lblBeginDt.Text;

                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                    // objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                    objPTMaster.EPM_END_DATE = strDate + " " + strTime;
                    // TextFileWriting(Convert.ToString(ViewState["EPM_START_DATE"]));
                    // TextFileWriting(strDate + " " + strTime);

                    objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                    objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                    objPTMaster.AddPatientProcess();


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.lnkHome_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("~/Registration/PatientWaitingList.aspx");
        }

        protected void chkInsVerified_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "HPV_INS_VERIFY_STATUS='Pending'";
                Session["HPV_INS_VERIFY_STATUS"] = "Pending";
                if (chkInsVerified.Checked == true)
                {
                    FieldNameWithValues = " HPV_INS_VERIFY_STATUS='Completed' ";
                    Session["HPV_INS_VERIFY_STATUS"] = "Completed";
                }


                string Criteria = " HPV_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  HPV_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.chkInsVerified_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpEMRClinicStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                CommonBAL objCom = new CommonBAL();

                string FieldNameWithValues = " HPV_EMR_CLINIC_STATUS='" + drpEMRClinicStatus.SelectedValue + "'";

                string Criteria = " HPV_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  HPV_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria);

                Session["HPV_EMR_CLINIC_STATUS"] = drpEMRClinicStatus.SelectedValue;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.drpgvEMRClinicStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void chkProcComp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string strDate = "", strTime = "";
                strDate = objCom.fnGetDateddMMyyyy();
                strTime = objCom.fnGetDate("HH:mm:ss");

                //   strDate = Convert.ToString(Session["HPV_DATE"]);
                //   strTime = Convert.ToString(Session["HPV_TIME"]);

                EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                objPTMaster.EPM_CRITICAL_NOTES = "";
                objPTMaster.ProcessCompleted = chkProcComp.Checked == true ? "Completed" : "onProcess";
                objPTMaster.EPM_VIP = "0";
                objPTMaster.EPM_ROYAL = "0";

                objPTMaster.EPM_START_DATE = lblBeginDt.Text;
                objPTMaster.EPM_END_DATE = lblEndDt.Text;

                if (chkProcComp.Checked == true)
                {
                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                    //objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                    objPTMaster.EPM_END_DATE = strDate + " " + strTime;
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        chkProcComp.Enabled = false;
                    }
                }
                else
                {
                    objPTMaster.EPM_END_DATE = lblEndDt.Text.Trim();
                }

                objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                objPTMaster.AddPatientProcess();
                BindEMR();
                BindPTVisitDetails();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientHeader.chkProcComp_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}