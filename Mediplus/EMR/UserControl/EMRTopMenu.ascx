﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMRTopMenu.ascx.cs" Inherits="Mediplus.EMR.UserControl.EMRTopMenu" %>

    
 <link href="../../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">

    function LoadPage(strPage) {
        alert('test');
        window.top.frames[2].location.href = strPage;

        if (strPage == '../Default.aspx') {
            parent.location.href = strPage;
        }
    }

    function LabResultReport(TransNo, RptType) {
        var Report = "LabReport.rpt";


        if (RptType.toUpperCase() == 'U') {
            Report = "LabReportUsr.rpt";
        }
        var Criteria = " 1=1 ";

        if (TransNo != "") {

            Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    }

    function RadResultReport(TransNo) {

        var Report = "RadiologyReport.rpt";
        var Criteria = " 1=1 ";

        if (TransNo != "") {

            Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();


        }

    }

    function ShowClaimPrintPDF(Report, FileNo, BranchId, EPM_ID) {

        var win = '';
        /*
        if (Report == "EMR_DAMAN.rpt") {

            var Criteria = " 1=1 ";
            Criteria += ' AND {HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'';
            win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')

        }
        else {

            win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\' and  {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'and  {EMR_PT_MASTER.EPM_ID}=' + EPM_ID, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
        }
        */
        var Criteria = " 1=1 ";
        Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
        win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\' and  {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'and  {EMR_PT_MASTER.EPM_ID}=' + EPM_ID, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        win.focus();


    }

    function ShowAuthorizationPrintPDF(Report, FileNo, BranchId, EPM_ID) {


        var Criteria = " 1=1 ";
        Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
        win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\' and  {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'and  {EMR_PT_MASTER.EPM_ID}=' + EPM_ID, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        win.focus();


    }

    function ShowPrescriptionPrintPDF(FileNo, BranchId, EPM_ID) {

        var win = '';
        var Report = "Prescription.rpt"; //"EMRTestReport.rpt";


        var Criteria = " 1=1 ";
        Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
        win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


        win.focus();


    }

    function ShowPatientDischarge(FileNo, BranchId, EPM_ID) {

        var win = '';
        var Report = "EMR_PatientDischarge.rpt"; //"EMRTestReport.rpt";


        var Criteria = " 1=1 ";
        Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
        win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


        win.focus();


    }

    function ShowUltrasound(ReportType, FileNo, BranchId, EPM_ID) {

        var win = '';
        var Report = "UltrasoundSingle.rpt"; //"EMRTestReport.rpt";


        if (ReportType == 'UltrasoundSingle') {
            Report = "UltrasoundSingle.rpt";
        }
        else if (ReportType == 'UltrasoundTwins') {
            Report = "UltrasoundTwins.rpt";
        }
        else if (ReportType == 'UltrasoundTriplets') {
            Report = "UltrasoundTriplets.rpt";
        }


        var Criteria = " 1=1 ";
        Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
        win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


        win.focus();


    }

    function ShowReferral(ReferalType, FileNo, BranchId, EMRID) {
        var Report;
        if (ReferalType == "IntRef") {
            Report = "IntReferral.rpt";

        }
        else {
            Report = "ExtReferral.rpt";
        }

        var Criteria = " 1=1 ";
        Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
        Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

        //var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
        var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

        win.focus();

    }

    function ConfirmProcessComplete() {

        var vEPM_STATUS = '<%= Convert.ToString(Session["EPM_STATUS"]) %>'
        var vEMR_COMP_PROC_CONFIRM = '<%= Convert.ToString(Session["EMR_COMP_PROC_CONFIRM"]) %>'

        // alert(vEPM_STATUS);
        // alert(vEMR_COMP_PROC_CONFIRM);

        if (vEPM_STATUS != 'Y' && vEMR_COMP_PROC_CONFIRM != "0") {
            var isSrtProc = window.confirm('Do you want to complete the process? ');
            document.getElementById('<%=hidCompleteProc.ClientID%>').value = isSrtProc;
            }

            return true;

        }
        function ShowDrWiseIncome(DrId, PTID) {
            var Report = "";

            Report = "HmsStaffwiseIncome.rpt";

            var Criteria = " 1=1 ";

            if (DrId != "") {

                Criteria += ' AND {HMS_STAFF_WISE_INCOME.HIM_DR_CODE}=\'' + DrId + '\'';
            }

            if (PTID != "") {

                Criteria += ' AND {HMS_STAFF_WISE_INCOME.HIM_PT_ID}=\'' + PTID + '\'';
            }



            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }


        function ShowPTBalHistory(FileNo, BranchId, EMRID) {
            var Report = "";
            Report = "HmsPtBalHist.rpt";

            var Criteria = " 1=1 ";
            Criteria += ' AND {HMS_PATIENT_BALANCE_HISTORY.HPB_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {HMS_PATIENT_BALANCE_HISTORY.HPB_BRANCH_ID}=\'' + BranchId + '\'';
            // Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

            //var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

        function ShowOperationNotes(FileNo, BranchId, EMRID) {
            var Report = "";
            Report = "EMR_OperationNotes.rpt";

            var Criteria = " 1=1 ";
            Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

            //var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }


        function ConfirmLogOut() {


            var EMR_LOGOUT_CONFIRM = '<%= Convert.ToString(Session["EMR_LOGOUT_CONFIRM"]) %>'

                // alert(vEPM_STATUS);
                // alert(EMR_LOGOUT_CONFIRM);
                var isSrtProc = true;
                if (EMR_LOGOUT_CONFIRM != "0") {
                    isSrtProc = window.confirm('Are you sure, you want to logout?');

                }

                return isSrtProc;

            }


            function ShowAlergyMessage() {
                document.getElementById('divMessage').style.display = 'block';
            }

            function HideMessage() {

                document.getElementById('divMessage').style.display = 'none';
            }

</script>
<input type="hidden" id="hidCompleteProc" runat="server" />
 
<div>
      <table style="width: 80%;">
                        <tr>
                            <td style="width: 230px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Name:</span>
                                 <asp:Label ID="lblPTName" runat="server" Font-Bold="true" Style="font-size: 11px;color:#D64535;" CssClass="lblCaption1"></asp:Label>
                            </td>
                            <td style="width: 150px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Patient#:</span>
                                 <asp:Label ID="lblPTID" runat="server"   Font-Bold="true" Style="font-size: 11px;color:#D64535;" CssClass="lblCaption1"></asp:Label>
                            </td>
                             <td style="width: 150px;">
                               

                                 <span class="lblCaption1" style="width: 100px;font-weight:bold;">Visit Type:</span>
 
                                    <asp:Label ID="lblPatientType" runat="server" Font-Bold="true" Style="font-size: 11px;color:#D64535;"  CssClass="lblCaption1"></asp:Label>
                            </td>
                            <td style="width: 160px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Nationality:</span>
                                <asp:Label ID="lblNationality" runat="server"   CssClass="lblCaption1"></asp:Label>
                            </td>
                           <td style="width: 230px;">
                                  <asp:CheckBox ID="chkProcComp" runat="server" Width="120px" Height="20px" Text="Process Completed" Style="font-size: 11px;border:solid;border-radius:5px;border-width:thin;border-color:#D64535;background-color:#D64535;color:#fff;cursor:pointer;font-weight:bold;"    CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkProcComp_CheckedChanged" />

                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Company:</span>
                                 <asp:Label ID="lblProviderName" runat="server"  CssClass="lblCaption1"></asp:Label>
                            </td>
                            <td style="width: 150px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Sex:</span>
                                <asp:Label ID="lblSex" runat="server"   CssClass="lblCaption1"></asp:Label>
                               &nbsp;<asp:Label ID="lblPregnant" runat="server" ForeColor="#D64535" Font-Bold="true" style="color: #D64535 ;font-weight:bolder;font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style="width: 150px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Mobile:</span>
                                <asp:Label ID="lblMobile" runat="server"   CssClass="lblCaption1"></asp:Label>
                            </td>


                            <td style="width: 160px;">
                               <asp:Label ID="lblhdStartDate" runat="server"  Font-Bold="true"   CssClass="lblCaption1" Text="Start Date:"></asp:Label>

                                <asp:Label ID="lblBeginDt" runat="server"   CssClass="lblCaption1"></asp:Label>
                              

                            </td>
                             <td style="width: 230px;">
                                 <span class="lblCaption1" style="width: 100px;font-weight:bold;">Policy No:</span>
                                  <asp:Label ID="lblPolicyNo" runat="server"   CssClass="lblCaption1"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px;">
                                <asp:Label ID="lblhdRefBy" runat="server" Font-Bold="true"  CssClass="lblCaption1" Text="Ref By:"></asp:Label>
                                <asp:Label ID="lblRefBy" runat="server"   CssClass="lblCaption1"></asp:Label>
                            </td>
                            <td style="width: 150px;" class="lblCaption1">
                              <span class="lblCaption1" style="width: 100px;font-weight:bold;"> Age&nbsp;&nbsp;&nbsp;:</span>
                   
                       <asp:Label ID="lblAge" runat="server"   CssClass="lblCaption1"></asp:Label> <br />
                                  <asp:Label ID="lblhdDOB" runat="server"    CssClass="lblCaption1" Text="DOB&nbsp;&nbsp;:"></asp:Label>
                        <asp:Label ID="lblDOB" runat="server"   CssClass="lblCaption1"></asp:Label>
                            </td>
                             <td style="width: 150px;">
                                <asp:Label ID="lblhdEmiratesID" runat="server"  Font-Bold="true"  CssClass="lblCaption1" Text="EmiratesID&nbsp;:"></asp:Label>
                                 <asp:Label ID="lblEmiratesID" runat="server"  CssClass="lblCaption1"></asp:Label>
                            </td>
                            <td style="width: 160px;">
                                
                                <asp:Label ID="lblhdEndDate" runat="server"  Font-Bold="true"     CssClass="lblCaption1" Text="End Date:"></asp:Label>
                                <asp:Label ID="lblEndDt" runat="server"   CssClass="lblCaption1"></asp:Label>
                            </td>
                             <td style="width: 230px;">
                              

                                  <span class="lblCaption1" style="width: 100px;font-weight:bold;">MR.No:</span>
                                <asp:Label ID="lblEMR_ID" runat="server" CssClass="lblCaption1"></asp:Label>

                            </td>

                        </tr>
                          <tr id="trExtraDtls" runat="server" visible="false"  >
                            <td style="width: 230px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">Language:</span>
                                  <asp:Label ID="lblLanguage" runat="server"   CssClass="lblCaption1"  Visible="false"></asp:Label>
                            </td>
                               <td style="width: 150px;" class="lblCaption1">
                                    <span class="lblCaption1" style="width: 100px;font-weight:bold;">Passport No:</span>
                                        <asp:Label ID="lblPassportNo" runat="server"  CssClass="lblCaption1"  Visible="false"></asp:Label>

                               </td>
                               <td style="width: 150px;">
                                <span class="lblCaption1" style="width: 100px;font-weight:bold;">PT. Comp.:</span>
                                                                  <asp:Label ID="lblPTCompany" runat="server"  CssClass="lblCaption1"  Visible="false"></asp:Label>

                                   </td>
                                
                            </tr>
                    <tr id="trAlergy" runat="server" visible="false"  >
                            <td colspan="5"  style="font-weight:bold;"  class="lblCaption1">
                                Allergy  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:  
                                <asp:Label ID="lblAllergy" runat="server" style="color:#D64535;font-weight:bold;"  CssClass="lblCaption1"   ></asp:Label>
                                 
                            </td>
                           
                        </tr>
                        <tr id="trSocialHist" runat="server" visible="false"  >
                            <td colspan="5"   style="font-weight:bold;"   class="lblCaption1">
                                Social History  &nbsp; :  
                                 <asp:Label ID="lblSocialHistory" runat="server" style="color:#D64535;font-weight:bold;"  CssClass="lblCaption1"   ></asp:Label>
                               
                            </td>
                           
                        </tr>
                         <tr id="trPastHist" runat="server" visible="false"  >
                            <td colspan="5"  style="font-weight:bold;"   class="lblCaption1">
                                Past History  &nbsp; :  
                                <asp:Label ID="lblPastHistory" runat="server" style="color:#D64535;font-weight:bold;"  CssClass="lblCaption1"   ></asp:Label>
                                
                            </td>
                           
                        </tr>
                    </table>

                     
                <div id="divInsImage" runat="server" visible="false" class="imageRow" style="position: absolute; right: 10px; z-index: 1000; top: 50px;">
                    <div id="site-info" class="set">
                        <div class="single first">

                            <a href="Javascript:ShowInsuranceCardZoom();">
                                <asp:Image ID="imgFront" runat="server" Height="105px" Width="150px" />

                            </a>

                        </div>
                    </div>
                </div>

</div>
<div style="background-color:#666; height: 30px;width:100%"  >

    <div style="text-align: left; vertical-align: middle; padding-left: 30px;">

         
        <asp:ImageButton ID="imgHome" runat="server" ImageUrl="~/EMR/Images/home_menu.png" OnClick="imgHome_Click" Width="25px" Height="25px" OnClientClick="return ConfirmProcessComplete();" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <select id="selReports" runat="server" class="lblCaption1"></select>
        <asp:Button ID="btnPrint" runat="server" Style="width: 50px; border-radius: 5px 5px 5px 5px" CssClass="gray" Text="Print" OnClick="btnPrint_Click" />
        &nbsp;
        <asp:CheckBox ID="chkInsVerified" runat="server" Text="Insurance Verified" Style="color: #ffffff; text-decoration: none;" class="label" Checked="false" AutoPostBack="true" OnCheckedChanged="chkInsVerified_CheckedChanged" Visible="false" />
      &nbsp; &nbsp; <span id="spnStatus" runat="server"  class="lblCaption1"  style="color:white;" visible="false" >  Clinic Status</span>
        <asp:DropDownList ID="drpEMRClinicStatus" runat="server" CssClass="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpEMRClinicStatus_SelectedIndexChanged" Visible="false">
            <asp:ListItem Text="Waiting" Value="Waiting" Selected="True"></asp:ListItem>
            <asp:ListItem Text="With Doctor" Value="With Doctor"></asp:ListItem>
            <asp:ListItem Text="Waiting Lab" Value="Waiting Lab"></asp:ListItem>
            <asp:ListItem Text="PT Come Back" Value="PT Come Back"></asp:ListItem>
            <asp:ListItem Text="Investigation TO DO" Value="Investigation TO DO"></asp:ListItem>
            <asp:ListItem Text="Seen" Value="Seen"></asp:ListItem>
             <asp:ListItem Text="Surgery" Value="Surgery"></asp:ListItem>
            <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
            <asp:ListItem Text="Billed" Value="Billed"></asp:ListItem>
        </asp:DropDownList>


        <a id="lnlAllergy" runat="server" visible="false" href="javascript:ShowAlergyMessage()" style="color: #ffffff; text-decoration: none;" class="label">Allergy</a>

       
    </div>

</div>
     