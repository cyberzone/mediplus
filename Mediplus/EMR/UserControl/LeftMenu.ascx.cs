﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.UserControl
{
    public partial class LeftMenu : System.Web.UI.UserControl
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    papulateMenuTree();

                    //TreeNode RootNode = OutputDirectory();
                    //TreeNode treeNode = new TreeNode(Convert.ToString(Session["HPV_DEP_NAME"]));
                    //MyTree.Nodes.Add(RootNode);


                    //if (MyTree.Nodes.Count > 0)
                    //{
                    //    MyTree.Nodes[0].Expand();
                    //}
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       LeftMenu.Page_Load");
                TextFileWriting(ex.Message.ToString());

            }
        }

        void papulateMenuTree()
        {
            //string Criteria = " 1=1 ";
            //Criteria += " and DesigID IN (" + Session["DesigId"] + ") AND ( ParentId=" + Session["ParentId"] + " or M.menuid=" + Session["ParentId"] + " )";// + ViewState["EmpId"];


            //objRole = new clsRole(strCon);
            //DS = new DataSet();
            //DS = objRole.GetAllLeftMenu(Criteria);

            DataSet DS=new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria1 = " 1=1 AND  EM_MENU_STATUS=1 AND EM_MENU_TYPE='MENU' AND EM_MENU_FOR ='OP_EMR' ";
            Criteria1 += " AND (EM_DEPT_ID  LIKE'%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR EM_DEPT_ID='' OR EM_DEPT_ID IS NULL)";
            Criteria1 += " AND (EM_DEPT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["User_DeptID"]) + "|%' OR EM_DEPT_ID_HIDE='' OR EM_DEPT_ID_HIDE IS NULL )";


            Int32 i;
            
            //DataTable DT = new DataTable();
            //DT = RemoveDuplicateRows(DS.Tables[0], "MenuId");
             DS = objCom.fnGetFieldValue(" * ", "EMR_MENUS", Criteria1, "EM_MENU_ORDER");


            if (DS.Tables[0].Rows.Count > 0)
            {
                //DS.Relations.Add("NodeRelation1", DT.Columns["MenuId"], DT.Columns["ParentId"]);
               foreach (DataRow dbRow in DS.Tables[0].Rows )
                {
                   // DataRow dbRow = DT.Rows[i];
                    //if (DT.Rows[i]["MenuId"].ToString() != DT.Rows[i]["ParentId"].ToString())
                    //{
                    //@"../Images/Icons/"+ Convert.ToString(dbRow["EM_MENU_ICON"])
                    MenuItem L1 = new MenuItem(Convert.ToString(dbRow["EM_MENU_NAME"]), Convert.ToString(dbRow["EM_MENU_ID"]),"", Convert.ToString(dbRow["EM_MENU_URL"]), "_self");
                        Menu1.Items.Add(L1);
                    ///}
                    ///
                    

                }
            }
        }

        

       

    }
}