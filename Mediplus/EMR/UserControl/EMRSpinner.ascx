﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMRSpinner.ascx.cs" Inherits="Mediplus.EMR.UserControl.EMRSpinner" %>
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

<script  type="text/javascript">
    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else {
            return false;
        }
    }

    function Increase(vName) {

        var vData = document.getElementById(vName).value;
        if (vData != '') {
            var Result = '';
            Result = parseFloat(vData) + parseFloat(0.01);
            document.getElementById(vName).value = Result.toFixed(2)

        }
        else {
            document.getElementById(vName).value = 0.01;
        }



    }

    function Decrease(vName) {

        var vData = document.getElementById(vName).value;
        if (vData != '') {
            var Result = '';
            Result = parseFloat(vData) - parseFloat(0.01);
            document.getElementById(vName).value = Result.toFixed(2)

        }
        else {
            document.getElementById(vName).value = -0.1;
        }



    }
</script>




<table>
    <tr>
        <td style="text-align: right;">
            <input type="text"  id="txtSpinner"   onkeypress="return OnlyNumeric(event);" class="lblCaption1" style="width:50px;height:20px;" value="<%=SpinnerValue %>" />
        </td>
        <td style="text-align: left;">
            <img id="img1" src="../Images/sort_asc.png" onclick="Increase('txtSpinner');" /><br />
            <img id="img2" src="../Images/sort_desc.png" onclick="Decrease('txtSpinner');" />
        </td>
    </tr>
</table>
