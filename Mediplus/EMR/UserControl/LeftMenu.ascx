﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftMenu.ascx.cs" Inherits="Mediplus.EMR.UserControl.LeftMenu" %>
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<link href="../Styles/MenuStyle.css" rel="Stylesheet" type="text/css" />
<style type="text/css">
  
     .MenuStyle
    {
        background-color: #595959;
        color: #fff;
        font-size: 12px;
        font-family: Arial;
        border: none;
        height: 22px;
        width:100%;
        padding: 1px 15px;
    }
     
        

        .MenuStyle:hover
        {
            background-color: #D64535;
            color: #fff;
            height: 25px;
            width: 100%;
            vertical-align: central;
        }

</style>

<div style="padding: 0;  height: 800px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;background-color: #595959;">
    
            <asp:UpdatePanel ID="pnlMenu" runat="server" >
                <ContentTemplate>
          
                        <asp:Menu ID="Menu1" runat="server" Orientation="Vertical" Width="100%" Style="width: 100%; position: relative; float: left;"
                            StaticMenuItemStyle-CssClass="MenuStyle">
                        </asp:Menu>
                      </ContentTemplate>
            </asp:UpdatePanel>
  
</div>
