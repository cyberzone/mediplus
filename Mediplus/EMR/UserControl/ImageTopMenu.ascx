﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageTopMenu.ascx.cs" Inherits="Mediplus.EMR.UserControl.ImageTopMenu" %>
<div id="maindivcs" style="margin-left: 230px; padding-left: 5px;">

             <div id="myTopnav" class="Topnav">
              
            <ul class="ImageMenu" id="Ul1" style="margin-left: 0px;">
                <li class="clickChange" id="liHome" onclick="changeColor('liHome');">
                    <a id="aHome" onclick="BindFrameSrc('Home1.aspx','Home')">
                        <img class="icon" src="design/Images/home1.png" alt="" title="Home" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="li360" onclick="changeColor('li360');">
                    <a id="aPatient360" onclick="BindFrameSrc('Patient360.aspx','Patient360')">
                        <img class="icon" src="Images/360_icon.png" style="width: 40px; height: 40px;" alt="" title="360°">
                    </a>
                </li>
                <li class="defaultIconColor" id="liAppointment" onclick="changeColor('liAppointment');">
                    <a id="aAppointmentLink" onclick="BindFrameSrc('HMS/Registration/AppointmentDay.aspx','AppointmentDay')">
                        <img class="icon" src="Images/TopMenuIcons/Appointment.png" alt="Appointment" title="Appointment" style="width: 40px; height: 40px;">
                    </a>
                </li>

                <li class="defaultIconColor" id="liRegistration" onclick="changeColor('liRegistration');">
                    <a id="aRegister" onclick="BindFrameSrc('HMS/Registration/PTRegistration.aspx','Registration')">
                        <img class="icon" src="Images/TopMenuIcons/Registration.png" alt="Registration" title="Registration" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="lieAuth" onclick="changeColor('lieAuth');">
                    <a id="aeAuthApproval" onclick="BindFrameSrc('DeptHome.aspx?MenuName=Insurance','Insurance')">
                        <img class="icon" src="Images/TopMenuIcons/eAuthorization.png" alt="" title="Insurance" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liInvoice" onclick="changeColor('liInvoice');">
                    <a id="aInvoice" onclick="BindFrameSrc('HMS/Billing/Invoice.aspx','Invoice')">
                        <img class="icon" src="Images/TopMenuIcons/Invoice.png" alt="Billing" title="Billing" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liEMR" onclick="changeColor('liEMR');">
                    <a id="aEMRLink" onclick="BindFrameSrc('EMR/Registration/PatientWaitingList.aspx','EMR')">
                        <img class="icon" src="Images/TopMenuIcons/EMR.png" alt="EMR" title="EMR" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liRad" onclick="changeColor('liRad');">
                    <a id="aRadiologyLink" onclick="BindFrameSrc('DeptHome.aspx?MenuName=Radiology','Radiology')">
                        <img class="icon" src="Images/TopMenuIcons/Radilogy.png" alt="Radiology" title="Radiology" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liLab" onclick="changeColor('liLab');">
                    <a id="aLaboratoryLink" onclick="BindFrameSrc('DeptHome.aspx?MenuName=Laboratory','Laboratory')">
                        <img class="icon" src="Images/TopMenuIcons/Laboratory.png" alt="Laboratory" title="Laboratory" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liMasters" onclick="changeColor('liMasters');">
                    <a id="aMasterLink" onclick="BindFrameSrc('DeptHome.aspx?MenuName=Masters','Masters')">
                        <img class="icon" src="Images/TopMenuIcons/Masters.png" alt="Masters" title="Masters" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liAccounts" onclick="changeColor('liAccounts');">
                    <a id="aAccountLink" onclick="BindFrameSrc('DeptHome.aspx?MenuName=Accounts','Accounts')">
                        <img class="icon" src="Images/TopMenuIcons/Accounts.png" alt="Accounts" title="Accounts" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liInventory" onclick="changeColor('liInventory');">
                    <a id="aInventory" onclick="BindFrameSrc('DeptHome.aspx?MenuName=Inventory','Inventory')">
                        <img class="icon" src="Images/TopMenuIcons/E-CLAIM.png" alt="Inventory" title="Inventory" style="width: 40px; height: 40px;">
                    </a>
                </li>
                <li class="defaultIconColor" id="liReports" onclick="changeColor('liReports');"  >
                    <a id="aReports" onclick="BindFrameSrc('DeptHome.aspx',Reports')">
                        <img class="icon" src="Images/TopMenuIcons/Reports.png" alt="Reports" title="Reports" style="width: 40px; height: 40px;">
                    </a>
                </li>

                <li class="defaultIconColor" id="liMRDAudit" onclick="changeColor('liMRDAudit');" >
                    <a id="aMRDAudit" onclick="BindFrameSrc('DeptHome.aspx?MenuName=PatientSearch','PatientSearch')">
                        <img class="icon" src="Images/Zoom.png" alt="Insurance" title="PatientSearch" style="width: 40px; height: 40px;">
                    </a>
                </li>
                
                
            </ul>
             
            </div>
              
            <div id="divHeaderLine" style="height: 5px; width: 82%; background-color: #595959; border-bottom-style: solid; border-bottom-color: #595959; border-bottom-width: 5px;cursor:pointer;" title="Open /Hide Top Menu"  onclick="openTopNav()" >
            </div>

</div>