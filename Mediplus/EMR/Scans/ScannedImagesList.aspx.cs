﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Media;
using System.Drawing;
using System.Drawing.Imaging;
using EMR_BAL;

namespace Mediplus.EMR.Scans
{
    public partial class ScannedImagesList : System.Web.UI.Page
    {
        DataSet DS;
        CommonBAL objCom = new CommonBAL();


        #region Methods

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_SCANNING' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["EMR_SCAN_PATH"] = "0";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                }
            }

        }

        void papulateMenuTree()
        {
            string SCanPath, FileNoFolder = "";
            SCanPath = Convert.ToString(ViewState["EMR_SCAN_PATH"]);
            FileNoFolder = Convert.ToString(Session["EMR_PT_ID"]);

            DS = new DataSet();

            string[] folders = System.IO.Directory.GetDirectories(@SCanPath + "\\" + FileNoFolder, "*", System.IO.SearchOption.AllDirectories);



            for (int DirectoryCount = 0; DirectoryCount < folders.Length; DirectoryCount++)
            {


                MenuItem L1 = new MenuItem(folders[DirectoryCount], folders[DirectoryCount], "", folders[DirectoryCount], "_self");
                // Menu1.Items.Add(L1);

            }

        }

        void BindScanFile()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " and ESF_PT_ID='" + Convert.ToString(ViewState["PT_ID"]) + "'";
            // Criteria += " and ESF_CAT_ID='" + Convert.ToString(ViewState["Category"]) + "'";
            Criteria += " and ESF_FILENAME='" + Convert.ToString(ViewState["FileName"]) + "'";

            DS = objCom.ScanFilesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string ScanPath = Convert.ToString(DS.Tables[0].Rows[0]["ESF_FULLPATH"]);
                string FileExtension = Convert.ToString(DS.Tables[0].Rows[0]["ESF_EXTENSION"]);
                string ScanFileName = Convert.ToString(ViewState["FileName"]);
                string ScanSourcePath = @Convert.ToString(ViewState["EMR_SCAN_PATH"]) + ScanPath + ScanFileName;


                if (System.IO.File.Exists(ScanSourcePath) == true)
                {
                    if (FileExtension.ToLower() == ".pdf")
                    {
                        Response.ContentType = "Application/pdf";
                    }
                    else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
                    {
                        Response.ContentType = "application/ms-word";
                    }
                    else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
                    {
                        Response.ContentType = "application/vnd.xls";
                    }
                    else if (FileExtension.ToLower() == ".jpg")
                    {
                        Response.ContentType = "image/JPEG";
                    }
                    else if (FileExtension.ToLower() == ".png")
                    {
                        Response.ContentType = "image/png";
                    }
                    else if (FileExtension.ToLower() == ".bmp")
                    {
                        Response.ContentType = "image/bmp";
                    }
                    else if (FileExtension.ToLower() == ".gif")
                    {
                        Response.ContentType = "image/gif";
                    }



                    Response.WriteFile(ScanSourcePath);
                    Response.End();


                }

            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                BindScreenCustomization();
                string SCanPath, FileNoFolder = "";
                SCanPath = Convert.ToString(ViewState["EMR_SCAN_PATH"]) + @"/XrayImages";
                FileNoFolder = Convert.ToString(Session["EMR_PT_ID"]);

                System.IO.DirectoryInfo RootDir = new System.IO.DirectoryInfo(SCanPath + "\\" + FileNoFolder);

                if (System.IO.Directory.Exists(SCanPath + "\\" + FileNoFolder) == true)
                {
                    // output the directory into a node
                    TreeNode RootNode = OutputDirectory(RootDir, null);
                    // add the output to the tree

                    MyTree.Nodes.Add(RootNode);
                }

                if (MyTree.Nodes.Count > 0)
                {
                    MyTree.Nodes[0].Expand();
                }
                //MyTree.Nodes[0].ChildNodes[0].Expand();

                //MyTree.ExpandAll();
                // papulateMenuTree();
            }

        }

        TreeNode OutputDirectory(System.IO.DirectoryInfo directory, TreeNode parentNode)
        {




            // validate param
            if (directory == null) return null;
            // create a node for this directory
            TreeNode DirNode = new TreeNode(directory.Name);
            // get subdirectories of the current directory
            System.IO.DirectoryInfo[] SubDirectories = directory.GetDirectories();
            // OutputDirectory(SubDirectories[0], "Directories");
            // output each subdirectory


            for (int DirectoryCount = 0; DirectoryCount < SubDirectories.Length; DirectoryCount++)
            {
                OutputDirectory(SubDirectories[DirectoryCount], DirNode);
            }
            // output the current directories file

            System.IO.FileInfo[] Files = directory.GetFiles();
            for (int FileCount = 0; FileCount < Files.Length; FileCount++)
            {

                string strFullName = Files[FileCount].Name;
                string[] arrFullName = strFullName.Split('_');
                string strPT_ID = "";
                int intstrLen = 0;

                if (arrFullName.Length > 0)
                {
                    //intstrLen = arrFullName[0].Length + arrFullName[1].Length;


                    //if (arrFullName.Length == 8)
                    //{
                    //  strPT_ID = Convert.ToString(arrFullName[2]);
                    //   // intstrLen = intstrLen + strPT_ID.Length;
                    //}
                    //if (arrFullName.Length == 9)
                    //{
                    //    strPT_ID = Convert.ToString(arrFullName[3]);
                    //  //  intstrLen = intstrLen + arrFullName[2].Length;
                    //   // intstrLen = intstrLen + strPT_ID.Length;
                    //}

                    //if (strPT_ID.ToUpper() == Convert.ToString(Session["EMR_PT_ID"]))
                    //{
                    string strFIleName = "";
                    if (strFullName.Length > 31)
                    {
                        strFIleName = strFullName.Substring(strFullName.Length - 31, 31);
                    }
                    else
                    {
                        strFIleName = strFullName;
                    }

                    DirNode.ChildNodes.Add(new TreeNode(strFIleName, Files[FileCount].Name));

                    //}
                }
            }


            // otherwise add this node to the parent and return the parent
            if (parentNode == null)
            {
                return DirNode;
            }
            else
            {
                parentNode.ChildNodes.Add(DirNode);
                return parentNode;
            }
        }

        protected void MyTree_SelectedNodeChanged(object sender, EventArgs e)
        {

            string Parent1 = "", Parent2 = "";
            ImageEditorPDC.Src = "";
            //MyTree.SelectedNode.Parent.Text 
            //MyTree.SelectedNode.Parent.Parent.Text 

            string strFileName = MyTree.SelectedNode.Value;


            ViewState["FileName"] = strFileName;
            Parent1 = MyTree.SelectedNode.Parent.Text;//"03-08-2015"
            Parent2 = MyTree.SelectedNode.Text;//"general""


            ImageEditorPDC.Src = "DisplayScannedAttachments.aspx?PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&FileName=" + strFileName + "&Parent1=" + Parent1 + "&Parent2=" + Parent2;
            // ImageEditorPDC.Attributes.Add("onclick", "return ShowAttachment('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + strFileName + "');");

            //  }

            //   ViewState["Parent1"] = Parent1;
            //  ViewState["Parent2"] = Parent2;
            //  btnZoom.Attributes.Add("ondblclick", "ShowAttachment1()");


        }



        protected void btnZoom_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(ViewState["FileName"]) + "','" + Convert.ToString(ViewState["Parent1"]) + "','" + Convert.ToString(ViewState["Parent2"]) + "');", true);
        }
    }
}