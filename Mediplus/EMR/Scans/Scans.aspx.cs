﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Scans
{
    public partial class Scans : System.Web.UI.Page
    {
        DataSet DS;
        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("~/EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }
        
        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_SCANNING' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["EMR_SCAN_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }


        }

        void BindCategory()
        {
            objCom = new CommonBAL();

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESC_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' and ESC_ACTIVE='1' ";
            ds = objCom.ScanCategoryGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = ds;
                drpCategory.DataValueField = "ESC_NAME";
                drpCategory.DataTextField = "ESC_NAME";
                drpCategory.DataBind();



            }
            //drpCategory.Items.Insert(0, "--- Select ---");
            //drpCategory.Items[0].Value = "0";

            ds.Clear();
            ds.Dispose();
        }
      
        void Clear()
        {
            if (drpCategory.Items.Count > 0)
                drpCategory.SelectedIndex = 0;

           
            txtComment.Text = "";
            fileLogo.Dispose();
        }
        #endregion


       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {


                //if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                //{
                //    btnSave.Visible = false;
                //}
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                //if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                //{
                //    btnSave.Visible = false;
                //}
                try
                {
                    if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                    {
                        trRemarks.Visible = true;
                    }
                    BindScreenCustomization();
                    BindCategory();


                   
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Scans.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                 
                if (fileLogo.FileName == "")
                {
                    lblStatus.Text = "Please select the file";
                    goto FundEnd;
                }

                string SCanPath, FileNoFolder = "", DateFolder = "", CatFolder = "", FullPath = "", FileExt = "";

                SCanPath = Convert.ToString(ViewState["EMR_SCAN_PATH"]);
                FileNoFolder = Convert.ToString(Session["EMR_PT_ID"]);
                
                 string strVisitDate = Convert.ToString(Session["HPV_DATE"]);
                 string[] arrDate = strVisitDate.Split('/');

                 DateFolder = arrDate[0] + "-" + arrDate[1] + "-" + arrDate[2];
                



                CatFolder = drpCategory.SelectedValue;


                if (!Directory.Exists(SCanPath + "\\" + FileNoFolder))
                {
                    Directory.CreateDirectory(SCanPath + "\\" + FileNoFolder);
                }

                if (!Directory.Exists(SCanPath + "\\" + FileNoFolder + "\\" + DateFolder))
                {
                    Directory.CreateDirectory(SCanPath + "\\" + FileNoFolder + "\\" + DateFolder);
                }

                if (!Directory.Exists(SCanPath + "\\" + FileNoFolder + "\\" + DateFolder + "\\" + CatFolder))
                {
                    Directory.CreateDirectory(SCanPath + "\\" + FileNoFolder + "\\" + DateFolder + "\\" + CatFolder);
                }

                FileExt = Path.GetExtension(fileLogo.FileName);
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

                FullPath = @SCanPath + "\\" + FileNoFolder + "\\" + DateFolder + "\\" + CatFolder;

                fileLogo.SaveAs(FullPath + "\\" + strName + FileExt);


                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                 
                EMR_PTScans objScans = new EMR_PTScans();
                objScans.ESF_BRANCH = Convert.ToString(Session["Branch_ID"]);
                objScans.ESF_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                objScans.ESF_CAT_ID = drpCategory.SelectedValue;
                objScans.ESF_SLNO = "1";
                objScans.ESF_CREATED_USER =Convert.ToString(Session["HPV_DR_ID"]);
                objScans.ESF_CREATED_DATE = strFromDate.ToString("dd/MM/yyyy"); ;
                objScans.ESF_DEP_ID =Convert.ToString( Session["HPV_DEP_NAME"]);
                objScans.ESF_EMR_ID = Convert.ToString(  Session["EMR_ID"]);
                objScans.ESF_FILENAME = strName + FileExt;
                objScans.ESF_FULLPATH = "\\" + FileNoFolder + "\\" + DateFolder + "\\" + CatFolder + "\\";
                objScans.ESF_EXTENSION = FileExt;
                objScans.ESF_COMMENT = txtComment.Text;
                objScans.ESF_REMARKS = txtRemarks.Text;
                objScans.HMS_SP_EMRScanFileAdd();
               
                // Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowUploadMsg()", true);

            FundEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Scans.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

    }
}