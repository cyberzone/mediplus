﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;


namespace Mediplus.EMR.OPDAssessment
{
    public partial class Index : System.Web.UI.Page
    {


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {


            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPDAssess = new EMR_OPDAssessment();

            ds = objOPDAssess.WEMR_spS_GetAssessments();
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvOPDAssessment.DataSource = ds;
                gvOPDAssessment.DataBind();

            }
            else
            {
                gvOPDAssessment.DataBind();
            }
        }


        void BindOPDData(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_OPD"] != "" && ViewState["EMR_OPD"] != null)
            {

                DS = (DataSet)ViewState["EMR_OPD"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["OPD_FIELD_VALUE"]);
                    strValue = Convert.ToString(DR["OPD_DETAILS"]);
                }
            }


        }

        void BindOPDDataAll()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and EOPD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";// and EOPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            ds = objOPD.EMR_OPDGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_OPD"] = ds;
            }

        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='OPDAssessment'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }


        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }


        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }

                    BindTemplate();
                    BindOPDDataAll();
                    BindData();
                   
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       OPDAssessment.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvOPDAssessment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                RadioButtonList radPTAnswer = (RadioButtonList)e.Row.FindControl("radPTAnswer");
                TextBox txtDtls = (TextBox)e.Row.FindControl("txtDtls");

                if (lblLevelType.Text == "1")
                {
                    radPTAnswer.Visible = false;
                    txtDtls.Visible = false;
                    //lblDescription.Font.Bold = true;
                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and OPD_FIELD_ID='" + lblFieldID.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);
                    radPTAnswer.SelectedValue = strValueYes;
                    txtDtls.Text = strValue;
                }

            }
        }


        #endregion


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (Int32 i = 0; i < gvOPDAssessment.Rows.Count; i++)
                {

                    Label lblLevelType = (Label)gvOPDAssessment.Rows[i].FindControl("lblLevelType");
                    Label lblFieldID = (Label)gvOPDAssessment.Rows[i].FindControl("lblFieldID");
                    RadioButtonList radPTAnswer = (RadioButtonList)gvOPDAssessment.Rows[i].FindControl("radPTAnswer");
                    TextBox txtDtls = (TextBox)gvOPDAssessment.Rows[i].FindControl("txtDtls");

                    if (lblLevelType.Text != "1" && radPTAnswer.SelectedValue != "")
                    {
                        EMR_OPDAssessment objOPD = new EMR_OPDAssessment();
                        objOPD.branchid = Convert.ToString(Session["Branch_ID"]);
                        objOPD.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                        objOPD.fieldid = lblFieldID.Text;
                        objOPD.value = radPTAnswer.SelectedValue;
                        objOPD.comment = txtDtls.Text;
                        objOPD.EOPD_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                        objOPD.EMROPDAssessmentAdd();
                    }
                }

                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        UpdateAuditDtls();
                    }
                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtTemplateName.Value.Trim()  == "")
                {
                    goto FunEnd;
                }
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type =   "OPDAssessment";
                objCom.Description = txtTemplateName.Value;
                 objCom.TemplateData = "";

                 if (chkTemplateOtherDr.Checked == true)
                 {
                     objCom.AllDr = "1";
                 }
                 else
                 {
                     objCom.AllDr = "0";
                 }


                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();



                EMR_OPDAssessment objOPD = new EMR_OPDAssessment();
                objOPD.branchid = Convert.ToString(Session["Branch_ID"]);
                objOPD.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                objOPD.code = TemplateCode;

                objOPD.WEMR_spI_SaveOPDAssessmentTemplate();


                BindTemplate();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
                FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      OPDAssessment.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (drpTemplate.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

               
                EMR_OPDAssessment objOPD = new EMR_OPDAssessment();
                objOPD.branchid = Convert.ToString(Session["Branch_ID"]);
                objOPD.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                objOPD.code = drpTemplate.SelectedValue;

                objOPD.WEMR_spS_ShowOPDAssessmentTemplate();
                BindOPDDataAll();
                BindData();

                FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      OPDAssessment.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void lnkDeleteTemp_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpTemplate.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                CommonBAL objCom = new CommonBAL();

                string Criteria = " 1=1  and ET_TYPE='OPDAssessment' and ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND ET_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria);


                Criteria =" 1=1 and EOPD_BRANCH_ID='"+  Convert.ToString(Session["Branch_ID"])  +"' AND  EOPD_ID='"+  Convert.ToString(Session["EMR_ID"]) +"'";
                Criteria += " AND OPD_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_OPD_TEMPLATES", Criteria);

               
                BindTemplate();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      OPDAssessment.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}