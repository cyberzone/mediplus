﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.Referral
{
    public partial class Index : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();
        EMR_PTReferral objRef = new EMR_PTReferral();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        DataSet GetReferal()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND (Segment = 'REFERENCETYPE') AND (ISNULL(Value, '') <> '')  AND (Status = 1)";
            objCom = new CommonBAL();

            DS = objCom.EMR_DYNAMIC_CONTENT(Criteria);
            ReferenceType.DataSource = DS;
            ReferenceType.DataValueField = "Value";
            ReferenceType.DataTextField = "Value";
            ReferenceType.DataBind();


            return DS;
        }

        DataSet GetStaff()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.GetStaffMaster(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                InDoctorID.DataSource = DS;
                InDoctorID.DataValueField = "HSFM_STAFF_ID";
                InDoctorID.DataTextField = "FullName";
                InDoctorID.DataBind();
            }

            return DS;
        }

        void BindReferral()
        {
            objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " and EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID=" + Convert.ToString(Session["EMR_ID"]);
            DS = objRef.EMRPTReferenceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(DS.Tables[0].Rows[0]["EPR_TYPE"]) != null && Convert.ToString(DS.Tables[0].Rows[0]["EPR_TYPE"]).Equals("2"))
                {
                    OutType.Checked = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "EnableOutsideDr();", true);
                }
                else
                {
                    InType.Checked = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "EnableInsideDr();", true);
                }



                if (DS.Tables[0].Rows[0].IsNull("EPR_REFTYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPR_REFTYPE"]) != "")
                {
                    for (int intCount = 0; intCount < ReferenceType.Items.Count; intCount++)
                    {
                        if (ReferenceType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["EPR_REFTYPE"]))
                        {
                            ReferenceType.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_REFTYPE"]);
                            goto ForPayer;
                        }

                    }
                }

            ForPayer: ;





                InDoctorID.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_DR_CODE"]);
                OutHospital.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_OUT_HOSPITAL"]);
                OutDoctor.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_OUT_DRNAME"]);
                KinName.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_KIN_NAME"]);
                KinAddress.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_KIN_ADDRESS"]);
                KinPhone.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_KIN_PHONE"]);
                Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_REMARKS"]);
                Diagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_PRO_DIAG"]);
                BeforeTransfer.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_BEFORE_TRANSFER"]);
                AfterTransfer.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_AFTER_TRANSFER"]);
                Recommended.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_MED_RECOMMENDED"]);
                ManagementTransfer.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_MANAGEMENT_TRANFER"]);


                Currentstatusofpatient.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_PATIENT_CURRENT_STATUS"]);
                Specialendorsement.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPR_SPECIAL_ENDORSEMENT"]);





            }

        }

        void BindEMRPTReferanceCond()
        {
            objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " and EPRC_ID=" + Convert.ToString(Session["EMR_ID"]) + " and EPRC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objRef.EMRPTReferanceCondGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "G_CONDITION")
                    {
                        txtComment1.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime1.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP1.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR1.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO21.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR1.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp1.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);
                    }


                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "RESP_SYS")
                    {
                        txtComment2.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime2.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP2.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR2.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO22.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR2.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp2.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);

                    }

                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "CVS")
                    {

                        txtComment3.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime3.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP3.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR3.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO23.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR3.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp3.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);
                    }




                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "CNS")
                    {
                        txtComment4.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime4.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP4.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR4.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO24.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR4.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp4.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);

                    }
                }

            }

        }

        void SaveEMRPTReferanceCond()
        {
            objRef = new EMR_PTReferral();
            objRef.branchid = Convert.ToString(Session["Branch_ID"]);
            objRef.patientmasterid = Convert.ToString(Session["EMR_ID"]);
            objRef.EPRC_ORG_FUNCTION = "G_CONDITION";
            objRef.EPRC_COMMENT = txtComment1.Text;
            objRef.EPRC_TIME = txtTime1.Text;
            objRef.EPRC_BP = txtBP1.Text;
            objRef.EPRC_HR = txtHR1.Text;
            objRef.EPRC_SPO2 = txtSPO21.Text;
            objRef.EPRC_RR = txtRR1.Text;
            objRef.EPRC_TEMPERATURE = txtTemp1.Text;
            objRef.EMRPTReferanceCondAdd();


            objRef = new EMR_PTReferral();
            objRef.branchid = Convert.ToString(Session["Branch_ID"]);
            objRef.patientmasterid = Convert.ToString(Session["EMR_ID"]);
            objRef.EPRC_ORG_FUNCTION = "RESP_SYS";
            objRef.EPRC_COMMENT = txtComment2.Text;
            objRef.EPRC_TIME = txtTime2.Text;
            objRef.EPRC_BP = txtBP2.Text;
            objRef.EPRC_HR = txtHR2.Text;
            objRef.EPRC_SPO2 = txtSPO22.Text;
            objRef.EPRC_RR = txtRR2.Text;
            objRef.EPRC_TEMPERATURE = txtTemp2.Text;
            objRef.EMRPTReferanceCondAdd();


            objRef = new EMR_PTReferral();
            objRef.branchid = Convert.ToString(Session["Branch_ID"]);
            objRef.patientmasterid = Convert.ToString(Session["EMR_ID"]);
            objRef.EPRC_ORG_FUNCTION = "CVS";
            objRef.EPRC_COMMENT = txtComment3.Text;
            objRef.EPRC_TIME = txtTime3.Text;
            objRef.EPRC_BP = txtBP3.Text;
            objRef.EPRC_HR = txtHR3.Text;
            objRef.EPRC_SPO2 = txtSPO23.Text;
            objRef.EPRC_RR = txtRR3.Text;
            objRef.EPRC_TEMPERATURE = txtTemp3.Text;
            objRef.EMRPTReferanceCondAdd();


            objRef = new EMR_PTReferral();
            objRef.branchid = Convert.ToString(Session["Branch_ID"]);
            objRef.patientmasterid = Convert.ToString(Session["EMR_ID"]);
            objRef.EPRC_ORG_FUNCTION = "CNS";
            objRef.EPRC_COMMENT = txtComment4.Text;
            objRef.EPRC_TIME = txtTime4.Text;
            objRef.EPRC_BP = txtBP4.Text;
            objRef.EPRC_HR = txtHR4.Text;
            objRef.EPRC_SPO2 = txtSPO24.Text;
            objRef.EPRC_RR = txtRR4.Text;
            objRef.EPRC_TEMPERATURE = txtTemp4.Text;
            objRef.EMRPTReferanceCondAdd();


        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }

        void BindHistory()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_ID   !='" + Convert.ToString(Session["EMR_ID"]) + "' ";


            DS = objCom.ReferenceHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.DataSource = DS;
                drpHistory.DataTextField = "Name";
                drpHistory.DataValueField = "Code";
                drpHistory.DataBind();
            }
            drpHistory.Items.Insert(0, "----Select History---");
            drpHistory.Items[0].Value = "0";


        }

        void CheckReferralType(string EMR_ID)
        {
            ViewState["ReferalType"] = "IntRef";
            EMR_PTReferral objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " and EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID=" + EMR_ID;
            DS = objRef.EMRPTReferenceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(DS.Tables[0].Rows[0]["EPR_TYPE"]) != null && Convert.ToString(DS.Tables[0].Rows[0]["EPR_TYPE"]).Equals("2"))
                {
                    ViewState["ReferalType"] = "ExtRef";

                }
                else
                {
                    ViewState["ReferalType"] = "IntRef";

                }
            }


        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtTime1.Text = strDate + " " + strTime;
                    txtTime2.Text = strDate + " " + strTime;
                    txtTime3.Text = strDate + " " + strTime;
                    txtTime4.Text = strDate + " " + strTime;
                    GetReferal();
                    GetStaff();
                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }

                    BindReferral();
                    BindHistory();
                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {
                        divPatientCondition.Visible = true;
                        BindEMRPTReferanceCond();
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Referral.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objRef = new EMR_PTReferral();
                objRef.branchid = Convert.ToString(Session["Branch_ID"]);
                objRef.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                objRef.referencetype = ReferenceType.Value;
                objRef.remarks = Remarks.Value;
                objRef.type = InType.Checked == true ? "1" : "2";
                objRef.indoctorid = InDoctorID.Value;
                objRef.outhospital = OutHospital.Value;
                objRef.outdoctor = OutDoctor.Value;
                objRef.kinname = KinName.Value;
                objRef.kinaddress = KinAddress.Value;
                objRef.kinphone = KinPhone.Value;
                objRef.diagnosis = Diagnosis.Value;
                objRef.recommended = Recommended.Value;
                objRef.managementtransfer = ManagementTransfer.Value;
                objRef.beforetransfer = BeforeTransfer.Value;
                objRef.aftertransfer = AfterTransfer.Value;
                objRef.PatientCurrentstatus = Currentstatusofpatient.Value;
                objRef.SpecialEndorsement = Specialendorsement.Value;
                objRef.templatecode = "";
                objRef.EMRPTReferenceAdd();

                if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                {
                    SaveEMRPTReferanceCond();
                }

                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        UpdateAuditDtls();
                    }
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Referral.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void imgbtnPrescHistView_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }


                if (drpHistory.SelectedIndex != 0)
                {
                    CheckReferralType(strEMR_ID);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Referral", "ShowReferral('" + Convert.ToString(ViewState["ReferalType"]) + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + strEMR_ID + "');", true);

                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }
    }
}