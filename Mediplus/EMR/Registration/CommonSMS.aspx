﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonSMS.aspx.cs" Inherits="Mediplus.EMR.Registration.CommonSMS" %>


<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <title>Appointment</title>
    <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
        function val() {
            var label;
            label = document.getElementById('lblStatus');
            label.style.color = 'red';
            document.getElementById('lblStatus').innerHTML = " ";

            /*
            if (document.getElementById('txtFileNo').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the File No";
                document.getElementById('txtFileNo').focus();
                return false;
            }
            */


            if (document.getElementById('txtEmail').value != "") {
                if (echeck(document.getElementById('txtEmail').value) == false) {
                    document.getElementById('txtEmail').focus();
                    return false;
                }
            }


            return true



        }



        function SendSMS() {
            var APIKey = "4f9c26c4-eb01-4bc2-9e67-a0f8d58ce4f6201306";
            var SenderID = "MIDAC DUBAI";
            var MobileNo = "0502213045";
            var Content = "test Message";
            var Language = "0"

            alert(APIKey + '$' + SenderID + '$' + MobileNo + '$' + Content + '$' + Language);

            exec('D:\\SampleProjects\\SMS_Axonme\\SMS_Axonme\\bin\\Debug\\SMS_Axonme.exe', APIKey + '$"' + SenderID + '"$' + MobileNo + '$"' + Content + '"$' + Language);

        }


        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
             <table >
                <tr>
                    <td class="PageHeader">SMS Sending
                     </td>
                </tr>
            </table>
                <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%"">
                    <tr>
                        <td>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </td>
                    </tr>

                </table>
            <table width="100%">
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblMobileNo" runat="server" CssClass="lblCaption1"
                            Text="Mobile No"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobile" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                            Text="Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPTName" runat="server" CssClass="label" BorderWidth="1px"  BorderColor="#cccccc" Width="200px"  ></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                     <td class="style2">
                        <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                            Text="Date & Time"></asp:Label>
                    </td>
                    <td>
                         <asp:TextBox ID="txtFromDate" runat="server" Width="72px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="red" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                                enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                            </asp:calendarextender>
                            <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                           
                         <asp:DropDownList ID="drpSTHour" CssClass="label" style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpSTMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>         
                            <asp:DropDownList ID="drpSTAM" runat="server" visible="false" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                                    <asp:Button ID="btnLoadMSG" runat="server" Text="Show SMS" CssClass="button orange small" Width="100px" OnClick="btnLoadMSG_Click"   />

                    </td>
                </tr>
                 <tr>
                    <td class="style2">
                        <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                            Text="Content"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContent" runat="server" CssClass="label" BorderWidth="1px"  BorderColor="#cccccc" Width="500px"  Height="70px" TextMode="MultiLine" ></asp:TextBox>
                    </td>
                </tr>
               
                <tr>
                       <td></td>
                        <td>
                            <asp:Button ID="btnSendSMS" runat="server" Text="Send SMS" CssClass="button orange small" Width="100px" OnClick="btnSendSMS_Click" OnClientClick="return val();" />
                            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button orange small" Width="100px" OnClick="btnClear_Click"   />
                            </td>
                    </tr>
            </table>
        </div>
    </form>
</body>
</html>
