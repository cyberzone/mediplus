﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.Registration
{
    public partial class CommonSMS : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        public static string APPOINTMENTMSG = (string)System.Configuration.ConfigurationSettings.AppSettings["APPOINTMENTMSG"];

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "24";
                }


            }


        }

        void BindTime()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");



                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");




            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);



                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindTime12Hrs()
        {
            int AppointmentInterval = 05;// Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = 01; //Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = 12;// Convert.ToInt32(ViewState["AppointmentEnd"]);
            int index = 0;

            for (int i = 1; i <= 12; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");



                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");



            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);




                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["SMS_APIKey"] = "";
            ViewState["SMS_SenderID"] = "";
            ViewState["SMS_TYPE"] = "";

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "SMS_APIKey")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SMS_APIKey"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "SMS_SenderID")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SMS_SenderID"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "SMS_TYPE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SMS_TYPE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }




                }
            }



        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                // BindSystemOption();

                //if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                //{

                BindTime12Hrs();
                drpSTAM.Visible = true;

                BindScreenCustomization();


                //}
                //else
                //{
                //    BindTime();

                //}
            }
        }

        protected void btnSendSMS_Click(object sender, EventArgs e)
        {
            if (txtMobile.Text.Trim() == "")
            {
                lblStatus.Text = "Please enter Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            string MobileNumber = txtMobile.Text.Trim();

            if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
            {
                lblStatus.Text = "Please check the Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            clsSMS objSMS = new clsSMS();
            string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";

            string strContent = txtContent.Text;
            //strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + strNewLineChar + txtFName.Text.Trim() + " " + txtLName.Text.Trim()
            //   + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;

            // if (GlobelValues.FileDescription.ToUpper() == "SMCH")
            //{
            //strContent = "";
            //if (drpSMSLanguage.SelectedValue == "8")
            //{
            //    strContent = APPOINTMENTMSG_A.Replace("|PatientName|", txtFName.Text.Trim());
            //}
            //else
            //{
            // strContent = APPOINTMENTMSG.Replace("|PatientName|", txtFName.Text.Trim());
            //  }

            ////strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());
            ////strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue);
            ////strContent = strContent.Replace("|department|", Convert.ToString(ViewState["Department"]));
            //}
            //strContent = strContent.Replace("|PatientName|", txtPTName.Text.Trim());
            //strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());
            //strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue);
            // strContent = strContent.Replace("|department|", Convert.ToString(ViewState["Department"]));

            objSMS.MobileNumber = txtMobile.Text.Trim(); // "0502213045";
            objSMS.template = strContent;

            objSMS.SMS_APIKey = Convert.ToString(ViewState["SMS_APIKey"]);
            objSMS.SMS_SenderID = Convert.ToString(ViewState["SMS_SenderID"]);


            Boolean boolResult = false;



            boolResult = objSMS.SendSMS_Axome(Convert.ToInt32(0));


            if (boolResult == true)
            {
                lblStatus.Text = "SMS Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblStatus.Text = "SMS Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "SendSMS()", true);
        FunEnd: ;
        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtMobile.Text = "";
            txtPTName.Text = "";
            txtContent.Text = "";
        }

        protected void btnLoadMSG_Click(object sender, EventArgs e)
        {
            string strContent = "";
            strContent = APPOINTMENTMSG.Replace("|PatientName|", txtPTName.Text.Trim());

            string strSelectedDate = txtFromDate.Text;  //System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
            string strday = strTSelDate.DayOfWeek.ToString();

            strContent = strContent.Replace("|date|", txtFromDate.Text.Trim());
            strContent = strContent.Replace("|time|", drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + " " + drpSTAM.SelectedValue + "," + strday);

            txtContent.Text = strContent;
        }
    }
}