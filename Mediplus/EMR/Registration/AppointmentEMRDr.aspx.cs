﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using System.Drawing;
using EMR_BAL;


namespace Mediplus.EMR.Registration
{
    public partial class AppointmentEMRDr : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();
        CommonBAL objCom = new CommonBAL();

        #region Methods



        void TextFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }


            }


        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HMS' ";



            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.ScreenCustomizationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_BOOKED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Booked"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_CANCELLED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Cancelled"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_CONFIRMED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Confirmed"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_FILE_DRAWN")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["FileDrawn"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_MEETING")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Meeting"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_NOT_ARRIVED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NotArrived"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_NOTE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Note"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OTHERS")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Others"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OUT_OFFICE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OutOfOffice"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_PT_ARRIVED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PTArrived"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_PT_BILLED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PTBilled"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_SEEN_BY_DR")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SeenByDr"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_SEEN_BY_NURSE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["SeenByNurse"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_WAITING")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Waiting"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_FONT_SIZE")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["FontSize"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_COMPLETED")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["Completed"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DS.Tables[0].Rows[i]["SEGMENT"]) == "APMNT_COLOR_OT_CASES")
                    {
                        if (DS.Tables[0].Rows[i].IsNull("CUST_VALUE") == false)
                        {
                            ViewState["OTCases"] = Convert.ToString(DS.Tables[0].Rows[i]["CUST_VALUE"]);
                        }

                    }
                }
            }

        }


        void BindAppointment()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(ViewState["DrID"]) + "'";


            objCom = new CommonBAL();

            DataSet DSDoctors = new DataSet();
            DSDoctors = objCom.GetStaffMaster(Criteria);


            //Criteria = " 1=1 AND HROS_TYPE='D' ";

            //DataSet DSRoster = new DataSet();
            //DSRoster = objCom.RosterMasterGet(Criteria);

            string strSelectedDate = txtFromDate.Text;  //System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            DateTime strTSelDate = Convert.ToDateTime(strForStartDate);
            string strday = strTSelDate.DayOfWeek.ToString();

            strData.Append("<Table border='1' style='border-color:black;width:100%' ><tr>");
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
            {

                strData.Append("<td width='250px'     class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px' style='display:none;'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

            }
            else
            {

                strData.Append("<td width='250px'  style='display:none;'    class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");

                strData.Append("<td width='250px'  class='Header' >");
                strData.Append(strSelectedDate + "</br>" + strday);
                strData.Append("</td>");
            }
            if (DSDoctors.Tables[0].Rows.Count > 0)
            {
                //style='font-family:Times New Roman,font-size:5;color:#2078c0;text-align:center'>

                for (Int32 i = 0; i <= DSDoctors.Tables[0].Rows.Count - 1; i++)
                {
                    strData.Append("<td width=100%  class='Header1' >");
                    strData.Append(Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "</BR>" + Convert.ToString(DSDoctors.Tables[0].Rows[i]["HSFM_FNAME"]));
                    strData.Append("</td>");
                }
            }
            strData.Append("</tr>");


            Int32 intday = strTSelDate.Day;


            String strlTime;
            String strNewTime1 = "";


            //strrtime = strFrom1.Substring(strFrom1.Length - 2, 2);
            strlTime = Convert.ToString(AppointmentStart); //strFrom1.Substring(0, 2);
            strNewTime1 = "00";// strFrom1.Substring(strFrom1.Length - 2, 2);

            AppointmentStart = AppointmentStart * 100;
            AppointmentEnd = AppointmentEnd * 100;

            // TextFileWriting("AppointmentStart"+ Convert.ToString(AppointmentStart));
            // TextFileWriting("AppointmentEnd" + Convert.ToString(AppointmentEnd));

            int[] arrCount = new int[DSDoctors.Tables[0].Rows.Count];

            for (Int32 i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                if (strNewTime1 == "")
                {
                    strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                }

                // i = i + AppointmentInterval;
                string[] arrlTime = strlTime.Split(':');
                if (arrlTime.Length > 1)
                {
                    strlTime = arrlTime[0];
                }

                if (Convert.ToInt32(strNewTime1) >= 60)
                {
                    strlTime = Convert.ToString(Convert.ToInt32(Convert.ToInt32(strlTime) + 1).ToString("D2")) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                }
                else
                {
                    strlTime = Convert.ToString(Convert.ToInt32(strlTime).ToString("D2") + ":" + strNewTime1);
                }

                i = Convert.ToInt32(strlTime.Replace(":", ""));


                if (Convert.ToInt32(strlTime.Replace(":", "")) <= AppointmentEnd && strlTime != "00:00")
                {
                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "24")
                    {
                        strData.Append("<tr>");
                        strData.Append("<td  class='RowHeader'>");
                        strData.Append(strlTime);
                        strData.Append("</td>");

                        DateTime dtstlTime;
                        dtstlTime = Convert.ToDateTime(strlTime);
                        strData.Append("<td  style='display:none;'    class='RowHeader'>");
                        strData.Append(dtstlTime.ToString(@"hh\:mm tt"));
                        strData.Append("</td>");

                    }
                    else
                    {

                        strData.Append("<tr>");
                        strData.Append("<td style='display:none;'  class='RowHeader'>");
                        strData.Append(strlTime);
                        strData.Append("</td>");

                        DateTime dtstlTime;
                        dtstlTime = Convert.ToDateTime(strlTime);
                        strData.Append("<td    class='RowHeader'>");
                        strData.Append(dtstlTime.ToString(@"hh\:mm tt"));
                        strData.Append("</td>");

                    }
                    //   TextFileWriting(strlTime);


                    for (int j = 0; j <= DSDoctors.Tables[0].Rows.Count - 1; j++)
                    {
                        if (arrCount[j] > 0)
                        {
                            arrCount[j] = arrCount[j] - 1;
                            goto ForEnd;
                        }

                        DataSet DSAppt = new DataSet();
                        DSAppt = GetAppointment(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]), strlTime);
                        if (DSAppt.Tables[0].Rows.Count > 0)
                        {
                            String strlTime2 = "";
                            Int32 AppointmentStart2 = 0, AppointmentEnd2 = 0;

                            string strStartTime = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]);
                            //string[] arrStartTime = strStartTime.Split(':');
                            //strStartTime = arrStartTime[0] +""+ arrStartTime[1];
                            strStartTime = strStartTime.Replace(":", "");


                            string strStartFnish = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]);
                            //string[] arrStartFnish =strStartFnish.Split(':');
                            //strStartFnish = arrStartFnish[0] + "" + arrStartFnish[1];
                            strStartFnish = strStartFnish.Replace(":", "");

                            AppointmentStart2 = Convert.ToInt32(strStartTime);
                            AppointmentEnd2 = Convert.ToInt32(strStartFnish);

                            strlTime2 = strStartTime.Substring(0, 2);
                            Int32 intRowCount = 0;
                            String strNewTime2 = Convert.ToString(AppointmentStart2);
                            strNewTime2 = strNewTime2.Substring(strNewTime2.Length - 2, 2);

                            for (Int32 k = AppointmentStart2; k <= AppointmentEnd2; k++)
                            {
                                if (strNewTime2 == "")
                                {
                                    strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
                                }

                                string[] arrlTime2 = strlTime2.Split(':');
                                if (arrlTime2.Length > 1)
                                {
                                    strlTime2 = arrlTime2[0];
                                }

                                if (Convert.ToInt32(strNewTime2) >= 60)
                                {
                                    strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                                }
                                else
                                {
                                    strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
                                }

                                k = Convert.ToInt32(strlTime2.Replace(":", ""));
                                strNewTime2 = "";

                                intRowCount = intRowCount + 1;

                            }

                            arrCount[j] = intRowCount - 2;

                            string strCustColor = "";
                            switch (Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]))
                            {
                                case "1":
                                    strCustColor = Convert.ToString(ViewState["Confirmed"]);
                                    break;
                                case "2":
                                    strCustColor = Convert.ToString(ViewState["Cancelled"]);
                                    break;
                                case "3":
                                    strCustColor = Convert.ToString(ViewState["Waiting"]);
                                    break;
                                case "4":
                                    strCustColor = Convert.ToString(ViewState["Booked"]);
                                    break;
                                case "5":
                                    strCustColor = Convert.ToString(ViewState["FileDrawn"]);
                                    break;
                                case "6":
                                    strCustColor = Convert.ToString(ViewState["Meeting"]);
                                    break;
                                case "7":
                                    strCustColor = Convert.ToString(ViewState["Note"]);
                                    break;
                                case "8":
                                    strCustColor = Convert.ToString(ViewState["OutOfOffice"]);
                                    break;
                                case "9":
                                    strCustColor = Convert.ToString(ViewState["PTArrived"]);
                                    break;
                                case "10":
                                    strCustColor = Convert.ToString(ViewState["PTBilled"]);
                                    break;
                                case "11":
                                    strCustColor = Convert.ToString(ViewState["SeenByDr"]);
                                    break;
                                case "12":
                                    strCustColor = Convert.ToString(ViewState["SeenByNurse"]);
                                    break;
                                case "13":
                                    strCustColor = Convert.ToString(ViewState["Others"]);
                                    break;
                                case "14":
                                    strCustColor = Convert.ToString(ViewState["NotArrived"]);
                                    break;
                                case "15":
                                    strCustColor = Convert.ToString(ViewState["Completed"]);
                                    break;
                                case "16":
                                    strCustColor = Convert.ToString(ViewState["OTCases"]);
                                    break;
                                default:
                                    break;

                            }
                            strCustColor = strCustColor.Replace("|", ",");
                            //  Color color = Color.FromArgb( Convert.ToInt32(strCustColor));


                        //    string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
                        //    strData.Append("<td   rowspan=" + Convert.ToString(intRowCount - 1) + " style=' vertical-align:top;border-color:black;background-color: rgb(" + strCustColor + ");'>");
                        //    strData.Append("<a class='Content'   href='javascript:PatientPopup1(" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APPOINTMENTID"]) + ",0,0,0)'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]) + "(File# " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + ")<br/> " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]) + "<br/> Mob:" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILENO"]) + "<br/>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) + "<br/>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAS_STATUS"]) + "</a>");
                        //    strData.Append("</td>");
                        //}
                        //else
                        //{

                        //    strData.Append("<td    style='border-color:black;background-color: white;'>");

                        //    strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "','" + Convert.ToString(ViewState["PatientID"]) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
                        //    strData.Append("</td>");

                        //}


                            string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
                            strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + "  rowspan=" + Convert.ToString(intRowCount - 1) + " ondblclick=PatientPopup1(" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APPOINTMENTID"]) + ",0,0,0)   align='center' class='AppBox'   rowspan=" + Convert.ToString(intRowCount - 1) + " style='vertical-align:middle;border-color:black;background-color: " + strColor + ";  cursor:pointer;'>");
                            strData.Append("<span class=lblCaption1>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]) + "(File# " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + ")<br/> " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]) + "<br/> Mob:" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILENO"]) + "<br/>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) + "<br/>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAS_STATUS"]) + "<br/>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]) + "</span>");
                            strData.Append("</td>");
                        }
                        else
                        {

                            strData.Append("<td width=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]) + "  height=" + Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"]) + " ondblclick=PatientPopup1('" + 0 + "','" + strlTime + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "','" + Convert.ToString(ViewState["PatientID"]) + "')   class='AppBox' style='border-color:black;background-color: white;  cursor:pointer;'>");
                            strData.Append("</td>");

                        }
                    ForEnd: ;
                    }




                    strData.Append("</tr>");
                }

                strNewTime1 = "";

            }


            //String strrtime2;
        //String strlTime2;
        //String strNewTime2;


            //strrtime2 = strFrom2.Substring(strFrom2.Length - 2, 2);
        //strlTime2 = strFrom2.Substring(0, 2);
        //strNewTime2 = strFrom2.Substring(strFrom2.Length - 2, 2);


            //for (Int32 i = intStart2; i <= intEnd2; i++)
        //{
        //    if (strNewTime2 == "")
        //    {
        //        strNewTime2 = Convert.ToString(Convert.ToInt32(strlTime2.Substring(strlTime2.Length - 2, 2)) + AppointmentInterval);
        //    }

            //    // i = i + AppointmentInterval;
        //    string[] arrlTime = strlTime2.Split(':');
        //    if (arrlTime.Length > 1)
        //    {
        //        strlTime2 = arrlTime[0];
        //    }

            //    if (Convert.ToInt32(strNewTime2) >= 60)
        //    {
        //        strlTime2 = Convert.ToString(Convert.ToInt32(strlTime2) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime2) - 60);
        //    }
        //    else
        //    {
        //        strlTime2 = Convert.ToString(strlTime2 + ":" + strNewTime2);
        //    }

            //    i = Convert.ToInt32(strlTime2.Replace(":", ""));


            //    if (Convert.ToInt32(strlTime2.Replace(":", "")) <= Convert.ToInt32(strTo2.Replace(":", "")) && strlTime2 != "00:00")
        //    {
        //        strData.Append("<tr>");
        //        strData.Append("<td   class='RowHeader'>");
        //        strData.Append(strlTime2);
        //        strData.Append("</td>");

            //        for (int j = 0; j <= DSDoctors.Tables[0].Rows.Count - 1; j++)
        //        {

            //            DataSet DSAppt = new DataSet();
        //            DSAppt = GetAppointment(Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]), strlTime2);
        //            if (DSAppt.Tables[0].Rows.Count > 0)
        //            {
        //                string strColor = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAC_COLOR"]);
        //                strData.Append("<td   style='vertical-align:top;background-color: " + strColor + ";'>");
        //                strData.Append("<a class='Content'   href='javascript:PatientPopup1(" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_APPOINTMENTID"]) + ",0,0,0)'>" + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) + " - " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"] + " " + Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"])) + "</a>");
        //                strData.Append("</td>");
        //            }
        //            else
        //            {
        //                strData.Append("<td    style='background-color: white;'>");
        //                strData.Append("<a style='text-decoration:none;' href=javascript:PatientPopup1('" + 0 + "','" + strlTime2 + "','" + Convert.ToString(DSDoctors.Tables[0].Rows[j]["HSFM_STAFF_ID"]) + "')>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>");
        //                strData.Append("</td>");

            //            }

            //        }


            //        strData.Append("</tr>");
        //    }

            //    strNewTime2 = "";

            //}





        FunEnd: ;



        }

        DataSet GetAppointment(string DRCode, string FromTime)
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            string strlTime;
            strlTime = FromTime;

            string strNewTime1 = "";
            if (strNewTime1 == "")
            {
                strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
            }

            // i = i + AppointmentInterval;


            string[] arrlTime = strlTime.Split(':');
            if (arrlTime.Length > 1)
            {
                strlTime = arrlTime[0];
            }

            if (Convert.ToInt32(strNewTime1) >= 60)
            {
                strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
            }
            else
            {
                strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
            }

            string Criteria = " 1=1 ";

            if (radAppStatus.SelectedIndex != 0)
            {
                Criteria += "  AND HAM_STATUS IN (" + hidStatus.Value + ")";
            }
            else
            {
                Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";
            }
            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += " AND HAM_DR_CODE='" + DRCode + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            Criteria += "   AND   HAM_STARTTIME = '" + strForStartDate + " " + FromTime + ":00'";


            //Criteria += "  AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00' AND   HAM_STARTTIME <= '" + strForStartDate + " " + FromTime + ":00'";
            //Criteria += "  AND HAM_FINISHTIME >= '" + strForStartDate + " " + strlTime + ":00' AND   HAM_FINISHTIME <= '" + strForStartDate + " " + strlTime + ":00'";


            objCom = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = objCom.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                //    arrAppDtls[0] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_APPOINTMENTID"]);
                //    arrAppDtls[1] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_FILENUMBER"]);
                //    arrAppDtls[2] = Convert.ToString(DS.Tables[0].Rows[0]["HAM_PT_NAME"]);


            }


            return DSAppt;
        }

        void GetAppointmentStatus()
        {
            objCom = new CommonBAL();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1 ";

            if (radAppStatus.SelectedIndex != 0)
            {
                Criteria += " AND HAS_STATUS='" + radAppStatus.SelectedValue + "'";
            }
            else
            {
                Criteria += " AND HAS_STATUS='" + radAppStatus.Items[1].Value + "' OR  HAS_STATUS='" + radAppStatus.Items[2].Value + "'";
            }
            DS = objCom.AppointmentStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }



        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnNewApp.Enabled = false;




            }


            if (strPermission == "7")
            {

                btnNewApp.Enabled = false;



            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../SessionExpired.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                try
                {



                    ViewState["Date"] = "0";
                    //  txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    objCom = new CommonBAL();
                    string strDate = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    txtFromDate.Text = strDate;

                    ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                    ViewState["DrID"] = Convert.ToString(Request.QueryString["DrID"]);
                    ViewState["PatientID"] = Convert.ToString(Request.QueryString["PatientID"]);
                    hidDrID.Value = Convert.ToString(ViewState["DrID"]);
                    hidPatientID.Value = Convert.ToString(ViewState["PatientID"]);


                    if (Convert.ToString(ViewState["Date"]) != "0" && Convert.ToString(ViewState["Date"]) != "")
                    {
                        txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                    }
                    // BindDrDept();
                    BindSystemOption();
                    BindScreenCustomization();
                    GetAppointmentStatus();
                    BindAppointment();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString());
                    TextFileWriting("AppointmentDay.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppointmentStatus();


                BindAppointment();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.aspx_txtFromDate_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void radAppStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAppointmentStatus();

                BindAppointment();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("AppointmentDay.aspx_radAppStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetAppointmentStatus();

            BindAppointment();
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowReport('" + ViewState["DrID"] + "');", true);

        }
    }
}