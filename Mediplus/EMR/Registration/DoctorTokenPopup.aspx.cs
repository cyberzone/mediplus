﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Registration
{
    public partial class DoctorTokenPopup : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindGrid()
        {
            string Criteria = " 1=1 ";

            CommonBAL com = new CommonBAL();
            ds = com.DoctorTokenGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            gvDoctorToken.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvDoctorToken.Visible = true;
                gvDoctorToken.DataSource = DV;
                gvDoctorToken.DataBind();

                
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }


            ds.Clear();
            ds.Dispose();
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {

                    

                    BindGrid();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      DoctorTokenPopup.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvDoctorToken_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvDoctorToken.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvDoctorToken_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      DoctorTokenPopup.gvPTList_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }
    }
}