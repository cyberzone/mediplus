﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoctorTokenPopup.aspx.cs" Inherits="Mediplus.EMR.Registration.DoctorTokenPopup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title>Doctor Token List</title>

</head>
<body>
    <form id="form1" runat="server">
          <meta http-equiv="refresh" content="60" />
    <div>
      <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table width="98%">
                <tr>
                    <td class="PageHeader">Doctor Token List
                    </td>
                    <td align="right" >
                        <input type="button" value="Refresh" class="button orange small" onClick="window.location.reload()"/>
                    </td>
                </tr>
            </table>

          <div style="padding-top: 0px; width: 98%; height: 550px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvDoctorToken" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvDoctorToken_Sorting"
                            EnableModelValidation="True" Width="100%" OnPageIndexChanging="gvDoctorToken_PageIndexChanging" PageSize="100" GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                             <AlternatingRowStyle CssClass="GridAlterRow" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="Doctor Code" SortExpression="HDT_DR_CODE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDrCode" CssClass="label" Font-Bold="true" runat="server" Text='<%# Bind("HDT_DR_CODE") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HPM_PT_FNAME">
                                    <ItemTemplate>
                                            <asp:Label ID="lblDrName" CssClass="label"  Font-Bold="true" runat="server" Text='<%# Bind("HDT_DR_NAME") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Token" SortExpression="HDT_CURRENT_TOKEN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCurToken" CssClass="label"  Font-Bold="true" runat="server" Text='<%# Bind("HDT_CURRENT_TOKEN") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Next Token" SortExpression="HDT_CURRENT_TOKEN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNextToken" CssClass="label"  Font-Bold="true" runat="server" Text='<%# Bind("HDT_NEXT_TOKEN") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
    </div>
    </form>
</body>
</html>
