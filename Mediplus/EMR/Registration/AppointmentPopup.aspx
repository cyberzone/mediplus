﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppointmentPopup.aspx.cs" Inherits="Mediplus.EMR.Registration.AppointmentPopup" %>


<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
       <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <title>Appointment</title>
     <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        // <![CDATA[

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function passvalue(strDate, strPageName, DrId, DeptId) {
            if (strPageName == "ApptDay") {
                opener.location.href = "AppointmentDay.aspx?Date=" + strDate + "&DeptID=" + DeptId;;
            }
            if (strPageName == "ApptWeek") {
                opener.location.href = "AppointmentWeek.aspx?Date=" + strDate + "&DrID=" + DrId;
            }
            if (strPageName == "ApptEmrDr") {
                opener.location.href = "AppointmentEMRDr.aspx?Date=" + strDate + "&DrID=" + DrId;
            }
            opener.focus();
            self.close();

        }


        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }

        function val() {
            var label;
            label = document.getElementById('lblStatus');
            label.style.color = 'red';
            document.getElementById('lblStatus').innerHTML = " ";

            /*
            if (document.getElementById('txtFileNo').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the File No";
                document.getElementById('txtFileNo').focus();
                return false;
            }
            */

            if (document.getElementById('txtFName').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the First Name";
                document.getElementById('txtFName').focus();
                return false;
            }

            if (document.getElementById('txtFromDate').value == "") {
                document.getElementById('lblStatus').innerHTML = "Please enter the Date";
                document.getElementById('txtFromDate').focus();
                return false;
            }


            if (document.getElementById('txtFromDate').value != "") {
                if (isDate(document.getElementById('txtFromDate').value) == false) {
                    document.getElementById('txtFromDate').focus()
                    return false;
                }
            }


            if (document.getElementById('txtEmail').value != "") {
                if (echeck(document.getElementById('txtEmail').value) == false) {
                    document.getElementById('txtEmail').focus();
                    return false;
                }
            }


            return true



        }


        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=Appointment&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin1", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            return true;

        }

        function BindPTDetails(HPM_PT_ID, PatientName, Mobile, Email) {
            document.getElementById('txtFileNo').value = HPM_PT_ID;
            document.getElementById('txtFName').value = PatientName;
            document.getElementById('txtMobile').value = Mobile;
            document.getElementById('txtEmail').value = Email;
        }



        function ServIdSelected() {
            if (document.getElementById('txtServCode').value != "") {
                var Data = document.getElementById('txtServCode').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('txtServCode').value = Data1[0];
                    document.getElementById('txtServName').value = Data1[1];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('txtServName').value != "") {
                var Data = document.getElementById('txtServName').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('txtServCode').value = Data1[0];
                    document.getElementById('txtServName').value = Data1[1];
                }
            }

            return true;
        }

    </script>

      <style>
          .AutoExtender
          {
              font-family: Verdana, Helvetica, sans-serif;
              font-size: .8em;
              font-weight: normal;
              border: solid 1px #006699;
              line-height: 20px;
              padding: 10px;
              background-color: White;
              margin-left: 10px;
          }

          .AutoExtenderList
          {
              border-bottom: dotted 1px #006699;
              cursor: pointer;
              color: Maroon;
          }

          .AutoExtenderHighlight
          {
              color: White;
              background-color: #006699;
              cursor: pointer;
          }

          #divwidth
          {
              width: 400px !important;
          }

              #divwidth div
              {
                  width: 400px !important;
              }
      </style>
</head>
<body  >
    <form id="form1" runat="server">
        <div>
              <input type="hidden" id="hidPermission" runat="server" value="9" />
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
             <asp:HiddenField ID="hidStatus" runat="server" />
            <div style="padding-top: 0px;">
               
                <table >
                <tr>
                    <td class="PageHeader">Appointment
                        <asp:CheckBox ID="chkOTAppointment" visible="false" runat="server" CssClass="lblCaption1" Text="OT Appointment" OnCheckedChanged="chkOTAppointment_CheckedChanged" Checked="false" AutoPostBack="true" />

                    </td>
                </tr>
            </table>
                <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%"">
                    <tr>
                        <td>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </td>
                    </tr>

                </table>
                <table width="100%">
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label1" runat="server" CssClass="lblCaption1"
                                Text="File No"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFileNo" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="200px" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" CssClass="lblCaption1"
                                Text="Service"></asp:Label>

                        </td>
                        <td>
                             <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="" Checked="false" />
                            <asp:DropDownList ID="drpService" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="290px" AutoPostBack="true" OnSelectedIndexChanged="drpService_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label3" runat="server" CssClass="lblCaption1"
                                Text="First Name"></asp:Label>
                        </td>
                        <td>
                                 <asp:Label ID="lblFullName" runat="server" CssClass="label"  visible="false" ></asp:Label>
                            <asp:TextBox ID="txtFName" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="Red" Width="200px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" CssClass="lblCaption1"
                                Text="Status"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpStatus" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="315px" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label5" runat="server" CssClass="lblCaption1"
                                Text="Last Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLName" runat="server" CssClass="label" BorderWidth="1px"  BackColor="#e3f7ef" BorderColor="#cccccc" Width="200px" ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label6" runat="server" CssClass="lblCaption1"
                                Text="Doctor"></asp:Label>

                        </td>
                        <td>
                            <asp:DropDownList ID="drpDoctor" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="315px">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label7" runat="server" CssClass="lblCaption1"
                                Text="Mobile"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMobile" runat="server" CssClass="label"  BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" Width="200px"  ondblclick="return PatientPopup1('Mobile1',this.value);"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" CssClass="lblCaption1"
                                Text="Date"></asp:Label>

                        </td>
                        <td class="label">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="72px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="red" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                                enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                            </asp:calendarextender>
                            <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                           
                          
                        </td>
                      
                  
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label9" runat="server" CssClass="lblCaption1"
                                Text="Email"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                        </td>
                       <td class="lblCaption1">
                            Time
                       </td>
                        <td class="lblCaption1">
                        <asp:DropDownList ID="drpSTHour" CssClass="label" style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpSTMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>         
                            <asp:DropDownList ID="drpSTAM" runat="server" visible="false" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                               To
                            <asp:DropDownList ID="drpFIHour" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpFIMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>
                            <asp:DropDownList ID="drpFIAM" runat="server" visible="false"  CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                        </td>
                        
                    </tr>
                        <tr>
 <td>
                            <asp:Label ID="Label10" runat="server" CssClass="lblCaption1"
                                Text="Remarks"></asp:Label>

                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtRemarks" runat="server" Width="580px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" TextMode="MultiLine" Height="30px"></asp:TextBox>
                        </td>

                        </tr>
                      <tr>
                       
                        <td>
                            <asp:Label ID="Label12" runat="server" CssClass="lblCaption1"
                                Text="Service"></asp:Label>

                        </td>
                        <td colspan="3">
                           <asp:Label ID="lblServices" runat="server" CssClass="lblCaption1"
                                ></asp:Label>
                        </td>
                    </tr>
                    <div id="divOTAppointment" runat="server" visible="false">
                     <tr>
                         <td class="lblCaption1" style="height: 30px;">Procedure 
                        </td>
                        <td   colspan="3" >
                
                            <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" onblur="return ServIdSelected()"></asp:TextBox>
                        
                        
                            <asp:TextBox ID="txtServName" runat="server" Width="500px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                            <div id="divwidth" style="visibility: hidden;"></div>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                            </asp:AutoCompleteExtender>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="height: 30px;">Surgery Type 
                        </td>
                        <td>
                                <asp:DropDownList ID="drpSurgeryType" runat="server"  CssClass="label"    BorderWidth="1px"  BorderColor="#cccccc" Width="200px" >
                                     <asp:ListItem Value="" >--- Select ---</asp:ListItem>
                                     <asp:ListItem Value="Elective">Elective</asp:ListItem>
                                     <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                                     <asp:ListItem Value="Normal">Normal</asp:ListItem>
                                </asp:DropDownList>

                         </td>
                         <td class="lblCaption1" style="height: 30px;"> Anesthesia Type
                        </td>
                        <td>
                             <asp:DropDownList ID="drpAnesthesia" runat="server"  CssClass="label"    BorderWidth="1px"  BorderColor="#cccccc" Width="315px" >
                                     <asp:ListItem Value="" >--- Select ---</asp:ListItem>
                                     <asp:ListItem Value="General">General</asp:ListItem>
                                     <asp:ListItem Value="Local">Local</asp:ListItem>
                              </asp:DropDownList>

                         </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="height: 30px;">Anesthetist 
                        </td>
                        <td >
                             <asp:DropDownList ID="drpAnesthetist" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="200px">
                            </asp:DropDownList>
                         </td>
                        <td class="lblCaption1">
                            Time In
                       </td>
                        <td class="lblCaption1">
                        <asp:DropDownList ID="drpTimeInHour" CssClass="label" style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpTimeInMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>         
                            <asp:DropDownList ID="drpTimeInAM" runat="server" visible="false" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                               Out
                            <asp:DropDownList ID="drpTimeOutHour" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                            </asp:DropDownList>
                            :
                             <asp:DropDownList ID="drpTimeOutMin" CssClass="label"  style="font-size:10px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px">
                             </asp:DropDownList>
                            <asp:DropDownList ID="drpTimeOutAM" runat="server" visible="false"  CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="55px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                        </td>
                    </tr>
                        <tr>
                        <td class="lblCaption1" style="height: 30px;">Special Request
                        </td>
                        <td colspan="3">
                           <asp:TextBox ID="txtSpecialRequest" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" Width="580px"  TextMode="MultiLine" Height="30px" ></asp:TextBox>
                         </td>
                        </tr>
                    </div>
                    <tr>
                       <td></td>
                        <td colspan="2">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return val();" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button orange small" Width="70px" OnClick="btnCancel_Click" ToolTip="Cancel the Appointment" />
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="button orange small" Width="70px"  OnClick="btnDelete_Click" />
                         
                           
                            
                        </td>
                        <td>
                             <asp:Button ID="btnSendSMS" runat="server" Text="Send SMS" CssClass="button orange small" Width="90px"  OnClick="btnSendSMS_Click"/>
                             <asp:Button ID="btnSendMail" runat="server" Text="Send Mail" CssClass="button orange small" Width="90px"  OnClick="btnSendMail_Click"/>
                             <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button orange small" Width="90px" OnClientClick="window.close()" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 50px;"></td>
                    </tr>
                </table>
                <div style="padding-left: 0px; padding-top: 0px; width: 100%; height: 50px; overflow: auto; border: thin; border-color: #000; border-style: groove;">
                <table style="width: 100%; border: 0px;" cellpadding="0px" cellspacing="0px">
                    <tr style="background-color: #4fbdf0;">
                        <td class="label" style="width: 200px; color: #ffffff;">Created User</td>
                        <td class="label" style="width: 100px; color: #ffffff;">Created Date</td>
                        <td class="label" style="width: 200px; color: #ffffff;">Modified User</td>
                        <td class="label" style="width: 100px; color: #ffffff;">Modified Date</td>

                    </tr>
                    <tr>
                        <td colspan="4" style="height: 5px;"></td>
                    </tr>
                    <tr>

                        <td class="label">
                            <asp:Label ID="lblCreatedUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="label">
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="label">
                            <asp:Label ID="lblModifiedUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                        </td>

                    </tr>
                </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
