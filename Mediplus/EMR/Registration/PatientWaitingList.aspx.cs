﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.Registration
{
    public partial class PatientWaitingList : System.Web.UI.Page
    {
        # region Variable Declaration

        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEMR_PTMast = new EMR_PTMasterBAL();


        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }

            drpDoctor.Items.Insert(0, "--- All ---");
            drpDoctor.Items[0].Value = "0";


        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";//AND HPV_TYPE='OP' 


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            if (GlobalValues.FileDescription == "ITTIHAD")
            {
                if (txtSrcFileNo.Text.Trim() == "" && txtSrcMobile1.Text.Trim() == "")
                {
                    if (txtFromDate.Text != "")
                    {
                        Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
                    }
                }
            }
            else
            {
                if (txtFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
                }

            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (GlobalValues.FileDescription == "ITTIHAD" && txtSrcFileNo.Text.Trim() == "" && txtSrcMobile1.Text.Trim() == "")
            {
                if (txtSrcFileNo.Text.Trim() == "" && txtSrcMobile1.Text.Trim() == "")
                {
                    if (txtToDate.Text != "")
                    {
                        Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
                    }
                }
            }
            else
            {

                if (txtToDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
                }
            }

            if (txtSrcFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID like '%" + txtSrcFileNo.Text.Trim() + "%'";

            }

            if (txtSrcName.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_NAME LIKE '%" + txtSrcName.Text.Trim() + "%'";
            }

            if (txtSrcMobile1.Text.Trim() != "")
            {
                Criteria += " AND HPV_MOBILE= '" + txtSrcMobile1.Text.Trim() + "'";
            }

            //BELOW CHECKING FOR SOME VISIT DETAILS NOT DISPLAY FOR DOCTORS ONLY CODER CAN SEE THIS VISIT IN EMR. FOR THAT PT REGISTRATION TAB 3 OTHER-1 TEXT BOX THEY WILL GIE 'N'.
            if (GlobalValues.FileDescription == "KHALID" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS")
            {
                Criteria += " AND RTRIM(LTRIM(HPM_OTHERINFO5)) <> 'N'";
            }

            //if (drpStatus.SelectedValue == "W")
            //{
            //    //Criteria += "AND HPV_STATUS <>'F'   AND HPV_STATUS <>'C' AND HPV_STATUS <>'D'    AND ( HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='' ) ";
            //    Criteria += "AND ( HPV_STATUS  IS NULL OR  HPV_STATUS='W' )   AND ( HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='' ) ";
            //}

            //if (drpStatus.SelectedValue == "W")
            //{
            //    Criteria += " AND ( HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='' ) ";
            //}

            if (GlobalValues.FileDescription == "ALANAMEL")
            {

                if (drpEMRClinicStatus.SelectedValue == "Waiting")
                {
                    Criteria += " AND ( HPV_EMR_CLINIC_STATUS  IS NULL OR  HPV_EMR_CLINIC_STATUS='Waiting' )  ";
                }
                //else if (drpEMRClinicStatus.SelectedValue == "Completed")
                //{
                //    Criteria += " AND ( HPV_EMR_CLINIC_STATUS='Completed' OR  HPV_EMR_STATUS='Y'  )  ";
                //}
                else if (drpEMRClinicStatus.SelectedValue == "")
                {

                }
                else
                {

                    Criteria += " AND  HPV_EMR_CLINIC_STATUS='" + drpEMRClinicStatus.SelectedValue + "'";
                }
            }
            else
            {

                if (drpStatus.SelectedValue != "")
                {

                    if (drpStatus.SelectedValue == "W")
                    {
                        //Criteria += "AND HPV_STATUS <>'F'   AND HPV_STATUS <>'C' AND HPV_STATUS <>'D'    AND ( HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='' ) ";
                        Criteria += "AND ( HPV_STATUS  IS NULL OR  HPV_STATUS='W'  OR  HPV_STATUS='F' )   AND ( HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='' ) ";
                    }
                    if (drpStatus.SelectedValue == "C")
                    {
                        Criteria += " AND (HPV_EMR_STATUS='Y' ) ";
                    }
                    if (drpStatus.SelectedValue == "P")
                    {
                        Criteria += " AND (HPV_EMR_STATUS='P' ) ";
                    }
                    if (drpStatus.SelectedValue == "I")
                    {
                        Criteria += " AND (HPV_STATUS='I' ) ";
                    }

                    if (drpStatus.SelectedValue == "V")
                    {
                        Criteria += " AND (HPV_STATUS='V' ) ";
                    }
                }


                if (drpPatientType.SelectedIndex != 0)
                {
                    if (drpPatientType.SelectedIndex == 1)
                    {
                        Criteria += " AND ( HPV_PT_TYPE ='CA' OR  HPV_PT_TYPE ='CASH') ";
                    }
                    else if (drpPatientType.SelectedIndex == 2)
                    {
                        Criteria += " AND ( HPV_PT_TYPE ='CR' OR  HPV_PT_TYPE ='CREDIT') ";
                    }

                }

            }
            if (drpSrcCompany.SelectedIndex != 0)
            {
                if (Convert.ToString(ViewState["NetworkdIDs"]) != null && Convert.ToString(ViewState["NetworkdIDs"]) != "")
                {
                    Criteria += " AND ( HPV_COMP_ID = '" + drpSrcCompany.SelectedValue + "' OR   HPV_COMP_ID in(" + Convert.ToString(ViewState["NetworkdIDs"]) + "))";
                }
                else
                {
                    Criteria += " AND HPV_COMP_ID = '" + drpSrcCompany.SelectedValue + "'";
                }

            }

            if (Convert.ToString(ViewState["DISPLAY_OTHER_DR_WAITING"]) != "1")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    // HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='' 

                    if (Convert.ToString(Session["EMR_COMMOM_DR_ID"]) != "")
                    {
                        Criteria += " AND  ( HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR  (HPV_DR_ID='" + Convert.ToString(Session["EMR_COMMOM_DR_ID"]) + "' AND HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='')) ";

                    }
                    else
                    {
                        Criteria += " AND   HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "'";

                    }

                }
            }
            else
            {

                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST" && Convert.ToString(Session["User_DeptID"]) == "DENTAL")
                {
                    Criteria += " AND  ( HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR   HPV_EMR_STATUS  IS NULL OR  HPV_EMR_STATUS='')  AND HPV_DEP_NAME='DENTAL' ";
                }
                else if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST" && Convert.ToString(Session["User_DeptID"]) != "DENTAL")
                {
                    Criteria += " AND   HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "'";

                }
            }

            if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                {
                    Criteria += " AND HPV_EMR_ID IN (SELECT DISTINCT  HPV_EMR_ID  FROM HMS_PATIENT_VISIT INNER JOIN EMR_PT_INSTRUCTION ON CAST(HPV_EMR_ID AS VARCHAR)  =CAST(EPC_ID AS VARCHAR) WHERE " + Criteria + ")";
                }
            }


            if (Convert.ToString(Session["User_Category"]).ToUpper() == "RADIOLOGIST")
            {
                Criteria += " AND HPV_EMR_ID IN (SELECT DISTINCT  HPV_EMR_ID  FROM HMS_PATIENT_VISIT INNER JOIN EMR_PT_RADIOLOGY ON CAST(HPV_EMR_ID AS VARCHAR)  =CAST(EPR_ID AS VARCHAR) WHERE " + Criteria + ")";
            }

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "PATHOLOGIST")
            {
                Criteria += " AND HPV_EMR_ID IN (SELECT DISTINCT  HPV_EMR_ID  FROM HMS_PATIENT_VISIT INNER JOIN EMR_PT_LABORATORY  ON CAST(HPV_EMR_ID AS VARCHAR)  =CAST(EPL_ID AS VARCHAR) WHERE " + Criteria + ")";
            }

            if (drpDoctor.SelectedIndex != 0)
            {
                if (Convert.ToString(Session["EMR_COMMOM_DR_ID"]) != "" || Convert.ToString(ViewState["DISPLAY_OTHER_DR_WAITING"]) == "1" || Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS" || Convert.ToString(Session["User_Category"]).ToUpper() == "RADIOLOGIST" || Convert.ToString(Session["User_Category"]).ToUpper() == "PATHOLOGIST")
                {

                    Criteria += " AND HPV_DR_ID='" + drpDoctor.SelectedValue + "'";
                }


            }
            DataSet DS = new DataSet();

            //if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
            //{
            //    DS = objCom.PatientVisitNursingOrderGet(Criteria);
            //}
            //else
            //{
            DS = objCom.PatientVisitGet(Criteria);
            //  }
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            // TextFileWriting(Criteria);
            //TextFileWriting( Convert.ToString(DS.Tables[0].Rows.Count));
            lblTotal.Text = "0";

            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();
                lblTotal.Text = DS.Tables[0].Rows.Count.ToString();

                if (Convert.ToString(ViewState["TOKEN_DISPLAY"]) == "1")
                {

                    gvGridView.Columns[0].Visible = true;

                }


            }

            // if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS" || Convert.ToString(Session["User_Category"]).ToUpper() == "RADIOLOGIST" || Convert.ToString(Session["User_Category"]).ToUpper() == "PATHOLOGIST")

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
            {
                gvGridView.Columns[3].Visible = true;
            }


            if (GlobalValues.FileDescription == "ALANAMEL")
            {
                gvGridView.Columns[11].Visible = false;
                gvGridView.Columns[12].Visible = true;
            }

            if ((Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTOR") || Convert.ToString(Session["EMR_COMMOM_DR_ID"]) != "" || Convert.ToString(ViewState["DISPLAY_OTHER_DR_WAITING"]) == "1")
            {
                gvGridView.Columns[13].Visible = true;

            }

            if (Convert.ToString(ViewState["EMR_EDIT_OPTION"]) == "1")
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
                {
                    gvGridView.Columns[16].Visible = true;
                }
            }


            Int32 intTotalWaiting = 0, intTotalCompleted = 0, intTotalInProcess = 0;
            if (DS.Tables[0].Rows.Count > 0)
            {


                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["HPV_EMR_STATUS"]) == "" || DR.IsNull("HPV_EMR_STATUS") == true)
                    {
                        intTotalWaiting = intTotalWaiting + 1;
                    }

                    if (Convert.ToString(DR["HPV_EMR_STATUS"]).ToUpper() == "Y")
                    {
                        intTotalCompleted = intTotalCompleted + 1;
                    }

                    if (Convert.ToString(DR["HPV_EMR_STATUS"]).ToUpper() == "P")
                    {
                        intTotalInProcess = intTotalInProcess + 1;
                    }
                }

            }
            lblTotalWaiting.Text = Convert.ToString(intTotalWaiting);
            lblTotalCompleted.Text = Convert.ToString(intTotalCompleted);
            lblTotalInProcess.Text = Convert.ToString(intTotalInProcess);


        }

        void SaveEmrPTMaster(out string EMR_ID)
        {
            EMR_ID = "0";
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            Int32 intCurRow = Convert.ToInt32(ViewState["SelectIndex"]);

            Label lblPatientId = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPatientId");
            Label lblPTName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPTName");
            Label lblAge = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblAge");


            Label lblCompID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblCompID");
            Label lblCompName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblCompName");
            Label lblDrID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrID");
            Label lblDrName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrName");
            Label lblDeptName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDeptName");
            Label lblVisitType = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitType");
            Label lblRefDrCode = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblRefDrCode");
            Label lblVisitDate = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitDate");


            string strAge = "0y0y";
            string strAgeType = "Y";
            string[] arrAge = strAge.Split(' ');
            if (arrAge.Length > 1)
            {
                strAge = arrAge[0];
                strAgeType = arrAge[1];
            }


            objEMR_PTMast = new EMR_PTMasterBAL();
            objEMR_PTMast.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objEMR_PTMast.EPM_ID = "0";
            objEMR_PTMast.EPM_DATE = strFromDate.ToString("dd/MM/yyyy");
            objEMR_PTMast.EPM_PT_ID = lblPatientId.Text;
            objEMR_PTMast.EPM_PT_NAME = lblPTName.Text;
            objEMR_PTMast.EPM_AGE = strAge;// lblAge.Text;
            objEMR_PTMast.EPM_AGE_TYPE = strAgeType; //"Y";
            objEMR_PTMast.EPM_DR_CODE = lblDrID.Text;
            objEMR_PTMast.EPM_DR_NAME = lblDrName.Text;
            objEMR_PTMast.EPM_INS_CODE = lblCompID.Text;
            objEMR_PTMast.EPM_INS_NAME = lblCompName.Text;

            objEMR_PTMast.EPM_START_DATE = strFromDate.ToString("dd/MM/yyyy"); ;
            objEMR_PTMast.EPM_END_DATE = strFromDate.ToString("dd/MM/yyyy");
            objEMR_PTMast.EPM_DEP_ID = lblDeptName.Text;
            objEMR_PTMast.EPM_DEP_NAME = lblDeptName.Text;
            objEMR_PTMast.EMR_PT_TYPE = lblVisitType.Text;
            objEMR_PTMast.EPM_TYPE = "OP";
            objEMR_PTMast.EPM_REF_DR_CODE = lblRefDrCode.Text;
            objEMR_PTMast.VisitDate = lblVisitDate.Text;
            objEMR_PTMast.StartProcess = hidSrtProc.Value == "true" ? "Y" : "N";
            objEMR_PTMast.UserId = Convert.ToString(Session["User_ID"]);

            objEMR_PTMast.AddEMR_PTMaster(out EMR_ID);


        }

        void SaveEmrPTMasterNew()
        {
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            Int32 intCurRow = Convert.ToInt32(ViewState["SelectIndex"]);

            Label lblPatientId = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPatientId");
            Label lblPTName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPTName");
            Label lblAge = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblAge");


            Label lblCompID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblCompID");
            Label lblCompName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblCompName");
            Label lblDrID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrID");
            Label lblDrName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrName");
            Label lblDeptName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDeptName");
            Label lblVisitType = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitType");
            Label lblRefDrCode = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblRefDrCode");
            Label lblVisitDate = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitDate");


            string strAge = lblAge.Text;
            string strAgeType = "Y";
            string[] arrAge = strAge.Split(' ');

            if (lblAge.Text.ToUpper() == "0Y0Y")
            {
                strAge = "0";
                strAgeType = "Y";
            }
            else
            {

                if (arrAge.Length > 1)
                {
                    strAge = arrAge[0];
                    strAgeType = arrAge[1];
                }
                else
                {
                    if (strAge.LastIndexOf('Y') > 0)
                    {
                        strAge = strAge.Substring(0, strAge.LastIndexOf('Y'));
                    }

                }

            }

            objEMR_PTMast = new EMR_PTMasterBAL();
            objEMR_PTMast.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objEMR_PTMast.EPM_PT_ID = lblPatientId.Text;
            objEMR_PTMast.EPM_PT_NAME = lblPTName.Text;
            objEMR_PTMast.EPM_AGE = strAge;// lblAge.Text;
            objEMR_PTMast.EPM_AGE_TYPE = strAgeType; //"Y";
            objEMR_PTMast.EPM_DR_CODE = lblDrID.Text;
            objEMR_PTMast.EPM_DR_NAME = lblDrName.Text;
            objEMR_PTMast.EPM_INS_CODE = lblCompID.Text;
            objEMR_PTMast.EPM_INS_NAME = lblCompName.Text;

            objEMR_PTMast.EPM_DEP_ID = lblDeptName.Text;
            objEMR_PTMast.EPM_DEP_NAME = lblDeptName.Text;
            objEMR_PTMast.EMR_PT_TYPE = lblVisitType.Text;
            objEMR_PTMast.EPM_TYPE = "OP";
            objEMR_PTMast.EPM_REF_DR_CODE = lblRefDrCode.Text;
            objEMR_PTMast.VisitDate = lblVisitDate.Text;
            objEMR_PTMast.UserId = Convert.ToString(Session["User_ID"]);

            objEMR_PTMast.AddEMR_PTMasterNew();


        }


        void EMRPTStartProcess(string EMR_ID)
        {

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            Int32 intCurRow = Convert.ToInt32(ViewState["SelectIndex"]);

            Label lblPatientId = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPatientId");

            Label lblDrID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrID");

            Label lblVisitDate = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitDate");


            objEMR_PTMast = new EMR_PTMasterBAL();
            objEMR_PTMast.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objEMR_PTMast.EPM_ID = EMR_ID;
            objEMR_PTMast.EPM_PT_ID = lblPatientId.Text;
            objEMR_PTMast.EPM_DR_CODE = lblDrID.Text;
            objEMR_PTMast.VisitDate = lblVisitDate.Text;
            objEMR_PTMast.UserId = Convert.ToString(Session["User_ID"]);
            objEMR_PTMast.EMRPTStartProcess();


        }


        void EMRPTStartProcessNew(string EMR_ID)
        {



            string strDate = "", strTime = "";
            strDate = Convert.ToString(Session["HPV_DATE"]);
            strTime = Convert.ToString(Session["HPV_TIME"]);


            objEMR_PTMast = new EMR_PTMasterBAL();
            objEMR_PTMast.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objEMR_PTMast.EPM_ID = EMR_ID;
            objEMR_PTMast.EPM_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
            objEMR_PTMast.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
            objEMR_PTMast.VisitDate = strDate;// lblVisitDate.Text;
            objEMR_PTMast.VisitDateTime = strDate + " " + strTime;// lblVisitDate.Text;
            objEMR_PTMast.UserId = Convert.ToString(Session["User_ID"]);
            objEMR_PTMast.EMRPTStartProcessNew();


        }

        void UpdateEMRIDInVisit(string EMR_ID)
        {


            string strDate = "";
            strDate = Convert.ToString(Session["HPV_DATE"]);


            objEMR_PTMast = new EMR_PTMasterBAL();
            objEMR_PTMast.EPM_ID = EMR_ID;
            objEMR_PTMast.EPM_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
            objEMR_PTMast.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
            objEMR_PTMast.VisitDate = strDate;// lblVisitDate.Text;
            objEMR_PTMast.EMRUpdateEMRIDInVisit();


        }

        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND HDM_STATUS='A'  ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }

        void BindCompany()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            objCom = new CommonBAL();
            ds = objCom.GetCompanyMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcCompany.DataSource = ds;
                drpSrcCompany.DataValueField = "HCM_COMP_ID";
                drpSrcCompany.DataTextField = "HCM_NAME";
                drpSrcCompany.DataBind();
                drpSrcCompany.Items.Insert(0, "--- All ---");
                drpSrcCompany.Items[0].Value = "0";

            }

            ds.Clear();
            ds.Dispose();

        }

        void BindSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE = '" + drpSrcCompany.SelectedValue + "'";

            DataSet ds = new DataSet();
            objCom = new CommonBAL();


            string strNetworkdIDs = "";
            ViewState["NetworkdIDs"] = "";

            ds = objCom.GetCompanyMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    strNetworkdIDs += "'" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "',";
                }

                strNetworkdIDs = strNetworkdIDs.Substring(0, strNetworkdIDs.Length - 1);
                ViewState["NetworkdIDs"] = strNetworkdIDs;

            }


        }

        Boolean CheckEMRAvailable()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  EPM_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' AND CONVERT(VARCHAR,EPM_DATE,103)='" + Convert.ToString(Session["HPV_DATE"]) + "'";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;

        }

        void GetEMRID(out string EMR_ID)
        {
            EMR_ID = "0";
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  EPM_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' AND CONVERT(VARCHAR,EPM_DATE,103)='" + Convert.ToString(Session["HPV_DATE"]) + "'";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                EMR_ID = Convert.ToString(ds.Tables[0].Rows[0]["EPM_ID"]);

            }


        }

        DataSet GetEMRDtls(string EMR_ID)
        {
            string Criteria1 = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria1 += " AND EPM_ID = " + EMR_ID;

            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria1);

            return DS;
        }

        void GetStaffDtls(string StaffID, out string StaffName, out string DeptName)
        {
            StaffName = "";
            DeptName = "";
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_STAFF_ID='" + StaffID + "'";

            DataSet ds = new DataSet();
            objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                StaffName = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                DeptName = Convert.ToString(ds.Tables[0].Rows[0]["HSFM_DEPT_ID"]);


            }


        }

        void BindNursingtInstruction()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPC_BRANCH_ID	 ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) = CONVERT(datetime,convert(varchar(10),GETDATE(),101),101)";

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" || Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTOR")
            {
                Criteria += " AND EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "'";
            }


            EMR_PTMasterBAL objPTMast = new EMR_PTMasterBAL();

            DS = objPTMast.PTInstructionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                lnkNursingOrder.Text = Convert.ToString(DS.Tables[0].Rows.Count);
            }



        }

        void BindPharmacyHistoryData()
        {

            DataSet ds = new DataSet();
            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();

            string Criteria = " 1=1 ";
            Criteria += " AND EPPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND CONVERT(datetime,convert(varchar(10),EPPI_DATE,101),101) = CONVERT(datetime,convert(varchar(10),GETDATE(),101),101)";

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" || Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTOR")
            {
                Criteria += " AND EPPI_CREATED_USER='" + Convert.ToString(Session["User_ID"]) + "'";
            }

            ds = objCom.PharmacyInternalGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lnkPharmacyInternal.Text = Convert.ToString(ds.Tables[0].Rows.Count);

            }

        }

        void BindSegmentVisitDtlsAllergy()
        {
            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = 'ALLERGY'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";


            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    strValue += Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + ":&nbsp;";
                    strValue += " <span class='lblCaption1'  style='color: #D64535;font-weight: normal; font-size: 11px;' >" + Convert.ToString(DR["ESVD_VALUE"]) + "</span>, ";

                }
            }
            Session["PT_ALLERGY"] = strValue;

        }


        void BindSegmentVisitDtlsSocialHist()
        {
            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = 'HIST_SOCIAL'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";


            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    strValue += Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + ":&nbsp;";
                    strValue += " <span class='lblCaption1'  style='color: #D64535;font-weight: normal; font-size: 11px;' >" + Convert.ToString(DR["ESVD_VALUE"]) + "</span>, ";

                }
            }
            Session["PT_HIST_SOCIAL"] = strValue;

        }




        void BindSegmentVisitDtlsPastHist()
        {
            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = 'HIST_PAST'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";


            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    strValue += Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + ":&nbsp;";
                    strValue += " <span class='lblCaption1'  style='color: #D64535;font-weight: normal; font-size: 11px;' >" + Convert.ToString(DR["ESVD_VALUE"]) + "</span>, ";

                }
            }
            Session["PT_HIST_PAST"] = strValue;

        }



        void Clear()
        {

            Session["EMR_PT_ID"] = "";
            Session["HPV_DR_ID"] = "";
            Session["HPV_DR_NAME"] = "";
            Session["HPV_DEP_NAME"] = "";
            Session["HPV_DEP_ID"] = "";
            Session["HPV_VISIT_TYPE"] = "";
            Session["HPV_COMP_ID"] = "";
            Session["HPV_DATE"] = "";
            Session["HPV_TIME"] = "";
            Session["Photo_ID"] = "";
            Session["HPV_PT_TYPE"] = "";

            Session["HPM_SEX"] = "";
            hidSrtProc.Value = "false";

            Session["PT_ALLERGY"] = "";

        }

        void BindEMRData()
        {
            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "' AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    Session["EPM_DATE"] = Convert.ToString(DR["EPM_DATEDesc"]);
                    Session["EMRCurrentDateDiff"] = Convert.ToString(DR["EMRCurrentDateDiff"]);
                    if (DR.IsNull("EPM_STATUS") == false && Convert.ToString(DR["EPM_STATUS"]) != "")
                    {
                        Session["EPM_STATUS"] = Convert.ToString(DR["EPM_STATUS"]);
                    }
                }

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_WL' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["INCOME_SUMMARY_BTN"] = "0";
            ViewState["MEDICAL_LEAVE_RPT_BTN"] = "0";
            ViewState["RESULT_BTN"] = "0";
            ViewState["APPOINTMENT_BTN"] = "0";
            ViewState["PT_WAITING_ADD_BTN"] = "0";
            ViewState["NEXT_TOKEN_BTN"] = "0";
            ViewState["TOKEN_LIST_BTN"] = "0";
            ViewState["NURSING_ORDER_BTN"] = "0";

            ViewState["COLOR_CHANGE_TIIME1"] = "0";
            ViewState["COLOR_CHANGE_TIIME2"] = "0";

            ViewState["TOKEN_DISPLAY"] = "0";
            ViewState["DISPLAY_OTHER_DR_WAITING"] = "0";
            ViewState["EMR_EDIT_OPTION"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "INCOME_SUMMARY_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["INCOME_SUMMARY_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "MEDICAL_LEAVE_RPT_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["MEDICAL_LEAVE_RPT_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "RESULT_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["RESULT_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "APPOINTMENT_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["APPOINTMENT_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PT_WAITING_ADD_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_WAITING_ADD_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "NEXT_TOKEN_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NEXT_TOKEN_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "TOKEN_LIST_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["TOKEN_LIST_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "NURSING_ORDER_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["NURSING_ORDER_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "COLOR_CHANGE_TIIME1")
                    {
                        if (DR.IsNull("CUST_VALUE") == false && Convert.ToString(DR["CUST_VALUE"]) != "")
                        {
                            ViewState["COLOR_CHANGE_TIIME1"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "COLOR_CHANGE_TIIME2")
                    {
                        if (DR.IsNull("CUST_VALUE") == false && Convert.ToString(DR["CUST_VALUE"]) != "")
                        {
                            ViewState["COLOR_CHANGE_TIIME2"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "TOKEN_DISPLAY")
                    {
                        if (DR.IsNull("CUST_VALUE") == false && Convert.ToString(DR["CUST_VALUE"]) != "")
                        {
                            ViewState["TOKEN_DISPLAY"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "DISPLAY_OTHER_DR_WAITING")
                    {
                        if (DR.IsNull("CUST_VALUE") == false && Convert.ToString(DR["CUST_VALUE"]) != "")
                        {
                            ViewState["DISPLAY_OTHER_DR_WAITING"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_EDIT_OPTION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false && Convert.ToString(DR["CUST_VALUE"]) != "")
                        {
                            ViewState["EMR_EDIT_OPTION"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }



        }

        void BindTime12HrsFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("False");


            drpSTHour.DataSource = DSHours;
            drpSTHour.DataTextField = "Name";
            drpSTHour.DataValueField = "Code";
            drpSTHour.DataBind();

            drpFinHour.DataSource = DSHours;
            drpFinHour.DataTextField = "Name";
            drpFinHour.DataValueField = "Code";
            drpFinHour.DataBind();





            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");

            drpSTMin.DataSource = DSMin;
            drpSTMin.DataTextField = "Name";
            drpSTMin.DataValueField = "Code";
            drpSTMin.DataBind();

            drpFinMin.DataSource = DSMin;
            drpFinMin.DataTextField = "Name";
            drpFinMin.DataValueField = "Code";
            drpFinMin.DataBind();


        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {

                ViewState["SortOrder"] = " HPV_DATE Desc";

                hidFileDesc.Value = GlobalValues.FileDescription.ToUpper();
                Clear();

                if (Convert.ToInt32(Session["HUM_REFRESH_INTERVAL"]) != 0)
                {
                    Timer1.Enabled = true;
                    Timer1.Interval = Convert.ToInt32(Session["HUM_REFRESH_INTERVAL"]) * 60000;
                }



                BindScreenCustomization();
                BindDoctor();
                BindTime12HrsFromDB();

                lblColorChangeTime1.Text = Convert.ToString(ViewState["COLOR_CHANGE_TIIME1"]);
                lblColorChangeTime2.Text = Convert.ToString(ViewState["COLOR_CHANGE_TIIME2"]);

                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    //SetPermission();
                }
                try
                {


                    if (GlobalValues.FileDescription == "SUMMERLAND" || GlobalValues.FileDescription == "ALANAMEL")
                    {
                        lnkNursingOrder.Visible = false;
                        lnkPharmacyInternal.Visible = true;
                        btnSendSMS.Visible = true;
                        drpStatus.Visible = false;
                        drpEMRClinicStatus.Visible = true;
                    }
                    else
                    {
                        lnkNursingOrder.Visible = true;
                        lnkPharmacyInternal.Visible = false;
                    }

                    if (GlobalValues.FileDescription.ToUpper() == "MAMOON")
                    {
                        lblNursingOrder.Text = "Doctor Order :";

                    }


                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" && Convert.ToString(ViewState["PT_WAITING_ADD_BTN"]) == "1")
                    {
                        btnPatientList.Visible = true;
                    }

                    if (Convert.ToString(ViewState["MEDICAL_LEAVE_RPT_BTN"]) == "1")
                    {
                        btnReport.Visible = true;
                    }


                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS" || Convert.ToString(Session["User_Category"]).ToUpper() == "RADIOLOGIST" || Convert.ToString(Session["User_Category"]).ToUpper() == "PATHOLOGIST")
                    {
                        lblDoctor.Visible = true;
                        drpDoctor.Visible = true;
                    }
                    if (Convert.ToString(Session["EMR_COMMOM_DR_ID"]) != "" || Convert.ToString(ViewState["DISPLAY_OTHER_DR_WAITING"]) == "1")
                    {

                        lblDoctor.Visible = true;
                        drpDoctor.Visible = true;

                    }


                    if (Convert.ToString(ViewState["NEXT_TOKEN_BTN"]) == "1" && Convert.ToString(ViewState["TOKEN_LIST_BTN"]) == "1")
                    {
                        if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
                        {
                            btnUpdateToken.Visible = true;
                        }


                        if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
                        {
                            btnDoctorToken.Visible = true;

                        }

                    }


                    if (Convert.ToString(ViewState["RESULT_BTN"]) == "1")
                    {
                        btnResult.Visible = true;
                    }

                    if (Convert.ToString(ViewState["APPOINTMENT_BTN"]) == "1")
                    {
                        btnAppoint.Visible = true;
                    }
                    if (Convert.ToString(ViewState["INCOME_SUMMARY_BTN"]) == "1")
                    {
                        btnDrWiseIncome.Visible = true;
                    }

                    if (GlobalValues.FileDescription == "MAMPILLY" && Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {
                        btnPatientList.Visible = false;
                        btnReport.Visible = false;
                        btnDrWiseIncome.Visible = false;

                    }


                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    if (Convert.ToString(Session["WaitingLIstFromDate"]) != null && Convert.ToString(Session["WaitingLIstFromDate"]) != "")
                    {

                        txtFromDate.Text = Convert.ToString(Session["WaitingLIstFromDate"]);
                    }


                    if (Convert.ToString(Session["WaitingLIstToDate"]) != null && Convert.ToString(Session["WaitingLIstToDate"]) != "")
                    {

                        txtToDate.Text = Convert.ToString(Session["WaitingLIstToDate"]);
                    }


                    if (Convert.ToString(Session["WaitingLIstStatus"]) != null)
                    {
                        if (GlobalValues.FileDescription == "ALANAMEL")
                        {
                            drpEMRClinicStatus.SelectedIndex = Convert.ToInt32(Session["WaitingLIstStatus"]);
                        }
                        else
                        {
                            drpStatus.SelectedIndex = Convert.ToInt32(Session["WaitingLIstStatus"]);
                        }
                    }

                    

                    Session["WaitingLIstFromDate"] = txtFromDate.Text;
                    Session["WaitingLIstToDate"] = txtToDate.Text;
                    if (GlobalValues.FileDescription == "ALANAMEL")
                    {
                        Session["WaitingLIstStatus"] = drpEMRClinicStatus.SelectedIndex;
                    }
                    else
                    {
                        Session["WaitingLIstStatus"] = drpStatus.SelectedIndex;
                    }

                    Session["HPV_INS_VERIFY_STATUS"] = "";



                    BindCompany();
                    BindGrid();
                    BindNursingtInstruction();
                    BindPharmacyHistoryData();


                    if (Convert.ToString(ViewState["TOKEN_DISPLAY"]) == "1")
                    {
                        if (gvGridView.Rows.Count > 0)
                        {
                            gvGridView.Columns[0].Visible = true;
                        }
                        trToken.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

            //while (true)
            //{
            //    BindGrid();
            //    //Console.WriteLine("10 Sec sleeping....");
            //    System.Threading.Thread.Sleep(30000);
            //}

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                Session["WaitingLIstFromDate"] = txtFromDate.Text;
                Session["WaitingLIstToDate"] = txtToDate.Text;
                if (GlobalValues.FileDescription == "ALANAMEL")
                {
                    Session["WaitingLIstStatus"] = drpEMRClinicStatus.SelectedIndex;
                }
                else
                {
                    Session["WaitingLIstStatus"] = drpStatus.SelectedIndex;
                }

                BindGrid();
                BindNursingtInstruction();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvGridView.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            TextFileWriting(System.DateTime.Now.ToString() + "Edit_Click Started");

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;


            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblPTName = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");
            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
            Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
            Label lblDrName = (Label)gvScanCard.Cells[0].FindControl("lblDrName");
            Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");
            Label lblVisitType = (Label)gvScanCard.Cells[0].FindControl("lblVisitType");
            Label lblCompID = (Label)gvScanCard.Cells[0].FindControl("lblCompID");
            Label lblVisitDate = (Label)gvScanCard.Cells[0].FindControl("lblVisitDate");
            Label lblVisitTime = (Label)gvScanCard.Cells[0].FindControl("lblVisitTime");
            Label lblgvVisit_PhotoID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_PhotoID");
            Label lblgvPTType = (Label)gvScanCard.Cells[0].FindControl("lblgvPTType");
            Label lblSex = (Label)gvScanCard.Cells[0].FindControl("lblSex");
            Label lblInsStatus = (Label)gvScanCard.Cells[0].FindControl("lblInsStatus");
            Label lblClinicStatus = (Label)gvScanCard.Cells[0].FindControl("lblClinicStatus");

            Session["EMR_PT_ID"] = lblPatientId.Text;
            Session["EMR_PT_NAME"] = lblPTName.Text;
            Session["HPV_DR_ID"] = lblDrID.Text;
            Session["HPV_DR_NAME"] = lblDrName.Text;
            Session["HPV_DEP_NAME"] = lblDeptName.Text;
            Session["HPV_DEP_ID"] = BindDepID(lblDeptName.Text);
            Session["HPV_VISIT_TYPE"] = lblVisitType.Text;
            Session["HPV_COMP_ID"] = lblCompID.Text;
            Session["HPV_DATE"] = lblVisitDate.Text;
            Session["HPV_TIME"] = lblVisitTime.Text;
            Session["Photo_ID"] = lblgvVisit_PhotoID.Text;
            Session["HPV_PT_TYPE"] = lblgvPTType.Text;
            Session["HPV_INS_VERIFY_STATUS"] = lblInsStatus.Text;
            Session["HPV_EMR_CLINIC_STATUS"] = lblClinicStatus.Text;


            Session["HPM_SEX"] = lblSex.Text;

            TextFileWriting("lblEMR_ID is  : " + lblEMR_ID.Text + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));


            string EMR_ID = lblEMR_ID.Text;

            if (EMR_ID != "")
            {
                string Criteria1 = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria1 += " AND EPM_ID = '" + EMR_ID + "' AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

                DataSet ds1 = new DataSet();
                objEmrPTMast = new EMR_PTMasterBAL();
                ds1 = objEmrPTMast.GetEMR_PTMaster(Criteria1);
                if (ds1.Tables[0].Rows.Count <= 0)
                {
                    TextFileWriting("EMR ID not Matched");
                    GetEMRID(out EMR_ID);
                    if (EMR_ID == "0")
                    {
                        EMR_ID = "";
                        hidSrtProc.Value = "true";
                    }

                }

            }

            try
            {
                if (EMR_ID == "")
                {
                    //SaveEmrPTMaster(out EMR_ID);

                    if (CheckEMRAvailable() == false)
                    {
                        SaveEmrPTMasterNew();
                        TextFileWriting("EMR Created");
                        GetEMRID(out EMR_ID);
                        TextFileWriting("GetEMRID() return this EMR ID  : " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));


                        UpdateEMRIDInVisit(EMR_ID);
                        TextFileWriting("Updated EMR ID in visit table EMR id is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));

                        if (hidSrtProc.Value == "true")
                        {
                            //** STRRT FOLLOWING UPDATION FOR CHANGE THE DOCTOR RELATED DATA FROM COMMON DOCTOR TO EMR LOGED IN DOCTOR. COMMON DR ID MENTION IN SCREENCUSTOMIZATION TABLE
                            if (Convert.ToString(Session["EMR_COMMOM_DR_ID"]) != "" || Convert.ToString(ViewState["DISPLAY_OTHER_DR_WAITING"]) == "1")
                            {
                                if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
                                {
                                    if (Convert.ToString(Session["HPV_DR_ID"]) != Convert.ToString(Session["User_Code"]))
                                    {
                                        string StaffName = "", DeptName = "";
                                        GetStaffDtls(Convert.ToString(Session["User_Code"]), out StaffName, out DeptName);

                                        CommonBAL objCom = new CommonBAL();
                                        string Criteria = "HPV_SEQNO=" + lblSeqNo.Text;
                                        string FieldNameWithValues = " HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "', HPV_DR_NAME ='" + StaffName + "'"
                                             + ",  HPV_DEP_NAME='" + DeptName + "'";
                                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria); ;



                                        Criteria = "HPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                                        FieldNameWithValues = " HPM_DR_ID='" + Convert.ToString(Session["User_Code"]) + "', HPM_DR_NAME ='" + StaffName + "'"
                                             + ",  HPM_DEP_NAME='" + DeptName + "'";
                                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_MASTER", Criteria); ;

                                        Criteria = "EPM_ID='" + EMR_ID + "'";
                                        FieldNameWithValues = " EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "', EPM_DR_NAME ='" + StaffName + "'"
                                             + ",  EPM_DEP_ID='" + DeptName + "', EPM_DEP_NAME='" + DeptName + "'";
                                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria); ;



                                        Session["HPV_DR_ID"] = Convert.ToString(Session["User_Code"]);
                                        Session["HPV_DR_NAME"] = StaffName;
                                        Session["HPV_DEP_NAME"] = DeptName;
                                        Session["HPV_DEP_ID"] = BindDepID(lblDeptName.Text);

                                    }
                                }

                            }
                            //** END  UPDATION FOR CHANGE THE DOCTOR RELATED DATA FROM COMMON DOCTOR TO EMR LOGED IN DOCTOR. COMMON DR ID MENTION IN SCREENCUSTOMIZATION TABLE

                            EMRPTStartProcessNew(EMR_ID);
                            TextFileWriting("Process Started EMR ID is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                        }

                    }
                    else
                    {
                        GetEMRID(out EMR_ID);
                        UpdateEMRIDInVisit(EMR_ID);
                        TextFileWriting("Updated EMR ID in visit table EMR id is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                    }
                }
                else
                {
                    if (hidSrtProc.Value == "true")
                    {
                        //** STRRT FOLLOWING UPDATION FOR CHANGE THE DOCTOR RELATED DATA FROM COMMON DOCTOR TO EMR LOGED IN DOCTOR. COMMON DR ID MENTION IN SCREENCUSTOMIZATION TABLE
                        if (Convert.ToString(Session["EMR_COMMOM_DR_ID"]) != "" || Convert.ToString(ViewState["DISPLAY_OTHER_DR_WAITING"]) == "1")
                        {
                            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS")
                            {
                                if (Convert.ToString(Session["HPV_DR_ID"]) != Convert.ToString(Session["User_Code"]))
                                {
                                    string StaffName = "", DeptName = "";
                                    GetStaffDtls(Convert.ToString(Session["User_Code"]), out StaffName, out DeptName);

                                    CommonBAL objCom = new CommonBAL();
                                    string Criteria = "HPV_SEQNO=" + lblSeqNo.Text;
                                    string FieldNameWithValues = " HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "', HPV_DR_NAME ='" + StaffName + "'"
                                         + ",  HPV_DEP_NAME='" + DeptName + "'";
                                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_VISIT", Criteria); ;



                                    Criteria = "HPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                                    FieldNameWithValues = " HPM_DR_ID='" + Convert.ToString(Session["User_Code"]) + "', HPM_DR_NAME ='" + StaffName + "'"
                                         + ",  HPM_DEP_NAME='" + DeptName + "'";
                                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_PATIENT_MASTER", Criteria); ;

                                    Criteria = "EPM_ID='" + EMR_ID + "'";
                                    FieldNameWithValues = " EPM_DR_CODE='" + Convert.ToString(Session["User_Code"]) + "', EPM_DR_NAME ='" + StaffName + "'"
                                         + ",  EPM_DEP_ID='" + DeptName + "', EPM_DEP_NAME='" + DeptName + "'";
                                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria); ;



                                    Session["HPV_DR_ID"] = Convert.ToString(Session["User_Code"]);
                                    Session["HPV_DR_NAME"] = StaffName;
                                    Session["HPV_DEP_NAME"] = DeptName;
                                    Session["HPV_DEP_ID"] = BindDepID(lblDeptName.Text);

                                }
                            }
                        }
                        //** END  UPDATION FOR CHANGE THE DOCTOR RELATED DATA FROM COMMON DOCTOR TO EMR LOGED IN DOCTOR. COMMON DR ID MENTION IN SCREENCUSTOMIZATION TABLE


                        EMRPTStartProcessNew(EMR_ID);
                        TextFileWriting("Process Started for existing, EMR ID is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                    }
                }

                Session["EMR_ID"] = EMR_ID; //EMR_ID;

                BindEMRData();
                if (Convert.ToString(Session["EMR_ALLERGY_DISPLAY"]) == "1")
                {
                    BindSegmentVisitDtlsAllergy();
                }
                if (Convert.ToString(Session["EMR_HIST_SOCIAL_DISPLAY"]) == "1")
                {
                    BindSegmentVisitDtlsSocialHist();
                }
                if (Convert.ToString(Session["EMR_HIST_PAST_DISPLAY"]) == "1")
                {
                    BindSegmentVisitDtlsPastHist();
                }

                GlobalValues.EMR_ID = EMR_ID;
                GlobalValues.EMR_PT_ID = lblPatientId.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

            if (EMR_ID != "")
            {
                TextFileWriting("EMR ID checking in PT Master  : " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                //DataSet DS = new DataSet();
                //string strCriteria = " 1=1 ";
                //strCriteria += " AND HPV_EMR_ID='" + EMR_ID + "' AND HPV_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                //CommonBAL objCom = new CommonBAL();
                //DS = objCom.PatientVisitGet(strCriteria);
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + EMR_ID + "' AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

                DataSet ds = new DataSet();
                objEmrPTMast = new EMR_PTMasterBAL();
                ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    /*
                    if (GlobalValues.FileDescription.ToUpper() == "ALAMAL".ToUpper())
                    {
                        string strEMR_Date = Convert.ToString(ds.Tables[0].Rows[0]["EPM_DateDesc"]);

                        if (Convert.ToString(Session["HPV_DATE"]) != strEMR_Date)
                        {
                            objCom = new CommonBAL();
                            string FieldNameWithValues = "EPM_Date=convert(datetime,'" + Convert.ToString(Session["HPV_DATE"]) +"',103) ";
                            FieldNameWithValues += ", EPM_START_DATE=convert(datetime,'" + Convert.ToString(Session["HPV_DATE"]) + "',103)";

                            if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]).ToUpper() == "Y")
                            {
                                FieldNameWithValues += ", EPM_END_DATE=convert(datetime,'" + Convert.ToString(Session["HPV_DATE"]) + "',103)";
                            }

                            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);



                        }

                    }
                     * */

                    TextFileWriting("PT Master having data");
                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {
                        Response.Redirect("~/Patient/History.aspx?MenuName=OP_EMR");
                    }
                    else
                    {
                        Response.Redirect("../Patient/PatientDashboard.aspx?MenuName=OP_EMR&PatientID=" + lblPatientId.Text.Trim() + "&EMR_ID=" + EMR_ID);
                    }

                }
            }


        FunEnd: ;
        }

        #endregion

        protected void drpSrcCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubCompany();
        }

        protected void btnUpdateToken_Click(object sender, EventArgs e)
        {
            try
            {

                EMR_PTMasterBAL objPT = new EMR_PTMasterBAL();
                objPT.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPT.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                objPT.UpdateNextToken();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.btnUpdateToken_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnTodayDate_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                drpStatus.SelectedIndex = 0;

                Session["WaitingLIstFromDate"] = txtFromDate.Text;
                Session["WaitingLIstToDate"] = txtToDate.Text;
                if (GlobalValues.FileDescription == "ALANAMEL")
                {
                    Session["WaitingLIstStatus"] = drpEMRClinicStatus.SelectedIndex;
                }
                else
                {
                    Session["WaitingLIstStatus"] = drpStatus.SelectedIndex;
                }

                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.btnTodayDate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            BindGrid();
        }

       

        protected void btnShowToken_Click(object sender, EventArgs e)
        {
            try
            {
                if (hidTokenPTID.Value == "")
                {
                    goto FunEnd;
                }

                if (txtSelectedToken.Text.Trim() == "")
                {
                    goto FunEnd;
                }



                objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.PT_ID = hidTokenPTID.Value;
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DR_NAME = Convert.ToString(Session["FullName"]);

                objCom.ETD_CURRENT_TOKEN = txtSelectedToken.Text.Trim();
                objCom.ETD_DR_ROOM_NO = Convert.ToString(Session["HSFM_ROOM"]);
                objCom.ETD_STATUS = "In Process";
                objCom.UserID = Convert.ToString(Session["User_ID"]);
                objCom.EMRTokenDisplayAdd();


            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.btnShowToken_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatusValue = (Label)e.Row.FindControl("lblStatusValue");
                Label lblType = (Label)e.Row.FindControl("lblType");
                Label lblVisitDate = (Label)e.Row.FindControl("lblVisitDate");
                Label lblVisitDateTime = (Label)e.Row.FindControl("lblVisitDateTime");
                Label lblVisitTime = (Label)e.Row.FindControl("lblVisitTime");

                Label lblPTWaitingTime = (Label)e.Row.FindControl("lblPTWaitingTime");
                Label lblClinicStatus = (Label)e.Row.FindControl("lblClinicStatus");

                Int64 intPTWaitingTime = Convert.ToInt64(lblPTWaitingTime.Text);



                string strStatus = "";

                if (GlobalValues.FileDescription == "ALANAMEL")
                {
                    strStatus = lblClinicStatus.Text;

                }
                else
                {
                    strStatus = lblType.Text;
                }



                if (strStatus.ToLower() == "waiting".ToLower())
                {


                    Int64 ColorChangeTime1 = 0, ColorChangeTime2 = 0;

                    ColorChangeTime1 = Convert.ToInt64(ViewState["COLOR_CHANGE_TIIME1"]);
                    ColorChangeTime2 = Convert.ToInt64(ViewState["COLOR_CHANGE_TIIME2"]);

                    if (ColorChangeTime1 != 0 && ColorChangeTime2 != 0)
                    {


                        if (intPTWaitingTime >= ColorChangeTime1 && intPTWaitingTime < ColorChangeTime2)
                        {
                            lblVisitDate.Style.Add("color", "#FFA500");
                            lblVisitDateTime.Style.Add("color", "#FFA500");
                            lblVisitTime.Style.Add("color", "#FFA500");


                            lblVisitDate.Style.Add("font-weight", "bold");
                            lblVisitDateTime.Style.Add("font-weight", "bold");
                            lblVisitTime.Style.Add("font-weight", "bold");
                        }
                        else if (intPTWaitingTime >= ColorChangeTime2)
                        {
                            lblVisitDate.Style.Add("color", "#FF0000"); //;FF3333
                            lblVisitDateTime.Style.Add("color", "#FF0000");
                            lblVisitTime.Style.Add("color", "#FF0000");

                            lblVisitDate.Style.Add("font-weight", "bold");
                            lblVisitDateTime.Style.Add("font-weight", "bold");
                            lblVisitTime.Style.Add("font-weight", "bold");

                        }
                    }

                }
            }
        }

        protected void EditEMR_Click(object sender, EventArgs e)
        {
            try
            {
                lblEditFileNo.Text = "";
                lblEditName.Text = "";

                txtEditEMRDate.Text = "";
                txtEditEMRSt.Text = "";
                if (drpSTHour.Items.Count > 0)
                    drpSTHour.SelectedIndex = 0;

                if (drpSTMin.Items.Count > 0)
                    drpSTMin.SelectedIndex = 0;

                drpSTAM.SelectedIndex = 0;

                txtEditEMREnd.Text = "";
                if (drpFinHour.Items.Count > 0)
                    drpFinHour.SelectedIndex = 0;

                if (drpFinMin.Items.Count > 0)
                    drpFinMin.SelectedIndex = 0;

                drpFinAM.SelectedIndex = 0;

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowEMREditDisplayPopup()", true);
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;


                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
                Label lblPTName = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
                Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");
                Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
                Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
                Label lblDrName = (Label)gvScanCard.Cells[0].FindControl("lblDrName");
                Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");
                Label lblVisitType = (Label)gvScanCard.Cells[0].FindControl("lblVisitType");
                Label lblCompID = (Label)gvScanCard.Cells[0].FindControl("lblCompID");
                Label lblVisitDate = (Label)gvScanCard.Cells[0].FindControl("lblVisitDate");
                Label lblVisitTime = (Label)gvScanCard.Cells[0].FindControl("lblVisitTime");
                Label lblgvVisit_PhotoID = (Label)gvScanCard.Cells[0].FindControl("lblgvVisit_PhotoID");
                Label lblgvPTType = (Label)gvScanCard.Cells[0].FindControl("lblgvPTType");
                Label lblSex = (Label)gvScanCard.Cells[0].FindControl("lblSex");
                Label lblInsStatus = (Label)gvScanCard.Cells[0].FindControl("lblInsStatus");
                Label lblClinicStatus = (Label)gvScanCard.Cells[0].FindControl("lblClinicStatus");

                lblEditFileNo.Text = lblPatientId.Text;
                lblEditName.Text = lblPTName.Text;

                ViewState["EMR_ID"] = lblEMR_ID.Text;
                ViewState["PT_ID"] = lblPatientId.Text;
                ViewState["HPV_SEQNO"] = lblSeqNo.Text;

                if (lblEMR_ID.Text != "" && lblEMR_ID.Text != null)
                {
                    DataSet DS = new DataSet();
                    DS = GetEMRDtls(lblEMR_ID.Text);

                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {

                        if (DR.IsNull("EPM_DATE") == false && Convert.ToString(DR["EPM_DATE"]) != "")
                        {
                            txtEditEMRDate.Text = Convert.ToString(DR["EPM_DATEDesc"]);
                        }



                        if (DR.IsNull("EPM_START_DATE") == false && Convert.ToString(DR["EPM_START_DATE"]) != "")
                        {

                            string strTimeUp = Convert.ToString(DR["EPM_START_DATEDesc"]);

                            string[] arrTimeUp = strTimeUp.Split(' ');
                            if (arrTimeUp.Length > 1)
                            {
                                txtEditEMRSt.Text = arrTimeUp[0];

                                drpSTAM.SelectedValue = arrTimeUp[2];

                                string[] arrTimeUp1 = arrTimeUp[1].Split(':');

                                if (arrTimeUp1.Length > 1)
                                {
                                    drpSTHour.SelectedValue = arrTimeUp1[0];
                                    drpSTMin.SelectedValue = arrTimeUp1[1];
                                }

                            }


                        }


                        if (DR.IsNull("EPM_END_DATE") == false && Convert.ToString(DR["EPM_END_DATE"]) != "")
                        {



                            string strFinished = Convert.ToString(DR["EPM_END_DATEDesc"]);

                            string[] arrFinished = strFinished.Split(' ');
                            if (arrFinished.Length > 1)
                            {
                                txtEditEMREnd.Text = arrFinished[0];

                                drpFinAM.SelectedValue = arrFinished[2];



                                string[] arrFinished1 = arrFinished[1].Split(':');

                                if (arrFinished1.Length > 1)
                                {
                                    drpFinHour.SelectedValue = arrFinished1[0];
                                    drpFinMin.SelectedValue = arrFinished1[1];
                                }

                            }
                        }
                    }

                }



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.DeleteService_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btnEMRUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(ViewState["EMR_ID"]) == "" || Convert.ToString(ViewState["EMR_ID"]) == null)
                {
                    goto FunEnd;
                }

                if (Convert.ToString(ViewState["PT_ID"]) == "" || Convert.ToString(ViewState["PT_ID"]) == null)
                {
                    goto FunEnd;
                }
                //if (Convert.ToString(ViewState["HPV_SEQNO"]) == "" || Convert.ToString(ViewState["HPV_SEQNO"]) == null)
                //{
                //    goto FunEnd;
                //}



                string strEMRStTime = txtEditEMRSt.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00" + " " + drpSTAM.SelectedValue;
                string strEMRFinTime = txtEditEMREnd.Text.Trim() + " " + drpFinHour.SelectedValue + ":" + drpFinMin.SelectedValue + ":00" + " " + drpFinAM.SelectedValue;

                string Criteria = " 1=1 AND  EPM_PT_ID='" + Convert.ToString(ViewState["PT_ID"]) + "' AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' AND EPM_ID = " + Convert.ToString(ViewState["EMR_ID"]);


                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPM_DATE=CONVERT(DATETIME,'" + txtEditEMRDate.Text.Trim() + "',103),EPM_START_DATE=CONVERT(DATETIME,'" + strEMRStTime + "',103),EPM_END_DATE=CONVERT(DATETIME,'" + strEMRFinTime + "',103)";
                FieldNameWithValues += ",EPM_MODIFIED_USER='" + Convert.ToString(Session["User_ID"]) + "',EPM_MODIFIED_DATE=getdate()";


                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);

                //AND HPV_SEQNO='" + Convert.ToString(ViewState["HPV_SEQNO"]) + "'
                objCom = new CommonBAL();
                string Criteria1 = " 1=1   AND HPV_PT_ID='" + Convert.ToString(ViewState["PT_ID"]) + "'  AND HPV_EMR_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";

                string FieldNameWithValues1 = "HPV_DATE=CONVERT(DATETIME,'" + strEMRStTime + "',103),HPV_TIME='" + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00" + " " + drpSTAM.SelectedValue + "'";

                objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_PATIENT_VISIT", Criteria1);

                TextFileWriting(System.DateTime.Now.ToString() + "Following EMR Updated EMR ID is " + Convert.ToString(ViewState["EMR_ID"]) + " PT ID is " + Convert.ToString(ViewState["PT_ID"]) + " HPV_SEQNO IS " + Convert.ToString(ViewState["HPV_SEQNO"]));

                BindGrid();
            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.btnEMRUpdate_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}