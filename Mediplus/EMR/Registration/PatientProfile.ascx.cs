﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;

namespace Mediplus.EMR.Registration
{
    public partial class PatientProfile : System.Web.UI.UserControl
    {
        CommonBAL objCom = new CommonBAL();
        public string PT_ID { set; get; }

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindPTProfile()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND   HPM_PT_ID = '" + PT_ID + "' ";




            DataSet DS = new DataSet();


            CommonBAL com = new CommonBAL();
            DS = com.PatientMasterGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            if (DS.Tables[0].Rows.Count > 0)
            {
                lblFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]);
                lblName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
            }

        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {


                    BindPTProfile();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientProfile.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }
    }
}