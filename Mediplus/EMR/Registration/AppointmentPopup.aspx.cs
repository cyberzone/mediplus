﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
 
using EMR_BAL;


namespace Mediplus.EMR.Registration
{
    public partial class AppointmentPopup : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

       
        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    ViewState["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    ViewState["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    ViewState["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    ViewState["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    ViewState["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    ViewState["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "24";
                }


            }


        }

        void GetAppointment()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HAM_APPOINTMENTID='" + Convert.ToString(ViewState["AppId"]) + "'";


            CommonBAL dbo = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            string[] arrAppDtls = { " " };

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_FILENUMBER") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_FILENUMBER"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_PT_NAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]) != "")
                {
                    txtFName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_PT_NAME"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_LASTNAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]) != "")
                {
                    txtLName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_LASTNAME"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MOBILENO") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILENO"]) != "")
                {
                    txtMobile.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MOBILENO"]);
                }
                else
                {
                    txtMobile.Text = "";
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_EMAIL") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_EMAIL"]) != "")
                {
                    txtEmail.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_EMAIL"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentDate") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentDate"]) != "")
                {
                    txtFromDate.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentDate"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentSTTime") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]) != "")
                {

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadStartTime12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]));
                    }
                    else
                    {
                        LoadStartTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentSTTime"]));
                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("AppintmentFITime") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]) != "")
                {
                    LoadFinishTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadFinishTime12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));
                    }
                    else
                    {
                        LoadFinishTime(Convert.ToString(DSAppt.Tables[0].Rows[0]["AppintmentFITime"]));
                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_REMARKS") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]) != "")
                {
                    txtRemarks.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_REMARKS"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_DR_CODE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_DR_CODE"]) != "")
                {
                    drpDoctor.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_DR_CODE"]);
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SERVICE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) != "")
                {
                    if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SERVICE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) != "" && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]) != "0")
                    {
                        for (int intCount = 0; intCount < drpService.Items.Count; intCount++)
                        {
                            if (drpService.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]))
                            {
                                drpService.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]);
                                goto ForMStatus;
                            }

                        }
                    }
                ForMStatus: ;


                    lblServices.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SERVICE"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_STATUS") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]) != "")
                {
                    drpStatus.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_STATUS"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_CREATEDUSER") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATEDUSER"]) != "")
                {
                    lblCreatedUser.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATEDUSER"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_CREATED_DATE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_CREATED_DATE"]) != "")
                {
                    DateTime dtCreatedDate;
                    dtCreatedDate = Convert.ToDateTime(DSAppt.Tables[0].Rows[0]["HAM_CREATED_DATE"]);

                    lblCreatedDate.Text = dtCreatedDate.ToString("dd/MM/yyyy HH:mm");
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_MODIFIED_DATE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_DATE"]) != "")
                {
                    DateTime dtModifiedDate;
                    dtModifiedDate = Convert.ToDateTime(DSAppt.Tables[0].Rows[0]["HAM_MODIFIED_DATE"]);
                    lblModifiedDate.Text = dtModifiedDate.ToString("dd/MM/yyyy HH:mm");
                }


                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_OTAPPOINTMENT") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_OTAPPOINTMENT"]) != "")
                {
                    chkOTAppointment.Checked = Convert.ToBoolean(DSAppt.Tables[0].Rows[0]["HAM_OTAPPOINTMENT"]);

                    if (chkOTAppointment.Checked == true)
                    {
                        divOTAppointment.Visible = true;
                    }
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HSAM_PROCEDURE_CODE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HSAM_PROCEDURE_CODE"]) != "")
                {
                    txtServCode.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HSAM_PROCEDURE_CODE"]);
                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_HSAM_PROCEDURE_DESC") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_HSAM_PROCEDURE_DESC"]) != "")
                {
                    txtServName.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_HSAM_PROCEDURE_DESC"]);
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SURGERYTYPE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SURGERYTYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpSurgeryType.Items.Count; intCount++)
                    {
                        if (drpSurgeryType.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SURGERYTYPE"]))
                        {
                            drpSurgeryType.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SURGERYTYPE"]);
                        }

                    }


                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_ANESTHESIA_TYPE") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHESIA_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpAnesthesia.Items.Count; intCount++)
                    {
                        if (drpAnesthesia.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHESIA_TYPE"]))
                        {
                            drpAnesthesia.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHESIA_TYPE"]);
                        }

                    }
                }

                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_ANESTHETIST_NAME") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHETIST_NAME"]) != "")
                {
                    for (int intCount = 0; intCount < drpAnesthetist.Items.Count; intCount++)
                    {
                        if (drpAnesthetist.Items[intCount].Value == Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHETIST_NAME"]))
                        {
                            drpAnesthetist.SelectedValue = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_ANESTHETIST_NAME"]);
                        }

                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("HAM_SPECIAL_REQUEST") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SPECIAL_REQUEST"]) != "")
                {
                    txtSpecialRequest.Text = Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_SPECIAL_REQUEST"]);
                }




                if (DSAppt.Tables[0].Rows[0].IsNull("TimeIn") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeIn"]) != "")
                {

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadTimeIn12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeIn"]));
                    }
                    else
                    {
                        LoadTimeIn(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeIn"]));
                    }

                }
                if (DSAppt.Tables[0].Rows[0].IsNull("TimeOut") == false && Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeOut"]) != "")
                {
                    // LoadTimeOut(Convert.ToString(DSAppt.Tables[0].Rows[0]["HAM_TIME_OUT"]));

                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadTimeOut12Hrs(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeOut"]));
                    }
                    else
                    {
                        LoadTimeOut(Convert.ToString(DSAppt.Tables[0].Rows[0]["TimeOut"]));
                    }

                }



            }

        }

        Boolean CheckAppointment()
        {

            string Criteria = " 1=1 ";

            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";
            if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
            {
                Criteria += " AND HAM_FILENUMBER !='" + txtFileNo.Text.Trim() + "'";
            }

            if (chkOTAppointment.Checked == false)
            {
                Criteria += " AND HAM_DR_CODE='" + drpDoctor.SelectedValue + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";
            }

            string strSTHour, strFIHours;
            if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
            {
                strSTHour = drpSTHour.SelectedValue;
                strFIHours = drpFIHour.SelectedValue;
                if (drpSTAM.SelectedValue == "PM")
                {
                    Int32 intStTime = 0;

                    if (drpSTHour.SelectedValue != "12")
                    {
                        intStTime = Convert.ToInt32(drpSTHour.SelectedValue) + 12;
                        strSTHour = Convert.ToString(intStTime);
                    }
                    else
                    {
                        strSTHour = drpSTHour.SelectedValue;
                    }

                }
                else
                {
                    strSTHour = drpSTHour.SelectedValue;
                }


                if (drpFIAM.SelectedValue == "PM")
                {
                    Int32 intFITime = 0;

                    if (drpFIHour.SelectedValue != "12")
                    {
                        intFITime = Convert.ToInt32(drpFIHour.SelectedValue) + 12;
                        strFIHours = Convert.ToString(intFITime);
                    }
                    else
                    {
                        strFIHours = drpFIHour.SelectedValue;
                    }


                }
                else
                {
                    strFIHours = drpFIHour.SelectedValue;
                }



            }
            else
            {
                strSTHour = drpSTHour.SelectedValue;
                strFIHours = drpFIHour.SelectedValue;

            }




            string strSTTime = strForStartDate + " " + strSTHour + ":" + drpSTMin.SelectedValue + ":00";
            string strFITime = strForStartDate + " " + strFIHours + ":" + drpFIMin.SelectedValue + ":00";
            Criteria += "  AND( HAM_STARTTIME BETWEEN  '" + strSTTime + "' AND  '" + strFITime + "'";
            Criteria += "  AND  HAM_FINISHTIME BETWEEN  '" + strSTTime + "'  AND '" + strFITime + "')";



            CommonBAL dbo = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;
        }

        Boolean CheckAppointment1()
        {

            string Criteria = " 1=1 ";

            string strSelectedDate = txtFromDate.Text.Trim(); // System.DateTime.Now.ToString("dd/MM/yyyy");

            string strStartDate = strSelectedDate;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            Criteria += "  AND HAM_STATUS NOT IN (" + hidStatus.Value + ")";

            Criteria += " AND HAM_FILENUMBER !='" + txtFileNo.Text.Trim() + "'";

            Criteria += " AND HAM_DR_CODE='" + drpDoctor.SelectedValue + "'";// AND HAM_STARTTIME >= '" + strForStartDate + " " + FromTime + ":00'";

            //  Criteria += "  AND HAM_STARTTIME = '" + strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00'";

            string strSTTime = strForStartDate + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
            string strFITime = strForStartDate + " " + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + ":00";
            Criteria += "  AND( HAM_STARTTIME BETWEEN  DATEADD(minute, 1,'" + strSTTime + "') AND   DATEADD(minute, -1,'" + strFITime + "')";
            Criteria += "  OR  HAM_FINISHTIME BETWEEN  DATEADD(minute, 1,'" + strSTTime + "') AND DATEADD(minute, -1,'" + strFITime + "'))";



            CommonBAL dbo = new CommonBAL();
            DataSet DSAppt = new DataSet();

            DSAppt = dbo.AppointmentOutlookGet(Criteria);

            if (DSAppt.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;
        }

        void GetAppointmentStatus()
        {
            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            hidStatus.Value = "";
            string Criteria = " 1=1 ";


            Criteria += " AND HAS_STATUS='Waiting' OR  HAS_STATUS='Cancelled'";

            DS = dbo.AppointmentStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strId = ""; ;
                for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                {
                    strId += Convert.ToString(DS.Tables[0].Rows[i]["HAS_ID"]) + ",";
                }
                if (strId.Length > 1)
                {
                    hidStatus.Value = strId.Substring(0, strId.Length - 1);
                }

            }

        }

        void BindPatientDtls()
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";

            }
            if (txtMobile.Text.Trim() != "")
            {
                Criteria += " AND HPM_MOBILE = '" + txtMobile.Text + "'";
            }



            txtFName.Text = "";
            txtLName.Text = "";
            txtMobile.Text = "";
            txtEmail.Text = "";
            lblFullName.Text = "";

            ds = dbo.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]) != "")
                {
                    txtFileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_ID"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_FNAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]) != "")
                {
                    txtFName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_FNAME"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_LNAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_LNAME"]) != "")
                {
                    txtLName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_LNAME"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_EMAIL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]) != "")
                {
                    txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_EMAIL"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) != "")
                {
                    lblFullName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                }


            }
        }

        void BindDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();
            }




        }

        void BindAnesthetistDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present' AND HSFM_DEPT_ID='ANAETHESIOLOGY' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpAnesthetist.DataSource = ds;
                drpAnesthetist.DataValueField = "HSFM_STAFF_ID";
                drpAnesthetist.DataTextField = "FullName";
                drpAnesthetist.DataBind();
            }

            drpAnesthetist.Items.Insert(0, "--- Select ---");
            drpAnesthetist.Items[0].Value = "0";


        }

        void BindAppointmentServices()
        {
            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentServicesGet();

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpService.DataSource = DS;
                drpService.DataTextField = "HAS_SERVICENAME";
                drpService.DataValueField = "HAS_SERVICENAME";
                drpService.DataBind();
            }

        }

        void BindAppointmentStatus()
        {
            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            DS = dbo.AppointmentStatusGet("1=1");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpStatus.DataSource = DS;
                drpStatus.DataTextField = "HAS_STATUS";
                drpStatus.DataValueField = "HAS_ID";
                drpStatus.DataBind();

                //drpStatus.SelectedValue = "4";
                for (int intCount = 0; intCount < drpStatus.Items.Count; intCount++)
                {
                    if (drpStatus.Items[intCount].Text == "Booked")
                    {
                        drpStatus.SelectedValue = DS.Tables[0].Rows[intCount]["HAS_ID"].ToString();
                    }

                }

            }

        }

        void BindTime()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");

                drpFIHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpFIHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");


                drpTimeInHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpTimeInHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");

                drpTimeOutHour.Items.Insert(index, Convert.ToInt32(AppointmentStart + index).ToString("D2"));
                drpTimeOutHour.Items[index].Value = Convert.ToInt32(AppointmentStart + index).ToString("D2");

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            drpTimeInMin.Items.Insert(0, Convert.ToString("00"));
            drpTimeInMin.Items[0].Value = Convert.ToString("00");

            drpTimeOutMin.Items.Insert(0, Convert.ToString("00"));
            drpTimeOutMin.Items[0].Value = Convert.ToString("00");



            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                drpTimeInMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpTimeInMin.Items[index].Value = Convert.ToString(j - intCount);

                drpTimeOutMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpTimeOutMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindTime12Hrs()
        {
            int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
            int AppointmentStart = Convert.ToInt32(ViewState["AppointmentStart"]);
            int AppointmentEnd = Convert.ToInt32(ViewState["AppointmentEnd"]);
            int index = 0;

            for (int i = 1; i <= 12; i++)
            {
                drpSTHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
                drpSTHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

                drpFIHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
                drpFIHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void LoadStartTime(string strSTTime)
        {

            string[] arrSTTime = strSTTime.Split(':');

            if (arrSTTime.Length > 1)
            {
                for (int intCount = 0; intCount < drpSTHour.Items.Count; intCount++)
                {
                    if (drpSTHour.Items[intCount].Value == arrSTTime[0])
                    {
                        drpSTHour.SelectedValue = arrSTTime[0];
                        intCount = drpSTHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpSTMin.Items.Count; intCount++)
                {
                    if (drpSTMin.Items[intCount].Value == arrSTTime[1])
                    {
                        drpSTMin.SelectedValue = arrSTTime[1];
                        intCount = drpSTMin.Items.Count;
                    }

                }


            }
        }

        void LoadFinishTime(string strFITime)
        {
            string[] arrFITime = strFITime.Split(':');

            if (arrFITime.Length > 1)
            {
                for (int intCount = 0; intCount < drpFIHour.Items.Count; intCount++)
                {
                    if (drpFIHour.Items[intCount].Value == arrFITime[0])
                    {
                        drpFIHour.SelectedValue = arrFITime[0];
                        intCount = drpFIHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpFIMin.Items.Count; intCount++)
                {
                    if (drpFIMin.Items[intCount].Value == arrFITime[1])
                    {
                        drpFIMin.SelectedValue = arrFITime[1];
                        intCount = drpFIMin.Items.Count;
                    }

                }

            }
        }

        void LoadStartTime12Hrs(string strSTTime)
        {

            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strSTTime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                string strHour = arrTime[0];
                if (arrTime[0].Length == 1)
                {
                    strHour = "0" + arrTime[0];

                }

                for (int intCount = 0; intCount < drpSTHour.Items.Count; intCount++)
                {


                    if (drpSTHour.Items[intCount].Value == strHour)
                    {
                        drpSTHour.SelectedValue = strHour;
                        intCount = drpSTHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpSTMin.Items.Count; intCount++)
                {
                    if (drpSTMin.Items[intCount].Value == arrTime[1])
                    {
                        drpSTMin.SelectedValue = arrTime[1];
                        intCount = drpSTMin.Items.Count;
                    }

                }


                string[] arrSTTime1 = strFormatTie.Split(' ');


                if (arrSTTime.Length > 1)
                {
                    drpSTAM.SelectedValue = arrSTTime1[2];

                }


            }
        }

        void LoadFinishTime12Hrs(string strFITime)
        {
            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strFITime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                string strHour = arrTime[0];
                if (arrTime[0].Length == 1)
                {
                    strHour = "0" + arrTime[0];

                }

                for (int intCount = 0; intCount < drpFIHour.Items.Count; intCount++)
                {
                    if (drpFIHour.Items[intCount].Value == strHour)
                    {
                        drpFIHour.SelectedValue = strHour;
                        intCount = drpFIHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpFIMin.Items.Count; intCount++)
                {
                    if (drpFIMin.Items[intCount].Value == arrTime[1])
                    {
                        drpFIMin.SelectedValue = arrTime[1];
                        intCount = drpFIMin.Items.Count;
                    }

                }
                string[] arrSTTime1 = strFormatTie.Split(' ');

                if (arrSTTime.Length > 1)
                {
                    drpFIAM.SelectedValue = arrSTTime1[2];

                }


            }
        }



        void LoadTimeIn(string strSTTime)
        {

            string[] arrSTTime = strSTTime.Split(':');

            if (arrSTTime.Length > 1)
            {
                for (int intCount = 0; intCount < drpTimeInHour.Items.Count; intCount++)
                {
                    if (drpTimeInHour.Items[intCount].Value == arrSTTime[0])
                    {
                        drpTimeInHour.SelectedValue = arrSTTime[0];
                        intCount = drpTimeInHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeInMin.Items.Count; intCount++)
                {
                    if (drpTimeInMin.Items[intCount].Value == arrSTTime[1])
                    {
                        drpTimeInMin.SelectedValue = arrSTTime[1];
                        intCount = drpTimeInMin.Items.Count;
                    }

                }


            }
        }

        void LoadTimeOut(string strFITime)
        {
            string[] arrFITime = strFITime.Split(':');

            if (arrFITime.Length > 1)
            {
                for (int intCount = 0; intCount < drpTimeOutHour.Items.Count; intCount++)
                {
                    if (drpTimeOutHour.Items[intCount].Value == arrFITime[0])
                    {
                        drpTimeOutHour.SelectedValue = arrFITime[0];
                        intCount = drpTimeOutHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeOutMin.Items.Count; intCount++)
                {
                    if (drpTimeOutMin.Items[intCount].Value == arrFITime[1])
                    {
                        drpTimeOutMin.SelectedValue = arrFITime[1];
                        intCount = drpTimeOutMin.Items.Count;
                    }

                }

            }
        }

        void LoadTimeIn12Hrs(string strSTTime)
        {

            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strSTTime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                for (int intCount = 0; intCount < drpTimeInHour.Items.Count; intCount++)
                {
                    if (drpTimeInHour.Items[intCount].Value == arrTime[0])
                    {
                        drpTimeInHour.SelectedValue = arrTime[0];
                        intCount = drpTimeInHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeInMin.Items.Count; intCount++)
                {
                    if (drpTimeInMin.Items[intCount].Value == arrTime[1])
                    {
                        drpTimeInMin.SelectedValue = arrTime[1];
                        intCount = drpTimeInMin.Items.Count;
                    }

                }


                string[] arrSTTime1 = strFormatTie.Split(' ');


                if (arrSTTime.Length > 1)
                {
                    drpTimeInAM.SelectedValue = arrSTTime1[2];

                }


            }
        }

        void LoadTimeOut12Hrs(string strFITime)
        {
            DateTime dtstlTime;
            dtstlTime = Convert.ToDateTime(strFITime);
            dtstlTime.ToString(@"hh\:mm tt");
            string strFormatTie = Convert.ToString(dtstlTime);



            string[] arrSTTime = strFormatTie.Split(' ');
            string strTime = "";
            if (arrSTTime.Length > 0)
            {
                strTime = arrSTTime[1];
            }

            string[] arrTime = strTime.Split(':');



            if (arrSTTime.Length > 0)
            {
                for (int intCount = 0; intCount < drpTimeOutHour.Items.Count; intCount++)
                {
                    if (drpTimeOutHour.Items[intCount].Value == arrTime[0])
                    {
                        drpTimeOutHour.SelectedValue = arrTime[0];
                        intCount = drpTimeOutHour.Items.Count;
                    }

                }

                for (int intCount = 0; intCount < drpTimeOutMin.Items.Count; intCount++)
                {
                    if (drpTimeOutMin.Items[intCount].Value == arrTime[1])
                    {
                        drpTimeOutMin.SelectedValue = arrTime[1];
                        intCount = drpTimeOutMin.Items.Count;
                    }

                }
                string[] arrSTTime1 = strFormatTie.Split(' ');

                if (arrSTTime.Length > 1)
                {
                    drpTimeOutAM.SelectedValue = arrSTTime1[2];

                }


            }
        }


        void GetServiceMasterName(string ServID, out string ServName)
        {

            ServName = "";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_HAAD_CODE  ='" + ServID + "'";

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            txtServName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServName = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
            }

        }

        void GetDepartmentID()
        {

            string strDeptID = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + drpDoctor.SelectedValue + "'";
            CommonBAL dbo = new CommonBAL();
            DS = dbo.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strDeptID = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_DEPT_ID"]);
            }

            ViewState["Department"] = strDeptID;
        }


        void GetDepartmentName()
        {
            string strDeptID = "", Department = "";

            // strDeptID = GetDepartmentID();

            CommonBAL dbo = new CommonBAL();
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_STATUS ='A' and HDM_DEP_ID='" + strDeptID + "'";
            ds = dbo.DepMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Department = Convert.ToString(ds.Tables[0].Rows[0]["HDM_DEP_NAME"]);
            }

            ViewState["Department"] = Department;
        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='APPOINT' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            CommonBAL dbo = new CommonBAL();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {


                btnSave.Enabled = false;
                btnCancel.Enabled = false;
                btnDelete.Enabled = false;

                btnSendSMS.Enabled = false;
                btnSendMail.Enabled = false;
                btnClose.Enabled = false;





            }


            if (strPermission == "7")
            {

                btnSave.Enabled = false;
                btnCancel.Enabled = false;

                btnSendSMS.Enabled = false;
                btnSendMail.Enabled = false;
                btnClose.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");

            }
        }

        #endregion

        #region AutoExtenderFunctions


        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID  like '" + prefixText + "%' ";
            CommonBAL objCom = new CommonBAL();
            DS = objCom.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_HAAD_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            CommonBAL objCom = new CommonBAL();
            DS = objCom.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_HAAD_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../SessionExpired.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();
                BindSystemOption();
                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                {

                    BindTime12Hrs();
                    drpSTAM.Visible = true;
                    drpFIAM.Visible = true;
                    drpTimeInAM.Visible = true;
                    drpTimeOutAM.Visible = true;



                }
                else
                {
                    BindTime();
                    drpTimeInAM.Visible = false;
                    drpTimeOutAM.Visible = false;
                }

                ViewState["PageName"] = Convert.ToString(Request.QueryString["PageName"]);
                ViewState["AppId"] = Convert.ToString(Request.QueryString["AppId"]);
                ViewState["Date"] = Convert.ToString(Request.QueryString["Date"]);
                ViewState["Time"] = Convert.ToString(Request.QueryString["Time"]);
                ViewState["DrID"] = Convert.ToString(Request.QueryString["DrID"]);
                ViewState["PatientID"] = Convert.ToString(Request.QueryString["PatientID"]);
                ViewState["DeptID"] = Convert.ToString(Request.QueryString["DeptID"]);

                if (Convert.ToString(ViewState["PageName"]) == "ApptEmrDr")
                {
                    drpDoctor.Enabled = false;
                }

                if (Convert.ToString(ViewState["PatientID"]) != "" && Convert.ToString(ViewState["PatientID"]) != null)
                {
                    txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);
                    BindPatientDtls();
                }

                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    chkOTAppointment.Visible = true;
                }

                BindDoctor();
                BindAnesthetistDoctor();
                BindAppointmentServices();
                BindAppointmentStatus();
                GetAppointment();
                GetAppointmentStatus();
                if (Convert.ToString(ViewState["Date"]) != null && Convert.ToString(ViewState["Date"]) != "0")
                {
                    txtFromDate.Text = Convert.ToString(ViewState["Date"]);
                }
                if (Convert.ToString(ViewState["Time"]) != null && Convert.ToString(ViewState["Time"]) != "0")
                {
                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadStartTime12Hrs(Convert.ToString(ViewState["Time"]));
                    }
                    else
                    {
                        LoadStartTime(Convert.ToString(ViewState["Time"]));
                    }


                    int AppointmentInterval = Convert.ToInt32(ViewState["AppointmentInterval"]);
                    string strlTime;
                    strlTime = Convert.ToString(ViewState["Time"]);// drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;

                    string strNewTime1 = "";
                    if (strNewTime1 == "")
                    {
                        strNewTime1 = Convert.ToString(Convert.ToInt32(strlTime.Substring(strlTime.Length - 2, 2)) + AppointmentInterval);
                    }

                    string[] arrlTime = strlTime.Split(':');
                    if (arrlTime.Length > 1)
                    {
                        strlTime = arrlTime[0];
                    }

                    if (Convert.ToInt32(strNewTime1) >= 60)
                    {
                        strlTime = Convert.ToString(Convert.ToInt32(strlTime) + 1) + ":00";// +Convert.ToString(Convert.ToInt32(strNewTime1) - 60);
                    }
                    else
                    {
                        strlTime = Convert.ToString(strlTime + ":" + strNewTime1);
                    }





                    if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                    {
                        LoadFinishTime12Hrs(strlTime);
                    }
                    else
                    {
                        LoadFinishTime(strlTime);
                    }


                }

                if (Convert.ToString(ViewState["DrID"]) != null && Convert.ToString(ViewState["DrID"]) != "0")
                {
                    drpDoctor.SelectedValue = Convert.ToString(ViewState["DrID"]);
                }




            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckAppointment() == true)
                {
                    lblStatus.Text = "Appointment already exist for this Time";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto SaveEnd;
                }

                string strStartDate = txtFromDate.Text;
                string[] arrDate = strStartDate.Split('/');
                string strForStartDate = "";

                if (arrDate.Length > 1)
                {
                    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
                string strSTHour, strFIHours, stsTimeInHour, stsTimeOutHour;


                if (Convert.ToString(ViewState["HSOM_APPOINTMENT_DSPLY_FORMAT"]) == "12")
                {
                    //-------------------------------------------------------
                    strSTHour = drpSTHour.SelectedValue;
                    strFIHours = drpFIHour.SelectedValue;
                    if (drpSTAM.SelectedValue == "PM")
                    {
                        Int32 intStTime = 0;

                        if (drpSTHour.SelectedValue != "12")
                        {
                            intStTime = Convert.ToInt32(drpSTHour.SelectedValue) + 12;
                            strSTHour = Convert.ToString(intStTime);
                        }
                        else
                        {
                            strSTHour = drpSTHour.SelectedValue;
                        }

                    }
                    else
                    {
                        strSTHour = drpSTHour.SelectedValue;
                    }


                    if (drpFIAM.SelectedValue == "PM")
                    {
                        Int32 intFITime = 0;

                        if (drpFIHour.SelectedValue != "12")
                        {
                            intFITime = Convert.ToInt32(drpFIHour.SelectedValue) + 12;
                            strFIHours = Convert.ToString(intFITime);
                        }
                        else
                        {
                            strFIHours = drpFIHour.SelectedValue;
                        }


                    }
                    else
                    {
                        strFIHours = drpFIHour.SelectedValue;
                    }

                    //-----------------------------------------------------------------

                    //-------------------------------------------------------
                    stsTimeInHour = drpTimeInHour.SelectedValue;
                    stsTimeOutHour = drpTimeOutHour.SelectedValue;
                    if (drpSTAM.SelectedValue == "PM")
                    {
                        Int32 intStTime = 0;

                        if (drpTimeInHour.SelectedValue != "12")
                        {
                            intStTime = Convert.ToInt32(drpTimeInHour.SelectedValue) + 12;
                            stsTimeInHour = Convert.ToString(intStTime);
                        }
                        else
                        {
                            stsTimeInHour = drpTimeInHour.SelectedValue;
                        }

                    }
                    else
                    {
                        stsTimeInHour = drpTimeInHour.SelectedValue;
                    }


                    if (drpFIAM.SelectedValue == "PM")
                    {
                        Int32 intFITime = 0;

                        if (drpTimeOutHour.SelectedValue != "12")
                        {
                            intFITime = Convert.ToInt32(drpTimeOutHour.SelectedValue) + 12;
                            stsTimeOutHour = Convert.ToString(intFITime);
                        }
                        else
                        {
                            stsTimeOutHour = drpTimeOutHour.SelectedValue;
                        }


                    }
                    else
                    {
                        stsTimeOutHour = drpTimeOutHour.SelectedValue;
                    }

                    //-----------------------------------------------------------------

                }
                else
                {
                    strSTHour = drpSTHour.SelectedValue;
                    strFIHours = drpFIHour.SelectedValue;

                    stsTimeInHour = drpTimeInHour.SelectedValue;
                    stsTimeOutHour = drpTimeOutHour.SelectedValue;

                }

                //  goto SaveEnd;
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());
                con.Open();
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "HMS_SP_AppointmentOutlookAdd";
                cmd.Parameters.Add(new SqlParameter("@HAM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(Session["Branch_ID"]);
                cmd.Parameters.Add(new SqlParameter("@HAM_DR_CODE", SqlDbType.VarChar)).Value = drpDoctor.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_DR_NAME", SqlDbType.VarChar)).Value = drpDoctor.SelectedItem.Text;
                cmd.Parameters.Add(new SqlParameter("@HAM_PT_CODE", SqlDbType.VarChar)).Value = "Reg:No " + txtFileNo.Text.Trim() + " Mobile: " + txtMobile.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_PT_NAME", SqlDbType.VarChar)).Value = txtFName.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_STARTTIME", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + strSTHour + ":" + drpSTMin.SelectedValue + ":00";
                cmd.Parameters.Add(new SqlParameter("@HAM_FINISHTIME", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + strFIHours + ":" + drpFIMin.SelectedValue + ":00";
                cmd.Parameters.Add(new SqlParameter("@HAM_REMARKS", SqlDbType.VarChar)).Value = txtRemarks.Text;
                cmd.Parameters.Add(new SqlParameter("@HAM_STATUS", SqlDbType.VarChar)).Value = drpStatus.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_LASTNAME", SqlDbType.VarChar)).Value = txtLName.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_FILENUMBER", SqlDbType.VarChar)).Value = txtFileNo.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_MOBILENO", SqlDbType.VarChar)).Value = txtMobile.Text.Trim();

                if (chkStatus.Checked == true)
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_SERVICE", SqlDbType.VarChar)).Value = lblServices.Text;
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_SERVICE", SqlDbType.VarChar)).Value = drpService.SelectedValue;
                }
                cmd.Parameters.Add(new SqlParameter("@HAM_EMAIL", SqlDbType.VarChar)).Value = txtEmail.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_CREATEDUSER", SqlDbType.VarChar)).Value = Convert.ToString(Session["User_ID"]);


                cmd.Parameters.Add(new SqlParameter("@HAM_OTAPPOINTMENT", SqlDbType.Bit)).Value = chkOTAppointment.Checked;
                cmd.Parameters.Add(new SqlParameter("@HSAM_PROCEDURE_CODE", SqlDbType.VarChar)).Value = txtServCode.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_HSAM_PROCEDURE_DESC", SqlDbType.VarChar)).Value = txtServName.Text.Trim();
                cmd.Parameters.Add(new SqlParameter("@HAM_SURGERYTYPE", SqlDbType.VarChar)).Value = drpSurgeryType.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_ANESTHESIA_TYPE", SqlDbType.VarChar)).Value = drpAnesthesia.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_ANESTHETIST_NAME", SqlDbType.VarChar)).Value = drpAnesthetist.SelectedValue;
                cmd.Parameters.Add(new SqlParameter("@HAM_SPECIAL_REQUEST", SqlDbType.VarChar)).Value = txtSpecialRequest.Text.Trim();

                if (stsTimeInHour != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_TIME_IN", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + stsTimeInHour + ":" + drpTimeInMin.SelectedValue + ":00";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_TIME_IN", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + "00:00:00";

                }

                if (stsTimeOutHour != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_TIME_OUT", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + stsTimeOutHour + ":" + drpTimeOutMin.SelectedValue + ":00";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_TIME_OUT", SqlDbType.VarChar)).Value = txtFromDate.Text.Trim() + " " + "00:00:00";
                }

                if (Convert.ToString(ViewState["AppId"]) != "" && Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_APPOINTMENTID", SqlDbType.Int)).Value = Convert.ToString(ViewState["AppId"]);
                    cmd.Parameters.Add(new SqlParameter("@MODE", SqlDbType.Char)).Value = "M";

                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@HAM_APPOINTMENTID", SqlDbType.Int)).Value = 0;
                    cmd.Parameters.Add(new SqlParameter("@MODE", SqlDbType.Char)).Value = "A";
                }


                cmd.ExecuteNonQuery();
                con.Close();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "','" + Convert.ToString(ViewState["DeptID"]) + "');", true);

            SaveEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Details Not Saved";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                objCom = new CommonBAL();
                string strStatusId = "0";

                for (int i = 0; i <= drpStatus.Items.Count - 1; i++)
                {
                    if (drpStatus.Items[i].Text == "Cancelled")
                    {
                        strStatusId = drpStatus.Items[i].Value;
                        goto ForEnd;
                    }
                }
            ForEnd: ;
                if (Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    string Criteria = " HAM_APPOINTMENTID=" + Convert.ToString(ViewState["AppId"]);

                    string FieldWithValue = " HAM_STATUS='" + strStatusId + "'";

                    objCom.fnUpdateTableData(FieldWithValue, "HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria);




                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnCancel_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindPatientDtls();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.aspx_txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtMobile_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindPatientDtls();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.aspx_txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void btnSendSMS_Click(object sender, EventArgs e)
        {

            if (txtMobile.Text.Trim() == "")
            {
                lblStatus.Text = "Please enter Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            string MobileNumber = txtMobile.Text.Trim();

            if (MobileNumber.Length < 10 || MobileNumber.Substring(0, 2) != "05")
            {
                lblStatus.Text = "Please check the Mobile Number";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }
            clsSMS objSMS = new clsSMS();
            string strNewLineChar = "\n";// "char(2)=char(10) + char(13)";

            string strContent = "";
            strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + strNewLineChar + txtFName.Text.Trim() + " " + txtLName.Text.Trim()
               + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;

            objSMS.MobileNumber = txtMobile.Text.Trim(); // "0502213045";
            objSMS.template = strContent;
            Boolean boolResult = objSMS.SendSMS();
            if (boolResult == true)
            {
                lblStatus.Text = "SMS Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblStatus.Text = "SMS Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        FunEnd: ;

        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text.Trim() == "")
                {
                    lblStatus.Text = "Please enter Email ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                CommonBAL objCom = new CommonBAL();

                string strNewLineChar = "\n";
                string strContent = "";
                strContent = txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + strNewLineChar + txtFName.Text.Trim() + " " + txtLName.Text.Trim()
                   + strNewLineChar + "MOB:" + txtMobile.Text.Trim() + strNewLineChar + "DR:" + drpDoctor.SelectedItem.Text;

                string Details_String = "";

                Details_String = Details_String + "<TABLE cellpadding='2' cellspacing='2'>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>Dear  <b>" + lblFullName.Text + ",</b></TD></TR>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='Left'>Appointment Details</TD></TR>";
                Details_String = Details_String + "<TR><TD valign='top' align='left'><B>Date & Time : </B></TD> <TD>" + txtFromDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + "-" + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue + "</TD></TR>";
                Details_String = Details_String + "<TR><TD valign='top' align='left'><B>Doctor Name : </B></TD> <TD>" + "DR:" + drpDoctor.SelectedItem.Text + "</TD></TR>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='center'>&nbsp;</TD></TR>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>Regards</TD></TR>";
                Details_String = Details_String + "<TR><TD COLSPAN=2 align='left'>" + GlobalValues.HospitalName + "</TD></TR>";

                Details_String = Details_String + "</TABLE>";

                int intStatuls;

                intStatuls = objCom.HTMLMailSend(txtEmail.Text.Trim(), "Reg Appointment", Details_String);
                lblStatus.Text = "Mail Send Successfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnSendMail_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Mail Not Send";
                lblStatus.ForeColor = System.Drawing.Color.Red;

            }

        }



        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                objCom = new CommonBAL();
                if (Convert.ToString(ViewState["AppId"]) != null && Convert.ToString(ViewState["AppId"]) != "0")
                {
                    string Criteria = " HAM_APPOINTMENTID=" + Convert.ToString(ViewState["AppId"]);
                    objCom.fnDeleteTableData("HMS_APPOINTMENT_OUTLOOKSTYLE", Criteria);

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + txtFromDate.Text.Trim() + "','" + Convert.ToString(ViewState["PageName"]) + "','" + drpDoctor.SelectedValue + "');", true);


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void drpService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkStatus.Checked == true)
                {
                    lblServices.Text = lblServices.Text + "," + drpService.SelectedValue;
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AppointmentPopup.drpService_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void chkOTAppointment_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOTAppointment.Checked == true)
            {
                divOTAppointment.Visible = true;
            }
            else
            {
                divOTAppointment.Visible = false;
            }

        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            string ServName = "";

            GetServiceMasterName(txtServCode.Text.Trim(), out  ServName);

            txtServName.Text = ServName;


        }
    }
}