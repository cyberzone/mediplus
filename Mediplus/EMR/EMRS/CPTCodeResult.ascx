﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CPTCodeResult.ascx.cs" Inherits="Mediplus.EMR.EMRS.CPTCodeResult" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" >

</script>

<div>
    <input type="hidden" id="hidENMCodeManual" runat="server" value="true" />
<table  style="width:100%">
    <tr>
        <td  style="width: 37%;vertical-align:top;">
            <table style="border-collapse: collapse; border: 1px solid #dcdcdc;width:100%;height: 25px;text-align:">
                <tr>
                    <td class="lblCaption" style="border-collapse: collapse; border: 1px solid #dcdcdc; height: 25px;">Final Diagnosis
                    </td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse; border: 1px solid #dcdcdc; height: 25px;">
                        <asp:Label ID="lblFinalDiag" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption" style="border-collapse: collapse; border: 1px solid #dcdcdc; height: 25px;">Plan
                    </td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse; border: 1px solid #dcdcdc; height: 25px;">
                        <asp:Label ID="lblPlan" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvCPT" runat="server" AllowSorting="True" AutoGenerateColumns="False"   
                            EnableModelValidation="True" Width="100%" GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Blue" Height="10px" />
                            <RowStyle CssClass="GridRow" Height="10px" />

                            <Columns>

                                <asp:TemplateField HeaderText="EM" HeaderStyle-Width="110px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLabReqEPRID" CssClass="label" runat="server" Text='<%# Bind("CPTCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="200px">
                                    <ItemTemplate>
                                        <asp:Label ID="Label27" CssClass="label" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fee">
                                    <ItemTemplate>
                                        <asp:Label ID="Label28" CssClass="label" runat="server" Text='<%# Bind("Fee") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Claim Fee">
                                    <ItemTemplate>

                                        <asp:Label ID="Label29" CssClass="label" runat="server" Text='<%# Bind("ClaimFee") %>'></asp:Label>

                                    </ItemTemplate>

                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>

        </td>

        <td   style="width: 63%;vertical-align:top;">
            <table style="width:100%">
                <tr>
                    <td class="lblCaption1" style="font-weight: bold;" colspan="2">EM Result Summary
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 30px; background-color:#666;">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">(1) History</span>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px; width: 300px;">Level of HPI
                    </td>
                    <td>
                        <asp:Label ID="lblHPIResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">No. of ROS
                    </td>
                    <td>
                        <asp:Label ID="lblROSResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">No. of PFSH
                    </td>
                    <td>
                        <asp:Label ID="lblPFSHResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Level of History Documented
                    </td>
                    <td>
                        <asp:Label ID="lblFinalHistoryResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="height: 30px;  background-color:#666;">
                        <span class="lblCaption BoldStyle" style="color: #fff;">(2) Examination Type</span>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Examination
                    </td>
                    <td>
                        <asp:Label ID="lblPEResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 30px;  background-color:#666;">
                        <span class="lblCaption BoldStyle" style="color: #fff;">(3) Medical Decision Making</span>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Problem Point Scored
                    </td>
                    <td>
                        <asp:Label ID="lblPSResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Data Point Scored
                    </td>
                    <td>
                        <asp:Label ID="lblDPResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Risk Management
                    </td>
                    <td>
                        <asp:Label ID="lblRiskResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1" style="height: 20px;">Final MDM
                    </td>
                    <td>
                        <asp:Label ID="lblMDMResult" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="height: 30px;  background-color:#666;">
                        <span class="lblCaption BoldStyle" style="color: #fff;">(4) Selected CPT Code</span>
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1" style="height: 20px;"></td>
                    <td style="height: 50px;">
                         <asp:TextBox ID="txtENMCode" runat="server" CssClass="lblCaption1" BorderWidth="0px" Font-Size="30" BorderColor="#cccccc" Width="200px" Height="50PX" MaxLength="10" ReadOnly="true"></asp:TextBox>
                           
                    </td>
                </tr>
               <tr>
     <td>
         <asp:CheckBox ID="chEmergency" runat="server" CssClass="lblCaption1" Text="Emergency" /> 
         &nbsp;&nbsp;&nbsp;
         <asp:DropDownList ID="drpENMCode" runat="server" CssClass="TextBoxStyle"  Height="35px"  Font-Size="18"  Visible="false" ></asp:DropDownList>
     </td>
        <td  style="text-align: right;">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                  <asp:Button ID="btnEvaluate" runat="server" CssClass="button red small" Width="100px" Text="Evaluate" OnClick="btnEvaluate_Click" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnEvaluate" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
    </tr>
            </table>
        </td>
    </tr>
   
</table>
 
</div>