﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.EMRS
{
    public partial class CPTCodeResult : System.Web.UI.UserControl
    {
        EMRSBAL objEMRS = new EMRSBAL();
        CommonBAL objCom = new CommonBAL();

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR' AND SEGMENT='EMR_EMCODE_MANUAL' ";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            ViewState["EMR_EMCODE_MANUAL"] = "0";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_EMCODE_MANUAL")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_EMCODE_MANUAL"] = Convert.ToString(DR["CUST_VALUE"]);
                        }
                    }


                }
            }


        }

        void BindWEMR_spS_GetEMRS()
        {

            DataSet DS = new DataSet();
            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRS();
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("FinalDiagnosis") == false && Convert.ToString(DS.Tables[0].Rows[0]["FinalDiagnosis"]) != "")
                    lblFinalDiag.Text = DS.Tables[0].Rows[0]["FinalDiagnosis"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("Plan") == false && Convert.ToString(DS.Tables[0].Rows[0]["Plan"]) != "")
                    lblPlan.Text = DS.Tables[0].Rows[0]["Plan"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("ExaminationType") == false && Convert.ToString(DS.Tables[0].Rows[0]["ExaminationType"]) != "")
                    ViewState["ExaminationType"] = DS.Tables[0].Rows[0]["ExaminationType"].ToString();

            }
            else
            {
                lblFinalDiag.Text = "";
                lblPlan.Text = "";


            }
        }

        void BindEMRS_PT_DETAILS()
        {
            DataSet DS = new DataSet();
            objEMRS = new EMRSBAL();
            string Criteria = "1=1";
            Criteria += " and EMRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' and EMRS_SEQNO='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objEMRS.EMRS_PT_DETAILSGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                // if (DS.Tables[0].Rows[0].IsNull("EMRS_CPT") == false && Convert.ToString(DS.Tables[0].Rows[0]["EMRS_CPT"]) != "" && txtENMCode.Text == "")
                //   txtENMCode.Text = DS.Tables[0].Rows[0]["EMRS_CPT"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("EMRS_CPT_MANUAL") == false && Convert.ToString(DS.Tables[0].Rows[0]["EMRS_CPT_MANUAL"]) != "")
                {
                    drpENMCode.SelectedValue = DS.Tables[0].Rows[0]["EMRS_CPT_MANUAL"].ToString();
                    // txtENMCode.Text = DS.Tables[0].Rows[0]["EMRS_CPT_MANUAL"].ToString();

                }

            }

        }


        void BindWEMR_spS_GetEMCPTInvoice()
        {
            if (txtENMCode.Text.Trim() == "")
            {
                goto FunEnd;
            }
            objEMRS = new EMRSBAL();
            DataSet DS = new DataSet();
            DS = objEMRS.WEMR_spS_GetEMCPTInvoice(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_ID"]), txtENMCode.Text.Trim());
            if (DS.Tables.Count > 0)
            {
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvCPT.DataSource = DS;
                    gvCPT.DataBind();
                }
                else
                {
                    gvCPT.DataBind();
                }
            }

        FunEnd: ;
        }


        public void CPTCodeResultvaluate()
        {
            string HPIResult = "", ROSResult = "N/A", PFSHResult = "N/A", FinalHistoryResult = "", PEResult = "N/A", PSResult = "Minimal", DPResult = "Minimal";
            double PSTotal = 0, DPTotal = 0, RiskTotal = 0;
            int HPICount = 0, ROSCount = 0, PFSHCount = 0, PECount = 0, PEOrganCount = 0, TimeDifference = 0;

            string RiskResult = "Minimal", MDMResult = "", CPTCode = "", RiskPPResult = "", RiskDGPResult = "", RiskMOSResult = "", PatientType = "New";


            if (!Convert.ToString(Session["HPV_VISIT_TYPE"]).Equals("New"))
            {
                PatientType = "Established";
            }
            DataSet DS = new DataSet();
            SegmentBAL objEMRS = new SegmentBAL();
            // DS = objEMRS.WEMR_spS_GetSegmentVisits(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_ID"]));
            if (Session["SegmentVisit"] != "" && Session["SegmentVisit"] != null)
            {

                DS = (DataSet)Session["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    string SubType = "";
                    string strValue = "", strType = "", strSubType = "";
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        if (DR.IsNull("ESVD_VALUE_YES") == false && Convert.ToString(DR["ESVD_VALUE_YES"]) != "")
                            strValue = Convert.ToString(DR["ESVD_VALUE_YES"]);

                        if (DR.IsNull("ESVD_TYPE") == false && Convert.ToString(DR["ESVD_TYPE"]) != "")
                            strType = Convert.ToString(DR["ESVD_TYPE"]);

                        if (DR.IsNull("ESVD_SUBTYPE") == false && Convert.ToString(DR["ESVD_SUBTYPE"]) != "")
                            strSubType = Convert.ToString(DR["ESVD_SUBTYPE"]);


                        if (strValue.Equals("Y") || strValue.Equals("N"))  //if (strValue.Equals("Y"))
                        {
                            if (strType.Equals("HPI"))
                            {
                                HPICount++;
                            }
                            else if (strType.Equals("ROS"))
                            {
                                ROSCount++;
                            }
                            //else if (strType.Equals("HIST_PAST") || strType.Equals("HIST_FAMILY") || strType.Equals("HIST_SOCIAL"))
                            //{
                            //    PFSHCount++;
                            //}
                            else if (strType.Equals("PE"))
                            {
                                PECount++;
                            }
                            else if (strType.Equals("PE") && (!SubType.Equals(strSubType)))
                            {
                                PEOrganCount++;
                            }
                        }
                        SubType = Convert.ToString(DR["ESVD_SUBTYPE"]);
                    }

                }
            }

            if (Session["SegmentVisitByPT"] != "" && Session["SegmentVisitByPT"] != null)
            {

                DS = (DataSet)Session["SegmentVisitByPT"];

                if (DS.Tables[0].Rows.Count > 0)
                {

                    string strValue = "", strType = "", strSubType = "";
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        if (DR.IsNull("ESVD_VALUE_YES") == false && Convert.ToString(DR["ESVD_VALUE_YES"]) != "")
                            strValue = Convert.ToString(DR["ESVD_VALUE_YES"]);

                        if (DR.IsNull("ESVD_TYPE") == false && Convert.ToString(DR["ESVD_TYPE"]) != "")
                            strType = Convert.ToString(DR["ESVD_TYPE"]);

                        if (DR.IsNull("ESVD_SUBTYPE") == false && Convert.ToString(DR["ESVD_SUBTYPE"]) != "")
                            strSubType = Convert.ToString(DR["ESVD_SUBTYPE"]);


                        if (strValue.Equals("Y") || strValue.Equals("N"))  //if (strValue.Equals("Y"))
                        {

                            if (strType.Equals("HIST_PAST") || strType.Equals("HIST_FAMILY") || strType.Equals("HIST_SOCIAL"))
                            {
                                PFSHCount++;
                            }

                        }
                    }



                }


            }

            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.WEMR_spS_GetEMRSDatas(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {


                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string strType = "", strSubType = "", strPoints = "";
                    if (DS.Tables[0].Rows[i].IsNull("Type") == false && Convert.ToString(DS.Tables[0].Rows[i]["Type"]) != "")
                        strType = Convert.ToString(DS.Tables[0].Rows[i]["Type"]);

                    if (DS.Tables[0].Rows[i].IsNull("SubType") == false && Convert.ToString(DS.Tables[0].Rows[i]["SubType"]) != "")
                        strSubType = Convert.ToString(DS.Tables[0].Rows[i]["SubType"]);

                    if (DS.Tables[0].Rows[i].IsNull("Points") == false && Convert.ToString(DS.Tables[0].Rows[i]["Points"]) != "")
                        strPoints = Convert.ToString(DS.Tables[0].Rows[i]["Points"]);


                    if (strType.Equals("EMRS_PS"))
                    {
                        PSTotal += Double.Parse(strPoints == "" ? "0" : strPoints);
                    }
                    else if (strType.Equals("EMRS_DP"))
                    {
                        DPTotal += Double.Parse(strPoints == "" ? "0" : strPoints);
                    }
                    else if (strType.Equals("RISK_PPS"))
                    {
                        RiskPPResult = strSubType;
                    }
                    else if (strType.Equals("RISK_DGP"))
                    {
                        RiskDGPResult = strSubType;
                    }
                    else if (strType.Equals("RISK_MOS"))
                    {
                        RiskMOSResult = strSubType;
                    }
                }


            }

            int HPICoderCount = 0;

            string ChronicStatus = "", LimitedHistory = "";


            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.WEMR_spS_GetEMRSHPI(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {



                if (DS.Tables[0].Rows[0].IsNull("Location") == false && Convert.ToString(DS.Tables[0].Rows[0]["Location"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Location"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Context") == false && Convert.ToString(DS.Tables[0].Rows[0]["Context"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Context"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Duration") == false && Convert.ToString(DS.Tables[0].Rows[0]["Duration"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Duration"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("ModifyingFactor") == false && Convert.ToString(DS.Tables[0].Rows[0]["ModifyingFactor"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["ModifyingFactor"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Quality") == false && Convert.ToString(DS.Tables[0].Rows[0]["Quality"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Quality"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Severity") == false && Convert.ToString(DS.Tables[0].Rows[0]["Severity"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Severity"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("SignsSymptoms") == false && Convert.ToString(DS.Tables[0].Rows[0]["SignsSymptoms"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["SignsSymptoms"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Timing") == false && Convert.ToString(DS.Tables[0].Rows[0]["Timing"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Timing"]) == "1")
                    HPICoderCount++;

                if (HPICoderCount != 0)
                    HPICount = HPICoderCount;



                if (DS.Tables[0].Rows[0].IsNull("ChronicStatus") == false && Convert.ToString(DS.Tables[0].Rows[0]["ChronicStatus"]) != "")
                    ChronicStatus = DS.Tables[0].Rows[0]["ChronicStatus"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("LimitedHistory") == false && Convert.ToString(DS.Tables[0].Rows[0]["LimitedHistory"]) != "")
                    LimitedHistory = DS.Tables[0].Rows[0]["LimitedHistory"].ToString();




                if (HPICount >= 4 || (ChronicStatus != null && ChronicStatus.Equals("1")) || (LimitedHistory != null && LimitedHistory.Equals("1")))
                {
                    HPIResult = "Extended";
                }
                else if (HPICount >= 1 && HPICount <= 3) // please verify it in en coding 
                {
                    HPIResult = "Brief";
                }

                lblHPIResult.Text = HPIResult;
            }
            else
            {
                lblHPIResult.Text = "";


            }



            // result for ROS
            if (ROSCount == 1)
            {
                ROSResult = "Pertinent to Problem";
            }
            else if (ROSCount >= 2 && ROSCount <= 9)
            {
                ROSResult = "Extended";
            }
            else if (ROSCount >= 10)
            {
                ROSResult = "Complete";
            }

            // result for PFSH
            if (PFSHCount == 1)
            {
                PFSHResult = "Pertinent to Problem";
            }
            else if (PFSHCount > 1)
            {
                PFSHResult = "Complete";
            }



            int Hx = 0, Exam = 0, MDM = 0;
            // result for Level of History Documented
            if (HPIResult.Equals("Brief") && ROSResult.Equals("N/A") && PFSHResult.Equals("N/A"))
            {
                FinalHistoryResult = "Problem Focused";
                Hx = 1;
            }
            else if (HPIResult.Equals("Brief") && !ROSResult.Equals("N/A"))
            {
                FinalHistoryResult = "Expandable Problem Focused";
                Hx = 2;
            }
            else if (HPIResult.Equals("Extended") && ROSResult.Equals("Extended") && PFSHResult.Equals("Pertinent to Problem"))
            {
                FinalHistoryResult = "Detailed";
                Hx = 3;
            }
            else if (HPIResult.Equals("Extended") && ROSResult.Equals("Complete") && PFSHResult.Equals("Complete"))
            {
                FinalHistoryResult = "Comprehensive";
                Hx = 4;
            }


            // --------------------------------------------
            string ExaminationType = Convert.ToString(ViewState["ExaminationType"]);
            // Examination Level result
            if (ExaminationType == null)
            {
                ExaminationType = "1997";
            }
            if (ExaminationType.Equals("1995"))
            {
                if (PECount == 1)
                {
                    PEResult = "Problem Focused";
                }
                else if (PECount >= 2 && PECount <= 7)
                {
                    PEResult = "Detailed";
                }
                else if (PECount >= 8)
                {
                    PEResult = "Comprehensive";
                }
            }
            else if (ExaminationType.Equals("1997"))
            {
                if (PECount >= 1 && PECount <= 5)
                {
                    PEResult = "Problem Focused";
                    Exam = 1;
                }
                else if (PECount >= 6 && PECount <= 11)
                {
                    PEResult = "Expandable Problem Focused";
                    Exam = 2;
                }
                else if (PECount >= 12 && PECount <= 17)
                {
                    PEResult = "Detailed";
                    Exam = 3;
                }
                else if (PECount >= 18)
                {
                    PEResult = "Comprehensive";
                    Exam = 4;
                }

                if (PEOrganCount >= 9 && (PECount == 18))
                {
                    PEResult = "Comprehensive";
                    Exam = 4;
                }
                else if (PEOrganCount >= 2 && (PECount == 12 && PECount <= 17))
                {
                    PEResult = "Detailed";
                    Exam = 3;
                }
            }





            lblROSResult.Text = ROSResult;
            lblPFSHResult.Text = PFSHResult;
            lblFinalHistoryResult.Text = FinalHistoryResult;
            lblPEResult.Text = PEResult;

            //--------------------------------------------------------------------------------------------------------



            // Problem Focused
            if (PSTotal == 1)
            {
                PSResult = "Minimal";
            }
            else if (PSTotal == 2)
            {
                PSResult = "Limited";
            }
            else if (PSTotal == 3)
            {
                PSResult = "Multiple";
            }
            else if (PSTotal >= 4)
            {
                PSResult = "Extensive";
            }
            /*else
            {
                PSResult = "Minimal";
            }*/

            // Data Reviewed
            if (DPTotal <= 1)
            {
                DPResult = "Minimal";
            }
            else if (DPTotal == 2)
            {
                DPResult = "Limited";
            }
            else if (DPTotal == 3)
            {
                DPResult = "Multiple";
            }
            else if (DPTotal >= 4)
            {
                DPResult = "Extensive";
            }


            // Risk Assessment result
            if (RiskPPResult.Equals("HIGH") || RiskDGPResult.Equals("HIGH") || RiskMOSResult.Equals("HIGH"))
            {
                RiskResult = "High";
            }
            else if (RiskPPResult.Equals("MOD") || RiskDGPResult.Equals("MOD") || RiskMOSResult.Equals("MOD"))
            {
                RiskResult = "Moderate";
            }
            else if (RiskPPResult.Equals("LOW") || RiskDGPResult.Equals("LOW") || RiskMOSResult.Equals("LOW"))
            {
                RiskResult = "Low";
            }
            else if (RiskPPResult.Equals("MIN") || RiskDGPResult.Equals("MIN") || RiskMOSResult.Equals("MIN"))
            {
                RiskResult = "Minmal";
            }
            /*else
            {
                RiskResult = "Minimal";
            }*/

            // calculating MDM
            if (PSResult.Equals("Minimal") || DPResult.Equals("Minimal") || RiskResult.Equals("Minimal"))
            {
                MDMResult = "Straight Forward";
                MDM = 1;
            }
            else if (PSResult.Equals("Limited") || DPResult.Equals("Limited") || RiskResult.Equals("Low"))
            {
                MDMResult = "Low Complexity";
                MDM = 2;
            }
            else if (PSResult.Equals("Multiple") || DPResult.Equals("Multiple") || RiskResult.Equals("Moderate"))
            {
                MDMResult = "Moderate Complexity";
                MDM = 3;
            }
            else if (PSResult.Equals("Extensive") || DPResult.Equals("Extensive") || RiskResult.Equals("High"))
            {
                MDMResult = "High Complexity";
                MDM = 4;
            }

            lblPSResult.Text = PSResult;
            lblDPResult.Text = DPResult;
            lblRiskResult.Text = RiskResult;
            lblMDMResult.Text = MDMResult;

            // calculating the final en coding    
            if (PatientType.Equals("New"))
            {
                if (FinalHistoryResult.Equals("Problem Focused") && PEResult.Equals("Problem Focused") && MDMResult.Equals("Straight Forward"))
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99201";
                }
                else if (FinalHistoryResult.Equals("Expandable Problem Focused") && PEResult.Equals("Expandable Problem Focused")
                    && MDMResult.Equals("Straight Forward"))
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99202";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Detailed") && MDMResult.Equals("Low Complexity"))
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99203";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive") && MDMResult.Equals("Moderate Complexity"))
                {
                    CPTCode = chEmergency.Checked == true ? "99284" : "99204";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive") && MDMResult.Equals("High Complexity"))
                {
                    CPTCode = chEmergency.Checked == true ? "99285" : "99205";
                }
                else if (FinalHistoryResult.Equals("Problem Focused") && PEResult.Equals("Problem Focused") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99201";
                }
                if (PEResult.Equals("Problem Focused") && MDMResult.Equals("Straight Forward") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99201";
                }
                else if (FinalHistoryResult.Equals("Expandable Problem Focused") && PEResult.Equals("Expandable Problem Focused") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99201";
                }
                else if (PEResult.Equals("Expandable Problem Focused") && MDMResult.Equals("Straight Forward") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99201";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Detailed") && MDMResult.Equals("Straight Forward") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99202";
                }
                else if (PEResult.Equals("Detailed") && MDMResult.Equals("Low Complexity") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99202";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99203";
                }
                else if (PEResult.Equals("Comprehensive") && MDMResult.Equals("Moderate Complexity") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99203";
                }
                else if (PEResult.Equals("Comprehensive") && MDMResult.Equals("High Complexity") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99284" : "99204";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Detailed") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99203";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Expandable Problem Focused") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99202";
                }
                else if (CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99201";
                }
            }
            else if (PatientType.Equals("Established"))
            {
                if (FinalHistoryResult.Equals("Problem Focused") && PEResult.Equals("Problem Focused") && MDMResult.Equals("Straight Forward"))
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                else if (FinalHistoryResult.Equals("Expandable Problem Focused") && PEResult.Equals("Expandable Problem Focused")
                    && MDMResult.Equals("Straight Forward"))
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99212";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Detailed") && MDMResult.Equals("Low Complexity"))
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99213";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive") && MDMResult.Equals("Moderate Complexity"))
                {
                    CPTCode = chEmergency.Checked == true ? "99284" : "99214";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive") && MDMResult.Equals("High Complexity"))
                {
                    CPTCode = chEmergency.Checked == true ? "99285" : "99215";
                }
                else if (FinalHistoryResult.Equals("Problem Focused") && PEResult.Equals("Problem Focused") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                if (PEResult.Equals("Problem Focused") && MDMResult.Equals("Straight Forward") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                else if (FinalHistoryResult.Equals("Expandable Problem Focused") && PEResult.Equals("Expandable Problem Focused") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                else if (PEResult.Equals("Expandable Problem Focused") && MDMResult.Equals("Straight Forward") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Detailed") && MDMResult.Equals("Straight Forward") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99212";
                }
                else if (PEResult.Equals("Detailed") && MDMResult.Equals("Low Complexity") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99212";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99213";
                }
                else if (PEResult.Equals("Comprehensive") && MDMResult.Equals("Moderate Complexity") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99213";
                }
                else if (PEResult.Equals("Comprehensive") && MDMResult.Equals("High Complexity") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99284" : "99214";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Detailed") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99283" : "99213";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Expandable Problem Focused") && CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99282" : "99212";
                }
                else if (CPTCode == "")
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                /*
                if (FinalHistoryResult.Equals("N/A") || PEResult.Equals("N/A") || MDMResult.Equals(""))
                {
                    CPTCode = chEmergency.Checked == true ? "99281" : "99211";
                }
                else
                {
                    int EMCode = 1;
                    int[] EMResultData = new int[] { Hx, Exam, MDM };
                    Array.Sort(EMResultData);
                    if (EMResultData.Distinct().Count() == 3)
                    {
                        EMCode = EMResultData.Min();
                    }
                    else
                    {
                        EMCode = EMResultData[1];
                    }
                    if (EMCode == 1)
                    {
                        CPTCode = chEmergency.Checked == true ? "99282" : "99212";
                    }
                    else if (EMCode == 2)
                    {
                        CPTCode = chEmergency.Checked == true ? "99283" : "99213";
                    }
                    else if (EMCode == 3)
                    {
                        CPTCode = chEmergency.Checked == true ? "99284" : "99214";
                    }
                    else if (EMCode == 4)
                    {
                        CPTCode = chEmergency.Checked == true ? "99285" : "99215";
                    }
                }
                */

            }

            if (Convert.ToString(Session["HPV_VISIT_TYPE"]).Equals("New") || Convert.ToString(Session["HPV_VISIT_TYPE"]).Equals("Established"))
            {
                txtENMCode.Text = CPTCode;


                if (!CPTCode.Equals("") && HPICount > 0)
                {
                    EMRSBAL _emrsBAL = new EMRSBAL();
                    _emrsBAL.BranchID = Convert.ToString(Session["Branch_ID"]);
                    _emrsBAL.EMRID = Convert.ToString(Session["EMR_ID"]);
                    _emrsBAL.FileNumber = Convert.ToString(Session["EMR_PT_ID"]);
                    _emrsBAL.HPILevel = HPIResult;
                    _emrsBAL.ROS = ROSResult;
                    _emrsBAL.PFSH = PFSHResult;
                    _emrsBAL.DocumentedHistory = FinalHistoryResult;
                    _emrsBAL.examinationtype = PEResult;
                    _emrsBAL.ProblemPointScored = PSResult;
                    _emrsBAL.DataPointScored = DPResult;
                    _emrsBAL.RiskManagement = RiskResult;
                    _emrsBAL.MDM = MDMResult;
                    //if (GlobalValues.FileDescription.ToUpper() == "ALAMAL" && hidENMCodeManual.Value == "true")
                    //{
                    //    _emrsBAL.CPTCode = drpENMCode.SelectedValue;
                    //}
                    //else
                    //{
                    if (Convert.ToString(ViewState["EMR_EMCODE_MANUAL"]) == "1")
                    {
                        _emrsBAL.CPTCode = drpENMCode.SelectedValue;
                    }
                    else
                    {
                        _emrsBAL.CPTCode = CPTCode;
                    }

                    // }
                    _emrsBAL.SaveEMResult();

                    if (chEmergency.Checked == false)
                    {
                        BindWEMR_spS_GetEMCPTInvoice();
                    }
                    else
                    {
                        gvCPT.DataBind();
                    }
                }

            }
        }

        void BindENMCode()
        {
            string PatientType = "New";
            if (!Convert.ToString(Session["HPV_VISIT_TYPE"]).Equals("New"))
            {
                PatientType = "Established";
            }

            DataTable DT = new DataTable();
            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";
            DT.Columns.Add(Code);

            DataRow objrow;

            if (chEmergency.Checked == false)
            {
                if (PatientType.Equals("New"))
                {
                    objrow = DT.NewRow();
                    objrow[0] = "99201";
                    DT.Rows.Add(objrow);

                    objrow = DT.NewRow();
                    objrow[0] = "99202";
                    DT.Rows.Add(objrow);

                    objrow = DT.NewRow();
                    objrow[0] = "99203";
                    DT.Rows.Add(objrow);


                    objrow = DT.NewRow();
                    objrow[0] = "99204";
                    DT.Rows.Add(objrow);


                    objrow = DT.NewRow();
                    objrow[0] = "99205";
                    DT.Rows.Add(objrow);
                }
                else
                {
                    objrow = DT.NewRow();
                    objrow[0] = "99211";
                    DT.Rows.Add(objrow);

                    objrow = DT.NewRow();
                    objrow[0] = "99212";
                    DT.Rows.Add(objrow);

                    objrow = DT.NewRow();
                    objrow[0] = "99213";
                    DT.Rows.Add(objrow);


                    objrow = DT.NewRow();
                    objrow[0] = "99214";
                    DT.Rows.Add(objrow);


                    objrow = DT.NewRow();
                    objrow[0] = "99215";
                    DT.Rows.Add(objrow);
                }

            }
            else
            {
                objrow = DT.NewRow();
                objrow[0] = "99281";
                DT.Rows.Add(objrow);

                objrow = DT.NewRow();
                objrow[0] = "99282";
                DT.Rows.Add(objrow);

                objrow = DT.NewRow();
                objrow[0] = "99283";
                DT.Rows.Add(objrow);


                objrow = DT.NewRow();
                objrow[0] = "99284";
                DT.Rows.Add(objrow);


                objrow = DT.NewRow();
                objrow[0] = "99285";
                DT.Rows.Add(objrow);
            }

            DT.AcceptChanges();

            drpENMCode.DataSource = DT;
            drpENMCode.DataValueField = "Code";
            drpENMCode.DataTextField = "Code";
            drpENMCode.DataBind();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindScreenCustomization();

                BindENMCode();
                if (Convert.ToString(ViewState["EMR_EMCODE_MANUAL"]) == "1")
                {
                    drpENMCode.Visible = true;
                    gvCPT.Visible = false;
                    txtENMCode.Visible = false;
                    BindEMRS_PT_DETAILS();

                }

                BindWEMR_spS_GetEMRS();

                //  BindWEMR_spS_GetEMCPTInvoice();
                CPTCodeResultvaluate();



            }
        }

        protected void btnEvaluate_Click(object sender, EventArgs e)
        {

            if (Convert.ToString(ViewState["EMR_EMCODE_MANUAL"]) == "1")
            {
                string Criteria = " EMRS_SEQNO='" + Convert.ToString(Session["EMR_ID"]) + "' AND EMRS_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                string FieldNameWithValues = " EMRS_CPT ='" + drpENMCode.SelectedValue + "', EMRS_CPT_MANUAL='" + drpENMCode.SelectedValue + "'";
                CommonBAL objCom = new CommonBAL();
                objCom.fnUpdateTableData(FieldNameWithValues, "EMRS_PT_DETAILS", Criteria);
            }
            else
            {
                CPTCodeResultvaluate();
            }
        }
    }
}