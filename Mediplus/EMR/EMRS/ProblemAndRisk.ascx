﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProblemAndRisk.ascx.cs" Inherits="Mediplus.EMR.EMRS.ProblemAndRisk" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/EMR/EMRS/RiskAssessmentControl.ascx" TagPrefix="UC1" TagName="RiskAssessmentControl" %>

 

<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" >
    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }

    function BindScore(NumberID, PointID, SCoreID) {

 
        var vNumber=document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanelProblemRisk_ProblemAndRisk_" + NumberID).value;
        var vPoints=document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanelProblemRisk_ProblemAndRisk_" + PointID).value;
         
        var vScore;
         vScore = vNumber * vPoints;
       
        document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanelProblemRisk_ProblemAndRisk_" + SCoreID).value = vScore;

    }
</script>

<table style="width:100%">
    <tr>
        <td>
 
<div id="ProblemStatusContainer">
    <fieldset>
        <legend class="lblCaption1" style="font-weight: bold;">Problem Status</legend>        
        <table class="table spacy">
            <tr>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server" ></asp:PlaceHolder>
            </tr>
        </table>
    </fieldset>
      
</div>
        </td>
    </tr>
<tr>
        <td>

<div id="RiskAssessmentContainer">
    <h2>Risk Assessment</h2>
    
     <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelDataPoints" HeaderText="Presenting Problems" Width="100%">
            <ContentTemplate>
                 <UC1:RiskAssessmentControl ID="RiskAssessmentControl1" runat="server"  SegmentType="RISK_PPS"  />
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanelProblemRisk" HeaderText="Diagnostic Procedures" Width="100%">
            <ContentTemplate>
                <UC1:RiskAssessmentControl ID="RiskAssessmentControl2" runat="server"  SegmentType="RISK_DGP"  />
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanelCPTCodeResult" HeaderText="Management Options Seleected" Width="100%">
            <ContentTemplate>
                   <UC1:RiskAssessmentControl ID="RiskAssessmentControl3" runat="server"  SegmentType="RISK_MOS"  />
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
</div>
        </td>
    </tr>

</table>