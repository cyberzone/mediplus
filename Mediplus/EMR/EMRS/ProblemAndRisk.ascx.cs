﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;



namespace Mediplus.EMR.EMRS
{
    public partial class ProblemAndRisk : System.Web.UI.UserControl
    {
        private static SegmentBAL _segmentBAL = new SegmentBAL();

        EMRSBAL objEMRS = new EMRSBAL();
        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public DataSet GetSegmentMaster()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " and esm_type='EMRS_PS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }


        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (Session["SegmentVisit"] != "" && Session["SegmentVisit"] != null)
            {

                DS = (DataSet)Session["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }


        public void GetEMRS()
        {
            DataSet DS = new DataSet();

            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRSDatas();
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["EMRSData"] = DS;
            }

        }

        public void GetEMRSDatas(string SubType, out bool _checked, out string Comments, out string Number, out string Points, out string Score)
        {
            _checked = false;
            Comments = "";
            Number = "";
            Points = "";
            Score = "";

            DataSet DS = new DataSet();

            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRSDatas();
            if (DS.Tables[0].Rows.Count > 0)
            {
                string Selected = "";

                if (DS.Tables[0].Rows[0].IsNull("Selected") == false && Convert.ToString(DS.Tables[0].Rows[0]["Selected"]) != "")
                {
                    Selected = Convert.ToString(DS.Tables[0].Rows[0]["Selected"]);
                    _checked = Selected != null && Selected.Equals("1");

                }
                if (DS.Tables[0].Rows[0].IsNull("Comments") == false && Convert.ToString(DS.Tables[0].Rows[0]["Comments"]) != "")
                    Comments = Convert.ToString(DS.Tables[0].Rows[0]["Comments"]);

                if (DS.Tables[0].Rows[0].IsNull("Number") == false && Convert.ToString(DS.Tables[0].Rows[0]["Number"]) != "")
                    Number = Convert.ToString(DS.Tables[0].Rows[0]["Number"]);

                if (DS.Tables[0].Rows[0].IsNull("Points") == false && Convert.ToString(DS.Tables[0].Rows[0]["Points"]) != "")
                    Points = Convert.ToString(DS.Tables[0].Rows[0]["Points"]);

                if (DS.Tables[0].Rows[0].IsNull("Score") == false && Convert.ToString(DS.Tables[0].Rows[0]["Score"]) != "")
                    Score = Convert.ToString(DS.Tables[0].Rows[0]["Score"]);


            }

        }

        public void GetEMRSDatasSelected(string Criteria, out  bool Selected, out string Comments, out string Number, out string Points, out string Score)
        {
            Selected = false;
            Comments = "";
            Number = "";
            Points = "";
            Score = "";

            DataSet DS = new DataSet();

            if (ViewState["EMRSData"] != "" && ViewState["EMRSData"] != null)
            {

                DS = (DataSet)ViewState["EMRSData"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {
                    if (DR.IsNull("Selected") == false && Convert.ToString(DR["Selected"]) != "")
                        Selected = Convert.ToBoolean(DR["Selected"]);

                    if (  DR.IsNull("Comments") == false && Convert.ToString(DR["Comments"]) != "")
                        Comments = Convert.ToString(DR["Comments"]);

                    if (DR.IsNull("Number") == false && Convert.ToString(DR["Number"]) != "")
                        Number = Convert.ToString(DR["Number"]);

                    if (DR.IsNull("Points") == false && Convert.ToString(DR["Points"]) != "")
                        Points = Convert.ToString(DR["Points"]);

                    if (DR.IsNull("Score") == false && Convert.ToString(DR["Score"]) != "")
                        Score = Convert.ToString(DR["Score"]);
                }
            }



        }

        public void CreateSegCtrls()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();

            HtmlGenericControl trhe = new HtmlGenericControl("tr");

            HtmlGenericControl tdh1 = new HtmlGenericControl("td");
            HtmlGenericControl tdh2 = new HtmlGenericControl("td");
            HtmlGenericControl tdh3 = new HtmlGenericControl("td");
            HtmlGenericControl tdh4 = new HtmlGenericControl("td");
            HtmlGenericControl tdh5 = new HtmlGenericControl("td");


            Label lblH1 = new Label();
            lblH1.Text = "Assessment Status";
            lblH1.CssClass = "lblCaption1";
            lblH1.Style.Add("Font-Weight", "bold");

            Label lblH2 = new Label();
            lblH2.Text = "Problems";
            lblH2.CssClass = "lblCaption1";
            lblH2.Style.Add("Font-Weight", "bold");

            Label lblH3 = new Label();
            lblH3.Text = "Number";
            lblH3.CssClass = "lblCaption1";
            lblH3.Style.Add("Font-Weight", "bold");

            Label lblH4 = new Label();
            lblH4.Text = "Points";
            lblH4.CssClass = "lblCaption1";
            lblH4.Style.Add("Font-Weight", "bold");

            Label lblH5 = new Label();
            lblH5.Text = "Score";
            lblH5.CssClass = "lblCaption1";
            lblH5.Style.Add("Font-Weight", "bold");

            tdh1.Controls.Add(lblH1);
            tdh2.Controls.Add(lblH2);
            tdh3.Controls.Add(lblH3);
            tdh4.Controls.Add(lblH4);
            tdh5.Controls.Add(lblH5);

            tdh1.Style.Add("text-align", "left");
            tdh1.Style.Add("width", "50%");
            tdh1.Style.Add("height", "10px");

            tdh2.Style.Add("text-align", "left");
            tdh2.Style.Add("width", "20%");
            tdh2.Style.Add("height", "10px");

            tdh3.Style.Add("text-align", "left");
            tdh3.Style.Add("width", "10%");
            tdh3.Style.Add("height", "10px");

            tdh4.Style.Add("text-align", "left");
            tdh4.Style.Add("width", "10%");
            tdh4.Style.Add("height", "10px");

            tdh5.Style.Add("text-align", "left");
            tdh5.Style.Add("width", "10%");
            tdh5.Style.Add("height", "10px");

            trhe.Controls.Add(tdh1);
            trhe.Controls.Add(tdh2);
            trhe.Controls.Add(tdh3);
            trhe.Controls.Add(tdh4);
            trhe.Controls.Add(tdh5);

            PlaceHolder1.Controls.Add(trhe);
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                DS1 = SegmentTemplatesGet(Criteria1);



                Int32 i = 1;
                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {

                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND Type='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND SubType='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND FieldName='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";
                    bool _checked = false;
                    string Comments = "", Number = "", Points = "", Score = "";
                    GetEMRSDatasSelected(Criteria2, out _checked, out  Comments, out Number, out Points, out  Score);



                    // GetEMRSDatas(Convert.ToString(DR1["EST_SUBTYPE"]), out  _checked, out  Comments, out Number, out Points,out  Score);



                    HtmlGenericControl tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td1 = new HtmlGenericControl("td");
                    HtmlGenericControl td2 = new HtmlGenericControl("td");
                    HtmlGenericControl td3 = new HtmlGenericControl("td");
                    HtmlGenericControl td4 = new HtmlGenericControl("td");
                    HtmlGenericControl td5 = new HtmlGenericControl("td");






                    Label lblCaption = new Label();
                    lblCaption.ID = "lbl" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    lblCaption.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                    lblCaption.CssClass = "lblCaption1";


                    TextBox txtComments = new TextBox();
                    txtComments.ID = "txtComments" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    txtComments.Text = _checked == true ? Comments : "";
                    txtComments.Width = 150;
                    txtComments.Height = 15;
                    txtComments.Style.Add("resize", "none");


                    TextBox txtNumber = new TextBox();
                    txtNumber.ID = "txtNumber" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    txtNumber.Text = _checked == true ? Number : "";
                    txtNumber.Width = 30;
                    txtNumber.Height = 15;
                    txtNumber.Style.Add("resize", "none");
                    


                    TextBox txtPoints = new TextBox();
                    txtPoints.ID = "txtPoints" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    txtPoints.Text = Convert.ToString(DR["ESM_POINTS"]) == null ? "" : Convert.ToString(DR["ESM_POINTS"]);
                    txtPoints.Width = 30;
                    txtPoints.Height = 15;
                    txtPoints.Style.Add("resize", "none");
                    txtPoints.Enabled = false;


                    TextBox txtScore = new TextBox();
                    txtScore.ID = "txtScore" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    txtScore.Text = _checked == true ? Score : "";
                    txtScore.Width = 30;
                    txtScore.Height = 15;
                    txtScore.Style.Add("resize", "none");
                    txtScore.Enabled = false;

                    txtNumber.Attributes.Add("onblur", "BindScore('" + txtNumber.ID + "','" + txtPoints.ID + "','" + txtScore.ID + "')");

                    td1.Controls.Add(lblCaption);
                    td2.Controls.Add(txtComments);
                    td3.Controls.Add(txtNumber);
                    td4.Controls.Add(txtPoints);
                    td5.Controls.Add(txtScore);


                    tr.Controls.Add(td1);
                    tr.Controls.Add(td2);
                    tr.Controls.Add(td3);
                    tr.Controls.Add(td4);
                    tr.Controls.Add(td5);


                    PlaceHolder1.Controls.Add(tr);




                    i = i + 1;
                }


            }
        }

        public string fnSave1(string Type, string SubType, string FieldName, string selected, string Comments, string Number, string Points, string Score)
        {
            objEMRS = new EMRSBAL();

            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);

            objEMRS.type = Type;//"EMRS_DP";
            objEMRS.subtype = SubType;
            objEMRS.selected = selected;
            objEMRS.comments = Comments;
            objEMRS.number = Number;
            objEMRS.points = Points;
            objEMRS.score = Score;
            objEMRS.fieldname = FieldName;
            objEMRS.EMRSDataAdd();
            GetEMRS();
            return "";
        }

        public void fnProblemAndRiskSave()
        {
            try
            {

                DataSet DS = new DataSet();

                DS = GetSegmentMaster();
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();
                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                    DS1 = SegmentTemplatesGet(Criteria1);

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {
                        
                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue, out  strValueYes);
                        // CheckBox chk = (CheckBox)PlaceHolder1.FindControl("chk" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                        TextBox txtComments = (TextBox)PlaceHolder1.FindControl("txtComments" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                        TextBox txtNumber = (TextBox)PlaceHolder1.FindControl("txtNumber" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                        TextBox txtPoints = (TextBox)PlaceHolder1.FindControl("txtPoints" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                        TextBox txtScore = (TextBox)PlaceHolder1.FindControl("txtScore" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));

                        strValueYes = txtNumber.Text.Trim() != "" ? "1" : "0";
                        if (txtNumber.Text.Trim() != "")
                        {
                           
                            fnSave1(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR1["EST_SUBTYPE"]), Convert.ToString(DR1["EST_FIELDNAME"]), strValueYes, txtComments.Text, txtNumber.Text, txtPoints.Text, txtNumber.Text);
                        }

                        i = i + 1;
                    }
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ProblemAndRisk.fnProblemAndRiskSave");
                TextFileWriting(ex.Message.ToString());
            }


        }


        public void fnRiskAssessmentSave()
        {
            try
            {
                RiskAssessmentControl1.fnRiskAssessmentSave();
                RiskAssessmentControl2.fnRiskAssessmentSave();
                RiskAssessmentControl3.fnRiskAssessmentSave();
                GetEMRS();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ProblemAndRisk.fnRiskAssessmentSave");
                TextFileWriting(ex.Message.ToString());
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                GetEMRS();
                // GetEMRSDatas();
            }

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls();

        }
    }
}