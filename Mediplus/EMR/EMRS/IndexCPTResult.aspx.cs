﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.EMRS
{
    public partial class IndexCPTResult : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void SegmentVisitDtlsGet(string Criteria)
        {

            SegmentBAL _segmentBAL = new SegmentBAL();
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["SegmentVisit"] = DS;
            }




        }
        public void SegmentVisitDtlsbyptGet(string Criteria)
        {

            SegmentBAL _segmentBAL = new SegmentBAL();
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["SegmentVisitByPT"] = DS;
            }




        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    //ComplaintHistory uc =
                    //      (ComplaintHistory)Page.LoadControl("ComplaintHistory.ascx");
                    //PlaceHolderCompHist.Controls.Add(uc);
                    //uc.fnSave();
                    //CPTCodeResult ucCPTResult =
                    //                      (CPTCodeResult)Page.LoadControl("CPTCodeResult.ascx");
                    //PlaceHolderCPTResult.Controls.Add(ucCPTResult);

                    SegmentVisitDtlsGet(" 1=1 ");
                    SegmentVisitDtlsbyptGet(" 1=1 ");

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

     
    }
}