﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.EMRS
{
    public partial class Index : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void SegmentVisitDtlsGet(string Criteria)
        {

            SegmentBAL _segmentBAL = new SegmentBAL();
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["SegmentVisit"] = DS;
            }


          

        }
        public void SegmentVisitDtlsbyptGet(string Criteria)
        {

            SegmentBAL _segmentBAL = new SegmentBAL();
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["SegmentVisitByPT"] = DS;
            }




        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                if (!IsPostBack)
                {
                    //ComplaintHistory uc =
                    //      (ComplaintHistory)Page.LoadControl("ComplaintHistory.ascx");
                    //PlaceHolderCompHist.Controls.Add(uc);
                    //uc.fnSave();
                    //CPTCodeResult ucCPTResult =
                    //                      (CPTCodeResult)Page.LoadControl("CPTCodeResult.ascx");
                    //PlaceHolderCPTResult.Controls.Add(ucCPTResult);

                    SegmentVisitDtlsGet(" 1=1 ");
                    SegmentVisitDtlsbyptGet(" 1=1 ");

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (TabContainer1.ActiveTabIndex  == 0)
                //{
                    ComplaintHistory.fnSave();
                //}
                //if (TabContainer1.ActiveTabIndex == 1)
                //{
                    DataPoints.fnSave();
                //}
                //if (TabContainer1.ActiveTabIndex == 2)
                //{
                    ProblemAndRisk.fnProblemAndRiskSave();
                    ProblemAndRisk.fnRiskAssessmentSave();
                    TabCPTCodeResult.CPTCodeResultvaluate();

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
               // }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}