﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComplaintHistory.ascx.cs" Inherits="Mediplus.EMR.EMRS.ComplaintHistory" %>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

    function HPICheckSelect() {
        document.getElementById("Location").checked = true;
        document.getElementById("Quality").checked = true;
        document.getElementById("Timings").checked = true;
        document.getElementById("Severity").checked = true;
        document.getElementById("Duration").checked = true;
        document.getElementById("Context").checked = true;
        document.getElementById("ModifyingFactors").checked = true;
        document.getElementById("SignsSymptoms").checked = true;
    }


</script>


<table class="table spacy">
    <tr>
        <th style="text-align: left;" class="lblCaption1">
            <label for="ChiefComplaint">Chief Complaint</label></th>
    </tr>
    <tr>
        <td>
            <textarea name="ChiefComplaint" id="ChiefComplaint" runat="server" style="width: 100%; height: 100px;" readonly="readonly"></textarea>
        </td>
    </tr>
</table>


<table class="table spacy">
    <tr>
        <th colspan="8" style="text-align: left;" class="lblCaption1">History of present Illness (HPI)</th>
    </tr>
    <tr>
        <td>
            <asp:CheckBox ID="chkLocation" class="lblCaption1" runat="server" Text="Location" Enabled="false" />
        </td>
        <td>
            <asp:CheckBox ID="chkQuality" class="lblCaption1" runat="server" Text="Quality" Enabled="false"/>
        </td>
        <td>
            <asp:CheckBox ID="chkTimings" class="lblCaption1" runat="server" Text="Timings" Enabled="false" />
        </td>
        <td>
            <asp:CheckBox ID="chkSeverity" class="lblCaption1" runat="server" Text="Severity"  Enabled="false"/>
        </td>

        <td>
            <asp:CheckBox ID="chkDuration" class="lblCaption1" runat="server" Text="Duration" Enabled="false" />
        </td>
        <td>
            <asp:CheckBox ID="chkContext" class="lblCaption1" runat="server" Text="Context"  Enabled="false"/>
        </td>
        <td>
            <asp:CheckBox ID="chkModifyingFactors" class="lblCaption1" runat="server" Text="Modifying Factor"  Enabled="false"/>
        </td>
        <td>
            <asp:CheckBox ID="chkSignsSymptoms" class="lblCaption1" runat="server" Text="Associated Signs & Symptoms" Enabled="false"/>
        </td>

    </tr>
    <tr>
        <td colspan="8">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">
            <input type="checkbox" name="ChronicStatus" id="chkChronicStatus" runat="server" value="1" class="hpi-row" data-rel="two" />
            <label for="ChronicStatus" class="lblCaption1">Status of three Chronic or inactive problems</label></td>
        <td colspan="4">
            <input type="checkbox" name="LimitedHistory" id="chkLimitedHistory" runat="server" value="1" class="hpi-row" data-rel="three" />
            <label for="LimitedHistory" class="lblCaption1">History Limited Due to</label>
            <input type="text" name="HistoryReason" id="txtHistoryReason" runat="server" size="40" />
        </td>
    </tr>
</table>


<table class="table spacy">
    <tr>
        <th colspan="2" style="text-align: left;" class="lblCaption1">
            <label for="IllnessHistory">Details of Illness</label></th>
    </tr>
    <tr>
        <td colspan="2">
            <textarea name="IllnessHistory" id="IllnessHistory" style="width: 100%; height: 100px;" readonly="readonly"><%= IllnessHistory %></textarea></td>
    </tr>
    <tr>
        <th colspan="2" style="text-align: left;" class="lblCaption1">
            <label for="MedicalHistory">Pertinent Medical History (PMH)</label></th>
    </tr>
    <tr>
        <td colspan="2">
            <textarea name="MedicalHistory" id="MedicalHistory" style="width: 100%; height: 100px;" readonly="readonly"><%= MedicalHistory %></textarea></td>
    </tr>
    <tr>
        <th colspan="2" style="text-align: left;" class="lblCaption1">
            <label for="FamilylHistory">Family History (FH)</label></th>
    </tr>
    <tr>
        <td colspan="2">
            <textarea name="FamilylHistory" id="FamilylHistory" style="width: 100%; height: 100px;" readonly="readonly"><%= FamilylHistory %></textarea></td>
    </tr>
    <tr>
        <th colspan="2" style="text-align: left;" class="lblCaption1">
            <label for="SociallHistory">Social History (SH)</label></th>
    </tr>
    <tr>
        <td colspan="2">
            <textarea name="SociallHistory" id="SociallHistory" style="width: 100%; height: 100px;" readonly="readonly"><%= SociallHistory %></textarea></td>
    </tr>
</table>

