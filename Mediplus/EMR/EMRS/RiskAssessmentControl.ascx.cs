﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.EMRS
{
    public partial class RiskAssessmentControl : System.Web.UI.UserControl
    {

        private static SegmentBAL _segmentBAL = new SegmentBAL();

        EMRSBAL objEMRS = new EMRSBAL();

        public string SegmentType { set; get; }

        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BuildRemittance()
        {
            DataTable dt = new DataTable();

            DataColumn BranchID = new DataColumn();
            BranchID.ColumnName = "BranchID";

            DataColumn PatientMasterID = new DataColumn();
            PatientMasterID.ColumnName = "PatientMasterID";

            DataColumn Type = new DataColumn();
            Type.ColumnName = "Type";

            DataColumn SubType = new DataColumn();
            SubType.ColumnName = "SubType";

            DataColumn Selected = new DataColumn();
            Selected.ColumnName = "Selected";

            DataColumn Comments = new DataColumn();
            Comments.ColumnName = "Comments";

            DataColumn Number = new DataColumn();
            Number.ColumnName = "Number";

            DataColumn Points = new DataColumn();
            Points.ColumnName = "Points";

            DataColumn Score = new DataColumn();
            Score.ColumnName = "Score";

            DataColumn FieldName = new DataColumn();
            FieldName.ColumnName = "FieldName";

            dt.Columns.Add(BranchID);
            dt.Columns.Add(PatientMasterID);
            dt.Columns.Add(Type);
            dt.Columns.Add(SubType);
            dt.Columns.Add(Selected);
            dt.Columns.Add(Comments);
            dt.Columns.Add(Number);
            dt.Columns.Add(Points);
            dt.Columns.Add(Score);
            dt.Columns.Add(FieldName);


            DataSet DS = new DataSet();
            DS.Tables.Add(dt);


            ViewState["WEMR_EMRSDataType"] = DS;

        }

        public DataSet GetSegmentMaster()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " and esm_type='" + SegmentType + "'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (Session["SegmentVisit"] != "" && Session["SegmentVisit"] != null)
            {

                DS = (DataSet)Session["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }


        public void GetEMRS()
        {
            DataSet DS = new DataSet();

            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRSDatas();
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["EMRSData"] = DS;
            }

        }

        public void GetEMRSDatas(string SubType, out bool _checked, out string _pointValue)
        {
            _checked = false;
            _pointValue = "";
            DataSet DS = new DataSet();

            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRS();
            if (DS.Tables[0].Rows.Count > 0)
            {

            }

        }

        public void GetEMRSDatasSelected(string Criteria, out  bool Selected)
        {
            Selected = false;
            DataSet DS = new DataSet();

            if (ViewState["EMRSData"] != "" && ViewState["EMRSData"] != null)
            {

                DS = (DataSet)ViewState["EMRSData"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {
                    if (Convert.ToString(DR["Selected"]) != "" && Convert.ToString(DR["Selected"]) != null)
                    {
                        Selected = Convert.ToBoolean(DR["Selected"]);
                    }
                }
            }



        }

        public void CreateSegCtrls()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                DS1 = SegmentTemplatesGet(Criteria1);

                Panel myFieldSet = new Panel();
                myFieldSet.GroupingText = Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                Int32 i = 1;
                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {

                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND Type='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND SubType='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND FieldName='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";
                    bool _checked = false;
                    GetEMRSDatasSelected(Criteria2, out _checked);


                    //string _pointValue;
                    //GetEMRSDatas(Convert.ToString(DR1["EST_SUBTYPE"]), out  _checked, out  _pointValue);



                    //HtmlGenericControl tr = new HtmlGenericControl("tr");

                    //HtmlGenericControl td1 = new HtmlGenericControl("td");




                    CheckBox chk = new CheckBox();
                    chk.ID = "chk" + Convert.ToString(i) + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    chk.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                    chk.CssClass = "lblCaption1";

                    chk.Checked = _checked;


                    myFieldSet.Controls.Add(chk);


                    //td1.Controls.Add(myFieldSet);


                    //tr.Controls.Add(td1);


                    PresentingProblems.Controls.Add(myFieldSet);




                    i = i + 1;
                }


            }
        }

        public void SaveEMRSDataTypeToDB()
        {

            DataSet DS = new DataSet();
            DS = (DataSet)ViewState["WEMR_EMRSDataType"];

            objEMRS = new EMRSBAL();
            objEMRS.WEMR_spI_SaveEMRSDatas(DS.Tables[0]);

        }

        public string fnSave1(string Type, string SubType, string FieldName, string selected, string Value)
        {
            
            objEMRS = new EMRSBAL();

            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);

            objEMRS.type = Type;//"EMRS_DP";
            objEMRS.subtype = SubType;
            objEMRS.selected = selected;
            objEMRS.comments = "";
            objEMRS.number = "";
            objEMRS.points = Value;
            objEMRS.score = "";
            objEMRS.fieldname = FieldName;
            objEMRS.EMRSDataAdd();
                        
             
            /*
            DataSet DS = new DataSet();
            DS = (DataSet)ViewState["WEMR_EMRSDataType"];

            DataRow DR;
            DR = DS.Tables[0].NewRow();
            DR["BranchID"] = Convert.ToString(Session["Branch_ID"]);
            DR["PatientMasterID"] = Convert.ToString(Session["EMR_ID"]);
            DR["Type"] = Type;
            DR["SubType"] = SubType;
            DR["Selected"] = selected;

            DR["Comments"] = "";
            DR["Number"] = 0;
            DR["Points"] = Value;
            DR["Score"] = 0;
            DR["FieldName"] = Value;

            DS.Tables[0].Rows.Add(DR);
            DS.AcceptChanges();
            ViewState["WEMR_EMRSDataType"] = DS;
            */
            return "";
        }

        public void fnRiskAssessmentSave()
        {

            try
            {
                EMRSDataDelete();

                DataSet DS = new DataSet();

                DS = GetSegmentMaster();
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();
                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                    DS1 = SegmentTemplatesGet(Criteria1);

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {
                        
                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue, out  strValueYes);
                        CheckBox chk = (CheckBox)PresentingProblems.FindControl("chk" + Convert.ToString(i) + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));

                        // Label lbl = (Label)PlaceHolder1.FindControl("lbl" + Convert.ToString(i) + "-" + strValue);
                        // lbl.Text = txt.Text;
                        if (chk != null)
                        {
                            strValueYes = chk.Checked == true ? "1" : "0";
                            if (chk.Checked == true)
                            {
                                fnSave1(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR1["EST_SUBTYPE"]), Convert.ToString(DR1["EST_FIELDNAME"]), strValueYes, "");
                            }

                        }
                        i = i + 1;
                    }

                    //SaveEMRSDataTypeToDB();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VisitDetailsRemarks.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        void EMRSDataDelete()
        {
            objEMRS = new EMRSBAL();

            string Criteria = " 1=1 AND EMRSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND   EMRSD_ID='" + Convert.ToString(Session["EMR_ID"])+"'";
            Criteria += " AND EMRSD_TYPE='" + SegmentType + "'";

            objEMRS.EMRSDataDelete(Criteria);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                GetEMRS();
                // GetEMRSDatas();
               // BuildRemittance();
            }

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls();

        }

    }
}