﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.EMRS
{
    public partial class ComplaintHistory : System.Web.UI.UserControl
    {
        private static SegmentBAL _segmentBAL = new SegmentBAL();
        EMRSBAL objEMRS = new EMRSBAL();


        public string IllnessHistory = "", MedicalHistory = "", FamilylHistory = "", SociallHistory = "";

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindPatientHistory()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPH_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objCom.EMR_PTHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ChiefComplaint.InnerText = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]));
            }

        }

        public void SegmentVisitDtlsALLGet()
        {
            DataSet DS = new DataSet();


            if (Session["SegmentVisit"] != "" && Session["SegmentVisit"] != null)
            {

                DS = (DataSet)Session["SegmentVisit"];


                if (DS.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "CC" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                        {

                        }
                        if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "HPI" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                        {

  
                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("LOCATION") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkLocation.Checked = true;
                            }
                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("QUALITY") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkQuality.Checked = true;
                            }
                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("TIMINGS") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkTimings.Checked = true;
                            }
                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("SEVERITY") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkSeverity.Checked = true;
                            }
                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("DURATION") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkDuration.Checked = true;
                            }

                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("CONTEXT") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkContext.Checked = true;
                            }

                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("MODIFYING FACTORS") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkModifyingFactors.Checked = true;
                            }

                            if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper().Equals("ASSOCIATED SIGNS AND SYMPTOMS") && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                            {
                                chkSignsSymptoms.Checked = true;
                            }


                            IllnessHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                        }
                    }
                }
            }


        }


        public void SegmentVisitDtlsALLGetByPTID()
        {
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "HIST_PAST" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        MedicalHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }

                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "DENTAL_HISTORY" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        MedicalHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }

                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "DENTAL_HISTORY" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        MedicalHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }


                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "MENTAL_HISTORY" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        MedicalHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }

                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "NEONATAL_HISTORY" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        MedicalHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }

                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "HIST_FAMILY" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        FamilylHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }


                    if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "HIST_SOCIAL" && Convert.ToString(DR["ESVD_VALUE_YES"]).ToUpper() == "Y")
                    {
                        SociallHistory += (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + " : " + Convert.ToString(DR["ESVD_VALUE"]) + "\n");
                    }

                }
            }


        }

        public string fnSave()
        {
            objEMRS = new EMRSBAL();

            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            objEMRS.location = chkLocation.Checked == true ? "1" : "0";
            objEMRS.quality = chkQuality.Checked == true ? "1" : "0";
            objEMRS.severity = chkSeverity.Checked == true ? "1" : "0";
            objEMRS.signssymptoms = chkSignsSymptoms.Checked == true ? "1" : "0";
            objEMRS.chronicstatus = chkChronicStatus.Checked == true ? "1" : "0";
            objEMRS.timing = chkTimings.Checked == true ? "1" : "0";
            objEMRS.modifyingfactor = chkModifyingFactors.Checked == true ? "1" : "0";
            objEMRS.historyreason = txtHistoryReason.Value;
            objEMRS.limitedhistory = chkLimitedHistory.Checked == true ? "1" : "0";
            objEMRS.context = chkContext.Checked == true ? "1" : "0";
            objEMRS.duration = chkDuration.Checked == true ? "1" : "0";

            objEMRS.SaveEMRSHPI();

            return "";
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    BindPatientHistory();
                    SegmentVisitDtlsALLGet();
                    SegmentVisitDtlsALLGetByPTID();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     ComplaintHistory.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}