﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.EMRS
{
    public partial class DataPoints : System.Web.UI.UserControl
    {
        private static SegmentBAL _segmentBAL = new SegmentBAL();

        EMRSBAL objEMRS = new EMRSBAL();
        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public DataSet GetSegmentMaster()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " and esm_type='EMRS_DP'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public string SegmentVisitDtlsGet(string Criteria)
        {
            string strValue = "";
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                strValue = Convert.ToString(DS.Tables[0].Rows[0]["ESVD_VALUE"]);
            }


            return strValue;

        }


        void BindRadiology()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTRadiology objPro = new EMR_PTRadiology();
            DS = objPro.RadiologyGet(Criteria);
            ViewState["RadCount"] = DS.Tables[0].Rows.Count;

        FunEnd: ;
        }

        void BindLaboratory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPL_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTLaboratory objPro = new EMR_PTLaboratory();
            DS = objPro.LaboratoryGet(Criteria);
            ViewState["LabCount"] = DS.Tables[0].Rows.Count;
        FunEnd: ;
        }

        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            DS = objPro.ProceduresGet(Criteria);
            ViewState["ProcCount"] = DS.Tables[0].Rows.Count;
        FunEnd: ;
        }

        public void GetEMRS()
        {
            DataSet DS = new DataSet();

            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRSDatas();
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["EMRSData"] = DS;
            }

        }

        public void GetEMRSDatas(string SubType, out bool _checked, out string _pointValue)
        {
            _checked = false;
            _pointValue = "";
            DataSet DS = new DataSet();

            objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            DS = objEMRS.WEMR_spS_GetEMRS();
            if (DS.Tables[0].Rows.Count > 0)
            {
                string IsLaboratory = "", IsRadiology = "", IsProcedures = "";

                if (DS.Tables[0].Rows[0].IsNull("IsLaboratory") == false && Convert.ToString(DS.Tables[0].Rows[0]["IsLaboratory"]) != "")
                    IsLaboratory = Convert.ToString(DS.Tables[0].Rows[0]["IsLaboratory"]);

                if (DS.Tables[0].Rows[0].IsNull("IsRadiology") == false && Convert.ToString(DS.Tables[0].Rows[0]["IsRadiology"]) != "")
                    IsRadiology = Convert.ToString(DS.Tables[0].Rows[0]["IsRadiology"]);

                if (DS.Tables[0].Rows[0].IsNull("IsProcedures") == false && Convert.ToString(DS.Tables[0].Rows[0]["IsProcedures"]) != "")
                    IsProcedures = Convert.ToString(DS.Tables[0].Rows[0]["IsProcedures"]);

                //bool _checked = false;
                //string _pointValue = "";

                if (SubType.Equals("001"))
                {
                    _checked = Int32.Parse(IsLaboratory == null ? "0" : IsLaboratory) > 0;
                    _pointValue = (Convert.ToInt32(IsRadiology) >= 4) ? "2" : "1";
                }
                else if (SubType.Equals("002"))
                {
                    _checked = Int32.Parse(IsRadiology == null ? "0" : IsRadiology) > 0;
                    _pointValue = (Convert.ToInt32(IsLaboratory) >= 4) ? "2" : "1";
                }
                else if (SubType.Equals("003"))
                {
                    _checked = Int32.Parse(IsProcedures == null ? "0" : IsProcedures) > 0;
                    _pointValue = (Convert.ToInt32(IsProcedures) >= 4) ? "2" : "1";
                }
            }

        }

        public void GetEMRSDatasSelected(string Criteria, out  bool Selected)
        {
            Selected = false;
            DataSet DS = new DataSet();

            if (ViewState["EMRSData"] != "" && ViewState["EMRSData"] != null)
            {

                DS = (DataSet)ViewState["EMRSData"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {
                    if (Convert.ToString(DR["Selected"]) != "" && Convert.ToString(DR["Selected"]) != null)
                    {
                        Selected = Convert.ToBoolean(DR["Selected"]);
                    }
                }
            }



        }

        public void CreateSegCtrls()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                DS1 = SegmentTemplatesGet(Criteria1);

                Int32 i = 1;
                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValue = "";
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND Type='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND SubType='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND FieldName='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";
                    bool _checked = false;
                    GetEMRSDatasSelected(Criteria2, out _checked);


                    string _pointValue;
                    GetEMRSDatas(Convert.ToString(DR1["EST_SUBTYPE"]), out  _checked, out  _pointValue);



                    HtmlGenericControl tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td1 = new HtmlGenericControl("td");
                    HtmlGenericControl td2 = new HtmlGenericControl("td");
                    td1.Style.Add("text-align", "left");
                    td1.Style.Add("width", "80%");
                    td1.Style.Add("height", "10px");

                    td2.Style.Add("text-align", "left");
                    td2.Style.Add("width", "20%");
                    td1.Style.Add("height", "10px");

                    CheckBox chk = new CheckBox();
                    chk.ID = "chk" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    chk.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                    chk.CssClass = "lblCaption1";

                    chk.Checked = _checked;


                    TextBox txtBox = new TextBox();
                    txtBox.ID = "txt" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    txtBox.Text = _checked == true ? _pointValue : "";
                    txtBox.Width = 50;
                    txtBox.Height = 15;
                    txtBox.Style.Add("resize", "none");
                    txtBox.Enabled = false;


                    HtmlInputHidden hid = new HtmlInputHidden();
                    hid.ID = "hid" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    hid.Value = _pointValue;


                    chk.Attributes.Add("onclick", "BindPintValue('" + chk.ID + "','" + txtBox.ID + "','" + _pointValue + "')");

                    td1.Controls.Add(chk);
                    td2.Controls.Add(txtBox);

                    tr.Controls.Add(td1);




                    tr.Controls.Add(td2);
                    PlaceHolder1.Controls.Add(tr);




                    i = i + 1;
                }


            }
        }

        public string fnSave1(string Type, string SubType, string FieldName,string selected, string Value)
        {
            objEMRS = new EMRSBAL();

            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);

            objEMRS.type = Type;//"EMRS_DP";
            objEMRS.subtype = SubType;
            objEMRS.selected = selected;
            objEMRS.comments = txtDataReviewed.Value ;
            objEMRS.number ="";
            objEMRS.points = Value;
            objEMRS.score ="";
            objEMRS.fieldname = FieldName;
            objEMRS.EMRSDataAdd();
            return "";
        }

        public void fnSave()
        {

            try
            {

                DataSet DS = new DataSet();

                DS = GetSegmentMaster();
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();
                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                    DS1 = SegmentTemplatesGet(Criteria1);

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {
                        string strValue = "", selected="";
                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        strValue = SegmentVisitDtlsGet(Criteria2);
                        CheckBox chk = (CheckBox)PlaceHolder1.FindControl("chk" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                        TextBox txt = (TextBox)PlaceHolder1.FindControl( "txt" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                        TextBox hid = (TextBox)PlaceHolder1.FindControl("hid" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));

                        // Label lbl = (Label)PlaceHolder1.FindControl("lbl" + Convert.ToString(i) + "-" + strValue);
                        // lbl.Text = txt.Text;
                        selected = chk.Checked == true? "1":"0";
                        fnSave1(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR1["EST_SUBTYPE"]), Convert.ToString(DR1["EST_FIELDNAME"]),selected, txt.Text);

                        i = i + 1;
                    }
                }
                
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VisitDetailsRemarks.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindRadiology();
                //BindLaboratory();
                //BindProcedure();

                GetEMRS();
                // GetEMRSDatas();
            }

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls();

        }
    }
}