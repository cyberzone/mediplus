﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class EMR_EducationalForm : System.Web.UI.UserControl
    {
        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();
        string strValue = "";


        public string EMR_ID { set; get; }
        public string PT_ID { set; get; }
        public string DEPT_ID { set; get; }

        void SegmentLoad()
        {
            DataSet DSSegMast = new DataSet();
            string Criteria = " 1=1 and ESM_STATUS=1 AND ESM_TYPE IN ('EDU_PART1','EDU_PART2','EDU_PART3')";

            DSSegMast = objSeg.SegmentMasterGet(Criteria);
            if (DSSegMast.Tables[0].Rows.Count > 0)
            {
                strValue = "<table style='width:100%;' >";
                foreach (DataRow DRSegmast in DSSegMast.Tables[0].Rows)
                {
                    strValue += "<tr><td class='ReportCaption BoldStyle' >" + Convert.ToString(DRSegmast["ESM_SUBTYPE_ALIAS"]) + " </td></tr>";


                    DataSet DSSegDtls = new DataSet();
                    string Criteria1 = " 1=1  AND ESVD_VALUE_YES='Y'";
                    //  Criteria1 += " AND ESVD_TYPE='" + DRSegmast["ESM_TYPE"] + "' AND ESVD_SUBTYPE='" + DRSegmast["ESM_SUBTYPE"] + "'";

                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + DEPT_ID + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + DEPT_ID + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DRSegmast["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DRSegmast["ESM_SUBTYPE"]) + "'";
                    Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";



                    if (Convert.ToString(DRSegmast["ESM_TYPE"]).ToUpper() == "EDU_PART1" && Convert.ToString(DRSegmast["ESM_SUBTYPE"]).ToUpper() == "IEN" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
                    {
                        Criteria1 += " AND ESVD_ID = '" + PT_ID + "'";
                    }
                    else
                    {

                        Criteria1 += " AND ESVD_ID = '" + EMR_ID + "'";
                    }

                    DSSegDtls = objSeg.SegmentVisitDtlsAndSegmentTemplatesGet(Criteria1);
                    if (DSSegDtls.Tables[0].Rows.Count > 0)
                    {
                        strValue += "<tr><td> ";
                        foreach (DataRow DRSegDtls in DSSegDtls.Tables[0].Rows)
                        {
                            strValue += "<input type='checkbox' disabled='disabled' checked />" + Convert.ToString(DRSegDtls["EST_FIELDNAME_ALIAS"]) + " &nbsp;";
                        }
                        strValue += "</td></tr>";
                    }

                    strValue += "</tr>";
                }


                strValue += "</table>";
            }
        }


        void BindEMR_EDUCATIONALFORM()
        {
            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria1 = " 1=1 ";
            Criteria1 += " AND EFM_ID='" + EMR_ID + "'";
            DS = objCom.EMR_EDUCATIONALFORMGet(Criteria1);
            if (DS.Tables[0].Rows.Count > 0)
            {

                lbllanguage.InnerText = Convert.ToString(DS.Tables[0].Rows[0]["EFM_LANGUAGE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_READ"]) == "1")
                {
                    chkRead.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_WRITE"]) == "1")
                {
                    chkWrite.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_SPEAK"]) == "1")
                {
                    chkSpeak.Checked = true;
                }


                lblReligion.InnerText = Convert.ToString(DS.Tables[0].Rows[0]["EFM_RELEGION"]);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                strValue = "";

                BindEMR_EDUCATIONALFORM();
                SegmentLoad();
                lblEduFormDiet.Text = strValue;
            }
        }
    }
}