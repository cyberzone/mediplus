﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
using System.Web.UI.HtmlControls;


namespace Mediplus.EMR.WebReports
{
    public partial class ClinicalSummary : System.Web.UI.Page
    {
        public string EMR_PT_ID = "", SegCC = "", CC = "", HPI = "", HIST_PAST = "", HIST_FAMILY = "", HIST_SOCIAL = "", HIST_SURGICAL, ROS = "", ALLERGY = "", ProcedureNotes = "", NursingOrder = "", ChronicIllness = "";
        public string NatalHistory = "", ENTLocalExam = "", MentalAssessment = "", NarrativeDiagnosis = "";
        public string Branch_ID = "", ClinicalNotes = "", CriticalNote = "", CuppingPoint = "", CuppingHistory = "", DentalRemarks = "", DentalFollowNotes = "", OzoneSession = "", BloodSugarReading = "";
        public string DiagnosisRemarks = "";
        public string DentalHistory = "", ExtraOralExam = "", IntraOralExam = "", GingivalExam;

        public string Problems = "", ProgressNotes = "", Precautions = "", DetailedAssessments = "", TreatmentPlans = "";
        public string INVOICE_ID = "", EM_Code = "", INVOICE_GROSS_AMT = "", INVOICE_NET_AMT = "", HIM_CLAIM_AMOUNT = "", HIM_PT_AMOUNT = "", HIM_CO_INS_AMOUNT = "";


        public string EMR_CLSUMMARY_SEGMENTS = "", EMR_CLSUMMARY_SEGMENTS1 = "";

        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();
        EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
        EMR_PTLaboratory objLab = new EMR_PTLaboratory();
        EMR_PTRadiology objRad = new EMR_PTRadiology();
        EMR_PTProcedure objPTProc = new EMR_PTProcedure();

        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();


        DataSet DS;

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND (SCREENNAME='EMR_DENTAL' OR SCREENNAME='EMR_DEPT'  OR SCREENNAME='EMR') ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DENTAL_TREATMENT_IMAGE_PATH"] = "0";
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";
            ViewState["EMR_EMCODE_MANUAL"] = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DENTAL_TREATMENT_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DENTAL_TREATMENT_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_EMCODE_MANUAL")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_EMCODE_MANUAL"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                }
            }


        }

        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";// AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                hidEMRDeptName.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("EPM_DEP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]) != "")
                    lblDept.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]) != "")
                    lblEmrDate.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                    lblEmrStTime.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATEDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                    lblEmrEndTime.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATEDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_NAME"]) != "")
                    lblPTFullName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                    lblFileNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                    lblNationality.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_NATIONALITY"]));



                if (DS.Tables[0].Rows[0].IsNull("HPM_AGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE"]) != "")
                    lblAge.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE"]));
                lblAgeType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE"]));
                lblAge1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE1"]));
                lblAgeType1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE1"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                    lblMobile.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]) != "")
                    lblEPMID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]) != "")
                    lblSex.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                    lblInsCo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                    lblPolicyType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                    lblPolicyNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]));


                if (DS.Tables[0].Rows[0].IsNull("EPM_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]) != "")
                    lblDrName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]));

                lblDrCode.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                    lblEmiratesID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]));

                if (DS.Tables[0].Rows[0].IsNull("EMR_PT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_TYPE"]) != "")
                    lblVisitType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_TYPE"]));


                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                    lblTreatmentPlan.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                else
                    lblTreatmentPlan.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                    lblFollowupNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]));
                else
                    lblFollowupNotes.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_OPTION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_OPTION"]) != "")
                {
                    lblTreatOptionCaption.Visible = true;
                    lblTreatOption.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_OPTION"]));
                }
                else
                {
                    lblTreatOptionCaption.Visible = false;
                    lblTreatOption.Text = "";
                }



                if (DS.Tables[0].Rows[0].IsNull("EPM_EDUCATION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_EDUCATION"]) != "")
                {
                    lblPTEducationnCaption.Visible = true;
                    lblPTEducationn.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_EDUCATION"]));
                }
                else
                {
                    lblPTEducationnCaption.Visible = false;
                    lblPTEducationn.Text = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("EPM_PROCEDURE_REMARKS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PROCEDURE_REMARKS"]) != "")
                    ProcedureNotes = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PROCEDURE_REMARKS"]));
                else
                    ProcedureNotes = "";




                //if (DS.Tables[0].Rows[0].IsNull("EPM_CLINICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CLINICAL_NOTES"]) != "")
                //    ClinicalNotes = Server.HtmlDecode(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CLINICAL_NOTES"]));
                //else
                //    ClinicalNotes = "";


                if (DS.Tables[0].Rows[0].IsNull("EPM_CRITICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]) != "")
                    CriticalNote = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]));
                else
                    CriticalNote = "";


                if (DS.Tables[0].Rows[0].IsNull("EPM_ULTRASOUND_FINDINGS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_ULTRASOUND_FINDINGS"]) != "")
                    lblUltrasoundFindings.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_ULTRASOUND_FINDINGS"]));
                else
                    lblUltrasoundFindings.Text = "";


                if (DS.Tables[0].Rows[0].IsNull("EPM_NARRATIVE_DIAGNOSIS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_NARRATIVE_DIAGNOSIS"]) != "")
                    NarrativeDiagnosis = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_NARRATIVE_DIAGNOSIS"]));
                else
                    NarrativeDiagnosis = "";


                CC = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]));
                lblCC.Text = CC;
                if (DS.Tables[0].Rows[0].IsNull("EPM_CC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]) != "")
                {
                    lblCC.Text = CC;
                    lblCC.Visible = true;
                }

            }

            hidEMRDeptID.Value = BindDepID(hidEMRDeptName.Value);


            lblTriageStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_TRIAGE_STATUS"]);
            lblTriageStart.Text = Convert.ToString(DS.Tables[0].Rows[0]["EEPM_TRIAGE_STARTDesc"]);
            lblTriageEnd.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_TRIAGE_ENDDesc"]);
        }

        void BindPatientVisit()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_EMR_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DS = objCom.PatientVisitGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["HPV_DATE"] = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATEDesc"]));

                lblTriageReason.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPV_TRIAGE_REASON"]));
            }
        }


      



        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        void BindSegmentVisitDtls1(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataRow[] result = DS.Tables[0].Select(Criteria);

                    if (result.Count() > 0)
                    {
                        strValue += " <span class='ReportCaption BoldStyle' >" + Convert.ToString(result[0]["EMD_MENU_NAME"]) + "</span></BR> ";

                        strValue += "<table style='width:100%;' >";
                        foreach (DataRow DR in result)
                        {
                            strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                        }


                        strValue += "</table>";
                    }

                }
            }

        }


        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " AND ESM_TYPE='" + SegmentType + "'";//HIST_PAST'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        void BindSegmentVisitDtlsWithSubHeader(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "";
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                DS1 = SegmentTemplatesGet(Criteria1);

                IsDataAvailable = false;

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    strHeader = "<table style='width:100%;' >";
                    strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='ReportCaption' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]) + "</td></tr>";

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {

                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue1 = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                        if (strValue1 != "")
                        {
                            IsDataAvailable = true;
                            strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strData += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + strValue1 + "</td></tr>";
                        }

                    }
                    if (IsDataAvailable == true)
                    {
                        strValue += strHeader;
                        strValue += strData;
                        strValue += "</table>";
                        strData = "";
                    }


                }

            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }


        void BindSegmentVisitDtlsWithSubHeader1(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "", strMainHeader = ""; ;
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strMenuName = GetEMRMenuName(strType);

                if (strMenuName != "")
                {
                    strMainHeader += " <span class='ReportCaption BoldStyle' >" + strMenuName + "</span></BR> ";

                }

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();
                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                    Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                    DS1 = SegmentTemplatesGet(Criteria1);

                    IsDataAvailable = false;

                    if (DS1.Tables[0].Rows.Count > 0)
                    {

                        strHeader = "<table style='width:100%;' >";
                        strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='ReportCaption' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]) + "</td></tr>";

                        Int32 i = 1;
                        foreach (DataRow DR1 in DS1.Tables[0].Rows)
                        {

                            string Criteria2 = "1=1 ";
                            Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                            Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                            Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                            string strValue1 = "", strValueYes = "";
                            SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                            if (strValue1 != "")
                            {
                                IsDataAvailable = true;
                                strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                                strData += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + strValue1 + "</td></tr>";
                            }

                        }
                        if (IsDataAvailable == true)
                        {
                            strValue += strHeader;
                            strValue += strData;
                            strValue += "</table>";
                            strData = "";
                        }


                    }
                }
                strValue = strMainHeader + strValue;
            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }


        ////void BindWEMR_spS_GetPainScore()
        ////{
        ////    DS = new DataSet();
        ////    objCom = new CommonBAL();
        ////    string Criteria = " 1=1 ";
        ////    Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";
        ////    DS = objEmrPTMast.GetEMR_PTMaster(Criteria);

        ////    trPain1.Visible = false;
        ////    trPain2.Visible = false;
        ////    trPain3.Visible = false;
        ////    trPain4.Visible = false;
        ////    trPain5.Visible = false;
        ////    trPain6.Visible = false;
        ////    if (DS.Tables[0].Rows.Count > 0)
        ////    {
        ////        string strPainValue = "";
        ////        strPainValue = Convert.ToString(DS.Tables[0].Rows[0]["EPM_PAINSCORE"]);
        ////        if (strPainValue == "0" || strPainValue == "1")
        ////            trPain1.Visible = true;
        ////        if (strPainValue == "2" || strPainValue == "3")
        ////            trPain2.Visible = true;
        ////        if (strPainValue == "4" || strPainValue == "5")
        ////            trPain3.Visible = true;
        ////        if (strPainValue == "6" || strPainValue == "7")
        ////            trPain4.Visible = true;
        ////        if (strPainValue == "8" || strPainValue == "9")
        ////            trPain5.Visible = true;
        ////        if (strPainValue == "10")
        ////            trPain6.Visible = true;


        ////    }

        ////}




        //void BindLaboratoryResult()
        //{

        //    DataSet DS = new DataSet();
        //    objCom = new CommonBAL();
        //    DS = objCom.LaboratoryResultGet(Convert.ToString(Session["Branch_ID"]), lblFileNo.Text.Trim());
        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        gvLabResult.DataSource = DS;
        //        gvLabResult.DataBind();
        //    }
        //    else
        //    {
        //        gvLabResult.DataBind();
        //    }
        //}




        //void BindRadiologyResult()
        //{

        //    DataSet DS = new DataSet();
        //    objCom = new CommonBAL();
        //    DS = objCom.RadiologyResultGet(Convert.ToString(Session["Branch_ID"]), lblFileNo.Text.Trim());
        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        gvRadResult.DataSource = DS;
        //        gvRadResult.DataBind();
        //    }
        //    else
        //    {
        //        gvRadResult.DataBind();
        //    }
        //}

        void BindDentalMaster()
        {
            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1 ";

            Criteria += " AND  DDMR_TRANS_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DS = objDental.EMRDentalMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                DentalRemarks = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_REMARKS"]);
                DentalFollowNotes = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_FOLLOWUP"]);
            }
        }

        void BindEMR_DENTAL_TRANS()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND DDMR_TRANS_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.EMR_DENTAL_TRANSGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDentalTreatment.DataSource = DS;
                gvDentalTreatment.DataBind();
            }
            else
            {
                gvDentalTreatment.DataBind();
            }




        }

        void BindEMR_DENTAL_ADL()
        {

            string Criteria = " 1=1 ";
            // Criteria += " AND EDA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EDA_TRANS_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.EMR_DENTAL_ADLGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                divDentalADL.Visible = true;
                lblPericapical1.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_PERIAPICAL1"]);
                lblPericapical2.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_PERIAPICAL2"]);
                lblBitewing1.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_BITWING1"]);
                lblBitewing2.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_BITWING2"]);
                lblOcclusal1.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_OCCLUSAL1"]);
                lblOcclusal2.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_OCCLUSAL2"]);
                lblPanoramic1.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_PANORAMIC1"]);
                lblPanoramic2.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_DT_PANORAMIC2"]);


                lblPericapical1Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_PERIAPICAL1"]);
                lblPericapical2Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_PERIAPICAL2"]);
                lblBitewing1Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_BITWING1"]);
                lblBitewing2Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_BITWING2"]);
                lblOcclusal1Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_OCCLUSAL1"]);
                lblOcclusal2Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_OCCLUSAL2"]);
                lblPanoramic1Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_PANORAMIC1"]);
                lblPanoramic2Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["EDA_DC_REMARKS_PANORAMIC2"]);

            }


        }

        void BindDepartmentImage(string SegmentType, string strImgID)
        {



            string DentalPath = "", strFileNo = "", strEPMDate = "", PBMI_FileName = "";



            strFileNo = lblFileNo.Text.Trim().Replace("/", "_");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
            strEPMDate = lblEmrDate.Text.Trim().Replace("/", "");

            PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + Convert.ToString(ViewState["EMR_ID"]) + "@" + SegmentType + ".jpg";

            // 00028@23122014@11@DCF

            if (File.Exists(@DentalPath + SegmentType + @"\" + strFileNo + @"\" + PBMI_FileName))
            {
                if (strImgID == "img1")
                {
                    img1.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img1.Visible = true;
                }

                if (strImgID == "img2")
                {
                    img2.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img2.Visible = true;
                }

                if (strImgID == "img3")
                {
                    img3.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img3.Visible = true;
                }


                if (strImgID == "img4")
                {
                    img4.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img4.Visible = true;
                }

                if (SegmentType == "img5")
                {
                    img5.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img5.Visible = true;
                }

                if (strImgID == "img6")
                {
                    img6.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img6.Visible = true;
                }

                if (strImgID == "img7")
                {
                    img7.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img7.Visible = true;
                }

                if (strImgID == "img8")
                {
                    img8.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img8.Visible = true;
                }


                if (strImgID == "img9")
                {
                    img9.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img9.Visible = true;
                }

                if (SegmentType == "img10")
                {
                    img10.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img10.Visible = true;
                }

            }



        }

        //void BindNursingtInstruction()
        //{
        //    DataSet DS = new DataSet();
        //    string Criteria = " 1=1 ";
        //    Criteria += " AND EPC_BRANCH_ID	 = '" + Convert.ToString(Session["Branch_ID"]) + "'";
        //    Criteria += " AND EPC_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";


        //    DS = objCom.NursingOrderGet(Criteria);
        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        if (DS.Tables[0].Rows[0].IsNull("EPC_INSTTRUCTION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_INSTTRUCTION"]) != "")
        //            NursingOrder = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPC_INSTTRUCTION"]));
        //        else
        //            NursingOrder = "";
        //    }


        //}

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = Convert.ToString(Session["EMR_SEGMENTS_SAVE_PTID"]).Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            //   Criteria += " AND (  ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";


            if (GlobalValues.FileDescription.ToUpper() == "SMCH")
            {
                //// Criteria += " AND   ESVD_TYPE in ( " + strSegmentType + " ) ";

                string CriteriaTemp = "";


                string strVisitDate = Convert.ToString(ViewState["HPV_DATE"]);
                DateTime dtVisitDate, dtToday;
                TimeSpan Diff;

                if (strVisitDate != "")
                {
                    dtVisitDate = DateTime.ParseExact(strVisitDate, "dd/MM/yyyy", null);
                    dtToday = DateTime.ParseExact("29/01/2018", "dd/MM/yyyy", null);    //DateTime.Today.ToString()
                    Diff = dtVisitDate.Date - dtToday.Date;

                    if (Diff.Days <= 0)
                    {
                        CriteriaTemp = " AND ( (ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR  ESVD_ID='" + EMR_PT_ID + "' )  AND ESVD_EMR_ID IS NULL)";
                        CriteriaTemp = Criteria + CriteriaTemp;
                        objSeg = new SegmentBAL();
                        DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(CriteriaTemp);
                    }

                    else
                    {

                        CriteriaTemp = " AND  (ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR  ESVD_EMR_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "')";

                        CriteriaTemp = Criteria + CriteriaTemp;

                        objSeg = new SegmentBAL();
                        DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(CriteriaTemp);

                        if (DS.Tables[0].Rows.Count <= 0)
                        {

                            if (Convert.ToBoolean(ViewState["PatientLatestEMR"]) == true)
                            {

                                CriteriaTemp = " AND (   ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ESVD_PT_ID='" + EMR_PT_ID + "' ) ";
                                CriteriaTemp = Criteria + CriteriaTemp;
                                objSeg = new SegmentBAL();
                                DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(CriteriaTemp);

                                if (DS.Tables[0].Rows.Count <= 0)
                                {
                                    CriteriaTemp = " AND ( ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR   ESVD_ID='" + EMR_PT_ID + "' )";
                                    CriteriaTemp = Criteria + CriteriaTemp;
                                    objSeg = new SegmentBAL();
                                    DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(CriteriaTemp);
                                }

                            }
                        }
                    }

                }
                else
                {
                    Criteria += " AND (  ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ESVD_PT_ID ='" + EMR_PT_ID + "' OR  ESVD_EMR_ID= '" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";
                    objSeg = new SegmentBAL();
                    DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);
                }



            }
            else
            {
                Criteria += " AND (  ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ESVD_PT_ID ='" + EMR_PT_ID + "' OR  ESVD_EMR_ID= '" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";
                objSeg = new SegmentBAL();
                DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            }





            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }


        DataSet GetMenus(string Type)
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = Type; //"OPTH_CHARTS";
            objCom.DEP_ID = hidEMRDeptName.Value;

            DS = objCom.GetMenus();
            return DS;

        }



        void BindMultipleImages()
        {


            string DentalPath = "", strFileNo = "", strEPMDate = "", strFullPath = "";
            string PBMI_FileName = "";
            string SegmentType1 = "EYER";

            string strVisitDate = Convert.ToString(ViewState["HPV_DATE"]);
            string[] arrDate = strVisitDate.Split('/');
            string DateFolder = arrDate[0] + "-" + arrDate[1] + "-" + arrDate[2];

            strFileNo = EMR_PT_ID.Replace("/", "_");      // Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_");  
            strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
            strFullPath = DentalPath + @"\" + SegmentType1 + @"\" + strFileNo + @"\" + DateFolder;

            if (!Directory.Exists(strFullPath))
            {
                goto FunEnd;
            }

            string[] strFileNames = System.IO.Directory.GetFiles(strFullPath);
            for (int FileCount = 0; FileCount < strFileNames.Length; FileCount++)
            {

                PBMI_FileName = strFileNames[FileCount];
                PBMI_FileName = PBMI_FileName.Substring(PBMI_FileName.LastIndexOf(@"\") + 1, PBMI_FileName.Length - (PBMI_FileName.LastIndexOf(@"\") + 1));

                if (System.IO.File.Exists(strFullPath + @"\" + PBMI_FileName))
                {
                    HtmlGenericControl tr1 = new HtmlGenericControl("tr");
                    HtmlGenericControl td1 = new HtmlGenericControl("td");

                    Image img = new Image();
                    img.CssClass = "ImageStyle";
                    // img.ImageUrl = "DisplayScandImage.aspx?EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=EYER&ID=" + Convert.ToString(i);
                    img.ImageUrl = "../Ophthalmology/DisplayScandImage.aspx?PT_ID=" + strFileNo + "&FolderName=" + DateFolder + "&FileName=" + PBMI_FileName + "&SegmentType=EYER";

                    td1.Controls.Add(img);
                    tr1.Controls.Add(td1);
                    PlaceHolder1.Controls.Add(tr1);

                }
            }

        FunEnd: ;
        }

        void BindInvoice()
        {
            INVOICE_ID = "";
            INVOICE_GROSS_AMT = "";
            INVOICE_NET_AMT = "";

            HIM_CLAIM_AMOUNT = "";
            HIM_PT_AMOUNT = "";
            HIM_CO_INS_AMOUNT = "";

            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();

            string Criteria = " 1=1 ";
            Criteria = " HIM_EMR_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DS = objInv.InvoiceMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                INVOICE_ID = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_ID"]);
                INVOICE_GROSS_AMT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]);
                INVOICE_NET_AMT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_NET_AMOUNT"]);


                HIM_CLAIM_AMOUNT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                HIM_PT_AMOUNT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_AMOUNT"]);
                HIM_CO_INS_AMOUNT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CO_INS_AMOUNT"]);


            }
            else
            {
                DS = new DataSet();
                Criteria = " HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_PT_ID='" + EMR_PT_ID + "'  AND CONVERT(VARCHAR(10),HIM_DATE,103)='" + lblEmrDate.Text + "' AND HIM_DR_CODE = '" + lblDrCode.Text + "'";
                DS = objInv.InvoiceMasterGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    INVOICE_ID = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_ID"]);

                    INVOICE_GROSS_AMT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]);
                    INVOICE_NET_AMT = Convert.ToString(DS.Tables[0].Rows[0]["HIM_NET_AMOUNT"]);

                }

            }




        }


        void BindEMRS_PT_DETAILS()
        {
            EM_Code = "";
            DataSet DS = new DataSet();
            EMRSBAL objEMRS = new EMRSBAL();
            string Criteria = "1=1";
            Criteria += " and EMRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' and EMRS_SEQNO='" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DS = objEMRS.EMRS_PT_DETAILSGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(ViewState["EMR_EMCODE_MANUAL"]) == "1")
                {
                    EM_Code = DS.Tables[0].Rows[0]["EMRS_CPT_MANUAL"].ToString();
                }
                else
                {
                    EM_Code = DS.Tables[0].Rows[0]["EMRS_CPT"].ToString();
                }


            }

        }


        string BindPTHeaderSegment(string SegmentType)
        {

            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + SegmentType + "'";  // 'ROS' 'HIST_PAST';

            string strPTIDSegment = Convert.ToString(Session["EMR_SEGMENTS_SAVE_PTID"]); // hidSegTypes_SavePT_ID.Value;

            //if (strPTIDSegment.IndexOf(SegmentType) > -1)
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            //}
            //else
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            //}

            string ESVD_ID = "";

            if (strPTIDSegment.IndexOf("|" + SegmentType + "|") > -1)
            {
                ESVD_ID = EMR_PT_ID;// Convert.ToString(Session["EMR_PT_ID"]);
            }
            else
            {
                ESVD_ID = Convert.ToString(ViewState["EMR_ID"]);// Convert.ToString(Session["EMR_ID"]);
            }


            BindSegmentVisitDtls1(SegmentType, ESVD_ID, out  strValue);

            //SegmentBAL objSeg = new SegmentBAL();
            //DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias1(Criteria);


            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow DR in DS.Tables[0].Rows)
            //    {
            //        strValue += Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + ":&nbsp;";
            //        strValue += " <span class='ReportCaption'  style='color: #FFFF00;font-weight: bold; font-size: 11px;' >" + Convert.ToString(DR["ESVD_VALUE"]) + "</span>, ";

            //    }

            //    strValue += "</BR>";
            //}

            return strValue;
        }

        public void SegmentVisitDtlsALLGet()
        {

            string strSegment = Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS"]);

            string strSegmentDtls = "";
            string[] arrhidSegTypes = strSegment.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                if (arrhidSegTypes[intSegType] != "")
                {
                    strSegmentDtls += BindPTHeaderSegment(arrhidSegTypes[intSegType]);

                }

            }
            EMR_CLSUMMARY_SEGMENTS = strSegmentDtls;



        }


        string BindPTHeaderSegment1(string SegmentType)
        {

            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + SegmentType + "'";  // 'ROS' 'HIST_PAST';

            string strPTIDSegment = Convert.ToString(Session["EMR_SEGMENTS_SAVE_PTID"]); // hidSegTypes_SavePT_ID.Value;

            //if (strPTIDSegment.IndexOf(SegmentType) > -1)
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            //}
            //else
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            //}

            string ESVD_ID = "";

            if (strPTIDSegment.IndexOf("|" + SegmentType + "|") > -1)
            {
                ESVD_ID = EMR_PT_ID;// Convert.ToString(Session["EMR_PT_ID"]);
            }
            else
            {
                ESVD_ID = Convert.ToString(ViewState["EMR_ID"]);// Convert.ToString(Session["EMR_ID"]);
            }


            BindSegmentVisitDtlsWithSubHeader1(SegmentType, ESVD_ID, out  strValue);


            return strValue;
        }

        public void SegmentVisitDtlsALLGet1()
        {

            string strSegment = Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS1"]);

            string strSegmentDtls = "";
            string[] arrhidSegTypes = strSegment.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                if (arrhidSegTypes[intSegType] != "")
                {
                    strSegmentDtls += BindPTHeaderSegment1(arrhidSegTypes[intSegType]);

                }

            }
            EMR_CLSUMMARY_SEGMENTS1 = strSegmentDtls;



        }


        string GetEMRMenuName(string SegType)
        {
            string strMenuName = "";
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();

            string Criteria = " EMD_MENU_ACTION='" + SegType + "'";
            Criteria += "  AND (  EMD_DEPT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EMD_DEPT_ID ='' OR EMD_DEPT_ID is null ) ";
            Criteria += "  AND (  EMD_DEPT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EMD_DEPT_ID_HIDE ='' OR EMD_DEPT_ID_HIDE is null ) ";


            DS = objCom.fnGetFieldValue("*", "EMR_MENU_DETAILS", Criteria, "EMD_MENU_NAME");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strMenuName = Convert.ToString(DS.Tables[0].Rows[0]["EMD_MENU_NAME"]);
            }


            return strMenuName;
        }

        void GetPatientLatestEMR()
        {
            ViewState["PatientLatestEMR"] = true;

            string strLatestEMRID = "";
            CommonBAL objCom = new CommonBAL();
            string Criteria = " EPM_PT_ID='" + EMR_PT_ID + "'";

            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" TOP 1  EPM_ID", "EMR_PT_MASTER", Criteria, "  EPM_DATE desc");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strLatestEMRID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);


                if (strLatestEMRID == Convert.ToString(ViewState["EMR_ID"]))
                {
                    ViewState["PatientLatestEMR"] = true;
                }
                else
                {
                    ViewState["PatientLatestEMR"] = false;
                }
            }



        }

        public void BindVital()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                lblTriageDoneBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedUserName"]);

            }

        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    if (GlobalValues.FileDescription == "ALANAMEL")
                    {
                        divUltrasoundFindings.Visible = true;

                    }
                    ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                    EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                    pnlDiagnosis.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    Laboratories.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    Radiologies.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    Procedures.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    Prescriptions.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                    if (GlobalValues.FileDescription == "HOLISTIC")
                    {
                        PharmacyInternal.Visible = true;
                        PharmacyInternal.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                    }

                    //CPTCodeResult.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    pnlPainScore.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                    VitalSignReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                    ctrlNursingOrder.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);


                    pnlOphthalExamination.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    pnlOpticals.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                    pnlCommonRemarks.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                    GetPatientLatestEMR();
                    BindPatientVisit();
                    BindScreenCustomization();
                    //TextFileWriting("1");
                    BindEMRPTMaster();
                    //TextFileWriting("2");
                  
                    //TextFileWriting("3");
                    string Criteria = " 1=1 ";
                    SegmentVisitDtlsALLGet(Criteria);
                    if (GlobalValues.FileDescription.ToUpper() != "SMCH")
                    {
                        BindInvoice();
                    }


                    if (Convert.ToString(Session["TRIAGE_OPTION"]) == "1")
                    {
                        divTriageDtls.Visible = true;
                        BindVital();
                    }


                    if (GlobalValues.FileDescription == "GHANIM")
                    {
                        lblTreatPlan.Text = "Plan Of Care";

                        lblTreatOptionCaption.Text = "Assessment";

                    }
                    //BELOW GODING FOR HIDE THE EMR DIAGNOSIS AND DISPLAY THE INVOICE DIAGNOSIS
                    ////if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    ////{
                    ////    pnlDiagnosis.Visible = false;
                    ////    pnlInvoiceDiagnosis.Visible = true;

                    ////    pnlInvoiceDiagnosis.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    ////    pnlInvoiceDiagnosis.INVOICE_ID = INVOICE_ID;
                    ////    pnlInvoiceDiagnosis.InvoiceDiagnosisData();

                    ////}

                    BindEMRS_PT_DETAILS();
                    string strValue = "";
                    if (GlobalValues.FileDescription.ToUpper() == "MAMPILLY" || GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {
                        BindSegmentVisitDtls("CC", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        SegCC = strValue;

                    }

                    strValue = "";
                    /////   BindSegmentVisitDtls("HPI", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    HPI = strValue;


                    if (Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS"]) != "")
                    {
                        SegmentVisitDtlsALLGet();
                    }

                    if (Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS1"]) != "")
                    {
                        SegmentVisitDtlsALLGet1();
                    }


                    if (Convert.ToString(Session["EMR_NARRATIVE_DIAGNOSIS"]) == "1")
                    {

                        divNarrativeDiag.Visible = true;
                    }

                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {

                        /////  strValue = "";
                        /// // BindSegmentVisitDtls("Cupping Point", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        /// // CuppingPoint = strValue;

                        ///// strValue = "";
                        //  BindSegmentVisitDtls("Cupping History", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        /////  CuppingHistory = strValue;


                        /////  strValue = "";
                        /////  BindSegmentVisitDtls("Ozone Session", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        /////  OzoneSession = strValue;




                        strValue = "";
                        BindSegmentVisitDtls("Blood Sugar Reading", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        BloodSugarReading = strValue;


                    }


                    //TextFileWriting("9");
                    //////if (hidEMRDeptName.Value.ToUpper() == "PEDIATRIC")
                    //////{
                    //////    strValue = "";
                    //////    BindSegmentVisitDtls("NEONATAL_HISTORY", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    //////    NatalHistory = strValue;
                    //////    divPediatric_natal.Visible = true;

                    //////}
                    ////if (hidEMRDeptName.Value.ToUpper() == "ENT")
                    ////{
                    ////    strValue = "";
                    ////    BindSegmentVisitDtls("LOCAL_EXAM", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    ////    ENTLocalExam = strValue;
                    ////    divENTLocalExam.Visible = true;

                    ////}

                    //////if (hidEMRDeptName.Value.ToUpper() == "PSYCHOLOGY")
                    //////{
                    //////    strValue = "";
                    //////    BindSegmentVisitDtls("MENTAL_ASSESSMENT", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    //////    MentalAssessment = strValue;
                    //////    divPsychologyMentalAss.Visible = true;

                    //////}


                    ctrlEduForm.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                    ctrlEduForm.PT_ID = lblFileNo.Text;
                    ctrlEduForm.DEPT_ID = hidEMRDeptID.Value;

                    //TextFileWriting("10");

                    ////  BindWEMR_spS_GetPainScore();
                    //TextFileWriting("11");


                    if (hidEMRDeptName.Value.ToUpper() == "GYNAECOLOGY")
                    {
                        divGynaecology.Visible = true;
                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("GYNAC_ANTENATAL_ASSESSMENT", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        lblGynaecAntenatalAsses.Text = strValue;

                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("GYNAC_PRENATAL_ASSESSMENT", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        lblGynaecPrenatalAsses.Text = strValue;

                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("GYNAC_POSTPARTUM_ASSESSMENT", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        lblGynaecPostpartumlAsses.Text = strValue;

                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("Menstrual&Gynec.History", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        lblMenstrualGynecHistory.Text = strValue;

                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("Obstetrics History", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        lblObstetricsHistory.Text = strValue;

                    }
                    if (hidEMRDeptName.Value.ToUpper() == "PHYSIOTHERAPY")
                    {
                        divphysiotherapy.Visible = true;

                        strValue = "";
                        BindSegmentVisitDtls("THERAPY_PROBLEMS", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        Problems = strValue;

                        strValue = "";
                        BindSegmentVisitDtls("THERAPY_PROGRESS_NOTES", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        ProgressNotes = strValue;

                        strValue = "";
                        BindSegmentVisitDtls("PRECAUTIONS", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        Precautions = strValue;

                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("THERAPY_DETAILED_ASSESSMENT", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        DetailedAssessments = strValue;

                        strValue = "";
                        BindSegmentVisitDtlsWithSubHeader("THERAPY_TREATMENT_PLANS", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        TreatmentPlans = strValue;

                    }


                    //TextFileWriting("12");

                    //TextFileWriting("13");
                    if (hidEMRDeptName.Value.ToUpper() == "DENTAL" || hidEMRDeptName.Value.ToUpper() == "DENT" || GlobalValues.FileDescription.ToUpper() == "AMWAJ")
                    {
                        //////divDentalHistory.Visible = true;
                        //////strValue = "";
                        //////BindSegmentVisitDtls("DENTAL_HISTORY", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                        //////DentalHistory = strValue;


                        if (GlobalValues.FileDescription.ToUpper() == "MEDICAL_LASER" || GlobalValues.FileDescription.ToUpper() == "AMWAJ")
                        {

                            //////divExtraOralExam.Visible = true;
                            //////strValue = "";
                            //////BindSegmentVisitDtlsWithSubHeader("EXTRA_ORAL_EXAMINATION", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                            //////ExtraOralExam = strValue;

                            //////divIntraOralExam.Visible = true;
                            //////strValue = "";
                            //////BindSegmentVisitDtlsWithSubHeader("INTRA_ORAL_EXAMINATION", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                            //////IntraOralExam = strValue;

                            //////if (GlobalValues.FileDescription.ToUpper() == "MEDICAL_LASER")
                            //////{
                            //////    divGingivalExam.Visible = true;
                            //////    strValue = "";
                            //////    BindSegmentVisitDtls("GINGIVAL_EXAMINATION", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                            //////    GingivalExam = strValue;
                            //////}

                        }

                        BindDentalMaster();
                        BindEMR_DENTAL_TRANS();
                        BindEMR_DENTAL_ADL();
                        divDental.Visible = true;
                    }

                    //TextFileWriting("14");
                    //if (hidEMRDeptName.Value.ToUpper() == "PEDIATRIC")
                    //{
                    //    divDeptImages.Visible = true;
                    //    BindDepartmentImage("PBMI", "img1");
                    //    BindDepartmentImage("PHCAW", "img2");
                    //    BindDepartmentImage("PLAW", "img3");
                    //    BindDepartmentImage("PSAW", "img4");
                    //    BindDepartmentImage("PWSP", "img5");
                    //}

                    //TextFileWriting("15");
                    // BindNursingtInstruction();
                    //TextFileWriting("16"); 

                    if (hidEMRDeptName.Value.ToUpper() == "OPHTHALMOLOGY" || hidEMRDeptName.Value.ToUpper() == "OPTHALMOLOGY" || hidEMRDeptName.Value.ToUpper() == "OPTHAL" || hidEMRDeptName.Value.ToUpper() == "CONSULTANT OPHTHAL" || hidEMRDeptName.Value.ToUpper() == "OPHTHALMOLOGIST" || hidEMRDeptName.Value.ToUpper() == "OPTHAL")
                    {
                        divOphthalmology.Visible = true;
                        BindMultipleImages();
                    }





                    divDeptImages.Visible = true;
                    Int32 i = 1;
                    DataSet DS = new DataSet();



                    if (hidEMRDeptName.Value.ToUpper() == "PEDIATRIC")
                    {

                        DS = GetMenus("PEDIATRICS_OPTIONS");
                    }

                    if (hidEMRDeptName.Value.ToUpper() == "OPHTHALMOLOGY" || hidEMRDeptName.Value.ToUpper() == "OPTHALMOLOGY" || hidEMRDeptName.Value.ToUpper() == "OPTHAL" || hidEMRDeptName.Value.ToUpper() == "CONSULTANT OPHTHAL" || hidEMRDeptName.Value.ToUpper() == "OPHTHALMOLOGIST" || hidEMRDeptName.Value.ToUpper() == "OPTHAL")
                    {
                        DS = GetMenus("OPTH_CHARTS");
                    }


                    if (DS.Tables.Count > 0)
                    {

                        foreach (DataRow DR in DS.Tables[0].Rows)
                        {
                            divDeptImages.Visible = true;
                            BindDepartmentImage(Convert.ToString(DR["MenuAction"]), "img" + Convert.ToString(i));
                            i = i + 1;

                        }
                    }


                    if (hidEMRDeptName.Value.ToUpper() == "DENTAL" || hidEMRDeptName.Value.ToUpper() == "DENT")
                    {
                        divDeptImages.Visible = true;
                        BindDepartmentImage("DC", "img1");
                        BindDepartmentImage("DCF", "img2");
                        BindDepartmentImage("PDC", "img3");


                    }

                    if (hidEMRDeptName.Value.ToUpper() == "PHYSIOTHERAPY")
                    {
                        divDeptImages.Visible = true;
                        BindDepartmentImage("THERAPYC", "img1");



                    }

                    if (hidEMRDeptName.Value.ToUpper() == "ENT")
                    {
                        divDeptImages.Visible = true;
                        BindDepartmentImage("EARC", "img1");
                        BindDepartmentImage("NOSEC", "img2");
                        BindDepartmentImage("THROATC", "img3");
                        BindDepartmentImage("NECKC", "img4");

                    }
                    if (hidEMRDeptName.Value.ToUpper() == "ORTHOPEDIC")
                    {
                        divDeptImages.Visible = true;
                        // BindDepartmentImage("EARC", "img1");

                    }


                    pnlDoctorSignature.DR_ID = lblDrCode.Text;
                    pnlDoctorSignature.EMR_DATE = lblEmrDate.Text.Trim();

                    //if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                    //{
                    //    divClinicalNotes.Visible = true;
                    //}

                    //if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    //{
                    //    divCriticalNote.Visible = true;
                    //}

                    //////if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    //////{
                    //////    divCriticalNote.Visible = true;
                    //////    divCuppingPoint.Visible = true;
                    //////    divCuppingHistory.Visible = true;
                    //////    divOzoneSession.Visible = true;

                    //////    divBloodSugarReading.Visible = true;
                    //////}




                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ClinicalSummary.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}