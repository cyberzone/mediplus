﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="DentalExaminations.aspx.cs" Inherits="Mediplus.EMR.WebReports.DentalExaminations" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx" TagPrefix="UC1" TagName="Diagnosis" %>

<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

        .BoldStyle
        {
            font-weight: bold;
        }

        .ImageStyle
        {
            height: 200px;
            width: 150px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY" />
    <input type="hidden" id="hidEMRDeptID" runat="server" />
    <input type="hidden" id="hidEMRDeptName" runat="server" />

    <table style="width: 100%; text-align: center; vertical-align: top;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img style="padding: 1px; border: none;" src="images/SummerLand_Logo.jpeg" />
            </td>
        </tr>
    </table>
    
    <br />
    <table style="width: 100%;">
        <tr>
            <td colspan="2"  class="lblCaption1 BoldStyle"><u>EXTRA-ORAL EXAMINATION</u>
            </td>
        </tr>
         <tr>
            <td colspan="2" style="height:10px;">

            </td>
        </tr>
        <tr>
            <td colspan="2"  class="lblCaption1 BoldStyle">INSPECTION
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" style="width:200px;">General Appearance:
            </td>
            <td>
                <label id="lblGeneralAppearance" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Swellings
            </td>
            <td>
                <label id="lblSwellings" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle">Skeletal pattem
            </td>
            <td>
                <label id="lblSkeletalPattem" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Lip Competency
            </td>
            <td>
                <label id="lblLipCompetency" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height:10px;">

            </td>
        </tr>
         <tr>
            <td colspan="2"  class="lblCaption1 BoldStyle">PALAPATION
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle">Lymph Nodes
            </td>
            <td>
                <label id="lblLymphNodes" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle">Temporomandibular Joint
            </td>
            <td>
                <label id="lblTempJoint" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Muscles of mastification
            </td>
            <td>
                <label id="lblMusclesMastification" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td colspan="2" style="height:20px;">

            </td>
        </tr>

         <tr>
            <td colspan="2"  class="lblCaption1 BoldStyle"><u>INTRA ORAL EXAMINATION</u>
            </td>
        </tr>

         <tr>
            <td class="lblCaption1 BoldStyle">Soft tissues: 
            </td>
            <td class="lblCaption1">
                Buccal Sulcus &nbsp;&nbsp;
                <label id="lblBuccalSulcus" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
               Lingual Sulcus&nbsp;&nbsp;
                <label id="lblLingualSulcus" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
              Floor of the mouth&nbsp;&nbsp;
                <label id="lblFloormouth" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
             Retromolar Region&nbsp;&nbsp;
                <label id="lblRetromolarRegion" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
        
         <tr>
            <td class="lblCaption1 BoldStyle">Teeth Present:
            </td>
            <td class="lblCaption1">
               Teeth Missing&nbsp;&nbsp;
                <label id="lblTeethMissing" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
          <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
               Extracted&nbsp;&nbsp;
                <label id="lblExtracted" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
              Congenitally Missing&nbsp;&nbsp;
                <label id="lblCongenitallyMissing" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
          <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
              Impacted Teeth&nbsp;&nbsp;
                <label id="lblImpactedTeeth" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle"> 
            </td>
            <td class="lblCaption1">
             Carious Teeth&nbsp;&nbsp;
                <label id="lblCariousTeeth" runat="server" class="lblCaption1"></label>
            </td>
        </tr>
         <tr>
            <td colspan="2" style="height:20px;">

            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" style="vertical-align:top;">
            DIAGNOSIS : 
            </td>
              <td class="lblCaption1" style="vertical-align:top;">
                  <%=Diagnosis %>
                  
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" style="vertical-align:top;">
           Treatment Plan:
            </td>
              <td class="lblCaption1" style="vertical-align:top;">
                  <%=EPM_TREATMENT_PLAN %>
            </td>
        </tr>
    </table>
    <br />


    <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"></UC1:DoctorSignature>
</asp:Content>
