﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class Prescriptions : System.Web.UI.UserControl
    {
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
        public string EMR_ID { set; get; }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
           // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            if (Convert.ToString(Session["EMR_REQUIRED_AUDIT"]) == "1")
            {
                Criteria += " AND (  EPP_STATUS <>'D'  OR EPP_STATUS IS NULL )  ";
            }

            Criteria += " AND EPP_ID  IN (" + EMR_ID + ")";

            DataSet DS = new DataSet();
            CommonBAL objWebrpt = new CommonBAL();
            DS = objPhar.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();

            }
            else
            {
                gvPharmacy.DataBind();
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
          //  if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);

                BindPharmacy();
            }
        }
    }
}