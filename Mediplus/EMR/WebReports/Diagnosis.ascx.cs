﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;



namespace Mediplus.EMR.WebReports
{
    public partial class Diagnosis : System.Web.UI.UserControl
    {
        EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();


        public string EMR_ID { set; get; }

       
        public void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
          //  Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (Convert.ToString(Session["EMR_REQUIRED_AUDIT"]) == "1")
            {
                Criteria += " AND (  EPD_STATUS <>'D'  OR EPD_STATUS IS NULL )  ";
            }

            Criteria += " AND EPD_ID  IN (" + EMR_ID + ")";



            DataSet DS = new DataSet();

            DS = objDiag.DiagnosisGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();

                DataColumn Type = new DataColumn();
                Type.ColumnName = "Type";

                DS.Tables[0].Columns.Add(Type);

                Int32 i = 0;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    if (i == 0)
                    {
                        DR["Type"] = "Principal Diagnosis:";
                    }
                    else
                    {
                        DR["Type"] = "Secondary Diagnosis:";

                    }

                    i = i + 1;
                }

                DS.Tables[0].AcceptChanges();
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();

              

            }
            else
            {
                gvDiagnosis.DataBind();
            }
        FunEnd: ;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);

                if (EMR_ID != "" && EMR_ID  !=null)
                {
                    BindDiagnosis();
                }

            }
        }
    }
}