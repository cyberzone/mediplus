﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class DentalExaminations : System.Web.UI.Page
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string EPM_TREATMENT_PLAN { set; get; }

        public string EXTRA_ORAL_EXAMINATION { set; get; }
        public string INTRA_ORAL_EXAMINATION { set; get; }

        public string Diagnosis { set; get; }



        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();

        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }



        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";// AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                hidEMRDeptName.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]);


                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                    EPM_TREATMENT_PLAN = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));


            }

            hidEMRDeptID.Value = BindDepID(hidEMRDeptName.Value);
        }

        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }



        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + EMR_ID + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " AND ESM_TYPE='" + SegmentType + "'";//HIST_PAST'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        void BindSegmentVisitDtlsWithSubHeader(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "";
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                DS1 = SegmentTemplatesGet(Criteria1);

                IsDataAvailable = false;

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    strHeader = "<table style='width:100%;' >";
                    strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]) + "</td></tr>";

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {

                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue1 = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                        if (strValue1 != "")
                        {
                            IsDataAvailable = true;
                            strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strData += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + strValue1 + "</td></tr>";
                        }

                    }
                    if (IsDataAvailable == true)
                    {
                        strValue += strHeader;
                        strValue += strData;
                        strValue += "</table>";
                        strData = "";
                    }


                }

            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }

        void BindSegmentVisitDtls(string strType, string strSubType, string ESVD_ID)
        {
            string strValueYes = "", strValue = ""; ;
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';

            if (strSubType != "")
            {
                Criteria += " AND ESVD_SUBTYPE = '" + strSubType + "'";
            }




            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                        strValue = Convert.ToString(DR["ESVD_VALUE"]);

                        if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper() == "INSPECTION")
                        {
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "1".ToUpper())
                            {
                                lblGeneralAppearance.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "2".ToUpper())
                            {
                                lblSwellings.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "3".ToUpper())
                            {
                                lblSkeletalPattem.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "4".ToUpper())
                            {
                                lblLipCompetency.InnerText = strValue;
                            }

                        }

                        if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper() == "PALAPATION")
                        {
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "1".ToUpper())
                            {
                                lblLymphNodes.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "2".ToUpper())
                            {
                                lblTempJoint.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "3".ToUpper())
                            {
                                lblMusclesMastification.InnerText = strValue;
                            }


                        }

                        if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper() == "SOFT_TISSUES")
                        {

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "1".ToUpper())
                            {
                                lblBuccalSulcus.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "2".ToUpper())
                            {
                                lblLingualSulcus.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "3".ToUpper())
                            {
                                lblFloormouth.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "4".ToUpper())
                            {
                                lblRetromolarRegion.InnerText = strValue;
                            }
                        }


                        if (Convert.ToString(DR["ESVD_SUBTYPE"]).ToUpper() == "TEETH_PRESENT")
                        {

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "1".ToUpper())
                            {
                                lblTeethMissing.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "2".ToUpper())
                            {
                                lblExtracted.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "3".ToUpper())
                            {
                                lblCongenitallyMissing.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "4".ToUpper())
                            {
                                lblImpactedTeeth.InnerText = strValue;
                            }
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "5".ToUpper())
                            {
                                lblCariousTeeth.InnerText = strValue;
                            }
                        }
                    }

                }
            }

        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            //  Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + EMR_ID + "'";


            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

              string  strValue = "<table style='width:100%;' >";

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    strValue += "<tr>";
                    strValue += "<td style='height:25px;width:80%;'>" + Convert.ToString(DR["EPD_DIAG_CODE"]) + "-" + Convert.ToString(DR["EPD_DIAG_NAME"]) + "</td></tr>";
                }


                strValue += "</table>";
                Diagnosis = strValue;

            }

        FunEnd: ;
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                try
                {

                    EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                   
                    BindEMRPTMaster();
                    BindDiagnosis();

                    string Criteria = " 1=1 ";
                    SegmentVisitDtlsALLGet(Criteria);
                   // string strValue = "";

                    //strValue = "";
                    //BindSegmentVisitDtlsWithSubHeader("EXTRA_ORAL_EXAMINATION", EMR_ID, out  strValue);
                    //EXTRA_ORAL_EXAMINATION = strValue;


                    //strValue = "";
                    //BindSegmentVisitDtlsWithSubHeader("INTRA_ORAL_EXAMINATION", EMR_ID, out  strValue);
                    //INTRA_ORAL_EXAMINATION = strValue;

                    BindSegmentVisitDtls("EXTRA_ORAL_EXAMINATION", "INSPECTION", EMR_ID);
                    BindSegmentVisitDtls("EXTRA_ORAL_EXAMINATION", "PALAPATION", EMR_ID);

                    BindSegmentVisitDtls("INTRA_ORAL_EXAMINATION", "SOFT_TISSUES", EMR_ID);
                    BindSegmentVisitDtls("INTRA_ORAL_EXAMINATION", "TEETH_PRESENT", EMR_ID);

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ClinicalSummary.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}