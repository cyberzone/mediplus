﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class CPTCodeResult : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        EMRSBAL objEMRS = new EMRSBAL();
        CommonBAL objCom = new CommonBAL();

 

        void BindWEMR_spS_GetEMCPTInvoice()
        {
            if (txtENMCode.Text.Trim() == "")
            {
                goto FunEnd;
            }
            objEMRS = new EMRSBAL();
            DataSet DS = new DataSet();
            DS = objEMRS.WEMR_spS_GetEMCPTInvoice(Convert.ToString(Session["Branch_ID"]), EMR_ID, txtENMCode.Text.Trim());
            if (DS.Tables.Count > 0)
            {
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvCPT.DataSource = DS;
                    gvCPT.DataBind();
                }
                else
                {
                    gvCPT.DataBind();
                }
            }

        FunEnd: ;
        }


        void BindWEMR_spS_GetEMRSHPI()
        {
            string HPIResult = "", ROSResult = "N/A", PFSHResult = "N/A", FinalHistoryResult = "", PEResult = "N/A", PSResult = "Minimal", DPResult = "Minimal";
            double PSTotal = 0, DPTotal = 0, RiskTotal = 0;
            int HPICount = 0, ROSCount = 0, PFSHCount = 0, PECount = 0, PEOrganCount = 0, TimeDifference = 0;

            string RiskResult = "Minimal", MDMResult = "", CPTCode = "", RiskPPResult = "", RiskDGPResult = "", RiskMOSResult = "", PatientType = "New";


            if (!Convert.ToString(Session["HPV_VISIT_TYPE"]).Equals("New"))
            {
                PatientType = "Established";
            }
            DataSet DS = new DataSet();
            SegmentBAL objEMRS = new SegmentBAL();
            DS = objEMRS.WEMR_spS_GetSegmentVisits(Convert.ToString(Session["Branch_ID"]), EMR_ID);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string SubType = "";
                string strValue = "", strType = "", strSubType = "";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (DR.IsNull("Value") == false && Convert.ToString(DR["Value"]) != "")
                        strValue = Convert.ToString(DR["Value"]);

                    if (DR.IsNull("Type") == false && Convert.ToString(DR["Type"]) != "")
                        strType = Convert.ToString(DR["Type"]);

                    if (DR.IsNull("SubType") == false && Convert.ToString(DR["SubType"]) != "")
                        strSubType = Convert.ToString(DR["SubType"]);


                    if (strValue.Equals("Y"))
                    {
                        if (strType.Equals("HPI"))
                        {
                            HPICount++;
                        }
                        else if (strType.Equals("ROS"))
                        {
                            ROSCount++;
                        }
                        else if (strType.Equals("HIST_PAST") || strType.Equals("HIST_FAMILY") || strType.Equals("HIST_SOCIAL"))
                        {
                            PFSHCount++;
                        }
                        else if (strType.Equals("PE"))
                        {
                            PECount++;
                        }
                        else if (strType.Equals("PE") && (!SubType.Equals(strSubType)))
                        {
                            PEOrganCount++;
                        }
                    }
                    SubType = Convert.ToString(DR["SubType"]);
                }

            }




            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.WEMR_spS_GetEMRSDatas(Convert.ToString(Session["Branch_ID"]), EMR_ID);
            if (DS.Tables[0].Rows.Count > 0)
            {


                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string strType = "", strSubType = "", strPoints = "";
                    if (DS.Tables[0].Rows[i].IsNull("Type") == false && Convert.ToString(DS.Tables[0].Rows[i]["Type"]) != "")
                        strType = Convert.ToString(DS.Tables[0].Rows[i]["Type"]);

                    if (DS.Tables[0].Rows[i].IsNull("SubType") == false && Convert.ToString(DS.Tables[0].Rows[i]["SubType"]) != "")
                        strSubType = Convert.ToString(DS.Tables[0].Rows[i]["SubType"]);

                    if (DS.Tables[0].Rows[i].IsNull("Points") == false && Convert.ToString(DS.Tables[0].Rows[i]["Points"]) != "")
                        strPoints = Convert.ToString(DS.Tables[0].Rows[i]["Points"]);


                    if (strType.Equals("EMRS_PS"))
                    {
                        PSTotal += Double.Parse(strPoints);
                    }
                    else if (strType.Equals("EMRS_DP"))
                    {
                        DPTotal += Double.Parse(strPoints == "" ? "0" : strPoints);
                    }
                    else if (strType.Equals("RISK_PPS"))
                    {
                        RiskPPResult = strSubType;
                    }
                    else if (strType.Equals("RISK_DGP"))
                    {
                        RiskDGPResult = strSubType;
                    }
                    else if (strType.Equals("RISK_MOS"))
                    {
                        RiskMOSResult = strSubType;
                    }
                }


            }

            int HPICoderCount = 0;

            string ChronicStatus = "", LimitedHistory = "";


            DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.WEMR_spS_GetEMRSHPI(Convert.ToString(Session["Branch_ID"]), EMR_ID);
            if (DS.Tables[0].Rows.Count > 0)
            {



                if (DS.Tables[0].Rows[0].IsNull("Location") == false && Convert.ToString(DS.Tables[0].Rows[0]["Location"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Location"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Context") == false && Convert.ToString(DS.Tables[0].Rows[0]["Context"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Context"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Duration") == false && Convert.ToString(DS.Tables[0].Rows[0]["Duration"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Duration"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("ModifyingFactor") == false && Convert.ToString(DS.Tables[0].Rows[0]["ModifyingFactor"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["ModifyingFactor"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Quality") == false && Convert.ToString(DS.Tables[0].Rows[0]["Quality"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Quality"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Severity") == false && Convert.ToString(DS.Tables[0].Rows[0]["Severity"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Severity"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("SignsSymptoms") == false && Convert.ToString(DS.Tables[0].Rows[0]["SignsSymptoms"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["SignsSymptoms"]) == "1")
                    HPICoderCount++;

                if (DS.Tables[0].Rows[0].IsNull("Timing") == false && Convert.ToString(DS.Tables[0].Rows[0]["Timing"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["Timing"]) == "1")
                    HPICoderCount++;

                if (HPICoderCount != 0)
                    HPICount = HPICoderCount;



                if (DS.Tables[0].Rows[0].IsNull("ChronicStatus") == false && Convert.ToString(DS.Tables[0].Rows[0]["ChronicStatus"]) != "")
                    ChronicStatus = DS.Tables[0].Rows[0]["ChronicStatus"].ToString();

                if (DS.Tables[0].Rows[0].IsNull("LimitedHistory") == false && Convert.ToString(DS.Tables[0].Rows[0]["LimitedHistory"]) != "")
                    LimitedHistory = DS.Tables[0].Rows[0]["LimitedHistory"].ToString();




                if (HPICount >= 4 || (ChronicStatus != null && ChronicStatus.Equals("1")) || (LimitedHistory != null && LimitedHistory.Equals("1")))
                {
                    HPIResult = "Extended";
                }
                else if (HPICount >= 1 && HPICount <= 3) // please verify it in en coding 
                {
                    HPIResult = "Brief";
                }

                lblHPIResult.Text = HPIResult;
            }
            else
            {
                lblHPIResult.Text = "";


            }



            // result for ROS
            if (ROSCount == 1)
            {
                ROSResult = "Pertinent to Problem";
            }
            else if (ROSCount >= 2 && ROSCount <= 9)
            {
                ROSResult = "Extended";
            }
            else if (ROSCount >= 10)
            {
                ROSResult = "Complete";
            }

            // result for PFSH
            if (PFSHCount == 1)
            {
                PFSHResult = "Pertinent to Problem";
            }
            else if (PFSHCount > 1)
            {
                PFSHResult = "Complete";
            }



            int Hx = 0, Exam = 0, MDM = 0;
            // result for Level of History Documented
            if (HPIResult.Equals("Brief") && ROSResult.Equals("N/A") && PFSHResult.Equals("N/A"))
            {
                FinalHistoryResult = "Problem Focused";
                Hx = 1;
            }
            else if (HPIResult.Equals("Brief") && !ROSResult.Equals("N/A"))
            {
                FinalHistoryResult = "Expandable Problem Focused";
                Hx = 2;
            }
            else if (HPIResult.Equals("Brief") || ROSResult.Equals("N/A") || PFSHResult.Equals("N/A"))
            {
                FinalHistoryResult = "Problem Focused";
                Hx = 1;
            }
            else if (HPIResult.Equals("Extended") || ROSResult.Equals("Extended") || PFSHResult.Equals("Pertinent"))
            {
                FinalHistoryResult = "Detailed";
                Hx = 3;
            }
            else if (HPIResult.Equals("Extended") || ROSResult.Equals("Complete") || PFSHResult.Equals("Complete"))
            {
                FinalHistoryResult = "Comprehensive";
                Hx = 4;
            }


            // --------------------------------------------
            string ExaminationType = Convert.ToString(ViewState["ExaminationType"]);
            // Examination Level result
            if (ExaminationType == null)
            {
                ExaminationType = "1997";
            }
            if (ExaminationType.Equals("1995"))
            {
                if (PECount == 1)
                {
                    PEResult = "Problem Focused";
                }
                else if (PECount >= 2 && PECount <= 7)
                {
                    PEResult = "Detailed";
                }
                else if (PECount >= 8)
                {
                    PEResult = "Comprehensive";
                }
            }
            else if (ExaminationType.Equals("1997"))
            {
                if (PECount >= 1 && PECount <= 5)
                {
                    PEResult = "Problem Focused";
                    Exam = 1;
                }
                else if (PECount >= 6 && PECount <= 11)
                {
                    PEResult = "Expandable Problem Focused";
                    Exam = 2;
                }
                else if (PECount >= 12 && PECount <= 17)
                {
                    PEResult = "Detailed";
                    Exam = 3;
                }
                else if (PECount >= 18)
                {
                    PEResult = "Comprehensive";
                    Exam = 4;
                }

                if (PEOrganCount >= 9 && (PECount == 18))
                {
                    PEResult = "Comprehensive";
                    Exam = 4;
                }
                else if (PEOrganCount >= 2 && (PECount == 12 && PECount <= 17))
                {
                    PEResult = "Detailed";
                    Exam = 3;
                }
            }





            lblROSResult.Text = ROSResult;
            lblPFSHResult.Text = PFSHResult;
            lblFinalHistoryResult.Text = FinalHistoryResult;
            lblPEResult.Text = PEResult;

            //--------------------------------------------------------------------------------------------------------



            // Problem Focused
            if (PSTotal == 1)
            {
                PSResult = "Minimal";
            }
            else if (PSTotal == 2)
            {
                PSResult = "Limited";
            }
            else if (PSTotal == 3)
            {
                PSResult = "Multiple";
            }
            else if (PSTotal >= 4)
            {
                PSResult = "Extensive";
            }
            /*else
            {
                PSResult = "Minimal";
            }*/

            // Data Reviewed
            if (DPTotal <= 1)
            {
                DPResult = "Minimal";
            }
            else if (DPTotal == 2)
            {
                DPResult = "Limited";
            }
            else if (DPTotal == 3)
            {
                DPResult = "Multiple";
            }
            else if (DPTotal >= 4)
            {
                DPResult = "Extensive";
            }


            // Risk Assessment result
            if (RiskPPResult.Equals("HIGH") || RiskDGPResult.Equals("HIGH") || RiskMOSResult.Equals("HIGH"))
            {
                RiskResult = "High";
            }
            else if (RiskPPResult.Equals("MOD") || RiskDGPResult.Equals("MOD") || RiskMOSResult.Equals("MOD"))
            {
                RiskResult = "Moderate";
            }
            else if (RiskPPResult.Equals("LOW") || RiskDGPResult.Equals("LOW") || RiskMOSResult.Equals("LOW"))
            {
                RiskResult = "Low";
            }
            else if (RiskPPResult.Equals("MIN") || RiskDGPResult.Equals("MIN") || RiskMOSResult.Equals("MIN"))
            {
                RiskResult = "Minmal";
            }
            /*else
            {
                RiskResult = "Minimal";
            }*/

            // calculating MDM
            if (PSResult.Equals("Minimal") || DPResult.Equals("Minimal") || RiskResult.Equals("Minimal"))
            {
                MDMResult = "Straight Forward";
                MDM = 1;
            }
            else if (PSResult.Equals("Limited") || DPResult.Equals("Limited") || RiskResult.Equals("Low"))
            {
                MDMResult = "Low Complexity";
                MDM = 2;
            }
            else if (PSResult.Equals("Multiple") || DPResult.Equals("Multiple") || RiskResult.Equals("Moderate"))
            {
                MDMResult = "Moderate Complexity";
                MDM = 3;
            }
            else if (PSResult.Equals("Extensive") || DPResult.Equals("Extensive") || RiskResult.Equals("High"))
            {
                MDMResult = "High Complexity";
                MDM = 4;
            }

            lblPSResult.Text = PSResult;
            lblDPResult.Text = DPResult;
            lblRiskResult.Text = RiskResult;
            lblMDMResult.Text = MDMResult;



            // calculating the final en coding    
            if (PatientType.Equals("New"))
            {
                if (FinalHistoryResult.Equals("Problem Focused") && PEResult.Equals("Problem Focused") && MDMResult.Equals("Straight Forward"))
                {
                    CPTCode = "99201";
                }
                else if (FinalHistoryResult.Equals("Expandable Problem Focused") && PEResult.Equals("Expandable Problem Focused")
                    && MDMResult.Equals("Straight Forward"))
                {
                    CPTCode = "99202";
                }
                else if (FinalHistoryResult.Equals("Detailed") && PEResult.Equals("Detailed")
                    && MDMResult.Equals("Low Complexity"))
                {
                    CPTCode = "99203";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive")
                    && MDMResult.Equals("Moderate Complexity"))
                {
                    CPTCode = "99204";
                }
                else if (FinalHistoryResult.Equals("Comprehensive") && PEResult.Equals("Comprehensive")
                    && MDMResult.Equals("High Complexity"))
                {
                    CPTCode = "99205";
                }
            }
            else if (PatientType.Equals("Established"))
            {
                if (FinalHistoryResult.Equals("N/A") || PEResult.Equals("N/A") || MDMResult.Equals(""))
                {
                    CPTCode = "99211";
                }
                else
                {
                    int EMCode = 1;
                    int[] EMResultData = new int[] { Hx, Exam, MDM };
                    Array.Sort(EMResultData);
                    if (EMResultData.Distinct().Count() == 3)
                    {
                        EMCode = EMResultData.Min();
                    }
                    else
                    {
                        EMCode = EMResultData[1];
                    }
                    if (EMCode == 1)
                    {
                        CPTCode = "99212";
                    }
                    else if (EMCode == 2)
                    {
                        CPTCode = "99213";
                    }
                    else if (EMCode == 3)
                    {
                        CPTCode = "99214";
                    }
                    else if (EMCode == 4)
                    {
                        CPTCode = "99215";
                    }
                }


            }

            txtENMCode.Text = CPTCode;


            if (!CPTCode.Equals("") && HPICount > 0)
            {
                

                BindWEMR_spS_GetEMCPTInvoice();
            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                // BindEMRS_PT_DETAILS();
                //  BindWEMR_spS_GetEMCPTInvoice();
                BindWEMR_spS_GetEMRSHPI();

            }
        }

    }
}