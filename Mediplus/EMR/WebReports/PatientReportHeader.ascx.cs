﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;



namespace Mediplus.EMR.WebReports
{
    public partial class PatientReportHeader : System.Web.UI.UserControl
    {
        #region Methods

        public string PolicyType = "", PolicyExp = "", PolicyNo = "", VisitType = "", CardNo = "";

        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();

        public string EMR_ID { set; get; }

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                hidEMRDeptID.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]);

                //if (DS.Tables[0].Rows[0].IsNull("EPM_DEP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]) != "")
                //    lblDept.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]) != "")
                //    lblEmrDate.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                //    lblEmrStTime.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                //    lblEmrEndTime.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_NAME"]) != "")
                    lblPTFullName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                    lblFileNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                    lblNationality.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_NATIONALITY"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_ADDR") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_ADDR"]) != "")
                    lblAddress.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_ADDR"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_DOBDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_DOBDesc"]) != "")
                    lblDOB.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_DOBDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_AGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE"]) != "")
                    lblAge.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE"]));
                lblAgeType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE"]));
                lblAge1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE1"]));
                lblAgeType1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE1"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                    lblMobile.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]) != "")
                //    lblEPMID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]) != "")
                    lblSex.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                    lblInsCo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                    PolicyNo = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                    PolicyType = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_CONT_EXPDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATE"]) != "")
                    PolicyExp = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_ID_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_ID_NO"]) != "")
                    CardNo = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_ID_NO"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]) != "")
                //    lblDrName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]));
                //// lblDrName1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]));
                //lblDrCode.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                    lblEmiratesID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]));

                if (DS.Tables[0].Rows[0].IsNull("EMR_PT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_TYPE"]) != "")
                    VisitType = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_TYPE"]));




                //if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                //    lblTreatmentPlan.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                //else
                //    lblTreatmentPlan.Text = "";

                //if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                //    lblFollowupNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]));
                //else
                //    lblFollowupNotes.Text = "";



            }


        }

        public void BindVital()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();
            EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                lblWeight.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                lblBMI.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);

            }

        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                BindEMRPTMaster();
                // BindStaffDtls();
                BindVital();
            }
        }
    }
}