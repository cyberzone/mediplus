﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class CommonRemarks : System.Web.UI.UserControl
    {
        public string strCommonRemarks = "";

        public string EMR_ID = "";


        #region Methods

        void BindSegmentVisitDtls(out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            //  Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            //  Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        public void SegmentVisitDtlsGet(string Criteria)
        {

            DataSet DS = new DataSet();
            Criteria += "  AND EST_STATUS=1  AND EST_TYPE='VISIT_DETAILS_REMARKS' ";
            Criteria += "    AND ESVD_ID=" + EMR_ID;

            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.SegmentVisitSegTemplateGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Criteria = " 1=1 ";
                SegmentVisitDtlsGet(Criteria);
                string strValue = "";
                BindSegmentVisitDtls(out  strValue);
                strCommonRemarks = strValue;

            }
        }
    }
}