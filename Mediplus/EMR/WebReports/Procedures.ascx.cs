﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class Procedures : System.Web.UI.UserControl
    {
        public string EMR_ID = "";
        EMR_PTProcedure objPTProc = new EMR_PTProcedure();

       public void BindProcedure()
        {
            string Criteria = " 1=1 ";
           // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            if (Convert.ToString(Session["EMR_REQUIRED_AUDIT"]) == "1")
            {
                Criteria += " AND (  EPP_STATUS <>'D'  OR EPP_STATUS IS NULL )  ";
            }

            Criteria += " AND EPP_ID IN (" + EMR_ID + ")";

            DataSet DS = new DataSet();
            CommonBAL objWebrpt = new CommonBAL();
            DS = objPTProc.ProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProcedure.DataSource = DS;
                gvProcedure.DataBind();
            }
            else
            {
                gvProcedure.DataBind();
            }




        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (EMR_ID != "" && EMR_ID != null)
                {
                    BindProcedure();
                }
            }
        }
    }
}