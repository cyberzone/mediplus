﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class Prescription : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";
        public string CriticalNotes;
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PHY' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["EMR_PHY_SCREEN"] = "HAAD";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_PHY_SCREEN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_PHY_SCREEN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }




                }
            }



        }

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                DR_ID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));
                if (DS.Tables[0].Rows[0].IsNull("EPM_CRITICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]) != "")
                    CriticalNotes = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]));

                EMR_DATE = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]);

            }



        }

        void ShowPrescriptionRequest()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HPR_EMR_ID='" + EMR_ID + "'";
          

            ePrescriptionBAL objePresc = new ePrescriptionBAL();
            DataSet DS = new DataSet();
            DS = objePresc.PrescriptionRequestGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lbleAuthSRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPR_REFERENCE_NO"]);
               
                if (Convert.ToString(DS.Tables[0].Rows[0]["ApprovalStatus"]) != "" && DS.Tables[0].Rows[0].IsNull("ApprovalStatus") == false)
                {
                    lbleAuthStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["ApprovalStatus"]);
                }
                else
                {
                    lbleAuthStatus.Text = "Request Sent";
                }
                //lbleAuthComment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPR_FILE_COMMENTS"]);

            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                pnlPatientReportHeader.EMR_ID = EMR_ID;
                pnlDiagnosis.EMR_ID = EMR_ID;
                pnlPrescriptions.EMR_ID = EMR_ID;

                BindScreenCustomization();
                BindEMRPTMaster();
                pnlDoctorSignature.DR_ID = DR_ID;
                pnlDoctorSignature.EMR_DATE = EMR_DATE;

                if (Convert.ToString(ViewState["EMR_PHY_SCREEN"]).ToUpper() == "DHA")
                {
                    tbleAuth.Visible = true;
                    ShowPrescriptionRequest();
                }

            }
        }
    }
}