﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class PrescriptionsAudit : System.Web.UI.UserControl
    {
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
        public string EMR_ID { set; get; }

        void BindPharmacy()
        {


            string Criteria = " 1=1 AND AUDIT_TYPE='ADDENDUM' ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            Criteria += " AND EPP_ID  IN (" + EMR_ID + ")";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.AuditLogDataGet(Criteria, "PHARMACY");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();


                string[] arrEMR_ID = EMR_ID.Split(',');

                if (arrEMR_ID.Length > 1)
                {
                    gvPharmacy.Columns[0].Visible = true;
                }

            }
            else
            {
                gvPharmacy.DataBind();
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //  if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);

                BindPharmacy();
            }
        }
    }
}