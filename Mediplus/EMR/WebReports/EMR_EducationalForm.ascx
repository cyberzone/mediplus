﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMR_EducationalForm.ascx.cs" Inherits="Mediplus.EMR.WebReports.EMR_EducationalForm" %>

 <input type="hidden" id="EDU_PART1_SAVE_PT_ID" runat="server" value="FALSE" />
<table style="width:100%">
     <tr>
          <td class="ReportCaption"   >
              Educational Form
          </td>
       </tr>
    <tr>
        <td>
            <table style="width:100%">
                
                    <tr>
                        <td style=" border: 1px solid #dcdcdc;height:25px;"><label for="Language" class="ReportCaption" >Patient's language</label>
                            <span>: </span>
                            <label id="lbllanguage" class="ReportCaption" runat="server" ></label>
                        </td>
                        <td class="ReportCaption"  style=" border: 1px solid #dcdcdc;height:25px;"><label class="ReportCaption" >Patient's literacy</label>
                            <input type="checkbox" name="Read" id="chkRead" value="0"   disabled="disabled" class="ReportCaption" runat="server" > Read
                            <input type="checkbox" name="Write" id="chkWrite" value="0"   disabled="disabled" class="ReportCaption" runat="server" > Write
                            <input type="checkbox" name="Speak" id="chkSpeak" value="0"   disabled="disabled" class="ReportCaption" runat="server" > Speak
                        </td>
                        <td style=" border: 1px solid #dcdcdc;height:25px;"><label for="ReligionBelief" class="ReportCaption" >Religion/Belief</label>
                            <span>: </span>
                             <label id="lblReligion" class="ReportCaption" runat="server" ></label>
                        </td>
                    </tr>
                
            </table>
        </td>
    </tr>
   
      <tr>
           <td style="height:20px;">
                <asp:label id="lblEduFormDiet" cssclass="ReportCaption"  runat="server"></asp:label>
               
           </td>
              </tr>
</table>
