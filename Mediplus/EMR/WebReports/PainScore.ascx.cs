﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.WebReports
{
    public partial class PainScore : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindPainScore()
        {

            string Criteria = " 1=1  ";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPainScore = "";
                strPainScore = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE"]);

                


                if (strPainScore != "")
                {
                    if (strPainScore == "0")
                        PainValue0.Checked = true;
                    if (strPainScore == "1")
                        PainValue1.Checked = true;
                    if (strPainScore == "2")
                        PainValue2.Checked = true;
                    if (strPainScore == "3")
                        PainValue3.Checked = true;
                    if (strPainScore == "4")
                        PainValue4.Checked = true;
                    if (strPainScore == "5")
                        PainValue5.Checked = true;
                    if (strPainScore == "6")
                        PainValue6.Checked = true;
                    if (strPainScore == "7")
                        PainValue7.Checked = true;
                    if (strPainScore == "8")
                        PainValue8.Checked = true;
                    if (strPainScore == "9")
                        PainValue9.Checked = true;
                    if (strPainScore == "10")
                        PainValue10.Checked = true;

                }


            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
             if (!IsPostBack)
            {
                BindPainScore();
            }

          
        }
    }
}