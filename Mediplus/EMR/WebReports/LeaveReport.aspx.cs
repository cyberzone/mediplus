﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;


namespace Mediplus.EMR.WebReports
{
    public partial class LeaveReport : System.Web.UI.Page
    {
        public string FileNo = "";

        public string LeaveType = "", Diagnosis = "", TreatmentDesc = "", FromDate = "", ToDate = "";
        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindLeaveReport()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPLR_PT_ID  ='" + FileNo + "'";


            Criteria += " AND EPLR_ID  ='" + Convert.ToString(ViewState["EPLR_ID"]) + "'";





            DataSet DS = new DataSet();
            EMR_PTMedicalReport objMR = new EMR_PTMedicalReport();
            DS = objMR.PTLeaveReportGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPLR_LEAVE_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPLR_LEAVE_TYPE"]) != "")
                    LeaveType = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPLR_LEAVE_TYPE"]));

                if (DS.Tables[0].Rows[0].IsNull("EPLR_DIAGNOSIS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPLR_DIAGNOSIS"]) != "")
                    Diagnosis = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPLR_DIAGNOSIS"]));

                if (DS.Tables[0].Rows[0].IsNull("EPLR_TREATMENT") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPLR_TREATMENT"]) != "")
                    TreatmentDesc = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPLR_TREATMENT"]));

                if (DS.Tables[0].Rows[0].IsNull("EPLR_FROM_DATEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPLR_FROM_DATEDesc"]) != "")
                    FromDate = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPLR_FROM_DATEDesc"]));


                if (DS.Tables[0].Rows[0].IsNull("EPLR_TO_DATEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPLR_TO_DATEDesc"]) != "")
                    ToDate = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPLR_TO_DATEDesc"]));


            }



        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["EPLR_ID"] = Convert.ToString(Request.QueryString["EPLR_ID"]);
                    FileNo = Convert.ToString(Request.QueryString["FileNo"]);

                    pnlPatientHeader.FileNo = FileNo;
                    pnlDoctorSignature.DR_ID = Convert.ToString(Session["User_Code"]);
                    BindLeaveReport();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      LeaveReport.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}