﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace MediplusEMR.WebReports.SHReports
{
    public partial class OutPatientSummary : System.Web.UI.Page
    {
        string strValue = "";
        public string EMR_PT_ID = "",HPI="", HIST_PAST = "",   HIST_SURGICAL, ALLERGY = "";

        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";
        public string CriticalNotes;
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        CommonBAL objCom = new CommonBAL();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
        SegmentBAL objSeg = new SegmentBAL();
        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                DR_ID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));
                if (DS.Tables[0].Rows[0].IsNull("EPM_CRITICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]) != "")
                    CriticalNotes = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]));

                EMR_DATE = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]);

            }



        }
        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";

            DataSet DS = new DataSet();
            CommonBAL objWebrpt = new CommonBAL();
            DS = objPhar.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                strValue = "<table style='width: 100%;' cellpadding='0' cellspacing='0' ><tr >";

                int index = 0;
                strValue += "<tr>";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (index % 3 == 0)
                    {
                        strValue += "</tr>";
                        strValue += "<tr>";
                    }
                    strValue += "<td class='lblCaption1' style='width:33%;' >" + Convert.ToString(DR["EPP_PHY_NAME"]) + " </td>";

                    index = index + 1;

                }
                strValue += "</tr>";
            }


        }

        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + EMR_ID + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);
                EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);

                pnlPatientReportHeader.EMR_ID = EMR_ID;
                pnlDiagnosis.EMR_ID = EMR_ID;

              
                BindPharmacy();
                lblPrescription.Text = strValue;
                BindEMRPTMaster();

                pnlDoctorSignature.DR_ID = DR_ID;
                pnlDoctorSignature.EMR_DATE = EMR_DATE;


                string Criteria = " 1=1 ";
                SegmentVisitDtlsALLGet(Criteria);




                strValue = "";
                BindSegmentVisitDtls("HIST_PAST", EMR_PT_ID, out  strValue);
                HIST_PAST = strValue;

                strValue = "";
                BindSegmentVisitDtls("HIST_SURGICAL", EMR_PT_ID, out  strValue);
                HIST_SURGICAL = strValue;

                strValue = "";
                BindSegmentVisitDtls("ALLERGY", EMR_PT_ID, out  strValue);
                ALLERGY = strValue;

                strValue = "";
                BindSegmentVisitDtls("HPI", EMR_ID , out  strValue);
                HPI = strValue;

            }
        }
    }
}