﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports.SHReports
{
    public partial class PatientTransferForm : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";


        CommonBAL objCom = new CommonBAL();

        EMR_PTReferral objRef = new EMR_PTReferral();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindReferral()
        {
            objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " and EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID=" + Convert.ToString(Session["EMR_ID"]);
            DS = objRef.EMRPTReferenceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                lblRecevHosp.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_OUT_HOSPITAL"]);
                lblConfBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_OUT_DRNAME"]);
                lblRecevHosp1.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_OUT_HOSPITAL"]);
                lblDiagprocedure.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_PRO_DIAG"]);

                lblReasonForSending.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_REMARKS"]);//Reason for Referral
                lblCurrentStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_PATIENT_CURRENT_STATUS"]);//Patient Condition During Transfer
                lblSpecialEndor.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPR_SPECIAL_ENDORSEMENT"]); // Recommended Medications


            }

        }


        void BindEMRPTReferanceCond()
        {
            objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " and EPRC_ID=" + EMR_ID + " and EPRC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objRef.EMRPTReferanceCondGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "G_CONDITION")
                    {
                        txtComment1.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime1.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP1.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR1.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO21.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR1.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp1.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);
                    }


                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "RESP_SYS")
                    {
                        txtComment2.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime2.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP2.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR2.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO22.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR2.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp2.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);

                    }

                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "CVS")
                    {

                        txtComment3.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime3.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP3.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR3.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO23.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR3.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp3.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);
                    }




                    if (Convert.ToString(DR["EPRC_ORG_FUNCTION"]) == "CNS")
                    {
                        txtComment4.Text = Convert.ToString(DR["EPRC_COMMENT"]);
                        txtTime4.Text = Convert.ToString(DR["EPRC_TIMEDateTime"]);
                        txtBP4.Text = Convert.ToString(DR["EPRC_BP"]);
                        txtHR4.Text = Convert.ToString(DR["EPRC_HR"]);
                        txtSPO24.Text = Convert.ToString(DR["EPRC_SPO2"]);
                        txtRR4.Text = Convert.ToString(DR["EPRC_RR"]);
                        txtTemp4.Text = Convert.ToString(DR["EPRC_TEMPERATURE"]);

                    }
                }

            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    pnlDiagnosis.EMR_ID = EMR_ID;


                    BindReferral();
                    BindEMRPTReferanceCond();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      WebReport/PatientTransferForm.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}