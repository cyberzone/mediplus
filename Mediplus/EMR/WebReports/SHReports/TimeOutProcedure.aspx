﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="TimeOutProcedure.aspx.cs" Inherits="EMR.WebReports.TimeOutProcedure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        

        .box-title
        {
           
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
       <table style="width:100%;text-align:center;vertical-align:top;" >
        <tr>
            <td >
                     <img style="padding:1px;height:100px;border:none;" src="images/Report_Logo.PNG"  />
            </td>
        </tr>

    </table>
   <span class="box-title">Time out Procedure Checklist</span>
  
    <table style="width:100%;">
        <tr>
            <td class="lblCaption1" style="width:150px;">
               Name of Procedure :
            </td>
            <td>
                <asp:Label ID="NameOfProcedure" runat="server" CssClass="lblCaption1" Width ="200px"  ></asp:Label>

                

            </td>
        </tr>
    </table>
   

        <asp:GridView ID="gvOPDAssessment" runat="server" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="95%"   OnRowDataBound="gvOPDAssessment_RowDataBound">
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
            
            <Columns>
                <asp:TemplateField HeaderText="Time Out checklist" HeaderStyle-Width="70%">
                    <ItemTemplate>
                        <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("TOP_LEVEL") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("TOP_FIELD_ID") %>' Visible="false"></asp:Label>

                        <asp:Label ID="lblFieldName" CssClass="lblCaption1" runat="server" Text='<%# Bind("TOP_FIELD_NAME") %>' Width="100%"></asp:Label>

                        
                           

                    </ItemTemplate>

                </asp:TemplateField>
                 
                <asp:TemplateField HeaderText="Yes" HeaderStyle-Width="10%"  ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                      <asp:Label ID="lblYes" CssClass="lblCaption1" runat="server" Width="100%"></asp:Label>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="No" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblNo" CssClass="lblCaption1" runat="server" Width="100%"></asp:Label>
                    </ItemTemplate>

                </asp:TemplateField>
                  <asp:TemplateField HeaderText="N/A" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblNA" CssClass="lblCaption1" runat="server"  Width="100%"></asp:Label>
                    </ItemTemplate>

                </asp:TemplateField>

            </Columns>

        </asp:GridView>
    
     <div style="clear:both">&nbsp;</div>	
    <span class="lblCaption1" >   Signature (Who Called for time out):  </span>
</asp:Content>
