﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;


namespace MediplusEMR.WebReports
{
    public partial class FallPreventionAsses : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";
        Int32 sumFooterValue = 0;

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASSESSMENT_MASTER", "1=1 AND EPA_TYPE='PART1' ", "FPA_ORDER");
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvOPDAssessment.DataSource = ds;
                gvOPDAssessment.DataBind();

            }
            else
            {
                gvOPDAssessment.DataBind();
            }
        }

        void BindData1()
        {


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASSESSMENT_MASTER", "1=1 AND EPA_TYPE='PART2' ", "FPA_ORDER");
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPart2.DataSource = ds;
                gvPart2.DataBind();

            }
            else
            {
                gvPart2.DataBind();
            }
        }

        void BindOPDData(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_OPD"] != "" && ViewState["EMR_OPD"] != null)
            {

                DS = (DataSet)ViewState["EMR_OPD"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["FPAD_FIELD_VALUE"]);
                    strValue = Convert.ToString(DR["FPAD_DETAILS"]);
                }
            }


        }

        void BindOPDDataAll()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and FPAD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPAD_ID='" + EMR_ID + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASESSMENT_DTLS", Criteria, "FPAD_FIELD_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_OPD"] = ds;
            }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    BindOPDDataAll();
                    BindData();
                    BindData1();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       WebReports/FallPreventionAsses.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvOPDAssessment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                Label lblFieldName = (Label)e.Row.FindControl("lblFieldName");
                Label lblYes = (Label)e.Row.FindControl("lblYes");
                Label lblDtls = (Label)e.Row.FindControl("lblDtls");

                if (lblLevelType.Text == "1")
                {

                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and FPAD_FIELD_ID='" + lblFieldID.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);

                    if (lblLevelType.Text == "2")
                    {
                        lblFieldName.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + lblFieldName.Text;
                    }

                    if (strValueYes == "Y") lblYes.Text = "Yes"; else if (strValueYes == "N") lblYes.Text = "No"; else lblYes.Text = "";

                    lblDtls.Text = strValue;
                    if (strValue != "")
                    {
                        sumFooterValue += Convert.ToInt32(strValue);
                    }


                }


            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                lblTotal.Text = sumFooterValue.ToString();
            }

        }

        protected void gvPart2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType1 = (Label)e.Row.FindControl("lblLevelType1");
                Label lblFieldID1 = (Label)e.Row.FindControl("lblFieldID1");
                CheckBox chkPTAnswer = (CheckBox)e.Row.FindControl("chkPTAnswer");


                if (lblLevelType1.Text == "1")
                {
                    chkPTAnswer.Visible = false;

                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and FPAD_FIELD_ID='" + lblFieldID1.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);

                    if (strValueYes.ToUpper() == "Y")
                    {
                        chkPTAnswer.Checked = true;
                    }
                    else
                    {
                        chkPTAnswer.Checked = false;
                    }

                }

            }
        }

        #endregion
    }
}