﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace MediplusEMR.WebReports
{
    public partial class DentalCaseHistory : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";

        CommonBAL objCom = new CommonBAL();
        EMR_CaseHistory objCH = new EMR_CaseHistory();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        DataSet DS = new DataSet();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindOralHygieneStatus()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND EOH_ID=" + EMR_ID;

            DS = objCom.fnGetFieldValue(" * ", "EMR_ORALHYGIENESTATUS", Criteria, "EOH_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EOH_TYPE"]) == "DEBRIS_SCORE")
                    {
                        txtDS16.Text = Convert.ToString(DR["EOH_16"]);
                        txtDS11.Text = Convert.ToString(DR["EOH_11"]);
                        txtDSX.Text = Convert.ToString(DR["EOH_X"]);
                        txtDS26.Text = Convert.ToString(DR["EOH_26"]);

                        txtDS46.Text = Convert.ToString(DR["EOH_46"]);
                        txtDSX_1.Text = Convert.ToString(DR["EOH_X_1"]);

                        txtDS31.Text = Convert.ToString(DR["EOH_31"]);
                        txtDS36.Text = Convert.ToString(DR["EOH_36"]);


                    }


                    if (Convert.ToString(DR["EOH_TYPE"]) == "CALCULUS")
                    {
                        txtCA16.Text = Convert.ToString(DR["EOH_16"]);
                        txtCA11.Text = Convert.ToString(DR["EOH_11"]);
                        txtCAX.Text = Convert.ToString(DR["EOH_X"]);
                        txtCA26.Text = Convert.ToString(DR["EOH_26"]);

                        txtCA46.Text = Convert.ToString(DR["EOH_46"]);
                        txtCAX_1.Text = Convert.ToString(DR["EOH_X_1"]);

                        txtCA31.Text = Convert.ToString(DR["EOH_31"]);
                        txtCA36.Text = Convert.ToString(DR["EOH_36"]);
                    }


                    lblTotalScore.Text = Convert.ToString(DR["EOH_TOTAL_SCORE"]);
                    hidTotalScore.Value = Convert.ToString(DR["EOH_TOTAL_SCORE"]);

                }
            }
        }


        void BindGingivalStatus()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND EGS_ID=" + EMR_ID;

            DS = objCom.fnGetFieldValue(" * ", "EMR_GINGIVALSTATUS", Criteria, "EGS_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EGS_TYPE"]) == "CLINICAL_FEATURES")
                    {

                        txtColorCF.Text = Convert.ToString(DR["EGS_COLOR"]);
                        txtContourCF.Text = Convert.ToString(DR["EGS_CONTOUR"]);
                        txtSizeCF.Text = Convert.ToString(DR["EGS_SIZE"]);
                        txtConsistencyCF.Text = Convert.ToString(DR["EGS_CONSISTENCY"]);

                        txtPositionCF.Text = Convert.ToString(DR["EGS_POSITION"]);
                        txtBleedingProbingCF.Text = Convert.ToString(DR["EGS_BLEEDING_PROBING"]);


                    }


                    if (Convert.ToString(DR["EGS_TYPE"]) == "TOOTH_SEGMENT")
                    {
                        txtColorTS.Text = Convert.ToString(DR["EGS_COLOR"]);
                        txtContourTS.Text = Convert.ToString(DR["EGS_CONTOUR"]);
                        txtSizeTS.Text = Convert.ToString(DR["EGS_SIZE"]);
                        txtConsistencyTS.Text = Convert.ToString(DR["EGS_CONSISTENCY"]);

                        txtPositionTS.Text = Convert.ToString(DR["EGS_POSITION"]);
                        txtBleedingProbingTS.Text = Convert.ToString(DR["EGS_BLEEDING_PROBING"]);


                    }




                }
            }
        }

        void BindPeriodontalStatus()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND EPS_ID=" + EMR_ID;

            DS = objCom.fnGetFieldValue(" * ", "EMR_PERIODONTAL_STATUS", Criteria, "EPS_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EPS_TYPE"]) == "PROBING_DEPTH")
                    {

                        txtProbingDepth18.Text = Convert.ToString(DR["EPS_18"]);
                        txtProbingDepth17.Text = Convert.ToString(DR["EPS_17"]);
                        txtProbingDepth16.Text = Convert.ToString(DR["EPS_16"]);
                        txtProbingDepth15.Text = Convert.ToString(DR["EPS_15"]);
                        txtProbingDepth14.Text = Convert.ToString(DR["EPS_14"]);
                        txtProbingDepth13.Text = Convert.ToString(DR["EPS_13"]);
                        txtProbingDepth12.Text = Convert.ToString(DR["EPS_12"]);
                        txtProbingDepth11.Text = Convert.ToString(DR["EPS_11"]);


                        txtProbingDepth21.Text = Convert.ToString(DR["EPS_21"]);
                        txtProbingDepth22.Text = Convert.ToString(DR["EPS_22"]);
                        txtProbingDepth23.Text = Convert.ToString(DR["EPS_23"]);
                        txtProbingDepth24.Text = Convert.ToString(DR["EPS_24"]);
                        txtProbingDepth25.Text = Convert.ToString(DR["EPS_25"]);
                        txtProbingDepth26.Text = Convert.ToString(DR["EPS_26"]);
                        txtProbingDepth27.Text = Convert.ToString(DR["EPS_27"]);
                        txtProbingDepth28.Text = Convert.ToString(DR["EPS_28"]);


                        txtProbingDepth48.Text = Convert.ToString(DR["EPS_48"]);
                        txtProbingDepth47.Text = Convert.ToString(DR["EPS_47"]);
                        txtProbingDepth46.Text = Convert.ToString(DR["EPS_46"]);
                        txtProbingDepth45.Text = Convert.ToString(DR["EPS_45"]);
                        txtProbingDepth44.Text = Convert.ToString(DR["EPS_44"]);
                        txtProbingDepth43.Text = Convert.ToString(DR["EPS_43"]);
                        txtProbingDepth42.Text = Convert.ToString(DR["EPS_42"]);
                        txtProbingDepth41.Text = Convert.ToString(DR["EPS_41"]);

                        txtProbingDepth31.Text = Convert.ToString(DR["EPS_31"]);
                        txtProbingDepth32.Text = Convert.ToString(DR["EPS_32"]);
                        txtProbingDepth33.Text = Convert.ToString(DR["EPS_33"]);
                        txtProbingDepth34.Text = Convert.ToString(DR["EPS_34"]);
                        txtProbingDepth35.Text = Convert.ToString(DR["EPS_35"]);
                        txtProbingDepth36.Text = Convert.ToString(DR["EPS_36"]);
                        txtProbingDepth37.Text = Convert.ToString(DR["EPS_37"]);
                        txtProbingDepth38.Text = Convert.ToString(DR["EPS_38"]);

                    }


                    if (Convert.ToString(DR["EPS_TYPE"]) == "MOBILITY")
                    {
                        txtMobility18.Text = Convert.ToString(DR["EPS_18"]);
                        txtMobility17.Text = Convert.ToString(DR["EPS_17"]);
                        txtMobility16.Text = Convert.ToString(DR["EPS_16"]);
                        txtMobility15.Text = Convert.ToString(DR["EPS_15"]);
                        txtMobility14.Text = Convert.ToString(DR["EPS_14"]);
                        txtMobility13.Text = Convert.ToString(DR["EPS_13"]);
                        txtMobility12.Text = Convert.ToString(DR["EPS_12"]);
                        txtMobility11.Text = Convert.ToString(DR["EPS_11"]);


                        txtMobility21.Text = Convert.ToString(DR["EPS_21"]);
                        txtMobility22.Text = Convert.ToString(DR["EPS_22"]);
                        txtMobility23.Text = Convert.ToString(DR["EPS_23"]);
                        txtMobility24.Text = Convert.ToString(DR["EPS_24"]);
                        txtMobility25.Text = Convert.ToString(DR["EPS_25"]);
                        txtMobility26.Text = Convert.ToString(DR["EPS_26"]);
                        txtMobility27.Text = Convert.ToString(DR["EPS_27"]);
                        txtMobility28.Text = Convert.ToString(DR["EPS_28"]);


                        txtMobility48.Text = Convert.ToString(DR["EPS_48"]);
                        txtMobility47.Text = Convert.ToString(DR["EPS_47"]);
                        txtMobility46.Text = Convert.ToString(DR["EPS_46"]);
                        txtMobility45.Text = Convert.ToString(DR["EPS_45"]);
                        txtMobility44.Text = Convert.ToString(DR["EPS_44"]);
                        txtMobility43.Text = Convert.ToString(DR["EPS_43"]);
                        txtMobility42.Text = Convert.ToString(DR["EPS_42"]);
                        txtMobility41.Text = Convert.ToString(DR["EPS_41"]);

                        txtMobility31.Text = Convert.ToString(DR["EPS_31"]);
                        txtMobility32.Text = Convert.ToString(DR["EPS_32"]);
                        txtMobility33.Text = Convert.ToString(DR["EPS_33"]);
                        txtMobility34.Text = Convert.ToString(DR["EPS_34"]);
                        txtMobility35.Text = Convert.ToString(DR["EPS_35"]);
                        txtMobility36.Text = Convert.ToString(DR["EPS_36"]);
                        txtMobility37.Text = Convert.ToString(DR["EPS_37"]);
                        txtMobility38.Text = Convert.ToString(DR["EPS_38"]);


                    }


                    if (Convert.ToString(DR["EPS_TYPE"]) == "FURCATION")
                    {
                        txtFurcation18.Text = Convert.ToString(DR["EPS_18"]);
                        txtFurcation17.Text = Convert.ToString(DR["EPS_17"]);
                        txtFurcation16.Text = Convert.ToString(DR["EPS_16"]);
                        txtFurcation15.Text = Convert.ToString(DR["EPS_15"]);
                        txtFurcation14.Text = Convert.ToString(DR["EPS_14"]);
                        txtFurcation13.Text = Convert.ToString(DR["EPS_13"]);
                        txtFurcation12.Text = Convert.ToString(DR["EPS_12"]);
                        txtFurcation11.Text = Convert.ToString(DR["EPS_11"]);


                        txtFurcation21.Text = Convert.ToString(DR["EPS_21"]);
                        txtFurcation22.Text = Convert.ToString(DR["EPS_22"]);
                        txtFurcation23.Text = Convert.ToString(DR["EPS_23"]);
                        txtFurcation24.Text = Convert.ToString(DR["EPS_24"]);
                        txtFurcation25.Text = Convert.ToString(DR["EPS_25"]);
                        txtFurcation26.Text = Convert.ToString(DR["EPS_26"]);
                        txtFurcation27.Text = Convert.ToString(DR["EPS_27"]);
                        txtFurcation28.Text = Convert.ToString(DR["EPS_28"]);


                        txtFurcation48.Text = Convert.ToString(DR["EPS_48"]);
                        txtFurcation47.Text = Convert.ToString(DR["EPS_47"]);
                        txtFurcation46.Text = Convert.ToString(DR["EPS_46"]);
                        txtFurcation45.Text = Convert.ToString(DR["EPS_45"]);
                        txtFurcation44.Text = Convert.ToString(DR["EPS_44"]);
                        txtFurcation43.Text = Convert.ToString(DR["EPS_43"]);
                        txtFurcation42.Text = Convert.ToString(DR["EPS_42"]);
                        txtFurcation41.Text = Convert.ToString(DR["EPS_41"]);

                        txtFurcation31.Text = Convert.ToString(DR["EPS_31"]);
                        txtFurcation32.Text = Convert.ToString(DR["EPS_32"]);
                        txtFurcation33.Text = Convert.ToString(DR["EPS_33"]);
                        txtFurcation34.Text = Convert.ToString(DR["EPS_34"]);
                        txtFurcation35.Text = Convert.ToString(DR["EPS_35"]);
                        txtFurcation36.Text = Convert.ToString(DR["EPS_36"]);
                        txtFurcation37.Text = Convert.ToString(DR["EPS_37"]);
                        txtFurcation38.Text = Convert.ToString(DR["EPS_38"]);



                    }



                    if (Convert.ToString(DR["EPS_TYPE"]) == "RECESSION")
                    {
                        txtRecession18.Text = Convert.ToString(DR["EPS_18"]);
                        txtRecession17.Text = Convert.ToString(DR["EPS_17"]);
                        txtRecession16.Text = Convert.ToString(DR["EPS_16"]);
                        txtRecession15.Text = Convert.ToString(DR["EPS_15"]);
                        txtRecession14.Text = Convert.ToString(DR["EPS_14"]);
                        txtRecession13.Text = Convert.ToString(DR["EPS_13"]);
                        txtRecession12.Text = Convert.ToString(DR["EPS_12"]);
                        txtRecession11.Text = Convert.ToString(DR["EPS_11"]);


                        txtRecession21.Text = Convert.ToString(DR["EPS_21"]);
                        txtRecession22.Text = Convert.ToString(DR["EPS_22"]);
                        txtRecession23.Text = Convert.ToString(DR["EPS_23"]);
                        txtRecession24.Text = Convert.ToString(DR["EPS_24"]);
                        txtRecession25.Text = Convert.ToString(DR["EPS_25"]);
                        txtRecession26.Text = Convert.ToString(DR["EPS_26"]);
                        txtRecession27.Text = Convert.ToString(DR["EPS_27"]);
                        txtRecession28.Text = Convert.ToString(DR["EPS_28"]);


                        txtRecession48.Text = Convert.ToString(DR["EPS_48"]);
                        txtRecession47.Text = Convert.ToString(DR["EPS_47"]);
                        txtRecession46.Text = Convert.ToString(DR["EPS_46"]);
                        txtRecession45.Text = Convert.ToString(DR["EPS_45"]);
                        txtRecession44.Text = Convert.ToString(DR["EPS_44"]);
                        txtRecession43.Text = Convert.ToString(DR["EPS_43"]);
                        txtRecession42.Text = Convert.ToString(DR["EPS_42"]);
                        txtRecession41.Text = Convert.ToString(DR["EPS_41"]);

                        txtRecession31.Text = Convert.ToString(DR["EPS_31"]);
                        txtRecession32.Text = Convert.ToString(DR["EPS_32"]);
                        txtRecession33.Text = Convert.ToString(DR["EPS_33"]);
                        txtRecession34.Text = Convert.ToString(DR["EPS_34"]);
                        txtRecession35.Text = Convert.ToString(DR["EPS_35"]);
                        txtRecession36.Text = Convert.ToString(DR["EPS_36"]);
                        txtRecession37.Text = Convert.ToString(DR["EPS_37"]);
                        txtRecession38.Text = Convert.ToString(DR["EPS_38"]);


                    }


                    if (Convert.ToString(DR["EPS_TYPE"]) == "LOSSOFATT")
                    {
                        txtLossOfAtt18.Text = Convert.ToString(DR["EPS_18"]);
                        txtLossOfAtt17.Text = Convert.ToString(DR["EPS_17"]);
                        txtLossOfAtt16.Text = Convert.ToString(DR["EPS_16"]);
                        txtLossOfAtt15.Text = Convert.ToString(DR["EPS_15"]);
                        txtLossOfAtt14.Text = Convert.ToString(DR["EPS_14"]);
                        txtLossOfAtt13.Text = Convert.ToString(DR["EPS_13"]);
                        txtLossOfAtt12.Text = Convert.ToString(DR["EPS_12"]);
                        txtLossOfAtt11.Text = Convert.ToString(DR["EPS_11"]);


                        txtLossOfAtt21.Text = Convert.ToString(DR["EPS_21"]);
                        txtLossOfAtt22.Text = Convert.ToString(DR["EPS_22"]);
                        txtLossOfAtt23.Text = Convert.ToString(DR["EPS_23"]);
                        txtLossOfAtt24.Text = Convert.ToString(DR["EPS_24"]);
                        txtLossOfAtt25.Text = Convert.ToString(DR["EPS_25"]);
                        txtLossOfAtt26.Text = Convert.ToString(DR["EPS_26"]);
                        txtLossOfAtt27.Text = Convert.ToString(DR["EPS_27"]);
                        txtLossOfAtt28.Text = Convert.ToString(DR["EPS_28"]);


                        txtLossOfAtt48.Text = Convert.ToString(DR["EPS_48"]);
                        txtLossOfAtt47.Text = Convert.ToString(DR["EPS_47"]);
                        txtLossOfAtt46.Text = Convert.ToString(DR["EPS_46"]);
                        txtLossOfAtt45.Text = Convert.ToString(DR["EPS_45"]);
                        txtLossOfAtt44.Text = Convert.ToString(DR["EPS_44"]);
                        txtLossOfAtt43.Text = Convert.ToString(DR["EPS_43"]);
                        txtLossOfAtt42.Text = Convert.ToString(DR["EPS_42"]);
                        txtLossOfAtt41.Text = Convert.ToString(DR["EPS_41"]);

                        txtLossOfAtt31.Text = Convert.ToString(DR["EPS_31"]);
                        txtLossOfAtt32.Text = Convert.ToString(DR["EPS_32"]);
                        txtLossOfAtt33.Text = Convert.ToString(DR["EPS_33"]);
                        txtLossOfAtt34.Text = Convert.ToString(DR["EPS_34"]);
                        txtLossOfAtt35.Text = Convert.ToString(DR["EPS_35"]);
                        txtLossOfAtt36.Text = Convert.ToString(DR["EPS_36"]);
                        txtLossOfAtt37.Text = Convert.ToString(DR["EPS_37"]);
                        txtLossOfAtt38.Text = Convert.ToString(DR["EPS_38"]);


                    }


                }
            }
        }

        void BindDentalMaster()
        {
            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1 ";

            Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND DDMR_TRANS_ID='" + EMR_ID + "'";
            DS = objDental.EMRDentalMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                txtToothBrush.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_TOOTH_BRUSH"]);
                txtBrushingTechnique.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_BRUSHING_TECHNIQUE"]);

                txtRootCanalTreated.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_ROOTCANALTREATE"]);
                txtPeriapicalLesion.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_PERIAPICALLESION"]);

                txtRemainingRoots.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_REMAININGROOTS"]);
                txtDMFScore.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_DMFSCORE"]);


                radAngleClassifi.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_ANGLE_CLASSIFI"]);

                txtOverjet.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_OVERJET"]);
                txtOverbite.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_OVERBITE"]);

                drpParafunctionalHabits.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_PARAFUNCTIONALHABITS"]);
                txtToothBrush.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_TOOTH_BRUSH"]);



                string DDMR_DENTAL_FLOSS = "0", DDMR_TOOTHPICKS = "0", DDMR_INTERPROXIMAL_BRUSHES = "0";

                if (DS.Tables[0].Rows[0].IsNull("DDMR_DENTAL_FLOSS") == false)
                    chkDentalfloss.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["DDMR_DENTAL_FLOSS"]);
                if (DS.Tables[0].Rows[0].IsNull("DDMR_TOOTHPICKS") == false)
                    chkToothpicks.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["DDMR_TOOTHPICKS"]);
                if (DS.Tables[0].Rows[0].IsNull("DDMR_INTERPROXIMAL_BRUSHES") == false)
                    chkInterproximalBrushes.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["DDMR_INTERPROXIMAL_BRUSHES"]);


                txtRadiographicFindings.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_RADIOGRAPHIC_FINDINGS"]);
                txtPrognosis.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_PROGNOSIS"]);
                lblFollowNotes.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_FOLLOWUP"]);
               
            }
        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND (SCREENNAME='EMR_DENTAL' OR SCREENNAME='EMR_DEPT' ) ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DENTAL_TREATMENT_IMAGE_PATH"] = "0";
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DENTAL_TREATMENT_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DENTAL_TREATMENT_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                }
            }


        }

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 "; 
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                    lblTreatmentPlan.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                else
                    lblTreatmentPlan.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                    lblFollowupNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]));
                else
                    lblFollowupNotes.Text = "";
            }


        }

        void BindDepartmentImage(string SegmentType, string strImgID)
        {



            string DentalPath = "", strFileNo = "", strEPMDate = "", PBMI_FileName = "";



            strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); ;
            strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
           

            PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + EMR_ID + "@" + SegmentType + ".jpg";

            // 00028@23122014@11@DCF

            if (File.Exists(@DentalPath + SegmentType + @"\" + strFileNo + @"\" + PBMI_FileName))
            {
                if (strImgID == "img1")
                {
                    img1.ImageUrl = @"../Department/DisplayDeptImage.aspx?EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";
                    img1.Visible = true;
                }

            }



        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
                {
                    Session["ErrorMsg"] = "Session Expired Please login again";
                    Response.Redirect("../../../ErrorPage.aspx");
                }

                try
                {
                    EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    pnlDiagnosis.EMR_ID = EMR_ID;
                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {
                        BindOralHygieneStatus();
                        BindGingivalStatus();
                        BindPeriodontalStatus();
                        BindDentalMaster();
                        BindScreenCustomization();
                        BindDepartmentImage("PDC", "img1");
                        BindEMRPTMaster();
                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      WebReports/DentalCaseHistory.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}