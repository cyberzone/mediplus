﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="FallPreventionAsses.aspx.cs" Inherits="MediplusEMR.WebReports.FallPreventionAsses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        

        .box-title
        {
           
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
     <table style="width:100%;text-align:center;vertical-align:top;" >
        <tr>
            <td >
                     <img style="padding:1px;height:100px;border:none;" src="../images/Report_Logo.PNG"  />
            </td>
        </tr>

    </table>
   <span class="box-title">Fall Prevention Assessment tool</span>
    <br />   <br />   
     <span class="lblCaption1" >    Morse Fall Scale  </span>
     <br /><br />
   <asp:GridView ID="gvOPDAssessment" runat="server" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="95%"   OnRowDataBound="gvOPDAssessment_RowDataBound" ShowFooter ="true">
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
            <FooterStyle CssClass="lblCaption1"  Font-Bold="true"/>
            <Columns>
                <asp:TemplateField HeaderText="Item" HeaderStyle-Width="70%">
                    <ItemTemplate>
                        <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_LEVEL") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_ID") %>' Visible="false"></asp:Label>

                        <asp:Label ID="lblFieldName" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_NAME") %>' Width="100%"></asp:Label>

                        
                           

                    </ItemTemplate>
                     <FooterTemplate>
                          Please  implement following Intervention if  Score is = > 15
                     </FooterTemplate>
                       
                </asp:TemplateField>
                 
                <asp:TemplateField HeaderText="Scale " HeaderStyle-Width="10%"  ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                      <asp:Label ID="lblYes" CssClass="lblCaption1" runat="server" Width="100%"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        Total
                    </FooterTemplate>
                </asp:TemplateField>
              
                  <asp:TemplateField HeaderText="Scoring" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblDtls" CssClass="lblCaption1" runat="server" Width="100%"></asp:Label>
                    </ItemTemplate>
                        <FooterTemplate>
                        <asp:Label ID="lblTotal" CssClass="lblCaption1" runat="server" Width="100%"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                 
            </Columns>
    

        </asp:GridView>
        <span class="lblCaption1" > </span>
    <br />
     <span class="lblCaption1" >  Fall prevention interventions: check as appropriate for the patient :</span>
      <br />  <br />
      <asp:GridView ID="gvPart2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="99%" GridLines="None"  OnRowDataBound="gvPart2_RowDataBound" ShowHeader="false">
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
          
            <Columns>
                  <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                    <ItemTemplate>
                       
                        <asp:CheckBox ID="chkPTAnswer" runat="server"  Enabled="false" />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="85%">
                    <ItemTemplate>
                        <asp:Label ID="lblLevelType1" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_LEVEL") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFieldID1" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_ID") %>' Visible="false"></asp:Label>

                        <asp:Label ID="Label2" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_NAME") %>' Width="100%"></asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>
              
                
            </Columns>

        </asp:GridView>
       <br />
    <br />
       <span class="lblCaption1" >    Fall risk Scale and interventions initiated by: ____________________   Employee No.________
Date:  ________________  Time: _____________________  </span>
</asp:Content>
