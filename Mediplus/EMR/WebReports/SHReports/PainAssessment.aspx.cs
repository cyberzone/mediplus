﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace Mediplus.EMR.WebReports
{
    public partial class PainAssessment : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "", PT_ID = "";

        EMR_PTPainAssessment objPainAss = new EMR_PTPainAssessment();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindgvPainAssGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            DataSet DS = new DataSet();
            objPainAss = new EMR_PTPainAssessment();
            DS = objPainAss.PTPainAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPainAss.DataSource = DS;
                gvPainAss.DataBind();
            }
            else
            {
                gvPainAss.DataBind();
            }
        }

     

   

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
             
                pnlPatientReportHeader.EMR_ID = EMR_ID;
             
                if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                {
                  
                    BindgvPainAssGrid();
                }

            }
        }
    }
}