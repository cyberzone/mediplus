﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;
namespace MediplusEMR.WebReports
{
    public partial class EMR_EducationalFormShaHol : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        string strValue = "";



        public string EMR_ID = "", DR_ID = "", EMR_DATE = "", PT_ID = "";


        /*
         * 
         *  void SegmentLoad()
        {
            DataSet DSSegMast = new DataSet();
            string Criteria = " 1=1 and ESM_STATUS=1 AND ESM_TYPE IN ('EDU_PART1','EDU_PART2','EDU_PART3')";

            DSSegMast = objSeg.SegmentMasterGet(Criteria);
            if (DSSegMast.Tables[0].Rows.Count > 0)
            {
                strValue = "<table style='width: 100%;border-width:thin;border-color:lightgray;' cellpadding='0' cellspacing='0' border='1' ><tr>";

                strValue += "<td  style='text-align:center;font-weight:bold;'> Date </td> ";
                foreach (DataRow DRSegmast in DSSegMast.Tables[0].Rows)
                {
                    strValue += "<td class='lblCaption1 BoldStyle' style='text-align:center;font-weight:bold;'>" + Convert.ToString(DRSegmast["ESM_SUBTYPE_ALIAS"]) + " </td>";

                   
                }
                strValue += "</tr>";

                strValue += "<tr>";
                strValue += "<td valign='Top'></td> ";
                foreach (DataRow DRSegmast in DSSegMast.Tables[0].Rows)
                {
                   


                    DataSet DSSegDtls = new DataSet();
                    string Criteria1 = " 1=1  AND ESVD_VALUE_YES='Y'";
                    Criteria1 += " AND ESVD_TYPE='" + DRSegmast["ESM_TYPE"] + "' AND ESVD_SUBTYPE='" + DRSegmast["ESM_SUBTYPE"] + "'";
                    if (Convert.ToString(DRSegmast["ESM_TYPE"]).ToUpper() == "EDU_PART1" && Convert.ToString(DRSegmast["ESM_SUBTYPE"]).ToUpper() == "IEN" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
                    {
                        Criteria1 += " AND ESVD_ID = '" + PT_ID + "'";
                    }
                    else
                    {

                        Criteria1 += " AND ESVD_ID = '" + EMR_ID + "'";
                    }

                    DSSegDtls = objSeg.SegmentVisitDtlsAndSegmentTemplatesGet(Criteria1);
                    if (DSSegDtls.Tables[0].Rows.Count > 0)
                    {
                      
                        strValue += "<td valign='Top'> ";
                        foreach (DataRow DRSegDtls in DSSegDtls.Tables[0].Rows)
                        {
                            strValue += "<input type='checkbox' disabled='disabled' checked />" + Convert.ToString(DRSegDtls["EST_FIELDNAME_ALIAS"]) + " &nbsp;";
                            strValue += "</br>";
                        }
                        strValue += "</td>";
                    }

                   
                }
                strValue += "</tr>";
                strValue += "</table>";
               
            }
        }
         * */

        void SegmentLoad()
        {
            DataSet DSSegMast = new DataSet();
            string Criteria = " 1=1 and ESM_STATUS=1 AND ESM_TYPE IN ('EDU_PART1','EDU_PART2','EDU_PART3')";

            DSSegMast = objSeg.SegmentMasterGet(Criteria);
            if (DSSegMast.Tables[0].Rows.Count > 0)
            {
                strValue = "<table style='width: 100%;border-width:thin;border-color:lightgray;' cellpadding='0' cellspacing='0' border='1' ><tr >";

                strValue += "<td  style='text-align:center;font-weight:bold;'> Date </td> ";
                foreach (DataRow DRSegmast in DSSegMast.Tables[0].Rows)
                {
                    strValue += "<td class='lblCaption1 BoldStyle' style='text-align:center;font-weight:bold;'>" + Convert.ToString(DRSegmast["ESM_SUBTYPE_ALIAS"]) + " </td>";


                }
                strValue += "</tr>";

                objEmrPTMast = new EMR_PTMasterBAL();
                DataSet DSPTMaster = new DataSet();
                string PTMCriteria = " 1=1 ";
                PTMCriteria += " AND EPM_PT_ID = '" + PT_ID + "'";
                DSPTMaster = objEmrPTMast.GetEMR_PTMaster(PTMCriteria);
                if (DSPTMaster.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow DRPTMaster in DSPTMaster.Tables[0].Rows)
                    {
                        strValue += "<tr>";
                        strValue += "<td valign='Top'>" + Convert.ToString( DRPTMaster["EPM_DATEDesc"]) + "</td> ";
                        foreach (DataRow DRSegmast in DSSegMast.Tables[0].Rows)
                        {



                            DataSet DSSegDtls = new DataSet();
                            string Criteria1 = " 1=1  AND ESVD_VALUE_YES='Y'";
                            Criteria1 += " AND ESVD_TYPE='" + DRSegmast["ESM_TYPE"] + "' AND ESVD_SUBTYPE='" + DRSegmast["ESM_SUBTYPE"] + "'";
                            if (Convert.ToString(DRSegmast["ESM_TYPE"]).ToUpper() == "EDU_PART1" && Convert.ToString(DRSegmast["ESM_SUBTYPE"]).ToUpper() == "IEN" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
                            {
                                Criteria1 += " AND ESVD_ID = '" + PT_ID + "'";
                            }
                            else
                            {

                                Criteria1 += " AND ESVD_ID = '" + Convert.ToString( DRPTMaster["EPM_ID"]) + "'";
                            }

                            DSSegDtls = objSeg.SegmentVisitDtlsAndSegmentTemplatesGet(Criteria1);
                            if (DSSegDtls.Tables[0].Rows.Count > 0)
                            {

                                strValue += "<td valign='Top'> ";
                                foreach (DataRow DRSegDtls in DSSegDtls.Tables[0].Rows)
                                {
                                    strValue += "<input type='checkbox' disabled='disabled' checked />" + Convert.ToString(DRSegDtls["EST_FIELDNAME_ALIAS"]) + " &nbsp;";
                                    strValue += "</br>";
                                }
                                strValue += "</td>";
                            }
                            else
                            {
                                strValue += "<td valign='Top'> ";
                                strValue += "</td>";
                            }


                        }
                        strValue += "</tr>";
                    }

                }
                strValue += "</table>";

            }
        }

        void BindEMR_EDUCATIONALFORM()
        {
            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria1 = " 1=1 ";
            Criteria1 += " AND EFM_ID='" + EMR_ID + "'";
            DS = objCom.EMR_EDUCATIONALFORMGet(Criteria1);
            if (DS.Tables[0].Rows.Count > 0)
            {

                lbllanguage.InnerText = Convert.ToString(DS.Tables[0].Rows[0]["EFM_LANGUAGE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_READ"]) == "1")
                {
                    chkRead.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_WRITE"]) == "1")
                {
                    chkWrite.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_SPEAK"]) == "1")
                {
                    chkSpeak.Checked = true;
                }


                lblReligion.InnerText = Convert.ToString(DS.Tables[0].Rows[0]["EFM_RELEGION"]);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                strValue = "";
                pnlPatientReportHeader.EMR_ID = EMR_ID;
                BindEMR_EDUCATIONALFORM();
                SegmentLoad();
                lblEduFormDiet.Text = strValue;
            }
        }
    }
}