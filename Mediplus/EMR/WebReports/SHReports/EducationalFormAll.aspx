﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="EducationalFormAll.aspx.cs" Inherits="MediplusEMR.WebReports.EMR_EducationalFormShaHol" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <style type="text/css">
        

        .box-title
        {
           
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <table style="width:100%;text-align:center;vertical-align:top;" >
        <tr>
            <td >
                     <img style="padding:1px;height:100px;border:none;" src="../images/Report_Logo.PNG"  />
            </td>
        </tr>

    </table>
   <span class="box-title">Interdisciplinary Health Education Record Form</span>
    <UC1:PatientReportHeader id="pnlPatientReportHeader" runat="server"    ></UC1:PatientReportHeader>
    <input type="hidden" id="EDU_PART1_SAVE_PT_ID" runat="server" value="FALSE" />
<table style="width:100%">
    
    <tr>
        <td>
            <table style="width:100%">
                <tr>
                    <tr>
                        <td style=" border: 1px solid #dcdcdc;height:25px;"><label for="Language" class="lblCaption1" >Patient's language</label>
                            <span>: </span>
                            <label id="lbllanguage" class="lblCaption1" runat="server" ></label>
                        </td>
                        <td class="lblCaption1"  style=" border: 1px solid #dcdcdc;height:25px;"><label class="lblCaption1" >Patient's literacy</label>
                            <input type="checkbox" name="Read" id="chkRead" value="1" checked="" disabled="disabled" class="lblCaption1" runat="server" > Read
                            <input type="checkbox" name="Write" id="chkWrite" value="1" checked="" disabled="disabled" class="lblCaption1" runat="server" > Write
                            <input type="checkbox" name="Speak" id="chkSpeak" value="1" checked="" disabled="disabled" class="lblCaption1" runat="server" > Speak
                        </td>
                        <td style=" border: 1px solid #dcdcdc;height:25px;"><label for="ReligionBelief" class="lblCaption1" >Religion/Belief</label>
                            <span>: </span>
                             <label id="lblReligion" class="lblCaption1" runat="server" ></label>
                        </td>
                    </tr>
                </tr>
            </table>
        </td>
    </tr>
   
      <tr>
           <td style="height:20px;">
                <asp:label id="lblEduFormDiet" cssclass="lblCaption1"  runat="server"></asp:label>
               
           </td>
              </tr>
</table>
</asp:Content>
