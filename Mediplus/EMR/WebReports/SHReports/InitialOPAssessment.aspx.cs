﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace MediplusEMR.WebReports.SHReports
{
    public partial class InitialOPAssessment : System.Web.UI.Page
    {
        string strValue = "";
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "", PT_ID = "";
        public string strPainValue = "", ALLERGY = "", EMR_PT_ID = "", HPI = "", HIST_PAST = "", HIST_SURGICAL;

        public string Weight = "", Height = "", BMI = "", TemperatureF = "", TemperatureC = "", Pulse = "", Respiration = "", BpSystolic = "", BpDiastolic = "", Sp02 = "", LMPDate = "";
        public string CC = "", Prescription = "";


        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();

        DataSet DS;

        void BindData()
        {


            DataSet ds = new DataSet();


            string Criteria = " 1=1   and EIOA_PT_ID ='" + EMR_PT_ID + "' AND EIOA_ID='" + EMR_ID + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_INITIAL_OPASSESSMENT", Criteria, "EIOA_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in ds.Tables[0].Rows)
                {

                    radFunctional.SelectedValue = Convert.ToString(DR["EIOA_FUNCTIONAL"]);
                    radAssRequired.SelectedValue = Convert.ToString(DR["EIOA_ASSESSMENT_REQUIRED"]);
                    radAppetite.SelectedValue = Convert.ToString(DR["EIOA_APPETITE"]);
                    radSwallowing.SelectedValue = Convert.ToString(DR["EIOA_SWALLOWING"]);
                    radDentition.SelectedValue = Convert.ToString(DR["EIOA_DENTITION"]);

                    string strWaitLoss = Convert.ToString(DR["EIOA_WEIGHT_LOSS"]);
                    string[] arrWaitLoss = strWaitLoss.Split('|');

                    if (arrWaitLoss.Length > 0)
                    {


                        if (arrWaitLoss[0] == "No")
                            radWeightLossNo.Checked = true;
                        else if (arrWaitLoss[0] == "Yes")
                            radWeightLossYes.Checked = true;

                        txtKgLoss.Text = arrWaitLoss[1];
                        txtMonthsLoss.Text = arrWaitLoss[2];
                    }

                    string strWaitgain = Convert.ToString(DR["EIOA_WEIGHT_GAIN"]);
                    string[] arrWaitgain = strWaitgain.Split('|');

                    if (arrWaitgain.Length > 0)
                    {

                        if (arrWaitgain[0] == "No")
                            radWeightgainNo.Checked = true;
                        else if (arrWaitgain[0] == "Yes")
                            radWeightgainYes.Checked = true;

                        txtKgGain.Text = arrWaitgain[1];
                        txtMonthsGain.Text = arrWaitgain[2];
                    }

                    string strChronicIllness = Convert.ToString(DR["EIOA_CHRONIC_ILLNESS"]);
                    string[] arrChronicIllness = strChronicIllness.Split('|');


                    if (arrChronicIllness.Length > 0)
                    {
                        string strSelectedData = arrChronicIllness[0];
                        string[] arrSelectedData = strSelectedData.Split('~');

                        if (arrSelectedData.Length > 0)
                        {

                            for (Int32 j = 0; j < arrSelectedData.Length - 1; j++)
                            {
                                if (arrSelectedData[j] == "None")
                                {
                                    radChronicIllnessNone.Checked = true;
                                }
                                else if (arrSelectedData[j] == "DM")
                                {
                                    radChronicIllnessDM.Checked = true;
                                }
                                else if (arrSelectedData[j] == "HTN")
                                {
                                    radChronicIllnessHTN.Checked = true;
                                }
                                else if (arrSelectedData[j] == "CAD")
                                {
                                    radChronicIllnessCAD.Checked = true;
                                }
                                else if (arrSelectedData[j] == "Dyslipidemia")
                                {
                                    radChronicIllnessDyslipidemia.Checked = true;
                                }
                                else if (arrSelectedData[j] == "Others")
                                {
                                    radChronicIllnessOthers.Checked = true;
                                }


                            }

                        }

                        txtChronicIllness.Text = arrChronicIllness[1];
                    }











                    radGastrostomy.SelectedValue = Convert.ToString(DR["EIOA_GASTROSTOMY"]);
                    radNGTube.SelectedValue = Convert.ToString(DR["EIOA_NG_TUBE"]);

                    string strTypeofDiet = Convert.ToString(DR["EIOA_DIET_TYPE"]);
                    string[] arrTypeofDiet = strTypeofDiet.Split('|');

                    if (arrTypeofDiet.Length > 0)
                    {

                        if (arrTypeofDiet[0] == "Normal")
                            radTypeofDietNormal.Checked = true;
                        else if (arrTypeofDiet[0] == "Special")
                            radTypeofDietSpecial.Checked = true;

                        txtTypeofDiet.Text = arrTypeofDiet[1];

                    }



                    radMood.SelectedValue = Convert.ToString(DR["EIOA_MOOD"]);
                    radExpressed.SelectedValue = Convert.ToString(DR["EIOA_EXPRESSED"]);
                    radSupportSystem.SelectedValue = Convert.ToString(DR["EIOA_SUPPORT_SYSTEM"]);
                    txtCurrentMedications.Text = Convert.ToString(DR["EIOA_CURRENT_MEDICATIONS"]);

                    string strAddAssmentNeeded = Convert.ToString(DR["EIOA_ADDITIONAL_ASSESSMENT"]);
                    string[] arrAddAssmentNeeded = strAddAssmentNeeded.Split('|');

                    if (arrAddAssmentNeeded.Length > 0)
                    {
                        radtxtAddAssmentNeeded.SelectedValue = arrAddAssmentNeeded[0];
                        txtAddAssmentNeeded.Text = arrAddAssmentNeeded[1];

                    }


                    txtMeasurableGoals.Text = Convert.ToString(DR["EIOA_MEASURABLE_GOALS"]);
                    txtIntervention.Text = Convert.ToString(DR["EIOA_INTERVENTION"]);
                    txtEvaluation.Text = Convert.ToString(DR["EIOA_EVALUATION"]);
                    txtPhysicalTherapy.Text = Convert.ToString(DR["EIOA_PHYSICAL_THERAPY"]);


                }
            }




        }


        void BindWEMR_spS_GetPainScore()
        {
            DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);

            trPain1.Visible = false;
            trPain2.Visible = false;
            trPain3.Visible = false;
            trPain4.Visible = false;
            trPain5.Visible = false;
            trPain6.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                hidEMRDeptID.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("EPM_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]) != "")
                    EMR_DATE = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]));


                strPainValue = Convert.ToString(DS.Tables[0].Rows[0]["EPM_PAINSCORE"]);
                if (strPainValue == "0" || strPainValue == "1")
                    trPain1.Visible = true;
                if (strPainValue == "2" || strPainValue == "3")
                    trPain2.Visible = true;
                if (strPainValue == "4" || strPainValue == "5")
                    trPain3.Visible = true;
                if (strPainValue == "6" || strPainValue == "7")
                    trPain4.Visible = true;
                if (strPainValue == "8" || strPainValue == "9")
                    trPain5.Visible = true;
                if (strPainValue == "10")
                    trPain6.Visible = true;


            }

        }

        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + EMR_ID + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " AND ESM_TYPE='" + SegmentType + "'";//HIST_PAST'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        void BindSegmentVisitDtlsWithSubHeader(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "";
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                DS1 = SegmentTemplatesGet(Criteria1);

                IsDataAvailable = false;

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    strHeader = "<table style='width:100%;' >";
                    strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE"]) + "</td></tr>";

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {

                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue1 = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                        if (strValue1 != "")
                        {
                            IsDataAvailable = true;
                            strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strData += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + strValue1 + "</td></tr>";
                        }

                    }
                    if (IsDataAvailable == true)
                    {
                        strValue += strHeader;
                        strValue += strData;
                        strValue += "</table>";
                        strData = "";
                    }


                }

            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }

        public void BindVital()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Weight = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                Height = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                BMI = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);

                TemperatureF = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);
                TemperatureC = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE_C"]);
                Pulse = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PULSE"]);
                Respiration = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                BpSystolic = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                BpDiastolic = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);
                Sp02 = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SPO2"]);



                LMPDate = Convert.ToString(DS.Tables[0].Rows[0]["EPV_LMP_DATE"]);
                if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "0")
                {
                    chkPregNo.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "1")
                {
                    chkPregYes.Checked = true;
                }

            }

        }

        void BindPatientHistory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPH_ID = '" + EMR_ID + "'";
            DS = objCom.EMR_PTHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                CC = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]));
            }

        }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";

            DataSet DS = new DataSet();
            objPhar = new EMR_PTPharmacy();
            DS = objPhar.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                strValue = "<table style='width: 100%;' cellpadding='0' cellspacing='0' ><tr >";

                int index = 0;
                strValue += "<tr>";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (index % 3 == 0)
                    {
                        strValue += "</tr>";
                        strValue += "<tr>";
                    }
                    strValue += "<td class='lblCaption1' style='width:33%;' >" + Convert.ToString(DR["EPP_PHY_NAME"]) + " </td>";

                    index = index + 1;

                }
                strValue += "</tr>";
            }
            Prescription = strValue;

        }

        void BindPharmacyInternal()
        {

            DataSet ds = new DataSet();
            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();

            string Criteria = " 1=1 ";


            Criteria += " AND EPPI_ID='" + EMR_ID + "'";
            ds = objCom.PharmacyInternalGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = ds;
                gvPharmacy.DataBind();

            }
            else
            {
                gvPharmacy.DataBind();
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);

                pnlPatientReportHeader.EMR_ID = EMR_ID;
                pnlDiagnosis.EMR_ID = EMR_ID;
                Laboratories.EMR_ID = EMR_ID;
                Radiologies.EMR_ID = EMR_ID;

                pnlDoctorSignature.DR_ID = DR_ID;
                pnlDoctorSignature.EMR_DATE = EMR_DATE;

                BindData();
                string Criteria = " 1=1 ";
                SegmentVisitDtlsALLGet(Criteria);

                BindWEMR_spS_GetPainScore();
                BindVital();
                strValue = "";
                BindSegmentVisitDtls("HPI", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                HPI = strValue;

                strValue = "";
                BindSegmentVisitDtlsWithSubHeader("PE", Convert.ToString(ViewState["EMR_ID"]), out  strValue);

                //TextFileWriting("6");
                lblPE.Text = strValue;

                strValue = "";
                BindSegmentVisitDtls("ALLERGY", EMR_PT_ID, out  strValue);
                ALLERGY = strValue;


                strValue = "";
                BindSegmentVisitDtls("HIST_PAST", EMR_PT_ID, out  strValue);
                HIST_PAST = strValue;

                strValue = "";
                BindSegmentVisitDtls("HIST_SURGICAL", EMR_PT_ID, out  strValue);
                HIST_SURGICAL = strValue;

                BindPatientHistory();

                BindPharmacy();
                BindPharmacyInternal();



            }
        }


    }
}