﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="InitialOPAssessment.aspx.cs" Inherits="MediplusEMR.WebReports.SHReports.InitialOPAssessment" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx" TagPrefix="UC1" TagName="Diagnosis" %>
<%@ Register Src="~/EMR/WebReports/VitalSignReport.ascx" TagPrefix="UC1" TagName="VitalSignReport" %>
<%@ Register Src="~/EMR/WebReports/Radiologies.ascx" TagPrefix="UC1" TagName="Radiologies" %>
<%@ Register Src="~/EMR/WebReports/Laboratories.ascx" TagPrefix="UC1" TagName="Laboratories" %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY" />
      <input type="hidden" id="hidEMRDeptID" runat="server" />

<input type="hidden" id="hidOPDFieldName" runat="server" value="Tobacco use|Alcohol use|Appetite|Swallowing|Weight Loss|Weight gain|Chronic illness|Gastrostomy|Type of Diet" />
    <table style="width: 100%; text-align: center; vertical-align: top;">
        <tr>
            <td>
                <img style="padding: 1px; height: 100px; border: none;" src="../images/Report_Logo.PNG" />
            </td>
        </tr>

    </table>
    <span class="box-title">Integrated Adult initial out Patient Assessment</span>
    <table style="width: 100%">
        <tr>
            <td style="width:100%;text-align:center;"">
                <span style="width:100%;text-align:center;" class="lblCaption1" > (To be filled by the Nurses)</span>
            </td>
        </tr>
    </table>
    <UC1:PatientReportHeader ID="pnlPatientReportHeader" runat="server"></UC1:PatientReportHeader>
    <br />
    <table style="width: 100%">
        <tr>
            <td class="lblCaption1 BoldStyle">Pulse: <%=Pulse %>/min&nbsp;&nbsp;BP: <%=BpSystolic %>/<%=BpDiastolic %>mm of Hg&nbsp;&nbsp;      Temp:<%=TemperatureF %>F&nbsp;(<%=TemperatureC %>C Resp:<%=Respiration %>/min    SPO2:<%=Sp02 %>%

            </td>
        </tr>
        <tr>
            <td>
                <table class="table spacy" style="width: 100%;">
                    
                    <tr id="trPain1" runat="server" visible="false">
                        <td style="border: 1px solid #dcdcdc; height: 25px;">
                            <label class="lblCaption1">
                              Pain Score:  No Pain
                            </label>
                            &nbsp;&nbsp;<img src="../../Images/smilie-1.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
                    </tr>
                    <tr id="trPain2" runat="server" visible="false">
                        <td class="lblCaption1">&nbsp;Pain Score: <%=strPainValue %>
                            <label for="PainValue2" class="lblCaption1">Mild annoying pain</label>&nbsp;&nbsp;<img src="../../Images/smilie-2.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
                    </tr>
                    <tr id="trPain3" runat="server" visible="false">
                        <td class="lblCaption1">&nbsp;Pain Score: <%=strPainValue %>
                            <label for="PainValue4" class="lblCaption1">Nagging,uncomfortable troublesome pain</label>&nbsp;&nbsp;<img src="../../Images/smilie-3.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
                    </tr>
                    <tr id="trPain4" runat="server" visible="false">
                        <td>&nbsp;Pain Score: <%=strPainValue %>
                            <label for="PainValue6" class="lblCaption1">Distressing, miserable Pain</label>&nbsp;&nbsp;<img src="../../Images/smilie-4.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
                    </tr>
                    <tr id="trPain5" runat="server" visible="false">
                        <td>&nbsp;Pain Score: <%=strPainValue %>
                            <label for="PainValue8" class="lblCaption1">Intense, dreadful horrible pain</label>&nbsp;&nbsp;<img src="../../Images/smilie-5.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
                    </tr>
                    <tr id="trPain6" runat="server" visible="false">
                        <td>&nbsp;Pain Score: <%=strPainValue %>
                            <label for="PainValue10" class="lblCaption1">Worst possible, unbearable, excruciating Pain</label>&nbsp;&nbsp;<img src="../../Images/smilie-6.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Allergy:<%=ALLERGY %>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Pregnant:
                <asp:CheckBox ID="chkPregNo" runat="server" Text="No" Enabled="false" />
                <asp:CheckBox ID="chkPregYes" runat="server" Text="Yes" Enabled="false" />
                LMP: <%=LMPDate %>
            </td>
        </tr>
       

                    <tr>
                        <td class="lblCaption1" style="width: 300px">Functional		 
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="radFunctional" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="250px" Enabled="false">
                                        <asp:ListItem Text="Independent" Value="Independent"></asp:ListItem>
                                        <asp:ListItem Text="Assisted" Value="Assisted"></asp:ListItem>
                                        <asp:ListItem Text="Dependent" Value="Dependent"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Fall Risk Assessment required as per the policy		 
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="radAssRequired" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="250px" Enabled="false">
                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                    </asp:RadioButtonList>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>

        <tr>
            <td class="lblCaption1 BoldStyle"><u>Nutritional screening </u> (Please √  appropriate)
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Ht&nbsp; <%=Height %> cm.	Wt&nbsp; <%=Weight %>Kg.	BMI: <%=BMI %>
            </td>
        </tr>
    
      
         
           
            <tr>
                <td class="lblCaption1" style="width: 100px">Appetite: 		
                </td>
                <td class="lblCaption1">
                   
                            <asp:RadioButtonList ID="radAppetite" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="255px"  Enabled="false">
                                <asp:ListItem Text="Good" Value="Good"></asp:ListItem>
                                <asp:ListItem Text="Fair" Value="Fair"></asp:ListItem>
                                <asp:ListItem Text="Poor" Value="Poor"></asp:ListItem>
                            </asp:RadioButtonList>
                   

                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Swallowing: 		
                </td>
                <td class="lblCaption1">
                    
                            <asp:RadioButtonList ID="radSwallowing" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="205px"  Enabled="false">
                                <asp:ListItem Text="Normal" Value="Normal"></asp:ListItem>
                                <asp:ListItem Text="Abnormal" Value="Abnormal"></asp:ListItem>
                            </asp:RadioButtonList>
                         
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Dentition: 		
                </td>
                <td   class="lblCaption1">
                   
                            <asp:RadioButtonList ID="radDentition" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="255px"  Enabled="false">
                                <asp:ListItem Text="Good" Value="Good"></asp:ListItem>
                                <asp:ListItem Text="Fair" Value="Fair"></asp:ListItem>
                                <asp:ListItem Text="Poor" Value="Poor"></asp:ListItem>
                            </asp:RadioButtonList>
                   

                </td>


            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Weight Loss 		
                </td>
                <td class="lblCaption1">
                       <input type="radio" id="radWeightLossNo" name="radWeightLoss" runat="server" class="lblCaption1" value="No" disabled="disabled"/>No 
                       <input type="radio" id="radWeightLossYes" name="radWeightLoss"  runat="server" class="lblCaption1" value="Yes" disabled="disabled" />Yes
                       &nbsp;
                            (If Yes, &nbsp;<asp:TextBox ID="txtKgLoss" runat="server" CssClass="label" Width="30px"  ReadOnly="true"></asp:TextBox>
                            Kgs in
                 &nbsp;<asp:TextBox ID="txtMonthsLoss" runat="server" CssClass="label" Width="30px" ReadOnly="true"></asp:TextBox>
                            Months)
                      

                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Weight gain
                </td>
                <td class="lblCaption1">
                      <input type="radio" id="radWeightgainNo" name="radWeightgain"  runat="server" class="lblCaption1" value="No"  disabled="disabled"/>No
                       <input type="radio" id="radWeightgainYes" name="radWeightgain"  runat="server" class="lblCaption1" value="Yes"  disabled="disabled"/>Yes
                            &nbsp;   (If Yes, &nbsp;<asp:TextBox ID="txtKgGain" runat="server" CssClass="label" Width="30px"  ReadOnly="true"></asp:TextBox>
                            Kgs in
                 &nbsp;<asp:TextBox ID="txtMonthsGain" runat="server" CssClass="label" Width="30px"  ReadOnly="true"></asp:TextBox>
                            Months) 

                      
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Chronic illness
                </td>
                <td class="lblCaption1"  >
                       <input type="radio" id="radChronicIllnessNone" name="radChronicIllnessNone"  runat="server" class="lblCaption1" value="None" disabled="disabled" />None
                       <input type="radio" id="radChronicIllnessDM" name="radChronicIllnessDM"  runat="server" class="lblCaption1" value="DM" disabled="disabled" />DM
                       <input type="radio" id="radChronicIllnessHTN" name="radChronicIllnessHTN"  runat="server" class="lblCaption1" value="HTN" disabled="disabled" />HTN
                       <input type="radio" id="radChronicIllnessCAD" name="radChronicIllnessCAD"  runat="server" class="lblCaption1" value="CAD" disabled="disabled"/>CAD
                       <input type="radio" id="radChronicIllnessDyslipidemia" name="radChronicIllnessDyslipidemia"  runat="server" class="lblCaption1" value="Dyslipidemia" disabled="disabled" />Dyslipidemia
                       <input type="radio" id="radChronicIllnessOthers" name="radChronicIllnessOthers"  runat="server" class="lblCaption1" value="Others" disabled="disabled" />Others
                        &nbsp;:&nbsp;<asp:Label ID="txtChronicIllness" runat="server" CssClass="label" ></asp:Label>
                            
                
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Gastrostomy
                </td>
                <td class="lblCaption1">
               
                            <asp:RadioButtonList ID="radGastrostomy" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="190px"  Enabled="false">
                                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                       
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">NG Tube
                </td>
                <td class="lblCaption1">
                  
                            <asp:RadioButtonList ID="radNGTube" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="190px"  Enabled="false">
                                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                       
                </td>


            </tr>

            <tr>
                <td class="lblCaption1" style="width: 100px">Type of Diet
                </td>
                <td class="lblCaption1">
                     <input type="radio" id="radTypeofDietNormal" name="radChronicIllness"  runat="server" class="lblCaption1" value="Normal" disabled="disabled"  />Normal
                       <input type="radio" id="radTypeofDietSpecial" name="radChronicIllness"  runat="server" class="lblCaption1" value="Special" disabled="disabled" />Special
                        &nbsp;
                          
                         <asp:Label ID="txtTypeofDiet" runat="server" CssClass="label" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" colspan="2"><u>Psychosocial Assessment</u> (Please √ appropriate)
                </td>
            </tr>

            <tr>
                <td class="lblCaption1" style="width: 100px">Mood
                </td>
                <td class="lblCaption1"  >
                    
                            <asp:RadioButtonList ID="radMood" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="672px"  Enabled="false">
                                <asp:ListItem Text="Calm" Value="Calm"></asp:ListItem>
                                <asp:ListItem Text="Sad" Value="Sad"></asp:ListItem>
                                <asp:ListItem Text="Anxious" Value="Anxious"></asp:ListItem>
                                <asp:ListItem Text="Tearful" Value="Tearful"></asp:ListItem>
                                <asp:ListItem Text="Combative" Value="Combative"></asp:ListItem>
                                <asp:ListItem Text="Agitated" Value="Agitated"></asp:ListItem>
                            </asp:RadioButtonList>
                        
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Expressed 
                </td>
                <td class="lblCaption1"  >
                    
                            <asp:RadioButtonList ID="radExpressed" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="580px"  Enabled="false">
                                <asp:ListItem Text="None" Value="None"></asp:ListItem>
                                <asp:ListItem Text="Shame" Value="Shame"></asp:ListItem>
                                <asp:ListItem Text="Guilt" Value="Guilt"></asp:ListItem>
                                <asp:ListItem Text="Negative Feelings about self" Value="Negative Feelings about self"></asp:ListItem>

                            </asp:RadioButtonList>
                        
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="width: 100px">Support System
                </td>
                <td class="lblCaption1" >
                    
                            <asp:RadioButtonList ID="radSupportSystem" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="670px" Enabled="false">
                                <asp:ListItem Text="Parents" Value="Parents"></asp:ListItem>
                                <asp:ListItem Text="Spouse" Value="Spouse"></asp:ListItem>
                                <asp:ListItem Text="Children" Value="Children"></asp:ListItem>
                                <asp:ListItem Text="Relatives" Value="Relatives"></asp:ListItem>
                                <asp:ListItem Text="Friends" Value="Friends"></asp:ListItem>
                                <asp:ListItem Text="Stay Alone" Value="Stay Alone"></asp:ListItem>
                                <asp:ListItem Text="None" Value="None"></asp:ListItem>
                            </asp:RadioButtonList>
                        

                </td>
            </tr>
       
     </table>
     
    
     <table style="width: 100%">
        <tr>
            <td style="width:100%;text-align:center;"">
                <span style="width:100%;text-align:center;" class="lblCaption1" > (To be filled by the Doctors)</span>
            </td>
        </tr>
    </table>
     <table style="width: 100%">
        <tr>
            <td class="lblCaption1 BoldStyle">Chief Complaints 
            </td>
        </tr>
        <tr>
            <td>
                <span class="lblCaption1"><%=CC %></span>
            </td>
        </tr>

        <tr>
            <td class="lblCaption1 BoldStyle">History of present illness
            </td>
        </tr>
        <tr>
            <td>
                <span class="lblCaption1"><%=HPI %></span>
            </td>
        </tr>
          <tr>
            <td class="lblCaption1 BoldStyle">Past Medical and significant History:
            </td>
        </tr>
        <tr>
            <td >
                <span class="lblCaption1"><%=HIST_PAST %></span>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Previous Surgeries
            </td>
        </tr>
        <tr>
            <td >
                <span class="lblCaption1"><%=HIST_SURGICAL %></span>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle">Allergies 
            </td>
        </tr>
        <tr>
            <td >
                <span class="lblCaption1"><%=ALLERGY %></span>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle">Current Medication:
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtCurrentMedications" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1 BoldStyle">Examination: 
            </td>
        </tr>
        <tr>
            <td>
             <asp:Label ID="lblPE"  runat="server" CssClass="lblCaption1" ></asp:Label>

            </td>
        </tr>
     </table>


       <div style="clear: both">&nbsp;</div>

    <UC1:Diagnosis ID="pnlDiagnosis" runat="server"></UC1:Diagnosis>
    <table style="width:100%">
        <tr>
            <td class="lblCaption1 BoldStyle">Additional assessment is needed for geriatric  patient ( more than 65 Years , disabled patient, depended patient).
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="radtxtAddAssmentNeeded" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="100px" Enabled="false">
                    <asp:ListItem Text="No" Value="No"></asp:ListItem>
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                </asp:RadioButtonList>
                </td>
        </tr>
        <tr>
            <td>
                   <asp:TextBox ID="txtAddAssmentNeeded" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td class="lblCaption1 BoldStyle"> Plan of Care :( The goals to be achieved including short term and long term goals) 
            </td>
        </tr>
        <tr>
            <td>
               
        <table style="width:100%">
            <tr>
                <td class="lblCaption1">Measurable goals 
                </td>
                <td class="lblCaption1">Intervention 
                </td>
                <td class="lblCaption1">Evaluation 
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMeasurableGoals" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtIntervention" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"  ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEvaluation" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"  ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
            </td>
        </tr>
    </table>

       
             
             
    <table>
        <tr>
            <td class="lblCaption1 BoldStyle">Investigations (details orders available in the systems)
            </td>
        </tr>
    </table>
      <UC1:Laboratories ID="Laboratories" runat="server"></UC1:Laboratories>
    <UC1:Radiologies ID="Radiologies" runat="server"></UC1:Radiologies>
        <table style="width:100%">
        <tr>
            <td class="lblCaption1 BoldStyle">Physical Therapy or others
            </td>
        </tr>
    <tr>
        <td>
           
                <asp:TextBox ID="txtPhysicalTherapy" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
            
        </td>
    </tr>
    </table>

     <span class="lblCaption1">Medication to be given in the centers when applicable</span>
    <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"   Width="100%"
        EnableModelValidation="True">
        <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
        <RowStyle CssClass="GridRow" />


        <Columns>

            <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                <ItemTemplate>

                    <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_CODE") %>'></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_NAME") %>'></asp:Label>



                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DOSAGE") %>'></asp:Label>



                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_ROUTE") %>'></asp:Label>



                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>

                    <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPEDesc") %>'></asp:Label>
                    <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPE") %>' Visible="false"></asp:Label>


                </ItemTemplate>
            </asp:TemplateField>



        </Columns>
    </asp:GridView>
    <br />
    <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"></UC1:DoctorSignature>
</asp:Content>
