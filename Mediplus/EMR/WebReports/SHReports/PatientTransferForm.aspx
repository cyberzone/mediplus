﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="PatientTransferForm.aspx.cs" Inherits="Mediplus.EMR.WebReports.SHReports.PatientTransferForm" %>

<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx" TagPrefix="UC1" TagName="Diagnosis" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <table style="width: 100%; text-align: center; vertical-align: top;">
        <tr>
            <td>
                <img style="padding: 1px; height: 100px; border: none;" src="../images/Report_Logo.PNG" />
            </td>
        </tr>

    </table>
    <span class="box-title">Patient Transfer Form</span>
    <table style="width: 100%">
        <tr>
            <td class="lblCaption1">Receiving Hospital:  &nbsp;
                <asp:Label ID="lblRecevHosp" runat="server" CssClass="label"></asp:Label>
                &nbsp;Confirmed By:
                <asp:Label ID="lblConfBy" runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    <div style="clear: both">&nbsp;</div>

    <UC1:Diagnosis ID="pnlDiagnosis" runat="server"></UC1:Diagnosis>

     <table style="width: 100%">
          <tr>
            <td class="lblCaption1">The Patient is sent to: 
                <br />
                 <asp:Label ID="lblRecevHosp1" runat="server" CssClass="label"></asp:Label>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">
                Diagnostic procedure
                <br />
                 <asp:Label ID="lblDiagprocedure" runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
         <tr>
            <td class="lblCaption1">Treatment and Medication
                <br />
                 <asp:Label ID="Label2" runat="server" CssClass="label"></asp:Label>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Reason for sending patient outside   SIHHC  
                <br />
                <asp:Label ID="lblReasonForSending" runat="server" CssClass="label"></asp:Label>

            </td>
        </tr>
         </table>
        <div id="divPatientCondition" runat="server" >
        <table>
            <tr>
                <td>
                    <label   class="lblCaption1" style="font-weight:bold;">Patient Condition at time of Transfer </label>
                </td>
            </tr>
        </table>
        <table style="width: 100%; border-width: thin; border-color: lightgray;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td class="lblCaption1" style="width: 100px">Org. Function
                </td>
                <td class="lblCaption1" style="width: 400px">Comment
                </td>
                <td class="lblCaption1" style="width: 100px">Time 
                </td>
                <td class="lblCaption1" style="width: 50px">BP
                </td>
                <td class="lblCaption1" style="width: 50px">HR
                </td>
                <td class="lblCaption1" style="width: 50px">SPO2 
                </td>
                <td class="lblCaption1" style="width: 50px">R.R
                </td>
                <td class="lblCaption1" style="width: 50px">Temp.
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">G. Condition
                </td>
                <td>
                    <asp:TextBox ID="txtComment1" runat="server" Style="resize: none;" CssClass="label" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%" ReadOnly="true"   ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime1" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP1" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR1" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO21" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR1" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp1" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td class="lblCaption1">Resp. Sys
                </td>
                <td>
                    <asp:TextBox ID="txtComment2" runat="server" Style="resize: none;" CssClass="label" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime2" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP2" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR2" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO22" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR2" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp2" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">C.V.S
                </td>
                <td>
                    <asp:TextBox ID="txtComment3" runat="server" Style="resize: none;" CssClass="label" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime3" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP3" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR3" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO23" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR3" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp3" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">CNS
                </td>
                <td>
                    <asp:TextBox ID="txtComment4" runat="server" Style="resize: none;" CssClass="label" BorderStyle="None" TextMode="MultiLine" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTime4" runat="server" CssClass="label" BorderStyle="None" Height="30px" Width="99%" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBP4" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtHR4" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSPO24" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRR4" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTemp4" runat="server" CssClass="label" BorderStyle="None" Height="30px"  Width="99%" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>



        </table>
            </div>
 
    <table style="width: 100%">
        <tr>
            <td class="lblCaption1">Current status of patient:
                <br />
                <asp:Label ID="lblCurrentStatus" runat="server" CssClass="label"></asp:Label>

            </td>
        </tr>
<tr>
            <td class="lblCaption1">Special endorsement: 
                <br />
                <asp:Label ID="lblSpecialEndor" runat="server" CssClass="label"></asp:Label>

            </td>
        </tr>

    </table>

</asp:Content>
