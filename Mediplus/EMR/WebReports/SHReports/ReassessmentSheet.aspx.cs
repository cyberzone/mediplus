﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace Mediplus.EMR.WebReports.SHReports
{
    public partial class ReassessmentSheet : System.Web.UI.Page
    {
        string strValue = "";
        public string CC = "";
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";
        public string CriticalNotes;
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        CommonBAL objCom = new CommonBAL();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();


        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                DR_ID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));
                if (DS.Tables[0].Rows[0].IsNull("EPM_CRITICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]) != "")
                    CriticalNotes = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]));

                EMR_DATE = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]);

            }



        }

       
        void BindPatientHistory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPH_ID = '" + EMR_ID + "'";
            DS = objCom.EMR_PTHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                CC = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]));
            }

        }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";

            DataSet DS = new DataSet();
            CommonBAL objWebrpt = new CommonBAL();
            DS = objPhar.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                strValue = "<table style='width: 100%;' cellpadding='0' cellspacing='0' ><tr >";

                int index=0;
                strValue += "<tr>";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (index % 3 == 0)
                    {
                        strValue += "</tr>";
                        strValue += "<tr>";
                    }
                    strValue += "<td class='lblCaption1' >" + Convert.ToString(DR["EPP_PHY_NAME"]) + " </td>";

                    index = index +1;
                   
                }
                 strValue += "</tr>";
            }
             

        }


        void BindPharmacyInternal()
        {

            DataSet ds = new DataSet();
            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();

            string Criteria = " 1=1 ";
           

            Criteria += " AND EPPI_ID='" + EMR_ID + "'";
            ds = objCom.PharmacyInternalGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = ds;
                gvPharmacy.DataBind();

            }
            else
            {
                gvPharmacy.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                pnlPatientReportHeader.EMR_ID = EMR_ID;
                pnlDiagnosis.EMR_ID = EMR_ID;
                

                BindEMRPTMaster();
               

                BindPatientHistory();
                BindPharmacy();
                lblPrescription.Text = strValue;
                BindPharmacyInternal();

            }
        }
    }
}