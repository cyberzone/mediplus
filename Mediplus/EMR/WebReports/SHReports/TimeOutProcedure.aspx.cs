﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace EMR.WebReports
{
    public partial class TimeOutProcedure : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";

        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND TOP_TYPE='PART1' ";

            DS = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_MASTER", Criteria, "TOP_ORDER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string strValue = "", strValueYes = "";
                    Criteria = " 1=1 ";
                    Criteria += " and TOPD_FIELD_ID='" + Convert.ToString(DR["TOP_FIELD_ID"]) + "'";
                    BindOPDData(Criteria, out strValue, out strValueYes);
                    if (Convert.ToString(DR["TOP_FIELD_NAME"]).ToUpper() == "NAMEOFPROCEDURE")
                    {
                        NameOfProcedure.Text = strValue;
                    }
                }



            }



            DS = new DataSet();

            Criteria = " 1=1 ";
            Criteria += " AND TOP_TYPE='PART2' ";


            DS = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_MASTER", Criteria, "TOP_ORDER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvOPDAssessment.DataSource = DS;
                gvOPDAssessment.DataBind();

            }
            else
            {
                gvOPDAssessment.DataBind();
            }
            
        }

        void BindOPDData(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_OPD"] != "" && ViewState["EMR_OPD"] != null)
            {

                DS = (DataSet)ViewState["EMR_OPD"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["TOPD_FIELD_VALUE"]);
                    strValue = Convert.ToString(DR["TOPD_DETAILS"]);
                }
            }


        }
        void BindOPDDataAll()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   AND TOPD_ID='" + EMR_ID + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_DTLS", Criteria, "TOPD_FIELD_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_OPD"] = ds;
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    BindOPDDataAll();
                    BindData();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       WebReports/TimeOutProcedure.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvOPDAssessment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                Label lblFieldName = (Label)e.Row.FindControl("lblFieldName");
                Label lblYes = (Label)e.Row.FindControl("lblYes");
                Label lblNo = (Label)e.Row.FindControl("lblNo");
                Label lblNA = (Label)e.Row.FindControl("lblNA");

                if (lblLevelType.Text == "1")
                {
                     
                    
                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and TOPD_FIELD_ID='" + lblFieldID.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);

                    if (lblLevelType.Text == "2")
                    {
                        lblFieldName.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + lblFieldName.Text;
                    }

                    if (strValueYes == "Y") lblYes.Text = "Yes"; else lblYes.Text = "";
                    if (strValueYes == "N") lblNo.Text = "No"; else lblNo.Text = "";
                    if (strValueYes == "D") lblNA.Text = "N/A"; else lblNA.Text = "";

                }

            }
        }

    }
}