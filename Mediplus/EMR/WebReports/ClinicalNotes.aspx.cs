﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class ClinicalNotes : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "";
        public string strClinicalNotes = "";
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

        void BindClinicalNotes()
        {
            string Criteria = " 1=1  AND ECN_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ECN_EMR_ID = '" + EMR_ID + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet DS = new DataSet();
            EMR_ClinicalNotes obj = new EMR_ClinicalNotes();
            DS = obj.EMRClinicalNotesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
             
                lblClinicalNotes1.Text = "";

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["ECN_ID"]) == "1")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes1.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnCopyNotes1.Visible = true;
                           
                        }

                    }

                    if (Convert.ToString(DR["ECN_ID"]) == "2")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes2.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnCopyNotes2.Visible = true;
                        }

                    }


                    if (Convert.ToString(DR["ECN_ID"]) == "3")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes3.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnCopyNotes3.Visible = true;
                        }

                    }
                    if (Convert.ToString(DR["ECN_ID"]) == "4")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes4.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnCopyNotes4.Visible = true;
                        }

                    }

                    if (Convert.ToString(DR["ECN_ID"]) == "5")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes5.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnCopyNotes5.Visible = true;
                        }

                    }


                }


            }
            else
            {
              
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                pnlPatientReportHeader.EMR_ID = EMR_ID;


                BindClinicalNotes();
                pnlDoctorSignature.DR_ID = DR_ID;
                pnlDoctorSignature.EMR_DATE = EMR_DATE;

            }
        }

        protected void btnCopyNotes1_Click(object sender, EventArgs e)
        {
            Session["CopyClinicalNotes"] = lblClinicalNotes1.Text;
        }

        protected void btnCopyNotes2_Click(object sender, EventArgs e)
        {
            Session["CopyClinicalNotes"] = lblClinicalNotes2.Text;
        }

        protected void btnCopyNotes3_Click(object sender, EventArgs e)
        {
            Session["CopyClinicalNotes"] = lblClinicalNotes3.Text;
        }

        protected void btnCopyNotes4_Click(object sender, EventArgs e)
        {
            Session["CopyClinicalNotes"] = lblClinicalNotes4.Text;
        }

        protected void btnCopyNotes5_Click(object sender, EventArgs e)
        {
            Session["CopyClinicalNotes"] = lblClinicalNotes5.Text;
        }
    }
}