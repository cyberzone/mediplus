﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class Radiology : System.Web.UI.Page
    {
        public string EMR_ID = "", EMR_PT_ID = "", DR_ID, EMR_DATE = "";
        public string ChiefComplaint, Allergy;
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        CommonBAL objCom = new CommonBAL();

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                EMR_DATE = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]);

            }



        }

        void BindPatientHistory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPH_ID = '" + EMR_ID + "'";
            DS = objCom.EMR_PTHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ChiefComplaint = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]));
            }

        }

        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            SegmentBAL objSeg = new SegmentBAL();
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            DS = objSeg.SegmentVisitDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strValue = "<table style='width:100%;' >";
                for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                    strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                }


                strValue += "</table>";

            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);
                BindEMRPTMaster();

                pnlPatientReportHeader.EMR_ID = EMR_ID;
                pnlDiagnosis.EMR_ID = EMR_ID;
                Radiologies.EMR_ID = EMR_ID;

                BindPatientHistory();
                string strValue = "";
                BindSegmentVisitDtls("ALLERGY", EMR_PT_ID, out  strValue);
                Allergy = strValue;
                pnlDoctorSignature.DR_ID = DR_ID;
                pnlDoctorSignature.EMR_DATE = EMR_DATE;

            }
        }
    }
}