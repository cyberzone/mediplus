﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;


namespace Mediplus.EMR.WebReports
{
    public partial class MedicalReport : System.Web.UI.Page
    {
        public string EMR_PT_ID = "", CC = "", HPI = "", HIST_PAST = "", HIST_FAMILY = "", HIST_SOCIAL = "", HIST_SURGICAL, ROS = "", ALLERGY = "", ProcedureNotes = "", NursingOrder = "";
        public string DR_ID = "";
        public string InvstRemarks="", Recommendation = "";
        CommonBAL objCom = new CommonBAL();
        SegmentBAL objSeg = new SegmentBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindPatientHistory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPH_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            DS = objCom.EMR_PTHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                CC = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]));
            }

        }

        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " AND ESM_TYPE='" + SegmentType + "'";//HIST_PAST'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        void BindSegmentVisitDtlsWithSubHeader(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "";
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                DS1 = SegmentTemplatesGet(Criteria1);

                IsDataAvailable = false;

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    strHeader = "<table style='width:100%;' >";
                    strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE"]) + "</td></tr>";

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {

                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue1 = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                        if (strValue1 != "")
                        {
                            IsDataAvailable = true;
                            strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strData += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + strValue1 + "</td></tr>";
                        }

                    }
                    if (IsDataAvailable == true)
                    {
                        strValue += strHeader;
                        strValue += strData;
                        strValue += "</table>";
                        strData = "";
                    }


                }

            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }


        void BindMedicalReportBind()
        {
            string Criteria = " 1=1 ";
            Criteria += " and EPMR_EMR_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            

            DataSet DS = new DataSet();
            EMR_PTMedicalReport objMR = new EMR_PTMedicalReport();
            DS = objMR.PTMedicalReportGet(Criteria);
 
            if (DS.Tables[0].Rows.Count > 0)
            {

                InvstRemarks = Convert.ToString(DS.Tables[0].Rows[0]["EPMR_INVEST_REMARKS"]);
                Recommendation = Convert.ToString(DS.Tables[0].Rows[0]["EPMR_RECOMMEND"]);

            }
             


        }
        void BindEMR()
        { 
            
            DataSet DS=new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " and EPM_ID='" + Convert.ToString(ViewState["EMR_ID"]) +"'";

            EMR_PTMasterBAL objPTMast = new EMR_PTMasterBAL();

            DS = objPTMast.GetEMR_PTMaster(Criteria);
 


            if (DS.Tables[0].Rows.Count > 0)
            {
                DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]);

            }

            


        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                    EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                    

                    pnlPatientReportHeader.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                   

                    BindEMR();
                    string Criteria = " 1=1 ";
                    SegmentVisitDtlsALLGet(Criteria);

                    BindPatientHistory();
                    string strValue = "";
                    BindSegmentVisitDtls("HPI", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    HPI = strValue;
                    //TextFileWriting("4");
                    strValue = "";
                    BindSegmentVisitDtls("ROS", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    ROS = strValue;
                    //TextFileWriting("5");
                    strValue = "";
                    BindSegmentVisitDtlsWithSubHeader("PE", Convert.ToString(ViewState["EMR_ID"]), out  strValue);
                    lblPE.Text = strValue;

                    BindMedicalReportBind();
                    pnlDoctorSignature.DR_ID = DR_ID;

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}