﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.WebReports
{
    public partial class ClinicalSummaryAll : System.Web.UI.Page
    {
        public string EMR_ID="",DR_ID = "",EMR_PT_ID="";
        
        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                   oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND (SCREENNAME='EMR_DENTAL' OR SCREENNAME='EMR_DEPT' ) ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DENTAL_TREATMENT_IMAGE_PATH"] = "0";
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DENTAL_TREATMENT_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DENTAL_TREATMENT_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                }
            }


        }


         void BindEMRIds()
        {
             string strEMRIds="";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";// AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_PT_ID = '" + EMR_PT_ID + "' AND EPM_DR_CODE='" + DR_ID +"'" ;
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach(DataRow DR in DS.Tables[0].Rows )
                {
                    
                   strEMRIds += Convert.ToString(DR["EPM_ID"])+"|";
                    
                   

                }

                strEMRIds = strEMRIds.Substring(0, strEMRIds.Length - 1);

            }
            ViewState["EMRIds"] = strEMRIds;

         }

         void BindEMRAllData()
         {
             string strEMRIds= Convert.ToString( ViewState["EMRIds"]);
             string[] arrEMRIds = strEMRIds.Split('|');
             for (Int32 i = 0; i < arrEMRIds.Length; i++)
             { 
                  
                 // <iframe id="ImageEditorPDC" runat="server" src="OpenDepartmentalImage.aspx?ImageType=<%=SegmentType %>" style="width:100%;height:600px"></iframe>      

                   string strRptPath = "../WebReports/ClinicalSummary.aspx";
                   string rptcall = @strRptPath + "?EMR_ID=" + arrEMRIds[i] + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);


                 HtmlIframe ifrm = new HtmlIframe();
                 ifrm.Src = rptcall;
                 ifrm.Style.Add("width", "100%");
                 ifrm.Style.Add("HEIGHT", "1500px");
                 PlaceHolder1.Controls.Add(ifrm);


             }


         }
        #endregion

         protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    this.Page.Title = "ClinicalSummaryAll";

                     EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                    DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);

                    BindEMRIds();
                    BindEMRAllData();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}