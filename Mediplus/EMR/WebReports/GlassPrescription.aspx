﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="GlassPrescription.aspx.cs" Inherits="Mediplus.EMR.WebReports.GlassPrescription" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx"  TagPrefix ="UC1" TagName="Diagnosis"%>
<%@ Register Src="~/EMR/WebReports/StandardPrescriptions.ascx" TagPrefix="UC1" TagName="StandardPrescriptions"  %>
<%@ Register Src="~/EMR/WebReports/Opticals.ascx" TagPrefix="UC1" TagName="Opticals"  %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
         #site-logo
         {
             display: none;
         }

         .box-header
         {
             display: none;
         }

         table
         {
             border-collapse: collapse;
         }

         /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

         .BoldStyle
         {
             font-weight: bold;
         }
     </style>
   <style type="text/css">
        

        .box-title
        {
           
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" runat="server">
   <table style="width:100%;text-align:center;vertical-align:top;" >
        <tr>
            <td >
                     <img style="padding:1px;height:100px;border:none;" src="images/Report_Logo.PNG"  />
            </td>
        </tr>

    </table>
   
    <span class="box-title">Opticals</span>
    <UC1:PatientReportHeader id="pnlPatientReportHeader" runat="server"    ></UC1:PatientReportHeader>
    <%if(  CriticalNotes != null) %>
    <%{%> 
    <div style="clear:both">&nbsp;</div>	
            <label class="lblCaption1">Allergies if any: <%= CriticalNotes != null?CriticalNotes:"" %></label> 
     <%}%> 
    <div style="clear:both">&nbsp;</div>	
           
    <div style="clear:both">&nbsp;</div>	
       <UC1:Opticals ID="pnlOpticals" runat="server"   ></UC1:Opticals>
    <div style="clear:both">&nbsp;</div>	
       <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"   ></UC1:DoctorSignature>
</asp:Content>
