﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class DoctorSignature : System.Web.UI.UserControl
    {
        //  public string EMR_ID { set; get; }
        public string DR_ID { set; get; }
        public string EMR_DATE { set; get; }


        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

        //void BindEMRPTMaster()
        //{
        //    DataSet DS = new DataSet();
        //    string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
        //    Criteria += " AND EPM_ID = '" + EMR_ID + "'";
        //    DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
        //    if (DS.Tables[0].Rows.Count > 0)
        //    {



        //        DR_ID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));


        //    }


        //}


        void BindStaffDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_STAFF_ID = '" + DR_ID + "'";
            DS = objCom.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblDrName1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["FullName"]));

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]) != "")
                    lblDrLicenseNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]));

            }
        }
        void BindStaffSignature()
        {
            imgStaffSig.ImageUrl = "../DisplaySignatureStaff.aspx?DR_ID=" + DR_ID;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if ( GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                {
                    imgStaffSig.Visible = false;

                }
                if (EMR_DATE != "" && EMR_DATE != null )
                {
                    lblDate.Text = EMR_DATE;
                }
                else
                {
                    lblDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                }
                DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);
                // BindEMRPTMaster();
                BindStaffDtls();
                BindStaffSignature();
            }
        }
    }
}