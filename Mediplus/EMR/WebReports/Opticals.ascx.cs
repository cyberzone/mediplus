﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.WebReports
{
    public partial class Opticals : System.Web.UI.UserControl
    {
        public string rdsph, rdcyl, rdaxis, ldsph, ldcyl, ldaxis, rnsph, rncyl, rnaxis, lnsph, lncyl, lnaxis, remarks;

        public string EMR_ID { set; get; }
        public string Branch_ID { set; get; }

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string FieldName = "EOP_R_D_SPH,EOP_R_D_CYL,EOP_R_D_AX,EOP_L_D_SPH,EOP_L_D_CYL,EOP_L_D_AX,EOP_R_N_SPH,EOP_R_N_CYL,EOP_R_N_AX,EOP_L_N_SPH,EOP_L_N_CYL,EOP_L_N_AX,EOP_REMARKS";
             string Criteria =" EOP_ID='" + EMR_ID +"'";
             DS = objCom.fnGetFieldValue(FieldName, "EMR_OPTHALMOLOGY_POWERDETAILS", Criteria, "EOP_ID");
           // DS = objOphth.WEMR_spS_GetRefraction(Branch_ID, EMR_ID);
            if (DS.Tables[0].Rows.Count > 0)
            {

                rdsph = Convert.ToString(DS.Tables[0].Rows[0]["EOP_R_D_SPH"]);
                rdcyl = Convert.ToString(DS.Tables[0].Rows[0]["EOP_R_D_CYL"]);
                rdaxis = Convert.ToString(DS.Tables[0].Rows[0]["EOP_R_D_AX"]);
                ldsph = Convert.ToString(DS.Tables[0].Rows[0]["EOP_L_D_SPH"]);
                ldcyl = Convert.ToString(DS.Tables[0].Rows[0]["EOP_L_D_CYL"]);
                ldaxis = Convert.ToString(DS.Tables[0].Rows[0]["EOP_L_D_AX"]);
                rnsph = Convert.ToString(DS.Tables[0].Rows[0]["EOP_R_N_SPH"]);
                rncyl = Convert.ToString(DS.Tables[0].Rows[0]["EOP_R_N_CYL"]);
                rnaxis = Convert.ToString(DS.Tables[0].Rows[0]["EOP_R_N_AX"]);
                lnsph = Convert.ToString(DS.Tables[0].Rows[0]["EOP_L_N_SPH"]);
                lncyl = Convert.ToString(DS.Tables[0].Rows[0]["EOP_L_N_CYL"]);
                lnaxis = Convert.ToString(DS.Tables[0].Rows[0]["EOP_L_N_AX"]);
                remarks = Convert.ToString(DS.Tables[0].Rows[0]["EOP_REMARKS"]);
            }
        }

      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "       Opticals.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }
    }
}