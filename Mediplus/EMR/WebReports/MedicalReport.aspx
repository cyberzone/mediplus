﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="MedicalReport.aspx.cs" Inherits="Mediplus.EMR.WebReports.MedicalReport" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
         #site-logo
         {
             display: none;
         }

         .box-header
         {
             display: none;
         }

         table
         {
             border-collapse: collapse;
         }

         /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

         .BoldStyle
         {
             font-weight: bold;
         }
     </style>
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>


</asp:Content>
 
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" runat="server">
     <img style="padding:1px;" src="images/Report_Logo.PNG" />
        <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY" />
      <table style="border:none;" >
        
        <tr>
            <td class="PageHeader">Laboratory Request 
               
            </td>
        </tr>
    </table>
    <UC1:PatientReportHeader id="pnlPatientReportHeader" runat="server"    ></UC1:PatientReportHeader>
    <br />
     <table width="100%">
              <tr>
                  <td class="lblCaption1 BoldStyle">
                     Chief Complaints 
                  </td>
              </tr>
              <tr>
                  <td style="height:20px;">
                    
                      
                      <span class="lblCaption1"><%=CC %></span>
                  </td>
              </tr>
                <tr>
                       <td class="lblCaption1 BoldStyle">
                        History of Present Illness
                      </td>
                  </tr>
                 <tr>
                      <td style="height:20px;">
                        <span class="lblCaption1"><%=HPI %></span>
                      </td>
                  </tr>
             
             
              <tr>
                   <td class="lblCaption1 BoldStyle">
                    Review of System
                  </td>
              </tr>
             <tr>
                  <td style="height:20px;">
                    <span class="lblCaption1"><%=ROS %></span>
                  </td>
              </tr>
               

             <tr>
                   <td class="lblCaption1 BoldStyle">
                   Physical Exam
                  </td>
              </tr>
             <tr>
                  <td style="height:20px;">
                     <asp:label id="lblPE" cssclass="lblCaption1"  runat="server"></asp:label>
                  </td>
              </tr>

           <tr>
                   <td class="lblCaption1 BoldStyle">
                   Investigation Remarks
                  </td>
              </tr>
             <tr>
                  <td style="height:20px;">
                    <span class="lblCaption1"><%=InvstRemarks %></span>
                  </td>
              </tr>
              <tr>
                   <td class="lblCaption1 BoldStyle">
                   Recommendation
                  </td>
              </tr>
             <tr>
                  <td style="height:20px;">
                    <span class="lblCaption1"><%=Recommendation %></span>
                  </td>
              </tr>
          </table>
          <br />


     <br />
    <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"   ></UC1:DoctorSignature>
</asp:Content>
