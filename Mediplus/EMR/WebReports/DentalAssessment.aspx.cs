﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class DentalAssessment : System.Web.UI.Page
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }


        public string EPM_PT_ID { set; get; }
        public string EPM_PT_NAME { set; get; }

        public string EPM_DATE { set; get; }

        public string EPH_CC { set; get; }

        public string HPM_REF_DR_NAME { set; get; }
        public string HPM_INS_COMP_NAME { set; get; }
        public string HPM_MARITAL { set; get; }
        public string HPM_OCCUPATION { set; get; }
        public string HPM_PREV_VISIT { set; get; }
        public string HPM_SEX { set; get; }


        public string EPV_WEIGHT { set; get; }
        public string EPV_HEIGHT { set; get; }

        public string EPV_BP_SYSTOLIC { set; get; }
        public string EPV_BP_DIASTOLIC { set; get; }
        public string EPV_IS_PREGNANT { set; get; }



        public string HPI { set; get; }
        public string HIST_SURGICAL { set; get; }
        public string HIST_PAST { set; get; }
        public string ALLERGY_PENICILLIN { set; get; }
        public string ALLERGY { set; get; }
        public string HIST_SOCIAL { set; get; }

        public string DENTAL_HISTORY { set; get; }
        public string PSYCHOLOGICAL_STATUS { set; get; }

        public string Penicillin { set; get; }
        public string Medications { set; get; }


        public string AllergyToPenicillin { set; get; }
        public string Diabetes { set; get; }
        public string Teuberculosis { set; get; }
        public string HighBloodPressure { set; get; }
        public string HepatitisJaundice { set; get; }
        public string HeartAttackChestPainAngina { set; get; }
        public string SteroidTherapy { set; get; }
        public string Heartmurmur { set; get; }
        public string EpilespsyFaintingAttacks { set; get; }
        public string Stroke { set; get; }
        public string RheumaticFever { set; get; }
        public string BleedingDiseaseDisorder { set; get; }
        public string Asthma { set; get; }
        public string OtherIllnesses { set; get; }

        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }



        void BindPatientHistory()
        {
            string CC = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPH_ID = '" + EMR_ID + "'";
            DS = objCom.EMR_PTHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("EPH_CC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]) != "")
                {
                    EPH_CC = Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]);

                }
            }

        }

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";// AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + EMR_ID + "'";
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPM_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                    EPM_PT_ID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_NAME"]) != "")
                    EPM_PT_NAME = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PT_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("EPM_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]) != "")
                    EPM_DATE = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATEDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_REF_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]) != "")
                    HPM_REF_DR_NAME = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]));


                if (DS.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                    HPM_INS_COMP_NAME = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]));


                if (DS.Tables[0].Rows[0].IsNull("HPM_MARITAL") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_MARITAL"]) != "")
                    HPM_MARITAL = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_MARITAL"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_OCCUPATION") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_OCCUPATION"]) != "")
                    HPM_OCCUPATION = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_OCCUPATION"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_PREV_VISIT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_PREV_VISIT"]) != "")
                {
                    DateTime dtPrevVisit;
                    dtPrevVisit = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPM_PREV_VISIT"]);
                    HPM_PREV_VISIT = dtPrevVisit.ToString("dd/MM/yyyy");
                }
                if (DS.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]) != "")
                    HPM_SEX = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]));

            }


        }

        public void BindVital()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                EPV_WEIGHT = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                EPV_HEIGHT = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);

                EPV_BP_SYSTOLIC = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                EPV_BP_DIASTOLIC = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);


                if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "1")
                {
                    radPregnantYes.Checked = true;
                }
                else
                {
                    radPregnantNo.Checked = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EPV_IS_LACTATING") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_LACTATING"]) != "")
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_LACTATING"]).ToUpper() == "true".ToUpper())
                    {
                        radLactatingYes.Checked = true;
                    }
                    else
                    {
                        radLactatingNo.Checked = true;
                    }

                }


            }



        }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";

            DataSet DS = new DataSet();
            CommonBAL objWebrpt = new CommonBAL();
            DS = objPhar.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    Medications += Convert.ToString(DR["EPP_PHY_NAME"]) + ", ";
                }

            }



        }


        string SegmentTemplatesGet(string strType, string strSubType, string strFieldNameAlias)
        {

            string strFieldID = "";
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";
            Criteria += " and EST_TYPE='" + strType + "' AND EST_SUBTYPE='" + strSubType + "' AND EST_FIELDNAME_ALIAS='" + strFieldNameAlias + "'";


            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.SegmentTemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strFieldID = Convert.ToString(DS.Tables[0].Rows[0]["EST_FIELDNAME"]);
            }

            return strFieldID;

        }


        void BindSegmentVisitDtls(string strType, string strSubType, string strFieldNameAlias, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';

            if (strSubType != "")
            {
                Criteria += " AND ESVD_SUBTYPE = '" + strSubType + "'";
            }



            if (strFieldNameAlias != "")
            {
                // string strFieldID = "";
                // strFieldID = SegmentTemplatesGet(strType, strSubType, strFieldNameAlias);
                // Criteria += " AND ESVD_FIELDNAME = '" + strFieldID + "'";

                Criteria += " AND EST_FIELDNAME_ALIAS = '" + strFieldNameAlias + "'";
            }


            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 0px solid #dcdcdc;width:40%;font-weight:bold;'>&nbsp;&nbsp;" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + ":&nbsp;</td>";
                        strValue += "<td style=' border: 0px solid #dcdcdc;width:60%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + EMR_ID + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            SegmentBAL objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }


        void BindSegmentVisitDtls(string strType, string strSubType, string ESVD_ID)
        {
            string strValueYes = "", strValue = ""; ;
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';

            if (strSubType != "")
            {
                Criteria += " AND ESVD_SUBTYPE = '" + strSubType + "'";
            }




            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                        strValue = Convert.ToString(DR["ESVD_VALUE"]);
                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Allergy to Penicillin".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radPenicillinYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radPenicillinNo.Checked = true;
                            }
                        }


                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Diabetes".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radDiabYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radDiabNo.Checked = true;
                            }
                        }

                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Teuberculosis".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radTeuberculosisYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radTeuberculosisNo.Checked = true;
                            }
                        }





                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "High Blood Pressure".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radBPYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radBPNo.Checked = true;
                            }
                        }


                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Hepatitis/Jaundice".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radHepatitisYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radHepatitisNo.Checked = true;
                            }
                        }



                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Heart Attack/ Chest Pain/ Angina".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radHeartAttackYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radHeartAttackNo.Checked = true;
                            }
                        }

                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Steroid Therapy".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radSteroidTherapyYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radSteroidTherapyNo.Checked = true;
                            }
                        }



                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Heart murmur".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radHeartmurmurYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radHeartmurmurNo.Checked = true;
                            }
                        }



                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Epilespsy/Fainting Attacks".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radEpilespsyYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radEpilespsyNo.Checked = true;
                            }
                        }

                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Stroke".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radStrokeYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radStrokeNo.Checked = true;
                            }
                        }

                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Rheumatic Fever".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radRheumaticFeverYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radRheumaticFeverNo.Checked = true;
                            }
                        }


                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Bleeding Disease/Disorder".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radBleedingYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radBleedingNo.Checked = true;
                            }
                        }


                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Asthma".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radAsthmaYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radAsthmaNo.Checked = true;
                            }
                        }



                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Other Illnesses".ToUpper())
                        {
                            OtherIllnesses = strValue;
                        }


                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "ChewTobacco".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radChewTobaccoYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radChewTobaccoNo.Checked = true;
                            }
                        }

                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Smoker".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radSmokerYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radSmokerNo.Checked = true;
                            }
                        }


                        if (Convert.ToString(DR["EST_FIELDNAME_ALIAS"]).ToUpper() == "Alcohol".ToUpper())
                        {
                            if (strValueYes == "Y")
                            {
                                radAlcoholYes.Checked = true;
                            }

                            if (strValueYes == "N")
                            {
                                radAlcoholNo.Checked = true;
                            }
                        }


                        if (Convert.ToString(DR["ESVD_TYPE"]).ToUpper() == "PSYCHOLOGICAL_STATUS")
                        {
                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "1".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radDentistVisitRegYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radDentistVisitRegNo.Checked = true;
                                }

                                lblDentistVisitReg.InnerText = strValue;
                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "2".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radTraumaTeethYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radTraumaTeethNo.Checked = true;
                                }

                                lblTraumaTeeth.InnerText = strValue;
                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "3".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radSensitivityYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radSensitivityNo.Checked = true;
                                }

                                lblSensitivity.InnerText = strValue;
                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "4".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radSwellingYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radSwellingNo.Checked = true;
                                }

                                lblSwelling.InnerText = strValue;
                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "5".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radRootCanalYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radRootCanalNo.Checked = true;
                                }

                                lblRootCanal.InnerText = strValue;
                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "6".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radPastOrthTreatYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radPastOrthTreatNo.Checked = true;
                                }

                                lblPastOrthTreat.InnerText = strValue;
                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "7".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radBleedinggumsYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radBleedinggumsNo.Checked = true;
                                }


                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "8".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radBadTasteYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radBadTasteNo.Checked = true;
                                }

                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "9".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radBadBreathYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radBadBreathNo.Checked = true;
                                }

                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "10".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radFoodWeddingYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radFoodWeddingNo.Checked = true;
                                }

                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "11".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radGumBoilsYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radGumBoilsNo.Checked = true;
                                }

                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "12".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radFrequentUlcersYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radFrequentUlcersNo.Checked = true;
                                }

                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "13".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radMouthBreathrYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radMouthBreathrNo.Checked = true;
                                }

                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "14".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radBruxerYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radBruxerNo.Checked = true;
                                }

                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "15".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radClickingjointsYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radClickingjointsNo.Checked = true;
                                }

                            }


                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "16".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radDifficultychewingYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radDifficultychewingNo.Checked = true;
                                }

                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "17".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radDiffMouthOpenYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radDiffMouthOpenNo.Checked = true;
                                }

                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "18".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radWisdomTeethProbYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radWisdomTeethProbNo.Checked = true;
                                }

                            }

                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "19".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radProbBleedYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radProbBleedNo.Checked = true;
                                }

                            }



                            if (Convert.ToString(DR["ESVD_FIELDNAME"]).ToUpper() == "20".ToUpper())
                            {
                                if (strValueYes == "Y")
                                {
                                    radBrushinghabitsYes.Checked = true;
                                }

                                if (strValueYes == "N")
                                {
                                    radBrushinghabitsNo.Checked = true;
                                }

                            }
                        }

                    }

                }
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                try
                {

                    EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                    EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);

                    PNLPainScore.EMR_ID = EMR_ID;

                    BindEMRPTMaster();
                    BindPatientHistory();
                    BindVital();
                    BindPharmacy();
                    string Criteria = " 1=1 ";
                    SegmentVisitDtlsALLGet(Criteria);
                    string strValue = "";

                    strValue = "";
                    BindSegmentVisitDtls("HPI", "", "", EMR_ID, out  strValue);
                    HPI = strValue;



                    strValue = "";
                    BindSegmentVisitDtls("HIST_SURGICAL", "", "", EMR_PT_ID, out  strValue);
                    HIST_SURGICAL = strValue;


                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "", "", EMR_PT_ID, out  strValue);
                    HIST_PAST = strValue;


                    strValue = "";
                    BindSegmentVisitDtls("ALLERGY", "", "", EMR_PT_ID, out  strValue);
                    ALLERGY = strValue;



                    //strValue = "";
                    //BindSegmentVisitDtls("HIST_SOCIAL", "", "", EMR_PT_ID, out  strValue);
                    //HIST_SOCIAL = strValue;

                    //strValue = "";
                    //BindSegmentVisitDtls("DENTAL_HISTORY", "", "", EMR_ID, out  strValue);
                    //DENTAL_HISTORY = strValue;

                    strValue = "";
                    BindSegmentVisitDtls("PSYCHOLOGICAL_STATUS", "", "", EMR_ID, out  strValue);
                    PSYCHOLOGICAL_STATUS = strValue;

                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Allergy to Penicillin", EMR_PT_ID, out  strValue);
                    AllergyToPenicillin = strValue;


                    BindSegmentVisitDtls("HIST_PAST", "General", EMR_PT_ID);

                    BindSegmentVisitDtls("HIST_SOCIAL", "General", EMR_PT_ID);

                    BindSegmentVisitDtls("PSYCHOLOGICAL_STATUS", "PSYCHOLOGICAL_STATUS_COMMON", EMR_ID);


                    /*
                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Diabetes", EMR_PT_ID, out  strValue);
                    Diabetes = strValue;


                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Teuberculosis", EMR_PT_ID, out  strValue);
                    Teuberculosis = strValue;



                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "High Blood Pressure", EMR_PT_ID, out  strValue);
                    HighBloodPressure = strValue;

                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Hepatitis/Jaundice", EMR_PT_ID, out  strValue);
                    HepatitisJaundice = strValue;



                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Heart Attack/ Chest Pain/ Angina", EMR_PT_ID, out  strValue);
                    HeartAttackChestPainAngina = strValue;


                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Steroid Therapy", EMR_PT_ID, out  strValue);
                    SteroidTherapy = strValue;



                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Heart murmur", EMR_PT_ID, out  strValue);
                    Heartmurmur = strValue;




                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Epilespsy/Fainting Attacks", EMR_PT_ID, out  strValue);
                    EpilespsyFaintingAttacks = strValue;


                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Stroke", EMR_PT_ID, out  strValue);
                    Stroke = strValue;

                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Rheumatic Fever", EMR_PT_ID, out  strValue);
                    RheumaticFever = strValue;

                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Bleeding Disease/Disorder", EMR_PT_ID, out  strValue);
                    BleedingDiseaseDisorder = strValue;

                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Asthma", EMR_PT_ID, out  strValue);
                    Asthma = strValue;
                   
                    strValue = "";
                    BindSegmentVisitDtls("HIST_PAST", "General", "Other Illnesses", EMR_PT_ID, out  strValue);
                    OtherIllnesses = strValue;
  */

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ClinicalSummary.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}