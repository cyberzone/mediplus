﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class DiagnosisAudit : System.Web.UI.UserControl
    {
        EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
        public string EMR_ID { set; get; }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 AND AUDIT_TYPE='ADDENDUM'";
            //  Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID  IN (" + EMR_ID + ")";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.AuditLogDataGet(Criteria, "DIAGNOSIS");

            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();

                DataColumn Type = new DataColumn();
                Type.ColumnName = "Type";

                DS.Tables[0].Columns.Add(Type);

                Int32 i = 0;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    if (i == 0)
                    {
                        DR["Type"] = "Principal Diagnosis:";
                    }
                    else
                    {
                        DR["Type"] = "Secondary Diagnosis:";

                    }

                    i = i + 1;
                }

                DS.Tables[0].AcceptChanges();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvDiagnosis.DataSource = DS;
                    gvDiagnosis.DataBind();

                    string[] arrEMR_ID = EMR_ID.Split(',');

                    if (arrEMR_ID.Length > 1)
                    {
                        gvDiagnosis.Columns[0].Visible = true;
                    }

                }
            }
            else
            {
                gvDiagnosis.DataBind();
            }
        FunEnd: ;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                BindDiagnosis();

            }
        }
    }
}