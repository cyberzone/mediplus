﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LaboratoriesAudit.ascx.cs" Inherits="Mediplus.EMR.WebReports.LaboratoriesAudit" %>

<table style="width: 100%;">
    <tr>
        <td class="ReportCaption BoldStyle">Laboratory  
        </td>
    </tr>
    <tr>
        <td class="ReportCaption  BoldStyle">
            <asp:GridView ID="gvLabRequest" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="ReportCaption" Font-Bold="true" />
                <RowStyle CssClass="ReportCaption" />

                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <asp:Label ID="Label27" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPL_LAB_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Label ID="Label28" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPL_LAB_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField  HeaderText="User" DataField="AUDIT_USER_ID"  HeaderStyle-Width="100px" />
                    <asp:BoundField  HeaderText="Date" DataField="AUDIT_DATEDesc"  HeaderStyle-Width="100px" />
                    <asp:BoundField  HeaderText="Reason" DataField="AUDIT_REASON"  HeaderStyle-Width="200px"/>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
