﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;


namespace Mediplus.EMR.WebReports
{
    public partial class VisitDetailsRemarks : System.Web.UI.UserControl
    {
        public string EMR_ID = "", EMR_PT_ID = "", DR_ID, EMR_DATE = "";

        private static SegmentBAL _segmentBAL = new SegmentBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public DataSet GetSegmentMaster()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " and esm_type='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public string SegmentVisitDtlsGet(string Criteria)
        {
            string strValue = "";
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + EMR_ID + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                strValue = Convert.ToString(DS.Tables[0].Rows[0]["ESVD_VALUE"]);
            }


            return strValue;

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            ViewState["control"] = "textbox";
            //   createDynamicTextBox("TextBox");

            CreateSegCtrls();

        }

        public void CreateSegCtrls()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "'";

                DS1 = SegmentTemplatesGet(Criteria1);

                Int32 i = 1;
                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValue = "";
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                    strValue = SegmentVisitDtlsGet(Criteria2);


                    HtmlGenericControl tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td1 = new HtmlGenericControl("td");
                    Label lbl = new Label();
                    //  lbl.ID = "lbl" + Convert.ToString(i) + "-" + strValue;
                    lbl.ID = "lbl" + i + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");

                    lbl.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) +" : ";
                    lbl.CssClass = "lblCaption1";
                    lbl.Style.Add("font-weight", "bold");

                    td1.Controls.Add(lbl);
                    tr.Controls.Add(td1);

                    HtmlGenericControl td2 = new HtmlGenericControl("td");
                    Label txtBox = new Label();
                 //   txtBox.ID = "txt" + Convert.ToString(i) + "-" + strValue;
                    txtBox.ID = "txt" + i + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");

                    txtBox.Text = strValue;
                    txtBox.CssClass = "lblCaption1";

                    td2.Controls.Add(txtBox);
                    tr.Controls.Add(td2);
                    if (strValue != "")
                    {
                        PlaceHolder1.Controls.Add(tr);
                    }




                    i = i + 1;
                }


            }
        }
    }
}