﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.WebReports
{
    public partial class AddendumSummary : System.Web.UI.Page
    {
        public string EMR_PT_ID = "", ProcedureNotes = "", AUDIT_USER_ID = "", AUDIT_DATEDesc = "", AUDIT_REASON = "";
        public string EMR_CLSUMMARY_SEGMENTS = "", EMR_CLSUMMARY_SEGMENTS1 = "";


        SegmentBAL objSeg = new SegmentBAL();
        #region Methods
        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }

        void BindEMRPTMaster()
        {
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND AUDIT_TYPE='ADDENDUM' ";
            Criteria += " AND EPM_ID = '" + Convert.ToString(ViewState["EMR_ID"]) + "'";
            CommonBAL objCom = new CommonBAL();
            DS = objCom.AuditLogDataGet(Criteria, "PT_MASTER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                hidEMRDeptName.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("EPH_CC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]) != "")
                    lblCC.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]);


                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                    lblTreatmentPlan.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                else
                    lblTreatmentPlan.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                    lblFollowupNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]));
                else
                    lblFollowupNotes.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_PROCEDURE_REMARKS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_PROCEDURE_REMARKS"]) != "")
                    ProcedureNotes = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PROCEDURE_REMARKS"]));
                else
                    ProcedureNotes = "";

                hidEMRDeptID.Value = BindDepID(hidEMRDeptName.Value);

                if (DS.Tables[0].Rows[0].IsNull("AUDIT_USER_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["AUDIT_USER_ID"]) != "")
                    AUDIT_USER_ID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["AUDIT_USER_ID"]));
                else
                    AUDIT_USER_ID = "";

                if (DS.Tables[0].Rows[0].IsNull("AUDIT_DATEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["AUDIT_DATEDesc"]) != "")
                    AUDIT_DATEDesc = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["AUDIT_DATEDesc"]));
                else
                    AUDIT_DATEDesc = "";

                if (DS.Tables[0].Rows[0].IsNull("AUDIT_REASON") == false && Convert.ToString(DS.Tables[0].Rows[0]["AUDIT_REASON"]) != "")
                    AUDIT_REASON = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["AUDIT_REASON"]));
                else
                    AUDIT_REASON = "";


            }

        }

        void BindSegmentVisitDtls(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        void BindSegmentVisitDtls1(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {

                    DataRow[] result = DS.Tables[0].Select(Criteria);

                    if (result.Count() > 0)
                    {
                        strValue += " <span class='ReportCaption BoldStyle' >" + Convert.ToString(result[0]["EMD_MENU_NAME"]) + "</span></BR> ";

                        strValue += "<table style='width:100%;' >";
                        foreach (DataRow DR in result)
                        {
                            strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                        }


                        strValue += "</table>";
                    }

                }
            }

        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " AND ESM_TYPE='" + SegmentType + "'";//HIST_PAST'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = objSeg.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        void BindSegmentVisitDtlsWithSubHeader(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "";
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                DS1 = SegmentTemplatesGet(Criteria1);

                IsDataAvailable = false;

                if (DS1.Tables[0].Rows.Count > 0)
                {
                    strHeader = "<table style='width:100%;' >";
                    strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='ReportCaption' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]) + "</td></tr>";

                    Int32 i = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {

                        string Criteria2 = "1=1 ";
                        Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string strValue1 = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                        if (strValue1 != "")
                        {
                            IsDataAvailable = true;
                            strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                            strData += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + strValue1 + "</td></tr>";
                        }

                    }
                    if (IsDataAvailable == true)
                    {
                        strValue += strHeader;
                        strValue += strData;
                        strValue += "</table>";
                        strData = "";
                    }


                }

            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }


        void BindSegmentVisitDtlsWithSubHeader1(string strType, string ESVD_ID, out string strValue)
        {
            strValue = "";
            string strHeader = "", strData = "", strMainHeader = ""; ;
            Boolean IsDataAvailable = false;

            DataSet DS = new DataSet();
            DS = GetSegmentMaster(strType);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strMenuName = GetEMRMenuName(strType);

                if (strMenuName != "")
                {
                    strMainHeader += " <span class='ReportCaption BoldStyle' >" + strMenuName + "</span></BR> ";

                }

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();
                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                    Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                    DS1 = SegmentTemplatesGet(Criteria1);

                    IsDataAvailable = false;

                    if (DS1.Tables[0].Rows.Count > 0)
                    {

                        strHeader = "<table style='width:100%;' >";
                        strHeader += "<tr><td style=' border: 0px solid #dcdcdc;height:25px;width:200px;' class='ReportCaption' colspan='2' >" + Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]) + "</td></tr>";

                        Int32 i = 1;
                        foreach (DataRow DR1 in DS1.Tables[0].Rows)
                        {

                            string Criteria2 = "1=1 ";
                            Criteria2 += " AND ESVD_ID='" + ESVD_ID + "'";
                            Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                            Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                            string strValue1 = "", strValueYes = "";
                            SegmentVisitDtlsGet(Criteria2, out  strValue1, out  strValueYes);
                            if (strValue1 != "")
                            {
                                IsDataAvailable = true;
                                strData += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) + "</td>";
                                strData += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + strValue1 + "</td></tr>";
                            }

                        }
                        if (IsDataAvailable == true)
                        {
                            strValue += strHeader;
                            strValue += strData;
                            strValue += "</table>";
                            strData = "";
                        }


                    }
                }
                strValue = strMainHeader + strValue;
            }
            /*
                        string Criteria = " 1=1 ";
                        Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
                        Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
                        DS = objSeg.SegmentVisitDtlsGet(Criteria);
                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            strValue = "<table style='width:100%;' >";
                            for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                            {
                                strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:200px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_SUBTYPE"]) + "</td>";
                                strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;'>" + Convert.ToString(DS.Tables[0].Rows[i]["ESVD_VALUE"]) + "</td></tr>";

                            }


                            strValue += "</table>";

                        }*/

        }


        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = Convert.ToString(Session["EMR_SEGMENTS_SAVE_PTID"]).Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND AUDIT_TYPE='ADDENDUM' AND (  ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ( ESVD_ID='" + EMR_PT_ID + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            CommonBAL objSeg = new CommonBAL();
            DS = objSeg.EMRSegmentVisitDtlsAuditWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }

        string BindPTHeaderSegment(string SegmentType)
        {

            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + SegmentType + "'";  // 'ROS' 'HIST_PAST';

            string strPTIDSegment = Convert.ToString(Session["EMR_SEGMENTS_SAVE_PTID"]); // hidSegTypes_SavePT_ID.Value;

            //if (strPTIDSegment.IndexOf(SegmentType) > -1)
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            //}
            //else
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            //}

            string ESVD_ID = "";

            if (strPTIDSegment.IndexOf("|" + SegmentType + "|") > -1)
            {
                ESVD_ID = EMR_PT_ID;// Convert.ToString(Session["EMR_PT_ID"]);
            }
            else
            {
                ESVD_ID = Convert.ToString(ViewState["EMR_ID"]);// Convert.ToString(Session["EMR_ID"]);
            }


            BindSegmentVisitDtls1(SegmentType, ESVD_ID, out  strValue);

            //SegmentBAL objSeg = new SegmentBAL();
            //DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias1(Criteria);


            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow DR in DS.Tables[0].Rows)
            //    {
            //        strValue += Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + ":&nbsp;";
            //        strValue += " <span class='ReportCaption'  style='color: #FFFF00;font-weight: bold; font-size: 11px;' >" + Convert.ToString(DR["ESVD_VALUE"]) + "</span>, ";

            //    }

            //    strValue += "</BR>";
            //}

            return strValue;
        }

        public void SegmentVisitDtlsALLGet()
        {

            string strSegment = Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS"]);

            string strSegmentDtls = "";
            string[] arrhidSegTypes = strSegment.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                if (arrhidSegTypes[intSegType] != "")
                {
                    strSegmentDtls += BindPTHeaderSegment(arrhidSegTypes[intSegType]);

                }

            }
            EMR_CLSUMMARY_SEGMENTS = strSegmentDtls;



        }


        string BindPTHeaderSegment1(string SegmentType)
        {

            string strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + SegmentType + "'";  // 'ROS' 'HIST_PAST';

            string strPTIDSegment = Convert.ToString(Session["EMR_SEGMENTS_SAVE_PTID"]); // hidSegTypes_SavePT_ID.Value;

            //if (strPTIDSegment.IndexOf(SegmentType) > -1)
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            //}
            //else
            //{
            //    Criteria += " AND ESVD_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            //}

            string ESVD_ID = "";

            if (strPTIDSegment.IndexOf("|" + SegmentType + "|") > -1)
            {
                ESVD_ID = EMR_PT_ID;// Convert.ToString(Session["EMR_PT_ID"]);
            }
            else
            {
                ESVD_ID = Convert.ToString(ViewState["EMR_ID"]);// Convert.ToString(Session["EMR_ID"]);
            }


            BindSegmentVisitDtlsWithSubHeader1(SegmentType, ESVD_ID, out  strValue);


            return strValue;
        }

        public void SegmentVisitDtlsALLGet1()
        {

            string strSegment = Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS1"]);

            string strSegmentDtls = "";
            string[] arrhidSegTypes = strSegment.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                if (arrhidSegTypes[intSegType] != "")
                {
                    strSegmentDtls += BindPTHeaderSegment1(arrhidSegTypes[intSegType]);

                }

            }
            EMR_CLSUMMARY_SEGMENTS1 = strSegmentDtls;



        }


        string GetEMRMenuName(string SegType)
        {
            string strMenuName = "";
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();

            string Criteria = " EMD_MENU_ACTION='" + SegType + "'";
            Criteria += "  AND (  EMD_DEPT_ID LIKE '%|" + hidEMRDeptID.Value + "|%' OR EMD_DEPT_ID ='' OR EMD_DEPT_ID is null ) ";
            Criteria += "  AND (  EMD_DEPT_ID_HIDE NOT LIKE '%|" + hidEMRDeptID.Value + "|%' OR EMD_DEPT_ID_HIDE ='' OR EMD_DEPT_ID_HIDE is null ) ";


            DS = objCom.fnGetFieldValue("*", "EMR_MENU_DETAILS", Criteria, "EMD_MENU_NAME");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strMenuName = Convert.ToString(DS.Tables[0].Rows[0]["EMD_MENU_NAME"]);
            }


            return strMenuName;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                pnlDiagnosis.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                pnlProcedures.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                pnlRadiologies.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                pnlLaboratories.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                pnlPrescriptions.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                pnlVitalSign.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);

                BindEMRPTMaster();

                string Criteria = " 1=1 ";
                SegmentVisitDtlsALLGet(Criteria);

                if (Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS"]) != "")
                {
                    SegmentVisitDtlsALLGet();
                }

                if (Convert.ToString(Session["EMR_CLSUMMARY_SEGMENTS1"]) != "")
                {
                    SegmentVisitDtlsALLGet1();
                }


            }
        }
    }
}