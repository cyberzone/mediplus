﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;


namespace Mediplus.EMR.WebReports
{
    public partial class Radiologies : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        EMR_PTRadiology objRad = new EMR_PTRadiology();

        void BindRadiologyRequest()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            //Criteria += " AND EPR_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            if (Convert.ToString(Session["EMR_REQUIRED_AUDIT"]) == "1")
            {
                Criteria += " AND (  EPR_STATUS <>'D'  OR EPR_STATUS IS NULL )  ";
            }

            Criteria += "AND EPR_ID in (" + EMR_ID + ")";


            DS = objRad.RadiologyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadRequest.DataSource = DS;
                gvRadRequest.DataBind();
            }
            else
            {
                gvRadRequest.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRadiologyRequest();
            }
        }
    }
}