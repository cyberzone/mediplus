﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Radiologies.ascx.cs" Inherits="Mediplus.EMR.WebReports.Radiologies" %>


<table style="width:100%;">
                                                     <tr>
                                      <td class="ReportCaption BoldStyle"  >
                                      Radiology  
                                      </td>
                                  </tr>
                                   <tr>
                                       <td class="ReportCaption  BoldStyle">
                                  <asp:GridView ID="gvRadRequest" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="ReportCaption" Font-Bold="true" />
                                            <RowStyle CssClass="ReportCaption" />
                            
                                            <Columns>
                                                <asp:TemplateField HeaderText="Code"  HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label31" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPR_RAD_CODE") %>'  ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="90%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label32" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPR_RAD_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Price" HeaderStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                        
                                                            <asp:Label ID="Label33" CssClass="ReportCaption" runat="server" Text="0.00"  ></asp:Label>
                                        
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                 
                                           </Columns>
                                         </asp:GridView>
                                              </td>
                               </tr>
               </table>