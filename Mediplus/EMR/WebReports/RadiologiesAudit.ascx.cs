﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace Mediplus.EMR.WebReports
{
    public partial class RadiologiesAudit : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        EMR_PTRadiology objRad = new EMR_PTRadiology();

        void BindRadiologyRequest()
        {

            string Criteria = " 1=1 AND AUDIT_TYPE='ADDENDUM' ";


            Criteria += " AND EPR_ID  IN (" + EMR_ID + ")";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.AuditLogDataGet(Criteria, "RADIOLOGY");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadRequest.DataSource = DS;
                gvRadRequest.DataBind();
            }
            else
            {
                gvRadRequest.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRadiologyRequest();
            }
        }
    }
}