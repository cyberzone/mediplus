﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;

namespace MediplusEMR.WebReports.InPatient
{
    public partial class AdmissionSummary : System.Web.UI.Page
    {
        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_EMR_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("IAS_DEPARTMENT") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]) != "")
                    lblDept.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATE"]) != "")
                    lblEmrDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATEDesc"]);



                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]) != "")
                    lblPTFullName.Text =Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]) != "")
                    lblFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]) != "")
                    lblNationality.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]);



                if (DS.Tables[0].Rows[0].IsNull("IAS_AGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]) != "")
                    lblAge.Text =Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]);
                //lblAgeType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //lblAge1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE1"]);
                //lblAgeType1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_MOBILE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]) != "")
                    lblMobile.Text =Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_IP_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]) != "")
                    lblEPMID.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]) != "")
                    lblSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]) != "")
                    lblInsCo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]) != "")
                    lblPolicyType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]) != "")
                    lblPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]);


                if (DS.Tables[0].Rows[0].IsNull("IAS_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]) != "")
                    lblDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);

                lblDrCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]);

                //if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                //    lblEmiratesID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_TYPE"]) != "")
                    lblVisitType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_TYPE"]);




                lblAdmissionNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);
                lblAdmissionType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                lblAddmissionMode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);




                //lblNationality.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]);

                //lblFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]);
                //lblDoctor.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                //lblPolicyType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]);


                //lblAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]);
                //lblSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]);
                //lblAdmissionDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateDesc"]) + " " + Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTimeDesc"]);
                //lblPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]);

                //lblMobile.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                //lblDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOBDesc"]);
                //lblProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);


                ////lblRoom.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                ////lblBead.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOB"]);
                ////lblWard.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);

                return true;
            }
            else
            {

                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }


            if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                ViewState["EMR_PT_ID"] = Convert.ToString(Request.QueryString["EMR_PT_ID"]);

                ViewState["DR_ID"] = Convert.ToString(Request.QueryString["DR_ID"]);
                ViewState["IAS_ADMISSION_NO"] = Convert.ToString(Request.QueryString["ADMISSION_NO"]);

                lblAdmissionNo.Text = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);
                // BindEMRPTMaster();
                BindAdmissionSummary();

                ucAdmissionSummaryReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                ucAdmissionSummaryReport.IAS_ADMISSION_NO = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);
                ucAdmissionSummaryReport.BindAdmissionSummary();


            }
        }
    }
}