﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace MediplusEMR.WebReports.InPatient
{
    public partial class AdmissionSummaryReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_Log.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("AdmissionSummaryReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_EMR_ID='" + EMR_ID + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                radAdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);
                AttendingPhysician.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ATTENDING_PHYSICIANName"]);

                ProvisionalDiagnosis.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROVISIONAL_DIAGNOSIS"]);
                Laboratory.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_LABORATORY"]);
                Radiological.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_RADIOLOGICAL"]);
                //ReferralPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]);
                OtherAdmissionSummary.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_OTHERS"]);



                ProcedurePlanned.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROCEDURE_PLANNED"]);
                Anesthesia.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ANESTHESIA"]);
                Treatment.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_TREATMENT"]);
                ReasonforAdmission.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REASON_FOR_ADMISSION"]);
                Remarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REMARKS"]);

            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                if (!IsPostBack)
                {


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}