﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="ReassessmentSheet.aspx.cs" Inherits="Mediplus.EMR.WebReports.ReassessmentSheet" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/VitalSignReport.ascx" TagPrefix="UC1" TagName="VitalSignReport" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx"  TagPrefix ="UC1" TagName="Diagnosis"%>
<%@ Register Src="~/EMR/WebReports/StandardPrescriptions.ascx" TagPrefix="UC1" TagName="StandardPrescriptions"  %>
<%@ Register Src="~/EMR/WebReports/Prescriptions.ascx" TagPrefix="UC1" TagName="Prescriptions"  %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        

        .box-title
        {
           
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
     <table style="width:100%;text-align:center;vertical-align:top;" >
        <tr>
            <td >
                     <img style="padding:1px;height:100px;border:none;" src="images/Report_Logo.PNG"  />
            </td>
        </tr>

    </table>
   <span class="box-title">Prescription</span>
           
     
   
     <UC1:PatientReportHeader id="pnlPatientReportHeader" runat="server"    ></UC1:PatientReportHeader>
    <%if(  CriticalNotes != null) %>
    <%{%> 
    <div style="clear:both">&nbsp;</div>	
            <label class="lblCaption1">Allergies if any: <%= CriticalNotes != null?CriticalNotes:"" %></label> 
     <%}%> 
    <div style="clear:both">&nbsp;</div>	
            <UC1:Diagnosis ID="pnlDiagnosis" runat="server"   ></UC1:Diagnosis>
   
       <UC1:Prescriptions ID="pnlPrescriptions" runat="server"   ></UC1:Prescriptions>
    <br />
    <table id="tbleAuth" runat="server" style="width:100%;text-align:center;border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td style="width:80%;text-align:center;border: 1px solid #dcdcdc" class="lblCaption1  BoldStyle">
                E-Authorization Reference No
            </td>
            <td style="width:20%;text-align:center;border: 1px solid #dcdcdc" class="lblCaption1  BoldStyle">
                Status
            </td>
        </tr>
        <tr>
            <td style="width:80%;text-align:center;border: 1px solid #dcdcdc" class="lblCaption1">
                <asp:Label ID="lbleAuthSRefNo" runat="server" CssClass="label"  Font-Bold="true" ></asp:Label>
            </td>
            <td style="width:20%;text-align:center;border: 1px solid #dcdcdc" class="lblCaption1">
                 <asp:Label ID="lbleAuthStatus" runat="server" CssClass="label"   Font-Bold="true" ></asp:Label>
            </td>
        </tr>
    </table>
    <div style="clear:both">&nbsp;</div>	
       <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"   ></UC1:DoctorSignature>
</asp:Content>
