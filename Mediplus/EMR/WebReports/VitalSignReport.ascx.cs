﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.WebReports
{
    public partial class VitalSignReport : System.Web.UI.UserControl
    {
        public string EMR_ID = "";

        EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
        public string Weight, Height, BMI, Temperature, Pulse, Respiration, BpSystolic, BpDiastolic, Sp02, CapillaryBloodSugar, JaundiceMeter, AbdominalGirth, HeadCircumference, ChestCircumference, EcgReport, Others, BirthWeight, Hemoglobin, EDDDate;
        public string LMPDate, Pregnant;
        public void BindVital()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                Weight = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                Height = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                BMI = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);
                Temperature = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);
                Pulse = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PULSE"]);
                Respiration = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                BpSystolic = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                BpDiastolic = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);

                Hemoglobin = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEMOGLOBIN"]);

                AbdominalGirth = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_ABDOMIANL_GIRTH"]);

                HeadCircumference = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_HEADCIRC"]);
                ChestCircumference = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BIRTH_WEIGHT"]);

                BirthWeight = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_CHESTCIRC"]);
                Sp02 = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SPO2"]);
                CapillaryBloodSugar = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_CAPILLARY_BLOOD_SUGAR"]);
                JaundiceMeter = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_JAUNDICE_METER"]);
                EcgReport = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_ECG_REPORT"]);
                Others = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_OTHERS"]);


                if (Convert.ToString(Session["HPM_SEX"]).ToUpper() == "FEMALE")
                {
                    trFemale.Visible = true;

                    LMPDate = Convert.ToString(DS.Tables[0].Rows[0]["EPV_LMP_DATE"]);
                    EDDDate = Convert.ToString(DS.Tables[0].Rows[0]["EPV_EDD_DATE"]);
                    if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "1")
                    {
                        Pregnant = "Yes";
                    }
                    else
                    {
                        Pregnant = "No";
                    }

                }

                //if (txtTemperatureF.Value != "")
                //{
                //    //var temp = Convert.ToDecimal(txtTemperatureF.Value);
                //    //var tempInF = Math.Round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;

                //    //txtTemperatureC.Value = Convert.ToString(tempInF);
                //    //   ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "FtoC('');", true);
                //    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "FtoC();", true);
                //}

            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindVital();

                if (GlobalValues.FileDescription == "HOLISTIC")
                {



                    // trCapillary.Visible = false;
                    trECG.Visible = false;
                    trJaundice.Visible = false;
                    //  tblLocation.Visible = false;

                }

            }
        }
    }
}