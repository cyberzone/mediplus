﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PharmacyInternal.ascx.cs" Inherits="Mediplus.EMR.WebReports.PharmacyInternal" %>
<table width="100%">
    <tr>
        <td class="ReportCaption BoldStyle">Pharmacy Internal
        </td>
    </tr>
    <tr>
        <td class="ReportCaption  BoldStyle">
            <asp:GridView ID="gvPharmacyHistory" runat="server" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="ReportCaption" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                <RowStyle CssClass="ReportCaption" />


                <Columns>
                    <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <%# Eval("EPPI_DATEDesc")  %>    <%# Eval("EPPI_DATETimeDesc")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_PHY_CODE")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_PHY_NAME")  %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_DOSAGE")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                               <%# Eval("EPPI_ROUTEDesc")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_FREQUENCYTYPEDesc")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Duration" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_DURATION_TYPEDesc")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_REMARKS")  %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>

                            <%# Eval("EPPI_GIVENBY_NAME")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_GIVEN_DATEDesc")  %>     <%# Eval("EPPI_GIVEN_DATETimeDesc")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Verified By" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                        <ItemTemplate>
                            <%# Eval("EPPI_VERIFIEDBY_NAME")  %>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# Eval("EPPI_VERIFIED_DATEDesc")  %>   <%# Eval("EPPI_VERIFIED_DATETimeDesc")  %>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
            </asp:GridView>

        </td>
    </tr>
</table>
