﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.WebReports
{
    public partial class Laboratories : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        EMR_PTLaboratory objLab = new EMR_PTLaboratory();

        void BindLaboratoryRequest()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            //Criteria += " AND EPL_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            if (Convert.ToString(Session["EMR_REQUIRED_AUDIT"]) == "1")
            {
                Criteria += " AND (  EPL_STATUS <>'D'  OR EPL_STATUS IS NULL )  ";
            }

            Criteria += " AND EPL_ID in (" + EMR_ID + ")";


            DS = objLab.LaboratoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLabRequest.DataSource = DS;
                gvLabRequest.DataBind();
            }
            else
            {
                gvLabRequest.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLaboratoryRequest();
            }
        }
    }
}