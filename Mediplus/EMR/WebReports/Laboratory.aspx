﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="Laboratory.aspx.cs" Inherits="Mediplus.EMR.WebReports.Laboratory" %>
<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/Diagnosis.ascx"  TagPrefix ="UC1" TagName="Diagnosis"%>
<%@ Register Src="~/EMR/WebReports/Laboratories.ascx" TagPrefix="UC1" TagName="Laboratories"  %>
<%@ Register Src="~/EMR/WebReports/DoctorSignature.ascx" TagPrefix="UC1" TagName="DoctorSignature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         #site-logo
         {
             display: none;
         }

         .box-header
         {
             display: none;
         }

         table
         {
             border-collapse: collapse;
         }

         /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

         .BoldStyle
         {
             font-weight: bold;
         }
     </style>
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
 
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" runat="server">

    <div class="box">
        <div >
            <h4 class="box-title">Laboratory Request </h4>
        </div>
    </div>
    
    <UC1:PatientReportHeader id="pnlPatientReportHeader" runat="server"    ></UC1:PatientReportHeader>
  
    <div style="clear:both">&nbsp;</div>	
            <UC1:Diagnosis ID="pnlDiagnosis" runat="server"   ></UC1:Diagnosis>
      <div style="clear:both">&nbsp;</div>	
     <label class="lblCaption1" style="font-weight:bold;"> Chief Complaints</label><br />
            <label class="lblCaption1"><%= ChiefComplaint != null?ChiefComplaint.Replace("\\n","<br/>"):"" %></label> <br /><br />
    <label class="lblCaption1" style="font-weight:bold;"> Allergy</label><br />
            <label class="lblCaption1"><%= Allergy != null?Allergy.Replace("\\n","<br/>"):"" %></label> 
    <div style="clear:both">&nbsp;</div>	
       <UC1:Laboratories ID="Laboratories" runat="server"   ></UC1:Laboratories>
    <div style="clear:both">&nbsp;</div>	
       <UC1:DoctorSignature ID="pnlDoctorSignature" runat="server"   ></UC1:DoctorSignature>
</asp:Content>
