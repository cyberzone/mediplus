﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="AddendumSummary.aspx.cs" Inherits="Mediplus.EMR.WebReports.AddendumSummary" %>
<%@ Register Src="~/EMR/WebReports/DiagnosisAudit.ascx" TagPrefix="UC1" TagName="Diagnosis" %>
<%@ Register Src="~/EMR/WebReports/ProceduresAudit.ascx" TagPrefix="UC1" TagName="Procedures" %>
<%@ Register Src="~/EMR/WebReports/RadiologiesAudit.ascx" TagPrefix="UC1" TagName="Radiologies" %>
<%@ Register Src="~/EMR/WebReports/LaboratoriesAudit.ascx" TagPrefix="UC1" TagName="Laboratories" %>
<%@ Register Src="~/EMR/WebReports/PrescriptionsAudit.ascx" TagPrefix="UC1" TagName="Prescriptions" %>
<%@ Register Src="~/EMR/WebReports/VitalSignAudit.ascx" TagPrefix="UC1" TagName="VitalSign" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
     <input type="hidden" id="hidEMRDeptID" runat="server" />
    <input type="hidden" id="hidEMRDeptName" runat="server" />
 
    <div  class="ReportHeaderDiv">
       <span  class="ReportHeader"> Addendum Summary </span>   
    </div>
 <br /><br /> 

      <table   style="width:100%;">
         <tr>
            <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc;width:100px;">User
            </td>
             <td style="border: 1px solid #dcdcdc;width:100px;">
                <span class="ReportCaption" ><%=AUDIT_USER_ID  %></span>
            </td>
              <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc;width:100px;">Date
            </td>
             <td style="border: 1px solid #dcdcdc;width:100px;">
                <span class="ReportCaption" ><%=AUDIT_DATEDesc  %></span>
            </td>
              <td class="ReportCaption BoldStyle" style="border: 1px solid #dcdcdc;width:100px;">Reason
            </td>
             <td style="border: 1px solid #dcdcdc;width:100px;">
                <span class="ReportCaption" ><%=AUDIT_REASON  %></span>
            </td>

         </tr>

        <tr>
            <td class="ReportCaption BoldStyle" colspan="6">Chief Complaints 
            </td>
        </tr>
        <tr>
            <td colspan="6">

                
                 <asp:TextBox ID="lblCC" Visible="false" CssClass="ReportCaption" runat="server" TextMode="MultiLine" Height="50px" Width="100%" ReadOnly="true" style="resize:both;border:none;" ></asp:TextBox>
            </td>
        </tr>
            <tr>
            <td colspan="6">
                <span class="ReportCaption"><%=EMR_CLSUMMARY_SEGMENTS  %></span>
            </td>
        </tr>
     <tr>
            <td colspan="6">
                <span class="ReportCaption"><%=EMR_CLSUMMARY_SEGMENTS1  %></span>
            </td>
        </tr>
       </table>

    <UC1:Diagnosis ID="pnlDiagnosis" runat="server"></UC1:Diagnosis>
   
   <UC1:Laboratories ID="pnlLaboratories" runat="server"></UC1:Laboratories>
   <UC1:Radiologies ID="pnlRadiologies" runat="server"></UC1:Radiologies>
   <UC1:Procedures ID="pnlProcedures" runat="server"></UC1:Procedures>

    
    <% if (ProcedureNotes != "" && ProcedureNotes != null)
       {  %>
    <div>
        <span class="ReportCaption">Procedure Notes </span>
        <br />
        <span class="ReportCaption"><%=ProcedureNotes %></span>
    </div>
    <%} %>

    <UC1:Prescriptions ID="pnlPrescriptions" runat="server"></UC1:Prescriptions>


    <UC1:VitalSign ID="pnlVitalSign" runat="server"></UC1:VitalSign>

      <table width="100%" style="border-collapse: collapse;">
       
        <tr>
            <td class="ReportCaption" style="font-weight:bold;" >Treatment Plan</td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="lblTreatmentPlan" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
        </tr>
       
        <tr>
            <td class="ReportCaption" style="font-weight:bold;" >FollowupNotes
            </td>
        </tr>
         
        <tr>
            <td >
                <asp:Label ID="lblFollowupNotes" CssClass="ReportCaption" runat="server"></asp:Label>
            </td>
        </tr>
    </table>



</asp:Content>
