﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class ProceduresAudit : System.Web.UI.UserControl
    {
        public string EMR_ID { get; set; }

        EMR_PTProcedure objPTProc = new EMR_PTProcedure();

        void BindProcedure()
        {
            string Criteria = " 1=1 AND AUDIT_TYPE='ADDENDUM' ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            Criteria += " AND EPP_ID  IN (" + EMR_ID + ")";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.AuditLogDataGet(Criteria, "PROCEDURE");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProcedure.DataSource = DS;
                gvProcedure.DataBind();

                string[] arrEMR_ID = EMR_ID.Split(',');

                if (arrEMR_ID.Length > 1)
                {
                    gvProcedure.Columns[0].Visible = true;
                }

            }
            else
            {
                gvProcedure.DataBind();
            }




        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProcedure();
            }
        }
    }
}