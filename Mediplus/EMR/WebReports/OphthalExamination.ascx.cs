﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.WebReports
{
    public partial class OphthalExamination : System.Web.UI.UserControl
    {
        public string EMR_ID = "";

        public string EOPE_BRANCH_ID { set; get; }
        public string EOPE_ID { set; get; }
        public string EOPE_R_UCVA { set; get; }
        public string EOPE_R_PH { set; get; }
        public string EOPE_R_BCVA { set; get; }
        public string EOPE_R_FIELD { set; get; }
        public string EOPE_R_IOP { set; get; }
        public string EOPE_R_EOM { set; get; }
        public string EOPE_R_LIDS { set; get; }
        public string EOPE_R_CONJUNCTIVA { set; get; }
        public string EOPE_R_SCLERA { set; get; }
        public string EOPE_R_CORNEA { set; get; }
        public string EOPE_R_AC { set; get; }
        public string EOPE_R_ANGLE { set; get; }
        public string EOPE_R_IRIS { set; get; }
        public string EOPE_R_LENS { set; get; }
        public string EOPE_R_VITREOUS { set; get; }
        public string EOPE_R_OPTICNERVE { set; get; }
        public string EOPE_R_CHOROID { set; get; }
        public string EOPE_L_UCVA { set; get; }
        public string EOPE_L_PH { set; get; }
        public string EOPE_L_BCVA { set; get; }
        public string EOPE_L_FIELD { set; get; }
        public string EOPE_L_IOP { set; get; }
        public string EOPE_L_EOM { set; get; }
        public string EOPE_L_LIDS { set; get; }
        public string EOPE_L_CONJUNCTIVA { set; get; }
        public string EOPE_L_SCLERA { set; get; }
        public string EOPE_L_CORNEA { set; get; }
        public string EOPE_L_AC { set; get; }
        public string EOPE_L_ANGLE { set; get; }
        public string EOPE_L_IRIS { set; get; }
        public string EOPE_L_LENS { set; get; }
        public string EOPE_L_VITREOUS { set; get; }
        public string EOPE_L_OPTICNERVE { set; get; }
        public string EOPE_L_CHOROID { set; get; }
        public string EOPE_OCT { set; get; }
        public string EOPE_FA { set; get; }
        public string EOPE_FIELD { set; get; }
        public string EOPE_R_NPDR { set; get; }
        public string EOPE_R_CSME { set; get; }
        public string EOPE_L_NPDR { set; get; }
        public string EOPE_L_CSME { set; get; }
        public string EOPE_R_COLORVISION { set; get; }
        public string EOPE_L_COLORVISION { set; get; }
        public string EOPE_R_FIELD_STATUS { set; get; }
        public string EOPE_L_FIELD_STATUS { set; get; }
        public string EOPE_R_IOP_STATUS { set; get; }
        public string EOPE_L_IOP_STATUS { set; get; }
        public string EOPE_R_EOM_STATUS { set; get; }
        public string EOPE_L_EOM_STATUS { set; get; }
        public string EOPE_R_LIDS_STATUS { set; get; }
        public string EOPE_L_LIDS_STATUS { set; get; }
        public string EOPE_R_CONJUNCTIVA_STATUS { set; get; }
        public string EOPE_L_CONJUNCTIVA_STATUS { set; get; }
        public string EOPE_R_SCLERA_STATUS { set; get; }
        public string EOPE_L_SCLERA_STATUS { set; get; }
        public string EOPE_R_CORNEA_STATUS { set; get; }
        public string EOPE_L_CORNEA_STATUS { set; get; }
        public string EOPE_R_AC_STATUS { set; get; }
        public string EOPE_L_AC_STATUS { set; get; }
        public string EOPE_R_ANGLE_STATUS { set; get; }
        public string EOPE_L_ANGLE_STATUS { set; get; }
        public string EOPE_R_IRIS_STATUS { set; get; }
        public string EOPE_L_IRIS_STATUS { set; get; }
        public string EOPE_R_LENS_STATUS { set; get; }
        public string EOPE_L_LENS_STATUS { set; get; }
        public string EOPE_R_VITREOUS_STATUS { set; get; }
        public string EOPE_L_VITREOUS_STATUS { set; get; }
        public string EOPE_R_OPTICNERVE_STATUS { set; get; }
        public string EOPE_L_OPTICNERVE_STATUS { set; get; }
        public string EOPE_R_CHOROID_STATUS { set; get; }
        public string EOPE_L_CHOROID_STATUS { set; get; }
        public string EOPE_R_NPDR_STATUS { set; get; }
        public string EOPE_L_NPDR_STATUS { set; get; }
        public string EOPE_R_CSME_STATUS { set; get; }
        public string EOPE_L_CSME_STATUS { set; get; }
        public string EOPE_R_GONIOSCOPY { set; get; }
        public string EOPE_L_GONIOSCOPY { set; get; }
        public string EOPE_R_BCVA_TYPE { set; get; }
        public string EOPE_L_BCVA_TYPE { set; get; }
        public string EOPE_R_NPDR_TYPE { set; get; }
        public string EOPE_L_NPDR_TYPE { set; get; }
        public string EOPE_R_GONIOSCOPY_STATUS { set; get; }
        public string EOPE_L_GONIOSCOPY_STATUS { set; get; }
        public string EOPE_R_PUPIL { set; get; }
        public string EOPE_L_PUPIL { set; get; }
        public string EOPE_R_PUPIL_STATUS { set; get; }
        public string EOPE_L_PUPIL_STATUS { set; get; }


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {

            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            // Criteria += " AND EOPE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EOPE_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            Criteria += "  AND EOPE_ID='" + EMR_ID + "'";
            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                EOPE_R_UCVA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_UCVA"]);
                EOPE_R_PH = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PH"]);
                EOPE_R_BCVA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_BCVA"]);
                EOPE_R_COLORVISION = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_COLORVISION"]);


                EOPE_L_UCVA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_UCVA"]);
                EOPE_L_PH = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PH"]);
                EOPE_L_BCVA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_BCVA"]);
                EOPE_L_COLORVISION = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_COLORVISION"]);

                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_FIELD") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_FIELD"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_FIELD") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_FIELD"]) != "")
                {
                    //txtField_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_FIELD"]);
                    //txtField_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_FIELD"]);

                    EOPE_R_FIELD = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_FIELD"]);
                    EOPE_L_FIELD = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_FIELD"]);

                    trField.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_IOP") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IOP"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_IOP") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IOP"]) != "")
                {
                    //txtIOP_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IOP"]);
                    //txtIOP_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IOP"]);

                    EOPE_R_IOP = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IOP"]);
                    EOPE_L_IOP = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IOP"]);
                    trIOP.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_EOM") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_EOM"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_EOM") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_EOM"]) != "")
                {
                    //txtEOM_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_EOM"]);
                    //txtEOM_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_EOM"]);

                    EOPE_R_EOM = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_EOM"]);
                    EOPE_L_EOM = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_EOM"]);


                    trEOM.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_LIDS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LIDS"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_LIDS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LIDS"]) != "")
                {
                    //txtLids_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LIDS"]);
                    //txtLids_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LIDS"]);

                    EOPE_R_LIDS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LIDS"]);
                    EOPE_L_LIDS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LIDS"]);


                    trLids.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_CONJUNCTIVA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CONJUNCTIVA"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_CONJUNCTIVA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CONJUNCTIVA"]) != "")
                {
                    //txtConjunctiva_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CONJUNCTIVA"]);
                    //txtConjunctiva_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CONJUNCTIVA"]);

                    EOPE_R_CONJUNCTIVA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CONJUNCTIVA"]);
                    EOPE_L_CONJUNCTIVA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CONJUNCTIVA"]);

                    trConjunctiva.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_SCLERA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_SCLERA"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_SCLERA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_SCLERA"]) != "")
                {
                    //txtSclera_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_SCLERA"]);
                    //txtSclera_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_SCLERA"]);

                    EOPE_R_SCLERA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_SCLERA"]);
                    EOPE_L_SCLERA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_SCLERA"]);

                    trSclera.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_CORNEA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CORNEA"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_CORNEA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CORNEA"]) != "")
                {
                    //txtCornea_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CORNEA"]);
                    //txtCornea_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CORNEA"]);

                    EOPE_R_CORNEA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CORNEA"]);
                    EOPE_L_CORNEA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CORNEA"]);

                    trCornea.Visible = true;

                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_AC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_AC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC"]) != "")
                {
                    //txtAC_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC"]);
                    //txtAC_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC"]);

                    EOPE_R_AC = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC"]);
                    EOPE_L_AC = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC"]);


                    trAC.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_AC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_AC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC"]) != "")
                {
                    //txtAngle_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_ANGLE"]);
                    //txtAngle_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_ANGLE"]);

                    EOPE_R_ANGLE = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_ANGLE"]);
                    EOPE_L_ANGLE = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_ANGLE"]);

                    trAngle.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_IRIS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IRIS"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_IRIS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IRIS"]) != "")
                {
                    //txtIris_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IRIS"]);
                    //txtIris_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IRIS"]);

                    EOPE_R_IRIS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IRIS"]);
                    EOPE_L_IRIS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IRIS"]);

                    trIris.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_LENS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LENS"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_LENS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LENS"]) != "")
                {
                    //txtLens_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LENS"]);
                    //txtLens_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LENS"]);

                    EOPE_R_LENS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LENS"]);
                    EOPE_L_LENS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LENS"]);

                    trLens.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_VITREOUS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_VITREOUS"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_VITREOUS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_VITREOUS"]) != "")
                {
                    //txtVitreous_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_VITREOUS"]);
                    //txtVitreous_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_VITREOUS"]);

                    EOPE_R_VITREOUS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_VITREOUS"]);
                    EOPE_L_VITREOUS = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_VITREOUS"]);

                    trVitreous.Visible = true;
                }
                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_OPTICNERVE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_OPTICNERVE"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_OPTICNERVE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_OPTICNERVE"]) != "")
                {
                    //txtOpticNerve_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_OPTICNERVE"]);
                    //txtOpticNerve_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_OPTICNERVE"]);


                    EOPE_R_OPTICNERVE = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_OPTICNERVE"]);
                    EOPE_L_OPTICNERVE = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_OPTICNERVE"]);


                    trOpticNerve.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_CHOROID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_CHOROID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID"]) != "")
                {
                    //txtChoroid_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID"]);
                    //txtChoroid_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID"]);

                    EOPE_R_CHOROID = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID"]);
                    EOPE_L_CHOROID = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID"]);

                    trChoroid.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_GONIOSCOPY") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_GONIOSCOPY"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_GONIOSCOPY") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_GONIOSCOPY"]) != "")
                {
                    //txtGonioscopy_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_GONIOSCOPY"]);
                    //txtGonioscopy_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_GONIOSCOPY"]);

                    EOPE_R_GONIOSCOPY = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_GONIOSCOPY"]);
                    EOPE_L_GONIOSCOPY = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_GONIOSCOPY"]);


                    trGonioscopy.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_R_PUPIL") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PUPIL"]) != "" || DS.Tables[0].Rows[0].IsNull("EOPE_L_PUPIL") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PUPIL"]) != "")
                {
                    //txtPupil_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PUPIL"]);
                    //txtPupil_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PUPIL"]);

                    EOPE_R_PUPIL = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PUPIL"]);
                    EOPE_L_PUPIL = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PUPIL"]);

                    trPupil.Visible = true;
                }


                EOPE_R_NPDR = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_NPDR"]);
                EOPE_R_CSME = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CSME"]);
                EOPE_L_NPDR = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_NPDR"]);
                EOPE_L_CSME = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CSME"]);









                if (DS.Tables[0].Rows[0].IsNull("EOPE_OCT") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_OCT"]) != "")
                {

                    EOPE_OCT = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_OCT"]);
                    trOCT.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_FA") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_FA"]) != "")
                {
                    EOPE_FA = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_FA"]);
                    trFA.Visible = true;
                }

                if (DS.Tables[0].Rows[0].IsNull("EOPE_FIELD") == false && Convert.ToString(DS.Tables[0].Rows[0]["EOPE_FIELD"]) != "")
                {
                    EOPE_FIELD = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_FIELD"]);
                    trField1.Visible = true;
                }






            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    BindData();
                     if(GlobalValues.FileDescription.ToUpper() == "MAMPILLY")
                    {
                        lblNPDR.Visible = false;
                        lblCSME.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "       Ophthalmology.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }
    }
}