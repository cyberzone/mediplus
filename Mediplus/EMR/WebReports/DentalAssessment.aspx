﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="DentalAssessment.aspx.cs" Inherits="Mediplus.EMR.WebReports.DentalAssessment" %>

<%@ Register Src="~/EMR/WebReports/PatientReportHeader.ascx" TagPrefix="UC1" TagName="PatientReportHeader" %>
<%@ Register Src="~/EMR/WebReports/PainScore.ascx" TagPrefix="UC1" TagName="PainScore" %>
<%@ Register Src="~/EMR/WebReports/Prescriptions.ascx" TagPrefix="UC1" TagName="Prescriptions" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        /*table, th, td
         {
             border: 1px solid #dcdcdc;
             height: 25px;
         }*/

        .BoldStyle
        {
            font-weight: bold;
        }

        .ImageStyle
        {
            height: 200px;
            width: 150px;
        }

        .BorderDottedStyle
        {
            border-bottom-style: dotted;
            border-width: thin;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
    <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY" />

    <table style="width: 100%; text-align: center; vertical-align: top;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img style="padding: 1px; border: none;" src="images/SummerLand_Logo.jpeg" />
            </td>
        </tr>
    </table>
    <table width="100%" style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td class="ReportCaption BoldStyle">Patient #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%=EPM_PT_ID %><br />
                Patient Name&nbsp;:&nbsp;<%=EPM_PT_NAME %><br />
                Sex&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%=HPM_SEX %><br />
            </td>
            <td>
                <span class="ReportCaption" style="font-family: Segoe UI, Arial, Helvetica, sans-serif;">
                    <h1>Dental Assessment </h1>
                </span>
            </td>

            <td class="ReportCaption BoldStyle">DATE : <%=EPM_DATE %>
            </td>
        </tr>
    </table>

    <br />

    <UC1:PainScore ID="PNLPainScore" runat="server"></UC1:PainScore>
    <br />
    <table width="100%">
        <tr>
            <td style="vertical-align: top;">


                <table width="100%">
                    <tr>
                        <td class="ReportCaption BoldStyle" style="width: 200px;">Chief Dental Complaints :&nbsp;
                        </td>
                        <td class="ReportCaption">
                            <%=EPH_CC %> 
                        </td>
                    </tr>

                </table>
                <br />
                <table cellpadding="3" cellspacing="3" style="width: 100%;">
                    <tr>
                        <td class="ReportCaption">
                            <span class="ReportCaption BoldStyle">MEDICAL HISTORY (ALL MANDATORY)</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption" style="vertical-align: top; width: 50%;"><span style="font-weight: bold">Referred By:</span>&nbsp;
                         
                            <%=HPM_REF_DR_NAME %> 
                            
                        </td>

                        <td colspan="2" class="ReportCaption" style="vertical-align: top; width: 50%;"><span style="font-weight: bold">Insurance / Health Card:</span>&nbsp;
                         
                            <%=HPM_INS_COMP_NAME %>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption" style="vertical-align: top;"><span style="font-weight: bold">Height:</span>&nbsp;
                         
                            <%=EPV_HEIGHT %>&nbsp;
                            <span style="font-weight: bold">Weight: </span>&nbsp;
                             <%=EPV_WEIGHT %>

                        </td>

                        <td colspan="2" class="ReportCaption" style="vertical-align: top;"><span style="font-weight: bold">Blood Pressure:</span>&nbsp;
                        
                            <%=EPV_BP_SYSTOLIC %> /
                            <%=EPV_BP_DIASTOLIC %>

                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption" style="vertical-align: top;"><span style="font-weight: bold">Marital Status:</span>&nbsp;
                        
                            <%=HPM_MARITAL %>
                        </td>

                        <td colspan="2" class="ReportCaption" style="vertical-align: top;"><span style="font-weight: bold">Occupation:</span>&nbsp;
                        
                            <%=HPM_OCCUPATION %>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption" style="vertical-align: top;"><span style="font-weight: bold">When was your last check-up:</span>&nbsp;
                            <%=HPM_PREV_VISIT %>
                        </td>

                        <td colspan="2" class="ReportCaption" style="vertical-align: top;"><span style="font-weight: bold">Are you under the care of any physician:</span>&nbsp;

                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption BoldStyle" style="vertical-align: top;"><span style="font-weight: bold">Serious Illness:</span>&nbsp;
                              &nbsp;&nbsp;&nbsp;<span class="ReportCaption"><%=HPI %></span>
                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" style="vertical-align: top;"><span style="font-weight: bold">Operation in the past:</span>&nbsp;
                              &nbsp;&nbsp;&nbsp; <span class="ReportCaption"><%=HIST_SURGICAL %></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="ReportCaption BoldStyle" style="vertical-align: top;"><span style="font-weight: bold">Current Medications:</span>&nbsp;
                              
                              &nbsp;&nbsp;&nbsp; <span class="ReportCaption"><%=Medications %></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Allergy to Penicillin
                              
                        </td>
                        <td style="vertical-align: top;">
                            <input type="radio" id="radPenicillinYes" name="radPenicillinYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radPenicillinNo" name="radPenicillinYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" style="vertical-align: top;"><span style="font-weight: bold">Other Allergies:</span>&nbsp;
                              &nbsp;&nbsp;&nbsp; <span class="ReportCaption"><%=ALLERGY %></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Diabetes  
                        </td>
                        <td>
                            <input type="radio" id="radDiabYes" name="radDiabYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radDiabNo" name="radDiabYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Teuberculosis
                        </td>
                        <td>
                            <input type="radio" id="radTeuberculosisYes" name="radTeuberculosisYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radTeuberculosisNo" name="radTeuberculosisNo" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">High Blood Pressure  
                        </td>
                        <td>
                            <input type="radio" id="radBPYes" name="radBPYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBPNo" name="radBPYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Hepatitis/Jaundice
                        </td>
                        <td>
                            <input type="radio" id="radHepatitisYes" name="radHepatitisYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radHepatitisNo" name="radHepatitisYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Heart Attack/ Chest Pain/ Angina
                        </td>
                        <td>
                            <input type="radio" id="radHeartAttackYes" name="radHeartAttackYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radHeartAttackNo" name="radHeartAttackYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Steroid Therapy
                        </td>
                        <td>
                            <input type="radio" id="radSteroidTherapyYes" name="radSteroidTherapyYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radSteroidTherapyNo" name="radSteroidTherapyYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Heart murmur
                        </td>
                        <td>
                            <input type="radio" id="radHeartmurmurYes" name="radHeartmurmurYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radHeartmurmurNo" name="radHeartmurmurYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Epilespsy/Fainting Attacks
                        </td>
                        <td>
                            <input type="radio" id="radEpilespsyYes" name="radEpilespsyYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radEpilespsyNo" name="radEpilespsyYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>


                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Stroke
                        </td>
                        <td>
                            <input type="radio" id="radStrokeYes" name="radStrokeYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radStrokeNo" name="radStrokeYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Rheumatic Fever
                        </td>
                        <td>
                            <input type="radio" id="radRheumaticFeverYes" name="radRheumaticFeverYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radRheumaticFeverNo" name="radRheumaticFeverNo" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Bleeding Disease/Disorder
                        </td>
                        <td>
                            <input type="radio" id="radBleedingYes" name="radBleedingYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBleedingNo" name="radBleedingYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                        <td class="ReportCaption BoldStyle" style="vertical-align: top;">Asthma
                        </td>
                        <td>
                            <input type="radio" id="radAsthmaYes" name="radAsthmaYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radAsthmaNo" name="radAsthmaYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>

                    <tr>

                        <td colspan="4" class="ReportCaption BoldStyle" style="vertical-align: top;">Other Illnesses :  
                             <span class="ReportCaption"><%=OtherIllnesses %></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption BoldStyle"><u>Social History</u>
                            ChewTobacco: 
                             <input type="radio" id="radChewTobaccoYes" name="radChewTobaccoYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radChewTobaccoNo" name="radChewTobaccoYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle">Smoker: 
                             <input type="radio" id="radSmokerYes" name="radSmokerYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radSmokerNo" name="radSmokerYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                            &nbsp;&nbsp;&nbsp;&nbsp; Alcohol: 
                             <input type="radio" id="radAlcoholYes" name="radAlcoholYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radAlcoholNo" name="radAlcoholYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="ReportCaption BoldStyle"><u>FOR WOMAN:</u>
                            Pregnant
                             <input type="radio" id="radPregnantYes" name="radPregnantYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radPregnantNo" name="radPregnantYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle">Lactating: 
                             <input type="radio" id="radLactatingYes" name="radLactatingYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radLactatingNo" name="radLactatingYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>

                </table>

               <table cellpadding="3" cellspacing="3" style="width: 100%;">

                    <tr>
                        <td colspan="2" class="ReportCaption BoldStyle" style="width:50%">PSYCHOLOGICAL STATUS:
                        </td>
                        <td colspan="2"  style="width:50%">

                        </td>
                    </tr>
                    <tr>
                        <td  class="ReportCaption BoldStyle" >Do you visit your Dentist regularly
                            </td>
                         <td> 
                             <input type="radio" id="radDentistVisitRegYes" name="radDentistVisitRegYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radDentistVisitRegNo" name="radDentistVisitRegYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" >Notes:
                             <label id="lblDentistVisitReg" runat="server" class="label"></label>

                        </td>
                    </tr>
                    <tr>
                        <td  class="ReportCaption BoldStyle" >Have you had any trauma to the teeth
                            </td>
                         <td> 
                             <input type="radio" id="radTraumaTeethYes" name="radTraumaTeethYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radTraumaTeethNo" name="radTraumaTeethYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" >Notes:
                             <label id="lblTraumaTeeth" runat="server" class="label"></label>

                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle"> Complaints of teeth sensitivity
                            </td>
                         <td> 
                             <input type="radio" id="radSensitivityYes" name="radSensitivityYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radSensitivityNo" name="radSensitivityYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" >Notes:
                             <label id="lblSensitivity" runat="server" class="label"></label>

                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" >History of swelling (jaws/soft tissue of face)
                        </td>
                        <td>
                            <input type="radio" id="radSwellingYes" name="radSwellingYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radSwellingNo" name="radSwellingYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" >Notes:
                             <label id="lblSwelling" runat="server" class="label"></label>

                        </td>
                    </tr>
                    <tr>
                        <td class="ReportCaption BoldStyle" >Past root canal Treatment
                        </td>
                        <td>
                            <input type="radio" id="radRootCanalYes" name="radRootCanalYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radRootCanalNo" name="radRootCanalYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" >Notes:
                             <label id="lblRootCanal" runat="server" class="label"></label>

                        </td>
                    </tr>

                    <tr>
                        <td class="ReportCaption BoldStyle" >Past orthodontic treatment
                        </td>
                        <td>
                            <input type="radio" id="radPastOrthTreatYes" name="radPastOrthTreatYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radPastOrthTreatNo" name="radPastOrthTreatYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td colspan="2" class="ReportCaption BoldStyle" >Notes:
                             <label id="lblPastOrthTreat" runat="server" class="label"></label>

                        </td>
                    </tr>
                   <tr>
                        <td class="ReportCaption BoldStyle" >Bleeding gums
                        </td>
                        <td>
                            <input type="radio" id="radBleedinggumsYes" name="radBleedinggumsYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBleedinggumsNo" name="radBleedinggumsYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Bad Taste
                        </td>
                        <td>
                            <input type="radio" id="radBadTasteYes" name="radBadTasteYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBadTasteNo" name="radBadTasteYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>

                   <tr>
                        <td class="ReportCaption BoldStyle" >Bad Breath
                        </td>
                        <td>
                            <input type="radio" id="radBadBreathYes" name="radBadBreathYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBadBreathNo" name="radBadBreathYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Food Wedding
                        </td>
                        <td>
                            <input type="radio" id="radFoodWeddingYes" name="radFoodWeddingYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radFoodWeddingNo" name="radFoodWeddingYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>

                   <tr>
                        <td class="ReportCaption BoldStyle" >Gum Boils
                        </td>
                        <td>
                            <input type="radio" id="radGumBoilsYes" name="radGumBoils" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radGumBoilsNo" name="radGumBoils" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Frequent Ulcers
                        </td>
                        <td>
                            <input type="radio" id="radFrequentUlcersYes" name="radFrequentUlcers" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radFrequentUlcersNo" name="radFrequentUlcers" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>

                    <tr>
                        <td class="ReportCaption BoldStyle" >Mouth Breathr
                        </td>
                        <td>
                            <input type="radio" id="radMouthBreathrYes" name="radMouthBreathr" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radMouthBreathrNo" name="radMouthBreathr" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Bruxer
                        </td>
                        <td>
                            <input type="radio" id="radBruxerYes" name="radBruxer" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBruxerNo" name="radBruxer" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>

                   
                    <tr>
                        <td class="ReportCaption BoldStyle" >Clicking of joints
                        </td>
                        <td>
                            <input type="radio" id="radClickingjointsYes" name="radClickingjointsYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radClickingjointsNo" name="radClickingjointsYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Difficulty of chewing
                        </td>
                        <td>
                            <input type="radio" id="radDifficultychewingYes" name="radDifficultychewing" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radDifficultychewingNo" name="radDifficultychewing" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>

                   <tr>
                        <td class="ReportCaption BoldStyle" >Difficulty in mouth opening
                        </td>
                        <td>
                            <input type="radio" id="radDiffMouthOpenYes" name="radDiffMouthOpenYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radDiffMouthOpenNo" name="radDiffMouthOpenYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Wisdom teeth problems
                        </td>
                        <td>
                            <input type="radio" id="radWisdomTeethProbYes" name="radWisdomTeethProbYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radWisdomTeethProbNo" name="radWisdomTeethProbYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>

                    <tr>
                        <td class="ReportCaption BoldStyle" >Prolong bleeding after extractions
                        </td>
                        <td>
                            <input type="radio" id="radProbBleedYes" name="radProbBleedYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radProbBleedNo" name="radProbBleedYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                        <td   class="ReportCaption BoldStyle" >Brushing habits/ flossing habits
                        </td>
                        <td>
                            <input type="radio" id="radBrushinghabitsYes" name="radBrushinghabitsYes" runat="server" disabled="disabled" /><span class="ReportCaption">Yes</span>
                            <input type="radio" id="radBrushinghabitsNo" name="radBrushinghabitsYes" runat="server" disabled="disabled" /><span class="ReportCaption">No</span>

                        </td>
                    </tr>
                </table>
            </td>


        </tr>
    </table>
</asp:Content>
