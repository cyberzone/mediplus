﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.WebReports
{
    public partial class ClinicalNotesAll : System.Web.UI.Page
    {
        public string EMR_ID = "", DR_ID = "", EMR_DATE = "", EMR_PT_ID="";
        public string strClinicalNotes = "";
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

        void BindClinicalNotes()
        {
            string Criteria = " 1=1  AND ECN_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND ECN_EMR_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";
            Criteria += " AND ECN_PT_ID =  '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            DataSet DS = new DataSet();
            EMR_ClinicalNotes obj = new EMR_ClinicalNotes();
            DS = obj.EMRClinicalNotesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                lblClinicalNotes1.Text = "";

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["ECN_ID"]) == "1")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes1.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));

                        }

                    }

                    if (Convert.ToString(DR["ECN_ID"]) == "2")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes2.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));

                        }

                    }


                    if (Convert.ToString(DR["ECN_ID"]) == "3")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes3.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));

                        }

                    }
                    if (Convert.ToString(DR["ECN_ID"]) == "4")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes4.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));

                        }

                    }

                    if (Convert.ToString(DR["ECN_ID"]) == "5")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes5.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));

                        }

                    }


                }


            }
            else
            {

            }
        }

        void BindClinicalNotesAll()
        {
            string Criteria = " 1=1  AND ECN_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND ECN_EMR_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";
            Criteria += " AND ECN_PT_ID =  '" + EMR_PT_ID + "'";

            DataSet DS = new DataSet();
            EMR_ClinicalNotes obj = new EMR_ClinicalNotes();
            DS = obj.EMRClinicalNotesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                lblClinicalNotes1.Text = "";

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    HtmlGenericControl trHe1 = new HtmlGenericControl("tr");
                    HtmlGenericControl tdHe1 = new HtmlGenericControl("td");

                    Label lbl = new Label();
                    lbl.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                    lbl.CssClass = "label";
                                       

                    Literal ltllblGap = new Literal();
                    ltllblGap.Text = "<br />";


                    tdHe1.Controls.Add(lbl);
                    tdHe1.Controls.Add(ltllblGap);
                    trHe1.Controls.Add(tdHe1);


                    PlaceHolder1.Controls.Add(trHe1);
                }


            }
            else
            {

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
                pnlPatientReportHeader.EMR_ID = EMR_ID;


                // BindClinicalNotes();
                BindClinicalNotesAll();
                pnlDoctorSignature.DR_ID = DR_ID;
                pnlDoctorSignature.EMR_DATE = EMR_DATE;

            }
        }

         
    }
}