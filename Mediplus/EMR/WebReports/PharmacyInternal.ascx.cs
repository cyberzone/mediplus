﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
 

namespace Mediplus.EMR.WebReports
{
    public partial class PharmacyInternal : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }

        void BindPharmacyInternal()
        {

            DataSet ds = new DataSet();
            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();

            string Criteria = " 1=1 ";
            Criteria += " AND EPPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPPI_ID  ='" + EMR_ID + "'";


            ds = objCom.PharmacyInternalGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacyHistory.DataSource = ds;
                gvPharmacyHistory.DataBind();

            }
            else
            {
                gvPharmacyHistory.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);

                BindPharmacyInternal();
            }
        }
    }
}