﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrescriptionsAudit.ascx.cs" Inherits="Mediplus.EMR.WebReports.PrescriptionsAudit" %>
<table width="100%">
      <tr>
         <td class="ReportCaption BoldStyle"  >
            Prescription
          </td>
      </tr>
    <tr>
        <td class="ReportCaption  BoldStyle">
           <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"  
                EnableModelValidation="True" Width="100%" >
                <HeaderStyle CssClass="ReportCaption" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
                <RowStyle CssClass="ReportCaption"  HorizontalAlign="Center" />

                <Columns>
                    <asp:TemplateField HeaderText="Drug Code"  HeaderStyle-Width="100px"  >
                        <ItemTemplate>
                          
                        <%# Eval("EPP_PHY_CODE")  %>

                        </ItemTemplate>

                    </asp:TemplateField>
                     
                    <asp:TemplateField HeaderText="Description"  >
                        <ItemTemplate>
                      
                        <%# Convert.ToString(Eval("EPP_PHY_NAME")).Length > 100 ? Convert.ToString(Eval("EPP_PHY_NAME")).Substring(0,100) : Eval("EPP_PHY_NAME") %>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dosage/Time/Route"  HeaderStyle-Width="100px"  >
                        <ItemTemplate>
                          
                      <%# Eval("EPP_DOSAGE1") %> &nbsp;
                      <%# Eval("EPP_DOSAGE") %> &nbsp;
                      <%# Eval("EPP_ROUTEDesc") %> &nbsp;

                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="100px" >
                        <ItemTemplate>

                               <%# Eval("EPP_DURATION") %> &nbsp; <%# Eval("EPP_DURATION_TYPEDesc") %> 

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="50px" >
                        <ItemTemplate>

                             <%# Eval("EPP_QTY") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Refil" Visible="false">
                        <ItemTemplate>

                             <%# Eval("EPP_REFILL") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks" >
                        <ItemTemplate>

                             <%# Eval("EPP_REMARKS") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Taken" Visible="false">
                        <ItemTemplate>

                             <%# Eval("EPP_TAKEN") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Taken"  >
                        <ItemTemplate>

                             <%# Eval("EPP_TIMEDesc") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:BoundField HeaderText="User" DataField="AUDIT_USER_ID" HeaderStyle-Width="100px" />
                    <asp:BoundField HeaderText="Date" DataField="AUDIT_DATEDesc" HeaderStyle-Width="100px" />
                    <asp:BoundField HeaderText="Reason" DataField="AUDIT_REASON" HeaderStyle-Width="200px" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>