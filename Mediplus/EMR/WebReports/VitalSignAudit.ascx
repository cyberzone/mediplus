﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VitalSignAudit.ascx.cs" Inherits="Mediplus.EMR.WebReports.VitalSignAudit" %>

<div id="VitalSignReportDiv" runat="server" >
  <span class="ReportCaption  BoldStyle" > Vital Sign</span>
	<table class="table grid spacy family-illness"  border="1" style="width:100%">
		<tr style="height:25px;">
			<td class="ReportCaption" style="font-weight:bold;">Weight :</td>
			<td class="ReportCaption"><span ><%= Weight %></span>Kg</td>
			<td class="ReportCaption" style="font-weight:bold;">Height :</td>
			<td class="ReportCaption"><span c><%= Height %></span> cm</td>
			<td class="ReportCaption" style="font-weight:bold;">BMI :</td>
			<td class="ReportCaption"><span ><%= BMI %></span></td>
			<td class="ReportCaption" style="font-weight:bold;">Temp :</td>
			<td class="ReportCaption"><span ><%= Temperature %></span> F</td>
			<td class="ReportCaption" style="font-weight:bold;">Pulse :</td>
			<td class="ReportCaption"><span><%= Pulse %></span> Beats/Min</td>
		</tr>
		<tr style="background-color:#f0f0f0;height:25px;">
			<td class="ReportCaption" style="font-weight:bold;">Respiration</td>
			<td class="ReportCaption"><span ><%= Respiration %></span> /m</td>
			<td class="ReportCaption" style="font-weight:bold;">BP: Systolic :</td>
			<td class="ReportCaption"><span ><%= BpSystolic %></span> mm/Hg</td>
			<td class="ReportCaption" style="font-weight:bold;">BP: Diastolic :</td>
			<td class="ReportCaption"><span ><%= BpDiastolic %></span> mm/Hg</td>
			<td class="ReportCaption">SPO2</td>
			<td class="ReportCaption"><span class="ReportCaption"><%= Sp02 %></span> %</td>
			<td></td>
			<td></td>
		</tr>
		<tr style="height:25px;">
			<td class="ReportCaption" style="font-weight:bold;" colspan="2">Capilory Blood Sugar :</td>
			<td></td>
			<td class="ReportCaption" colspan="2"><span ><%= CapillaryBloodSugar %></span> mg/dL</td>
			<td></td>
			<td class="ReportCaption" style="font-weight:bold;" colspan="2">Jaundice Meter :</td>
			<td ></td>
			<td class="ReportCaption"><span ><%= JaundiceMeter %></span> mg/dL</td>
		</tr>
		<tr style="background-color:#f0f0f0;height:25px;">
			<td class="ReportCaption" style="font-weight:bold;" >Abdominal Grith :</td>
			<td class="ReportCaption"><span ><%=  AbdominalGirth %></span></td>
			<td class="ReportCaption">cm</td>
			<td class="ReportCaption" style="font-weight:bold;">Head Circumfrence :</td>
			<td class="ReportCaption"><span ><%= HeadCircumference %></span></td>
			<td class="ReportCaption">cm</td>
			<td class="ReportCaption" style="font-weight:bold;">Chest Circumfrence :</td>
			<td class="ReportCaption"><span><%= ChestCircumference %></span> cm</td>
			<td class="ReportCaption">&nbsp;</td>
			<td class="ReportCaption"></td>
		</tr>
		<tr style="height:25px;">
			<td class="ReportCaption" style="font-weight:bold;">ECG :</td>
			<td class="ReportCaption"  colspan="2" ><%= EcgReport %></td>
            <td class="ReportCaption" style="font-weight:bold;">User :</td>
			<td class="ReportCaption"  ><%= AUDIT_USER_ID %></td>
            <td class="ReportCaption" style="font-weight:bold;">Date :</td>
			<td class="ReportCaption"  ><%= AUDIT_DATEDesc %></td>
             <td class="ReportCaption" style="font-weight:bold;">Reason :</td>
			<td class="ReportCaption"  colspan="2" ><%= AUDIT_REASON %></td>
		</tr>
		<tr style="background-color:#f0f0f0;height:25px;">
			<td class="ReportCaption" style="font-weight:bold;">Others :</td>
			<td  class="ReportCaption"colspan="9" ><%= Others %></td>
		</tr>
        <tr id="trFemale" runat="server" style="background-color:#f0f0f0;height:25px;" visible="false">
			<td class="ReportCaption" style="font-weight:bold;">LMP Date :</td>
			<td  class="ReportCaption" ><%= LMPDate %></td>
            <td class="ReportCaption" style="font-weight:bold;">Pregnant :</td>
			<td  class="ReportCaption" colspan="7" ><%= Pregnant %></td>

		</tr>
	</table>
</div> 