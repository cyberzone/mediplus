﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;


namespace Mediplus.EMR.WebReports
{
    public partial class PatientHeader : System.Web.UI.UserControl
    {

        public   string BranchName = "", FullName = "", Nationality = "", Address = "", Age = "", MobileNo = "", Sex = "", EmiratesID = "";
        public  string CompanyName = "", PolicyNo = "", PolicyType = "", ExpiryOn = "", PatientType = "", PTCompany="";


        #region Methods

        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
        EMR_PTPharmacy objPhar = new EMR_PTPharmacy();

        public string FileNo { set; get; }

        void BindPatientMaster()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  AND hPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + FileNo + "'";
            DS = objCom.PatientMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {



                //if (DS.Tables[0].Rows[0].IsNull("EPM_DEP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]) != "")
                //    lblDept.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DEP_NAME"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]) != "")
                //    lblEmrDate.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                //    lblEmrStTime.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                //    lblEmrEndTime.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_BRANCH_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_BRANCH_ID"]) != "")
                    BranchName = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_BRANCH_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]) != "")
                    FileNo = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]));


                if (DS.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(DS.Tables[0].Rows[0]["FullName"]) != "")
                    FullName = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["FullName"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                    Nationality = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_NATIONALITY"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_ADDR") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_ADDR"]) != "")
                   Address = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_ADDR"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_AGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE"]) != "")
                {

                    Age = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE"])) + " ";
                    Age += CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE"])) + " ";
                    Age += CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE1"])) + " ";
                    Age += CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE1"])) + " ";


                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                    MobileNo = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]));

                //if (DS.Tables[0].Rows[0].IsNull("EPM_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]) != "")
                //    lblEPMID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]) != "")
                    Sex = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                    EmiratesID = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]));

                
                if (DS.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                  CompanyName = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]));


                if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                    PolicyNo = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                    PolicyType = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_CONT_EXPDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATE"]) != "")
                    ExpiryOn = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]));

                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "")
                    PTCompany = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);




                //if (DS.Tables[0].Rows[0].IsNull("EPM_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]) != "")
                //    lblDrName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]));
                //// lblDrName1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_NAME"]));
                //lblDrCode.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]));


                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) != "")
                    PatientType = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]));
                



                //if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                //    lblTreatmentPlan.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                //else
                //    lblTreatmentPlan.Text = "";

                //if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                //    lblFollowupNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]));
                //else
                //    lblFollowupNotes.Text = "";



            }


        }



        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindPatientMaster();

            }
        }
    }
}