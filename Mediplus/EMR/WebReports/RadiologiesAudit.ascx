﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RadiologiesAudit.ascx.cs" Inherits="Mediplus.EMR.WebReports.RadiologiesAudit" %>

<table style="width: 100%;">
    <tr>
        <td class="ReportCaption BoldStyle">Radiology  
        </td>
    </tr>
    <tr>
        <td class="ReportCaption  BoldStyle">
            <asp:GridView ID="gvRadRequest" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="ReportCaption" Font-Bold="true" />
                <RowStyle CssClass="ReportCaption" />

                <Columns>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <asp:Label ID="Label31" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPR_RAD_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Label ID="Label32" CssClass="ReportCaption" runat="server" Text='<%# Bind("EPR_RAD_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:BoundField HeaderText="User" DataField="AUDIT_USER_ID" HeaderStyle-Width="100px" />
                    <asp:BoundField HeaderText="Date" DataField="AUDIT_DATEDesc" HeaderStyle-Width="100px" />
                    <asp:BoundField HeaderText="Reason" DataField="AUDIT_REASON" HeaderStyle-Width="200px" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>