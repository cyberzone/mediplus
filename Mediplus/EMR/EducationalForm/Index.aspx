﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.EducationalForm.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/EMR/EducationalForm/EducationalFormControl.ascx" TagPrefix="UC1" TagName="EducationalFormControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
   
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.5em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>

      <script type="text/javascript">
          function ShowMessage() {
              $("#myMessage").show();
              setTimeout(function () {
                  var selectedEffect = 'blind';
                  var options = {};
                  $("#myMessage").hide();
              }, 2000);
              return true;
          }

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
      <div id="divAuditDtls" runat="server" visible="false"  style="text-align: right; width: 80%;" class="lblCaption1">
        Creaded By :
                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Modified By
                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
    </div>

    <div style="padding-left:60%;width:80%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
        </div>
      <table width="80%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Educational Form</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

     <table class="table spacy" style="width:80%;">                
                <tr>
                    <td><label for="Language" class="lblCaption1" >Patient's language</label>
                        <input type="text" name="Language" id="Language"  runat="server" />
                    </td>
                    <td><label class="lblCaption1" >Patient's literacy</label>
                        <input type="checkbox" name="Read" id="Read" value="1"   runat="server" class="lblCaption1"  /><span class="lblCaption1" > Read </span>
                        <input type="checkbox" name="Write" id="Write" value="1"  runat="server"  class="lblCaption1" /> <span class="lblCaption1" >  Write </span>
                        <input type="checkbox" name="Speak" id="Speak" value="1"  runat="server" class="lblCaption1"  /> <span class="lblCaption1" >  Speak </span>
                    </td>
                    <td><label for="ReligionBelief" class="lblCaption1" >Religion/Belief</label>
                        <input type="text" name="ReligionBelief" id="ReligionBelief"  runat="server" />
                    </td>
                </tr>
            </table>

    <div style="width:80%">
     <UC1:EducationalFormControl ID="EducationalFormControlPart1" runat="server"  SegmentType="EDU_PART1" SegmentHeader="Part I"  />
     <UC1:EducationalFormControl ID="EducationalFormControlPart2" runat="server"  SegmentType="EDU_PART2"  SegmentHeader="Part II" />
     <UC1:EducationalFormControl ID="EducationalFormControlPart3" runat="server"  SegmentType="EDU_PART3" SegmentHeader="Part III"  />
    </div>
    <br />
<br />
</asp:Content>
