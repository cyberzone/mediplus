﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.EducationalForm
{
    public partial class EducationalFormControl : System.Web.UI.UserControl
    {
        private static SegmentBAL _segmentBAL = new SegmentBAL();

        EMRSBAL objEMRS = new EMRSBAL();

        public string SegmentType { set; get; }
        public string SegmentHeader { set; get; }

        #region Methods


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        public DataSet GetSegmentMaster()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " and esm_type='" + SegmentType + "'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        public void SegmentVisitDtlsALLGet(string Criteria)
        {

            DataSet DS = new DataSet();
            Criteria += " AND  ESVD_TYPE ='" + SegmentType + "'";
            if(   EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
            {
            Criteria += " AND (  ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "' OR ( ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' and   ESVD_SUBTYPE='IEN' )) ";
            }
            else
            {
                Criteria += " AND    ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            }
            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }


        void fnSave(string Type, string SubType, string FieldName, string Value, string ValueYes)
        {

            SegmentBAL objSeg = new SegmentBAL();
            objSeg.BranchID = Convert.ToString(Session["Branch_ID"]);

            if (Type.ToUpper() == "EDU_PART1" && SubType.ToUpper() == "IEN" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
            {
                objSeg.EMRID = Convert.ToString(Session["EMR_PT_ID"]);
            }
            else
            {
                objSeg.EMRID = Convert.ToString(Session["EMR_ID"]);
            }
            objSeg.Type = Type;
            objSeg.SubType = SubType;

            objSeg.FieldName = FieldName;
            objSeg.Value = Value;
            objSeg.ValueYes = ValueYes;
            objSeg.TemplateCode = "";
            objSeg.EMRSegmentVisitDtlsAdd();
        }


        void DeleteSegmentDtlsByEMR_ID()
        {
            SegmentBAL objSeg = new SegmentBAL();

            string Criteria = " 1=1 ";
            Criteria += " AND  ESVD_BRANCH_ID= '" + Convert.ToString(Session["Branch_ID"]) + "' AND  ESVD_ID= '" + Convert.ToString(Session["EMR_ID"]) + "' AND  ESVD_TYPE= '" + SegmentType + "' ";
            objSeg.EMRSegmentVisitDtlsDelete(Criteria);
        }

        void DeleteSegmentDtlsByPatientID()
        {
            SegmentBAL objSeg = new SegmentBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  ESVD_BRANCH_ID= '" + Convert.ToString(Session["Branch_ID"]) + "' AND  ESVD_ID= '" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  ESVD_TYPE= '" + SegmentType + "' ";
            Criteria += " AND  ESVD_SUBTYPE='IEN' ";

            objSeg.EMRSegmentVisitDtlsDelete(Criteria);
        }


        public void SaveEducationalForm()
        {
            Int32 i = 1;


            if (SegmentType.ToUpper() == "EDU_PART1" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
            {
                DeleteSegmentDtlsByPatientID();
            }
            else
            {
                DeleteSegmentDtlsByEMR_ID();
            }
           

            DataSet DS = new DataSet();
            DS = GetSegmentMaster();
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 and (EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID LIKE '%" + Convert.ToString(Session["HPV_DEP_NAME"]) + "%') ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";

                DS1 = SegmentTemplatesGet(Criteria1);


                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string Value = "", ValueYes = "N";
                    TextBox txt = (TextBox)PresentingProblems.FindControl("txt" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                    if (txt != null)
                    {

                        Value = txt.Text.Trim();

                    }


                    CheckBox chk = (CheckBox)PresentingProblems.FindControl("chk" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]));
                    if (chk != null)
                    {
                        if (chk.Checked == true)
                        {
                            ValueYes = "Y";
                        }
                    }

                    if (ValueYes == "Y" || Value != "")
                    {
                        fnSave(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR1["EST_SUBTYPE"]), Convert.ToString(DR1["EST_FIELDNAME"]), Value, ValueYes);
                    }

                    i = i + 1;
                }


            }


        }


        public void CreateSegCtrls()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();
            Label lblHeader = new Label();
            lblHeader.Text = SegmentHeader;
            lblHeader.CssClass = "lblCaption1";
            PresentingProblems.Controls.Add(lblHeader);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";

                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";

                DS1 = SegmentTemplatesGet(Criteria1);



                Panel myFieldSet = new Panel();
                myFieldSet.GroupingText = Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]); //= ; 
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                Int32 i = 1;
                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {

                    string Criteria2 = "1=1 ";
                    if (Convert.ToString(DR1["EST_TYPE"]).ToUpper() == "EDU_PART1" && Convert.ToString(DR1["EST_SUBTYPE"]).ToUpper() == "IEN" && SegmentType.ToUpper() == "EDU_PART1" && EDU_PART1_SAVE_PT_ID.Value.ToUpper() == "TRUE")
                    {
                        Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND ESVD_TYPE='EDU_PART1' AND ESVD_SUBTYPE='IEN' ";
                    }
                    else
                    {
                        Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                    }

                    Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                    string TemplateType = "";
                    TemplateType = Convert.ToString(DR1["EST_TYPE"]) + "|" + Convert.ToString(DR1["EST_SUBTYPE"]) + "|" + Convert.ToString(DR1["EST_FIELDNAME"]);

                    string strValue = "", strValueYes = "";

                    SegmentVisitDtlsGet(Criteria2, out  strValue, out  strValueYes);


                    //string _pointValue;
                    //GetEMRSDatas(Convert.ToString(DR1["EST_SUBTYPE"]), out  _checked, out  _pointValue);



                    //HtmlGenericControl tr = new HtmlGenericControl("tr");

                    //HtmlGenericControl td1 = new HtmlGenericControl("td");




                    CheckBox chk = new CheckBox();
                    chk.ID = "chk" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    chk.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                    chk.CssClass = "lblCaption1";
                    chk.Width = 200;
                    if (strValueYes != "")
                    {
                        if (strValueYes.ToUpper() == "N")
                            chk.Checked = false;
                        if (strValueYes.ToUpper() == "Y")
                            chk.Checked = true;

                    }


                    TextBox txt = new TextBox();
                    txt.ID = "txt" + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                    txt.Width = 135;




                    myFieldSet.Controls.Add(chk);
                    if (Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]).ToUpper().Equals("OTHERS"))
                    {
                        chk.Width = 60;
                        txt.Text = strValue;
                        myFieldSet.Controls.Add(txt);
                    }

                    if (i % 4 == 0)
                    {
                        Literal lt = new Literal();
                        lt.Text = "<br /><br />";
                        myFieldSet.Controls.Add(lt);
                    }

                    //td1.Controls.Add(myFieldSet);


                    //tr.Controls.Add(td1);


                    PresentingProblems.Controls.Add(myFieldSet);




                    i = i + 1;
                }


            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                string Criteria = " 1=1 ";
                SegmentVisitDtlsALLGet(Criteria);

            }

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls();

        }
    }
}