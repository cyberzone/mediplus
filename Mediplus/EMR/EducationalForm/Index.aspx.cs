﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;


namespace Mediplus.EMR.EducationalForm
{
    public partial class Index : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindEducationalForm()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria1 = " 1=1 ";
           // Criteria1 += " AND EFM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria1 += " AND EFM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            DS = objCom.EMR_EDUCATIONALFORMGet(Criteria1);
            if (DS.Tables[0].Rows.Count > 0)
            {

                Language.Value = Convert.ToString(DS.Tables[0].Rows[0]["EFM_LANGUAGE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_READ"]) == "1")
                {
                    Read.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_WRITE"]) == "1")
                {
                    Write.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_SPEAK"]) == "1")
                {
                    Speak.Checked = true;
                }


                ReligionBelief.Value = Convert.ToString(DS.Tables[0].Rows[0]["EFM_RELEGION"]);
            }
        }

        void SaveEducationalForm()
        {
            EMR_EducationalForm objEduFrm = new EMR_EducationalForm();

            objEduFrm.branchid = Convert.ToString(Session["Branch_ID"]);
            objEduFrm.patientmasterid =  Convert.ToString(Session["EMR_ID"]);

            objEduFrm.language = Language.Value;
            objEduFrm.read = Read.Checked == true ? "1" : "0";

            objEduFrm.write = Write.Checked == true ? "1" : "0";
            objEduFrm.speak = Speak.Checked == true ? "1" : "0";
            objEduFrm.religion = ReligionBelief.Value;

            objEduFrm.WEMR_spI_SaveEducationalForm();
        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }

                    BindEducationalForm();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveEducationalForm();
                EducationalFormControlPart1.SaveEducationalForm();
                EducationalFormControlPart2.SaveEducationalForm();
                EducationalFormControlPart3.SaveEducationalForm();

                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        UpdateAuditDtls();
                    }
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}