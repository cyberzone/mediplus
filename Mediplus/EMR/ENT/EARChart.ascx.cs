﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus.EMR.ENT
{
    public partial class EARChart : System.Web.UI.UserControl
    {

        public void BindPediatricImage()
        {

            ImageEditorPDC.Src = "../Department/OpenDepartmentalImage.aspx?ImageType=" + hidSegmentType.Value;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPediatricImage();

            }
        }
    }
}