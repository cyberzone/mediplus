﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="AdmissionSummary.aspx.cs" Inherits="Mediplus.EMR.InPatient.AdmissionSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
     <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

     
     
       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">


    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
             font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }


         function ShowErrorMessage(vMessage) {

             document.getElementById("divMessage").style.display = 'block';
             document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
         }

         function HideErrorMessage() {

             document.getElementById("divMessage").style.display = 'none';
             document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
          <div class="box">
        <div class="box-header">
            <h3 class="box-title">Admission Summary </h3>
        </div>
    </div>

     <br />
        <div style="padding-left:60%;width:80%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully  </div>
        </div>
  <div style="padding-left: 60%; width: 80%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top;">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                    </td>
                </tr>
            </table>

        </div>
    </div>
        <div style="float: right;">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save Record" OnClick="btnSave_Click"  />
           

                </ContentTemplate>
            </asp:UpdatePanel>
       
    </div>
    <br />
        <fieldset style="width:80%;">
            <legend class="lblCaption1">Admission Summary</legend>
        
      
         
            <fieldset>
                <legend></legend>
            <table id="admissionSummary" class="table spacy">
                <tr>
  
                    <td>
                        <label for="InpatientNo"  class="lblCaption1">Inpatient Number</label></td>
                    <td>
                        <input type="text" id="txtInpatientNo" runat="server" class="TextBoxStyle"  name="InpatientNo" value="" readonly="readonly" /></td>
                      <td>
                        <label for="AdmissionElective" class="lblCaption1">Admission</label>
                    </td>
                    <td colspan="2">
                         
                        <asp:RadioButtonList ID="radAdmissionType" runat="server" CssClass="lblCaption1"  RepeatDirection="Horizontal" >
                               <asp:ListItem Value="Elective" Text="Elective" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Emergency" Text="Emergency"></asp:ListItem>
                         
                        </asp:RadioButtonList>
                    </td>
                </tr>
             
                <tr>
                    <td>
                        <label for="AdmissionType" class="lblCaption1">Admission Type</label></td>
                    <td>
                        <asp:dropdownlist id="AdmissionType" runat="server"  style="width: 144px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="AdmissionType_SelectedIndexChanged">
                             <asp:ListItem Value="DAYCARE">Day Car</asp:ListItem>
                          <asp:ListItem Value="INPATIENT">InPatient</asp:ListItem>
                         <asp:ListItem Value="OBSERVATION">Observation</asp:ListItem>
                       </asp:dropdownlist>
                    </td>
                    <td>
                        <label for="AdmissionDateTime" class="lblCaption1">Admission Date & Time</label></td>
                    <td>
                       
                         <asp:TextBox ID="txtAdmissionDate" runat="server" Width="70px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txtAdmissionDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                          <asp:DropDownList ID="drpAdmissionHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server"   Width="48px"></asp:DropDownList>
                          <asp:DropDownList ID="drpAdmissionMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server"   Width="48px"></asp:DropDownList>

                    </td>
                </tr>
                <tr>                    
                    <td>
                        <label for="AttendingPhysician" class="lblCaption1">Attending Physician</label></td>
                    <td>
                        <select  id="AttendingPhysician" runat="server" style="width:200px;" class="TextBoxStyle"><option value=""  >Select</option></select></td>
                     
                    <td  >
                        <label for="ReferralPhysician" class="lblCaption1">Referral to Physician 1 </label></td>
                    <td>
                        <select id="ReferralPhysician"  runat="server" style="width:200px;" class="TextBoxStyle" ><option value="" >Select</option></select></td>
                
                </tr>
                <tr>                    
                    <td>
                        <label for="ReferralPhysician1" class="lblCaption1">Referral to Physician 2</label></td>
                    <td>
                        <select  id="ReferralPhysician1" runat="server" style="width:200px;" class="TextBoxStyle"><option value=""  >Select</option></select></td>
                     
                    <td  >
                        <label for="ReferralPhysician2" class="lblCaption1">Referral to Physician 3</label></td>
                    <td>
                        <select id="ReferralPhysician2"  runat="server" style="width:200px;" class="TextBoxStyle" ><option value="" >Select</option></select></td>
                
              </tr>
                </table>
            </fieldset>
            
            <fieldset>
                    <legend class="lblCaption1">Investigations Requested</legend>
            <table class="table spacy">

                <tr>
                    <td>
                        <label for="ProvisionalDiagonosis" class="lblCaption1">Provisional Diagnosis</label></td>
                    <td>
                        <textarea id="ProvisionalDiagnosis"  runat="server" name="ProvisionalDiagnosis"   style="height:50px;" cols="100" class="TextBoxStyle" ></textarea></td>
                </tr>
                <tr>
                    <td style="width:227px;">
                        <label for="Laboratory" class="lblCaption1">Laboratory</label></td>
                    <td>
                        <textarea id="Laboratory" runat="server" name="Laboratory" style="height:50px;"  cols="100" class="TextBoxStyle"></textarea></td>
                </tr>
                <tr>
                    <td style="width:227px;">
                        <label for="Radiological" class="lblCaption1">Radiological</label></td>
                    <td>
                        <textarea  id="Radiological" runat="server" name="Radiological"  style="height:50px;"  cols="100" class="TextBoxStyle"></textarea></td>
                </tr>
                <tr>
                    <td style="width:227px;">
                        <label for="OtherAdmissionSummary" class="lblCaption1">Others</label>
                    </td>
                    <td>
                       <textarea id="OtherAdmissionSummary" runat="server"  name="OtherAdmissionSummary"  style="height:50px;"  cols="100"  class="TextBoxStyle"></textarea>
                    </td>
                </tr>
                </table>
            </fieldset>
            <fieldset>
                    <legend></legend>
                 <table class="table spacy">
                <tr>
                    <td  >
                        <label for="ProcedurePlanned" class="lblCaption1">Procedure Planned</label></td>
                    <td>
                        <textarea id="ProcedurePlanned" runat="server"  name="ProcedurePlanned" style="height:50px;"  cols="100" class="TextBoxStyle"></textarea></td>
                </tr>
                <tr>
                    <td  >
                        <label for="Anesthesia" class="lblCaption1">Anesthesia</label></td>
                    <td>
                        <input type="text" id="Anesthesia" runat="server"  name="Anesthesia" style="width:500px;" class="TextBoxStyle"/></td>
                </tr>
                <tr>
                    <td  >
                        <label for="Treatment" class="lblCaption1">Treatment</label></td>
                    <td>
                        <textarea id="Treatment" runat="server"  name="Treatment" style="height:50px;"  cols="100"  class="TextBoxStyle"></textarea></td>
                </tr>
             
                <tr>
                    <td  >
                        <label for="ReasonforAdmission" class="lblCaption1">Reason for Admission</label></td>
                    <td>
                        <textarea id="ReasonforAdmission"  runat="server" name="ReasonforAdmission"  style="height:50px;"  cols="100" class="TextBoxStyle"></textarea>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <label for="Remarks" class="lblCaption1">Remarks</label></td>
                    <td>
                        <textarea id="Remarks" runat="server"  name="Remarks"  style="height:50px;"  cols="100" class="TextBoxStyle"></textarea>
                    </td>
                </tr>
               
            </table>
            </fieldset>
          
       
       </fieldset>
    </div>
</asp:Content>

