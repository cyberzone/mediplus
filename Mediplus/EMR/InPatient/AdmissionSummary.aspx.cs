﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
 

namespace Mediplus.EMR.InPatient
{
    public partial class AdmissionSummary : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }

                drpAdmissionHour.Items.Insert(index, Convert.ToString(strHour));
                drpAdmissionHour.Items[index].Value = Convert.ToString(strHour);



                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;




            drpAdmissionMin.Items.Insert(0, Convert.ToString("00"));
            drpAdmissionMin.Items[0].Value = Convert.ToString("00");





            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpAdmissionMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpAdmissionMin.Items[index].Value = Convert.ToString(j - intCount);



                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void GetStaff()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.GetStaffMaster(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                AttendingPhysician.DataSource = DS;
                AttendingPhysician.DataValueField = "HSFM_STAFF_ID";
                AttendingPhysician.DataTextField = "FullName";
                AttendingPhysician.DataBind();


                ReferralPhysician.DataSource = DS;
                ReferralPhysician.DataValueField = "HSFM_STAFF_ID";
                ReferralPhysician.DataTextField = "FullName";
                ReferralPhysician.DataBind();

                ReferralPhysician1.DataSource = DS;
                ReferralPhysician1.DataValueField = "HSFM_STAFF_ID";
                ReferralPhysician1.DataTextField = "FullName";
                ReferralPhysician1.DataBind();

                ReferralPhysician2.DataSource = DS;
                ReferralPhysician2.DataValueField = "HSFM_STAFF_ID";
                ReferralPhysician2.DataTextField = "FullName";
                ReferralPhysician2.DataBind();
            }

            AttendingPhysician.Items.Insert(0, "--- Select ---");
            AttendingPhysician.Items[0].Value = "";


            ReferralPhysician.Items.Insert(0, "--- Select ---");
            ReferralPhysician.Items[0].Value = "";

            ReferralPhysician1.Items.Insert(0, "--- Select ---");
            ReferralPhysician1.Items[0].Value = "";


            ReferralPhysician2.Items.Insert(0, "--- Select ---");
            ReferralPhysician2.Items[0].Value = "";
        }

        void GetRefDr()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objCom = new CommonBAL();
            DS = objCom.RefDoctorMasterGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                ReferralPhysician.DataSource = DS;
                ReferralPhysician.DataValueField = "HRDM_REF_ID";
                ReferralPhysician.DataTextField = "FullName";
                ReferralPhysician.DataBind();

                ReferralPhysician1.DataSource = DS;
                ReferralPhysician1.DataValueField = "HRDM_REF_ID";
                ReferralPhysician1.DataTextField = "FullName";
                ReferralPhysician1.DataBind();

                ReferralPhysician2.DataSource = DS;
                ReferralPhysician2.DataValueField = "HRDM_REF_ID";
                ReferralPhysician2.DataTextField = "FullName";
                ReferralPhysician2.DataBind();



            }
            ReferralPhysician.Items.Insert(0, "--- Select ---");
            ReferralPhysician.Items[0].Value = "";

            ReferralPhysician1.Items.Insert(0, "--- Select ---");
            ReferralPhysician1.Items[0].Value = "";


            ReferralPhysician2.Items.Insert(0, "--- Select ---");
            ReferralPhysician2.Items[0].Value = "";




        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                ProvisionalDiagnosis.Value += Convert.ToString(DR["EPD_DIAG_CODE"]) + " - " + Convert.ToString(DR["EPD_DIAG_NAME"]) + ", ";
            }

        FunEnd: ;
        }

        void BindLaboratory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPL_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTLaboratory objPro = new EMR_PTLaboratory();
            DS = objPro.LaboratoryGet(Criteria);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Laboratory.Value += Convert.ToString(DR["EPL_LAB_CODE"]) + " - " + Convert.ToString(DR["EPL_LAB_NAME"]) + ", ";
            }
        FunEnd: ;
        }

        void BindRadiology()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTRadiology objPro = new EMR_PTRadiology();
            DS = objPro.RadiologyGet(Criteria);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Radiological.Value += Convert.ToString(DR["EPR_RAD_CODE"]) + " - " + Convert.ToString(DR["EPR_RAD_NAME"]) + ", ";
            }

        FunEnd: ;
        }

        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            DS = objPro.ProceduresGet(Criteria);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                ProcedurePlanned.Value += Convert.ToString(DR["EPP_DIAG_CODE"]) + " - " + Convert.ToString(DR["EPP_DIAG_NAME"]) + ", ";
            }
        FunEnd: ;
        }


        void BindPatientDetails()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            DS = objCom.PatientMasterGet(Criteria);
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["FileNo"] = Convert.ToString(DR["HPM_PT_ID"]);
                    ViewState["FullName"] = Convert.ToString(DR["FullName"]);

                    ViewState["Age"] = Convert.ToString(DR["HPM_AGE"]) + " " + Convert.ToString(DR["HPM_AGE_TYPE"]) + " " + Convert.ToString(DR["HPM_AGE1"]) + " " + Convert.ToString(DR["HPM_AGE_TYPE1"]);
                    ViewState["HPM_SEX"] = Convert.ToString(DR["HPM_SEX"]);

                    ViewState["HPM_POBOX"] = Convert.ToString(DR["HPM_POBOX"]);


                    ViewState["HPM_PT_TYPE"] = Convert.ToString(DR["HPM_PT_TYPE"]);

                    ViewState["HPM_PT_MNAME"] = Convert.ToString(DR["HPM_PT_MNAME"]);
                    ViewState["HPM_PT_LNAME"] = Convert.ToString(DR["HPM_PT_LNAME"]);

                    ViewState["HPM_DOB"] = Convert.ToString(DR["HPM_DOB"]);
                    ViewState["HPM_ADDR"] = Convert.ToString(DR["HPM_ADDR"]);
                    ViewState["HPM_CITY"] = Convert.ToString(DR["HPM_CITY"]);
                    ViewState["HPM_AREA"] = Convert.ToString(DR["HPM_AREA"]);
                    ViewState["HPM_COUNTRY"] = Convert.ToString(DR["HPM_COUNTRY"]);
                    ViewState["HPM_NATIONALITY"] = Convert.ToString(DR["HPM_NATIONALITY"]);
                    ViewState["HPM_BLOOD_GROUP"] = Convert.ToString(DR["HPM_BLOOD_GROUP"]);
                    ViewState["HPM_MARITAL"] = Convert.ToString(DR["HPM_MARITAL"]);
                    ViewState["HPM_PHONE1"] = Convert.ToString(DR["HPM_PHONE1"]);//RESIDENCE
                    ViewState["HPM_PHONE3"] = Convert.ToString(DR["HPM_PHONE3"]);//OFFICE
                    ViewState["HPM_MOBILE"] = Convert.ToString(DR["HPM_MOBILE"]);
                    ViewState["HPM_FAX"] = Convert.ToString(DR["HPM_FAX"]);
                    ViewState["HPM_INS_COMP_ID"] = Convert.ToString(DR["HPM_INS_COMP_ID"]);
                    ViewState["HPM_INS_COMP_NAME"] = Convert.ToString(DR["HPM_INS_COMP_NAME"]);

                    ViewState["HPM_POLICY_NO"] = Convert.ToString(DR["HPM_POLICY_NO"]);
                    ViewState["HPM_POLICY_TYPE"] = Convert.ToString(DR["HPM_POLICY_TYPE"]);
                    ViewState["HPM_PT_COMP_NAME"] = Convert.ToString(DR["HPM_PT_COMP_NAME"]);
                    ViewState["HPM_DED_TYPE"] = Convert.ToString(DR["HPM_DED_TYPE"]);
                    ViewState["HPM_DED_AMT"] = Convert.ToString(DR["HPM_DED_AMT"]);
                    ViewState["HPM_COINS_TYPE"] = Convert.ToString(DR["HPM_COINS_TYPE"]);
                    ViewState["HPM_COINS_AMT"] = Convert.ToString(DR["HPM_COINS_AMT"]);
                    ViewState["HPM_ID_NO"] = Convert.ToString(DR["HPM_ID_NO"]);





                }
            }
        }

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["Mode"] = "Edit";
                AdmissionType.Enabled = false;
                txtInpatientNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);

                txtAdmissionDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateDesc"]);

                string strArrivalDate = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTimeDesc"]);
                string[] arrArrivalDater = strArrivalDate.Split(':');
                if (arrArrivalDater.Length > 1)
                {
                    drpAdmissionHour.SelectedValue = arrArrivalDater[0];
                    drpAdmissionMin.SelectedValue = arrArrivalDater[1];

                }

                ProvisionalDiagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROVISIONAL_DIAGNOSIS"]);
                AttendingPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ATTENDING_PHYSICIAN"]);
                Laboratory.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_LABORATORY"]);
                Radiological.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_RADIOLOGICAL"]);
                OtherAdmissionSummary.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_OTHERS"]);
                ProcedurePlanned.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROCEDURE_PLANNED"]);
                Anesthesia.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ANESTHESIA"]);
                Treatment.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_TREATMENT"]);

                ReferralPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_REFERRAL_PHYSICIAN1") == false)
                {
                    ReferralPhysician1.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN1"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("IAS_REFERRAL_PHYSICIAN2") == false)
                {
                    ReferralPhysician2.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN2"]);
                }

                radAdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);
                AdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);

                ReasonforAdmission.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REASON_FOR_ADMISSION"]);
                Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REMARKS"]);
                return true;
            }
            else
            {

                return false;
            }
        }

        Boolean CheckAdmissionSummary()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND  IAS_PT_ID='" + Convert.ToString(ViewState["FileNo"]) + "'";



            string strStartDate = txtAdmissionDate.Text.Trim();
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtAdmissionDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) = '" + strForStartDate + "'";
            }

            IP_AdmissionSummary objAdmSum = new IP_AdmissionSummary();
            DS = objAdmSum.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                return true;
            }

            return false;


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {
                    ViewState["Mode"] = "New";

                    BindTime();
                    GetStaff();
                    //  GetRefDr();
                    txtInpatientNo.Value = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPDAYCARE");


                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtAdmissionDate.Text = strDate;


                    if (BindAdmissionSummary() == false)
                    {
                        AttendingPhysician.Value = Convert.ToString(Session["HPV_DR_ID"]);
                        BindPatientDetails();
                        BindDiagnosis();
                        BindLaboratory();
                        BindRadiology();
                        BindProcedure();
                    }




                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AdmissionSummary.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                IP_AdmissionSummary objAS = new IP_AdmissionSummary();

                if (Convert.ToString(ViewState["Mode"]) == "New")
                {
                    if (CheckAdmissionSummary() == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Already Registred this patient for Same Date.')", true);
                        goto FunEnd;

                    }
                }


                objAS.BranchID = Convert.ToString(Session["Branch_ID"]);
                objAS.AdmissionID = txtInpatientNo.Value;
                objAS.PatientName = Convert.ToString(ViewState["FullName"]);
                objAS.Sex = Convert.ToString(ViewState["HPM_SEX"]);
                objAS.Age = Convert.ToString(ViewState["Age"]);

                //Param.Add("inpatientno",InpatientNO);

                objAS.FileNumber = Convert.ToString(ViewState["FileNo"]);
                objAS.AdmissionDateTime = txtAdmissionDate.Text.Trim() + " " + drpAdmissionHour.SelectedValue + ":" + drpAdmissionMin.SelectedValue + ":00";
                objAS.ProvisionalDiagnosis = ProvisionalDiagnosis.Value;
                objAS.AttendingPhysician = AttendingPhysician.Value;
                objAS.Laboratory = Laboratory.Value;
                objAS.Radiological = Radiological.Value;
                objAS.Others = OtherAdmissionSummary.Value;
                objAS.ProcedurePlanned = ProcedurePlanned.Value;
                objAS.Anesthesia = Anesthesia.Value;
                objAS.Treatment = Treatment.Value;
                objAS.ReferralPhysician = ReferralPhysician.Value;
                objAS.AdmittingPhysician = AttendingPhysician.Value;
                objAS.ReasonForAdmission = ReasonforAdmission.Value;
                objAS.AdmissionType = AdmissionType.SelectedValue;
                objAS.Remarks = Remarks.Value;
                objAS.PatientMasterID = Convert.ToString(Session["EMR_ID"]);
                objAS.DoctorID = Convert.ToString(Session["HPV_DR_ID"]);
                objAS.Department = Convert.ToString(Session["HPV_DEP_NAME"]); //Convert.ToString(Session["HPV_DEP_ID"]);   
                objAS.DoctorName = Convert.ToString(Session["HPV_DR_NAME"]);

                objAS.PatientType = Convert.ToString(ViewState["HPM_PT_TYPE"]);
                objAS.MiddleName = Convert.ToString(ViewState["HPM_PT_MNAME"]);
                objAS.LastName = Convert.ToString(ViewState["HPM_PT_LNAME"]);
                objAS.DOB = Convert.ToString(ViewState["HPM_DOB"]);
                objAS.POBox = Convert.ToString(ViewState["HPM_POBOX"]);
                objAS.Address = Convert.ToString(ViewState["HPM_ADDR"]);
                //objAS.Remarks = Convert.ToString(ViewState["HPM_REMARKS"]);
                objAS.City = Convert.ToString(ViewState["HPM_CITY"]);
                objAS.Area = Convert.ToString(ViewState["HPM_AREA"]);
                objAS.Country = Convert.ToString(ViewState["HPM_COUNTRY"]);
                objAS.Nationality = Convert.ToString(ViewState["HPM_NATIONALITY"]);
                objAS.BloodGroup = Convert.ToString(ViewState["HPM_BLOOD_GROUP"]);
                objAS.Marital = Convert.ToString(ViewState["HPM_MARITAL"]);
                objAS.ResPhone = Convert.ToString(ViewState["HPM_PHONE1"]);
                objAS.OfficePhone = Convert.ToString(ViewState["HPM_PHONE3"]);
                objAS.Mobile = Convert.ToString(ViewState["HPM_MOBILE"]);
                objAS.Fax = Convert.ToString(ViewState["HPM_FAX"]);
                objAS.InsuranceCompanyID = Convert.ToString(ViewState["HPM_INS_COMP_ID"]);
                objAS.InsuranceCompanyName = Convert.ToString(ViewState["HPM_INS_COMP_NAME"]);
                objAS.PolicyNo = Convert.ToString(ViewState["HPM_POLICY_NO"]);
                objAS.PolicyType = Convert.ToString(ViewState["HPM_POLICY_TYPE"]);
                objAS.ContExPDate = "";
                objAS.CompanyCashPatient = "";
                objAS.PatientCompanyName = Convert.ToString(ViewState["HPM_PT_COMP_NAME"]);
                objAS.DedType = Convert.ToString(ViewState["HPM_DED_TYPE"]);
                objAS.DedAmount = Convert.ToString(ViewState["HPM_DED_AMT"]);
                objAS.CoinsType = Convert.ToString(ViewState["HPM_COINS_TYPE"]);
                objAS.CoinsAmount = Convert.ToString(ViewState["HPM_COINS_AMT"]);
                objAS.IDCardNO = Convert.ToString(ViewState["HPM_ID_NO"]);
                objAS.ReferenceNo = "";
                objAS.admittedfor = "";
                objAS.AdmissionMode = radAdmissionType.SelectedValue;
                objAS.AdmisisonModeDesc = "";
                objAS.ModifiedUser = Convert.ToString(Session["User_ID"]);
                objAS.CreatedUser = Convert.ToString(Session["User_ID"]);
                objAS.InsuranceTypeTreatment = "";
                objAS.Status = "NA";
                objAS.PackageAmount = "";

                objAS.cptdrgcode = "";
                objAS.cptdrgamount = "";
                objAS.approvalcode = "";
                objAS.expiraydate = "";
                objAS.proposeddatetime = txtAdmissionDate.Text.Trim() + " " + drpAdmissionHour.SelectedValue + ":" + drpAdmissionMin.SelectedValue + ":00";
                objAS.IAS_REFERRAL_PHYSICIAN1 = ReferralPhysician1.Value;
                objAS.IAS_REFERRAL_PHYSICIAN2 = ReferralPhysician2.Value;

                objAS.SaveIPAdmissionSummary();


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      AdmissionSummary.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void AdmissionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AdmissionType.SelectedValue == "DAYCARE")
            {
                txtInpatientNo.Value = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPDAYCARE");
            }
            else if (AdmissionType.SelectedValue == "INPATIENT")
            {
                txtInpatientNo.Value = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPADMISS");
            }
            else if (AdmissionType.SelectedValue == "OBSERVATION")
            {
                txtInpatientNo.Value = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPOBSERV");
            }
        }
    }
}