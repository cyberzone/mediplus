﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
namespace Mediplus.EMR
{
    public partial class DisplaySignatureStaff : System.Web.UI.Page
    {
        public string DR_ID { set; get; }

        void BindStaffSignature()
        {
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSP_STAFF_ID='" + DR_ID + "'";

            DS = objCom.StaffPhotoGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                if (DS.Tables[0].Rows[0].IsNull("HSP_Signature") == false)
                {

                    ViewState["SignatureImgStaff"] = (Byte[])DS.Tables[0].Rows[0]["HSP_Signature"];

                }



            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["SignatureImgStaff"] = "";
            DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);


            Byte[] bytImage;
            BindStaffSignature();
            if (Convert.ToString(ViewState["SignatureImgStaff"]) != "")
            {
                bytImage = (Byte[])ViewState["SignatureImgStaff"];
                //Session["SignatureImg1"] = "";

                Response.BinaryWrite(bytImage);

            }
        }
    }
}