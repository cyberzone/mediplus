﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.Dental.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="Treatment.ascx" TagPrefix="UC1" TagName="Treatment" %>
<%@ Register Src="DentalChart.ascx" TagPrefix="UC1" TagName="Dental" %>
<%@ Register Src="ClinicalFindingChart.ascx" TagPrefix="UC1" TagName="ClinicalFinding" %>
<%@ Register Src="PeriodonticChart.ascx" TagPrefix="UC1" TagName="Periodontic" %>

<%@ Register Src="CaseHistory.ascx" TagPrefix="UC1" TagName="CaseHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
   

     
    <script src="../../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../../Styles/Accordionstyles.css">


    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

       <%-- <script type="text/javascript" src="../Scripts/Patient/PatientMaster.js" ></script>--%>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
     <script type="text/javascript">
         function ShowMessage() {
             $("#myMessage").show();
             setTimeout(function () {
                 var selectedEffect = 'blind';
                 var options = {};
                 $("#myMessage").hide();
             }, 2000);
             return true;
         }

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><%=PageName %> </h3>
                </div>
            </div>
          <div style="padding-left:60%;width:100%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
        </div>
      <table style="width:100%" >
          <tr>
             
              <td align="right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" Text="Save Record" />
                    </ContentTemplate>
                </asp:UpdatePanel>

              </td>
          </tr>
      </table>
          <div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="80%">
                    <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Treatment" Width="100%">
                        <ContentTemplate>
                              <UC1:Treatment ID="pnlTreatment" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Dental Chart" Width="100%">
                        <ContentTemplate>
                              <UC1:Dental ID="pnlDental" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Clinical Finding" Width="100%">
                        <ContentTemplate>
                               <UC1:ClinicalFinding ID="pnlClinicalFinding" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Periodontic Chart" Width="100%"    >
                        <ContentTemplate>
                               <UC1:Periodontic ID="pnlPeriodontic" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="tabCaseHistory" HeaderText="Case History" Width="100%"  Visible="false"   >
                        <ContentTemplate>
                               <UC1:CaseHistory ID="pnlCaseHistory" runat="server"  />
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </div>

         </div>
</asp:Content>
