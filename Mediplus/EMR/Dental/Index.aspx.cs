﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;
namespace Mediplus.EMR.Dental
{
    public partial class Index : System.Web.UI.Page
    {
        public string PageName = "";

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        static string strSessionDeptId, strUserCode;

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static string[] GetServiceID(string prefixText)
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;
            CommonBAL dbo = new CommonBAL();
            String Criteria = " 1=1 AND HHS_STATUS='Active' ";

            Criteria += " AND (  HHS_CODE  LIKE '" + prefixText + "%' OR  HHS_DESCRIPTION  LIKE '" + prefixText + "%') ";
          //  DS = dbo.HaadServiceGet(Criteria, "DENTAL", "30");
            DS = dbo.HaadServicessListGet("DENTAL", prefixText, "", strSessionDeptId);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["Code"]) + "-" + Convert.ToString(DS.Tables[0].Rows[i]["Description"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static string[] GetDiagnosisID(string prefixText)
        {

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND (  DIM_ICD_ID  like '" + prefixText + "%' OR  DIM_ICD_DESCRIPTION  like '" + prefixText + "%') ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                PageName = Request.QueryString["PageName"];
                strSessionDeptId = Convert.ToString(Session["User_DeptID"]);


                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;


                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                {
                    tabCaseHistory.Visible = true;
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                pnlTreatment.SaveDentalMaster();
                pnlTreatment.SaveTreatment();

                if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                {
                    pnlCaseHistory.SaveCaseHistory();
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Dental.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}