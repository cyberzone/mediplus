﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClinicalFindingChart.ascx.cs" Inherits="Mediplus.EMR.Dental.ClinicalFindingChart" %>
<input type="hidden" id="hidSegmentType" runat="server" value="DCF" />

<table style="width:800px"  >
    <tr>
        <td style="width:100px;" class="lblCaption1">  
            <div style="background-color:#00FFFF;width:20px;height:20px;"></div> Caries
        </td>
        <td style="width:100px;" class="lblCaption1">
                <div style="background-color:#FF0000;width:20px;height:20px;"></div> Missing Tooth
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#0000FF;width:20px;height:20px;"></div> Caries Exposed
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#A52A2A;width:20px;height:20px;"></div> Filling
        </td>
          <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#008000;width:20px;height:20px;"></div> Extraction
        </td>
         <td style="width:150px;" class="lblCaption1">
             <div style="background-color:#FFA500;width:20px;height:20px;"></div> Rootcanal Pending
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#00FF00;width:20px;height:20px;"></div> Rootcanal Done
        </td>
          <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#FFFF00;width:20px;height:20px;"></div> Calculus
        </td>



    </tr>
    <tr>
        <td style="height:10px;">

        </td>
    </tr>
    <tr>
          <td style="width:100px;" class="lblCaption1">  
            <div style="background-color:#800080;width:20px;height:20px;"></div> Crown
        </td>
        <td style="width:100px;" class="lblCaption1">
                <div style="background-color:#808000;width:20px;height:20px;"></div> Bridge
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#000080;width:20px;height:20px;"></div> Dentin Caries
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#FF00FF;width:20px;height:20px;"></div> Broken Tooth
        </td>
          <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#008080;width:20px;height:20px;"></div> Composite Filling
        </td>
         <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#FF7F50;width:20px;height:20px;"></div> Root Stump
        </td>
         <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#4B0082;width:20px;height:20px;"></div> Impacted Tooth
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#87CEEB;width:20px;height:20px;"></div> Abscess
        </td>

    </tr>
    <tr>
        
       <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#F5DEB3;width:20px;height:20px;"></div> Attrition
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#A9A9A9;width:20px;height:20px;"></div> abrasion
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#7FFFD4;width:20px;height:20px;"></div> Non Vital Tooth
        </td>
        <td style="width:100px;" class="lblCaption1">
             <div style="background-color:#9ACD32;width:20px;height:20px;"></div> Deep pocket
        </td>


    </tr>
</table>
<table>

    <tr>
        <td>
                <iframe id="ImageEditorPDC" runat="server" src="../Department/OpenDepartmentalImage.aspx?ImageType=<%=SegmentType %>" style="width:100%;height:600px"></iframe>      

        </td>
    </tr>
   
</table>
<br />