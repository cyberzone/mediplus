﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.Web.UI.HtmlControls;
using System.IO;


namespace Mediplus.EMR.Dental
{
    public partial class Treatment : System.Web.UI.UserControl
    {
        /*
        void LoadDentalImage()
        {
            string imagePath = "", imageNewPath;
            System.Drawing.Image image = System.Drawing.Image.FromFile(imagePath);
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image))
            {
                System.Drawing.Color customColor = System.Drawing.Color.FromArgb(50, System.Drawing.Color.Gray);
                System.Drawing.SolidBrush shadowBrush = new System.Drawing.SolidBrush(customColor);
                g.FillRectangles(shadowBrush, new System.Drawing.RectangleF[] { rectFToFill });
            }
            image.Save(imageNewPath);


        }
         * */

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BuildDentalTrans()
        {

            DataTable dt = new DataTable();

            DataColumn DDMR_BRANCH_ID = new DataColumn();
            DDMR_BRANCH_ID.ColumnName = "DDMR_BRANCH_ID";

            DataColumn DDMR_TRANS_ID = new DataColumn();
            DDMR_TRANS_ID.ColumnName = "DDMR_TRANS_ID";


            DataColumn DDMR_TOOTH_NO = new DataColumn();
            DDMR_TOOTH_NO.ColumnName = "DDMR_TOOTH_NO";

            DataColumn DDMR_TRNT_ID = new DataColumn();
            DDMR_TRNT_ID.ColumnName = "DDMR_TRNT_ID";

            DataColumn DDMR_TRNT_DESCRIPTION = new DataColumn();
            DDMR_TRNT_DESCRIPTION.ColumnName = "DDMR_TRNT_DESCRIPTION";


            DataColumn DDMR_TRNT_COST = new DataColumn();
            DDMR_TRNT_COST.ColumnName = "DDMR_TRNT_COST";




            DataColumn DDMR_TRNT_ICD_DESCRIPTION = new DataColumn();
            DDMR_TRNT_ICD_DESCRIPTION.ColumnName = "DDMR_TRNT_ICD_DESCRIPTION";

            DataColumn DDMR_TRNT_COMMENTS = new DataColumn();
            DDMR_TRNT_COMMENTS.ColumnName = "DDMR_TRNT_COMMENTS";


            DataColumn Treatment = new DataColumn();
            Treatment.ColumnName = "Treatment";


            DataColumn ICDDesc = new DataColumn();
            ICDDesc.ColumnName = "ICDDesc";



            dt.Columns.Add(DDMR_BRANCH_ID);
            dt.Columns.Add(DDMR_TRANS_ID);
            dt.Columns.Add(DDMR_TOOTH_NO);


            dt.Columns.Add(DDMR_TRNT_ID);
            dt.Columns.Add(DDMR_TRNT_DESCRIPTION);
            dt.Columns.Add(DDMR_TRNT_COST);

            dt.Columns.Add(DDMR_TRNT_ICD_DESCRIPTION);

            dt.Columns.Add(DDMR_TRNT_COMMENTS);

            dt.Columns.Add(Treatment);
            dt.Columns.Add(ICDDesc);


            ViewState["EMR_DENTAL_TRANS"] = dt;

        }

        void AddDentalTrans(string ToothNo)
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["EMR_DENTAL_TRANS"];
            DataRow objrow;
            objrow = DT.NewRow();
            objrow["DDMR_BRANCH_ID"] = Convert.ToString(Session["Branch_ID"]);
            objrow["DDMR_TRANS_ID"] = Convert.ToString(Session["EMR_ID"]);
            objrow["DDMR_TOOTH_NO"] = ToothNo;

            objrow["DDMR_TRNT_ID"] = 0;
            objrow["DDMR_TRNT_DESCRIPTION"] = 0;
            objrow["DDMR_TRNT_COST"] = 0.00;

            objrow["DDMR_TRNT_ICD_DESCRIPTION"] = "";
            objrow["DDMR_TOOTH_NO"] = 1;
            objrow["DDMR_TRNT_ID"] = 0;

            objrow["DDMR_TRNT_ICD_DESCRIPTION"] = 0;

            objrow["Treatment"] = "";
            objrow["ICDDesc"] = "";


            DT.Rows.Add(objrow);
            ViewState["EMR_DENTAL_TRANS"] = DT;





        }



        void SaveDentalTrans(string ToothNo)
        {
            EMR_Dental objDental = new EMR_Dental();

            objDental.DDMR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objDental.DDMR_TRANS_ID = Convert.ToString(Session["EMR_ID"]);
            objDental.DDMR_TOOTH_NO = ToothNo;

            objDental.EMRDentalTransAdd();


        }



        void BindDentalMaster()
        {
            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1 ";

            Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND DDMR_TRANS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objDental.EMRDentalMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_REMARKS"]);
                txtFollowNotes.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_FOLLOWUP"]);
            }
        }


        public void SaveDentalMaster()
        {
            EMR_Dental objDental = new EMR_Dental();

            objDental.DDMR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objDental.DDMR_TRANS_ID = Convert.ToString(Session["EMR_ID"]);
            objDental.DDMR_REMARKS = txtRemarks.Text; ;
            objDental.DDMR_FOLLOWUP = txtFollowNotes.Text; ;

            objDental.EMRDentalMasterAdd();
        }

        void BindgvSOServicePopup(string HaadCode, out string ServiceID)
        {
            ServiceID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            if (Convert.ToString(Session["HPV_COMP_ID"]) != "")
            {
                Criteria += " AND EDMS_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EDMS_HAAD_CODE	='" + HaadCode + "' AND EDMS_COMP_ID = '" + Convert.ToString(Session["HPV_COMP_ID"]) + "' AND EDMS_DR_CODE ='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";
            }
            else
            {
                Criteria += " AND EDMS_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EDMS_HAAD_CODE	='" + HaadCode + "' AND ( EDMS_COMP_ID IS NULL OR  EDMS_COMP_ID ='')  AND EDMS_DR_CODE ='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";
            }

            if (Convert.ToString(Session["HPV_COMP_ID"]) != "")
            {
                DS = new DataSet();
                objCom = new CommonBAL();
                Criteria = " HAS_COMP_ID='" + Convert.ToString(Session["HPV_COMP_ID"]) + "' AND  HSM_HAAD_CODE ='" + HaadCode + "'";

                DS = objCom.ServiceMasterAgrementServiceGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                }
            }
            else
            {
                DS = new DataSet();
                objCom = new CommonBAL();
                Criteria = " HSM_HAAD_CODE ='" + HaadCode + "'";

                DS = objCom.ServiceMasterGet(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {

                    ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);


                }
                else
                {
                    ServiceID = HaadCode;
                }
            }


        }

        public void SaveTreatment()
        {

            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1 ";

            Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND DDMR_TRANS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objDental.EMRDentalTransGet(Criteria);




            if (DS.Tables[0].Rows.Count > 0)
            {


                int j = 1;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DropDownList drp1 = (DropDownList)PlaceHolder1.FindControl("drpTeeth" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]));
                    TextBox txt1 = (TextBox)PlaceHolder1.FindControl("txtTreatment" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]));
                    TextBox txt2 = (TextBox)PlaceHolder1.FindControl("txtICDDesc" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]));
                    TextBox txt3 = (TextBox)PlaceHolder1.FindControl("txtCost" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]));
                    TextBox txt4 = (TextBox)PlaceHolder1.FindControl("txtComments" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]));

                    TextBox txt5 = (TextBox)PlaceHolder1.FindControl("txtSlNo" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]));

                    string[] arrtxtgvTreatment = txt1.Text.Split('~');
                    string[] arrtxtgvICDDesc = txt2.Text.Split('~');

                    string strTrntID = "", strTrntDesc = "", strICDID = "", strICDDesc = "";

                    if (arrtxtgvTreatment.Length > 1)
                    {
                        strTrntID = arrtxtgvTreatment[0];
                        strTrntDesc = arrtxtgvTreatment[1];
                    }

                    if (arrtxtgvICDDesc.Length > 1)
                    {
                        strICDID = arrtxtgvICDDesc[0];
                        strICDDesc = arrtxtgvICDDesc[1];
                    }

                    string ServiceID = "";
                    BindgvSOServicePopup(strTrntID, out   ServiceID);


                    objDental.DDMR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objDental.DDMR_TRANS_ID = Convert.ToString(Session["EMR_ID"]);
                    objDental.DDMR_TOOTH_NO = drp1.SelectedValue;

                    objDental.DDMR_TRNT_ID = strTrntID;
                    objDental.DDMR_TRNT_DESCRIPTION = strTrntDesc;


                    objDental.DDMR_TRNT_ICD = strICDID;
                    objDental.DDMR_TRNT_ICD_DESCRIPTION = strICDDesc;

                    objDental.DDMR_TRNT_COST = txt3.Text;
                    objDental.DDMR_TRNT_COMMENTS = txt4.Text;
                    objDental.DDMR_SL_NO = txt5.Text;
                    objDental.DDMR_SERV_ID = ServiceID;

                    objDental.EMRDentalTransUpdate();

                    j = j + 1;
                }

            }


        }


        void BindImageData()
        {
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowTest();", true);

            // MyBody.Attributes.Add("onload", "doThis('rm1','1');");

            // Page.RegisterStartupScript("load", "<script> doThis('rm1','1');</script>");
            // Page.RegisterStartupScript("load", "<script> doThis('rm2','2');</script>");
            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1  and DDMR_TOOTH_NO!='' and DDMR_TOOTH_NO !='0'  ";

            Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND DDMR_TRANS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objDental.EMRDentalTransGet(Criteria);

            string strToothNo = "";
            int i = 0;

            foreach (DataRow DR in DS.Tables[0].Rows)
            {


                if (strToothNo.IndexOf("|" + Convert.ToString(DR["DDMR_TOOTH_NO"]) + "|") != -1)
                {

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "rm1" + Convert.ToString(DR["DDMR_TOOTH_NO"]), "doThis('rm" + Convert.ToString(DR["DDMR_TOOTH_NO"]) + "','" + Convert.ToString(DR["DDMR_TOOTH_NO"]) + "','Brown');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "rm" + Convert.ToString(DR["DDMR_TOOTH_NO"]), "doThis('rm" + Convert.ToString(DR["DDMR_TOOTH_NO"]) + "','" + Convert.ToString(DR["DDMR_TOOTH_NO"]) + "','Green');", true);
                }

                if (strToothNo != "")
                {
                    strToothNo += Convert.ToString(DR["DDMR_TOOTH_NO"]) + "|";
                }
                else
                {
                    strToothNo = "|" + Convert.ToString(DR["DDMR_TOOTH_NO"]) + "|";
                }


                i = i + 1;
            }

            // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "rm1", "doThis('rm1','1');", true);
            // ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "rm2", "doThis('rm2','2');", true);


        }

        DataTable BuildTeathNo()
        {
            DataTable DT = new DataTable();

            DataColumn ToothNo = new DataColumn();
            ToothNo.ColumnName = "ToothNo";
            DT.Columns.Add(ToothNo);

            DataRow objrow;

            for (int i = 1; i <= 32; i++)
            {
                objrow = DT.NewRow();
                objrow["ToothNo"] = i;
                DT.Rows.Add(objrow);

            }


            objrow = DT.NewRow();
            objrow["ToothNo"] = "A";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "B";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "C";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "D";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["ToothNo"] = "E";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "F";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "G";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "H";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["ToothNo"] = "I";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "J";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "K";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "L";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "M";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "N";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["ToothNo"] = "O";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "P";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["ToothNo"] = "Q";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "R";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["ToothNo"] = "S";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "T";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "other";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["ToothNo"] = "0";
            DT.Rows.Add(objrow);

            DT.AcceptChanges();

            return DT;
        }

        void BindDentalControls()
        {
            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1 ";

            Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND DDMR_TRANS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objDental.EMRDentalTransGet(Criteria);




            if (DS.Tables[0].Rows.Count > 0)
            {

                HtmlGenericControl trTop1 = new HtmlGenericControl("tr");

                HtmlGenericControl tdTop1 = new HtmlGenericControl("td");
                HtmlGenericControl tdTop2 = new HtmlGenericControl("td");
                HtmlGenericControl tdTop3 = new HtmlGenericControl("td");
                HtmlGenericControl tdTop4 = new HtmlGenericControl("td");
                HtmlGenericControl tdTop5 = new HtmlGenericControl("td");
                HtmlGenericControl tdTop6 = new HtmlGenericControl("td");



                trTop1.Style.Add("background-image", "'../Images/menubar-bg.jpg'");
                tdTop1.Style.Add("background-image", "'../Images/menubar-bg.jpg'");
                tdTop2.Style.Add("background-image", "'../Images/menubar-bg.jpg'");
                tdTop3.Style.Add("background-image", "'../Images/menubar-bg.jpg'");
                tdTop4.Style.Add("background-image", "'../Images/menubar-bg.jpg'");
                tdTop5.Style.Add("background-image", "'../Images/menubar-bg.jpg'");

                tdTop1.Style.Add("height", "20px");
                tdTop2.Style.Add("height", "20px");
                tdTop3.Style.Add("height", "20px");
                tdTop4.Style.Add("height", "20px");
                tdTop5.Style.Add("height", "20px");


                Label lblHeader1 = new Label();
                lblHeader1.Text = "Teeth";
                lblHeader1.CssClass = "lblCaption";
                lblHeader1.Style.Add("font-family", "Segoe UI,arial");
                lblHeader1.Style.Add("font-size", "13px");
                lblHeader1.Style.Add("width", "95%");
                lblHeader1.BorderStyle = BorderStyle.None;
                lblHeader1.Style.Add("color", "#ffffff");


                Label lblHeader2 = new Label();
                lblHeader2.Text = "Treatment";
                lblHeader2.CssClass = "lblCaption";
                lblHeader2.Style.Add("font-family", "Segoe UI,arial");
                lblHeader2.Style.Add("font-size", "13px");
                lblHeader2.Style.Add("width", "95%");
                lblHeader2.Style.Add("color", "#ffffff");


                Label lblHeader3 = new Label();
                lblHeader3.Text = "ICD Code / Description";
                lblHeader3.CssClass = "lblCaption";
                lblHeader3.Style.Add("font-family", "Segoe UI,arial");
                lblHeader3.Style.Add("font-size", "13px");
                lblHeader3.Style.Add("width", "95%");
                lblHeader3.Style.Add("color", "#ffffff");
                // lblHeader3.Visible = false;

                Label lblHeader4 = new Label();
                lblHeader4.Text = "Cost";
                lblHeader4.CssClass = "lblCaption";
                lblHeader4.Style.Add("font-family", "Segoe UI,arial");
                lblHeader4.Style.Add("font-size", "13px");
                lblHeader4.Style.Add("width", "95%");
                lblHeader4.Style.Add("color", "#ffffff");

                Label lblHeader5 = new Label();
                lblHeader5.Text = "Comments";
                lblHeader5.CssClass = "lblCaption";
                lblHeader5.Style.Add("font-family", "Segoe UI,arial");
                lblHeader5.Style.Add("font-size", "13px");
                lblHeader5.Style.Add("width", "95%");
                lblHeader5.Style.Add("color", "#ffffff");

                tdTop1.Controls.Add(lblHeader1);
                tdTop2.Controls.Add(lblHeader2);
                tdTop3.Controls.Add(lblHeader3);
                tdTop4.Controls.Add(lblHeader4);
                tdTop5.Controls.Add(lblHeader5);

                trTop1.Controls.Add(tdTop1);
                trTop1.Controls.Add(tdTop2);
                trTop1.Controls.Add(tdTop3);
                trTop1.Controls.Add(tdTop4);
                trTop1.Controls.Add(tdTop5);
                trTop1.Controls.Add(tdTop6);


                PlaceHolder1.Controls.Add(trTop1);

                int j = 1;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    HtmlGenericControl trHe1 = new HtmlGenericControl("tr");

                    HtmlGenericControl tdHeTeeth = new HtmlGenericControl("td");
                    HtmlGenericControl tdHe1 = new HtmlGenericControl("td");
                    HtmlGenericControl tdHe2 = new HtmlGenericControl("td");
                    HtmlGenericControl tdHe3 = new HtmlGenericControl("td");
                    HtmlGenericControl tdHe4 = new HtmlGenericControl("td");
                    HtmlGenericControl tdHe5 = new HtmlGenericControl("td");



                    DropDownList drpTeeth = new DropDownList();
                    drpTeeth.ID = "drpTeeth" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    drpTeeth.CssClass = "lblCaption";
                    drpTeeth.Style.Add("width", "60px");
                    drpTeeth.Height = 24;
                    drpTeeth.CssClass = "label";
                    drpTeeth.DataSource = BuildTeathNo();
                    drpTeeth.DataValueField = "ToothNo";
                    drpTeeth.DataTextField = "ToothNo";
                    drpTeeth.DataBind();


                    drpTeeth.SelectedValue = Convert.ToString(DR["DDMR_TOOTH_NO"]);


                    //  EventHandler reh = new EventHandler(txtService_TextChange);

                    //  imgDel.Click += new System.Web.UI.ImageClickEventHandler(imgDel_Click);

                    TextBox txtBox1 = new TextBox();
                    txtBox1.ID = "txtTreatment" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    txtBox1.Text = Convert.ToString(DR["Treatment"]);
                    txtBox1.Style.Add("width", "200px");
                    txtBox1.Height = 20;
                    txtBox1.Style.Add("resize", "none");
                    txtBox1.CssClass = "label";
                    txtBox1.TextChanged += new
                        EventHandler(textBox_TextChanged);


                    TextBox txtBox2 = new TextBox();
                    txtBox2.ID = "txtICDDesc" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    txtBox2.Text = Convert.ToString(DR["ICDDesc"]);
                    txtBox2.Style.Add("width", "200px");
                    txtBox2.Height = 20;
                    txtBox2.Style.Add("resize", "none");
                    txtBox2.CssClass = "label";
                    //txtBox2.Visible = false;

                    TextBox txtBox3 = new TextBox();
                    txtBox3.ID = "txtCost" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    txtBox3.Text = Convert.ToString(DR["DDMR_TRNT_COST"]);
                    txtBox3.Style.Add("width", "50px");
                    txtBox3.Height = 20;
                    txtBox3.Style.Add("resize", "none");
                    txtBox3.CssClass = "label";



                    TextBox txtBox4 = new TextBox();
                    txtBox4.ID = "txtComments" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    txtBox4.Text = Convert.ToString(DR["DDMR_TRNT_COMMENTS"]);
                    txtBox4.Style.Add("width", "170px");
                    txtBox4.Height = 20;
                    txtBox4.Style.Add("resize", "none");
                    txtBox4.CssClass = "label";


                    TextBox txtBox5 = new TextBox();
                    txtBox5.ID = "txtSlNo" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    txtBox5.Text = Convert.ToString(DR["DDMR_SL_NO"]);
                    txtBox5.Style.Add("width", "50px");
                    txtBox5.Height = 20;
                    txtBox5.Style.Add("resize", "none");
                    txtBox5.CssClass = "label";
                    txtBox5.Visible = false;


                    ImageButton imgDel = new ImageButton();
                    imgDel.ID = "img" + j + Convert.ToString(DR["DDMR_TOOTH_NO"]);
                    imgDel.Style.Add("width", "16px");
                    imgDel.Style.Add("height", "16px");
                    imgDel.ImageUrl = "~/Images/icon_delete.jpg";
                    imgDel.AlternateText = Convert.ToString(DR["DDMR_TOOTH_NO"]) + "-" + Convert.ToString(DR["DDMR_SL_NO"]);
                    imgDel.Click += new System.Web.UI.ImageClickEventHandler(imgDel_Click);

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        imgDel.Visible = false;

                    }
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        imgDel.Visible = false;
                    }


                    AjaxControlToolkit.AutoCompleteExtender AutoCompExtServ = new AjaxControlToolkit.AutoCompleteExtender();
                    AutoCompExtServ.TargetControlID = txtBox1.ID;
                    AutoCompExtServ.ServiceMethod = "GetServiceID";
                    AutoCompExtServ.ServicePath = "~/EMR/Dental/Index.aspx";
                    AutoCompExtServ.ID = "txtAutoExtTreatment" + j; ;
                    AutoCompExtServ.CompletionListCssClass = "AutoExtender";
                    AutoCompExtServ.CompletionListItemCssClass = "AutoExtenderList";
                    AutoCompExtServ.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                    // AutoCompExtServ.CompletionListElementID = "divwidth";
                    // AutoCompExtServ.ContextKey = Convert.ToString(DR["DDMR_TRNT_ICD"]) + " - " + Convert.ToString(DR["DDMR_TRNT_ICD_DESCRIPTION"]);
                    AutoCompExtServ.UseContextKey = true;
                    AutoCompExtServ.CompletionInterval = 10;
                    AutoCompExtServ.CompletionSetCount = 15;
                    AutoCompExtServ.EnableCaching = true;
                    AutoCompExtServ.MinimumPrefixLength = 1;


                    AjaxControlToolkit.AutoCompleteExtender AutoCompExtDiag = new AjaxControlToolkit.AutoCompleteExtender();
                    AutoCompExtDiag.TargetControlID = txtBox2.ID;
                    AutoCompExtDiag.ServiceMethod = "GetDiagnosisID";
                    AutoCompExtDiag.ServicePath = "~/EMR/Dental/Index.aspx";
                    AutoCompExtDiag.ID = "txtAutoExtDiag" + j; ;
                    AutoCompExtDiag.CompletionListCssClass = "AutoExtender";
                    AutoCompExtDiag.CompletionListItemCssClass = "AutoExtenderList";
                    AutoCompExtDiag.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                    // AutoCompExtDiag.CompletionListElementID = "divwidth";
                    // AutoCompExtDiag.ContextKey = Convert.ToString(DR["DDMR_TRNT_ICD"]) + " - " + Convert.ToString(DR["DDMR_TRNT_ICD_DESCRIPTION"]);
                    AutoCompExtDiag.UseContextKey = true;
                    AutoCompExtDiag.CompletionInterval = 10;
                    AutoCompExtDiag.CompletionSetCount = 15;
                    AutoCompExtDiag.EnableCaching = true;
                    AutoCompExtDiag.MinimumPrefixLength = 1;






                    tdHeTeeth.Controls.Add(drpTeeth);
                    tdHe1.Controls.Add(txtBox1);
                    tdHe1.Controls.Add(AutoCompExtServ);

                    tdHe2.Controls.Add(txtBox2);
                    tdHe2.Controls.Add(AutoCompExtDiag);

                    tdHe3.Controls.Add(txtBox3);
                    tdHe4.Controls.Add(txtBox4);
                    tdHe4.Controls.Add(txtBox5);

                    tdHe5.Controls.Add(imgDel);



                    trHe1.Controls.Add(tdHeTeeth);
                    trHe1.Controls.Add(tdHe1);
                    trHe1.Controls.Add(tdHe2);
                    trHe1.Controls.Add(tdHe3);
                    trHe1.Controls.Add(tdHe4);
                    trHe1.Controls.Add(tdHe5);





                    PlaceHolder1.Controls.Add(trHe1);

                    j = j + 1;
                }
            }


        }

        public void generateDynamicControls()
        {
            BindDentalControls();

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                string strPageName = "", strToothNo = "";
                strPageName = Convert.ToString(Request.QueryString["PageName"]);
                strToothNo = Convert.ToString(Request.QueryString["ToothNo"]);

                BuildDentalTrans();

                if ((strPageName == "Dental" || strPageName == "Orthodontic") && strToothNo != "" && strToothNo != null)
                {
                    SaveDentalTrans(strToothNo);
                }
                BindDentalMaster();
                // BindDentalTrans();

                BindImageData();
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnNew.Visible = false;
                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnNew.Visible = false;
                }

            }

        }

        protected void imgDel_Click(object sender, EventArgs e)
        {
            ImageButton img1 = (ImageButton)sender;

            string strToothNo = img1.AlternateText;

            string[] arrID = strToothNo.Split('-');

            string strSlNo = "";
            if (arrID.Length > 0)
            {
                strToothNo = arrID[0];
                strSlNo = arrID[1];
            }


            CommonBAL objComm = new CommonBAL();
            string Criteria = " DDMR_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' and DDMR_TRANS_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND DDMR_TOOTH_NO='" + strToothNo + "'";
            if (strSlNo != "")
            {
                Criteria += " AND DDMR_SL_NO='" + strSlNo + "'";
            }

            objComm.fnDeleteTableData("EMR_DENTAL_TRANS", Criteria);
            Response.Redirect("Index.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {

            try
            {

                SaveDentalTrans("0");




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Dental.Treatment.btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("Index.aspx?PageName=Dental");



        }

        protected void textBox_TextChanged(object sender, EventArgs e)
        {
            // Your code here
        }


    }



}