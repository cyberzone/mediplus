﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.Dental
{
    public partial class CaseHistory : System.Web.UI.UserControl
    {
        CommonBAL objCom = new CommonBAL();
        EMR_CaseHistory objCH = new EMR_CaseHistory();
        DataSet DS = new DataSet();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindOralHygieneStatus()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND EOH_ID=" + Convert.ToString(Session["EMR_ID"]);

            DS = objCom.fnGetFieldValue(" * ", "EMR_ORALHYGIENESTATUS", Criteria, "EOH_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EOH_TYPE"]) == "DEBRIS_SCORE")
                    {
                        txtDS16.Text = Convert.ToString(DR["EOH_16"]);
                        txtDS11.Text = Convert.ToString(DR["EOH_11"]);
                        txtDSX.Text = Convert.ToString(DR["EOH_X"]);
                        txtDS26.Text = Convert.ToString(DR["EOH_26"]);

                        txtDS46.Text = Convert.ToString(DR["EOH_46"]);
                        txtDSX_1.Text = Convert.ToString(DR["EOH_X_1"]);

                        txtDS31.Text = Convert.ToString(DR["EOH_31"]);
                        txtDS36.Text = Convert.ToString(DR["EOH_36"]);


                    }


                    if (Convert.ToString(DR["EOH_TYPE"]) == "CALCULUS")
                    {
                        txtCA16.Text = Convert.ToString(DR["EOH_16"]);
                        txtCA11.Text = Convert.ToString(DR["EOH_11"]);
                        txtCAX.Text = Convert.ToString(DR["EOH_X"]);
                        txtCA26.Text = Convert.ToString(DR["EOH_26"]);

                        txtCA46.Text = Convert.ToString(DR["EOH_46"]);
                        txtCAX_1.Text = Convert.ToString(DR["EOH_X_1"]);

                        txtCA31.Text = Convert.ToString(DR["EOH_31"]);
                        txtCA36.Text = Convert.ToString(DR["EOH_36"]);
                    }


                    lblTotalScore.Text = Convert.ToString(DR["EOH_TOTAL_SCORE"]);
                    hidTotalScore.Value = Convert.ToString(DR["EOH_TOTAL_SCORE"]);

                }
            }
        }


        void BindGingivalStatus()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND EGS_ID=" + Convert.ToString(Session["EMR_ID"]);

            DS = objCom.fnGetFieldValue(" * ", "EMR_GINGIVALSTATUS", Criteria, "EGS_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EGS_TYPE"]) == "CLINICAL_FEATURES")
                    {

                        txtColorCF.Text = Convert.ToString(DR["EGS_COLOR"]);
                        txtContourCF.Text = Convert.ToString(DR["EGS_CONTOUR"]);
                        txtSizeCF.Text = Convert.ToString(DR["EGS_SIZE"]);
                        txtConsistencyCF.Text = Convert.ToString(DR["EGS_CONSISTENCY"]);

                        txtPositionCF.Text = Convert.ToString(DR["EGS_POSITION"]);
                        txtBleedingProbingCF.Text = Convert.ToString(DR["EGS_BLEEDING_PROBING"]);


                    }


                    if (Convert.ToString(DR["EGS_TYPE"]) == "TOOTH_SEGMENT")
                    {
                        txtColorTS.Text = Convert.ToString(DR["EGS_COLOR"]);
                        txtContourTS.Text = Convert.ToString(DR["EGS_CONTOUR"]);
                        txtSizeTS.Text = Convert.ToString(DR["EGS_SIZE"]);
                        txtConsistencyTS.Text = Convert.ToString(DR["EGS_CONSISTENCY"]);

                        txtPositionTS.Text = Convert.ToString(DR["EGS_POSITION"]);
                        txtBleedingProbingTS.Text = Convert.ToString(DR["EGS_BLEEDING_PROBING"]);


                    }




                }
            }
        }

        void BindPeriodontalStatus()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND EPS_ID=" + Convert.ToString(Session["EMR_ID"]);

            DS = objCom.fnGetFieldValue(" * ", "EMR_PERIODONTAL_STATUS", Criteria, "EPS_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["EPS_TYPE"]) == "PROBING_DEPTH")
                    {

                        txtProbingDepth18.Text = Convert.ToString(DR["EPS_18"]);
                        txtProbingDepth17.Text = Convert.ToString(DR["EPS_17"]);
                        txtProbingDepth16.Text = Convert.ToString(DR["EPS_16"]);
                        txtProbingDepth15.Text = Convert.ToString(DR["EPS_15"]);
                        txtProbingDepth14.Text = Convert.ToString(DR["EPS_14"]);
                        txtProbingDepth13.Text = Convert.ToString(DR["EPS_13"]);
                        txtProbingDepth12.Text = Convert.ToString(DR["EPS_12"]);
                        txtProbingDepth11.Text = Convert.ToString(DR["EPS_11"]);


                        txtProbingDepth21.Text = Convert.ToString(DR["EPS_21"]);
                        txtProbingDepth22.Text = Convert.ToString(DR["EPS_22"]);
                        txtProbingDepth23.Text = Convert.ToString(DR["EPS_23"]);
                        txtProbingDepth24.Text = Convert.ToString(DR["EPS_24"]);
                        txtProbingDepth25.Text = Convert.ToString(DR["EPS_25"]);
                        txtProbingDepth26.Text = Convert.ToString(DR["EPS_26"]);
                        txtProbingDepth27.Text = Convert.ToString(DR["EPS_27"]);
                        txtProbingDepth28.Text = Convert.ToString(DR["EPS_28"]);


                        txtProbingDepth48.Text = Convert.ToString(DR["EPS_48"]);
                        txtProbingDepth47.Text = Convert.ToString(DR["EPS_47"]);
                        txtProbingDepth46.Text = Convert.ToString(DR["EPS_46"]);
                        txtProbingDepth45.Text = Convert.ToString(DR["EPS_45"]);
                        txtProbingDepth44.Text = Convert.ToString(DR["EPS_44"]);
                        txtProbingDepth43.Text = Convert.ToString(DR["EPS_43"]);
                        txtProbingDepth42.Text = Convert.ToString(DR["EPS_42"]);
                        txtProbingDepth41.Text = Convert.ToString(DR["EPS_41"]);

                        txtProbingDepth31.Text = Convert.ToString(DR["EPS_31"]);
                        txtProbingDepth32.Text = Convert.ToString(DR["EPS_32"]);
                        txtProbingDepth33.Text = Convert.ToString(DR["EPS_33"]);
                        txtProbingDepth34.Text = Convert.ToString(DR["EPS_34"]);
                        txtProbingDepth35.Text = Convert.ToString(DR["EPS_35"]);
                        txtProbingDepth36.Text = Convert.ToString(DR["EPS_36"]);
                        txtProbingDepth37.Text = Convert.ToString(DR["EPS_37"]);
                        txtProbingDepth38.Text = Convert.ToString(DR["EPS_38"]);

                    }


                    if (Convert.ToString(DR["EPS_TYPE"]) == "MOBILITY")
                    {
                        txtMobility18.Text = Convert.ToString(DR["EPS_18"]);
                        txtMobility17.Text = Convert.ToString(DR["EPS_17"]);
                        txtMobility16.Text = Convert.ToString(DR["EPS_16"]);
                        txtMobility15.Text = Convert.ToString(DR["EPS_15"]);
                        txtMobility14.Text = Convert.ToString(DR["EPS_14"]);
                        txtMobility13.Text = Convert.ToString(DR["EPS_13"]);
                        txtMobility12.Text = Convert.ToString(DR["EPS_12"]);
                        txtMobility11.Text = Convert.ToString(DR["EPS_11"]);


                        txtMobility21.Text = Convert.ToString(DR["EPS_21"]);
                        txtMobility22.Text = Convert.ToString(DR["EPS_22"]);
                        txtMobility23.Text = Convert.ToString(DR["EPS_23"]);
                        txtMobility24.Text = Convert.ToString(DR["EPS_24"]);
                        txtMobility25.Text = Convert.ToString(DR["EPS_25"]);
                        txtMobility26.Text = Convert.ToString(DR["EPS_26"]);
                        txtMobility27.Text = Convert.ToString(DR["EPS_27"]);
                        txtMobility28.Text = Convert.ToString(DR["EPS_28"]);


                        txtMobility48.Text = Convert.ToString(DR["EPS_48"]);
                        txtMobility47.Text = Convert.ToString(DR["EPS_47"]);
                        txtMobility46.Text = Convert.ToString(DR["EPS_46"]);
                        txtMobility45.Text = Convert.ToString(DR["EPS_45"]);
                        txtMobility44.Text = Convert.ToString(DR["EPS_44"]);
                        txtMobility43.Text = Convert.ToString(DR["EPS_43"]);
                        txtMobility42.Text = Convert.ToString(DR["EPS_42"]);
                        txtMobility41.Text = Convert.ToString(DR["EPS_41"]);

                        txtMobility31.Text = Convert.ToString(DR["EPS_31"]);
                        txtMobility32.Text = Convert.ToString(DR["EPS_32"]);
                        txtMobility33.Text = Convert.ToString(DR["EPS_33"]);
                        txtMobility34.Text = Convert.ToString(DR["EPS_34"]);
                        txtMobility35.Text = Convert.ToString(DR["EPS_35"]);
                        txtMobility36.Text = Convert.ToString(DR["EPS_36"]);
                        txtMobility37.Text = Convert.ToString(DR["EPS_37"]);
                        txtMobility38.Text = Convert.ToString(DR["EPS_38"]);


                    }


                    if (Convert.ToString(DR["EPS_TYPE"]) == "FURCATION")
                    {
                        txtFurcation18.Text = Convert.ToString(DR["EPS_18"]);
                        txtFurcation17.Text = Convert.ToString(DR["EPS_17"]);
                        txtFurcation16.Text = Convert.ToString(DR["EPS_16"]);
                        txtFurcation15.Text = Convert.ToString(DR["EPS_15"]);
                        txtFurcation14.Text = Convert.ToString(DR["EPS_14"]);
                        txtFurcation13.Text = Convert.ToString(DR["EPS_13"]);
                        txtFurcation12.Text = Convert.ToString(DR["EPS_12"]);
                        txtFurcation11.Text = Convert.ToString(DR["EPS_11"]);


                        txtFurcation21.Text = Convert.ToString(DR["EPS_21"]);
                        txtFurcation22.Text = Convert.ToString(DR["EPS_22"]);
                        txtFurcation23.Text = Convert.ToString(DR["EPS_23"]);
                        txtFurcation24.Text = Convert.ToString(DR["EPS_24"]);
                        txtFurcation25.Text = Convert.ToString(DR["EPS_25"]);
                        txtFurcation26.Text = Convert.ToString(DR["EPS_26"]);
                        txtFurcation27.Text = Convert.ToString(DR["EPS_27"]);
                        txtFurcation28.Text = Convert.ToString(DR["EPS_28"]);


                        txtFurcation48.Text = Convert.ToString(DR["EPS_48"]);
                        txtFurcation47.Text = Convert.ToString(DR["EPS_47"]);
                        txtFurcation46.Text = Convert.ToString(DR["EPS_46"]);
                        txtFurcation45.Text = Convert.ToString(DR["EPS_45"]);
                        txtFurcation44.Text = Convert.ToString(DR["EPS_44"]);
                        txtFurcation43.Text = Convert.ToString(DR["EPS_43"]);
                        txtFurcation42.Text = Convert.ToString(DR["EPS_42"]);
                        txtFurcation41.Text = Convert.ToString(DR["EPS_41"]);

                        txtFurcation31.Text = Convert.ToString(DR["EPS_31"]);
                        txtFurcation32.Text = Convert.ToString(DR["EPS_32"]);
                        txtFurcation33.Text = Convert.ToString(DR["EPS_33"]);
                        txtFurcation34.Text = Convert.ToString(DR["EPS_34"]);
                        txtFurcation35.Text = Convert.ToString(DR["EPS_35"]);
                        txtFurcation36.Text = Convert.ToString(DR["EPS_36"]);
                        txtFurcation37.Text = Convert.ToString(DR["EPS_37"]);
                        txtFurcation38.Text = Convert.ToString(DR["EPS_38"]);



                    }



                    if (Convert.ToString(DR["EPS_TYPE"]) == "RECESSION")
                    {
                        txtRecession18.Text = Convert.ToString(DR["EPS_18"]);
                        txtRecession17.Text = Convert.ToString(DR["EPS_17"]);
                        txtRecession16.Text = Convert.ToString(DR["EPS_16"]);
                        txtRecession15.Text = Convert.ToString(DR["EPS_15"]);
                        txtRecession14.Text = Convert.ToString(DR["EPS_14"]);
                        txtRecession13.Text = Convert.ToString(DR["EPS_13"]);
                        txtRecession12.Text = Convert.ToString(DR["EPS_12"]);
                        txtRecession11.Text = Convert.ToString(DR["EPS_11"]);


                        txtRecession21.Text = Convert.ToString(DR["EPS_21"]);
                        txtRecession22.Text = Convert.ToString(DR["EPS_22"]);
                        txtRecession23.Text = Convert.ToString(DR["EPS_23"]);
                        txtRecession24.Text = Convert.ToString(DR["EPS_24"]);
                        txtRecession25.Text = Convert.ToString(DR["EPS_25"]);
                        txtRecession26.Text = Convert.ToString(DR["EPS_26"]);
                        txtRecession27.Text = Convert.ToString(DR["EPS_27"]);
                        txtRecession28.Text = Convert.ToString(DR["EPS_28"]);


                        txtRecession48.Text = Convert.ToString(DR["EPS_48"]);
                        txtRecession47.Text = Convert.ToString(DR["EPS_47"]);
                        txtRecession46.Text = Convert.ToString(DR["EPS_46"]);
                        txtRecession45.Text = Convert.ToString(DR["EPS_45"]);
                        txtRecession44.Text = Convert.ToString(DR["EPS_44"]);
                        txtRecession43.Text = Convert.ToString(DR["EPS_43"]);
                        txtRecession42.Text = Convert.ToString(DR["EPS_42"]);
                        txtRecession41.Text = Convert.ToString(DR["EPS_41"]);

                        txtRecession31.Text = Convert.ToString(DR["EPS_31"]);
                        txtRecession32.Text = Convert.ToString(DR["EPS_32"]);
                        txtRecession33.Text = Convert.ToString(DR["EPS_33"]);
                        txtRecession34.Text = Convert.ToString(DR["EPS_34"]);
                        txtRecession35.Text = Convert.ToString(DR["EPS_35"]);
                        txtRecession36.Text = Convert.ToString(DR["EPS_36"]);
                        txtRecession37.Text = Convert.ToString(DR["EPS_37"]);
                        txtRecession38.Text = Convert.ToString(DR["EPS_38"]);


                    }


                    if (Convert.ToString(DR["EPS_TYPE"]) == "LOSSOFATT")
                    {
                        txtLossOfAtt18.Text = Convert.ToString(DR["EPS_18"]);
                        txtLossOfAtt17.Text = Convert.ToString(DR["EPS_17"]);
                        txtLossOfAtt16.Text = Convert.ToString(DR["EPS_16"]);
                        txtLossOfAtt15.Text = Convert.ToString(DR["EPS_15"]);
                        txtLossOfAtt14.Text = Convert.ToString(DR["EPS_14"]);
                        txtLossOfAtt13.Text = Convert.ToString(DR["EPS_13"]);
                        txtLossOfAtt12.Text = Convert.ToString(DR["EPS_12"]);
                        txtLossOfAtt11.Text = Convert.ToString(DR["EPS_11"]);


                        txtLossOfAtt21.Text = Convert.ToString(DR["EPS_21"]);
                        txtLossOfAtt22.Text = Convert.ToString(DR["EPS_22"]);
                        txtLossOfAtt23.Text = Convert.ToString(DR["EPS_23"]);
                        txtLossOfAtt24.Text = Convert.ToString(DR["EPS_24"]);
                        txtLossOfAtt25.Text = Convert.ToString(DR["EPS_25"]);
                        txtLossOfAtt26.Text = Convert.ToString(DR["EPS_26"]);
                        txtLossOfAtt27.Text = Convert.ToString(DR["EPS_27"]);
                        txtLossOfAtt28.Text = Convert.ToString(DR["EPS_28"]);


                        txtLossOfAtt48.Text = Convert.ToString(DR["EPS_48"]);
                        txtLossOfAtt47.Text = Convert.ToString(DR["EPS_47"]);
                        txtLossOfAtt46.Text = Convert.ToString(DR["EPS_46"]);
                        txtLossOfAtt45.Text = Convert.ToString(DR["EPS_45"]);
                        txtLossOfAtt44.Text = Convert.ToString(DR["EPS_44"]);
                        txtLossOfAtt43.Text = Convert.ToString(DR["EPS_43"]);
                        txtLossOfAtt42.Text = Convert.ToString(DR["EPS_42"]);
                        txtLossOfAtt41.Text = Convert.ToString(DR["EPS_41"]);

                        txtLossOfAtt31.Text = Convert.ToString(DR["EPS_31"]);
                        txtLossOfAtt32.Text = Convert.ToString(DR["EPS_32"]);
                        txtLossOfAtt33.Text = Convert.ToString(DR["EPS_33"]);
                        txtLossOfAtt34.Text = Convert.ToString(DR["EPS_34"]);
                        txtLossOfAtt35.Text = Convert.ToString(DR["EPS_35"]);
                        txtLossOfAtt36.Text = Convert.ToString(DR["EPS_36"]);
                        txtLossOfAtt37.Text = Convert.ToString(DR["EPS_37"]);
                        txtLossOfAtt38.Text = Convert.ToString(DR["EPS_38"]);


                    }


                }
            }
        }

        void BindDentalMaster()
        {
            DataSet DS = new DataSet();
            EMR_Dental objDental = new EMR_Dental();

            string Criteria = " 1=1 ";

            Criteria += " AND DDMR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND DDMR_TRANS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objDental.EMRDentalMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                txtToothBrush.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_TOOTH_BRUSH"]);
                txtBrushingTechnique.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_BRUSHING_TECHNIQUE"]);

                txtRootCanalTreated.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_ROOTCANALTREATE"]);
                txtPeriapicalLesion.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_PERIAPICALLESION"]);

                txtRemainingRoots.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_REMAININGROOTS"]);
                txtDMFScore.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_DMFSCORE"]);

             
                radAngleClassifi.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_ANGLE_CLASSIFI"]);

                txtOverjet.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_OVERJET"]);
                txtOverbite.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_OVERBITE"]);

                drpParafunctionalHabits.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_PARAFUNCTIONALHABITS"]);
                txtToothBrush.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_TOOTH_BRUSH"]);



                string DDMR_DENTAL_FLOSS = "0", DDMR_TOOTHPICKS = "0", DDMR_INTERPROXIMAL_BRUSHES = "0";

                if(DS.Tables[0].Rows[0].IsNull("DDMR_DENTAL_FLOSS") == false)
                chkDentalfloss.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["DDMR_DENTAL_FLOSS"]);
                if (DS.Tables[0].Rows[0].IsNull("DDMR_TOOTHPICKS") == false)
                chkToothpicks.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["DDMR_TOOTHPICKS"]);
                if (DS.Tables[0].Rows[0].IsNull("DDMR_INTERPROXIMAL_BRUSHES") == false)
                chkInterproximalBrushes.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["DDMR_INTERPROXIMAL_BRUSHES"]);


                txtRadiographicFindings.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_RADIOGRAPHIC_FINDINGS"]);
                txtPrognosis.Text = Convert.ToString(DS.Tables[0].Rows[0]["DDMR_PROGNOSIS"]);

            }
        }


        void SaveOralHygieneStatus()
        {
            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EOH_TYPE = "DEBRIS_SCORE";
            objCH.EOH_16 = txtDS16.Text;
            objCH.EOH_11 = txtDS11.Text;
            objCH.EOH_X = txtDSX.Text;
            objCH.EOH_26 = txtDS26.Text;
            objCH.EOH_46 = txtDS46.Text;
            objCH.EOH_X_1 = txtDSX_1.Text;
            objCH.EOH_31 = txtDS31.Text;
            objCH.EOH_36 = txtDS36.Text;
            objCH.EOH_TOTAL_SCORE = hidTotalScore.Value;
            objCH.OralHygieneStatusAdd();


            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EOH_TYPE = "CALCULUS";
            objCH.EOH_16 = txtCA16.Text;
            objCH.EOH_11 = txtCA11.Text;
            objCH.EOH_X = txtCAX.Text;
            objCH.EOH_26 = txtCA26.Text;
            objCH.EOH_46 = txtCA46.Text;
            objCH.EOH_X_1 = txtCAX_1.Text;
            objCH.EOH_31 = txtCA31.Text;
            objCH.EOH_36 = txtCA36.Text;
            objCH.EOH_TOTAL_SCORE = hidTotalScore.Value;


            objCH.OralHygieneStatusAdd();
            BindOralHygieneStatus();
        }

        void SaveGingivalStatus()
        {
            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EGS_TYPE = "CLINICAL_FEATURES";
            objCH.EGS_COLOR = txtColorCF.Text;
            objCH.EGS_CONTOUR = txtContourCF.Text;
            objCH.EGS_SIZE = txtSizeCF.Text;
            objCH.EGS_CONSISTENCY = txtConsistencyCF.Text;
            objCH.EGS_POSITION = txtPositionCF.Text;
            objCH.EGS_BLEEDING_PROBING = txtBleedingProbingCF.Text;

            objCH.GingivalStatusAdd();


            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EGS_TYPE = "TOOTH_SEGMENT";
            objCH.EGS_COLOR = txtColorTS.Text;
            objCH.EGS_CONTOUR = txtContourTS.Text;
            objCH.EGS_SIZE = txtSizeTS.Text;
            objCH.EGS_CONSISTENCY = txtConsistencyTS.Text;
            objCH.EGS_POSITION = txtPositionTS.Text;
            objCH.EGS_BLEEDING_PROBING = txtBleedingProbingTS.Text;


            objCH.GingivalStatusAdd();


        }

        void SavePeriodontalStatus()
        {
            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EPS_TYPE = "PROBING_DEPTH";
            objCH.EPS_18 = txtProbingDepth18.Text;
            objCH.EPS_17 = txtProbingDepth17.Text;
            objCH.EPS_16 = txtProbingDepth16.Text;
            objCH.EPS_15 = txtProbingDepth15.Text;
            objCH.EPS_14 = txtProbingDepth14.Text;
            objCH.EPS_13 = txtProbingDepth13.Text;
            objCH.EPS_12 = txtProbingDepth12.Text;
            objCH.EPS_11 = txtProbingDepth11.Text;
            objCH.EPS_21 = txtProbingDepth21.Text;
            objCH.EPS_22 = txtProbingDepth22.Text;
            objCH.EPS_23 = txtProbingDepth23.Text;
            objCH.EPS_24 = txtProbingDepth24.Text;
            objCH.EPS_25 = txtProbingDepth25.Text;
            objCH.EPS_26 = txtProbingDepth26.Text;
            objCH.EPS_27 = txtProbingDepth27.Text;
            objCH.EPS_28 = txtProbingDepth28.Text;
            objCH.EPS_48 = txtProbingDepth48.Text;
            objCH.EPS_47 = txtProbingDepth47.Text;
            objCH.EPS_46 = txtProbingDepth46.Text;
            objCH.EPS_45 = txtProbingDepth45.Text;
            objCH.EPS_44 = txtProbingDepth44.Text;
            objCH.EPS_43 = txtProbingDepth43.Text;
            objCH.EPS_42 = txtProbingDepth42.Text;
            objCH.EPS_41 = txtProbingDepth41.Text;
            objCH.EPS_31 = txtProbingDepth31.Text;
            objCH.EPS_32 = txtProbingDepth32.Text;
            objCH.EPS_33 = txtProbingDepth33.Text;
            objCH.EPS_34 = txtProbingDepth34.Text;
            objCH.EPS_35 = txtProbingDepth35.Text;
            objCH.EPS_36 = txtProbingDepth36.Text;
            objCH.EPS_37 = txtProbingDepth37.Text;
            objCH.EPS_38 = txtProbingDepth38.Text;
            objCH.PeriodontalStatusAdd();


            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EPS_TYPE = "MOBILITY";
            objCH.EPS_18 = txtMobility18.Text;
            objCH.EPS_17 = txtMobility17.Text;
            objCH.EPS_16 = txtMobility16.Text;
            objCH.EPS_15 = txtMobility15.Text;
            objCH.EPS_14 = txtMobility14.Text;
            objCH.EPS_13 = txtMobility13.Text;
            objCH.EPS_12 = txtMobility12.Text;
            objCH.EPS_11 = txtMobility11.Text;
            objCH.EPS_21 = txtMobility21.Text;
            objCH.EPS_22 = txtMobility22.Text;
            objCH.EPS_23 = txtMobility23.Text;
            objCH.EPS_24 = txtMobility24.Text;
            objCH.EPS_25 = txtMobility25.Text;
            objCH.EPS_26 = txtMobility26.Text;
            objCH.EPS_27 = txtMobility27.Text;
            objCH.EPS_28 = txtMobility28.Text;
            objCH.EPS_48 = txtMobility48.Text;
            objCH.EPS_47 = txtMobility47.Text;
            objCH.EPS_46 = txtMobility46.Text;
            objCH.EPS_45 = txtMobility45.Text;
            objCH.EPS_44 = txtMobility44.Text;
            objCH.EPS_43 = txtMobility43.Text;
            objCH.EPS_42 = txtMobility42.Text;
            objCH.EPS_41 = txtMobility41.Text;
            objCH.EPS_31 = txtMobility31.Text;
            objCH.EPS_32 = txtMobility32.Text;
            objCH.EPS_33 = txtMobility33.Text;
            objCH.EPS_34 = txtMobility34.Text;
            objCH.EPS_35 = txtMobility35.Text;
            objCH.EPS_36 = txtMobility36.Text;
            objCH.EPS_37 = txtMobility37.Text;
            objCH.EPS_38 = txtMobility38.Text;
            objCH.PeriodontalStatusAdd();

            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EPS_TYPE = "FURCATION";
            objCH.EPS_18 = txtFurcation18.Text;
            objCH.EPS_17 = txtFurcation17.Text;
            objCH.EPS_16 = txtFurcation16.Text;
            objCH.EPS_15 = txtFurcation15.Text;
            objCH.EPS_14 = txtFurcation14.Text;
            objCH.EPS_13 = txtFurcation13.Text;
            objCH.EPS_12 = txtFurcation12.Text;
            objCH.EPS_11 = txtFurcation11.Text;
            objCH.EPS_21 = txtFurcation21.Text;
            objCH.EPS_22 = txtFurcation22.Text;
            objCH.EPS_23 = txtFurcation23.Text;
            objCH.EPS_24 = txtFurcation24.Text;
            objCH.EPS_25 = txtFurcation25.Text;
            objCH.EPS_26 = txtFurcation26.Text;
            objCH.EPS_27 = txtFurcation27.Text;
            objCH.EPS_28 = txtFurcation28.Text;
            objCH.EPS_48 = txtFurcation48.Text;
            objCH.EPS_47 = txtFurcation47.Text;
            objCH.EPS_46 = txtFurcation46.Text;
            objCH.EPS_45 = txtFurcation45.Text;
            objCH.EPS_44 = txtFurcation44.Text;
            objCH.EPS_43 = txtFurcation43.Text;
            objCH.EPS_42 = txtFurcation42.Text;
            objCH.EPS_41 = txtFurcation41.Text;
            objCH.EPS_31 = txtFurcation31.Text;
            objCH.EPS_32 = txtFurcation32.Text;
            objCH.EPS_33 = txtFurcation33.Text;
            objCH.EPS_34 = txtFurcation34.Text;
            objCH.EPS_35 = txtFurcation35.Text;
            objCH.EPS_36 = txtFurcation36.Text;
            objCH.EPS_37 = txtFurcation37.Text;
            objCH.EPS_38 = txtFurcation38.Text;
            objCH.PeriodontalStatusAdd();


            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EPS_TYPE = "RECESSION";
            objCH.EPS_18 = txtRecession18.Text;
            objCH.EPS_17 = txtRecession17.Text;
            objCH.EPS_16 = txtRecession16.Text;
            objCH.EPS_15 = txtRecession15.Text;
            objCH.EPS_14 = txtRecession14.Text;
            objCH.EPS_13 = txtRecession13.Text;
            objCH.EPS_12 = txtRecession12.Text;
            objCH.EPS_11 = txtRecession11.Text;
            objCH.EPS_21 = txtRecession21.Text;
            objCH.EPS_22 = txtRecession22.Text;
            objCH.EPS_23 = txtRecession23.Text;
            objCH.EPS_24 = txtRecession24.Text;
            objCH.EPS_25 = txtRecession25.Text;
            objCH.EPS_26 = txtRecession26.Text;
            objCH.EPS_27 = txtRecession27.Text;
            objCH.EPS_28 = txtRecession28.Text;
            objCH.EPS_48 = txtRecession48.Text;
            objCH.EPS_47 = txtRecession47.Text;
            objCH.EPS_46 = txtRecession46.Text;
            objCH.EPS_45 = txtRecession45.Text;
            objCH.EPS_44 = txtRecession44.Text;
            objCH.EPS_43 = txtRecession43.Text;
            objCH.EPS_42 = txtRecession42.Text;
            objCH.EPS_41 = txtRecession41.Text;
            objCH.EPS_31 = txtRecession31.Text;
            objCH.EPS_32 = txtRecession32.Text;
            objCH.EPS_33 = txtRecession33.Text;
            objCH.EPS_34 = txtRecession34.Text;
            objCH.EPS_35 = txtRecession35.Text;
            objCH.EPS_36 = txtRecession36.Text;
            objCH.EPS_37 = txtRecession37.Text;
            objCH.EPS_38 = txtRecession38.Text;
            objCH.PeriodontalStatusAdd();

            objCH = new EMR_CaseHistory();
            objCH.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCH.EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objCH.EPS_TYPE = "LOSSOFATT";
            objCH.EPS_18 = txtLossOfAtt18.Text;
            objCH.EPS_17 = txtLossOfAtt17.Text;
            objCH.EPS_16 = txtLossOfAtt16.Text;
            objCH.EPS_15 = txtLossOfAtt15.Text;
            objCH.EPS_14 = txtLossOfAtt14.Text;
            objCH.EPS_13 = txtLossOfAtt13.Text;
            objCH.EPS_12 = txtLossOfAtt12.Text;
            objCH.EPS_11 = txtLossOfAtt11.Text;
            objCH.EPS_21 = txtLossOfAtt21.Text;
            objCH.EPS_22 = txtLossOfAtt22.Text;
            objCH.EPS_23 = txtLossOfAtt23.Text;
            objCH.EPS_24 = txtLossOfAtt24.Text;
            objCH.EPS_25 = txtLossOfAtt25.Text;
            objCH.EPS_26 = txtLossOfAtt26.Text;
            objCH.EPS_27 = txtLossOfAtt27.Text;
            objCH.EPS_28 = txtLossOfAtt28.Text;
            objCH.EPS_48 = txtLossOfAtt48.Text;
            objCH.EPS_47 = txtLossOfAtt47.Text;
            objCH.EPS_46 = txtLossOfAtt46.Text;
            objCH.EPS_45 = txtLossOfAtt45.Text;
            objCH.EPS_44 = txtLossOfAtt44.Text;
            objCH.EPS_43 = txtLossOfAtt43.Text;
            objCH.EPS_42 = txtLossOfAtt42.Text;
            objCH.EPS_41 = txtLossOfAtt41.Text;
            objCH.EPS_31 = txtLossOfAtt31.Text;
            objCH.EPS_32 = txtLossOfAtt32.Text;
            objCH.EPS_33 = txtLossOfAtt33.Text;
            objCH.EPS_34 = txtLossOfAtt34.Text;
            objCH.EPS_35 = txtLossOfAtt35.Text;
            objCH.EPS_36 = txtLossOfAtt36.Text;
            objCH.EPS_37 = txtLossOfAtt37.Text;
            objCH.EPS_38 = txtLossOfAtt38.Text;
            objCH.PeriodontalStatusAdd();




        }

        void UpdateDentalMaster()
        {
            objCom = new CommonBAL();
            DS = new DataSet();

            string DDMR_DENTAL_FLOSS = "0", DDMR_TOOTHPICKS = "0", DDMR_INTERPROXIMAL_BRUSHES = "0";

            if (chkDentalfloss.Checked == true)
            {
                DDMR_DENTAL_FLOSS = "1";
            }

            if (chkToothpicks.Checked == true)
            {
                DDMR_TOOTHPICKS = "1";
            }

            if (chkInterproximalBrushes.Checked == true)
            {
                DDMR_INTERPROXIMAL_BRUSHES = "1";
            }
            //string Criteria = " 1=1 ";

            //Criteria += " AND DDMR_TRANS_ID=" + Convert.ToString(Session["EMR_ID"]);


            //string FieldNameWithValues = " DDMR_TOOTH_BRUSH='" + txtToothBrush.Text + "', DDMR_BRUSHING_TECHNIQUE='" + txtBrushingTechnique.Text +"'," ;
            //FieldNameWithValues += "DDMR_DENTAL_FLOSS=" + DDMR_DENTAL_FLOSS + ", DDMR_TOOTHPICKS=" + DDMR_TOOTHPICKS + ",DDMR_INTERPROXIMAL_BRUSHES=" + DDMR_INTERPROXIMAL_BRUSHES + ",";
            //   FieldNameWithValues += "DDMR_RADIOGRAPHIC_FINDINGS='" + txtRadiographicFindings.Text + "',DDMR_PROGNOSIS='" + txtPrognosis.Text + "'"; 

            //objCom.fnUpdateTableData(FieldNameWithValues, "EMR_DENTAL_MASTER", Criteria);

            EMR_Dental objDe = new EMR_Dental();

            objDe.DDMR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objDe.DDMR_TRANS_ID = Convert.ToString(Session["EMR_ID"]);
            objDe.DDMR_ROOTCANALTREATE = txtRootCanalTreated.Text;
            objDe.DDMR_PERIAPICALLESION = txtPeriapicalLesion.Text;
            objDe.DDMR_REMAININGROOTS = txtRemainingRoots.Text;
            objDe.DDMR_DMFSCORE = txtDMFScore.Text;
            objDe.DDMR_ANGLE_CLASSIFI = radAngleClassifi.SelectedValue;
            objDe.DDMR_OVERJET = txtOverjet.Text;
            objDe.DDMR_OVERBITE = txtOverbite.Text;
            objDe.DDMR_PARAFUNCTIONALHABITS = drpParafunctionalHabits.SelectedValue;
            objDe.DDMR_TOOTH_BRUSH = txtToothBrush.Text;
            objDe.DDMR_BRUSHING_TECHNIQUE = txtBrushingTechnique.Text;
            objDe.DDMR_DENTAL_FLOSS = DDMR_DENTAL_FLOSS;
            objDe.DDMR_TOOTHPICKS = DDMR_TOOTHPICKS;
            objDe.DDMR_INTERPROXIMAL_BRUSHES = DDMR_INTERPROXIMAL_BRUSHES;
            objDe.DDMR_RADIOGRAPHIC_FINDINGS = txtRadiographicFindings.Text;
            objDe.DDMR_PROGNOSIS = txtPrognosis.Text;
            objDe.EMRDentalMasterUpdate();

        }

        public void SaveCaseHistory()
        {
            SaveOralHygieneStatus();
            SaveGingivalStatus();
            SavePeriodontalStatus();

            UpdateDentalMaster();
        }



        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {
                        BindOralHygieneStatus();
                        BindGingivalStatus();
                        BindPeriodontalStatus();
                        BindDentalMaster();
                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CaseHistory.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }
    }
}