﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR
{
    public partial class PatientHeader : System.Web.UI.MasterPage
    {
        CommonBAL objCom = new CommonBAL();
        public string Allergy = "", SocialHistory = "", PastHistory = "";

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindEMR()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "' AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                lblEMRDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["EPM_DATEDesc"]);
               
               


                lblEMR_ID.Text = Convert.ToString(ds.Tables[0].Rows[0]["EPM_ID"]);

                string RefDrID = "";

                if (ds.Tables[0].Rows[0].IsNull("EPM_PT_ID") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_PT_ID"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PT_ID"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPTID.Text = str;
                    lblPTID.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PT_ID"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_REF_DR_CODE") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_REF_DR_CODE"]) != "")
                {
                    RefDrID = Convert.ToString(ds.Tables[0].Rows[0]["EPM_REF_DR_CODE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_STATUS") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]) != "")
                {
                     

                    if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]).ToUpper() == "Y")
                    {
                        chkProcComp.Checked = true;
                    }
                    else
                    {
                        chkProcComp.Checked = false;
                    }
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_VIP") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]) != "")
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]).ToUpper() == "1")
                    {
                        chkVIP.Checked = true;
                    }
                    else
                    {
                        chkVIP.Checked = false;
                    }
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_ROYAL") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]) != "")
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]).ToUpper() == "1")
                    {
                        chkRoyal.Checked = true;
                    }
                    else
                    {
                        chkRoyal.Checked = false;
                    }
                }


                if (ds.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblBeginDt.Text = str;
                    lblBeginDt.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(ds.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["EPM_END_DATEDesc"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblEndDt.Text = str;
                    lblEndDt.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["EPM_END_DATEDesc"]);
                }


             
                BindPTDetails();
                BindPatientPhoto();

                if (RefDrID != "")
                {
                    string RefDrName = "";
                    RefDrName = BindRefDrData(RefDrID);

                    if (RefDrName != "")
                    {
                        string str = RefDrName;
                        if (str.Length > 15)
                        {
                            str = str.Substring(0, 15);
                        }
                        lblRefBy.Text = str;
                        lblRefBy.ToolTip = RefDrName;
                    }

                }
            }
            else
            {
                Clear();
            }

        }

        void BindPTVisitDetails()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND HPV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPV_PT_ID = '" + lblPTID.Text + "' ";
            Criteria += " AND HPV_EMR_ID = '" + Convert.ToString(Session["EMR_ID"]) + "' ";

            DataSet DS = new DataSet();
            DS = objCom.PatientVisitGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPV_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]) != "")
                {

                    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblProviderName.Text = str;
                    lblProviderName.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HPV_VISIT_TYPEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPEDesc"]) != "")
                {

                    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPEDesc"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPatientType.Text = str;
                    lblPatientType.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPV_VISIT_TYPEDesc"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("AgeDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["AgeDesc"]) != "")
                {
                    string str = Convert.ToString(DS.Tables[0].Rows[0]["AgeDesc"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblAge.Text = str;
                    lblAge.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["AgeDesc"]);
                }

                //if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                //{
                //    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblPolicyNo.Text = str;
                //    lblPolicyNo.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //}

                //if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                //{
                //    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblPolicyType.Text = str;
                //    lblPolicyType.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //}
            }



        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            objCom = new CommonBAL();
            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                if (ds.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPTName.Text = str;
                    lblPTName.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);

                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblNationality.Text = str;
                    lblNationality.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                }


                //if (ds.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                //{

                //    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblProviderName.Text = str;
                //    lblProviderName.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //}

                if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }

                    lblSex.Text = str;
                    lblSex.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                   
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblMobile.Text = str;
                    lblMobile.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                }
                /*
                if (ds.Tables[0].Rows[0].IsNull("HPM_AGE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblAge.Text = str;
                    lblAge.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                }

                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                 
                 */
                lblDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);


                if (ds.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblEmiratesID.Text = str;
                    lblEmiratesID.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                    //if (str.Length > 15)
                    //{
                    //    str = str.Substring(0, 15);
                    //}
                    lblPolicyNo.Text = str;
                    lblPolicyNo.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                    Session["HPM_POLICY_NO"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPolicyType.Text = str;
                    lblPolicyType.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_PHONE1") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblLanguage.Text = str;
                    lblLanguage.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PHONE1"]);
                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_FAX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblPassportNo.Text = str;
                    lblPassportNo.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_FAX"]);
                }



                if (ds.Tables[0].Rows[0].IsNull("HPM_PT_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                    if (str.Length > 16)
                    {
                        str = str.Substring(0, 16);
                    }
                    lblPTCompany.Text = str;
                    lblPTCompany.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                }



            }

        }

        void BindPatientPhoto()
        {
            imgFront.ImageUrl = "DisplayCardImage.aspx";

            //objCom = new CommonBAL();
            //string Criteria = " 1=1 ";
            //Criteria += " AND HPP_BRANCH_ID='" + Session["Branch_ID"] + "' AND  HPP_PT_ID='" + lblPTID.Text + "'";
            //DataSet DS = new DataSet();
            //DS = objCom.PatientPhotoGet(Criteria);


            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
            //    {
            //        imgFront.Visible = true;
            //        Session["HTI_IMAGE1"] = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
            //        imgFront.ImageUrl = "~/DisplayCardImage.aspx";
            //    }

            //}



        }

        string BindRefDrData(string StaffID)
        {
            string RefDrName = "";

            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HRDM_REF_ID='" + StaffID + "'";

            DS = objCom.RefDoctorMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                RefDrName = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
            }

            return RefDrName;
        }

        void Clear()
        {

            lblPTID.Text = "";
            lblEMR_ID.Text = "";
            lblRefBy.Text = "";
            lblBeginDt.Text = "";
            lblEndDt.Text = "";

            lblPTName.Text = "";

            lblNationality.Text = "";

            lblProviderName.Text = "";
            lblSex.Text = "";
            lblMobile.Text = "";
            lblAge.Text = "";

            lblEmiratesID.Text = "";
            lblPolicyNo.Text = "";
            lblPolicyType.Text = "";
            imgFront.Visible = false;


        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {

                if (Convert.ToString(Session["EMR_ID"]) == "" || Convert.ToString(Session["EMR_ID"]) == null)
                {
                    goto FunEnd;
                }

               
                try
                {
                    BindEMR();
                    BindPTVisitDetails();

                    if (Convert.ToString(Session["EMR_ALLERGY_DISPLAY"]) == "1")
                    {
                         trAlergy.Visible = true;
                         Allergy = Convert.ToString(Session["PT_ALLERGY"]);

                    }

                    if (Convert.ToString(Session["EMR_HIST_SOCIAL_DISPLAY"]) == "1")
                    {
                        trSocialHist.Visible = true;
                         SocialHistory = Convert.ToString(Session["PT_HIST_SOCIAL"]);

                    }

                    if (Convert.ToString(Session["EMR_HIST_PAST_DISPLAY"]) == "1")
                    {
                        trPastHist.Visible = true;
                        PastHistory = Convert.ToString(Session["PT_HIST_PAST"]);

                    }

                  //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlergyMessage('" + Convert.ToString(Session["PT_ALLERGY"]) + "')", true);
                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {
                       // trAlergy.Visible = true;
                       // Allergy = Convert.ToString(Session["PT_ALLERGY"]);
                        lblhdStartDate.Visible = false;
                        lblBeginDt.Visible = false;

                        lblhdEndDate.Visible = false;
                        lblEndDt.Visible = false;


                        lblhdDOB.Visible = false;
                        lblDOB.Visible = false;

                        lblhdEmiratesID.Visible = false;
                        lblEmiratesID.Visible = false;

                        lblhdPolicyType.Visible = false;
                        lblPolicyType.Visible = false;

                        lblhdRefBy.Visible = false;
                        lblRefBy.Visible = false;

                        chkVIP.Visible = false;
                        chkRoyal.Visible = false;
                    }
                    if (GlobalValues.FileDescription.ToUpper() == "REENABEEGUM")
                    {
                        trExtraDtls.Visible = true;
                        lblhdLanguage.Visible = true;
                        lblLanguage.Visible = true;


                        lblhdPassportNo.Visible = true;
                        lblPassportNo.Visible = true;

                        lblhdPTCompany.Visible = true;
                        lblPTCompany.Visible = true;


                    }
                      
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
                    {
                        chkProcComp.Enabled = false;
                    }

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {

                        chkProcComp.Enabled = false;
                    }

                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                         chkProcComp.Enabled = false;
                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientHeader.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        FunEnd: ;
        }

        protected void chkProcComp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                string strDate = "", strTime = "";
                strDate = objCom.fnGetDateddMMyyyy();
                strTime = objCom.fnGetDate("HH:mm:ss");

                //   strDate = Convert.ToString(Session["HPV_DATE"]);
                //   strTime = Convert.ToString(Session["HPV_TIME"]);

                EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                objPTMaster.EPM_CRITICAL_NOTES = "";
                objPTMaster.ProcessCompleted = chkProcComp.Checked == true ? "Completed" : "onProcess";
                objPTMaster.EPM_VIP = chkVIP.Checked == true ? "1" : "0";
                objPTMaster.EPM_ROYAL = chkRoyal.Checked == true ? "1" : "0";

                objPTMaster.EPM_START_DATE = lblBeginDt.Text;
                objPTMaster.EPM_END_DATE = lblEndDt.Text;

                if (chkProcComp.Checked == true)
                {
                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                    //objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                    objPTMaster.EPM_END_DATE = strDate + " " + strTime;
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        chkProcComp.Enabled = false;
                    }
                }
                else
                {
                    objPTMaster.EPM_END_DATE = lblEndDt.Text.Trim();
                }

                objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                objPTMaster.AddPatientProcess();
                BindEMR();
                BindPTVisitDetails();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientHeader.chkProcComp_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}