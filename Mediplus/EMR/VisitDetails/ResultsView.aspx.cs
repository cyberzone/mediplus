﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;
namespace Mediplus.EMR.VisitDetails
{
    public partial class ResultsView : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }
        
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    LabResult.FileNo = Convert.ToString(Session["EMR_PT_ID"]);
                    LabResult.BindLaboratoryResult();
                    RadiologyResult.FileNo = Convert.ToString(Session["EMR_PT_ID"]);
                    RadiologyResult.BindRadiologyResult();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ResultsView.gvbtnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

       

    }
}