﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.VisitDetails
{
    public partial class PharmacyForDHA1 : System.Web.UI.Page
    {
        static string strSessionDeptId, strUserCode;
        static Int32 intSrcType;

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PHY' AND SEGMENT='PHY_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet("Pharmacy", SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }

                if (GlobalValues.FileDescription == "ALNOOR")
                {
                    Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
                }
                else
                {
                    Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "' )";
                }

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;

                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";

            if (GlobalValues.FileDescription == "ALNOOR")
            {
                Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
            }
            else
            {
                Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";
            }

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();

            // if (drpTemplate.SelectedIndex == 0)
            // {
            //    DS = objPro.PharmacyGet(Criteria);
            //}
            //else
            //{
            //    Criteria = " 1=1 ";
            //    Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //    Criteria += " AND ET_CODE='" + drpTemplate.SelectedValue + "'";

            //    DS = objPro.PharmacyTemplatGe(Criteria);
            //}

            DS = objPro.PharmacyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvPharmacy.Columns[0].Visible = false;

                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvPharmacy.Columns[0].Visible = false;
                }

                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvPharmacy.Columns[0].Visible = false;
                }
            }
            else
            {
                gvPharmacy.DataBind();
            }
        FunEnd: ;
        }

        Boolean CheckPharmacy(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPP_LAB_CODE='" + Code + "'";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();
            DS = objPro.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        //void BindDossage()
        //{
        //    DataSet DS = new DataSet();
        //    CommonBAL objCom = new CommonBAL();
        //    string Criteria = " 1=1 AND EPT_ACTIVE=1 ";
        //    Criteria += " AND EPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
        //    DS = objCom.EMRTimeGet(Criteria);


        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        drpDossage.DataSource = DS;
        //        drpDossage.DataTextField = "EPT_NAME";
        //        drpDossage.DataValueField = "EPT_NAME";
        //        drpDossage.DataBind();
        //    }
        //    drpDossage.Items.Insert(0, "Select When");
        //    drpDossage.Items[0].Value = "";

        //    // objCom.BindMasterDropdown(Criteria, out drpDossage);

        //}

        void BindTaken()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPT_ACTIVE=1 ";
            Criteria += " AND EPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRTakenGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTaken.DataSource = DS;
                drpTaken.DataTextField = "EPT_NAME";
                drpTaken.DataValueField = "EPT_CODE";
                drpTaken.DataBind();
            }
            drpTaken.Items.Insert(0, "Select How");
            drpTaken.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindRoute()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPR_ACTIVE=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRRouteGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoute.DataSource = DS;
                drpRoute.DataTextField = "EPR_NAME";
                drpRoute.DataValueField = "EPR_CODE";
                drpRoute.DataBind();
            }
            drpRoute.Items.Insert(0, "Select Route");
            drpRoute.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='Pharmacy'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";


            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindHistory()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_ID   !='" + Convert.ToString(Session["EMR_ID"]) + "' ";


            DS = objCom.PharmacyHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.DataSource = DS;
                drpHistory.DataTextField = "Name";
                drpHistory.DataValueField = "Code";
                drpHistory.DataBind();
            }
            drpHistory.Items.Insert(0, "----Select History---");
            drpHistory.Items[0].Value = "0";

            /*
            DataSet DS = new DataSet();
            EMR_PTPharmacy objPTPhar = new EMR_PTPharmacy();

            DS = objPTPhar.WEMR_spS_GetPharmacyHistoryList(Convert.ToString(Session["EMR_PT_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.Items.Clear();

                drpHistory.Items.Insert(0, "Use Prescriptions");
                drpHistory.Items[0].Value = "0";

                for (int j = 0; j <= DS.Tables[0].Rows.Count - 1; j++)
                {

                    drpHistory.Items.Insert(1, Convert.ToString(DS.Tables[0].Rows[j]["EPM_Date"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["PatientMasterID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["DoctorID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["DoctorName"]));
                    drpHistory.Items[1].Value = Convert.ToString(DS.Tables[0].Rows[j]["PatientMasterID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[j]["BranchID"]);

                }
            }
            
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.Items.Clear();

                drpHistory.Items.Insert(0, "Use Prescriptions");
                drpHistory.Items[0].Value = "0";

                for (int j = 0; j <= DS.Tables[0].Rows.Count - 1; j++)
                {

                    drpHistory.Items.Insert(1, Convert.ToString(DS.Tables[0].Rows[j]["EPM_DateDesc"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["EPM_ID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["EPM_DR_CODE"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["EPM_DR_NAME"]));
                    drpHistory.Items[1].Value = Convert.ToString(DS.Tables[0].Rows[j]["EPM_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[j]["EPM_BRANCH_ID"]);

                }
            }

            // objCom.BindMasterDropdown(Criteria, out drpDossage);
             */
        }


        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }


        void ClearPhar()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            if (GlobalValues.FileDescription.ToUpper() == "ADVCARE")
            {
                txtDuration.Text = "7";
                txtUnit.Text = "1";
                txtFreqyency.Text = "3";
            }
            else
            {
                txtDuration.Text = "";
                txtUnit.Text = "";
                txtFreqyency.Text = "";
                txtRemarks.Text = "";
            }
            if (drpDurationType.Items.Count > 0)
                drpDurationType.SelectedIndex = 1;
            // txtQty.Text = "";
            //if (drpDossage.Items.Count > 0)
            //    drpDossage.SelectedIndex = 0;

            if (drpTaken.Items.Count > 0)
                drpTaken.SelectedIndex = 0;

            if (drpRoute.Items.Count > 0)
                drpRoute.SelectedIndex = 0;

            txtDossage1.Text = "";

            // txtRefil.Text = "";
            //  txtFromDate.Text = "";
            // txtToDate.Text = "";

            // chkLifeLong.Checked = false;



            if (drpUnitType.Items.Count > 0)
                drpUnitType.SelectedIndex = 0;

            if (drpFreqType.Items.Count > 0)
                drpFreqType.SelectedIndex = 0;



        }

        void ShowPrescriptionRequest()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HPR_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "' AND HPR_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria += " AND HPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ePrescriptionBAL objePresc = new ePrescriptionBAL();
            DataSet DS = new DataSet();
            DS = objePresc.PrescriptionRequestGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lbleAuthSRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPR_REFERENCE_NO"]);
                if (Convert.ToString(DS.Tables[0].Rows[0]["ApprovalStatus"]) != "" && DS.Tables[0].Rows[0].IsNull("ApprovalStatus") == false)
                {
                    lbleAuthStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["ApprovalStatus"]);
                }
                else
                {
                    lbleAuthStatus.Text = "Request Sent";
                }
                lbleAuthComment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPR_FILE_COMMENTS"]);

            }

        }

        void BindPharmacyHistory(string EMR_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();

            DS = objPro.PharmacyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHistory.DataSource = DS;
                gvHistory.DataBind();

            }
            else
            {
                gvHistory.DataBind();
            }



        }


        string CheckPayerID()
        {
            string strPayerID = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_COMP_ID='" + Session["HPV_COMP_ID"] + "'";
            DS = objCom.GetCompanyMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_PAYERID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]) != "")
                    strPayerID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);
            }

            return strPayerID;
        }

        string CheckClinicianID()
        {
            string strClinicianID = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_STAFF_ID='" + Session["HPV_DR_ID"] + "'";
            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]) != "")
                    strClinicianID = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);
            }

            return strClinicianID;
        }


        void BindInstructions()
        {

            if (txtUnit.Text.Trim() != "" && txtFreqyency.Text.Trim() != "" && txtDuration.Text.Trim() != "")
            {
                string strInstructions = "";
                strInstructions = "Take " + txtUnit.Text.Trim() + " " + drpUnitType.SelectedItem.Text + "," + txtFreqyency.Text.Trim() + " Time (" + drpFreqType.SelectedItem.Text + ") for " + txtDuration.Text.Trim() + " " + drpDurationType.SelectedItem.Text + ".";



                string frequencytype = drpFreqType.SelectedValue;
                string durationtype = drpDurationType.SelectedValue;

                decimal units = 0, frequency = 0, duration = 0, durationMix = 0, decTotal = 0;

                units = Convert.ToDecimal(txtUnit.Text.Trim());
                frequency = Convert.ToDecimal(txtFreqyency.Text.Trim());
                duration = Convert.ToDecimal(txtDuration.Text.Trim());
                durationMix = Convert.ToDecimal(drpDurationType.SelectedValue);

                if (durationtype == "3")
                {
                    durationMix = 7;

                }
                else if (durationtype == "1")
                {
                    durationMix = 30;

                }
                else if (durationtype == "2")
                {
                    durationMix = 365;

                }
                else
                {
                    durationMix = 1;
                }

                if (frequencytype == "04")
                {

                    decTotal = ((units * frequency * 1 * duration) * durationMix);
                    //  alert(Total)
                }
                else if (frequencytype == "05")
                {

                    decTotal = ((units * frequency * 24 * duration) * durationMix);
                    //  alert(Total)
                }
                else if (frequencytype == "06")
                {

                    decTotal = (((units * frequency * duration) / 7) * durationMix);
                    //  alert(Total)
                }
                txtRemarks.Text = strInstructions;
                if (drpUnitType.SelectedValue == "01")
                {
                    txtQty.Text = Convert.ToString(decTotal);

                }
                else
                {
                    txtQty.Text = "1";

                }


            }
            else
            {
                txtRemarks.Text = "";
                txtQty.Text = "";
            }

        }

        void GetHaadServiceName(string HaadCode, out string HaadDesc)
        {
            HaadDesc = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HHS_TYPE_VALUE = 5 and HHS_CODE='" + HaadCode + "'";
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            ds = dbo.HaadServiceGet(Criteria, "", "100");

            if (ds.Tables[0].Rows.Count > 0)
            {

                HaadDesc = Convert.ToString(ds.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
            }

        }


        Boolean CheckServiceAvailable()
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            ds = dbo.HaadServicessListGet("Pharmacy", txtNewServCode.Text.Trim(), "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;
            if (intSrcType == 0)
            {
                ds = dbo.HaadServicessListGet("Pharmacy", prefixText, "", strSessionDeptId);
            }
            else
            {
                string Criteria = " 1=1 ";

                Criteria += " AND( EDF_CODE like'%" + prefixText + "%' OR  EDF_NAME  like'%" + prefixText + "%')";

                Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + strUserCode + "' OR EDF_DEP_ID='" + strSessionDeptId + "' )";

                ds = dbo.FavoritesGet(Criteria);

            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["ProductName"].ToString() + '~' + ds.Tables[0].Rows[i]["Description"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }


            if (Session["User_ID"] == null) { goto FunEnd; }

            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "PharmacyForDHA")
            {
                BindData();
                txtSearch.Focus();

            }
            lblMessage.Text = "";


            if (!IsPostBack)
            {
                try
                {
                    if (Convert.ToString(Session["HUM_ADDNEW_RECORD_FROMEMR"]) == "True")
                    {
                        btnNewService.Visible = true;
                    }

                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    strUserCode = Convert.ToString(Session["User_Code"]);


                    if (Convert.ToString(Session["HPV_PT_TYPE"]).ToUpper() == "CR" || Convert.ToString(Session["HPV_PT_TYPE"]).ToUpper() == "CREDIT")
                    {
                        tblAuth.Visible = true;
                    }

                    if (GlobalValues.FileDescription.ToUpper() == "ADVCARE")
                    {

                        txtDuration.Text = "7";
                        txtUnit.Text = "1";
                        txtFreqyency.Text = "3";
                    }
                    if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                    {
                        lblDosage.Visible = false;
                        txtDossage1.Visible = false;

                    }

                    BindScreenCustomization();
                    if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                    {
                        drpSrcType.SelectedIndex = 1;
                        btnDeleteFav.Visible = true;
                        btnAddFav.Visible = false;
                    }
                    else
                    {
                        drpSrcType.SelectedIndex = 0;
                        btnAddFav.Visible = true;
                        btnDeleteFav.Visible = false;
                    }
                    intSrcType = drpSrcType.SelectedIndex;

                    //DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    //txtToDate.Text = strFromDate.AddDays(3).ToString("dd/MM/yyyy");

                    // BindDossage();
                    BindTaken();
                    BindRoute();
                    BindTemplate();
                    BindData();
                    BindPharmacy();
                    BindHistory();
                    ShowPrescriptionRequest();
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;


                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_TEMPLATE"]) == "1")
                    {
                        divTemplate.Visible = true;
                    }


                    if (Convert.ToString(Session["EMR_DISPLAY_HISTORY"]) == "1")
                    {
                        divHistoryDrp.Visible = true;
                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        FunEnd: ;
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");
                Label lblProdName = (Label)gvScanCard.Cells[0].FindControl("lblProdName");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblProdName.Text + " " + lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            if (drpSrcType.SelectedIndex == 0)
            {
                btnAddFav.Visible = true;
                btnDeleteFav.Visible = false;
            }
            else
            {
                btnAddFav.Visible = false;
                btnDeleteFav.Visible = true;
            }
            intSrcType = drpSrcType.SelectedIndex;

            BindData();
        FunEnd: ;
        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "PHY";
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "PHY";
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void Add_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string HaadDesc = "";

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");

                HaadDesc = lblDesc.Text;
                if (drpSrcType.SelectedIndex == 1)
                {
                    GetHaadServiceName(lblCode.Text, out  HaadDesc);
                }

                txtServCode.Text = lblCode.Text;
                txtServName.Text = HaadDesc.Trim();   //lblDesc.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;

        }

        protected void btnAddPhar_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                //if (GlobalValues.FileDescription.ToUpper() == "ALNOOR")
                //{
                //    if (txtUnit.Text.Trim() == "")
                //    {
                //        lblMessage.Text = "Please Enter Unit";
                //        lblMessage.ForeColor = System.Drawing.Color.Red;
                //        goto FunEnd;

                //    }
                //    if (txtFreqyency.Text.Trim() == "")
                //    {
                //        lblMessage.Text = "Please Enter Freqyency";
                //        lblMessage.ForeColor = System.Drawing.Color.Red;
                //        goto FunEnd;

                //    }

                //    if (txtDuration.Text.Trim() == "")
                //    {
                //        lblMessage.Text = "Please Enter Duration";
                //        lblMessage.ForeColor = System.Drawing.Color.Red;
                //        goto FunEnd;

                //    }

                //}
                if (txtServCode.Text == "" || txtServCode.Text == null)
                {
                    lblMessage.Text = "Please select the Code";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                Int32 intPos = 1;
                if (gvPharmacy.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvPharmacy.Rows.Count) + 1;

                }



                EMR_PTPharmacy objDiag = new EMR_PTPharmacy();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_PHY_CODE = txtServCode.Text;
                objDiag.EPP_PHY_NAME = txtServName.Text.Replace("'", "''");

                objDiag.EPP_DOSAGE = "";// drpDossage.SelectedValue;
                objDiag.EPP_ROUTE = drpRoute.SelectedValue;
                //objDiag.EPP_REFILL =""// txtRefil.Text.Trim();
                //if (chkLifeLong.Checked == true)
                //{
                //    objDiag.EPP_LIFE_LONG = "1";
                //}
                //else
                //{
                objDiag.EPP_LIFE_LONG = "0";
                //}
                objDiag.EPP_DURATION = txtDuration.Text;
                objDiag.EPP_DURATION_TYPE = drpDurationType.SelectedValue;
                objDiag.EPP_START_DATE = "";// txtFromDate.Text;
                objDiag.EPP_END_DATE = "";//txtToDate.Text;
                objDiag.EPP_REMARKS = txtRemarks.Text.Replace("'", "''");
                objDiag.EPP_REFERENCE = "";
                objDiag.EPP_QTY = txtQty.Text.Trim();
                objDiag.EPP_ICD = "";
                objDiag.EPP_DOSAGE1 = txtDossage1.Text.Trim();
                objDiag.EPP_TYPE = "";
                objDiag.EPP_INV_IsuNo = "";
                objDiag.EPP_HHS_CODE = "";
                objDiag.EPP_TEMPLATE_CODE = "";
                objDiag.EPP_REF_OUTSIDE = "0";
                objDiag.EPP_TAKEN = "";
                objDiag.EPP_TIME = drpTaken.SelectedValue;

                objDiag.EPP_FREQUENCY = txtFreqyency.Text.Trim();
                objDiag.EPP_FREQUENCYTYPE = drpFreqType.SelectedValue;
                objDiag.EPP_UNIT = txtUnit.Text.Trim();
                objDiag.EPP_UNITTYPE = drpUnitType.SelectedValue;
                objDiag.PharmacyAdd();

                BindPharmacy();
                ClearPhar();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;

        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");


                EMR_PTPharmacy objDiag = new EMR_PTPharmacy();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_PHY_CODE = lblPhyCode.Text;

                objDiag.PharmacyDelete();


                BindPharmacy();
                ClearPhar();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void PhySelect_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");

                Label lblPhyDuration = (Label)gvScanCard.Cells[0].FindControl("lblPhyDuration");
                Label lblPhyDurationType = (Label)gvScanCard.Cells[0].FindControl("lblPhyDurationType");

                Label lblPhyQTY = (Label)gvScanCard.Cells[0].FindControl("lblPhyQTY");
                Label lblPhyDosage = (Label)gvScanCard.Cells[0].FindControl("lblPhyDosage");

                Label lblPhyTime = (Label)gvScanCard.Cells[0].FindControl("lblPhyTime");
                Label lblPhyRoute = (Label)gvScanCard.Cells[0].FindControl("lblPhyRoute");



                Label lblPhyRefil = (Label)gvScanCard.Cells[0].FindControl("lblPhyRefil");
                Label lblPhyStDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyStDate");
                Label lblPhyEndDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyEndDate");

                Label lblPhyLifeLong = (Label)gvScanCard.Cells[0].FindControl("lblPhyLifeLong");
                Label lblPhyRemarks = (Label)gvScanCard.Cells[0].FindControl("lblPhyRemarks");
                Label lblPhyDossage1 = (Label)gvScanCard.Cells[0].FindControl("lblPhyDossage1");

                Label lblPhyUnit = (Label)gvScanCard.Cells[0].FindControl("lblPhyUnit");
                Label lblPhyUnitType = (Label)gvScanCard.Cells[0].FindControl("lblPhyUnitType");

                Label lblPhyFrequency = (Label)gvScanCard.Cells[0].FindControl("lblPhyFrequency");
                Label lblPhyFrequencyType = (Label)gvScanCard.Cells[0].FindControl("lblPhyFrequencyType");



                txtServCode.Text = lblPhyCode.Text;
                txtServName.Text = lblPhyDesc.Text;
                txtDuration.Text = lblPhyDuration.Text;

                drpDurationType.SelectedValue = lblPhyDurationType.Text;
                txtQty.Text = lblPhyQTY.Text;

                txtDossage1.Text = lblPhyDossage1.Text;

                //  drpDossage.SelectedValue = lblPhyDosage.Text;

                drpTaken.SelectedValue = lblPhyTime.Text;
                drpRoute.SelectedValue = lblPhyRoute.Text;

                txtUnit.Text = lblPhyUnit.Text;
                drpUnitType.SelectedValue = lblPhyUnitType.Text;

                txtFreqyency.Text = lblPhyFrequency.Text;
                drpFreqType.SelectedValue = lblPhyFrequencyType.Text;


                // txtRefil.Text = lblPhyRefil.Text;
                // txtFromDate.Text = lblPhyStDate.Text;
                // txtToDate.Text = lblPhyEndDate.Text;

                //  chkLifeLong.Checked = Convert.ToBoolean(lblPhyLifeLong.Text);
                txtRemarks.Text = lblPhyRemarks.Text;



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        FunEnd: ;
        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "Pharmacy";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = TemplateCode;
                objPhar.PharmacyTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = drpTemplate.SelectedValue;
                objPhar.PharmacyTemplateDelete();
                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                    objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.TemplateCode = drpTemplate.SelectedValue;
                    objPhar.PharmacyAddFromTemplate();
                    BindPharmacy();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }



        protected void Instructions_Changed(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            //Take 1 Table(s), 3 Time Per Day for 5 Day(s)
            BindInstructions();

        FunEnd: ;
        }

        protected void btnReqeAuth_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                if (Convert.ToString(Session["HPM_POLICY_NO"]) != "" && Convert.ToString(Session["HPM_POLICY_NO"]) != null)
                {
                    string PayerID = "", ClinicianID = "";
                    PayerID = CheckPayerID();
                    ClinicianID = CheckClinicianID();

                    if (PayerID == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertMessage('Payer ID is Empty')", true);
                        goto FunEnd;
                    }

                    if (ClinicianID == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertMessage('Clinician ID is Empty')", true);
                        goto FunEnd;
                    }


                    ePrescriptionBAL objePresc = new ePrescriptionBAL();
                    DataSet DS = new DataSet();
                    objePresc.HPR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objePresc.HPR_EMR_ID = Convert.ToString(Session["EMR_ID"]);
                    objePresc.HPR_MEMBER_ID = Convert.ToString(Session["HPM_POLICY_NO"]);
                    objePresc.UserID = Convert.ToString(Session["User_ID"]);
                    objePresc.UserID = Convert.ToString(Session["User_ID"]);

                    objePresc.SaveEApproval();
                    ShowPrescriptionRequest();

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertMessage('Authorization Request Sent!')", true);
                    goto FunEnd;

                }




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnReqeAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnReqhResubmit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                if (Convert.ToString(Session["HPM_POLICY_NO"]) != "" && Convert.ToString(Session["HPM_POLICY_NO"]) != null)
                {
                    string PayerID = "", ClinicianID = "";
                    PayerID = CheckPayerID();
                    ClinicianID = CheckClinicianID();

                    if (PayerID == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertMessage('Payer ID is Empty')", true);
                        goto FunEnd;
                    }

                    if (ClinicianID == "")
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertMessage('Clinician ID is Empty')", true);
                        goto FunEnd;
                    }

                    ePrescriptionBAL objePresc = new ePrescriptionBAL();
                    objePresc.HPR_EMR_ID = Convert.ToString(Session["EMR_ID"]);
                    objePresc.DeletePrescriptionRequest();


                    objePresc = new ePrescriptionBAL();
                    DataSet DS = new DataSet();
                    objePresc.HPR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objePresc.HPR_EMR_ID = Convert.ToString(Session["EMR_ID"]);
                    objePresc.HPR_MEMBER_ID = Convert.ToString(Session["HPM_POLICY_NO"]);
                    objePresc.UserID = Convert.ToString(Session["User_ID"]);
                    objePresc.UserID = Convert.ToString(Session["User_ID"]);

                    objePresc.SaveEApproval();
                    ShowPrescriptionRequest();

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertMessage('Authorization Resubmit Sent!')", true);
                    goto FunEnd;

                }




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnReqhResubmit_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;

        }

        protected void btnRefresheAuth_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                ShowPrescriptionRequest();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnRefresheAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnAddNewService_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                lblNewServStatus.Text = "";
                if (txtNewServCode.Text == "" || txtNewServCode.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Code";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtNewServDesc.Text == "" || txtNewServDesc.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtNewProdName.Text == "" || txtNewProdName.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Product Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (CheckServiceAvailable() == true)
                {
                    lblNewServStatus.Text = "Medicine  Already available";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.ServCode = txtNewServCode.Text.Trim();
                objCom.ServDesc = txtNewServDesc.Text;
                objCom.ProductName = txtNewProdName.Text;
                objCom.ServType = "DRUG";
                objCom.ServTypeValue = "5";
                objCom.UserID = Convert.ToString(Session["User_ID"]);

                objCom.HaadServiceManualAdd();

                lblNewServStatus.Text = "New Medicine added";
                lblNewServStatus.Visible = true;
                lblNewServStatus.ForeColor = System.Drawing.Color.Green;

                txtNewServCode.Text = "";
                txtNewServDesc.Text = "";
                txtNewProdName.Text = "";



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddNewService_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void imgbtnDown_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {
                    EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                    objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.SelectedBranch_ID = BranchID;
                    objPhar.SelectedEMR_ID = strEMR_ID;

                    objPhar.PharmacyAddFromHistory();
                    BindPharmacy();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyStandard.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void imgbtnPrescHistView_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }

                //  string strRptPath = "";

                if (drpHistory.SelectedIndex != 0)
                {
                    BindPharmacyHistory(strEMR_ID);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistory()", true);
                    //strRptPath = "../WebReports/Prescription.aspx";
                    //string rptcall = @strRptPath + "?EMR_ID=" + strEMR_ID + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + strDR_Code;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyStandard.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }
        #endregion
    }
}