﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.VisitDetails
{
    public partial class Index : System.Web.UI.Page
    {
        # region Variable Declaration

        CommonBAL objCom = new CommonBAL();
      

        #endregion
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PHY' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["EMR_PHY_SCREEN"] = "HAAD";
           
           

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_PHY_SCREEN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_PHY_SCREEN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                  


                }
            }



        }


        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='VisitDetails'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

           if (!IsPostBack)
            {
                btnDiagnosis.CssClass = "TabButtonWhite";
                frmVisit.Src = "~/EMR/VisitDetails/Diagnosis.aspx";

                BindScreenCustomization();
                BindTemplate();
                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    BindAuditDtls();
                    divAuditDtls.Visible = true;
                }
            }
            
        }

        protected void btnDiagnosis_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";


            btnDiagnosis.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/Diagnosis.aspx";
        }

        protected void btnProcedure_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnProcedure.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/Procedures.aspx";
        }

        protected void btnRadiology_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnRadiology.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/Radiology.aspx";

        }

        protected void btnLaboratory_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/Laboratory.aspx";
        }

        protected void btnPharmacy_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnPharmacy.CssClass = "TabButtonWhite";


            if (Convert.ToString(ViewState["EMR_PHY_SCREEN"]).ToUpper() == "DHA")
            {
                frmVisit.Src = "~/EMR/VisitDetails/PharmacyForDHA.aspx"; 
            }
            else
            {
                frmVisit.Src = "~/EMR/VisitDetails/PharmacyStandard.aspx";
               
            }

           


        }

        protected void btnOthers_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnOthers.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/Others.aspx";
        }

        protected void btnNursingOrder_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnNursingOrder.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/NursingOrder.aspx";
        }

        protected void btnCommonRemarks_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnOthers.CssClass = "TabButtonBlue";
            btnNursingOrder.CssClass = "TabButtonBlue";
            btnCommonRemarks.CssClass = "TabButtonBlue";

            btnCommonRemarks.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/EMR/VisitDetails/VisitDetailsRemarks.aspx";

        }


        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "VisitDetails";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTDiagnosis objPhar = new EMR_PTDiagnosis();
                objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = TemplateCode;
                objPhar.VisitDetailsTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_PT_PROCEDURES_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='VisitDetails'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTDiagnosis objPro = new EMR_PTDiagnosis();
                    objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPro.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                    objPro.TemplateCode = drpTemplate.SelectedValue;
                    objPro.VisitDetailsAddFromTemplate();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        
    }
}