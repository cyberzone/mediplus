﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Radiology.aspx.cs" Inherits="EMR.VisitDetails.Radiology1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
  <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

 
 <script language="javascript" type="text/javascript">


     function OnlyNumeric(evt) {
         var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
         if (chCode >= 48 && chCode <= 57 ||
              chCode == 46) {
             return true;
         }
         else

             return false;
     }
    </script>


 <script language="javascript" type="text/javascript">
     function refreshPanelRad(strValue) {
         // alert(strValue)
         __doPostBack('<%= updatePanelRad.UniqueID %>', "Radiology");

     }

     function setCursorRad(el, st, end) {
         if (el.setSelectionRange) {
             el.focus();
             el.setSelectionRange(st, end);
         } else {
             if (el.createTextRange) {
                 range = el.createTextRange();
                 range.collapse(true);
                 range.moveEnd('character', end);
                 range.moveStart('character', st);
                 range.select();
             }
         }
     }

     function ShowSearchRad() {
         //  SetCaretAtEnd();

         var elem = document.getElementById("<%=txtSearch.ClientID%>")

         var elemLen = elem.value.length;
         //alert(elemLen);
         setCursorRad(elem, elemLen, elemLen)
     }

     function ShowSOServicePopup() {

         document.getElementById("divSOServicePopup").style.display = 'block';


     }

     function HideSOServicePopup() {

         document.getElementById("divSOServicePopup").style.display = 'none';

     }

     function ShowHistory() {

         document.getElementById("divHistory").style.display = 'block';


     }

     function HideHistory() {

         document.getElementById("divHistory").style.display = 'none';

     }


    </script>

</head>
<body>
    <form id="form1" runat="server">
           <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

    <div>
    <table style="width: 50%">
    <tr>
        <td>
              <asp:UpdatePanel runat="server" ID="updatePanel5">
                <ContentTemplate>
            <asp:Button ID="btnAddFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px" Text="Add Fav."  OnClick="btnAddFav_Click"/>
             <asp:Button ID="btnDeleteFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px" Text="Delete Fav." OnClick="btnDeleteFav_Click" Visible="false" />
                     </ContentTemplate>
                  </asp:UpdatePanel>
        </td>
        <td>
              <asp:UpdatePanel runat="server" ID="updatePanel4">
                <ContentTemplate>
            <asp:DropDownList ID="drpSrcType" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpSrcType_SelectedIndexChanged">
                <asp:ListItem Selected="True">Full List</asp:ListItem>
                <asp:ListItem>Favorite Only</asp:ListItem>
            </asp:DropDownList>
                     </ContentTemplate>
                  </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanelRad">
                <ContentTemplate>
            <asp:TextBox ID="txtSearch" runat="server" CssClass="lblCaption1" OnKeyUp="refreshPanelRad(this.value);"  onfocus="ShowSearchRad();"  ></asp:TextBox>
                   
                    </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="txtSearch" />
                   
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
   
</table>
<table>
    <tr>
        <td style="width: 50%; vertical-align: top;">

            <div style="float: left;">
                <table  style="width:100%">

                    <tr>
                        <td colspan="3">
                               <div style="padding-top: 0px; width: 100%;height:600px;   overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                    <asp:UpdatePanel runat="server" ID="updatePanel1">
                                    <ContentTemplate>
                            <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   OnPageIndexChanging="gvServicess_PageIndexChanging"  GridLines="None"   >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                 <FooterStyle CssClass="FooterStyle" />
                                <Columns>
                                    <asp:TemplateField HeaderText=""  HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                             <asp:ImageButton ID="lnkAdd" runat="server" ToolTip="Add" ImageUrl="~/EMR/Images/AddButton.jpg" Height="18" Width="18"
                                                OnClick="Add_Click" />
                                        </ItemTemplate>
                                           <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click"  >
                                                  <asp:Label ID="lblPrice" CssClass="label" BorderStyle="None" runat="server" Visible="false" Font-Size="11px" Width="70px" Style="text-align: right; padding-right: 5px;" Text='<%# Bind("Price") %>'></asp:Label>

                                                <asp:Label ID="lblCode" CssClass="label" BorderStyle="None"  runat="server" Font-Size="11px" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                             </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Width="85%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDesc" runat="server" OnClick="Edit_Click"  >
                                                <asp:Label ID="lblDesc" CssClass="label" BorderStyle="None" Width="100%" runat="server" Font-Size="11px" Text='<%# Bind("Description") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                     
                                </Columns>
                               <PagerStyle CssClass="PagerStyle" /> 
                                
                            </asp:GridView>
                                         </ContentTemplate>
                                 </asp:UpdatePanel>

                             </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>

        <td style="width: 5px;"></td>

        <td style="width: 50%; vertical-align: top;">
            <div>
                <table style="width:100%">
                    <tr>
                        <td class="lblCaption1" >
                             <asp:UpdatePanel runat="server" ID="updatePanel3" >
                                  <ContentTemplate>
                                     <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label" ></asp:Label>
                                  </ContentTemplate>
                              </asp:UpdatePanel>
                            Remarks:<br />
                            <input type="hidden" runat="server" id="hidProcCode" />
                        </td>
                        <td class="lblCaption1" >
                            Qty:
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="lblCaption1" Width="98%" AutoPostBack="true" OnTextChanged="txtRemarks_TextChanged"></asp:TextBox>

                        </td>
                        <td>
                            <asp:TextBox ID="txtQty" runat="server" CssClass="lblCaption1" Width="100px" Text="1" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </td>
                     
                    </tr>
                     <tr>
                           <td class="lblCaption1"  colspan="3">
                               <div id="divHistoryDrp" runat="server" visible="false">
                               History   &nbsp;&nbsp; 
                              <asp:UpdatePanel runat="server" ID="updatePanel11">
                                    <ContentTemplate>
                                 <asp:DropDownList ID="drpHistory" CssClass="label" runat="server" Width="300px" BorderColor="#cccccc">
                                </asp:DropDownList>
                         
                                  <asp:ImageButton  ID="imgbtnPrescHistView"    runat="server" ToolTip="View selected Radiology" ImageUrl="~//Images/zoom.png" 
                                                       OnClick="imgbtnPrescHistView_Click"  style="height:15px;width:25px;"  />
                                  &nbsp;&nbsp; 
                                  <asp:ImageButton  ID="imgbtnDown"    runat="server" ToolTip="Click here to Import selected Radiology" ImageUrl="~/EMR/Images/Down.gif" 
                                                       OnClick="imgbtnDown_Click"  />

                                        </ContentTemplate>
                                  </asp:UpdatePanel>

                                   </div>
                            </td>
                        </tr>

                      <tr>
                        <td class="lblCaption1" colspan="2" >
                            
                             <div id="divTemplate" runat="server" visible="false">Template&nbsp;
                        
                            <asp:DropDownList ID="drpTemplate" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>

                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label orange" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px" CssClass="orange" />
</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <div style="padding-top: 0px; width: 100%;  height:400px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                   <asp:UpdatePanel runat="server" ID="updatePanel2">
                                    <ContentTemplate>
                           <asp:GridView ID="gvRadiology" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   GridLines="None" >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />

                                <Columns>
                                     <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code"  HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                           
                                                <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("EPR_RAD_CODE") %>'></asp:Label>
                                               
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description"   HeaderStyle-Width="70%"  HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            
                                                 <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px"  Width="100%"  runat="server" Text='<%# Bind("EPR_RAD_NAME") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="10%"   HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            
                                                  <asp:Label ID="lblDiagQTY" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("EPR_QTY") %>'></asp:Label>
                                             
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Serv. ID" HeaderStyle-Width="10%"   HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                             
                                                  <asp:Label ID="llbServiceID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("EPR_SERV_ID") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                                         </ContentTemplate>
                                 </asp:UpdatePanel>
                            </div>
                        </td>
                        
                    </tr>
                </table>
            </div>

        </td>
    </tr>
</table>
         <div id="divSOServicePopup" style="display: none; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 400px; top:10px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">

                                    <input type="button" id="Button2" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideSOServicePopup()" />
                                </td>
                            </tr>
                        </table>
                        
                                    <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:UpdatePanel runat="server" ID="updatePanel13">
                                                        <ContentTemplate>

                                                            <asp:GridView ID="gvServicePopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                EnableModelValidation="True" GridLines="None" Width="100%">
                                                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                                <RowStyle CssClass="GridRow" />

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkgvServiceID" CssClass="lblCaption1"  runat="server"  OnClick="ServicePopupEdit_Click"  OnClientClick="" >
                                                                                <asp:Label ID="lblgvServiceID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkgvServiceName" CssClass="lblCaption1"  runat="server"  OnClick="ServicePopupEdit_Click"  OnClientClick="" >
                                                                            <asp:Label ID="lblgvServiceName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_NAME") %>'></asp:Label>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkgvHAADCode" CssClass="lblCaption1"  runat="server"  OnClick="ServicePopupEdit_Click"  OnClientClick="" >
                                                                            <asp:Label ID="lblgvHAADCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_HAAD_CODE") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                               <asp:LinkButton ID="lnkgvAmount" CssClass="lblCaption1"  runat="server"  OnClick="ServicePopupEdit_Click"  OnClientClick="" >
                                                                            <asp:Label ID="lblgvAmount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_FEE") %>'></asp:Label>
                                                                              </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>

                                            </tr>

                                        </table>
                                    </div>
                                    <asp:UpdatePanel runat="server" ID="updatePanel9">
                                                        <ContentTemplate>
                                                      <asp:Button ID="btnMapDataAdd" runat="server" OnClick="btnMapDataAdd_Click" Text="Add" CssClass="button orange small" Style="padding-left: 2px; padding-right: 2px; width: 100px;" />

                                            <asp:Button ID="btnAddToDB" runat="server" OnClick="btnAddToDB_Click" Text="Add To DB" CssClass="button orange small" Style="padding-left: 2px; padding-right: 2px; width: 100px;" />
                                                            </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

          <div style="padding-left: 40%; width: 100%;">
               <div id="divHistory" style=" display: none;border: groove; height: 350px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:250px;">
                     <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">
                                     <asp:UpdatePanel runat="server" ID="updatePanel12">
                                    <ContentTemplate>
                                    <input type="button" id="btnHistoryClose" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideHistory()" />
                                        </ContentTemplate>
                                         </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                         <asp:UpdatePanel runat="server" ID="updatePanel14">
                                    <ContentTemplate>
                  
                           <asp:GridView ID="gvHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   GridLines="None"   Width="100%"  >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />

                              <Columns>
                                    <asp:TemplateField HeaderText="Code"  HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                           
                                                <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("EPR_RAD_CODE") %>'></asp:Label>
                                               
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description"   HeaderStyle-Width="70%"  HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            
                                                 <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px"  Width="100%"  runat="server" Text='<%# Bind("EPR_RAD_NAME") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="10%"   HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            
                                                  <asp:Label ID="lblDiagQTY" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("EPR_QTY") %>'></asp:Label>
                                             
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Serv. ID" HeaderStyle-Width="10%"   HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                             
                                                  <asp:Label ID="llbServiceID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("EPR_SERV_ID") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                                    </ContentTemplate>
                                        
                                        </asp:UpdatePanel>
                                         

                                  
               </div>
           
           </div>

         <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td class="lblCaption1">Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>

    </div>

    </form>
</body>
     <style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>
</html>
