﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


namespace Mediplus.EMR.VisitDetails
{
    public partial class ResultsViewPopup : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }


            if (!IsPostBack)
            {
                //LabResult.FileNo = txtSrcFileNo.Text.Trim();
                //LabResult.BindLaboratoryResult();
                //RadiologyResult.BindRadiologyResult();
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSrcFileNo.Text.Trim() != "")
                {
                    LabResult.FileNo = txtSrcFileNo.Text.Trim();
                    LabResult.BindLaboratoryResult();
                    RadiologyResult.FileNo = txtSrcFileNo.Text.Trim();
                    RadiologyResult.BindRadiologyResult();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ResultsViewPopup.btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}