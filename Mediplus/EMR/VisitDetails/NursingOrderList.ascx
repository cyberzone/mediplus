﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NursingOrderList.ascx.cs" Inherits="Mediplus.EMR.VisitDetails.NursingOrderList" %>
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">


        function ShowPTDtlsPopup() {
            document.getElementById("divOverlayNurseOrder").style.display = 'block';


        }

        function HidePTDtlsPopup() {
            document.getElementById("divOverlayNurseOrder").style.display = 'none';


        }

</script>

<div id="divOverlayNurseOrder" class="Overlay" style="display:block;">
    <div id="div1"   style="height: 550px; width: 1100px;position: fixed; left: 150px; top: 250px;" > 
      <img src= '<%= ResolveUrl("~/Images/Nurse.png")%>'  style="border:none;width: 40px; height: 40px;" />
        <span  style="color:white;font-size:17px;"  class="label">
            <asp:Label ID="lblNursingHeader" runat="server"  class="label" Text=" Nursing Order"   style="color:white;font-size:17px;"  ></asp:Label>
        </span>
    <div id="div30" style="display: block; overflow: hidden; border: groove; height: 350px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px;  position: fixed; left: 150px; top: 300px;" >
         <div id="divHeaderLine" style="height: 1px; width: 90%; background-color: #D64535; border-bottom-style: solid; border-bottom-color: #D64535; border-bottom-width: 5px;position:absolute; top:0px;left:0px;"  >
            </div>
         <div style="width: 100%; text-align: right; float: right;position:absolute; top:-0px;right:0px;">
               <img src= '<%= ResolveUrl("~/Images/Close.png")%>'   style="height: 25px; width: 25px;cursor:pointer;"   onclick="HidePTDtlsPopup()" />
           </div>      
          <input  id="btnNursingHeader" runat="server"  type="button" class="PageSubHeaderBlue"   style="width:150px;"   value=" Nursing Order "   disabled="disabled" />
        <img src= '<%= ResolveUrl("~/Images/TabBlueCornor.png")%>'    style="border:none;height:20px;width:37px;"/>
                    
        <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
 

          <asp:GridView ID="gvNursingInsttruction" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%" GridLines="None" >
                             <HeaderStyle CssClass="GridHeader" Height="20px" BackColor="#a6a6a6"  ForeColor="#ffffff"/>
                            <RowStyle CssClass="GridRow" Height="20px" />
                             <AlternatingRowStyle CssClass="GridAlterRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="File No." HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPM_PT_ID") %>'></asp:Label>
                                         
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" Name" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPM_PT_NAME") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Doctor" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPM_DR_NAME") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblInsttruction" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Comment">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblComment" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>

                               <asp:TemplateField HeaderText="User Name">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblIUserName" CssClass="label" runat="server" Text='<%# Bind("HUM_USER_NAME") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>  
                                 <asp:TemplateField HeaderText="Time ">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblIUserName" CssClass="label" runat="server" Text='<%# Bind("EPC_TIME") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>                     
                    </Columns>

                </asp:GridView>

 </div>

    </div>
    </div>
 </div>