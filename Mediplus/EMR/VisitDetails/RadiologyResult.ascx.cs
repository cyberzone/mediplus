﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.VisitDetails
{
    public partial class RadiologyResult : System.Web.UI.UserControl
    {
        public string FileNo;

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindRadiologyResult()
        {
            string Criteria = " 1=1 ";
            // if (FileNo != "" && FileNo != null)
            // {
            Criteria += " AND RTR_PATIENT_ID='" + FileNo + "'";
            // }

            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.RadiologyResultGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadResult.DataSource = DS;
                gvRadResult.DataBind();
            }
            else
            {
                gvRadResult.DataBind();
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    // BindRadiologyResult();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RadiologyResult.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvRadResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvRadResult.PageIndex = e.NewPageIndex;
                BindRadiologyResult();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RadiologyResult.gvRadResult_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void RadEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblRadTransNo = (Label)gvScanCard.Cells[0].FindControl("lblRadTransNo");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "RadResultReport('" + lblRadTransNo.Text.Trim() + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RadiologyResult.RadEdit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

    }
}