﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="ResultsView.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.ResultsView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/EMR/VisitDetails/LabResult.ascx" TagPrefix="UC1" TagName="LabResult" %>
<%@ Register Src="~/EMR/VisitDetails/RadiologyResult.ascx" TagPrefix="UC1" TagName="RadiologyResult" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

         <script language="javascript" type="text/javascript">
        function LabResultReport(TransNo,RptType) {

            var Report = "LabReport.rpt";


            if (RptType.toUpperCase() == 'U') {
                Report = "LabReportUsr.rpt";
            }
            var Criteria = " 1=1 ";

            if (TransNo != "") {

                Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';

                var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                win.focus();

            }

        }

        function RadResultReport(TransNo) {

            var Report = "RadiologyReport.rpt";
            var Criteria = " 1=1 ";

            if (TransNo != "") {

                Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

                var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
                win.focus();


            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Results </h3>
        </div>
    </div>
    <br />
    <br />
    <br />
    <div style="padding: 5px;    border: thin; border-color: #777; border-style: groove;width:80%">
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Laboratory" Width="100%">
            <ContentTemplate>
                 <UC1:LabResult ID="LabResult" runat="server" />

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Radiology" Width="100%">
            <ContentTemplate>
                 <UC1:RadiologyResult ID="RadiologyResult" runat="server" />
            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>

     </div>

</asp:Content>
