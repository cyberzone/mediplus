﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LabResult.ascx.cs" Inherits=" Mediplus.EMR.VisitDetails.LabResult" %>
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
    function LabResultReport(ReportName, TransNo, RptType) {

        var Report = "LabReport.rpt";


        if (RptType.toUpperCase() == 'U') {
            Report = "LabReportUsr.rpt";
        }
        var Criteria = " 1=1 ";

        if (TransNo != "") {

            Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'LabReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    }


</script>
<asp:GridView ID="gvLabResult" runat="server" AllowSorting="True" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%" AllowPaging="true" OnPageIndexChanging="gvLabResult_PageIndexChanging" PageSize="30">
    <HeaderStyle CssClass="GridHeader_Blue" />
    <RowStyle CssClass="GridRow" />
    <AlternatingRowStyle CssClass="GridAlterRow" />

    <Columns>
        <asp:TemplateField HeaderText="TransNo" HeaderStyle-Width="110px"  Visible="false">
            <ItemTemplate>
                <asp:LinkButton ID="lnkTranNo" runat="server" OnClick="Edit_Click">
                    <asp:Label ID="lblReportType" CssClass="label" runat="server" Text='<%# Bind("ReportType") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lblLabTransNo" CssClass="label" runat="server" Text='<%# Bind("TransNo") %>'></asp:Label>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <asp:LinkButton ID="lnkDate" runat="server" OnClick="Edit_Click">
                    <asp:Label ID="Label17" CssClass="label" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Caption" HeaderStyle-Width="110px">
            <ItemTemplate>
                <asp:LinkButton ID="lnkCaption" runat="server" OnClick="Edit_Click">
                    <asp:Label ID="lblCaption" CssClass="GridRow" runat="server" Text='<%# Bind("Caption") %>'></asp:Label>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Test Name">
            <ItemTemplate>
                <asp:LinkButton ID="lnkTestName" runat="server" OnClick="Edit_Click">
                    <asp:Label ID="lblTestName" CssClass="label" runat="server" Text='<%# Bind("LabTestNames") %>'></asp:Label>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Doctor">
            <ItemTemplate>
                <asp:LinkButton ID="lnkDr" runat="server" OnClick="Edit_Click">
                    <asp:Label ID="Label18" CssClass="label" runat="server" Text='<%# Bind("DoctorName") %>'></asp:Label>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>

    </Columns>
</asp:GridView>
