﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR.VisitDetails
{
    public partial class Others1 : System.Web.UI.Page
    {
        public string TreatmentPlan = "", FollowupNotes = "";
        CommonBAL objCom = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindEMR()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_PLAN") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]) != "")
                    //TreatmentPlan = clsWebReport.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                    txtTreatmentPlan.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                else
                    txtTreatmentPlan.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_FOLLOWUP_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]) != "")
                    txtFollowupNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_FOLLOWUP_NOTES"]));
                else
                    txtFollowupNotes.Text = "";


                if (DS.Tables[0].Rows[0].IsNull("EPM_TREATMENT_OPTION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_OPTION"]) != "")
                    txtTreatmentOption.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_OPTION"]));
                else
                    txtTreatmentOption.Text = "";

                if (DS.Tables[0].Rows[0].IsNull("EPM_EDUCATION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_EDUCATION"]) != "")
                    txtEducation.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_EDUCATION"]));
                else
                    txtEducation.Text = "";


                if (DS.Tables[0].Rows[0].IsNull("EPM_ULTRASOUND_FINDINGS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_ULTRASOUND_FINDINGS"]) != "")
                    txtUltrasoundFindings.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_ULTRASOUND_FINDINGS"]));
                else
                    txtUltrasoundFindings.Text = "";



            }

        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND ET_TYPE='TREATMENTPLAN'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTemplates.DataSource = DS;
                gvTemplates.DataBind();
            }
            else
            {
                gvTemplates.DataBind();
            }

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindTemplateFollowup()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='FOLLOWUPNOTES'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTemplates2.DataSource = DS;
                gvTemplates2.DataBind();
            }
            else
            {
                gvTemplates2.DataBind();
            }

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (GlobalValues.FileDescription == "MEDICAL_LASER")
                    {
                        divTreatmentOption.Visible = true;
                        divEducation.Visible = true;
                    }

                    if (GlobalValues.FileDescription == "ALANAMEL")
                    {
                        divUltrasoundFindings.Visible = true;

                    }


                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        txtTreatmentPlan.Enabled = false;
                        txtFollowupNotes.Enabled = false;


                    }

                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        txtTreatmentPlan.Enabled = false;
                        txtFollowupNotes.Enabled = false;
                    }
                    BindEMR();
                    BindTemplate();
                    BindTemplateFollowup();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtTreatmentPlan_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
                EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
                objPTM.EPM_TREATMENT_PLAN = txtTreatmentPlan.Text.Replace("'", "''");
                objPTM.EPM_FOLLOWUP_NOTES = txtFollowupNotes.Text.Replace("'", "''");
                objPTM.UpdateEMR_PTMasterOthers(Criteria);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.txtTreatmentPlan_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFollowupNotes_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
                EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
                objPTM.EPM_TREATMENT_PLAN = txtTreatmentPlan.Text.Replace("'", "''");
                objPTM.EPM_FOLLOWUP_NOTES = txtFollowupNotes.Text.Replace("'", "''");
                objPTM.UpdateEMR_PTMasterOthers(Criteria);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.txtTreatmentPlan_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = hidCreateTempType.Value;// "TREATMENTPLAN";
                objCom.Description = txtTemplateName.Value;
                if (hidCreateTempType.Value == "TREATMENTPLAN")
                {
                    objCom.TemplateData = txtTreatmentPlan.Text;
                }
                else
                {
                    objCom.TemplateData = txtFollowupNotes.Text;
                }
                objCom.AllDr = "0";

                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();

                BindTemplate();
                BindTemplateFollowup();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void TempSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTempCode = (Label)gvScanCard.Cells[0].FindControl("lblTempCode");
                Label lblTempData = (Label)gvScanCard.Cells[0].FindControl("lblTempData");
                // txtTreatmentPlan.Text = lblTempData.Text;
                if (hidCreateTempType.Value == "TREATMENTPLAN")
                {
                    txtTreatmentPlan.Text = lblTempData.Text;
                }
                else
                {
                    txtFollowupNotes.Text = lblTempData.Text;
                }


                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
                EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
                objPTM.EPM_TREATMENT_PLAN = CommonBAL.ConvertToTxt(txtTreatmentPlan.Text);
                objPTM.EPM_FOLLOWUP_NOTES = CommonBAL.ConvertToTxt(txtFollowupNotes.Text);
                objPTM.UpdateEMR_PTMasterOthers(Criteria);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.TempSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtTreatmentOption_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

                string FieldNameWithValues = " EPM_TREATMENT_OPTION='" + txtTreatmentOption.Text.Trim().Replace("'", "''") + "'";
                objCom = new CommonBAL();

                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.txtTreatmentOption_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtEducation_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

                string FieldNameWithValues = " EPM_EDUCATION='" + txtEducation.Text.Trim().Replace("'", "''") + "'";
                objCom = new CommonBAL();

                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.txtEducation_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtUltrasoundFindings_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

                string FieldNameWithValues = " EPM_ULTRASOUND_FINDINGS ='" + txtUltrasoundFindings.Text.Trim().Replace("'", "''") + "'";
                objCom = new CommonBAL();

                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.txtUltrasoundFindings_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvDeleteTemp1_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTempCode = (Label)gvScanCard.Cells[0].FindControl("lblTempCode");
                Label lblTempData = (Label)gvScanCard.Cells[0].FindControl("lblTempData");


                CommonBAL objCom = new CommonBAL();

                string Criteria = " 1=1  and ET_TYPE='TREATMENTPLAN'";
                Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
                Criteria += " AND ET_CODE='" + lblTempCode.Text + "'";

                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria);


                BindTemplate();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.gvDeleteTemp1_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }


        protected void gvDeleteTemp2_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTempCode = (Label)gvScanCard.Cells[0].FindControl("lblTempCode");
                Label lblTempData = (Label)gvScanCard.Cells[0].FindControl("lblTempData");


                CommonBAL objCom = new CommonBAL();

                string Criteria = " 1=1  and ET_TYPE='FOLLOWUPNOTES'";
                Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
                Criteria += " AND ET_CODE='" + lblTempCode.Text + "'";

                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria);


                BindTemplateFollowup();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.gvDeleteTemp1_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }
    }
}