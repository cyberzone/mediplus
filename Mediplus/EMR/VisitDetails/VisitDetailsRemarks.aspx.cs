﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace EMR.VisitDetails
{
    public partial class VisitDetailsRemarks1 : System.Web.UI.Page
    {
        private static SegmentBAL _segmentBAL = new SegmentBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public DataSet GetSegmentMaster()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " and esm_type='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public string SegmentVisitDtlsGet(string Criteria)
        {
            string strValue = "";
            DataSet DS = new DataSet();

            Criteria += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                strValue = Convert.ToString(DS.Tables[0].Rows[0]["ESVD_VALUE"]);
            }


            return strValue;

        }


        void DeleteSegmentDtlsByEMR_ID(string SegmentType)
        {
            SegmentBAL objSeg = new SegmentBAL();

            string Criteria = " 1=1 ";
            Criteria += " AND  ESVD_BRANCH_ID= '" + Convert.ToString(Session["Branch_ID"]) + "' AND  ESVD_ID= '" + Convert.ToString(Session["EMR_ID"]) + "' AND  ESVD_TYPE= '" + SegmentType + "' ";
            objSeg.EMRSegmentVisitDtlsDelete(Criteria);
        }



        void fnSave(string Type, string SubType, string FieldName, string Value)
        {

            SegmentBAL objSeg = new SegmentBAL();
            objSeg.BranchID = Convert.ToString(Session["Branch_ID"]);
            objSeg.EMRID = Convert.ToString(Session["EMR_ID"]);
            objSeg.Type = Type;
            objSeg.SubType = SubType;

            objSeg.FieldName = FieldName;
            objSeg.Value = Value;
            objSeg.ValueYes = "Y";
            objSeg.TemplateCode = "";
            objSeg.EMRSegmentVisitDtlsAdd();
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    ViewState["count"] = 1;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VisitDetailsRemarks.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            ViewState["control"] = "textbox";
            //   createDynamicTextBox("TextBox");

            CreateSegCtrls();

        }

        public void createDynamicTextBox(string _TextBoxId)
        {
            HtmlGenericControl tr = new HtmlGenericControl("tr");

            HtmlGenericControl td1 = new HtmlGenericControl("td");
            Label lbl = new Label();
            lbl.ID = "lbl" + _TextBoxId.Replace(" ", "").ToLower();
            lbl.Text = _TextBoxId;
            td1.Controls.Add(lbl);
            tr.Controls.Add(td1);

            HtmlGenericControl td2 = new HtmlGenericControl("td");
            TextBox txtBox = new TextBox();
            txtBox.ID = _TextBoxId.Replace(" ", "").ToLower();
            td2.Controls.Add(txtBox);
            tr.Controls.Add(td2);
            PlaceHolder1.Controls.Add(tr);



            //  HtmlGenericControl td3 = new HtmlGenericControl("td");
            TextBox txtBox1 = new TextBox();
            txtBox1.ID = "tes34";
            td2.Controls.Add(txtBox1);
            tr.Controls.Add(td2);
            PlaceHolder1.Controls.Add(tr);


        }

        public void CreateSegCtrls()
        {
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 AND EST_STATUS=1 ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "'";

                DS1 = SegmentTemplatesGet(Criteria1);

                Int32 i = 1;
                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValue = "";
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                    strValue = SegmentVisitDtlsGet(Criteria2);


                    HtmlGenericControl tr = new HtmlGenericControl("tr");

                    HtmlGenericControl td1 = new HtmlGenericControl("td");
                    Label lbl = new Label();
                   // lbl.ID = "lbl" + Convert.ToString(i) + "-" + strValue;
                    lbl.ID = "lbl" + i + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");

                    lbl.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                    lbl.CssClass = "lblCaption1";


                    td1.Controls.Add(lbl);
                    tr.Controls.Add(td1);

                    HtmlGenericControl td2 = new HtmlGenericControl("td");
                    TextBox txtBox = new TextBox();
                    //txtBox.ID = "txt" + Convert.ToString(i) + "-" + strValue;
                    txtBox.ID = "txt" + i + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");

                    txtBox.Text = strValue;
                    txtBox.TextMode = TextBoxMode.MultiLine;
                    txtBox.Width = 700;
                    txtBox.Height = 50;

                    td2.Controls.Add(txtBox);
                    tr.Controls.Add(td2);
                    PlaceHolder1.Controls.Add(tr);




                    i = i + 1;
                }


            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                switch (Convert.ToString(ViewState["control"]).ToLower())
                {
                    case "textbox":
                        //TextBox txt = (TextBox)PlaceHolder1.FindControl("tes34");
                        //lblValue.Text = txt.Text;


                        DeleteSegmentDtlsByEMR_ID("VISIT_DETAILS_REMARKS");

                        DataSet DS = new DataSet();

                        DS = GetSegmentMaster();
                        foreach (DataRow DR in DS.Tables[0].Rows)
                        {
                            DataSet DS1 = new DataSet();
                            string Criteria1 = "1=1 AND EST_STATUS=1 ";
                            Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "'";

                            DS1 = SegmentTemplatesGet(Criteria1);

                            Int32 i = 1;
                            foreach (DataRow DR1 in DS1.Tables[0].Rows)
                            {
                      

                              //  TextBox txt = (TextBox)PlaceHolder1.FindControl("txt" + Convert.ToString(i) + "-" + strValue);
                                TextBox txt = (TextBox)PlaceHolder1.FindControl("txt" + i + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", ""));


                                // Label lbl = (Label)PlaceHolder1.FindControl("lbl" + Convert.ToString(i) + "-" + strValue);
                                // lbl.Text = txt.Text;

                                fnSave(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR1["EST_SUBTYPE"]), Convert.ToString(DR1["EST_FIELDNAME"]), txt.Text);

                                i = i + 1;
                            }

                        }
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VisitDetailsRemarks.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }
    }
}