﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;
namespace Mediplus.EMR.VisitDetails
{
    public partial class NursingOrderPopup : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindNursingtInstruction()
        {
          
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPC_BRANCH_ID	 = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) = CONVERT(datetime,convert(varchar(10),GETDATE(),101),101)";

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTORS" || Convert.ToString(Session["User_Category"]).ToUpper() == "DOCTOR")
            {
                Criteria += " AND EPM_DR_CODE='" + Convert.ToString( Session["User_Code"])  +"'";
            }


            //DS = objCom.EMRPT_InstructionWithEMRData(Criteria);
            EMR_PTMasterBAL objPTMast = new EMR_PTMasterBAL();

            DS = objPTMast.PTInstructionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursingInsttruction.DataSource = DS;
                gvNursingInsttruction.DataBind();
            }
            else
            {
                gvNursingInsttruction.DataBind();
            }


        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    BindNursingtInstruction();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      NursingOrderPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }
    }
}