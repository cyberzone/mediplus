﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.VisitDetails
{
    public partial class Procedures1 : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PROC' AND SEGMENT='PROC_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }




                drpSheduleHour.Items.Insert(index, Convert.ToString(strHour));
                drpSheduleHour.Items[index].Value = Convert.ToString(strHour);

                drpSheduleEndHour.Items.Insert(index, Convert.ToString(strHour));
                drpSheduleEndHour.Items[index].Value = Convert.ToString(strHour);




                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;


            drpSheduleMin.Items.Insert(0, Convert.ToString("00"));
            drpSheduleMin.Items[0].Value = Convert.ToString("00");
            drpSheduleEndMin.Items.Insert(0, Convert.ToString("00"));
            drpSheduleEndMin.Items[0].Value = Convert.ToString("00");


            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSheduleMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSheduleMin.Items[index].Value = Convert.ToString(j - intCount);

                drpSheduleEndMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSheduleEndMin.Items[index].Value = Convert.ToString(j - intCount);

                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet(drpServiceType.SelectedItem.Text, SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }

                if (GlobalValues.FileDescription == "ALNOOR")
                {
                    Criteria += "  AND EDF_TYPE='" + drpServiceType.SelectedValue + "' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
                }
                else
                {
                    Criteria += " AND EDF_TYPE='" + drpServiceType.SelectedValue + "' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "' )";
                }

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }


                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";
            if (GlobalValues.FileDescription == "ALNOOR")
            {
                Criteria += "  AND EDF_TYPE='" + drpServiceType.SelectedValue + "' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
            }
            else
            {
                Criteria += " AND EDF_TYPE='" + drpServiceType.SelectedValue + "' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";
            }

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            DS = objPro.ProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProcedure.DataSource = DS;
                gvProcedure.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvProcedure.Columns[0].Visible = false;

                }


                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvProcedure.Columns[0].Visible = false;
                }

                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvProcedure.Columns[0].Visible = false;
                }

            }
            else
            {
                gvProcedure.DataBind();
            }

            txtRemarks.Text = "";
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (Convert.ToString(DR["EPP_REMARKS"]) != "")
                    txtRemarks.Text = Convert.ToString(DR["EPP_REMARKS"]);
            }
        FunEnd: ;
        }

        Boolean CheckProcedures(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPP_DIAG_CODE='" + Code + "'";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS = new DataSet();
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            DS = objPro.ProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }


        void ClearProc()
        {
            hidProcCode.Value = "";
            hidProcCode.Value = "";
            txtRemarks.Text = "";
            txtQty.Text = "1";
            txtPrice.Text = "1";
            // txtProcNotes.Text = "";
        }

        void BindProcedureNotes()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            EMR_PTMasterBAL objPTMast = new EMR_PTMasterBAL();
            DS = objPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtProcNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_PROCEDURE_REMARKS"]));
            }

        }

        void UpdateProcedureNotes()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            string ProceNotes = txtProcNotes.Text.Replace("'", "''");
            string FieldNameWithValues = " EPM_PROCEDURE_REMARKS='" + ProceNotes + "'";

            CommonBAL objCom = new CommonBAL();
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);

        }


        void BindNurse()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND ( HSFM_CATEGORY= 'Nurse' OR HSFM_CATEGORY='Doctors') ";
            Criteria += "  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpWardNurse.DataSource = DS;
                drpWardNurse.DataTextField = "FullName";
                drpWardNurse.DataValueField = "HSFM_STAFF_ID";
                drpWardNurse.DataBind();

                for (int intCount = 0; intCount < drpWardNurse.Items.Count; intCount++)
                {
                    if (drpWardNurse.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpWardNurse.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForBoold;
                    }

                }
            ForBoold: ;

            }
            drpWardNurse.Items.Insert(0, "--- Select ---");
            drpWardNurse.Items[0].Value = "";



        }


        void BindPRocedureSchedul()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND EPPS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            // Criteria += " AND EPPS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            Criteria += " AND EPPS_ID IN (SELECT EPM_ID FROM EMR_PT_MASTER WHERE EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "')";

            Criteria += " AND EPPS_PRO_CODE='" + Convert.ToString(ViewState["ScheduleProCode"]) + "'";
            DataSet DS = new DataSet();
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            DS = objPro.ProcedureScheduleGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProSchedule.DataSource = DS;
                gvProSchedule.DataBind();

            }
            else
            {
                gvProSchedule.DataBind();
            }

        }

        Boolean CheckPassword(string UserID, string Password)
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_REMARK ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void ClearPhySchedule()
        {
            // txtSheduleDate.Text =
            drpSheduleHour.SelectedIndex = 0;
            drpSheduleMin.SelectedIndex = 0;

            drpSheduleEndHour.SelectedIndex = 0;
            drpSheduleEndMin.SelectedIndex = 0;

            txtSheduleComment.Text = "";
            lblSheduleMsg.Text = "";
            lblSheduleMsg.Visible = false;
        }

        Boolean CheckServiceAvailable()
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            ds = dbo.HaadServicessListGet(drpNewServiceType.SelectedItem.Text, txtNewServCode.Text.Trim(), "", Convert.ToString(Session["User_DeptID"]));

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void BindgvSOServicePopup(string HaadCode, out string ServiceID)
        {
            ServiceID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            if (Convert.ToString(Session["HPV_COMP_ID"]) != "")
            {
                Criteria += " AND EDMS_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EDMS_HAAD_CODE	='" + HaadCode + "' AND EDMS_COMP_ID = '" + Convert.ToString(Session["HPV_COMP_ID"]) + "' AND EDMS_DR_CODE ='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";
            }
            else
            {
                Criteria += " AND EDMS_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EDMS_HAAD_CODE	='" + HaadCode + "' AND ( EDMS_COMP_ID IS NULL OR  EDMS_COMP_ID ='')  AND EDMS_DR_CODE ='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";
            }
            DS = objCom.fnGetFieldValue("*", "EMR_DR_MAPPED_SERVICES", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["EDMS_SERV_ID"]);
            }
            else
            {
                if (Convert.ToString(Session["HPV_COMP_ID"]) != "")
                {
                    DS = new DataSet();
                    objCom = new CommonBAL();
                    Criteria = " HAS_COMP_ID='" + Convert.ToString(Session["HPV_COMP_ID"]) + "' AND  HSM_HAAD_CODE ='" + HaadCode + "'";

                    DS = objCom.ServiceMasterAgrementServiceGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (DS.Tables[0].Rows.Count > 1)
                        {
                            ServiceID = "";
                            gvServicePopup.DataSource = DS;
                            gvServicePopup.DataBind();
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSOServicePopup()", true);
                        }

                        if (DS.Tables[0].Rows.Count == 1)
                        {
                            ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                        }
                    }
                    else
                    {
                        DS = new DataSet();
                        objCom = new CommonBAL();
                        Criteria = " HSM_HAAD_CODE ='" + HaadCode + "'";

                        DS = objCom.ServiceMasterGet(Criteria);

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            if (DS.Tables[0].Rows.Count > 1)
                            {
                                ServiceID = "";
                                gvServicePopup.DataSource = DS;
                                gvServicePopup.DataBind();
                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSOServicePopup()", true);
                            }
                            if (DS.Tables[0].Rows.Count == 1)
                            {
                                ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                            }
                        }
                        else
                        {
                            ServiceID = HaadCode;
                        }
                    }
                }
                else
                {
                    DS = new DataSet();
                    objCom = new CommonBAL();
                    Criteria = " HSM_HAAD_CODE ='" + HaadCode + "'";

                    DS = objCom.ServiceMasterGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (DS.Tables[0].Rows.Count > 1)
                        {
                            ServiceID = "";
                            gvServicePopup.DataSource = DS;
                            gvServicePopup.DataBind();
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSOServicePopup()", true);
                        }
                        if (DS.Tables[0].Rows.Count == 1)
                        {
                            ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                        }
                    }
                    else
                    {
                        ServiceID = HaadCode;
                    }

                }
            }
        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='Procedure'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindHistory()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_ID   !='" + Convert.ToString(Session["EMR_ID"]) + "' ";


            DS = objCom.ProcedureHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.DataSource = DS;
                drpHistory.DataTextField = "Name";
                drpHistory.DataValueField = "Code";
                drpHistory.DataBind();
            }
            drpHistory.Items.Insert(0, "----Select History---");
            drpHistory.Items[0].Value = "0";


        }

        void BindProcedureHistory(string EMR_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";



            DataSet DS = new DataSet();
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            DS = objPro.ProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHistory.DataSource = DS;
                gvHistory.DataBind();

            }
            else
            {
                gvHistory.DataBind();
            }


        FunEnd: ;
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            } 
            
            if (Session["User_ID"] == null) { goto FunEnd; }

            lblMessage.Text = "";
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "Procedure")
            {
                BindData();
                txtSearch.Focus();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "ShowSearch();", true);



            }
            if (!IsPostBack)
            {
                try
                {
                    if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                    {

                        lblCapPassword.Visible = false;
                        txtWardNursePass.Visible = false;
                    }

                    BindScreenCustomization();

                    if (Convert.ToString(Session["HUM_ADDNEW_RECORD_FROMEMR"]) == "True")
                    {
                        btnNewService.Visible = true;
                    }


                    if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                    {
                        drpSrcType.SelectedIndex = 1;
                        btnDeleteFav.Visible = true;
                        btnAddFav.Visible = false;
                    }
                    else
                    {
                        drpSrcType.SelectedIndex = 0;
                        btnAddFav.Visible = true;
                        btnDeleteFav.Visible = false;
                    }

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtSheduleDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtSheduleEndDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    BindTime();
                    BindNurse();
                    BindData();
                    BindProcedure();
                    BindProcedureNotes();
                    BindTemplate();
                    BindHistory();
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnProcUpdate.Visible = false;


                    }


                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnProcUpdate.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;


                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnProcUpdate.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnProcUpdate.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_TEMPLATE"]) == "1")
                    {
                        divTemplate.Visible = true;
                    }


                    if (Convert.ToString(Session["EMR_DISPLAY_HISTORY"]) == "1")
                    {
                        divHistoryDrp.Visible = true;
                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        FunEnd: ;
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void Add_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                UpdateProcedureNotes();

                Int32 intPos = 1;
                if (gvProcedure.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvProcedure.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");


                if (CheckProcedures(lblCode.Text) == true)
                {
                    lblMessage.Text = "This code already added";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string ServiceID = "";

                BindgvSOServicePopup(lblCode.Text, out   ServiceID);



                if (ServiceID != "")
                {
                    EMR_PTProcedure objDiag = new EMR_PTProcedure();
                    objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objDiag.EPP_DIAG_CODE = lblCode.Text;
                    objDiag.EPP_DIAG_NAME = lblDesc.Text;
                    objDiag.EPP_COST = lblPrice.Text.Trim();
                    objDiag.EPP_REMARKS = txtRemarks.Text.Trim().Replace("'", "''"); ;
                    objDiag.EPP_QTY = txtQty.Text.Trim();
                    objDiag.EPP_PRICE = txtPrice.Text.Trim();
                    objDiag.EPP_ORDERTYPE = drpOrderType.SelectedValue;
                    objDiag.EPP_USEPRICE = txtPrice.Text.Trim();
                    objDiag.EPP_NOTES = "";
                    objDiag.EPP_TEMPLATE_CODE = "";
                    objDiag.Action = "A";
                    objDiag.EPP_SERV_ID = ServiceID;
                    objDiag.ProceduresAdd();
                }



                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPP_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_PROCEDURES", Criteria);

                BindProcedure();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }


        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            if (drpSrcType.SelectedIndex == 0)
            {
                btnAddFav.Visible = true;
                btnDeleteFav.Visible = false;
            }
            else
            {
                btnAddFav.Visible = false;
                btnDeleteFav.Visible = true;
            }
            BindData();
        FunEnd: ;
        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = drpServiceType.SelectedValue;
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = drpServiceType.SelectedValue;
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");


                EMR_PTProcedure objDiag = new EMR_PTProcedure();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_DIAG_CODE = lblDiagCode.Text;

                objDiag.ProceduresDelete();


                BindProcedure();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }


        protected void ProcEdit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");

                Label lblDiagRemaark = (Label)gvScanCard.Cells[0].FindControl("lblDiagRemaark");
                Label lblDiagQTY = (Label)gvScanCard.Cells[0].FindControl("lblDiagQTY");
                Label lblDiagUserPrice = (Label)gvScanCard.Cells[0].FindControl("lblDiagUserPrice");
                Label lblOrderType = (Label)gvScanCard.Cells[0].FindControl("lblOrderType");
                Label lblNotes = (Label)gvScanCard.Cells[0].FindControl("lblNotes");

                hidProcCode.Value = lblDiagCode.Text;
                txtRemarks.Text = lblDiagRemaark.Text;
                txtQty.Text = lblDiagQTY.Text;
                txtPrice.Text = lblDiagUserPrice.Text;
                //txtProcNotes.Text = lblNotes.Text;

                if (lblOrderType.Text != "")
                {

                    for (int intCount = 0; intCount < drpOrderType.Items.Count; intCount++)
                    {
                        if (drpOrderType.Items[intCount].Value == lblOrderType.Text)
                        {
                            drpOrderType.SelectedValue = lblOrderType.Text;

                        }
                    }
                }


                //EMR_PTProcedure objDiag = new EMR_PTProcedure();
                //objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                //objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                //objDiag.EPP_DIAG_CODE = lblDiagCode.Text;
                //objDiag.EPP_REMARKS = lblDiagRemaark.Text.Trim();
                //objDiag.EPP_QTY = lblDiagQTY.Text.Trim();
                //objDiag.EPP_ORDERTYPE = lblOrderType.Text;
                //objDiag.EPP_USEPRICE = lblDiagUserPrice.Text.Trim();
                //objDiag.EPP_NOTES = lblNotes.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void btnProcUpdate_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                UpdateProcedureNotes();

                if (hidProcCode.Value == "")
                {
                    goto FunEnd;
                }
                EMR_PTProcedure objDiag = new EMR_PTProcedure();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_DIAG_CODE = hidProcCode.Value;
                objDiag.EPP_REMARKS = txtRemarks.Text.Trim();
                objDiag.EPP_QTY = txtQty.Text.Trim();
                objDiag.EPP_ORDERTYPE = drpOrderType.SelectedValue;
                objDiag.EPP_USEPRICE = txtPrice.Text.Trim();
                objDiag.EPP_NOTES = "";
                objDiag.Action = "M";
                objDiag.ProceduresAdd();


                ClearProc();
                BindProcedure();



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }


        #endregion

        protected void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            CommonBAL objCom = new CommonBAL();
            string FieldNameWithValues = "EPP_REMARKS='" + txtRemarks.Text + "'";
            string Criteria = "EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_PROCEDURES", Criteria);

        FunEnd: ;
        }

        protected void txtProcNotes_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            UpdateProcedureNotes();

        FunEnd: ;
        }


        protected void ProSchedule_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                lblSheduleMsg.Text = "";

                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");
                Label lblDiagDesc = (Label)gvScanCard.Cells[0].FindControl("lblDiagDesc");

                string strMed = lblDiagDesc.Text;


                ViewState["ScheduleProCode"] = lblDiagCode.Text;

                lblSheMedicinCode.Text = lblDiagCode.Text;

                if (strMed.Length >= 100)
                {
                    lblSheMedicinDesc.Text = strMed.Substring(0, 100);
                }
                else
                {
                    lblSheMedicinDesc.Text = strMed;
                }
                BindPRocedureSchedul();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPhySchedule()", true);




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }


        protected void btnPhyScheduleAdd_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            lblSheduleMsg.Text = "";

            if (GlobalValues.FileDescription.ToUpper() != "SMCH")
            {
                if (CheckPassword(drpWardNurse.SelectedValue, txtWardNursePass.Text.Trim()) == false)
                {
                    lblSheduleMsg.Text = "Wrong Password";
                    lblSheduleMsg.Visible = true;
                    lblSheduleMsg.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
            }

            if (drpWardNurse.SelectedIndex == 0)
            {
                lblSheduleMsg.Text = "Select Nurse Name";
                lblSheduleMsg.Visible = true;
                goto FunEnd;
            }
            EMR_PTProcedure objPro = new EMR_PTProcedure();
            objPro.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPro.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPro.EPPS_PRO_CODE = Convert.ToString(ViewState["ScheduleProCode"]);

            objPro.EPPS_DATE = txtSheduleDate.Text + " " + drpSheduleHour.SelectedValue + ":" + drpSheduleMin.SelectedValue + ":00";
            objPro.EPPS_ENDTIME = txtSheduleEndDate.Text + " " + drpSheduleEndHour.SelectedValue + ":" + drpSheduleEndMin.SelectedValue + ":00";

            objPro.EPPS_GIVENBY = drpWardNurse.SelectedValue;
            objPro.EPPS_REMARKS = txtSheduleComment.Text;
            objPro.ProcedureScheduleAdd();

            ClearPhySchedule();
            BindPRocedureSchedul();
            lblSheduleMsg.Text = "Data Saved";
            lblSheduleMsg.Visible = true;
            lblSheduleMsg.ForeColor = System.Drawing.Color.Green;
        //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePhyScedule()", true);

        FunEnd: ;
        }


        protected void btnAddNewService_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {


                lblNewServStatus.Text = "";
                lblNewServStatus.Visible = false;

                if (txtNewServCode.Text == "" || txtNewServCode.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Code";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtNewServDesc.Text == "" || txtNewServDesc.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtNewProdName.Text == "" || txtNewProdName.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Product Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (CheckServiceAvailable() == true)
                {
                    lblNewServStatus.Text = "CPT  Already available";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.ServCode = txtNewServCode.Text.Trim();
                objCom.ServDesc = txtNewServDesc.Text;
                objCom.ProductName = txtNewProdName.Text;
                objCom.ServType = drpNewServiceType.SelectedValue;// "DRUG";

                if (drpNewServiceType.SelectedValue == "CPT")
                {
                    objCom.ServTypeValue = "3";
                }
                else if (drpNewServiceType.SelectedValue == "SERVICE")
                {
                    objCom.ServTypeValue = "8";
                }
                else if (drpNewServiceType.SelectedValue == "DRUG")
                {
                    objCom.ServTypeValue = "5";
                }

                objCom.UserID = Convert.ToString(Session["User_ID"]);

                objCom.HaadServiceManualAdd();

                lblNewServStatus.Text = "New CPT added";
                lblNewServStatus.Visible = true;
                lblNewServStatus.ForeColor = System.Drawing.Color.Green;

                txtNewServCode.Text = "";
                txtNewServDesc.Text = "";
                txtNewProdName.Text = "";
                drpNewServiceType.SelectedIndex = 0;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddNewService_Click");
                TextFileWriting(ex.Message.ToString());
            }


        FunEnd: ;
        }

        protected void ServicePopupEdit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                UpdateProcedureNotes();

                Int32 intPos = 1;
                if (gvProcedure.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvProcedure.Rows.Count) + 1;

                }

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblgvServiceID = (Label)gvScanCard.Cells[0].FindControl("lblgvServiceID");
                Label lblgvHAADCode = (Label)gvScanCard.Cells[0].FindControl("lblgvHAADCode");
                Label lblgvServiceName = (Label)gvScanCard.Cells[0].FindControl("lblgvServiceName");
                Label lblgvAmount = (Label)gvScanCard.Cells[0].FindControl("lblgvAmount");


                ViewState["ServicePopupSelIndex"] = gvScanCard.RowIndex;

                gvServicePopup.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                ViewState["SelectedHAADID"] = lblgvHAADCode.Text;
                ViewState["SelectedServiceName"] = lblgvServiceName.Text;
                ViewState["SelectedAmount"] = lblgvAmount.Text;
                ViewState["SelectedServiceID"] = lblgvServiceID.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnMapDataAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void btnMapDataAdd_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                UpdateProcedureNotes();


                EMR_PTProcedure objDiag = new EMR_PTProcedure();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_DIAG_CODE = Convert.ToString(ViewState["SelectedHAADID"]);
                objDiag.EPP_DIAG_NAME = Convert.ToString(ViewState["SelectedServiceName"]);
                objDiag.EPP_COST = Convert.ToString(ViewState["SelectedAmount"]);
                objDiag.EPP_REMARKS = txtRemarks.Text.Trim().Replace("'", "''"); ;
                objDiag.EPP_QTY = txtQty.Text.Trim();
                objDiag.EPP_PRICE = txtPrice.Text.Trim();
                objDiag.EPP_ORDERTYPE = drpOrderType.SelectedValue;
                objDiag.EPP_USEPRICE = txtPrice.Text.Trim();
                objDiag.EPP_NOTES = "";
                objDiag.EPP_TEMPLATE_CODE = "";
                objDiag.Action = "A";
                objDiag.EPP_SERV_ID = Convert.ToString(ViewState["SelectedServiceID"]);
                objDiag.ProceduresAdd();




                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPP_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_PROCEDURES", Criteria);

                BindProcedure();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideSOServicePopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnMapDataAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnAddToDB_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.HAADCode = Convert.ToString(ViewState["SelectedHAADID"]);
            objCom.ServCode = Convert.ToString(ViewState["SelectedServiceID"]);
            objCom.CompanyID = Convert.ToString(Session["HPV_COMP_ID"]);
            objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
            objCom.DRMappedServicesAdd();
        FunEnd: ;

        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "Procedure";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTProcedure objPhar = new EMR_PTProcedure();
                objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = TemplateCode;
                objPhar.ProceduresTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_PT_PROCEDURES_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='Procedure'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTProcedure objPro = new EMR_PTProcedure();
                    objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPro.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objPro.TemplateCode = drpTemplate.SelectedValue;
                    objPro.ProceduresAddFromTemplate();
                    BindProcedure();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void imgbtnDown_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {
                    EMR_PTProcedure objPhar = new EMR_PTProcedure();
                    objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.SelectedBranch_ID = BranchID;
                    objPhar.SelectedEMR_ID = strEMR_ID;

                    objPhar.ProcedureAddFromHistory();
                    BindProcedure();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void imgbtnPrescHistView_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {

                    BindProcedureHistory(strEMR_ID);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistory()", true);
                    //strRptPath = "../WebReports/Prescription.aspx";
                    //string rptcall = @strRptPath + "?EMR_ID=" + strEMR_ID + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + strDR_Code;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }
    }
}