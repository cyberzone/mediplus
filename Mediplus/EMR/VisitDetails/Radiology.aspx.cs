﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace EMR.VisitDetails
{
    public partial class Radiology1 : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_RAD' AND SEGMENT='RAD_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }


        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet("Radiology", SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }

                if (GlobalValues.FileDescription == "ALNOOR")
                {
                    Criteria += " AND EDF_TYPE='RAD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
                }
                else
                {
                    Criteria += " AND EDF_TYPE='RAD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";
                }

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;

                }

                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {

                    gvServicess.Columns[0].Visible = false;

                }

            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";
            if (GlobalValues.FileDescription == "ALNOOR")
            {
                Criteria += " AND EDF_TYPE='RAD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
            }
            else
            {
                Criteria += " AND EDF_TYPE='RAD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";
            }
            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindRadiology()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTRadiology objPro = new EMR_PTRadiology();
            DS = objPro.RadiologyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadiology.DataSource = DS;
                gvRadiology.DataBind();
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvRadiology.Columns[0].Visible = false;

                }

                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvRadiology.Columns[0].Visible = false;

                }

                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvRadiology.Columns[0].Visible = false;
                }
            }
            else
            {
                gvRadiology.DataBind();
            }

            txtRemarks.Text = "";
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (Convert.ToString(DR["EPR_REMARKS"]) != "")
                    txtRemarks.Text = Convert.ToString(DR["EPR_REMARKS"]);
            }

        FunEnd: ;
        }

        Boolean CheckRadiology(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPR_RAD_CODE='" + Code + "'";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS = new DataSet();
            EMR_PTRadiology objPro = new EMR_PTRadiology();
            DS = objPro.RadiologyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }


        void ClearProc()
        {
            hidProcCode.Value = "";
            hidProcCode.Value = "";
            txtRemarks.Text = "";
            txtQty.Text = "";

        }

        void BindgvSOServicePopup(string HaadCode, out string ServiceID)
        {
            ServiceID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            if (Convert.ToString(Session["HPV_COMP_ID"]) != "")
            {
                Criteria += " AND EDMS_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EDMS_HAAD_CODE	='" + HaadCode + "' AND EDMS_COMP_ID = '" + Convert.ToString(Session["HPV_COMP_ID"]) + "' AND EDMS_DR_CODE ='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";
            }
            else
            {
                Criteria += " AND EDMS_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EDMS_HAAD_CODE	='" + HaadCode + "' AND ( EDMS_COMP_ID IS NULL OR  EDMS_COMP_ID ='')  AND EDMS_DR_CODE ='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";
            }

            DS = objCom.fnGetFieldValue("*", "EMR_DR_MAPPED_SERVICES", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["EDMS_SERV_ID"]);
            }
            else
            {
                if (Convert.ToString(Session["HPV_COMP_ID"]) != "")
                {
                    DS = new DataSet();
                    objCom = new CommonBAL();
                    Criteria = " HAS_COMP_ID='" + Convert.ToString(Session["HPV_COMP_ID"]) + "' AND  HSM_HAAD_CODE ='" + HaadCode + "'";

                    DS = objCom.ServiceMasterAgrementServiceGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (DS.Tables[0].Rows.Count > 1)
                        {
                            ServiceID = "";
                            gvServicePopup.DataSource = DS;
                            gvServicePopup.DataBind();
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSOServicePopup()", true);
                        }

                        if (DS.Tables[0].Rows.Count == 1)
                        {
                            ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                        }
                    }
                    else
                    {
                        DS = new DataSet();
                        objCom = new CommonBAL();
                        Criteria = " HSM_HAAD_CODE ='" + HaadCode + "'";

                        DS = objCom.ServiceMasterGet(Criteria);

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            if (DS.Tables[0].Rows.Count > 1)
                            {
                                ServiceID = "";
                                gvServicePopup.DataSource = DS;
                                gvServicePopup.DataBind();
                                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSOServicePopup()", true);
                            }
                            if (DS.Tables[0].Rows.Count == 1)
                            {
                                ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                            }
                        }
                        else
                        {
                            ServiceID = HaadCode;
                        }
                    }
                }
                else
                {
                    DS = new DataSet();
                    objCom = new CommonBAL();
                    Criteria = " HSM_HAAD_CODE ='" + HaadCode + "'";

                    DS = objCom.ServiceMasterGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (DS.Tables[0].Rows.Count > 1)
                        {
                            ServiceID = "";
                            gvServicePopup.DataSource = DS;
                            gvServicePopup.DataBind();
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowSOServicePopup()", true);
                        }
                        if (DS.Tables[0].Rows.Count == 1)
                        {
                            ServiceID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);

                        }
                    }
                    else
                    {
                        ServiceID = HaadCode;
                    }

                }
            }
        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='Radiology'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindHistory()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_ID   !='" + Convert.ToString(Session["EMR_ID"]) + "' ";


            DS = objCom.RadiologyHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.DataSource = DS;
                drpHistory.DataTextField = "Name";
                drpHistory.DataValueField = "Code";
                drpHistory.DataBind();
            }
            drpHistory.Items.Insert(0, "----Select History---");
            drpHistory.Items[0].Value = "0";


        }

        void BindRadiologyHistory(string EMR_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPR_ID='" + EMR_ID + "'";



            DataSet DS = new DataSet();
            EMR_PTRadiology objPro = new EMR_PTRadiology();
            DS = objPro.RadiologyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHistory.DataSource = DS;
                gvHistory.DataBind();

            }
            else
            {
                gvHistory.DataBind();
            }


        FunEnd: ;
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }


            if (Session["User_ID"] == null) { goto FunEnd; }

            lblMessage.Text = "";
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "Radiology")
            {
                BindData();
                txtSearch.Focus();

            }

            if (!IsPostBack)
            {
                try
                {
                    BindTemplate();
                    BindHistory();
                    BindScreenCustomization();
                    if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                    {
                        drpSrcType.SelectedIndex = 1;
                        btnDeleteFav.Visible = true;
                        btnAddFav.Visible = false;
                    }
                    else
                    {
                        drpSrcType.SelectedIndex = 0;
                        btnAddFav.Visible = true;
                        btnDeleteFav.Visible = false;
                    }

                    BindData();
                    BindRadiology();

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;


                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_TEMPLATE"]) == "1")
                    {
                        divTemplate.Visible = true;
                    }


                    if (Convert.ToString(Session["EMR_DISPLAY_HISTORY"]) == "1")
                    {
                        divHistoryDrp.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        FunEnd: ;
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            if (drpSrcType.SelectedIndex == 0)
            {
                btnAddFav.Visible = true;
                btnDeleteFav.Visible = false;
            }
            else
            {
                btnAddFav.Visible = false;
                btnDeleteFav.Visible = true;
            }
            BindData();
        FunEnd: ;
        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "RAD";
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "RAD";
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }


        FunEnd: ;
        }


        protected void Add_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                Int32 intPos = 1;
                if (gvRadiology.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvRadiology.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");


                if (CheckRadiology(lblCode.Text) == true)
                {
                    lblMessage.Text = "This code already added";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                string ServiceID = "";

                BindgvSOServicePopup(lblCode.Text, out   ServiceID);



                if (ServiceID != "")
                {


                    EMR_PTRadiology objDiag = new EMR_PTRadiology();
                    objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objDiag.EPR_ID = Convert.ToString(Session["EMR_ID"]);
                    objDiag.EPR_RAD_CODE = lblCode.Text;
                    objDiag.EPR_RAD_NAME = lblDesc.Text;
                    objDiag.EPR_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                    objDiag.EPR_QTY = txtQty.Text.Trim();
                    objDiag.EPR_TEMPLATE_CODE = "";
                    objDiag.EPR_SERV_ID = ServiceID;
                    objDiag.RadiologyAdd();

                }


                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPR_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_RADIOLOGY", Criteria);

                BindRadiology();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");


                EMR_PTRadiology objDiag = new EMR_PTRadiology();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPR_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPR_RAD_CODE = lblDiagCode.Text;

                objDiag.RadiologyDelete();


                BindRadiology();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        #endregion

        protected void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            CommonBAL objCom = new CommonBAL();
            string FieldNameWithValues = "EPR_REMARKS='" + txtRemarks.Text + "'";
            string Criteria = "EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_RADIOLOGY", Criteria);

        FunEnd: ;
        }

        protected void ServicePopupEdit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblgvServiceID = (Label)gvScanCard.Cells[0].FindControl("lblgvServiceID");
                Label lblgvHAADCode = (Label)gvScanCard.Cells[0].FindControl("lblgvHAADCode");
                Label lblgvServiceName = (Label)gvScanCard.Cells[0].FindControl("lblgvServiceName");
                Label lblgvAmount = (Label)gvScanCard.Cells[0].FindControl("lblgvAmount");


                ViewState["ServicePopupSelIndex"] = gvScanCard.RowIndex;

                gvServicePopup.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                ViewState["SelectedHAADID"] = lblgvHAADCode.Text;
                ViewState["SelectedServiceName"] = lblgvServiceName.Text;
                ViewState["SelectedAmount"] = lblgvAmount.Text;
                ViewState["SelectedServiceID"] = lblgvServiceID.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnMapDataAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;


        }

        protected void btnMapDataAdd_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                EMR_PTRadiology objDiag = new EMR_PTRadiology();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPR_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPR_RAD_CODE = Convert.ToString(ViewState["SelectedHAADID"]);
                objDiag.EPR_RAD_NAME = Convert.ToString(ViewState["SelectedServiceName"]);
                objDiag.EPR_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                objDiag.EPR_QTY = txtQty.Text.Trim();
                objDiag.EPR_TEMPLATE_CODE = "";
                objDiag.EPR_SERV_ID = Convert.ToString(ViewState["SelectedServiceID"]);
                objDiag.RadiologyAdd();


                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPR_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_RADIOLOGY", Criteria);

                BindRadiology();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideSOServicePopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnMapDataAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnAddToDB_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.HAADCode = Convert.ToString(ViewState["SelectedHAADID"]);
            objCom.ServCode = Convert.ToString(ViewState["SelectedServiceID"]);
            objCom.CompanyID = Convert.ToString(Session["HPV_COMP_ID"]);
            objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
            objCom.DRMappedServicesAdd();
        FunEnd: ;

        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "Radiology";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTRadiology objPro = new EMR_PTRadiology();
                objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPro.EPR_ID = Convert.ToString(Session["EMR_ID"]);
                objPro.TemplateCode = TemplateCode;
                objPro.RadiologyTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPR_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_PT_RADIOLOGY_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='Radiology'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTRadiology objPro = new EMR_PTRadiology();
                    objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPro.EPR_ID = Convert.ToString(Session["EMR_ID"]);
                    objPro.TemplateCode = drpTemplate.SelectedValue;
                    objPro.RadiologyAddFromTemplate();
                    BindRadiology();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void imgbtnDown_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {
                    EMR_PTRadiology objPro = new EMR_PTRadiology();
                    objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPro.EPR_ID = Convert.ToString(Session["EMR_ID"]);
                    objPro.SelectedBranch_ID = BranchID;
                    objPro.SelectedEMR_ID = strEMR_ID;

                    objPro.RadiologyAddFromHistory();
                    BindRadiology();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void imgbtnPrescHistView_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {

                    BindRadiologyHistory(strEMR_ID);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistory()", true);
                    //strRptPath = "../WebReports/Prescription.aspx";
                    //string rptcall = @strRptPath + "?EMR_ID=" + strEMR_ID + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + strDR_Code;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Radiology.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }
    }
}