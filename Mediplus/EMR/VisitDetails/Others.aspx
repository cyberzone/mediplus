﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Others.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.Others1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
  <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        /*
          $(document).ready(function () {
              alert('test');
              $.ajax({
                  type: "POST",
                  url: "VisitDetails.aspx/SaveOthers",
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (response) {
                      var names = response.d;
                      alert(names);
                  },
                  failure: function (response) {
                      alert(response.d);
                  }
              });
          });
         
         */

        function CreateTempClick(Type) {
            var vcrTemp = document.getElementById("<%=hidCreateTempType.ClientID%>");
            vcrTemp.value = Type;

        }

</script>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <div>
    <input type="hidden" id="hidCreateTempType" runat="server" />
         <div id="divUltrasoundFindings" runat="server" visible="false">
        <table class="table spacy"  >
            <tr>
                <td class="lblCaption1"><label for="UltrasoundFindings">Ultrasound Findings</label></td>
            </tr>
            
            <tr>
                <td>
                    <asp:TextBox ID="txtUltrasoundFindings" runat="server" CssClass="label" TextMode="MultiLine" style="width:100%;height:100px;" AutoPostBack="true" OnTextChanged="txtUltrasoundFindings_TextChanged"></asp:TextBox>

                </td>
            </tr>
        </table>
    </div>

    <div id="TreatmentPlanContainer">
        
      
        <table class="table spacy">
            
            <tr>
                <td class="lblCaption1" ><label for="TreatmentPlan">Treatment Plan</label></td>                
            </tr>
            <tr>
                <td>
                     <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="button orange small" Text="Create Template" Enabled="false"    OnClientClick="CreateTempClick('TREATMENTPLAN');"
                                Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                             <asp:LinkButton ID="lnkDisplayTemp" runat="server" CssClass="button orange small" Text="Select Template" Enabled="false"   OnClientClick="CreateTempClick('TREATMENTPLAN');"
                                Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:TextBox ID="txtTreatmentPlan" runat="server" CssClass="label" TextMode="MultiLine" style="width:100%;height:100px;" AutoPostBack="true" OnTextChanged="txtTreatmentPlan_TextChanged"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="FollowupNotesContainer">
        <table class="table spacy"  >
            <tr>
                <td class="lblCaption1"><label for="FollowupNotes">Follow Up Notes</label></td>
            </tr>
            <tr>
                <td>
                     <asp:LinkButton ID="lnkSaveTemp1" runat="server" CssClass="button orange small" Text="Create Template" Enabled="false" OnClientClick="CreateTempClick('FOLLOWUPNOTES');"
                                Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                             <asp:LinkButton ID="lnkDisplayTemp1" runat="server" CssClass="button orange small" Text="Select Template" Enabled="false" OnClientClick="CreateTempClick('FOLLOWUPNOTES');"
                                Style="padding-left: 5px; padding-right: 5px; width: 100px; height: 30px; vertical-align: central;"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtFollowupNotes" runat="server" CssClass="label" TextMode="MultiLine" style="width:100%;height:100px;" AutoPostBack="true" OnTextChanged="txtFollowupNotes_TextChanged"></asp:TextBox>

                </td>
            </tr>
        </table>
    </div>

    <div id="divTreatmentOption" runat="server" visible="false">
        <table class="table spacy"  >
            <tr>
                <td class="lblCaption1"><label for="FollowupNotes">Treatment Option</label></td>
            </tr>
            
            <tr>
                <td>
                    <asp:TextBox ID="txtTreatmentOption" runat="server" CssClass="label" TextMode="MultiLine" style="width:100%;height:100px;" AutoPostBack="true" OnTextChanged="txtTreatmentOption_TextChanged"></asp:TextBox>

                </td>
            </tr>
        </table>
    </div>

    <div id="divEducation" runat="server" visible="false">
        <table class="table spacy"  >
            <tr>
                <td class="lblCaption1"><label for="FollowupNotes">Education</label></td>
            </tr>
            
            <tr>
                <td>
                    <asp:TextBox ID="txtEducation" runat="server" CssClass="label" TextMode="MultiLine" style="width:100%;height:100px;" AutoPostBack="true" OnTextChanged="txtEducation_TextChanged"></asp:TextBox>

                </td>
            </tr>
        </table>
    </div>
<asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkSaveTemp1"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Template List
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" style="width:250px;" />
                            </td>
                        </tr>
                        
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtender6" runat="server" TargetControlID="lnkDisplayTemp"
    PopupControlID="pnlDisplayTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnDisplayClose"
    PopupDragHandleControlID="pnlDisplayTemplate">
</asp:ModalPopupExtender>

<asp:Panel ID="pnlDisplayTemplate" runat="server" Height="500px" Width="300px" CssClass="modalPopup">
 <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnDisplayClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Template List
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 400px; vertical-align: top;">
                  <div style="padding-top: 0px; width: 100%;height:390px;   overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <asp:GridView ID="gvTemplates" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                    EnableModelValidation="True" Width="290px" ShowHeader="false"  >
                                    <HeaderStyle CssClass="GridHeader"   />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />

                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Width="90%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkName" runat="server" OnClick="TempSelect_Click">
                                                <asp:Label ID="lblTempCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_CODE") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblTempData" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_TEMPLATE") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lbTempName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=""  HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvDeleteTemp1" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="15" Width="15"
                                                OnClick="gvDeleteTemp1_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                      </div>
           </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnDisplayClose1" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="lnkDisplayTemp1"   
    PopupControlID="pnlDisplayTemplate1" BackgroundCssClass="modalBackground" CancelControlID="btnDisplayClose2"
    PopupDragHandleControlID="pnlDisplayTemplate1">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlDisplayTemplate1" runat="server" Height="500px" Width="300px" CssClass="modalPopup">
 <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnDisplayClose2" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Template List
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 400px; vertical-align: top;">
           <div style="padding-top: 0px; width: 100%;height:390px;   overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <asp:GridView ID="gvTemplates2" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                    EnableModelValidation="True" Width="290px" ShowHeader="false" >
                                    <HeaderStyle CssClass="GridHeader"   />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />

                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left"  >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkName" runat="server" OnClick="TempSelect_Click">
                                                <asp:Label ID="lblTempCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_CODE") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblTempData" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_TEMPLATE") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lbTempName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ET_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=""  HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvDeleteTemp2" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="15" Width="15"
                                                OnClick="gvDeleteTemp2_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                  </div>
           </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="Button2" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>

    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    </div>
    </form>
</body>
</html>

