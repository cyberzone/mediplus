﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NursingOrderPopup.aspx.cs" Inherits="Mediplus.EMR.VisitDetails.NursingOrderPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <fieldset>
            <legend class="lblCaption1" style="font-weight:bold;" >Nurse Instruction</legend>

          <asp:GridView ID="gvNursingInsttruction" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="99%">
                             <HeaderStyle CssClass="GridHeader" Height="20px" />
                            <RowStyle CssClass="GridRow" Height="20px" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="File No." HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPM_PT_ID") %>'></asp:Label>
                                         
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Patient Name" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPM_PT_NAME") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Doctor" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# Bind("EPM_DR_NAME") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Instruction">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblInsttruction" CssClass="label" runat="server" Text='<%# Bind("EPC_INSTTRUCTION") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Comment">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblComment" CssClass="label" runat="server" Text='<%# Bind("EPC_COMMENT") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>

                               <asp:TemplateField HeaderText="User Name">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblIUserName" CssClass="label" runat="server" Text='<%# Bind("HUM_USER_NAME") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>  
                                 <asp:TemplateField HeaderText="Time ">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblIUserName" CssClass="label" runat="server" Text='<%# Bind("EPC_TIME") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>                     
                    </Columns>

                </asp:GridView>

           </fieldset>
    </div>
    </form>
</body>
</html>
